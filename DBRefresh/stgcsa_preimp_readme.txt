Note: stgcsa_preimp_readme.txt provides information for importing both STGCSA and CDCONLINE schemas.
Depending on whether you want to import both these schemas or just one of them,
follow the instructions below:

1) Download required files:
For STGCSA you will need stgcsa_preimp.sql and STGCSA_AUG2020.zip.
For CDCONLINE you will need cdconline_preimp.sql and CDCONLINE_AUG2020.zip. 

2) Identify the disk location associated with Oracle Directory object DATA_PUMP_DIR 
with the following SQL statement:
select * from dba_directories where directory_name='DATA_PUMP_DIR';

3) Unzip STGCSA_AUG2020.zip and/or CDCONLINE_AUG2020.zip to the disk location named by DATA_PUMP_DIR
(Files STGCSA_AUG2020.DMP and/or CDCONLINE_AUG2020.DMP will be created.)

4) Copy stgcsa_preimp.sql and/or cdconline_preimp.sql to the disk location named by DATA_PUMP_DIR.

5) If you do not already have a tablespace called STGCSADATATBS and/or CDCONLINEDATATBS,
edit stgcsa_preimp.sql and/or cdconline_preimp.sql and follow instructions towards the top to identify 
a location for the tablespace, and uncomment the CREATE TABLESPACE statement and change the file name to
name the proper disk location.

6) Open a command prompt window and CD to the disk location named by DATA_PUMP_DIR.

7) Connect to your database as a DBA account using SQL*Plus, and run these scripts individually by entering:
@stgcsa_preimp.sql and/or @cdconline_preimp.sql.

RUNNING PREIMP.SQL MAY ISSUE MANY ERROR MESSAGES AS IT TRIES TO CREATE ACCOUNTS THAT MAY ALREADY EXIST 
AND/OR CLEAN UP OLD STUFF THAT MAY NOT EXIST. THIS IS OK.

If you did not have an STGCSA and/or CDCONLINE account, it will be created with the password CAREFUL.
 If you already had an STGCSA and/or CDCONLINE account, password should not have changed.

8) Exit sqlplus and from command prompt in same directory, run data pump import for each schema 
that you want to load. 
 
Note you must change the password if it is not CAREFUL, and if you need a "database alias" (TNS name)
to connect to your DB from the command line, include it where indicated.
 
impdp stgcsa/careful@[database_alias] directory=DATA_PUMP_DIR dumpfile=STGCSA_AUG2020 and/or
impdp cdconline/careful@[database_alias] directory=DATA_PUMP_DIR dumpfile=CDCONLINE_AUG2020

9) If you loaded the STGCSA schema, again run sqlplus and connect as STGCSA. Run:
exec stgcsa.stgcsa_resequence.

Note... the STGCSA schema contains PL/SQL procedures that use both STGCSA and CDCONLINE tables.
If you only load the STGCSA schema, or if it is loaded first, these procedures will be invalid
since the CDCONLINE objects were not found.  This is OK.  
If both schemas were loaded then invalid objects
should become valid if used. 



-- Do not edit
-- csa_preimp - Apr 2019 version 1.1 - J. Low, Select Computing
-- Use this script to prepare DB for import of CSA data pump .dmp file.
-- If tablespace does not already exist, uncomment "CREATE TABLESPACE" below.
-- Some statements will fail!
-- If you have an old copy of the schema, create use and role statements should fail
-- If your DB is new (no CSA schema) DROP TABLE statements will fail
--
--
-- Following check makes sure script is not being run against Mars!!!!
spool csa_preimp
whenever sqlerror exit
declare
  gn varchar2(100);
  cnt number;
begin
  select max(global_name), count(*) into gn, cnt from global_name
    where global_name like '%.SBA.GOV';
  if cnt>0 then
    raise_application_error(-20001,'Oops, looks like you are '
      ||'running on real SBA data base... dont do that!');
  end if;
end;
/
whenever sqlerror continue
--
--  ---  EDIT here if needed ---    1
--
-- uncomment and edit next "CREATE TABLESPACE" statement
-- if tablespace does not already exist
-- Use query: select * from dba_data_files; 
-- to see where other oracle data files reside

--create tablespace csadatatbs 
 --datafile '<somewhere>\DATAFILE\csadatatbs.dbf' size 10m autoextend on;



---For old copy of schema, following create statements will fail
create user csa identified by careful;
grant dba to csa;
alter user csa 
default tablespace csadatatbs quota unlimited on csadatatbs;
  
create role CSADEVROLE;                                                         
create role CSAREADALLROLE;                                                     
create role CSAUPDTROLE; 




---for new schema, following drop statements will fail
drop TABLE CSA.SOFVDBENTR2TBL cascade constraints;                              
drop TABLE CSA.SOFVBGABTBL cascade constraints;                                 
drop TABLE CSA.XOVER_PYMT110RPTARCHVTBL cascade constraints;                                                  
drop TABLE CSA.XOVER_SOFVWIREKEYTOTTBL cascade constraints;                     
drop TABLE CSA.SOFVWIREKEYTOTTBL cascade constraints;                           
drop TABLE CSA.SOFVDRTBL cascade constraints;                                   
drop TABLE CSA.PYMT110RPTARCHVTBL cascade constraints;                          
drop TABLE CSA.XOVER_SOFVDRTBL cascade constraints;                             
drop TABLE CSA.XOVER_STGCSA_DEL_KEYS cascade constraints;                       
drop TABLE CSA.XOVER_STATUS cascade constraints;                                
drop TABLE CSA.SOFVLND1TBL cascade constraints;                                 
drop TABLE CSA.SOFVLND6TBL cascade constraints;                                 
drop TABLE CSA.SOFVLNDYTBL cascade constraints;                                 
drop TABLE CSA.SOFVLNYRTBL cascade constraints;                                 
drop TABLE CSA.SOFVOTRNTBL cascade constraints;                                 
drop TABLE CSA.SOFVP99MTBL cascade constraints;                                 
drop TABLE CSA.SOFVPAYOUTTBL cascade constraints;                               
drop TABLE CSA.SOFVPREXTBL cascade constraints;                                 
drop TABLE CSA.SOFVPROPTBL cascade constraints;                                 
drop TABLE CSA.EXPIMPTESTTBL cascade constraints;                               
drop TABLE CSA.SOFVA99DTBL cascade constraints;                                 
drop TABLE CSA.SOFVA99MTBL cascade constraints;                                 
drop TABLE CSA.SOFVAUDTTBL cascade constraints;                                 
drop TABLE CSA.SOFVB99MTBL cascade constraints;                                 
drop TABLE CSA.SOFVBFFATBL cascade constraints;                                 
drop TABLE CSA.SOFVBGDFTBL cascade constraints;                                 
drop TABLE CSA.SOFVBGFWTBL cascade constraints;                                 
drop TABLE CSA.SOFVBGW9TBL cascade constraints;                                 
drop TABLE CSA.SOFVPYMLTBL cascade constraints;                                 
drop TABLE CSA.SOFVPYMTTBL cascade constraints;                                 
drop TABLE CSA.SOFVRTSCTBL cascade constraints;                                 
drop TABLE CSA.TEMPCMSTTBL cascade constraints;                                 
drop TABLE CSA.TEMPLNDBTBL cascade constraints;                                 
drop TABLE CSA.TEMPNPAYTBL cascade constraints;                                 
drop TABLE CSA.TEMPPAYPTBL cascade constraints;                                 
drop TABLE CSA.XOVER_SOFVPYMTTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVAUDTTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVBFFATBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVBGDFTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVBGW9TBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVCDCMSTRTBL cascade constraints;                        
drop TABLE CSA.XOVER_SOFVCTCHTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVDFPYTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVDUEBTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVFTPOTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVGNTYTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVGRGCTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVLND1TBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVOTRNTBL cascade constraints;                           
drop TABLE CSA.XOVER_SOFVRTSCTBL cascade constraints;                           
drop TABLE CSA.SOFVBGWLTBL cascade constraints;                                 
drop TABLE CSA.SOFVCD9DTBL cascade constraints;                                 
drop TABLE CSA.SOFVCD9MTBL cascade constraints;                                 
drop TABLE CSA.SOFVCDC7TBL cascade constraints;                                 
drop TABLE CSA.SOFVCDCMSTRTBL cascade constraints;                              
drop TABLE CSA.SOFVCHMDTBL cascade constraints;                                 
drop TABLE CSA.SOFVCTCHTBL cascade constraints;                                 
drop TABLE CSA.SOFVDBENTR1TBL cascade constraints;                              
drop TABLE CSA.SOFVDBENTR3TBL cascade constraints;                              
drop TABLE CSA.SOFVDFPYTBL cascade constraints;                                 
drop TABLE CSA.SOFVDUEBTBL cascade constraints;                                 
drop TABLE CSA.SOFVFFA8TBL cascade constraints;                                 
drop TABLE CSA.SOFVFTPOTBL cascade constraints;                                 
drop TABLE CSA.SOFVGNTYTBL cascade constraints;                                 
drop TABLE CSA.SOFVGRGCTBL cascade constraints;                                 
drop TABLE CSA.SOFVLC98TBL cascade constraints;                                 
drop TABLE CSA.SOFVLN98TBL cascade constraints;                                 
drop TABLE CSA.SOFVLNALTBL cascade constraints;                                 
drop TABLE CSA.SOFVLNASTBL cascade constraints;                             
    
purge tablespace CSADATATBS;
alter tablespace csadatatbs coalesce;

drop FUNCTION CSA.DATES_TO_CHAR1;                                               
drop FUNCTION CSA.FIX_CSA_DATES2;                                               
drop FUNCTION CSA.CSA_FORMAT_TIMESTAMP;                                         
drop FUNCTION CSA.FIX_CSA_DATES_PYMT3;                                          
drop FUNCTION CSA.FIX_CSA_RATE;                                                 
drop FUNCTION CSA.FIX_CSA_DOLLARS;                                              
drop FUNCTION CSA.DATES_TO_CHAR2;                                               
drop FUNCTION CSA.DATES_TO_CHAR4;                                               
drop FUNCTION CSA.FIX_CSA_DATES1;                                               
drop FUNCTION CSA.FIX_CSA_DATES4;                                               
drop FUNCTION CSA.DATES_TO_CHAR3;                                               
drop FUNCTION CSA.FIX_CSA_DATES3;                                               
drop FUNCTION CSA.FIX_YYMM_DATES;                                                
drop PROCEDURE CSA.XOVER_CSA_INITIALIZE_CSP;                                    
drop PROCEDURE CSA.XOVER_CSA_MERGE_CSP;                                         
drop VIEW CSA.SC_SOFVBFFATBL;                                                   
drop VIEW CSA.SC_SOFVCDCMSTRTBL;                                                
drop VIEW CSA.SC_SOFVBGW9TBL;                                                   
drop VIEW CSA.SC_SOFVDFPYTBL;                                                   
drop VIEW CSA.SC_SOFVDUEBTBL;                                                   
drop VIEW CSA.SC_SOFVFTPOTBL;                                                   
drop VIEW CSA.SC_SOFVGNTYTBL;                                                   
drop VIEW CSA.SC_SOFVLND1TBL;                                                   
drop VIEW CSA.SC_SOFVPYMTTBL;                                                   
drop VIEW CSA.SC_SOFVBGDFTBL;                                                   
drop VIEW CSA.SC_SOFVCTCHTBL;                                                   
drop VIEW CSA.SC_SOFVGRGCTBL;                                                   
drop VIEW CSA.SC_SOFVOTRNTBL;                                                   
drop VIEW CSA.SC_SOFVRTSCTBL;                                                   
drop VIEW CSA.XOVER_SOFVWIREKEYTOTTBL_R2V;                                      
drop VIEW CSA.XOVER_SOFVDRTBL_VIEW;                                             
drop VIEW CSA.XOVER_STATUS_CSA_V;                                               
drop VIEW CSA.XOVER_SOFVWIREKEYTOTTBL_REV2_VIEW;                                
drop VIEW CSA.XOVER_SOFVBGDFTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVBGW9TBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVCDCMSTRTBL_REV2_VIEW;                                   
drop VIEW CSA.XOVER_SOFVCTCHTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVDFPYTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVBFFATBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVDUEBTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVDRTBL_REV2_VIEW;                                        
drop VIEW CSA.XOVER_SOFVGNTYTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVGRGCTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVLND1TBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVOTRNTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVPYMTTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_SOFVRTSCTBL_REV2_VIEW;                                      
drop VIEW CSA.XOVER_PYMT110RPTARCHVTBL_R2V;                                     
drop VIEW CSA.XOVER_SOFVCDCMSTRTBL_VIEW;                                        
drop VIEW CSA.XOVER_SOFVAUDTTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVLND2TBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVRTSCTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVPYMTTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVCTCHTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVFTPOTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVGNTYTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVGRGCTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVLND1TBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVBGDFTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVDUEBTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVDFPYTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVBFFATBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVBGW9TBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVOTRNTBL_VIEW;                                           
drop VIEW CSA.XOVER_SOFVPROPTBL_VIEW;  

prompt all done!
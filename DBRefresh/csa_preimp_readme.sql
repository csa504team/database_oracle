1) Download required files:
For CSA you will need csa_preimp.sql and CSA_JUNE2020.zip.

2) Identify the disk location associated with Oracle Directory object DATA_PUMP_DIR 
with the following SQL statement:
select * from dba_directories where directory_name='DATA_PUMP_DIR';

3) Unzip CSA_JUNE2020.zip to the disk location named by DATA_PUMP_DIR
(File CSA_JUNE2020.DMP will be created.)

4) Copy csa_preimp.sql to the disk location named by DATA_PUMP_DIR.

5) If you do not already have a tablespace called CSADATATBS,
edit csa_preimp.sql and follow instructions towards the top to identify 
a location for the tablespace, and uncomment the CREATE TABLESPACE statement and change the file name to
name the proper disk location.

6) Open a command prompt window and CD to the disk location named by DATA_PUMP_DIR.

7) Connect to your database as a DBA account using SQL*Plus, and run the script by entering:
@csa_preimp.sql 

RUNNING PREIMP.SQL MAY ISSUE MANY ERROR MESSAGES AS IT TRIES TO CREATE ACCOUNTS THAT MAY ALREADY EXIST 
AND/OR CLEAN UP OLD STUFF THAT MAY NOT EXIST. THIS IS OK.

If you did not have a CSA account, it will be created with the password CAREFUL.
 If you already had an CSA account, password should not have changed.

8) Exit sqlplus and from command prompt in same directory, run data pump import for CSA schema to load.
Note you must change the password if it is not CAREFUL, and if you need a "database alias" (TNS name)
to connect to your DB from the command line, include it where indicated.
 
impdp csa/careful[@database_alias] directory=DATA_PUMP_DIR dumpfile=CSA_JUNE2020 




--  V1.2 14 Aug 2019, J. Low, J. Gorowada
--  Initialize output vars
  select ' ' x_status, ' ' x_time, ' ' xtime2, 
    ' ' stgcsa_init_dt, ' ' stgcsa_export_start_dt, ' ' CSA_MERGE_END_DT, 
    ' ' csa_init_dt, ' ' csa_export_start_dt, ' ' STGCSA_MERGE_END_DT
  from dual;   
set termout on  
--
-- now process all the info and produce a report
declare
  n number;
  err_step char(2):='  ';
  err_dt varchar2(20):='';
  err_msg varchar2(4000):='';
  
  err_step_name varchar2(80):='';
  msg_status varchar2(200);
  msg_state varchar2(80);
begin 
  -- overall status, is it between runs?
  msg_status:='Crossover &&onlinedb/&&batchdb is running since: &&x_init_fdt';
  if '&&S_XOVER_LAST_EVENT' = 'SM' then
    msg_status:='Crossover &&onlinedb/&&batchdb is not running.  Online DB open since: &&x_done_fdt';
    msg_state:='Online DB Open';
  elsif '&&S_XOVER_LAST_EVENT' = 'SI' then
    -- other side has status
    if '&&C_XOVER_LAST_EVENT' = 'CI'  
        and '&&STGCSA_INIT_RDT' > 
          greatest('&&CSA_INIT_RDT',nvl('&&S_ERR_RDT','19000101000000')) 
    then 
      -- batch has not seen latest crossover yet
        msg_state:='Batch DB awaiting incoming data load';  
    elsif '&&C_XOVER_LAST_EVENT' = 'SX' and '&&C_ERR_STEP' <= '  ' then 
      msg_state:='Batch DB now merging new data before opening';  
    elsif '&&C_XOVER_LAST_EVENT' = 'SX' and '&&C_ERR_STEP' = 'CM' then 
      msg_state:='Error merging new data from Online into Batch DB'; 
      ERR_STEP:='CM';
      ERR_msg:='&&C_ERR_MSG';
      err_dt:='&&C_ERR_DT';
    elsif '&&C_XOVER_LAST_EVENT' = 'CM'  then 
      msg_state:='Batch processing Open, opened at &&csa_merge_end_hdt'; 
    elsif '&&C_XOVER_LAST_EVENT' = 'CI'  
        and '&&STGCSA_INIT_RDT' <= '&&CSA_INIT_RDT' then    
      -- batch got latest crossover and initiated return, even if online clueless
      msg_state:='Online DB awaiting data return from batch which closed at &&csa_init_hdt'; 
    else  
      msg_state:='Error: Crossover status in conflict or an unclear error state'; 
      dbms_output.put_line('huh? S_XOVER_LAST_EVENT &&S_XOVER_LAST_EVENT');
      dbms_output.put_line('huh? C_XOVER_LAST_EVENT &&C_XOVER_LAST_EVENT');
      dbms_output.put_line('huh? STGCSA_INIT_RDT &&STGCSA_INIT_RDT');
      dbms_output.put_line('huh? CSA_INIT_RDT &&CSA_INIT_RDT');
      dbms_output.put_line('huh? S_ERR_RDT &&S_ERR_RDT');
    end if;  
  elsif '&&S_XOVER_LAST_EVENT' = 'CX' and '&&S_ERR_STEP' <= '  ' then 
      msg_state:='Online DB now merging returned data before opening';  
  elsif '&&S_XOVER_LAST_EVENT' = 'CX' and '&&S_ERR_STEP' = 'SM' then 
      msg_state:='Error merging returned data from Batch into Online DB';  
      ERR_STEP:='SM';
      ERR_msg:='&&S_ERR_MSG';
      err_dt:='&&S_ERR_DT';
  else
    msg_state:='Error: Crossover status in conflict or an unclear error state'; 
  end if;
  dbms_output.put_line(chr(13)||chr(10)||msg_status);
  dbms_output.put_line('@&&now: '||msg_state);
  if err_step>' ' then
    dbms_output.put_line('Error occurred in step '||err_step||' at '||err_dt);
    dbms_output.put_line('ErrMsg...');    
    dbms_output.put_line(err_msg);
  elsif msg_state='Error: Crossover status in conflict or an unclear error state'  then
    dbms_output.put_line('User Action:  Examine data in CSA/STGCSA.XOVER_STATUS'
      ||' tables to determine true last event and error.');
    dbms_output.put_line('Online DB (STGCSA) state:');  
    dbms_output.put_line('Last OK step:&&S_XOVER_LAST_EVENT'
      ||' Error step:&&S_ERR_STEP at &&S_ERR_DT ErrMsg:');
    dbms_output.put_line('&&S_ERR_MSG');  
    dbms_output.put_line('Batch DB (CSA) state:');  
    dbms_output.put_line('Last OK step:&&C_XOVER_LAST_EVENT'
      ||' Error step:&&C_ERR_STEP at &&C_ERR_DT');
    dbms_output.put_line('&&S_ERR_MSG');  
  end if;    
end;
/    

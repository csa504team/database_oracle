--  V1.2 14 Aug 2019, J. Low, J. Gorowada
SET SERVEROUT ON FORMAT WRAPPED
set termout on 
alter session set NLS_DATE_FORMAT = 'HH24:mi:ss';
declare
  online_open_time  varchar2(2000):='       Online DB open since:';
  online_close_time  varchar2(2000):='       Online DB closed at:';
  online_export_time  varchar2(2000):='       Online DB data exported at:';
  online_open_error  varchar2(2000):='  ';
  batch_open_time  varchar2(2000):='       Batch DB open since:';
  batch_close_time  varchar2(2000):='       Batch DB closed at:';
  batch_export_time  varchar2(2000):='       Batch DB data exported at:';
  batch_open_error  varchar2(2000):='  ';
  last_event varchar2(2000):='  ';
  msg varchar2(2000):='  ';
  xover_open_status varchar2(80);
  ctl_rpt  varchar2(2000);
  ctl_rpt2  varchar2(2000);
begin

  last_event:='&&XOVER_LAST_EVENT';
  ctl_rpt:='&&ctl_rpt';
  ctl_rpt2:='&&ctl_rpt2';
  msg:='Current / Prior crossover event times';

  if last_event='SI' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

  elsif last_event='SX' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

	online_export_time:='       Online DB data exported at: &&STGCSA_export_start_HDT';

	if '&&C_ERR_STEP'='CM' then

		last_event:='CM';

		batch_open_time:='Error: Batch DB merge failed at      &&C_ERR_DT';

		batch_open_error:='       Error occurred while merging data into Batch DB.';
		

	end if; 

  elsif last_event='CM' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

	online_export_time:='       Online DB data exported at: &&STGCSA_export_start_HDT';
  
	batch_open_time:='       Batch DB opened at:         &&CSA_MERGE_END_HDT';

  elsif last_event='CI' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

	online_export_time:='       Online DB data exported at: &&STGCSA_export_start_HDT';
  
	batch_open_time:='       Batch DB open since:        &&CSA_MERGE_END_HDT';

	batch_close_time:='       Batch DB closed at:         &&CSA_INIT_HDT';


  elsif last_event='CX' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

	online_export_time:='       Online DB data exported at: &&STGCSA_export_start_HDT';
	  
	batch_open_time:='       Batch DB opened at:         &&CSA_MERGE_END_HDT';

	batch_close_time:='       Batch DB closed at:         &&CSA_INIT_HDT';

	batch_export_time:='       Batch DB data exported at:  &&CSA_EXPORT_START_HDT';

    if '&&S_ERR_STEP'='SM' then

		last_event:='SM';

		online_open_time:='Error: Online DB merge failed at:  &&S_ERR_DT';

		online_open_error:='       Error occurred while merging data into Online DB.';
		
	end if;

  elsif last_event='SM' then

	online_close_time:='       Online DB closed at:        &&STGCSA_INIT_HDT';

	online_export_time:='       Online DB data exported at: &&STGCSA_export_start_HDT';
  
	batch_open_time:='       Batch DB opened at:         &&CSA_MERGE_END_HDT';

	batch_close_time:='       Batch DB closed at:         &&CSA_INIT_HDT';

	batch_export_time:='       Batch DB data exported at:  &&CSA_EXPORT_START_HDT';
  
	if &&ctl_cnt=0 then
	  online_open_time:='       Online DB open since:       &&STGCSA_MERGE_END_HDT';
	else 
	  online_open_error:='        Job control card(s) are not closed.';
	  online_open_time:='WARNING: "unclosed" job steps remain after merge &&S_ERR_DT';
	end if;  
  end if;  
  dbms_output.put_line(xover_open_status);
  dbms_output.put_line(msg);
  if('&&C_ERR_STEP'!=' ') then
	dbms_output.put_line(batch_open_error);
  elsif ('&&S_ERR_STEP'!=' ') then
	dbms_output.put_line(online_open_error);
  end if;
  dbms_output.put_line(online_close_time);
  dbms_output.put_line(online_export_time);
  dbms_output.put_line(batch_open_time);
  dbms_output.put_line(batch_close_time);
  dbms_output.put_line(batch_export_time);
  dbms_output.put_line(online_open_time);
  dbms_output.put_line(ctl_rpt); 
  dbms_output.put_line(ctl_rpt2); 

end;
/  
  
  
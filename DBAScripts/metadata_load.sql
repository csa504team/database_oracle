-- script to run on gen env (ORCL) to add table/column definitions into metadata
--   (REFTBLSTBL, REFCOLSTBL)
-- Script must be edited for owner/table name, default options
--
--desc stgcsa.reftblstbl
--desc stgcsa.refcolstbl
define ownr=CDCONLINE
define tbl=RPTSCHDLPODOLLRTBL
--
insert into stgcsa.reftblstbl
  select 
    owner, table_name, null, 'N','N','N','GLOBAL','N','Y','N','Y',null
  from dba_tables
  where owner='&&OWNR' and table_name='&&TBL'
    and not exists
    (select * from stgcsa.reftblstbl where sname='&&OWNR' and oname='&&TBL');
insert into stgcsa.refcolstbl
  select 
    owner, table_name, column_name, null, 'N','N','N','N',null,'GLOBAL','N',null,null,null,null,'N','N'    
  from dba_tab_columns  
  where owner='&&OWNR' and table_name='&&TBL'   
    and not exists
    (select * from stgcsa.refcolstbl where sname='&&OWNR' and oname='&&TBL' and cname=column_name);
    
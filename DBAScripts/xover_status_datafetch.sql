-- xover_status_datafetch v1.2 14 Aug 2019, J. Low, J. Gorowada
---------------  Configure settings below  -------------------------------
--  
accept username prompt "User to connect as -> " 
accept pw prompt "Password for &&username (same on both) -> " hide
define onlinedb=tstcafs1
define batchdb=tstcala1
-----------------  End of configuration part -----------------------
--
-- formatting, etc 
set verify off
set pagesize 0
--
-- XOVER_STATUS variables Naming sanity(?) (Multiple versions of these vars) 
-- S_ vars, data comes from STGCSA, C_ comes from CSA, 
--   "no prifix" vars are assigned from S_ or C_ depending 
-- SQLPLUSD "Variables" are all text, so for dates fetch in different formats
-- _DT is time only, _FDT is formatted Mon DD HH:MI:SS, _RDT (REAL) is yyyymmddhh24miss 
--
-- working stuff 
column x_status new_value x_status
column x_init_fdt new_value x_init_fdt
column x_done_fdt new_value x_done_fdt 
column now new_value now
--
-- stuff added for including ctlcard status
column  ctl_cnt new_value ctl_cnt
column ctl_header new_value ctl_header
column mfj_cnt new_value mfj_cnt
column mfj_creatdt new_value mfj_creatdt
column mfj_jobtypid new_value mfj_jobtypeid
column jq_cnt new_value jq_cnt
column jq_creatdt new_value jq_creatdt
column jq_jobtypcd new_value jq_jobtypecd
column jq_statcd new_value jq_statcd
column jq_commtxt new_value jq_commtxt
column ctl_rpt new_value ctl_rpt
column ctl_rpt2 new_value ctl_rpt2


 

--
-- column defs (vars) to get XOVER_STATUS values

column XOVER_LAST_EVENT new_value XOVER_LAST_EVENT                              
column C_XOVER_LAST_EVENT new_value C_XOVER_LAST_EVENT                          
column S_XOVER_LAST_EVENT new_value S_XOVER_LAST_EVENT                          
                                                                                
column CSA_INIT_HDT new_value CSA_INIT_HDT                                        
column CSA_INIT_RDT new_value CSA_INIT_RDT                                        
column C_CSA_INIT_DT new_value C_CSA_INIT_DT                                    
column S_CSA_INIT_DT new_value S_CSA_INIT_DT                                    
                                                                                
column CSA_MERGE_END_HDT new_value CSA_MERGE_END_HDT                              
column CSA_MERGE_END_RDT new_value CSA_MERGE_END_RDT                              
column C_CSA_MERGE_END_DT new_value C_CSA_MERGE_END_DT                          
column S_CSA_MERGE_END_DT new_value S_CSA_MERGE_END_DT                          
                                                                                
column CSA_EXPORT_START_HDT new_value CSA_EXPORT_START_HDT                        
column CSA_EXPORT_START_RDT new_value CSA_EXPORT_START_RDT                        
column C_CSA_EXPORT_START_DT new_value C_CSA_EXPORT_START_DT                    
column S_CSA_EXPORT_START_DT new_value S_CSA_EXPORT_START_DT                    
                                                                                
column STGCSA_INIT_HDT new_value STGCSA_INIT_HDT                                  
column STGCSA_INIT_RDT new_value STGCSA_INIT_RDT                                  
column C_STGCSA_INIT_DT new_value C_STGCSA_INIT_DT                              
column S_STGCSA_INIT_DT new_value S_STGCSA_INIT_DT                              
                                                                                
column STGCSA_MERGE_END_HDT new_value STGCSA_MERGE_END_HDT                        
column STGCSA_MERGE_END_RDT new_value STGCSA_MERGE_END_RDT                        
column C_STGCSA_MERGE_END_DT new_value C_STGCSA_MERGE_END_DT                    
column S_STGCSA_MERGE_END_DT new_value S_STGCSA_MERGE_END_DT                    
                                                                                
column STGCSA_EXPORT_START_HDT new_value STGCSA_EXPORT_START_HDT                  
column STGCSA_EXPORT_START_RDT new_value STGCSA_EXPORT_START_RDT                  
column C_STGCSA_EXPORT_START_DT new_value C_STGCSA_EXPORT_START_DT              
column S_STGCSA_EXPORT_START_DT new_value S_STGCSA_EXPORT_START_DT              
                                                                                
column STGCNT_SOFVBGW9TBL new_value STGCNT_SOFVBGW9TBL                          
column C_STGCNT_SOFVBGW9TBL new_value C_STGCNT_SOFVBGW9TBL                      
column S_STGCNT_SOFVBGW9TBL new_value S_STGCNT_SOFVBGW9TBL                      
                                                                                
column STGCNT_SOFVCDCMSTRTBL new_value STGCNT_SOFVCDCMSTRTBL                    
column C_STGCNT_SOFVCDCMSTRTBL new_value C_STGCNT_SOFVCDCMSTRTBL                
column S_STGCNT_SOFVCDCMSTRTBL new_value S_STGCNT_SOFVCDCMSTRTBL                
                                                                                
column STGCNT_SOFVCTCHTBL new_value STGCNT_SOFVCTCHTBL                          
column C_STGCNT_SOFVCTCHTBL new_value C_STGCNT_SOFVCTCHTBL                      
column S_STGCNT_SOFVCTCHTBL new_value S_STGCNT_SOFVCTCHTBL                      
                                                                                
column STGCNT_SOFVDFPYTBL new_value STGCNT_SOFVDFPYTBL                          
column C_STGCNT_SOFVDFPYTBL new_value C_STGCNT_SOFVDFPYTBL                      
column S_STGCNT_SOFVDFPYTBL new_value S_STGCNT_SOFVDFPYTBL                      
                                                                                
column STGCNT_SOFVDUEBTBL new_value STGCNT_SOFVDUEBTBL                          
column C_STGCNT_SOFVDUEBTBL new_value C_STGCNT_SOFVDUEBTBL                      
column S_STGCNT_SOFVDUEBTBL new_value S_STGCNT_SOFVDUEBTBL                      
                                                                                
column STGCNT_SOFVFTPOTBL new_value STGCNT_SOFVFTPOTBL                          
column C_STGCNT_SOFVFTPOTBL new_value C_STGCNT_SOFVFTPOTBL                      
column S_STGCNT_SOFVFTPOTBL new_value S_STGCNT_SOFVFTPOTBL                      
                                                                                
column STGCNT_SOFVGNTYTBL new_value STGCNT_SOFVGNTYTBL                          
column C_STGCNT_SOFVGNTYTBL new_value C_STGCNT_SOFVGNTYTBL                      
column S_STGCNT_SOFVGNTYTBL new_value S_STGCNT_SOFVGNTYTBL                      
                                                                                
column STGCNT_SOFVGRGCTBL new_value STGCNT_SOFVGRGCTBL                          
column C_STGCNT_SOFVGRGCTBL new_value C_STGCNT_SOFVGRGCTBL                      
column S_STGCNT_SOFVGRGCTBL new_value S_STGCNT_SOFVGRGCTBL                      
                                                                                
column STGCNT_SOFVLND1TBL new_value STGCNT_SOFVLND1TBL                          
column C_STGCNT_SOFVLND1TBL new_value C_STGCNT_SOFVLND1TBL                      
column S_STGCNT_SOFVLND1TBL new_value S_STGCNT_SOFVLND1TBL                      
                                                                                
column STGCNT_SOFVLND2TBL new_value STGCNT_SOFVLND2TBL                          
column C_STGCNT_SOFVLND2TBL new_value C_STGCNT_SOFVLND2TBL                      
column S_STGCNT_SOFVLND2TBL new_value S_STGCNT_SOFVLND2TBL                      
                                                                                
column STGCNT_SOFVOTRNTBL new_value STGCNT_SOFVOTRNTBL                          
column C_STGCNT_SOFVOTRNTBL new_value C_STGCNT_SOFVOTRNTBL                      
column S_STGCNT_SOFVOTRNTBL new_value S_STGCNT_SOFVOTRNTBL                      
                                                                                
column STGCNT_SOFVPYMTTBL new_value STGCNT_SOFVPYMTTBL                          
column C_STGCNT_SOFVPYMTTBL new_value C_STGCNT_SOFVPYMTTBL                      
column S_STGCNT_SOFVPYMTTBL new_value S_STGCNT_SOFVPYMTTBL                      
                                                                                
column STGCNT_SOFVRTSCTBL new_value STGCNT_SOFVRTSCTBL                          
column C_STGCNT_SOFVRTSCTBL new_value C_STGCNT_SOFVRTSCTBL                      
column S_STGCNT_SOFVRTSCTBL new_value S_STGCNT_SOFVRTSCTBL                      
                                                                                
column STGCNT_SOFVAUDTTBL new_value STGCNT_SOFVAUDTTBL                          
column C_STGCNT_SOFVAUDTTBL new_value C_STGCNT_SOFVAUDTTBL                      
column S_STGCNT_SOFVAUDTTBL new_value S_STGCNT_SOFVAUDTTBL                      
                                                                                
column STGCNT_SOFVBFFATBL new_value STGCNT_SOFVBFFATBL                          
column C_STGCNT_SOFVBFFATBL new_value C_STGCNT_SOFVBFFATBL                      
column S_STGCNT_SOFVBFFATBL new_value S_STGCNT_SOFVBFFATBL                      
                                                                                
column STGCNT_SOFVBGDFTBL new_value STGCNT_SOFVBGDFTBL                          
column C_STGCNT_SOFVBGDFTBL new_value C_STGCNT_SOFVBGDFTBL                      
column S_STGCNT_SOFVBGDFTBL new_value S_STGCNT_SOFVBGDFTBL                      
                                                                                
column STGCNT_SOFVDRTBL new_value STGCNT_SOFVDRTBL                              
column C_STGCNT_SOFVDRTBL new_value C_STGCNT_SOFVDRTBL                          
column S_STGCNT_SOFVDRTBL new_value S_STGCNT_SOFVDRTBL                          
                                                                                
column CSACNT_SOFVBGW9TBL new_value CSACNT_SOFVBGW9TBL                          
column C_CSACNT_SOFVBGW9TBL new_value C_CSACNT_SOFVBGW9TBL                      
column S_CSACNT_SOFVBGW9TBL new_value S_CSACNT_SOFVBGW9TBL                      
                                                                                
column CSACNT_SOFVCDCMSTRTBL new_value CSACNT_SOFVCDCMSTRTBL                    
column C_CSACNT_SOFVCDCMSTRTBL new_value C_CSACNT_SOFVCDCMSTRTBL                
column S_CSACNT_SOFVCDCMSTRTBL new_value S_CSACNT_SOFVCDCMSTRTBL                
                                                                                
column CSACNT_SOFVCTCHTBL new_value CSACNT_SOFVCTCHTBL                          
column C_CSACNT_SOFVCTCHTBL new_value C_CSACNT_SOFVCTCHTBL                      
column S_CSACNT_SOFVCTCHTBL new_value S_CSACNT_SOFVCTCHTBL                      
                                                                                
column CSACNT_SOFVDFPYTBL new_value CSACNT_SOFVDFPYTBL                          
column C_CSACNT_SOFVDFPYTBL new_value C_CSACNT_SOFVDFPYTBL                      
column S_CSACNT_SOFVDFPYTBL new_value S_CSACNT_SOFVDFPYTBL                      
                                                                                
column CSACNT_SOFVDUEBTBL new_value CSACNT_SOFVDUEBTBL                          
column C_CSACNT_SOFVDUEBTBL new_value C_CSACNT_SOFVDUEBTBL                      
column S_CSACNT_SOFVDUEBTBL new_value S_CSACNT_SOFVDUEBTBL                      
                                                                                
column CSACNT_SOFVFTPOTBL new_value CSACNT_SOFVFTPOTBL                          
column C_CSACNT_SOFVFTPOTBL new_value C_CSACNT_SOFVFTPOTBL                      
column S_CSACNT_SOFVFTPOTBL new_value S_CSACNT_SOFVFTPOTBL                      
                                                                                
column CSACNT_SOFVGNTYTBL new_value CSACNT_SOFVGNTYTBL                          
column C_CSACNT_SOFVGNTYTBL new_value C_CSACNT_SOFVGNTYTBL                      
column S_CSACNT_SOFVGNTYTBL new_value S_CSACNT_SOFVGNTYTBL                      
                                                                                
column CSACNT_SOFVGRGCTBL new_value CSACNT_SOFVGRGCTBL                          
column C_CSACNT_SOFVGRGCTBL new_value C_CSACNT_SOFVGRGCTBL                      
column S_CSACNT_SOFVGRGCTBL new_value S_CSACNT_SOFVGRGCTBL                      
                                                                                
column CSACNT_SOFVLND1TBL new_value CSACNT_SOFVLND1TBL                          
column C_CSACNT_SOFVLND1TBL new_value C_CSACNT_SOFVLND1TBL                      
column S_CSACNT_SOFVLND1TBL new_value S_CSACNT_SOFVLND1TBL                      
                                                                                
column CSACNT_SOFVLND2TBL new_value CSACNT_SOFVLND2TBL                          
column C_CSACNT_SOFVLND2TBL new_value C_CSACNT_SOFVLND2TBL                      
column S_CSACNT_SOFVLND2TBL new_value S_CSACNT_SOFVLND2TBL                      
                                                                                
column CSACNT_SOFVOTRNTBL new_value CSACNT_SOFVOTRNTBL                          
column C_CSACNT_SOFVOTRNTBL new_value C_CSACNT_SOFVOTRNTBL                      
column S_CSACNT_SOFVOTRNTBL new_value S_CSACNT_SOFVOTRNTBL                      
                                                                                
column CSACNT_SOFVPYMTTBL new_value CSACNT_SOFVPYMTTBL                          
column C_CSACNT_SOFVPYMTTBL new_value C_CSACNT_SOFVPYMTTBL                      
column S_CSACNT_SOFVPYMTTBL new_value S_CSACNT_SOFVPYMTTBL                      
                                                                                
column CSACNT_SOFVRTSCTBL new_value CSACNT_SOFVRTSCTBL                          
column C_CSACNT_SOFVRTSCTBL new_value C_CSACNT_SOFVRTSCTBL                      
column S_CSACNT_SOFVRTSCTBL new_value S_CSACNT_SOFVRTSCTBL                      
                                                                                
column CSACNT_SOFVAUDTTBL new_value CSACNT_SOFVAUDTTBL                          
column C_CSACNT_SOFVAUDTTBL new_value C_CSACNT_SOFVAUDTTBL                      
column S_CSACNT_SOFVAUDTTBL new_value S_CSACNT_SOFVAUDTTBL                      
                                                                                
column CSACNT_SOFVBFFATBL new_value CSACNT_SOFVBFFATBL                          
column C_CSACNT_SOFVBFFATBL new_value C_CSACNT_SOFVBFFATBL                      
column S_CSACNT_SOFVBFFATBL new_value S_CSACNT_SOFVBFFATBL                      
                                                                                
column CSACNT_SOFVBGDFTBL new_value CSACNT_SOFVBGDFTBL                          
column C_CSACNT_SOFVBGDFTBL new_value C_CSACNT_SOFVBGDFTBL                      
column S_CSACNT_SOFVBGDFTBL new_value S_CSACNT_SOFVBGDFTBL                      
                                                                                
column CSACNT_SOFVDRTBL new_value CSACNT_SOFVDRTBL                              
column C_CSACNT_SOFVDRTBL new_value C_CSACNT_SOFVDRTBL                          
column S_CSACNT_SOFVDRTBL new_value S_CSACNT_SOFVDRTBL                          
                                                                                
column ERR_STEP new_value ERR_STEP                                              
column C_ERR_STEP new_value C_ERR_STEP                                          
column S_ERR_STEP new_value S_ERR_STEP                                          
                                                                                
column ERR_DT new_value ERR_DT                                                  
column C_ERR_DT new_value C_ERR_DT                                              
column C_ERR_FDT new_value C_ERR_FDT                                              
column C_ERR_RDT new_value C_ERR_RDT                                              
column S_ERR_DT new_value S_ERR_DT                                              
column S_ERR_FDT new_value S_ERR_FDT                                              
column S_ERR_RDT new_value S_ERR_RDT                                              
                                                                                
column ERR_MSG new_value ERR_MSG                                                
column C_ERR_MSG new_value C_ERR_MSG                                            
column S_ERR_MSG new_value S_ERR_MSG    

--
-- Get data from batch
-- The important thing about these selects is the column aliases (must) match
-- the SQL*Plus column name definitions above
set termout off
set feedback off
connect &&username/&&pw@&&batchdb

alter session set nls_date_format='hh24:mi:ss';

      

 
select
    sysdate now,
    xover_last_event, 
    XOVER_LAST_EVENT  C_XOVER_LAST_EVENT,                                       
    CSA_INIT_DT CSA_INIT_HDT,
    to_char(CSA_INIT_DT,'YYYYMMDDHH24MISS') CSA_INIT_RDT,                                                                 
    CSA_MERGE_END_DT CSA_MERGE_END_HDT,
    to_char(CSA_MERGE_END_DT,'YYYYMMDDHH24MISS') CSA_MERGE_END_RDT,                        
    CSA_MERGE_END_DT  C_CSA_MERGE_END_DT,                                       
    to_char(STGCSA_EXPORT_START_DT,'YYYYMMDDHH24MISS') CSA_EXPORT_START_RDT,                        
    CSA_EXPORT_START_DT  C_CSA_EXPORT_START_DT,                                 
    STGCSA_INIT_DT  C_STGCSA_INIT_DT,                                           
    STGCSA_MERGE_END_DT  C_STGCSA_MERGE_END_DT,     
    STGCSA_EXPORT_START_DT STGCSA_EXPORT_START_HDT,
    STGCSA_EXPORT_START_DT  C_STGCSA_EXPORT_START_DT,   
    STGCNT_SOFVBGW9TBL  C_STGCNT_SOFVBGW9TBL,                                   
    STGCNT_SOFVCDCMSTRTBL  C_STGCNT_SOFVCDCMSTRTBL,                             
    STGCNT_SOFVCTCHTBL  C_STGCNT_SOFVCTCHTBL,                                   
    STGCNT_SOFVDFPYTBL  C_STGCNT_SOFVDFPYTBL,                                   
    STGCNT_SOFVDUEBTBL  C_STGCNT_SOFVDUEBTBL,                                   
    STGCNT_SOFVFTPOTBL  C_STGCNT_SOFVFTPOTBL,                                   
    STGCNT_SOFVGNTYTBL  C_STGCNT_SOFVGNTYTBL,                                   
    STGCNT_SOFVGRGCTBL  C_STGCNT_SOFVGRGCTBL,                                   
    STGCNT_SOFVLND1TBL  C_STGCNT_SOFVLND1TBL,                                   
    STGCNT_SOFVLND2TBL  C_STGCNT_SOFVLND2TBL,                                   
    STGCNT_SOFVOTRNTBL  C_STGCNT_SOFVOTRNTBL,                                   
    STGCNT_SOFVPYMTTBL  C_STGCNT_SOFVPYMTTBL,                                   
    STGCNT_SOFVRTSCTBL  C_STGCNT_SOFVRTSCTBL,                                   
    STGCNT_SOFVAUDTTBL  C_STGCNT_SOFVAUDTTBL,                                   
    STGCNT_SOFVBFFATBL  C_STGCNT_SOFVBFFATBL,                                   
    STGCNT_SOFVBGDFTBL  C_STGCNT_SOFVBGDFTBL,                                   
    STGCNT_SOFVDRTBL  C_STGCNT_SOFVDRTBL,                                       
    CSACNT_SOFVBGW9TBL  C_CSACNT_SOFVBGW9TBL,                                   
    CSACNT_SOFVCDCMSTRTBL  C_CSACNT_SOFVCDCMSTRTBL,                             
    CSACNT_SOFVCTCHTBL  C_CSACNT_SOFVCTCHTBL,                                   
    CSACNT_SOFVDFPYTBL  C_CSACNT_SOFVDFPYTBL,                                   
    CSACNT_SOFVDUEBTBL  C_CSACNT_SOFVDUEBTBL,                                   
    CSACNT_SOFVFTPOTBL  C_CSACNT_SOFVFTPOTBL,                                   
    CSACNT_SOFVGNTYTBL  C_CSACNT_SOFVGNTYTBL,                                   
    CSACNT_SOFVGRGCTBL  C_CSACNT_SOFVGRGCTBL,                                   
    CSACNT_SOFVLND1TBL  C_CSACNT_SOFVLND1TBL,                                   
    CSACNT_SOFVLND2TBL  C_CSACNT_SOFVLND2TBL,                                   
    CSACNT_SOFVOTRNTBL  C_CSACNT_SOFVOTRNTBL,                                   
    CSACNT_SOFVPYMTTBL  C_CSACNT_SOFVPYMTTBL,                                   
    CSACNT_SOFVRTSCTBL  C_CSACNT_SOFVRTSCTBL,                                   
    CSACNT_SOFVAUDTTBL  C_CSACNT_SOFVAUDTTBL,                                   
    CSACNT_SOFVBFFATBL  C_CSACNT_SOFVBFFATBL,                                   
    CSACNT_SOFVBGDFTBL  C_CSACNT_SOFVBGDFTBL,                                   
    CSACNT_SOFVDRTBL  C_CSACNT_SOFVDRTBL,                                       
    nvl(ERR_STEP,'  ')  C_ERR_STEP,                                                       
    ERR_DT  C_ERR_DT,                                                           
    to_char(ERR_DT,'YYYYMMDDHH24MISS') C_ERR_RDT,
    to_char(ERR_DT,'Mon DD HH24MISS') C_ERR_FDT,
    ERR_MSG  C_ERR_MSG        
  from csa.xover_status;   
  
--
-- Get data from online/STGCSA (already have BACTH/CSA status/times in vars
set termout off
connect &&username/&&pw@&&onlinedb
alter session set nls_date_format='hh24:mi:ss';
set serveroutput on
--set feedback on
s--et termout on
--
-- select info about MFJOB and JOBQUE skybot/rocess control tables
--
-- get info about job control cards.  DB open with incomplete jcc is an error
with 
  last_jq as (select * from STGCSA.JOBQUETBL where seqnmb=
            (select max(seqnmb) from STGCSA.JOBQUETBL where enddt is not null)),
  last_mfj as (select * from STGCSA.mfjobTBL where jobid=
            (select max(jobid) from STGCSA.mfjobTBL where completedt is not null))         
select mfj_cnt + jq_cnt ctl_cnt, mfj_cnt, mfj_creatdt, mfj_jobtypeid,
    jq_cnt, jq_creatdt, jq_jobtypcd, jq_statcd, jq_commtxt,
    case 
      when mfj_cnt + jq_cnt = 0 then '       No unclosed job/que steps'
      else chr(13)||chr(10)||(mfj_cnt + jq_cnt)||' Open CTL Cards:'
        ||chr(13)||chr(10)||'Table: Started:       Code(s):  User:' 
        ||decode(mfj_creatdt,null,'',chr(13)||chr(10)||'MFJOB  ')
        ||to_char(mfj_creatdt,'mm/dd hh24:mi:ss')||' '
        ||mfj_jobtypeid
        ||decode(jq_creatdt,null,'',chr(13)||chr(10)||'JOBQUE ')
        ||to_char(jq_creatdt,'mm/dd hh24:mi:ss')||' '
        ||decode(jq_jobtypcd,null,'',rpad(jq_jobtypcd||'('||jq_statcd||')',10))||jq_userid
        ||decode(jq_commtxt,null,'',chr(13)||chr(10)||' CMNT: '||jq_commtxt)
      end ctl_rpt,
      chr(13)||chr(10)||'Most recently closed cards:'
        ||chr(13)||chr(10)
        ||'Table: Started:       Ended:         Code(s):  User:'
        ||chr(13)||chr(10)||'MFJOB  '||to_char(last_mfj.creatdt,'mm/dd hh24:mi:ss')||' '
        ||to_char(last_mfj.completedt,'mm/dd hh24:mi:ss')||' '
        ||last_mfj.currentjobtypeid 
        ||chr(13)||chr(10)||'JOBQUE '||to_char(last_jq.creatdt,'mm/dd hh24:mi:ss')||' '
        ||to_char(last_jq.enddt,'mm/dd hh24:mi:ss')||' '
        ||rpad(last_jq.jobtypcd||'('||last_jq.statcd||')',10)||rpad(last_jq.creatuserid,14)||' ' 
        ||decode(last_jq.commtxt,null,'',chr(13)||chr(10)||' CMNT: '||last_jq.commtxt) ctl_rpt2
  from last_jq, last_mfj,
    (select count(*) mfj_cnt, max(creatdt) mfj_creatdt, 
        max(currentjobtypeid) mfj_jobtypeid  
      from stgcsa.mfjobtbl where completedt is null) mfj_open,
    (select count(*) jq_cnt, max(creatdt) jq_creatdt, 
        max(jobtypcd) jq_jobtypcd, max(statcd) jq_statcd, 
        max(creatuserid)jq_userid, max(commtxt) jq_commtxt  
      from stgcsa.jobquetbl where enddt is null) jq_open;
--
--
select
    XOVER_LAST_EVENT  S_XOVER_LAST_EVENT,    
    -- get unified status, tricky (already have BATCH/CSA status, is it right?)
    case '&&C_XOVER_LAST_EVENT'
      -- When CSA status CI then CSA does not know status, look at STGCSA
      when 'CI' then
        case 
          -- when STGCSA status=SI then cant tell if real status CI or SI need most recent
          when XOVER_LAST_EVENT='SI'   
            and stgcsa_init_dt >=to_date('&&CSA_INIT_RDT','YYYYMMDDHH24MISS') 
          then 
            'SI'
          when XOVER_LAST_EVENT='SI'   
            and stgcsa_init_dt <to_date('&&CSA_INIT_RDT','YYYYMMDDHH24MISS')
          then 
            'CI'
          else xover_last_event
        end 
      else '&&C_XOVER_LAST_EVENT'
    end xover_last_event,        
    CSA_INIT_DT  S_CSA_INIT_DT,                                                 
    CSA_MERGE_END_DT  S_CSA_MERGE_END_DT,                                       
    CSA_EXPORT_START_DT  S_CSA_EXPORT_START_DT,                                 
    CSA_EXPORT_START_DT CSA_EXPORT_START_HDT,
    STGCSA_INIT_DT STGCSA_INIT_HDT,  
    to_char(STGCSA_INIT_DT,'YYYYMMDDHH24MISS') STGCSA_INIT_RDT,                                                                 
    to_char(stgcsa_init_dt,'Mon DD hh24:mi:ss') x_init_fdt,                                                 
    STGCSA_INIT_DT  S_STGCSA_INIT_DT,                                           
    STGCSA_MERGE_END_DT STGCSA_MERGE_END_HDT,
    to_char(STGCSA_MERGE_END_DT,'YYYYMMDDHH24MISS') STGCSA_MERGE_END_RDT,                        
    to_char(stgcsa_merge_end_dt,'Mon DD hh24:mi:ss') x_done_fdt,                             
    STGCSA_MERGE_END_DT  S_STGCSA_MERGE_END_DT,                                 
    to_char(STGCSA_EXPORT_START_DT,'YYYYMMDDHH24MISS') STGCSA_EXPORT_START_RDT,                        
    STGCSA_EXPORT_START_DT  S_STGCSA_EXPORT_START_DT,                           
    STGCNT_SOFVBGW9TBL  S_STGCNT_SOFVBGW9TBL,                                   
    STGCNT_SOFVCDCMSTRTBL  S_STGCNT_SOFVCDCMSTRTBL,                             
    STGCNT_SOFVCTCHTBL  S_STGCNT_SOFVCTCHTBL,                                   
    STGCNT_SOFVDFPYTBL  S_STGCNT_SOFVDFPYTBL,                                   
    STGCNT_SOFVDUEBTBL  S_STGCNT_SOFVDUEBTBL,                                   
    STGCNT_SOFVFTPOTBL  S_STGCNT_SOFVFTPOTBL,                                   
    STGCNT_SOFVGNTYTBL  S_STGCNT_SOFVGNTYTBL,                                   
    STGCNT_SOFVGRGCTBL  S_STGCNT_SOFVGRGCTBL,                                   
    STGCNT_SOFVLND1TBL  S_STGCNT_SOFVLND1TBL,                                   
    STGCNT_SOFVLND2TBL  S_STGCNT_SOFVLND2TBL,                                   
    STGCNT_SOFVOTRNTBL  S_STGCNT_SOFVOTRNTBL,                                   
    STGCNT_SOFVPYMTTBL  S_STGCNT_SOFVPYMTTBL,                                   
    STGCNT_SOFVRTSCTBL  S_STGCNT_SOFVRTSCTBL,                                   
    STGCNT_SOFVAUDTTBL  S_STGCNT_SOFVAUDTTBL,                                   
    STGCNT_SOFVBFFATBL  S_STGCNT_SOFVBFFATBL,                                   
    STGCNT_SOFVBGDFTBL  S_STGCNT_SOFVBGDFTBL,                                   
    STGCNT_SOFVDRTBL  S_STGCNT_SOFVDRTBL,                                       
    CSACNT_SOFVBGW9TBL  S_CSACNT_SOFVBGW9TBL,                                   
    CSACNT_SOFVCDCMSTRTBL  S_CSACNT_SOFVCDCMSTRTBL,                             
    CSACNT_SOFVCTCHTBL  S_CSACNT_SOFVCTCHTBL,                                   
    CSACNT_SOFVDFPYTBL  S_CSACNT_SOFVDFPYTBL,                                   
    CSACNT_SOFVDUEBTBL  S_CSACNT_SOFVDUEBTBL,                                   
    CSACNT_SOFVFTPOTBL  S_CSACNT_SOFVFTPOTBL,                                   
    CSACNT_SOFVGNTYTBL  S_CSACNT_SOFVGNTYTBL,                                   
    CSACNT_SOFVGRGCTBL  S_CSACNT_SOFVGRGCTBL,                                   
    CSACNT_SOFVLND1TBL  S_CSACNT_SOFVLND1TBL,                                   
    CSACNT_SOFVLND2TBL  S_CSACNT_SOFVLND2TBL,                                   
    CSACNT_SOFVOTRNTBL  S_CSACNT_SOFVOTRNTBL,                                   
    CSACNT_SOFVPYMTTBL  S_CSACNT_SOFVPYMTTBL,                                   
    CSACNT_SOFVRTSCTBL  S_CSACNT_SOFVRTSCTBL,                                   
    CSACNT_SOFVAUDTTBL  S_CSACNT_SOFVAUDTTBL,                                   
    CSACNT_SOFVBFFATBL  S_CSACNT_SOFVBFFATBL,                                   
    CSACNT_SOFVBGDFTBL  S_CSACNT_SOFVBGDFTBL,                                   
    CSACNT_SOFVDRTBL  S_CSACNT_SOFVDRTBL,                                       
    nvl(ERR_STEP,'  ')  S_ERR_STEP,                                                       
    ERR_DT  S_ERR_DT,                                                           
    to_char(ERR_DT,'YYYYMMDDHH24MISS') S_ERR_RDT,
    to_char(ERR_DT,'Mon DD HH24MISS') S_ERR_FDT,
    ERR_MSG  S_ERR_MSG
  from stgcsa.xover_status;   
      
  
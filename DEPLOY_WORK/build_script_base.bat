echo off
set el=0
set filecheck_errfound=1
set deploy_name=%1
if "%2"=="" goto defaultvalue
set package_name=%2
goto done
:defaultvalue
set package_name=default
:done
set GIT_BASE=%BBBASE%
set deploy_work=%BBBASE%\DEPLOY_WORK
::set GIT_PATH="C:\Program Files\Git\bin\git.exe"
echo Building scripts for %deploy_name%
set TSP=%date:~-4%%date:~4,2%%date:~7,2%%time:~0,2%%time:~3,2%%time:~6,2%
set outfile=%BBBase%\DEPLOY\%DEPLOY_NAME%\%deploy_name%_%package_name%.sql
echo Building deploy script from files in %git_base%
echo execute GIT Status to list untracked files:
git status 
@echo off
setlocal
:PROMPT
::SET /P AREYOUSURE=Are you sure you want to continue (Y/[N])?
echo --
echo Ctrl+C now to quit, or ...
pause
echo define deploy_name=%deploy_name%>%outfile%
echo define package_name=%package_name%>>%outfile%
echo define package_buildtime=%TSP%>>%outfile%
echo column tsp new_value tsp >>%outfile%
echo column gn new_value gn >>%outfile%
echo column usrnm new_value usrnm >>%outfile%
echo column datetime new_value datetime >>%outfile%
echo select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, >>%outfile%
echo to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; >>%outfile%
echo spool ^&^&deploy_name._^&^&gn._^&^&package_name._^&^&tsp..log >>%outfile%
echo -- deploy scripts for deploy %deploy_name%_%package_name% created on %date% %time% by %username%>>%outfile%
echo prompt deploy scripts for deploy %deploy_name%_%package_name% created on %date% %time% by %username%>>%outfile%
type %deploy_work%\script_header.sql>>%outfile%
rem Check to see if deploymsg file exists. If not use default.
dir "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_Deploymsg.txt" >nul 2>&1
set el=%errorlevel%
if '%el%' == '1' goto use_default
  set deploy_msg="%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_Deploymsg.txt"
  goto deploymsg_alldone  
:use_default
  set deploy_msg="%BBBase%\DEPLOY_WORK\default_Deploymsg.txt"
  goto deploymsg_alldone 
:deploymsg_alldone   
echo prompt Instructions for deploy %deploy_name%_%package_name%: >>%outfile%
echo set termout on >>%outfile%
type %deploy_msg% >>%outfile%
echo prompt Hit enter to continue ( or Ctrl+C to abort)... >>%outfile%
echo set termout off >>%outfile%
echo accept x >>%outfile%
::IF /I "%AREYOUSURE%" NEQ "Y" GOTO END
echo /* execute GIT Pull to ensure all files are current >>%outfile%
git pull>> %outfile% 2>&1
echo   */ >>%outfile%
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call checkiffileexists %%a 
if '%el%'=='0' goto allfilesgood
  echo ERROR - One or more named files not found - Deploy script not created
  echo ERROR - Script not completed because not all files were found>>%outfile%
  exit /b
:allfilesgood
echo /* contents of deploy %deploy_work%\file_list.txt>>%outfile%
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call contentsofdeploy %%a
echo */>>"%outfile%"
echo -->>"%outfile%"
echo -->>"%outfile%"
echo -->>"%outfile%"

echo -- Deploy files start here:>>"%outfile%"
echo set echo on >>"%outfile%"
echo set termout on >>"%outfile%"
::set termout on
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call copy_file "%BBBase%\%%a" , %outfile%
type %deploy_work%\script_footer.sql>> %outfile%
echo Deploy script %deploy_name%_%package_name%.SQL completed
:END
endlocal




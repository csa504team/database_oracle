-- deploy scripts for deploy testjg created on Fri 06/21/2019 12:55:27.62 by Jasleen Gorowada
define deploy_name=testjg
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select global_name gn, user usrnm, to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors:' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
-- contents of deploy
set echo off
prompt '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&DEPLOY_NAME._&&TSP..LST

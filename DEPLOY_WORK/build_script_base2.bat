echo off
set el=0
set filecheck_errfound=1
set deploy_name=%1
if "%2"=="" goto defaultvalue
set package_name=%2
goto done
:defaultvalue
set package_name=default
:done
set GIT_BASE=%BBBASE%
set deploy_work=%BBBASE%\DEPLOY_WORK
set GIT_PATH="C:\Program Files\Git\bin\git.exe"
echo Building scripts for %deploy_name%
set TSP=%date:~-4%%date:~4,2%%date:~7,2%%time:~0,2%%time:~3,2%%time:~6,2%
set outfile=%BBBase%\DEPLOY\%DEPLOY_NAME%\%deploy_name%_%package_name%.sql
echo -- deploy scripts for deploy %deploy_name%_%package_name% created on %date% %time% by %username%>%outfile%
echo Building deploy script from files in %git_base%
echo define deploy_name=%deploy_name%>>%outfile%
echo define package_name=%package_name%>>%outfile%
type %deploy_work%\script_header.sql>>%outfile%
echo /* execute GIT Pull to ensure all files are current >>%outfile%
git pull>> %outfile% 2>&1
echo   */ >>%outfile%
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call checkiffileexists %%a 
if '%el%'=='0' goto allfilesgood
  echo ERROR - One or more named files not found - Deploy script not created
  echo ERROR - Script not completed because not all files were found>>%outfile%
  exit /b
:allfilesgood
echo -- contents of deploy>"%deploy_work%\file_list.txt"
echo /* contents of deploy %deploy_work%\file_list.txt>>%outfile%
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call contentsofdeploy %%a
echo */>>"%outfile%"
echo -->>"%outfile%"
echo -->>"%outfile%"
echo -->>"%outfile%"

echo -- Deploy files start here:>>"%outfile%"
echo set termout on >>"%outfile%"
for /f "tokens=* delims=" %%a in ('type "%BBBase%\DEPLOY\%DEPLOY_NAME%\%package_name%_FileList.txt"') do call copy_file "%BBBase%\%%a" , %outfile%
type %deploy_work%\script_footer.sql>> %outfile%
echo Deploy script %deploy_name%_%package_name%.SQL completed


-- deploy scripts for deploy testjl created on Sun 06/23/2019 15:07:47.18 by Jasleen Gorowada
define deploy_name=testjl
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select global_name gn, user usrnm, to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors:' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script.sql 
commit 73cd99e15aa50b962c98f6c0cbe4c502d917cfba
Author: John low <jlow@selectcomputing.com>
Date:   Fri Jun 21 11:25:11 2019 -0400

    deploy_work_updates

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_2.sql 
commit 73cd99e15aa50b962c98f6c0cbe4c502d917cfba
Author: John low <jlow@selectcomputing.com>
Date:   Fri Jun 21 11:25:11 2019 -0400

    deploy_work_updates

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_3.sql 
commit a68afdfe98b19a16207ae76184f674d3c6e9f787
Author: Jasleen0605 <jasleen0605@gmail.com>
Date:   Sun Jun 23 14:55:32 2019 -0400

    updates

*/
--
--
--
-- Deploy files start here:
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script.sql"
select sysdate from dual;
select to_char(sysdate,'xxx') from dual;
select 'all done' from dual;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_2.sql"
select 'this is script two' from dual;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_3.sql"
select 'this is script three' from dual;
set echo off
prompt '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&DEPLOY_NAME._&&TSP..LST

-- deploy scripts for deploy CSADEV-129 created on Tue 06/11/2019  9:17:38.46 by Jasleen Gorowada
define deploy_name=CSADEV-129
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
select global_name gn, user usrnm, to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&tsp
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set echo on
-- contents of deploy
  -- create_wiredashboardtbl.sql 
  -- alter_ltrppworkupaudttbl.sql 
  -- alter_ppwirerpttbl_dtrecvd.sql 
  -- alter_ppwirerpttbl_sadate.sql 
  -- create_ltrppworkupaudtinstsp.sql 
  -- create_ppwirerpttblseltsp.sql 
  -- create_prwireselcsp.sql 
CREATE TABLE cdconline.ppwirerpttbl
(
	loannmb CHAR(10 CHAR) NOT NULL
	, prepaydt DATE NOT NULL
	, borrnm VARCHAR2(80 BYTE)
	, cdcregncd CHAR(2 BYTE) NOT NULL 
	, cdcnmb CHAR(4 BYTE) NOT NULL 
	, duetopploanamt NUMBER(19, 4)
	, wireamt NUMBER(19, 4)
	, diffamt NUMBER(19, 4)
	, isarchived CHAR(1 CHAR) DEFAULT 'N'
	, creatuserid VARCHAR2(32 BYTE) NOT NULL 
	, creatdt DATE DEFAULT SYSDATE NOT NULL 
	, lastupdtuserid VARCHAR2(32 BYTE) NOT NULL 
	, lastupdtdt DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON cdconline.ppwirerpttbl TO cdconlinereadallrole;
ALTER TABLE cdconline.ltrppworkupaudttbl ADD duetopploanamt NUMBER(19, 4);
ALTER TABLE cdconline.ppwirerpttbl ADD wirerecvdt DATE;
ALTER TABLE cdconline.ppwirerpttbl ADD semiandt DATE;

CREATE OR replace PROCEDURE cdconline.ltrppworkupaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_ppworkupltr CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL,
p_duetopploanamt NUMBER := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrppworkupaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrppworkupaudttbl 
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       ppworkupltr, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt,
					   duetopploanamt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_ppworkupltr, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE,
					   p_duetopploanamt); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrppworkupaudttsp; 

                 RAISE; 
             END; 
END; 
/

CREATE OR replace PROCEDURE cdconline.ppwirerptseltsp ( 
p_identifier in NUMBER,  
p_cdcregncd in CHAR, 
p_cdcnmb in CHAR,
p_prepaydt in DATE,
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns ALL wire report data for agents
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
				SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns wire report data for a specific month
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				WHERE
					TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns wire report data for a specific month, cdc
	ELSIF p_Identifier = 2 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				WHERE
					TRIM(cdcnmb) = TRIM(p_cdcnmb)
				AND
					TRIM(cdcregncd) = TRIM(p_cdcregncd)
				AND
					TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns all valid prepayment dates
	ELSIF p_Identifier = 3 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				DISTINCT(prepaydt)
			FROM 
				cdconline.ppwirerpttbl
			ORDER BY
				prepaydt DESC;
		END;
	END IF;
END;
/
GRANT EXECUTE ON cdconline.ppwirerptseltsp TO cdconlinereadallrole,cdconlineprtread,cdconlinegovread;
/
CREATE OR replace PROCEDURE cdconline.prwireselcsp ( 
p_identifier  NUMBER := NULL,  
p_prepaydt    DATE := SYSDATE, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns payments from payment review by the prepay date (month-year) irrespective of cdc number and cdc region
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				ct.transid,
				ct.loannmb AS loannmb,
				ct.transind1,
				ct.creatdt,
				ct.statidcur,
				mcta.maxoftransattrid,
				to_number(mcta.attrval) AS amount,
				mcta2.attrval AS imptdt,
				mcta3.attrval AS postdate,
				mcta1.attrval AS "CURRENT",
				rts.statnm AS status,
				mcta22.attrval AS batchno,
				mcta19.attrval AS pymttypeid,
				lt.cdcregncd,
				lt.cdcnmb 
			FROM
				stgcsa.coretranstbl ct 
				LEFT JOIN
					cdconline.loantbl lt 
					ON ct.loannmb = lt.loannmb 
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 20 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta 
					ON ct.transid = mcta.transid 		--PymtAmt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 24 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta1 
					ON ct.transid = mcta1.transid 		--Current
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 34 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta2 
					ON ct.transid = mcta2.transid 		--ImpDt
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 27 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta3 
					ON ct.transid = mcta3.transid 		--PostDt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 19 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta19 
					ON ct.transid = mcta19.transid 		--PymtTyp
				LEFT OUTER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 22 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta22 
					ON ct.transid = mcta22.transid 		--BatchNo
				INNER JOIN
					stgcsa.reftransstattbl rts 
					ON rts.statid = ct.statidcur 
			WHERE
				transtypid = 3 
				AND ct.statidcur IN 
				(
					7,
					18,
					33
				)
				AND LAST_DAY(TRUNC(mcta2.attrval)) = LAST_DAY(TRUNC(p_prepaydt))
			;
		END;
	-- returns payments from payment review by the prepay date (month-year), cdc number, and cdc region
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				ct.transid,
				ct.loannmb AS loannmb,
				ct.transind1,
				ct.creatdt,
				ct.statidcur,
				mcta.maxoftransattrid,
				to_number(mcta.attrval) AS amount,
				mcta2.attrval AS imptdt,
				mcta3.attrval AS postdate,
				mcta1.attrval AS "CURRENT",
				rts.statnm AS status,
				mcta22.attrval AS batchno,
				mcta19.attrval AS pymttypeid,
				lt.cdcregncd,
				lt.cdcnmb 
			FROM
				stgcsa.coretranstbl ct 
				LEFT JOIN
					cdconline.loantbl lt 
					ON ct.loannmb = lt.loannmb 
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 20 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta 
					ON ct.transid = mcta.transid 		--PymtAmt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 24 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta1 
					ON ct.transid = mcta1.transid 		--Current
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 34 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta2 
					ON ct.transid = mcta2.transid 		--ImpDt
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 27 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta3 
					ON ct.transid = mcta3.transid 		--PostDt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 19 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta19 
					ON ct.transid = mcta19.transid 		--PymtTyp
				LEFT OUTER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 22 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta22 
					ON ct.transid = mcta22.transid 		--BatchNo
				INNER JOIN
					stgcsa.reftransstattbl rts 
					ON rts.statid = ct.statidcur 
			WHERE
				transtypid = 3 
				AND ct.statidcur IN 
				(
					7,
					18,
					33
				)
				AND LAST_DAY(TRUNC(mcta2.attrval)) = LAST_DAY(TRUNC(p_prepaydt))
				AND TRIM(lt.cdcregncd) = TRIM(p_cdcregncd)
				AND TRIM(lt.cdcnmb) = TRIM(p_cdcnmb)
			;
		END;
	END IF;
END;
/
GRANT EXECUTE ON cdconline.prwireselcsp TO cdconlinereadallrole,cdconlineprtread,cdconlinegovread;
/
set echo off
prompt &&deploy_name ended
select '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
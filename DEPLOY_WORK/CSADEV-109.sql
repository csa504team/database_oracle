-- deploy scripts for deploy CSADEV-109 created on Mon 05/13/2019 17:11:23.29 by Jasleen Gorowada
define deploy_name=CSADEV-109
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
select global_name gn, user usrnm, to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&tsp
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set echo on
-- contents of deploy
  -- create_ltrtlseaudttbl.sql 
  -- create_ltrtranscthistaudttbl.sql 
  -- create_ltrppworkupaudttbl.sql 
  -- create_ltrppworkupaudtinstsp.sql 
  -- create_ltrppworkupaudtseltsp.sql 
  -- create_ltrrlseaudtinstsp.sql 
  -- create_ltrrlseaudtseltsp.sql 
  -- create_ltrtranscthistaudtinstsp.sql 
  -- create_ltrtranscthistaudtseltsp.sql 
  -- CSADEV-109_grants.sql 
  -- CSADEV-109_rolegrants.sql 
CREATE TABLE CDCONLINE.LtrRlseAudtTbl
(
	RQSTREF NUMBER(10, 0) NOT NULL 
	, RQSTDT DATE NOT NULL 
	, LOANNMB CHAR(10 CHAR) NOT NULL 
	, RQSTCD NUMBER(3, 0) NOT NULL 
	, PREPAYDT DATE 
	, CDCREGNCD CHAR(2 BYTE) NOT NULL 
	, CDCNMB CHAR(4 BYTE) NOT NULL 
	, LTRRLSE CLOB
	, CREATUSERID VARCHAR2(32 BYTE) NOT NULL 
	, CREATDT DATE DEFAULT SYSDATE NOT NULL 
	, LASTUPDTUSERID VARCHAR2(32 BYTE) NOT NULL 
	, LASTUPDTDT DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO CDCONLINEREADALLROLE;

CREATE TABLE CDCONLINE.LtrTransctHistAudtTbl
(
	RQSTREF NUMBER(10, 0) NOT NULL 
	, RQSTDT DATE NOT NULL 
	, LOANNMB CHAR(10 CHAR) NOT NULL 
	, RQSTCD NUMBER(3, 0) NOT NULL 
	, PREPAYDT DATE 
	, CDCREGNCD CHAR(2 BYTE) NOT NULL 
	, CDCNMB CHAR(4 BYTE) NOT NULL 
	, PPTRANSCTHIST CLOB
	, CREATUSERID VARCHAR2(32 BYTE) NOT NULL 
	, CREATDT DATE DEFAULT SYSDATE NOT NULL 
	, LASTUPDTUSERID VARCHAR2(32 BYTE) NOT NULL 
	, LASTUPDTDT DATE DEFAULT SYSDATE NOT NULL
);

CREATE TABLE CDCONLINE.LtrPPWorkUpAudtTbl
(
	RQSTREF NUMBER(10, 0) NOT NULL 
	, RQSTDT DATE NOT NULL 
	, LOANNMB CHAR(10 CHAR) NOT NULL 
	, RQSTCD NUMBER(3, 0) NOT NULL 
	, PREPAYDT DATE 
	, CDCREGNCD CHAR(2 BYTE) NOT NULL 
	, CDCNMB CHAR(4 BYTE) NOT NULL 
	, PPWORKUPLTR CLOB
	, CREATUSERID VARCHAR2(32 BYTE) NOT NULL 
	, CREATDT DATE DEFAULT SYSDATE NOT NULL 
	, LASTUPDTUSERID VARCHAR2(32 BYTE) NOT NULL 
	, LASTUPDTDT DATE DEFAULT SYSDATE NOT NULL
);

CREATE OR replace PROCEDURE cdconline.ltrppworkupaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_ppworkupltr CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrppworkupaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrppworkupaudttbl 
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       ppworkupltr, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_ppworkupltr, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE ); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrppworkupaudttsp; 

                 RAISE; 
             END; 
END; 
/
CREATE OR replace PROCEDURE cdconline.ltrppworkupaudtseltsp ( 
p_identifier  NUMBER := NULL,  
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns all archived versions, irrespective of input parameters; should be used by admins only for a future enhancement, never cdcs
    IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, ppworkupltr
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrppworkupaudttbl
			ORDER BY
				loannmb ASC, rqstcd ASC, prepaydt ASC, creatdt DESC;
		END;
	-- returns all archived versions, filtered by input parameters, including p_cdcregncd and p_cdcnmb; should be used by cdcs
	ELSIF p_Identifier = 1 then
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, ppworkupltr
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrppworkupaudttbl
			WHERE
				TRUNC(prepaydt) = TRUNC(p_prepaydt)
			AND
				loannmb = p_loannmb
			AND
				rqstcd = p_rqstcd
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			ORDER BY
				creatdt DESC;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlinereadallrole,cdconlineprtread;
/
CREATE OR replace PROCEDURE cdconline.ltrrlseaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_ltrrlse     CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrrlseaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrrlseaudttbl
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       ltrrlse, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_ltrrlse, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE ); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrrlseaudttsp; 

                 RAISE; 
             END; 
END; 


GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlineprtread;
/
CREATE OR replace PROCEDURE cdconline.ltrrlseaudtseltsp ( 
p_identifier  NUMBER := NULL,  
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns all archived versions, irrespective of input parameters; should be used by admins only, never cdcs
    IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, ltrrlse
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrrlseaudttbl
			ORDER BY
				loannmb ASC, rqstcd ASC, prepaydt ASC, creatdt DESC;
		END;
	-- returns all archived versions, filtered by input parameters, including p_cdcregncd and p_cdcnmb; should be used by cdcs
	ELSIF p_Identifier = 1 then
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, ltrrlse
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrrlseaudttbl
			WHERE
				TRUNC(prepaydt) = TRUNC(p_prepaydt)
			AND
				loannmb = p_loannmb
			AND
				rqstcd = p_rqstcd
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			ORDER BY
				creatdt DESC;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlinereadallrole,cdconlineprtread;
/
CREATE OR replace PROCEDURE cdconline.ltrtranscthistaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_pptranscthist CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrtranscthistaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrtranscthistaudttbl
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       pptranscthist, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_pptranscthist, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE ); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrtranscthistaudttbl; 

                 RAISE; 
             END; 
END; 
/
CREATE OR replace PROCEDURE cdconline.ltrtranscthistaudtseltsp ( 
p_identifier  NUMBER := NULL,  
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns all archived versions, irrespective of input parameters; should be used by admins only, never cdcs
    IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, pptranscthist
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrtranscthistaudttbl
			ORDER BY
				loannmb ASC, rqstcd ASC, prepaydt ASC, creatdt DESC;
		END;
	-- returns all archived versions, filtered by input parameters, including p_cdcregncd and p_cdcnmb; should be used by cdcs
	ELSIF p_Identifier = 1 then
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, pptranscthist
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrtranscthistaudttbl
			WHERE
				TRUNC(prepaydt) = TRUNC(p_prepaydt)
			AND
				loannmb = p_loannmb
			AND
				rqstcd = p_rqstcd
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			ORDER BY
				creatdt DESC;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinereadallrole,cdconlineprtread;
/
GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.LtrPPWorkUpAudtTbl TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO CDCONLINEREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.LtrPPWorkUpAudtTbl TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.LtrTransctHistAudtTbl TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO CDCONLINEREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.LtrTransctHistAudtTbl TO CSAUPDTROLE;

GRANT EXECUTE ON 
  CDCONLINE.ltrppworkupaudtinstsp
to CDCONLINEPRTREAD;  
                                                            
GRANT EXECUTE ON 
  CDCONLINE.ltrtranscthistaudtinstsp
to CDCONLINEPRTREAD;  


/
grant select on stgcsa.sofvpymttbl to CDCONLINEREADALLROLE;
grant select on stgcsa.sofvpymttbl to CDCONLINEPRTREAD;

/
set echo off
prompt &&deploy_name ended
select '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
echo dir %git_base%\%1 >nul 2>&1
set el=%errorlevel%
if '%el%' == '1' goto checkrc_isbad
  goto checkrc_alldone 
:checkrc_isbad
  echo %1 not found!
  set filecheck_errfound=1
  goto checkrc_alldone  
:checkrc_alldone  

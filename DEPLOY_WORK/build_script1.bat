@echo off
set deploy_name=%1
set deploy_work=D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK
echo Building scripts for %deploy_name%
echo -- deploy scripts for deploy %deploy_name% created on %date% %time% by %username%>"%deploy_work%\%deploy_name%.sql"
echo define deploy_name=%deploy_name%>>"%deploy_work%\%deploy_name%.sql"
type %deploy_work%\script_header.sql>>"%deploy_work%\%deploy_name%.sql
echo -- contents of deploy>"%deploy_work%\file_list.txt"
for /f "tokens=* delims=" %%a in ('type "D:\Files\FileList.txt"') do echo   -- %%a >>"%deploy_work%\file_list.txt"
type %deploy_work%\file_list.txt>>"%deploy_work%\%deploy_name%.sql
for /f "tokens=* delims=" %%a in ('type "D:\Files\FileList.txt"') do call copy_file "D:\dba_oracle_Bitbucket\database_oracle\DEPLOY\prepayment_workup_balance\%%a" ,"%deploy_work%\%deploy_name%.sql"
type %deploy_work%\script_footer.sql>>"%deploy_work%\%deploy_name%.sql

CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTSELTSP(
p_IDENTIFIER        NUMBER  := 0,
p_RETVAL        OUT NUMBER,
p_CDCPRTID          NUMBER  := 0,
p_CDCBNKAGRMTSEQNMB NUMBER  := 0,
p_SELCUR        OUT  SYS_REFCURSOR
)
AS
/*
RY :05/07/2019: Created to capture agreements between Certified Development Company and banks
*/
BEGIN

        
    IF  p_Identifier = 0 THEN
    /* Select Row On the basis of Key*/
    BEGIN
        OPEN p_SelCur FOR
        SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL 
        WHERE CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSEQNMB =p_CDCBNKAGRMTSEQNMB;        
        p_RETVAL := SQL%ROWCOUNT;            
    END;
    ELSIF  p_Identifier = 2 THEN
    /*Check The Existance Of Row*/
    BEGIN
        OPEN p_SelCur FOR
        SELECT 1 FROM PARTNER.CDCBNKAGRMTTBL  
        WHERE CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSEQNMB =p_CDCBNKAGRMTSEQNMB; 
        p_RETVAL := SQL%ROWCOUNT;                            
    END;
    ELSIF  p_Identifier = 11 THEN
    /* GETS ALL ACTIVE AGREEMENTS BASED ON PRTID*/
    BEGIN
        
        OPEN p_SelCur FOR
         SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              p.prtlocnm as BNKNM,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL l,partner.prtloctbl p 
        WHERE l.BNKHQLOCID = p.locid(+)
        and CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSTRTDT<=SYSDATE 
        AND (CDCBNKAGRMTENDDT >=SYSDATE OR CDCBNKAGRMTENDDT IS NULL);

        p_RETVAL :=SQL%ROWCOUNT;
    END;
    ELSIF  p_Identifier = 12 THEN
    /* GETS ALL AGREEMENTS BASED ON PRTID*/
    BEGIN
        
        OPEN p_SelCur FOR
         SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              p.prtlocnm as BNKNM,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL l,partner.prtloctbl p
        WHERE l.BNKHQLOCID = p.locid(+)
        and CDCPRTID =p_CDCPRTID;

        p_RETVAL :=SQL%ROWCOUNT;
    END;
  
    END IF;
                
EXCEPTION
WHEN OTHERS THEN
RAISE;
                            
END CDCBNKAGRMTSELTSP;
/

grant execute on PARTNER.CDCBNKAGRMTselTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTsELTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTselTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTselTSP to 'CDCOnlineCorpGovPrtUpdate; 


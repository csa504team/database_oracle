CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTDELTSP(
p_IDENTIFIER             NUMBER  := 0,
p_RETVAL             OUT NUMBER,
p_CDCPRTID               NUMBER  :=NULL,
p_BNKHQLOCID             NUMBER  :=NULL,
p_CDCBNKAGRMTSEQNMB      NUMBER:=NULL,
p_CDCBNKAGRMTSTRTDT      DATE    :=NULL,
p_CDCBNKAGRMTENDDT       DATE    :=NULL,
p_CREATUSERID            VARCHAR2:=NULL
)
AS
/*
RY :07/29/2019: Created to delete the records from agreement table for the given prtid and locid.
*/
BEGIN
    
    SAVEPOINT CDCBNKAGRMTDEL;
        
        IF  p_Identifier = 0 THEN
       BEGIN 
       DELETE from PARTNER.CDCBNKAGRMTTBL
				where CDCPRTID=p_CDCPRTID  
                 AND  BNKHQLOCID=p_BNKHQLOCID
                 AND  CDCBNKAGRMTSEQNMB=P_CDCBNKAGRMTSEQNMB ;   

        p_RETVAL :=SQL%ROWCOUNT;
        
        END;
        
       END IF;
                
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK TO CDCBNKAGRMTDEL;
RAISE;
        
END;
/
grant execute on PARTNER.CDCBNKAGRMTDELTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to 'CDCOnlineCorpGovPrtUpdate; 

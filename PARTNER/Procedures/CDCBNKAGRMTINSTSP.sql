CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTINSTSP(
p_IDENTIFIER             NUMBER  := 0,
p_RETVAL             OUT NUMBER,
p_CDCBNKAGRMTSEQNMB  OUT NUMBER,
p_CDCPRTID               NUMBER  :=NULL,
p_BNKHQLOCID             NUMBER  :=NULL,
p_CDCBNKAGRMTSTRTDT      DATE    :=NULL,
p_CDCBNKAGRMTENDDT       DATE    :=NULL,
p_CREATUSERID            VARCHAR2:=NULL
)
AS
/*RY :05/07/2019: Created to capture agreements between Certified Development Company and banks.
*/
BEGIN
    
    SAVEPOINT CDCBNKAGRMTINS;
        
        IF  p_Identifier = 0 THEN
        
        BEGIN
        
        BEGIN
        SELECT NVL(MAX(CDCBNKAGRMTSEQNMB),0)+1 INTO p_CDCBNKAGRMTSEQNMB
        FROM PARTNER.CDCBNKAGRMTTBL
        WHERE CDCPRTID = p_CDCPRTID;
        EXCEPTION WHEN OTHERS THEN
        p_CDCBNKAGRMTSEQNMB := 1;
        END;
           
       INSERT INTO PARTNER.CDCBNKAGRMTTBL(CDCPRTID,
                                          CDCBNKAGRMTSEQNMB,
                                          BNKHQLOCID ,
                                          CDCBNKAGRMTSTRTDT,
                                          CDCBNKAGRMTENDDT,
                                          CREATUSERID,
                                          CREATDT,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
                                    VALUES(p_CDCPRTID,
                                          p_CDCBNKAGRMTSEQNMB,
                                          p_BNKHQLOCID ,
                                          p_CDCBNKAGRMTSTRTDT,
                                          p_CDCBNKAGRMTENDDT,
                                          p_CREATUSERID,
                                          SYSDATE,
                                          p_CREATUSERID,
                                          SYSDATE);

        p_RETVAL :=SQL%ROWCOUNT;
        
        END;
        
       END IF;
                
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK TO CDCBNKAGRMTINS;
RAISE;
        
END;
/

grant execute on PARTNER.CDCBNKAGRMTinsTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to 'CDCOnlineCorpGovPrtUpdate; 


SET DEFINE OFF;
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E00', 'the purchase of land described as [Description] and located at [Address]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E01', 'the purchase of land described as [Description] and located at [Address]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E02', 'the purchase of land and existing improvements described as [Description] and located at [Address]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E04', '[ProcdSubTypDesc2] described as [Description] and [ProcdSubTypDesc1] located at [Address]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E10', 'the [ProcdSubTypDesc] of machinery and equipment described as [Description] and located at [Address]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E15', 'the refinance of debt owed to [Lender] and secured by [ProcdSubTypDesc] described as [Description]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E19', 'eligible business expenses under Debt Refinancing  described as [Description]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E21', 'other Expenses described as [Description]');
Insert into SBAREF.PROCTXTBLCK
   (LOANPROCDTYPCD, TEXTBLOCK)
 Values
   ('E22', 'professional fees described as [Description]');
COMMIT;

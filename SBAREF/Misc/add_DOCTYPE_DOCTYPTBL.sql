INSERT INTO SBAREF.DOCTYPTBL (
   DOCTYPCD, DOCTYPDESCTXT, DOCTYPSTRTDT, 
   DOCTYPENDDT, CREATUSERID, CREATDT, 
   TAB, DOCTYPPREAPPIND) 
VALUES ( 1258,
 'T10 - SBA Form 159 - Itemization and Supporting Documentation',
 trunc(sysdate),
 null,
 user,
 trunc(sysdate),
 'T10',
 null );
 commit;
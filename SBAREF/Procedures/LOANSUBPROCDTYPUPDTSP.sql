CREATE OR REPLACE PROCEDURE SBAREF.LOANSUBPROCDTYPUPDTSP (p_Identifier               NUMBER := 0,
                                                     p_RetVal               OUT NUMBER,
                                                    p_LOANSUBPROCDTYPCD     NUMBER :=0,    
                                                    p_PROCDTYPCD       CHAR := NULL,    
                                                    p_LOANPROCDTYPCD      CHAR := NULL,     
                                                    p_SUBPROCDTYPDESCTXT  VARCHAR2 := NULL, 
                                                    p_LOANPROCDSUBTYPSTRTDT  DATE := NULL,
                                                    p_LOANPROCDSUBTYPENDDT   DATE := NULL,
                                                    p_CREATUSERID           VARCHAR2 := NULL,
                                                    p_LASTUPDTUSERID       VARCHAR2 := NULL)
AS
BEGIN
    SAVEPOINT LOANSUBPROCDTYPUPDTSP;
   IF p_Identifier = 0
   THEN
     
      BEGIN
UPDATE SBAREF.LOANSUBPROCDTYPTBL
SET    LOANSUBPROCDTYPCD     = p_LOANSUBPROCDTYPCD,
       PROCDTYPCD            = p_PROCDTYPCD,
       LOANPROCDTYPCD        = p_LOANPROCDTYPCD,
       SUBPROCDTYPDESCTXT    = p_SUBPROCDTYPDESCTXT,
       LOANPROCDSUBTYPSTRTDT = p_LOANPROCDSUBTYPSTRTDT,
       LOANPROCDSUBTYPENDDT  = p_LOANPROCDSUBTYPENDDT,
       CREATUSERID           = p_CREATUSERID,
       LASTUPDTUSERID        = p_LASTUPDTUSERID,
       LASTUPDTDT            = sysdate
;   
         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         ROLLBACK TO LOANSUBPROCDTYPUPDTSP;
         RAISE;
      END;
END;
/

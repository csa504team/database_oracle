CREATE OR REPLACE PROCEDURE SBAREF.LOANSUBPROCDTYPINSTSP (p_Identifier               NUMBER := 0,
                                                     p_RetVal               OUT NUMBER,
                                                    p_LOANSUBPROCDTYPCD     NUMBER :=0,    
                                                    p_PROCDTYPCD       CHAR := NULL,    
                                                    p_LOANPROCDTYPCD      CHAR := NULL,     
                                                    p_SUBPROCDTYPDESCTXT  VARCHAR2 := NULL, 
                                                    p_LOANPROCDSUBTYPSTRTDT  DATE := NULL,
                                                    p_LOANPROCDSUBTYPENDDT   DATE := NULL,
                                                    p_CREATUSERID           VARCHAR2 := NULL,
                                                    p_LASTUPDTUSERID       VARCHAR2 := NULL)
AS
BEGIN
    SAVEPOINT LOANSUBPROCDTYPINSTSP;
   IF p_Identifier = 0
   THEN
     
      BEGIN
            INSERT INTO SBAREF.LOANSUBPROCDTYPTBL (
   LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, 
   SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, LOANPROCDSUBTYPENDDT, 
   CREATUSERID, CREATDT, LASTUPDTUSERID, 
   LASTUPDTDT) 
VALUES (           
                                                    p_LOANSUBPROCDTYPCD ,  
                                                    p_PROCDTYPCD ,         
                                                    p_LOANPROCDTYPCD  ,    
                                                    p_SUBPROCDTYPDESCTXT  , 
                                                    p_LOANPROCDSUBTYPSTRTDT,
                                                    p_LOANPROCDSUBTYPENDDT,
                                                    p_CREATUSERID         ,
                                                    sysdate            , 
                                                    p_LASTUPDTUSERID      ,
                                                    sysdate   ) ;      
         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         ROLLBACK TO LOANSUBPROCDTYPINSTSP;
         RAISE;
      END;
END;
/

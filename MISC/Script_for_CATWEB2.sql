
--Insert 1268 into DOCTYPBUSPRCSMTHDTBL
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, '7AG', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
COMMIT;

----**Move Legacy Documents

update LOANDOCS.DOCTBL  set DOCTYPCD=1268 where DOCTYPCD=927 and BUSPRCSTYPCD=1;
COMMIT;

----** First script is to fix the spelling error of Document name and second script is to take off the end date and make document optional in Origination.

update SBAREF.DOCTYPTBL set DOCTYPDESCTXT='T10 - SBA Form 159 - Fee Disclosure and Compensation Form' where DOCTYPCD=927;
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY') where DOCTYPCD=927 and   PRCSMTHDCD='7AG' and BUSPRCSTYPCD=1 ;
commit;

----** Add DOCTYPCD 1268 for remaining 13 7A loans

Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'ITR', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, '7EW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CLP', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CLW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CTR', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'PLP', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'PLW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SAB', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SGC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SLC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'STC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CAI', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CAT', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
commit;

----**This will add code 1258 for all other 7A loans for both Servicing and Origination and also update DOCTYPCD 927 to be optional and have an end date of null since all legacy --documents have been moved over to 1268.

Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'ITR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, '7EW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CTR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'PLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'PLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SAB',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SGC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SLC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'STC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CAI',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'ITR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, '7EW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CTR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'PLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'PLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SAB',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SGC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SLC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'STC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CAI',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                            commit;

update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'ITR' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = '7EW' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CLP' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CLW' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CTR' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'PLP' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'PLW' and BUSPRCSTYPCD=1 ;                                                                                                              
commit;                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SAB' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SGC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SLC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'STC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CAI' and BUSPRCSTYPCD=1 ;


commit;    
                                                                                                                                       
----**Add code for SBX
-- 1258
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 1,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );

 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 2,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
 
 -- 927
 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 927,
 1,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );

 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 927,
 2,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
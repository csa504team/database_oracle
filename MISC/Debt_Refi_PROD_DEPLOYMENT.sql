--===================================================================================================
                                -- Requirement # 1
--===================================================================================================


-- 4.	Veteran - Processing Method Tbl

Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (1, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (2, '5RX', TO_DATE('1/1/1966', 'MM/DD/YYYY'), TO_DATE('10/6/2005', 'MM/DD/YYYY'), user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (3, '5RX', TO_DATE('1/1/1966', 'MM/DD/YYYY'), TO_DATE('10/6/2005', 'MM/DD/YYYY'), user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (4, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (5, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (6, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (7, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (8, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (9, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
Insert into SBAREF.LOANVETCDTBL
   (VETCD, PRCSMTHDCD, VETSTRTDT, VETENDDT, CREATUSERID, 
    CREATDT)
 Values
   (10, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate);
COMMIT;



-- 6.	Eligibility PrcsMthCd

Insert into SBAREF.LOANELIGPRCSMTHDTBL
   (LOANELIGCD, PRCSMTHDCD, LOANELIGPRCSMTHDSTRTDT, LOANELIGPRCSMTHDENDDT, CREATUSERID, 
    CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (103, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate, user, sysdate);
Insert into SBAREF.LOANELIGPRCSMTHDTBL
   (LOANELIGCD, PRCSMTHDCD, LOANELIGPRCSMTHDSTRTDT, LOANELIGPRCSMTHDENDDT, CREATUSERID, 
    CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (104, '5RX', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, user, 
    sysdate, user, sysdate);
COMMIT;



-- 7.	Interest Rate Type Table

/*
update SBAREF.IMRTTYPTBL set IMRTTYPSTRTDT = to_date('27-12-2020','DD-MM-YYYY') where PRCSMTHDCD='5RX';
*/

Insert into SBAREF.IMRTTYPTBL
   (IMRTTYPCD, PRCSMTHDCD, IMRTTYPDESCTXT, IMRTTYPSTRTDT, IMRTTYPENDDT, 
    IMRTTYPMFCD, IMRTTYPIND, IMINTRTLKUPURL, IMRTTYPCREATUSERID, IMRTTYPCREATDT)
 Values
   ('NR1', '5RX', '504 Note Rate for 10 years', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, 
    'P', 'F', NULL, user, sysdate);
Insert into SBAREF.IMRTTYPTBL
   (IMRTTYPCD, PRCSMTHDCD, IMRTTYPDESCTXT, IMRTTYPSTRTDT, IMRTTYPENDDT, 
    IMRTTYPMFCD, IMRTTYPIND, IMINTRTLKUPURL, IMRTTYPCREATUSERID, IMRTTYPCREATDT)
 Values
   ('NR2', '5RX', '504 Note Rate for 20 years', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, 
    'P', 'F', NULL, user, sysdate);
Insert into SBAREF.IMRTTYPTBL
   (IMRTTYPCD, PRCSMTHDCD, IMRTTYPDESCTXT, IMRTTYPSTRTDT, IMRTTYPENDDT, 
    IMRTTYPMFCD, IMRTTYPIND, IMINTRTLKUPURL, IMRTTYPCREATUSERID, IMRTTYPCREATDT)
 Values
   ('NR3', '5RX', '504 Note Rate for 25 years', TO_DATE('12/27/2020', 'MM/DD/YYYY'), NULL, 
    'P', 'F', NULL, user, sysdate);
COMMIT;


-- 8.	ORIG Parameter Value Table
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)
				VALUES (62,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 62),
                '100',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                             
				VALUES (80,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 80),
                '0',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                VALUES (150,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 150),
                '100',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                VALUES (116,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 116),
                'Sacramento504eTransServicing@sba.gov',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                VALUES (173,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 173),
                '0933',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                 VALUES (141,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 141),
                '5000000',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                 VALUES (38,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 38),
                '5500000',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                '',
                'OCADB',
                SYSDATE);                                                                                                                            
                                                                                                                                                      
                                                                                                                                                      
INSERT INTO LoanApp.imprmtrvaltbl (IMPRMTRSEQNMB,
                                           IMPRMTRVALSEQNMB,
                                           IMPRMTRVALTXT,
                                           IMPRMTRVALSTRTDT,
                                           PRCSMTHDCD,
                                           RECOVIND,
                                           IMPRMTRVALCREATUSERID,
                                           IMPRMTRVALCREATDT)                                                                                        
                 VALUES (150,
                (SELECT MAX (IMPRMTRVALSEQNMB) + 1
                 FROM LoanApp.imprmtrvaltbl
                 WHERE IMPRMTRSEQNMB = 150),
                '100',
                TO_DATE ('11-04-2021','dd-mm-yyyy'),
                '5RX',
                'R',
                'OCADB',
                SYSDATE);                                                                                                                            
commit;                                                                                                                                                   
                                                                                                                                                      


-- 2. Run two update queries below to set proper sort order on 2 new added Use of Proceed items.
 
UPDATE SBAREF.LoanProcdTypTbl SET LoanProcdTypOrd = 21 WHERE ProcdTypCd = 'E' AND LoanProcdTypCd = '19';

UPDATE SBAREF.LoanProcdTypTbl SET LoanProcdTypOrd = 3 WHERE ProcdTypCd = 'E' AND LoanProcdTypCd = '01';
COMMIT; 

/*
-- 3. Inserting description for new loan new processing code type 
INSERT INTO SBAREF.PROCTXTBLCK (LOANPROCDTYPCD, TEXTBLOCK)
VALUES ('E00', 'the purchase of land only');
COMMIT; 
*/

--===================================================================================================
                                -- Requirement 2a & 2b
--===================================================================================================

--  Run insert script below to add new  “Use of Proceeds” types

INSERT INTO SBAREF.LoanProcdTypTbl (LOANPROCDTYPCD,PROCDTYPCD,LOANPROCDTYPDESCTXT,LOANPROCDTYPCMNT,LOANPROCDTYPCDSTRTDT,LOANPROCDTYPCDENDDT,LOANPROCDTYPCREATUSERID,LOANPROCDTYPCREATDT,LOANPROCDTYPORD)
VALUES (19,	'E', 'Eligible business expenses under Debt Refinancing', ' ', TO_DATE ('12-04-2021','dd-mm-yyyy'), null, 'OCADB', TO_DATE ('12-04-2021','dd-mm-yyyy'),	21);
COMMIT;

/*
INSERT INTO SBAREF.LoanProcdTypTbl (LOANPROCDTYPCD,PROCDTYPCD,LOANPROCDTYPDESCTXT,LOANPROCDTYPCMNT,LOANPROCDTYPCDSTRTDT,LOANPROCDTYPCDENDDT,LOANPROCDTYPCREATUSERID,LOANPROCDTYPCREATDT,LOANPROCDTYPORD)
VALUES (00,	'E', 'Land Only', ' ', TO_DATE ('12-04-2021','dd-mm-yyyy'), null, 'OCADB', TO_DATE ('12-04-2021','dd-mm-yyyy'),	3);
COMMIT;
*/

-- Run update script below to update info for E01 loan proceed type
UPDATE SBAREF.LoanProcdTypTbl 
SET LOANPROCDTYPDESCTXT = 'Land only',
LOANPROCDTYPCDENDDT = null
WHERE PROCDTYPCD = 'E' 
AND LOANPROCDTYPCD = '01';
COMMIT;


----------------------------update other affected tables-----
--Remove E00 record from loan proceeds type table
DELETE 
FROM SBAREF.LoanProcdTypTbl 
WHERE PROCDTYPCD = 'E' 
AND (LOANPROCDTYPCD = '00' OR LOANPROCDTYPCD = '00');
COMMIT;
--Update E00 to E01
update  LOANAPP.LOANPROCDTBL  set LOANPROCDTYPCD='01' where PROCDTYPCD='E' and (LOANPROCDTYPCD='00' or LOANPROCDTYPCD='0') ;
commit;



--===================================================================================================
                                -- Requirement 3a
--===================================================================================================
-- Run update query below to edit check-box option under Public Policy/Community Development Goals

UPDATE SBAREF.econdevobjctcdtbl
SET ECONDEVOBJCTTXT = 'Changes Necessitated by Federal Budget Cutbacks or Base Closures'
WHERE ECONDEVOBJCTCD = 'C06';
COMMIT;



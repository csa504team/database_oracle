CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLDSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCNM varchar2 := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select  DOCID,
            UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            CREATUSERID,
            CREATDT,
            PGID,
            PGCATID,
            DOCNM,
            LASTUPDUSERID,
            LASTUPDDT    
            from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
            where    CREATDT in(select max (CREATDT) from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
        where DOCNM = p_DOCNM );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
/


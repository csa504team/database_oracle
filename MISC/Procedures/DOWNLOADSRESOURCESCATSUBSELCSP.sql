CREATE OR REPLACE PROCEDURE FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP
(
  p_ISACTIVE IN NUMBER,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		 SELECT 
        dr.downloadsResourcesID,
        dr.downloadsResourcesCatID,
        dr.downloadsResourcesDate,
        dr.downloadsResourcesTitle,
        dr.downloadsResourcesContent,
        drc.downloadsResourcesCatDesc,
        dr.downloadsResourcesFileName,
        dr.downloadsResourcesFileLocation,
        drcs.downloadsResourcesCatSubDesc  ,
        dr.attachments,
       drcs.downloadsResourcesCatSubID  
        from FTAWIKI.DOWNLOADSRESOURCESTBL dr
      left join FTAWIKI.DOWNLOADSRESOURCESCATTBL drc on dr.downloadsResourcesCatID = drc.downloadsResourcesCatID
      left join FTAWIKI.DOWNLOADSRESOURCESCATSUBTBL drcs on dr.downloadsResourcesCatSubID = drcs.downloadsResourcesCatSubID
      where dr.isactive = p_ISACTIVE ORDER BY dr.downloadsResourcesDate DESC;

               

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/


GRANT EXECUTE ON FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP TO CNTRDCHICA;

GRANT EXECUTE ON FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP TO FTAPORTALWIKIMANAGERROLE;

GRANT EXECUTE ON FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP TO FTAWIKIREADALLROLE;

CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLOADSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_PGID number := null,
p_PGCATID number := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select  DOCID,
             UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            CREATUSERID,
            CREATDT,
            PGID,
            PGCATID,
            DOCNM,
            LASTUPDUSERID,
            LASTUPDDT    
            from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
            where    CREATDT in(select max (CREATDT) from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
        where PGID = p_PGID and PGCATID = p_PGCATID );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
/


CREATE OR REPLACE PROCEDURE FTAWIKI.NEWSUPDTSP
(  p_IDENTIFIER           NUMBER := 0,
  p_RETVAL           OUT NUMBER,
  p_NEWSID             NUMBER,
  p_NEWSDATE           DATE,
  p_NEWSCONTENT        VARCHAR2,
  p_NEWSTITLE          VARCHAR2,
  p_NEWSFILENAME       VARCHAR2,
  p_CREATDT        DATE,
  p_CREATUSERID          NUMBER,
  p_LASTUPDTUSERID          NUMBER,
  p_LASTUPDTDT  DATE,
  p_ATTACHMENTS BLOB
)
AS

BEGIN
    SAVEPOINT NEWSUPDTSP;

    IF p_IDENTIFIER = 0
    THEN
        BEGIN
        UPDATE FTAWIKI.NEWSTBL
SET    NEWSDATE          = p_NEWSDATE,
       NEWSCONTENT       = p_NEWSCONTENT,
       NEWSTITLE         = p_NEWSTITLE,
       NEWSFILENAME      =p_NEWSFILENAME,
       CREATDT       = p_CREATDT,
       CREATUSERID         = p_CREATUSERID,
       LASTUPDTUSERID         = p_LASTUPDTUSERID,
       LASTUPDTDT = p_LASTUPDTDT,
       ATTACHMENTS = p_ATTACHMENTS 
WHERE  NEWSID            = p_NEWSID
;
			
            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO NEWSUPDTSP;
        END;
END NEWSUPDTSP;
/


GRANT EXECUTE ON FTAWIKI.NEWSUPDTSP TO CNTRDCHICA;

GRANT EXECUTE ON FTAWIKI.NEWSUPDTSP TO FTAPORTALWIKIMANAGERROLE;

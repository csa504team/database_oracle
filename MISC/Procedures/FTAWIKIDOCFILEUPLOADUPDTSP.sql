CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLOADUPDTSP
(
p_Identifier number := 0,
p_RetVal out number,
p_PGID  number:=0,
p_PGCATID number:=0,
p_DOCDATA blob :=null,
p_DOCID  number:=0
)
as
begin
    savepoint FTAWIKIDOCFILEUPLOADUPDTSP;
    if p_Identifier = 0 then
    begin
        update FTAWIKI.FTAWIKIDOCFILEUPLOADTBL set
        PGID = p_PGID,
        PGCATID = p_PGCATID,
        DOCDATA = UTL_COMPRESS.lz_compress(p_DOCData),
        LASTUPDUSERID = user,
        LASTUPDDT = sysdate
        where DocId = p_DocId;        
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
exception when others then
begin
    rollback to FTAWIKIDOCFILEUPLOADUPDTSP;
    raise;
end;
end;
/


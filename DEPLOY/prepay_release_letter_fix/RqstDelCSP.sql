CREATE OR REPLACE PROCEDURE CDCONLINE.RqstDelCSP
 (
 p_sba_loan_no IN CHAR,
 p_prepay_date IN DATE,
 p_type IN NUMBER,
 p_uID IN VARCHAR2,
 p_level_id IN NUMBER,
 p_dbMsg OUT VARCHAR2
 )
 AS
 --exec cdc_req_delete '8798374006' , '09/17/2015', 4 , 'csmlevel', 7, dbMsg
 ----Check if Deletion can happen
 v_deleteFlag CHAR;
 
 BEGIN
 
 IF ( p_type = '4' ) THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 ----Prepayment Actual
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb
 FROM LoanTbl
 WHERE LoanStatCd = 'PREPAID'
 AND LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 IF ( p_level_id >= 7 ) THEN
 
 BEGIN
 v_deleteFlag := 'Y' ;
 p_dbMsg := 'Prepayment Actual record has been Deleted' ;
 
 END;
 ELSE
 
 BEGIN
 IF ( p_level_id = 1
 OR p_level_id = 5 ) THEN
 
 ----Checking if user is a CDC OR SBA level
 BEGIN
 v_deleteFlag := 'N' ;
 p_dbMsg := 'Please contact the CSA to delete this Prepayment Actual record' ;
 
 END;
 END IF;
 
 END;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 v_deleteFlag := 'Y' ;
 p_dbMsg := 'Prepayment Actual record has been Deleted' ;
 
 END;
 END IF;
 
 END;
 ELSE
 
 ---- Prepayment Estimate
 BEGIN
 v_deleteFlag := 'Y' ;
 p_dbMsg := 'Prepayment Estimate record has been Deleted' ;
 
 END;
 END IF;
 IF ( v_deleteFlag = 'Y' ) THEN
 DECLARE
 v_ref_num NUMBER(10,0);
 
 BEGIN
 -- ZM:	Case 1: An open actual can be deleted
--		Case 2: A closed actual cannot be deleted
--		Case 3: An open estimate can be deleted
--		Case 4: A closed estimate can be deleted

 SELECT MAX(RqstREF)
 INTO v_ref_num
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(p_prepay_date)
 --AND RqstCd = p_type AND CDCPORTFLSTATCDCD = 'OP' AND ROWNUM < 2;
AND ((RqstCd = 4 AND CDCPORTFLSTATCDCD = 'OP') OR (RqstCd = 9)) AND RqstCd = p_type AND ROWNUM < 2;
 
 
 
 DELETE RqstCntlTbl
 
 WHERE RqstREF = v_ref_num;
 DELETE RqstTbl
 
 WHERE RqstREF = v_ref_num;
 DELETE RqstDtlTbl
 
 WHERE RqstREF = v_ref_num;
 DELETE LoanSaveTbl
 
 WHERE RqstREF = v_ref_num;
 DELETE PymtSaveTbl
 
 WHERE RqstREF = v_ref_num;
 UPDATE WiresTbl
 SET WireStatCd = 'OP'
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(p_prepay_date)
 AND RqstCd = p_type;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
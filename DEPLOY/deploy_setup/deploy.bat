@echo off
call setvars.bat
set "logbase=%dplybase%\logs"
set "deploydir=%svnbase%\DEPLOY\%1\"
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
set "logdir=%logbase%\%1\d%fullstamp%"
echo here
echo %logdir%
mkdir %logbase%
mkdir %logbase%\%1
mkdir %logdir%

sqlplus /nolog @%deploydir%%2  %2 %logdir% %svnbase%





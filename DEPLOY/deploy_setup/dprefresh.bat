REM dprefresh.bat v1.0 jl 5 Jan 2018
@echo off
call setvars.bat
echo script to perform a datapump import of stgcsa schema to a local DB
echo expecting %1 dmp file in %dpdir%...
echo use password for stgcsa on your local db
dir %dpdir%\%1*

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
echo ...
echo running preimp script to add/remove stuff

sqlplus /nolog @%dpdir%/%1_preimport %dpdir%
echo running import
impdp stgcsa directory=DATA_PUMP_DIR dumpfile=%1 logfile=%1_%fullstamp% table_exists_action=replace





<!--- Saved 01/28/2018 14:42:20. --->
PROCEDURE PRPMFWIREPOSTINGINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_BORRFEEAMT NUMBER:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_CSAFEEAMT NUMBER:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CDCFEEAPPLAMT NUMBER:=null
 ,p_CDCFEEDISBAMT NUMBER:=null
 ,p_CDCDISBAMT NUMBER:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_ADJTOAPPLINTAMT NUMBER:=null
 ,p_ADJTOAPPLPRINAMT NUMBER:=null
 ,p_LATEFEEADJAMT NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_CURBALAMT NUMBER:=null
 ,p_PREPAYAMT NUMBER:=null
 ,p_WIRECMNT VARCHAR2:=null
 ,p_ADJTYP VARCHAR2:=null
 ,p_OVRWRITECD VARCHAR2:=null
 ,p_POSTINGTYP VARCHAR2:=null
 ,p_POSTINGSCHDLDT DATE:=null
 ,p_POSTINGACTUALDT DATE:=null
 ,p_WIREPOSTINGID NUMBER:=null
 ,p_TRANSID NUMBER:=null
 ,p_MFSTATCD VARCHAR2:=null
 ,p_ACHDEL NUMBER:=null
 ,p_OVRPAYAMT NUMBER:=null
 ,p_CDCREPDAMT NUMBER:=null
 ,p_CSAREPDAMT NUMBER:=null
 ,p_REPDAMT NUMBER:=null
 ,p_PREPAYDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_ZEROBALAMT NUMBER:=null
 ,p_SEMIANCDCNMB CHAR:=null
 ,p_SEMIANNOTEBALAMT NUMBER:=null
 ,p_SEMIANWIREAMT NUMBER:=null
 ,p_SEMIANPRINACCRAMT NUMBER:=null
 ,p_SEMIANINTACCRAMT NUMBER:=null
 ,p_SEMIANOPENGNTYAMT NUMBER:=null
 ,p_SEMIANOPENGNTYERRAMT NUMBER:=null
 ,p_SEMIANOPENGNTYREMAINAMT NUMBER:=null
 ,p_SEMIANPIPYMTAFT6MOAMT NUMBER:=null
 ,p_SEMIANPISHORTOVRAMT NUMBER:=null
 ,p_SEMIANESCROWPRIORPYMTAMT NUMBER:=null
 ,p_SEMIANDBENTRBALAMT NUMBER:=null
 ,p_SEMIANPREPAYPREMAMT NUMBER:=null
 ,p_SEMIANAMT NUMBER:=null
 ,p_SEMIANSBADUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANSBABEHINDAMT NUMBER:=null
 ,p_SEMIANSBAACCRAMT NUMBER:=null
 ,p_SEMIANSBAPRJCTDAMT NUMBER:=null
 ,p_SEMIANCSADUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANCSABEHINDAMT NUMBER:=null
 ,p_SEMIANCSAACCRAMT NUMBER:=null
 ,p_SEMIANCSAPRJCTDAMT NUMBER:=null
 ,p_SEMIANCDCDUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANCDCBEHINDAMT NUMBER:=null
 ,p_SEMIANCDCACCRAMT NUMBER:=null
 ,p_SEMIANACCRINTONDELAMT NUMBER:=null
 ,p_SEMIANLATEFEEAMT NUMBER:=null
 ,p_SEMIANPROOFAMT NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:28:56
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:28:56
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPMFWIREPOSTINGTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PRPMFWIREPOSTINGTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PRPMFWIREPOSTINGINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PRPMFWIREPOSTINGTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_BORRFEEAMT';
 new_rec.BORRFEEAMT:=runtime.validate_column_value(P_BORRFEEAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','BORRFEEAMT','NUMBER',22);
 cur_col_name:='P_BORRPAIDTHRUDT';
 new_rec.BORRPAIDTHRUDT:=runtime.validate_column_value(P_BORRPAIDTHRUDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','BORRPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_CSAFEEAMT';
 new_rec.CSAFEEAMT:=runtime.validate_column_value(P_CSAFEEAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CSAFEEAMT','NUMBER',22);
 cur_col_name:='P_CSAPAIDTHRUDT';
 new_rec.CSAPAIDTHRUDT:=runtime.validate_column_value(P_CSAPAIDTHRUDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CSAPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_CDCFEEAPPLAMT';
 new_rec.CDCFEEAPPLAMT:=runtime.validate_column_value(P_CDCFEEAPPLAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CDCFEEAPPLAMT','NUMBER',22);
 cur_col_name:='P_CDCFEEDISBAMT';
 new_rec.CDCFEEDISBAMT:=runtime.validate_column_value(P_CDCFEEDISBAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CDCFEEDISBAMT','NUMBER',22);
 cur_col_name:='P_CDCDISBAMT';
 new_rec.CDCDISBAMT:=runtime.validate_column_value(P_CDCDISBAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CDCDISBAMT','NUMBER',22);
 cur_col_name:='P_CDCPAIDTHRUDT';
 new_rec.CDCPAIDTHRUDT:=runtime.validate_column_value(P_CDCPAIDTHRUDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CDCPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_INTPAIDTHRUDT';
 new_rec.INTPAIDTHRUDT:=runtime.validate_column_value(P_INTPAIDTHRUDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','INTPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_ADJTOAPPLINTAMT';
 new_rec.ADJTOAPPLINTAMT:=runtime.validate_column_value(P_ADJTOAPPLINTAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','ADJTOAPPLINTAMT','NUMBER',22);
 cur_col_name:='P_ADJTOAPPLPRINAMT';
 new_rec.ADJTOAPPLPRINAMT:=runtime.validate_column_value(P_ADJTOAPPLPRINAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','ADJTOAPPLPRINAMT','NUMBER',22);
 cur_col_name:='P_LATEFEEADJAMT';
 new_rec.LATEFEEADJAMT:=runtime.validate_column_value(P_LATEFEEADJAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','LATEFEEADJAMT','NUMBER',22);
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=runtime.validate_column_value(P_UNALLOCAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','UNALLOCAMT','NUMBER',22);
 cur_col_name:='P_CURBALAMT';
 new_rec.CURBALAMT:=runtime.validate_column_value(P_CURBALAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CURBALAMT','NUMBER',22);
 cur_col_name:='P_PREPAYAMT';
 new_rec.PREPAYAMT:=runtime.validate_column_value(P_PREPAYAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','PREPAYAMT','NUMBER',22);
 cur_col_name:='P_WIRECMNT';
 new_rec.WIRECMNT:=runtime.validate_column_value(P_WIRECMNT,
 'STGCSA','PRPMFWIREPOSTINGTBL','WIRECMNT','VARCHAR2',80);
 cur_col_name:='P_ADJTYP';
 new_rec.ADJTYP:=runtime.validate_column_value(P_ADJTYP,
 'STGCSA','PRPMFWIREPOSTINGTBL','ADJTYP','VARCHAR2',10);
 cur_col_name:='P_OVRWRITECD';
 new_rec.OVRWRITECD:=runtime.validate_column_value(P_OVRWRITECD,
 'STGCSA','PRPMFWIREPOSTINGTBL','OVRWRITECD','VARCHAR2',10);
 cur_col_name:='P_POSTINGTYP';
 new_rec.POSTINGTYP:=runtime.validate_column_value(P_POSTINGTYP,
 'STGCSA','PRPMFWIREPOSTINGTBL','POSTINGTYP','VARCHAR2',10);
 cur_col_name:='P_POSTINGSCHDLDT';
 new_rec.POSTINGSCHDLDT:=runtime.validate_column_value(P_POSTINGSCHDLDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','POSTINGSCHDLDT','DATE',7);
 cur_col_name:='P_POSTINGACTUALDT';
 new_rec.POSTINGACTUALDT:=runtime.validate_column_value(P_POSTINGACTUALDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','POSTINGACTUALDT','DATE',7);
 cur_col_name:='P_WIREPOSTINGID';
 new_rec.WIREPOSTINGID:=runtime.validate_column_value(P_WIREPOSTINGID,
 'STGCSA','PRPMFWIREPOSTINGTBL','WIREPOSTINGID','NUMBER',22);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','PRPMFWIREPOSTINGTBL','TRANSID','NUMBER',22);
 cur_col_name:='P_MFSTATCD';
 new_rec.MFSTATCD:=runtime.validate_column_value(P_MFSTATCD,
 'STGCSA','PRPMFWIREPOSTINGTBL','MFSTATCD','VARCHAR2',10);
 cur_col_name:='P_ACHDEL';
 new_rec.ACHDEL:=runtime.validate_column_value(P_ACHDEL,
 'STGCSA','PRPMFWIREPOSTINGTBL','ACHDEL','NUMBER',22);
 cur_col_name:='P_OVRPAYAMT';
 new_rec.OVRPAYAMT:=runtime.validate_column_value(P_OVRPAYAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','OVRPAYAMT','NUMBER',22);
 cur_col_name:='P_CDCREPDAMT';
 new_rec.CDCREPDAMT:=runtime.validate_column_value(P_CDCREPDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CDCREPDAMT','NUMBER',22);
 cur_col_name:='P_CSAREPDAMT';
 new_rec.CSAREPDAMT:=runtime.validate_column_value(P_CSAREPDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','CSAREPDAMT','NUMBER',22);
 cur_col_name:='P_REPDAMT';
 new_rec.REPDAMT:=runtime.validate_column_value(P_REPDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','REPDAMT','NUMBER',22);
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=runtime.validate_column_value(P_PREPAYDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','PREPAYDT','DATE',7);
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=runtime.validate_column_value(P_SEMIANDT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANDT','DATE',7);
 cur_col_name:='P_ZEROBALAMT';
 new_rec.ZEROBALAMT:=runtime.validate_column_value(P_ZEROBALAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','ZEROBALAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCDCNMB';
 new_rec.SEMIANCDCNMB:=runtime.validate_column_value(P_SEMIANCDCNMB,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCDCNMB','CHAR',8);
 cur_col_name:='P_SEMIANNOTEBALAMT';
 new_rec.SEMIANNOTEBALAMT:=runtime.validate_column_value(P_SEMIANNOTEBALAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANNOTEBALAMT','NUMBER',22);
 cur_col_name:='P_SEMIANWIREAMT';
 new_rec.SEMIANWIREAMT:=runtime.validate_column_value(P_SEMIANWIREAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANWIREAMT','NUMBER',22);
 cur_col_name:='P_SEMIANPRINACCRAMT';
 new_rec.SEMIANPRINACCRAMT:=runtime.validate_column_value(P_SEMIANPRINACCRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANPRINACCRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANINTACCRAMT';
 new_rec.SEMIANINTACCRAMT:=runtime.validate_column_value(P_SEMIANINTACCRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANINTACCRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANOPENGNTYAMT';
 new_rec.SEMIANOPENGNTYAMT:=runtime.validate_column_value(P_SEMIANOPENGNTYAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANOPENGNTYAMT','NUMBER',22);
 cur_col_name:='P_SEMIANOPENGNTYERRAMT';
 new_rec.SEMIANOPENGNTYERRAMT:=runtime.validate_column_value(P_SEMIANOPENGNTYERRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANOPENGNTYERRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANOPENGNTYREMAINAMT';
 new_rec.SEMIANOPENGNTYREMAINAMT:=runtime.validate_column_value(P_SEMIANOPENGNTYREMAINAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANOPENGNTYREMAINAMT','NUMBER',22);
 cur_col_name:='P_SEMIANPIPYMTAFT6MOAMT';
 new_rec.SEMIANPIPYMTAFT6MOAMT:=runtime.validate_column_value(P_SEMIANPIPYMTAFT6MOAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANPIPYMTAFT6MOAMT','NUMBER',22);
 cur_col_name:='P_SEMIANPISHORTOVRAMT';
 new_rec.SEMIANPISHORTOVRAMT:=runtime.validate_column_value(P_SEMIANPISHORTOVRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANPISHORTOVRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANESCROWPRIORPYMTAMT';
 new_rec.SEMIANESCROWPRIORPYMTAMT:=runtime.validate_column_value(P_SEMIANESCROWPRIORPYMTAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANESCROWPRIORPYMTAMT','NUMBER',22);
 cur_col_name:='P_SEMIANDBENTRBALAMT';
 new_rec.SEMIANDBENTRBALAMT:=runtime.validate_column_value(P_SEMIANDBENTRBALAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANDBENTRBALAMT','NUMBER',22);
 cur_col_name:='P_SEMIANPREPAYPREMAMT';
 new_rec.SEMIANPREPAYPREMAMT:=runtime.validate_column_value(P_SEMIANPREPAYPREMAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANPREPAYPREMAMT','NUMBER',22);
 cur_col_name:='P_SEMIANAMT';
 new_rec.SEMIANAMT:=runtime.validate_column_value(P_SEMIANAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANAMT','NUMBER',22);
 cur_col_name:='P_SEMIANSBADUEFROMBORRAMT';
 new_rec.SEMIANSBADUEFROMBORRAMT:=runtime.validate_column_value(P_SEMIANSBADUEFROMBORRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANSBADUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANSBABEHINDAMT';
 new_rec.SEMIANSBABEHINDAMT:=runtime.validate_column_value(P_SEMIANSBABEHINDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANSBABEHINDAMT','NUMBER',22);
 cur_col_name:='P_SEMIANSBAACCRAMT';
 new_rec.SEMIANSBAACCRAMT:=runtime.validate_column_value(P_SEMIANSBAACCRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANSBAACCRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANSBAPRJCTDAMT';
 new_rec.SEMIANSBAPRJCTDAMT:=runtime.validate_column_value(P_SEMIANSBAPRJCTDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANSBAPRJCTDAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCSADUEFROMBORRAMT';
 new_rec.SEMIANCSADUEFROMBORRAMT:=runtime.validate_column_value(P_SEMIANCSADUEFROMBORRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCSADUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCSABEHINDAMT';
 new_rec.SEMIANCSABEHINDAMT:=runtime.validate_column_value(P_SEMIANCSABEHINDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCSABEHINDAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCSAACCRAMT';
 new_rec.SEMIANCSAACCRAMT:=runtime.validate_column_value(P_SEMIANCSAACCRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCSAACCRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCSAPRJCTDAMT';
 new_rec.SEMIANCSAPRJCTDAMT:=runtime.validate_column_value(P_SEMIANCSAPRJCTDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCSAPRJCTDAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCDCDUEFROMBORRAMT';
 new_rec.SEMIANCDCDUEFROMBORRAMT:=runtime.validate_column_value(P_SEMIANCDCDUEFROMBORRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCDCDUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCDCBEHINDAMT';
 new_rec.SEMIANCDCBEHINDAMT:=runtime.validate_column_value(P_SEMIANCDCBEHINDAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCDCBEHINDAMT','NUMBER',22);
 cur_col_name:='P_SEMIANCDCACCRAMT';
 new_rec.SEMIANCDCACCRAMT:=runtime.validate_column_value(P_SEMIANCDCACCRAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANCDCACCRAMT','NUMBER',22);
 cur_col_name:='P_SEMIANACCRINTONDELAMT';
 new_rec.SEMIANACCRINTONDELAMT:=runtime.validate_column_value(P_SEMIANACCRINTONDELAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANACCRINTONDELAMT','NUMBER',22);
 cur_col_name:='P_SEMIANLATEFEEAMT';
 new_rec.SEMIANLATEFEEAMT:=runtime.validate_column_value(P_SEMIANLATEFEEAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANLATEFEEAMT','NUMBER',22);
 cur_col_name:='P_SEMIANPROOFAMT';
 new_rec.SEMIANPROOFAMT:=runtime.validate_column_value(P_SEMIANPROOFAMT,
 'STGCSA','PRPMFWIREPOSTINGTBL','SEMIANPROOFAMT','NUMBER',22);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PRPMFWIREPOSTINGTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 p_errval:=-20002;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PRPMFWIREPOSTINGAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PRPMFWIREPOSTINGTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
 p_errmsg:='Got dupe key on insert into PRPMFWIREPOSTINGTBL for key '
 ||'(WIREPOSTINGID)=('||new_rec.WIREPOSTINGID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFWIREPOSTINGINSTSP build 2018-01-23 08:28:56';
 ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFWIREPOSTINGINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPMFWIREPOSTINGINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PRPMFWIREPOSTINGINSTSP build 2018-01-23 08:28:56'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PRPMFWIREPOSTINGINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


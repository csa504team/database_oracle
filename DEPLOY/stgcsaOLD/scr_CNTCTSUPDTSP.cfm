<!--- Saved 01/28/2018 14:41:38. --->
PROCEDURE CNTCTSUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_BOUNCEDCHK VARCHAR2:=null
 ,p_BUSEMAILADR VARCHAR2:=null
 ,p_CDCID NUMBER:=null
 ,p_CDCNMB CHAR:=null
 ,p_CNTCTBUSFAXNMB VARCHAR2:=null
 ,p_CNTCTBUSPHNNMB VARCHAR2:=null
 ,p_CNTCTCDC VARCHAR2:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_CNTCTFUNDINGPHNNMB CHAR:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTMAINPHNNMB CHAR:=null
 ,p_CNTCTSERVICINGPHNNMB CHAR:=null
 ,p_CNTCTSID NUMBER:=null
 ,p_FUNDINGCNTCT VARCHAR2:=null
 ,p_LASTNMCNTCT VARCHAR2:=null
 ,p_MAILDOCPOC VARCHAR2:=null
 ,p_MAINCNTCT VARCHAR2:=null
 ,p_PROBLEMLOANMTHDOFCNTCT VARCHAR2:=null
 ,p_SERVICINGCNTCTNM VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:06
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:18:06
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CNTCTSUPDTSP performs UPDATE on STGCSA.CNTCTSTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CNTCTSID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CNTCTSTBL%rowtype;
 new_rec STGCSA.CNTCTSTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CNTCTSTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTSUPDTSP build 2018-01-23 08:18:06 '
 ||'Select of STGCSA.CNTCTSTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTSUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CDCNMB is not null then
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=P_CDCNMB; 
 end if;
 if P_CNTCTFIRSTNM is not null then
 cur_col_name:='P_CNTCTFIRSTNM';
 new_rec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM; 
 end if;
 if P_LASTNMCNTCT is not null then
 cur_col_name:='P_LASTNMCNTCT';
 new_rec.LASTNMCNTCT:=P_LASTNMCNTCT; 
 end if;
 if P_CNTCTMAINPHNNMB is not null then
 cur_col_name:='P_CNTCTMAINPHNNMB';
 new_rec.CNTCTMAINPHNNMB:=P_CNTCTMAINPHNNMB; 
 end if;
 if P_CNTCTSERVICINGPHNNMB is not null then
 cur_col_name:='P_CNTCTSERVICINGPHNNMB';
 new_rec.CNTCTSERVICINGPHNNMB:=P_CNTCTSERVICINGPHNNMB; 
 end if;
 if P_CNTCTFUNDINGPHNNMB is not null then
 cur_col_name:='P_CNTCTFUNDINGPHNNMB';
 new_rec.CNTCTFUNDINGPHNNMB:=P_CNTCTFUNDINGPHNNMB; 
 end if;
 if P_BOUNCEDCHK is not null then
 cur_col_name:='P_BOUNCEDCHK';
 new_rec.BOUNCEDCHK:=P_BOUNCEDCHK; 
 end if;
 if P_MAILDOCPOC is not null then
 cur_col_name:='P_MAILDOCPOC';
 new_rec.MAILDOCPOC:=P_MAILDOCPOC; 
 end if;
 if P_PROBLEMLOANMTHDOFCNTCT is not null then
 cur_col_name:='P_PROBLEMLOANMTHDOFCNTCT';
 new_rec.PROBLEMLOANMTHDOFCNTCT:=P_PROBLEMLOANMTHDOFCNTCT; 
 end if;
 if P_BUSEMAILADR is not null then
 cur_col_name:='P_BUSEMAILADR';
 new_rec.BUSEMAILADR:=P_BUSEMAILADR; 
 end if;
 if P_CNTCTBUSPHNNMB is not null then
 cur_col_name:='P_CNTCTBUSPHNNMB';
 new_rec.CNTCTBUSPHNNMB:=P_CNTCTBUSPHNNMB; 
 end if;
 if P_CNTCTBUSFAXNMB is not null then
 cur_col_name:='P_CNTCTBUSFAXNMB';
 new_rec.CNTCTBUSFAXNMB:=P_CNTCTBUSFAXNMB; 
 end if;
 if P_CNTCTCDC is not null then
 cur_col_name:='P_CNTCTCDC';
 new_rec.CNTCTCDC:=P_CNTCTCDC; 
 end if;
 if P_CNTCTMAILADRSTR1NM is not null then
 cur_col_name:='P_CNTCTMAILADRSTR1NM';
 new_rec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM; 
 end if;
 if P_CNTCTMAILADRSTR2NM is not null then
 cur_col_name:='P_CNTCTMAILADRSTR2NM';
 new_rec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM; 
 end if;
 if P_CNTCTMAILADRCTYNM is not null then
 cur_col_name:='P_CNTCTMAILADRCTYNM';
 new_rec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM; 
 end if;
 if P_CNTCTMAILADRSTCD is not null then
 cur_col_name:='P_CNTCTMAILADRSTCD';
 new_rec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD; 
 end if;
 if P_CNTCTMAILADRZIPCD is not null then
 cur_col_name:='P_CNTCTMAILADRZIPCD';
 new_rec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD; 
 end if;
 if P_CNTCTMAILADRZIP4CD is not null then
 cur_col_name:='P_CNTCTMAILADRZIP4CD';
 new_rec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD; 
 end if;
 if P_CNTCTSID is not null then
 cur_col_name:='P_CNTCTSID';
 new_rec.CNTCTSID:=P_CNTCTSID; 
 end if;
 if P_MAINCNTCT is not null then
 cur_col_name:='P_MAINCNTCT';
 new_rec.MAINCNTCT:=P_MAINCNTCT; 
 end if;
 if P_SERVICINGCNTCTNM is not null then
 cur_col_name:='P_SERVICINGCNTCTNM';
 new_rec.SERVICINGCNTCTNM:=P_SERVICINGCNTCTNM; 
 end if;
 if P_FUNDINGCNTCT is not null then
 cur_col_name:='P_FUNDINGCNTCT';
 new_rec.FUNDINGCNTCT:=P_FUNDINGCNTCT; 
 end if;
 if P_CDCID is not null then
 cur_col_name:='P_CDCID';
 new_rec.CDCID:=P_CDCID; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CNTCTSAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CNTCTSTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CDCNMB=new_rec.CDCNMB
 ,CNTCTFIRSTNM=new_rec.CNTCTFIRSTNM
 ,LASTNMCNTCT=new_rec.LASTNMCNTCT
 ,CNTCTMAINPHNNMB=new_rec.CNTCTMAINPHNNMB
 ,CNTCTSERVICINGPHNNMB=new_rec.CNTCTSERVICINGPHNNMB
 ,CNTCTFUNDINGPHNNMB=new_rec.CNTCTFUNDINGPHNNMB
 ,BOUNCEDCHK=new_rec.BOUNCEDCHK
 ,MAILDOCPOC=new_rec.MAILDOCPOC
 ,PROBLEMLOANMTHDOFCNTCT=new_rec.PROBLEMLOANMTHDOFCNTCT
 ,BUSEMAILADR=new_rec.BUSEMAILADR
 ,CNTCTBUSPHNNMB=new_rec.CNTCTBUSPHNNMB
 ,CNTCTBUSFAXNMB=new_rec.CNTCTBUSFAXNMB
 ,CNTCTCDC=new_rec.CNTCTCDC
 ,CNTCTMAILADRSTR1NM=new_rec.CNTCTMAILADRSTR1NM
 ,CNTCTMAILADRSTR2NM=new_rec.CNTCTMAILADRSTR2NM
 ,CNTCTMAILADRCTYNM=new_rec.CNTCTMAILADRCTYNM
 ,CNTCTMAILADRSTCD=new_rec.CNTCTMAILADRSTCD
 ,CNTCTMAILADRZIPCD=new_rec.CNTCTMAILADRZIPCD
 ,CNTCTMAILADRZIP4CD=new_rec.CNTCTMAILADRZIP4CD
 ,MAINCNTCT=new_rec.MAINCNTCT
 ,SERVICINGCNTCTNM=new_rec.SERVICINGCNTCTNM
 ,FUNDINGCNTCT=new_rec.FUNDINGCNTCT
 ,CDCID=new_rec.CDCID
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CNTCTSUPDTSP build 2018-01-23 08:18:06 '
 ||'while doing update on STGCSA.CNTCTSTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTSUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CNTCTSUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CNTCTSID)=('||P_CNTCTSID||') ';
 select rowid into rec_rowid from STGCSA.CNTCTSTBL where 1=1
 and CNTCTSID=P_CNTCTSID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTSUPDTSP build 2018-01-23 08:18:06 '
 ||'No STGCSA.CNTCTSTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTSUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In CNTCTSUPDTSP build 2018-01-23 08:18:06 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTSUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTSUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In CNTCTSUPDTSP build 2018-01-23 08:18:06'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTSUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CNTCTSUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


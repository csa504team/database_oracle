<!--- Saved 01/28/2018 14:41:39. --->
PROCEDURE CORECONTROLVALINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CONTROLID NUMBER:=null
 ,p_CONTROLNM VARCHAR2:=null
 ,p_CONTROLDESC VARCHAR2:=null
 ,p_CONTROLVALTXT VARCHAR2:=null
 ,p_CONTROLVALNMB NUMBER:=null
 ,p_CONTROLVALBOOL NUMBER:=null
 ,p_CONTROLVALTIMEDT DATE:=null
 ,p_CONTROLVALUSER VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:45
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:18:45
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CORECONTROLVALTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.CORECONTROLVALTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint CORECONTROLVALINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CONTROLID';
 new_rec.CONTROLID:=runtime.validate_column_value(P_CONTROLID,
 'STGCSA','CORECONTROLVALTBL','CONTROLID','NUMBER',22);
 cur_col_name:='P_CONTROLNM';
 new_rec.CONTROLNM:=runtime.validate_column_value(P_CONTROLNM,
 'STGCSA','CORECONTROLVALTBL','CONTROLNM','VARCHAR2',100);
 cur_col_name:='P_CONTROLDESC';
 new_rec.CONTROLDESC:=runtime.validate_column_value(P_CONTROLDESC,
 'STGCSA','CORECONTROLVALTBL','CONTROLDESC','VARCHAR2',250);
 cur_col_name:='P_CONTROLVALTXT';
 new_rec.CONTROLVALTXT:=runtime.validate_column_value(P_CONTROLVALTXT,
 'STGCSA','CORECONTROLVALTBL','CONTROLVALTXT','VARCHAR2',250);
 cur_col_name:='P_CONTROLVALNMB';
 new_rec.CONTROLVALNMB:=runtime.validate_column_value(P_CONTROLVALNMB,
 'STGCSA','CORECONTROLVALTBL','CONTROLVALNMB','NUMBER',22);
 cur_col_name:='P_CONTROLVALBOOL';
 new_rec.CONTROLVALBOOL:=runtime.validate_column_value(P_CONTROLVALBOOL,
 'STGCSA','CORECONTROLVALTBL','CONTROLVALBOOL','NUMBER',22);
 cur_col_name:='P_CONTROLVALTIMEDT';
 new_rec.CONTROLVALTIMEDT:=runtime.validate_column_value(P_CONTROLVALTIMEDT,
 'STGCSA','CORECONTROLVALTBL','CONTROLVALTIMEDT','DATE',7);
 cur_col_name:='P_CONTROLVALUSER';
 new_rec.CONTROLVALUSER:=runtime.validate_column_value(P_CONTROLVALUSER,
 'STGCSA','CORECONTROLVALTBL','CONTROLVALUSER','VARCHAR2',10);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','CORECONTROLVALTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO CORECONTROLVALINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.CORECONTROLVALAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.CORECONTROLVALTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO CORECONTROLVALINSTSP;
 p_errmsg:='Got dupe key on insert into CORECONTROLVALTBL for key '
 ||'(CONTROLID)=('||new_rec.CONTROLID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in CORECONTROLVALINSTSP build 2018-01-23 08:18:45';
 ROLLBACK TO CORECONTROLVALINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CORECONTROLVALINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CORECONTROLVALINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CORECONTROLVALINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in CORECONTROLVALINSTSP build 2018-01-23 08:18:45'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CORECONTROLVALINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END CORECONTROLVALINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


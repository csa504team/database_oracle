<!--- Saved 01/28/2018 14:42:48. --->
PROCEDURE SOFVPYMTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_PYMTPOSTCDCAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCDCDISBAMT NUMBER:=null
 ,p_PYMTPOSTCDCDUEAMT NUMBER:=null
 ,p_PYMTPOSTCDCPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCDCPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCDCREPDAMT NUMBER:=null
 ,p_PYMTPOSTCDCSBADISBAMT NUMBER:=null
 ,p_PYMTPOSTCMNT VARCHAR2:=null
 ,p_PYMTPOSTCSAACTUALINITFEEAMT NUMBER:=null
 ,p_PYMTPOSTCSAAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCSADUEAMT NUMBER:=null
 ,p_PYMTPOSTCSAPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCSAPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCSAREPDAMT NUMBER:=null
 ,p_PYMTPOSTDUEAMT NUMBER:=null
 ,p_PYMTPOSTESCRCPTSTDAMT NUMBER:=null
 ,p_PYMTPOSTGNTYREPAYAMT NUMBER:=null
 ,p_PYMTPOSTINTACCRADJAMT NUMBER:=null
 ,p_PYMTPOSTINTACTUALBASISDAYS NUMBER:=null
 ,p_PYMTPOSTINTAPPLAMT NUMBER:=null
 ,p_PYMTPOSTINTBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTINTBASISCALC NUMBER:=null
 ,p_PYMTPOSTINTDUEAMT NUMBER:=null
 ,p_PYMTPOSTINTPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTINTPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTINTRJCTAMT NUMBER:=null
 ,p_PYMTPOSTINTRTUSEDPCT NUMBER:=null
 ,p_PYMTPOSTINTWAIVEDAMT NUMBER:=null
 ,p_PYMTPOSTLATEFEEASSESSAMT NUMBER:=null
 ,p_PYMTPOSTLATEFEEPAIDAMT NUMBER:=null
 ,p_PYMTPOSTLEFTOVRAMT NUMBER:=null
 ,p_PYMTPOSTLEFTOVRRJCTAMT NUMBER:=null
 ,p_PYMTPOSTLPYMTAMT NUMBER:=null
 ,p_PYMTPOSTPAIDBYCOLSONDTX CHAR:=null
 ,p_PYMTPOSTPREPAYAMT NUMBER:=null
 ,p_PYMTPOSTPRINAPPLAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALLEFTAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTPRINDUEAMT NUMBER:=null
 ,p_PYMTPOSTPRINRJCTAMT NUMBER:=null
 ,p_PYMTPOSTPYMTDT DATE:=null
 ,p_PYMTPOSTPYMTNMB CHAR:=null
 ,p_PYMTPOSTPYMTTYP CHAR:=null
 ,p_PYMTPOSTREMITTEDAMT NUMBER:=null
 ,p_PYMTPOSTREMITTEDRJCTAMT NUMBER:=null
 ,p_PYMTPOSTRJCTCD CHAR:=null
 ,p_PYMTPOSTRJCTDT DATE:=null
 ,p_PYMTPOSTSBAFEEAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEAPPLAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEENDDTX CHAR:=null
 ,p_PYMTPOSTSBAFEEREPDAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEESTARTDTX CHAR:=null
 ,p_PYMTPOSTXADJCD CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:35:46
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:35:46
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVPYMTDELTSP performs DELETE on STGCSA.SOFVPYMTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, PYMTPOSTPYMTDT
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVPYMTTBL%rowtype;
 new_rec STGCSA.SOFVPYMTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVPYMTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVPYMTDELTSP build 2018-01-23 08:35:46 '
 ||'Select of STGCSA.SOFVPYMTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVPYMTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVPYMTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVPYMTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVPYMTDELTSP build 2018-01-23 08:35:46 '
 ||'while doing delete on STGCSA.SOFVPYMTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVPYMTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVPYMTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVPYMTTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ''(PYMTPOSTPYMTDT)=('||P_PYMTPOSTPYMTDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVPYMTTBL where 1=1
 and LOANNMB=P_LOANNMB and PYMTPOSTPYMTDT=P_PYMTPOSTPYMTDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVPYMTDELTSP build 2018-01-23 08:35:46 '
 ||'No STGCSA.SOFVPYMTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVPYMTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVPYMTDELTSP build 2018-01-23 08:35:46 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVPYMTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVPYMTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVPYMTDELTSP build 2018-01-23 08:35:46'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVPYMTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVPYMTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


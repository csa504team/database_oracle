<!--- Saved 01/28/2018 14:41:38. --->
PROCEDURE CNTCTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCREF NUMBER:=null
 ,p_CNTCTEMAILADR VARCHAR2:=null
 ,p_CNTCTFAXNMB VARCHAR2:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_CNTCTID NUMBER:=null
 ,p_CNTCTLASTNM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTPHNNMB VARCHAR2:=null
 ,p_CNTCTPHNXTN CHAR:=null
 ,p_CNTCTTITL VARCHAR2:=null
 ,p_CREATBY NUMBER:=null
 ,p_ENTID NUMBER:=null
 ,p_UPDTBY NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:24
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:18:24
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CNTCTUPDTSP performs UPDATE on STGCSA.CNTCTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CNTCTID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CNTCTTBL%rowtype;
 new_rec STGCSA.CNTCTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CNTCTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTUPDTSP build 2018-01-23 08:18:24 '
 ||'Select of STGCSA.CNTCTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CNTCTID is not null then
 cur_col_name:='P_CNTCTID';
 new_rec.CNTCTID:=P_CNTCTID; 
 end if;
 if P_CNTCTFIRSTNM is not null then
 cur_col_name:='P_CNTCTFIRSTNM';
 new_rec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM; 
 end if;
 if P_CNTCTLASTNM is not null then
 cur_col_name:='P_CNTCTLASTNM';
 new_rec.CNTCTLASTNM:=P_CNTCTLASTNM; 
 end if;
 if P_CNTCTEMAILADR is not null then
 cur_col_name:='P_CNTCTEMAILADR';
 new_rec.CNTCTEMAILADR:=P_CNTCTEMAILADR; 
 end if;
 if P_CNTCTPHNNMB is not null then
 cur_col_name:='P_CNTCTPHNNMB';
 new_rec.CNTCTPHNNMB:=P_CNTCTPHNNMB; 
 end if;
 if P_CNTCTPHNXTN is not null then
 cur_col_name:='P_CNTCTPHNXTN';
 new_rec.CNTCTPHNXTN:=P_CNTCTPHNXTN; 
 end if;
 if P_CNTCTFAXNMB is not null then
 cur_col_name:='P_CNTCTFAXNMB';
 new_rec.CNTCTFAXNMB:=P_CNTCTFAXNMB; 
 end if;
 if P_CNTCTMAILADRSTR1NM is not null then
 cur_col_name:='P_CNTCTMAILADRSTR1NM';
 new_rec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM; 
 end if;
 if P_CNTCTMAILADRSTR2NM is not null then
 cur_col_name:='P_CNTCTMAILADRSTR2NM';
 new_rec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM; 
 end if;
 if P_CNTCTMAILADRCTYNM is not null then
 cur_col_name:='P_CNTCTMAILADRCTYNM';
 new_rec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM; 
 end if;
 if P_CNTCTMAILADRSTCD is not null then
 cur_col_name:='P_CNTCTMAILADRSTCD';
 new_rec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD; 
 end if;
 if P_CNTCTMAILADRZIPCD is not null then
 cur_col_name:='P_CNTCTMAILADRZIPCD';
 new_rec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD; 
 end if;
 if P_CNTCTMAILADRZIP4CD is not null then
 cur_col_name:='P_CNTCTMAILADRZIP4CD';
 new_rec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD; 
 end if;
 if P_CNTCTTITL is not null then
 cur_col_name:='P_CNTCTTITL';
 new_rec.CNTCTTITL:=P_CNTCTTITL; 
 end if;
 if P_CDCREF is not null then
 cur_col_name:='P_CDCREF';
 new_rec.CDCREF:=P_CDCREF; 
 end if;
 if P_CREATBY is not null then
 cur_col_name:='P_CREATBY';
 new_rec.CREATBY:=P_CREATBY; 
 end if;
 if P_UPDTBY is not null then
 cur_col_name:='P_UPDTBY';
 new_rec.UPDTBY:=P_UPDTBY; 
 end if;
 if P_ENTID is not null then
 cur_col_name:='P_ENTID';
 new_rec.ENTID:=P_ENTID; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CNTCTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CNTCTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CNTCTFIRSTNM=new_rec.CNTCTFIRSTNM
 ,CNTCTLASTNM=new_rec.CNTCTLASTNM
 ,CNTCTEMAILADR=new_rec.CNTCTEMAILADR
 ,CNTCTPHNNMB=new_rec.CNTCTPHNNMB
 ,CNTCTPHNXTN=new_rec.CNTCTPHNXTN
 ,CNTCTFAXNMB=new_rec.CNTCTFAXNMB
 ,CNTCTMAILADRSTR1NM=new_rec.CNTCTMAILADRSTR1NM
 ,CNTCTMAILADRSTR2NM=new_rec.CNTCTMAILADRSTR2NM
 ,CNTCTMAILADRCTYNM=new_rec.CNTCTMAILADRCTYNM
 ,CNTCTMAILADRSTCD=new_rec.CNTCTMAILADRSTCD
 ,CNTCTMAILADRZIPCD=new_rec.CNTCTMAILADRZIPCD
 ,CNTCTMAILADRZIP4CD=new_rec.CNTCTMAILADRZIP4CD
 ,CNTCTTITL=new_rec.CNTCTTITL
 ,CDCREF=new_rec.CDCREF
 ,CREATBY=new_rec.CREATBY
 ,UPDTBY=new_rec.UPDTBY
 ,ENTID=new_rec.ENTID
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CNTCTUPDTSP build 2018-01-23 08:18:24 '
 ||'while doing update on STGCSA.CNTCTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CNTCTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CNTCTID)=('||P_CNTCTID||') ';
 select rowid into rec_rowid from STGCSA.CNTCTTBL where 1=1
 and CNTCTID=P_CNTCTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTUPDTSP build 2018-01-23 08:18:24 '
 ||'No STGCSA.CNTCTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In CNTCTUPDTSP build 2018-01-23 08:18:24 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In CNTCTUPDTSP build 2018-01-23 08:18:24'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CNTCTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


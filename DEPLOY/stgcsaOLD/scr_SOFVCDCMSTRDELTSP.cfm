<!--- Saved 01/28/2018 14:42:32. --->
PROCEDURE SOFVCDCMSTRDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCACHACCTNMB VARCHAR2:=null
 ,p_CDCACHACCTTYP CHAR:=null
 ,p_CDCACHBNK VARCHAR2:=null
 ,p_CDCACHBNKIDNMB VARCHAR2:=null
 ,p_CDCACHBR CHAR:=null
 ,p_CDCACHDEPSTR VARCHAR2:=null
 ,p_CDCACHMAILADRCTYNM VARCHAR2:=null
 ,p_CDCACHMAILADRSTCD CHAR:=null
 ,p_CDCACHMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCACHMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHMAILADRZIP4CD CHAR:=null
 ,p_CDCACHMAILADRZIPCD CHAR:=null
 ,p_CDCACHPRENTIND CHAR:=null
 ,p_CDCACHROUTSYM CHAR:=null
 ,p_CDCACHSIGNDT DATE:=null
 ,p_CDCACHTRANSCD CHAR:=null
 ,p_CDCAREACD CHAR:=null
 ,p_CDCCERTNMB CHAR:=null
 ,p_CDCCLSBALAMT NUMBER:=null
 ,p_CDCCLSBALDT DATE:=null
 ,p_CDCCNTCT VARCHAR2:=null
 ,p_CDCDIST CHAR:=null
 ,p_CDCEXCH CHAR:=null
 ,p_CDCEXTN CHAR:=null
 ,p_CDCFAXAREACD CHAR:=null
 ,p_CDCFAXEXCH CHAR:=null
 ,p_CDCFAXEXTN CHAR:=null
 ,p_CDCHLDPYMT CHAR:=null
 ,p_CDCLMAINTNDT DATE:=null
 ,p_CDCMAILADRCTYNM VARCHAR2:=null
 ,p_CDCMAILADRSTCD CHAR:=null
 ,p_CDCMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCMAILADRZIP4CD CHAR:=null
 ,p_CDCMAILADRZIPCD CHAR:=null
 ,p_CDCNAMADDRID CHAR:=null
 ,p_CDCNETPREPAYAMT NUMBER:=null
 ,p_CDCNM VARCHAR2:=null
 ,p_CDCPOLKNMB CHAR:=null
 ,p_CDCPRENOTETESTDT DATE:=null
 ,p_CDCPREPAYCLSBALAMT NUMBER:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCSETUPDT DATE:=null
 ,p_CDCSTATCD CHAR:=null
 ,p_CDCTAXID CHAR:=null
 ,p_CDCTOTDOLLRDBENTRAMT NUMBER:=null
 ,p_CDCTOTNMBPART NUMBER:=null
 ,p_CDCTOTPAIDDBENTRAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:21
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:31:22
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVCDCMSTRDELTSP performs DELETE on STGCSA.SOFVCDCMSTRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CDCREGNCD, CDCCERTNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 new_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVCDCMSTRTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCDCMSTRDELTSP build 2018-01-23 08:31:21 '
 ||'Select of STGCSA.SOFVCDCMSTRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCDCMSTRDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVCDCMSTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVCDCMSTRTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVCDCMSTRDELTSP build 2018-01-23 08:31:21 '
 ||'while doing delete on STGCSA.SOFVCDCMSTRTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCDCMSTRDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVCDCMSTRDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCDCMSTRTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CDCREGNCD)=('||P_CDCREGNCD||') ''(CDCCERTNMB)=('||P_CDCCERTNMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVCDCMSTRTBL where 1=1
 and CDCREGNCD=P_CDCREGNCD and CDCCERTNMB=P_CDCCERTNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCDCMSTRDELTSP build 2018-01-23 08:31:21 '
 ||'No STGCSA.SOFVCDCMSTRTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCDCMSTRDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVCDCMSTRDELTSP build 2018-01-23 08:31:21 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCDCMSTRDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCDCMSTRDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVCDCMSTRDELTSP build 2018-01-23 08:31:21'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCDCMSTRDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVCDCMSTRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


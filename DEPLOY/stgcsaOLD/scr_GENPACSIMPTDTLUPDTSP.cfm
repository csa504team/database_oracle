<!--- Saved 01/28/2018 14:42:07. --->
PROCEDURE GENPACSIMPTDTLUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ABABNKNM VARCHAR2:=null
 ,p_ABABNKNMB VARCHAR2:=null
 ,p_ACCTNM VARCHAR2:=null
 ,p_ACHADDENDA VARCHAR2:=null
 ,p_APPVBY VARCHAR2:=null
 ,p_BNKIBAN VARCHAR2:=null
 ,p_BNKIRC VARCHAR2:=null
 ,p_DISBCD VARCHAR2:=null
 ,p_INITBY VARCHAR2:=null
 ,p_INTRMEDRYABANMB VARCHAR2:=null
 ,p_INTRMEDRYSWIFT VARCHAR2:=null
 ,p_INTRNLEXTRNLACCT VARCHAR2:=null
 ,p_PACSACCT VARCHAR2:=null
 ,p_PACSIMPTDTLID NUMBER:=null
 ,p_PACSIMPTID NUMBER:=null
 ,p_PACSIMPTPTIC VARCHAR2:=null
 ,p_POSTEDDT DATE:=null
 ,p_POSTERLOC VARCHAR2:=null
 ,p_PRININC VARCHAR2:=null
 ,p_PYMTTYP VARCHAR2:=null
 ,p_RCPTCD VARCHAR2:=null
 ,p_RELTDTBLNM VARCHAR2:=null
 ,p_RELTDTBLREF NUMBER:=null
 ,p_RQSTAU VARCHAR2:=null
 ,p_RQSTID VARCHAR2:=null
 ,p_RQSTTYP VARCHAR2:=null
 ,p_SEIBTCHID VARCHAR2:=null
 ,p_SEITRANTYP VARCHAR2:=null
 ,p_STATCD VARCHAR2:=null
 ,p_SWIFTBNK VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANAU VARCHAR2:=null
 ,p_TRANSAMT VARCHAR2:=null
 ,p_TRANSDT DATE:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_TRANSINFO VARCHAR2:=null
 ,p_TRANSTYP VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 17:11:13
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 17:11:13
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure GENPACSIMPTDTLUPDTSP performs UPDATE on STGCSA.GENPACSIMPTDTLTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PACSIMPTDTLID
 for P_IDENTIFIER=1 qualification is on RQSTID
 for P_IDENTIFIER=2 qualification is on PACSIMPTID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.GENPACSIMPTDTLTBL%rowtype;
 new_rec STGCSA.GENPACSIMPTDTLTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.GENPACSIMPTDTLTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'Select of STGCSA.GENPACSIMPTDTLTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PACSIMPTDTLID is not null then
 cur_col_name:='P_PACSIMPTDTLID';
 new_rec.PACSIMPTDTLID:=P_PACSIMPTDTLID; 
 end if;
 if P_PACSIMPTID is not null then
 cur_col_name:='P_PACSIMPTID';
 new_rec.PACSIMPTID:=P_PACSIMPTID; 
 end if;
 if P_RQSTID is not null then
 cur_col_name:='P_RQSTID';
 new_rec.RQSTID:=P_RQSTID; 
 end if;
 if P_RELTDTBLNM is not null then
 cur_col_name:='P_RELTDTBLNM';
 new_rec.RELTDTBLNM:=P_RELTDTBLNM; 
 end if;
 if P_RELTDTBLREF is not null then
 cur_col_name:='P_RELTDTBLREF';
 new_rec.RELTDTBLREF:=P_RELTDTBLREF; 
 end if;
 if P_RQSTTYP is not null then
 cur_col_name:='P_RQSTTYP';
 new_rec.RQSTTYP:=P_RQSTTYP; 
 end if;
 if P_PYMTTYP is not null then
 cur_col_name:='P_PYMTTYP';
 new_rec.PYMTTYP:=P_PYMTTYP; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_TRANSAMT is not null then
 cur_col_name:='P_TRANSAMT';
 new_rec.TRANSAMT:=P_TRANSAMT; 
 end if;
 if P_TRANSTYP is not null then
 cur_col_name:='P_TRANSTYP';
 new_rec.TRANSTYP:=P_TRANSTYP; 
 end if;
 if P_PACSACCT is not null then
 cur_col_name:='P_PACSACCT';
 new_rec.PACSACCT:=P_PACSACCT; 
 end if;
 if P_ACCTNM is not null then
 cur_col_name:='P_ACCTNM';
 new_rec.ACCTNM:=P_ACCTNM; 
 end if;
 if P_INTRNLEXTRNLACCT is not null then
 cur_col_name:='P_INTRNLEXTRNLACCT';
 new_rec.INTRNLEXTRNLACCT:=P_INTRNLEXTRNLACCT; 
 end if;
 if P_TRANAU is not null then
 cur_col_name:='P_TRANAU';
 new_rec.TRANAU:=P_TRANAU; 
 end if;
 if P_ABABNKNMB is not null then
 cur_col_name:='P_ABABNKNMB';
 new_rec.ABABNKNMB:=P_ABABNKNMB; 
 end if;
 if P_ABABNKNM is not null then
 cur_col_name:='P_ABABNKNM';
 new_rec.ABABNKNM:=P_ABABNKNM; 
 end if;
 if P_SWIFTBNK is not null then
 cur_col_name:='P_SWIFTBNK';
 new_rec.SWIFTBNK:=P_SWIFTBNK; 
 end if;
 if P_INTRMEDRYABANMB is not null then
 cur_col_name:='P_INTRMEDRYABANMB';
 new_rec.INTRMEDRYABANMB:=P_INTRMEDRYABANMB; 
 end if;
 if P_INTRMEDRYSWIFT is not null then
 cur_col_name:='P_INTRMEDRYSWIFT';
 new_rec.INTRMEDRYSWIFT:=P_INTRMEDRYSWIFT; 
 end if;
 if P_BNKIRC is not null then
 cur_col_name:='P_BNKIRC';
 new_rec.BNKIRC:=P_BNKIRC; 
 end if;
 if P_BNKIBAN is not null then
 cur_col_name:='P_BNKIBAN';
 new_rec.BNKIBAN:=P_BNKIBAN; 
 end if;
 if P_TRANSINFO is not null then
 cur_col_name:='P_TRANSINFO';
 new_rec.TRANSINFO:=P_TRANSINFO; 
 end if;
 if P_STATCD is not null then
 cur_col_name:='P_STATCD';
 new_rec.STATCD:=P_STATCD; 
 end if;
 if P_TRANSDT is not null then
 cur_col_name:='P_TRANSDT';
 new_rec.TRANSDT:=P_TRANSDT; 
 end if;
 if P_POSTEDDT is not null then
 cur_col_name:='P_POSTEDDT';
 new_rec.POSTEDDT:=P_POSTEDDT; 
 end if;
 if P_POSTERLOC is not null then
 cur_col_name:='P_POSTERLOC';
 new_rec.POSTERLOC:=P_POSTERLOC; 
 end if;
 if P_PACSIMPTPTIC is not null then
 cur_col_name:='P_PACSIMPTPTIC';
 new_rec.PACSIMPTPTIC:=P_PACSIMPTPTIC; 
 end if;
 if P_RCPTCD is not null then
 cur_col_name:='P_RCPTCD';
 new_rec.RCPTCD:=P_RCPTCD; 
 end if;
 if P_DISBCD is not null then
 cur_col_name:='P_DISBCD';
 new_rec.DISBCD:=P_DISBCD; 
 end if;
 if P_PRININC is not null then
 cur_col_name:='P_PRININC';
 new_rec.PRININC:=P_PRININC; 
 end if;
 if P_SEIBTCHID is not null then
 cur_col_name:='P_SEIBTCHID';
 new_rec.SEIBTCHID:=P_SEIBTCHID; 
 end if;
 if P_SEITRANTYP is not null then
 cur_col_name:='P_SEITRANTYP';
 new_rec.SEITRANTYP:=P_SEITRANTYP; 
 end if;
 if P_RQSTAU is not null then
 cur_col_name:='P_RQSTAU';
 new_rec.RQSTAU:=P_RQSTAU; 
 end if;
 if P_INITBY is not null then
 cur_col_name:='P_INITBY';
 new_rec.INITBY:=P_INITBY; 
 end if;
 if P_APPVBY is not null then
 cur_col_name:='P_APPVBY';
 new_rec.APPVBY:=P_APPVBY; 
 end if;
 if P_ACHADDENDA is not null then
 cur_col_name:='P_ACHADDENDA';
 new_rec.ACHADDENDA:=P_ACHADDENDA; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.GENPACSIMPTDTLAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENPACSIMPTDTLTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,PACSIMPTID=new_rec.PACSIMPTID
 ,RQSTID=new_rec.RQSTID
 ,RELTDTBLNM=new_rec.RELTDTBLNM
 ,RELTDTBLREF=new_rec.RELTDTBLREF
 ,RQSTTYP=new_rec.RQSTTYP
 ,PYMTTYP=new_rec.PYMTTYP
 ,TRANSID=new_rec.TRANSID
 ,TRANSAMT=new_rec.TRANSAMT
 ,TRANSTYP=new_rec.TRANSTYP
 ,PACSACCT=new_rec.PACSACCT
 ,ACCTNM=new_rec.ACCTNM
 ,INTRNLEXTRNLACCT=new_rec.INTRNLEXTRNLACCT
 ,TRANAU=new_rec.TRANAU
 ,ABABNKNMB=new_rec.ABABNKNMB
 ,ABABNKNM=new_rec.ABABNKNM
 ,SWIFTBNK=new_rec.SWIFTBNK
 ,INTRMEDRYABANMB=new_rec.INTRMEDRYABANMB
 ,INTRMEDRYSWIFT=new_rec.INTRMEDRYSWIFT
 ,BNKIRC=new_rec.BNKIRC
 ,BNKIBAN=new_rec.BNKIBAN
 ,TRANSINFO=new_rec.TRANSINFO
 ,STATCD=new_rec.STATCD
 ,TRANSDT=new_rec.TRANSDT
 ,POSTEDDT=new_rec.POSTEDDT
 ,POSTERLOC=new_rec.POSTERLOC
 ,PACSIMPTPTIC=new_rec.PACSIMPTPTIC
 ,RCPTCD=new_rec.RCPTCD
 ,DISBCD=new_rec.DISBCD
 ,PRININC=new_rec.PRININC
 ,SEIBTCHID=new_rec.SEIBTCHID
 ,SEITRANTYP=new_rec.SEITRANTYP
 ,RQSTAU=new_rec.RQSTAU
 ,INITBY=new_rec.INITBY
 ,APPVBY=new_rec.APPVBY
 ,ACHADDENDA=new_rec.ACHADDENDA
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'while doing update on STGCSA.GENPACSIMPTDTLTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENPACSIMPTDTLUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PACSIMPTDTLID)=('||P_PACSIMPTDTLID||') ';
 select rowid into rec_rowid from STGCSA.GENPACSIMPTDTLTBL where 1=1
 and PACSIMPTDTLID=P_PACSIMPTDTLID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'No STGCSA.GENPACSIMPTDTLTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(RQSTID)=('||P_RQSTID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.GENPACSIMPTDTLTBL a where 1=1 
 and RQSTID=P_RQSTID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'Oracle returned error fetching STGCSA.GENPACSIMPTDTLTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'No STGCSA.GENPACSIMPTDTLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(PACSIMPTID)=('||P_PACSIMPTID||') '||'';
 begin
 for r2 in (select rowid from STGCSA.GENPACSIMPTDTLTBL a where 1=1 
 and PACSIMPTID=P_PACSIMPTID
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'Oracle returned error fetching STGCSA.GENPACSIMPTDTLTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 ||'No STGCSA.GENPACSIMPTDTLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TRANSID:=new_rec.TRANSID; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENPACSIMPTDTLUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENPACSIMPTDTLUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In GENPACSIMPTDTLUPDTSP build 2018-01-23 17:11:13'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENPACSIMPTDTLUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END GENPACSIMPTDTLUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


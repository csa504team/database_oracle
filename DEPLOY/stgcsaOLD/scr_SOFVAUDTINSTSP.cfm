<!--- Saved 12/31/2017 09:00:20. --->
PROCEDURE SOFVAUDTINSTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2
 ,p_AUDTRECRDKEY CHAR:=null
 ,p_AUDTTRANSDT TIMESTAMP:=null
 ,p_AUDTSEQNMB NUMBER:=null
 ,p_AUDTOPRTRINITIAL CHAR:=null
 ,p_AUDTTRMNLID CHAR:=null
 ,p_AUDTCHNGFILE CHAR:=null
 ,p_AUDTACTNCD CHAR:=null
 ,p_AUDTBEFCNTNTS VARCHAR2:=null
 ,p_AUDTAFTCNTNTS VARCHAR2:=null
 ,p_AUDTFLDNM CHAR:=null
 ,p_AUDTEDTCD CHAR:=null
 ,p_AUDTUSERID VARCHAR2:=null
 ) as
 /*
 Created on: 2017-10-29 14:25:59
 Created by: GENR
 Crerated from template stdtblins_template v2.0 24 Oct 2017 on 2017-10-29 14:25:59
 Using SNAP V1.2 24 Oct 2017 J. Low Binary Frond, Select Computing
 Purpose : standard insert of row to table STGCSA.SOFVAUDTTBL
 */
 errfound boolean;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 rec_t1 STGCSA.SOFVAUDTTBL%rowtype;
 begin
 -- setup
 savepoint SOFVAUDTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null; 
 rec_t1.creatuserid:=p_userid;
 rec_t1.creatdt:=sysdate;
 rec_t1.lastupdtuserid:=p_userid;
 rec_t1.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure 
 -- copy pased table column parm values to rec_t1
 rec_t1.AUDTRECRDKEY:=P_AUDTRECRDKEY;
 rec_t1.AUDTTRANSDT:=P_AUDTTRANSDT;
 rec_t1.AUDTSEQNMB:=P_AUDTSEQNMB;
 rec_t1.AUDTOPRTRINITIAL:=P_AUDTOPRTRINITIAL;
 rec_t1.AUDTTRMNLID:=P_AUDTTRMNLID;
 rec_t1.AUDTCHNGFILE:=P_AUDTCHNGFILE;
 rec_t1.AUDTACTNCD:=P_AUDTACTNCD;
 rec_t1.AUDTBEFCNTNTS:=P_AUDTBEFCNTNTS;
 rec_t1.AUDTAFTCNTNTS:=P_AUDTAFTCNTNTS;
 rec_t1.AUDTFLDNM:=P_AUDTFLDNM;
 rec_t1.AUDTEDTCD:=P_AUDTEDTCD;
 rec_t1.AUDTUSERID:=P_AUDTUSERID;
 
 -- check for passed nulls 
 --
 If rec_t1.AUDTRECRDKEY is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTRECRDKEY,
 'STGCSA','SOFVAUDTTBL','AUDTRECRDKEY',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTRECRDKEY';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTTRANSDT is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTTRANSDT,
 'STGCSA','SOFVAUDTTBL','AUDTTRANSDT',
 'TIMESTAMP(6)',11,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTTRANSDT';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTSEQNMB is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTSEQNMB,
 'STGCSA','SOFVAUDTTBL','AUDTSEQNMB',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTSEQNMB';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTOPRTRINITIAL is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTOPRTRINITIAL,
 'STGCSA','SOFVAUDTTBL','AUDTOPRTRINITIAL',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTOPRTRINITIAL';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTTRMNLID is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTTRMNLID,
 'STGCSA','SOFVAUDTTBL','AUDTTRMNLID',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTTRMNLID';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTCHNGFILE is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTCHNGFILE,
 'STGCSA','SOFVAUDTTBL','AUDTCHNGFILE',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTCHNGFILE';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTACTNCD is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTACTNCD,
 'STGCSA','SOFVAUDTTBL','AUDTACTNCD',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTACTNCD';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTBEFCNTNTS is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTBEFCNTNTS,
 'STGCSA','SOFVAUDTTBL','AUDTBEFCNTNTS',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTBEFCNTNTS';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTAFTCNTNTS is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTAFTCNTNTS,
 'STGCSA','SOFVAUDTTBL','AUDTAFTCNTNTS',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTAFTCNTNTS';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTFLDNM is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTFLDNM,
 'STGCSA','SOFVAUDTTBL','AUDTFLDNM',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTFLDNM';
 end if;
 end if;
 end if;
 --
 If rec_t1.AUDTEDTCD is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTEDTCD,
 'STGCSA','SOFVAUDTTBL','AUDTEDTCD',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 end if;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTEDTCD';
 end if;
 end if;
 --
 If rec_t1.AUDTUSERID is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.AUDTUSERID,
 'STGCSA','SOFVAUDTTBL','AUDTUSERID',
 'VARCHAR2',32,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', AUDTUSERID';
 end if;
 end if;
 end if;
 -- end of nulls checking
 --
 if errfound=true then
 ROLLBACK TO SOFVAUDTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more NULL inputs: '||substr(errstr,3);
 else 
 --
 insert into STGCSA.SOFVAUDTTBL values rec_t1;
 p_RETVAL := SQL%ROWCOUNT;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVAUDTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO SOFVAUDTINSTSP;
 --
 END SOFVAUDTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


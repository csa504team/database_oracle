<!--- Saved 01/28/2018 14:41:58. --->
PROCEDURE GENEMAILARCHVINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMAILID NUMBER:=null
 ,p_DBMDL VARCHAR2:=null
 ,p_RELTDTBLNM VARCHAR2:=null
 ,p_RELTDTBLREF NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_EMAILCATID NUMBER:=null
 ,p_EMAILTO VARCHAR2:=null
 ,p_EMAILFROMNM VARCHAR2:=null
 ,p_EMAILFROMADR VARCHAR2:=null
 ,p_EMAILCC VARCHAR2:=null
 ,p_EMAILSUBJ VARCHAR2:=null
 ,p_EMAILBODY VARCHAR2:=null
 ,p_EMAILSENTDT DATE:=null
 ,p_MSGFILENM VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:23:26
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:23:26
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.GENEMAILARCHVTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.GENEMAILARCHVTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint GENEMAILARCHVINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_DBMDL:=P_DBMDL; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_EMAILID';
 new_rec.EMAILID:=runtime.validate_column_value(P_EMAILID,
 'STGCSA','GENEMAILARCHVTBL','EMAILID','NUMBER',22);
 cur_col_name:='P_DBMDL';
 new_rec.DBMDL:=runtime.validate_column_value(P_DBMDL,
 'STGCSA','GENEMAILARCHVTBL','DBMDL','VARCHAR2',50);
 cur_col_name:='P_RELTDTBLNM';
 new_rec.RELTDTBLNM:=runtime.validate_column_value(P_RELTDTBLNM,
 'STGCSA','GENEMAILARCHVTBL','RELTDTBLNM','VARCHAR2',100);
 cur_col_name:='P_RELTDTBLREF';
 new_rec.RELTDTBLREF:=runtime.validate_column_value(P_RELTDTBLREF,
 'STGCSA','GENEMAILARCHVTBL','RELTDTBLREF','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','GENEMAILARCHVTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_EMAILCATID';
 new_rec.EMAILCATID:=runtime.validate_column_value(P_EMAILCATID,
 'STGCSA','GENEMAILARCHVTBL','EMAILCATID','NUMBER',22);
 cur_col_name:='P_EMAILTO';
 new_rec.EMAILTO:=runtime.validate_column_value(P_EMAILTO,
 'STGCSA','GENEMAILARCHVTBL','EMAILTO','VARCHAR2',260);
 cur_col_name:='P_EMAILFROMNM';
 new_rec.EMAILFROMNM:=runtime.validate_column_value(P_EMAILFROMNM,
 'STGCSA','GENEMAILARCHVTBL','EMAILFROMNM','VARCHAR2',260);
 cur_col_name:='P_EMAILFROMADR';
 new_rec.EMAILFROMADR:=runtime.validate_column_value(P_EMAILFROMADR,
 'STGCSA','GENEMAILARCHVTBL','EMAILFROMADR','VARCHAR2',150);
 cur_col_name:='P_EMAILCC';
 new_rec.EMAILCC:=runtime.validate_column_value(P_EMAILCC,
 'STGCSA','GENEMAILARCHVTBL','EMAILCC','VARCHAR2',260);
 cur_col_name:='P_EMAILSUBJ';
 new_rec.EMAILSUBJ:=runtime.validate_column_value(P_EMAILSUBJ,
 'STGCSA','GENEMAILARCHVTBL','EMAILSUBJ','VARCHAR2',260);
 cur_col_name:='P_EMAILBODY';
 new_rec.EMAILBODY:=runtime.validate_column_value(P_EMAILBODY,
 'STGCSA','GENEMAILARCHVTBL','EMAILBODY','VARCHAR2',1000);
 cur_col_name:='P_EMAILSENTDT';
 new_rec.EMAILSENTDT:=runtime.validate_column_value(P_EMAILSENTDT,
 'STGCSA','GENEMAILARCHVTBL','EMAILSENTDT','DATE',7);
 cur_col_name:='P_MSGFILENM';
 new_rec.MSGFILENM:=runtime.validate_column_value(P_MSGFILENM,
 'STGCSA','GENEMAILARCHVTBL','MSGFILENM','VARCHAR2',260);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','GENEMAILARCHVTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO GENEMAILARCHVINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.GENEMAILARCHVAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.GENEMAILARCHVTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO GENEMAILARCHVINSTSP;
 p_errmsg:='Got dupe key on insert into GENEMAILARCHVTBL for key '
 ||'(EMAILID)=('||new_rec.EMAILID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENEMAILARCHVINSTSP build 2018-01-23 08:23:26';
 ROLLBACK TO GENEMAILARCHVINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILARCHVINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILARCHVINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILARCHVINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in GENEMAILARCHVINSTSP build 2018-01-23 08:23:26'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILARCHVINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END GENEMAILARCHVINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


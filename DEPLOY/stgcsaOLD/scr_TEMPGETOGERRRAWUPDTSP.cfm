<!--- Saved 01/28/2018 14:42:56. --->
PROCEDURE TEMPGETOGERRRAWUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DTCHKFALSE NUMBER:=null
 ,p_DTCHKIND CHAR:=null
 ,p_DTCHKTRUE NUMBER:=null
 ,p_GETOPENGNTYERRRAWID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_OPENGNTYERRAMT NUMBER:=null
 ,p_POSTINGDT DATE:=null
 ,p_PREPAYDT DATE:=null
 ,p_THIRDTHURSDAYDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:36:49
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:36:49
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure TEMPGETOGERRRAWUPDTSP performs UPDATE on STGCSA.TEMPGETOGERRRAWTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: GETOPENGNTYERRRAWID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.TEMPGETOGERRRAWTBL%rowtype;
 new_rec STGCSA.TEMPGETOGERRRAWTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.TEMPGETOGERRRAWTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPGETOGERRRAWUPDTSP build 2018-01-23 08:36:49 '
 ||'Select of STGCSA.TEMPGETOGERRRAWTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPGETOGERRRAWUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_GETOPENGNTYERRRAWID is not null then
 cur_col_name:='P_GETOPENGNTYERRRAWID';
 new_rec.GETOPENGNTYERRRAWID:=P_GETOPENGNTYERRRAWID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_OPENGNTYERRAMT is not null then
 cur_col_name:='P_OPENGNTYERRAMT';
 new_rec.OPENGNTYERRAMT:=P_OPENGNTYERRAMT; 
 end if;
 if P_THIRDTHURSDAYDT is not null then
 cur_col_name:='P_THIRDTHURSDAYDT';
 new_rec.THIRDTHURSDAYDT:=P_THIRDTHURSDAYDT; 
 end if;
 if P_PREPAYDT is not null then
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_POSTINGDT is not null then
 cur_col_name:='P_POSTINGDT';
 new_rec.POSTINGDT:=P_POSTINGDT; 
 end if;
 if P_DTCHKIND is not null then
 cur_col_name:='P_DTCHKIND';
 new_rec.DTCHKIND:=P_DTCHKIND; 
 end if;
 if P_DTCHKTRUE is not null then
 cur_col_name:='P_DTCHKTRUE';
 new_rec.DTCHKTRUE:=P_DTCHKTRUE; 
 end if;
 if P_DTCHKFALSE is not null then
 cur_col_name:='P_DTCHKFALSE';
 new_rec.DTCHKFALSE:=P_DTCHKFALSE; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.TEMPGETOGERRRAWAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.TEMPGETOGERRRAWTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,OPENGNTYERRAMT=new_rec.OPENGNTYERRAMT
 ,THIRDTHURSDAYDT=new_rec.THIRDTHURSDAYDT
 ,PREPAYDT=new_rec.PREPAYDT
 ,POSTINGDT=new_rec.POSTINGDT
 ,DTCHKIND=new_rec.DTCHKIND
 ,DTCHKTRUE=new_rec.DTCHKTRUE
 ,DTCHKFALSE=new_rec.DTCHKFALSE
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In TEMPGETOGERRRAWUPDTSP build 2018-01-23 08:36:49 '
 ||'while doing update on STGCSA.TEMPGETOGERRRAWTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPGETOGERRRAWUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint TEMPGETOGERRRAWUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(GETOPENGNTYERRRAWID)=('||P_GETOPENGNTYERRRAWID||') ';
 select rowid into rec_rowid from STGCSA.TEMPGETOGERRRAWTBL where 1=1
 and GETOPENGNTYERRRAWID=P_GETOPENGNTYERRRAWID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPGETOGERRRAWUPDTSP build 2018-01-23 08:36:49 '
 ||'No STGCSA.TEMPGETOGERRRAWTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO TEMPGETOGERRRAWUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In TEMPGETOGERRRAWUPDTSP build 2018-01-23 08:36:49 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPGETOGERRRAWUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'TEMPGETOGERRRAWUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In TEMPGETOGERRRAWUPDTSP build 2018-01-23 08:36:49'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO TEMPGETOGERRRAWUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END TEMPGETOGERRRAWUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


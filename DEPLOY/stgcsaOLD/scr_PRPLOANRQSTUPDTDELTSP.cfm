<!--- Saved 01/28/2018 14:42:16. --->
PROCEDURE PRPLOANRQSTUPDTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CORRECTVALAMT NUMBER:=null
 ,p_FORMNM VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_LOANRQSTFLDNM VARCHAR2:=null
 ,p_PRPCDCUPDTID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:56
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:27:56
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPLOANRQSTUPDTDELTSP performs DELETE on STGCSA.PRPLOANRQSTUPDTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPCDCUPDTID
 for P_IDENTIFIER=1 qualification is on TRANSID, LOANRQSTFLDNM, FORMNM
 for P_IDENTIFIER=2 qualification is on LOANNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPLOANRQSTUPDTTBL%rowtype;
 new_rec STGCSA.PRPLOANRQSTUPDTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPLOANRQSTUPDTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'Select of STGCSA.PRPLOANRQSTUPDTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPLOANRQSTUPDTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.PRPLOANRQSTUPDTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'while doing delete on STGCSA.PRPLOANRQSTUPDTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPLOANRQSTUPDTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPCDCUPDTID)=('||P_PRPCDCUPDTID||') ';
 select rowid into rec_rowid from STGCSA.PRPLOANRQSTUPDTTBL where 1=1
 and PRPCDCUPDTID=P_PRPCDCUPDTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(FORMNM)=('||P_FORMNM||') '||
 '(LOANRQSTFLDNM)=('||P_LOANRQSTFLDNM||') '||
 '(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.PRPLOANRQSTUPDTTBL a where 1=1 
 
 and FORMNM=P_FORMNM
 and LOANRQSTFLDNM=P_LOANRQSTFLDNM
 and TRANSID=P_TRANSID) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'Oracle returned error fetching STGCSA.PRPLOANRQSTUPDTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to delete one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r2 in (select rowid from STGCSA.PRPLOANRQSTUPDTTBL a where 1=1 
 
 and LOANNMB=P_LOANNMB) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'Oracle returned error fetching STGCSA.PRPLOANRQSTUPDTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB; log_TRANSID:=old_rec.TRANSID; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTUPDTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTUPDTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PRPLOANRQSTUPDTDELTSP build 2018-01-23 08:27:56'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTUPDTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPLOANRQSTUPDTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


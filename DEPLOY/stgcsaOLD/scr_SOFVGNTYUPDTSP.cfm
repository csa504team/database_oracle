<!--- Saved 01/28/2018 14:42:39. --->
PROCEDURE SOFVGNTYUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_GNTYCLSDT DATE:=null
 ,p_GNTYCMNT VARCHAR2:=null
 ,p_GNTYCMNT2 VARCHAR2:=null
 ,p_GNTYCREATDT DATE:=null
 ,p_GNTYDT DATE:=null
 ,p_GNTYGNTYAMT NUMBER:=null
 ,p_GNTYGNTYORGLDT DATE:=null
 ,p_GNTYLASTMAINTNDT DATE:=null
 ,p_GNTYSTATCD CHAR:=null
 ,p_GNTYTYP CHAR:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:53
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:32:53
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVGNTYUPDTSP performs UPDATE on STGCSA.SOFVGNTYTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, GNTYDT, GNTYTYP
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVGNTYTBL%rowtype;
 new_rec STGCSA.SOFVGNTYTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVGNTYTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVGNTYUPDTSP build 2018-01-23 08:32:53 '
 ||'Select of STGCSA.SOFVGNTYTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVGNTYUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_GNTYDT is not null then
 cur_col_name:='P_GNTYDT';
 new_rec.GNTYDT:=P_GNTYDT; 
 end if;
 if P_GNTYTYP is not null then
 cur_col_name:='P_GNTYTYP';
 new_rec.GNTYTYP:=P_GNTYTYP; 
 end if;
 if P_GNTYGNTYAMT is not null then
 cur_col_name:='P_GNTYGNTYAMT';
 new_rec.GNTYGNTYAMT:=P_GNTYGNTYAMT; 
 end if;
 if P_GNTYGNTYORGLDT is not null then
 cur_col_name:='P_GNTYGNTYORGLDT';
 new_rec.GNTYGNTYORGLDT:=P_GNTYGNTYORGLDT; 
 end if;
 if P_GNTYSTATCD is not null then
 cur_col_name:='P_GNTYSTATCD';
 new_rec.GNTYSTATCD:=P_GNTYSTATCD; 
 end if;
 if P_GNTYCLSDT is not null then
 cur_col_name:='P_GNTYCLSDT';
 new_rec.GNTYCLSDT:=P_GNTYCLSDT; 
 end if;
 if P_GNTYCREATDT is not null then
 cur_col_name:='P_GNTYCREATDT';
 new_rec.GNTYCREATDT:=P_GNTYCREATDT; 
 end if;
 if P_GNTYLASTMAINTNDT is not null then
 cur_col_name:='P_GNTYLASTMAINTNDT';
 new_rec.GNTYLASTMAINTNDT:=P_GNTYLASTMAINTNDT; 
 end if;
 if P_GNTYCMNT is not null then
 cur_col_name:='P_GNTYCMNT';
 new_rec.GNTYCMNT:=P_GNTYCMNT; 
 end if;
 if P_GNTYCMNT2 is not null then
 cur_col_name:='P_GNTYCMNT2';
 new_rec.GNTYCMNT2:=P_GNTYCMNT2; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVGNTYAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVGNTYTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,GNTYGNTYAMT=new_rec.GNTYGNTYAMT
 ,GNTYGNTYORGLDT=new_rec.GNTYGNTYORGLDT
 ,GNTYSTATCD=new_rec.GNTYSTATCD
 ,GNTYCLSDT=new_rec.GNTYCLSDT
 ,GNTYCREATDT=new_rec.GNTYCREATDT
 ,GNTYLASTMAINTNDT=new_rec.GNTYLASTMAINTNDT
 ,GNTYCMNT=new_rec.GNTYCMNT
 ,GNTYCMNT2=new_rec.GNTYCMNT2
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVGNTYUPDTSP build 2018-01-23 08:32:53 '
 ||'while doing update on STGCSA.SOFVGNTYTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVGNTYUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVGNTYUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVGNTYTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ''(GNTYDT)=('||P_GNTYDT||') ''(GNTYTYP)=('||P_GNTYTYP||') ';
 select rowid into rec_rowid from STGCSA.SOFVGNTYTBL where 1=1
 and LOANNMB=P_LOANNMB and GNTYDT=P_GNTYDT and GNTYTYP=P_GNTYTYP;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVGNTYUPDTSP build 2018-01-23 08:32:53 '
 ||'No STGCSA.SOFVGNTYTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVGNTYUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVGNTYUPDTSP build 2018-01-23 08:32:53 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVGNTYUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVGNTYUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVGNTYUPDTSP build 2018-01-23 08:32:53'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVGNTYUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVGNTYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:08. --->
PROCEDURE IMPTARPRPTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_IMPTARPRPTID NUMBER:=null
 ,p_STARTDT DATE:=null
 ,p_ENDDT DATE:=null
 ,p_BNKID VARCHAR2:=null
 ,p_BNKNM VARCHAR2:=null
 ,p_BNKSTCD CHAR:=null
 ,p_ACCTNMB VARCHAR2:=null
 ,p_ACCTTYP VARCHAR2:=null
 ,p_ACCTNM VARCHAR2:=null
 ,p_CURCY VARCHAR2:=null
 ,p_SCTNRPTNM VARCHAR2:=null
 ,p_TRANSTYP VARCHAR2:=null
 ,p_SRLNMBREFNMB VARCHAR2:=null
 ,p_CHKAMT NUMBER:=null
 ,p_ISSDT DATE:=null
 ,p_POSTEDDT DATE:=null
 ,p_STOPDT DATE:=null
 ,p_RLSEDT DATE:=null
 ,p_ASOFDT DATE:=null
 ,p_RVRSDDT DATE:=null
 ,p_DRCRDTCD VARCHAR2:=null
 ,p_OPTINFOTRANDESC VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:26:13
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:26:13
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.IMPTARPRPTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.IMPTARPRPTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint IMPTARPRPTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_IMPTARPRPTID';
 new_rec.IMPTARPRPTID:=runtime.validate_column_value(P_IMPTARPRPTID,
 'STGCSA','IMPTARPRPTTBL','IMPTARPRPTID','NUMBER',22);
 cur_col_name:='P_STARTDT';
 new_rec.STARTDT:=runtime.validate_column_value(P_STARTDT,
 'STGCSA','IMPTARPRPTTBL','STARTDT','DATE',7);
 cur_col_name:='P_ENDDT';
 new_rec.ENDDT:=runtime.validate_column_value(P_ENDDT,
 'STGCSA','IMPTARPRPTTBL','ENDDT','DATE',7);
 cur_col_name:='P_BNKID';
 new_rec.BNKID:=runtime.validate_column_value(P_BNKID,
 'STGCSA','IMPTARPRPTTBL','BNKID','VARCHAR2',255);
 cur_col_name:='P_BNKNM';
 new_rec.BNKNM:=runtime.validate_column_value(P_BNKNM,
 'STGCSA','IMPTARPRPTTBL','BNKNM','VARCHAR2',80);
 cur_col_name:='P_BNKSTCD';
 new_rec.BNKSTCD:=runtime.validate_column_value(P_BNKSTCD,
 'STGCSA','IMPTARPRPTTBL','BNKSTCD','CHAR',2);
 cur_col_name:='P_ACCTNMB';
 new_rec.ACCTNMB:=runtime.validate_column_value(P_ACCTNMB,
 'STGCSA','IMPTARPRPTTBL','ACCTNMB','VARCHAR2',20);
 cur_col_name:='P_ACCTTYP';
 new_rec.ACCTTYP:=runtime.validate_column_value(P_ACCTTYP,
 'STGCSA','IMPTARPRPTTBL','ACCTTYP','VARCHAR2',255);
 cur_col_name:='P_ACCTNM';
 new_rec.ACCTNM:=runtime.validate_column_value(P_ACCTNM,
 'STGCSA','IMPTARPRPTTBL','ACCTNM','VARCHAR2',80);
 cur_col_name:='P_CURCY';
 new_rec.CURCY:=runtime.validate_column_value(P_CURCY,
 'STGCSA','IMPTARPRPTTBL','CURCY','VARCHAR2',255);
 cur_col_name:='P_SCTNRPTNM';
 new_rec.SCTNRPTNM:=runtime.validate_column_value(P_SCTNRPTNM,
 'STGCSA','IMPTARPRPTTBL','SCTNRPTNM','VARCHAR2',80);
 cur_col_name:='P_TRANSTYP';
 new_rec.TRANSTYP:=runtime.validate_column_value(P_TRANSTYP,
 'STGCSA','IMPTARPRPTTBL','TRANSTYP','VARCHAR2',255);
 cur_col_name:='P_SRLNMBREFNMB';
 new_rec.SRLNMBREFNMB:=runtime.validate_column_value(P_SRLNMBREFNMB,
 'STGCSA','IMPTARPRPTTBL','SRLNMBREFNMB','VARCHAR2',255);
 cur_col_name:='P_CHKAMT';
 new_rec.CHKAMT:=runtime.validate_column_value(P_CHKAMT,
 'STGCSA','IMPTARPRPTTBL','CHKAMT','NUMBER',22);
 cur_col_name:='P_ISSDT';
 new_rec.ISSDT:=runtime.validate_column_value(P_ISSDT,
 'STGCSA','IMPTARPRPTTBL','ISSDT','DATE',7);
 cur_col_name:='P_POSTEDDT';
 new_rec.POSTEDDT:=runtime.validate_column_value(P_POSTEDDT,
 'STGCSA','IMPTARPRPTTBL','POSTEDDT','DATE',7);
 cur_col_name:='P_STOPDT';
 new_rec.STOPDT:=runtime.validate_column_value(P_STOPDT,
 'STGCSA','IMPTARPRPTTBL','STOPDT','DATE',7);
 cur_col_name:='P_RLSEDT';
 new_rec.RLSEDT:=runtime.validate_column_value(P_RLSEDT,
 'STGCSA','IMPTARPRPTTBL','RLSEDT','DATE',7);
 cur_col_name:='P_ASOFDT';
 new_rec.ASOFDT:=runtime.validate_column_value(P_ASOFDT,
 'STGCSA','IMPTARPRPTTBL','ASOFDT','DATE',7);
 cur_col_name:='P_RVRSDDT';
 new_rec.RVRSDDT:=runtime.validate_column_value(P_RVRSDDT,
 'STGCSA','IMPTARPRPTTBL','RVRSDDT','DATE',7);
 cur_col_name:='P_DRCRDTCD';
 new_rec.DRCRDTCD:=runtime.validate_column_value(P_DRCRDTCD,
 'STGCSA','IMPTARPRPTTBL','DRCRDTCD','VARCHAR2',255);
 cur_col_name:='P_OPTINFOTRANDESC';
 new_rec.OPTINFOTRANDESC:=runtime.validate_column_value(P_OPTINFOTRANDESC,
 'STGCSA','IMPTARPRPTTBL','OPTINFOTRANDESC','VARCHAR2',255);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO IMPTARPRPTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.IMPTARPRPTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.IMPTARPRPTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO IMPTARPRPTINSTSP;
 p_errmsg:='Got dupe key on insert into IMPTARPRPTTBL for key '
 ||'(IMPTARPRPTID)=('||new_rec.IMPTARPRPTID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in IMPTARPRPTINSTSP build 2018-01-23 08:26:13';
 ROLLBACK TO IMPTARPRPTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO IMPTARPRPTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End IMPTARPRPTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'IMPTARPRPTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in IMPTARPRPTINSTSP build 2018-01-23 08:26:13'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO IMPTARPRPTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END IMPTARPRPTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:15. --->
PROCEDURE PRPLOANRQSTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACTUALRQST VARCHAR2:=null
 ,p_AMORTCDCAMT NUMBER:=null
 ,p_AMORTCSAAMT NUMBER:=null
 ,p_AMORTSBAAMT NUMBER:=null
 ,p_APPVDT DATE:=null
 ,p_BALAMT NUMBER:=null
 ,p_BALCALC1AMT NUMBER:=null
 ,p_BALCALC2AMT NUMBER:=null
 ,p_BALCALC3AMT NUMBER:=null
 ,p_BALCALC4AMT NUMBER:=null
 ,p_BALCALC5AMT NUMBER:=null
 ,p_BALCALC6AMT NUMBER:=null
 ,p_BALCALCAMT NUMBER:=null
 ,p_BEHINDAMT NUMBER:=null
 ,p_BORRFEEAMT NUMBER:=null
 ,p_BORRNM VARCHAR2:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_BUSDAY6DT DATE:=null
 ,p_CDCBEHINDAMT NUMBER:=null
 ,p_CDCDUEFROMBORRAMT NUMBER:=null
 ,p_CDCFEECALCONLYAMT NUMBER:=null
 ,p_CDCFEEDISBAMT NUMBER:=null
 ,p_CDCFEENEEDEDAMT NUMBER:=null
 ,p_CDCNMB CHAR:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_CDCPCT NUMBER:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCSBAFEEDISBAMT NUMBER:=null
 ,p_CSABEHINDAMT NUMBER:=null
 ,p_CSADUEFROMBORRAMT NUMBER:=null
 ,p_CSAFEECALCAMT NUMBER:=null
 ,p_CSAFEECALCONLYAMT NUMBER:=null
 ,p_CSAFEENEEDEDAMT NUMBER:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CURRFEEBALAMT NUMBER:=null
 ,p_DBENTRBALAMT NUMBER:=null
 ,p_DUEFROMBORRAMT NUMBER:=null
 ,p_ESCROWAMT NUMBER:=null
 ,p_FEECALCAMT NUMBER:=null
 ,p_FEECALCONLYAMT NUMBER:=null
 ,p_FEENEEDEDAMT NUMBER:=null
 ,p_FIVEYRADJ NUMBER:=null
 ,p_FIVEYRADJCDCAMT NUMBER:=null
 ,p_FIVEYRADJCSAAMT NUMBER:=null
 ,p_FIVEYRADJSBAAMT NUMBER:=null
 ,p_GFDAMT NUMBER:=null
 ,p_INT2AMT NUMBER:=null
 ,p_INT3AMT NUMBER:=null
 ,p_INT4AMT NUMBER:=null
 ,p_INT5AMT NUMBER:=null
 ,p_INT6AMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_INTNEEDEDAMT NUMBER:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_ISSDT DATE:=null
 ,p_LATEFEENEEDEDAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_LOANSTAT VARCHAR2:=null
 ,p_MODELNQ NUMBER:=null
 ,p_NOTEBALCALCAMT NUMBER:=null
 ,p_NOTERTPCT NUMBER:=null
 ,p_OPENGNTYAMT NUMBER:=null
 ,p_OPENGNTYERRAMT NUMBER:=null
 ,p_OPENGNTYREGAMT NUMBER:=null
 ,p_OPENGNTYUNALLOCAMT NUMBER:=null
 ,p_PI6MOAMT NUMBER:=null
 ,p_PIAMT NUMBER:=null
 ,p_PIPROJAMT NUMBER:=null
 ,p_POSTINGDT DATE:=null
 ,p_POSTINGMO NUMBER:=null
 ,p_POSTINGYRNMB CHAR:=null
 ,p_PREPAYAMT NUMBER:=null
 ,p_PREPAYCALCDT DATE:=null
 ,p_PREPAYDT DATE:=null
 ,p_PREPAYMO NUMBER:=null
 ,p_PREPAYPCT NUMBER:=null
 ,p_PREPAYPREMAMT NUMBER:=null
 ,p_PREPAYYRNMB CHAR:=null
 ,p_PRIN2AMT NUMBER:=null
 ,p_PRIN3AMT NUMBER:=null
 ,p_PRIN4AMT NUMBER:=null
 ,p_PRIN5AMT NUMBER:=null
 ,p_PRIN6AMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_PRINCURAMT NUMBER:=null
 ,p_PRINNEEDEDAMT NUMBER:=null
 ,p_PRINPROJAMT NUMBER:=null
 ,p_RQSTCD NUMBER:=null
 ,p_RQSTREF NUMBER:=null
 ,p_SEMIANMO NUMBER:=null
 ,p_SEMIANNAMT NUMBER:=null
 ,p_SEMIANNDT DATE:=null
 ,p_SEMIANNPYMTAMT NUMBER:=null
 ,p_THIRDTHURSDAY DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_USERLCK VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:38
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:27:38
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPLOANRQSTDELTSP performs DELETE on STGCSA.PRPLOANRQSTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 new_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPLOANRQSTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTDELTSP build 2018-01-23 08:27:38 '
 ||'Select of STGCSA.PRPLOANRQSTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPLOANRQSTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.PRPLOANRQSTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPLOANRQSTDELTSP build 2018-01-23 08:27:38 '
 ||'while doing delete on STGCSA.PRPLOANRQSTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPLOANRQSTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ';
 select rowid into rec_rowid from STGCSA.PRPLOANRQSTTBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTDELTSP build 2018-01-23 08:27:38 '
 ||'No STGCSA.PRPLOANRQSTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPLOANRQSTDELTSP build 2018-01-23 08:27:38 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PRPLOANRQSTDELTSP build 2018-01-23 08:27:38'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPLOANRQSTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


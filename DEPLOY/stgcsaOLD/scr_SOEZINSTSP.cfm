<!--- Saved 01/28/2018 14:42:27. --->
PROCEDURE SOEZINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_SOEZID NUMBER:=null
 ,p_SOEZLOANNMB CHAR:=null
 ,p_SOEZAMT NUMBER:=null
 ,p_SOEZRS VARCHAR2:=null
 ,p_SOEZCDCNMB CHAR:=null
 ,p_REPDT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:39
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:29:39
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOEZTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOEZTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint SOEZINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_SOEZID';
 new_rec.SOEZID:=runtime.validate_column_value(P_SOEZID,
 'STGCSA','SOEZTBL','SOEZID','NUMBER',22);
 cur_col_name:='P_SOEZLOANNMB';
 new_rec.SOEZLOANNMB:=runtime.validate_column_value(P_SOEZLOANNMB,
 'STGCSA','SOEZTBL','SOEZLOANNMB','CHAR',10);
 cur_col_name:='P_SOEZAMT';
 new_rec.SOEZAMT:=runtime.validate_column_value(P_SOEZAMT,
 'STGCSA','SOEZTBL','SOEZAMT','NUMBER',22);
 cur_col_name:='P_SOEZRS';
 new_rec.SOEZRS:=runtime.validate_column_value(P_SOEZRS,
 'STGCSA','SOEZTBL','SOEZRS','VARCHAR2',255);
 cur_col_name:='P_SOEZCDCNMB';
 new_rec.SOEZCDCNMB:=runtime.validate_column_value(P_SOEZCDCNMB,
 'STGCSA','SOEZTBL','SOEZCDCNMB','CHAR',8);
 cur_col_name:='P_REPDT';
 new_rec.REPDT:=runtime.validate_column_value(P_REPDT,
 'STGCSA','SOEZTBL','REPDT','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOEZINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOEZAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOEZTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOEZINSTSP;
 p_errmsg:='Got dupe key on insert into SOEZTBL for key '
 ||'(SOEZID)=('||new_rec.SOEZID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOEZINSTSP build 2018-01-23 08:29:39';
 ROLLBACK TO SOEZINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOEZINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOEZINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOEZINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOEZINSTSP build 2018-01-23 08:29:39'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOEZINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOEZINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


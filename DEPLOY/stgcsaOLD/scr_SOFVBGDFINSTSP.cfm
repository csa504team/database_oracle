<!--- Saved 01/28/2018 14:42:29. --->
PROCEDURE SOFVBGDFINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DEFRMNTGPKEY CHAR:=null
 ,p_DEFRMNTSTRTDT1 DATE:=null
 ,p_DEFRMNTENDDT1 DATE:=null
 ,p_DEFRMNTREQ1AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT2 DATE:=null
 ,p_DEFRMNTENDDT2 DATE:=null
 ,p_DEFRMNTREQ2AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT3 DATE:=null
 ,p_DEFRMNTENDDT3 DATE:=null
 ,p_DEFRMNTREQ3AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT4 DATE:=null
 ,p_DEFRMNTENDDT4 DATE:=null
 ,p_DEFRMNTREQ4AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT5 DATE:=null
 ,p_DEFRMNTENDDT5 DATE:=null
 ,p_DEFRMNTREQ5AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT6 DATE:=null
 ,p_DEFRMNTENDDT6 DATE:=null
 ,p_DEFRMNTREQ6AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT7 DATE:=null
 ,p_DEFRMNTENDDT7 DATE:=null
 ,p_DEFRMNTREQ7AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT8 DATE:=null
 ,p_DEFRMNTENDDT8 DATE:=null
 ,p_DEFRMNTREQ8AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT9 DATE:=null
 ,p_DEFRMNTENDDT9 DATE:=null
 ,p_DEFRMNTREQ9AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT10 DATE:=null
 ,p_DEFRMNTENDDT10 DATE:=null
 ,p_DEFRMNTREQ10AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT11 DATE:=null
 ,p_DEFRMNTENDDT11 DATE:=null
 ,p_DEFRMNTREQ11AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT12 DATE:=null
 ,p_DEFRMNTENDDT12 DATE:=null
 ,p_DEFRMNTREQ12AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT13 DATE:=null
 ,p_DEFRMNTENDDT13 DATE:=null
 ,p_DEFRMNTREQ13AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT14 DATE:=null
 ,p_DEFRMNTENDDT14 DATE:=null
 ,p_DEFRMNTREQ14AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT15 DATE:=null
 ,p_DEFRMNTENDDT15 DATE:=null
 ,p_DEFRMNTREQ15AMT NUMBER:=null
 ,p_DEFRMNTDTCREAT DATE:=null
 ,p_DEFRMNTDTMOD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:30:16
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:30:16
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVBGDFTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVBGDFTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVBGDFINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBGDFTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_DEFRMNTGPKEY';
 new_rec.DEFRMNTGPKEY:=runtime.validate_column_value(P_DEFRMNTGPKEY,
 'STGCSA','SOFVBGDFTBL','DEFRMNTGPKEY','CHAR',10);
 cur_col_name:='P_DEFRMNTSTRTDT1';
 new_rec.DEFRMNTSTRTDT1:=runtime.validate_column_value(P_DEFRMNTSTRTDT1,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT1','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT1';
 new_rec.DEFRMNTENDDT1:=runtime.validate_column_value(P_DEFRMNTENDDT1,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT1','DATE',7);
 cur_col_name:='P_DEFRMNTREQ1AMT';
 new_rec.DEFRMNTREQ1AMT:=runtime.validate_column_value(P_DEFRMNTREQ1AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ1AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT2';
 new_rec.DEFRMNTSTRTDT2:=runtime.validate_column_value(P_DEFRMNTSTRTDT2,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT2','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT2';
 new_rec.DEFRMNTENDDT2:=runtime.validate_column_value(P_DEFRMNTENDDT2,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT2','DATE',7);
 cur_col_name:='P_DEFRMNTREQ2AMT';
 new_rec.DEFRMNTREQ2AMT:=runtime.validate_column_value(P_DEFRMNTREQ2AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ2AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT3';
 new_rec.DEFRMNTSTRTDT3:=runtime.validate_column_value(P_DEFRMNTSTRTDT3,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT3','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT3';
 new_rec.DEFRMNTENDDT3:=runtime.validate_column_value(P_DEFRMNTENDDT3,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT3','DATE',7);
 cur_col_name:='P_DEFRMNTREQ3AMT';
 new_rec.DEFRMNTREQ3AMT:=runtime.validate_column_value(P_DEFRMNTREQ3AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ3AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT4';
 new_rec.DEFRMNTSTRTDT4:=runtime.validate_column_value(P_DEFRMNTSTRTDT4,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT4','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT4';
 new_rec.DEFRMNTENDDT4:=runtime.validate_column_value(P_DEFRMNTENDDT4,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT4','DATE',7);
 cur_col_name:='P_DEFRMNTREQ4AMT';
 new_rec.DEFRMNTREQ4AMT:=runtime.validate_column_value(P_DEFRMNTREQ4AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ4AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT5';
 new_rec.DEFRMNTSTRTDT5:=runtime.validate_column_value(P_DEFRMNTSTRTDT5,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT5','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT5';
 new_rec.DEFRMNTENDDT5:=runtime.validate_column_value(P_DEFRMNTENDDT5,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT5','DATE',7);
 cur_col_name:='P_DEFRMNTREQ5AMT';
 new_rec.DEFRMNTREQ5AMT:=runtime.validate_column_value(P_DEFRMNTREQ5AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ5AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT6';
 new_rec.DEFRMNTSTRTDT6:=runtime.validate_column_value(P_DEFRMNTSTRTDT6,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT6','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT6';
 new_rec.DEFRMNTENDDT6:=runtime.validate_column_value(P_DEFRMNTENDDT6,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT6','DATE',7);
 cur_col_name:='P_DEFRMNTREQ6AMT';
 new_rec.DEFRMNTREQ6AMT:=runtime.validate_column_value(P_DEFRMNTREQ6AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ6AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT7';
 new_rec.DEFRMNTSTRTDT7:=runtime.validate_column_value(P_DEFRMNTSTRTDT7,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT7','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT7';
 new_rec.DEFRMNTENDDT7:=runtime.validate_column_value(P_DEFRMNTENDDT7,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT7','DATE',7);
 cur_col_name:='P_DEFRMNTREQ7AMT';
 new_rec.DEFRMNTREQ7AMT:=runtime.validate_column_value(P_DEFRMNTREQ7AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ7AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT8';
 new_rec.DEFRMNTSTRTDT8:=runtime.validate_column_value(P_DEFRMNTSTRTDT8,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT8','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT8';
 new_rec.DEFRMNTENDDT8:=runtime.validate_column_value(P_DEFRMNTENDDT8,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT8','DATE',7);
 cur_col_name:='P_DEFRMNTREQ8AMT';
 new_rec.DEFRMNTREQ8AMT:=runtime.validate_column_value(P_DEFRMNTREQ8AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ8AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT9';
 new_rec.DEFRMNTSTRTDT9:=runtime.validate_column_value(P_DEFRMNTSTRTDT9,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT9','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT9';
 new_rec.DEFRMNTENDDT9:=runtime.validate_column_value(P_DEFRMNTENDDT9,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT9','DATE',7);
 cur_col_name:='P_DEFRMNTREQ9AMT';
 new_rec.DEFRMNTREQ9AMT:=runtime.validate_column_value(P_DEFRMNTREQ9AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ9AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT10';
 new_rec.DEFRMNTSTRTDT10:=runtime.validate_column_value(P_DEFRMNTSTRTDT10,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT10','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT10';
 new_rec.DEFRMNTENDDT10:=runtime.validate_column_value(P_DEFRMNTENDDT10,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT10','DATE',7);
 cur_col_name:='P_DEFRMNTREQ10AMT';
 new_rec.DEFRMNTREQ10AMT:=runtime.validate_column_value(P_DEFRMNTREQ10AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ10AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT11';
 new_rec.DEFRMNTSTRTDT11:=runtime.validate_column_value(P_DEFRMNTSTRTDT11,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT11','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT11';
 new_rec.DEFRMNTENDDT11:=runtime.validate_column_value(P_DEFRMNTENDDT11,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT11','DATE',7);
 cur_col_name:='P_DEFRMNTREQ11AMT';
 new_rec.DEFRMNTREQ11AMT:=runtime.validate_column_value(P_DEFRMNTREQ11AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ11AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT12';
 new_rec.DEFRMNTSTRTDT12:=runtime.validate_column_value(P_DEFRMNTSTRTDT12,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT12','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT12';
 new_rec.DEFRMNTENDDT12:=runtime.validate_column_value(P_DEFRMNTENDDT12,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT12','DATE',7);
 cur_col_name:='P_DEFRMNTREQ12AMT';
 new_rec.DEFRMNTREQ12AMT:=runtime.validate_column_value(P_DEFRMNTREQ12AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ12AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT13';
 new_rec.DEFRMNTSTRTDT13:=runtime.validate_column_value(P_DEFRMNTSTRTDT13,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT13','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT13';
 new_rec.DEFRMNTENDDT13:=runtime.validate_column_value(P_DEFRMNTENDDT13,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT13','DATE',7);
 cur_col_name:='P_DEFRMNTREQ13AMT';
 new_rec.DEFRMNTREQ13AMT:=runtime.validate_column_value(P_DEFRMNTREQ13AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ13AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT14';
 new_rec.DEFRMNTSTRTDT14:=runtime.validate_column_value(P_DEFRMNTSTRTDT14,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT14','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT14';
 new_rec.DEFRMNTENDDT14:=runtime.validate_column_value(P_DEFRMNTENDDT14,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT14','DATE',7);
 cur_col_name:='P_DEFRMNTREQ14AMT';
 new_rec.DEFRMNTREQ14AMT:=runtime.validate_column_value(P_DEFRMNTREQ14AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ14AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTSTRTDT15';
 new_rec.DEFRMNTSTRTDT15:=runtime.validate_column_value(P_DEFRMNTSTRTDT15,
 'STGCSA','SOFVBGDFTBL','DEFRMNTSTRTDT15','DATE',7);
 cur_col_name:='P_DEFRMNTENDDT15';
 new_rec.DEFRMNTENDDT15:=runtime.validate_column_value(P_DEFRMNTENDDT15,
 'STGCSA','SOFVBGDFTBL','DEFRMNTENDDT15','DATE',7);
 cur_col_name:='P_DEFRMNTREQ15AMT';
 new_rec.DEFRMNTREQ15AMT:=runtime.validate_column_value(P_DEFRMNTREQ15AMT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTREQ15AMT','NUMBER',22);
 cur_col_name:='P_DEFRMNTDTCREAT';
 new_rec.DEFRMNTDTCREAT:=runtime.validate_column_value(P_DEFRMNTDTCREAT,
 'STGCSA','SOFVBGDFTBL','DEFRMNTDTCREAT','DATE',7);
 cur_col_name:='P_DEFRMNTDTMOD';
 new_rec.DEFRMNTDTMOD:=runtime.validate_column_value(P_DEFRMNTDTMOD,
 'STGCSA','SOFVBGDFTBL','DEFRMNTDTMOD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVBGDFINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVBGDFAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVBGDFTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVBGDFINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVBGDFTBL for key '
 ||'(DEFRMNTGPKEY)=('||new_rec.DEFRMNTGPKEY||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVBGDFINSTSP build 2018-01-23 08:30:16';
 ROLLBACK TO SOFVBGDFINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBGDFINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBGDFINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBGDFINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVBGDFINSTSP build 2018-01-23 08:30:16'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBGDFINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVBGDFINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


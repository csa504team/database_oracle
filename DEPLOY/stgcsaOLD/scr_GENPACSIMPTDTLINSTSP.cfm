<!--- Saved 01/28/2018 14:42:07. --->
PROCEDURE GENPACSIMPTDTLINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PACSIMPTDTLID NUMBER:=null
 ,p_PACSIMPTID NUMBER:=null
 ,p_RQSTID VARCHAR2:=null
 ,p_RELTDTBLNM VARCHAR2:=null
 ,p_RELTDTBLREF NUMBER:=null
 ,p_RQSTTYP VARCHAR2:=null
 ,p_PYMTTYP VARCHAR2:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_TRANSAMT VARCHAR2:=null
 ,p_TRANSTYP VARCHAR2:=null
 ,p_PACSACCT VARCHAR2:=null
 ,p_ACCTNM VARCHAR2:=null
 ,p_INTRNLEXTRNLACCT VARCHAR2:=null
 ,p_TRANAU VARCHAR2:=null
 ,p_ABABNKNMB VARCHAR2:=null
 ,p_ABABNKNM VARCHAR2:=null
 ,p_SWIFTBNK VARCHAR2:=null
 ,p_INTRMEDRYABANMB VARCHAR2:=null
 ,p_INTRMEDRYSWIFT VARCHAR2:=null
 ,p_BNKIRC VARCHAR2:=null
 ,p_BNKIBAN VARCHAR2:=null
 ,p_TRANSINFO VARCHAR2:=null
 ,p_STATCD VARCHAR2:=null
 ,p_TRANSDT DATE:=null
 ,p_POSTEDDT DATE:=null
 ,p_POSTERLOC VARCHAR2:=null
 ,p_PACSIMPTPTIC VARCHAR2:=null
 ,p_RCPTCD VARCHAR2:=null
 ,p_DISBCD VARCHAR2:=null
 ,p_PRININC VARCHAR2:=null
 ,p_SEIBTCHID VARCHAR2:=null
 ,p_SEITRANTYP VARCHAR2:=null
 ,p_RQSTAU VARCHAR2:=null
 ,p_INITBY VARCHAR2:=null
 ,p_APPVBY VARCHAR2:=null
 ,p_ACHADDENDA VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 17:11:18
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 17:11:18
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.GENPACSIMPTDTLTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.GENPACSIMPTDTLTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint GENPACSIMPTDTLINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PACSIMPTDTLID';
 new_rec.PACSIMPTDTLID:=runtime.validate_column_value(P_PACSIMPTDTLID,
 'STGCSA','GENPACSIMPTDTLTBL','PACSIMPTDTLID','NUMBER',22);
 cur_col_name:='P_PACSIMPTID';
 new_rec.PACSIMPTID:=runtime.validate_column_value(P_PACSIMPTID,
 'STGCSA','GENPACSIMPTDTLTBL','PACSIMPTID','NUMBER',22);
 cur_col_name:='P_RQSTID';
 new_rec.RQSTID:=runtime.validate_column_value(P_RQSTID,
 'STGCSA','GENPACSIMPTDTLTBL','RQSTID','VARCHAR2',250);
 cur_col_name:='P_RELTDTBLNM';
 new_rec.RELTDTBLNM:=runtime.validate_column_value(P_RELTDTBLNM,
 'STGCSA','GENPACSIMPTDTLTBL','RELTDTBLNM','VARCHAR2',250);
 cur_col_name:='P_RELTDTBLREF';
 new_rec.RELTDTBLREF:=runtime.validate_column_value(P_RELTDTBLREF,
 'STGCSA','GENPACSIMPTDTLTBL','RELTDTBLREF','NUMBER',22);
 cur_col_name:='P_RQSTTYP';
 new_rec.RQSTTYP:=runtime.validate_column_value(P_RQSTTYP,
 'STGCSA','GENPACSIMPTDTLTBL','RQSTTYP','VARCHAR2',250);
 cur_col_name:='P_PYMTTYP';
 new_rec.PYMTTYP:=runtime.validate_column_value(P_PYMTTYP,
 'STGCSA','GENPACSIMPTDTLTBL','PYMTTYP','VARCHAR2',250);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','GENPACSIMPTDTLTBL','TRANSID','VARCHAR2',250);
 cur_col_name:='P_TRANSAMT';
 new_rec.TRANSAMT:=runtime.validate_column_value(P_TRANSAMT,
 'STGCSA','GENPACSIMPTDTLTBL','TRANSAMT','VARCHAR2',250);
 cur_col_name:='P_TRANSTYP';
 new_rec.TRANSTYP:=runtime.validate_column_value(P_TRANSTYP,
 'STGCSA','GENPACSIMPTDTLTBL','TRANSTYP','VARCHAR2',250);
 cur_col_name:='P_PACSACCT';
 new_rec.PACSACCT:=runtime.validate_column_value(P_PACSACCT,
 'STGCSA','GENPACSIMPTDTLTBL','PACSACCT','VARCHAR2',250);
 cur_col_name:='P_ACCTNM';
 new_rec.ACCTNM:=runtime.validate_column_value(P_ACCTNM,
 'STGCSA','GENPACSIMPTDTLTBL','ACCTNM','VARCHAR2',250);
 cur_col_name:='P_INTRNLEXTRNLACCT';
 new_rec.INTRNLEXTRNLACCT:=runtime.validate_column_value(P_INTRNLEXTRNLACCT,
 'STGCSA','GENPACSIMPTDTLTBL','INTRNLEXTRNLACCT','VARCHAR2',250);
 cur_col_name:='P_TRANAU';
 new_rec.TRANAU:=runtime.validate_column_value(P_TRANAU,
 'STGCSA','GENPACSIMPTDTLTBL','TRANAU','VARCHAR2',250);
 cur_col_name:='P_ABABNKNMB';
 new_rec.ABABNKNMB:=runtime.validate_column_value(P_ABABNKNMB,
 'STGCSA','GENPACSIMPTDTLTBL','ABABNKNMB','VARCHAR2',250);
 cur_col_name:='P_ABABNKNM';
 new_rec.ABABNKNM:=runtime.validate_column_value(P_ABABNKNM,
 'STGCSA','GENPACSIMPTDTLTBL','ABABNKNM','VARCHAR2',250);
 cur_col_name:='P_SWIFTBNK';
 new_rec.SWIFTBNK:=runtime.validate_column_value(P_SWIFTBNK,
 'STGCSA','GENPACSIMPTDTLTBL','SWIFTBNK','VARCHAR2',250);
 cur_col_name:='P_INTRMEDRYABANMB';
 new_rec.INTRMEDRYABANMB:=runtime.validate_column_value(P_INTRMEDRYABANMB,
 'STGCSA','GENPACSIMPTDTLTBL','INTRMEDRYABANMB','VARCHAR2',250);
 cur_col_name:='P_INTRMEDRYSWIFT';
 new_rec.INTRMEDRYSWIFT:=runtime.validate_column_value(P_INTRMEDRYSWIFT,
 'STGCSA','GENPACSIMPTDTLTBL','INTRMEDRYSWIFT','VARCHAR2',250);
 cur_col_name:='P_BNKIRC';
 new_rec.BNKIRC:=runtime.validate_column_value(P_BNKIRC,
 'STGCSA','GENPACSIMPTDTLTBL','BNKIRC','VARCHAR2',250);
 cur_col_name:='P_BNKIBAN';
 new_rec.BNKIBAN:=runtime.validate_column_value(P_BNKIBAN,
 'STGCSA','GENPACSIMPTDTLTBL','BNKIBAN','VARCHAR2',250);
 cur_col_name:='P_TRANSINFO';
 new_rec.TRANSINFO:=runtime.validate_column_value(P_TRANSINFO,
 'STGCSA','GENPACSIMPTDTLTBL','TRANSINFO','VARCHAR2',250);
 cur_col_name:='P_STATCD';
 new_rec.STATCD:=runtime.validate_column_value(P_STATCD,
 'STGCSA','GENPACSIMPTDTLTBL','STATCD','VARCHAR2',250);
 cur_col_name:='P_TRANSDT';
 new_rec.TRANSDT:=runtime.validate_column_value(P_TRANSDT,
 'STGCSA','GENPACSIMPTDTLTBL','TRANSDT','DATE',7);
 cur_col_name:='P_POSTEDDT';
 new_rec.POSTEDDT:=runtime.validate_column_value(P_POSTEDDT,
 'STGCSA','GENPACSIMPTDTLTBL','POSTEDDT','DATE',7);
 cur_col_name:='P_POSTERLOC';
 new_rec.POSTERLOC:=runtime.validate_column_value(P_POSTERLOC,
 'STGCSA','GENPACSIMPTDTLTBL','POSTERLOC','VARCHAR2',250);
 cur_col_name:='P_PACSIMPTPTIC';
 new_rec.PACSIMPTPTIC:=runtime.validate_column_value(P_PACSIMPTPTIC,
 'STGCSA','GENPACSIMPTDTLTBL','PACSIMPTPTIC','VARCHAR2',250);
 cur_col_name:='P_RCPTCD';
 new_rec.RCPTCD:=runtime.validate_column_value(P_RCPTCD,
 'STGCSA','GENPACSIMPTDTLTBL','RCPTCD','VARCHAR2',250);
 cur_col_name:='P_DISBCD';
 new_rec.DISBCD:=runtime.validate_column_value(P_DISBCD,
 'STGCSA','GENPACSIMPTDTLTBL','DISBCD','VARCHAR2',250);
 cur_col_name:='P_PRININC';
 new_rec.PRININC:=runtime.validate_column_value(P_PRININC,
 'STGCSA','GENPACSIMPTDTLTBL','PRININC','VARCHAR2',10);
 cur_col_name:='P_SEIBTCHID';
 new_rec.SEIBTCHID:=runtime.validate_column_value(P_SEIBTCHID,
 'STGCSA','GENPACSIMPTDTLTBL','SEIBTCHID','VARCHAR2',250);
 cur_col_name:='P_SEITRANTYP';
 new_rec.SEITRANTYP:=runtime.validate_column_value(P_SEITRANTYP,
 'STGCSA','GENPACSIMPTDTLTBL','SEITRANTYP','VARCHAR2',250);
 cur_col_name:='P_RQSTAU';
 new_rec.RQSTAU:=runtime.validate_column_value(P_RQSTAU,
 'STGCSA','GENPACSIMPTDTLTBL','RQSTAU','VARCHAR2',250);
 cur_col_name:='P_INITBY';
 new_rec.INITBY:=runtime.validate_column_value(P_INITBY,
 'STGCSA','GENPACSIMPTDTLTBL','INITBY','VARCHAR2',250);
 cur_col_name:='P_APPVBY';
 new_rec.APPVBY:=runtime.validate_column_value(P_APPVBY,
 'STGCSA','GENPACSIMPTDTLTBL','APPVBY','VARCHAR2',250);
 cur_col_name:='P_ACHADDENDA';
 new_rec.ACHADDENDA:=runtime.validate_column_value(P_ACHADDENDA,
 'STGCSA','GENPACSIMPTDTLTBL','ACHADDENDA','VARCHAR2',250);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','GENPACSIMPTDTLTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO GENPACSIMPTDTLINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.GENPACSIMPTDTLAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.GENPACSIMPTDTLTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO GENPACSIMPTDTLINSTSP;
 p_errmsg:='Got dupe key on insert into GENPACSIMPTDTLTBL for key '
 ||'(PACSIMPTDTLID)=('||new_rec.PACSIMPTDTLID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENPACSIMPTDTLINSTSP build 2018-01-23 17:11:18';
 ROLLBACK TO GENPACSIMPTDTLINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENPACSIMPTDTLINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENPACSIMPTDTLINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENPACSIMPTDTLINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in GENPACSIMPTDTLINSTSP build 2018-01-23 17:11:18'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENPACSIMPTDTLINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END GENPACSIMPTDTLINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:36. --->
PROCEDURE SOFVDUEBINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_DUEFROMBORRRECRDTYP NUMBER:=null
 ,p_DUEFROMBORRPOSTINGDT DATE:=null
 ,p_DUEFROMBORRDOLLRAMT NUMBER:=null
 ,p_DUEFROMBORRRJCTDT DATE:=null
 ,p_DUEFROMBORRRJCTCD CHAR:=null
 ,p_DUEFROMBORRRECRDSTATCD NUMBER:=null
 ,p_DUEFROMBORRCLSDT DATE:=null
 ,p_DUEFROMBORRCMNT1 VARCHAR2:=null
 ,p_DUEFROMBORRCMNT2 VARCHAR2:=null
 ,p_DUEFROMBORRRPTDAILY CHAR:=null
 ,p_DUEFROMBORRDISBIND CHAR:=null
 ,p_DUEFROMBORRDELACH CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:20
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:32:20
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVDUEBTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVDUEBTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVDUEBINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVDUEBTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVDUEBTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_DUEFROMBORRRECRDTYP';
 new_rec.DUEFROMBORRRECRDTYP:=runtime.validate_column_value(P_DUEFROMBORRRECRDTYP,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRRECRDTYP','NUMBER',22);
 cur_col_name:='P_DUEFROMBORRPOSTINGDT';
 new_rec.DUEFROMBORRPOSTINGDT:=runtime.validate_column_value(P_DUEFROMBORRPOSTINGDT,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRPOSTINGDT','DATE',7);
 cur_col_name:='P_DUEFROMBORRDOLLRAMT';
 new_rec.DUEFROMBORRDOLLRAMT:=runtime.validate_column_value(P_DUEFROMBORRDOLLRAMT,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRDOLLRAMT','NUMBER',22);
 cur_col_name:='P_DUEFROMBORRRJCTDT';
 new_rec.DUEFROMBORRRJCTDT:=runtime.validate_column_value(P_DUEFROMBORRRJCTDT,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRRJCTDT','DATE',7);
 cur_col_name:='P_DUEFROMBORRRJCTCD';
 new_rec.DUEFROMBORRRJCTCD:=runtime.validate_column_value(P_DUEFROMBORRRJCTCD,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRRJCTCD','CHAR',3);
 cur_col_name:='P_DUEFROMBORRRECRDSTATCD';
 new_rec.DUEFROMBORRRECRDSTATCD:=runtime.validate_column_value(P_DUEFROMBORRRECRDSTATCD,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRRECRDSTATCD','NUMBER',22);
 cur_col_name:='P_DUEFROMBORRCLSDT';
 new_rec.DUEFROMBORRCLSDT:=runtime.validate_column_value(P_DUEFROMBORRCLSDT,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRCLSDT','DATE',7);
 cur_col_name:='P_DUEFROMBORRCMNT1';
 new_rec.DUEFROMBORRCMNT1:=runtime.validate_column_value(P_DUEFROMBORRCMNT1,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRCMNT1','VARCHAR2',80);
 cur_col_name:='P_DUEFROMBORRCMNT2';
 new_rec.DUEFROMBORRCMNT2:=runtime.validate_column_value(P_DUEFROMBORRCMNT2,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRCMNT2','VARCHAR2',80);
 cur_col_name:='P_DUEFROMBORRRPTDAILY';
 new_rec.DUEFROMBORRRPTDAILY:=runtime.validate_column_value(P_DUEFROMBORRRPTDAILY,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRRPTDAILY','CHAR',1);
 cur_col_name:='P_DUEFROMBORRDISBIND';
 new_rec.DUEFROMBORRDISBIND:=runtime.validate_column_value(P_DUEFROMBORRDISBIND,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRDISBIND','CHAR',1);
 cur_col_name:='P_DUEFROMBORRDELACH';
 new_rec.DUEFROMBORRDELACH:=runtime.validate_column_value(P_DUEFROMBORRDELACH,
 'STGCSA','SOFVDUEBTBL','DUEFROMBORRDELACH','CHAR',1);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVDUEBINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVDUEBAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVDUEBTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVDUEBINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVDUEBTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ' ||'(DUEFROMBORRRECRDTYP)=('||new_rec.DUEFROMBORRRECRDTYP||') ' ||'(DUEFROMBORRPOSTINGDT)=('||new_rec.DUEFROMBORRPOSTINGDT||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVDUEBINSTSP build 2018-01-23 08:32:20';
 ROLLBACK TO SOFVDUEBINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVDUEBINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVDUEBINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVDUEBINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVDUEBINSTSP build 2018-01-23 08:32:20'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVDUEBINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVDUEBINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


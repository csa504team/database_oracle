<!--- Saved 01/28/2018 14:42:17. --->
PROCEDURE PRPLOANRQSTUPDTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CORRECTVALAMT NUMBER:=null
 ,p_FORMNM VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_LOANRQSTFLDNM VARCHAR2:=null
 ,p_PRPCDCUPDTID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:47
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:27:47
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPLOANRQSTUPDTUPDTSP performs UPDATE on STGCSA.PRPLOANRQSTUPDTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPCDCUPDTID
 for P_IDENTIFIER=1 qualification is on TRANSID, LOANRQSTFLDNM, FORMNM
 for P_IDENTIFIER=2 qualification is on LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPLOANRQSTUPDTTBL%rowtype;
 new_rec STGCSA.PRPLOANRQSTUPDTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPLOANRQSTUPDTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'Select of STGCSA.PRPLOANRQSTUPDTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PRPCDCUPDTID is not null then
 cur_col_name:='P_PRPCDCUPDTID';
 new_rec.PRPCDCUPDTID:=P_PRPCDCUPDTID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_FORMNM is not null then
 cur_col_name:='P_FORMNM';
 new_rec.FORMNM:=P_FORMNM; 
 end if;
 if P_LOANRQSTFLDNM is not null then
 cur_col_name:='P_LOANRQSTFLDNM';
 new_rec.LOANRQSTFLDNM:=P_LOANRQSTFLDNM; 
 end if;
 if P_CORRECTVALAMT is not null then
 cur_col_name:='P_CORRECTVALAMT';
 new_rec.CORRECTVALAMT:=P_CORRECTVALAMT; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_TRANSTYPID is not null then
 cur_col_name:='P_TRANSTYPID';
 new_rec.TRANSTYPID:=P_TRANSTYPID; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPLOANRQSTUPDTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPLOANRQSTUPDTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,FORMNM=new_rec.FORMNM
 ,LOANRQSTFLDNM=new_rec.LOANRQSTFLDNM
 ,CORRECTVALAMT=new_rec.CORRECTVALAMT
 ,TRANSID=new_rec.TRANSID
 ,TRANSTYPID=new_rec.TRANSTYPID
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'while doing update on STGCSA.PRPLOANRQSTUPDTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPLOANRQSTUPDTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPCDCUPDTID)=('||P_PRPCDCUPDTID||') ';
 select rowid into rec_rowid from STGCSA.PRPLOANRQSTUPDTTBL where 1=1
 and PRPCDCUPDTID=P_PRPCDCUPDTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(FORMNM)=('||P_FORMNM||') '||
 '(LOANRQSTFLDNM)=('||P_LOANRQSTFLDNM||') '||
 '(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.PRPLOANRQSTUPDTTBL a where 1=1 
 and FORMNM=P_FORMNM
 and LOANRQSTFLDNM=P_LOANRQSTFLDNM
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'Oracle returned error fetching STGCSA.PRPLOANRQSTUPDTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r2 in (select rowid from STGCSA.PRPLOANRQSTUPDTTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'Oracle returned error fetching STGCSA.PRPLOANRQSTUPDTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'No STGCSA.PRPLOANRQSTUPDTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB; log_TRANSID:=new_rec.TRANSID; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTUPDTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTUPDTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PRPLOANRQSTUPDTUPDTSP build 2018-01-23 08:27:47'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTUPDTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPLOANRQSTUPDTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


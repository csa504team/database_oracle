<!--- Saved 01/28/2018 14:41:36. --->
PROCEDURE CNTCTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CNTCTID NUMBER:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_CNTCTLASTNM VARCHAR2:=null
 ,p_CNTCTEMAILADR VARCHAR2:=null
 ,p_CNTCTPHNNMB VARCHAR2:=null
 ,p_CNTCTPHNXTN CHAR:=null
 ,p_CNTCTFAXNMB VARCHAR2:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTTITL VARCHAR2:=null
 ,p_CDCREF NUMBER:=null
 ,p_CREATBY NUMBER:=null
 ,p_UPDTBY NUMBER:=null
 ,p_ENTID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:29
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:18:29
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CNTCTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.CNTCTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint CNTCTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CNTCTID';
 new_rec.CNTCTID:=runtime.validate_column_value(P_CNTCTID,
 'STGCSA','CNTCTTBL','CNTCTID','NUMBER',22);
 cur_col_name:='P_CNTCTFIRSTNM';
 new_rec.CNTCTFIRSTNM:=runtime.validate_column_value(P_CNTCTFIRSTNM,
 'STGCSA','CNTCTTBL','CNTCTFIRSTNM','VARCHAR2',80);
 cur_col_name:='P_CNTCTLASTNM';
 new_rec.CNTCTLASTNM:=runtime.validate_column_value(P_CNTCTLASTNM,
 'STGCSA','CNTCTTBL','CNTCTLASTNM','VARCHAR2',80);
 cur_col_name:='P_CNTCTEMAILADR';
 new_rec.CNTCTEMAILADR:=runtime.validate_column_value(P_CNTCTEMAILADR,
 'STGCSA','CNTCTTBL','CNTCTEMAILADR','VARCHAR2',80);
 cur_col_name:='P_CNTCTPHNNMB';
 new_rec.CNTCTPHNNMB:=runtime.validate_column_value(P_CNTCTPHNNMB,
 'STGCSA','CNTCTTBL','CNTCTPHNNMB','VARCHAR2',30);
 cur_col_name:='P_CNTCTPHNXTN';
 new_rec.CNTCTPHNXTN:=runtime.validate_column_value(P_CNTCTPHNXTN,
 'STGCSA','CNTCTTBL','CNTCTPHNXTN','CHAR',10);
 cur_col_name:='P_CNTCTFAXNMB';
 new_rec.CNTCTFAXNMB:=runtime.validate_column_value(P_CNTCTFAXNMB,
 'STGCSA','CNTCTTBL','CNTCTFAXNMB','VARCHAR2',20);
 cur_col_name:='P_CNTCTMAILADRSTR1NM';
 new_rec.CNTCTMAILADRSTR1NM:=runtime.validate_column_value(P_CNTCTMAILADRSTR1NM,
 'STGCSA','CNTCTTBL','CNTCTMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRSTR2NM';
 new_rec.CNTCTMAILADRSTR2NM:=runtime.validate_column_value(P_CNTCTMAILADRSTR2NM,
 'STGCSA','CNTCTTBL','CNTCTMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRCTYNM';
 new_rec.CNTCTMAILADRCTYNM:=runtime.validate_column_value(P_CNTCTMAILADRCTYNM,
 'STGCSA','CNTCTTBL','CNTCTMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRSTCD';
 new_rec.CNTCTMAILADRSTCD:=runtime.validate_column_value(P_CNTCTMAILADRSTCD,
 'STGCSA','CNTCTTBL','CNTCTMAILADRSTCD','CHAR',2);
 cur_col_name:='P_CNTCTMAILADRZIPCD';
 new_rec.CNTCTMAILADRZIPCD:=runtime.validate_column_value(P_CNTCTMAILADRZIPCD,
 'STGCSA','CNTCTTBL','CNTCTMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_CNTCTMAILADRZIP4CD';
 new_rec.CNTCTMAILADRZIP4CD:=runtime.validate_column_value(P_CNTCTMAILADRZIP4CD,
 'STGCSA','CNTCTTBL','CNTCTMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_CNTCTTITL';
 new_rec.CNTCTTITL:=runtime.validate_column_value(P_CNTCTTITL,
 'STGCSA','CNTCTTBL','CNTCTTITL','VARCHAR2',255);
 cur_col_name:='P_CDCREF';
 new_rec.CDCREF:=runtime.validate_column_value(P_CDCREF,
 'STGCSA','CNTCTTBL','CDCREF','NUMBER',22);
 cur_col_name:='P_CREATBY';
 new_rec.CREATBY:=runtime.validate_column_value(P_CREATBY,
 'STGCSA','CNTCTTBL','CREATBY','NUMBER',22);
 cur_col_name:='P_UPDTBY';
 new_rec.UPDTBY:=runtime.validate_column_value(P_UPDTBY,
 'STGCSA','CNTCTTBL','UPDTBY','NUMBER',22);
 cur_col_name:='P_ENTID';
 new_rec.ENTID:=runtime.validate_column_value(P_ENTID,
 'STGCSA','CNTCTTBL','ENTID','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO CNTCTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.CNTCTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.CNTCTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO CNTCTINSTSP;
 p_errmsg:='Got dupe key on insert into CNTCTTBL for key '
 ||'(CNTCTID)=('||new_rec.CNTCTID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in CNTCTINSTSP build 2018-01-23 08:18:29';
 ROLLBACK TO CNTCTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in CNTCTINSTSP build 2018-01-23 08:18:29'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END CNTCTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:41:32. --->
PROCEDURE ACCPRCSCYCSTATINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CYCPRCSID NUMBER:=null
 ,p_CYCID NUMBER:=null
 ,p_PRCSID NUMBER:=null
 ,p_SCHDLDT DATE:=null
 ,p_SCHDLUSER VARCHAR2:=null
 ,p_SETUPDT DATE:=null
 ,p_LASTREFRESHDT DATE:=null
 ,p_INITIALRVWCMPLT NUMBER:=null
 ,p_INITIALRVWUSER VARCHAR2:=null
 ,p_INITIALRVWDT DATE:=null
 ,p_PEERRVWCMPLT NUMBER:=null
 ,p_PEERRVWUSER VARCHAR2:=null
 ,p_PEERRVWDT DATE:=null
 ,p_UPDTCMPLT NUMBER:=null
 ,p_UPDTCMPLTUSER VARCHAR2:=null
 ,p_UPDTCMPLTDT DATE:=null
 ,p_MGRRVWCMPLT NUMBER:=null
 ,p_MGRRVWUSER VARCHAR2:=null
 ,p_MGRRVWDT DATE:=null
 ,p_UPDTRUNNING NUMBER:=null
 ,p_UPDTRUNNINGUSER VARCHAR2:=null
 ,p_PRCSCYCCMNT VARCHAR2:=null
 ,p_PROPOSEDDT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:16:40
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:16:40
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCPRCSCYCSTATTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.ACCPRCSCYCSTATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint ACCPRCSCYCSTATINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CYCPRCSID';
 new_rec.CYCPRCSID:=runtime.validate_column_value(P_CYCPRCSID,
 'STGCSA','ACCPRCSCYCSTATTBL','CYCPRCSID','NUMBER',22);
 cur_col_name:='P_CYCID';
 new_rec.CYCID:=runtime.validate_column_value(P_CYCID,
 'STGCSA','ACCPRCSCYCSTATTBL','CYCID','NUMBER',22);
 cur_col_name:='P_PRCSID';
 new_rec.PRCSID:=runtime.validate_column_value(P_PRCSID,
 'STGCSA','ACCPRCSCYCSTATTBL','PRCSID','NUMBER',22);
 cur_col_name:='P_SCHDLDT';
 new_rec.SCHDLDT:=runtime.validate_column_value(P_SCHDLDT,
 'STGCSA','ACCPRCSCYCSTATTBL','SCHDLDT','DATE',7);
 cur_col_name:='P_SCHDLUSER';
 new_rec.SCHDLUSER:=runtime.validate_column_value(P_SCHDLUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','SCHDLUSER','VARCHAR2',100);
 cur_col_name:='P_SETUPDT';
 new_rec.SETUPDT:=runtime.validate_column_value(P_SETUPDT,
 'STGCSA','ACCPRCSCYCSTATTBL','SETUPDT','DATE',7);
 cur_col_name:='P_LASTREFRESHDT';
 new_rec.LASTREFRESHDT:=runtime.validate_column_value(P_LASTREFRESHDT,
 'STGCSA','ACCPRCSCYCSTATTBL','LASTREFRESHDT','DATE',7);
 cur_col_name:='P_INITIALRVWCMPLT';
 new_rec.INITIALRVWCMPLT:=runtime.validate_column_value(P_INITIALRVWCMPLT,
 'STGCSA','ACCPRCSCYCSTATTBL','INITIALRVWCMPLT','NUMBER',22);
 cur_col_name:='P_INITIALRVWUSER';
 new_rec.INITIALRVWUSER:=runtime.validate_column_value(P_INITIALRVWUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','INITIALRVWUSER','VARCHAR2',100);
 cur_col_name:='P_INITIALRVWDT';
 new_rec.INITIALRVWDT:=runtime.validate_column_value(P_INITIALRVWDT,
 'STGCSA','ACCPRCSCYCSTATTBL','INITIALRVWDT','DATE',7);
 cur_col_name:='P_PEERRVWCMPLT';
 new_rec.PEERRVWCMPLT:=runtime.validate_column_value(P_PEERRVWCMPLT,
 'STGCSA','ACCPRCSCYCSTATTBL','PEERRVWCMPLT','NUMBER',22);
 cur_col_name:='P_PEERRVWUSER';
 new_rec.PEERRVWUSER:=runtime.validate_column_value(P_PEERRVWUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','PEERRVWUSER','VARCHAR2',10);
 cur_col_name:='P_PEERRVWDT';
 new_rec.PEERRVWDT:=runtime.validate_column_value(P_PEERRVWDT,
 'STGCSA','ACCPRCSCYCSTATTBL','PEERRVWDT','DATE',7);
 cur_col_name:='P_UPDTCMPLT';
 new_rec.UPDTCMPLT:=runtime.validate_column_value(P_UPDTCMPLT,
 'STGCSA','ACCPRCSCYCSTATTBL','UPDTCMPLT','NUMBER',22);
 cur_col_name:='P_UPDTCMPLTUSER';
 new_rec.UPDTCMPLTUSER:=runtime.validate_column_value(P_UPDTCMPLTUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','UPDTCMPLTUSER','VARCHAR2',100);
 cur_col_name:='P_UPDTCMPLTDT';
 new_rec.UPDTCMPLTDT:=runtime.validate_column_value(P_UPDTCMPLTDT,
 'STGCSA','ACCPRCSCYCSTATTBL','UPDTCMPLTDT','DATE',7);
 cur_col_name:='P_MGRRVWCMPLT';
 new_rec.MGRRVWCMPLT:=runtime.validate_column_value(P_MGRRVWCMPLT,
 'STGCSA','ACCPRCSCYCSTATTBL','MGRRVWCMPLT','NUMBER',22);
 cur_col_name:='P_MGRRVWUSER';
 new_rec.MGRRVWUSER:=runtime.validate_column_value(P_MGRRVWUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','MGRRVWUSER','VARCHAR2',100);
 cur_col_name:='P_MGRRVWDT';
 new_rec.MGRRVWDT:=runtime.validate_column_value(P_MGRRVWDT,
 'STGCSA','ACCPRCSCYCSTATTBL','MGRRVWDT','DATE',7);
 cur_col_name:='P_UPDTRUNNING';
 new_rec.UPDTRUNNING:=runtime.validate_column_value(P_UPDTRUNNING,
 'STGCSA','ACCPRCSCYCSTATTBL','UPDTRUNNING','NUMBER',22);
 cur_col_name:='P_UPDTRUNNINGUSER';
 new_rec.UPDTRUNNINGUSER:=runtime.validate_column_value(P_UPDTRUNNINGUSER,
 'STGCSA','ACCPRCSCYCSTATTBL','UPDTRUNNINGUSER','VARCHAR2',100);
 cur_col_name:='P_PRCSCYCCMNT';
 new_rec.PRCSCYCCMNT:=runtime.validate_column_value(P_PRCSCYCCMNT,
 'STGCSA','ACCPRCSCYCSTATTBL','PRCSCYCCMNT','VARCHAR2',1000);
 cur_col_name:='P_PROPOSEDDT';
 new_rec.PROPOSEDDT:=runtime.validate_column_value(P_PROPOSEDDT,
 'STGCSA','ACCPRCSCYCSTATTBL','PROPOSEDDT','DATE',7);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','ACCPRCSCYCSTATTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO ACCPRCSCYCSTATINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.ACCPRCSCYCSTATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.ACCPRCSCYCSTATTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO ACCPRCSCYCSTATINSTSP;
 p_errmsg:='Got dupe key on insert into ACCPRCSCYCSTATTBL for key '
 ||'(CYCPRCSID)=('||new_rec.CYCPRCSID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCPRCSCYCSTATINSTSP build 2018-01-23 08:16:40';
 ROLLBACK TO ACCPRCSCYCSTATINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCPRCSCYCSTATINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCPRCSCYCSTATINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCPRCSCYCSTATINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in ACCPRCSCYCSTATINSTSP build 2018-01-23 08:16:40'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCPRCSCYCSTATINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END ACCPRCSCYCSTATINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


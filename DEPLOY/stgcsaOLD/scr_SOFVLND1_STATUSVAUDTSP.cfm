<!--- Saved 01/28/2018 14:42:45. --->
PROCEDURE SOFVLND1_STATUSVAUDTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2,
 action_cd char,
 AUDTEDTCD char,
 old_rec STGCSA.SOFVLND1TBL%rowtype,
 new_rec STGCSA.SOFVLND1TBL%rowtype) as
 /*
 Created on: 2018-01-23 08:34:39
 Created by: GENR
 Crerated from template auditme_template v2.02 20 Dec 2017 on 2018-01-23 08:34:39
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Purpose : Write to SOFVAUDTTBL for specific changed columns in
 STGCSA.SOFVLND1_STATUSV (SOFVLND1TBL)
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 aud_rec STGCSA.SOFVAUDTTBL%rowtype;
 n number:=0;
 e number:=0;
 tsp timestamp;
 key_from_new_rec STGCSA.SOFVAUDTTBL.AUDTRECRDKEY%type;
 begin
 savepoint SOFVLND1_STATUSVAUDTSP;
 -- hopeful return
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=null;
 -- setup log rec
 select systimestamp,sysdate,sys_context('userenv','session_user')
 into aud_rec.audttransdt, aud_rec.CREATDT, aud_rec.creatuserid from dual;
 aud_rec.LASTUPDTDT:=aud_rec.CREATDT;
 aud_rec.lastupdtuserid:=aud_rec.creatuserid;
 aud_rec.audtseqnmb:=0;
 aud_rec.AUDTOPRTRINITIAL:=' ';
 aud_rec.AUDTTRMNLID:=' ';
 aud_rec.AUDTACTNCD:=action_cd;
 aud_rec.AUDTEDTCD:=AUDTEDTCD;
 aud_rec.AUDTUSERID:=p_userid;
 -- save key value... if add or del then one rec is nulls
 aud_rec.audtrecrdkey:=rpad(substr(''
 ||old_rec.LOANNMB,1,15),15);
 key_from_new_rec:=rpad(substr(''
 ||new_rec.LOANNMB,1,15),15);
 if aud_rec.audtrecrdkey is null then
 aud_rec.audtrecrdkey:=key_from_new_rec;
 end if;
 -- are any columns auditing cares about different?
 if old_rec.LOANDTLBORRMAILADRSTR1NM=new_rec.LOANDTLBORRMAILADRSTR1NM 
 or (old_rec.LOANDTLBORRMAILADRSTR1NM is null and new_rec.LOANDTLBORRMAILADRSTR1NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.LOANDTLBORRMAILADRSTR1NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.LOANDTLBORRMAILADRSTR1NM; 
 end if; 
 if new_rec.LOANDTLBORRMAILADRSTR1NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.LOANDTLBORRMAILADRSTR1NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='LDSTRET1';
 aud_rec.audtchngfile:='BGLOANDB';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.LOANDTLBORRMAILADRCTYNM=new_rec.LOANDTLBORRMAILADRCTYNM 
 or (old_rec.LOANDTLBORRMAILADRCTYNM is null and new_rec.LOANDTLBORRMAILADRCTYNM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.LOANDTLBORRMAILADRCTYNM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.LOANDTLBORRMAILADRCTYNM; 
 end if; 
 if new_rec.LOANDTLBORRMAILADRCTYNM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.LOANDTLBORRMAILADRCTYNM; 
 end if;
 
 aud_rec.AUDTFLDNM:='LDCITY1 ';
 aud_rec.audtchngfile:='BGLOANDB';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.LOANDTLBORRMAILADRSTCD=new_rec.LOANDTLBORRMAILADRSTCD 
 or (old_rec.LOANDTLBORRMAILADRSTCD is null and new_rec.LOANDTLBORRMAILADRSTCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.LOANDTLBORRMAILADRSTCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.LOANDTLBORRMAILADRSTCD; 
 end if; 
 if new_rec.LOANDTLBORRMAILADRSTCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.LOANDTLBORRMAILADRSTCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='LDSTATE1';
 aud_rec.audtchngfile:='BGLOANDB';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.LOANDTLBORRMAILADRZIPCD=new_rec.LOANDTLBORRMAILADRZIPCD 
 or (old_rec.LOANDTLBORRMAILADRZIPCD is null and new_rec.LOANDTLBORRMAILADRZIPCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.LOANDTLBORRMAILADRZIPCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.LOANDTLBORRMAILADRZIPCD; 
 end if; 
 if new_rec.LOANDTLBORRMAILADRZIPCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.LOANDTLBORRMAILADRZIPCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='LDZIP! ';
 aud_rec.audtchngfile:='BGLOANDB';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 p_retval:=aud_rec.audtseqnmb;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:='In OXH in SOFVLND1_STATUSVAUDTSP Build 2018-01-23 08:34:39 got: '||sqlerrm(SQLCODE);
 ROLLBACK TO SOFVLND1_STATUSVAUDTSP;
 --
 END SOFVLND1_STATUSVAUDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


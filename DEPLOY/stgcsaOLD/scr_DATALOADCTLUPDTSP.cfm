<!--- Saved 10/31/2017 13:18:30. --->
PROCEDURE DATALOADCTLUpdTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_PROCESSID NUMBER:=NULL, P_PROCESSCODE VARCHAR2:=NULL,
 P_PROCESSNAME VARCHAR2:=NULL, P_STARTDT TIMESTAMP:=NULL,
 P_ENDDT TIMESTAMP:=NULL, P_INUSEIND NUMBER:=NULL)
 
 as
 /*
 <!--- generated 2017-09-28 14:03:51 by SYS --->
 Created on: 2017-09-28 14:03:51
 Created by: SYS
 Crerated from genr, stdtblupd_template v 1.0
 Purpose : standard update by key for table STGCSA.DATALOADCTLTBL
 */
 BEGIN 
 savepoint DATALOADCTLUpdTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 update STGCSA.DATALOADCTLTBL set (
 PROCESSCODE, PROCESSNAME, STARTDT, ENDDT, INUSEIND)=
 (select 
 P_PROCESSCODE, P_PROCESSNAME, P_STARTDT, P_ENDDT, P_INUSEIND
 from dual) where (
 PROCESSID)=
 (select 
 P_PROCESSID
 from dual); 
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO DATALOADCTLUpdTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END DATALOADCTLUpdTsp;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


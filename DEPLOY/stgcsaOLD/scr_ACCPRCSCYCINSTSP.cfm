<!--- Saved 01/28/2018 14:41:31. --->
PROCEDURE ACCPRCSCYCINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CYCID NUMBER:=null
 ,p_CYCYR NUMBER:=null
 ,p_CYCMO NUMBER:=null
 ,p_CYCMOTXT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_BUSDAY1DT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:16:58
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:16:58
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCPRCSCYCTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.ACCPRCSCYCTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint ACCPRCSCYCINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CYCID';
 new_rec.CYCID:=runtime.validate_column_value(P_CYCID,
 'STGCSA','ACCPRCSCYCTBL','CYCID','NUMBER',22);
 cur_col_name:='P_CYCYR';
 new_rec.CYCYR:=runtime.validate_column_value(P_CYCYR,
 'STGCSA','ACCPRCSCYCTBL','CYCYR','NUMBER',22);
 cur_col_name:='P_CYCMO';
 new_rec.CYCMO:=runtime.validate_column_value(P_CYCMO,
 'STGCSA','ACCPRCSCYCTBL','CYCMO','NUMBER',22);
 cur_col_name:='P_CYCMOTXT';
 new_rec.CYCMOTXT:=runtime.validate_column_value(P_CYCMOTXT,
 'STGCSA','ACCPRCSCYCTBL','CYCMOTXT','VARCHAR2',100);
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=runtime.validate_column_value(P_SEMIANDT,
 'STGCSA','ACCPRCSCYCTBL','SEMIANDT','DATE',7);
 cur_col_name:='P_BUSDAY1DT';
 new_rec.BUSDAY1DT:=runtime.validate_column_value(P_BUSDAY1DT,
 'STGCSA','ACCPRCSCYCTBL','BUSDAY1DT','DATE',7);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','ACCPRCSCYCTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO ACCPRCSCYCINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.ACCPRCSCYCAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.ACCPRCSCYCTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO ACCPRCSCYCINSTSP;
 p_errmsg:='Got dupe key on insert into ACCPRCSCYCTBL for key '
 ||'(CYCID)=('||new_rec.CYCID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCPRCSCYCINSTSP build 2018-01-23 08:16:58';
 ROLLBACK TO ACCPRCSCYCINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCPRCSCYCINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCPRCSCYCINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCPRCSCYCINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in ACCPRCSCYCINSTSP build 2018-01-23 08:16:58'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCPRCSCYCINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END ACCPRCSCYCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 12/31/2017 09:00:18. --->
PROCEDURE COREACTVTYLOGInsTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_LOGENTRYID NUMBER:=NULL, P_LOANNMB CHAR:=NULL, P_TRANSID NUMBER:=NULL,
 P_DBMDL VARCHAR2:=NULL, P_ACTVTYLOGCAT VARCHAR2:=NULL,
 P_ACTVTYDTLS VARCHAR2:=NULL, P_ENTRYDT DATE:=NULL,
 P_USERID VARCHAR2:=NULL, P_USERNM VARCHAR2:=NULL,
 P_TIMESTAMPFLD DATE:=NULL, P_CREATUSERID VARCHAR2:=NULL,
 P_CREATDT DATE:=NULL, P_LASTUPDTUSERID VARCHAR2:=NULL,
 P_LASTUPDTDT DATE:=NULL)
 
 as
 /*
 <!--- generated 2017-09-28 14:03:30 by SYS --->
 Created on: 2017-09-28 14:03:30
 Created by: SYS
 Crerated from genr, stdtblins_template v 1.0
 Purpose : standard insert of row to table STGCSA.COREACTVTYLOGTBL
 */
 BEGIN 
 savepoint COREACTVTYLOGInsTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 insert into STGCSA.COREACTVTYLOGTBL (
 LOGENTRYID, LOANNMB, TRANSID, DBMDL, ACTVTYLOGCAT, ACTVTYDTLS, ENTRYDT,
 USERID, USERNM, TIMESTAMPFLD, CREATUSERID, CREATDT, LASTUPDTUSERID,
 LASTUPDTDT
 ) values (
 P_LOGENTRYID, P_LOANNMB, P_TRANSID, P_DBMDL, P_ACTVTYLOGCAT,
 P_ACTVTYDTLS, P_ENTRYDT, P_USERID, P_USERNM, P_TIMESTAMPFLD,
 P_CREATUSERID, P_CREATDT, P_LASTUPDTUSERID, P_LASTUPDTDT
 );
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO COREACTVTYLOGInsTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END COREACTVTYLOGInsTsp; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


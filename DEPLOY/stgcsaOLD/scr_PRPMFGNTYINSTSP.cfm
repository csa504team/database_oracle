<!--- Saved 01/28/2018 14:42:19. --->
PROCEDURE PRPMFGNTYINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPMFGNTYID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_OPENGNTYDT DATE:=null
 ,p_OPENGNTYTYP NUMBER:=null
 ,p_GNTYAMT NUMBER:=null
 ,p_STATCD NUMBER:=null
 ,p_GNTYCLS VARCHAR2:=null
 ,p_PRPCMNT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_PREPAYDT DATE:=null
 ,p_OPENGNTYDTCONVERTED DATE:=null
 ,p_OPENGNTYDTPREPAYDTDIFF NUMBER:=null
 ,p_GNTYIND VARCHAR2:=null
 ,p_SEMIANDTCONVERTED DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:28:33
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:28:33
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPMFGNTYTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PRPMFGNTYTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PRPMFGNTYINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PRPMFGNTYID';
 new_rec.PRPMFGNTYID:=runtime.validate_column_value(P_PRPMFGNTYID,
 'STGCSA','PRPMFGNTYTBL','PRPMFGNTYID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PRPMFGNTYTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_OPENGNTYDT';
 new_rec.OPENGNTYDT:=runtime.validate_column_value(P_OPENGNTYDT,
 'STGCSA','PRPMFGNTYTBL','OPENGNTYDT','DATE',7);
 cur_col_name:='P_OPENGNTYTYP';
 new_rec.OPENGNTYTYP:=runtime.validate_column_value(P_OPENGNTYTYP,
 'STGCSA','PRPMFGNTYTBL','OPENGNTYTYP','NUMBER',22);
 cur_col_name:='P_GNTYAMT';
 new_rec.GNTYAMT:=runtime.validate_column_value(P_GNTYAMT,
 'STGCSA','PRPMFGNTYTBL','GNTYAMT','NUMBER',22);
 cur_col_name:='P_STATCD';
 new_rec.STATCD:=runtime.validate_column_value(P_STATCD,
 'STGCSA','PRPMFGNTYTBL','STATCD','NUMBER',22);
 cur_col_name:='P_GNTYCLS';
 new_rec.GNTYCLS:=runtime.validate_column_value(P_GNTYCLS,
 'STGCSA','PRPMFGNTYTBL','GNTYCLS','VARCHAR2',10);
 cur_col_name:='P_PRPCMNT';
 new_rec.PRPCMNT:=runtime.validate_column_value(P_PRPCMNT,
 'STGCSA','PRPMFGNTYTBL','PRPCMNT','VARCHAR2',80);
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=runtime.validate_column_value(P_SEMIANDT,
 'STGCSA','PRPMFGNTYTBL','SEMIANDT','DATE',7);
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=runtime.validate_column_value(P_PREPAYDT,
 'STGCSA','PRPMFGNTYTBL','PREPAYDT','DATE',7);
 cur_col_name:='P_OPENGNTYDTCONVERTED';
 new_rec.OPENGNTYDTCONVERTED:=runtime.validate_column_value(P_OPENGNTYDTCONVERTED,
 'STGCSA','PRPMFGNTYTBL','OPENGNTYDTCONVERTED','DATE',7);
 cur_col_name:='P_OPENGNTYDTPREPAYDTDIFF';
 new_rec.OPENGNTYDTPREPAYDTDIFF:=runtime.validate_column_value(P_OPENGNTYDTPREPAYDTDIFF,
 'STGCSA','PRPMFGNTYTBL','OPENGNTYDTPREPAYDTDIFF','NUMBER',22);
 cur_col_name:='P_GNTYIND';
 new_rec.GNTYIND:=runtime.validate_column_value(P_GNTYIND,
 'STGCSA','PRPMFGNTYTBL','GNTYIND','VARCHAR2',50);
 cur_col_name:='P_SEMIANDTCONVERTED';
 new_rec.SEMIANDTCONVERTED:=runtime.validate_column_value(P_SEMIANDTCONVERTED,
 'STGCSA','PRPMFGNTYTBL','SEMIANDTCONVERTED','DATE',7);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PRPMFGNTYTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PRPMFGNTYINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PRPMFGNTYAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PRPMFGNTYTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PRPMFGNTYINSTSP;
 p_errmsg:='Got dupe key on insert into PRPMFGNTYTBL for key '
 ||'(PRPMFGNTYID)=('||new_rec.PRPMFGNTYID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFGNTYINSTSP build 2018-01-23 08:28:33';
 ROLLBACK TO PRPMFGNTYINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPMFGNTYINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFGNTYINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPMFGNTYINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PRPMFGNTYINSTSP build 2018-01-23 08:28:33'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPMFGNTYINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PRPMFGNTYINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


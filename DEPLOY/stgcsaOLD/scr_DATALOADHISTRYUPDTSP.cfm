<!--- Saved 10/31/2017 13:18:31. --->
PROCEDURE DATALOADHISTRYUpdTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_HISTRYID NUMBER:=NULL, P_PROCESSID NUMBER:=NULL,
 P_PROCESSSTARTDT TIMESTAMP:=NULL, P_USERID VARCHAR2:=NULL)
 
 as
 /*
 <!--- generated 2017-09-28 14:04:13 by SYS --->
 Created on: 2017-09-28 14:04:13
 Created by: SYS
 Crerated from genr, stdtblupd_template v 1.0
 Purpose : standard update by key for table STGCSA.DATALOADHISTRYTBL
 */
 BEGIN 
 savepoint DATALOADHISTRYUpdTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 update STGCSA.DATALOADHISTRYTBL set (
 PROCESSID, PROCESSSTARTDT, USERID)=
 (select 
 P_PROCESSID, P_PROCESSSTARTDT, P_USERID
 from dual) where (
 HISTRYID)=
 (select 
 P_HISTRYID
 from dual); 
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO DATALOADHISTRYUpdTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END DATALOADHISTRYUpdTsp;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:23. --->
PROCEDURE PYMT110RPTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCAMT NUMBER:=null
 ,p_CDCRAMT NUMBER:=null
 ,p_CSAAMT NUMBER:=null
 ,p_CSARAMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_LFAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_POSTAMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_PYMT110ID NUMBER:=null
 ,p_SBAAMT NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-27 15:31:34
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-27 15:31:34
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PYMT110RPTDELTSP performs DELETE on STGCSA.PYMT110RPTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PYMT110ID
 for P_IDENTIFIER=2 qualification is on PYMT110ID =0 or PYMT110ID<>0
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PYMT110RPTTBL%rowtype;
 new_rec STGCSA.PYMT110RPTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PYMT110RPTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'Select of STGCSA.PYMT110RPTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PYMT110RPTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PYMT110RPTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.PYMT110RPTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'while doing delete on STGCSA.PYMT110RPTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PYMT110RPTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PYMT110RPTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TRANSID:=P_TRANSID; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PYMT110ID)=('||P_PYMT110ID||') ';
 select rowid into rec_rowid from STGCSA.PYMT110RPTTBL where 1=1
 and PYMT110ID=P_PYMT110ID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'No STGCSA.PYMT110RPTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 2 then 
 -- case to delete one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 keystouse:='(PYMT110ID =0 or PYMT110ID<>0) '||'';
 begin
 for r2 in (select rowid from STGCSA.PYMT110RPTTBL a where 1=1 
 
 
 and PYMT110ID =0 or PYMT110ID<>0) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PYMT110RPTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'Oracle returned error fetching STGCSA.PYMT110RPTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PYMT110RPTDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'No STGCSA.PYMT110RPTTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PYMT110RPTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In PYMT110RPTDELTSP build 2018-01-27 15:31:34 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_TRANSID:=old_rec.TRANSID; log_LOANNMB:=old_rec.LOANNMB; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PYMT110RPTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'PYMT110RPTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PYMT110RPTDELTSP build 2018-01-27 15:31:34'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PYMT110RPTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PYMT110RPTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


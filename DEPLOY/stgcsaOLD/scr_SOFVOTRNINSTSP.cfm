<!--- Saved 01/28/2018 14:42:48. --->
PROCEDURE SOFVOTRNINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_ONLNTRANSTRANSDT TIMESTAMP:=null
 ,p_ONLNTRANSTRMNLID CHAR:=null
 ,p_ONLNTRANSOPRTRINITIAL CHAR:=null
 ,p_ONLNTRANSFILENM CHAR:=null
 ,p_ONLNTRANSSCN CHAR:=null
 ,p_ONLNTRANSFIL10 CHAR:=null
 ,p_ONLNTRANSDETPYMTDT DATE:=null
 ,p_ONLNTRANSDTLREST CHAR:=null
 ,p_ONLNTRANSDTLREST5 CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:35:17
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:35:17
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVOTRNTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVOTRNTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVOTRNINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVOTRNTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVOTRNTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_ONLNTRANSTRANSDT';
 new_rec.ONLNTRANSTRANSDT:=runtime.validate_column_value(P_ONLNTRANSTRANSDT,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSTRANSDT','TIMESTAMP(6)',11);
 cur_col_name:='P_ONLNTRANSTRMNLID';
 new_rec.ONLNTRANSTRMNLID:=runtime.validate_column_value(P_ONLNTRANSTRMNLID,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSTRMNLID','CHAR',4);
 cur_col_name:='P_ONLNTRANSOPRTRINITIAL';
 new_rec.ONLNTRANSOPRTRINITIAL:=runtime.validate_column_value(P_ONLNTRANSOPRTRINITIAL,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSOPRTRINITIAL','CHAR',4);
 cur_col_name:='P_ONLNTRANSFILENM';
 new_rec.ONLNTRANSFILENM:=runtime.validate_column_value(P_ONLNTRANSFILENM,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSFILENM','CHAR',8);
 cur_col_name:='P_ONLNTRANSSCN';
 new_rec.ONLNTRANSSCN:=runtime.validate_column_value(P_ONLNTRANSSCN,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSSCN','CHAR',8);
 cur_col_name:='P_ONLNTRANSFIL10';
 new_rec.ONLNTRANSFIL10:=runtime.validate_column_value(P_ONLNTRANSFIL10,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSFIL10','CHAR',10);
 cur_col_name:='P_ONLNTRANSDETPYMTDT';
 new_rec.ONLNTRANSDETPYMTDT:=runtime.validate_column_value(P_ONLNTRANSDETPYMTDT,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSDETPYMTDT','DATE',7);
 cur_col_name:='P_ONLNTRANSDTLREST';
 new_rec.ONLNTRANSDTLREST:=runtime.validate_column_value(P_ONLNTRANSDTLREST,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSDTLREST','CHAR',9);
 cur_col_name:='P_ONLNTRANSDTLREST5';
 new_rec.ONLNTRANSDTLREST5:=runtime.validate_column_value(P_ONLNTRANSDTLREST5,
 'STGCSA','SOFVOTRNTBL','ONLNTRANSDTLREST5','CHAR',5);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVOTRNINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVOTRNAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVOTRNTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVOTRNINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVOTRNTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ' ||'(ONLNTRANSTRANSDT)=('||new_rec.ONLNTRANSTRANSDT||') ' ||'(ONLNTRANSTRMNLID)=('||new_rec.ONLNTRANSTRMNLID||') ' ||'(ONLNTRANSOPRTRINITIAL)=('||new_rec.ONLNTRANSOPRTRINITIAL||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVOTRNINSTSP build 2018-01-23 08:35:17';
 ROLLBACK TO SOFVOTRNINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVOTRNINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVOTRNINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVOTRNINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVOTRNINSTSP build 2018-01-23 08:35:17'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVOTRNINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVOTRNINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


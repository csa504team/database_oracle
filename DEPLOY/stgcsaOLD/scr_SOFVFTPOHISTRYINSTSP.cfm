<!--- Saved 10/31/2017 13:18:41. --->
PROCEDURE SOFVFTPOHISTRYINSTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2
 ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
 ,p_PREPOSTRVWTRANSCD CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTINGCHKDGT CHAR:=null
 ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
 ,p_PREPOSTRVWPYMTAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
 ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
 ,p_PREPOSTRVWADDENDRECIND NUMBER:=null
 ,p_PREPOSTRVWTRACENMB NUMBER:=null
 ,p_PREPOSTRVWCMNT VARCHAR2:=null
 ,p_DELUSERID VARCHAR2:=null
 ,p_DELDT DATE:=null
 ) as
 /*
 Created on: 2017-10-29 14:27:01
 Created by: GENR
 Crerated from template stdtblins_template v2.0 24 Oct 2017 on 2017-10-29 14:27:01
 Using SNAP V1.2 24 Oct 2017 J. Low Binary Frond, Select Computing
 Purpose : standard insert of row to table STGCSA.SOFVFTPOHISTRYTBL
 */
 errfound boolean;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 rec_t1 STGCSA.SOFVFTPOHISTRYTBL%rowtype;
 begin
 -- setup
 savepoint SOFVFTPOHISTRYINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null; 
 rec_t1.creatuserid:=p_userid;
 rec_t1.creatdt:=sysdate;
 rec_t1.lastupdtuserid:=p_userid;
 rec_t1.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure 
 -- copy pased table column parm values to rec_t1
 rec_t1.PREPOSTRVWRECRDTYPCD:=P_PREPOSTRVWRECRDTYPCD;
 rec_t1.PREPOSTRVWTRANSCD:=P_PREPOSTRVWTRANSCD;
 rec_t1.PREPOSTRVWTRANSITROUTINGNMB:=P_PREPOSTRVWTRANSITROUTINGNMB;
 rec_t1.PREPOSTRVWTRANSITROUTINGCHKDGT:=P_PREPOSTRVWTRANSITROUTINGCHKDGT;
 rec_t1.PREPOSTRVWBNKACCTNMB:=P_PREPOSTRVWBNKACCTNMB;
 rec_t1.PREPOSTRVWPYMTAMT:=P_PREPOSTRVWPYMTAMT;
 rec_t1.LOANNMB:=P_LOANNMB;
 rec_t1.PREPOSTRVWINDVLNM:=P_PREPOSTRVWINDVLNM;
 rec_t1.PREPOSTRVWCOMPBNKDISC:=P_PREPOSTRVWCOMPBNKDISC;
 rec_t1.PREPOSTRVWADDENDRECIND:=P_PREPOSTRVWADDENDRECIND;
 rec_t1.PREPOSTRVWTRACENMB:=P_PREPOSTRVWTRACENMB;
 rec_t1.PREPOSTRVWCMNT:=P_PREPOSTRVWCMNT;
 rec_t1.DELUSERID:=P_DELUSERID;
 rec_t1.DELDT:=P_DELDT;
 
 -- check for passed nulls 
 --
 If rec_t1.PREPOSTRVWRECRDTYPCD is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWRECRDTYPCD,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWRECRDTYPCD',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWRECRDTYPCD';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWTRANSCD is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWTRANSCD,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWTRANSCD',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWTRANSCD';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWTRANSITROUTINGNMB is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWTRANSITROUTINGNMB,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWTRANSITROUTINGNMB',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWTRANSITROUTINGNMB';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWTRANSITROUTINGCHKDGT is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWTRANSITROUTINGCHKDGT,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWTRANSITROUTINGCHKDGT',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWTRANSITROUTINGCHKDGT';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWBNKACCTNMB is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWBNKACCTNMB,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWBNKACCTNMB',
 'VARCHAR2',20,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWBNKACCTNMB';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWPYMTAMT is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWPYMTAMT,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWPYMTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWPYMTAMT';
 end if;
 end if;
 end if;
 --
 If rec_t1.LOANNMB is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.LOANNMB,
 'STGCSA','SOFVFTPOHISTRYTBL','LOANNMB',
 'CHAR',10,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', LOANNMB';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWINDVLNM is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWINDVLNM,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWINDVLNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWINDVLNM';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWCOMPBNKDISC is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWCOMPBNKDISC,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWCOMPBNKDISC',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWCOMPBNKDISC';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWADDENDRECIND is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWADDENDRECIND,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWADDENDRECIND',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWADDENDRECIND';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWTRACENMB is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWTRACENMB,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWTRACENMB',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWTRACENMB';
 end if;
 end if;
 end if;
 --
 If rec_t1.PREPOSTRVWCMNT is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.PREPOSTRVWCMNT,
 'STGCSA','SOFVFTPOHISTRYTBL','PREPOSTRVWCMNT',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', PREPOSTRVWCMNT';
 end if;
 end if;
 end if;
 --
 If rec_t1.DELUSERID is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.DELUSERID,
 'STGCSA','SOFVFTPOHISTRYTBL','DELUSERID',
 'VARCHAR2',30,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', DELUSERID';
 end if;
 end if;
 end if;
 --
 If rec_t1.DELDT is null then 
 RUNTIME.DEFAULT_COLUMN_VALUE(rec_t1.DELDT,
 'STGCSA','SOFVFTPOHISTRYTBL','DELDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3800 then
 errstr:=errstr||', DELDT';
 end if;
 end if;
 end if;
 -- end of nulls checking
 --
 if errfound=true then
 ROLLBACK TO SOFVFTPOHISTRYINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more NULL inputs: '||substr(errstr,3);
 else 
 --
 insert into STGCSA.SOFVFTPOHISTRYTBL values rec_t1;
 p_RETVAL := SQL%ROWCOUNT;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVFTPOHISTRYINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO SOFVFTPOHISTRYINSTSP;
 --
 END SOFVFTPOHISTRYINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


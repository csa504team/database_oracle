<!--- Saved 01/28/2018 14:42:30. --->
PROCEDURE SOFVBGDFUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DEFRMNTDTCREAT DATE:=null
 ,p_DEFRMNTDTMOD DATE:=null
 ,p_DEFRMNTENDDT1 DATE:=null
 ,p_DEFRMNTENDDT10 DATE:=null
 ,p_DEFRMNTENDDT11 DATE:=null
 ,p_DEFRMNTENDDT12 DATE:=null
 ,p_DEFRMNTENDDT13 DATE:=null
 ,p_DEFRMNTENDDT14 DATE:=null
 ,p_DEFRMNTENDDT15 DATE:=null
 ,p_DEFRMNTENDDT2 DATE:=null
 ,p_DEFRMNTENDDT3 DATE:=null
 ,p_DEFRMNTENDDT4 DATE:=null
 ,p_DEFRMNTENDDT5 DATE:=null
 ,p_DEFRMNTENDDT6 DATE:=null
 ,p_DEFRMNTENDDT7 DATE:=null
 ,p_DEFRMNTENDDT8 DATE:=null
 ,p_DEFRMNTENDDT9 DATE:=null
 ,p_DEFRMNTGPKEY CHAR:=null
 ,p_DEFRMNTREQ10AMT NUMBER:=null
 ,p_DEFRMNTREQ11AMT NUMBER:=null
 ,p_DEFRMNTREQ12AMT NUMBER:=null
 ,p_DEFRMNTREQ13AMT NUMBER:=null
 ,p_DEFRMNTREQ14AMT NUMBER:=null
 ,p_DEFRMNTREQ15AMT NUMBER:=null
 ,p_DEFRMNTREQ1AMT NUMBER:=null
 ,p_DEFRMNTREQ2AMT NUMBER:=null
 ,p_DEFRMNTREQ3AMT NUMBER:=null
 ,p_DEFRMNTREQ4AMT NUMBER:=null
 ,p_DEFRMNTREQ5AMT NUMBER:=null
 ,p_DEFRMNTREQ6AMT NUMBER:=null
 ,p_DEFRMNTREQ7AMT NUMBER:=null
 ,p_DEFRMNTREQ8AMT NUMBER:=null
 ,p_DEFRMNTREQ9AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT1 DATE:=null
 ,p_DEFRMNTSTRTDT10 DATE:=null
 ,p_DEFRMNTSTRTDT11 DATE:=null
 ,p_DEFRMNTSTRTDT12 DATE:=null
 ,p_DEFRMNTSTRTDT13 DATE:=null
 ,p_DEFRMNTSTRTDT14 DATE:=null
 ,p_DEFRMNTSTRTDT15 DATE:=null
 ,p_DEFRMNTSTRTDT2 DATE:=null
 ,p_DEFRMNTSTRTDT3 DATE:=null
 ,p_DEFRMNTSTRTDT4 DATE:=null
 ,p_DEFRMNTSTRTDT5 DATE:=null
 ,p_DEFRMNTSTRTDT6 DATE:=null
 ,p_DEFRMNTSTRTDT7 DATE:=null
 ,p_DEFRMNTSTRTDT8 DATE:=null
 ,p_DEFRMNTSTRTDT9 DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:30:10
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:30:10
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVBGDFUPDTSP performs UPDATE on STGCSA.SOFVBGDFTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: DEFRMNTGPKEY
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVBGDFTBL%rowtype;
 new_rec STGCSA.SOFVBGDFTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVBGDFTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBGDFUPDTSP build 2018-01-23 08:30:10 '
 ||'Select of STGCSA.SOFVBGDFTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBGDFUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_DEFRMNTGPKEY is not null then
 cur_col_name:='P_DEFRMNTGPKEY';
 new_rec.DEFRMNTGPKEY:=P_DEFRMNTGPKEY; 
 end if;
 if P_DEFRMNTSTRTDT1 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT1';
 new_rec.DEFRMNTSTRTDT1:=P_DEFRMNTSTRTDT1; 
 end if;
 if P_DEFRMNTENDDT1 is not null then
 cur_col_name:='P_DEFRMNTENDDT1';
 new_rec.DEFRMNTENDDT1:=P_DEFRMNTENDDT1; 
 end if;
 if P_DEFRMNTREQ1AMT is not null then
 cur_col_name:='P_DEFRMNTREQ1AMT';
 new_rec.DEFRMNTREQ1AMT:=P_DEFRMNTREQ1AMT; 
 end if;
 if P_DEFRMNTSTRTDT2 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT2';
 new_rec.DEFRMNTSTRTDT2:=P_DEFRMNTSTRTDT2; 
 end if;
 if P_DEFRMNTENDDT2 is not null then
 cur_col_name:='P_DEFRMNTENDDT2';
 new_rec.DEFRMNTENDDT2:=P_DEFRMNTENDDT2; 
 end if;
 if P_DEFRMNTREQ2AMT is not null then
 cur_col_name:='P_DEFRMNTREQ2AMT';
 new_rec.DEFRMNTREQ2AMT:=P_DEFRMNTREQ2AMT; 
 end if;
 if P_DEFRMNTSTRTDT3 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT3';
 new_rec.DEFRMNTSTRTDT3:=P_DEFRMNTSTRTDT3; 
 end if;
 if P_DEFRMNTENDDT3 is not null then
 cur_col_name:='P_DEFRMNTENDDT3';
 new_rec.DEFRMNTENDDT3:=P_DEFRMNTENDDT3; 
 end if;
 if P_DEFRMNTREQ3AMT is not null then
 cur_col_name:='P_DEFRMNTREQ3AMT';
 new_rec.DEFRMNTREQ3AMT:=P_DEFRMNTREQ3AMT; 
 end if;
 if P_DEFRMNTSTRTDT4 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT4';
 new_rec.DEFRMNTSTRTDT4:=P_DEFRMNTSTRTDT4; 
 end if;
 if P_DEFRMNTENDDT4 is not null then
 cur_col_name:='P_DEFRMNTENDDT4';
 new_rec.DEFRMNTENDDT4:=P_DEFRMNTENDDT4; 
 end if;
 if P_DEFRMNTREQ4AMT is not null then
 cur_col_name:='P_DEFRMNTREQ4AMT';
 new_rec.DEFRMNTREQ4AMT:=P_DEFRMNTREQ4AMT; 
 end if;
 if P_DEFRMNTSTRTDT5 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT5';
 new_rec.DEFRMNTSTRTDT5:=P_DEFRMNTSTRTDT5; 
 end if;
 if P_DEFRMNTENDDT5 is not null then
 cur_col_name:='P_DEFRMNTENDDT5';
 new_rec.DEFRMNTENDDT5:=P_DEFRMNTENDDT5; 
 end if;
 if P_DEFRMNTREQ5AMT is not null then
 cur_col_name:='P_DEFRMNTREQ5AMT';
 new_rec.DEFRMNTREQ5AMT:=P_DEFRMNTREQ5AMT; 
 end if;
 if P_DEFRMNTSTRTDT6 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT6';
 new_rec.DEFRMNTSTRTDT6:=P_DEFRMNTSTRTDT6; 
 end if;
 if P_DEFRMNTENDDT6 is not null then
 cur_col_name:='P_DEFRMNTENDDT6';
 new_rec.DEFRMNTENDDT6:=P_DEFRMNTENDDT6; 
 end if;
 if P_DEFRMNTREQ6AMT is not null then
 cur_col_name:='P_DEFRMNTREQ6AMT';
 new_rec.DEFRMNTREQ6AMT:=P_DEFRMNTREQ6AMT; 
 end if;
 if P_DEFRMNTSTRTDT7 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT7';
 new_rec.DEFRMNTSTRTDT7:=P_DEFRMNTSTRTDT7; 
 end if;
 if P_DEFRMNTENDDT7 is not null then
 cur_col_name:='P_DEFRMNTENDDT7';
 new_rec.DEFRMNTENDDT7:=P_DEFRMNTENDDT7; 
 end if;
 if P_DEFRMNTREQ7AMT is not null then
 cur_col_name:='P_DEFRMNTREQ7AMT';
 new_rec.DEFRMNTREQ7AMT:=P_DEFRMNTREQ7AMT; 
 end if;
 if P_DEFRMNTSTRTDT8 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT8';
 new_rec.DEFRMNTSTRTDT8:=P_DEFRMNTSTRTDT8; 
 end if;
 if P_DEFRMNTENDDT8 is not null then
 cur_col_name:='P_DEFRMNTENDDT8';
 new_rec.DEFRMNTENDDT8:=P_DEFRMNTENDDT8; 
 end if;
 if P_DEFRMNTREQ8AMT is not null then
 cur_col_name:='P_DEFRMNTREQ8AMT';
 new_rec.DEFRMNTREQ8AMT:=P_DEFRMNTREQ8AMT; 
 end if;
 if P_DEFRMNTSTRTDT9 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT9';
 new_rec.DEFRMNTSTRTDT9:=P_DEFRMNTSTRTDT9; 
 end if;
 if P_DEFRMNTENDDT9 is not null then
 cur_col_name:='P_DEFRMNTENDDT9';
 new_rec.DEFRMNTENDDT9:=P_DEFRMNTENDDT9; 
 end if;
 if P_DEFRMNTREQ9AMT is not null then
 cur_col_name:='P_DEFRMNTREQ9AMT';
 new_rec.DEFRMNTREQ9AMT:=P_DEFRMNTREQ9AMT; 
 end if;
 if P_DEFRMNTSTRTDT10 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT10';
 new_rec.DEFRMNTSTRTDT10:=P_DEFRMNTSTRTDT10; 
 end if;
 if P_DEFRMNTENDDT10 is not null then
 cur_col_name:='P_DEFRMNTENDDT10';
 new_rec.DEFRMNTENDDT10:=P_DEFRMNTENDDT10; 
 end if;
 if P_DEFRMNTREQ10AMT is not null then
 cur_col_name:='P_DEFRMNTREQ10AMT';
 new_rec.DEFRMNTREQ10AMT:=P_DEFRMNTREQ10AMT; 
 end if;
 if P_DEFRMNTSTRTDT11 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT11';
 new_rec.DEFRMNTSTRTDT11:=P_DEFRMNTSTRTDT11; 
 end if;
 if P_DEFRMNTENDDT11 is not null then
 cur_col_name:='P_DEFRMNTENDDT11';
 new_rec.DEFRMNTENDDT11:=P_DEFRMNTENDDT11; 
 end if;
 if P_DEFRMNTREQ11AMT is not null then
 cur_col_name:='P_DEFRMNTREQ11AMT';
 new_rec.DEFRMNTREQ11AMT:=P_DEFRMNTREQ11AMT; 
 end if;
 if P_DEFRMNTSTRTDT12 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT12';
 new_rec.DEFRMNTSTRTDT12:=P_DEFRMNTSTRTDT12; 
 end if;
 if P_DEFRMNTENDDT12 is not null then
 cur_col_name:='P_DEFRMNTENDDT12';
 new_rec.DEFRMNTENDDT12:=P_DEFRMNTENDDT12; 
 end if;
 if P_DEFRMNTREQ12AMT is not null then
 cur_col_name:='P_DEFRMNTREQ12AMT';
 new_rec.DEFRMNTREQ12AMT:=P_DEFRMNTREQ12AMT; 
 end if;
 if P_DEFRMNTSTRTDT13 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT13';
 new_rec.DEFRMNTSTRTDT13:=P_DEFRMNTSTRTDT13; 
 end if;
 if P_DEFRMNTENDDT13 is not null then
 cur_col_name:='P_DEFRMNTENDDT13';
 new_rec.DEFRMNTENDDT13:=P_DEFRMNTENDDT13; 
 end if;
 if P_DEFRMNTREQ13AMT is not null then
 cur_col_name:='P_DEFRMNTREQ13AMT';
 new_rec.DEFRMNTREQ13AMT:=P_DEFRMNTREQ13AMT; 
 end if;
 if P_DEFRMNTSTRTDT14 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT14';
 new_rec.DEFRMNTSTRTDT14:=P_DEFRMNTSTRTDT14; 
 end if;
 if P_DEFRMNTENDDT14 is not null then
 cur_col_name:='P_DEFRMNTENDDT14';
 new_rec.DEFRMNTENDDT14:=P_DEFRMNTENDDT14; 
 end if;
 if P_DEFRMNTREQ14AMT is not null then
 cur_col_name:='P_DEFRMNTREQ14AMT';
 new_rec.DEFRMNTREQ14AMT:=P_DEFRMNTREQ14AMT; 
 end if;
 if P_DEFRMNTSTRTDT15 is not null then
 cur_col_name:='P_DEFRMNTSTRTDT15';
 new_rec.DEFRMNTSTRTDT15:=P_DEFRMNTSTRTDT15; 
 end if;
 if P_DEFRMNTENDDT15 is not null then
 cur_col_name:='P_DEFRMNTENDDT15';
 new_rec.DEFRMNTENDDT15:=P_DEFRMNTENDDT15; 
 end if;
 if P_DEFRMNTREQ15AMT is not null then
 cur_col_name:='P_DEFRMNTREQ15AMT';
 new_rec.DEFRMNTREQ15AMT:=P_DEFRMNTREQ15AMT; 
 end if;
 if P_DEFRMNTDTCREAT is not null then
 cur_col_name:='P_DEFRMNTDTCREAT';
 new_rec.DEFRMNTDTCREAT:=P_DEFRMNTDTCREAT; 
 end if;
 if P_DEFRMNTDTMOD is not null then
 cur_col_name:='P_DEFRMNTDTMOD';
 new_rec.DEFRMNTDTMOD:=P_DEFRMNTDTMOD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVBGDFAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVBGDFTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,DEFRMNTSTRTDT1=new_rec.DEFRMNTSTRTDT1
 ,DEFRMNTENDDT1=new_rec.DEFRMNTENDDT1
 ,DEFRMNTREQ1AMT=new_rec.DEFRMNTREQ1AMT
 ,DEFRMNTSTRTDT2=new_rec.DEFRMNTSTRTDT2
 ,DEFRMNTENDDT2=new_rec.DEFRMNTENDDT2
 ,DEFRMNTREQ2AMT=new_rec.DEFRMNTREQ2AMT
 ,DEFRMNTSTRTDT3=new_rec.DEFRMNTSTRTDT3
 ,DEFRMNTENDDT3=new_rec.DEFRMNTENDDT3
 ,DEFRMNTREQ3AMT=new_rec.DEFRMNTREQ3AMT
 ,DEFRMNTSTRTDT4=new_rec.DEFRMNTSTRTDT4
 ,DEFRMNTENDDT4=new_rec.DEFRMNTENDDT4
 ,DEFRMNTREQ4AMT=new_rec.DEFRMNTREQ4AMT
 ,DEFRMNTSTRTDT5=new_rec.DEFRMNTSTRTDT5
 ,DEFRMNTENDDT5=new_rec.DEFRMNTENDDT5
 ,DEFRMNTREQ5AMT=new_rec.DEFRMNTREQ5AMT
 ,DEFRMNTSTRTDT6=new_rec.DEFRMNTSTRTDT6
 ,DEFRMNTENDDT6=new_rec.DEFRMNTENDDT6
 ,DEFRMNTREQ6AMT=new_rec.DEFRMNTREQ6AMT
 ,DEFRMNTSTRTDT7=new_rec.DEFRMNTSTRTDT7
 ,DEFRMNTENDDT7=new_rec.DEFRMNTENDDT7
 ,DEFRMNTREQ7AMT=new_rec.DEFRMNTREQ7AMT
 ,DEFRMNTSTRTDT8=new_rec.DEFRMNTSTRTDT8
 ,DEFRMNTENDDT8=new_rec.DEFRMNTENDDT8
 ,DEFRMNTREQ8AMT=new_rec.DEFRMNTREQ8AMT
 ,DEFRMNTSTRTDT9=new_rec.DEFRMNTSTRTDT9
 ,DEFRMNTENDDT9=new_rec.DEFRMNTENDDT9
 ,DEFRMNTREQ9AMT=new_rec.DEFRMNTREQ9AMT
 ,DEFRMNTSTRTDT10=new_rec.DEFRMNTSTRTDT10
 ,DEFRMNTENDDT10=new_rec.DEFRMNTENDDT10
 ,DEFRMNTREQ10AMT=new_rec.DEFRMNTREQ10AMT
 ,DEFRMNTSTRTDT11=new_rec.DEFRMNTSTRTDT11
 ,DEFRMNTENDDT11=new_rec.DEFRMNTENDDT11
 ,DEFRMNTREQ11AMT=new_rec.DEFRMNTREQ11AMT
 ,DEFRMNTSTRTDT12=new_rec.DEFRMNTSTRTDT12
 ,DEFRMNTENDDT12=new_rec.DEFRMNTENDDT12
 ,DEFRMNTREQ12AMT=new_rec.DEFRMNTREQ12AMT
 ,DEFRMNTSTRTDT13=new_rec.DEFRMNTSTRTDT13
 ,DEFRMNTENDDT13=new_rec.DEFRMNTENDDT13
 ,DEFRMNTREQ13AMT=new_rec.DEFRMNTREQ13AMT
 ,DEFRMNTSTRTDT14=new_rec.DEFRMNTSTRTDT14
 ,DEFRMNTENDDT14=new_rec.DEFRMNTENDDT14
 ,DEFRMNTREQ14AMT=new_rec.DEFRMNTREQ14AMT
 ,DEFRMNTSTRTDT15=new_rec.DEFRMNTSTRTDT15
 ,DEFRMNTENDDT15=new_rec.DEFRMNTENDDT15
 ,DEFRMNTREQ15AMT=new_rec.DEFRMNTREQ15AMT
 ,DEFRMNTDTCREAT=new_rec.DEFRMNTDTCREAT
 ,DEFRMNTDTMOD=new_rec.DEFRMNTDTMOD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVBGDFUPDTSP build 2018-01-23 08:30:10 '
 ||'while doing update on STGCSA.SOFVBGDFTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBGDFUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVBGDFUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBGDFTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(DEFRMNTGPKEY)=('||P_DEFRMNTGPKEY||') ';
 select rowid into rec_rowid from STGCSA.SOFVBGDFTBL where 1=1
 and DEFRMNTGPKEY=P_DEFRMNTGPKEY;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBGDFUPDTSP build 2018-01-23 08:30:10 '
 ||'No STGCSA.SOFVBGDFTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBGDFUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVBGDFUPDTSP build 2018-01-23 08:30:10 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBGDFUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBGDFUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVBGDFUPDTSP build 2018-01-23 08:30:10'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBGDFUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVBGDFUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


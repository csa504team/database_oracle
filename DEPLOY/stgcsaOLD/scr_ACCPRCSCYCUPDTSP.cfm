<!--- Saved 01/28/2018 14:41:32. --->
PROCEDURE ACCPRCSCYCUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_BUSDAY1DT DATE:=null
 ,p_CYCID NUMBER:=null
 ,p_CYCMO NUMBER:=null
 ,p_CYCMOTXT VARCHAR2:=null
 ,p_CYCYR NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:16:53
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:16:53
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure ACCPRCSCYCUPDTSP performs UPDATE on STGCSA.ACCPRCSCYCTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CYCID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.ACCPRCSCYCTBL%rowtype;
 new_rec STGCSA.ACCPRCSCYCTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.ACCPRCSCYCTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCUPDTSP build 2018-01-23 08:16:53 '
 ||'Select of STGCSA.ACCPRCSCYCTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO ACCPRCSCYCUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CYCID is not null then
 cur_col_name:='P_CYCID';
 new_rec.CYCID:=P_CYCID; 
 end if;
 if P_CYCYR is not null then
 cur_col_name:='P_CYCYR';
 new_rec.CYCYR:=P_CYCYR; 
 end if;
 if P_CYCMO is not null then
 cur_col_name:='P_CYCMO';
 new_rec.CYCMO:=P_CYCMO; 
 end if;
 if P_CYCMOTXT is not null then
 cur_col_name:='P_CYCMOTXT';
 new_rec.CYCMOTXT:=P_CYCMOTXT; 
 end if;
 if P_SEMIANDT is not null then
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_BUSDAY1DT is not null then
 cur_col_name:='P_BUSDAY1DT';
 new_rec.BUSDAY1DT:=P_BUSDAY1DT; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.ACCPRCSCYCAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.ACCPRCSCYCTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CYCYR=new_rec.CYCYR
 ,CYCMO=new_rec.CYCMO
 ,CYCMOTXT=new_rec.CYCMOTXT
 ,SEMIANDT=new_rec.SEMIANDT
 ,BUSDAY1DT=new_rec.BUSDAY1DT
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In ACCPRCSCYCUPDTSP build 2018-01-23 08:16:53 '
 ||'while doing update on STGCSA.ACCPRCSCYCTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO ACCPRCSCYCUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCPRCSCYCUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CYCID)=('||P_CYCID||') ';
 select rowid into rec_rowid from STGCSA.ACCPRCSCYCTBL where 1=1
 and CYCID=P_CYCID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCUPDTSP build 2018-01-23 08:16:53 '
 ||'No STGCSA.ACCPRCSCYCTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCPRCSCYCUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In ACCPRCSCYCUPDTSP build 2018-01-23 08:16:53 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCPRCSCYCUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCPRCSCYCUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In ACCPRCSCYCUPDTSP build 2018-01-23 08:16:53'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCPRCSCYCUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END ACCPRCSCYCUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


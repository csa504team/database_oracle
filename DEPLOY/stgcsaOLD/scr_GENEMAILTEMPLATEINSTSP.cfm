<!--- Saved 01/28/2018 14:42:05. --->
PROCEDURE GENEMAILTEMPLATEINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMAILTEMPLATEID NUMBER:=null
 ,p_EMAILCATID NUMBER:=null
 ,p_DBMDL VARCHAR2:=null
 ,p_TEMPLATENM VARCHAR2:=null
 ,p_DISTROLISTID NUMBER:=null
 ,p_SUBJLINE VARCHAR2:=null
 ,p_TEMPLATEHTML VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:25:09
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:25:09
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.GENEMAILTEMPLATETBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.GENEMAILTEMPLATETBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint GENEMAILTEMPLATEINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_DBMDL:=P_DBMDL; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_EMAILTEMPLATEID';
 new_rec.EMAILTEMPLATEID:=runtime.validate_column_value(P_EMAILTEMPLATEID,
 'STGCSA','GENEMAILTEMPLATETBL','EMAILTEMPLATEID','NUMBER',22);
 cur_col_name:='P_EMAILCATID';
 new_rec.EMAILCATID:=runtime.validate_column_value(P_EMAILCATID,
 'STGCSA','GENEMAILTEMPLATETBL','EMAILCATID','NUMBER',22);
 cur_col_name:='P_DBMDL';
 new_rec.DBMDL:=runtime.validate_column_value(P_DBMDL,
 'STGCSA','GENEMAILTEMPLATETBL','DBMDL','VARCHAR2',50);
 cur_col_name:='P_TEMPLATENM';
 new_rec.TEMPLATENM:=runtime.validate_column_value(P_TEMPLATENM,
 'STGCSA','GENEMAILTEMPLATETBL','TEMPLATENM','VARCHAR2',250);
 cur_col_name:='P_DISTROLISTID';
 new_rec.DISTROLISTID:=runtime.validate_column_value(P_DISTROLISTID,
 'STGCSA','GENEMAILTEMPLATETBL','DISTROLISTID','NUMBER',22);
 cur_col_name:='P_SUBJLINE';
 new_rec.SUBJLINE:=runtime.validate_column_value(P_SUBJLINE,
 'STGCSA','GENEMAILTEMPLATETBL','SUBJLINE','VARCHAR2',250);
 cur_col_name:='P_TEMPLATEHTML';
 new_rec.TEMPLATEHTML:=runtime.validate_column_value(P_TEMPLATEHTML,
 'STGCSA','GENEMAILTEMPLATETBL','TEMPLATEHTML','VARCHAR2',4000);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','GENEMAILTEMPLATETBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO GENEMAILTEMPLATEINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.GENEMAILTEMPLATEAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.GENEMAILTEMPLATETBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO GENEMAILTEMPLATEINSTSP;
 p_errmsg:='Got dupe key on insert into GENEMAILTEMPLATETBL for key '
 ||'(EMAILTEMPLATEID)=('||new_rec.EMAILTEMPLATEID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENEMAILTEMPLATEINSTSP build 2018-01-23 08:25:09';
 ROLLBACK TO GENEMAILTEMPLATEINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILTEMPLATEINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILTEMPLATEINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILTEMPLATEINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in GENEMAILTEMPLATEINSTSP build 2018-01-23 08:25:09'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILTEMPLATEINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END GENEMAILTEMPLATEINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


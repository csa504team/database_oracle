<!--- Saved 01/28/2018 14:42:28. --->
PROCEDURE SOFVBFFAUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ASSUMPKEYLOANNMB CHAR:=null
 ,p_ASSUMPLASTEFFDT DATE:=null
 ,p_ASSUMPMAILADRCTYNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTCD CHAR:=null
 ,p_ASSUMPMAILADRSTR1NM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR2NM VARCHAR2:=null
 ,p_ASSUMPMAILADRZIP4CD CHAR:=null
 ,p_ASSUMPMAILADRZIPCD CHAR:=null
 ,p_ASSUMPNM VARCHAR2:=null
 ,p_ASSUMPNMADRCHNGDT DATE:=null
 ,p_ASSUMPNMMAILADRCHNGIND CHAR:=null
 ,p_ASSUMPOFC CHAR:=null
 ,p_ASSUMPPRGRM CHAR:=null
 ,p_ASSUMPSTATCDOFLOAN CHAR:=null
 ,p_ASSUMPTAXFORMFLD CHAR:=null
 ,p_ASSUMPTAXID CHAR:=null
 ,p_ASSUMPTAXMAINTNDT DATE:=null
 ,p_ASSUMPVERIFICATIONDT DATE:=null
 ,p_ASSUMPVERIFICATIONIND CHAR:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:53
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:29:53
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVBFFAUPDTSP performs UPDATE on STGCSA.SOFVBFFATBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ASSUMPKEYLOANNMB, ASSUMPLASTEFFDT
 for P_IDENTIFIER=1 qualification is on LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVBFFATBL%rowtype;
 new_rec STGCSA.SOFVBFFATBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVBFFATBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'Select of STGCSA.SOFVBFFATBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBFFAUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_ASSUMPTAXID is not null then
 cur_col_name:='P_ASSUMPTAXID';
 new_rec.ASSUMPTAXID:=P_ASSUMPTAXID; 
 end if;
 if P_ASSUMPNM is not null then
 cur_col_name:='P_ASSUMPNM';
 new_rec.ASSUMPNM:=P_ASSUMPNM; 
 end if;
 if P_ASSUMPMAILADRSTR1NM is not null then
 cur_col_name:='P_ASSUMPMAILADRSTR1NM';
 new_rec.ASSUMPMAILADRSTR1NM:=P_ASSUMPMAILADRSTR1NM; 
 end if;
 if P_ASSUMPMAILADRSTR2NM is not null then
 cur_col_name:='P_ASSUMPMAILADRSTR2NM';
 new_rec.ASSUMPMAILADRSTR2NM:=P_ASSUMPMAILADRSTR2NM; 
 end if;
 if P_ASSUMPMAILADRCTYNM is not null then
 cur_col_name:='P_ASSUMPMAILADRCTYNM';
 new_rec.ASSUMPMAILADRCTYNM:=P_ASSUMPMAILADRCTYNM; 
 end if;
 if P_ASSUMPMAILADRSTCD is not null then
 cur_col_name:='P_ASSUMPMAILADRSTCD';
 new_rec.ASSUMPMAILADRSTCD:=P_ASSUMPMAILADRSTCD; 
 end if;
 if P_ASSUMPMAILADRZIPCD is not null then
 cur_col_name:='P_ASSUMPMAILADRZIPCD';
 new_rec.ASSUMPMAILADRZIPCD:=P_ASSUMPMAILADRZIPCD; 
 end if;
 if P_ASSUMPMAILADRZIP4CD is not null then
 cur_col_name:='P_ASSUMPMAILADRZIP4CD';
 new_rec.ASSUMPMAILADRZIP4CD:=P_ASSUMPMAILADRZIP4CD; 
 end if;
 if P_ASSUMPOFC is not null then
 cur_col_name:='P_ASSUMPOFC';
 new_rec.ASSUMPOFC:=P_ASSUMPOFC; 
 end if;
 if P_ASSUMPPRGRM is not null then
 cur_col_name:='P_ASSUMPPRGRM';
 new_rec.ASSUMPPRGRM:=P_ASSUMPPRGRM; 
 end if;
 if P_ASSUMPSTATCDOFLOAN is not null then
 cur_col_name:='P_ASSUMPSTATCDOFLOAN';
 new_rec.ASSUMPSTATCDOFLOAN:=P_ASSUMPSTATCDOFLOAN; 
 end if;
 if P_ASSUMPVERIFICATIONIND is not null then
 cur_col_name:='P_ASSUMPVERIFICATIONIND';
 new_rec.ASSUMPVERIFICATIONIND:=P_ASSUMPVERIFICATIONIND; 
 end if;
 if P_ASSUMPVERIFICATIONDT is not null then
 cur_col_name:='P_ASSUMPVERIFICATIONDT';
 new_rec.ASSUMPVERIFICATIONDT:=P_ASSUMPVERIFICATIONDT; 
 end if;
 if P_ASSUMPNMMAILADRCHNGIND is not null then
 cur_col_name:='P_ASSUMPNMMAILADRCHNGIND';
 new_rec.ASSUMPNMMAILADRCHNGIND:=P_ASSUMPNMMAILADRCHNGIND; 
 end if;
 if P_ASSUMPNMADRCHNGDT is not null then
 cur_col_name:='P_ASSUMPNMADRCHNGDT';
 new_rec.ASSUMPNMADRCHNGDT:=P_ASSUMPNMADRCHNGDT; 
 end if;
 if P_ASSUMPTAXFORMFLD is not null then
 cur_col_name:='P_ASSUMPTAXFORMFLD';
 new_rec.ASSUMPTAXFORMFLD:=P_ASSUMPTAXFORMFLD; 
 end if;
 if P_ASSUMPTAXMAINTNDT is not null then
 cur_col_name:='P_ASSUMPTAXMAINTNDT';
 new_rec.ASSUMPTAXMAINTNDT:=P_ASSUMPTAXMAINTNDT; 
 end if;
 if P_ASSUMPKEYLOANNMB is not null then
 cur_col_name:='P_ASSUMPKEYLOANNMB';
 new_rec.ASSUMPKEYLOANNMB:=P_ASSUMPKEYLOANNMB; 
 end if;
 if P_ASSUMPLASTEFFDT is not null then
 cur_col_name:='P_ASSUMPLASTEFFDT';
 new_rec.ASSUMPLASTEFFDT:=P_ASSUMPLASTEFFDT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVBFFAAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVBFFATBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,ASSUMPTAXID=new_rec.ASSUMPTAXID
 ,ASSUMPNM=new_rec.ASSUMPNM
 ,ASSUMPMAILADRSTR1NM=new_rec.ASSUMPMAILADRSTR1NM
 ,ASSUMPMAILADRSTR2NM=new_rec.ASSUMPMAILADRSTR2NM
 ,ASSUMPMAILADRCTYNM=new_rec.ASSUMPMAILADRCTYNM
 ,ASSUMPMAILADRSTCD=new_rec.ASSUMPMAILADRSTCD
 ,ASSUMPMAILADRZIPCD=new_rec.ASSUMPMAILADRZIPCD
 ,ASSUMPMAILADRZIP4CD=new_rec.ASSUMPMAILADRZIP4CD
 ,ASSUMPOFC=new_rec.ASSUMPOFC
 ,ASSUMPPRGRM=new_rec.ASSUMPPRGRM
 ,ASSUMPSTATCDOFLOAN=new_rec.ASSUMPSTATCDOFLOAN
 ,ASSUMPVERIFICATIONIND=new_rec.ASSUMPVERIFICATIONIND
 ,ASSUMPVERIFICATIONDT=new_rec.ASSUMPVERIFICATIONDT
 ,ASSUMPNMMAILADRCHNGIND=new_rec.ASSUMPNMMAILADRCHNGIND
 ,ASSUMPNMADRCHNGDT=new_rec.ASSUMPNMADRCHNGDT
 ,ASSUMPTAXFORMFLD=new_rec.ASSUMPTAXFORMFLD
 ,ASSUMPTAXMAINTNDT=new_rec.ASSUMPTAXMAINTNDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'while doing update on STGCSA.SOFVBFFATBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBFFAUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVBFFAUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBFFATBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ASSUMPKEYLOANNMB)=('||P_ASSUMPKEYLOANNMB||') ''(ASSUMPLASTEFFDT)=('||P_ASSUMPLASTEFFDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVBFFATBL where 1=1
 and ASSUMPKEYLOANNMB=P_ASSUMPKEYLOANNMB and ASSUMPLASTEFFDT=P_ASSUMPLASTEFFDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'No STGCSA.SOFVBFFATBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r1 in (select rowid from STGCSA.SOFVBFFATBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO SOFVBFFAUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'Oracle returned error fetching STGCSA.SOFVBFFATBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO SOFVBFFAUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'No STGCSA.SOFVBFFATBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBFFAUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBFFAUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBFFAUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVBFFAUPDTSP build 2018-01-23 08:29:53'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBFFAUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVBFFAUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


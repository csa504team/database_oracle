<!--- Saved 01/28/2018 14:42:09. --->
PROCEDURE IMPTCHKMSTRDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CHKACHCMPID VARCHAR2:=null
 ,p_CHKACHINTFMTCD VARCHAR2:=null
 ,p_CHKBATID VARCHAR2:=null
 ,p_CHKBNKTOBNKINFO VARCHAR2:=null
 ,p_CHKCCRDIVISN VARCHAR2:=null
 ,p_CHKCCREXPDT DATE:=null
 ,p_CHKCCRFACNM VARCHAR2:=null
 ,p_CHKCCRMCCCD VARCHAR2:=null
 ,p_CHKCCRMERCHNTID VARCHAR2:=null
 ,p_CHKCCRPAYEETYP VARCHAR2:=null
 ,p_CHKCEOCMPID VARCHAR2:=null
 ,p_CHKCEOTMPLID VARCHAR2:=null
 ,p_CHKCHKIMGDESC VARCHAR2:=null
 ,p_CHKCHKIMGID VARCHAR2:=null
 ,p_CHKCOURACCT VARCHAR2:=null
 ,p_CHKCOURNM VARCHAR2:=null
 ,p_CHKCRDDBTFL VARCHAR2:=null
 ,p_CHKDEL1FAXNMB CHAR:=null
 ,p_CHKDEL2FAXNMB CHAR:=null
 ,p_CHKDEL3FAXNMB CHAR:=null
 ,p_CHKDELCD NUMBER:=null
 ,p_CHKDELCNM VARCHAR2:=null
 ,p_CHKDELCOID1 VARCHAR2:=null
 ,p_CHKDELCOID2 VARCHAR2:=null
 ,p_CHKDELCOID3 VARCHAR2:=null
 ,p_CHKDELEMAILADR1 VARCHAR2:=null
 ,p_CHKDELEMAILADR2 VARCHAR2:=null
 ,p_CHKDELEMAILADR3 VARCHAR2:=null
 ,p_CHKDELNM2 VARCHAR2:=null
 ,p_CHKDELNM3 VARCHAR2:=null
 ,p_CHKDELPARTYADDNM VARCHAR2:=null
 ,p_CHKDELPARTYID VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRCNTRYNM VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRCTYNM VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRSTPRO VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKDELPARTYMAILADRSTR3NM VARCHAR2:=null
 ,p_CHKDELPARTYNM VARCHAR2:=null
 ,p_CHKDELPARTYPMAILADRSTCD CHAR:=null
 ,p_CHKDELRTNLOCCD VARCHAR2:=null
 ,p_CHKDELTYP1 VARCHAR2:=null
 ,p_CHKDELTYP2 VARCHAR2:=null
 ,p_CHKDELTYP3 VARCHAR2:=null
 ,p_CHKDELUSERID1 VARCHAR2:=null
 ,p_CHKDELUSERID2 VARCHAR2:=null
 ,p_CHKDELUSERID3 VARCHAR2:=null
 ,p_CHKDOCTMPLNMB VARCHAR2:=null
 ,p_CHKEDDBILLID VARCHAR2:=null
 ,p_CHKEDDCOURNMB1 VARCHAR2:=null
 ,p_CHKEDDCOURNMB2 VARCHAR2:=null
 ,p_CHKEDDCOURNMB3 VARCHAR2:=null
 ,p_CHKEDDDLVRCD1 VARCHAR2:=null
 ,p_CHKEDDDLVRCD2 VARCHAR2:=null
 ,p_CHKEDDDLVRCD3 VARCHAR2:=null
 ,p_CHKEDDHNDLCD VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTRY1NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTRY2NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTRY3NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTY1NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTY2NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRCTY3NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRST1 VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRST2 VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRST3 VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR11NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR12NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR13NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR21NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR22NM VARCHAR2:=null
 ,p_CHKEDDRECVMAILADRSTR23NM VARCHAR2:=null
 ,p_CHKEDDRECVPMAILADRSTCD1 CHAR:=null
 ,p_CHKEDDRECVPMAILADRSTCD2 CHAR:=null
 ,p_CHKEDDRECVPMAILADRSTCD3 CHAR:=null
 ,p_CHKEFFDT DATE:=null
 ,p_CHKFILEFRMT VARCHAR2:=null
 ,p_CHKFLXINSID1 VARCHAR2:=null
 ,p_CHKFLXINSID2 VARCHAR2:=null
 ,p_CHKFLXINSID3 VARCHAR2:=null
 ,p_CHKFLXINSTYP3 VARCHAR2:=null
 ,p_CHKFLXNISTYP1 VARCHAR2:=null
 ,p_CHKFLXNISTYP2 VARCHAR2:=null
 ,p_CHKFXCONNMB VARCHAR2:=null
 ,p_CHKFXTYP VARCHAR2:=null
 ,p_CHKIMPTCHKMSTRCUR VARCHAR2:=null
 ,p_CHKIMPTCHKMSTRID NUMBER:=null
 ,p_CHKINTBNKID1 VARCHAR2:=null
 ,p_CHKINTBNKID2 VARCHAR2:=null
 ,p_CHKINTBNKIDTYP1 VARCHAR2:=null
 ,p_CHKINTBNKIDTYP2 VARCHAR2:=null
 ,p_CHKINTBNKMAILADRCNTRYCD1 VARCHAR2:=null
 ,p_CHKINTBNKMAILADRCNTRYCD2 VARCHAR2:=null
 ,p_CHKINTBNKMAILADRCTY1NM VARCHAR2:=null
 ,p_CHKINTBNKMAILADRCTY2NM VARCHAR2:=null
 ,p_CHKINTBNKMAILADRSTPRO1 VARCHAR2:=null
 ,p_CHKINTBNKMAILADRSTPRO2 VARCHAR2:=null
 ,p_CHKINTBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKINTBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKINTBNKNM VARCHAR2:=null
 ,p_CHKINTBNKNM2 VARCHAR2:=null
 ,p_CHKINTBNKPMAILADRSTCD1 CHAR:=null
 ,p_CHKINTBNKPMAILADRSTCD2 CHAR:=null
 ,p_CHKINVDESC VARCHAR2:=null
 ,p_CHKINVDIST VARCHAR2:=null
 ,p_CHKINVDT DATE:=null
 ,p_CHKINVGRS VARCHAR2:=null
 ,p_CHKINVNET VARCHAR2:=null
 ,p_CHKINVNMB VARCHAR2:=null
 ,p_CHKINVTYP VARCHAR2:=null
 ,p_CHKNMB VARCHAR2:=null
 ,p_CHKOPRID VARCHAR2:=null
 ,p_CHKORDPARTYID VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRCNTRYNM VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRCTYNM VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRSTPRO VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKORDPARTYMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKORDPARTYNM VARCHAR2:=null
 ,p_CHKORDPARTYPMAILADRSTCD CHAR:=null
 ,p_CHKORGLACCT NUMBER:=null
 ,p_CHKORGLACCTCUR VARCHAR2:=null
 ,p_CHKORGLACCTTY VARCHAR2:=null
 ,p_CHKORGLBNKID NUMBER:=null
 ,p_CHKORGLBNKIDTY VARCHAR2:=null
 ,p_CHKORGLBNKMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKORGLBNKMAILADRCTYNM VARCHAR2:=null
 ,p_CHKORGLBNKMAILADRSTPRO VARCHAR2:=null
 ,p_CHKORGLBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKORGLBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKORGLBNKNM VARCHAR2:=null
 ,p_CHKORGLBNKPMAILADRSTCD CHAR:=null
 ,p_CHKORGLPARTYADDNM VARCHAR2:=null
 ,p_CHKORGLPARTYID VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRCNTRYNM VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRCTYNM VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRSTPRO VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKORGLPARTYMAILADRSTR3NM VARCHAR2:=null
 ,p_CHKORGLPARTYNM VARCHAR2:=null
 ,p_CHKORGLPARTYPMAILADRSTCD CHAR:=null
 ,p_CHKORGLTORECVPARTYINFO VARCHAR2:=null
 ,p_CHKPAYEEEMAILADR VARCHAR2:=null
 ,p_CHKPAYMTHD VARCHAR2:=null
 ,p_CHKPDPHNDLCD VARCHAR2:=null
 ,p_CHKPHNNMB CHAR:=null
 ,p_CHKPODESC VARCHAR2:=null
 ,p_CHKPONMB VARCHAR2:=null
 ,p_CHKPROCDT DATE:=null
 ,p_CHKPYMTAMT NUMBER:=null
 ,p_CHKPYMTFMTCD VARCHAR2:=null
 ,p_CHKRECVACCTCUR VARCHAR2:=null
 ,p_CHKRECVBNKID VARCHAR2:=null
 ,p_CHKRECVBNKIDTY VARCHAR2:=null
 ,p_CHKRECVBNKMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKRECVBNKMAILADRCTYNM VARCHAR2:=null
 ,p_CHKRECVBNKMAILADRSTPRO VARCHAR2:=null
 ,p_CHKRECVBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKRECVBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKRECVBNKNM VARCHAR2:=null
 ,p_CHKRECVBNKPMAILADRSTCD CHAR:=null
 ,p_CHKRECVBNKSECID VARCHAR2:=null
 ,p_CHKRECVPARTYACCT VARCHAR2:=null
 ,p_CHKRECVPARTYACCTTY VARCHAR2:=null
 ,p_CHKRECVPARTYADDNM VARCHAR2:=null
 ,p_CHKRECVPARTYID VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRCNTRYNM VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRCTYNM VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRSTPRO VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKRECVPARTYMAILADRSTR3NM VARCHAR2:=null
 ,p_CHKRECVPARTYNM VARCHAR2:=null
 ,p_CHKRECVPARTYPMAILADRSTCD CHAR:=null
 ,p_CHKRTNPARTYADDNM VARCHAR2:=null
 ,p_CHKRTNPARTYID VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRCNTRYCD VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRCNTRYNM VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRCTYNM VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRSTPRO VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRSTR1NM VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRSTR2NM VARCHAR2:=null
 ,p_CHKRTNPARTYMAILADRSTR3NM VARCHAR2:=null
 ,p_CHKRTNPARTYNM VARCHAR2:=null
 ,p_CHKRTNPARTYPMAILADRSTCD CHAR:=null
 ,p_CHKSECPWD11 VARCHAR2:=null
 ,p_CHKSECPWD12 VARCHAR2:=null
 ,p_CHKSECPWD13 VARCHAR2:=null
 ,p_CHKSECPWD21 VARCHAR2:=null
 ,p_CHKSECPWD22 VARCHAR2:=null
 ,p_CHKSECPWD23 VARCHAR2:=null
 ,p_CHKSECQ11 VARCHAR2:=null
 ,p_CHKSECQ12 VARCHAR2:=null
 ,p_CHKSECQ13 VARCHAR2:=null
 ,p_CHKSECQ21 VARCHAR2:=null
 ,p_CHKSECQ22 VARCHAR2:=null
 ,p_CHKSECQ23 VARCHAR2:=null
 ,p_CHKSECTYP1 VARCHAR2:=null
 ,p_CHKSECTYP2 VARCHAR2:=null
 ,p_CHKSECTYP3 VARCHAR2:=null
 ,p_CHKSEPAREFID VARCHAR2:=null
 ,p_CHKTRANSNMB VARCHAR2:=null
 ,p_CHKVALDT DATE:=null
 ,p_CHKWIRECHGIND CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:07
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:27:07
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure IMPTCHKMSTRDELTSP performs DELETE on STGCSA.IMPTCHKMSTRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CHKIMPTCHKMSTRID
 for P_IDENTIFIER=1 qualification is on CHKINVNMB like pctsign
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.IMPTCHKMSTRTBL%rowtype;
 new_rec STGCSA.IMPTCHKMSTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.IMPTCHKMSTRTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'Select of STGCSA.IMPTCHKMSTRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.IMPTCHKMSTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.IMPTCHKMSTRTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'while doing delete on STGCSA.IMPTCHKMSTRTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint IMPTCHKMSTRDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CHKIMPTCHKMSTRID)=('||P_CHKIMPTCHKMSTRID||') ';
 select rowid into rec_rowid from STGCSA.IMPTCHKMSTRTBL where 1=1
 and CHKIMPTCHKMSTRID=P_CHKIMPTCHKMSTRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'No STGCSA.IMPTCHKMSTRTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(CHKINVNMB like pctsign) '||'';
 begin
 for r1 in (select rowid from STGCSA.IMPTCHKMSTRTBL a where 1=1 
 
 
 and CHKINVNMB like pctsign) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'Oracle returned error fetching STGCSA.IMPTCHKMSTRTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'No STGCSA.IMPTCHKMSTRTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End IMPTCHKMSTRDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'IMPTCHKMSTRDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in IMPTCHKMSTRDELTSP build 2018-01-23 08:27:07'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO IMPTCHKMSTRDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END IMPTCHKMSTRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


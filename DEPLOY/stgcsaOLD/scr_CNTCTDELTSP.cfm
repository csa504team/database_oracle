<!--- Saved 01/28/2018 14:41:36. --->
PROCEDURE CNTCTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCREF NUMBER:=null
 ,p_CNTCTEMAILADR VARCHAR2:=null
 ,p_CNTCTFAXNMB VARCHAR2:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_CNTCTID NUMBER:=null
 ,p_CNTCTLASTNM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTPHNNMB VARCHAR2:=null
 ,p_CNTCTPHNXTN CHAR:=null
 ,p_CNTCTTITL VARCHAR2:=null
 ,p_CREATBY NUMBER:=null
 ,p_ENTID NUMBER:=null
 ,p_UPDTBY NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:32
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:18:32
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CNTCTDELTSP performs DELETE on STGCSA.CNTCTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CNTCTID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CNTCTTBL%rowtype;
 new_rec STGCSA.CNTCTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CNTCTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTDELTSP build 2018-01-23 08:18:32 '
 ||'Select of STGCSA.CNTCTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CNTCTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.CNTCTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CNTCTDELTSP build 2018-01-23 08:18:32 '
 ||'while doing delete on STGCSA.CNTCTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CNTCTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CNTCTID)=('||P_CNTCTID||') ';
 select rowid into rec_rowid from STGCSA.CNTCTTBL where 1=1
 and CNTCTID=P_CNTCTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTDELTSP build 2018-01-23 08:18:32 '
 ||'No STGCSA.CNTCTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In CNTCTDELTSP build 2018-01-23 08:18:32 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CNTCTDELTSP build 2018-01-23 08:18:32'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CNTCTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


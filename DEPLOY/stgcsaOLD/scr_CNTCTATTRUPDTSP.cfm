<!--- Saved 01/28/2018 14:41:35. --->
PROCEDURE CNTCTATTRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ATTRID NUMBER:=null
 ,p_ATTRVAL VARCHAR2:=null
 ,p_CDCCNTCTATTRID NUMBER:=null
 ,p_CNTCTID NUMBER:=null
 ,p_CREATBY NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:17:28
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:17:29
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CNTCTATTRUPDTSP performs UPDATE on STGCSA.CNTCTATTRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CDCCNTCTATTRID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CNTCTATTRTBL%rowtype;
 new_rec STGCSA.CNTCTATTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CNTCTATTRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTATTRUPDTSP build 2018-01-23 08:17:28 '
 ||'Select of STGCSA.CNTCTATTRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTATTRUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CDCCNTCTATTRID is not null then
 cur_col_name:='P_CDCCNTCTATTRID';
 new_rec.CDCCNTCTATTRID:=P_CDCCNTCTATTRID; 
 end if;
 if P_CNTCTID is not null then
 cur_col_name:='P_CNTCTID';
 new_rec.CNTCTID:=P_CNTCTID; 
 end if;
 if P_ATTRID is not null then
 cur_col_name:='P_ATTRID';
 new_rec.ATTRID:=P_ATTRID; 
 end if;
 if P_ATTRVAL is not null then
 cur_col_name:='P_ATTRVAL';
 new_rec.ATTRVAL:=P_ATTRVAL; 
 end if;
 if P_CREATBY is not null then
 cur_col_name:='P_CREATBY';
 new_rec.CREATBY:=P_CREATBY; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CNTCTATTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CNTCTATTRTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CNTCTID=new_rec.CNTCTID
 ,ATTRID=new_rec.ATTRID
 ,ATTRVAL=new_rec.ATTRVAL
 ,CREATBY=new_rec.CREATBY
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CNTCTATTRUPDTSP build 2018-01-23 08:17:28 '
 ||'while doing update on STGCSA.CNTCTATTRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CNTCTATTRUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CNTCTATTRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CDCCNTCTATTRID)=('||P_CDCCNTCTATTRID||') ';
 select rowid into rec_rowid from STGCSA.CNTCTATTRTBL where 1=1
 and CDCCNTCTATTRID=P_CDCCNTCTATTRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CNTCTATTRUPDTSP build 2018-01-23 08:17:28 '
 ||'No STGCSA.CNTCTATTRTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTATTRUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In CNTCTATTRUPDTSP build 2018-01-23 08:17:28 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTATTRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTATTRUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In CNTCTATTRUPDTSP build 2018-01-23 08:17:28'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTATTRUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CNTCTATTRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


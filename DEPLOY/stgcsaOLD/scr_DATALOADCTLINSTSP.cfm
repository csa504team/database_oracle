<!--- Saved 10/31/2017 13:18:30. --->
PROCEDURE DATALOADCTLInsTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_PROCESSID NUMBER:=NULL, P_PROCESSCODE VARCHAR2:=NULL,
 P_PROCESSNAME VARCHAR2:=NULL, P_STARTDT TIMESTAMP:=NULL,
 P_ENDDT TIMESTAMP:=NULL, P_INUSEIND NUMBER:=NULL)
 
 as
 /*
 <!--- generated 2017-09-28 14:03:51 by SYS --->
 Created on: 2017-09-28 14:03:51
 Created by: SYS
 Crerated from genr, stdtblins_template v 1.0
 Purpose : standard insert of row to table STGCSA.DATALOADCTLTBL
 */
 BEGIN 
 savepoint DATALOADCTLInsTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 insert into STGCSA.DATALOADCTLTBL (
 PROCESSID, PROCESSCODE, PROCESSNAME, STARTDT, ENDDT, INUSEIND
 ) values (
 P_PROCESSID, P_PROCESSCODE, P_PROCESSNAME, P_STARTDT, P_ENDDT,
 P_INUSEIND
 );
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO DATALOADCTLInsTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END DATALOADCTLInsTsp;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


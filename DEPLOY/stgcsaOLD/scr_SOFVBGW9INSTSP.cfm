<!--- Saved 01/28/2018 14:42:30. --->
PROCEDURE SOFVBGW9INSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_W9TAXID CHAR:=null
 ,p_W9NM VARCHAR2:=null
 ,p_W9MAILADRSTR1NM VARCHAR2:=null
 ,p_W9MAILADRSTR2NM VARCHAR2:=null
 ,p_W9MAILADRCTYNM VARCHAR2:=null
 ,p_W9MAILADRSTCD CHAR:=null
 ,p_W9MAILADRZIPCD CHAR:=null
 ,p_W9MAILADRZIP4CD CHAR:=null
 ,p_W9VERIFICATIONIND CHAR:=null
 ,p_W9VERIFICATIONDT DATE:=null
 ,p_W9NMMAILADRCHNGIND CHAR:=null
 ,p_W9NMADRCHNGDT DATE:=null
 ,p_W9TAXFORMFLD CHAR:=null
 ,p_W9TAXMAINTNDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:30:37
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:30:37
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVBGW9TBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVBGW9TBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVBGW9INSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBGW9TBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVBGW9TBL','LOANNMB','CHAR',10);
 cur_col_name:='P_W9TAXID';
 new_rec.W9TAXID:=runtime.validate_column_value(P_W9TAXID,
 'STGCSA','SOFVBGW9TBL','W9TAXID','CHAR',10);
 cur_col_name:='P_W9NM';
 new_rec.W9NM:=runtime.validate_column_value(P_W9NM,
 'STGCSA','SOFVBGW9TBL','W9NM','VARCHAR2',80);
 cur_col_name:='P_W9MAILADRSTR1NM';
 new_rec.W9MAILADRSTR1NM:=runtime.validate_column_value(P_W9MAILADRSTR1NM,
 'STGCSA','SOFVBGW9TBL','W9MAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_W9MAILADRSTR2NM';
 new_rec.W9MAILADRSTR2NM:=runtime.validate_column_value(P_W9MAILADRSTR2NM,
 'STGCSA','SOFVBGW9TBL','W9MAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_W9MAILADRCTYNM';
 new_rec.W9MAILADRCTYNM:=runtime.validate_column_value(P_W9MAILADRCTYNM,
 'STGCSA','SOFVBGW9TBL','W9MAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_W9MAILADRSTCD';
 new_rec.W9MAILADRSTCD:=runtime.validate_column_value(P_W9MAILADRSTCD,
 'STGCSA','SOFVBGW9TBL','W9MAILADRSTCD','CHAR',2);
 cur_col_name:='P_W9MAILADRZIPCD';
 new_rec.W9MAILADRZIPCD:=runtime.validate_column_value(P_W9MAILADRZIPCD,
 'STGCSA','SOFVBGW9TBL','W9MAILADRZIPCD','CHAR',5);
 cur_col_name:='P_W9MAILADRZIP4CD';
 new_rec.W9MAILADRZIP4CD:=runtime.validate_column_value(P_W9MAILADRZIP4CD,
 'STGCSA','SOFVBGW9TBL','W9MAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_W9VERIFICATIONIND';
 new_rec.W9VERIFICATIONIND:=runtime.validate_column_value(P_W9VERIFICATIONIND,
 'STGCSA','SOFVBGW9TBL','W9VERIFICATIONIND','CHAR',1);
 cur_col_name:='P_W9VERIFICATIONDT';
 new_rec.W9VERIFICATIONDT:=runtime.validate_column_value(P_W9VERIFICATIONDT,
 'STGCSA','SOFVBGW9TBL','W9VERIFICATIONDT','DATE',7);
 cur_col_name:='P_W9NMMAILADRCHNGIND';
 new_rec.W9NMMAILADRCHNGIND:=runtime.validate_column_value(P_W9NMMAILADRCHNGIND,
 'STGCSA','SOFVBGW9TBL','W9NMMAILADRCHNGIND','CHAR',1);
 cur_col_name:='P_W9NMADRCHNGDT';
 new_rec.W9NMADRCHNGDT:=runtime.validate_column_value(P_W9NMADRCHNGDT,
 'STGCSA','SOFVBGW9TBL','W9NMADRCHNGDT','DATE',7);
 cur_col_name:='P_W9TAXFORMFLD';
 new_rec.W9TAXFORMFLD:=runtime.validate_column_value(P_W9TAXFORMFLD,
 'STGCSA','SOFVBGW9TBL','W9TAXFORMFLD','CHAR',1);
 cur_col_name:='P_W9TAXMAINTNDT';
 new_rec.W9TAXMAINTNDT:=runtime.validate_column_value(P_W9TAXMAINTNDT,
 'STGCSA','SOFVBGW9TBL','W9TAXMAINTNDT','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVBGW9INSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVBGW9AUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVBGW9TBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVBGW9INSTSP;
 p_errmsg:='Got dupe key on insert into SOFVBGW9TBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVBGW9INSTSP build 2018-01-23 08:30:37';
 ROLLBACK TO SOFVBGW9INSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBGW9INSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBGW9INSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBGW9INSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVBGW9INSTSP build 2018-01-23 08:30:37'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBGW9INSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVBGW9INSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


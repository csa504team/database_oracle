<!--- Saved 01/28/2018 14:42:48. --->
PROCEDURE SOFVOTRNUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_ONLNTRANSDETPYMTDT DATE:=null
 ,p_ONLNTRANSDTLREST CHAR:=null
 ,p_ONLNTRANSDTLREST5 CHAR:=null
 ,p_ONLNTRANSFIL10 CHAR:=null
 ,p_ONLNTRANSFILENM CHAR:=null
 ,p_ONLNTRANSOPRTRINITIAL CHAR:=null
 ,p_ONLNTRANSSCN CHAR:=null
 ,p_ONLNTRANSTRANSDT TIMESTAMP:=null
 ,p_ONLNTRANSTRMNLID CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:35:12
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:35:12
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVOTRNUPDTSP performs UPDATE on STGCSA.SOFVOTRNTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, ONLNTRANSTRANSDT, ONLNTRANSTRMNLID, ONLNTRANSOPRTRINITIAL
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVOTRNTBL%rowtype;
 new_rec STGCSA.SOFVOTRNTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVOTRNTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVOTRNUPDTSP build 2018-01-23 08:35:12 '
 ||'Select of STGCSA.SOFVOTRNTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVOTRNUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_ONLNTRANSTRANSDT is not null then
 cur_col_name:='P_ONLNTRANSTRANSDT';
 new_rec.ONLNTRANSTRANSDT:=P_ONLNTRANSTRANSDT; 
 end if;
 if P_ONLNTRANSTRMNLID is not null then
 cur_col_name:='P_ONLNTRANSTRMNLID';
 new_rec.ONLNTRANSTRMNLID:=P_ONLNTRANSTRMNLID; 
 end if;
 if P_ONLNTRANSOPRTRINITIAL is not null then
 cur_col_name:='P_ONLNTRANSOPRTRINITIAL';
 new_rec.ONLNTRANSOPRTRINITIAL:=P_ONLNTRANSOPRTRINITIAL; 
 end if;
 if P_ONLNTRANSFILENM is not null then
 cur_col_name:='P_ONLNTRANSFILENM';
 new_rec.ONLNTRANSFILENM:=P_ONLNTRANSFILENM; 
 end if;
 if P_ONLNTRANSSCN is not null then
 cur_col_name:='P_ONLNTRANSSCN';
 new_rec.ONLNTRANSSCN:=P_ONLNTRANSSCN; 
 end if;
 if P_ONLNTRANSFIL10 is not null then
 cur_col_name:='P_ONLNTRANSFIL10';
 new_rec.ONLNTRANSFIL10:=P_ONLNTRANSFIL10; 
 end if;
 if P_ONLNTRANSDETPYMTDT is not null then
 cur_col_name:='P_ONLNTRANSDETPYMTDT';
 new_rec.ONLNTRANSDETPYMTDT:=P_ONLNTRANSDETPYMTDT; 
 end if;
 if P_ONLNTRANSDTLREST is not null then
 cur_col_name:='P_ONLNTRANSDTLREST';
 new_rec.ONLNTRANSDTLREST:=P_ONLNTRANSDTLREST; 
 end if;
 if P_ONLNTRANSDTLREST5 is not null then
 cur_col_name:='P_ONLNTRANSDTLREST5';
 new_rec.ONLNTRANSDTLREST5:=P_ONLNTRANSDTLREST5; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVOTRNAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVOTRNTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,ONLNTRANSFILENM=new_rec.ONLNTRANSFILENM
 ,ONLNTRANSSCN=new_rec.ONLNTRANSSCN
 ,ONLNTRANSFIL10=new_rec.ONLNTRANSFIL10
 ,ONLNTRANSDETPYMTDT=new_rec.ONLNTRANSDETPYMTDT
 ,ONLNTRANSDTLREST=new_rec.ONLNTRANSDTLREST
 ,ONLNTRANSDTLREST5=new_rec.ONLNTRANSDTLREST5
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVOTRNUPDTSP build 2018-01-23 08:35:12 '
 ||'while doing update on STGCSA.SOFVOTRNTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVOTRNUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVOTRNUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVOTRNTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ''(ONLNTRANSTRANSDT)=('||P_ONLNTRANSTRANSDT||') ''(ONLNTRANSTRMNLID)=('||P_ONLNTRANSTRMNLID||') ''(ONLNTRANSOPRTRINITIAL)=('||P_ONLNTRANSOPRTRINITIAL||') ';
 select rowid into rec_rowid from STGCSA.SOFVOTRNTBL where 1=1
 and LOANNMB=P_LOANNMB and ONLNTRANSTRANSDT=P_ONLNTRANSTRANSDT and ONLNTRANSTRMNLID=P_ONLNTRANSTRMNLID and ONLNTRANSOPRTRINITIAL=P_ONLNTRANSOPRTRINITIAL;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVOTRNUPDTSP build 2018-01-23 08:35:12 '
 ||'No STGCSA.SOFVOTRNTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVOTRNUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVOTRNUPDTSP build 2018-01-23 08:35:12 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVOTRNUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVOTRNUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVOTRNUPDTSP build 2018-01-23 08:35:12'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVOTRNUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVOTRNUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


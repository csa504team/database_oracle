<!--- Saved 01/28/2018 14:42:38. --->
PROCEDURE SOFVFTPOUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_PREPOSTRVWADDENDRECIND NUMBER:=null
 ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
 ,p_PREPOSTRVWCMNT VARCHAR2:=null
 ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
 ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
 ,p_PREPOSTRVWPYMTAMT NUMBER:=null
 ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
 ,p_PREPOSTRVWTRACENMB NUMBER:=null
 ,p_PREPOSTRVWTRANSCD CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTCHKDGT CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:34
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:32:34
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVFTPOUPDTSP performs UPDATE on STGCSA.SOFVFTPOTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVFTPOTBL%rowtype;
 new_rec STGCSA.SOFVFTPOTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVFTPOTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVFTPOUPDTSP build 2018-01-23 08:32:34 '
 ||'Select of STGCSA.SOFVFTPOTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVFTPOUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PREPOSTRVWRECRDTYPCD is not null then
 cur_col_name:='P_PREPOSTRVWRECRDTYPCD';
 new_rec.PREPOSTRVWRECRDTYPCD:=P_PREPOSTRVWRECRDTYPCD; 
 end if;
 if P_PREPOSTRVWTRANSCD is not null then
 cur_col_name:='P_PREPOSTRVWTRANSCD';
 new_rec.PREPOSTRVWTRANSCD:=P_PREPOSTRVWTRANSCD; 
 end if;
 if P_PREPOSTRVWTRANSITROUTINGNMB is not null then
 cur_col_name:='P_PREPOSTRVWTRANSITROUTINGNMB';
 new_rec.PREPOSTRVWTRANSITROUTINGNMB:=P_PREPOSTRVWTRANSITROUTINGNMB; 
 end if;
 if P_PREPOSTRVWTRANSITROUTCHKDGT is not null then
 cur_col_name:='P_PREPOSTRVWTRANSITROUTCHKDGT';
 new_rec.PREPOSTRVWTRANSITROUTINGCHKDGT:=P_PREPOSTRVWTRANSITROUTCHKDGT; 
 end if;
 if P_PREPOSTRVWBNKACCTNMB is not null then
 cur_col_name:='P_PREPOSTRVWBNKACCTNMB';
 new_rec.PREPOSTRVWBNKACCTNMB:=P_PREPOSTRVWBNKACCTNMB; 
 end if;
 if P_PREPOSTRVWPYMTAMT is not null then
 cur_col_name:='P_PREPOSTRVWPYMTAMT';
 new_rec.PREPOSTRVWPYMTAMT:=P_PREPOSTRVWPYMTAMT; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_PREPOSTRVWINDVLNM is not null then
 cur_col_name:='P_PREPOSTRVWINDVLNM';
 new_rec.PREPOSTRVWINDVLNM:=P_PREPOSTRVWINDVLNM; 
 end if;
 if P_PREPOSTRVWCOMPBNKDISC is not null then
 cur_col_name:='P_PREPOSTRVWCOMPBNKDISC';
 new_rec.PREPOSTRVWCOMPBNKDISC:=P_PREPOSTRVWCOMPBNKDISC; 
 end if;
 if P_PREPOSTRVWADDENDRECIND is not null then
 cur_col_name:='P_PREPOSTRVWADDENDRECIND';
 new_rec.PREPOSTRVWADDENDRECIND:=P_PREPOSTRVWADDENDRECIND; 
 end if;
 if P_PREPOSTRVWTRACENMB is not null then
 cur_col_name:='P_PREPOSTRVWTRACENMB';
 new_rec.PREPOSTRVWTRACENMB:=P_PREPOSTRVWTRACENMB; 
 end if;
 if P_PREPOSTRVWCMNT is not null then
 cur_col_name:='P_PREPOSTRVWCMNT';
 new_rec.PREPOSTRVWCMNT:=P_PREPOSTRVWCMNT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVFTPOAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVFTPOTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,PREPOSTRVWRECRDTYPCD=new_rec.PREPOSTRVWRECRDTYPCD
 ,PREPOSTRVWTRANSCD=new_rec.PREPOSTRVWTRANSCD
 ,PREPOSTRVWTRANSITROUTINGNMB=new_rec.PREPOSTRVWTRANSITROUTINGNMB
 ,PREPOSTRVWTRANSITROUTINGCHKDGT=new_rec.PREPOSTRVWTRANSITROUTINGCHKDGT
 ,PREPOSTRVWBNKACCTNMB=new_rec.PREPOSTRVWBNKACCTNMB
 ,PREPOSTRVWPYMTAMT=new_rec.PREPOSTRVWPYMTAMT
 ,PREPOSTRVWINDVLNM=new_rec.PREPOSTRVWINDVLNM
 ,PREPOSTRVWCOMPBNKDISC=new_rec.PREPOSTRVWCOMPBNKDISC
 ,PREPOSTRVWADDENDRECIND=new_rec.PREPOSTRVWADDENDRECIND
 ,PREPOSTRVWTRACENMB=new_rec.PREPOSTRVWTRACENMB
 ,PREPOSTRVWCMNT=new_rec.PREPOSTRVWCMNT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVFTPOUPDTSP build 2018-01-23 08:32:34 '
 ||'while doing update on STGCSA.SOFVFTPOTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVFTPOUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVFTPOUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVFTPOTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVFTPOTBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVFTPOUPDTSP build 2018-01-23 08:32:34 '
 ||'No STGCSA.SOFVFTPOTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVFTPOUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVFTPOUPDTSP build 2018-01-23 08:32:34 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVFTPOUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVFTPOUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVFTPOUPDTSP build 2018-01-23 08:32:34'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVFTPOUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVFTPOUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


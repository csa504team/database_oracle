<!--- Saved 01/28/2018 14:42:16. --->
PROCEDURE PRPLOANRQSTUPDTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPCDCUPDTID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_FORMNM VARCHAR2:=null
 ,p_LOANRQSTFLDNM VARCHAR2:=null
 ,p_CORRECTVALAMT NUMBER:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:52
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:27:52
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPLOANRQSTUPDTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PRPLOANRQSTUPDTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PRPLOANRQSTUPDTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PRPCDCUPDTID';
 new_rec.PRPCDCUPDTID:=runtime.validate_column_value(P_PRPCDCUPDTID,
 'STGCSA','PRPLOANRQSTUPDTTBL','PRPCDCUPDTID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PRPLOANRQSTUPDTTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_FORMNM';
 new_rec.FORMNM:=runtime.validate_column_value(P_FORMNM,
 'STGCSA','PRPLOANRQSTUPDTTBL','FORMNM','VARCHAR2',75);
 cur_col_name:='P_LOANRQSTFLDNM';
 new_rec.LOANRQSTFLDNM:=runtime.validate_column_value(P_LOANRQSTFLDNM,
 'STGCSA','PRPLOANRQSTUPDTTBL','LOANRQSTFLDNM','VARCHAR2',100);
 cur_col_name:='P_CORRECTVALAMT';
 new_rec.CORRECTVALAMT:=runtime.validate_column_value(P_CORRECTVALAMT,
 'STGCSA','PRPLOANRQSTUPDTTBL','CORRECTVALAMT','NUMBER',22);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','PRPLOANRQSTUPDTTBL','TRANSID','NUMBER',22);
 cur_col_name:='P_TRANSTYPID';
 new_rec.TRANSTYPID:=runtime.validate_column_value(P_TRANSTYPID,
 'STGCSA','PRPLOANRQSTUPDTTBL','TRANSTYPID','NUMBER',22);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PRPLOANRQSTUPDTTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PRPLOANRQSTUPDTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PRPLOANRQSTUPDTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PRPLOANRQSTUPDTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PRPLOANRQSTUPDTINSTSP;
 p_errmsg:='Got dupe key on insert into PRPLOANRQSTUPDTTBL for key '
 ||'(PRPCDCUPDTID)=('||new_rec.PRPCDCUPDTID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPLOANRQSTUPDTINSTSP build 2018-01-23 08:27:52';
 ROLLBACK TO PRPLOANRQSTUPDTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTUPDTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTUPDTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTUPDTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PRPLOANRQSTUPDTINSTSP build 2018-01-23 08:27:52'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTUPDTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PRPLOANRQSTUPDTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


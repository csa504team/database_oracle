<!--- Saved 01/28/2018 14:42:40. --->
PROCEDURE SOFVGRGCUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CALNDRCUTOFFIND CHAR:=null
 ,p_CALNDRDAY CHAR:=null
 ,p_CALNDRDESC VARCHAR2:=null
 ,p_CALNDRHOLIDAYIND CHAR:=null
 ,p_CALNDRKEYDT DATE:=null
 ,p_CALNDRPAYOUTIND CHAR:=null
 ,p_CALNDRREMITTANCEIND CHAR:=null
 ,p_CALNDRSUSPROLLIND CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:33:11
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:33:11
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVGRGCUPDTSP performs UPDATE on STGCSA.SOFVGRGCTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CALNDRKEYDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVGRGCTBL%rowtype;
 new_rec STGCSA.SOFVGRGCTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVGRGCTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVGRGCUPDTSP build 2018-01-23 08:33:11 '
 ||'Select of STGCSA.SOFVGRGCTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVGRGCUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CALNDRKEYDT is not null then
 cur_col_name:='P_CALNDRKEYDT';
 new_rec.CALNDRKEYDT:=P_CALNDRKEYDT; 
 end if;
 if P_CALNDRDAY is not null then
 cur_col_name:='P_CALNDRDAY';
 new_rec.CALNDRDAY:=P_CALNDRDAY; 
 end if;
 if P_CALNDRHOLIDAYIND is not null then
 cur_col_name:='P_CALNDRHOLIDAYIND';
 new_rec.CALNDRHOLIDAYIND:=P_CALNDRHOLIDAYIND; 
 end if;
 if P_CALNDRREMITTANCEIND is not null then
 cur_col_name:='P_CALNDRREMITTANCEIND';
 new_rec.CALNDRREMITTANCEIND:=P_CALNDRREMITTANCEIND; 
 end if;
 if P_CALNDRCUTOFFIND is not null then
 cur_col_name:='P_CALNDRCUTOFFIND';
 new_rec.CALNDRCUTOFFIND:=P_CALNDRCUTOFFIND; 
 end if;
 if P_CALNDRSUSPROLLIND is not null then
 cur_col_name:='P_CALNDRSUSPROLLIND';
 new_rec.CALNDRSUSPROLLIND:=P_CALNDRSUSPROLLIND; 
 end if;
 if P_CALNDRPAYOUTIND is not null then
 cur_col_name:='P_CALNDRPAYOUTIND';
 new_rec.CALNDRPAYOUTIND:=P_CALNDRPAYOUTIND; 
 end if;
 if P_CALNDRDESC is not null then
 cur_col_name:='P_CALNDRDESC';
 new_rec.CALNDRDESC:=P_CALNDRDESC; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVGRGCAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVGRGCTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CALNDRDAY=new_rec.CALNDRDAY
 ,CALNDRHOLIDAYIND=new_rec.CALNDRHOLIDAYIND
 ,CALNDRREMITTANCEIND=new_rec.CALNDRREMITTANCEIND
 ,CALNDRCUTOFFIND=new_rec.CALNDRCUTOFFIND
 ,CALNDRSUSPROLLIND=new_rec.CALNDRSUSPROLLIND
 ,CALNDRPAYOUTIND=new_rec.CALNDRPAYOUTIND
 ,CALNDRDESC=new_rec.CALNDRDESC
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVGRGCUPDTSP build 2018-01-23 08:33:11 '
 ||'while doing update on STGCSA.SOFVGRGCTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVGRGCUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVGRGCUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVGRGCTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CALNDRKEYDT)=('||P_CALNDRKEYDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVGRGCTBL where 1=1
 and CALNDRKEYDT=P_CALNDRKEYDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVGRGCUPDTSP build 2018-01-23 08:33:11 '
 ||'No STGCSA.SOFVGRGCTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVGRGCUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVGRGCUPDTSP build 2018-01-23 08:33:11 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVGRGCUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVGRGCUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVGRGCUPDTSP build 2018-01-23 08:33:11'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVGRGCUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVGRGCUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


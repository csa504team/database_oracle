<!--- Saved 01/28/2018 14:41:34. --->
PROCEDURE CHRTOPNGNTYSALLUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCRINTONDELNQLOANAMT NUMBER:=null
 ,p_ACCRINTPAIDDT DATE:=null
 ,p_CHRTOPNGNTYSALLID NUMBER:=null
 ,p_ERROPENGNTYAMT NUMBER:=null
 ,p_GNTYAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_OPENGNTYCLSDT DATE:=null
 ,p_OPENGNTYDIFFAMT NUMBER:=null
 ,p_OPENGNTYPAIDDT DATE:=null
 ,p_OPENGNTYVALIDATION NUMBER:=null
 ,p_OPNGNTYSCMNT VARCHAR2:=null
 ,p_PREPAYDT DATE:=null
 ,p_PROOFAMT NUMBER:=null
 ,p_REMAINDEROPENGNTYAMT NUMBER:=null
 ,p_REPORTDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_TOBECLSFLAG NUMBER:=null
 ,p_TOBEPAIDFLAG NUMBER:=null
 ,p_TOTOPENGNTYAMT NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_UPDTACCRINTDELNQLOANAMT NUMBER:=null
 ,p_UPDTUNALLOCPORTOFGUARANTEAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:17:10
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:17:10
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CHRTOPNGNTYSALLUPDTSP performs UPDATE on STGCSA.CHRTOPNGNTYSALLTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CHRTOPNGNTYSALLID
 for P_IDENTIFIER=1 qualification is on LOANNMB
 for P_IDENTIFIER=2 qualification is on LOANNMB, GNTYAMT, REPORTDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CHRTOPNGNTYSALLTBL%rowtype;
 new_rec STGCSA.CHRTOPNGNTYSALLTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CHRTOPNGNTYSALLTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'Select of STGCSA.CHRTOPNGNTYSALLTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CHRTOPNGNTYSALLID is not null then
 cur_col_name:='P_CHRTOPNGNTYSALLID';
 new_rec.CHRTOPNGNTYSALLID:=P_CHRTOPNGNTYSALLID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_PREPAYDT is not null then
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_SEMIANDT is not null then
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_TOTOPENGNTYAMT is not null then
 cur_col_name:='P_TOTOPENGNTYAMT';
 new_rec.TOTOPENGNTYAMT:=P_TOTOPENGNTYAMT; 
 end if;
 if P_ERROPENGNTYAMT is not null then
 cur_col_name:='P_ERROPENGNTYAMT';
 new_rec.ERROPENGNTYAMT:=P_ERROPENGNTYAMT; 
 end if;
 if P_REMAINDEROPENGNTYAMT is not null then
 cur_col_name:='P_REMAINDEROPENGNTYAMT';
 new_rec.REMAINDEROPENGNTYAMT:=P_REMAINDEROPENGNTYAMT; 
 end if;
 if P_OPENGNTYDIFFAMT is not null then
 cur_col_name:='P_OPENGNTYDIFFAMT';
 new_rec.OPENGNTYDIFFAMT:=P_OPENGNTYDIFFAMT; 
 end if;
 if P_PROOFAMT is not null then
 cur_col_name:='P_PROOFAMT';
 new_rec.PROOFAMT:=P_PROOFAMT; 
 end if;
 if P_OPNGNTYSCMNT is not null then
 cur_col_name:='P_OPNGNTYSCMNT';
 new_rec.OPNGNTYSCMNT:=P_OPNGNTYSCMNT; 
 end if;
 if P_OPENGNTYCLSDT is not null then
 cur_col_name:='P_OPENGNTYCLSDT';
 new_rec.OPENGNTYCLSDT:=P_OPENGNTYCLSDT; 
 end if;
 if P_OPENGNTYPAIDDT is not null then
 cur_col_name:='P_OPENGNTYPAIDDT';
 new_rec.OPENGNTYPAIDDT:=P_OPENGNTYPAIDDT; 
 end if;
 if P_TOBECLSFLAG is not null then
 cur_col_name:='P_TOBECLSFLAG';
 new_rec.TOBECLSFLAG:=P_TOBECLSFLAG; 
 end if;
 if P_TOBEPAIDFLAG is not null then
 cur_col_name:='P_TOBEPAIDFLAG';
 new_rec.TOBEPAIDFLAG:=P_TOBEPAIDFLAG; 
 end if;
 if P_GNTYAMT is not null then
 cur_col_name:='P_GNTYAMT';
 new_rec.GNTYAMT:=P_GNTYAMT; 
 end if;
 if P_OPENGNTYVALIDATION is not null then
 cur_col_name:='P_OPENGNTYVALIDATION';
 new_rec.OPENGNTYVALIDATION:=P_OPENGNTYVALIDATION; 
 end if;
 if P_UNALLOCAMT is not null then
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_UPDTUNALLOCPORTOFGUARANTEAMT is not null then
 cur_col_name:='P_UPDTUNALLOCPORTOFGUARANTEAMT';
 new_rec.UPDTUNALLOCPORTOFGUARANTEAMT:=P_UPDTUNALLOCPORTOFGUARANTEAMT; 
 end if;
 if P_ACCRINTONDELNQLOANAMT is not null then
 cur_col_name:='P_ACCRINTONDELNQLOANAMT';
 new_rec.ACCRINTONDELNQLOANAMT:=P_ACCRINTONDELNQLOANAMT; 
 end if;
 if P_UPDTACCRINTDELNQLOANAMT is not null then
 cur_col_name:='P_UPDTACCRINTDELNQLOANAMT';
 new_rec.UPDTACCRINTDELNQLOANAMT:=P_UPDTACCRINTDELNQLOANAMT; 
 end if;
 if P_ACCRINTPAIDDT is not null then
 cur_col_name:='P_ACCRINTPAIDDT';
 new_rec.ACCRINTPAIDDT:=P_ACCRINTPAIDDT; 
 end if;
 if P_REPORTDT is not null then
 cur_col_name:='P_REPORTDT';
 new_rec.REPORTDT:=P_REPORTDT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CHRTOPNGNTYSALLAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CHRTOPNGNTYSALLTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,PREPAYDT=new_rec.PREPAYDT
 ,SEMIANDT=new_rec.SEMIANDT
 ,TOTOPENGNTYAMT=new_rec.TOTOPENGNTYAMT
 ,ERROPENGNTYAMT=new_rec.ERROPENGNTYAMT
 ,REMAINDEROPENGNTYAMT=new_rec.REMAINDEROPENGNTYAMT
 ,OPENGNTYDIFFAMT=new_rec.OPENGNTYDIFFAMT
 ,PROOFAMT=new_rec.PROOFAMT
 ,OPNGNTYSCMNT=new_rec.OPNGNTYSCMNT
 ,OPENGNTYCLSDT=new_rec.OPENGNTYCLSDT
 ,OPENGNTYPAIDDT=new_rec.OPENGNTYPAIDDT
 ,TOBECLSFLAG=new_rec.TOBECLSFLAG
 ,TOBEPAIDFLAG=new_rec.TOBEPAIDFLAG
 ,GNTYAMT=new_rec.GNTYAMT
 ,OPENGNTYVALIDATION=new_rec.OPENGNTYVALIDATION
 ,UNALLOCAMT=new_rec.UNALLOCAMT
 ,UPDTUNALLOCPORTOFGUARANTEAMT=new_rec.UPDTUNALLOCPORTOFGUARANTEAMT
 ,ACCRINTONDELNQLOANAMT=new_rec.ACCRINTONDELNQLOANAMT
 ,UPDTACCRINTDELNQLOANAMT=new_rec.UPDTACCRINTDELNQLOANAMT
 ,ACCRINTPAIDDT=new_rec.ACCRINTPAIDDT
 ,REPORTDT=new_rec.REPORTDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'while doing update on STGCSA.CHRTOPNGNTYSALLTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CHRTOPNGNTYSALLUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CHRTOPNGNTYSALLID)=('||P_CHRTOPNGNTYSALLID||') ';
 select rowid into rec_rowid from STGCSA.CHRTOPNGNTYSALLTBL where 1=1
 and CHRTOPNGNTYSALLID=P_CHRTOPNGNTYSALLID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'No STGCSA.CHRTOPNGNTYSALLTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r1 in (select rowid from STGCSA.CHRTOPNGNTYSALLTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'Oracle returned error fetching STGCSA.CHRTOPNGNTYSALLTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'No STGCSA.CHRTOPNGNTYSALLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(GNTYAMT)=('||P_GNTYAMT||') '||
 '(LOANNMB)=('||P_LOANNMB||') '||
 '(REPORTDT)=('||P_REPORTDT||') '||'';
 begin
 for r2 in (select rowid from STGCSA.CHRTOPNGNTYSALLTBL a where 1=1 
 and GNTYAMT=P_GNTYAMT
 and LOANNMB=P_LOANNMB
 and REPORTDT=P_REPORTDT
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'Oracle returned error fetching STGCSA.CHRTOPNGNTYSALLTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'No STGCSA.CHRTOPNGNTYSALLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CHRTOPNGNTYSALLUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CHRTOPNGNTYSALLUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In CHRTOPNGNTYSALLUPDTSP build 2018-01-23 08:17:10'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CHRTOPNGNTYSALLUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


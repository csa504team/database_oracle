<!--- Saved 01/28/2018 14:42:09. --->
PROCEDURE IMPTARPRPTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCTNM VARCHAR2:=null
 ,p_ACCTNMB VARCHAR2:=null
 ,p_ACCTTYP VARCHAR2:=null
 ,p_ASOFDT DATE:=null
 ,p_BNKID VARCHAR2:=null
 ,p_BNKNM VARCHAR2:=null
 ,p_BNKSTCD CHAR:=null
 ,p_CHKAMT NUMBER:=null
 ,p_CURCY VARCHAR2:=null
 ,p_DRCRDTCD VARCHAR2:=null
 ,p_ENDDT DATE:=null
 ,p_IMPTARPRPTID NUMBER:=null
 ,p_ISSDT DATE:=null
 ,p_OPTINFOTRANDESC VARCHAR2:=null
 ,p_POSTEDDT DATE:=null
 ,p_RLSEDT DATE:=null
 ,p_RVRSDDT DATE:=null
 ,p_SCTNRPTNM VARCHAR2:=null
 ,p_SRLNMBREFNMB VARCHAR2:=null
 ,p_STARTDT DATE:=null
 ,p_STOPDT DATE:=null
 ,p_TRANSTYP VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:26:09
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:26:09
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure IMPTARPRPTUPDTSP performs UPDATE on STGCSA.IMPTARPRPTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: IMPTARPRPTID
 for P_IDENTIFIER=1 qualification is on IMPTARPRPTID like pctsign
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.IMPTARPRPTTBL%rowtype;
 new_rec STGCSA.IMPTARPRPTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.IMPTARPRPTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'Select of STGCSA.IMPTARPRPTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO IMPTARPRPTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_IMPTARPRPTID is not null then
 cur_col_name:='P_IMPTARPRPTID';
 new_rec.IMPTARPRPTID:=P_IMPTARPRPTID; 
 end if;
 if P_STARTDT is not null then
 cur_col_name:='P_STARTDT';
 new_rec.STARTDT:=P_STARTDT; 
 end if;
 if P_ENDDT is not null then
 cur_col_name:='P_ENDDT';
 new_rec.ENDDT:=P_ENDDT; 
 end if;
 if P_BNKID is not null then
 cur_col_name:='P_BNKID';
 new_rec.BNKID:=P_BNKID; 
 end if;
 if P_BNKNM is not null then
 cur_col_name:='P_BNKNM';
 new_rec.BNKNM:=P_BNKNM; 
 end if;
 if P_BNKSTCD is not null then
 cur_col_name:='P_BNKSTCD';
 new_rec.BNKSTCD:=P_BNKSTCD; 
 end if;
 if P_ACCTNMB is not null then
 cur_col_name:='P_ACCTNMB';
 new_rec.ACCTNMB:=P_ACCTNMB; 
 end if;
 if P_ACCTTYP is not null then
 cur_col_name:='P_ACCTTYP';
 new_rec.ACCTTYP:=P_ACCTTYP; 
 end if;
 if P_ACCTNM is not null then
 cur_col_name:='P_ACCTNM';
 new_rec.ACCTNM:=P_ACCTNM; 
 end if;
 if P_CURCY is not null then
 cur_col_name:='P_CURCY';
 new_rec.CURCY:=P_CURCY; 
 end if;
 if P_SCTNRPTNM is not null then
 cur_col_name:='P_SCTNRPTNM';
 new_rec.SCTNRPTNM:=P_SCTNRPTNM; 
 end if;
 if P_TRANSTYP is not null then
 cur_col_name:='P_TRANSTYP';
 new_rec.TRANSTYP:=P_TRANSTYP; 
 end if;
 if P_SRLNMBREFNMB is not null then
 cur_col_name:='P_SRLNMBREFNMB';
 new_rec.SRLNMBREFNMB:=P_SRLNMBREFNMB; 
 end if;
 if P_CHKAMT is not null then
 cur_col_name:='P_CHKAMT';
 new_rec.CHKAMT:=P_CHKAMT; 
 end if;
 if P_ISSDT is not null then
 cur_col_name:='P_ISSDT';
 new_rec.ISSDT:=P_ISSDT; 
 end if;
 if P_POSTEDDT is not null then
 cur_col_name:='P_POSTEDDT';
 new_rec.POSTEDDT:=P_POSTEDDT; 
 end if;
 if P_STOPDT is not null then
 cur_col_name:='P_STOPDT';
 new_rec.STOPDT:=P_STOPDT; 
 end if;
 if P_RLSEDT is not null then
 cur_col_name:='P_RLSEDT';
 new_rec.RLSEDT:=P_RLSEDT; 
 end if;
 if P_ASOFDT is not null then
 cur_col_name:='P_ASOFDT';
 new_rec.ASOFDT:=P_ASOFDT; 
 end if;
 if P_RVRSDDT is not null then
 cur_col_name:='P_RVRSDDT';
 new_rec.RVRSDDT:=P_RVRSDDT; 
 end if;
 if P_DRCRDTCD is not null then
 cur_col_name:='P_DRCRDTCD';
 new_rec.DRCRDTCD:=P_DRCRDTCD; 
 end if;
 if P_OPTINFOTRANDESC is not null then
 cur_col_name:='P_OPTINFOTRANDESC';
 new_rec.OPTINFOTRANDESC:=P_OPTINFOTRANDESC; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.IMPTARPRPTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.IMPTARPRPTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,STARTDT=new_rec.STARTDT
 ,ENDDT=new_rec.ENDDT
 ,BNKID=new_rec.BNKID
 ,BNKNM=new_rec.BNKNM
 ,BNKSTCD=new_rec.BNKSTCD
 ,ACCTNMB=new_rec.ACCTNMB
 ,ACCTTYP=new_rec.ACCTTYP
 ,ACCTNM=new_rec.ACCTNM
 ,CURCY=new_rec.CURCY
 ,SCTNRPTNM=new_rec.SCTNRPTNM
 ,TRANSTYP=new_rec.TRANSTYP
 ,SRLNMBREFNMB=new_rec.SRLNMBREFNMB
 ,CHKAMT=new_rec.CHKAMT
 ,ISSDT=new_rec.ISSDT
 ,POSTEDDT=new_rec.POSTEDDT
 ,STOPDT=new_rec.STOPDT
 ,RLSEDT=new_rec.RLSEDT
 ,ASOFDT=new_rec.ASOFDT
 ,RVRSDDT=new_rec.RVRSDDT
 ,DRCRDTCD=new_rec.DRCRDTCD
 ,OPTINFOTRANDESC=new_rec.OPTINFOTRANDESC
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'while doing update on STGCSA.IMPTARPRPTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO IMPTARPRPTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint IMPTARPRPTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(IMPTARPRPTID)=('||P_IMPTARPRPTID||') ';
 select rowid into rec_rowid from STGCSA.IMPTARPRPTTBL where 1=1
 and IMPTARPRPTID=P_IMPTARPRPTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'No STGCSA.IMPTARPRPTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(IMPTARPRPTID like pctsign) '||'';
 begin
 for r1 in (select rowid from STGCSA.IMPTARPRPTTBL a where 1=1 
 
 and IMPTARPRPTID like pctsign
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO IMPTARPRPTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'Oracle returned error fetching STGCSA.IMPTARPRPTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO IMPTARPRPTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'No STGCSA.IMPTARPRPTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO IMPTARPRPTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End IMPTARPRPTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'IMPTARPRPTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In IMPTARPRPTUPDTSP build 2018-01-23 08:26:09'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO IMPTARPRPTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END IMPTARPRPTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


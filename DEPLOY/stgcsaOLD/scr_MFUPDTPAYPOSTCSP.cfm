<!--- Saved 01/28/2018 14:42:12. --->
PROCEDURE MFUPDTPAYPOSTCSP (
 p_taskname VARCHAR2 :='',
 P_CREATUSERID VARCHAR2:=NULL,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2)
 as
 ---p1 NUMBER; p2 NUMBER; p3 VARCHAR2(200);
 v_sysdate date := sysdate;
 v_status char(1);
 v_lastupdtdt date;
 BEGIN 
 p_errval := 0;
 p_retval := -1000;
 p_errmsg := 'Success';
 
 SELECT stgcsa.isProcessAllowed( p_taskname ) into v_status from dual;
 
 IF v_status != 'Y' THEN
 p_errval := -22000;
 p_errmsg := 'Cannot perform MF Update for [' || Upper(p_taskname) || '] at this time';
 DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
 RETURN;
 END IF;
 
 SELECT NVL(MAX(taskenddt), to_date('2017-10-25', 'yyyy-mm-dd')) into v_lastupdtdt from STGCSA.mfjobdetailtbl where jobtypefk = 10;
 
 taskstartcsp(p_taskname, p_creatuserid, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
 DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
 
 /* here is the main update/merge statement */
 MERGE INTO stgCSA.SOFVFTPOTBL ORIG
 USING
 (
 SELECT
 --trn.transid,
 --trn.transtypid,
 trn.loannmb,
 trn.creatuserid,
 trn.creatdt,
 trn.lastupdtuserid,
 trn.lastupdtdt,
 --att1.attrval as pymttyp,
 att2.attrval as pymtamt,
 --att1.lastupdtdt as pymttyp_lastupdtdt,
 --att2.lastupdtdt as pymtamt_lastupdtdt,
 --att1.lastupdtuserid as pymttyp_lastupdtuserid,
 --att2.lastupdtuserid as pymtamt_lastupdtuserid,
 DECODE(att1.attrval, 1, 'C', 2, 'W', 3, 'A', 4, 'C', 5, 'W', 6, 'M') as PYMTTYPABBR
 --refp.PYMTTYPABBR
 FROM (SELECT DISTINCT transid FROM STGCSA.coretransattrtbl WHERE attrid IN (20)) X
 JOIN coretranstbl trn ON x.transid = trn.transid AND trn.LOANNMB IS NOT NULL AND trn.STATIDCUR = 19
 LEFT OUTER JOIN coretransattrtbl att1 ON trn.transid = att1.transid AND att1.attrid = 19
 LEFT OUTER JOIN coretransattrtbl att2 ON trn.transid = att2.transid AND att2.attrid = 20
 ---JOIN refpymttyptbl refp on att1.attrval = refp.pymttypid and refp.pymttypid not in (4,5)
 ) TMP ON (ORIG.LOANNMB = TMP.loannmb) 
 WHEN MATCHED THEN UPDATE SET 
 ORIG.PREPOSTRVWRECRDTYPCD = TMP.PYMTTYPABBR, 
 ORIG.PREPOSTRVWPYMTAMT = TMP.pymtamt, 
 ORIG.LASTUPDTUSERID = TMP.lastupdtuserid,
 ORIG.LASTUPDTDT = TMP.lastupdtdt
 WHEN NOT MATCHED THEN INSERT (PREPOSTRVWRECRDTYPCD,PREPOSTRVWTRANSCD,PREPOSTRVWTRANSITROUTINGNMB,PREPOSTRVWTRANSITROUTINGCHKDGT,PREPOSTRVWBNKACCTNMB,PREPOSTRVWPYMTAMT,LOANNMB,PREPOSTRVWINDVLNM,PREPOSTRVWCOMPBNKDISC,PREPOSTRVWADDENDRECIND,PREPOSTRVWTRACENMB,PREPOSTRVWCMNT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) 
 VALUES (TMP.PYMTTYPABBR,'00 ','00000000 ','0',' ',TMP.pymtamt,TMP.loannmb,' ',' ',0,0,' ',TMP.creatuserid,TMP.creatdt,TMP.lastupdtuserid,TMP.lastupdtdt);
 /* end of the main merge */
 
 MERGE INTO stgCSA.CORETRANSSTATTBL ORIG
 USING
 (
 SELECT
 trn.transid,
 --trn.transtypid,
 trn.loannmb,
 trn.creatuserid,
 trn.creatdt,
 trn.lastupdtuserid,
 trn.lastupdtdt
 FROM coretranstbl trn 
 JOIN stgCSA.SOFVFTPOTBL FTPO ON FTPO.LOANNMB = trn.loannmb
 WHERE trn.STATIDCUR = 19
 ) TMP ON (ORIG.TRANSID = TMP.transid AND ORIG.STATID = 25) 
 WHEN MATCHED THEN UPDATE SET 
 ORIG.LASTUPDTUSERID = TMP.lastupdtuserid,
 ORIG.LASTUPDTDT = TMP.lastupdtdt
 WHEN NOT MATCHED THEN INSERT (TRANSID,STATID,TIMESTAMPFLD,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) 
 VALUES (TMP.transid,25,TMP.creatdt,TMP.creatuserid,TMP.creatdt,TMP.lastupdtuserid,TMP.lastupdtdt);
 
 MERGE INTO stgCSA.CORETRANSTBL ORIG
 USING
 (
 SELECT
 trn.transid,
 --trn.transtypid,
 trn.loannmb,
 trn.creatuserid,
 trn.creatdt,
 trn.lastupdtuserid,
 trn.lastupdtdt
 FROM coretranstbl trn 
 JOIN stgCSA.SOFVFTPOTBL FTPO ON FTPO.LOANNMB = trn.loannmb
 WHERE trn.STATIDCUR = 19
 ) TMP ON (ORIG.TRANSID = TMP.transid) 
 WHEN MATCHED THEN UPDATE SET
 ORIG.STATIDCUR = 25,
 ORIG.LASTUPDTUSERID = TMP.lastupdtuserid,
 ORIG.LASTUPDTDT = TMP.lastupdtdt;
 
 TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
 DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg ); /* close it any way */
 
 --RAISE;
 END MFUPDTPAYPOSTCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


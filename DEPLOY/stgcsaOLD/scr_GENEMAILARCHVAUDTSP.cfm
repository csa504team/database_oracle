<!--- Saved 01/28/2018 14:41:57. --->
PROCEDURE GENEMAILARCHVAUDTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2,
 action_cd char,
 AUDTEDTCD char,
 old_rec STGCSA.GENEMAILARCHVTBL%rowtype,
 new_rec STGCSA.GENEMAILARCHVTBL%rowtype) as
 /*
 Created on: 2018-01-23 08:23:19
 Created by: GENR
 Crerated from template auditme_template v2.02 20 Dec 2017 on 2018-01-23 08:23:19
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Purpose : Write to SOFVAUDTTBL for specific changed columns in
 STGCSA.GENEMAILARCHVTBL (GENEMAILARCHVTBL)
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 aud_rec STGCSA.SOFVAUDTTBL%rowtype;
 n number:=0;
 e number:=0;
 tsp timestamp;
 key_from_new_rec STGCSA.SOFVAUDTTBL.AUDTRECRDKEY%type;
 begin
 savepoint GENEMAILARCHVAUDTSP;
 -- hopeful return
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=null;
 -- setup log rec
 select systimestamp,sysdate,sys_context('userenv','session_user')
 into aud_rec.audttransdt, aud_rec.CREATDT, aud_rec.creatuserid from dual;
 aud_rec.LASTUPDTDT:=aud_rec.CREATDT;
 aud_rec.lastupdtuserid:=aud_rec.creatuserid;
 aud_rec.audtseqnmb:=0;
 aud_rec.AUDTOPRTRINITIAL:=' ';
 aud_rec.AUDTTRMNLID:=' ';
 aud_rec.AUDTACTNCD:=action_cd;
 aud_rec.AUDTEDTCD:=AUDTEDTCD;
 aud_rec.AUDTUSERID:=p_userid;
 -- save key value... if add or del then one rec is nulls
 aud_rec.audtrecrdkey:=rpad(substr(''
 ||to_char(old_rec.EMAILID),1,15),15);
 key_from_new_rec:=rpad(substr(''
 ||to_char(new_rec.EMAILID),1,15),15);
 if aud_rec.audtrecrdkey is null then
 aud_rec.audtrecrdkey:=key_from_new_rec;
 end if;
 -- are any columns auditing cares about different?
 p_retval:=aud_rec.audtseqnmb;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:='In OXH in GENEMAILARCHVAUDTSP Build 2018-01-23 08:23:19 got: '||sqlerrm(SQLCODE);
 ROLLBACK TO GENEMAILARCHVAUDTSP;
 --
 END GENEMAILARCHVAUDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:21. --->
PROCEDURE PRPMFWIREPOSTINGUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACHDEL NUMBER:=null
 ,p_ADJTOAPPLINTAMT NUMBER:=null
 ,p_ADJTOAPPLPRINAMT NUMBER:=null
 ,p_ADJTYP VARCHAR2:=null
 ,p_BORRFEEAMT NUMBER:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_CDCDISBAMT NUMBER:=null
 ,p_CDCFEEAPPLAMT NUMBER:=null
 ,p_CDCFEEDISBAMT NUMBER:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_CDCREPDAMT NUMBER:=null
 ,p_CSAFEEAMT NUMBER:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CSAREPDAMT NUMBER:=null
 ,p_CURBALAMT NUMBER:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_LATEFEEADJAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_MFSTATCD VARCHAR2:=null
 ,p_OVRPAYAMT NUMBER:=null
 ,p_OVRWRITECD VARCHAR2:=null
 ,p_POSTINGACTUALDT DATE:=null
 ,p_POSTINGSCHDLDT DATE:=null
 ,p_POSTINGTYP VARCHAR2:=null
 ,p_PREPAYAMT NUMBER:=null
 ,p_PREPAYDT DATE:=null
 ,p_REPDAMT NUMBER:=null
 ,p_SEMIANACCRINTONDELAMT NUMBER:=null
 ,p_SEMIANAMT NUMBER:=null
 ,p_SEMIANCDCACCRAMT NUMBER:=null
 ,p_SEMIANCDCBEHINDAMT NUMBER:=null
 ,p_SEMIANCDCDUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANCDCNMB CHAR:=null
 ,p_SEMIANCSAACCRAMT NUMBER:=null
 ,p_SEMIANCSABEHINDAMT NUMBER:=null
 ,p_SEMIANCSADUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANCSAPRJCTDAMT NUMBER:=null
 ,p_SEMIANDBENTRBALAMT NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ,p_SEMIANESCROWPRIORPYMTAMT NUMBER:=null
 ,p_SEMIANINTACCRAMT NUMBER:=null
 ,p_SEMIANLATEFEEAMT NUMBER:=null
 ,p_SEMIANNOTEBALAMT NUMBER:=null
 ,p_SEMIANOPENGNTYAMT NUMBER:=null
 ,p_SEMIANOPENGNTYERRAMT NUMBER:=null
 ,p_SEMIANOPENGNTYREMAINAMT NUMBER:=null
 ,p_SEMIANPIPYMTAFT6MOAMT NUMBER:=null
 ,p_SEMIANPISHORTOVRAMT NUMBER:=null
 ,p_SEMIANPREPAYPREMAMT NUMBER:=null
 ,p_SEMIANPRINACCRAMT NUMBER:=null
 ,p_SEMIANPROOFAMT NUMBER:=null
 ,p_SEMIANSBAACCRAMT NUMBER:=null
 ,p_SEMIANSBABEHINDAMT NUMBER:=null
 ,p_SEMIANSBADUEFROMBORRAMT NUMBER:=null
 ,p_SEMIANSBAPRJCTDAMT NUMBER:=null
 ,p_SEMIANWIREAMT NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_WIRECMNT VARCHAR2:=null
 ,p_WIREPOSTINGID NUMBER:=null
 ,p_ZEROBALAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:28:50
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:28:50
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPMFWIREPOSTINGUPDTSP performs UPDATE on STGCSA.PRPMFWIREPOSTINGTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: WIREPOSTINGID
 for P_IDENTIFIER=1 qualification is on TRANSID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPMFWIREPOSTINGTBL%rowtype;
 new_rec STGCSA.PRPMFWIREPOSTINGTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPMFWIREPOSTINGTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'Select of STGCSA.PRPMFWIREPOSTINGTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_BORRFEEAMT is not null then
 cur_col_name:='P_BORRFEEAMT';
 new_rec.BORRFEEAMT:=P_BORRFEEAMT; 
 end if;
 if P_BORRPAIDTHRUDT is not null then
 cur_col_name:='P_BORRPAIDTHRUDT';
 new_rec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT; 
 end if;
 if P_CSAFEEAMT is not null then
 cur_col_name:='P_CSAFEEAMT';
 new_rec.CSAFEEAMT:=P_CSAFEEAMT; 
 end if;
 if P_CSAPAIDTHRUDT is not null then
 cur_col_name:='P_CSAPAIDTHRUDT';
 new_rec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT; 
 end if;
 if P_CDCFEEAPPLAMT is not null then
 cur_col_name:='P_CDCFEEAPPLAMT';
 new_rec.CDCFEEAPPLAMT:=P_CDCFEEAPPLAMT; 
 end if;
 if P_CDCFEEDISBAMT is not null then
 cur_col_name:='P_CDCFEEDISBAMT';
 new_rec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT; 
 end if;
 if P_CDCDISBAMT is not null then
 cur_col_name:='P_CDCDISBAMT';
 new_rec.CDCDISBAMT:=P_CDCDISBAMT; 
 end if;
 if P_CDCPAIDTHRUDT is not null then
 cur_col_name:='P_CDCPAIDTHRUDT';
 new_rec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT; 
 end if;
 if P_INTPAIDTHRUDT is not null then
 cur_col_name:='P_INTPAIDTHRUDT';
 new_rec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT; 
 end if;
 if P_ADJTOAPPLINTAMT is not null then
 cur_col_name:='P_ADJTOAPPLINTAMT';
 new_rec.ADJTOAPPLINTAMT:=P_ADJTOAPPLINTAMT; 
 end if;
 if P_ADJTOAPPLPRINAMT is not null then
 cur_col_name:='P_ADJTOAPPLPRINAMT';
 new_rec.ADJTOAPPLPRINAMT:=P_ADJTOAPPLPRINAMT; 
 end if;
 if P_LATEFEEADJAMT is not null then
 cur_col_name:='P_LATEFEEADJAMT';
 new_rec.LATEFEEADJAMT:=P_LATEFEEADJAMT; 
 end if;
 if P_UNALLOCAMT is not null then
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_CURBALAMT is not null then
 cur_col_name:='P_CURBALAMT';
 new_rec.CURBALAMT:=P_CURBALAMT; 
 end if;
 if P_PREPAYAMT is not null then
 cur_col_name:='P_PREPAYAMT';
 new_rec.PREPAYAMT:=P_PREPAYAMT; 
 end if;
 if P_WIRECMNT is not null then
 cur_col_name:='P_WIRECMNT';
 new_rec.WIRECMNT:=P_WIRECMNT; 
 end if;
 if P_ADJTYP is not null then
 cur_col_name:='P_ADJTYP';
 new_rec.ADJTYP:=P_ADJTYP; 
 end if;
 if P_OVRWRITECD is not null then
 cur_col_name:='P_OVRWRITECD';
 new_rec.OVRWRITECD:=P_OVRWRITECD; 
 end if;
 if P_POSTINGTYP is not null then
 cur_col_name:='P_POSTINGTYP';
 new_rec.POSTINGTYP:=P_POSTINGTYP; 
 end if;
 if P_POSTINGSCHDLDT is not null then
 cur_col_name:='P_POSTINGSCHDLDT';
 new_rec.POSTINGSCHDLDT:=P_POSTINGSCHDLDT; 
 end if;
 if P_POSTINGACTUALDT is not null then
 cur_col_name:='P_POSTINGACTUALDT';
 new_rec.POSTINGACTUALDT:=P_POSTINGACTUALDT; 
 end if;
 if P_WIREPOSTINGID is not null then
 cur_col_name:='P_WIREPOSTINGID';
 new_rec.WIREPOSTINGID:=P_WIREPOSTINGID; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_MFSTATCD is not null then
 cur_col_name:='P_MFSTATCD';
 new_rec.MFSTATCD:=P_MFSTATCD; 
 end if;
 if P_ACHDEL is not null then
 cur_col_name:='P_ACHDEL';
 new_rec.ACHDEL:=P_ACHDEL; 
 end if;
 if P_OVRPAYAMT is not null then
 cur_col_name:='P_OVRPAYAMT';
 new_rec.OVRPAYAMT:=P_OVRPAYAMT; 
 end if;
 if P_CDCREPDAMT is not null then
 cur_col_name:='P_CDCREPDAMT';
 new_rec.CDCREPDAMT:=P_CDCREPDAMT; 
 end if;
 if P_CSAREPDAMT is not null then
 cur_col_name:='P_CSAREPDAMT';
 new_rec.CSAREPDAMT:=P_CSAREPDAMT; 
 end if;
 if P_REPDAMT is not null then
 cur_col_name:='P_REPDAMT';
 new_rec.REPDAMT:=P_REPDAMT; 
 end if;
 if P_PREPAYDT is not null then
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_SEMIANDT is not null then
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_ZEROBALAMT is not null then
 cur_col_name:='P_ZEROBALAMT';
 new_rec.ZEROBALAMT:=P_ZEROBALAMT; 
 end if;
 if P_SEMIANCDCNMB is not null then
 cur_col_name:='P_SEMIANCDCNMB';
 new_rec.SEMIANCDCNMB:=P_SEMIANCDCNMB; 
 end if;
 if P_SEMIANNOTEBALAMT is not null then
 cur_col_name:='P_SEMIANNOTEBALAMT';
 new_rec.SEMIANNOTEBALAMT:=P_SEMIANNOTEBALAMT; 
 end if;
 if P_SEMIANWIREAMT is not null then
 cur_col_name:='P_SEMIANWIREAMT';
 new_rec.SEMIANWIREAMT:=P_SEMIANWIREAMT; 
 end if;
 if P_SEMIANPRINACCRAMT is not null then
 cur_col_name:='P_SEMIANPRINACCRAMT';
 new_rec.SEMIANPRINACCRAMT:=P_SEMIANPRINACCRAMT; 
 end if;
 if P_SEMIANINTACCRAMT is not null then
 cur_col_name:='P_SEMIANINTACCRAMT';
 new_rec.SEMIANINTACCRAMT:=P_SEMIANINTACCRAMT; 
 end if;
 if P_SEMIANOPENGNTYAMT is not null then
 cur_col_name:='P_SEMIANOPENGNTYAMT';
 new_rec.SEMIANOPENGNTYAMT:=P_SEMIANOPENGNTYAMT; 
 end if;
 if P_SEMIANOPENGNTYERRAMT is not null then
 cur_col_name:='P_SEMIANOPENGNTYERRAMT';
 new_rec.SEMIANOPENGNTYERRAMT:=P_SEMIANOPENGNTYERRAMT; 
 end if;
 if P_SEMIANOPENGNTYREMAINAMT is not null then
 cur_col_name:='P_SEMIANOPENGNTYREMAINAMT';
 new_rec.SEMIANOPENGNTYREMAINAMT:=P_SEMIANOPENGNTYREMAINAMT; 
 end if;
 if P_SEMIANPIPYMTAFT6MOAMT is not null then
 cur_col_name:='P_SEMIANPIPYMTAFT6MOAMT';
 new_rec.SEMIANPIPYMTAFT6MOAMT:=P_SEMIANPIPYMTAFT6MOAMT; 
 end if;
 if P_SEMIANPISHORTOVRAMT is not null then
 cur_col_name:='P_SEMIANPISHORTOVRAMT';
 new_rec.SEMIANPISHORTOVRAMT:=P_SEMIANPISHORTOVRAMT; 
 end if;
 if P_SEMIANESCROWPRIORPYMTAMT is not null then
 cur_col_name:='P_SEMIANESCROWPRIORPYMTAMT';
 new_rec.SEMIANESCROWPRIORPYMTAMT:=P_SEMIANESCROWPRIORPYMTAMT; 
 end if;
 if P_SEMIANDBENTRBALAMT is not null then
 cur_col_name:='P_SEMIANDBENTRBALAMT';
 new_rec.SEMIANDBENTRBALAMT:=P_SEMIANDBENTRBALAMT; 
 end if;
 if P_SEMIANPREPAYPREMAMT is not null then
 cur_col_name:='P_SEMIANPREPAYPREMAMT';
 new_rec.SEMIANPREPAYPREMAMT:=P_SEMIANPREPAYPREMAMT; 
 end if;
 if P_SEMIANAMT is not null then
 cur_col_name:='P_SEMIANAMT';
 new_rec.SEMIANAMT:=P_SEMIANAMT; 
 end if;
 if P_SEMIANSBADUEFROMBORRAMT is not null then
 cur_col_name:='P_SEMIANSBADUEFROMBORRAMT';
 new_rec.SEMIANSBADUEFROMBORRAMT:=P_SEMIANSBADUEFROMBORRAMT; 
 end if;
 if P_SEMIANSBABEHINDAMT is not null then
 cur_col_name:='P_SEMIANSBABEHINDAMT';
 new_rec.SEMIANSBABEHINDAMT:=P_SEMIANSBABEHINDAMT; 
 end if;
 if P_SEMIANSBAACCRAMT is not null then
 cur_col_name:='P_SEMIANSBAACCRAMT';
 new_rec.SEMIANSBAACCRAMT:=P_SEMIANSBAACCRAMT; 
 end if;
 if P_SEMIANSBAPRJCTDAMT is not null then
 cur_col_name:='P_SEMIANSBAPRJCTDAMT';
 new_rec.SEMIANSBAPRJCTDAMT:=P_SEMIANSBAPRJCTDAMT; 
 end if;
 if P_SEMIANCSADUEFROMBORRAMT is not null then
 cur_col_name:='P_SEMIANCSADUEFROMBORRAMT';
 new_rec.SEMIANCSADUEFROMBORRAMT:=P_SEMIANCSADUEFROMBORRAMT; 
 end if;
 if P_SEMIANCSABEHINDAMT is not null then
 cur_col_name:='P_SEMIANCSABEHINDAMT';
 new_rec.SEMIANCSABEHINDAMT:=P_SEMIANCSABEHINDAMT; 
 end if;
 if P_SEMIANCSAACCRAMT is not null then
 cur_col_name:='P_SEMIANCSAACCRAMT';
 new_rec.SEMIANCSAACCRAMT:=P_SEMIANCSAACCRAMT; 
 end if;
 if P_SEMIANCSAPRJCTDAMT is not null then
 cur_col_name:='P_SEMIANCSAPRJCTDAMT';
 new_rec.SEMIANCSAPRJCTDAMT:=P_SEMIANCSAPRJCTDAMT; 
 end if;
 if P_SEMIANCDCDUEFROMBORRAMT is not null then
 cur_col_name:='P_SEMIANCDCDUEFROMBORRAMT';
 new_rec.SEMIANCDCDUEFROMBORRAMT:=P_SEMIANCDCDUEFROMBORRAMT; 
 end if;
 if P_SEMIANCDCBEHINDAMT is not null then
 cur_col_name:='P_SEMIANCDCBEHINDAMT';
 new_rec.SEMIANCDCBEHINDAMT:=P_SEMIANCDCBEHINDAMT; 
 end if;
 if P_SEMIANCDCACCRAMT is not null then
 cur_col_name:='P_SEMIANCDCACCRAMT';
 new_rec.SEMIANCDCACCRAMT:=P_SEMIANCDCACCRAMT; 
 end if;
 if P_SEMIANACCRINTONDELAMT is not null then
 cur_col_name:='P_SEMIANACCRINTONDELAMT';
 new_rec.SEMIANACCRINTONDELAMT:=P_SEMIANACCRINTONDELAMT; 
 end if;
 if P_SEMIANLATEFEEAMT is not null then
 cur_col_name:='P_SEMIANLATEFEEAMT';
 new_rec.SEMIANLATEFEEAMT:=P_SEMIANLATEFEEAMT; 
 end if;
 if P_SEMIANPROOFAMT is not null then
 cur_col_name:='P_SEMIANPROOFAMT';
 new_rec.SEMIANPROOFAMT:=P_SEMIANPROOFAMT; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPMFWIREPOSTINGAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPMFWIREPOSTINGTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,BORRFEEAMT=new_rec.BORRFEEAMT
 ,BORRPAIDTHRUDT=new_rec.BORRPAIDTHRUDT
 ,CSAFEEAMT=new_rec.CSAFEEAMT
 ,CSAPAIDTHRUDT=new_rec.CSAPAIDTHRUDT
 ,CDCFEEAPPLAMT=new_rec.CDCFEEAPPLAMT
 ,CDCFEEDISBAMT=new_rec.CDCFEEDISBAMT
 ,CDCDISBAMT=new_rec.CDCDISBAMT
 ,CDCPAIDTHRUDT=new_rec.CDCPAIDTHRUDT
 ,INTPAIDTHRUDT=new_rec.INTPAIDTHRUDT
 ,ADJTOAPPLINTAMT=new_rec.ADJTOAPPLINTAMT
 ,ADJTOAPPLPRINAMT=new_rec.ADJTOAPPLPRINAMT
 ,LATEFEEADJAMT=new_rec.LATEFEEADJAMT
 ,UNALLOCAMT=new_rec.UNALLOCAMT
 ,CURBALAMT=new_rec.CURBALAMT
 ,PREPAYAMT=new_rec.PREPAYAMT
 ,POSTINGTYP=new_rec.POSTINGTYP
 ,WIRECMNT=new_rec.WIRECMNT
 ,ADJTYP=new_rec.ADJTYP
 ,OVRWRITECD=new_rec.OVRWRITECD
 ,POSTINGSCHDLDT=new_rec.POSTINGSCHDLDT
 ,POSTINGACTUALDT=new_rec.POSTINGACTUALDT
 ,TRANSID=new_rec.TRANSID
 ,MFSTATCD=new_rec.MFSTATCD
 ,ACHDEL=new_rec.ACHDEL
 ,OVRPAYAMT=new_rec.OVRPAYAMT
 ,CDCREPDAMT=new_rec.CDCREPDAMT
 ,CSAREPDAMT=new_rec.CSAREPDAMT
 ,REPDAMT=new_rec.REPDAMT
 ,PREPAYDT=new_rec.PREPAYDT
 ,SEMIANDT=new_rec.SEMIANDT
 ,ZEROBALAMT=new_rec.ZEROBALAMT
 ,SEMIANCDCNMB=new_rec.SEMIANCDCNMB
 ,SEMIANNOTEBALAMT=new_rec.SEMIANNOTEBALAMT
 ,SEMIANWIREAMT=new_rec.SEMIANWIREAMT
 ,SEMIANPRINACCRAMT=new_rec.SEMIANPRINACCRAMT
 ,SEMIANINTACCRAMT=new_rec.SEMIANINTACCRAMT
 ,SEMIANOPENGNTYAMT=new_rec.SEMIANOPENGNTYAMT
 ,SEMIANOPENGNTYERRAMT=new_rec.SEMIANOPENGNTYERRAMT
 ,SEMIANOPENGNTYREMAINAMT=new_rec.SEMIANOPENGNTYREMAINAMT
 ,SEMIANPIPYMTAFT6MOAMT=new_rec.SEMIANPIPYMTAFT6MOAMT
 ,SEMIANPISHORTOVRAMT=new_rec.SEMIANPISHORTOVRAMT
 ,SEMIANESCROWPRIORPYMTAMT=new_rec.SEMIANESCROWPRIORPYMTAMT
 ,SEMIANDBENTRBALAMT=new_rec.SEMIANDBENTRBALAMT
 ,SEMIANPREPAYPREMAMT=new_rec.SEMIANPREPAYPREMAMT
 ,SEMIANAMT=new_rec.SEMIANAMT
 ,SEMIANSBADUEFROMBORRAMT=new_rec.SEMIANSBADUEFROMBORRAMT
 ,SEMIANSBABEHINDAMT=new_rec.SEMIANSBABEHINDAMT
 ,SEMIANSBAACCRAMT=new_rec.SEMIANSBAACCRAMT
 ,SEMIANSBAPRJCTDAMT=new_rec.SEMIANSBAPRJCTDAMT
 ,SEMIANCSADUEFROMBORRAMT=new_rec.SEMIANCSADUEFROMBORRAMT
 ,SEMIANCSABEHINDAMT=new_rec.SEMIANCSABEHINDAMT
 ,SEMIANCSAACCRAMT=new_rec.SEMIANCSAACCRAMT
 ,SEMIANCSAPRJCTDAMT=new_rec.SEMIANCSAPRJCTDAMT
 ,SEMIANCDCDUEFROMBORRAMT=new_rec.SEMIANCDCDUEFROMBORRAMT
 ,SEMIANCDCBEHINDAMT=new_rec.SEMIANCDCBEHINDAMT
 ,SEMIANCDCACCRAMT=new_rec.SEMIANCDCACCRAMT
 ,SEMIANACCRINTONDELAMT=new_rec.SEMIANACCRINTONDELAMT
 ,SEMIANLATEFEEAMT=new_rec.SEMIANLATEFEEAMT
 ,SEMIANPROOFAMT=new_rec.SEMIANPROOFAMT
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'while doing update on STGCSA.PRPMFWIREPOSTINGTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFWIREPOSTINGUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(WIREPOSTINGID)=('||P_WIREPOSTINGID||') ';
 select rowid into rec_rowid from STGCSA.PRPMFWIREPOSTINGTBL where 1=1
 and WIREPOSTINGID=P_WIREPOSTINGID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'No STGCSA.PRPMFWIREPOSTINGTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.PRPMFWIREPOSTINGTBL a where 1=1 
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'Oracle returned error fetching STGCSA.PRPMFWIREPOSTINGTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'No STGCSA.PRPMFWIREPOSTINGTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB; log_TRANSID:=new_rec.TRANSID; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFWIREPOSTINGUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPMFWIREPOSTINGUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PRPMFWIREPOSTINGUPDTSP build 2018-01-23 08:28:50'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPMFWIREPOSTINGUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


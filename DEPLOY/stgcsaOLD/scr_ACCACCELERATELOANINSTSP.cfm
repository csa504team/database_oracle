<!--- Saved 01/28/2018 14:41:26. --->
PROCEDURE ACCACCELERATELOANINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCELERATNID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_TRANSID NUMBER:=null
 ,p_PRIORACCELERATN NUMBER:=null
 ,p_STATID NUMBER:=null
 ,p_ACCELERATECMNT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_MFINITIALSCRAPEDT DATE:=null
 ,p_MFLASTSCRAPEDT DATE:=null
 ,p_MFCURSTATID NUMBER:=null
 ,p_MFCURCMNT VARCHAR2:=null
 ,p_MFINITIALSTATID NUMBER:=null
 ,p_MFINITIALCMNT VARCHAR2:=null
 ,p_MFLASTUPDTDT DATE:=null
 ,p_MFISSDT DATE:=null
 ,p_MFLOANMATDT DATE:=null
 ,p_MFORGLPRINAMT NUMBER:=null
 ,p_MFINTRTPCT NUMBER:=null
 ,p_MFDBENTRAMT NUMBER:=null
 ,p_MFTOBECURAMT NUMBER:=null
 ,p_MFLASTPYMTDT DATE:=null
 ,p_MFESCROWPRIORPYMTAMT NUMBER:=null
 ,p_MFCDCPAIDTHRUDT CHAR:=null
 ,p_MFCDCMODELNQ NUMBER:=null
 ,p_MFCDCMOFEEAMT NUMBER:=null
 ,p_MFCDCFEEAMT NUMBER:=null
 ,p_MFCDCDUEFROMBORRAMT NUMBER:=null
 ,p_MFCSAPAIDTHRUDT CHAR:=null
 ,p_MFCSAMODELNQ NUMBER:=null
 ,p_MFCSAMOFEEAMT NUMBER:=null
 ,p_MFCSAFEEAMT NUMBER:=null
 ,p_MFCSADUEFROMBORRAMT NUMBER:=null
 ,p_PPAENTRYDT DATE:=null
 ,p_PPAGROUP VARCHAR2:=null
 ,p_PPACDCNMB CHAR:=null
 ,p_PPAORGLPRINAMT NUMBER:=null
 ,p_PPADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_PPAINTDUEATSEMIANAMT NUMBER:=null
 ,p_PPADUEATSEMIANAMT NUMBER:=null
 ,p_SBAENTRYDT DATE:=null
 ,p_SBALASTTRNSCRPTIMPTDT DATE:=null
 ,p_SBADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_SBAINTDUEATSEMIANAMT NUMBER:=null
 ,p_SBACSAFEEAMT NUMBER:=null
 ,p_SBACDCFEEAMT NUMBER:=null
 ,p_SBAESCROWPRIORPYMTAMT NUMBER:=null
 ,p_SBAACCELAMT NUMBER:=null
 ,p_SBADUEDT DATE:=null
 ,p_SBANOTERTPCT NUMBER:=null
 ,p_SBALASTNOTEBALAMT NUMBER:=null
 ,p_SBALASTPYMTDT DATE:=null
 ,p_SBAACCRINTDUEAMT NUMBER:=null
 ,p_SBAEXCESSAMT NUMBER:=null
 ,p_MFPREVSTATID NUMBER:=null
 ,p_MFPREVCMNT VARCHAR2:=null
 ,p_CDCNMB CHAR:=null
 ,p_SERVCNTRID VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 16:52:10
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 16:52:10
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCACCELERATELOANTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint ACCACCELERATELOANINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_ACCELERATNID';
 new_rec.ACCELERATNID:=runtime.validate_column_value(P_ACCELERATNID,
 'STGCSA','ACCACCELERATELOANTBL','ACCELERATNID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','ACCACCELERATELOANTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','ACCACCELERATELOANTBL','TRANSID','NUMBER',22);
 cur_col_name:='P_PRIORACCELERATN';
 new_rec.PRIORACCELERATN:=runtime.validate_column_value(P_PRIORACCELERATN,
 'STGCSA','ACCACCELERATELOANTBL','PRIORACCELERATN','NUMBER',22);
 cur_col_name:='P_STATID';
 new_rec.STATID:=runtime.validate_column_value(P_STATID,
 'STGCSA','ACCACCELERATELOANTBL','STATID','NUMBER',22);
 cur_col_name:='P_ACCELERATECMNT';
 new_rec.ACCELERATECMNT:=runtime.validate_column_value(P_ACCELERATECMNT,
 'STGCSA','ACCACCELERATELOANTBL','ACCELERATECMNT','VARCHAR2',1000);
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=runtime.validate_column_value(P_SEMIANDT,
 'STGCSA','ACCACCELERATELOANTBL','SEMIANDT','DATE',7);
 cur_col_name:='P_MFINITIALSCRAPEDT';
 new_rec.MFINITIALSCRAPEDT:=runtime.validate_column_value(P_MFINITIALSCRAPEDT,
 'STGCSA','ACCACCELERATELOANTBL','MFINITIALSCRAPEDT','DATE',7);
 cur_col_name:='P_MFLASTSCRAPEDT';
 new_rec.MFLASTSCRAPEDT:=runtime.validate_column_value(P_MFLASTSCRAPEDT,
 'STGCSA','ACCACCELERATELOANTBL','MFLASTSCRAPEDT','DATE',7);
 cur_col_name:='P_MFCURSTATID';
 new_rec.MFCURSTATID:=runtime.validate_column_value(P_MFCURSTATID,
 'STGCSA','ACCACCELERATELOANTBL','MFCURSTATID','NUMBER',22);
 cur_col_name:='P_MFCURCMNT';
 new_rec.MFCURCMNT:=runtime.validate_column_value(P_MFCURCMNT,
 'STGCSA','ACCACCELERATELOANTBL','MFCURCMNT','VARCHAR2',100);
 cur_col_name:='P_MFINITIALSTATID';
 new_rec.MFINITIALSTATID:=runtime.validate_column_value(P_MFINITIALSTATID,
 'STGCSA','ACCACCELERATELOANTBL','MFINITIALSTATID','NUMBER',22);
 cur_col_name:='P_MFINITIALCMNT';
 new_rec.MFINITIALCMNT:=runtime.validate_column_value(P_MFINITIALCMNT,
 'STGCSA','ACCACCELERATELOANTBL','MFINITIALCMNT','VARCHAR2',100);
 cur_col_name:='P_MFLASTUPDTDT';
 new_rec.MFLASTUPDTDT:=runtime.validate_column_value(P_MFLASTUPDTDT,
 'STGCSA','ACCACCELERATELOANTBL','MFLASTUPDTDT','DATE',7);
 cur_col_name:='P_MFISSDT';
 new_rec.MFISSDT:=runtime.validate_column_value(P_MFISSDT,
 'STGCSA','ACCACCELERATELOANTBL','MFISSDT','DATE',7);
 cur_col_name:='P_MFLOANMATDT';
 new_rec.MFLOANMATDT:=runtime.validate_column_value(P_MFLOANMATDT,
 'STGCSA','ACCACCELERATELOANTBL','MFLOANMATDT','DATE',7);
 cur_col_name:='P_MFORGLPRINAMT';
 new_rec.MFORGLPRINAMT:=runtime.validate_column_value(P_MFORGLPRINAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFORGLPRINAMT','NUMBER',22);
 cur_col_name:='P_MFINTRTPCT';
 new_rec.MFINTRTPCT:=runtime.validate_column_value(P_MFINTRTPCT,
 'STGCSA','ACCACCELERATELOANTBL','MFINTRTPCT','NUMBER',22);
 cur_col_name:='P_MFDBENTRAMT';
 new_rec.MFDBENTRAMT:=runtime.validate_column_value(P_MFDBENTRAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFDBENTRAMT','NUMBER',22);
 cur_col_name:='P_MFTOBECURAMT';
 new_rec.MFTOBECURAMT:=runtime.validate_column_value(P_MFTOBECURAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFTOBECURAMT','NUMBER',22);
 cur_col_name:='P_MFLASTPYMTDT';
 new_rec.MFLASTPYMTDT:=runtime.validate_column_value(P_MFLASTPYMTDT,
 'STGCSA','ACCACCELERATELOANTBL','MFLASTPYMTDT','DATE',7);
 cur_col_name:='P_MFESCROWPRIORPYMTAMT';
 new_rec.MFESCROWPRIORPYMTAMT:=runtime.validate_column_value(P_MFESCROWPRIORPYMTAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFESCROWPRIORPYMTAMT','NUMBER',22);
 cur_col_name:='P_MFCDCPAIDTHRUDT';
 new_rec.MFCDCPAIDTHRUDT:=runtime.validate_column_value(P_MFCDCPAIDTHRUDT,
 'STGCSA','ACCACCELERATELOANTBL','MFCDCPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_MFCDCMODELNQ';
 new_rec.MFCDCMODELNQ:=runtime.validate_column_value(P_MFCDCMODELNQ,
 'STGCSA','ACCACCELERATELOANTBL','MFCDCMODELNQ','NUMBER',22);
 cur_col_name:='P_MFCDCMOFEEAMT';
 new_rec.MFCDCMOFEEAMT:=runtime.validate_column_value(P_MFCDCMOFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCDCMOFEEAMT','NUMBER',22);
 cur_col_name:='P_MFCDCFEEAMT';
 new_rec.MFCDCFEEAMT:=runtime.validate_column_value(P_MFCDCFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCDCFEEAMT','NUMBER',22);
 cur_col_name:='P_MFCDCDUEFROMBORRAMT';
 new_rec.MFCDCDUEFROMBORRAMT:=runtime.validate_column_value(P_MFCDCDUEFROMBORRAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCDCDUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_MFCSAPAIDTHRUDT';
 new_rec.MFCSAPAIDTHRUDT:=runtime.validate_column_value(P_MFCSAPAIDTHRUDT,
 'STGCSA','ACCACCELERATELOANTBL','MFCSAPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_MFCSAMODELNQ';
 new_rec.MFCSAMODELNQ:=runtime.validate_column_value(P_MFCSAMODELNQ,
 'STGCSA','ACCACCELERATELOANTBL','MFCSAMODELNQ','NUMBER',22);
 cur_col_name:='P_MFCSAMOFEEAMT';
 new_rec.MFCSAMOFEEAMT:=runtime.validate_column_value(P_MFCSAMOFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCSAMOFEEAMT','NUMBER',22);
 cur_col_name:='P_MFCSAFEEAMT';
 new_rec.MFCSAFEEAMT:=runtime.validate_column_value(P_MFCSAFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCSAFEEAMT','NUMBER',22);
 cur_col_name:='P_MFCSADUEFROMBORRAMT';
 new_rec.MFCSADUEFROMBORRAMT:=runtime.validate_column_value(P_MFCSADUEFROMBORRAMT,
 'STGCSA','ACCACCELERATELOANTBL','MFCSADUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_PPAENTRYDT';
 new_rec.PPAENTRYDT:=runtime.validate_column_value(P_PPAENTRYDT,
 'STGCSA','ACCACCELERATELOANTBL','PPAENTRYDT','DATE',7);
 cur_col_name:='P_PPAGROUP';
 new_rec.PPAGROUP:=runtime.validate_column_value(P_PPAGROUP,
 'STGCSA','ACCACCELERATELOANTBL','PPAGROUP','VARCHAR2',20);
 cur_col_name:='P_PPACDCNMB';
 new_rec.PPACDCNMB:=runtime.validate_column_value(P_PPACDCNMB,
 'STGCSA','ACCACCELERATELOANTBL','PPACDCNMB','CHAR',8);
 cur_col_name:='P_PPAORGLPRINAMT';
 new_rec.PPAORGLPRINAMT:=runtime.validate_column_value(P_PPAORGLPRINAMT,
 'STGCSA','ACCACCELERATELOANTBL','PPAORGLPRINAMT','NUMBER',22);
 cur_col_name:='P_PPADBENTRBALATSEMIANAMT';
 new_rec.PPADBENTRBALATSEMIANAMT:=runtime.validate_column_value(P_PPADBENTRBALATSEMIANAMT,
 'STGCSA','ACCACCELERATELOANTBL','PPADBENTRBALATSEMIANAMT','NUMBER',22);
 cur_col_name:='P_PPAINTDUEATSEMIANAMT';
 new_rec.PPAINTDUEATSEMIANAMT:=runtime.validate_column_value(P_PPAINTDUEATSEMIANAMT,
 'STGCSA','ACCACCELERATELOANTBL','PPAINTDUEATSEMIANAMT','NUMBER',22);
 cur_col_name:='P_PPADUEATSEMIANAMT';
 new_rec.PPADUEATSEMIANAMT:=runtime.validate_column_value(P_PPADUEATSEMIANAMT,
 'STGCSA','ACCACCELERATELOANTBL','PPADUEATSEMIANAMT','NUMBER',22);
 cur_col_name:='P_SBAENTRYDT';
 new_rec.SBAENTRYDT:=runtime.validate_column_value(P_SBAENTRYDT,
 'STGCSA','ACCACCELERATELOANTBL','SBAENTRYDT','DATE',7);
 cur_col_name:='P_SBALASTTRNSCRPTIMPTDT';
 new_rec.SBALASTTRNSCRPTIMPTDT:=runtime.validate_column_value(P_SBALASTTRNSCRPTIMPTDT,
 'STGCSA','ACCACCELERATELOANTBL','SBALASTTRNSCRPTIMPTDT','DATE',7);
 cur_col_name:='P_SBADBENTRBALATSEMIANAMT';
 new_rec.SBADBENTRBALATSEMIANAMT:=runtime.validate_column_value(P_SBADBENTRBALATSEMIANAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBADBENTRBALATSEMIANAMT','NUMBER',22);
 cur_col_name:='P_SBAINTDUEATSEMIANAMT';
 new_rec.SBAINTDUEATSEMIANAMT:=runtime.validate_column_value(P_SBAINTDUEATSEMIANAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBAINTDUEATSEMIANAMT','NUMBER',22);
 cur_col_name:='P_SBACSAFEEAMT';
 new_rec.SBACSAFEEAMT:=runtime.validate_column_value(P_SBACSAFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBACSAFEEAMT','NUMBER',22);
 cur_col_name:='P_SBACDCFEEAMT';
 new_rec.SBACDCFEEAMT:=runtime.validate_column_value(P_SBACDCFEEAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBACDCFEEAMT','NUMBER',22);
 cur_col_name:='P_SBAESCROWPRIORPYMTAMT';
 new_rec.SBAESCROWPRIORPYMTAMT:=runtime.validate_column_value(P_SBAESCROWPRIORPYMTAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBAESCROWPRIORPYMTAMT','NUMBER',22);
 cur_col_name:='P_SBAACCELAMT';
 new_rec.SBAACCELAMT:=runtime.validate_column_value(P_SBAACCELAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBAACCELAMT','NUMBER',22);
 cur_col_name:='P_SBADUEDT';
 new_rec.SBADUEDT:=runtime.validate_column_value(P_SBADUEDT,
 'STGCSA','ACCACCELERATELOANTBL','SBADUEDT','DATE',7);
 cur_col_name:='P_SBANOTERTPCT';
 new_rec.SBANOTERTPCT:=runtime.validate_column_value(P_SBANOTERTPCT,
 'STGCSA','ACCACCELERATELOANTBL','SBANOTERTPCT','NUMBER',22);
 cur_col_name:='P_SBALASTNOTEBALAMT';
 new_rec.SBALASTNOTEBALAMT:=runtime.validate_column_value(P_SBALASTNOTEBALAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBALASTNOTEBALAMT','NUMBER',22);
 cur_col_name:='P_SBALASTPYMTDT';
 new_rec.SBALASTPYMTDT:=runtime.validate_column_value(P_SBALASTPYMTDT,
 'STGCSA','ACCACCELERATELOANTBL','SBALASTPYMTDT','DATE',7);
 cur_col_name:='P_SBAACCRINTDUEAMT';
 new_rec.SBAACCRINTDUEAMT:=runtime.validate_column_value(P_SBAACCRINTDUEAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBAACCRINTDUEAMT','NUMBER',22);
 cur_col_name:='P_SBAEXCESSAMT';
 new_rec.SBAEXCESSAMT:=runtime.validate_column_value(P_SBAEXCESSAMT,
 'STGCSA','ACCACCELERATELOANTBL','SBAEXCESSAMT','NUMBER',22);
 cur_col_name:='P_MFPREVSTATID';
 new_rec.MFPREVSTATID:=runtime.validate_column_value(P_MFPREVSTATID,
 'STGCSA','ACCACCELERATELOANTBL','MFPREVSTATID','NUMBER',22);
 cur_col_name:='P_MFPREVCMNT';
 new_rec.MFPREVCMNT:=runtime.validate_column_value(P_MFPREVCMNT,
 'STGCSA','ACCACCELERATELOANTBL','MFPREVCMNT','VARCHAR2',100);
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=runtime.validate_column_value(P_CDCNMB,
 'STGCSA','ACCACCELERATELOANTBL','CDCNMB','CHAR',8);
 cur_col_name:='P_SERVCNTRID';
 new_rec.SERVCNTRID:=runtime.validate_column_value(P_SERVCNTRID,
 'STGCSA','ACCACCELERATELOANTBL','SERVCNTRID','VARCHAR2',20);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','ACCACCELERATELOANTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.ACCACCELERATELOANAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.ACCACCELERATELOANTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 p_errmsg:='Got dupe key on insert into ACCACCELERATELOANTBL for key '
 ||'(ACCELERATNID)=('||new_rec.ACCELERATNID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCACCELERATELOANINSTSP build 2018-01-23 16:52:10';
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCACCELERATELOANINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCACCELERATELOANINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in ACCACCELERATELOANINSTSP build 2018-01-23 16:52:10'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END ACCACCELERATELOANINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


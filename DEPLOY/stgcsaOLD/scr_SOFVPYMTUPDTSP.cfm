<!--- Saved 01/28/2018 14:42:50. --->
PROCEDURE SOFVPYMTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_PYMTPOSTCDCAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCDCDISBAMT NUMBER:=null
 ,p_PYMTPOSTCDCDUEAMT NUMBER:=null
 ,p_PYMTPOSTCDCPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCDCPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCDCREPDAMT NUMBER:=null
 ,p_PYMTPOSTCDCSBADISBAMT NUMBER:=null
 ,p_PYMTPOSTCMNT VARCHAR2:=null
 ,p_PYMTPOSTCSAACTUALINITFEEAMT NUMBER:=null
 ,p_PYMTPOSTCSAAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCSADUEAMT NUMBER:=null
 ,p_PYMTPOSTCSAPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCSAPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCSAREPDAMT NUMBER:=null
 ,p_PYMTPOSTDUEAMT NUMBER:=null
 ,p_PYMTPOSTESCRCPTSTDAMT NUMBER:=null
 ,p_PYMTPOSTGNTYREPAYAMT NUMBER:=null
 ,p_PYMTPOSTINTACCRADJAMT NUMBER:=null
 ,p_PYMTPOSTINTACTUALBASISDAYS NUMBER:=null
 ,p_PYMTPOSTINTAPPLAMT NUMBER:=null
 ,p_PYMTPOSTINTBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTINTBASISCALC NUMBER:=null
 ,p_PYMTPOSTINTDUEAMT NUMBER:=null
 ,p_PYMTPOSTINTPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTINTPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTINTRJCTAMT NUMBER:=null
 ,p_PYMTPOSTINTRTUSEDPCT NUMBER:=null
 ,p_PYMTPOSTINTWAIVEDAMT NUMBER:=null
 ,p_PYMTPOSTLATEFEEASSESSAMT NUMBER:=null
 ,p_PYMTPOSTLATEFEEPAIDAMT NUMBER:=null
 ,p_PYMTPOSTLEFTOVRAMT NUMBER:=null
 ,p_PYMTPOSTLEFTOVRRJCTAMT NUMBER:=null
 ,p_PYMTPOSTLPYMTAMT NUMBER:=null
 ,p_PYMTPOSTPAIDBYCOLSONDTX CHAR:=null
 ,p_PYMTPOSTPREPAYAMT NUMBER:=null
 ,p_PYMTPOSTPRINAPPLAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALLEFTAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTPRINDUEAMT NUMBER:=null
 ,p_PYMTPOSTPRINRJCTAMT NUMBER:=null
 ,p_PYMTPOSTPYMTDT DATE:=null
 ,p_PYMTPOSTPYMTNMB CHAR:=null
 ,p_PYMTPOSTPYMTTYP CHAR:=null
 ,p_PYMTPOSTREMITTEDAMT NUMBER:=null
 ,p_PYMTPOSTREMITTEDRJCTAMT NUMBER:=null
 ,p_PYMTPOSTRJCTCD CHAR:=null
 ,p_PYMTPOSTRJCTDT DATE:=null
 ,p_PYMTPOSTSBAFEEAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEAPPLAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEENDDTX CHAR:=null
 ,p_PYMTPOSTSBAFEEREPDAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEESTARTDTX CHAR:=null
 ,p_PYMTPOSTXADJCD CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:35:33
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:35:33
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVPYMTUPDTSP performs UPDATE on STGCSA.SOFVPYMTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, PYMTPOSTPYMTDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVPYMTTBL%rowtype;
 new_rec STGCSA.SOFVPYMTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVPYMTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVPYMTUPDTSP build 2018-01-23 08:35:33 '
 ||'Select of STGCSA.SOFVPYMTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVPYMTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_PYMTPOSTPYMTDT is not null then
 cur_col_name:='P_PYMTPOSTPYMTDT';
 new_rec.PYMTPOSTPYMTDT:=P_PYMTPOSTPYMTDT; 
 end if;
 if P_PYMTPOSTPYMTTYP is not null then
 cur_col_name:='P_PYMTPOSTPYMTTYP';
 new_rec.PYMTPOSTPYMTTYP:=P_PYMTPOSTPYMTTYP; 
 end if;
 if P_PYMTPOSTDUEAMT is not null then
 cur_col_name:='P_PYMTPOSTDUEAMT';
 new_rec.PYMTPOSTDUEAMT:=P_PYMTPOSTDUEAMT; 
 end if;
 if P_PYMTPOSTREMITTEDAMT is not null then
 cur_col_name:='P_PYMTPOSTREMITTEDAMT';
 new_rec.PYMTPOSTREMITTEDAMT:=P_PYMTPOSTREMITTEDAMT; 
 end if;
 if P_PYMTPOSTPYMTNMB is not null then
 cur_col_name:='P_PYMTPOSTPYMTNMB';
 new_rec.PYMTPOSTPYMTNMB:=P_PYMTPOSTPYMTNMB; 
 end if;
 if P_PYMTPOSTCSAPAIDSTARTDTX is not null then
 cur_col_name:='P_PYMTPOSTCSAPAIDSTARTDTX';
 new_rec.PYMTPOSTCSAPAIDSTARTDTX:=P_PYMTPOSTCSAPAIDSTARTDTX; 
 end if;
 if P_PYMTPOSTCSAPAIDTHRUDTX is not null then
 cur_col_name:='P_PYMTPOSTCSAPAIDTHRUDTX';
 new_rec.PYMTPOSTCSAPAIDTHRUDTX:=P_PYMTPOSTCSAPAIDTHRUDTX; 
 end if;
 if P_PYMTPOSTCSADUEAMT is not null then
 cur_col_name:='P_PYMTPOSTCSADUEAMT';
 new_rec.PYMTPOSTCSADUEAMT:=P_PYMTPOSTCSADUEAMT; 
 end if;
 if P_PYMTPOSTCSAAPPLAMT is not null then
 cur_col_name:='P_PYMTPOSTCSAAPPLAMT';
 new_rec.PYMTPOSTCSAAPPLAMT:=P_PYMTPOSTCSAAPPLAMT; 
 end if;
 if P_PYMTPOSTCSAACTUALINITFEEAMT is not null then
 cur_col_name:='P_PYMTPOSTCSAACTUALINITFEEAMT';
 new_rec.PYMTPOSTCSAACTUALINITFEEAMT:=P_PYMTPOSTCSAACTUALINITFEEAMT; 
 end if;
 if P_PYMTPOSTCSAREPDAMT is not null then
 cur_col_name:='P_PYMTPOSTCSAREPDAMT';
 new_rec.PYMTPOSTCSAREPDAMT:=P_PYMTPOSTCSAREPDAMT; 
 end if;
 if P_PYMTPOSTSBAFEEREPDAMT is not null then
 cur_col_name:='P_PYMTPOSTSBAFEEREPDAMT';
 new_rec.PYMTPOSTSBAFEEREPDAMT:=P_PYMTPOSTSBAFEEREPDAMT; 
 end if;
 if P_PYMTPOSTCDCPAIDSTARTDTX is not null then
 cur_col_name:='P_PYMTPOSTCDCPAIDSTARTDTX';
 new_rec.PYMTPOSTCDCPAIDSTARTDTX:=P_PYMTPOSTCDCPAIDSTARTDTX; 
 end if;
 if P_PYMTPOSTCDCPAIDTHRUDTX is not null then
 cur_col_name:='P_PYMTPOSTCDCPAIDTHRUDTX';
 new_rec.PYMTPOSTCDCPAIDTHRUDTX:=P_PYMTPOSTCDCPAIDTHRUDTX; 
 end if;
 if P_PYMTPOSTCDCDUEAMT is not null then
 cur_col_name:='P_PYMTPOSTCDCDUEAMT';
 new_rec.PYMTPOSTCDCDUEAMT:=P_PYMTPOSTCDCDUEAMT; 
 end if;
 if P_PYMTPOSTCDCAPPLAMT is not null then
 cur_col_name:='P_PYMTPOSTCDCAPPLAMT';
 new_rec.PYMTPOSTCDCAPPLAMT:=P_PYMTPOSTCDCAPPLAMT; 
 end if;
 if P_PYMTPOSTCDCREPDAMT is not null then
 cur_col_name:='P_PYMTPOSTCDCREPDAMT';
 new_rec.PYMTPOSTCDCREPDAMT:=P_PYMTPOSTCDCREPDAMT; 
 end if;
 if P_PYMTPOSTSBAFEEAMT is not null then
 cur_col_name:='P_PYMTPOSTSBAFEEAMT';
 new_rec.PYMTPOSTSBAFEEAMT:=P_PYMTPOSTSBAFEEAMT; 
 end if;
 if P_PYMTPOSTSBAFEEAPPLAMT is not null then
 cur_col_name:='P_PYMTPOSTSBAFEEAPPLAMT';
 new_rec.PYMTPOSTSBAFEEAPPLAMT:=P_PYMTPOSTSBAFEEAPPLAMT; 
 end if;
 if P_PYMTPOSTINTPAIDSTARTDTX is not null then
 cur_col_name:='P_PYMTPOSTINTPAIDSTARTDTX';
 new_rec.PYMTPOSTINTPAIDSTARTDTX:=P_PYMTPOSTINTPAIDSTARTDTX; 
 end if;
 if P_PYMTPOSTINTPAIDTHRUDTX is not null then
 cur_col_name:='P_PYMTPOSTINTPAIDTHRUDTX';
 new_rec.PYMTPOSTINTPAIDTHRUDTX:=P_PYMTPOSTINTPAIDTHRUDTX; 
 end if;
 if P_PYMTPOSTINTBASISCALC is not null then
 cur_col_name:='P_PYMTPOSTINTBASISCALC';
 new_rec.PYMTPOSTINTBASISCALC:=P_PYMTPOSTINTBASISCALC; 
 end if;
 if P_PYMTPOSTINTRTUSEDPCT is not null then
 cur_col_name:='P_PYMTPOSTINTRTUSEDPCT';
 new_rec.PYMTPOSTINTRTUSEDPCT:=P_PYMTPOSTINTRTUSEDPCT; 
 end if;
 if P_PYMTPOSTINTBALUSEDAMT is not null then
 cur_col_name:='P_PYMTPOSTINTBALUSEDAMT';
 new_rec.PYMTPOSTINTBALUSEDAMT:=P_PYMTPOSTINTBALUSEDAMT; 
 end if;
 if P_PYMTPOSTINTDUEAMT is not null then
 cur_col_name:='P_PYMTPOSTINTDUEAMT';
 new_rec.PYMTPOSTINTDUEAMT:=P_PYMTPOSTINTDUEAMT; 
 end if;
 if P_PYMTPOSTINTAPPLAMT is not null then
 cur_col_name:='P_PYMTPOSTINTAPPLAMT';
 new_rec.PYMTPOSTINTAPPLAMT:=P_PYMTPOSTINTAPPLAMT; 
 end if;
 if P_PYMTPOSTINTACTUALBASISDAYS is not null then
 cur_col_name:='P_PYMTPOSTINTACTUALBASISDAYS';
 new_rec.PYMTPOSTINTACTUALBASISDAYS:=P_PYMTPOSTINTACTUALBASISDAYS; 
 end if;
 if P_PYMTPOSTINTACCRADJAMT is not null then
 cur_col_name:='P_PYMTPOSTINTACCRADJAMT';
 new_rec.PYMTPOSTINTACCRADJAMT:=P_PYMTPOSTINTACCRADJAMT; 
 end if;
 if P_PYMTPOSTINTRJCTAMT is not null then
 cur_col_name:='P_PYMTPOSTINTRJCTAMT';
 new_rec.PYMTPOSTINTRJCTAMT:=P_PYMTPOSTINTRJCTAMT; 
 end if;
 if P_PYMTPOSTSBAFEESTARTDTX is not null then
 cur_col_name:='P_PYMTPOSTSBAFEESTARTDTX';
 new_rec.PYMTPOSTSBAFEESTARTDTX:=P_PYMTPOSTSBAFEESTARTDTX; 
 end if;
 if P_PYMTPOSTPRINDUEAMT is not null then
 cur_col_name:='P_PYMTPOSTPRINDUEAMT';
 new_rec.PYMTPOSTPRINDUEAMT:=P_PYMTPOSTPRINDUEAMT; 
 end if;
 if P_PYMTPOSTPRINBALUSEDAMT is not null then
 cur_col_name:='P_PYMTPOSTPRINBALUSEDAMT';
 new_rec.PYMTPOSTPRINBALUSEDAMT:=P_PYMTPOSTPRINBALUSEDAMT; 
 end if;
 if P_PYMTPOSTPRINAPPLAMT is not null then
 cur_col_name:='P_PYMTPOSTPRINAPPLAMT';
 new_rec.PYMTPOSTPRINAPPLAMT:=P_PYMTPOSTPRINAPPLAMT; 
 end if;
 if P_PYMTPOSTPRINBALLEFTAMT is not null then
 cur_col_name:='P_PYMTPOSTPRINBALLEFTAMT';
 new_rec.PYMTPOSTPRINBALLEFTAMT:=P_PYMTPOSTPRINBALLEFTAMT; 
 end if;
 if P_PYMTPOSTPRINRJCTAMT is not null then
 cur_col_name:='P_PYMTPOSTPRINRJCTAMT';
 new_rec.PYMTPOSTPRINRJCTAMT:=P_PYMTPOSTPRINRJCTAMT; 
 end if;
 if P_PYMTPOSTGNTYREPAYAMT is not null then
 cur_col_name:='P_PYMTPOSTGNTYREPAYAMT';
 new_rec.PYMTPOSTGNTYREPAYAMT:=P_PYMTPOSTGNTYREPAYAMT; 
 end if;
 if P_PYMTPOSTSBAFEEENDDTX is not null then
 cur_col_name:='P_PYMTPOSTSBAFEEENDDTX';
 new_rec.PYMTPOSTSBAFEEENDDTX:=P_PYMTPOSTSBAFEEENDDTX; 
 end if;
 if P_PYMTPOSTPAIDBYCOLSONDTX is not null then
 cur_col_name:='P_PYMTPOSTPAIDBYCOLSONDTX';
 new_rec.PYMTPOSTPAIDBYCOLSONDTX:=P_PYMTPOSTPAIDBYCOLSONDTX; 
 end if;
 if P_PYMTPOSTLPYMTAMT is not null then
 cur_col_name:='P_PYMTPOSTLPYMTAMT';
 new_rec.PYMTPOSTLPYMTAMT:=P_PYMTPOSTLPYMTAMT; 
 end if;
 if P_PYMTPOSTESCRCPTSTDAMT is not null then
 cur_col_name:='P_PYMTPOSTESCRCPTSTDAMT';
 new_rec.PYMTPOSTESCRCPTSTDAMT:=P_PYMTPOSTESCRCPTSTDAMT; 
 end if;
 if P_PYMTPOSTLATEFEEASSESSAMT is not null then
 cur_col_name:='P_PYMTPOSTLATEFEEASSESSAMT';
 new_rec.PYMTPOSTLATEFEEASSESSAMT:=P_PYMTPOSTLATEFEEASSESSAMT; 
 end if;
 if P_PYMTPOSTCDCDISBAMT is not null then
 cur_col_name:='P_PYMTPOSTCDCDISBAMT';
 new_rec.PYMTPOSTCDCDISBAMT:=P_PYMTPOSTCDCDISBAMT; 
 end if;
 if P_PYMTPOSTCDCSBADISBAMT is not null then
 cur_col_name:='P_PYMTPOSTCDCSBADISBAMT';
 new_rec.PYMTPOSTCDCSBADISBAMT:=P_PYMTPOSTCDCSBADISBAMT; 
 end if;
 if P_PYMTPOSTXADJCD is not null then
 cur_col_name:='P_PYMTPOSTXADJCD';
 new_rec.PYMTPOSTXADJCD:=P_PYMTPOSTXADJCD; 
 end if;
 if P_PYMTPOSTLEFTOVRAMT is not null then
 cur_col_name:='P_PYMTPOSTLEFTOVRAMT';
 new_rec.PYMTPOSTLEFTOVRAMT:=P_PYMTPOSTLEFTOVRAMT; 
 end if;
 if P_PYMTPOSTRJCTCD is not null then
 cur_col_name:='P_PYMTPOSTRJCTCD';
 new_rec.PYMTPOSTRJCTCD:=P_PYMTPOSTRJCTCD; 
 end if;
 if P_PYMTPOSTCMNT is not null then
 cur_col_name:='P_PYMTPOSTCMNT';
 new_rec.PYMTPOSTCMNT:=P_PYMTPOSTCMNT; 
 end if;
 if P_PYMTPOSTREMITTEDRJCTAMT is not null then
 cur_col_name:='P_PYMTPOSTREMITTEDRJCTAMT';
 new_rec.PYMTPOSTREMITTEDRJCTAMT:=P_PYMTPOSTREMITTEDRJCTAMT; 
 end if;
 if P_PYMTPOSTLEFTOVRRJCTAMT is not null then
 cur_col_name:='P_PYMTPOSTLEFTOVRRJCTAMT';
 new_rec.PYMTPOSTLEFTOVRRJCTAMT:=P_PYMTPOSTLEFTOVRRJCTAMT; 
 end if;
 if P_PYMTPOSTRJCTDT is not null then
 cur_col_name:='P_PYMTPOSTRJCTDT';
 new_rec.PYMTPOSTRJCTDT:=P_PYMTPOSTRJCTDT; 
 end if;
 if P_PYMTPOSTLATEFEEPAIDAMT is not null then
 cur_col_name:='P_PYMTPOSTLATEFEEPAIDAMT';
 new_rec.PYMTPOSTLATEFEEPAIDAMT:=P_PYMTPOSTLATEFEEPAIDAMT; 
 end if;
 if P_PYMTPOSTINTWAIVEDAMT is not null then
 cur_col_name:='P_PYMTPOSTINTWAIVEDAMT';
 new_rec.PYMTPOSTINTWAIVEDAMT:=P_PYMTPOSTINTWAIVEDAMT; 
 end if;
 if P_PYMTPOSTPREPAYAMT is not null then
 cur_col_name:='P_PYMTPOSTPREPAYAMT';
 new_rec.PYMTPOSTPREPAYAMT:=P_PYMTPOSTPREPAYAMT; 
 end if;
 cur_col_name:=null;
 stgcsa.SOFVPYMTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVPYMTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,PYMTPOSTPYMTTYP=new_rec.PYMTPOSTPYMTTYP
 ,PYMTPOSTDUEAMT=new_rec.PYMTPOSTDUEAMT
 ,PYMTPOSTREMITTEDAMT=new_rec.PYMTPOSTREMITTEDAMT
 ,PYMTPOSTPYMTNMB=new_rec.PYMTPOSTPYMTNMB
 ,PYMTPOSTCSAPAIDSTARTDTX=new_rec.PYMTPOSTCSAPAIDSTARTDTX
 ,PYMTPOSTCSAPAIDTHRUDTX=new_rec.PYMTPOSTCSAPAIDTHRUDTX
 ,PYMTPOSTCSADUEAMT=new_rec.PYMTPOSTCSADUEAMT
 ,PYMTPOSTCSAAPPLAMT=new_rec.PYMTPOSTCSAAPPLAMT
 ,PYMTPOSTCSAACTUALINITFEEAMT=new_rec.PYMTPOSTCSAACTUALINITFEEAMT
 ,PYMTPOSTCSAREPDAMT=new_rec.PYMTPOSTCSAREPDAMT
 ,PYMTPOSTSBAFEEREPDAMT=new_rec.PYMTPOSTSBAFEEREPDAMT
 ,PYMTPOSTCDCPAIDSTARTDTX=new_rec.PYMTPOSTCDCPAIDSTARTDTX
 ,PYMTPOSTCDCPAIDTHRUDTX=new_rec.PYMTPOSTCDCPAIDTHRUDTX
 ,PYMTPOSTCDCDUEAMT=new_rec.PYMTPOSTCDCDUEAMT
 ,PYMTPOSTCDCAPPLAMT=new_rec.PYMTPOSTCDCAPPLAMT
 ,PYMTPOSTCDCREPDAMT=new_rec.PYMTPOSTCDCREPDAMT
 ,PYMTPOSTSBAFEEAMT=new_rec.PYMTPOSTSBAFEEAMT
 ,PYMTPOSTSBAFEEAPPLAMT=new_rec.PYMTPOSTSBAFEEAPPLAMT
 ,PYMTPOSTINTPAIDSTARTDTX=new_rec.PYMTPOSTINTPAIDSTARTDTX
 ,PYMTPOSTINTPAIDTHRUDTX=new_rec.PYMTPOSTINTPAIDTHRUDTX
 ,PYMTPOSTINTBASISCALC=new_rec.PYMTPOSTINTBASISCALC
 ,PYMTPOSTINTRTUSEDPCT=new_rec.PYMTPOSTINTRTUSEDPCT
 ,PYMTPOSTINTBALUSEDAMT=new_rec.PYMTPOSTINTBALUSEDAMT
 ,PYMTPOSTINTDUEAMT=new_rec.PYMTPOSTINTDUEAMT
 ,PYMTPOSTINTAPPLAMT=new_rec.PYMTPOSTINTAPPLAMT
 ,PYMTPOSTINTACTUALBASISDAYS=new_rec.PYMTPOSTINTACTUALBASISDAYS
 ,PYMTPOSTINTACCRADJAMT=new_rec.PYMTPOSTINTACCRADJAMT
 ,PYMTPOSTINTRJCTAMT=new_rec.PYMTPOSTINTRJCTAMT
 ,PYMTPOSTSBAFEESTARTDTX=new_rec.PYMTPOSTSBAFEESTARTDTX
 ,PYMTPOSTPRINDUEAMT=new_rec.PYMTPOSTPRINDUEAMT
 ,PYMTPOSTPRINBALUSEDAMT=new_rec.PYMTPOSTPRINBALUSEDAMT
 ,PYMTPOSTPRINAPPLAMT=new_rec.PYMTPOSTPRINAPPLAMT
 ,PYMTPOSTPRINBALLEFTAMT=new_rec.PYMTPOSTPRINBALLEFTAMT
 ,PYMTPOSTPRINRJCTAMT=new_rec.PYMTPOSTPRINRJCTAMT
 ,PYMTPOSTGNTYREPAYAMT=new_rec.PYMTPOSTGNTYREPAYAMT
 ,PYMTPOSTSBAFEEENDDTX=new_rec.PYMTPOSTSBAFEEENDDTX
 ,PYMTPOSTPAIDBYCOLSONDTX=new_rec.PYMTPOSTPAIDBYCOLSONDTX
 ,PYMTPOSTLPYMTAMT=new_rec.PYMTPOSTLPYMTAMT
 ,PYMTPOSTESCRCPTSTDAMT=new_rec.PYMTPOSTESCRCPTSTDAMT
 ,PYMTPOSTLATEFEEASSESSAMT=new_rec.PYMTPOSTLATEFEEASSESSAMT
 ,PYMTPOSTCDCDISBAMT=new_rec.PYMTPOSTCDCDISBAMT
 ,PYMTPOSTCDCSBADISBAMT=new_rec.PYMTPOSTCDCSBADISBAMT
 ,PYMTPOSTXADJCD=new_rec.PYMTPOSTXADJCD
 ,PYMTPOSTLEFTOVRAMT=new_rec.PYMTPOSTLEFTOVRAMT
 ,PYMTPOSTRJCTCD=new_rec.PYMTPOSTRJCTCD
 ,PYMTPOSTCMNT=new_rec.PYMTPOSTCMNT
 ,PYMTPOSTREMITTEDRJCTAMT=new_rec.PYMTPOSTREMITTEDRJCTAMT
 ,PYMTPOSTLEFTOVRRJCTAMT=new_rec.PYMTPOSTLEFTOVRRJCTAMT
 ,PYMTPOSTRJCTDT=new_rec.PYMTPOSTRJCTDT
 ,PYMTPOSTLATEFEEPAIDAMT=new_rec.PYMTPOSTLATEFEEPAIDAMT
 ,PYMTPOSTINTWAIVEDAMT=new_rec.PYMTPOSTINTWAIVEDAMT
 ,PYMTPOSTPREPAYAMT=new_rec.PYMTPOSTPREPAYAMT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVPYMTUPDTSP build 2018-01-23 08:35:33 '
 ||'while doing update on STGCSA.SOFVPYMTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVPYMTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVPYMTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVPYMTTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ''(PYMTPOSTPYMTDT)=('||P_PYMTPOSTPYMTDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVPYMTTBL where 1=1
 and LOANNMB=P_LOANNMB and PYMTPOSTPYMTDT=P_PYMTPOSTPYMTDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 end if;
 p_errmsg:=' In SOFVPYMTUPDTSP build 2018-01-23 08:35:33 '
 ||'No STGCSA.SOFVPYMTTBL row to update with key(s) '||keystouse;
 end;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVPYMTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVPYMTUPDTSP build 2018-01-23 08:35:33 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVPYMTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVPYMTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVPYMTUPDTSP build 2018-01-23 08:35:33'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVPYMTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVPYMTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


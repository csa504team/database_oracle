<!--- Saved 01/28/2018 14:42:34. --->
PROCEDURE SOFVCTCHDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CATCHUPPLANCREDT DATE:=null
 ,p_CATCHUPPLANENDDT1 DATE:=null
 ,p_CATCHUPPLANENDDT10 DATE:=null
 ,p_CATCHUPPLANENDDT11 DATE:=null
 ,p_CATCHUPPLANENDDT12 DATE:=null
 ,p_CATCHUPPLANENDDT13 DATE:=null
 ,p_CATCHUPPLANENDDT14 DATE:=null
 ,p_CATCHUPPLANENDDT15 DATE:=null
 ,p_CATCHUPPLANENDDT2 DATE:=null
 ,p_CATCHUPPLANENDDT3 DATE:=null
 ,p_CATCHUPPLANENDDT4 DATE:=null
 ,p_CATCHUPPLANENDDT5 DATE:=null
 ,p_CATCHUPPLANENDDT6 DATE:=null
 ,p_CATCHUPPLANENDDT7 DATE:=null
 ,p_CATCHUPPLANENDDT8 DATE:=null
 ,p_CATCHUPPLANENDDT9 DATE:=null
 ,p_CATCHUPPLANMODDT DATE:=null
 ,p_CATCHUPPLANRQ10AMT NUMBER:=null
 ,p_CATCHUPPLANRQ11AMT NUMBER:=null
 ,p_CATCHUPPLANRQ12AMT NUMBER:=null
 ,p_CATCHUPPLANRQ13AMT NUMBER:=null
 ,p_CATCHUPPLANRQ14AMT NUMBER:=null
 ,p_CATCHUPPLANRQ15AMT NUMBER:=null
 ,p_CATCHUPPLANRQ1AMT NUMBER:=null
 ,p_CATCHUPPLANRQ2AMT NUMBER:=null
 ,p_CATCHUPPLANRQ3AMT NUMBER:=null
 ,p_CATCHUPPLANRQ4AMT NUMBER:=null
 ,p_CATCHUPPLANRQ5AMT NUMBER:=null
 ,p_CATCHUPPLANRQ6AMT NUMBER:=null
 ,p_CATCHUPPLANRQ7AMT NUMBER:=null
 ,p_CATCHUPPLANRQ8AMT NUMBER:=null
 ,p_CATCHUPPLANRQ9AMT NUMBER:=null
 ,p_CATCHUPPLANSBANMB CHAR:=null
 ,p_CATCHUPPLANSTRTDT1 DATE:=null
 ,p_CATCHUPPLANSTRTDT10 DATE:=null
 ,p_CATCHUPPLANSTRTDT11 DATE:=null
 ,p_CATCHUPPLANSTRTDT12 DATE:=null
 ,p_CATCHUPPLANSTRTDT13 DATE:=null
 ,p_CATCHUPPLANSTRTDT14 DATE:=null
 ,p_CATCHUPPLANSTRTDT15 DATE:=null
 ,p_CATCHUPPLANSTRTDT2 DATE:=null
 ,p_CATCHUPPLANSTRTDT3 DATE:=null
 ,p_CATCHUPPLANSTRTDT4 DATE:=null
 ,p_CATCHUPPLANSTRTDT5 DATE:=null
 ,p_CATCHUPPLANSTRTDT6 DATE:=null
 ,p_CATCHUPPLANSTRTDT7 DATE:=null
 ,p_CATCHUPPLANSTRTDT8 DATE:=null
 ,p_CATCHUPPLANSTRTDT9 DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:44
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:31:44
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVCTCHDELTSP performs DELETE on STGCSA.SOFVCTCHTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CATCHUPPLANSBANMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVCTCHTBL%rowtype;
 new_rec STGCSA.SOFVCTCHTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVCTCHTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCTCHDELTSP build 2018-01-23 08:31:44 '
 ||'Select of STGCSA.SOFVCTCHTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCTCHDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVCTCHAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVCTCHTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVCTCHDELTSP build 2018-01-23 08:31:44 '
 ||'while doing delete on STGCSA.SOFVCTCHTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCTCHDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVCTCHDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCTCHTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CATCHUPPLANSBANMB)=('||P_CATCHUPPLANSBANMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVCTCHTBL where 1=1
 and CATCHUPPLANSBANMB=P_CATCHUPPLANSBANMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCTCHDELTSP build 2018-01-23 08:31:44 '
 ||'No STGCSA.SOFVCTCHTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCTCHDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVCTCHDELTSP build 2018-01-23 08:31:44 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCTCHDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCTCHDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVCTCHDELTSP build 2018-01-23 08:31:44'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCTCHDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVCTCHDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


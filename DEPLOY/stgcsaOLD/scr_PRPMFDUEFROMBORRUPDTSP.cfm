<!--- Saved 01/28/2018 14:42:18. --->
PROCEDURE PRPMFDUEFROMBORRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_APPRDT DATE:=null
 ,p_CDCDUEFROMBORRAMT NUMBER:=null
 ,p_CSADUEFROMBORRAMT NUMBER:=null
 ,p_CURRFEEBALAMT NUMBER:=null
 ,p_DUEFROMBORRAMT NUMBER:=null
 ,p_ESCPRIORPYMTAMT NUMBER:=null
 ,p_LATEFEEAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_MATRDT DATE:=null
 ,p_PRPMFDUEFROMBORRID NUMBER:=null
 ,p_SEMIANAMT NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:28:06
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:28:06
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPMFDUEFROMBORRUPDTSP performs UPDATE on STGCSA.PRPMFDUEFROMBORRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPMFDUEFROMBORRID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
 new_rec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPMFDUEFROMBORRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFDUEFROMBORRUPDTSP build 2018-01-23 08:28:06 '
 ||'Select of STGCSA.PRPMFDUEFROMBORRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PRPMFDUEFROMBORRID is not null then
 cur_col_name:='P_PRPMFDUEFROMBORRID';
 new_rec.PRPMFDUEFROMBORRID:=P_PRPMFDUEFROMBORRID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_SEMIANAMT is not null then
 cur_col_name:='P_SEMIANAMT';
 new_rec.SEMIANAMT:=P_SEMIANAMT; 
 end if;
 if P_ESCPRIORPYMTAMT is not null then
 cur_col_name:='P_ESCPRIORPYMTAMT';
 new_rec.ESCPRIORPYMTAMT:=P_ESCPRIORPYMTAMT; 
 end if;
 if P_CDCDUEFROMBORRAMT is not null then
 cur_col_name:='P_CDCDUEFROMBORRAMT';
 new_rec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT; 
 end if;
 if P_DUEFROMBORRAMT is not null then
 cur_col_name:='P_DUEFROMBORRAMT';
 new_rec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT; 
 end if;
 if P_CSADUEFROMBORRAMT is not null then
 cur_col_name:='P_CSADUEFROMBORRAMT';
 new_rec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT; 
 end if;
 if P_SEMIANDT is not null then
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_APPRDT is not null then
 cur_col_name:='P_APPRDT';
 new_rec.APPRDT:=P_APPRDT; 
 end if;
 if P_CURRFEEBALAMT is not null then
 cur_col_name:='P_CURRFEEBALAMT';
 new_rec.CURRFEEBALAMT:=P_CURRFEEBALAMT; 
 end if;
 if P_LATEFEEAMT is not null then
 cur_col_name:='P_LATEFEEAMT';
 new_rec.LATEFEEAMT:=P_LATEFEEAMT; 
 end if;
 if P_MATRDT is not null then
 cur_col_name:='P_MATRDT';
 new_rec.MATRDT:=P_MATRDT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPMFDUEFROMBORRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPMFDUEFROMBORRTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,SEMIANAMT=new_rec.SEMIANAMT
 ,ESCPRIORPYMTAMT=new_rec.ESCPRIORPYMTAMT
 ,CDCDUEFROMBORRAMT=new_rec.CDCDUEFROMBORRAMT
 ,DUEFROMBORRAMT=new_rec.DUEFROMBORRAMT
 ,CSADUEFROMBORRAMT=new_rec.CSADUEFROMBORRAMT
 ,SEMIANDT=new_rec.SEMIANDT
 ,APPRDT=new_rec.APPRDT
 ,CURRFEEBALAMT=new_rec.CURRFEEBALAMT
 ,LATEFEEAMT=new_rec.LATEFEEAMT
 ,MATRDT=new_rec.MATRDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPMFDUEFROMBORRUPDTSP build 2018-01-23 08:28:06 '
 ||'while doing update on STGCSA.PRPMFDUEFROMBORRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFDUEFROMBORRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPMFDUEFROMBORRID)=('||P_PRPMFDUEFROMBORRID||') ';
 select rowid into rec_rowid from STGCSA.PRPMFDUEFROMBORRTBL where 1=1
 and PRPMFDUEFROMBORRID=P_PRPMFDUEFROMBORRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFDUEFROMBORRUPDTSP build 2018-01-23 08:28:06 '
 ||'No STGCSA.PRPMFDUEFROMBORRTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPMFDUEFROMBORRUPDTSP build 2018-01-23 08:28:06 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFDUEFROMBORRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPMFDUEFROMBORRUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PRPMFDUEFROMBORRUPDTSP build 2018-01-23 08:28:06'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPMFDUEFROMBORRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:34. --->
PROCEDURE SOFVCTCHUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CATCHUPPLANCREDT DATE:=null
 ,p_CATCHUPPLANENDDT1 DATE:=null
 ,p_CATCHUPPLANENDDT10 DATE:=null
 ,p_CATCHUPPLANENDDT11 DATE:=null
 ,p_CATCHUPPLANENDDT12 DATE:=null
 ,p_CATCHUPPLANENDDT13 DATE:=null
 ,p_CATCHUPPLANENDDT14 DATE:=null
 ,p_CATCHUPPLANENDDT15 DATE:=null
 ,p_CATCHUPPLANENDDT2 DATE:=null
 ,p_CATCHUPPLANENDDT3 DATE:=null
 ,p_CATCHUPPLANENDDT4 DATE:=null
 ,p_CATCHUPPLANENDDT5 DATE:=null
 ,p_CATCHUPPLANENDDT6 DATE:=null
 ,p_CATCHUPPLANENDDT7 DATE:=null
 ,p_CATCHUPPLANENDDT8 DATE:=null
 ,p_CATCHUPPLANENDDT9 DATE:=null
 ,p_CATCHUPPLANMODDT DATE:=null
 ,p_CATCHUPPLANRQ10AMT NUMBER:=null
 ,p_CATCHUPPLANRQ11AMT NUMBER:=null
 ,p_CATCHUPPLANRQ12AMT NUMBER:=null
 ,p_CATCHUPPLANRQ13AMT NUMBER:=null
 ,p_CATCHUPPLANRQ14AMT NUMBER:=null
 ,p_CATCHUPPLANRQ15AMT NUMBER:=null
 ,p_CATCHUPPLANRQ1AMT NUMBER:=null
 ,p_CATCHUPPLANRQ2AMT NUMBER:=null
 ,p_CATCHUPPLANRQ3AMT NUMBER:=null
 ,p_CATCHUPPLANRQ4AMT NUMBER:=null
 ,p_CATCHUPPLANRQ5AMT NUMBER:=null
 ,p_CATCHUPPLANRQ6AMT NUMBER:=null
 ,p_CATCHUPPLANRQ7AMT NUMBER:=null
 ,p_CATCHUPPLANRQ8AMT NUMBER:=null
 ,p_CATCHUPPLANRQ9AMT NUMBER:=null
 ,p_CATCHUPPLANSBANMB CHAR:=null
 ,p_CATCHUPPLANSTRTDT1 DATE:=null
 ,p_CATCHUPPLANSTRTDT10 DATE:=null
 ,p_CATCHUPPLANSTRTDT11 DATE:=null
 ,p_CATCHUPPLANSTRTDT12 DATE:=null
 ,p_CATCHUPPLANSTRTDT13 DATE:=null
 ,p_CATCHUPPLANSTRTDT14 DATE:=null
 ,p_CATCHUPPLANSTRTDT15 DATE:=null
 ,p_CATCHUPPLANSTRTDT2 DATE:=null
 ,p_CATCHUPPLANSTRTDT3 DATE:=null
 ,p_CATCHUPPLANSTRTDT4 DATE:=null
 ,p_CATCHUPPLANSTRTDT5 DATE:=null
 ,p_CATCHUPPLANSTRTDT6 DATE:=null
 ,p_CATCHUPPLANSTRTDT7 DATE:=null
 ,p_CATCHUPPLANSTRTDT8 DATE:=null
 ,p_CATCHUPPLANSTRTDT9 DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:32
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:31:33
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVCTCHUPDTSP performs UPDATE on STGCSA.SOFVCTCHTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CATCHUPPLANSBANMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVCTCHTBL%rowtype;
 new_rec STGCSA.SOFVCTCHTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVCTCHTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCTCHUPDTSP build 2018-01-23 08:31:32 '
 ||'Select of STGCSA.SOFVCTCHTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCTCHUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CATCHUPPLANSBANMB is not null then
 cur_col_name:='P_CATCHUPPLANSBANMB';
 new_rec.CATCHUPPLANSBANMB:=P_CATCHUPPLANSBANMB; 
 end if;
 if P_CATCHUPPLANSTRTDT1 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT1';
 new_rec.CATCHUPPLANSTRTDT1:=P_CATCHUPPLANSTRTDT1; 
 end if;
 if P_CATCHUPPLANENDDT1 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT1';
 new_rec.CATCHUPPLANENDDT1:=P_CATCHUPPLANENDDT1; 
 end if;
 if P_CATCHUPPLANRQ1AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ1AMT';
 new_rec.CATCHUPPLANRQ1AMT:=P_CATCHUPPLANRQ1AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT2 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT2';
 new_rec.CATCHUPPLANSTRTDT2:=P_CATCHUPPLANSTRTDT2; 
 end if;
 if P_CATCHUPPLANENDDT2 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT2';
 new_rec.CATCHUPPLANENDDT2:=P_CATCHUPPLANENDDT2; 
 end if;
 if P_CATCHUPPLANRQ2AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ2AMT';
 new_rec.CATCHUPPLANRQ2AMT:=P_CATCHUPPLANRQ2AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT3 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT3';
 new_rec.CATCHUPPLANSTRTDT3:=P_CATCHUPPLANSTRTDT3; 
 end if;
 if P_CATCHUPPLANENDDT3 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT3';
 new_rec.CATCHUPPLANENDDT3:=P_CATCHUPPLANENDDT3; 
 end if;
 if P_CATCHUPPLANRQ3AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ3AMT';
 new_rec.CATCHUPPLANRQ3AMT:=P_CATCHUPPLANRQ3AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT4 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT4';
 new_rec.CATCHUPPLANSTRTDT4:=P_CATCHUPPLANSTRTDT4; 
 end if;
 if P_CATCHUPPLANENDDT4 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT4';
 new_rec.CATCHUPPLANENDDT4:=P_CATCHUPPLANENDDT4; 
 end if;
 if P_CATCHUPPLANRQ4AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ4AMT';
 new_rec.CATCHUPPLANRQ4AMT:=P_CATCHUPPLANRQ4AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT5 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT5';
 new_rec.CATCHUPPLANSTRTDT5:=P_CATCHUPPLANSTRTDT5; 
 end if;
 if P_CATCHUPPLANENDDT5 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT5';
 new_rec.CATCHUPPLANENDDT5:=P_CATCHUPPLANENDDT5; 
 end if;
 if P_CATCHUPPLANRQ5AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ5AMT';
 new_rec.CATCHUPPLANRQ5AMT:=P_CATCHUPPLANRQ5AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT6 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT6';
 new_rec.CATCHUPPLANSTRTDT6:=P_CATCHUPPLANSTRTDT6; 
 end if;
 if P_CATCHUPPLANENDDT6 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT6';
 new_rec.CATCHUPPLANENDDT6:=P_CATCHUPPLANENDDT6; 
 end if;
 if P_CATCHUPPLANRQ6AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ6AMT';
 new_rec.CATCHUPPLANRQ6AMT:=P_CATCHUPPLANRQ6AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT7 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT7';
 new_rec.CATCHUPPLANSTRTDT7:=P_CATCHUPPLANSTRTDT7; 
 end if;
 if P_CATCHUPPLANENDDT7 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT7';
 new_rec.CATCHUPPLANENDDT7:=P_CATCHUPPLANENDDT7; 
 end if;
 if P_CATCHUPPLANRQ7AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ7AMT';
 new_rec.CATCHUPPLANRQ7AMT:=P_CATCHUPPLANRQ7AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT8 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT8';
 new_rec.CATCHUPPLANSTRTDT8:=P_CATCHUPPLANSTRTDT8; 
 end if;
 if P_CATCHUPPLANENDDT8 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT8';
 new_rec.CATCHUPPLANENDDT8:=P_CATCHUPPLANENDDT8; 
 end if;
 if P_CATCHUPPLANRQ8AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ8AMT';
 new_rec.CATCHUPPLANRQ8AMT:=P_CATCHUPPLANRQ8AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT9 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT9';
 new_rec.CATCHUPPLANSTRTDT9:=P_CATCHUPPLANSTRTDT9; 
 end if;
 if P_CATCHUPPLANENDDT9 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT9';
 new_rec.CATCHUPPLANENDDT9:=P_CATCHUPPLANENDDT9; 
 end if;
 if P_CATCHUPPLANRQ9AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ9AMT';
 new_rec.CATCHUPPLANRQ9AMT:=P_CATCHUPPLANRQ9AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT10 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT10';
 new_rec.CATCHUPPLANSTRTDT10:=P_CATCHUPPLANSTRTDT10; 
 end if;
 if P_CATCHUPPLANENDDT10 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT10';
 new_rec.CATCHUPPLANENDDT10:=P_CATCHUPPLANENDDT10; 
 end if;
 if P_CATCHUPPLANRQ10AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ10AMT';
 new_rec.CATCHUPPLANRQ10AMT:=P_CATCHUPPLANRQ10AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT11 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT11';
 new_rec.CATCHUPPLANSTRTDT11:=P_CATCHUPPLANSTRTDT11; 
 end if;
 if P_CATCHUPPLANENDDT11 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT11';
 new_rec.CATCHUPPLANENDDT11:=P_CATCHUPPLANENDDT11; 
 end if;
 if P_CATCHUPPLANRQ11AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ11AMT';
 new_rec.CATCHUPPLANRQ11AMT:=P_CATCHUPPLANRQ11AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT12 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT12';
 new_rec.CATCHUPPLANSTRTDT12:=P_CATCHUPPLANSTRTDT12; 
 end if;
 if P_CATCHUPPLANENDDT12 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT12';
 new_rec.CATCHUPPLANENDDT12:=P_CATCHUPPLANENDDT12; 
 end if;
 if P_CATCHUPPLANRQ12AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ12AMT';
 new_rec.CATCHUPPLANRQ12AMT:=P_CATCHUPPLANRQ12AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT13 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT13';
 new_rec.CATCHUPPLANSTRTDT13:=P_CATCHUPPLANSTRTDT13; 
 end if;
 if P_CATCHUPPLANENDDT13 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT13';
 new_rec.CATCHUPPLANENDDT13:=P_CATCHUPPLANENDDT13; 
 end if;
 if P_CATCHUPPLANRQ13AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ13AMT';
 new_rec.CATCHUPPLANRQ13AMT:=P_CATCHUPPLANRQ13AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT14 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT14';
 new_rec.CATCHUPPLANSTRTDT14:=P_CATCHUPPLANSTRTDT14; 
 end if;
 if P_CATCHUPPLANENDDT14 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT14';
 new_rec.CATCHUPPLANENDDT14:=P_CATCHUPPLANENDDT14; 
 end if;
 if P_CATCHUPPLANRQ14AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ14AMT';
 new_rec.CATCHUPPLANRQ14AMT:=P_CATCHUPPLANRQ14AMT; 
 end if;
 if P_CATCHUPPLANSTRTDT15 is not null then
 cur_col_name:='P_CATCHUPPLANSTRTDT15';
 new_rec.CATCHUPPLANSTRTDT15:=P_CATCHUPPLANSTRTDT15; 
 end if;
 if P_CATCHUPPLANENDDT15 is not null then
 cur_col_name:='P_CATCHUPPLANENDDT15';
 new_rec.CATCHUPPLANENDDT15:=P_CATCHUPPLANENDDT15; 
 end if;
 if P_CATCHUPPLANRQ15AMT is not null then
 cur_col_name:='P_CATCHUPPLANRQ15AMT';
 new_rec.CATCHUPPLANRQ15AMT:=P_CATCHUPPLANRQ15AMT; 
 end if;
 if P_CATCHUPPLANCREDT is not null then
 cur_col_name:='P_CATCHUPPLANCREDT';
 new_rec.CATCHUPPLANCREDT:=P_CATCHUPPLANCREDT; 
 end if;
 if P_CATCHUPPLANMODDT is not null then
 cur_col_name:='P_CATCHUPPLANMODDT';
 new_rec.CATCHUPPLANMODDT:=P_CATCHUPPLANMODDT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVCTCHAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVCTCHTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CATCHUPPLANSTRTDT1=new_rec.CATCHUPPLANSTRTDT1
 ,CATCHUPPLANENDDT1=new_rec.CATCHUPPLANENDDT1
 ,CATCHUPPLANRQ1AMT=new_rec.CATCHUPPLANRQ1AMT
 ,CATCHUPPLANSTRTDT2=new_rec.CATCHUPPLANSTRTDT2
 ,CATCHUPPLANENDDT2=new_rec.CATCHUPPLANENDDT2
 ,CATCHUPPLANRQ2AMT=new_rec.CATCHUPPLANRQ2AMT
 ,CATCHUPPLANSTRTDT3=new_rec.CATCHUPPLANSTRTDT3
 ,CATCHUPPLANENDDT3=new_rec.CATCHUPPLANENDDT3
 ,CATCHUPPLANRQ3AMT=new_rec.CATCHUPPLANRQ3AMT
 ,CATCHUPPLANSTRTDT4=new_rec.CATCHUPPLANSTRTDT4
 ,CATCHUPPLANENDDT4=new_rec.CATCHUPPLANENDDT4
 ,CATCHUPPLANRQ4AMT=new_rec.CATCHUPPLANRQ4AMT
 ,CATCHUPPLANSTRTDT5=new_rec.CATCHUPPLANSTRTDT5
 ,CATCHUPPLANENDDT5=new_rec.CATCHUPPLANENDDT5
 ,CATCHUPPLANRQ5AMT=new_rec.CATCHUPPLANRQ5AMT
 ,CATCHUPPLANSTRTDT6=new_rec.CATCHUPPLANSTRTDT6
 ,CATCHUPPLANENDDT6=new_rec.CATCHUPPLANENDDT6
 ,CATCHUPPLANRQ6AMT=new_rec.CATCHUPPLANRQ6AMT
 ,CATCHUPPLANSTRTDT7=new_rec.CATCHUPPLANSTRTDT7
 ,CATCHUPPLANENDDT7=new_rec.CATCHUPPLANENDDT7
 ,CATCHUPPLANRQ7AMT=new_rec.CATCHUPPLANRQ7AMT
 ,CATCHUPPLANSTRTDT8=new_rec.CATCHUPPLANSTRTDT8
 ,CATCHUPPLANENDDT8=new_rec.CATCHUPPLANENDDT8
 ,CATCHUPPLANRQ8AMT=new_rec.CATCHUPPLANRQ8AMT
 ,CATCHUPPLANSTRTDT9=new_rec.CATCHUPPLANSTRTDT9
 ,CATCHUPPLANENDDT9=new_rec.CATCHUPPLANENDDT9
 ,CATCHUPPLANRQ9AMT=new_rec.CATCHUPPLANRQ9AMT
 ,CATCHUPPLANSTRTDT10=new_rec.CATCHUPPLANSTRTDT10
 ,CATCHUPPLANENDDT10=new_rec.CATCHUPPLANENDDT10
 ,CATCHUPPLANRQ10AMT=new_rec.CATCHUPPLANRQ10AMT
 ,CATCHUPPLANSTRTDT11=new_rec.CATCHUPPLANSTRTDT11
 ,CATCHUPPLANENDDT11=new_rec.CATCHUPPLANENDDT11
 ,CATCHUPPLANRQ11AMT=new_rec.CATCHUPPLANRQ11AMT
 ,CATCHUPPLANSTRTDT12=new_rec.CATCHUPPLANSTRTDT12
 ,CATCHUPPLANENDDT12=new_rec.CATCHUPPLANENDDT12
 ,CATCHUPPLANRQ12AMT=new_rec.CATCHUPPLANRQ12AMT
 ,CATCHUPPLANSTRTDT13=new_rec.CATCHUPPLANSTRTDT13
 ,CATCHUPPLANENDDT13=new_rec.CATCHUPPLANENDDT13
 ,CATCHUPPLANRQ13AMT=new_rec.CATCHUPPLANRQ13AMT
 ,CATCHUPPLANSTRTDT14=new_rec.CATCHUPPLANSTRTDT14
 ,CATCHUPPLANENDDT14=new_rec.CATCHUPPLANENDDT14
 ,CATCHUPPLANRQ14AMT=new_rec.CATCHUPPLANRQ14AMT
 ,CATCHUPPLANSTRTDT15=new_rec.CATCHUPPLANSTRTDT15
 ,CATCHUPPLANENDDT15=new_rec.CATCHUPPLANENDDT15
 ,CATCHUPPLANRQ15AMT=new_rec.CATCHUPPLANRQ15AMT
 ,CATCHUPPLANCREDT=new_rec.CATCHUPPLANCREDT
 ,CATCHUPPLANMODDT=new_rec.CATCHUPPLANMODDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVCTCHUPDTSP build 2018-01-23 08:31:32 '
 ||'while doing update on STGCSA.SOFVCTCHTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCTCHUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVCTCHUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCTCHTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CATCHUPPLANSBANMB)=('||P_CATCHUPPLANSBANMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVCTCHTBL where 1=1
 and CATCHUPPLANSBANMB=P_CATCHUPPLANSBANMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCTCHUPDTSP build 2018-01-23 08:31:32 '
 ||'No STGCSA.SOFVCTCHTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCTCHUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVCTCHUPDTSP build 2018-01-23 08:31:32 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCTCHUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCTCHUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVCTCHUPDTSP build 2018-01-23 08:31:32'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCTCHUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVCTCHUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


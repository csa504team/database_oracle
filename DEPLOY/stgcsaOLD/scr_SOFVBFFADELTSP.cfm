<!--- Saved 01/28/2018 14:42:28. --->
PROCEDURE SOFVBFFADELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ASSUMPKEYLOANNMB CHAR:=null
 ,p_ASSUMPLASTEFFDT DATE:=null
 ,p_ASSUMPMAILADRCTYNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTCD CHAR:=null
 ,p_ASSUMPMAILADRSTR1NM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR2NM VARCHAR2:=null
 ,p_ASSUMPMAILADRZIP4CD CHAR:=null
 ,p_ASSUMPMAILADRZIPCD CHAR:=null
 ,p_ASSUMPNM VARCHAR2:=null
 ,p_ASSUMPNMADRCHNGDT DATE:=null
 ,p_ASSUMPNMMAILADRCHNGIND CHAR:=null
 ,p_ASSUMPOFC CHAR:=null
 ,p_ASSUMPPRGRM CHAR:=null
 ,p_ASSUMPSTATCDOFLOAN CHAR:=null
 ,p_ASSUMPTAXFORMFLD CHAR:=null
 ,p_ASSUMPTAXID CHAR:=null
 ,p_ASSUMPTAXMAINTNDT DATE:=null
 ,p_ASSUMPVERIFICATIONDT DATE:=null
 ,p_ASSUMPVERIFICATIONIND CHAR:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:30:01
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:30:02
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVBFFADELTSP performs DELETE on STGCSA.SOFVBFFATBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ASSUMPKEYLOANNMB, ASSUMPLASTEFFDT
 for P_IDENTIFIER=1 qualification is on LOANNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVBFFATBL%rowtype;
 new_rec STGCSA.SOFVBFFATBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVBFFATBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'Select of STGCSA.SOFVBFFATBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBFFADELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVBFFAAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVBFFATBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'while doing delete on STGCSA.SOFVBFFATBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBFFADELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVBFFADELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBFFATBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ASSUMPKEYLOANNMB)=('||P_ASSUMPKEYLOANNMB||') ''(ASSUMPLASTEFFDT)=('||P_ASSUMPLASTEFFDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVBFFATBL where 1=1
 and ASSUMPKEYLOANNMB=P_ASSUMPKEYLOANNMB and ASSUMPLASTEFFDT=P_ASSUMPLASTEFFDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'No STGCSA.SOFVBFFATBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r1 in (select rowid from STGCSA.SOFVBFFATBL a where 1=1 
 
 and LOANNMB=P_LOANNMB) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO SOFVBFFADELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'Oracle returned error fetching STGCSA.SOFVBFFATBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO SOFVBFFADELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'No STGCSA.SOFVBFFATBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBFFADELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVBFFADELTSP build 2018-01-23 08:30:01 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBFFADELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBFFADELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVBFFADELTSP build 2018-01-23 08:30:01'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBFFADELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVBFFADELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:41:43. --->
PROCEDURE COREEMPUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMAILADR VARCHAR2:=null
 ,p_EMPACTV NUMBER:=null
 ,p_EMPID NUMBER:=null
 ,p_FIRSTNM VARCHAR2:=null
 ,p_FULLNM VARCHAR2:=null
 ,p_LASTNM VARCHAR2:=null
 ,p_NTWKID VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:19:28
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:19:28
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure COREEMPUPDTSP performs UPDATE on STGCSA.COREEMPTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMPID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.COREEMPTBL%rowtype;
 new_rec STGCSA.COREEMPTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.COREEMPTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPUPDTSP build 2018-01-23 08:19:28 '
 ||'Select of STGCSA.COREEMPTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_EMPID is not null then
 cur_col_name:='P_EMPID';
 new_rec.EMPID:=P_EMPID; 
 end if;
 if P_NTWKID is not null then
 cur_col_name:='P_NTWKID';
 new_rec.NTWKID:=P_NTWKID; 
 end if;
 if P_FIRSTNM is not null then
 cur_col_name:='P_FIRSTNM';
 new_rec.FIRSTNM:=P_FIRSTNM; 
 end if;
 if P_LASTNM is not null then
 cur_col_name:='P_LASTNM';
 new_rec.LASTNM:=P_LASTNM; 
 end if;
 if P_FULLNM is not null then
 cur_col_name:='P_FULLNM';
 new_rec.FULLNM:=P_FULLNM; 
 end if;
 if P_EMAILADR is not null then
 cur_col_name:='P_EMAILADR';
 new_rec.EMAILADR:=P_EMAILADR; 
 end if;
 if P_EMPACTV is not null then
 cur_col_name:='P_EMPACTV';
 new_rec.EMPACTV:=P_EMPACTV; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.COREEMPAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.COREEMPTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,NTWKID=new_rec.NTWKID
 ,FIRSTNM=new_rec.FIRSTNM
 ,LASTNM=new_rec.LASTNM
 ,FULLNM=new_rec.FULLNM
 ,EMAILADR=new_rec.EMAILADR
 ,EMPACTV=new_rec.EMPACTV
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In COREEMPUPDTSP build 2018-01-23 08:19:28 '
 ||'while doing update on STGCSA.COREEMPTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint COREEMPUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMPID)=('||P_EMPID||') ';
 select rowid into rec_rowid from STGCSA.COREEMPTBL where 1=1
 and EMPID=P_EMPID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPUPDTSP build 2018-01-23 08:19:28 '
 ||'No STGCSA.COREEMPTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO COREEMPUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In COREEMPUPDTSP build 2018-01-23 08:19:28 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End COREEMPUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'COREEMPUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In COREEMPUPDTSP build 2018-01-23 08:19:28'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO COREEMPUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END COREEMPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:05. --->
PROCEDURE GENEMAILTEMPLATEUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DBMDL VARCHAR2:=null
 ,p_DISTROLISTID NUMBER:=null
 ,p_EMAILCATID NUMBER:=null
 ,p_EMAILTEMPLATEID NUMBER:=null
 ,p_SUBJLINE VARCHAR2:=null
 ,p_TEMPLATEHTML VARCHAR2:=null
 ,p_TEMPLATENM VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:25:04
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:25:04
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure GENEMAILTEMPLATEUPDTSP performs UPDATE on STGCSA.GENEMAILTEMPLATETBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMAILTEMPLATEID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.GENEMAILTEMPLATETBL%rowtype;
 new_rec STGCSA.GENEMAILTEMPLATETBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.GENEMAILTEMPLATETBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILTEMPLATEUPDTSP build 2018-01-23 08:25:04 '
 ||'Select of STGCSA.GENEMAILTEMPLATETBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILTEMPLATEUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_EMAILTEMPLATEID is not null then
 cur_col_name:='P_EMAILTEMPLATEID';
 new_rec.EMAILTEMPLATEID:=P_EMAILTEMPLATEID; 
 end if;
 if P_EMAILCATID is not null then
 cur_col_name:='P_EMAILCATID';
 new_rec.EMAILCATID:=P_EMAILCATID; 
 end if;
 if P_DBMDL is not null then
 cur_col_name:='P_DBMDL';
 new_rec.DBMDL:=P_DBMDL; 
 end if;
 if P_TEMPLATENM is not null then
 cur_col_name:='P_TEMPLATENM';
 new_rec.TEMPLATENM:=P_TEMPLATENM; 
 end if;
 if P_DISTROLISTID is not null then
 cur_col_name:='P_DISTROLISTID';
 new_rec.DISTROLISTID:=P_DISTROLISTID; 
 end if;
 if P_SUBJLINE is not null then
 cur_col_name:='P_SUBJLINE';
 new_rec.SUBJLINE:=P_SUBJLINE; 
 end if;
 if P_TEMPLATEHTML is not null then
 cur_col_name:='P_TEMPLATEHTML';
 new_rec.TEMPLATEHTML:=P_TEMPLATEHTML; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.GENEMAILTEMPLATEAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENEMAILTEMPLATETBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,EMAILCATID=new_rec.EMAILCATID
 ,DBMDL=new_rec.DBMDL
 ,TEMPLATENM=new_rec.TEMPLATENM
 ,DISTROLISTID=new_rec.DISTROLISTID
 ,SUBJLINE=new_rec.SUBJLINE
 ,TEMPLATEHTML=new_rec.TEMPLATEHTML
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In GENEMAILTEMPLATEUPDTSP build 2018-01-23 08:25:04 '
 ||'while doing update on STGCSA.GENEMAILTEMPLATETBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILTEMPLATEUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENEMAILTEMPLATEUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_DBMDL:=P_DBMDL; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMAILTEMPLATEID)=('||P_EMAILTEMPLATEID||') ';
 select rowid into rec_rowid from STGCSA.GENEMAILTEMPLATETBL where 1=1
 and EMAILTEMPLATEID=P_EMAILTEMPLATEID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILTEMPLATEUPDTSP build 2018-01-23 08:25:04 '
 ||'No STGCSA.GENEMAILTEMPLATETBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILTEMPLATEUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In GENEMAILTEMPLATEUPDTSP build 2018-01-23 08:25:04 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_DBMDL:=new_rec.DBMDL; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILTEMPLATEUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILTEMPLATEUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In GENEMAILTEMPLATEUPDTSP build 2018-01-23 08:25:04'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILTEMPLATEUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END GENEMAILTEMPLATEUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by tmathias. 
DATE:				01/31/2018.
DESCRIPTION:		Standardized call to GENDBFNCTNUPDTSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	01/31/2018, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 46)
	and	(Left(CGI.Script_Name,    46) is "/cfincludes/oracle/stgcsa/spc_GENDBFNCTNUPDTSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"		default="">
<cfparam name="Variables.ErrMsg"			default="">
<cfparam name="Variables.SkippedSpcs"		default="">
<cfparam name="Variables.TxnErr"			default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs			= ListAppend(Variables.SkippedSpcs, "GENDBFNCTNUPDTSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"		default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"		default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"		default="call GENDBFNCTNUPDTSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName			= "GENDBFNCTNUPDTSP">
	<cfparam name="Variables.retval"		default="">
	<cfparam name="Variables.errval"		default="">
	<cfparam name="Variables.errmsg"		default="">
	<cfparam name="Variables.identifier"	default="0">
	<cfparam name="Variables.userid"		default="">
	<cfparam name="Variables.FNCTNACTV"		default="">
	<cfparam name="Variables.FNCTNDESC"		default="">
	<cfparam name="Variables.FNCTNID"		default="">
	<cfparam name="Variables.FNCTNNM"		default="">
	<cfparam name="Variables.TIMESTAMPFLD"	default="">
	<cftry>
		<cfstoredproc procedure="STGCSA.GENDBFNCTNUPDTSP" datasource="#Variables.db#"
											username="#Variables.username#" password="#Variables.password#">
		<cfprocparam		type="Out"		dbvarname=":p_retval"
											variable="Variables.retval"
											cfsqltype="CF_SQL_NUMBER">
		<cfprocparam		type="Out"		dbvarname=":p_errval"
											variable="Variables.errval"
											cfsqltype="CF_SQL_NUMBER">
		<cfprocparam		type="Out"		dbvarname=":p_errmsg"
											variable="Variables.errmsg"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfif Len(Variables.identifier) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_identifier"
											value="#Variables.identifier#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_identifier"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.userid) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_userid"
											value="#Variables.userid#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_userid"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.FNCTNACTV) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNACTV"
											value="#Variables.FNCTNACTV#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNACTV"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.FNCTNDESC) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNDESC"
											value="#Variables.FNCTNDESC#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNDESC"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.FNCTNID) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNID"
											value="#Variables.FNCTNID#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNID"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.FNCTNNM) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNNM"
											value="#Variables.FNCTNNM#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_FNCTNNM"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.TIMESTAMPFLD) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_TIMESTAMPFLD"
											value="#Variables.TIMESTAMPFLD#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_TIMESTAMPFLD"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


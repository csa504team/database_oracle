<!--- Saved 01/28/2018 14:42:31. --->
PROCEDURE SOFVBGW9_STATUSVAUDTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2,
 action_cd char,
 AUDTEDTCD char,
 old_rec STGCSA.SOFVBGW9TBL%rowtype,
 new_rec STGCSA.SOFVBGW9TBL%rowtype) as
 /*
 Created on: 2018-01-23 08:30:47
 Created by: GENR
 Crerated from template auditme_template v2.02 20 Dec 2017 on 2018-01-23 08:30:47
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Purpose : Write to SOFVAUDTTBL for specific changed columns in
 STGCSA.SOFVBGW9_STATUSV (SOFVBGW9TBL)
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 aud_rec STGCSA.SOFVAUDTTBL%rowtype;
 n number:=0;
 e number:=0;
 tsp timestamp;
 key_from_new_rec STGCSA.SOFVAUDTTBL.AUDTRECRDKEY%type;
 begin
 savepoint SOFVBGW9_STATUSVAUDTSP;
 -- hopeful return
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=null;
 -- setup log rec
 select systimestamp,sysdate,sys_context('userenv','session_user')
 into aud_rec.audttransdt, aud_rec.CREATDT, aud_rec.creatuserid from dual;
 aud_rec.LASTUPDTDT:=aud_rec.CREATDT;
 aud_rec.lastupdtuserid:=aud_rec.creatuserid;
 aud_rec.audtseqnmb:=0;
 aud_rec.AUDTOPRTRINITIAL:=' ';
 aud_rec.AUDTTRMNLID:=' ';
 aud_rec.AUDTACTNCD:=action_cd;
 aud_rec.AUDTEDTCD:=AUDTEDTCD;
 aud_rec.AUDTUSERID:=p_userid;
 -- save key value... if add or del then one rec is nulls
 aud_rec.audtrecrdkey:=rpad(substr(''
 ||old_rec.LOANNMB,1,15),15);
 key_from_new_rec:=rpad(substr(''
 ||new_rec.LOANNMB,1,15),15);
 if aud_rec.audtrecrdkey is null then
 aud_rec.audtrecrdkey:=key_from_new_rec;
 end if;
 -- are any columns auditing cares about different?
 if old_rec.W9NM=new_rec.W9NM 
 or (old_rec.W9NM is null and new_rec.W9NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9NM; 
 end if; 
 if new_rec.W9NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9NAME1 ';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.W9MAILADRSTR1NM=new_rec.W9MAILADRSTR1NM 
 or (old_rec.W9MAILADRSTR1NM is null and new_rec.W9MAILADRSTR1NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9MAILADRSTR1NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9MAILADRSTR1NM; 
 end if; 
 if new_rec.W9MAILADRSTR1NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9MAILADRSTR1NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9STREET';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.W9MAILADRCTYNM=new_rec.W9MAILADRCTYNM 
 or (old_rec.W9MAILADRCTYNM is null and new_rec.W9MAILADRCTYNM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9MAILADRCTYNM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9MAILADRCTYNM; 
 end if; 
 if new_rec.W9MAILADRCTYNM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9MAILADRCTYNM; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9CITY ';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.W9MAILADRSTCD=new_rec.W9MAILADRSTCD 
 or (old_rec.W9MAILADRSTCD is null and new_rec.W9MAILADRSTCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9MAILADRSTCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9MAILADRSTCD; 
 end if; 
 if new_rec.W9MAILADRSTCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9MAILADRSTCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9STATE ';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.W9MAILADRZIPCD=new_rec.W9MAILADRZIPCD 
 or (old_rec.W9MAILADRZIPCD is null and new_rec.W9MAILADRZIPCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9MAILADRZIPCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9MAILADRZIPCD; 
 end if; 
 if new_rec.W9MAILADRZIPCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9MAILADRZIPCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9ZIP1 ';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.W9MAILADRZIP4CD=new_rec.W9MAILADRZIP4CD 
 or (old_rec.W9MAILADRZIP4CD is null and new_rec.W9MAILADRZIP4CD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.W9MAILADRZIP4CD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.W9MAILADRZIP4CD; 
 end if; 
 if new_rec.W9MAILADRZIP4CD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.W9MAILADRZIP4CD; 
 end if;
 
 aud_rec.AUDTFLDNM:='W9ZIP2 ';
 aud_rec.audtchngfile:='BGW9FILE';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 p_retval:=aud_rec.audtseqnmb;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:='In OXH in SOFVBGW9_STATUSVAUDTSP Build 2018-01-23 08:30:47 got: '||sqlerrm(SQLCODE);
 ROLLBACK TO SOFVBGW9_STATUSVAUDTSP;
 --
 END SOFVBGW9_STATUSVAUDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:41:56. --->
PROCEDURE GENDBFNCTNINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_FNCTNID NUMBER:=null
 ,p_FNCTNNM VARCHAR2:=null
 ,p_FNCTNDESC VARCHAR2:=null
 ,p_FNCTNACTV NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:22:50
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:22:50
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.GENDBFNCTNTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.GENDBFNCTNTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint GENDBFNCTNINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_FNCTNID';
 new_rec.FNCTNID:=runtime.validate_column_value(P_FNCTNID,
 'STGCSA','GENDBFNCTNTBL','FNCTNID','NUMBER',22);
 cur_col_name:='P_FNCTNNM';
 new_rec.FNCTNNM:=runtime.validate_column_value(P_FNCTNNM,
 'STGCSA','GENDBFNCTNTBL','FNCTNNM','VARCHAR2',80);
 cur_col_name:='P_FNCTNDESC';
 new_rec.FNCTNDESC:=runtime.validate_column_value(P_FNCTNDESC,
 'STGCSA','GENDBFNCTNTBL','FNCTNDESC','VARCHAR2',100);
 cur_col_name:='P_FNCTNACTV';
 new_rec.FNCTNACTV:=runtime.validate_column_value(P_FNCTNACTV,
 'STGCSA','GENDBFNCTNTBL','FNCTNACTV','NUMBER',22);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','GENDBFNCTNTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO GENDBFNCTNINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.GENDBFNCTNAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.GENDBFNCTNTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO GENDBFNCTNINSTSP;
 p_errmsg:='Got dupe key on insert into GENDBFNCTNTBL for key '
 ||'(FNCTNID)=('||new_rec.FNCTNID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENDBFNCTNINSTSP build 2018-01-23 08:22:49';
 ROLLBACK TO GENDBFNCTNINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENDBFNCTNINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENDBFNCTNINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENDBFNCTNINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in GENDBFNCTNINSTSP build 2018-01-23 08:22:49'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENDBFNCTNINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END GENDBFNCTNINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


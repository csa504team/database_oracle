<!--- Saved 01/28/2018 14:41:58. --->
PROCEDURE GENEMAILARCHVUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DBMDL VARCHAR2:=null
 ,p_EMAILBODY VARCHAR2:=null
 ,p_EMAILCATID NUMBER:=null
 ,p_EMAILCC VARCHAR2:=null
 ,p_EMAILFROMADR VARCHAR2:=null
 ,p_EMAILFROMNM VARCHAR2:=null
 ,p_EMAILID NUMBER:=null
 ,p_EMAILSENTDT DATE:=null
 ,p_EMAILSUBJ VARCHAR2:=null
 ,p_EMAILTO VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_MSGFILENM VARCHAR2:=null
 ,p_RELTDTBLNM VARCHAR2:=null
 ,p_RELTDTBLREF NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:23:21
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:23:21
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure GENEMAILARCHVUPDTSP performs UPDATE on STGCSA.GENEMAILARCHVTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMAILID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.GENEMAILARCHVTBL%rowtype;
 new_rec STGCSA.GENEMAILARCHVTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.GENEMAILARCHVTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILARCHVUPDTSP build 2018-01-23 08:23:21 '
 ||'Select of STGCSA.GENEMAILARCHVTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_EMAILID is not null then
 cur_col_name:='P_EMAILID';
 new_rec.EMAILID:=P_EMAILID; 
 end if;
 if P_DBMDL is not null then
 cur_col_name:='P_DBMDL';
 new_rec.DBMDL:=P_DBMDL; 
 end if;
 if P_RELTDTBLNM is not null then
 cur_col_name:='P_RELTDTBLNM';
 new_rec.RELTDTBLNM:=P_RELTDTBLNM; 
 end if;
 if P_RELTDTBLREF is not null then
 cur_col_name:='P_RELTDTBLREF';
 new_rec.RELTDTBLREF:=P_RELTDTBLREF; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_EMAILCATID is not null then
 cur_col_name:='P_EMAILCATID';
 new_rec.EMAILCATID:=P_EMAILCATID; 
 end if;
 if P_EMAILTO is not null then
 cur_col_name:='P_EMAILTO';
 new_rec.EMAILTO:=P_EMAILTO; 
 end if;
 if P_EMAILFROMNM is not null then
 cur_col_name:='P_EMAILFROMNM';
 new_rec.EMAILFROMNM:=P_EMAILFROMNM; 
 end if;
 if P_EMAILFROMADR is not null then
 cur_col_name:='P_EMAILFROMADR';
 new_rec.EMAILFROMADR:=P_EMAILFROMADR; 
 end if;
 if P_EMAILCC is not null then
 cur_col_name:='P_EMAILCC';
 new_rec.EMAILCC:=P_EMAILCC; 
 end if;
 if P_EMAILSUBJ is not null then
 cur_col_name:='P_EMAILSUBJ';
 new_rec.EMAILSUBJ:=P_EMAILSUBJ; 
 end if;
 if P_EMAILBODY is not null then
 cur_col_name:='P_EMAILBODY';
 new_rec.EMAILBODY:=P_EMAILBODY; 
 end if;
 if P_EMAILSENTDT is not null then
 cur_col_name:='P_EMAILSENTDT';
 new_rec.EMAILSENTDT:=P_EMAILSENTDT; 
 end if;
 if P_MSGFILENM is not null then
 cur_col_name:='P_MSGFILENM';
 new_rec.MSGFILENM:=P_MSGFILENM; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.GENEMAILARCHVAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENEMAILARCHVTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,DBMDL=new_rec.DBMDL
 ,RELTDTBLNM=new_rec.RELTDTBLNM
 ,RELTDTBLREF=new_rec.RELTDTBLREF
 ,LOANNMB=new_rec.LOANNMB
 ,EMAILCATID=new_rec.EMAILCATID
 ,EMAILTO=new_rec.EMAILTO
 ,EMAILFROMNM=new_rec.EMAILFROMNM
 ,EMAILFROMADR=new_rec.EMAILFROMADR
 ,EMAILCC=new_rec.EMAILCC
 ,EMAILSUBJ=new_rec.EMAILSUBJ
 ,EMAILBODY=new_rec.EMAILBODY
 ,EMAILSENTDT=new_rec.EMAILSENTDT
 ,MSGFILENM=new_rec.MSGFILENM
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In GENEMAILARCHVUPDTSP build 2018-01-23 08:23:21 '
 ||'while doing update on STGCSA.GENEMAILARCHVTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENEMAILARCHVUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_DBMDL:=P_DBMDL; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMAILID)=('||P_EMAILID||') ';
 select rowid into rec_rowid from STGCSA.GENEMAILARCHVTBL where 1=1
 and EMAILID=P_EMAILID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILARCHVUPDTSP build 2018-01-23 08:23:21 '
 ||'No STGCSA.GENEMAILARCHVTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In GENEMAILARCHVUPDTSP build 2018-01-23 08:23:21 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_DBMDL:=new_rec.DBMDL; log_LOANNMB:=new_rec.LOANNMB; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILARCHVUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILARCHVUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In GENEMAILARCHVUPDTSP build 2018-01-23 08:23:21'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END GENEMAILARCHVUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


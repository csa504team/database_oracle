<!--- Saved 01/28/2018 14:42:55. --->
PROCEDURE TEMPGETOGERRINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_GETOPENGNTYERRID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_OPENGNTYERRAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:37:13
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:37:13
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.TEMPGETOGERRTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.TEMPGETOGERRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint TEMPGETOGERRINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_GETOPENGNTYERRID';
 new_rec.GETOPENGNTYERRID:=runtime.validate_column_value(P_GETOPENGNTYERRID,
 'STGCSA','TEMPGETOGERRTBL','GETOPENGNTYERRID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','TEMPGETOGERRTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_OPENGNTYERRAMT';
 new_rec.OPENGNTYERRAMT:=runtime.validate_column_value(P_OPENGNTYERRAMT,
 'STGCSA','TEMPGETOGERRTBL','OPENGNTYERRAMT','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO TEMPGETOGERRINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.TEMPGETOGERRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.TEMPGETOGERRTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO TEMPGETOGERRINSTSP;
 p_errmsg:='Got dupe key on insert into TEMPGETOGERRTBL for key '
 ||'(GETOPENGNTYERRID)=('||new_rec.GETOPENGNTYERRID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in TEMPGETOGERRINSTSP build 2018-01-23 08:37:13';
 ROLLBACK TO TEMPGETOGERRINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO TEMPGETOGERRINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPGETOGERRINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'TEMPGETOGERRINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in TEMPGETOGERRINSTSP build 2018-01-23 08:37:13'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO TEMPGETOGERRINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END TEMPGETOGERRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:49. --->
PROCEDURE SOFVPYMTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_PYMTPOSTPYMTDT DATE:=null
 ,p_PYMTPOSTPYMTTYP CHAR:=null
 ,p_PYMTPOSTDUEAMT NUMBER:=null
 ,p_PYMTPOSTREMITTEDAMT NUMBER:=null
 ,p_PYMTPOSTPYMTNMB CHAR:=null
 ,p_PYMTPOSTCSAPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCSAPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCSADUEAMT NUMBER:=null
 ,p_PYMTPOSTCSAAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCSAACTUALINITFEEAMT NUMBER:=null
 ,p_PYMTPOSTCSAREPDAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEREPDAMT NUMBER:=null
 ,p_PYMTPOSTCDCPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTCDCPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTCDCDUEAMT NUMBER:=null
 ,p_PYMTPOSTCDCAPPLAMT NUMBER:=null
 ,p_PYMTPOSTCDCREPDAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEAPPLAMT NUMBER:=null
 ,p_PYMTPOSTINTPAIDSTARTDTX CHAR:=null
 ,p_PYMTPOSTINTPAIDTHRUDTX CHAR:=null
 ,p_PYMTPOSTINTBASISCALC NUMBER:=null
 ,p_PYMTPOSTINTRTUSEDPCT NUMBER:=null
 ,p_PYMTPOSTINTBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTINTDUEAMT NUMBER:=null
 ,p_PYMTPOSTINTAPPLAMT NUMBER:=null
 ,p_PYMTPOSTINTACTUALBASISDAYS NUMBER:=null
 ,p_PYMTPOSTINTACCRADJAMT NUMBER:=null
 ,p_PYMTPOSTINTRJCTAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEESTARTDTX CHAR:=null
 ,p_PYMTPOSTPRINDUEAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALUSEDAMT NUMBER:=null
 ,p_PYMTPOSTPRINAPPLAMT NUMBER:=null
 ,p_PYMTPOSTPRINBALLEFTAMT NUMBER:=null
 ,p_PYMTPOSTPRINRJCTAMT NUMBER:=null
 ,p_PYMTPOSTGNTYREPAYAMT NUMBER:=null
 ,p_PYMTPOSTSBAFEEENDDTX CHAR:=null
 ,p_PYMTPOSTPAIDBYCOLSONDTX CHAR:=null
 ,p_PYMTPOSTLPYMTAMT NUMBER:=null
 ,p_PYMTPOSTESCRCPTSTDAMT NUMBER:=null
 ,p_PYMTPOSTLATEFEEASSESSAMT NUMBER:=null
 ,p_PYMTPOSTCDCDISBAMT NUMBER:=null
 ,p_PYMTPOSTCDCSBADISBAMT NUMBER:=null
 ,p_PYMTPOSTXADJCD CHAR:=null
 ,p_PYMTPOSTLEFTOVRAMT NUMBER:=null
 ,p_PYMTPOSTRJCTCD CHAR:=null
 ,p_PYMTPOSTCMNT VARCHAR2:=null
 ,p_PYMTPOSTREMITTEDRJCTAMT NUMBER:=null
 ,p_PYMTPOSTLEFTOVRRJCTAMT NUMBER:=null
 ,p_PYMTPOSTRJCTDT DATE:=null
 ,p_PYMTPOSTLATEFEEPAIDAMT NUMBER:=null
 ,p_PYMTPOSTINTWAIVEDAMT NUMBER:=null
 ,p_PYMTPOSTPREPAYAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:35:39
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:35:39
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVPYMTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVPYMTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVPYMTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVPYMTTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVPYMTTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_PYMTPOSTPYMTDT';
 new_rec.PYMTPOSTPYMTDT:=runtime.validate_column_value(P_PYMTPOSTPYMTDT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPYMTDT','DATE',7);
 cur_col_name:='P_PYMTPOSTPYMTTYP';
 new_rec.PYMTPOSTPYMTTYP:=runtime.validate_column_value(P_PYMTPOSTPYMTTYP,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPYMTTYP','CHAR',1);
 cur_col_name:='P_PYMTPOSTDUEAMT';
 new_rec.PYMTPOSTDUEAMT:=runtime.validate_column_value(P_PYMTPOSTDUEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTDUEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTREMITTEDAMT';
 new_rec.PYMTPOSTREMITTEDAMT:=runtime.validate_column_value(P_PYMTPOSTREMITTEDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTREMITTEDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPYMTNMB';
 new_rec.PYMTPOSTPYMTNMB:=runtime.validate_column_value(P_PYMTPOSTPYMTNMB,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPYMTNMB','CHAR',3);
 cur_col_name:='P_PYMTPOSTCSAPAIDSTARTDTX';
 new_rec.PYMTPOSTCSAPAIDSTARTDTX:=runtime.validate_column_value(P_PYMTPOSTCSAPAIDSTARTDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSAPAIDSTARTDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTCSAPAIDTHRUDTX';
 new_rec.PYMTPOSTCSAPAIDTHRUDTX:=runtime.validate_column_value(P_PYMTPOSTCSAPAIDTHRUDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSAPAIDTHRUDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTCSADUEAMT';
 new_rec.PYMTPOSTCSADUEAMT:=runtime.validate_column_value(P_PYMTPOSTCSADUEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSADUEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCSAAPPLAMT';
 new_rec.PYMTPOSTCSAAPPLAMT:=runtime.validate_column_value(P_PYMTPOSTCSAAPPLAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSAAPPLAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCSAACTUALINITFEEAMT';
 new_rec.PYMTPOSTCSAACTUALINITFEEAMT:=runtime.validate_column_value(P_PYMTPOSTCSAACTUALINITFEEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSAACTUALINITFEEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCSAREPDAMT';
 new_rec.PYMTPOSTCSAREPDAMT:=runtime.validate_column_value(P_PYMTPOSTCSAREPDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCSAREPDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTSBAFEEREPDAMT';
 new_rec.PYMTPOSTSBAFEEREPDAMT:=runtime.validate_column_value(P_PYMTPOSTSBAFEEREPDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTSBAFEEREPDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCDCPAIDSTARTDTX';
 new_rec.PYMTPOSTCDCPAIDSTARTDTX:=runtime.validate_column_value(P_PYMTPOSTCDCPAIDSTARTDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCPAIDSTARTDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTCDCPAIDTHRUDTX';
 new_rec.PYMTPOSTCDCPAIDTHRUDTX:=runtime.validate_column_value(P_PYMTPOSTCDCPAIDTHRUDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCPAIDTHRUDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTCDCDUEAMT';
 new_rec.PYMTPOSTCDCDUEAMT:=runtime.validate_column_value(P_PYMTPOSTCDCDUEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCDUEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCDCAPPLAMT';
 new_rec.PYMTPOSTCDCAPPLAMT:=runtime.validate_column_value(P_PYMTPOSTCDCAPPLAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCAPPLAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCDCREPDAMT';
 new_rec.PYMTPOSTCDCREPDAMT:=runtime.validate_column_value(P_PYMTPOSTCDCREPDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCREPDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTSBAFEEAMT';
 new_rec.PYMTPOSTSBAFEEAMT:=runtime.validate_column_value(P_PYMTPOSTSBAFEEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTSBAFEEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTSBAFEEAPPLAMT';
 new_rec.PYMTPOSTSBAFEEAPPLAMT:=runtime.validate_column_value(P_PYMTPOSTSBAFEEAPPLAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTSBAFEEAPPLAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTPAIDSTARTDTX';
 new_rec.PYMTPOSTINTPAIDSTARTDTX:=runtime.validate_column_value(P_PYMTPOSTINTPAIDSTARTDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTPAIDSTARTDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTINTPAIDTHRUDTX';
 new_rec.PYMTPOSTINTPAIDTHRUDTX:=runtime.validate_column_value(P_PYMTPOSTINTPAIDTHRUDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTPAIDTHRUDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTINTBASISCALC';
 new_rec.PYMTPOSTINTBASISCALC:=runtime.validate_column_value(P_PYMTPOSTINTBASISCALC,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTBASISCALC','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTRTUSEDPCT';
 new_rec.PYMTPOSTINTRTUSEDPCT:=runtime.validate_column_value(P_PYMTPOSTINTRTUSEDPCT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTRTUSEDPCT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTBALUSEDAMT';
 new_rec.PYMTPOSTINTBALUSEDAMT:=runtime.validate_column_value(P_PYMTPOSTINTBALUSEDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTBALUSEDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTDUEAMT';
 new_rec.PYMTPOSTINTDUEAMT:=runtime.validate_column_value(P_PYMTPOSTINTDUEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTDUEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTAPPLAMT';
 new_rec.PYMTPOSTINTAPPLAMT:=runtime.validate_column_value(P_PYMTPOSTINTAPPLAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTAPPLAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTACTUALBASISDAYS';
 new_rec.PYMTPOSTINTACTUALBASISDAYS:=runtime.validate_column_value(P_PYMTPOSTINTACTUALBASISDAYS,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTACTUALBASISDAYS','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTACCRADJAMT';
 new_rec.PYMTPOSTINTACCRADJAMT:=runtime.validate_column_value(P_PYMTPOSTINTACCRADJAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTACCRADJAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTRJCTAMT';
 new_rec.PYMTPOSTINTRJCTAMT:=runtime.validate_column_value(P_PYMTPOSTINTRJCTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTRJCTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTSBAFEESTARTDTX';
 new_rec.PYMTPOSTSBAFEESTARTDTX:=runtime.validate_column_value(P_PYMTPOSTSBAFEESTARTDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTSBAFEESTARTDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTPRINDUEAMT';
 new_rec.PYMTPOSTPRINDUEAMT:=runtime.validate_column_value(P_PYMTPOSTPRINDUEAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPRINDUEAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPRINBALUSEDAMT';
 new_rec.PYMTPOSTPRINBALUSEDAMT:=runtime.validate_column_value(P_PYMTPOSTPRINBALUSEDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPRINBALUSEDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPRINAPPLAMT';
 new_rec.PYMTPOSTPRINAPPLAMT:=runtime.validate_column_value(P_PYMTPOSTPRINAPPLAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPRINAPPLAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPRINBALLEFTAMT';
 new_rec.PYMTPOSTPRINBALLEFTAMT:=runtime.validate_column_value(P_PYMTPOSTPRINBALLEFTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPRINBALLEFTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPRINRJCTAMT';
 new_rec.PYMTPOSTPRINRJCTAMT:=runtime.validate_column_value(P_PYMTPOSTPRINRJCTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPRINRJCTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTGNTYREPAYAMT';
 new_rec.PYMTPOSTGNTYREPAYAMT:=runtime.validate_column_value(P_PYMTPOSTGNTYREPAYAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTGNTYREPAYAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTSBAFEEENDDTX';
 new_rec.PYMTPOSTSBAFEEENDDTX:=runtime.validate_column_value(P_PYMTPOSTSBAFEEENDDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTSBAFEEENDDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTPAIDBYCOLSONDTX';
 new_rec.PYMTPOSTPAIDBYCOLSONDTX:=runtime.validate_column_value(P_PYMTPOSTPAIDBYCOLSONDTX,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPAIDBYCOLSONDTX','CHAR',8);
 cur_col_name:='P_PYMTPOSTLPYMTAMT';
 new_rec.PYMTPOSTLPYMTAMT:=runtime.validate_column_value(P_PYMTPOSTLPYMTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTLPYMTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTESCRCPTSTDAMT';
 new_rec.PYMTPOSTESCRCPTSTDAMT:=runtime.validate_column_value(P_PYMTPOSTESCRCPTSTDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTESCRCPTSTDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTLATEFEEASSESSAMT';
 new_rec.PYMTPOSTLATEFEEASSESSAMT:=runtime.validate_column_value(P_PYMTPOSTLATEFEEASSESSAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTLATEFEEASSESSAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCDCDISBAMT';
 new_rec.PYMTPOSTCDCDISBAMT:=runtime.validate_column_value(P_PYMTPOSTCDCDISBAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCDISBAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTCDCSBADISBAMT';
 new_rec.PYMTPOSTCDCSBADISBAMT:=runtime.validate_column_value(P_PYMTPOSTCDCSBADISBAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCDCSBADISBAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTXADJCD';
 new_rec.PYMTPOSTXADJCD:=runtime.validate_column_value(P_PYMTPOSTXADJCD,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTXADJCD','CHAR',2);
 cur_col_name:='P_PYMTPOSTLEFTOVRAMT';
 new_rec.PYMTPOSTLEFTOVRAMT:=runtime.validate_column_value(P_PYMTPOSTLEFTOVRAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTLEFTOVRAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTRJCTCD';
 new_rec.PYMTPOSTRJCTCD:=runtime.validate_column_value(P_PYMTPOSTRJCTCD,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTRJCTCD','CHAR',3);
 cur_col_name:='P_PYMTPOSTCMNT';
 new_rec.PYMTPOSTCMNT:=runtime.validate_column_value(P_PYMTPOSTCMNT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTCMNT','VARCHAR2',80);
 cur_col_name:='P_PYMTPOSTREMITTEDRJCTAMT';
 new_rec.PYMTPOSTREMITTEDRJCTAMT:=runtime.validate_column_value(P_PYMTPOSTREMITTEDRJCTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTREMITTEDRJCTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTLEFTOVRRJCTAMT';
 new_rec.PYMTPOSTLEFTOVRRJCTAMT:=runtime.validate_column_value(P_PYMTPOSTLEFTOVRRJCTAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTLEFTOVRRJCTAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTRJCTDT';
 new_rec.PYMTPOSTRJCTDT:=runtime.validate_column_value(P_PYMTPOSTRJCTDT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTRJCTDT','DATE',7);
 cur_col_name:='P_PYMTPOSTLATEFEEPAIDAMT';
 new_rec.PYMTPOSTLATEFEEPAIDAMT:=runtime.validate_column_value(P_PYMTPOSTLATEFEEPAIDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTLATEFEEPAIDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTINTWAIVEDAMT';
 new_rec.PYMTPOSTINTWAIVEDAMT:=runtime.validate_column_value(P_PYMTPOSTINTWAIVEDAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTINTWAIVEDAMT','NUMBER',22);
 cur_col_name:='P_PYMTPOSTPREPAYAMT';
 new_rec.PYMTPOSTPREPAYAMT:=runtime.validate_column_value(P_PYMTPOSTPREPAYAMT,
 'STGCSA','SOFVPYMTTBL','PYMTPOSTPREPAYAMT','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVPYMTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVPYMTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVPYMTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 ROLLBACK TO SOFVPYMTINSTSP;
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 p_errmsg:='Got dupe key on insert into SOFVPYMTTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ' ||'(PYMTPOSTPYMTDT)=('||new_rec.PYMTPOSTPYMTDT||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVPYMTINSTSP build 2018-01-23 08:35:39';
 ROLLBACK TO SOFVPYMTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVPYMTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVPYMTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVPYMTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVPYMTINSTSP build 2018-01-23 08:35:39'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVPYMTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVPYMTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


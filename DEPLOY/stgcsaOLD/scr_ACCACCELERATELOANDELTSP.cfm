<!--- Saved 01/28/2018 14:41:26. --->
PROCEDURE ACCACCELERATELOANDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCELERATECMNT VARCHAR2:=null
 ,p_ACCELERATNID NUMBER:=null
 ,p_CDCNMB CHAR:=null
 ,p_LOANNMB CHAR:=null
 ,p_MFCDCDUEFROMBORRAMT NUMBER:=null
 ,p_MFCDCFEEAMT NUMBER:=null
 ,p_MFCDCMODELNQ NUMBER:=null
 ,p_MFCDCMOFEEAMT NUMBER:=null
 ,p_MFCDCPAIDTHRUDT CHAR:=null
 ,p_MFCSADUEFROMBORRAMT NUMBER:=null
 ,p_MFCSAFEEAMT NUMBER:=null
 ,p_MFCSAMODELNQ NUMBER:=null
 ,p_MFCSAMOFEEAMT NUMBER:=null
 ,p_MFCSAPAIDTHRUDT CHAR:=null
 ,p_MFCURCMNT VARCHAR2:=null
 ,p_MFCURSTATID NUMBER:=null
 ,p_MFDBENTRAMT NUMBER:=null
 ,p_MFESCROWPRIORPYMTAMT NUMBER:=null
 ,p_MFINITIALCMNT VARCHAR2:=null
 ,p_MFINITIALSCRAPEDT DATE:=null
 ,p_MFINITIALSTATID NUMBER:=null
 ,p_MFINTRTPCT NUMBER:=null
 ,p_MFISSDT DATE:=null
 ,p_MFLASTPYMTDT DATE:=null
 ,p_MFLASTSCRAPEDT DATE:=null
 ,p_MFLASTUPDTDT DATE:=null
 ,p_MFLOANMATDT DATE:=null
 ,p_MFORGLPRINAMT NUMBER:=null
 ,p_MFPREVCMNT VARCHAR2:=null
 ,p_MFPREVSTATID NUMBER:=null
 ,p_MFTOBECURAMT NUMBER:=null
 ,p_PPACDCNMB CHAR:=null
 ,p_PPADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_PPADUEATSEMIANAMT NUMBER:=null
 ,p_PPAENTRYDT DATE:=null
 ,p_PPAGROUP VARCHAR2:=null
 ,p_PPAINTDUEATSEMIANAMT NUMBER:=null
 ,p_PPAORGLPRINAMT NUMBER:=null
 ,p_PRIORACCELERATN NUMBER:=null
 ,p_SBAACCELAMT NUMBER:=null
 ,p_SBAACCRINTDUEAMT NUMBER:=null
 ,p_SBACDCFEEAMT NUMBER:=null
 ,p_SBACSAFEEAMT NUMBER:=null
 ,p_SBADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_SBADUEDT DATE:=null
 ,p_SBAENTRYDT DATE:=null
 ,p_SBAESCROWPRIORPYMTAMT NUMBER:=null
 ,p_SBAEXCESSAMT NUMBER:=null
 ,p_SBAINTDUEATSEMIANAMT NUMBER:=null
 ,p_SBALASTNOTEBALAMT NUMBER:=null
 ,p_SBALASTPYMTDT DATE:=null
 ,p_SBALASTTRNSCRPTIMPTDT DATE:=null
 ,p_SBANOTERTPCT NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ,p_SERVCNTRID VARCHAR2:=null
 ,p_STATID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 16:52:18
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 16:52:18
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure ACCACCELERATELOANDELTSP performs DELETE on STGCSA.ACCACCELERATELOANTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ACCELERATNID
 for P_IDENTIFIER=1 qualification is on SEMIANDT, STATID
 for P_IDENTIFIER=2 qualification is on STATID =49, SEMIANDT
 for P_IDENTIFIER=3 qualification is on STATID =50, SEMIANDT
 for P_IDENTIFIER=4 qualification is on TRANSID
 for P_IDENTIFIER=5 qualification is on LOANNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 new_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.ACCACCELERATELOANTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Select of STGCSA.ACCACCELERATELOANTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.ACCACCELERATELOANAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.ACCACCELERATELOANTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'while doing delete on STGCSA.ACCACCELERATELOANTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCACCELERATELOANDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ACCELERATNID)=('||P_ACCELERATNID||') ';
 select rowid into rec_rowid from STGCSA.ACCACCELERATELOANTBL where 1=1
 and ACCELERATNID=P_ACCELERATNID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID)=('||P_STATID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 
 and SEMIANDT=P_SEMIANDT
 and STATID=P_STATID) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to delete one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID =49) '||'';
 begin
 for r2 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =49) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to delete one or more rows based on keyset KEYS3
 if p_errval=0 then
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID =50) '||'';
 begin
 for r3 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =50) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 4 then 
 -- case to delete one or more rows based on keyset KEYS4
 if p_errval=0 then
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r4 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 
 and TRANSID=P_TRANSID) 
 loop
 rec_rowid:=r4.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 5 then 
 -- case to delete one or more rows based on keyset KEYS5
 if p_errval=0 then
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r5 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 
 and LOANNMB=P_LOANNMB) 
 loop
 rec_rowid:=r5.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB; log_TRANSID:=old_rec.TRANSID; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCACCELERATELOANDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCACCELERATELOANDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in ACCACCELERATELOANDELTSP build 2018-01-23 16:52:18'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END ACCACCELERATELOANDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 12/31/2017 09:00:19. --->
PROCEDURE SOFVAUDTDelTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_AUDTRECRDKEY CHAR:=NULL, P_AUDTTRANSDT TIMESTAMP:=NULL,
 P_AUDTSEQNMB NUMBER:=NULL)
 as
 /*
 <!--- generated 2017-09-28 14:04:37 by SYS --->
 Created on: 2017-09-28 14:04:37
 Created by: SYS
 Crerated from genr, stdtbldel_template v 1.0
 Purpose : standard delete by key for table STGCSA.SOFVAUDTTBL
 */
 BEGIN 
 savepoint SOFVAUDTDelTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 delete from STGCSA.SOFVAUDTTBL where (
 AUDTRECRDKEY, AUDTTRANSDT, AUDTSEQNMB)=
 (select 
 P_AUDTRECRDKEY, P_AUDTTRANSDT, P_AUDTSEQNMB
 from dual); 
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO SOFVAUDTDelTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END SOFVAUDTDelTsp; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:41:41. --->
PROCEDURE COREEMPATTRTYPDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ATTRASSOCID NUMBER:=null
 ,p_ATTRGROUPID NUMBER:=null
 ,p_ATTRTYP VARCHAR2:=null
 ,p_ATTRTYPID NUMBER:=null
 ,p_DBFNCTNID NUMBER:=null
 ,p_EMPACTV NUMBER:=null
 ,p_EMPDESC VARCHAR2:=null
 ,p_EMPSHOW NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:19:20
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:19:20
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure COREEMPATTRTYPDELTSP performs DELETE on STGCSA.COREEMPATTRTYPTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ATTRTYPID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.COREEMPATTRTYPTBL%rowtype;
 new_rec STGCSA.COREEMPATTRTYPTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.COREEMPATTRTYPTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPATTRTYPDELTSP build 2018-01-23 08:19:20 '
 ||'Select of STGCSA.COREEMPATTRTYPTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPATTRTYPDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.COREEMPATTRTYPAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.COREEMPATTRTYPTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In COREEMPATTRTYPDELTSP build 2018-01-23 08:19:20 '
 ||'while doing delete on STGCSA.COREEMPATTRTYPTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPATTRTYPDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint COREEMPATTRTYPDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ATTRTYPID)=('||P_ATTRTYPID||') ';
 select rowid into rec_rowid from STGCSA.COREEMPATTRTYPTBL where 1=1
 and ATTRTYPID=P_ATTRTYPID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPATTRTYPDELTSP build 2018-01-23 08:19:20 '
 ||'No STGCSA.COREEMPATTRTYPTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO COREEMPATTRTYPDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In COREEMPATTRTYPDELTSP build 2018-01-23 08:19:20 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End COREEMPATTRTYPDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'COREEMPATTRTYPDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in COREEMPATTRTYPDELTSP build 2018-01-23 08:19:20'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO COREEMPATTRTYPDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END COREEMPATTRTYPDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


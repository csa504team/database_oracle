<!--- Saved 01/28/2018 14:41:51. --->
PROCEDURE ENTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ENTID NUMBER:=null
 ,p_ENTNM VARCHAR2:=null
 ,p_ENTTYP VARCHAR2:=null
 ,p_ENTMAILADRSTR1NM VARCHAR2:=null
 ,p_ENTMAILADRSTR2NM VARCHAR2:=null
 ,p_ENTMAILADRCTYNM VARCHAR2:=null
 ,p_ENTMAILADRSTCD CHAR:=null
 ,p_ENTMAILADRZIPCD CHAR:=null
 ,p_ENTMAILADRZIP4CD CHAR:=null
 ,p_ENTPHNNMB CHAR:=null
 ,p_CREATBY NUMBER:=null
 ,p_UPDTBY NUMBER:=null
 ,p_REGNCD CHAR:=null
 ,p_ENTNMB CHAR:=null
 ,p_DISTCD CHAR:=null
 ,p_STATCD NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:21:31
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:21:31
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ENTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.ENTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint ENTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_ENTID';
 new_rec.ENTID:=runtime.validate_column_value(P_ENTID,
 'STGCSA','ENTTBL','ENTID','NUMBER',22);
 cur_col_name:='P_ENTNM';
 new_rec.ENTNM:=runtime.validate_column_value(P_ENTNM,
 'STGCSA','ENTTBL','ENTNM','VARCHAR2',80);
 cur_col_name:='P_ENTTYP';
 new_rec.ENTTYP:=runtime.validate_column_value(P_ENTTYP,
 'STGCSA','ENTTBL','ENTTYP','VARCHAR2',20);
 cur_col_name:='P_ENTMAILADRSTR1NM';
 new_rec.ENTMAILADRSTR1NM:=runtime.validate_column_value(P_ENTMAILADRSTR1NM,
 'STGCSA','ENTTBL','ENTMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_ENTMAILADRSTR2NM';
 new_rec.ENTMAILADRSTR2NM:=runtime.validate_column_value(P_ENTMAILADRSTR2NM,
 'STGCSA','ENTTBL','ENTMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_ENTMAILADRCTYNM';
 new_rec.ENTMAILADRCTYNM:=runtime.validate_column_value(P_ENTMAILADRCTYNM,
 'STGCSA','ENTTBL','ENTMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_ENTMAILADRSTCD';
 new_rec.ENTMAILADRSTCD:=runtime.validate_column_value(P_ENTMAILADRSTCD,
 'STGCSA','ENTTBL','ENTMAILADRSTCD','CHAR',2);
 cur_col_name:='P_ENTMAILADRZIPCD';
 new_rec.ENTMAILADRZIPCD:=runtime.validate_column_value(P_ENTMAILADRZIPCD,
 'STGCSA','ENTTBL','ENTMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_ENTMAILADRZIP4CD';
 new_rec.ENTMAILADRZIP4CD:=runtime.validate_column_value(P_ENTMAILADRZIP4CD,
 'STGCSA','ENTTBL','ENTMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_ENTPHNNMB';
 new_rec.ENTPHNNMB:=runtime.validate_column_value(P_ENTPHNNMB,
 'STGCSA','ENTTBL','ENTPHNNMB','CHAR',10);
 cur_col_name:='P_CREATBY';
 new_rec.CREATBY:=runtime.validate_column_value(P_CREATBY,
 'STGCSA','ENTTBL','CREATBY','NUMBER',22);
 cur_col_name:='P_UPDTBY';
 new_rec.UPDTBY:=runtime.validate_column_value(P_UPDTBY,
 'STGCSA','ENTTBL','UPDTBY','NUMBER',22);
 cur_col_name:='P_REGNCD';
 new_rec.REGNCD:=runtime.validate_column_value(P_REGNCD,
 'STGCSA','ENTTBL','REGNCD','CHAR',2);
 cur_col_name:='P_ENTNMB';
 new_rec.ENTNMB:=runtime.validate_column_value(P_ENTNMB,
 'STGCSA','ENTTBL','ENTNMB','CHAR',4);
 cur_col_name:='P_DISTCD';
 new_rec.DISTCD:=runtime.validate_column_value(P_DISTCD,
 'STGCSA','ENTTBL','DISTCD','CHAR',5);
 cur_col_name:='P_STATCD';
 new_rec.STATCD:=runtime.validate_column_value(P_STATCD,
 'STGCSA','ENTTBL','STATCD','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO ENTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.ENTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.ENTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO ENTINSTSP;
 p_errmsg:='Got dupe key on insert into ENTTBL for key '
 ||'(ENTID)=('||new_rec.ENTID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in ENTINSTSP build 2018-01-23 08:21:31';
 ROLLBACK TO ENTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ENTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ENTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ENTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in ENTINSTSP build 2018-01-23 08:21:31'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ENTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END ENTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


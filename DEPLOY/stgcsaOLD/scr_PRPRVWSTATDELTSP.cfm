<!--- Saved 01/28/2018 14:42:21. --->
PROCEDURE PRPRVWSTATDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_FEEVAL NUMBER:=null
 ,p_FEEVALBY VARCHAR2:=null
 ,p_FIFTHPOSTRVW NUMBER:=null
 ,p_FIFTHPOSTRVWBY VARCHAR2:=null
 ,p_FOURTHPOSTRVW NUMBER:=null
 ,p_FOURTHPOSTRVWBY VARCHAR2:=null
 ,p_INITIALPOSTRVW NUMBER:=null
 ,p_INITIALPOSTRVWBY VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_PROOFVAL NUMBER:=null
 ,p_PROOFVALBY VARCHAR2:=null
 ,p_PRPRVWSTATID NUMBER:=null
 ,p_RVWSBMT NUMBER:=null
 ,p_RVWSBMTBY VARCHAR2:=null
 ,p_RVWSBMTDT DATE:=null
 ,p_SCNDPOSTRVW NUMBER:=null
 ,p_SCNDPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST2RVW NUMBER:=null
 ,p_SIXTHMOPOST2RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST3RVW NUMBER:=null
 ,p_SIXTHMOPOST3RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST4RVW NUMBER:=null
 ,p_SIXTHMOPOST4RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST5RVW NUMBER:=null
 ,p_SIXTHMOPOST5RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOSTRVW NUMBER:=null
 ,p_SIXTHMOPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHPOSTRVW NUMBER:=null
 ,p_SIXTHPOSTRVWBY VARCHAR2:=null
 ,p_THIRDPOSTRVW NUMBER:=null
 ,p_THIRDPOSTRVWBY VARCHAR2:=null
 ,p_THISTRVW NUMBER:=null
 ,p_THISTRVWBY VARCHAR2:=null
 ,p_THISTVAL NUMBER:=null
 ,p_THISTVALBY VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ,p_VALSBMT NUMBER:=null
 ,p_VALSBMTBY VARCHAR2:=null
 ,p_VALSBMTDT DATE:=null
 ,p_WORKUPVAL NUMBER:=null
 ,p_WORKUPVALBY VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:25
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:29:25
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPRVWSTATDELTSP performs DELETE on STGCSA.PRPRVWSTATTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPRVWSTATID
 for P_IDENTIFIER=1 qualification is on TRANSID
 for P_IDENTIFIER=2 qualification is on TRANSTYPID, TRANSID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPRVWSTATTBL%rowtype;
 new_rec STGCSA.PRPRVWSTATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPRVWSTATTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'Select of STGCSA.PRPRVWSTATTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPRVWSTATDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPRVWSTATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.PRPRVWSTATTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'while doing delete on STGCSA.PRPRVWSTATTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPRVWSTATDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPRVWSTATDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPRVWSTATID)=('||P_PRPRVWSTATID||') ';
 select rowid into rec_rowid from STGCSA.PRPRVWSTATTBL where 1=1
 and PRPRVWSTATID=P_PRPRVWSTATID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'No STGCSA.PRPRVWSTATTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1 
 
 and TRANSID=P_TRANSID) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPRVWSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'Oracle returned error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPRVWSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'No STGCSA.PRPRVWSTATTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to delete one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||
 '(TRANSTYPID)=('||P_TRANSTYPID||') '||'';
 begin
 for r2 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1 
 
 and TRANSID=P_TRANSID
 and TRANSTYPID=P_TRANSTYPID) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPRVWSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'Oracle returned error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPRVWSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'No STGCSA.PRPRVWSTATTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPRVWSTATDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPRVWSTATDELTSP build 2018-01-23 08:29:25 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB; log_TRANSID:=old_rec.TRANSID; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPRVWSTATDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPRVWSTATDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PRPRVWSTATDELTSP build 2018-01-23 08:29:25'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPRVWSTATDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPRVWSTATDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


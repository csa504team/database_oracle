<!--- Saved 01/28/2018 14:41:31. --->
PROCEDURE ACCPRCSCYCSTATDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CYCID NUMBER:=null
 ,p_CYCPRCSID NUMBER:=null
 ,p_INITIALRVWCMPLT NUMBER:=null
 ,p_INITIALRVWDT DATE:=null
 ,p_INITIALRVWUSER VARCHAR2:=null
 ,p_LASTREFRESHDT DATE:=null
 ,p_MGRRVWCMPLT NUMBER:=null
 ,p_MGRRVWDT DATE:=null
 ,p_MGRRVWUSER VARCHAR2:=null
 ,p_PEERRVWCMPLT NUMBER:=null
 ,p_PEERRVWDT DATE:=null
 ,p_PEERRVWUSER VARCHAR2:=null
 ,p_PRCSCYCCMNT VARCHAR2:=null
 ,p_PRCSID NUMBER:=null
 ,p_PROPOSEDDT DATE:=null
 ,p_SCHDLDT DATE:=null
 ,p_SCHDLUSER VARCHAR2:=null
 ,p_SETUPDT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_UPDTCMPLT NUMBER:=null
 ,p_UPDTCMPLTDT DATE:=null
 ,p_UPDTCMPLTUSER VARCHAR2:=null
 ,p_UPDTRUNNING NUMBER:=null
 ,p_UPDTRUNNINGUSER VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:16:46
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:16:46
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure ACCPRCSCYCSTATDELTSP performs DELETE on STGCSA.ACCPRCSCYCSTATTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CYCPRCSID
 for P_IDENTIFIER=1 qualification is on CYCID, PRCSID
 for P_IDENTIFIER=2 qualification is on CYCID, PROPOSEDDT is not null
 for P_IDENTIFIER=3 qualification is on CYCID, PRCSID in (30,31,32)
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.ACCPRCSCYCSTATTBL%rowtype;
 new_rec STGCSA.ACCPRCSCYCSTATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.ACCPRCSCYCSTATTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'Select of STGCSA.ACCPRCSCYCSTATTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.ACCPRCSCYCSTATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.ACCPRCSCYCSTATTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'while doing delete on STGCSA.ACCPRCSCYCSTATTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCPRCSCYCSTATDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CYCPRCSID)=('||P_CYCPRCSID||') ';
 select rowid into rec_rowid from STGCSA.ACCPRCSCYCSTATTBL where 1=1
 and CYCPRCSID=P_CYCPRCSID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'No STGCSA.ACCPRCSCYCSTATTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(CYCID)=('||P_CYCID||') '||
 '(PRCSID)=('||P_PRCSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 
 and CYCID=P_CYCID
 and PRCSID=P_PRCSID) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'Oracle returned error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'No STGCSA.ACCPRCSCYCSTATTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to delete one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 keystouse:='(CYCID)=('||P_CYCID||') '||
 '(PROPOSEDDT is not null) '||'';
 begin
 for r2 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 
 and CYCID=P_CYCID
 
 and PROPOSEDDT is not null) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'Oracle returned error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'No STGCSA.ACCPRCSCYCSTATTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to delete one or more rows based on keyset KEYS3
 if p_errval=0 then
 
 keystouse:='(CYCID)=('||P_CYCID||') '||
 '(PRCSID in (30,31,32)) '||'';
 begin
 for r3 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 
 and CYCID=P_CYCID
 
 and PRCSID in (30,31,32)) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'Oracle returned error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'No STGCSA.ACCPRCSCYCSTATTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCPRCSCYCSTATDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCPRCSCYCSTATDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in ACCPRCSCYCSTATDELTSP build 2018-01-23 08:16:46'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCPRCSCYCSTATDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END ACCPRCSCYCSTATDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


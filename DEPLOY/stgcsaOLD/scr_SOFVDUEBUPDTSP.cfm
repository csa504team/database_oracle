<!--- Saved 01/28/2018 14:42:37. --->
PROCEDURE SOFVDUEBUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DUEFROMBORRCLSDT DATE:=null
 ,p_DUEFROMBORRCMNT1 VARCHAR2:=null
 ,p_DUEFROMBORRCMNT2 VARCHAR2:=null
 ,p_DUEFROMBORRDELACH CHAR:=null
 ,p_DUEFROMBORRDISBIND CHAR:=null
 ,p_DUEFROMBORRDOLLRAMT NUMBER:=null
 ,p_DUEFROMBORRPOSTINGDT DATE:=null
 ,p_DUEFROMBORRRECRDSTATCD NUMBER:=null
 ,p_DUEFROMBORRRECRDTYP NUMBER:=null
 ,p_DUEFROMBORRRJCTCD CHAR:=null
 ,p_DUEFROMBORRRJCTDT DATE:=null
 ,p_DUEFROMBORRRPTDAILY CHAR:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:15
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:32:15
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVDUEBUPDTSP performs UPDATE on STGCSA.SOFVDUEBTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, DUEFROMBORRRECRDTYP, DUEFROMBORRPOSTINGDT
 for P_IDENTIFIER=1 qualification is on DUEFROMBORRDOLLRAMT, DUEFROMBORRRECRDTYP, DUEFROMBORRRECRDSTATCD<>, LOANNMB, DUEFROMBORRPOSTINGDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVDUEBTBL%rowtype;
 new_rec STGCSA.SOFVDUEBTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVDUEBTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'Select of STGCSA.SOFVDUEBTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVDUEBUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_DUEFROMBORRRECRDTYP is not null then
 cur_col_name:='P_DUEFROMBORRRECRDTYP';
 new_rec.DUEFROMBORRRECRDTYP:=P_DUEFROMBORRRECRDTYP; 
 end if;
 if P_DUEFROMBORRPOSTINGDT is not null then
 cur_col_name:='P_DUEFROMBORRPOSTINGDT';
 new_rec.DUEFROMBORRPOSTINGDT:=P_DUEFROMBORRPOSTINGDT; 
 end if;
 if P_DUEFROMBORRDOLLRAMT is not null then
 cur_col_name:='P_DUEFROMBORRDOLLRAMT';
 new_rec.DUEFROMBORRDOLLRAMT:=P_DUEFROMBORRDOLLRAMT; 
 end if;
 if P_DUEFROMBORRRJCTDT is not null then
 cur_col_name:='P_DUEFROMBORRRJCTDT';
 new_rec.DUEFROMBORRRJCTDT:=P_DUEFROMBORRRJCTDT; 
 end if;
 if P_DUEFROMBORRRJCTCD is not null then
 cur_col_name:='P_DUEFROMBORRRJCTCD';
 new_rec.DUEFROMBORRRJCTCD:=P_DUEFROMBORRRJCTCD; 
 end if;
 if P_DUEFROMBORRRECRDSTATCD is not null then
 cur_col_name:='P_DUEFROMBORRRECRDSTATCD';
 new_rec.DUEFROMBORRRECRDSTATCD:=P_DUEFROMBORRRECRDSTATCD; 
 end if;
 if P_DUEFROMBORRCLSDT is not null then
 cur_col_name:='P_DUEFROMBORRCLSDT';
 new_rec.DUEFROMBORRCLSDT:=P_DUEFROMBORRCLSDT; 
 end if;
 if P_DUEFROMBORRCMNT1 is not null then
 cur_col_name:='P_DUEFROMBORRCMNT1';
 new_rec.DUEFROMBORRCMNT1:=P_DUEFROMBORRCMNT1; 
 end if;
 if P_DUEFROMBORRCMNT2 is not null then
 cur_col_name:='P_DUEFROMBORRCMNT2';
 new_rec.DUEFROMBORRCMNT2:=P_DUEFROMBORRCMNT2; 
 end if;
 if P_DUEFROMBORRRPTDAILY is not null then
 cur_col_name:='P_DUEFROMBORRRPTDAILY';
 new_rec.DUEFROMBORRRPTDAILY:=P_DUEFROMBORRRPTDAILY; 
 end if;
 if P_DUEFROMBORRDISBIND is not null then
 cur_col_name:='P_DUEFROMBORRDISBIND';
 new_rec.DUEFROMBORRDISBIND:=P_DUEFROMBORRDISBIND; 
 end if;
 if P_DUEFROMBORRDELACH is not null then
 cur_col_name:='P_DUEFROMBORRDELACH';
 new_rec.DUEFROMBORRDELACH:=P_DUEFROMBORRDELACH; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVDUEBAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVDUEBTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,DUEFROMBORRDOLLRAMT=new_rec.DUEFROMBORRDOLLRAMT
 ,DUEFROMBORRRJCTDT=new_rec.DUEFROMBORRRJCTDT
 ,DUEFROMBORRRJCTCD=new_rec.DUEFROMBORRRJCTCD
 ,DUEFROMBORRRECRDSTATCD=new_rec.DUEFROMBORRRECRDSTATCD
 ,DUEFROMBORRCLSDT=new_rec.DUEFROMBORRCLSDT
 ,DUEFROMBORRCMNT1=new_rec.DUEFROMBORRCMNT1
 ,DUEFROMBORRCMNT2=new_rec.DUEFROMBORRCMNT2
 ,DUEFROMBORRRPTDAILY=new_rec.DUEFROMBORRRPTDAILY
 ,DUEFROMBORRDISBIND=new_rec.DUEFROMBORRDISBIND
 ,DUEFROMBORRDELACH=new_rec.DUEFROMBORRDELACH
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'while doing update on STGCSA.SOFVDUEBTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVDUEBUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVDUEBUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVDUEBTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ''(DUEFROMBORRRECRDTYP)=('||P_DUEFROMBORRRECRDTYP||') ''(DUEFROMBORRPOSTINGDT)=('||P_DUEFROMBORRPOSTINGDT||') ';
 select rowid into rec_rowid from STGCSA.SOFVDUEBTBL where 1=1
 and LOANNMB=P_LOANNMB and DUEFROMBORRRECRDTYP=P_DUEFROMBORRRECRDTYP and DUEFROMBORRPOSTINGDT=P_DUEFROMBORRPOSTINGDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'No STGCSA.SOFVDUEBTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(DUEFROMBORRDOLLRAMT)=('||P_DUEFROMBORRDOLLRAMT||') '||
 '(DUEFROMBORRPOSTINGDT)=('||P_DUEFROMBORRPOSTINGDT||') '||
 '(DUEFROMBORRRECRDSTATCD)<>('||P_DUEFROMBORRRECRDSTATCD||') '||'(DUEFROMBORRRECRDTYP)=('||P_DUEFROMBORRRECRDTYP||') '||
 '(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r1 in (select rowid from STGCSA.SOFVDUEBTBL a where 1=1 
 and DUEFROMBORRDOLLRAMT=P_DUEFROMBORRDOLLRAMT
 and DUEFROMBORRPOSTINGDT=P_DUEFROMBORRPOSTINGDT
 
 and DUEFROMBORRRECRDSTATCD<>P_DUEFROMBORRRECRDSTATCD
 and DUEFROMBORRRECRDTYP=P_DUEFROMBORRRECRDTYP
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO SOFVDUEBUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'Oracle returned error fetching STGCSA.SOFVDUEBTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO SOFVDUEBUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'No STGCSA.SOFVDUEBTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVDUEBUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVDUEBUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVDUEBUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVDUEBUPDTSP build 2018-01-23 08:32:15'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVDUEBUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVDUEBUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


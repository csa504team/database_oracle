<!--- Saved 01/28/2018 14:42:27. --->
PROCEDURE SOFVBFFAAUDTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2,
 action_cd char,
 AUDTEDTCD char,
 old_rec STGCSA.SOFVBFFATBL%rowtype,
 new_rec STGCSA.SOFVBFFATBL%rowtype) as
 /*
 Created on: 2018-01-23 08:29:51
 Created by: GENR
 Crerated from template auditme_template v2.02 20 Dec 2017 on 2018-01-23 08:29:51
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Purpose : Write to SOFVAUDTTBL for specific changed columns in
 STGCSA.SOFVBFFATBL (SOFVBFFATBL)
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 aud_rec STGCSA.SOFVAUDTTBL%rowtype;
 n number:=0;
 e number:=0;
 tsp timestamp;
 key_from_new_rec STGCSA.SOFVAUDTTBL.AUDTRECRDKEY%type;
 begin
 savepoint SOFVBFFAAUDTSP;
 -- hopeful return
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=null;
 -- setup log rec
 select systimestamp,sysdate,sys_context('userenv','session_user')
 into aud_rec.audttransdt, aud_rec.CREATDT, aud_rec.creatuserid from dual;
 aud_rec.LASTUPDTDT:=aud_rec.CREATDT;
 aud_rec.lastupdtuserid:=aud_rec.creatuserid;
 aud_rec.audtseqnmb:=0;
 aud_rec.AUDTOPRTRINITIAL:=' ';
 aud_rec.AUDTTRMNLID:=' ';
 aud_rec.AUDTACTNCD:=action_cd;
 aud_rec.AUDTEDTCD:=AUDTEDTCD;
 aud_rec.AUDTUSERID:=p_userid;
 -- save key value... if add or del then one rec is nulls
 aud_rec.audtrecrdkey:=rpad(substr(''
 ||old_rec.ASSUMPKEYLOANNMB ||to_char(old_rec.ASSUMPLASTEFFDT,'YYYYMMDD'),1,15),15);
 key_from_new_rec:=rpad(substr(''
 ||new_rec.ASSUMPKEYLOANNMB ||to_char(new_rec.ASSUMPLASTEFFDT,'YYYYMMDD'),1,15),15);
 if aud_rec.audtrecrdkey is null then
 aud_rec.audtrecrdkey:=key_from_new_rec;
 end if;
 -- are any columns auditing cares about different?
 if old_rec.ASSUMPNM=new_rec.ASSUMPNM 
 or (old_rec.ASSUMPNM is null and new_rec.ASSUMPNM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPNM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPNM; 
 end if; 
 if new_rec.ASSUMPNM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPNM; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASNAME1 ';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.ASSUMPMAILADRSTR1NM=new_rec.ASSUMPMAILADRSTR1NM 
 or (old_rec.ASSUMPMAILADRSTR1NM is null and new_rec.ASSUMPMAILADRSTR1NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPMAILADRSTR1NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPMAILADRSTR1NM; 
 end if; 
 if new_rec.ASSUMPMAILADRSTR1NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPMAILADRSTR1NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASSTREET';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.ASSUMPMAILADRCTYNM=new_rec.ASSUMPMAILADRCTYNM 
 or (old_rec.ASSUMPMAILADRCTYNM is null and new_rec.ASSUMPMAILADRCTYNM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPMAILADRCTYNM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPMAILADRCTYNM; 
 end if; 
 if new_rec.ASSUMPMAILADRCTYNM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPMAILADRCTYNM; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASCITY ';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.ASSUMPMAILADRSTCD=new_rec.ASSUMPMAILADRSTCD 
 or (old_rec.ASSUMPMAILADRSTCD is null and new_rec.ASSUMPMAILADRSTCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPMAILADRSTCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPMAILADRSTCD; 
 end if; 
 if new_rec.ASSUMPMAILADRSTCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPMAILADRSTCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASSTATE ';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.ASSUMPMAILADRZIPCD=new_rec.ASSUMPMAILADRZIPCD 
 or (old_rec.ASSUMPMAILADRZIPCD is null and new_rec.ASSUMPMAILADRZIPCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPMAILADRZIPCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPMAILADRZIPCD; 
 end if; 
 if new_rec.ASSUMPMAILADRZIPCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPMAILADRZIPCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASZIP1 ';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.ASSUMPMAILADRZIP4CD=new_rec.ASSUMPMAILADRZIP4CD 
 or (old_rec.ASSUMPMAILADRZIP4CD is null and new_rec.ASSUMPMAILADRZIP4CD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.ASSUMPMAILADRZIP4CD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.ASSUMPMAILADRZIP4CD; 
 end if; 
 if new_rec.ASSUMPMAILADRZIP4CD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.ASSUMPMAILADRZIP4CD; 
 end if;
 
 aud_rec.AUDTFLDNM:='ASZIP2 ';
 aud_rec.audtchngfile:='BGW9ASSU';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 p_retval:=aud_rec.audtseqnmb;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:='In OXH in SOFVBFFAAUDTSP Build 2018-01-23 08:29:51 got: '||sqlerrm(SQLCODE);
 ROLLBACK TO SOFVBFFAAUDTSP;
 --
 END SOFVBFFAAUDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:15. --->
PROCEDURE PRPLOANRQSTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_RQSTREF NUMBER:=null
 ,p_RQSTCD NUMBER:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCNMB CHAR:=null
 ,p_LOANSTAT VARCHAR2:=null
 ,p_BORRNM VARCHAR2:=null
 ,p_PREPAYAMT NUMBER:=null
 ,p_PREPAYDT DATE:=null
 ,p_PREPAYMO NUMBER:=null
 ,p_PREPAYYRNMB CHAR:=null
 ,p_PREPAYCALCDT DATE:=null
 ,p_SEMIANNDT DATE:=null
 ,p_SEMIANMO NUMBER:=null
 ,p_SEMIANNAMT NUMBER:=null
 ,p_SEMIANNPYMTAMT NUMBER:=null
 ,p_PRINCURAMT NUMBER:=null
 ,p_BALAMT NUMBER:=null
 ,p_BALCALCAMT NUMBER:=null
 ,p_NOTEBALCALCAMT NUMBER:=null
 ,p_PIAMT NUMBER:=null
 ,p_NOTERTPCT NUMBER:=null
 ,p_ISSDT DATE:=null
 ,p_DBENTRBALAMT NUMBER:=null
 ,p_PREPAYPREMAMT NUMBER:=null
 ,p_POSTINGDT DATE:=null
 ,p_POSTINGMO NUMBER:=null
 ,p_POSTINGYRNMB CHAR:=null
 ,p_BUSDAY6DT DATE:=null
 ,p_THIRDTHURSDAY DATE:=null
 ,p_ACTUALRQST VARCHAR2:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_APPVDT DATE:=null
 ,p_PRINNEEDEDAMT NUMBER:=null
 ,p_INTNEEDEDAMT NUMBER:=null
 ,p_MODELNQ NUMBER:=null
 ,p_CURRFEEBALAMT NUMBER:=null
 ,p_CSAFEENEEDEDAMT NUMBER:=null
 ,p_CDCFEENEEDEDAMT NUMBER:=null
 ,p_FEENEEDEDAMT NUMBER:=null
 ,p_LATEFEENEEDEDAMT NUMBER:=null
 ,p_CSADUEFROMBORRAMT NUMBER:=null
 ,p_CDCDUEFROMBORRAMT NUMBER:=null
 ,p_DUEFROMBORRAMT NUMBER:=null
 ,p_CSABEHINDAMT NUMBER:=null
 ,p_CDCBEHINDAMT NUMBER:=null
 ,p_BEHINDAMT NUMBER:=null
 ,p_BORRFEEAMT NUMBER:=null
 ,p_CDCFEEDISBAMT NUMBER:=null
 ,p_AMORTCDCAMT NUMBER:=null
 ,p_AMORTSBAAMT NUMBER:=null
 ,p_AMORTCSAAMT NUMBER:=null
 ,p_CDCPCT NUMBER:=null
 ,p_PREPAYPCT NUMBER:=null
 ,p_FEECALCAMT NUMBER:=null
 ,p_CSAFEECALCAMT NUMBER:=null
 ,p_CSAFEECALCONLYAMT NUMBER:=null
 ,p_CDCFEECALCONLYAMT NUMBER:=null
 ,p_FEECALCONLYAMT NUMBER:=null
 ,p_FIVEYRADJ NUMBER:=null
 ,p_FIVEYRADJCSAAMT NUMBER:=null
 ,p_FIVEYRADJCDCAMT NUMBER:=null
 ,p_FIVEYRADJSBAAMT NUMBER:=null
 ,p_BALCALC1AMT NUMBER:=null
 ,p_BALCALC2AMT NUMBER:=null
 ,p_BALCALC3AMT NUMBER:=null
 ,p_BALCALC4AMT NUMBER:=null
 ,p_BALCALC5AMT NUMBER:=null
 ,p_BALCALC6AMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_PRIN2AMT NUMBER:=null
 ,p_PRIN3AMT NUMBER:=null
 ,p_PRIN4AMT NUMBER:=null
 ,p_PRIN5AMT NUMBER:=null
 ,p_PRIN6AMT NUMBER:=null
 ,p_PRINPROJAMT NUMBER:=null
 ,p_PIPROJAMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_INT2AMT NUMBER:=null
 ,p_INT3AMT NUMBER:=null
 ,p_INT4AMT NUMBER:=null
 ,p_INT5AMT NUMBER:=null
 ,p_INT6AMT NUMBER:=null
 ,p_ESCROWAMT NUMBER:=null
 ,p_PI6MOAMT NUMBER:=null
 ,p_GFDAMT NUMBER:=null
 ,p_OPENGNTYAMT NUMBER:=null
 ,p_OPENGNTYERRAMT NUMBER:=null
 ,p_OPENGNTYREGAMT NUMBER:=null
 ,p_OPENGNTYUNALLOCAMT NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_USERLCK VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_CDCSBAFEEDISBAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:29
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:27:29
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPLOANRQSTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PRPLOANRQSTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PRPLOANRQSTTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_RQSTREF';
 new_rec.RQSTREF:=runtime.validate_column_value(P_RQSTREF,
 'STGCSA','PRPLOANRQSTTBL','RQSTREF','NUMBER',22);
 cur_col_name:='P_RQSTCD';
 new_rec.RQSTCD:=runtime.validate_column_value(P_RQSTCD,
 'STGCSA','PRPLOANRQSTTBL','RQSTCD','NUMBER',22);
 cur_col_name:='P_CDCREGNCD';
 new_rec.CDCREGNCD:=runtime.validate_column_value(P_CDCREGNCD,
 'STGCSA','PRPLOANRQSTTBL','CDCREGNCD','CHAR',2);
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=runtime.validate_column_value(P_CDCNMB,
 'STGCSA','PRPLOANRQSTTBL','CDCNMB','CHAR',4);
 cur_col_name:='P_LOANSTAT';
 new_rec.LOANSTAT:=runtime.validate_column_value(P_LOANSTAT,
 'STGCSA','PRPLOANRQSTTBL','LOANSTAT','VARCHAR2',10);
 cur_col_name:='P_BORRNM';
 new_rec.BORRNM:=runtime.validate_column_value(P_BORRNM,
 'STGCSA','PRPLOANRQSTTBL','BORRNM','VARCHAR2',80);
 cur_col_name:='P_PREPAYAMT';
 new_rec.PREPAYAMT:=runtime.validate_column_value(P_PREPAYAMT,
 'STGCSA','PRPLOANRQSTTBL','PREPAYAMT','NUMBER',22);
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=runtime.validate_column_value(P_PREPAYDT,
 'STGCSA','PRPLOANRQSTTBL','PREPAYDT','DATE',7);
 cur_col_name:='P_PREPAYMO';
 new_rec.PREPAYMO:=runtime.validate_column_value(P_PREPAYMO,
 'STGCSA','PRPLOANRQSTTBL','PREPAYMO','NUMBER',22);
 cur_col_name:='P_PREPAYYRNMB';
 new_rec.PREPAYYRNMB:=runtime.validate_column_value(P_PREPAYYRNMB,
 'STGCSA','PRPLOANRQSTTBL','PREPAYYRNMB','CHAR',4);
 cur_col_name:='P_PREPAYCALCDT';
 new_rec.PREPAYCALCDT:=runtime.validate_column_value(P_PREPAYCALCDT,
 'STGCSA','PRPLOANRQSTTBL','PREPAYCALCDT','DATE',7);
 cur_col_name:='P_SEMIANNDT';
 new_rec.SEMIANNDT:=runtime.validate_column_value(P_SEMIANNDT,
 'STGCSA','PRPLOANRQSTTBL','SEMIANNDT','DATE',7);
 cur_col_name:='P_SEMIANMO';
 new_rec.SEMIANMO:=runtime.validate_column_value(P_SEMIANMO,
 'STGCSA','PRPLOANRQSTTBL','SEMIANMO','NUMBER',22);
 cur_col_name:='P_SEMIANNAMT';
 new_rec.SEMIANNAMT:=runtime.validate_column_value(P_SEMIANNAMT,
 'STGCSA','PRPLOANRQSTTBL','SEMIANNAMT','NUMBER',22);
 cur_col_name:='P_SEMIANNPYMTAMT';
 new_rec.SEMIANNPYMTAMT:=runtime.validate_column_value(P_SEMIANNPYMTAMT,
 'STGCSA','PRPLOANRQSTTBL','SEMIANNPYMTAMT','NUMBER',22);
 cur_col_name:='P_PRINCURAMT';
 new_rec.PRINCURAMT:=runtime.validate_column_value(P_PRINCURAMT,
 'STGCSA','PRPLOANRQSTTBL','PRINCURAMT','NUMBER',22);
 cur_col_name:='P_BALAMT';
 new_rec.BALAMT:=runtime.validate_column_value(P_BALAMT,
 'STGCSA','PRPLOANRQSTTBL','BALAMT','NUMBER',22);
 cur_col_name:='P_BALCALCAMT';
 new_rec.BALCALCAMT:=runtime.validate_column_value(P_BALCALCAMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALCAMT','NUMBER',22);
 cur_col_name:='P_NOTEBALCALCAMT';
 new_rec.NOTEBALCALCAMT:=runtime.validate_column_value(P_NOTEBALCALCAMT,
 'STGCSA','PRPLOANRQSTTBL','NOTEBALCALCAMT','NUMBER',22);
 cur_col_name:='P_PIAMT';
 new_rec.PIAMT:=runtime.validate_column_value(P_PIAMT,
 'STGCSA','PRPLOANRQSTTBL','PIAMT','NUMBER',22);
 cur_col_name:='P_NOTERTPCT';
 new_rec.NOTERTPCT:=runtime.validate_column_value(P_NOTERTPCT,
 'STGCSA','PRPLOANRQSTTBL','NOTERTPCT','NUMBER',22);
 cur_col_name:='P_ISSDT';
 new_rec.ISSDT:=runtime.validate_column_value(P_ISSDT,
 'STGCSA','PRPLOANRQSTTBL','ISSDT','DATE',7);
 cur_col_name:='P_DBENTRBALAMT';
 new_rec.DBENTRBALAMT:=runtime.validate_column_value(P_DBENTRBALAMT,
 'STGCSA','PRPLOANRQSTTBL','DBENTRBALAMT','NUMBER',22);
 cur_col_name:='P_PREPAYPREMAMT';
 new_rec.PREPAYPREMAMT:=runtime.validate_column_value(P_PREPAYPREMAMT,
 'STGCSA','PRPLOANRQSTTBL','PREPAYPREMAMT','NUMBER',22);
 cur_col_name:='P_POSTINGDT';
 new_rec.POSTINGDT:=runtime.validate_column_value(P_POSTINGDT,
 'STGCSA','PRPLOANRQSTTBL','POSTINGDT','DATE',7);
 cur_col_name:='P_POSTINGMO';
 new_rec.POSTINGMO:=runtime.validate_column_value(P_POSTINGMO,
 'STGCSA','PRPLOANRQSTTBL','POSTINGMO','NUMBER',22);
 cur_col_name:='P_POSTINGYRNMB';
 new_rec.POSTINGYRNMB:=runtime.validate_column_value(P_POSTINGYRNMB,
 'STGCSA','PRPLOANRQSTTBL','POSTINGYRNMB','CHAR',4);
 cur_col_name:='P_BUSDAY6DT';
 new_rec.BUSDAY6DT:=runtime.validate_column_value(P_BUSDAY6DT,
 'STGCSA','PRPLOANRQSTTBL','BUSDAY6DT','DATE',7);
 cur_col_name:='P_THIRDTHURSDAY';
 new_rec.THIRDTHURSDAY:=runtime.validate_column_value(P_THIRDTHURSDAY,
 'STGCSA','PRPLOANRQSTTBL','THIRDTHURSDAY','DATE',7);
 cur_col_name:='P_ACTUALRQST';
 new_rec.ACTUALRQST:=runtime.validate_column_value(P_ACTUALRQST,
 'STGCSA','PRPLOANRQSTTBL','ACTUALRQST','VARCHAR2',10);
 cur_col_name:='P_CSAPAIDTHRUDT';
 new_rec.CSAPAIDTHRUDT:=runtime.validate_column_value(P_CSAPAIDTHRUDT,
 'STGCSA','PRPLOANRQSTTBL','CSAPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_CDCPAIDTHRUDT';
 new_rec.CDCPAIDTHRUDT:=runtime.validate_column_value(P_CDCPAIDTHRUDT,
 'STGCSA','PRPLOANRQSTTBL','CDCPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_BORRPAIDTHRUDT';
 new_rec.BORRPAIDTHRUDT:=runtime.validate_column_value(P_BORRPAIDTHRUDT,
 'STGCSA','PRPLOANRQSTTBL','BORRPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_INTPAIDTHRUDT';
 new_rec.INTPAIDTHRUDT:=runtime.validate_column_value(P_INTPAIDTHRUDT,
 'STGCSA','PRPLOANRQSTTBL','INTPAIDTHRUDT','CHAR',10);
 cur_col_name:='P_APPVDT';
 new_rec.APPVDT:=runtime.validate_column_value(P_APPVDT,
 'STGCSA','PRPLOANRQSTTBL','APPVDT','DATE',7);
 cur_col_name:='P_PRINNEEDEDAMT';
 new_rec.PRINNEEDEDAMT:=runtime.validate_column_value(P_PRINNEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','PRINNEEDEDAMT','NUMBER',22);
 cur_col_name:='P_INTNEEDEDAMT';
 new_rec.INTNEEDEDAMT:=runtime.validate_column_value(P_INTNEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','INTNEEDEDAMT','NUMBER',22);
 cur_col_name:='P_MODELNQ';
 new_rec.MODELNQ:=runtime.validate_column_value(P_MODELNQ,
 'STGCSA','PRPLOANRQSTTBL','MODELNQ','NUMBER',22);
 cur_col_name:='P_CURRFEEBALAMT';
 new_rec.CURRFEEBALAMT:=runtime.validate_column_value(P_CURRFEEBALAMT,
 'STGCSA','PRPLOANRQSTTBL','CURRFEEBALAMT','NUMBER',22);
 cur_col_name:='P_CSAFEENEEDEDAMT';
 new_rec.CSAFEENEEDEDAMT:=runtime.validate_column_value(P_CSAFEENEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','CSAFEENEEDEDAMT','NUMBER',22);
 cur_col_name:='P_CDCFEENEEDEDAMT';
 new_rec.CDCFEENEEDEDAMT:=runtime.validate_column_value(P_CDCFEENEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCFEENEEDEDAMT','NUMBER',22);
 cur_col_name:='P_FEENEEDEDAMT';
 new_rec.FEENEEDEDAMT:=runtime.validate_column_value(P_FEENEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','FEENEEDEDAMT','NUMBER',22);
 cur_col_name:='P_LATEFEENEEDEDAMT';
 new_rec.LATEFEENEEDEDAMT:=runtime.validate_column_value(P_LATEFEENEEDEDAMT,
 'STGCSA','PRPLOANRQSTTBL','LATEFEENEEDEDAMT','NUMBER',22);
 cur_col_name:='P_CSADUEFROMBORRAMT';
 new_rec.CSADUEFROMBORRAMT:=runtime.validate_column_value(P_CSADUEFROMBORRAMT,
 'STGCSA','PRPLOANRQSTTBL','CSADUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_CDCDUEFROMBORRAMT';
 new_rec.CDCDUEFROMBORRAMT:=runtime.validate_column_value(P_CDCDUEFROMBORRAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCDUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_DUEFROMBORRAMT';
 new_rec.DUEFROMBORRAMT:=runtime.validate_column_value(P_DUEFROMBORRAMT,
 'STGCSA','PRPLOANRQSTTBL','DUEFROMBORRAMT','NUMBER',22);
 cur_col_name:='P_CSABEHINDAMT';
 new_rec.CSABEHINDAMT:=runtime.validate_column_value(P_CSABEHINDAMT,
 'STGCSA','PRPLOANRQSTTBL','CSABEHINDAMT','NUMBER',22);
 cur_col_name:='P_CDCBEHINDAMT';
 new_rec.CDCBEHINDAMT:=runtime.validate_column_value(P_CDCBEHINDAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCBEHINDAMT','NUMBER',22);
 cur_col_name:='P_BEHINDAMT';
 new_rec.BEHINDAMT:=runtime.validate_column_value(P_BEHINDAMT,
 'STGCSA','PRPLOANRQSTTBL','BEHINDAMT','NUMBER',22);
 cur_col_name:='P_BORRFEEAMT';
 new_rec.BORRFEEAMT:=runtime.validate_column_value(P_BORRFEEAMT,
 'STGCSA','PRPLOANRQSTTBL','BORRFEEAMT','NUMBER',22);
 cur_col_name:='P_CDCFEEDISBAMT';
 new_rec.CDCFEEDISBAMT:=runtime.validate_column_value(P_CDCFEEDISBAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCFEEDISBAMT','NUMBER',22);
 cur_col_name:='P_AMORTCDCAMT';
 new_rec.AMORTCDCAMT:=runtime.validate_column_value(P_AMORTCDCAMT,
 'STGCSA','PRPLOANRQSTTBL','AMORTCDCAMT','NUMBER',22);
 cur_col_name:='P_AMORTSBAAMT';
 new_rec.AMORTSBAAMT:=runtime.validate_column_value(P_AMORTSBAAMT,
 'STGCSA','PRPLOANRQSTTBL','AMORTSBAAMT','NUMBER',22);
 cur_col_name:='P_AMORTCSAAMT';
 new_rec.AMORTCSAAMT:=runtime.validate_column_value(P_AMORTCSAAMT,
 'STGCSA','PRPLOANRQSTTBL','AMORTCSAAMT','NUMBER',22);
 cur_col_name:='P_CDCPCT';
 new_rec.CDCPCT:=runtime.validate_column_value(P_CDCPCT,
 'STGCSA','PRPLOANRQSTTBL','CDCPCT','NUMBER',22);
 cur_col_name:='P_PREPAYPCT';
 new_rec.PREPAYPCT:=runtime.validate_column_value(P_PREPAYPCT,
 'STGCSA','PRPLOANRQSTTBL','PREPAYPCT','NUMBER',22);
 cur_col_name:='P_FEECALCAMT';
 new_rec.FEECALCAMT:=runtime.validate_column_value(P_FEECALCAMT,
 'STGCSA','PRPLOANRQSTTBL','FEECALCAMT','NUMBER',22);
 cur_col_name:='P_CSAFEECALCAMT';
 new_rec.CSAFEECALCAMT:=runtime.validate_column_value(P_CSAFEECALCAMT,
 'STGCSA','PRPLOANRQSTTBL','CSAFEECALCAMT','NUMBER',22);
 cur_col_name:='P_CSAFEECALCONLYAMT';
 new_rec.CSAFEECALCONLYAMT:=runtime.validate_column_value(P_CSAFEECALCONLYAMT,
 'STGCSA','PRPLOANRQSTTBL','CSAFEECALCONLYAMT','NUMBER',22);
 cur_col_name:='P_CDCFEECALCONLYAMT';
 new_rec.CDCFEECALCONLYAMT:=runtime.validate_column_value(P_CDCFEECALCONLYAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCFEECALCONLYAMT','NUMBER',22);
 cur_col_name:='P_FEECALCONLYAMT';
 new_rec.FEECALCONLYAMT:=runtime.validate_column_value(P_FEECALCONLYAMT,
 'STGCSA','PRPLOANRQSTTBL','FEECALCONLYAMT','NUMBER',22);
 cur_col_name:='P_FIVEYRADJ';
 new_rec.FIVEYRADJ:=runtime.validate_column_value(P_FIVEYRADJ,
 'STGCSA','PRPLOANRQSTTBL','FIVEYRADJ','NUMBER',22);
 cur_col_name:='P_FIVEYRADJCSAAMT';
 new_rec.FIVEYRADJCSAAMT:=runtime.validate_column_value(P_FIVEYRADJCSAAMT,
 'STGCSA','PRPLOANRQSTTBL','FIVEYRADJCSAAMT','NUMBER',22);
 cur_col_name:='P_FIVEYRADJCDCAMT';
 new_rec.FIVEYRADJCDCAMT:=runtime.validate_column_value(P_FIVEYRADJCDCAMT,
 'STGCSA','PRPLOANRQSTTBL','FIVEYRADJCDCAMT','NUMBER',22);
 cur_col_name:='P_FIVEYRADJSBAAMT';
 new_rec.FIVEYRADJSBAAMT:=runtime.validate_column_value(P_FIVEYRADJSBAAMT,
 'STGCSA','PRPLOANRQSTTBL','FIVEYRADJSBAAMT','NUMBER',22);
 cur_col_name:='P_BALCALC1AMT';
 new_rec.BALCALC1AMT:=runtime.validate_column_value(P_BALCALC1AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC1AMT','NUMBER',22);
 cur_col_name:='P_BALCALC2AMT';
 new_rec.BALCALC2AMT:=runtime.validate_column_value(P_BALCALC2AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC2AMT','NUMBER',22);
 cur_col_name:='P_BALCALC3AMT';
 new_rec.BALCALC3AMT:=runtime.validate_column_value(P_BALCALC3AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC3AMT','NUMBER',22);
 cur_col_name:='P_BALCALC4AMT';
 new_rec.BALCALC4AMT:=runtime.validate_column_value(P_BALCALC4AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC4AMT','NUMBER',22);
 cur_col_name:='P_BALCALC5AMT';
 new_rec.BALCALC5AMT:=runtime.validate_column_value(P_BALCALC5AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC5AMT','NUMBER',22);
 cur_col_name:='P_BALCALC6AMT';
 new_rec.BALCALC6AMT:=runtime.validate_column_value(P_BALCALC6AMT,
 'STGCSA','PRPLOANRQSTTBL','BALCALC6AMT','NUMBER',22);
 cur_col_name:='P_PRINAMT';
 new_rec.PRINAMT:=runtime.validate_column_value(P_PRINAMT,
 'STGCSA','PRPLOANRQSTTBL','PRINAMT','NUMBER',22);
 cur_col_name:='P_PRIN2AMT';
 new_rec.PRIN2AMT:=runtime.validate_column_value(P_PRIN2AMT,
 'STGCSA','PRPLOANRQSTTBL','PRIN2AMT','NUMBER',22);
 cur_col_name:='P_PRIN3AMT';
 new_rec.PRIN3AMT:=runtime.validate_column_value(P_PRIN3AMT,
 'STGCSA','PRPLOANRQSTTBL','PRIN3AMT','NUMBER',22);
 cur_col_name:='P_PRIN4AMT';
 new_rec.PRIN4AMT:=runtime.validate_column_value(P_PRIN4AMT,
 'STGCSA','PRPLOANRQSTTBL','PRIN4AMT','NUMBER',22);
 cur_col_name:='P_PRIN5AMT';
 new_rec.PRIN5AMT:=runtime.validate_column_value(P_PRIN5AMT,
 'STGCSA','PRPLOANRQSTTBL','PRIN5AMT','NUMBER',22);
 cur_col_name:='P_PRIN6AMT';
 new_rec.PRIN6AMT:=runtime.validate_column_value(P_PRIN6AMT,
 'STGCSA','PRPLOANRQSTTBL','PRIN6AMT','NUMBER',22);
 cur_col_name:='P_PRINPROJAMT';
 new_rec.PRINPROJAMT:=runtime.validate_column_value(P_PRINPROJAMT,
 'STGCSA','PRPLOANRQSTTBL','PRINPROJAMT','NUMBER',22);
 cur_col_name:='P_PIPROJAMT';
 new_rec.PIPROJAMT:=runtime.validate_column_value(P_PIPROJAMT,
 'STGCSA','PRPLOANRQSTTBL','PIPROJAMT','NUMBER',22);
 cur_col_name:='P_INTAMT';
 new_rec.INTAMT:=runtime.validate_column_value(P_INTAMT,
 'STGCSA','PRPLOANRQSTTBL','INTAMT','NUMBER',22);
 cur_col_name:='P_INT2AMT';
 new_rec.INT2AMT:=runtime.validate_column_value(P_INT2AMT,
 'STGCSA','PRPLOANRQSTTBL','INT2AMT','NUMBER',22);
 cur_col_name:='P_INT3AMT';
 new_rec.INT3AMT:=runtime.validate_column_value(P_INT3AMT,
 'STGCSA','PRPLOANRQSTTBL','INT3AMT','NUMBER',22);
 cur_col_name:='P_INT4AMT';
 new_rec.INT4AMT:=runtime.validate_column_value(P_INT4AMT,
 'STGCSA','PRPLOANRQSTTBL','INT4AMT','NUMBER',22);
 cur_col_name:='P_INT5AMT';
 new_rec.INT5AMT:=runtime.validate_column_value(P_INT5AMT,
 'STGCSA','PRPLOANRQSTTBL','INT5AMT','NUMBER',22);
 cur_col_name:='P_INT6AMT';
 new_rec.INT6AMT:=runtime.validate_column_value(P_INT6AMT,
 'STGCSA','PRPLOANRQSTTBL','INT6AMT','NUMBER',22);
 cur_col_name:='P_ESCROWAMT';
 new_rec.ESCROWAMT:=runtime.validate_column_value(P_ESCROWAMT,
 'STGCSA','PRPLOANRQSTTBL','ESCROWAMT','NUMBER',22);
 cur_col_name:='P_PI6MOAMT';
 new_rec.PI6MOAMT:=runtime.validate_column_value(P_PI6MOAMT,
 'STGCSA','PRPLOANRQSTTBL','PI6MOAMT','NUMBER',22);
 cur_col_name:='P_GFDAMT';
 new_rec.GFDAMT:=runtime.validate_column_value(P_GFDAMT,
 'STGCSA','PRPLOANRQSTTBL','GFDAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYAMT';
 new_rec.OPENGNTYAMT:=runtime.validate_column_value(P_OPENGNTYAMT,
 'STGCSA','PRPLOANRQSTTBL','OPENGNTYAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYERRAMT';
 new_rec.OPENGNTYERRAMT:=runtime.validate_column_value(P_OPENGNTYERRAMT,
 'STGCSA','PRPLOANRQSTTBL','OPENGNTYERRAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYREGAMT';
 new_rec.OPENGNTYREGAMT:=runtime.validate_column_value(P_OPENGNTYREGAMT,
 'STGCSA','PRPLOANRQSTTBL','OPENGNTYREGAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYUNALLOCAMT';
 new_rec.OPENGNTYUNALLOCAMT:=runtime.validate_column_value(P_OPENGNTYUNALLOCAMT,
 'STGCSA','PRPLOANRQSTTBL','OPENGNTYUNALLOCAMT','NUMBER',22);
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=runtime.validate_column_value(P_UNALLOCAMT,
 'STGCSA','PRPLOANRQSTTBL','UNALLOCAMT','NUMBER',22);
 cur_col_name:='P_USERLCK';
 new_rec.USERLCK:=runtime.validate_column_value(P_USERLCK,
 'STGCSA','PRPLOANRQSTTBL','USERLCK','VARCHAR2',99);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PRPLOANRQSTTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:='P_CDCSBAFEEDISBAMT';
 new_rec.CDCSBAFEEDISBAMT:=runtime.validate_column_value(P_CDCSBAFEEDISBAMT,
 'STGCSA','PRPLOANRQSTTBL','CDCSBAFEEDISBAMT','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PRPLOANRQSTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PRPLOANRQSTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PRPLOANRQSTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PRPLOANRQSTINSTSP;
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 p_errmsg:='Got dupe key on insert into PRPLOANRQSTTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ';
 else
 ||' in PRPLOANRQSTINSTSP build 2018-01-23 08:27:29';
 ROLLBACK TO PRPLOANRQSTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PRPLOANRQSTINSTSP build 2018-01-23 08:27:29'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PRPLOANRQSTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:00. --->
PROCEDURE GENEMAILCATUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CATACTV NUMBER:=null
 ,p_CATDESC VARCHAR2:=null
 ,p_CATNM VARCHAR2:=null
 ,p_DBMDL VARCHAR2:=null
 ,p_EMAILCATID NUMBER:=null
 ,p_SORTORD NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:23:55
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:23:55
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure GENEMAILCATUPDTSP performs UPDATE on STGCSA.GENEMAILCATTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMAILCATID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.GENEMAILCATTBL%rowtype;
 new_rec STGCSA.GENEMAILCATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 runtime.logger(logged_msg_id,'dbg',20,'entered GENEMAILCATUPDTSP, p_identifier='||p_identifier,
 p_userid);
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.GENEMAILCATTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILCATUPDTSP build 2018-01-23 08:23:55 '
 ||'Select of STGCSA.GENEMAILCATTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILCATUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 runtime.logger(logged_msg_id,'dbg',20,'old rec dbmdl is'||nvl(new_rec.DBMDL,'$NULL$'), p_userid);
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 runtime.logger(logged_msg_id,'dbg',20,'entered about to move parms', p_userid);
 if P_EMAILCATID is not null then
 cur_col_name:='P_EMAILCATID';
 new_rec.EMAILCATID:=P_EMAILCATID; 
 end if;
 runtime.logger(logged_msg_id,'dbg',20,'entry p_dbmdl is >'||nvl(p_DBMDL,'$null$')||'<', p_userid);
 if P_DBMDL is not null then
 cur_col_name:='P_DBMDL';
 new_rec.DBMDL:=P_DBMDL; 
 end if;
 if P_SORTORD is not null then
 cur_col_name:='P_SORTORD';
 new_rec.SORTORD:=P_SORTORD; 
 end if;
 if P_CATNM is not null then
 cur_col_name:='P_CATNM';
 new_rec.CATNM:=P_CATNM; 
 end if;
 if P_CATDESC is not null then
 cur_col_name:='P_CATDESC';
 new_rec.CATDESC:=P_CATDESC; 
 end if;
 if P_CATACTV is not null then
 cur_col_name:='P_CATACTV';
 new_rec.CATACTV:=P_CATACTV; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 runtime.logger(logged_msg_id,'dbg',20,'done move parms', p_userid);
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.GENEMAILCATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 runtime.logger(logged_msg_id,'dbg',20,'ready to update', p_userid);
 update STGCSA.GENEMAILCATTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,DBMDL=new_rec.DBMDL
 ,SORTORD=new_rec.SORTORD
 ,CATNM=new_rec.CATNM
 ,CATDESC=new_rec.CATDESC
 ,CATACTV=new_rec.CATACTV
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 runtime.logger(logged_msg_id,'dbg',20,'did update ', p_userid);
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In GENEMAILCATUPDTSP build 2018-01-23 08:23:55 '
 ||'while doing update on STGCSA.GENEMAILCATTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILCATUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENEMAILCATUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_DBMDL:=P_DBMDL; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMAILCATID)=('||P_EMAILCATID||') ';
 select rowid into rec_rowid from STGCSA.GENEMAILCATTBL where 1=1
 and EMAILCATID=P_EMAILCATID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILCATUPDTSP build 2018-01-23 08:23:55 '
 ||'No STGCSA.GENEMAILCATTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 runtime.logger(logged_msg_id,'dbg',20,'back from update', p_userid);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILCATUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In GENEMAILCATUPDTSP build 2018-01-23 08:23:55 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 runtime.logger(logged_msg_id,'dbg',20,'set log_dbmdl to >'||nvl(new_rec.DBMDL,'$NULL$')||'<', p_userid);
 
 log_DBMDL:=substr(new_rec.DBMDL,1,10);
 log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 runtime.logger(logged_msg_id,'dbg',20,'did it', p_userid);
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILCATUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 runtime.logger(logged_msg_id,'dbg',20,'logged end'||new_rec.DBMDL, p_userid);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILCATUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In GENEMAILCATUPDTSP build 2018-01-23 08:23:55'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILCATUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END GENEMAILCATUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


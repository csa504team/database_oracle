<!--- Saved 01/28/2018 14:42:28. --->
PROCEDURE SOFVBFFAINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_ASSUMPTAXID CHAR:=null
 ,p_ASSUMPNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR1NM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR2NM VARCHAR2:=null
 ,p_ASSUMPMAILADRCTYNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTCD CHAR:=null
 ,p_ASSUMPMAILADRZIPCD CHAR:=null
 ,p_ASSUMPMAILADRZIP4CD CHAR:=null
 ,p_ASSUMPOFC CHAR:=null
 ,p_ASSUMPPRGRM CHAR:=null
 ,p_ASSUMPSTATCDOFLOAN CHAR:=null
 ,p_ASSUMPVERIFICATIONIND CHAR:=null
 ,p_ASSUMPVERIFICATIONDT DATE:=null
 ,p_ASSUMPNMMAILADRCHNGIND CHAR:=null
 ,p_ASSUMPNMADRCHNGDT DATE:=null
 ,p_ASSUMPTAXFORMFLD CHAR:=null
 ,p_ASSUMPTAXMAINTNDT DATE:=null
 ,p_ASSUMPKEYLOANNMB CHAR:=null
 ,p_ASSUMPLASTEFFDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:58
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:29:58
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVBFFATBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVBFFATBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVBFFAINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBFFATBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVBFFATBL','LOANNMB','CHAR',10);
 cur_col_name:='P_ASSUMPTAXID';
 new_rec.ASSUMPTAXID:=runtime.validate_column_value(P_ASSUMPTAXID,
 'STGCSA','SOFVBFFATBL','ASSUMPTAXID','CHAR',10);
 cur_col_name:='P_ASSUMPNM';
 new_rec.ASSUMPNM:=runtime.validate_column_value(P_ASSUMPNM,
 'STGCSA','SOFVBFFATBL','ASSUMPNM','VARCHAR2',80);
 cur_col_name:='P_ASSUMPMAILADRSTR1NM';
 new_rec.ASSUMPMAILADRSTR1NM:=runtime.validate_column_value(P_ASSUMPMAILADRSTR1NM,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_ASSUMPMAILADRSTR2NM';
 new_rec.ASSUMPMAILADRSTR2NM:=runtime.validate_column_value(P_ASSUMPMAILADRSTR2NM,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_ASSUMPMAILADRCTYNM';
 new_rec.ASSUMPMAILADRCTYNM:=runtime.validate_column_value(P_ASSUMPMAILADRCTYNM,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_ASSUMPMAILADRSTCD';
 new_rec.ASSUMPMAILADRSTCD:=runtime.validate_column_value(P_ASSUMPMAILADRSTCD,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRSTCD','CHAR',2);
 cur_col_name:='P_ASSUMPMAILADRZIPCD';
 new_rec.ASSUMPMAILADRZIPCD:=runtime.validate_column_value(P_ASSUMPMAILADRZIPCD,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_ASSUMPMAILADRZIP4CD';
 new_rec.ASSUMPMAILADRZIP4CD:=runtime.validate_column_value(P_ASSUMPMAILADRZIP4CD,
 'STGCSA','SOFVBFFATBL','ASSUMPMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_ASSUMPOFC';
 new_rec.ASSUMPOFC:=runtime.validate_column_value(P_ASSUMPOFC,
 'STGCSA','SOFVBFFATBL','ASSUMPOFC','CHAR',5);
 cur_col_name:='P_ASSUMPPRGRM';
 new_rec.ASSUMPPRGRM:=runtime.validate_column_value(P_ASSUMPPRGRM,
 'STGCSA','SOFVBFFATBL','ASSUMPPRGRM','CHAR',3);
 cur_col_name:='P_ASSUMPSTATCDOFLOAN';
 new_rec.ASSUMPSTATCDOFLOAN:=runtime.validate_column_value(P_ASSUMPSTATCDOFLOAN,
 'STGCSA','SOFVBFFATBL','ASSUMPSTATCDOFLOAN','CHAR',2);
 cur_col_name:='P_ASSUMPVERIFICATIONIND';
 new_rec.ASSUMPVERIFICATIONIND:=runtime.validate_column_value(P_ASSUMPVERIFICATIONIND,
 'STGCSA','SOFVBFFATBL','ASSUMPVERIFICATIONIND','CHAR',1);
 cur_col_name:='P_ASSUMPVERIFICATIONDT';
 new_rec.ASSUMPVERIFICATIONDT:=runtime.validate_column_value(P_ASSUMPVERIFICATIONDT,
 'STGCSA','SOFVBFFATBL','ASSUMPVERIFICATIONDT','DATE',7);
 cur_col_name:='P_ASSUMPNMMAILADRCHNGIND';
 new_rec.ASSUMPNMMAILADRCHNGIND:=runtime.validate_column_value(P_ASSUMPNMMAILADRCHNGIND,
 'STGCSA','SOFVBFFATBL','ASSUMPNMMAILADRCHNGIND','CHAR',1);
 cur_col_name:='P_ASSUMPNMADRCHNGDT';
 new_rec.ASSUMPNMADRCHNGDT:=runtime.validate_column_value(P_ASSUMPNMADRCHNGDT,
 'STGCSA','SOFVBFFATBL','ASSUMPNMADRCHNGDT','DATE',7);
 cur_col_name:='P_ASSUMPTAXFORMFLD';
 new_rec.ASSUMPTAXFORMFLD:=runtime.validate_column_value(P_ASSUMPTAXFORMFLD,
 'STGCSA','SOFVBFFATBL','ASSUMPTAXFORMFLD','CHAR',1);
 cur_col_name:='P_ASSUMPTAXMAINTNDT';
 new_rec.ASSUMPTAXMAINTNDT:=runtime.validate_column_value(P_ASSUMPTAXMAINTNDT,
 'STGCSA','SOFVBFFATBL','ASSUMPTAXMAINTNDT','DATE',7);
 cur_col_name:='P_ASSUMPKEYLOANNMB';
 new_rec.ASSUMPKEYLOANNMB:=runtime.validate_column_value(P_ASSUMPKEYLOANNMB,
 'STGCSA','SOFVBFFATBL','ASSUMPKEYLOANNMB','CHAR',10);
 cur_col_name:='P_ASSUMPLASTEFFDT';
 new_rec.ASSUMPLASTEFFDT:=runtime.validate_column_value(P_ASSUMPLASTEFFDT,
 'STGCSA','SOFVBFFATBL','ASSUMPLASTEFFDT','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVBFFAINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVBFFAAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVBFFATBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVBFFAINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVBFFATBL for key '
 ||'(ASSUMPKEYLOANNMB)=('||new_rec.ASSUMPKEYLOANNMB||') ' ||'(ASSUMPLASTEFFDT)=('||new_rec.ASSUMPLASTEFFDT||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVBFFAINSTSP build 2018-01-23 08:29:58';
 ROLLBACK TO SOFVBFFAINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBFFAINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBFFAINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBFFAINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVBFFAINSTSP build 2018-01-23 08:29:58'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBFFAINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVBFFAINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


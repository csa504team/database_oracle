<!--- Saved 01/28/2018 14:41:51. --->
PROCEDURE ENTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CREATBY NUMBER:=null
 ,p_DISTCD CHAR:=null
 ,p_ENTID NUMBER:=null
 ,p_ENTMAILADRCTYNM VARCHAR2:=null
 ,p_ENTMAILADRSTCD CHAR:=null
 ,p_ENTMAILADRSTR1NM VARCHAR2:=null
 ,p_ENTMAILADRSTR2NM VARCHAR2:=null
 ,p_ENTMAILADRZIP4CD CHAR:=null
 ,p_ENTMAILADRZIPCD CHAR:=null
 ,p_ENTNM VARCHAR2:=null
 ,p_ENTNMB CHAR:=null
 ,p_ENTPHNNMB CHAR:=null
 ,p_ENTTYP VARCHAR2:=null
 ,p_REGNCD CHAR:=null
 ,p_STATCD NUMBER:=null
 ,p_UPDTBY NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:21:26
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:21:26
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure ENTUPDTSP performs UPDATE on STGCSA.ENTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ENTID
 for P_IDENTIFIER=1 qualification is on ENTNM, ENTTYP
 for P_IDENTIFIER=2 qualification is on ENTPHNNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.ENTTBL%rowtype;
 new_rec STGCSA.ENTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.ENTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'Select of STGCSA.ENTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO ENTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_ENTID is not null then
 cur_col_name:='P_ENTID';
 new_rec.ENTID:=P_ENTID; 
 end if;
 if P_ENTNM is not null then
 cur_col_name:='P_ENTNM';
 new_rec.ENTNM:=P_ENTNM; 
 end if;
 if P_ENTTYP is not null then
 cur_col_name:='P_ENTTYP';
 new_rec.ENTTYP:=P_ENTTYP; 
 end if;
 if P_ENTMAILADRSTR1NM is not null then
 cur_col_name:='P_ENTMAILADRSTR1NM';
 new_rec.ENTMAILADRSTR1NM:=P_ENTMAILADRSTR1NM; 
 end if;
 if P_ENTMAILADRSTR2NM is not null then
 cur_col_name:='P_ENTMAILADRSTR2NM';
 new_rec.ENTMAILADRSTR2NM:=P_ENTMAILADRSTR2NM; 
 end if;
 if P_ENTMAILADRCTYNM is not null then
 cur_col_name:='P_ENTMAILADRCTYNM';
 new_rec.ENTMAILADRCTYNM:=P_ENTMAILADRCTYNM; 
 end if;
 if P_ENTMAILADRSTCD is not null then
 cur_col_name:='P_ENTMAILADRSTCD';
 new_rec.ENTMAILADRSTCD:=P_ENTMAILADRSTCD; 
 end if;
 if P_ENTMAILADRZIPCD is not null then
 cur_col_name:='P_ENTMAILADRZIPCD';
 new_rec.ENTMAILADRZIPCD:=P_ENTMAILADRZIPCD; 
 end if;
 if P_ENTMAILADRZIP4CD is not null then
 cur_col_name:='P_ENTMAILADRZIP4CD';
 new_rec.ENTMAILADRZIP4CD:=P_ENTMAILADRZIP4CD; 
 end if;
 if P_ENTPHNNMB is not null then
 cur_col_name:='P_ENTPHNNMB';
 new_rec.ENTPHNNMB:=P_ENTPHNNMB; 
 end if;
 if P_CREATBY is not null then
 cur_col_name:='P_CREATBY';
 new_rec.CREATBY:=P_CREATBY; 
 end if;
 if P_UPDTBY is not null then
 cur_col_name:='P_UPDTBY';
 new_rec.UPDTBY:=P_UPDTBY; 
 end if;
 if P_REGNCD is not null then
 cur_col_name:='P_REGNCD';
 new_rec.REGNCD:=P_REGNCD; 
 end if;
 if P_ENTNMB is not null then
 cur_col_name:='P_ENTNMB';
 new_rec.ENTNMB:=P_ENTNMB; 
 end if;
 if P_DISTCD is not null then
 cur_col_name:='P_DISTCD';
 new_rec.DISTCD:=P_DISTCD; 
 end if;
 if P_STATCD is not null then
 cur_col_name:='P_STATCD';
 new_rec.STATCD:=P_STATCD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.ENTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.ENTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,ENTNM=new_rec.ENTNM
 ,ENTTYP=new_rec.ENTTYP
 ,ENTMAILADRSTR1NM=new_rec.ENTMAILADRSTR1NM
 ,ENTMAILADRSTR2NM=new_rec.ENTMAILADRSTR2NM
 ,ENTMAILADRCTYNM=new_rec.ENTMAILADRCTYNM
 ,ENTMAILADRSTCD=new_rec.ENTMAILADRSTCD
 ,ENTMAILADRZIPCD=new_rec.ENTMAILADRZIPCD
 ,ENTMAILADRZIP4CD=new_rec.ENTMAILADRZIP4CD
 ,ENTPHNNMB=new_rec.ENTPHNNMB
 ,CREATBY=new_rec.CREATBY
 ,UPDTBY=new_rec.UPDTBY
 ,REGNCD=new_rec.REGNCD
 ,ENTNMB=new_rec.ENTNMB
 ,DISTCD=new_rec.DISTCD
 ,STATCD=new_rec.STATCD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'while doing update on STGCSA.ENTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO ENTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ENTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ENTID)=('||P_ENTID||') ';
 select rowid into rec_rowid from STGCSA.ENTTBL where 1=1
 and ENTID=P_ENTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'No STGCSA.ENTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(ENTNM)=('||P_ENTNM||') '||
 '(ENTTYP)=('||P_ENTTYP||') '||'';
 begin
 for r1 in (select rowid from STGCSA.ENTTBL a where 1=1 
 and ENTNM=P_ENTNM
 and ENTTYP=P_ENTTYP
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ENTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'Oracle returned error fetching STGCSA.ENTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ENTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'No STGCSA.ENTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(ENTPHNNMB)=('||P_ENTPHNNMB||') '||'';
 begin
 for r2 in (select rowid from STGCSA.ENTTBL a where 1=1 
 and ENTPHNNMB=P_ENTPHNNMB
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ENTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'Oracle returned error fetching STGCSA.ENTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ENTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'No STGCSA.ENTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ENTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ENTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ENTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In ENTUPDTSP build 2018-01-23 08:21:26'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ENTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END ENTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


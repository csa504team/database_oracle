<!--- Saved 10/31/2017 13:18:31. --->
PROCEDURE DATALOADHISTRYInsTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_HISTRYID NUMBER:=NULL, P_PROCESSID NUMBER:=NULL,
 P_PROCESSSTARTDT TIMESTAMP:=NULL, P_USERID VARCHAR2:=NULL)
 
 as
 /*
 <!--- generated 2017-09-28 14:04:13 by SYS --->
 Created on: 2017-09-28 14:04:13
 Created by: SYS
 Crerated from genr, stdtblins_template v 1.0
 Purpose : standard insert of row to table STGCSA.DATALOADHISTRYTBL
 */
 BEGIN 
 savepoint DATALOADHISTRYInsTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 insert into STGCSA.DATALOADHISTRYTBL (
 HISTRYID, PROCESSID, PROCESSSTARTDT, USERID
 ) values (
 P_HISTRYID, P_PROCESSID, P_PROCESSSTARTDT, P_USERID
 );
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO DATALOADHISTRYInsTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END DATALOADHISTRYInsTsp;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


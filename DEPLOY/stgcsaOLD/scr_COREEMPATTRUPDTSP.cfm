<!--- Saved 01/28/2018 14:41:42. --->
PROCEDURE COREEMPATTRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACTVATTR NUMBER:=null
 ,p_ATTRTYPID NUMBER:=null
 ,p_ATTRVAL VARCHAR2:=null
 ,p_EFFDT DATE:=null
 ,p_EMPATTRID NUMBER:=null
 ,p_EMPATTRSHOW NUMBER:=null
 ,p_EMPID NUMBER:=null
 ,p_TRMDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:57
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:18:57
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure COREEMPATTRUPDTSP performs UPDATE on STGCSA.COREEMPATTRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMPATTRID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.COREEMPATTRTBL%rowtype;
 new_rec STGCSA.COREEMPATTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.COREEMPATTRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPATTRUPDTSP build 2018-01-23 08:18:57 '
 ||'Select of STGCSA.COREEMPATTRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPATTRUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_EMPATTRID is not null then
 cur_col_name:='P_EMPATTRID';
 new_rec.EMPATTRID:=P_EMPATTRID; 
 end if;
 if P_EMPID is not null then
 cur_col_name:='P_EMPID';
 new_rec.EMPID:=P_EMPID; 
 end if;
 if P_ATTRVAL is not null then
 cur_col_name:='P_ATTRVAL';
 new_rec.ATTRVAL:=P_ATTRVAL; 
 end if;
 if P_ATTRTYPID is not null then
 cur_col_name:='P_ATTRTYPID';
 new_rec.ATTRTYPID:=P_ATTRTYPID; 
 end if;
 if P_EFFDT is not null then
 cur_col_name:='P_EFFDT';
 new_rec.EFFDT:=P_EFFDT; 
 end if;
 if P_TRMDT is not null then
 cur_col_name:='P_TRMDT';
 new_rec.TRMDT:=P_TRMDT; 
 end if;
 if P_ACTVATTR is not null then
 cur_col_name:='P_ACTVATTR';
 new_rec.ACTVATTR:=P_ACTVATTR; 
 end if;
 if P_EMPATTRSHOW is not null then
 cur_col_name:='P_EMPATTRSHOW';
 new_rec.EMPATTRSHOW:=P_EMPATTRSHOW; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.COREEMPATTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.COREEMPATTRTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,EMPID=new_rec.EMPID
 ,ATTRVAL=new_rec.ATTRVAL
 ,ATTRTYPID=new_rec.ATTRTYPID
 ,EFFDT=new_rec.EFFDT
 ,TRMDT=new_rec.TRMDT
 ,ACTVATTR=new_rec.ACTVATTR
 ,EMPATTRSHOW=new_rec.EMPATTRSHOW
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In COREEMPATTRUPDTSP build 2018-01-23 08:18:57 '
 ||'while doing update on STGCSA.COREEMPATTRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO COREEMPATTRUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint COREEMPATTRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMPATTRID)=('||P_EMPATTRID||') ';
 select rowid into rec_rowid from STGCSA.COREEMPATTRTBL where 1=1
 and EMPATTRID=P_EMPATTRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In COREEMPATTRUPDTSP build 2018-01-23 08:18:57 '
 ||'No STGCSA.COREEMPATTRTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO COREEMPATTRUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In COREEMPATTRUPDTSP build 2018-01-23 08:18:57 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End COREEMPATTRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'COREEMPATTRUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In COREEMPATTRUPDTSP build 2018-01-23 08:18:57'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO COREEMPATTRUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END COREEMPATTRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


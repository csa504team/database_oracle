<!--- Saved 01/28/2018 14:42:33. --->
PROCEDURE SOFVCDCMSTRINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCCERTNMB CHAR:=null
 ,p_CDCPOLKNMB CHAR:=null
 ,p_CDCNM VARCHAR2:=null
 ,p_CDCMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCMAILADRCTYNM VARCHAR2:=null
 ,p_CDCMAILADRSTCD CHAR:=null
 ,p_CDCMAILADRZIPCD CHAR:=null
 ,p_CDCMAILADRZIP4CD CHAR:=null
 ,p_CDCCNTCT VARCHAR2:=null
 ,p_CDCAREACD CHAR:=null
 ,p_CDCEXCH CHAR:=null
 ,p_CDCEXTN CHAR:=null
 ,p_CDCSTATCD CHAR:=null
 ,p_CDCTAXID CHAR:=null
 ,p_CDCSETUPDT DATE:=null
 ,p_CDCLMAINTNDT DATE:=null
 ,p_CDCTOTDOLLRDBENTRAMT NUMBER:=null
 ,p_CDCTOTNMBPART NUMBER:=null
 ,p_CDCTOTPAIDDBENTRAMT NUMBER:=null
 ,p_CDCHLDPYMT CHAR:=null
 ,p_CDCDIST CHAR:=null
 ,p_CDCMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHBNK VARCHAR2:=null
 ,p_CDCACHMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCACHMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHMAILADRCTYNM VARCHAR2:=null
 ,p_CDCACHMAILADRSTCD CHAR:=null
 ,p_CDCACHMAILADRZIPCD CHAR:=null
 ,p_CDCACHMAILADRZIP4CD CHAR:=null
 ,p_CDCACHBR CHAR:=null
 ,p_CDCACHACCTTYP CHAR:=null
 ,p_CDCACHACCTNMB VARCHAR2:=null
 ,p_CDCACHROUTSYM CHAR:=null
 ,p_CDCACHTRANSCD CHAR:=null
 ,p_CDCACHBNKIDNMB VARCHAR2:=null
 ,p_CDCACHDEPSTR VARCHAR2:=null
 ,p_CDCACHSIGNDT DATE:=null
 ,p_CDCPRENOTETESTDT DATE:=null
 ,p_CDCACHPRENTIND CHAR:=null
 ,p_CDCCLSBALAMT NUMBER:=null
 ,p_CDCCLSBALDT DATE:=null
 ,p_CDCNETPREPAYAMT NUMBER:=null
 ,p_CDCFAXAREACD CHAR:=null
 ,p_CDCFAXEXCH CHAR:=null
 ,p_CDCFAXEXTN CHAR:=null
 ,p_CDCPREPAYCLSBALAMT NUMBER:=null
 ,p_CDCNAMADDRID CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:16
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:31:16
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVCDCMSTRTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVCDCMSTRINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCDCMSTRTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CDCREGNCD';
 new_rec.CDCREGNCD:=runtime.validate_column_value(P_CDCREGNCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCREGNCD','CHAR',2);
 cur_col_name:='P_CDCCERTNMB';
 new_rec.CDCCERTNMB:=runtime.validate_column_value(P_CDCCERTNMB,
 'STGCSA','SOFVCDCMSTRTBL','CDCCERTNMB','CHAR',4);
 cur_col_name:='P_CDCPOLKNMB';
 new_rec.CDCPOLKNMB:=runtime.validate_column_value(P_CDCPOLKNMB,
 'STGCSA','SOFVCDCMSTRTBL','CDCPOLKNMB','CHAR',7);
 cur_col_name:='P_CDCNM';
 new_rec.CDCNM:=runtime.validate_column_value(P_CDCNM,
 'STGCSA','SOFVCDCMSTRTBL','CDCNM','VARCHAR2',80);
 cur_col_name:='P_CDCMAILADRSTR1NM';
 new_rec.CDCMAILADRSTR1NM:=runtime.validate_column_value(P_CDCMAILADRSTR1NM,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_CDCMAILADRCTYNM';
 new_rec.CDCMAILADRCTYNM:=runtime.validate_column_value(P_CDCMAILADRCTYNM,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_CDCMAILADRSTCD';
 new_rec.CDCMAILADRSTCD:=runtime.validate_column_value(P_CDCMAILADRSTCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRSTCD','CHAR',2);
 cur_col_name:='P_CDCMAILADRZIPCD';
 new_rec.CDCMAILADRZIPCD:=runtime.validate_column_value(P_CDCMAILADRZIPCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_CDCMAILADRZIP4CD';
 new_rec.CDCMAILADRZIP4CD:=runtime.validate_column_value(P_CDCMAILADRZIP4CD,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_CDCCNTCT';
 new_rec.CDCCNTCT:=runtime.validate_column_value(P_CDCCNTCT,
 'STGCSA','SOFVCDCMSTRTBL','CDCCNTCT','VARCHAR2',80);
 cur_col_name:='P_CDCAREACD';
 new_rec.CDCAREACD:=runtime.validate_column_value(P_CDCAREACD,
 'STGCSA','SOFVCDCMSTRTBL','CDCAREACD','CHAR',3);
 cur_col_name:='P_CDCEXCH';
 new_rec.CDCEXCH:=runtime.validate_column_value(P_CDCEXCH,
 'STGCSA','SOFVCDCMSTRTBL','CDCEXCH','CHAR',3);
 cur_col_name:='P_CDCEXTN';
 new_rec.CDCEXTN:=runtime.validate_column_value(P_CDCEXTN,
 'STGCSA','SOFVCDCMSTRTBL','CDCEXTN','CHAR',4);
 cur_col_name:='P_CDCSTATCD';
 new_rec.CDCSTATCD:=runtime.validate_column_value(P_CDCSTATCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCSTATCD','CHAR',2);
 cur_col_name:='P_CDCTAXID';
 new_rec.CDCTAXID:=runtime.validate_column_value(P_CDCTAXID,
 'STGCSA','SOFVCDCMSTRTBL','CDCTAXID','CHAR',10);
 cur_col_name:='P_CDCSETUPDT';
 new_rec.CDCSETUPDT:=runtime.validate_column_value(P_CDCSETUPDT,
 'STGCSA','SOFVCDCMSTRTBL','CDCSETUPDT','DATE',7);
 cur_col_name:='P_CDCLMAINTNDT';
 new_rec.CDCLMAINTNDT:=runtime.validate_column_value(P_CDCLMAINTNDT,
 'STGCSA','SOFVCDCMSTRTBL','CDCLMAINTNDT','DATE',7);
 cur_col_name:='P_CDCTOTDOLLRDBENTRAMT';
 new_rec.CDCTOTDOLLRDBENTRAMT:=runtime.validate_column_value(P_CDCTOTDOLLRDBENTRAMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCTOTDOLLRDBENTRAMT','NUMBER',22);
 cur_col_name:='P_CDCTOTNMBPART';
 new_rec.CDCTOTNMBPART:=runtime.validate_column_value(P_CDCTOTNMBPART,
 'STGCSA','SOFVCDCMSTRTBL','CDCTOTNMBPART','NUMBER',22);
 cur_col_name:='P_CDCTOTPAIDDBENTRAMT';
 new_rec.CDCTOTPAIDDBENTRAMT:=runtime.validate_column_value(P_CDCTOTPAIDDBENTRAMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCTOTPAIDDBENTRAMT','NUMBER',22);
 cur_col_name:='P_CDCHLDPYMT';
 new_rec.CDCHLDPYMT:=runtime.validate_column_value(P_CDCHLDPYMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCHLDPYMT','CHAR',1);
 cur_col_name:='P_CDCDIST';
 new_rec.CDCDIST:=runtime.validate_column_value(P_CDCDIST,
 'STGCSA','SOFVCDCMSTRTBL','CDCDIST','CHAR',5);
 cur_col_name:='P_CDCMAILADRSTR2NM';
 new_rec.CDCMAILADRSTR2NM:=runtime.validate_column_value(P_CDCMAILADRSTR2NM,
 'STGCSA','SOFVCDCMSTRTBL','CDCMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_CDCACHBNK';
 new_rec.CDCACHBNK:=runtime.validate_column_value(P_CDCACHBNK,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHBNK','VARCHAR2',80);
 cur_col_name:='P_CDCACHMAILADRSTR1NM';
 new_rec.CDCACHMAILADRSTR1NM:=runtime.validate_column_value(P_CDCACHMAILADRSTR1NM,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_CDCACHMAILADRSTR2NM';
 new_rec.CDCACHMAILADRSTR2NM:=runtime.validate_column_value(P_CDCACHMAILADRSTR2NM,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_CDCACHMAILADRCTYNM';
 new_rec.CDCACHMAILADRCTYNM:=runtime.validate_column_value(P_CDCACHMAILADRCTYNM,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_CDCACHMAILADRSTCD';
 new_rec.CDCACHMAILADRSTCD:=runtime.validate_column_value(P_CDCACHMAILADRSTCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRSTCD','CHAR',2);
 cur_col_name:='P_CDCACHMAILADRZIPCD';
 new_rec.CDCACHMAILADRZIPCD:=runtime.validate_column_value(P_CDCACHMAILADRZIPCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_CDCACHMAILADRZIP4CD';
 new_rec.CDCACHMAILADRZIP4CD:=runtime.validate_column_value(P_CDCACHMAILADRZIP4CD,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_CDCACHBR';
 new_rec.CDCACHBR:=runtime.validate_column_value(P_CDCACHBR,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHBR','CHAR',15);
 cur_col_name:='P_CDCACHACCTTYP';
 new_rec.CDCACHACCTTYP:=runtime.validate_column_value(P_CDCACHACCTTYP,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHACCTTYP','CHAR',1);
 cur_col_name:='P_CDCACHACCTNMB';
 new_rec.CDCACHACCTNMB:=runtime.validate_column_value(P_CDCACHACCTNMB,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHACCTNMB','VARCHAR2',20);
 cur_col_name:='P_CDCACHROUTSYM';
 new_rec.CDCACHROUTSYM:=runtime.validate_column_value(P_CDCACHROUTSYM,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHROUTSYM','CHAR',15);
 cur_col_name:='P_CDCACHTRANSCD';
 new_rec.CDCACHTRANSCD:=runtime.validate_column_value(P_CDCACHTRANSCD,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHTRANSCD','CHAR',15);
 cur_col_name:='P_CDCACHBNKIDNMB';
 new_rec.CDCACHBNKIDNMB:=runtime.validate_column_value(P_CDCACHBNKIDNMB,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHBNKIDNMB','VARCHAR2',80);
 cur_col_name:='P_CDCACHDEPSTR';
 new_rec.CDCACHDEPSTR:=runtime.validate_column_value(P_CDCACHDEPSTR,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHDEPSTR','VARCHAR2',80);
 cur_col_name:='P_CDCACHSIGNDT';
 new_rec.CDCACHSIGNDT:=runtime.validate_column_value(P_CDCACHSIGNDT,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHSIGNDT','DATE',7);
 cur_col_name:='P_CDCPRENOTETESTDT';
 new_rec.CDCPRENOTETESTDT:=runtime.validate_column_value(P_CDCPRENOTETESTDT,
 'STGCSA','SOFVCDCMSTRTBL','CDCPRENOTETESTDT','DATE',7);
 cur_col_name:='P_CDCACHPRENTIND';
 new_rec.CDCACHPRENTIND:=runtime.validate_column_value(P_CDCACHPRENTIND,
 'STGCSA','SOFVCDCMSTRTBL','CDCACHPRENTIND','CHAR',1);
 cur_col_name:='P_CDCCLSBALAMT';
 new_rec.CDCCLSBALAMT:=runtime.validate_column_value(P_CDCCLSBALAMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCCLSBALAMT','NUMBER',22);
 cur_col_name:='P_CDCCLSBALDT';
 new_rec.CDCCLSBALDT:=runtime.validate_column_value(P_CDCCLSBALDT,
 'STGCSA','SOFVCDCMSTRTBL','CDCCLSBALDT','DATE',7);
 cur_col_name:='P_CDCNETPREPAYAMT';
 new_rec.CDCNETPREPAYAMT:=runtime.validate_column_value(P_CDCNETPREPAYAMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCNETPREPAYAMT','NUMBER',22);
 cur_col_name:='P_CDCFAXAREACD';
 new_rec.CDCFAXAREACD:=runtime.validate_column_value(P_CDCFAXAREACD,
 'STGCSA','SOFVCDCMSTRTBL','CDCFAXAREACD','CHAR',3);
 cur_col_name:='P_CDCFAXEXCH';
 new_rec.CDCFAXEXCH:=runtime.validate_column_value(P_CDCFAXEXCH,
 'STGCSA','SOFVCDCMSTRTBL','CDCFAXEXCH','CHAR',3);
 cur_col_name:='P_CDCFAXEXTN';
 new_rec.CDCFAXEXTN:=runtime.validate_column_value(P_CDCFAXEXTN,
 'STGCSA','SOFVCDCMSTRTBL','CDCFAXEXTN','CHAR',4);
 cur_col_name:='P_CDCPREPAYCLSBALAMT';
 new_rec.CDCPREPAYCLSBALAMT:=runtime.validate_column_value(P_CDCPREPAYCLSBALAMT,
 'STGCSA','SOFVCDCMSTRTBL','CDCPREPAYCLSBALAMT','NUMBER',22);
 cur_col_name:='P_CDCNAMADDRID';
 new_rec.CDCNAMADDRID:=runtime.validate_column_value(P_CDCNAMADDRID,
 'STGCSA','SOFVCDCMSTRTBL','CDCNAMADDRID','CHAR',6);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVCDCMSTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVCDCMSTRTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVCDCMSTRTBL for key '
 ||'(CDCREGNCD)=('||new_rec.CDCREGNCD||') ' ||'(CDCCERTNMB)=('||new_rec.CDCCERTNMB||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVCDCMSTRINSTSP build 2018-01-23 08:31:16';
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCDCMSTRINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCDCMSTRINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVCDCMSTRINSTSP build 2018-01-23 08:31:16'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVCDCMSTRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:58. --->
PROCEDURE TEMPMACROQUERYUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCCERTNMB CHAR:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CREATUSERID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_RECORDID NUMBER:=null
 ,p_STATUS CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:37:26
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:37:26
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure TEMPMACROQUERYUPDTSP performs UPDATE on STGCSA.TEMPMACROQUERYTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: RECORDID
 for P_IDENTIFIER=1 qualification is on CREATUSERID is not null or 1=1
 for P_IDENTIFIER=2 qualification is on CREATUSERID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.TEMPMACROQUERYTBL%rowtype;
 new_rec STGCSA.TEMPMACROQUERYTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.TEMPMACROQUERYTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'Select of STGCSA.TEMPMACROQUERYTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_RECORDID is not null then
 cur_col_name:='P_RECORDID';
 new_rec.RECORDID:=P_RECORDID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_CDCREGNCD is not null then
 cur_col_name:='P_CDCREGNCD';
 new_rec.CDCREGNCD:=P_CDCREGNCD; 
 end if;
 if P_CDCCERTNMB is not null then
 cur_col_name:='P_CDCCERTNMB';
 new_rec.CDCCERTNMB:=P_CDCCERTNMB; 
 end if;
 if P_STATUS is not null then
 cur_col_name:='P_STATUS';
 new_rec.STATUS:=P_STATUS; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.TEMPMACROQUERYAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.TEMPMACROQUERYTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,CDCREGNCD=new_rec.CDCREGNCD
 ,CDCCERTNMB=new_rec.CDCCERTNMB
 ,STATUS=new_rec.STATUS
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'while doing update on STGCSA.TEMPMACROQUERYTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint TEMPMACROQUERYUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(RECORDID)=('||P_RECORDID||') ';
 select rowid into rec_rowid from STGCSA.TEMPMACROQUERYTBL where 1=1
 and RECORDID=P_RECORDID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'No STGCSA.TEMPMACROQUERYTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(CREATUSERID is not null or 1=1) '||'';
 begin
 for r1 in (select rowid from STGCSA.TEMPMACROQUERYTBL a where 1=1 
 
 and CREATUSERID is not null or 1=1
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'Oracle returned error fetching STGCSA.TEMPMACROQUERYTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'No STGCSA.TEMPMACROQUERYTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(CREATUSERID)=('||P_CREATUSERID||') '||'';
 begin
 for r2 in (select rowid from STGCSA.TEMPMACROQUERYTBL a where 1=1 
 and CREATUSERID=P_CREATUSERID
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'Oracle returned error fetching STGCSA.TEMPMACROQUERYTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'No STGCSA.TEMPMACROQUERYTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPMACROQUERYUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'TEMPMACROQUERYUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In TEMPMACROQUERYUPDTSP build 2018-01-23 08:37:26'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END TEMPMACROQUERYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


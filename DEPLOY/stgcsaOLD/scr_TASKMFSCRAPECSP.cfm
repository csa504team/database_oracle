<!--- Saved 01/28/2018 14:42:52. --->
PROCEDURE TaskMFScrapeCSP ( p_CreatUserId varchar2 := NULL )
 AS
 p_retval number := 0;
 p_errval number := 0;
 p_errmsg varchar2(500) := '';
 BEGIN
 STGCSA.TaskActionCSP ('CSA2STG', 'E', 'CSA_Batch');
 
 UPDATE stgcsa.JobQueTbl
 SET StatCd = 'P',
 StrtDt = sysdate,
 CommTxt = 'MF Scraping in Processing...'
 WHERE StrtDt IS NULL
 AND StatCd = 'I'
 AND JobTypCd = '04';
 
 STGCSA.TaskStartCSP('MFSCRAPE', p_creatuserid, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
 STGCSA.TaskEndCSP('MFSCRAPE', p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
 
 UPDATE stgcsa.JobQueTbl
 SET StatCd = 'S',
 EndDt = sysdate,
 CommTxt = 'MF Scraping finished'
 WHERE EndDt IS NULL
 AND StatCd = 'P'
 AND JobTypCd = '04';
 
 COMMIT;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:22. --->
PROCEDURE PRPRVWSTATUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_FEEVAL NUMBER:=null
 ,p_FEEVALBY VARCHAR2:=null
 ,p_FIFTHPOSTRVW NUMBER:=null
 ,p_FIFTHPOSTRVWBY VARCHAR2:=null
 ,p_FOURTHPOSTRVW NUMBER:=null
 ,p_FOURTHPOSTRVWBY VARCHAR2:=null
 ,p_INITIALPOSTRVW NUMBER:=null
 ,p_INITIALPOSTRVWBY VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_PROOFVAL NUMBER:=null
 ,p_PROOFVALBY VARCHAR2:=null
 ,p_PRPRVWSTATID NUMBER:=null
 ,p_RVWSBMT NUMBER:=null
 ,p_RVWSBMTBY VARCHAR2:=null
 ,p_RVWSBMTDT DATE:=null
 ,p_SCNDPOSTRVW NUMBER:=null
 ,p_SCNDPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST2RVW NUMBER:=null
 ,p_SIXTHMOPOST2RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST3RVW NUMBER:=null
 ,p_SIXTHMOPOST3RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST4RVW NUMBER:=null
 ,p_SIXTHMOPOST4RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST5RVW NUMBER:=null
 ,p_SIXTHMOPOST5RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOSTRVW NUMBER:=null
 ,p_SIXTHMOPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHPOSTRVW NUMBER:=null
 ,p_SIXTHPOSTRVWBY VARCHAR2:=null
 ,p_THIRDPOSTRVW NUMBER:=null
 ,p_THIRDPOSTRVWBY VARCHAR2:=null
 ,p_THISTRVW NUMBER:=null
 ,p_THISTRVWBY VARCHAR2:=null
 ,p_THISTVAL NUMBER:=null
 ,p_THISTVALBY VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ,p_VALSBMT NUMBER:=null
 ,p_VALSBMTBY VARCHAR2:=null
 ,p_VALSBMTDT DATE:=null
 ,p_WORKUPVAL NUMBER:=null
 ,p_WORKUPVALBY VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:13
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:29:13
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPRVWSTATUPDTSP performs UPDATE on STGCSA.PRPRVWSTATTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPRVWSTATID
 for P_IDENTIFIER=1 qualification is on TRANSID
 for P_IDENTIFIER=2 qualification is on TRANSTYPID, TRANSID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPRVWSTATTBL%rowtype;
 new_rec STGCSA.PRPRVWSTATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPRVWSTATTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'Select of STGCSA.PRPRVWSTATTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPRVWSTATUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PRPRVWSTATID is not null then
 cur_col_name:='P_PRPRVWSTATID';
 new_rec.PRPRVWSTATID:=P_PRPRVWSTATID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_TRANSTYPID is not null then
 cur_col_name:='P_TRANSTYPID';
 new_rec.TRANSTYPID:=P_TRANSTYPID; 
 end if;
 if P_INITIALPOSTRVW is not null then
 cur_col_name:='P_INITIALPOSTRVW';
 new_rec.INITIALPOSTRVW:=P_INITIALPOSTRVW; 
 end if;
 if P_INITIALPOSTRVWBY is not null then
 cur_col_name:='P_INITIALPOSTRVWBY';
 new_rec.INITIALPOSTRVWBY:=P_INITIALPOSTRVWBY; 
 end if;
 if P_SCNDPOSTRVW is not null then
 cur_col_name:='P_SCNDPOSTRVW';
 new_rec.SCNDPOSTRVW:=P_SCNDPOSTRVW; 
 end if;
 if P_SCNDPOSTRVWBY is not null then
 cur_col_name:='P_SCNDPOSTRVWBY';
 new_rec.SCNDPOSTRVWBY:=P_SCNDPOSTRVWBY; 
 end if;
 if P_THIRDPOSTRVW is not null then
 cur_col_name:='P_THIRDPOSTRVW';
 new_rec.THIRDPOSTRVW:=P_THIRDPOSTRVW; 
 end if;
 if P_THIRDPOSTRVWBY is not null then
 cur_col_name:='P_THIRDPOSTRVWBY';
 new_rec.THIRDPOSTRVWBY:=P_THIRDPOSTRVWBY; 
 end if;
 if P_FOURTHPOSTRVW is not null then
 cur_col_name:='P_FOURTHPOSTRVW';
 new_rec.FOURTHPOSTRVW:=P_FOURTHPOSTRVW; 
 end if;
 if P_FOURTHPOSTRVWBY is not null then
 cur_col_name:='P_FOURTHPOSTRVWBY';
 new_rec.FOURTHPOSTRVWBY:=P_FOURTHPOSTRVWBY; 
 end if;
 if P_FIFTHPOSTRVW is not null then
 cur_col_name:='P_FIFTHPOSTRVW';
 new_rec.FIFTHPOSTRVW:=P_FIFTHPOSTRVW; 
 end if;
 if P_FIFTHPOSTRVWBY is not null then
 cur_col_name:='P_FIFTHPOSTRVWBY';
 new_rec.FIFTHPOSTRVWBY:=P_FIFTHPOSTRVWBY; 
 end if;
 if P_SIXTHPOSTRVW is not null then
 cur_col_name:='P_SIXTHPOSTRVW';
 new_rec.SIXTHPOSTRVW:=P_SIXTHPOSTRVW; 
 end if;
 if P_SIXTHPOSTRVWBY is not null then
 cur_col_name:='P_SIXTHPOSTRVWBY';
 new_rec.SIXTHPOSTRVWBY:=P_SIXTHPOSTRVWBY; 
 end if;
 if P_SIXTHMOPOSTRVW is not null then
 cur_col_name:='P_SIXTHMOPOSTRVW';
 new_rec.SIXTHMOPOSTRVW:=P_SIXTHMOPOSTRVW; 
 end if;
 if P_SIXTHMOPOSTRVWBY is not null then
 cur_col_name:='P_SIXTHMOPOSTRVWBY';
 new_rec.SIXTHMOPOSTRVWBY:=P_SIXTHMOPOSTRVWBY; 
 end if;
 if P_SIXTHMOPOST2RVW is not null then
 cur_col_name:='P_SIXTHMOPOST2RVW';
 new_rec.SIXTHMOPOST2RVW:=P_SIXTHMOPOST2RVW; 
 end if;
 if P_SIXTHMOPOST2RVWBY is not null then
 cur_col_name:='P_SIXTHMOPOST2RVWBY';
 new_rec.SIXTHMOPOST2RVWBY:=P_SIXTHMOPOST2RVWBY; 
 end if;
 if P_SIXTHMOPOST3RVW is not null then
 cur_col_name:='P_SIXTHMOPOST3RVW';
 new_rec.SIXTHMOPOST3RVW:=P_SIXTHMOPOST3RVW; 
 end if;
 if P_SIXTHMOPOST3RVWBY is not null then
 cur_col_name:='P_SIXTHMOPOST3RVWBY';
 new_rec.SIXTHMOPOST3RVWBY:=P_SIXTHMOPOST3RVWBY; 
 end if;
 if P_SIXTHMOPOST4RVW is not null then
 cur_col_name:='P_SIXTHMOPOST4RVW';
 new_rec.SIXTHMOPOST4RVW:=P_SIXTHMOPOST4RVW; 
 end if;
 if P_SIXTHMOPOST4RVWBY is not null then
 cur_col_name:='P_SIXTHMOPOST4RVWBY';
 new_rec.SIXTHMOPOST4RVWBY:=P_SIXTHMOPOST4RVWBY; 
 end if;
 if P_SIXTHMOPOST5RVW is not null then
 cur_col_name:='P_SIXTHMOPOST5RVW';
 new_rec.SIXTHMOPOST5RVW:=P_SIXTHMOPOST5RVW; 
 end if;
 if P_SIXTHMOPOST5RVWBY is not null then
 cur_col_name:='P_SIXTHMOPOST5RVWBY';
 new_rec.SIXTHMOPOST5RVWBY:=P_SIXTHMOPOST5RVWBY; 
 end if;
 if P_THISTRVW is not null then
 cur_col_name:='P_THISTRVW';
 new_rec.THISTRVW:=P_THISTRVW; 
 end if;
 if P_THISTRVWBY is not null then
 cur_col_name:='P_THISTRVWBY';
 new_rec.THISTRVWBY:=P_THISTRVWBY; 
 end if;
 if P_FEEVAL is not null then
 cur_col_name:='P_FEEVAL';
 new_rec.FEEVAL:=P_FEEVAL; 
 end if;
 if P_FEEVALBY is not null then
 cur_col_name:='P_FEEVALBY';
 new_rec.FEEVALBY:=P_FEEVALBY; 
 end if;
 if P_RVWSBMT is not null then
 cur_col_name:='P_RVWSBMT';
 new_rec.RVWSBMT:=P_RVWSBMT; 
 end if;
 if P_RVWSBMTBY is not null then
 cur_col_name:='P_RVWSBMTBY';
 new_rec.RVWSBMTBY:=P_RVWSBMTBY; 
 end if;
 if P_RVWSBMTDT is not null then
 cur_col_name:='P_RVWSBMTDT';
 new_rec.RVWSBMTDT:=P_RVWSBMTDT; 
 end if;
 if P_WORKUPVAL is not null then
 cur_col_name:='P_WORKUPVAL';
 new_rec.WORKUPVAL:=P_WORKUPVAL; 
 end if;
 if P_WORKUPVALBY is not null then
 cur_col_name:='P_WORKUPVALBY';
 new_rec.WORKUPVALBY:=P_WORKUPVALBY; 
 end if;
 if P_THISTVAL is not null then
 cur_col_name:='P_THISTVAL';
 new_rec.THISTVAL:=P_THISTVAL; 
 end if;
 if P_THISTVALBY is not null then
 cur_col_name:='P_THISTVALBY';
 new_rec.THISTVALBY:=P_THISTVALBY; 
 end if;
 if P_VALSBMT is not null then
 cur_col_name:='P_VALSBMT';
 new_rec.VALSBMT:=P_VALSBMT; 
 end if;
 if P_VALSBMTBY is not null then
 cur_col_name:='P_VALSBMTBY';
 new_rec.VALSBMTBY:=P_VALSBMTBY; 
 end if;
 if P_VALSBMTDT is not null then
 cur_col_name:='P_VALSBMTDT';
 new_rec.VALSBMTDT:=P_VALSBMTDT; 
 end if;
 if P_PROOFVAL is not null then
 cur_col_name:='P_PROOFVAL';
 new_rec.PROOFVAL:=P_PROOFVAL; 
 end if;
 if P_PROOFVALBY is not null then
 cur_col_name:='P_PROOFVALBY';
 new_rec.PROOFVALBY:=P_PROOFVALBY; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPRVWSTATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPRVWSTATTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,TRANSID=new_rec.TRANSID
 ,TRANSTYPID=new_rec.TRANSTYPID
 ,INITIALPOSTRVW=new_rec.INITIALPOSTRVW
 ,INITIALPOSTRVWBY=new_rec.INITIALPOSTRVWBY
 ,SCNDPOSTRVW=new_rec.SCNDPOSTRVW
 ,SCNDPOSTRVWBY=new_rec.SCNDPOSTRVWBY
 ,THIRDPOSTRVW=new_rec.THIRDPOSTRVW
 ,THIRDPOSTRVWBY=new_rec.THIRDPOSTRVWBY
 ,FOURTHPOSTRVW=new_rec.FOURTHPOSTRVW
 ,FOURTHPOSTRVWBY=new_rec.FOURTHPOSTRVWBY
 ,FIFTHPOSTRVW=new_rec.FIFTHPOSTRVW
 ,FIFTHPOSTRVWBY=new_rec.FIFTHPOSTRVWBY
 ,SIXTHPOSTRVW=new_rec.SIXTHPOSTRVW
 ,SIXTHPOSTRVWBY=new_rec.SIXTHPOSTRVWBY
 ,SIXTHMOPOSTRVW=new_rec.SIXTHMOPOSTRVW
 ,SIXTHMOPOSTRVWBY=new_rec.SIXTHMOPOSTRVWBY
 ,SIXTHMOPOST2RVW=new_rec.SIXTHMOPOST2RVW
 ,SIXTHMOPOST2RVWBY=new_rec.SIXTHMOPOST2RVWBY
 ,SIXTHMOPOST3RVW=new_rec.SIXTHMOPOST3RVW
 ,SIXTHMOPOST3RVWBY=new_rec.SIXTHMOPOST3RVWBY
 ,SIXTHMOPOST4RVW=new_rec.SIXTHMOPOST4RVW
 ,SIXTHMOPOST4RVWBY=new_rec.SIXTHMOPOST4RVWBY
 ,SIXTHMOPOST5RVW=new_rec.SIXTHMOPOST5RVW
 ,SIXTHMOPOST5RVWBY=new_rec.SIXTHMOPOST5RVWBY
 ,THISTRVW=new_rec.THISTRVW
 ,THISTRVWBY=new_rec.THISTRVWBY
 ,FEEVAL=new_rec.FEEVAL
 ,FEEVALBY=new_rec.FEEVALBY
 ,RVWSBMT=new_rec.RVWSBMT
 ,RVWSBMTBY=new_rec.RVWSBMTBY
 ,RVWSBMTDT=new_rec.RVWSBMTDT
 ,WORKUPVAL=new_rec.WORKUPVAL
 ,WORKUPVALBY=new_rec.WORKUPVALBY
 ,THISTVAL=new_rec.THISTVAL
 ,THISTVALBY=new_rec.THISTVALBY
 ,VALSBMT=new_rec.VALSBMT
 ,VALSBMTBY=new_rec.VALSBMTBY
 ,VALSBMTDT=new_rec.VALSBMTDT
 ,PROOFVAL=new_rec.PROOFVAL
 ,PROOFVALBY=new_rec.PROOFVALBY
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'while doing update on STGCSA.PRPRVWSTATTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPRVWSTATUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPRVWSTATUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPRVWSTATID)=('||P_PRPRVWSTATID||') ';
 select rowid into rec_rowid from STGCSA.PRPRVWSTATTBL where 1=1
 and PRPRVWSTATID=P_PRPRVWSTATID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'No STGCSA.PRPRVWSTATTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1 
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPRVWSTATUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'Oracle returned error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPRVWSTATUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'No STGCSA.PRPRVWSTATTBL row(s) to update with key(s) '
 ||keystouse;
 -- case to update one or more rows based on keyset KEYS2
 end if; 
 end if;
 when 2 then 
 if p_errval=0 then
 
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||
 '(TRANSTYPID)=('||P_TRANSTYPID||') '||'';
 begin
 for r2 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1 
 and TRANSID=P_TRANSID
 and TRANSTYPID=P_TRANSTYPID
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PRPRVWSTATUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'Oracle returned error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PRPRVWSTATUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'No STGCSA.PRPRVWSTATTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPRVWSTATUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB; log_TRANSID:=new_rec.TRANSID; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPRVWSTATUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPRVWSTATUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PRPRVWSTATUPDTSP build 2018-01-23 08:29:13'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPRVWSTATUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPRVWSTATUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


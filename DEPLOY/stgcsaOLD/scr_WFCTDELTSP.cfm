<!--- Saved 01/28/2018 14:42:58. --->
PROCEDURE WFCTDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCNMB CHAR:=null
 ,p_LOANNMB CHAR:=null
 ,p_REPDT NUMBER:=null
 ,p_WFCTAMT NUMBER:=null
 ,p_WFCTID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:37:57
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:37:57
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure WFCTDELTSP performs DELETE on STGCSA.WFCTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: WFCTID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.WFCTTBL%rowtype;
 new_rec STGCSA.WFCTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.WFCTTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In WFCTDELTSP build 2018-01-23 08:37:57 '
 ||'Select of STGCSA.WFCTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO WFCTDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.WFCTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.WFCTTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In WFCTDELTSP build 2018-01-23 08:37:57 '
 ||'while doing delete on STGCSA.WFCTTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO WFCTDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint WFCTDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(WFCTID)=('||P_WFCTID||') ';
 select rowid into rec_rowid from STGCSA.WFCTTBL where 1=1
 and WFCTID=P_WFCTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In WFCTDELTSP build 2018-01-23 08:37:57 '
 ||'No STGCSA.WFCTTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO WFCTDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In WFCTDELTSP build 2018-01-23 08:37:57 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End WFCTDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'WFCTDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in WFCTDELTSP build 2018-01-23 08:37:57'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO WFCTDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END WFCTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


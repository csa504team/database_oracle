<!--- Saved 01/28/2018 14:42:33. --->
PROCEDURE SOFVCDCMSTRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCACHACCTNMB VARCHAR2:=null
 ,p_CDCACHACCTTYP CHAR:=null
 ,p_CDCACHBNK VARCHAR2:=null
 ,p_CDCACHBNKIDNMB VARCHAR2:=null
 ,p_CDCACHBR CHAR:=null
 ,p_CDCACHDEPSTR VARCHAR2:=null
 ,p_CDCACHMAILADRCTYNM VARCHAR2:=null
 ,p_CDCACHMAILADRSTCD CHAR:=null
 ,p_CDCACHMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCACHMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHMAILADRZIP4CD CHAR:=null
 ,p_CDCACHMAILADRZIPCD CHAR:=null
 ,p_CDCACHPRENTIND CHAR:=null
 ,p_CDCACHROUTSYM CHAR:=null
 ,p_CDCACHSIGNDT DATE:=null
 ,p_CDCACHTRANSCD CHAR:=null
 ,p_CDCAREACD CHAR:=null
 ,p_CDCCERTNMB CHAR:=null
 ,p_CDCCLSBALAMT NUMBER:=null
 ,p_CDCCLSBALDT DATE:=null
 ,p_CDCCNTCT VARCHAR2:=null
 ,p_CDCDIST CHAR:=null
 ,p_CDCEXCH CHAR:=null
 ,p_CDCEXTN CHAR:=null
 ,p_CDCFAXAREACD CHAR:=null
 ,p_CDCFAXEXCH CHAR:=null
 ,p_CDCFAXEXTN CHAR:=null
 ,p_CDCHLDPYMT CHAR:=null
 ,p_CDCLMAINTNDT DATE:=null
 ,p_CDCMAILADRCTYNM VARCHAR2:=null
 ,p_CDCMAILADRSTCD CHAR:=null
 ,p_CDCMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCMAILADRZIP4CD CHAR:=null
 ,p_CDCMAILADRZIPCD CHAR:=null
 ,p_CDCNAMADDRID CHAR:=null
 ,p_CDCNETPREPAYAMT NUMBER:=null
 ,p_CDCNM VARCHAR2:=null
 ,p_CDCPOLKNMB CHAR:=null
 ,p_CDCPRENOTETESTDT DATE:=null
 ,p_CDCPREPAYCLSBALAMT NUMBER:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCSETUPDT DATE:=null
 ,p_CDCSTATCD CHAR:=null
 ,p_CDCTAXID CHAR:=null
 ,p_CDCTOTDOLLRDBENTRAMT NUMBER:=null
 ,p_CDCTOTNMBPART NUMBER:=null
 ,p_CDCTOTPAIDDBENTRAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:11
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:31:11
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVCDCMSTRUPDTSP performs UPDATE on STGCSA.SOFVCDCMSTRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CDCREGNCD, CDCCERTNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 new_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVCDCMSTRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCDCMSTRUPDTSP build 2018-01-23 08:31:11 '
 ||'Select of STGCSA.SOFVCDCMSTRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCDCMSTRUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CDCREGNCD is not null then
 cur_col_name:='P_CDCREGNCD';
 new_rec.CDCREGNCD:=P_CDCREGNCD; 
 end if;
 if P_CDCCERTNMB is not null then
 cur_col_name:='P_CDCCERTNMB';
 new_rec.CDCCERTNMB:=P_CDCCERTNMB; 
 end if;
 if P_CDCPOLKNMB is not null then
 cur_col_name:='P_CDCPOLKNMB';
 new_rec.CDCPOLKNMB:=P_CDCPOLKNMB; 
 end if;
 if P_CDCNM is not null then
 cur_col_name:='P_CDCNM';
 new_rec.CDCNM:=P_CDCNM; 
 end if;
 if P_CDCMAILADRSTR1NM is not null then
 cur_col_name:='P_CDCMAILADRSTR1NM';
 new_rec.CDCMAILADRSTR1NM:=P_CDCMAILADRSTR1NM; 
 end if;
 if P_CDCMAILADRCTYNM is not null then
 cur_col_name:='P_CDCMAILADRCTYNM';
 new_rec.CDCMAILADRCTYNM:=P_CDCMAILADRCTYNM; 
 end if;
 if P_CDCMAILADRSTCD is not null then
 cur_col_name:='P_CDCMAILADRSTCD';
 new_rec.CDCMAILADRSTCD:=P_CDCMAILADRSTCD; 
 end if;
 if P_CDCMAILADRZIPCD is not null then
 cur_col_name:='P_CDCMAILADRZIPCD';
 new_rec.CDCMAILADRZIPCD:=P_CDCMAILADRZIPCD; 
 end if;
 if P_CDCMAILADRZIP4CD is not null then
 cur_col_name:='P_CDCMAILADRZIP4CD';
 new_rec.CDCMAILADRZIP4CD:=P_CDCMAILADRZIP4CD; 
 end if;
 if P_CDCCNTCT is not null then
 cur_col_name:='P_CDCCNTCT';
 new_rec.CDCCNTCT:=P_CDCCNTCT; 
 end if;
 if P_CDCAREACD is not null then
 cur_col_name:='P_CDCAREACD';
 new_rec.CDCAREACD:=P_CDCAREACD; 
 end if;
 if P_CDCEXCH is not null then
 cur_col_name:='P_CDCEXCH';
 new_rec.CDCEXCH:=P_CDCEXCH; 
 end if;
 if P_CDCEXTN is not null then
 cur_col_name:='P_CDCEXTN';
 new_rec.CDCEXTN:=P_CDCEXTN; 
 end if;
 if P_CDCSTATCD is not null then
 cur_col_name:='P_CDCSTATCD';
 new_rec.CDCSTATCD:=P_CDCSTATCD; 
 end if;
 if P_CDCTAXID is not null then
 cur_col_name:='P_CDCTAXID';
 new_rec.CDCTAXID:=P_CDCTAXID; 
 end if;
 if P_CDCSETUPDT is not null then
 cur_col_name:='P_CDCSETUPDT';
 new_rec.CDCSETUPDT:=P_CDCSETUPDT; 
 end if;
 if P_CDCLMAINTNDT is not null then
 cur_col_name:='P_CDCLMAINTNDT';
 new_rec.CDCLMAINTNDT:=P_CDCLMAINTNDT; 
 end if;
 if P_CDCTOTDOLLRDBENTRAMT is not null then
 cur_col_name:='P_CDCTOTDOLLRDBENTRAMT';
 new_rec.CDCTOTDOLLRDBENTRAMT:=P_CDCTOTDOLLRDBENTRAMT; 
 end if;
 if P_CDCTOTNMBPART is not null then
 cur_col_name:='P_CDCTOTNMBPART';
 new_rec.CDCTOTNMBPART:=P_CDCTOTNMBPART; 
 end if;
 if P_CDCTOTPAIDDBENTRAMT is not null then
 cur_col_name:='P_CDCTOTPAIDDBENTRAMT';
 new_rec.CDCTOTPAIDDBENTRAMT:=P_CDCTOTPAIDDBENTRAMT; 
 end if;
 if P_CDCHLDPYMT is not null then
 cur_col_name:='P_CDCHLDPYMT';
 new_rec.CDCHLDPYMT:=P_CDCHLDPYMT; 
 end if;
 if P_CDCDIST is not null then
 cur_col_name:='P_CDCDIST';
 new_rec.CDCDIST:=P_CDCDIST; 
 end if;
 if P_CDCMAILADRSTR2NM is not null then
 cur_col_name:='P_CDCMAILADRSTR2NM';
 new_rec.CDCMAILADRSTR2NM:=P_CDCMAILADRSTR2NM; 
 end if;
 if P_CDCACHBNK is not null then
 cur_col_name:='P_CDCACHBNK';
 new_rec.CDCACHBNK:=P_CDCACHBNK; 
 end if;
 if P_CDCACHMAILADRSTR1NM is not null then
 cur_col_name:='P_CDCACHMAILADRSTR1NM';
 new_rec.CDCACHMAILADRSTR1NM:=P_CDCACHMAILADRSTR1NM; 
 end if;
 if P_CDCACHMAILADRSTR2NM is not null then
 cur_col_name:='P_CDCACHMAILADRSTR2NM';
 new_rec.CDCACHMAILADRSTR2NM:=P_CDCACHMAILADRSTR2NM; 
 end if;
 if P_CDCACHMAILADRCTYNM is not null then
 cur_col_name:='P_CDCACHMAILADRCTYNM';
 new_rec.CDCACHMAILADRCTYNM:=P_CDCACHMAILADRCTYNM; 
 end if;
 if P_CDCACHMAILADRSTCD is not null then
 cur_col_name:='P_CDCACHMAILADRSTCD';
 new_rec.CDCACHMAILADRSTCD:=P_CDCACHMAILADRSTCD; 
 end if;
 if P_CDCACHMAILADRZIPCD is not null then
 cur_col_name:='P_CDCACHMAILADRZIPCD';
 new_rec.CDCACHMAILADRZIPCD:=P_CDCACHMAILADRZIPCD; 
 end if;
 if P_CDCACHMAILADRZIP4CD is not null then
 cur_col_name:='P_CDCACHMAILADRZIP4CD';
 new_rec.CDCACHMAILADRZIP4CD:=P_CDCACHMAILADRZIP4CD; 
 end if;
 if P_CDCACHBR is not null then
 cur_col_name:='P_CDCACHBR';
 new_rec.CDCACHBR:=P_CDCACHBR; 
 end if;
 if P_CDCACHACCTTYP is not null then
 cur_col_name:='P_CDCACHACCTTYP';
 new_rec.CDCACHACCTTYP:=P_CDCACHACCTTYP; 
 end if;
 if P_CDCACHACCTNMB is not null then
 cur_col_name:='P_CDCACHACCTNMB';
 new_rec.CDCACHACCTNMB:=P_CDCACHACCTNMB; 
 end if;
 if P_CDCACHROUTSYM is not null then
 cur_col_name:='P_CDCACHROUTSYM';
 new_rec.CDCACHROUTSYM:=P_CDCACHROUTSYM; 
 end if;
 if P_CDCACHTRANSCD is not null then
 cur_col_name:='P_CDCACHTRANSCD';
 new_rec.CDCACHTRANSCD:=P_CDCACHTRANSCD; 
 end if;
 if P_CDCACHBNKIDNMB is not null then
 cur_col_name:='P_CDCACHBNKIDNMB';
 new_rec.CDCACHBNKIDNMB:=P_CDCACHBNKIDNMB; 
 end if;
 if P_CDCACHDEPSTR is not null then
 cur_col_name:='P_CDCACHDEPSTR';
 new_rec.CDCACHDEPSTR:=P_CDCACHDEPSTR; 
 end if;
 if P_CDCACHSIGNDT is not null then
 cur_col_name:='P_CDCACHSIGNDT';
 new_rec.CDCACHSIGNDT:=P_CDCACHSIGNDT; 
 end if;
 if P_CDCPRENOTETESTDT is not null then
 cur_col_name:='P_CDCPRENOTETESTDT';
 new_rec.CDCPRENOTETESTDT:=P_CDCPRENOTETESTDT; 
 end if;
 if P_CDCACHPRENTIND is not null then
 cur_col_name:='P_CDCACHPRENTIND';
 new_rec.CDCACHPRENTIND:=P_CDCACHPRENTIND; 
 end if;
 if P_CDCCLSBALAMT is not null then
 cur_col_name:='P_CDCCLSBALAMT';
 new_rec.CDCCLSBALAMT:=P_CDCCLSBALAMT; 
 end if;
 if P_CDCCLSBALDT is not null then
 cur_col_name:='P_CDCCLSBALDT';
 new_rec.CDCCLSBALDT:=P_CDCCLSBALDT; 
 end if;
 if P_CDCNETPREPAYAMT is not null then
 cur_col_name:='P_CDCNETPREPAYAMT';
 new_rec.CDCNETPREPAYAMT:=P_CDCNETPREPAYAMT; 
 end if;
 if P_CDCFAXAREACD is not null then
 cur_col_name:='P_CDCFAXAREACD';
 new_rec.CDCFAXAREACD:=P_CDCFAXAREACD; 
 end if;
 if P_CDCFAXEXCH is not null then
 cur_col_name:='P_CDCFAXEXCH';
 new_rec.CDCFAXEXCH:=P_CDCFAXEXCH; 
 end if;
 if P_CDCFAXEXTN is not null then
 cur_col_name:='P_CDCFAXEXTN';
 new_rec.CDCFAXEXTN:=P_CDCFAXEXTN; 
 end if;
 if P_CDCPREPAYCLSBALAMT is not null then
 cur_col_name:='P_CDCPREPAYCLSBALAMT';
 new_rec.CDCPREPAYCLSBALAMT:=P_CDCPREPAYCLSBALAMT; 
 end if;
 if P_CDCNAMADDRID is not null then
 cur_col_name:='P_CDCNAMADDRID';
 new_rec.CDCNAMADDRID:=P_CDCNAMADDRID; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVCDCMSTRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVCDCMSTRTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,CDCPOLKNMB=new_rec.CDCPOLKNMB
 ,CDCNM=new_rec.CDCNM
 ,CDCMAILADRSTR1NM=new_rec.CDCMAILADRSTR1NM
 ,CDCMAILADRCTYNM=new_rec.CDCMAILADRCTYNM
 ,CDCMAILADRSTCD=new_rec.CDCMAILADRSTCD
 ,CDCMAILADRZIPCD=new_rec.CDCMAILADRZIPCD
 ,CDCMAILADRZIP4CD=new_rec.CDCMAILADRZIP4CD
 ,CDCCNTCT=new_rec.CDCCNTCT
 ,CDCAREACD=new_rec.CDCAREACD
 ,CDCEXCH=new_rec.CDCEXCH
 ,CDCEXTN=new_rec.CDCEXTN
 ,CDCSTATCD=new_rec.CDCSTATCD
 ,CDCTAXID=new_rec.CDCTAXID
 ,CDCSETUPDT=new_rec.CDCSETUPDT
 ,CDCLMAINTNDT=new_rec.CDCLMAINTNDT
 ,CDCTOTDOLLRDBENTRAMT=new_rec.CDCTOTDOLLRDBENTRAMT
 ,CDCTOTNMBPART=new_rec.CDCTOTNMBPART
 ,CDCTOTPAIDDBENTRAMT=new_rec.CDCTOTPAIDDBENTRAMT
 ,CDCHLDPYMT=new_rec.CDCHLDPYMT
 ,CDCDIST=new_rec.CDCDIST
 ,CDCMAILADRSTR2NM=new_rec.CDCMAILADRSTR2NM
 ,CDCACHBNK=new_rec.CDCACHBNK
 ,CDCACHMAILADRSTR1NM=new_rec.CDCACHMAILADRSTR1NM
 ,CDCACHMAILADRSTR2NM=new_rec.CDCACHMAILADRSTR2NM
 ,CDCACHMAILADRCTYNM=new_rec.CDCACHMAILADRCTYNM
 ,CDCACHMAILADRSTCD=new_rec.CDCACHMAILADRSTCD
 ,CDCACHMAILADRZIPCD=new_rec.CDCACHMAILADRZIPCD
 ,CDCACHMAILADRZIP4CD=new_rec.CDCACHMAILADRZIP4CD
 ,CDCACHBR=new_rec.CDCACHBR
 ,CDCACHACCTTYP=new_rec.CDCACHACCTTYP
 ,CDCACHACCTNMB=new_rec.CDCACHACCTNMB
 ,CDCACHROUTSYM=new_rec.CDCACHROUTSYM
 ,CDCACHTRANSCD=new_rec.CDCACHTRANSCD
 ,CDCACHBNKIDNMB=new_rec.CDCACHBNKIDNMB
 ,CDCACHDEPSTR=new_rec.CDCACHDEPSTR
 ,CDCACHSIGNDT=new_rec.CDCACHSIGNDT
 ,CDCPRENOTETESTDT=new_rec.CDCPRENOTETESTDT
 ,CDCACHPRENTIND=new_rec.CDCACHPRENTIND
 ,CDCCLSBALAMT=new_rec.CDCCLSBALAMT
 ,CDCCLSBALDT=new_rec.CDCCLSBALDT
 ,CDCNETPREPAYAMT=new_rec.CDCNETPREPAYAMT
 ,CDCFAXAREACD=new_rec.CDCFAXAREACD
 ,CDCFAXEXCH=new_rec.CDCFAXEXCH
 ,CDCFAXEXTN=new_rec.CDCFAXEXTN
 ,CDCPREPAYCLSBALAMT=new_rec.CDCPREPAYCLSBALAMT
 ,CDCNAMADDRID=new_rec.CDCNAMADDRID
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVCDCMSTRUPDTSP build 2018-01-23 08:31:11 '
 ||'while doing update on STGCSA.SOFVCDCMSTRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVCDCMSTRUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVCDCMSTRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCDCMSTRTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CDCREGNCD)=('||P_CDCREGNCD||') ''(CDCCERTNMB)=('||P_CDCCERTNMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVCDCMSTRTBL where 1=1
 and CDCREGNCD=P_CDCREGNCD and CDCCERTNMB=P_CDCCERTNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVCDCMSTRUPDTSP build 2018-01-23 08:31:11 '
 ||'No STGCSA.SOFVCDCMSTRTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCDCMSTRUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVCDCMSTRUPDTSP build 2018-01-23 08:31:11 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCDCMSTRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCDCMSTRUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVCDCMSTRUPDTSP build 2018-01-23 08:31:11'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCDCMSTRUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVCDCMSTRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


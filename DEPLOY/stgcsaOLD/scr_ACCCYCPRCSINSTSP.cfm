<!--- Saved 01/28/2018 14:41:28. --->
PROCEDURE ACCCYCPRCSINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRCSID NUMBER:=null
 ,p_SORTORD NUMBER:=null
 ,p_PRCSCD VARCHAR2:=null
 ,p_PRCSNM VARCHAR2:=null
 ,p_PRCSDESC VARCHAR2:=null
 ,p_CYCPRCSACTV NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:16:19
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:16:19
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCCYCPRCSTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.ACCCYCPRCSTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint ACCCYCPRCSINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PRCSID';
 new_rec.PRCSID:=runtime.validate_column_value(P_PRCSID,
 'STGCSA','ACCCYCPRCSTBL','PRCSID','NUMBER',22);
 cur_col_name:='P_SORTORD';
 new_rec.SORTORD:=runtime.validate_column_value(P_SORTORD,
 'STGCSA','ACCCYCPRCSTBL','SORTORD','NUMBER',22);
 cur_col_name:='P_PRCSCD';
 new_rec.PRCSCD:=runtime.validate_column_value(P_PRCSCD,
 'STGCSA','ACCCYCPRCSTBL','PRCSCD','VARCHAR2',50);
 cur_col_name:='P_PRCSNM';
 new_rec.PRCSNM:=runtime.validate_column_value(P_PRCSNM,
 'STGCSA','ACCCYCPRCSTBL','PRCSNM','VARCHAR2',100);
 cur_col_name:='P_PRCSDESC';
 new_rec.PRCSDESC:=runtime.validate_column_value(P_PRCSDESC,
 'STGCSA','ACCCYCPRCSTBL','PRCSDESC','VARCHAR2',250);
 cur_col_name:='P_CYCPRCSACTV';
 new_rec.CYCPRCSACTV:=runtime.validate_column_value(P_CYCPRCSACTV,
 'STGCSA','ACCCYCPRCSTBL','CYCPRCSACTV','NUMBER',22);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','ACCCYCPRCSTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO ACCCYCPRCSINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.ACCCYCPRCSAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.ACCCYCPRCSTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO ACCCYCPRCSINSTSP;
 p_errmsg:='Got dupe key on insert into ACCCYCPRCSTBL for key '
 ||'(PRCSID)=('||new_rec.PRCSID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCCYCPRCSINSTSP build 2018-01-23 08:16:19';
 ROLLBACK TO ACCCYCPRCSINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCCYCPRCSINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCCYCPRCSINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCCYCPRCSINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in ACCCYCPRCSINSTSP build 2018-01-23 08:16:19'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCCYCPRCSINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END ACCCYCPRCSINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


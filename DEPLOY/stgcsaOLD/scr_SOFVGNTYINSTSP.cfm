<!--- Saved 01/28/2018 14:42:39. --->
PROCEDURE SOFVGNTYINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_GNTYDT DATE:=null
 ,p_GNTYTYP CHAR:=null
 ,p_GNTYGNTYAMT NUMBER:=null
 ,p_GNTYGNTYORGLDT DATE:=null
 ,p_GNTYSTATCD CHAR:=null
 ,p_GNTYCLSDT DATE:=null
 ,p_GNTYCREATDT DATE:=null
 ,p_GNTYLASTMAINTNDT DATE:=null
 ,p_GNTYCMNT VARCHAR2:=null
 ,p_GNTYCMNT2 VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:58
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:32:58
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVGNTYTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVGNTYTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVGNTYINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVGNTYTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVGNTYTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_GNTYDT';
 new_rec.GNTYDT:=runtime.validate_column_value(P_GNTYDT,
 'STGCSA','SOFVGNTYTBL','GNTYDT','DATE',7);
 cur_col_name:='P_GNTYTYP';
 new_rec.GNTYTYP:=runtime.validate_column_value(P_GNTYTYP,
 'STGCSA','SOFVGNTYTBL','GNTYTYP','CHAR',1);
 cur_col_name:='P_GNTYGNTYAMT';
 new_rec.GNTYGNTYAMT:=runtime.validate_column_value(P_GNTYGNTYAMT,
 'STGCSA','SOFVGNTYTBL','GNTYGNTYAMT','NUMBER',22);
 cur_col_name:='P_GNTYGNTYORGLDT';
 new_rec.GNTYGNTYORGLDT:=runtime.validate_column_value(P_GNTYGNTYORGLDT,
 'STGCSA','SOFVGNTYTBL','GNTYGNTYORGLDT','DATE',7);
 cur_col_name:='P_GNTYSTATCD';
 new_rec.GNTYSTATCD:=runtime.validate_column_value(P_GNTYSTATCD,
 'STGCSA','SOFVGNTYTBL','GNTYSTATCD','CHAR',1);
 cur_col_name:='P_GNTYCLSDT';
 new_rec.GNTYCLSDT:=runtime.validate_column_value(P_GNTYCLSDT,
 'STGCSA','SOFVGNTYTBL','GNTYCLSDT','DATE',7);
 cur_col_name:='P_GNTYCREATDT';
 new_rec.GNTYCREATDT:=runtime.validate_column_value(P_GNTYCREATDT,
 'STGCSA','SOFVGNTYTBL','GNTYCREATDT','DATE',7);
 cur_col_name:='P_GNTYLASTMAINTNDT';
 new_rec.GNTYLASTMAINTNDT:=runtime.validate_column_value(P_GNTYLASTMAINTNDT,
 'STGCSA','SOFVGNTYTBL','GNTYLASTMAINTNDT','DATE',7);
 cur_col_name:='P_GNTYCMNT';
 new_rec.GNTYCMNT:=runtime.validate_column_value(P_GNTYCMNT,
 'STGCSA','SOFVGNTYTBL','GNTYCMNT','VARCHAR2',80);
 cur_col_name:='P_GNTYCMNT2';
 new_rec.GNTYCMNT2:=runtime.validate_column_value(P_GNTYCMNT2,
 'STGCSA','SOFVGNTYTBL','GNTYCMNT2','VARCHAR2',80);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVGNTYINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVGNTYAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVGNTYTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVGNTYINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVGNTYTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ' ||'(GNTYDT)=('||new_rec.GNTYDT||') ' ||'(GNTYTYP)=('||new_rec.GNTYTYP||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVGNTYINSTSP build 2018-01-23 08:32:58';
 ROLLBACK TO SOFVGNTYINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVGNTYINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVGNTYINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVGNTYINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVGNTYINSTSP build 2018-01-23 08:32:58'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVGNTYINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVGNTYINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


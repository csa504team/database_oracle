<!--- Saved 01/28/2018 14:42:32. --->
PROCEDURE SOFVCDCMSTRAUDTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2,
 action_cd char,
 AUDTEDTCD char,
 old_rec STGCSA.SOFVCDCMSTRTBL%rowtype,
 new_rec STGCSA.SOFVCDCMSTRTBL%rowtype) as
 /*
 Created on: 2018-01-23 08:31:09
 Created by: GENR
 Crerated from template auditme_template v2.02 20 Dec 2017 on 2018-01-23 08:31:09
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Purpose : Write to SOFVAUDTTBL for specific changed columns in
 STGCSA.SOFVCDCMSTRTBL (SOFVCDCMSTRTBL)
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 aud_rec STGCSA.SOFVAUDTTBL%rowtype;
 n number:=0;
 e number:=0;
 tsp timestamp;
 key_from_new_rec STGCSA.SOFVAUDTTBL.AUDTRECRDKEY%type;
 begin
 savepoint SOFVCDCMSTRAUDTSP;
 -- hopeful return
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=null;
 -- setup log rec
 select systimestamp,sysdate,sys_context('userenv','session_user')
 into aud_rec.audttransdt, aud_rec.CREATDT, aud_rec.creatuserid from dual;
 aud_rec.LASTUPDTDT:=aud_rec.CREATDT;
 aud_rec.lastupdtuserid:=aud_rec.creatuserid;
 aud_rec.audtseqnmb:=0;
 aud_rec.AUDTOPRTRINITIAL:=' ';
 aud_rec.AUDTTRMNLID:=' ';
 aud_rec.AUDTACTNCD:=action_cd;
 aud_rec.AUDTEDTCD:=AUDTEDTCD;
 aud_rec.AUDTUSERID:=p_userid;
 -- save key value... if add or del then one rec is nulls
 aud_rec.audtrecrdkey:=rpad(substr(''
 ||old_rec.CDCREGNCD ||old_rec.CDCCERTNMB,1,15),15);
 key_from_new_rec:=rpad(substr(''
 ||new_rec.CDCREGNCD ||new_rec.CDCCERTNMB,1,15),15);
 if aud_rec.audtrecrdkey is null then
 aud_rec.audtrecrdkey:=key_from_new_rec;
 end if;
 -- are any columns auditing cares about different?
 if old_rec.CDCMAILADRCTYNM=new_rec.CDCMAILADRCTYNM 
 or (old_rec.CDCMAILADRCTYNM is null and new_rec.CDCMAILADRCTYNM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCMAILADRCTYNM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCMAILADRCTYNM; 
 end if; 
 if new_rec.CDCMAILADRCTYNM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCMAILADRCTYNM; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCCITY ';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCMAILADRSTCD=new_rec.CDCMAILADRSTCD 
 or (old_rec.CDCMAILADRSTCD is null and new_rec.CDCMAILADRSTCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCMAILADRSTCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCMAILADRSTCD; 
 end if; 
 if new_rec.CDCMAILADRSTCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCMAILADRSTCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCSTATE ';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCMAILADRZIPCD=new_rec.CDCMAILADRZIPCD 
 or (old_rec.CDCMAILADRZIPCD is null and new_rec.CDCMAILADRZIPCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCMAILADRZIPCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCMAILADRZIPCD; 
 end if; 
 if new_rec.CDCMAILADRZIPCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCMAILADRZIPCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCZIP ';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCSTATCD=new_rec.CDCSTATCD 
 or (old_rec.CDCSTATCD is null and new_rec.CDCSTATCD is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCSTATCD is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCSTATCD; 
 end if; 
 if new_rec.CDCSTATCD is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCSTATCD; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCSTATUS';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCACHMAILADRSTR1NM=new_rec.CDCACHMAILADRSTR1NM 
 or (old_rec.CDCACHMAILADRSTR1NM is null and new_rec.CDCACHMAILADRSTR1NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCACHMAILADRSTR1NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCACHMAILADRSTR1NM; 
 end if; 
 if new_rec.CDCACHMAILADRSTR1NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCACHMAILADRSTR1NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCSTREET';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCACHMAILADRSTR2NM=new_rec.CDCACHMAILADRSTR2NM 
 or (old_rec.CDCACHMAILADRSTR2NM is null and new_rec.CDCACHMAILADRSTR2NM is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCACHMAILADRSTR2NM is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCACHMAILADRSTR2NM; 
 end if; 
 if new_rec.CDCACHMAILADRSTR2NM is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCACHMAILADRSTR2NM; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCSTRET2';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 if old_rec.CDCNETPREPAYAMT=new_rec.CDCNETPREPAYAMT 
 or (old_rec.CDCNETPREPAYAMT is null and new_rec.CDCNETPREPAYAMT is null) 
 then 
 null;
 else 
 aud_rec.audtseqnmb:=aud_rec.audtseqnmb+1;
 if old_rec.CDCNETPREPAYAMT is null then
 aud_rec.AUDTBEFCNTNTS:='$NULL$';
 else 
 aud_rec.AUDTBEFCNTNTS:=old_rec.CDCNETPREPAYAMT; 
 end if; 
 if new_rec.CDCNETPREPAYAMT is null then
 aud_rec.AUDTAFTCNTNTS:='$NULL$'; 
 else
 aud_rec.AUDTAFTCNTNTS:=new_rec.CDCNETPREPAYAMT; 
 end if;
 
 aud_rec.AUDTFLDNM:='CCNETPPM';
 aud_rec.audtchngfile:='BGCDCMST';
 insert into stgcsa.sofvaudttbl values aud_rec;
 end if;
 p_retval:=aud_rec.audtseqnmb;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:='In OXH in SOFVCDCMSTRAUDTSP Build 2018-01-23 08:31:10 got: '||sqlerrm(SQLCODE);
 ROLLBACK TO SOFVCDCMSTRAUDTSP;
 --
 END SOFVCDCMSTRAUDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


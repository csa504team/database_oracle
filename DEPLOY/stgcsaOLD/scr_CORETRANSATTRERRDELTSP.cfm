<!--- Saved 01/28/2018 14:41:45. --->
PROCEDURE CORETRANSATTRERRDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ATTRID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSACTV NUMBER:=null
 ,p_TRANSERRID NUMBER:=null
 ,p_TRANSID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:20:12
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:20:12
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CORETRANSATTRERRDELTSP performs DELETE on STGCSA.CORETRANSATTRERRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: TRANSERRID
 for P_IDENTIFIER=1 qualification is on TRANSID, ATTRID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CORETRANSATTRERRTBL%rowtype;
 new_rec STGCSA.CORETRANSATTRERRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CORETRANSATTRERRTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'Select of STGCSA.CORETRANSATTRERRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CORETRANSATTRERRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.CORETRANSATTRERRTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'while doing delete on STGCSA.CORETRANSATTRERRTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CORETRANSATTRERRDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(TRANSERRID)=('||P_TRANSERRID||') ';
 select rowid into rec_rowid from STGCSA.CORETRANSATTRERRTBL where 1=1
 and TRANSERRID=P_TRANSERRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'No STGCSA.CORETRANSATTRERRTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to delete one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 keystouse:='(ATTRID)=('||P_ATTRID||') '||
 '(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.CORETRANSATTRERRTBL a where 1=1 
 
 and ATTRID=P_ATTRID
 and TRANSID=P_TRANSID) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'Oracle returned error fetching STGCSA.CORETRANSATTRERRTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'No STGCSA.CORETRANSATTRERRTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_TRANSID:=old_rec.TRANSID; log_TIMESTAMPFLD:=old_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CORETRANSATTRERRDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'CORETRANSATTRERRDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CORETRANSATTRERRDELTSP build 2018-01-23 08:20:12'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CORETRANSATTRERRDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CORETRANSATTRERRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


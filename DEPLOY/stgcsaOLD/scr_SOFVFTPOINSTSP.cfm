<!--- Saved 01/28/2018 14:42:38. --->
PROCEDURE SOFVFTPOINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
 ,p_PREPOSTRVWTRANSCD CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTCHKDGT CHAR:=null
 ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
 ,p_PREPOSTRVWPYMTAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
 ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
 ,p_PREPOSTRVWADDENDRECIND NUMBER:=null
 ,p_PREPOSTRVWTRACENMB NUMBER:=null
 ,p_PREPOSTRVWCMNT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:40
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:32:40
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVFTPOTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVFTPOTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVFTPOINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVFTPOTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PREPOSTRVWRECRDTYPCD';
 new_rec.PREPOSTRVWRECRDTYPCD:=runtime.validate_column_value(P_PREPOSTRVWRECRDTYPCD,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWRECRDTYPCD','CHAR',1);
 cur_col_name:='P_PREPOSTRVWTRANSCD';
 new_rec.PREPOSTRVWTRANSCD:=runtime.validate_column_value(P_PREPOSTRVWTRANSCD,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWTRANSCD','CHAR',15);
 cur_col_name:='P_PREPOSTRVWTRANSITROUTINGNMB';
 new_rec.PREPOSTRVWTRANSITROUTINGNMB:=runtime.validate_column_value(P_PREPOSTRVWTRANSITROUTINGNMB,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWTRANSITROUTINGNMB','CHAR',15);
 cur_col_name:='P_PREPOSTRVWTRANSITROUTCHKDGT';
 new_rec.PREPOSTRVWTRANSITROUTINGCHKDGT:=runtime.validate_column_value(P_PREPOSTRVWTRANSITROUTCHKDGT,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWTRANSITROUTINGCHKDGT','CHAR',1);
 cur_col_name:='P_PREPOSTRVWBNKACCTNMB';
 new_rec.PREPOSTRVWBNKACCTNMB:=runtime.validate_column_value(P_PREPOSTRVWBNKACCTNMB,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWBNKACCTNMB','VARCHAR2',20);
 cur_col_name:='P_PREPOSTRVWPYMTAMT';
 new_rec.PREPOSTRVWPYMTAMT:=runtime.validate_column_value(P_PREPOSTRVWPYMTAMT,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWPYMTAMT','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','SOFVFTPOTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_PREPOSTRVWINDVLNM';
 new_rec.PREPOSTRVWINDVLNM:=runtime.validate_column_value(P_PREPOSTRVWINDVLNM,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWINDVLNM','VARCHAR2',80);
 cur_col_name:='P_PREPOSTRVWCOMPBNKDISC';
 new_rec.PREPOSTRVWCOMPBNKDISC:=runtime.validate_column_value(P_PREPOSTRVWCOMPBNKDISC,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWCOMPBNKDISC','CHAR',2);
 cur_col_name:='P_PREPOSTRVWADDENDRECIND';
 new_rec.PREPOSTRVWADDENDRECIND:=runtime.validate_column_value(P_PREPOSTRVWADDENDRECIND,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWADDENDRECIND','NUMBER',22);
 cur_col_name:='P_PREPOSTRVWTRACENMB';
 new_rec.PREPOSTRVWTRACENMB:=runtime.validate_column_value(P_PREPOSTRVWTRACENMB,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWTRACENMB','NUMBER',22);
 cur_col_name:='P_PREPOSTRVWCMNT';
 new_rec.PREPOSTRVWCMNT:=runtime.validate_column_value(P_PREPOSTRVWCMNT,
 'STGCSA','SOFVFTPOTBL','PREPOSTRVWCMNT','VARCHAR2',80);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVFTPOINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVFTPOAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVFTPOTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVFTPOINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVFTPOTBL for key '
 ||'(LOANNMB)=('||new_rec.LOANNMB||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVFTPOINSTSP build 2018-01-23 08:32:40';
 ROLLBACK TO SOFVFTPOINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVFTPOINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVFTPOINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVFTPOINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVFTPOINSTSP build 2018-01-23 08:32:40'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVFTPOINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVFTPOINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:34. --->
PROCEDURE SOFVCTCHINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CATCHUPPLANSBANMB CHAR:=null
 ,p_CATCHUPPLANSTRTDT1 DATE:=null
 ,p_CATCHUPPLANENDDT1 DATE:=null
 ,p_CATCHUPPLANRQ1AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT2 DATE:=null
 ,p_CATCHUPPLANENDDT2 DATE:=null
 ,p_CATCHUPPLANRQ2AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT3 DATE:=null
 ,p_CATCHUPPLANENDDT3 DATE:=null
 ,p_CATCHUPPLANRQ3AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT4 DATE:=null
 ,p_CATCHUPPLANENDDT4 DATE:=null
 ,p_CATCHUPPLANRQ4AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT5 DATE:=null
 ,p_CATCHUPPLANENDDT5 DATE:=null
 ,p_CATCHUPPLANRQ5AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT6 DATE:=null
 ,p_CATCHUPPLANENDDT6 DATE:=null
 ,p_CATCHUPPLANRQ6AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT7 DATE:=null
 ,p_CATCHUPPLANENDDT7 DATE:=null
 ,p_CATCHUPPLANRQ7AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT8 DATE:=null
 ,p_CATCHUPPLANENDDT8 DATE:=null
 ,p_CATCHUPPLANRQ8AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT9 DATE:=null
 ,p_CATCHUPPLANENDDT9 DATE:=null
 ,p_CATCHUPPLANRQ9AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT10 DATE:=null
 ,p_CATCHUPPLANENDDT10 DATE:=null
 ,p_CATCHUPPLANRQ10AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT11 DATE:=null
 ,p_CATCHUPPLANENDDT11 DATE:=null
 ,p_CATCHUPPLANRQ11AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT12 DATE:=null
 ,p_CATCHUPPLANENDDT12 DATE:=null
 ,p_CATCHUPPLANRQ12AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT13 DATE:=null
 ,p_CATCHUPPLANENDDT13 DATE:=null
 ,p_CATCHUPPLANRQ13AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT14 DATE:=null
 ,p_CATCHUPPLANENDDT14 DATE:=null
 ,p_CATCHUPPLANRQ14AMT NUMBER:=null
 ,p_CATCHUPPLANSTRTDT15 DATE:=null
 ,p_CATCHUPPLANENDDT15 DATE:=null
 ,p_CATCHUPPLANRQ15AMT NUMBER:=null
 ,p_CATCHUPPLANCREDT DATE:=null
 ,p_CATCHUPPLANMODDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:31:39
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:31:39
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVCTCHTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVCTCHTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVCTCHINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCTCHTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CATCHUPPLANSBANMB';
 new_rec.CATCHUPPLANSBANMB:=runtime.validate_column_value(P_CATCHUPPLANSBANMB,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSBANMB','CHAR',10);
 cur_col_name:='P_CATCHUPPLANSTRTDT1';
 new_rec.CATCHUPPLANSTRTDT1:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT1,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT1','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT1';
 new_rec.CATCHUPPLANENDDT1:=runtime.validate_column_value(P_CATCHUPPLANENDDT1,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT1','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ1AMT';
 new_rec.CATCHUPPLANRQ1AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ1AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ1AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT2';
 new_rec.CATCHUPPLANSTRTDT2:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT2,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT2','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT2';
 new_rec.CATCHUPPLANENDDT2:=runtime.validate_column_value(P_CATCHUPPLANENDDT2,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT2','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ2AMT';
 new_rec.CATCHUPPLANRQ2AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ2AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ2AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT3';
 new_rec.CATCHUPPLANSTRTDT3:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT3,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT3','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT3';
 new_rec.CATCHUPPLANENDDT3:=runtime.validate_column_value(P_CATCHUPPLANENDDT3,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT3','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ3AMT';
 new_rec.CATCHUPPLANRQ3AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ3AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ3AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT4';
 new_rec.CATCHUPPLANSTRTDT4:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT4,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT4','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT4';
 new_rec.CATCHUPPLANENDDT4:=runtime.validate_column_value(P_CATCHUPPLANENDDT4,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT4','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ4AMT';
 new_rec.CATCHUPPLANRQ4AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ4AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ4AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT5';
 new_rec.CATCHUPPLANSTRTDT5:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT5,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT5','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT5';
 new_rec.CATCHUPPLANENDDT5:=runtime.validate_column_value(P_CATCHUPPLANENDDT5,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT5','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ5AMT';
 new_rec.CATCHUPPLANRQ5AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ5AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ5AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT6';
 new_rec.CATCHUPPLANSTRTDT6:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT6,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT6','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT6';
 new_rec.CATCHUPPLANENDDT6:=runtime.validate_column_value(P_CATCHUPPLANENDDT6,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT6','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ6AMT';
 new_rec.CATCHUPPLANRQ6AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ6AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ6AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT7';
 new_rec.CATCHUPPLANSTRTDT7:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT7,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT7','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT7';
 new_rec.CATCHUPPLANENDDT7:=runtime.validate_column_value(P_CATCHUPPLANENDDT7,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT7','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ7AMT';
 new_rec.CATCHUPPLANRQ7AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ7AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ7AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT8';
 new_rec.CATCHUPPLANSTRTDT8:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT8,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT8','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT8';
 new_rec.CATCHUPPLANENDDT8:=runtime.validate_column_value(P_CATCHUPPLANENDDT8,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT8','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ8AMT';
 new_rec.CATCHUPPLANRQ8AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ8AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ8AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT9';
 new_rec.CATCHUPPLANSTRTDT9:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT9,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT9','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT9';
 new_rec.CATCHUPPLANENDDT9:=runtime.validate_column_value(P_CATCHUPPLANENDDT9,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT9','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ9AMT';
 new_rec.CATCHUPPLANRQ9AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ9AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ9AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT10';
 new_rec.CATCHUPPLANSTRTDT10:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT10,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT10','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT10';
 new_rec.CATCHUPPLANENDDT10:=runtime.validate_column_value(P_CATCHUPPLANENDDT10,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT10','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ10AMT';
 new_rec.CATCHUPPLANRQ10AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ10AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ10AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT11';
 new_rec.CATCHUPPLANSTRTDT11:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT11,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT11','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT11';
 new_rec.CATCHUPPLANENDDT11:=runtime.validate_column_value(P_CATCHUPPLANENDDT11,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT11','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ11AMT';
 new_rec.CATCHUPPLANRQ11AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ11AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ11AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT12';
 new_rec.CATCHUPPLANSTRTDT12:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT12,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT12','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT12';
 new_rec.CATCHUPPLANENDDT12:=runtime.validate_column_value(P_CATCHUPPLANENDDT12,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT12','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ12AMT';
 new_rec.CATCHUPPLANRQ12AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ12AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ12AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT13';
 new_rec.CATCHUPPLANSTRTDT13:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT13,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT13','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT13';
 new_rec.CATCHUPPLANENDDT13:=runtime.validate_column_value(P_CATCHUPPLANENDDT13,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT13','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ13AMT';
 new_rec.CATCHUPPLANRQ13AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ13AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ13AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT14';
 new_rec.CATCHUPPLANSTRTDT14:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT14,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT14','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT14';
 new_rec.CATCHUPPLANENDDT14:=runtime.validate_column_value(P_CATCHUPPLANENDDT14,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT14','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ14AMT';
 new_rec.CATCHUPPLANRQ14AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ14AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ14AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANSTRTDT15';
 new_rec.CATCHUPPLANSTRTDT15:=runtime.validate_column_value(P_CATCHUPPLANSTRTDT15,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANSTRTDT15','DATE',7);
 cur_col_name:='P_CATCHUPPLANENDDT15';
 new_rec.CATCHUPPLANENDDT15:=runtime.validate_column_value(P_CATCHUPPLANENDDT15,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANENDDT15','DATE',7);
 cur_col_name:='P_CATCHUPPLANRQ15AMT';
 new_rec.CATCHUPPLANRQ15AMT:=runtime.validate_column_value(P_CATCHUPPLANRQ15AMT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANRQ15AMT','NUMBER',22);
 cur_col_name:='P_CATCHUPPLANCREDT';
 new_rec.CATCHUPPLANCREDT:=runtime.validate_column_value(P_CATCHUPPLANCREDT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANCREDT','DATE',7);
 cur_col_name:='P_CATCHUPPLANMODDT';
 new_rec.CATCHUPPLANMODDT:=runtime.validate_column_value(P_CATCHUPPLANMODDT,
 'STGCSA','SOFVCTCHTBL','CATCHUPPLANMODDT','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVCTCHINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVCTCHAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVCTCHTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVCTCHINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVCTCHTBL for key '
 ||'(CATCHUPPLANSBANMB)=('||new_rec.CATCHUPPLANSBANMB||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVCTCHINSTSP build 2018-01-23 08:31:39';
 ROLLBACK TO SOFVCTCHINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVCTCHINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVCTCHINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVCTCHINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 WHEN OTHERS THEN
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVCTCHINSTSP build 2018-01-23 08:31:39'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVCTCHINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVCTCHINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


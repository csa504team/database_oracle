<!--- Saved 01/28/2018 14:42:22. --->
PROCEDURE PRPRVWSTATINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPRVWSTATID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ,p_INITIALPOSTRVW NUMBER:=null
 ,p_INITIALPOSTRVWBY VARCHAR2:=null
 ,p_SCNDPOSTRVW NUMBER:=null
 ,p_SCNDPOSTRVWBY VARCHAR2:=null
 ,p_THIRDPOSTRVW NUMBER:=null
 ,p_THIRDPOSTRVWBY VARCHAR2:=null
 ,p_FOURTHPOSTRVW NUMBER:=null
 ,p_FOURTHPOSTRVWBY VARCHAR2:=null
 ,p_FIFTHPOSTRVW NUMBER:=null
 ,p_FIFTHPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHPOSTRVW NUMBER:=null
 ,p_SIXTHPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHMOPOSTRVW NUMBER:=null
 ,p_SIXTHMOPOSTRVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST2RVW NUMBER:=null
 ,p_SIXTHMOPOST2RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST3RVW NUMBER:=null
 ,p_SIXTHMOPOST3RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST4RVW NUMBER:=null
 ,p_SIXTHMOPOST4RVWBY VARCHAR2:=null
 ,p_SIXTHMOPOST5RVW NUMBER:=null
 ,p_SIXTHMOPOST5RVWBY VARCHAR2:=null
 ,p_THISTRVW NUMBER:=null
 ,p_THISTRVWBY VARCHAR2:=null
 ,p_FEEVAL NUMBER:=null
 ,p_FEEVALBY VARCHAR2:=null
 ,p_RVWSBMT NUMBER:=null
 ,p_RVWSBMTBY VARCHAR2:=null
 ,p_RVWSBMTDT DATE:=null
 ,p_WORKUPVAL NUMBER:=null
 ,p_WORKUPVALBY VARCHAR2:=null
 ,p_THISTVAL NUMBER:=null
 ,p_THISTVALBY VARCHAR2:=null
 ,p_VALSBMT NUMBER:=null
 ,p_VALSBMTBY VARCHAR2:=null
 ,p_VALSBMTDT DATE:=null
 ,p_PROOFVAL NUMBER:=null
 ,p_PROOFVALBY VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:29:19
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:29:19
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPRVWSTATTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PRPRVWSTATTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PRPRVWSTATINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PRPRVWSTATID';
 new_rec.PRPRVWSTATID:=runtime.validate_column_value(P_PRPRVWSTATID,
 'STGCSA','PRPRVWSTATTBL','PRPRVWSTATID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PRPRVWSTATTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','PRPRVWSTATTBL','TRANSID','NUMBER',22);
 cur_col_name:='P_TRANSTYPID';
 new_rec.TRANSTYPID:=runtime.validate_column_value(P_TRANSTYPID,
 'STGCSA','PRPRVWSTATTBL','TRANSTYPID','NUMBER',22);
 cur_col_name:='P_INITIALPOSTRVW';
 new_rec.INITIALPOSTRVW:=runtime.validate_column_value(P_INITIALPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','INITIALPOSTRVW','NUMBER',22);
 cur_col_name:='P_INITIALPOSTRVWBY';
 new_rec.INITIALPOSTRVWBY:=runtime.validate_column_value(P_INITIALPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','INITIALPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_SCNDPOSTRVW';
 new_rec.SCNDPOSTRVW:=runtime.validate_column_value(P_SCNDPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','SCNDPOSTRVW','NUMBER',22);
 cur_col_name:='P_SCNDPOSTRVWBY';
 new_rec.SCNDPOSTRVWBY:=runtime.validate_column_value(P_SCNDPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','SCNDPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_THIRDPOSTRVW';
 new_rec.THIRDPOSTRVW:=runtime.validate_column_value(P_THIRDPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','THIRDPOSTRVW','NUMBER',22);
 cur_col_name:='P_THIRDPOSTRVWBY';
 new_rec.THIRDPOSTRVWBY:=runtime.validate_column_value(P_THIRDPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','THIRDPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_FOURTHPOSTRVW';
 new_rec.FOURTHPOSTRVW:=runtime.validate_column_value(P_FOURTHPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','FOURTHPOSTRVW','NUMBER',22);
 cur_col_name:='P_FOURTHPOSTRVWBY';
 new_rec.FOURTHPOSTRVWBY:=runtime.validate_column_value(P_FOURTHPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','FOURTHPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_FIFTHPOSTRVW';
 new_rec.FIFTHPOSTRVW:=runtime.validate_column_value(P_FIFTHPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','FIFTHPOSTRVW','NUMBER',22);
 cur_col_name:='P_FIFTHPOSTRVWBY';
 new_rec.FIFTHPOSTRVWBY:=runtime.validate_column_value(P_FIFTHPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','FIFTHPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHPOSTRVW';
 new_rec.SIXTHPOSTRVW:=runtime.validate_column_value(P_SIXTHPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHPOSTRVW','NUMBER',22);
 cur_col_name:='P_SIXTHPOSTRVWBY';
 new_rec.SIXTHPOSTRVWBY:=runtime.validate_column_value(P_SIXTHPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHMOPOSTRVW';
 new_rec.SIXTHMOPOSTRVW:=runtime.validate_column_value(P_SIXTHMOPOSTRVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOSTRVW','NUMBER',22);
 cur_col_name:='P_SIXTHMOPOSTRVWBY';
 new_rec.SIXTHMOPOSTRVWBY:=runtime.validate_column_value(P_SIXTHMOPOSTRVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOSTRVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHMOPOST2RVW';
 new_rec.SIXTHMOPOST2RVW:=runtime.validate_column_value(P_SIXTHMOPOST2RVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST2RVW','NUMBER',22);
 cur_col_name:='P_SIXTHMOPOST2RVWBY';
 new_rec.SIXTHMOPOST2RVWBY:=runtime.validate_column_value(P_SIXTHMOPOST2RVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST2RVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHMOPOST3RVW';
 new_rec.SIXTHMOPOST3RVW:=runtime.validate_column_value(P_SIXTHMOPOST3RVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST3RVW','NUMBER',22);
 cur_col_name:='P_SIXTHMOPOST3RVWBY';
 new_rec.SIXTHMOPOST3RVWBY:=runtime.validate_column_value(P_SIXTHMOPOST3RVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST3RVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHMOPOST4RVW';
 new_rec.SIXTHMOPOST4RVW:=runtime.validate_column_value(P_SIXTHMOPOST4RVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST4RVW','NUMBER',22);
 cur_col_name:='P_SIXTHMOPOST4RVWBY';
 new_rec.SIXTHMOPOST4RVWBY:=runtime.validate_column_value(P_SIXTHMOPOST4RVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST4RVWBY','VARCHAR2',30);
 cur_col_name:='P_SIXTHMOPOST5RVW';
 new_rec.SIXTHMOPOST5RVW:=runtime.validate_column_value(P_SIXTHMOPOST5RVW,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST5RVW','NUMBER',22);
 cur_col_name:='P_SIXTHMOPOST5RVWBY';
 new_rec.SIXTHMOPOST5RVWBY:=runtime.validate_column_value(P_SIXTHMOPOST5RVWBY,
 'STGCSA','PRPRVWSTATTBL','SIXTHMOPOST5RVWBY','VARCHAR2',30);
 cur_col_name:='P_THISTRVW';
 new_rec.THISTRVW:=runtime.validate_column_value(P_THISTRVW,
 'STGCSA','PRPRVWSTATTBL','THISTRVW','NUMBER',22);
 cur_col_name:='P_THISTRVWBY';
 new_rec.THISTRVWBY:=runtime.validate_column_value(P_THISTRVWBY,
 'STGCSA','PRPRVWSTATTBL','THISTRVWBY','VARCHAR2',30);
 cur_col_name:='P_FEEVAL';
 new_rec.FEEVAL:=runtime.validate_column_value(P_FEEVAL,
 'STGCSA','PRPRVWSTATTBL','FEEVAL','NUMBER',22);
 cur_col_name:='P_FEEVALBY';
 new_rec.FEEVALBY:=runtime.validate_column_value(P_FEEVALBY,
 'STGCSA','PRPRVWSTATTBL','FEEVALBY','VARCHAR2',30);
 cur_col_name:='P_RVWSBMT';
 new_rec.RVWSBMT:=runtime.validate_column_value(P_RVWSBMT,
 'STGCSA','PRPRVWSTATTBL','RVWSBMT','NUMBER',22);
 cur_col_name:='P_RVWSBMTBY';
 new_rec.RVWSBMTBY:=runtime.validate_column_value(P_RVWSBMTBY,
 'STGCSA','PRPRVWSTATTBL','RVWSBMTBY','VARCHAR2',30);
 cur_col_name:='P_RVWSBMTDT';
 new_rec.RVWSBMTDT:=runtime.validate_column_value(P_RVWSBMTDT,
 'STGCSA','PRPRVWSTATTBL','RVWSBMTDT','DATE',7);
 cur_col_name:='P_WORKUPVAL';
 new_rec.WORKUPVAL:=runtime.validate_column_value(P_WORKUPVAL,
 'STGCSA','PRPRVWSTATTBL','WORKUPVAL','NUMBER',22);
 cur_col_name:='P_WORKUPVALBY';
 new_rec.WORKUPVALBY:=runtime.validate_column_value(P_WORKUPVALBY,
 'STGCSA','PRPRVWSTATTBL','WORKUPVALBY','VARCHAR2',30);
 cur_col_name:='P_THISTVAL';
 new_rec.THISTVAL:=runtime.validate_column_value(P_THISTVAL,
 'STGCSA','PRPRVWSTATTBL','THISTVAL','NUMBER',22);
 cur_col_name:='P_THISTVALBY';
 new_rec.THISTVALBY:=runtime.validate_column_value(P_THISTVALBY,
 'STGCSA','PRPRVWSTATTBL','THISTVALBY','VARCHAR2',30);
 cur_col_name:='P_VALSBMT';
 new_rec.VALSBMT:=runtime.validate_column_value(P_VALSBMT,
 'STGCSA','PRPRVWSTATTBL','VALSBMT','NUMBER',22);
 cur_col_name:='P_VALSBMTBY';
 new_rec.VALSBMTBY:=runtime.validate_column_value(P_VALSBMTBY,
 'STGCSA','PRPRVWSTATTBL','VALSBMTBY','VARCHAR2',30);
 cur_col_name:='P_VALSBMTDT';
 new_rec.VALSBMTDT:=runtime.validate_column_value(P_VALSBMTDT,
 'STGCSA','PRPRVWSTATTBL','VALSBMTDT','DATE',7);
 cur_col_name:='P_PROOFVAL';
 new_rec.PROOFVAL:=runtime.validate_column_value(P_PROOFVAL,
 'STGCSA','PRPRVWSTATTBL','PROOFVAL','NUMBER',22);
 cur_col_name:='P_PROOFVALBY';
 new_rec.PROOFVALBY:=runtime.validate_column_value(P_PROOFVALBY,
 'STGCSA','PRPRVWSTATTBL','PROOFVALBY','VARCHAR2',30);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PRPRVWSTATTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PRPRVWSTATINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PRPRVWSTATAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PRPRVWSTATTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PRPRVWSTATINSTSP;
 p_errmsg:='Got dupe key on insert into PRPRVWSTATTBL for key '
 ||'(PRPRVWSTATID)=('||new_rec.PRPRVWSTATID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPRVWSTATINSTSP build 2018-01-23 08:29:19';
 ROLLBACK TO PRPRVWSTATINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPRVWSTATINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPRVWSTATINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPRVWSTATINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PRPRVWSTATINSTSP build 2018-01-23 08:29:19'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPRVWSTATINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PRPRVWSTATINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


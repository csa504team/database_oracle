<!--- Saved 01/28/2018 14:42:51. --->
PROCEDURE SOFVRTSCINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRGMRTBASIS CHAR:=null
 ,p_PRGMRTFEEDESC CHAR:=null
 ,p_PRGMRTENDDT DATE:=null
 ,p_PRGMRTSTARTDT DATE:=null
 ,p_PRGMRTPCT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:36:01
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:36:01
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVRTSCTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.SOFVRTSCTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 begin
 -- setup
 savepoint SOFVRTSCINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVRTSCTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PRGMRTBASIS';
 new_rec.PRGMRTBASIS:=runtime.validate_column_value(P_PRGMRTBASIS,
 'STGCSA','SOFVRTSCTBL','PRGMRTBASIS','CHAR',2);
 cur_col_name:='P_PRGMRTFEEDESC';
 new_rec.PRGMRTFEEDESC:=runtime.validate_column_value(P_PRGMRTFEEDESC,
 'STGCSA','SOFVRTSCTBL','PRGMRTFEEDESC','CHAR',10);
 cur_col_name:='P_PRGMRTENDDT';
 new_rec.PRGMRTENDDT:=runtime.validate_column_value(P_PRGMRTENDDT,
 'STGCSA','SOFVRTSCTBL','PRGMRTENDDT','DATE',7);
 cur_col_name:='P_PRGMRTSTARTDT';
 new_rec.PRGMRTSTARTDT:=runtime.validate_column_value(P_PRGMRTSTARTDT,
 'STGCSA','SOFVRTSCTBL','PRGMRTSTARTDT','DATE',7);
 cur_col_name:='P_PRGMRTPCT';
 new_rec.PRGMRTPCT:=runtime.validate_column_value(P_PRGMRTPCT,
 'STGCSA','SOFVRTSCTBL','PRGMRTPCT','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO SOFVRTSCINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.SOFVRTSCAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVRTSCTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO SOFVRTSCINSTSP;
 p_errmsg:='Got dupe key on insert into SOFVRTSCTBL for key '
 ||'(PRGMRTFEEDESC)=('||new_rec.PRGMRTFEEDESC||') ' ||'(PRGMRTENDDT)=('||new_rec.PRGMRTENDDT||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVRTSCINSTSP build 2018-01-23 08:36:00';
 ROLLBACK TO SOFVRTSCINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVRTSCINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVRTSCINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVRTSCINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in SOFVRTSCINSTSP build 2018-01-23 08:36:00'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVRTSCINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END SOFVRTSCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


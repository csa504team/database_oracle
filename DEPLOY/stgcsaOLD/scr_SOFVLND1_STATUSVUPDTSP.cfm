<!--- Saved 01/28/2018 14:42:45. --->
PROCEDURE SOFVLND1_STATUSVUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANDTL503SUBTYP CHAR:=null
 ,p_LOANDTLACCTCD NUMBER:=null
 ,p_LOANDTLACCTCHNGCD CHAR:=null
 ,p_LOANDTLACHBNKACCT VARCHAR2:=null
 ,p_LOANDTLACHBNKACTYP CHAR:=null
 ,p_LOANDTLACHBNKBR CHAR:=null
 ,p_LOANDTLACHBNKIDNOORATTEN VARCHAR2:=null
 ,p_LOANDTLACHBNKMAILADRCTYNM VARCHAR2:=null
 ,p_LOANDTLACHBNKMAILADRSTCD CHAR:=null
 ,p_LOANDTLACHBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_LOANDTLACHBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_LOANDTLACHBNKMAILADRZIP4CD CHAR:=null
 ,p_LOANDTLACHBNKMAILADRZIPCD CHAR:=null
 ,p_LOANDTLACHBNKNM VARCHAR2:=null
 ,p_LOANDTLACHBNKROUTNMB CHAR:=null
 ,p_LOANDTLACHBNKTRANS CHAR:=null
 ,p_LOANDTLACHDEPNM VARCHAR2:=null
 ,p_LOANDTLACHLASTCHNGDT DATE:=null
 ,p_LOANDTLACHPRENTIND CHAR:=null
 ,p_LOANDTLACHSIGNDT DATE:=null
 ,p_LOANDTLACTUALCSAINITFEEAMT NUMBER:=null
 ,p_LOANDTLAMINTAMT NUMBER:=null
 ,p_LOANDTLAMPRINAMT NUMBER:=null
 ,p_LOANDTLAMPRINLEFTAMT NUMBER:=null
 ,p_LOANDTLAMSCHDLTYP CHAR:=null
 ,p_LOANDTLAMTHRUDTX CHAR:=null
 ,p_LOANDTLAPPIND CHAR:=null
 ,p_LOANDTLAPPVDT DATE:=null
 ,p_LOANDTLATTORNEYFEEAMT NUMBER:=null
 ,p_LOANDTLBORRMAILADRCTYNM VARCHAR2:=null
 ,p_LOANDTLBORRMAILADRSTCD CHAR:=null
 ,p_LOANDTLBORRMAILADRSTR1NM VARCHAR2:=null
 ,p_LOANDTLBORRMAILADRSTR2NM VARCHAR2:=null
 ,p_LOANDTLBORRMAILADRZIP4CD CHAR:=null
 ,p_LOANDTLBORRMAILADRZIPCD CHAR:=null
 ,p_LOANDTLBORRNM VARCHAR2:=null
 ,p_LOANDTLBORRNM2 VARCHAR2:=null
 ,p_LOANDTLCACCTNM VARCHAR2:=null
 ,p_LOANDTLCACCTNMB VARCHAR2:=null
 ,p_LOANDTLCATTEN VARCHAR2:=null
 ,p_LOANDTLCBNKMAILADRCTYNM VARCHAR2:=null
 ,p_LOANDTLCBNKMAILADRSTCD CHAR:=null
 ,p_LOANDTLCBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_LOANDTLCBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_LOANDTLCBNKMAILADRZIP4CD CHAR:=null
 ,p_LOANDTLCBNKMAILADRZIPCD CHAR:=null
 ,p_LOANDTLCBNKNM VARCHAR2:=null
 ,p_LOANDTLCDC1PCT NUMBER:=null
 ,p_LOANDTLCDCCERT CHAR:=null
 ,p_LOANDTLCDCDOLLR1AMT NUMBER:=null
 ,p_LOANDTLCDCFEEMTDAMT NUMBER:=null
 ,p_LOANDTLCDCFEEPCT NUMBER:=null
 ,p_LOANDTLCDCFEEPTDAMT NUMBER:=null
 ,p_LOANDTLCDCFEEYTDAMT NUMBER:=null
 ,p_LOANDTLCDCPAIDTHRUDTX CHAR:=null
 ,p_LOANDTLCDCPFEEDOLLRAMT NUMBER:=null
 ,p_LOANDTLCDCPFEEPCT NUMBER:=null
 ,p_LOANDTLCDCREGNCD CHAR:=null
 ,p_LOANDTLCHEMICALBASISDAYS NUMBER:=null
 ,p_LOANDTLCMNT1 VARCHAR2:=null
 ,p_LOANDTLCMNT2 VARCHAR2:=null
 ,p_LOANDTLCOLOANDTLACCT NUMBER:=null
 ,p_LOANDTLCONVDT DATE:=null
 ,p_LOANDTLCREATDT DATE:=null
 ,p_LOANDTLCROUTSYM CHAR:=null
 ,p_LOANDTLCSA1PCT NUMBER:=null
 ,p_LOANDTLCSADOLLR1AMT NUMBER:=null
 ,p_LOANDTLCSAFEEMTDAMT NUMBER:=null
 ,p_LOANDTLCSAFEEPCT NUMBER:=null
 ,p_LOANDTLCSAFEEPTDAMT NUMBER:=null
 ,p_LOANDTLCSAFEEYTDAMT NUMBER:=null
 ,p_LOANDTLCSAPAIDTHRUDTX CHAR:=null
 ,p_LOANDTLCTRANSCD CHAR:=null
 ,p_LOANDTLCURCDCFEEAMT NUMBER:=null
 ,p_LOANDTLCURCSAFEEAMT NUMBER:=null
 ,p_LOANDTLCURFEEBALAMT NUMBER:=null
 ,p_LOANDTLCURRESRVAMT NUMBER:=null
 ,p_LOANDTLDBENTRCURBALAMT NUMBER:=null
 ,p_LOANDTLDBENTRINTRTPCT NUMBER:=null
 ,p_LOANDTLDBENTRMATDT DATE:=null
 ,p_LOANDTLDBENTRPRINAMT NUMBER:=null
 ,p_LOANDTLDBENTRPROCDSAMT NUMBER:=null
 ,p_LOANDTLDBENTRPROCDSESCROWAMT NUMBER:=null
 ,p_LOANDTLDBENTRPYMTDTTRM CHAR:=null
 ,p_LOANDTLDEFPYMT CHAR:=null
 ,p_LOANDTLDEFPYMTDTX CHAR:=null
 ,p_LOANDTLDUEBORRAMT NUMBER:=null
 ,p_LOANDTLEXCESSDUEBORRAMT NUMBER:=null
 ,p_LOANDTLFEEBASEAMT NUMBER:=null
 ,p_LOANDTLFUNDFEEDOLLRAMT NUMBER:=null
 ,p_LOANDTLFUNDFEEPCT NUMBER:=null
 ,p_LOANDTLGNTYREPAYPTDAMT NUMBER:=null
 ,p_LOANDTLINITFEEDOLLRAMT NUMBER:=null
 ,p_LOANDTLINITFEEPCT NUMBER:=null
 ,p_LOANDTLINT1AMT NUMBER:=null
 ,p_LOANDTLINTLENDRIND CHAR:=null
 ,p_LOANDTLINTPAIDTHRUDTX CHAR:=null
 ,p_LOANDTLINTRMLENDR NUMBER:=null
 ,p_LOANDTLISSDT DATE:=null
 ,p_LOANDTLLASTDBPAYOUTAMT NUMBER:=null
 ,p_LOANDTLLATEIND CHAR:=null
 ,p_LOANDTLLENDRSBAFEEAMT NUMBER:=null
 ,p_LOANDTLLMAINTNDT DATE:=null
 ,p_LOANDTLLOANTYP CHAR:=null
 ,p_LOANDTLLPYMTDT DATE:=null
 ,p_LOANDTLLTFEEIND CHAR:=null
 ,p_LOANDTLMLACCTNMB VARCHAR2:=null
 ,p_LOANDTLNOTECURBALAMT NUMBER:=null
 ,p_LOANDTLNOTEDT DATE:=null
 ,p_LOANDTLNOTEINTRTPCT NUMBER:=null
 ,p_LOANDTLNOTEMATDT DATE:=null
 ,p_LOANDTLNOTEMOPYMTAMT NUMBER:=null
 ,p_LOANDTLNOTEPRINAMT NUMBER:=null
 ,p_LOANDTLNOTEPYMTDTTRM CHAR:=null
 ,p_LOANDTLPOOLSERS CHAR:=null
 ,p_LOANDTLPRENOTETESTDT DATE:=null
 ,p_LOANDTLPRGRM CHAR:=null
 ,p_LOANDTLPRGRMTYP CHAR:=null
 ,p_LOANDTLPRIN1AMT NUMBER:=null
 ,p_LOANDTLPRMAMT NUMBER:=null
 ,p_LOANDTLPYMT1AMT NUMBER:=null
 ,p_LOANDTLPYMTDT1X CHAR:=null
 ,p_LOANDTLPYMTNMBDUE NUMBER:=null
 ,p_LOANDTLPYMTRECV NUMBER:=null
 ,p_LOANDTLRACCTNM VARCHAR2:=null
 ,p_LOANDTLRACCTNMB VARCHAR2:=null
 ,p_LOANDTLRATTEN VARCHAR2:=null
 ,p_LOANDTLRBNKMAILADRCTYNM VARCHAR2:=null
 ,p_LOANDTLRBNKMAILADRSTCD CHAR:=null
 ,p_LOANDTLRBNKMAILADRSTR1NM VARCHAR2:=null
 ,p_LOANDTLRBNKMAILADRSTR2NM VARCHAR2:=null
 ,p_LOANDTLRBNKMAILADRZIP4CD CHAR:=null
 ,p_LOANDTLRBNKMAILADRZIPCD CHAR:=null
 ,p_LOANDTLRBNKNM VARCHAR2:=null
 ,p_LOANDTLREAMIND CHAR:=null
 ,p_LOANDTLREFIIND CHAR:=null
 ,p_LOANDTLRESRVAMT NUMBER:=null
 ,p_LOANDTLRESRVDEPAMT NUMBER:=null
 ,p_LOANDTLRESRVDEPPCT NUMBER:=null
 ,p_LOANDTLROLOANDTLACCT NUMBER:=null
 ,p_LOANDTLRROUTSYM CHAR:=null
 ,p_LOANDTLSBAFEEAMT NUMBER:=null
 ,p_LOANDTLSBAFEEPCT NUMBER:=null
 ,p_LOANDTLSBAOFC CHAR:=null
 ,p_LOANDTLSBAPAIDTHRUDTX CHAR:=null
 ,p_LOANDTLSBCIDNMB CHAR:=null
 ,p_LOANDTLSBCMAILADRCTYNM VARCHAR2:=null
 ,p_LOANDTLSBCMAILADRSTCD CHAR:=null
 ,p_LOANDTLSBCMAILADRSTR1NM VARCHAR2:=null
 ,p_LOANDTLSBCMAILADRSTR2NM VARCHAR2:=null
 ,p_LOANDTLSBCMAILADRZIP4CD CHAR:=null
 ,p_LOANDTLSBCMAILADRZIPCD CHAR:=null
 ,p_LOANDTLSBCNM VARCHAR2:=null
 ,p_LOANDTLSBCNM2 VARCHAR2:=null
 ,p_LOANDTLSEMIANNPYMTAMT NUMBER:=null
 ,p_LOANDTLSETUPIND CHAR:=null
 ,p_LOANDTLSOD CHAR:=null
 ,p_LOANDTLSPPAUTOPCT NUMBER:=null
 ,p_LOANDTLSPPAUTOPYMTAMT NUMBER:=null
 ,p_LOANDTLSPPIND CHAR:=null
 ,p_LOANDTLSPPLQDAMT NUMBER:=null
 ,p_LOANDTLSTATCDOFLOAN CHAR:=null
 ,p_LOANDTLSTATDT DATE:=null
 ,p_LOANDTLSTMTAVGMOBALAMT NUMBER:=null
 ,p_LOANDTLSTMTCLSBALAMT NUMBER:=null
 ,p_LOANDTLSTMTCLSDT DATE:=null
 ,p_LOANDTLSTMTINT10AMT NUMBER:=null
 ,p_LOANDTLSTMTINT11AMT NUMBER:=null
 ,p_LOANDTLSTMTINT12AMT NUMBER:=null
 ,p_LOANDTLSTMTINT13AMT NUMBER:=null
 ,p_LOANDTLSTMTINT1AMT NUMBER:=null
 ,p_LOANDTLSTMTINT2AMT NUMBER:=null
 ,p_LOANDTLSTMTINT3AMT NUMBER:=null
 ,p_LOANDTLSTMTINT4AMT NUMBER:=null
 ,p_LOANDTLSTMTINT5AMT NUMBER:=null
 ,p_LOANDTLSTMTINT6AMT NUMBER:=null
 ,p_LOANDTLSTMTINT7AMT NUMBER:=null
 ,p_LOANDTLSTMTINT8AMT NUMBER:=null
 ,p_LOANDTLSTMTINT9AMT NUMBER:=null
 ,p_LOANDTLSTMTNM VARCHAR2:=null
 ,p_LOANDTLSTRTBAL1AMT NUMBER:=null
 ,p_LOANDTLSTRTBAL2AMT NUMBER:=null
 ,p_LOANDTLSTRTBAL3AMT NUMBER:=null
 ,p_LOANDTLSTRTBAL4AMT NUMBER:=null
 ,p_LOANDTLSTRTBAL5AMT NUMBER:=null
 ,p_LOANDTLTAXID CHAR:=null
 ,p_LOANDTLTOBECURAMT NUMBER:=null
 ,p_LOANDTLTRANSCD CHAR:=null
 ,p_LOANDTLTRMYR NUMBER:=null
 ,p_LOANDTLUNDRWTRFEEMTDAMT NUMBER:=null
 ,p_LOANDTLUNDRWTRFEEPCT NUMBER:=null
 ,p_LOANDTLUNDRWTRFEEPTDAMT NUMBER:=null
 ,p_LOANDTLUNDRWTRFEEYTDAMT NUMBER:=null
 ,p_LOANDTLWITHHELOANDTLIND CHAR:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:34:42
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:34:42
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVLND1_STATUSVUPDTSP performs UPDATE on STGCSA.SOFVLND1_STATUSV
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVLND1TBL%rowtype;
 new_rec STGCSA.SOFVLND1TBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVLND1TBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVLND1_STATUSVUPDTSP build 2018-01-23 08:34:42 '
 ||'Select of STGCSA.SOFVLND1TBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVLND1_STATUSVUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:=null;
 end if;
 if new_rec.LOANDTLACHBNKROUTNMB<>old_rec.LOANDTLACHBNKROUTNMB
 or new_rec.LOANDTLACHBNKACCT<>old_rec.LOANDTLACHBNKACCT then
 new_rec.LOANDTLACHLASTCHNGDT:=sysdate;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVLND1AUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVLND1TBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVLND1_STATUSVUPDTSP build 2018-01-23 08:34:42 '
 ||'while doing update on STGCSA.SOFVLND1_STATUSV, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVLND1_STATUSVUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVLND1_STATUSVUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVLND1_STATUSV at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVLND1TBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVLND1_STATUSVUPDTSP build 2018-01-23 08:34:42 '
 ||'No STGCSA.SOFVLND1_STATUSV row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVLND1_STATUSVUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVLND1_STATUSVUPDTSP build 2018-01-23 08:34:42 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVLND1_STATUSVUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVLND1_STATUSVUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In SOFVLND1_STATUSVUPDTSP build 2018-01-23 08:34:42'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVLND1_STATUSVUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVLND1_STATUSVUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


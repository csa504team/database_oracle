<!--- Saved 01/28/2018 14:42:11. --->
PROCEDURE JobQueInsTSP
 /* Date : 11/01/2017
 Programmer : Prasad Mente
 Updated : Rick Wu (11/14/2017)
 Remarks :
 Parameters : No. Name Type Description
 *********************************************************************************
 Revised By Date Commments
 *********************************************************************************
 *********************************************************************************
 */
 ( p_JobTypCd varchar2 := NULL,
 p_StatCd varchar2 := NULL,
 p_CreatUserId varchar2 := NULL
 )
 AS
 v_Cnt NUMBER(10);
 v_SeqNmb number(10);
 v_StatCd char(1);
 v_isAllowed char(1);
 p_retval number := 0;
 p_errval number := 0;
 p_errmsg varchar2(500) := '';
 BEGIN
 -- check EXP/IMP Type
 IF (upper(p_JobTypCd) not in ('01','02','03','04'))
 THEN
 BEGIN
 --ROLLBACK;
 RAISE_APPLICATION_ERROR( -20001, 'Invalid Job Type');
 RETURN;
 END;
 END IF;
 
 IF p_JobTypCd = '01' THEN
 BEGIN
 -- check if STG2CSA is allowed
 SELECT stgcsa.isProcessAllowed('STG2CSA') INTO v_isAllowed FROM dual;
 
 IF v_isAllowed != 'Y' THEN
 RAISE_APPLICATION_ERROR( -20010, 'STG2CSA is not allowed');
 RETURN;
 END IF;
 END;
 END IF;
 
 --Insert
 SELECT NVL(MAX(SeqNmb), 0) + 1 into v_SeqNmb
 FROM STGCSA.JobQueTbl;
 
 -- make sure last status is not failure
 IF p_JobTypCd = '03'
 THEN
 select StatCd into v_StatCd from STGCSA.JobQueTbl where SeqNmb = v_SeqNmb - 1;
 IF v_StatCd = 'F'
 THEN
 RAISE_APPLICATION_ERROR( -20001, 'Last Status is a failure');
 RETURN;
 END IF;
 END IF;
 
 INSERT INTO STGCSA.JobQueTbl
 (
 SeqNmb,
 JobTypCd,
 CreatUserId,
 CreatDt,
 StatCd
 )
 VALUES (
 v_SeqNmb,
 p_JobTypCd,
 p_CreatUserId,
 SYSDATE,
 p_Statcd
 );
 
 IF p_JobTypCd = '01' THEN
 STGCSA.TaskStartCSP('STG2CSA', 'CSA_Batch', p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
 END IF;
 
 -- Rollback if there is a pending EXP/IMP request
 SELECT count(*) INTO v_Cnt FROM STGCSA.JobQueTbl
 WHERE EndDt IS NULL;
 IF v_Cnt > 1
 THEN
 BEGIN
 RAISE_APPLICATION_ERROR( -20001, 'There is pending task in DBExpImpTbl');
 --ROLLBACK;
 RETURN;
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/30/2017 21:44:21. --->
PROCEDURE SFOVLND1INSTSP(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_userid varchar2
 ,p_LOANNMB CHAR
 ,p_LOANDTLSBAOFC CHAR
 ,p_LOANDTLBORRNM VARCHAR2
 ,p_LOANDTLBORRMAILADRSTR1NM VARCHAR2
 ,p_LOANDTLBORRMAILADRSTR2NM VARCHAR2
 ,p_LOANDTLBORRMAILADRCTYNM VARCHAR2
 ,p_LOANDTLBORRMAILADRSTCD CHAR
 ,p_LOANDTLBORRMAILADRZIPCD CHAR
 ,p_LOANDTLBORRMAILADRZIP4CD CHAR
 ,p_LOANDTLSBCNM VARCHAR2
 ,p_LOANDTLSBCMAILADRSTR1NM VARCHAR2
 ,p_LOANDTLSBCMAILADRSTR2NM VARCHAR2
 ,p_LOANDTLSBCMAILADRCTYNM VARCHAR2
 ,p_LOANDTLSBCMAILADRSTCD CHAR
 ,p_LOANDTLSBCMAILADRZIPCD CHAR
 ,p_LOANDTLSBCMAILADRZIP4CD CHAR
 ,p_LOANDTLTAXID CHAR
 ,p_LOANDTLCDCREGNCD CHAR
 ,p_LOANDTLCDCCERT CHAR
 ,p_LOANDTLDEFPYMT CHAR
 ,p_LOANDTLDBENTRPRINAMT VARCHAR2
 ,p_LOANDTLDBENTRCURBALAMT VARCHAR2
 ,p_LOANDTLDBENTRINTRTPCT VARCHAR2
 ,p_LOANDTLISSDT DATE
 ,p_LOANDTLDEFPYMTDTX CHAR
 ,p_LOANDTLDBENTRPYMTDTTRM CHAR
 ,p_LOANDTLDBENTRPROCDSAMT VARCHAR2
 ,p_LOANDTLDBENTRPROCDSESCROWAMT VARCHAR2
 ,p_LOANDTLSEMIANNPYMTAMT VARCHAR2
 ,p_LOANDTLNOTEPRINAMT VARCHAR2
 ,p_LOANDTLNOTECURBALAMT VARCHAR2
 ,p_LOANDTLNOTEINTRTPCT VARCHAR2
 ,p_LOANDTLNOTEDT DATE
 ,p_LOANDTLACCTCD VARCHAR2
 ,p_LOANDTLACCTCHNGCD CHAR
 ,p_LOANDTLREAMIND CHAR
 ,p_LOANDTLNOTEPYMTDTTRM CHAR
 ,p_LOANDTLRESRVDEPAMT VARCHAR2
 ,p_LOANDTLUNDRWTRFEEPCT VARCHAR2
 ,p_LOANDTLUNDRWTRFEEMTDAMT VARCHAR2
 ,p_LOANDTLUNDRWTRFEEYTDAMT VARCHAR2
 ,p_LOANDTLUNDRWTRFEEPTDAMT VARCHAR2
 ,p_LOANDTLCDCFEEPCT VARCHAR2
 ,p_LOANDTLCDCFEEMTDAMT VARCHAR2
 ,p_LOANDTLCDCFEEYTDAMT VARCHAR2
 ,p_LOANDTLCDCFEEPTDAMT VARCHAR2
 ,p_LOANDTLCSAFEEPCT VARCHAR2
 ,p_LOANDTLCSAFEEMTDAMT VARCHAR2
 ,p_LOANDTLCSAFEEYTDAMT VARCHAR2
 ,p_LOANDTLCSAFEEPTDAMT VARCHAR2
 ,p_LOANDTLATTORNEYFEEAMT VARCHAR2
 ,p_LOANDTLINITFEEPCT VARCHAR2
 ,p_LOANDTLINITFEEDOLLRAMT VARCHAR2
 ,p_LOANDTLFUNDFEEPCT VARCHAR2
 ,p_LOANDTLFUNDFEEDOLLRAMT VARCHAR2
 ,p_LOANDTLCBNKNM VARCHAR2
 ,p_LOANDTLCBNKMAILADRSTR1NM VARCHAR2
 ,p_LOANDTLCBNKMAILADRSTR2NM VARCHAR2
 ,p_LOANDTLCBNKMAILADRCTYNM VARCHAR2
 ,p_LOANDTLCBNKMAILADRSTCD CHAR
 ,p_LOANDTLCBNKMAILADRZIPCD CHAR
 ,p_LOANDTLCBNKMAILADRZIP4CD CHAR
 ,p_LOANDTLCACCTNM VARCHAR2
 ,p_LOANDTLCOLOANDTLACCT VARCHAR2
 ,p_LOANDTLCROUTSYM CHAR
 ,p_LOANDTLCTRANSCD CHAR
 ,p_LOANDTLCATTEN VARCHAR2
 ,p_LOANDTLRBNKNM VARCHAR2
 ,p_LOANDTLRBNKMAILADRSTR1NM VARCHAR2
 ,p_LOANDTLRBNKMAILADRSTR2NM VARCHAR2
 ,p_LOANDTLRBNKMAILADRCTYNM VARCHAR2
 ,p_LOANDTLRBNKMAILADRSTCD CHAR
 ,p_LOANDTLRBNKMAILADRZIPCD CHAR
 ,p_LOANDTLRBNKMAILADRZIP4CD CHAR
 ,p_LOANDTLRACCTNM VARCHAR2
 ,p_LOANDTLROLOANDTLACCT VARCHAR2
 ,p_LOANDTLRROUTSYM CHAR
 ,p_LOANDTLTRANSCD CHAR
 ,p_LOANDTLRATTEN VARCHAR2
 ,p_LOANDTLPYMT1AMT VARCHAR2
 ,p_LOANDTLPYMTDT1X CHAR
 ,p_LOANDTLPRIN1AMT VARCHAR2
 ,p_LOANDTLINT1AMT VARCHAR2
 ,p_LOANDTLCSA1PCT VARCHAR2
 ,p_LOANDTLCSADOLLR1AMT VARCHAR2
 ,p_LOANDTLCDC1PCT VARCHAR2
 ,p_LOANDTLCDCDOLLR1AMT VARCHAR2
 ,p_LOANDTLSTMTNM VARCHAR2
 ,p_LOANDTLCURCSAFEEAMT VARCHAR2
 ,p_LOANDTLCURCDCFEEAMT VARCHAR2
 ,p_LOANDTLCURRESRVAMT VARCHAR2
 ,p_LOANDTLCURFEEBALAMT VARCHAR2
 ,p_LOANDTLAMPRINLEFTAMT VARCHAR2
 ,p_LOANDTLAMTHRUDTX CHAR
 ,p_LOANDTLAPPIND CHAR
 ,p_LOANDTLSPPIND CHAR
 ,p_LOANDTLSPPLQDAMT VARCHAR2
 ,p_LOANDTLSPPAUTOPYMTAMT VARCHAR2
 ,p_LOANDTLSPPAUTOPCT VARCHAR2
 ,p_LOANDTLSTMTINT1AMT VARCHAR2
 ,p_LOANDTLSTMTINT2AMT VARCHAR2
 ,p_LOANDTLSTMTINT3AMT VARCHAR2
 ,p_LOANDTLSTMTINT4AMT VARCHAR2
 ,p_LOANDTLSTMTINT5AMT VARCHAR2
 ,p_LOANDTLSTMTINT6AMT VARCHAR2
 ,p_LOANDTLSTMTINT7AMT VARCHAR2
 ,p_LOANDTLSTMTINT8AMT VARCHAR2
 ,p_LOANDTLSTMTINT9AMT VARCHAR2
 ,p_LOANDTLSTMTINT10AMT VARCHAR2
 ,p_LOANDTLSTMTINT11AMT VARCHAR2
 ,p_LOANDTLSTMTINT12AMT VARCHAR2
 ,p_LOANDTLSTMTINT13AMT VARCHAR2
 ,p_LOANDTLSTMTAVGMOBALAMT VARCHAR2
 ,p_LOANDTLAMPRINAMT VARCHAR2
 ,p_LOANDTLAMINTAMT VARCHAR2
 ,p_LOANDTLREFIIND CHAR
 ,p_LOANDTLSETUPIND CHAR
 ,p_LOANDTLCREATDT DATE
 ,p_LOANDTLLMAINTNDT DATE
 ,p_LOANDTLCONVDT DATE
 ,p_LOANDTLPRGRM CHAR
 ,p_LOANDTLNOTEMOPYMTAMT VARCHAR2
 ,p_LOANDTLDBENTRMATDT DATE
 ,p_LOANDTLNOTEMATDT DATE
 ,p_LOANDTLRESRVDEPPCT VARCHAR2
 ,p_LOANDTLCDCPFEEPCT VARCHAR2
 ,p_LOANDTLCDCPFEEDOLLRAMT VARCHAR2
 ,p_LOANDTLDUEBORRAMT VARCHAR2
 ,p_LOANDTLAPPVDT DATE
 ,p_LOANDTLLTFEEIND CHAR
 ,p_LOANDTLACHPRENTIND CHAR
 ,p_LOANDTLAMSCHDLTYP CHAR
 ,p_LOANDTLACHBNKNM VARCHAR2
 ,p_LOANDTLACHBNKMAILADRSTR1NM VARCHAR2
 ,p_LOANDTLACHBNKMAILADRSTR2NM VARCHAR2
 ,p_LOANDTLACHBNKMAILADRCTYNM VARCHAR2
 ,p_LOANDTLACHBNKMAILADRSTCD CHAR
 ,p_LOANDTLACHBNKMAILADRZIPCD CHAR
 ,p_LOANDTLACHBNKMAILADRZIP4CD CHAR
 ,p_LOANDTLACHBNKBR CHAR
 ,p_LOANDTLACHBNKACTYP CHAR
 ,p_LOANDTLACHBNKACCT VARCHAR2
 ,p_LOANDTLACHBNKROUTNMB CHAR
 ,p_LOANDTLACHBNKTRANS CHAR
 ,p_LOANDTLACHBNKIDNOORATTEN VARCHAR2
 ,p_LOANDTLACHDEPNM VARCHAR2
 ,p_LOANDTLACHSIGNDT DATE
 ,p_LOANDTLBORRNM2 VARCHAR2
 ,p_LOANDTLSBCNM2 VARCHAR2
 ,p_LOANDTLCACCTNMB VARCHAR2
 ,p_LOANDTLRACCTNMB VARCHAR2
 ,p_LOANDTLINTLENDRIND CHAR
 ,p_LOANDTLINTRMLENDR VARCHAR2
 ,p_LOANDTLPYMTRECV VARCHAR2
 ,p_LOANDTLTRMYR VARCHAR2
 ,p_LOANDTLLOANTYP CHAR
 ,p_LOANDTLMLACCTNMB VARCHAR2
 ,p_LOANDTLEXCESSDUEBORRAMT VARCHAR2
 ,p_LOANDTL503SUBTYP CHAR
 ,p_LOANDTLLPYMTDT DATE
 ,p_LOANDTLPOOLSERS CHAR
 ,p_LOANDTLSBAFEEPCT VARCHAR2
 ,p_LOANDTLSBAPAIDTHRUDTX CHAR
 ,p_LOANDTLSBAFEEAMT VARCHAR2
 ,p_LOANDTLPRMAMT VARCHAR2
 ,p_LOANDTLLENDRSBAFEEAMT VARCHAR2
 ,p_LOANDTLWITHHELOANDTLIND CHAR
 ,p_LOANDTLPRGRMTYP CHAR
 ,p_LOANDTLLATEIND CHAR
 ,p_LOANDTLCMNT1 VARCHAR2
 ,p_LOANDTLCMNT2 VARCHAR2
 ,p_LOANDTLSOD CHAR
 ,p_LOANDTLTOBECURAMT VARCHAR2
 ,p_LOANDTLGNTYREPAYPTDAMT VARCHAR2
 ,p_LOANDTLCDCPAIDTHRUDTX CHAR
 ,p_LOANDTLCSAPAIDTHRUDTX CHAR
 ,p_LOANDTLINTPAIDTHRUDTX CHAR
 ,p_LOANDTLSTRTBAL1AMT VARCHAR2
 ,p_LOANDTLSTRTBAL2AMT VARCHAR2
 ,p_LOANDTLSTRTBAL3AMT VARCHAR2
 ,p_LOANDTLSTRTBAL4AMT VARCHAR2
 ,p_LOANDTLSTRTBAL5AMT VARCHAR2
 ,p_LOANDTLSBCIDNMB CHAR
 ,p_LOANDTLACHLASTCHNGDT DATE
 ,p_LOANDTLPRENOTETESTDT DATE
 ,p_LOANDTLPYMTNMBDUE VARCHAR2
 ,p_LOANDTLRESRVAMT VARCHAR2
 ,p_LOANDTLFEEBASEAMT VARCHAR2
 ,p_LOANDTLSTATCDOFLOAN CHAR
 ,p_LOANDTLACTUALCSAINITFEEAMT VARCHAR2
 ,p_LOANDTLSTATDT DATE
 ,p_LOANDTLSTMTCLSBALAMT VARCHAR2
 ,p_LOANDTLSTMTCLSDT DATE
 ,p_LOANDTLLASTDBPAYOUTAMT VARCHAR2
 ,p_LOANDTLCHEMICALBASISDAYS VARCHAR2
 ) as
 /*
 Created on: 2017-10-27 14:27:06
 Created by: GENR
 Crerated from template stdtblins_template v2.0 24 Oct 2017 on 2017-10-27 14:27:06
 Using SNAP V1.2 24 Oct 2017 J. Low Binary Frond, Select Computing
 Purpose : standard insert of row to table STGCSA.SOFVLND1TBL
 */
 errfound boolean:=false;
 sucessfully_defaulted boolean;
 errstr varchar2(4000):=' ';
 rec_t1 STGCSA.SOFVLND1TBL%rowtype;
 begin
 -- setup
 savepoint SFOVLND1INSTSP;
 p_errval:=0;
 p_errmsg:=null; 
 rec_t1.creatuserid:=p_userid;
 rec_t1.creatdt:=sysdate;
 rec_t1.lastupdtuserid:=p_userid;
 rec_t1.lastupdtdt:=sysdate;
 STGCSA.jot('Entered SFOVLND1INSTSPdbg p_identifier='||p_identifier||'<');
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure 
 -- copy pased table column parm values to rec_t1
 -- for numeric colval parms, move to number with exception handler for errors
 begin
 rec_t1.LOANDTLDBENTRPRINAMT:=P_LOANDTLDBENTRPRINAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDBENTRPRINAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDBENTRPRINAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLDBENTRCURBALAMT:=P_LOANDTLDBENTRCURBALAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDBENTRCURBALAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDBENTRCURBALAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLDBENTRINTRTPCT:=P_LOANDTLDBENTRINTRTPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDBENTRINTRTPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDBENTRINTRTPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLDBENTRPROCDSAMT:=P_LOANDTLDBENTRPROCDSAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDBENTRPROCDSAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDBENTRPROCDSAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLDBENTRPROCDSESCROWAMT:=P_LOANDTLDBENTRPROCDSESCROWAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDBENTRPROCDSESCROWAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDBENTRPROCDSESCROWAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSEMIANNPYMTAMT:=P_LOANDTLSEMIANNPYMTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSEMIANNPYMTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSEMIANNPYMTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLNOTEPRINAMT:=P_LOANDTLNOTEPRINAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLNOTEPRINAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLNOTEPRINAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLNOTECURBALAMT:=P_LOANDTLNOTECURBALAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLNOTECURBALAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLNOTECURBALAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLNOTEINTRTPCT:=P_LOANDTLNOTEINTRTPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLNOTEINTRTPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLNOTEINTRTPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLACCTCD:=P_LOANDTLACCTCD;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLACCTCD got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLACCTCD:=0; 
 end; 
 begin
 rec_t1.LOANDTLRESRVDEPAMT:=P_LOANDTLRESRVDEPAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLRESRVDEPAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLRESRVDEPAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLUNDRWTRFEEPCT:=P_LOANDTLUNDRWTRFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLUNDRWTRFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLUNDRWTRFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLUNDRWTRFEEMTDAMT:=P_LOANDTLUNDRWTRFEEMTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLUNDRWTRFEEMTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLUNDRWTRFEEMTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLUNDRWTRFEEYTDAMT:=P_LOANDTLUNDRWTRFEEYTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLUNDRWTRFEEYTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLUNDRWTRFEEYTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLUNDRWTRFEEPTDAMT:=P_LOANDTLUNDRWTRFEEPTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLUNDRWTRFEEPTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLUNDRWTRFEEPTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCFEEPCT:=P_LOANDTLCDCFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCFEEMTDAMT:=P_LOANDTLCDCFEEMTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCFEEMTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCFEEMTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCFEEYTDAMT:=P_LOANDTLCDCFEEYTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCFEEYTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCFEEYTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCFEEPTDAMT:=P_LOANDTLCDCFEEPTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCFEEPTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCFEEPTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSAFEEPCT:=P_LOANDTLCSAFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSAFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSAFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSAFEEMTDAMT:=P_LOANDTLCSAFEEMTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSAFEEMTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSAFEEMTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSAFEEYTDAMT:=P_LOANDTLCSAFEEYTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSAFEEYTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSAFEEYTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSAFEEPTDAMT:=P_LOANDTLCSAFEEPTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSAFEEPTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSAFEEPTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLATTORNEYFEEAMT:=P_LOANDTLATTORNEYFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLATTORNEYFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLATTORNEYFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLINITFEEPCT:=P_LOANDTLINITFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLINITFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLINITFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLINITFEEDOLLRAMT:=P_LOANDTLINITFEEDOLLRAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLINITFEEDOLLRAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLINITFEEDOLLRAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLFUNDFEEPCT:=P_LOANDTLFUNDFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLFUNDFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLFUNDFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLFUNDFEEDOLLRAMT:=P_LOANDTLFUNDFEEDOLLRAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLFUNDFEEDOLLRAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLFUNDFEEDOLLRAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCOLOANDTLACCT:=P_LOANDTLCOLOANDTLACCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCOLOANDTLACCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCOLOANDTLACCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLROLOANDTLACCT:=P_LOANDTLROLOANDTLACCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLROLOANDTLACCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLROLOANDTLACCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLPYMT1AMT:=P_LOANDTLPYMT1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLPYMT1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLPYMT1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLPRIN1AMT:=P_LOANDTLPRIN1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLPRIN1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLPRIN1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLINT1AMT:=P_LOANDTLINT1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLINT1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLINT1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSA1PCT:=P_LOANDTLCSA1PCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSA1PCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSA1PCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCSADOLLR1AMT:=P_LOANDTLCSADOLLR1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCSADOLLR1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCSADOLLR1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDC1PCT:=P_LOANDTLCDC1PCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDC1PCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDC1PCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCDOLLR1AMT:=P_LOANDTLCDCDOLLR1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCDOLLR1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCDOLLR1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCURCSAFEEAMT:=P_LOANDTLCURCSAFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCURCSAFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCURCSAFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCURCDCFEEAMT:=P_LOANDTLCURCDCFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCURCDCFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCURCDCFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCURRESRVAMT:=P_LOANDTLCURRESRVAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCURRESRVAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCURRESRVAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCURFEEBALAMT:=P_LOANDTLCURFEEBALAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCURFEEBALAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCURFEEBALAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLAMPRINLEFTAMT:=P_LOANDTLAMPRINLEFTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLAMPRINLEFTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLAMPRINLEFTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSPPLQDAMT:=P_LOANDTLSPPLQDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSPPLQDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSPPLQDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSPPAUTOPYMTAMT:=P_LOANDTLSPPAUTOPYMTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSPPAUTOPYMTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSPPAUTOPYMTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSPPAUTOPCT:=P_LOANDTLSPPAUTOPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSPPAUTOPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSPPAUTOPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT1AMT:=P_LOANDTLSTMTINT1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT2AMT:=P_LOANDTLSTMTINT2AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT2AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT2AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT3AMT:=P_LOANDTLSTMTINT3AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT3AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT3AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT4AMT:=P_LOANDTLSTMTINT4AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT4AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT4AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT5AMT:=P_LOANDTLSTMTINT5AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT5AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT5AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT6AMT:=P_LOANDTLSTMTINT6AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT6AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT6AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT7AMT:=P_LOANDTLSTMTINT7AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT7AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT7AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT8AMT:=P_LOANDTLSTMTINT8AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT8AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT8AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT9AMT:=P_LOANDTLSTMTINT9AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT9AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT9AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT10AMT:=P_LOANDTLSTMTINT10AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT10AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT10AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT11AMT:=P_LOANDTLSTMTINT11AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT11AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT11AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT12AMT:=P_LOANDTLSTMTINT12AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT12AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT12AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTINT13AMT:=P_LOANDTLSTMTINT13AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTINT13AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTINT13AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTAVGMOBALAMT:=P_LOANDTLSTMTAVGMOBALAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTAVGMOBALAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTAVGMOBALAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLAMPRINAMT:=P_LOANDTLAMPRINAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLAMPRINAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLAMPRINAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLAMINTAMT:=P_LOANDTLAMINTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLAMINTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLAMINTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLNOTEMOPYMTAMT:=P_LOANDTLNOTEMOPYMTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLNOTEMOPYMTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLNOTEMOPYMTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLRESRVDEPPCT:=P_LOANDTLRESRVDEPPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLRESRVDEPPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLRESRVDEPPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCPFEEPCT:=P_LOANDTLCDCPFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCPFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCPFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCDCPFEEDOLLRAMT:=P_LOANDTLCDCPFEEDOLLRAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCDCPFEEDOLLRAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCDCPFEEDOLLRAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLDUEBORRAMT:=P_LOANDTLDUEBORRAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLDUEBORRAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLDUEBORRAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLINTRMLENDR:=P_LOANDTLINTRMLENDR;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLINTRMLENDR got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLINTRMLENDR:=0; 
 end; 
 begin
 rec_t1.LOANDTLPYMTRECV:=P_LOANDTLPYMTRECV;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLPYMTRECV got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLPYMTRECV:=0; 
 end; 
 begin
 rec_t1.LOANDTLTRMYR:=P_LOANDTLTRMYR;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLTRMYR got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLTRMYR:=0; 
 end; 
 begin
 rec_t1.LOANDTLEXCESSDUEBORRAMT:=P_LOANDTLEXCESSDUEBORRAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLEXCESSDUEBORRAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLEXCESSDUEBORRAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSBAFEEPCT:=P_LOANDTLSBAFEEPCT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSBAFEEPCT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSBAFEEPCT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSBAFEEAMT:=P_LOANDTLSBAFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSBAFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSBAFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLPRMAMT:=P_LOANDTLPRMAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLPRMAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLPRMAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLLENDRSBAFEEAMT:=P_LOANDTLLENDRSBAFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLLENDRSBAFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLLENDRSBAFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLTOBECURAMT:=P_LOANDTLTOBECURAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLTOBECURAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLTOBECURAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLGNTYREPAYPTDAMT:=P_LOANDTLGNTYREPAYPTDAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLGNTYREPAYPTDAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLGNTYREPAYPTDAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTRTBAL1AMT:=P_LOANDTLSTRTBAL1AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTRTBAL1AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTRTBAL1AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTRTBAL2AMT:=P_LOANDTLSTRTBAL2AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTRTBAL2AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTRTBAL2AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTRTBAL3AMT:=P_LOANDTLSTRTBAL3AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTRTBAL3AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTRTBAL3AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTRTBAL4AMT:=P_LOANDTLSTRTBAL4AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTRTBAL4AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTRTBAL4AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTRTBAL5AMT:=P_LOANDTLSTRTBAL5AMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTRTBAL5AMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTRTBAL5AMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLPYMTNMBDUE:=P_LOANDTLPYMTNMBDUE;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLPYMTNMBDUE got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLPYMTNMBDUE:=0; 
 end; 
 begin
 rec_t1.LOANDTLRESRVAMT:=P_LOANDTLRESRVAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLRESRVAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLRESRVAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLFEEBASEAMT:=P_LOANDTLFEEBASEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLFEEBASEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLFEEBASEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLACTUALCSAINITFEEAMT:=P_LOANDTLACTUALCSAINITFEEAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLACTUALCSAINITFEEAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLACTUALCSAINITFEEAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLSTMTCLSBALAMT:=P_LOANDTLSTMTCLSBALAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLSTMTCLSBALAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLSTMTCLSBALAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLLASTDBPAYOUTAMT:=P_LOANDTLLASTDBPAYOUTAMT;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLLASTDBPAYOUTAMT got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLLASTDBPAYOUTAMT:=0; 
 end; 
 begin
 rec_t1.LOANDTLCHEMICALBASISDAYS:=P_LOANDTLCHEMICALBASISDAYS;
 exception when others then 
 if length(errstr)<3800 then
 errstr:=errstr||' LOANDTLCHEMICALBASISDAYS got'||sqlerrm(sqlcode);
 end if;
 errfound:=true;
 rec_t1.LOANDTLCHEMICALBASISDAYS:=0; 
 end; 
 STGCSA.jot('After copying numeric parms, errstr:='||errstr);
 -- copy all non-numeric parms over
 rec_t1.LOANNMB:=P_LOANNMB;
 rec_t1.LOANDTLSBAOFC:=P_LOANDTLSBAOFC;
 rec_t1.LOANDTLBORRNM:=P_LOANDTLBORRNM;
 rec_t1.LOANDTLBORRMAILADRSTR1NM:=P_LOANDTLBORRMAILADRSTR1NM;
 rec_t1.LOANDTLBORRMAILADRSTR2NM:=P_LOANDTLBORRMAILADRSTR2NM;
 rec_t1.LOANDTLBORRMAILADRCTYNM:=P_LOANDTLBORRMAILADRCTYNM;
 rec_t1.LOANDTLBORRMAILADRSTCD:=P_LOANDTLBORRMAILADRSTCD;
 rec_t1.LOANDTLBORRMAILADRZIPCD:=P_LOANDTLBORRMAILADRZIPCD;
 rec_t1.LOANDTLBORRMAILADRZIP4CD:=P_LOANDTLBORRMAILADRZIP4CD;
 rec_t1.LOANDTLSBCNM:=P_LOANDTLSBCNM;
 rec_t1.LOANDTLSBCMAILADRSTR1NM:=P_LOANDTLSBCMAILADRSTR1NM;
 rec_t1.LOANDTLSBCMAILADRSTR2NM:=P_LOANDTLSBCMAILADRSTR2NM;
 rec_t1.LOANDTLSBCMAILADRCTYNM:=P_LOANDTLSBCMAILADRCTYNM;
 rec_t1.LOANDTLSBCMAILADRSTCD:=P_LOANDTLSBCMAILADRSTCD;
 rec_t1.LOANDTLSBCMAILADRZIPCD:=P_LOANDTLSBCMAILADRZIPCD;
 rec_t1.LOANDTLSBCMAILADRZIP4CD:=P_LOANDTLSBCMAILADRZIP4CD;
 rec_t1.LOANDTLTAXID:=P_LOANDTLTAXID;
 rec_t1.LOANDTLCDCREGNCD:=P_LOANDTLCDCREGNCD;
 rec_t1.LOANDTLCDCCERT:=P_LOANDTLCDCCERT;
 rec_t1.LOANDTLDEFPYMT:=P_LOANDTLDEFPYMT;
 rec_t1.LOANDTLISSDT:=P_LOANDTLISSDT;
 rec_t1.LOANDTLDEFPYMTDTX:=P_LOANDTLDEFPYMTDTX;
 rec_t1.LOANDTLDBENTRPYMTDTTRM:=P_LOANDTLDBENTRPYMTDTTRM;
 rec_t1.LOANDTLNOTEDT:=P_LOANDTLNOTEDT;
 rec_t1.LOANDTLACCTCHNGCD:=P_LOANDTLACCTCHNGCD;
 rec_t1.LOANDTLREAMIND:=P_LOANDTLREAMIND;
 rec_t1.LOANDTLNOTEPYMTDTTRM:=P_LOANDTLNOTEPYMTDTTRM;
 rec_t1.LOANDTLCBNKNM:=P_LOANDTLCBNKNM;
 rec_t1.LOANDTLCBNKMAILADRSTR1NM:=P_LOANDTLCBNKMAILADRSTR1NM;
 rec_t1.LOANDTLCBNKMAILADRSTR2NM:=P_LOANDTLCBNKMAILADRSTR2NM;
 rec_t1.LOANDTLCBNKMAILADRCTYNM:=P_LOANDTLCBNKMAILADRCTYNM;
 rec_t1.LOANDTLCBNKMAILADRSTCD:=P_LOANDTLCBNKMAILADRSTCD;
 rec_t1.LOANDTLCBNKMAILADRZIPCD:=P_LOANDTLCBNKMAILADRZIPCD;
 rec_t1.LOANDTLCBNKMAILADRZIP4CD:=P_LOANDTLCBNKMAILADRZIP4CD;
 rec_t1.LOANDTLCACCTNM:=P_LOANDTLCACCTNM;
 rec_t1.LOANDTLCROUTSYM:=P_LOANDTLCROUTSYM;
 rec_t1.LOANDTLCTRANSCD:=P_LOANDTLCTRANSCD;
 rec_t1.LOANDTLCATTEN:=P_LOANDTLCATTEN;
 rec_t1.LOANDTLRBNKNM:=P_LOANDTLRBNKNM;
 rec_t1.LOANDTLRBNKMAILADRSTR1NM:=P_LOANDTLRBNKMAILADRSTR1NM;
 rec_t1.LOANDTLRBNKMAILADRSTR2NM:=P_LOANDTLRBNKMAILADRSTR2NM;
 rec_t1.LOANDTLRBNKMAILADRCTYNM:=P_LOANDTLRBNKMAILADRCTYNM;
 rec_t1.LOANDTLRBNKMAILADRSTCD:=P_LOANDTLRBNKMAILADRSTCD;
 rec_t1.LOANDTLRBNKMAILADRZIPCD:=P_LOANDTLRBNKMAILADRZIPCD;
 rec_t1.LOANDTLRBNKMAILADRZIP4CD:=P_LOANDTLRBNKMAILADRZIP4CD;
 rec_t1.LOANDTLRACCTNM:=P_LOANDTLRACCTNM;
 rec_t1.LOANDTLRROUTSYM:=P_LOANDTLRROUTSYM;
 rec_t1.LOANDTLTRANSCD:=P_LOANDTLTRANSCD;
 rec_t1.LOANDTLRATTEN:=P_LOANDTLRATTEN;
 rec_t1.LOANDTLPYMTDT1X:=P_LOANDTLPYMTDT1X;
 rec_t1.LOANDTLSTMTNM:=P_LOANDTLSTMTNM;
 rec_t1.LOANDTLAMTHRUDTX:=P_LOANDTLAMTHRUDTX;
 rec_t1.LOANDTLAPPIND:=P_LOANDTLAPPIND;
 rec_t1.LOANDTLSPPIND:=P_LOANDTLSPPIND;
 rec_t1.LOANDTLREFIIND:=P_LOANDTLREFIIND;
 rec_t1.LOANDTLSETUPIND:=P_LOANDTLSETUPIND;
 rec_t1.LOANDTLCREATDT:=P_LOANDTLCREATDT;
 rec_t1.LOANDTLLMAINTNDT:=P_LOANDTLLMAINTNDT;
 rec_t1.LOANDTLCONVDT:=P_LOANDTLCONVDT;
 rec_t1.LOANDTLPRGRM:=P_LOANDTLPRGRM;
 rec_t1.LOANDTLDBENTRMATDT:=P_LOANDTLDBENTRMATDT;
 rec_t1.LOANDTLNOTEMATDT:=P_LOANDTLNOTEMATDT;
 rec_t1.LOANDTLAPPVDT:=P_LOANDTLAPPVDT;
 rec_t1.LOANDTLLTFEEIND:=P_LOANDTLLTFEEIND;
 rec_t1.LOANDTLACHPRENTIND:=P_LOANDTLACHPRENTIND;
 rec_t1.LOANDTLAMSCHDLTYP:=P_LOANDTLAMSCHDLTYP;
 rec_t1.LOANDTLACHBNKNM:=P_LOANDTLACHBNKNM;
 rec_t1.LOANDTLACHBNKMAILADRSTR1NM:=P_LOANDTLACHBNKMAILADRSTR1NM;
 rec_t1.LOANDTLACHBNKMAILADRSTR2NM:=P_LOANDTLACHBNKMAILADRSTR2NM;
 rec_t1.LOANDTLACHBNKMAILADRCTYNM:=P_LOANDTLACHBNKMAILADRCTYNM;
 rec_t1.LOANDTLACHBNKMAILADRSTCD:=P_LOANDTLACHBNKMAILADRSTCD;
 rec_t1.LOANDTLACHBNKMAILADRZIPCD:=P_LOANDTLACHBNKMAILADRZIPCD;
 rec_t1.LOANDTLACHBNKMAILADRZIP4CD:=P_LOANDTLACHBNKMAILADRZIP4CD;
 rec_t1.LOANDTLACHBNKBR:=P_LOANDTLACHBNKBR;
 rec_t1.LOANDTLACHBNKACTYP:=P_LOANDTLACHBNKACTYP;
 rec_t1.LOANDTLACHBNKACCT:=P_LOANDTLACHBNKACCT;
 rec_t1.LOANDTLACHBNKROUTNMB:=P_LOANDTLACHBNKROUTNMB;
 rec_t1.LOANDTLACHBNKTRANS:=P_LOANDTLACHBNKTRANS;
 rec_t1.LOANDTLACHBNKIDNOORATTEN:=P_LOANDTLACHBNKIDNOORATTEN;
 rec_t1.LOANDTLACHDEPNM:=P_LOANDTLACHDEPNM;
 rec_t1.LOANDTLACHSIGNDT:=P_LOANDTLACHSIGNDT;
 rec_t1.LOANDTLBORRNM2:=P_LOANDTLBORRNM2;
 rec_t1.LOANDTLSBCNM2:=P_LOANDTLSBCNM2;
 rec_t1.LOANDTLCACCTNMB:=P_LOANDTLCACCTNMB;
 rec_t1.LOANDTLRACCTNMB:=P_LOANDTLRACCTNMB;
 rec_t1.LOANDTLINTLENDRIND:=P_LOANDTLINTLENDRIND;
 rec_t1.LOANDTLLOANTYP:=P_LOANDTLLOANTYP;
 rec_t1.LOANDTLMLACCTNMB:=P_LOANDTLMLACCTNMB;
 rec_t1.LOANDTL503SUBTYP:=P_LOANDTL503SUBTYP;
 rec_t1.LOANDTLLPYMTDT:=P_LOANDTLLPYMTDT;
 rec_t1.LOANDTLPOOLSERS:=P_LOANDTLPOOLSERS;
 rec_t1.LOANDTLSBAPAIDTHRUDTX:=P_LOANDTLSBAPAIDTHRUDTX;
 rec_t1.LOANDTLWITHHELOANDTLIND:=P_LOANDTLWITHHELOANDTLIND;
 rec_t1.LOANDTLPRGRMTYP:=P_LOANDTLPRGRMTYP;
 rec_t1.LOANDTLLATEIND:=P_LOANDTLLATEIND;
 rec_t1.LOANDTLCMNT1:=P_LOANDTLCMNT1;
 rec_t1.LOANDTLCMNT2:=P_LOANDTLCMNT2;
 rec_t1.LOANDTLSOD:=P_LOANDTLSOD;
 rec_t1.LOANDTLCDCPAIDTHRUDTX:=P_LOANDTLCDCPAIDTHRUDTX;
 rec_t1.LOANDTLCSAPAIDTHRUDTX:=P_LOANDTLCSAPAIDTHRUDTX;
 rec_t1.LOANDTLINTPAIDTHRUDTX:=P_LOANDTLINTPAIDTHRUDTX;
 rec_t1.LOANDTLSBCIDNMB:=P_LOANDTLSBCIDNMB;
 rec_t1.LOANDTLACHLASTCHNGDT:=P_LOANDTLACHLASTCHNGDT;
 rec_t1.LOANDTLPRENOTETESTDT:=P_LOANDTLPRENOTETESTDT;
 rec_t1.LOANDTLSTATCDOFLOAN:=P_LOANDTLSTATCDOFLOAN;
 rec_t1.LOANDTLSTATDT:=P_LOANDTLSTATDT;
 rec_t1.LOANDTLSTMTCLSDT:=P_LOANDTLSTMTCLSDT;
 
 STGCSA.jot('After copying remaining parms');
 -- check for passed nulls 
 jot('checking LOANNMB datatype CHAR'||rec_t1.LOANNMB||'<');
 If rec_t1.LOANNMB is null then 
 rec_t1.LOANNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANNMB',
 'CHAR',10,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBAOFC datatype CHAR'||rec_t1.LOANDTLSBAOFC||'<');
 If rec_t1.LOANDTLSBAOFC is null then 
 rec_t1.LOANDTLSBAOFC:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBAOFC',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBAOFC';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRNM datatype VARCHAR2'||rec_t1.LOANDTLBORRNM||'<');
 If rec_t1.LOANDTLBORRNM is null then 
 rec_t1.LOANDTLBORRNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRSTR1NM datatype VARCHAR2'||rec_t1.LOANDTLBORRMAILADRSTR1NM||'<');
 If rec_t1.LOANDTLBORRMAILADRSTR1NM is null then 
 rec_t1.LOANDTLBORRMAILADRSTR1NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRSTR1NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRSTR1NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRSTR2NM datatype VARCHAR2'||rec_t1.LOANDTLBORRMAILADRSTR2NM||'<');
 If rec_t1.LOANDTLBORRMAILADRSTR2NM is null then 
 rec_t1.LOANDTLBORRMAILADRSTR2NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRSTR2NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRSTR2NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRCTYNM datatype VARCHAR2'||rec_t1.LOANDTLBORRMAILADRCTYNM||'<');
 If rec_t1.LOANDTLBORRMAILADRCTYNM is null then 
 rec_t1.LOANDTLBORRMAILADRCTYNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRCTYNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRCTYNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRSTCD datatype CHAR'||rec_t1.LOANDTLBORRMAILADRSTCD||'<');
 If rec_t1.LOANDTLBORRMAILADRSTCD is null then 
 rec_t1.LOANDTLBORRMAILADRSTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRSTCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRSTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRZIPCD datatype CHAR'||rec_t1.LOANDTLBORRMAILADRZIPCD||'<');
 If rec_t1.LOANDTLBORRMAILADRZIPCD is null then 
 rec_t1.LOANDTLBORRMAILADRZIPCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRZIPCD',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRZIPCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRMAILADRZIP4CD datatype CHAR'||rec_t1.LOANDTLBORRMAILADRZIP4CD||'<');
 If rec_t1.LOANDTLBORRMAILADRZIP4CD is null then 
 rec_t1.LOANDTLBORRMAILADRZIP4CD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRMAILADRZIP4CD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRMAILADRZIP4CD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCNM datatype VARCHAR2'||rec_t1.LOANDTLSBCNM||'<');
 If rec_t1.LOANDTLSBCNM is null then 
 rec_t1.LOANDTLSBCNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRSTR1NM datatype VARCHAR2'||rec_t1.LOANDTLSBCMAILADRSTR1NM||'<');
 If rec_t1.LOANDTLSBCMAILADRSTR1NM is null then 
 rec_t1.LOANDTLSBCMAILADRSTR1NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRSTR1NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRSTR1NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRSTR2NM datatype VARCHAR2'||rec_t1.LOANDTLSBCMAILADRSTR2NM||'<');
 If rec_t1.LOANDTLSBCMAILADRSTR2NM is null then 
 rec_t1.LOANDTLSBCMAILADRSTR2NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRSTR2NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRSTR2NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRCTYNM datatype VARCHAR2'||rec_t1.LOANDTLSBCMAILADRCTYNM||'<');
 If rec_t1.LOANDTLSBCMAILADRCTYNM is null then 
 rec_t1.LOANDTLSBCMAILADRCTYNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRCTYNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRCTYNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRSTCD datatype CHAR'||rec_t1.LOANDTLSBCMAILADRSTCD||'<');
 If rec_t1.LOANDTLSBCMAILADRSTCD is null then 
 rec_t1.LOANDTLSBCMAILADRSTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRSTCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRSTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRZIPCD datatype CHAR'||rec_t1.LOANDTLSBCMAILADRZIPCD||'<');
 If rec_t1.LOANDTLSBCMAILADRZIPCD is null then 
 rec_t1.LOANDTLSBCMAILADRZIPCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRZIPCD',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRZIPCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCMAILADRZIP4CD datatype CHAR'||rec_t1.LOANDTLSBCMAILADRZIP4CD||'<');
 If rec_t1.LOANDTLSBCMAILADRZIP4CD is null then 
 rec_t1.LOANDTLSBCMAILADRZIP4CD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCMAILADRZIP4CD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCMAILADRZIP4CD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLTAXID datatype CHAR'||rec_t1.LOANDTLTAXID||'<');
 If rec_t1.LOANDTLTAXID is null then 
 rec_t1.LOANDTLTAXID:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLTAXID',
 'CHAR',10,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLTAXID';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCREGNCD datatype CHAR'||rec_t1.LOANDTLCDCREGNCD||'<');
 If rec_t1.LOANDTLCDCREGNCD is null then 
 rec_t1.LOANDTLCDCREGNCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCREGNCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCREGNCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCCERT datatype CHAR'||rec_t1.LOANDTLCDCCERT||'<');
 If rec_t1.LOANDTLCDCCERT is null then 
 rec_t1.LOANDTLCDCCERT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCCERT',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCCERT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDEFPYMT datatype CHAR'||rec_t1.LOANDTLDEFPYMT||'<');
 If rec_t1.LOANDTLDEFPYMT is null then 
 rec_t1.LOANDTLDEFPYMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDEFPYMT',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDEFPYMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRPRINAMT datatype NUMBER'||rec_t1.LOANDTLDBENTRPRINAMT||'<');
 If rec_t1.LOANDTLDBENTRPRINAMT is null then 
 rec_t1.LOANDTLDBENTRPRINAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRPRINAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRPRINAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRCURBALAMT datatype NUMBER'||rec_t1.LOANDTLDBENTRCURBALAMT||'<');
 If rec_t1.LOANDTLDBENTRCURBALAMT is null then 
 rec_t1.LOANDTLDBENTRCURBALAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRCURBALAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRCURBALAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRINTRTPCT datatype NUMBER'||rec_t1.LOANDTLDBENTRINTRTPCT||'<');
 If rec_t1.LOANDTLDBENTRINTRTPCT is null then 
 rec_t1.LOANDTLDBENTRINTRTPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRINTRTPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRINTRTPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLISSDT datatype DATE'||rec_t1.LOANDTLISSDT||'<');
 If rec_t1.LOANDTLISSDT is null then 
 rec_t1.LOANDTLISSDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLISSDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLISSDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDEFPYMTDTX datatype CHAR'||rec_t1.LOANDTLDEFPYMTDTX||'<');
 If rec_t1.LOANDTLDEFPYMTDTX is null then 
 rec_t1.LOANDTLDEFPYMTDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDEFPYMTDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDEFPYMTDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRPYMTDTTRM datatype CHAR'||rec_t1.LOANDTLDBENTRPYMTDTTRM||'<');
 If rec_t1.LOANDTLDBENTRPYMTDTTRM is null then 
 rec_t1.LOANDTLDBENTRPYMTDTTRM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRPYMTDTTRM',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRPYMTDTTRM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRPROCDSAMT datatype NUMBER'||rec_t1.LOANDTLDBENTRPROCDSAMT||'<');
 If rec_t1.LOANDTLDBENTRPROCDSAMT is null then 
 rec_t1.LOANDTLDBENTRPROCDSAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRPROCDSAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRPROCDSAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRPROCDSESCROWAMT datatype NUMBER'||rec_t1.LOANDTLDBENTRPROCDSESCROWAMT||'<');
 If rec_t1.LOANDTLDBENTRPROCDSESCROWAMT is null then 
 rec_t1.LOANDTLDBENTRPROCDSESCROWAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRPROCDSESCROWAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRPROCDSESCROWAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSEMIANNPYMTAMT datatype NUMBER'||rec_t1.LOANDTLSEMIANNPYMTAMT||'<');
 If rec_t1.LOANDTLSEMIANNPYMTAMT is null then 
 rec_t1.LOANDTLSEMIANNPYMTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSEMIANNPYMTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSEMIANNPYMTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEPRINAMT datatype NUMBER'||rec_t1.LOANDTLNOTEPRINAMT||'<');
 If rec_t1.LOANDTLNOTEPRINAMT is null then 
 rec_t1.LOANDTLNOTEPRINAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEPRINAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEPRINAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTECURBALAMT datatype NUMBER'||rec_t1.LOANDTLNOTECURBALAMT||'<');
 If rec_t1.LOANDTLNOTECURBALAMT is null then 
 rec_t1.LOANDTLNOTECURBALAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTECURBALAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTECURBALAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEINTRTPCT datatype NUMBER'||rec_t1.LOANDTLNOTEINTRTPCT||'<');
 If rec_t1.LOANDTLNOTEINTRTPCT is null then 
 rec_t1.LOANDTLNOTEINTRTPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEINTRTPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEINTRTPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEDT datatype DATE'||rec_t1.LOANDTLNOTEDT||'<');
 If rec_t1.LOANDTLNOTEDT is null then 
 rec_t1.LOANDTLNOTEDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACCTCD datatype NUMBER'||rec_t1.LOANDTLACCTCD||'<');
 If rec_t1.LOANDTLACCTCD is null then 
 rec_t1.LOANDTLACCTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACCTCD',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACCTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACCTCHNGCD datatype CHAR'||rec_t1.LOANDTLACCTCHNGCD||'<');
 If rec_t1.LOANDTLACCTCHNGCD is null then 
 rec_t1.LOANDTLACCTCHNGCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACCTCHNGCD',
 'CHAR',3,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACCTCHNGCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLREAMIND datatype CHAR'||rec_t1.LOANDTLREAMIND||'<');
 If rec_t1.LOANDTLREAMIND is null then 
 rec_t1.LOANDTLREAMIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLREAMIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLREAMIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEPYMTDTTRM datatype CHAR'||rec_t1.LOANDTLNOTEPYMTDTTRM||'<');
 If rec_t1.LOANDTLNOTEPYMTDTTRM is null then 
 rec_t1.LOANDTLNOTEPYMTDTTRM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEPYMTDTTRM',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEPYMTDTTRM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRESRVDEPAMT datatype NUMBER'||rec_t1.LOANDTLRESRVDEPAMT||'<');
 If rec_t1.LOANDTLRESRVDEPAMT is null then 
 rec_t1.LOANDTLRESRVDEPAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRESRVDEPAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRESRVDEPAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLUNDRWTRFEEPCT datatype NUMBER'||rec_t1.LOANDTLUNDRWTRFEEPCT||'<');
 If rec_t1.LOANDTLUNDRWTRFEEPCT is null then 
 rec_t1.LOANDTLUNDRWTRFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLUNDRWTRFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLUNDRWTRFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLUNDRWTRFEEMTDAMT datatype NUMBER'||rec_t1.LOANDTLUNDRWTRFEEMTDAMT||'<');
 If rec_t1.LOANDTLUNDRWTRFEEMTDAMT is null then 
 rec_t1.LOANDTLUNDRWTRFEEMTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLUNDRWTRFEEMTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLUNDRWTRFEEMTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLUNDRWTRFEEYTDAMT datatype NUMBER'||rec_t1.LOANDTLUNDRWTRFEEYTDAMT||'<');
 If rec_t1.LOANDTLUNDRWTRFEEYTDAMT is null then 
 rec_t1.LOANDTLUNDRWTRFEEYTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLUNDRWTRFEEYTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLUNDRWTRFEEYTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLUNDRWTRFEEPTDAMT datatype NUMBER'||rec_t1.LOANDTLUNDRWTRFEEPTDAMT||'<');
 If rec_t1.LOANDTLUNDRWTRFEEPTDAMT is null then 
 rec_t1.LOANDTLUNDRWTRFEEPTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLUNDRWTRFEEPTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLUNDRWTRFEEPTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCFEEPCT datatype NUMBER'||rec_t1.LOANDTLCDCFEEPCT||'<');
 If rec_t1.LOANDTLCDCFEEPCT is null then 
 rec_t1.LOANDTLCDCFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCFEEMTDAMT datatype NUMBER'||rec_t1.LOANDTLCDCFEEMTDAMT||'<');
 If rec_t1.LOANDTLCDCFEEMTDAMT is null then 
 rec_t1.LOANDTLCDCFEEMTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCFEEMTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCFEEMTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCFEEYTDAMT datatype NUMBER'||rec_t1.LOANDTLCDCFEEYTDAMT||'<');
 If rec_t1.LOANDTLCDCFEEYTDAMT is null then 
 rec_t1.LOANDTLCDCFEEYTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCFEEYTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCFEEYTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCFEEPTDAMT datatype NUMBER'||rec_t1.LOANDTLCDCFEEPTDAMT||'<');
 If rec_t1.LOANDTLCDCFEEPTDAMT is null then 
 rec_t1.LOANDTLCDCFEEPTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCFEEPTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCFEEPTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSAFEEPCT datatype NUMBER'||rec_t1.LOANDTLCSAFEEPCT||'<');
 If rec_t1.LOANDTLCSAFEEPCT is null then 
 rec_t1.LOANDTLCSAFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSAFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSAFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSAFEEMTDAMT datatype NUMBER'||rec_t1.LOANDTLCSAFEEMTDAMT||'<');
 If rec_t1.LOANDTLCSAFEEMTDAMT is null then 
 rec_t1.LOANDTLCSAFEEMTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSAFEEMTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSAFEEMTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSAFEEYTDAMT datatype NUMBER'||rec_t1.LOANDTLCSAFEEYTDAMT||'<');
 If rec_t1.LOANDTLCSAFEEYTDAMT is null then 
 rec_t1.LOANDTLCSAFEEYTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSAFEEYTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSAFEEYTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSAFEEPTDAMT datatype NUMBER'||rec_t1.LOANDTLCSAFEEPTDAMT||'<');
 If rec_t1.LOANDTLCSAFEEPTDAMT is null then 
 rec_t1.LOANDTLCSAFEEPTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSAFEEPTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSAFEEPTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLATTORNEYFEEAMT datatype NUMBER'||rec_t1.LOANDTLATTORNEYFEEAMT||'<');
 If rec_t1.LOANDTLATTORNEYFEEAMT is null then 
 rec_t1.LOANDTLATTORNEYFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLATTORNEYFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLATTORNEYFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINITFEEPCT datatype NUMBER'||rec_t1.LOANDTLINITFEEPCT||'<');
 If rec_t1.LOANDTLINITFEEPCT is null then 
 rec_t1.LOANDTLINITFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINITFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINITFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINITFEEDOLLRAMT datatype NUMBER'||rec_t1.LOANDTLINITFEEDOLLRAMT||'<');
 If rec_t1.LOANDTLINITFEEDOLLRAMT is null then 
 rec_t1.LOANDTLINITFEEDOLLRAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINITFEEDOLLRAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINITFEEDOLLRAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLFUNDFEEPCT datatype NUMBER'||rec_t1.LOANDTLFUNDFEEPCT||'<');
 If rec_t1.LOANDTLFUNDFEEPCT is null then 
 rec_t1.LOANDTLFUNDFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLFUNDFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLFUNDFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLFUNDFEEDOLLRAMT datatype NUMBER'||rec_t1.LOANDTLFUNDFEEDOLLRAMT||'<');
 If rec_t1.LOANDTLFUNDFEEDOLLRAMT is null then 
 rec_t1.LOANDTLFUNDFEEDOLLRAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLFUNDFEEDOLLRAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLFUNDFEEDOLLRAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKNM datatype VARCHAR2'||rec_t1.LOANDTLCBNKNM||'<');
 If rec_t1.LOANDTLCBNKNM is null then 
 rec_t1.LOANDTLCBNKNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRSTR1NM datatype VARCHAR2'||rec_t1.LOANDTLCBNKMAILADRSTR1NM||'<');
 If rec_t1.LOANDTLCBNKMAILADRSTR1NM is null then 
 rec_t1.LOANDTLCBNKMAILADRSTR1NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRSTR1NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRSTR1NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRSTR2NM datatype VARCHAR2'||rec_t1.LOANDTLCBNKMAILADRSTR2NM||'<');
 If rec_t1.LOANDTLCBNKMAILADRSTR2NM is null then 
 rec_t1.LOANDTLCBNKMAILADRSTR2NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRSTR2NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRSTR2NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRCTYNM datatype VARCHAR2'||rec_t1.LOANDTLCBNKMAILADRCTYNM||'<');
 If rec_t1.LOANDTLCBNKMAILADRCTYNM is null then 
 rec_t1.LOANDTLCBNKMAILADRCTYNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRCTYNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRCTYNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRSTCD datatype CHAR'||rec_t1.LOANDTLCBNKMAILADRSTCD||'<');
 If rec_t1.LOANDTLCBNKMAILADRSTCD is null then 
 rec_t1.LOANDTLCBNKMAILADRSTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRSTCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRSTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRZIPCD datatype CHAR'||rec_t1.LOANDTLCBNKMAILADRZIPCD||'<');
 If rec_t1.LOANDTLCBNKMAILADRZIPCD is null then 
 rec_t1.LOANDTLCBNKMAILADRZIPCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRZIPCD',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRZIPCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCBNKMAILADRZIP4CD datatype CHAR'||rec_t1.LOANDTLCBNKMAILADRZIP4CD||'<');
 If rec_t1.LOANDTLCBNKMAILADRZIP4CD is null then 
 rec_t1.LOANDTLCBNKMAILADRZIP4CD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCBNKMAILADRZIP4CD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCBNKMAILADRZIP4CD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCACCTNM datatype VARCHAR2'||rec_t1.LOANDTLCACCTNM||'<');
 If rec_t1.LOANDTLCACCTNM is null then 
 rec_t1.LOANDTLCACCTNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCACCTNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCACCTNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCOLOANDTLACCT datatype NUMBER'||rec_t1.LOANDTLCOLOANDTLACCT||'<');
 If rec_t1.LOANDTLCOLOANDTLACCT is null then 
 rec_t1.LOANDTLCOLOANDTLACCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCOLOANDTLACCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCOLOANDTLACCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCROUTSYM datatype CHAR'||rec_t1.LOANDTLCROUTSYM||'<');
 If rec_t1.LOANDTLCROUTSYM is null then 
 rec_t1.LOANDTLCROUTSYM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCROUTSYM',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCROUTSYM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCTRANSCD datatype CHAR'||rec_t1.LOANDTLCTRANSCD||'<');
 If rec_t1.LOANDTLCTRANSCD is null then 
 rec_t1.LOANDTLCTRANSCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCTRANSCD',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCTRANSCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCATTEN datatype VARCHAR2'||rec_t1.LOANDTLCATTEN||'<');
 If rec_t1.LOANDTLCATTEN is null then 
 rec_t1.LOANDTLCATTEN:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCATTEN',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCATTEN';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKNM datatype VARCHAR2'||rec_t1.LOANDTLRBNKNM||'<');
 If rec_t1.LOANDTLRBNKNM is null then 
 rec_t1.LOANDTLRBNKNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRSTR1NM datatype VARCHAR2'||rec_t1.LOANDTLRBNKMAILADRSTR1NM||'<');
 If rec_t1.LOANDTLRBNKMAILADRSTR1NM is null then 
 rec_t1.LOANDTLRBNKMAILADRSTR1NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRSTR1NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRSTR1NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRSTR2NM datatype VARCHAR2'||rec_t1.LOANDTLRBNKMAILADRSTR2NM||'<');
 If rec_t1.LOANDTLRBNKMAILADRSTR2NM is null then 
 rec_t1.LOANDTLRBNKMAILADRSTR2NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRSTR2NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRSTR2NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRCTYNM datatype VARCHAR2'||rec_t1.LOANDTLRBNKMAILADRCTYNM||'<');
 If rec_t1.LOANDTLRBNKMAILADRCTYNM is null then 
 rec_t1.LOANDTLRBNKMAILADRCTYNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRCTYNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRCTYNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRSTCD datatype CHAR'||rec_t1.LOANDTLRBNKMAILADRSTCD||'<');
 If rec_t1.LOANDTLRBNKMAILADRSTCD is null then 
 rec_t1.LOANDTLRBNKMAILADRSTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRSTCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRSTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRZIPCD datatype CHAR'||rec_t1.LOANDTLRBNKMAILADRZIPCD||'<');
 If rec_t1.LOANDTLRBNKMAILADRZIPCD is null then 
 rec_t1.LOANDTLRBNKMAILADRZIPCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRZIPCD',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRZIPCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRBNKMAILADRZIP4CD datatype CHAR'||rec_t1.LOANDTLRBNKMAILADRZIP4CD||'<');
 If rec_t1.LOANDTLRBNKMAILADRZIP4CD is null then 
 rec_t1.LOANDTLRBNKMAILADRZIP4CD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRBNKMAILADRZIP4CD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRBNKMAILADRZIP4CD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRACCTNM datatype VARCHAR2'||rec_t1.LOANDTLRACCTNM||'<');
 If rec_t1.LOANDTLRACCTNM is null then 
 rec_t1.LOANDTLRACCTNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRACCTNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRACCTNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLROLOANDTLACCT datatype NUMBER'||rec_t1.LOANDTLROLOANDTLACCT||'<');
 If rec_t1.LOANDTLROLOANDTLACCT is null then 
 rec_t1.LOANDTLROLOANDTLACCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLROLOANDTLACCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLROLOANDTLACCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRROUTSYM datatype CHAR'||rec_t1.LOANDTLRROUTSYM||'<');
 If rec_t1.LOANDTLRROUTSYM is null then 
 rec_t1.LOANDTLRROUTSYM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRROUTSYM',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRROUTSYM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLTRANSCD datatype CHAR'||rec_t1.LOANDTLTRANSCD||'<');
 If rec_t1.LOANDTLTRANSCD is null then 
 rec_t1.LOANDTLTRANSCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLTRANSCD',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLTRANSCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRATTEN datatype VARCHAR2'||rec_t1.LOANDTLRATTEN||'<');
 If rec_t1.LOANDTLRATTEN is null then 
 rec_t1.LOANDTLRATTEN:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRATTEN',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRATTEN';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPYMT1AMT datatype NUMBER'||rec_t1.LOANDTLPYMT1AMT||'<');
 If rec_t1.LOANDTLPYMT1AMT is null then 
 rec_t1.LOANDTLPYMT1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPYMT1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPYMT1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPYMTDT1X datatype CHAR'||rec_t1.LOANDTLPYMTDT1X||'<');
 If rec_t1.LOANDTLPYMTDT1X is null then 
 rec_t1.LOANDTLPYMTDT1X:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPYMTDT1X',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPYMTDT1X';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPRIN1AMT datatype NUMBER'||rec_t1.LOANDTLPRIN1AMT||'<');
 If rec_t1.LOANDTLPRIN1AMT is null then 
 rec_t1.LOANDTLPRIN1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPRIN1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPRIN1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINT1AMT datatype NUMBER'||rec_t1.LOANDTLINT1AMT||'<');
 If rec_t1.LOANDTLINT1AMT is null then 
 rec_t1.LOANDTLINT1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINT1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINT1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSA1PCT datatype NUMBER'||rec_t1.LOANDTLCSA1PCT||'<');
 If rec_t1.LOANDTLCSA1PCT is null then 
 rec_t1.LOANDTLCSA1PCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSA1PCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSA1PCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSADOLLR1AMT datatype NUMBER'||rec_t1.LOANDTLCSADOLLR1AMT||'<');
 If rec_t1.LOANDTLCSADOLLR1AMT is null then 
 rec_t1.LOANDTLCSADOLLR1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSADOLLR1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSADOLLR1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDC1PCT datatype NUMBER'||rec_t1.LOANDTLCDC1PCT||'<');
 If rec_t1.LOANDTLCDC1PCT is null then 
 rec_t1.LOANDTLCDC1PCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDC1PCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDC1PCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCDOLLR1AMT datatype NUMBER'||rec_t1.LOANDTLCDCDOLLR1AMT||'<');
 If rec_t1.LOANDTLCDCDOLLR1AMT is null then 
 rec_t1.LOANDTLCDCDOLLR1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCDOLLR1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCDOLLR1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTNM datatype VARCHAR2'||rec_t1.LOANDTLSTMTNM||'<');
 If rec_t1.LOANDTLSTMTNM is null then 
 rec_t1.LOANDTLSTMTNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCURCSAFEEAMT datatype NUMBER'||rec_t1.LOANDTLCURCSAFEEAMT||'<');
 If rec_t1.LOANDTLCURCSAFEEAMT is null then 
 rec_t1.LOANDTLCURCSAFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCURCSAFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCURCSAFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCURCDCFEEAMT datatype NUMBER'||rec_t1.LOANDTLCURCDCFEEAMT||'<');
 If rec_t1.LOANDTLCURCDCFEEAMT is null then 
 rec_t1.LOANDTLCURCDCFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCURCDCFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCURCDCFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCURRESRVAMT datatype NUMBER'||rec_t1.LOANDTLCURRESRVAMT||'<');
 If rec_t1.LOANDTLCURRESRVAMT is null then 
 rec_t1.LOANDTLCURRESRVAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCURRESRVAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCURRESRVAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCURFEEBALAMT datatype NUMBER'||rec_t1.LOANDTLCURFEEBALAMT||'<');
 If rec_t1.LOANDTLCURFEEBALAMT is null then 
 rec_t1.LOANDTLCURFEEBALAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCURFEEBALAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCURFEEBALAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAMPRINLEFTAMT datatype NUMBER'||rec_t1.LOANDTLAMPRINLEFTAMT||'<');
 If rec_t1.LOANDTLAMPRINLEFTAMT is null then 
 rec_t1.LOANDTLAMPRINLEFTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAMPRINLEFTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAMPRINLEFTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAMTHRUDTX datatype CHAR'||rec_t1.LOANDTLAMTHRUDTX||'<');
 If rec_t1.LOANDTLAMTHRUDTX is null then 
 rec_t1.LOANDTLAMTHRUDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAMTHRUDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAMTHRUDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAPPIND datatype CHAR'||rec_t1.LOANDTLAPPIND||'<');
 If rec_t1.LOANDTLAPPIND is null then 
 rec_t1.LOANDTLAPPIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAPPIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAPPIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSPPIND datatype CHAR'||rec_t1.LOANDTLSPPIND||'<');
 If rec_t1.LOANDTLSPPIND is null then 
 rec_t1.LOANDTLSPPIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSPPIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSPPIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSPPLQDAMT datatype NUMBER'||rec_t1.LOANDTLSPPLQDAMT||'<');
 If rec_t1.LOANDTLSPPLQDAMT is null then 
 rec_t1.LOANDTLSPPLQDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSPPLQDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSPPLQDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSPPAUTOPYMTAMT datatype NUMBER'||rec_t1.LOANDTLSPPAUTOPYMTAMT||'<');
 If rec_t1.LOANDTLSPPAUTOPYMTAMT is null then 
 rec_t1.LOANDTLSPPAUTOPYMTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSPPAUTOPYMTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSPPAUTOPYMTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSPPAUTOPCT datatype NUMBER'||rec_t1.LOANDTLSPPAUTOPCT||'<');
 If rec_t1.LOANDTLSPPAUTOPCT is null then 
 rec_t1.LOANDTLSPPAUTOPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSPPAUTOPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSPPAUTOPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT1AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT1AMT||'<');
 If rec_t1.LOANDTLSTMTINT1AMT is null then 
 rec_t1.LOANDTLSTMTINT1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT2AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT2AMT||'<');
 If rec_t1.LOANDTLSTMTINT2AMT is null then 
 rec_t1.LOANDTLSTMTINT2AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT2AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT2AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT3AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT3AMT||'<');
 If rec_t1.LOANDTLSTMTINT3AMT is null then 
 rec_t1.LOANDTLSTMTINT3AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT3AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT3AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT4AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT4AMT||'<');
 If rec_t1.LOANDTLSTMTINT4AMT is null then 
 rec_t1.LOANDTLSTMTINT4AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT4AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT4AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT5AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT5AMT||'<');
 If rec_t1.LOANDTLSTMTINT5AMT is null then 
 rec_t1.LOANDTLSTMTINT5AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT5AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT5AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT6AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT6AMT||'<');
 If rec_t1.LOANDTLSTMTINT6AMT is null then 
 rec_t1.LOANDTLSTMTINT6AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT6AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT6AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT7AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT7AMT||'<');
 If rec_t1.LOANDTLSTMTINT7AMT is null then 
 rec_t1.LOANDTLSTMTINT7AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT7AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT7AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT8AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT8AMT||'<');
 If rec_t1.LOANDTLSTMTINT8AMT is null then 
 rec_t1.LOANDTLSTMTINT8AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT8AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT8AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT9AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT9AMT||'<');
 If rec_t1.LOANDTLSTMTINT9AMT is null then 
 rec_t1.LOANDTLSTMTINT9AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT9AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT9AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT10AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT10AMT||'<');
 If rec_t1.LOANDTLSTMTINT10AMT is null then 
 rec_t1.LOANDTLSTMTINT10AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT10AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT10AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT11AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT11AMT||'<');
 If rec_t1.LOANDTLSTMTINT11AMT is null then 
 rec_t1.LOANDTLSTMTINT11AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT11AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT11AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT12AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT12AMT||'<');
 If rec_t1.LOANDTLSTMTINT12AMT is null then 
 rec_t1.LOANDTLSTMTINT12AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT12AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT12AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTINT13AMT datatype NUMBER'||rec_t1.LOANDTLSTMTINT13AMT||'<');
 If rec_t1.LOANDTLSTMTINT13AMT is null then 
 rec_t1.LOANDTLSTMTINT13AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTINT13AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTINT13AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTAVGMOBALAMT datatype NUMBER'||rec_t1.LOANDTLSTMTAVGMOBALAMT||'<');
 If rec_t1.LOANDTLSTMTAVGMOBALAMT is null then 
 rec_t1.LOANDTLSTMTAVGMOBALAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTAVGMOBALAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTAVGMOBALAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAMPRINAMT datatype NUMBER'||rec_t1.LOANDTLAMPRINAMT||'<');
 If rec_t1.LOANDTLAMPRINAMT is null then 
 rec_t1.LOANDTLAMPRINAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAMPRINAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAMPRINAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAMINTAMT datatype NUMBER'||rec_t1.LOANDTLAMINTAMT||'<');
 If rec_t1.LOANDTLAMINTAMT is null then 
 rec_t1.LOANDTLAMINTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAMINTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAMINTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLREFIIND datatype CHAR'||rec_t1.LOANDTLREFIIND||'<');
 If rec_t1.LOANDTLREFIIND is null then 
 rec_t1.LOANDTLREFIIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLREFIIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLREFIIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSETUPIND datatype CHAR'||rec_t1.LOANDTLSETUPIND||'<');
 If rec_t1.LOANDTLSETUPIND is null then 
 rec_t1.LOANDTLSETUPIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSETUPIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSETUPIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCREATDT datatype DATE'||rec_t1.LOANDTLCREATDT||'<');
 If rec_t1.LOANDTLCREATDT is null then 
 rec_t1.LOANDTLCREATDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCREATDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCREATDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLMAINTNDT datatype DATE'||rec_t1.LOANDTLLMAINTNDT||'<');
 If rec_t1.LOANDTLLMAINTNDT is null then 
 rec_t1.LOANDTLLMAINTNDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLMAINTNDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLMAINTNDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCONVDT datatype DATE'||rec_t1.LOANDTLCONVDT||'<');
 If rec_t1.LOANDTLCONVDT is null then 
 rec_t1.LOANDTLCONVDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCONVDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCONVDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPRGRM datatype CHAR'||rec_t1.LOANDTLPRGRM||'<');
 If rec_t1.LOANDTLPRGRM is null then 
 rec_t1.LOANDTLPRGRM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPRGRM',
 'CHAR',3,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPRGRM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEMOPYMTAMT datatype NUMBER'||rec_t1.LOANDTLNOTEMOPYMTAMT||'<');
 If rec_t1.LOANDTLNOTEMOPYMTAMT is null then 
 rec_t1.LOANDTLNOTEMOPYMTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEMOPYMTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEMOPYMTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDBENTRMATDT datatype DATE'||rec_t1.LOANDTLDBENTRMATDT||'<');
 If rec_t1.LOANDTLDBENTRMATDT is null then 
 rec_t1.LOANDTLDBENTRMATDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDBENTRMATDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDBENTRMATDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLNOTEMATDT datatype DATE'||rec_t1.LOANDTLNOTEMATDT||'<');
 If rec_t1.LOANDTLNOTEMATDT is null then 
 rec_t1.LOANDTLNOTEMATDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLNOTEMATDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLNOTEMATDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRESRVDEPPCT datatype NUMBER'||rec_t1.LOANDTLRESRVDEPPCT||'<');
 If rec_t1.LOANDTLRESRVDEPPCT is null then 
 rec_t1.LOANDTLRESRVDEPPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRESRVDEPPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRESRVDEPPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCPFEEPCT datatype NUMBER'||rec_t1.LOANDTLCDCPFEEPCT||'<');
 If rec_t1.LOANDTLCDCPFEEPCT is null then 
 rec_t1.LOANDTLCDCPFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCPFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCPFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCPFEEDOLLRAMT datatype NUMBER'||rec_t1.LOANDTLCDCPFEEDOLLRAMT||'<');
 If rec_t1.LOANDTLCDCPFEEDOLLRAMT is null then 
 rec_t1.LOANDTLCDCPFEEDOLLRAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCPFEEDOLLRAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCPFEEDOLLRAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLDUEBORRAMT datatype NUMBER'||rec_t1.LOANDTLDUEBORRAMT||'<');
 If rec_t1.LOANDTLDUEBORRAMT is null then 
 rec_t1.LOANDTLDUEBORRAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLDUEBORRAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLDUEBORRAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAPPVDT datatype DATE'||rec_t1.LOANDTLAPPVDT||'<');
 If rec_t1.LOANDTLAPPVDT is null then 
 rec_t1.LOANDTLAPPVDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAPPVDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAPPVDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLTFEEIND datatype CHAR'||rec_t1.LOANDTLLTFEEIND||'<');
 If rec_t1.LOANDTLLTFEEIND is null then 
 rec_t1.LOANDTLLTFEEIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLTFEEIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLTFEEIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHPRENTIND datatype CHAR'||rec_t1.LOANDTLACHPRENTIND||'<');
 If rec_t1.LOANDTLACHPRENTIND is null then 
 rec_t1.LOANDTLACHPRENTIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHPRENTIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHPRENTIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLAMSCHDLTYP datatype CHAR'||rec_t1.LOANDTLAMSCHDLTYP||'<');
 If rec_t1.LOANDTLAMSCHDLTYP is null then 
 rec_t1.LOANDTLAMSCHDLTYP:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLAMSCHDLTYP',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLAMSCHDLTYP';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKNM datatype VARCHAR2'||rec_t1.LOANDTLACHBNKNM||'<');
 If rec_t1.LOANDTLACHBNKNM is null then 
 rec_t1.LOANDTLACHBNKNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRSTR1NM datatype VARCHAR2'||rec_t1.LOANDTLACHBNKMAILADRSTR1NM||'<');
 If rec_t1.LOANDTLACHBNKMAILADRSTR1NM is null then 
 rec_t1.LOANDTLACHBNKMAILADRSTR1NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRSTR1NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRSTR1NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRSTR2NM datatype VARCHAR2'||rec_t1.LOANDTLACHBNKMAILADRSTR2NM||'<');
 If rec_t1.LOANDTLACHBNKMAILADRSTR2NM is null then 
 rec_t1.LOANDTLACHBNKMAILADRSTR2NM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRSTR2NM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRSTR2NM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRCTYNM datatype VARCHAR2'||rec_t1.LOANDTLACHBNKMAILADRCTYNM||'<');
 If rec_t1.LOANDTLACHBNKMAILADRCTYNM is null then 
 rec_t1.LOANDTLACHBNKMAILADRCTYNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRCTYNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRCTYNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRSTCD datatype CHAR'||rec_t1.LOANDTLACHBNKMAILADRSTCD||'<');
 If rec_t1.LOANDTLACHBNKMAILADRSTCD is null then 
 rec_t1.LOANDTLACHBNKMAILADRSTCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRSTCD',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRSTCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRZIPCD datatype CHAR'||rec_t1.LOANDTLACHBNKMAILADRZIPCD||'<');
 If rec_t1.LOANDTLACHBNKMAILADRZIPCD is null then 
 rec_t1.LOANDTLACHBNKMAILADRZIPCD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRZIPCD',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRZIPCD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKMAILADRZIP4CD datatype CHAR'||rec_t1.LOANDTLACHBNKMAILADRZIP4CD||'<');
 If rec_t1.LOANDTLACHBNKMAILADRZIP4CD is null then 
 rec_t1.LOANDTLACHBNKMAILADRZIP4CD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKMAILADRZIP4CD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKMAILADRZIP4CD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKBR datatype CHAR'||rec_t1.LOANDTLACHBNKBR||'<');
 If rec_t1.LOANDTLACHBNKBR is null then 
 rec_t1.LOANDTLACHBNKBR:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKBR',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKBR';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKACTYP datatype CHAR'||rec_t1.LOANDTLACHBNKACTYP||'<');
 If rec_t1.LOANDTLACHBNKACTYP is null then 
 rec_t1.LOANDTLACHBNKACTYP:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKACTYP',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKACTYP';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKACCT datatype VARCHAR2'||rec_t1.LOANDTLACHBNKACCT||'<');
 If rec_t1.LOANDTLACHBNKACCT is null then 
 rec_t1.LOANDTLACHBNKACCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKACCT',
 'VARCHAR2',30,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKACCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKROUTNMB datatype CHAR'||rec_t1.LOANDTLACHBNKROUTNMB||'<');
 If rec_t1.LOANDTLACHBNKROUTNMB is null then 
 rec_t1.LOANDTLACHBNKROUTNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKROUTNMB',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKROUTNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKTRANS datatype CHAR'||rec_t1.LOANDTLACHBNKTRANS||'<');
 If rec_t1.LOANDTLACHBNKTRANS is null then 
 rec_t1.LOANDTLACHBNKTRANS:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKTRANS',
 'CHAR',15,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKTRANS';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHBNKIDNOORATTEN datatype VARCHAR2'||rec_t1.LOANDTLACHBNKIDNOORATTEN||'<');
 If rec_t1.LOANDTLACHBNKIDNOORATTEN is null then 
 rec_t1.LOANDTLACHBNKIDNOORATTEN:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHBNKIDNOORATTEN',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHBNKIDNOORATTEN';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHDEPNM datatype VARCHAR2'||rec_t1.LOANDTLACHDEPNM||'<');
 If rec_t1.LOANDTLACHDEPNM is null then 
 rec_t1.LOANDTLACHDEPNM:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHDEPNM',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHDEPNM';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHSIGNDT datatype DATE'||rec_t1.LOANDTLACHSIGNDT||'<');
 If rec_t1.LOANDTLACHSIGNDT is null then 
 rec_t1.LOANDTLACHSIGNDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHSIGNDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHSIGNDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLBORRNM2 datatype VARCHAR2'||rec_t1.LOANDTLBORRNM2||'<');
 If rec_t1.LOANDTLBORRNM2 is null then 
 rec_t1.LOANDTLBORRNM2:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLBORRNM2',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLBORRNM2';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCNM2 datatype VARCHAR2'||rec_t1.LOANDTLSBCNM2||'<');
 If rec_t1.LOANDTLSBCNM2 is null then 
 rec_t1.LOANDTLSBCNM2:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCNM2',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCNM2';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCACCTNMB datatype VARCHAR2'||rec_t1.LOANDTLCACCTNMB||'<');
 If rec_t1.LOANDTLCACCTNMB is null then 
 rec_t1.LOANDTLCACCTNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCACCTNMB',
 'VARCHAR2',20,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCACCTNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRACCTNMB datatype VARCHAR2'||rec_t1.LOANDTLRACCTNMB||'<');
 If rec_t1.LOANDTLRACCTNMB is null then 
 rec_t1.LOANDTLRACCTNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRACCTNMB',
 'VARCHAR2',20,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRACCTNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINTLENDRIND datatype CHAR'||rec_t1.LOANDTLINTLENDRIND||'<');
 If rec_t1.LOANDTLINTLENDRIND is null then 
 rec_t1.LOANDTLINTLENDRIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINTLENDRIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINTLENDRIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINTRMLENDR datatype NUMBER'||rec_t1.LOANDTLINTRMLENDR||'<');
 If rec_t1.LOANDTLINTRMLENDR is null then 
 rec_t1.LOANDTLINTRMLENDR:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINTRMLENDR',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINTRMLENDR';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPYMTRECV datatype NUMBER'||rec_t1.LOANDTLPYMTRECV||'<');
 If rec_t1.LOANDTLPYMTRECV is null then 
 rec_t1.LOANDTLPYMTRECV:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPYMTRECV',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPYMTRECV';
 end if;
 end if;
 end if;
 jot('checking LOANDTLTRMYR datatype NUMBER'||rec_t1.LOANDTLTRMYR||'<');
 If rec_t1.LOANDTLTRMYR is null then 
 rec_t1.LOANDTLTRMYR:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLTRMYR',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLTRMYR';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLOANTYP datatype CHAR'||rec_t1.LOANDTLLOANTYP||'<');
 If rec_t1.LOANDTLLOANTYP is null then 
 rec_t1.LOANDTLLOANTYP:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLOANTYP',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLOANTYP';
 end if;
 end if;
 end if;
 jot('checking LOANDTLMLACCTNMB datatype VARCHAR2'||rec_t1.LOANDTLMLACCTNMB||'<');
 If rec_t1.LOANDTLMLACCTNMB is null then 
 rec_t1.LOANDTLMLACCTNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLMLACCTNMB',
 'VARCHAR2',20,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLMLACCTNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLEXCESSDUEBORRAMT datatype NUMBER'||rec_t1.LOANDTLEXCESSDUEBORRAMT||'<');
 If rec_t1.LOANDTLEXCESSDUEBORRAMT is null then 
 rec_t1.LOANDTLEXCESSDUEBORRAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLEXCESSDUEBORRAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLEXCESSDUEBORRAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTL503SUBTYP datatype CHAR'||rec_t1.LOANDTL503SUBTYP||'<');
 If rec_t1.LOANDTL503SUBTYP is null then 
 rec_t1.LOANDTL503SUBTYP:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTL503SUBTYP',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTL503SUBTYP';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLPYMTDT datatype DATE'||rec_t1.LOANDTLLPYMTDT||'<');
 If rec_t1.LOANDTLLPYMTDT is null then 
 rec_t1.LOANDTLLPYMTDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLPYMTDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLPYMTDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPOOLSERS datatype CHAR'||rec_t1.LOANDTLPOOLSERS||'<');
 If rec_t1.LOANDTLPOOLSERS is null then 
 rec_t1.LOANDTLPOOLSERS:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPOOLSERS',
 'CHAR',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPOOLSERS';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBAFEEPCT datatype NUMBER'||rec_t1.LOANDTLSBAFEEPCT||'<');
 If rec_t1.LOANDTLSBAFEEPCT is null then 
 rec_t1.LOANDTLSBAFEEPCT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBAFEEPCT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBAFEEPCT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBAPAIDTHRUDTX datatype CHAR'||rec_t1.LOANDTLSBAPAIDTHRUDTX||'<');
 If rec_t1.LOANDTLSBAPAIDTHRUDTX is null then 
 rec_t1.LOANDTLSBAPAIDTHRUDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBAPAIDTHRUDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBAPAIDTHRUDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBAFEEAMT datatype NUMBER'||rec_t1.LOANDTLSBAFEEAMT||'<');
 If rec_t1.LOANDTLSBAFEEAMT is null then 
 rec_t1.LOANDTLSBAFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBAFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBAFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPRMAMT datatype NUMBER'||rec_t1.LOANDTLPRMAMT||'<');
 If rec_t1.LOANDTLPRMAMT is null then 
 rec_t1.LOANDTLPRMAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPRMAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPRMAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLENDRSBAFEEAMT datatype NUMBER'||rec_t1.LOANDTLLENDRSBAFEEAMT||'<');
 If rec_t1.LOANDTLLENDRSBAFEEAMT is null then 
 rec_t1.LOANDTLLENDRSBAFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLENDRSBAFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLENDRSBAFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLWITHHELOANDTLIND datatype CHAR'||rec_t1.LOANDTLWITHHELOANDTLIND||'<');
 If rec_t1.LOANDTLWITHHELOANDTLIND is null then 
 rec_t1.LOANDTLWITHHELOANDTLIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLWITHHELOANDTLIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLWITHHELOANDTLIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPRGRMTYP datatype CHAR'||rec_t1.LOANDTLPRGRMTYP||'<');
 If rec_t1.LOANDTLPRGRMTYP is null then 
 rec_t1.LOANDTLPRGRMTYP:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPRGRMTYP',
 'CHAR',3,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPRGRMTYP';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLATEIND datatype CHAR'||rec_t1.LOANDTLLATEIND||'<');
 If rec_t1.LOANDTLLATEIND is null then 
 rec_t1.LOANDTLLATEIND:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLATEIND',
 'CHAR',1,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLATEIND';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCMNT1 datatype VARCHAR2'||rec_t1.LOANDTLCMNT1||'<');
 If rec_t1.LOANDTLCMNT1 is null then 
 rec_t1.LOANDTLCMNT1:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCMNT1',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCMNT1';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCMNT2 datatype VARCHAR2'||rec_t1.LOANDTLCMNT2||'<');
 If rec_t1.LOANDTLCMNT2 is null then 
 rec_t1.LOANDTLCMNT2:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCMNT2',
 'VARCHAR2',80,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCMNT2';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSOD datatype CHAR'||rec_t1.LOANDTLSOD||'<');
 If rec_t1.LOANDTLSOD is null then 
 rec_t1.LOANDTLSOD:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSOD',
 'CHAR',4,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSOD';
 end if;
 end if;
 end if;
 jot('checking LOANDTLTOBECURAMT datatype NUMBER'||rec_t1.LOANDTLTOBECURAMT||'<');
 If rec_t1.LOANDTLTOBECURAMT is null then 
 rec_t1.LOANDTLTOBECURAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLTOBECURAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLTOBECURAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLGNTYREPAYPTDAMT datatype NUMBER'||rec_t1.LOANDTLGNTYREPAYPTDAMT||'<');
 If rec_t1.LOANDTLGNTYREPAYPTDAMT is null then 
 rec_t1.LOANDTLGNTYREPAYPTDAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLGNTYREPAYPTDAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLGNTYREPAYPTDAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCDCPAIDTHRUDTX datatype CHAR'||rec_t1.LOANDTLCDCPAIDTHRUDTX||'<');
 If rec_t1.LOANDTLCDCPAIDTHRUDTX is null then 
 rec_t1.LOANDTLCDCPAIDTHRUDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCDCPAIDTHRUDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCDCPAIDTHRUDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCSAPAIDTHRUDTX datatype CHAR'||rec_t1.LOANDTLCSAPAIDTHRUDTX||'<');
 If rec_t1.LOANDTLCSAPAIDTHRUDTX is null then 
 rec_t1.LOANDTLCSAPAIDTHRUDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCSAPAIDTHRUDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCSAPAIDTHRUDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLINTPAIDTHRUDTX datatype CHAR'||rec_t1.LOANDTLINTPAIDTHRUDTX||'<');
 If rec_t1.LOANDTLINTPAIDTHRUDTX is null then 
 rec_t1.LOANDTLINTPAIDTHRUDTX:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLINTPAIDTHRUDTX',
 'CHAR',8,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLINTPAIDTHRUDTX';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTRTBAL1AMT datatype NUMBER'||rec_t1.LOANDTLSTRTBAL1AMT||'<');
 If rec_t1.LOANDTLSTRTBAL1AMT is null then 
 rec_t1.LOANDTLSTRTBAL1AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTRTBAL1AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTRTBAL1AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTRTBAL2AMT datatype NUMBER'||rec_t1.LOANDTLSTRTBAL2AMT||'<');
 If rec_t1.LOANDTLSTRTBAL2AMT is null then 
 rec_t1.LOANDTLSTRTBAL2AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTRTBAL2AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTRTBAL2AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTRTBAL3AMT datatype NUMBER'||rec_t1.LOANDTLSTRTBAL3AMT||'<');
 If rec_t1.LOANDTLSTRTBAL3AMT is null then 
 rec_t1.LOANDTLSTRTBAL3AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTRTBAL3AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTRTBAL3AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTRTBAL4AMT datatype NUMBER'||rec_t1.LOANDTLSTRTBAL4AMT||'<');
 If rec_t1.LOANDTLSTRTBAL4AMT is null then 
 rec_t1.LOANDTLSTRTBAL4AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTRTBAL4AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTRTBAL4AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTRTBAL5AMT datatype NUMBER'||rec_t1.LOANDTLSTRTBAL5AMT||'<');
 If rec_t1.LOANDTLSTRTBAL5AMT is null then 
 rec_t1.LOANDTLSTRTBAL5AMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTRTBAL5AMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTRTBAL5AMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSBCIDNMB datatype CHAR'||rec_t1.LOANDTLSBCIDNMB||'<');
 If rec_t1.LOANDTLSBCIDNMB is null then 
 rec_t1.LOANDTLSBCIDNMB:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSBCIDNMB',
 'CHAR',5,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSBCIDNMB';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACHLASTCHNGDT datatype DATE'||rec_t1.LOANDTLACHLASTCHNGDT||'<');
 If rec_t1.LOANDTLACHLASTCHNGDT is null then 
 rec_t1.LOANDTLACHLASTCHNGDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACHLASTCHNGDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACHLASTCHNGDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPRENOTETESTDT datatype DATE'||rec_t1.LOANDTLPRENOTETESTDT||'<');
 If rec_t1.LOANDTLPRENOTETESTDT is null then 
 rec_t1.LOANDTLPRENOTETESTDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPRENOTETESTDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPRENOTETESTDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLPYMTNMBDUE datatype NUMBER'||rec_t1.LOANDTLPYMTNMBDUE||'<');
 If rec_t1.LOANDTLPYMTNMBDUE is null then 
 rec_t1.LOANDTLPYMTNMBDUE:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLPYMTNMBDUE',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLPYMTNMBDUE';
 end if;
 end if;
 end if;
 jot('checking LOANDTLRESRVAMT datatype NUMBER'||rec_t1.LOANDTLRESRVAMT||'<');
 If rec_t1.LOANDTLRESRVAMT is null then 
 rec_t1.LOANDTLRESRVAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLRESRVAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLRESRVAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLFEEBASEAMT datatype NUMBER'||rec_t1.LOANDTLFEEBASEAMT||'<');
 If rec_t1.LOANDTLFEEBASEAMT is null then 
 rec_t1.LOANDTLFEEBASEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLFEEBASEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLFEEBASEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTATCDOFLOAN datatype CHAR'||rec_t1.LOANDTLSTATCDOFLOAN||'<');
 If rec_t1.LOANDTLSTATCDOFLOAN is null then 
 rec_t1.LOANDTLSTATCDOFLOAN:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTATCDOFLOAN',
 'CHAR',2,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTATCDOFLOAN';
 end if;
 end if;
 end if;
 jot('checking LOANDTLACTUALCSAINITFEEAMT datatype NUMBER'||rec_t1.LOANDTLACTUALCSAINITFEEAMT||'<');
 If rec_t1.LOANDTLACTUALCSAINITFEEAMT is null then 
 rec_t1.LOANDTLACTUALCSAINITFEEAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLACTUALCSAINITFEEAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLACTUALCSAINITFEEAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTATDT datatype DATE'||rec_t1.LOANDTLSTATDT||'<');
 If rec_t1.LOANDTLSTATDT is null then 
 rec_t1.LOANDTLSTATDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTATDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTATDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTCLSBALAMT datatype NUMBER'||rec_t1.LOANDTLSTMTCLSBALAMT||'<');
 If rec_t1.LOANDTLSTMTCLSBALAMT is null then 
 rec_t1.LOANDTLSTMTCLSBALAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTCLSBALAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTCLSBALAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLSTMTCLSDT datatype DATE'||rec_t1.LOANDTLSTMTCLSDT||'<');
 If rec_t1.LOANDTLSTMTCLSDT is null then 
 rec_t1.LOANDTLSTMTCLSDT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLSTMTCLSDT',
 'DATE',7,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLSTMTCLSDT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLLASTDBPAYOUTAMT datatype NUMBER'||rec_t1.LOANDTLLASTDBPAYOUTAMT||'<');
 If rec_t1.LOANDTLLASTDBPAYOUTAMT is null then 
 rec_t1.LOANDTLLASTDBPAYOUTAMT:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLLASTDBPAYOUTAMT',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLLASTDBPAYOUTAMT';
 end if;
 end if;
 end if;
 jot('checking LOANDTLCHEMICALBASISDAYS datatype NUMBER'||rec_t1.LOANDTLCHEMICALBASISDAYS||'<');
 If rec_t1.LOANDTLCHEMICALBASISDAYS is null then 
 rec_t1.LOANDTLCHEMICALBASISDAYS:=stgcsa.DEFAULT_COLUMN_VALUE(
 'STGCSA','SOFVLND1TBL','LOANDTLCHEMICALBASISDAYS',
 'NUMBER',22,sucessfully_defaulted);
 if sucessfully_defaulted=false then
 errfound:=true;
 if length(errstr)<3950 then
 errstr:=errstr||', P_LOANDTLCHEMICALBASISDAYS';
 end if;
 end if;
 end if;
 -- end of nulls checking
 STGCSA.jot('after nulls check, errstr='||errstr||'<');
 --
 if errfound=true then
 ROLLBACK TO SFOVLND1INSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more inputs problems: '||errstr;
 else 
 --
 --insert into STGCSA.SOFVLND1TBL values rec_t1;
 p_RETVAL := SQL%ROWCOUNT;
 STGCSA.jot('Woulda inserted: errstr='||errstr||'<');
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SFOVLND1INSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 STGCSA.jot('In SFOVLND1INSTSP OXH, '||sqlerrm(SQLCODE));
 p_errval:=SQLCODE;
 p_errmsg:='In SFOVLND1INSTSP OXH, '||sqlerrm(SQLCODE); 
 ROLLBACK TO SFOVLND1INSTSP;
 --
 END SFOVLND1INSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


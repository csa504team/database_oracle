<!--- Saved 01/28/2018 14:41:27. --->
PROCEDURE ACCACCELERATELOANUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCELERATECMNT VARCHAR2:=null
 ,p_ACCELERATNID NUMBER:=null
 ,p_CDCNMB CHAR:=null
 ,p_LOANNMB CHAR:=null
 ,p_MFCDCDUEFROMBORRAMT NUMBER:=null
 ,p_MFCDCFEEAMT NUMBER:=null
 ,p_MFCDCMODELNQ NUMBER:=null
 ,p_MFCDCMOFEEAMT NUMBER:=null
 ,p_MFCDCPAIDTHRUDT CHAR:=null
 ,p_MFCSADUEFROMBORRAMT NUMBER:=null
 ,p_MFCSAFEEAMT NUMBER:=null
 ,p_MFCSAMODELNQ NUMBER:=null
 ,p_MFCSAMOFEEAMT NUMBER:=null
 ,p_MFCSAPAIDTHRUDT CHAR:=null
 ,p_MFCURCMNT VARCHAR2:=null
 ,p_MFCURSTATID NUMBER:=null
 ,p_MFDBENTRAMT NUMBER:=null
 ,p_MFESCROWPRIORPYMTAMT NUMBER:=null
 ,p_MFINITIALCMNT VARCHAR2:=null
 ,p_MFINITIALSCRAPEDT DATE:=null
 ,p_MFINITIALSTATID NUMBER:=null
 ,p_MFINTRTPCT NUMBER:=null
 ,p_MFISSDT DATE:=null
 ,p_MFLASTPYMTDT DATE:=null
 ,p_MFLASTSCRAPEDT DATE:=null
 ,p_MFLASTUPDTDT DATE:=null
 ,p_MFLOANMATDT DATE:=null
 ,p_MFORGLPRINAMT NUMBER:=null
 ,p_MFPREVCMNT VARCHAR2:=null
 ,p_MFPREVSTATID NUMBER:=null
 ,p_MFTOBECURAMT NUMBER:=null
 ,p_PPACDCNMB CHAR:=null
 ,p_PPADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_PPADUEATSEMIANAMT NUMBER:=null
 ,p_PPAENTRYDT DATE:=null
 ,p_PPAGROUP VARCHAR2:=null
 ,p_PPAINTDUEATSEMIANAMT NUMBER:=null
 ,p_PPAORGLPRINAMT NUMBER:=null
 ,p_PRIORACCELERATN NUMBER:=null
 ,p_SBAACCELAMT NUMBER:=null
 ,p_SBAACCRINTDUEAMT NUMBER:=null
 ,p_SBACDCFEEAMT NUMBER:=null
 ,p_SBACSAFEEAMT NUMBER:=null
 ,p_SBADBENTRBALATSEMIANAMT NUMBER:=null
 ,p_SBADUEDT DATE:=null
 ,p_SBAENTRYDT DATE:=null
 ,p_SBAESCROWPRIORPYMTAMT NUMBER:=null
 ,p_SBAEXCESSAMT NUMBER:=null
 ,p_SBAINTDUEATSEMIANAMT NUMBER:=null
 ,p_SBALASTNOTEBALAMT NUMBER:=null
 ,p_SBALASTPYMTDT DATE:=null
 ,p_SBALASTTRNSCRPTIMPTDT DATE:=null
 ,p_SBANOTERTPCT NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ,p_SERVCNTRID VARCHAR2:=null
 ,p_STATID NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 16:52:03
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 16:52:03
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure ACCACCELERATELOANUPDTSP performs UPDATE on STGCSA.ACCACCELERATELOANTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ACCELERATNID
 for P_IDENTIFIER=1 qualification is on SEMIANDT, STATID
 for P_IDENTIFIER=2 qualification is on STATID =49, SEMIANDT
 for P_IDENTIFIER=3 qualification is on STATID =50, SEMIANDT
 for P_IDENTIFIER=4 qualification is on TRANSID
 for P_IDENTIFIER=5 qualification is on LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 new_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.ACCACCELERATELOANTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Select of STGCSA.ACCACCELERATELOANTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_ACCELERATNID is not null then
 cur_col_name:='P_ACCELERATNID';
 new_rec.ACCELERATNID:=P_ACCELERATNID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_PRIORACCELERATN is not null then
 cur_col_name:='P_PRIORACCELERATN';
 new_rec.PRIORACCELERATN:=P_PRIORACCELERATN; 
 end if;
 if P_STATID is not null then
 cur_col_name:='P_STATID';
 new_rec.STATID:=P_STATID; 
 end if;
 if P_ACCELERATECMNT is not null then
 cur_col_name:='P_ACCELERATECMNT';
 new_rec.ACCELERATECMNT:=P_ACCELERATECMNT; 
 end if;
 if P_SEMIANDT is not null then
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_MFINITIALSCRAPEDT is not null then
 cur_col_name:='P_MFINITIALSCRAPEDT';
 new_rec.MFINITIALSCRAPEDT:=P_MFINITIALSCRAPEDT; 
 end if;
 if P_MFLASTSCRAPEDT is not null then
 cur_col_name:='P_MFLASTSCRAPEDT';
 new_rec.MFLASTSCRAPEDT:=P_MFLASTSCRAPEDT; 
 end if;
 if P_MFCURSTATID is not null then
 cur_col_name:='P_MFCURSTATID';
 new_rec.MFCURSTATID:=P_MFCURSTATID; 
 end if;
 if P_MFCURCMNT is not null then
 cur_col_name:='P_MFCURCMNT';
 new_rec.MFCURCMNT:=P_MFCURCMNT; 
 end if;
 if P_MFINITIALSTATID is not null then
 cur_col_name:='P_MFINITIALSTATID';
 new_rec.MFINITIALSTATID:=P_MFINITIALSTATID; 
 end if;
 if P_MFINITIALCMNT is not null then
 cur_col_name:='P_MFINITIALCMNT';
 new_rec.MFINITIALCMNT:=P_MFINITIALCMNT; 
 end if;
 if P_MFLASTUPDTDT is not null then
 cur_col_name:='P_MFLASTUPDTDT';
 new_rec.MFLASTUPDTDT:=P_MFLASTUPDTDT; 
 end if;
 if P_MFISSDT is not null then
 cur_col_name:='P_MFISSDT';
 new_rec.MFISSDT:=P_MFISSDT; 
 end if;
 if P_MFLOANMATDT is not null then
 cur_col_name:='P_MFLOANMATDT';
 new_rec.MFLOANMATDT:=P_MFLOANMATDT; 
 end if;
 if P_MFORGLPRINAMT is not null then
 cur_col_name:='P_MFORGLPRINAMT';
 new_rec.MFORGLPRINAMT:=P_MFORGLPRINAMT; 
 end if;
 if P_MFINTRTPCT is not null then
 cur_col_name:='P_MFINTRTPCT';
 new_rec.MFINTRTPCT:=P_MFINTRTPCT; 
 end if;
 if P_MFDBENTRAMT is not null then
 cur_col_name:='P_MFDBENTRAMT';
 new_rec.MFDBENTRAMT:=P_MFDBENTRAMT; 
 end if;
 if P_MFTOBECURAMT is not null then
 cur_col_name:='P_MFTOBECURAMT';
 new_rec.MFTOBECURAMT:=P_MFTOBECURAMT; 
 end if;
 if P_MFLASTPYMTDT is not null then
 cur_col_name:='P_MFLASTPYMTDT';
 new_rec.MFLASTPYMTDT:=P_MFLASTPYMTDT; 
 end if;
 if P_MFESCROWPRIORPYMTAMT is not null then
 cur_col_name:='P_MFESCROWPRIORPYMTAMT';
 new_rec.MFESCROWPRIORPYMTAMT:=P_MFESCROWPRIORPYMTAMT; 
 end if;
 if P_MFCDCPAIDTHRUDT is not null then
 cur_col_name:='P_MFCDCPAIDTHRUDT';
 new_rec.MFCDCPAIDTHRUDT:=P_MFCDCPAIDTHRUDT; 
 end if;
 if P_MFCDCMODELNQ is not null then
 cur_col_name:='P_MFCDCMODELNQ';
 new_rec.MFCDCMODELNQ:=P_MFCDCMODELNQ; 
 end if;
 if P_MFCDCMOFEEAMT is not null then
 cur_col_name:='P_MFCDCMOFEEAMT';
 new_rec.MFCDCMOFEEAMT:=P_MFCDCMOFEEAMT; 
 end if;
 if P_MFCDCFEEAMT is not null then
 cur_col_name:='P_MFCDCFEEAMT';
 new_rec.MFCDCFEEAMT:=P_MFCDCFEEAMT; 
 end if;
 if P_MFCDCDUEFROMBORRAMT is not null then
 cur_col_name:='P_MFCDCDUEFROMBORRAMT';
 new_rec.MFCDCDUEFROMBORRAMT:=P_MFCDCDUEFROMBORRAMT; 
 end if;
 if P_MFCSAPAIDTHRUDT is not null then
 cur_col_name:='P_MFCSAPAIDTHRUDT';
 new_rec.MFCSAPAIDTHRUDT:=P_MFCSAPAIDTHRUDT; 
 end if;
 if P_MFCSAMODELNQ is not null then
 cur_col_name:='P_MFCSAMODELNQ';
 new_rec.MFCSAMODELNQ:=P_MFCSAMODELNQ; 
 end if;
 if P_MFCSAMOFEEAMT is not null then
 cur_col_name:='P_MFCSAMOFEEAMT';
 new_rec.MFCSAMOFEEAMT:=P_MFCSAMOFEEAMT; 
 end if;
 if P_MFCSAFEEAMT is not null then
 cur_col_name:='P_MFCSAFEEAMT';
 new_rec.MFCSAFEEAMT:=P_MFCSAFEEAMT; 
 end if;
 if P_MFCSADUEFROMBORRAMT is not null then
 cur_col_name:='P_MFCSADUEFROMBORRAMT';
 new_rec.MFCSADUEFROMBORRAMT:=P_MFCSADUEFROMBORRAMT; 
 end if;
 if P_PPAENTRYDT is not null then
 cur_col_name:='P_PPAENTRYDT';
 new_rec.PPAENTRYDT:=P_PPAENTRYDT; 
 end if;
 if P_PPAGROUP is not null then
 cur_col_name:='P_PPAGROUP';
 new_rec.PPAGROUP:=P_PPAGROUP; 
 end if;
 if P_PPACDCNMB is not null then
 cur_col_name:='P_PPACDCNMB';
 new_rec.PPACDCNMB:=P_PPACDCNMB; 
 end if;
 if P_PPAORGLPRINAMT is not null then
 cur_col_name:='P_PPAORGLPRINAMT';
 new_rec.PPAORGLPRINAMT:=P_PPAORGLPRINAMT; 
 end if;
 if P_PPADBENTRBALATSEMIANAMT is not null then
 cur_col_name:='P_PPADBENTRBALATSEMIANAMT';
 new_rec.PPADBENTRBALATSEMIANAMT:=P_PPADBENTRBALATSEMIANAMT; 
 end if;
 if P_PPAINTDUEATSEMIANAMT is not null then
 cur_col_name:='P_PPAINTDUEATSEMIANAMT';
 new_rec.PPAINTDUEATSEMIANAMT:=P_PPAINTDUEATSEMIANAMT; 
 end if;
 if P_PPADUEATSEMIANAMT is not null then
 cur_col_name:='P_PPADUEATSEMIANAMT';
 new_rec.PPADUEATSEMIANAMT:=P_PPADUEATSEMIANAMT; 
 end if;
 if P_SBAENTRYDT is not null then
 cur_col_name:='P_SBAENTRYDT';
 new_rec.SBAENTRYDT:=P_SBAENTRYDT; 
 end if;
 if P_SBALASTTRNSCRPTIMPTDT is not null then
 cur_col_name:='P_SBALASTTRNSCRPTIMPTDT';
 new_rec.SBALASTTRNSCRPTIMPTDT:=P_SBALASTTRNSCRPTIMPTDT; 
 end if;
 if P_SBADBENTRBALATSEMIANAMT is not null then
 cur_col_name:='P_SBADBENTRBALATSEMIANAMT';
 new_rec.SBADBENTRBALATSEMIANAMT:=P_SBADBENTRBALATSEMIANAMT; 
 end if;
 if P_SBAINTDUEATSEMIANAMT is not null then
 cur_col_name:='P_SBAINTDUEATSEMIANAMT';
 new_rec.SBAINTDUEATSEMIANAMT:=P_SBAINTDUEATSEMIANAMT; 
 end if;
 if P_SBACSAFEEAMT is not null then
 cur_col_name:='P_SBACSAFEEAMT';
 new_rec.SBACSAFEEAMT:=P_SBACSAFEEAMT; 
 end if;
 if P_SBACDCFEEAMT is not null then
 cur_col_name:='P_SBACDCFEEAMT';
 new_rec.SBACDCFEEAMT:=P_SBACDCFEEAMT; 
 end if;
 if P_SBAESCROWPRIORPYMTAMT is not null then
 cur_col_name:='P_SBAESCROWPRIORPYMTAMT';
 new_rec.SBAESCROWPRIORPYMTAMT:=P_SBAESCROWPRIORPYMTAMT; 
 end if;
 if P_SBAACCELAMT is not null then
 cur_col_name:='P_SBAACCELAMT';
 new_rec.SBAACCELAMT:=P_SBAACCELAMT; 
 end if;
 if P_SBADUEDT is not null then
 cur_col_name:='P_SBADUEDT';
 new_rec.SBADUEDT:=P_SBADUEDT; 
 end if;
 if P_SBANOTERTPCT is not null then
 cur_col_name:='P_SBANOTERTPCT';
 new_rec.SBANOTERTPCT:=P_SBANOTERTPCT; 
 end if;
 if P_SBALASTNOTEBALAMT is not null then
 cur_col_name:='P_SBALASTNOTEBALAMT';
 new_rec.SBALASTNOTEBALAMT:=P_SBALASTNOTEBALAMT; 
 end if;
 if P_SBALASTPYMTDT is not null then
 cur_col_name:='P_SBALASTPYMTDT';
 new_rec.SBALASTPYMTDT:=P_SBALASTPYMTDT; 
 end if;
 if P_SBAACCRINTDUEAMT is not null then
 cur_col_name:='P_SBAACCRINTDUEAMT';
 new_rec.SBAACCRINTDUEAMT:=P_SBAACCRINTDUEAMT; 
 end if;
 if P_SBAEXCESSAMT is not null then
 cur_col_name:='P_SBAEXCESSAMT';
 new_rec.SBAEXCESSAMT:=P_SBAEXCESSAMT; 
 end if;
 if P_MFPREVSTATID is not null then
 cur_col_name:='P_MFPREVSTATID';
 new_rec.MFPREVSTATID:=P_MFPREVSTATID; 
 end if;
 if P_MFPREVCMNT is not null then
 cur_col_name:='P_MFPREVCMNT';
 new_rec.MFPREVCMNT:=P_MFPREVCMNT; 
 end if;
 if P_CDCNMB is not null then
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=P_CDCNMB; 
 end if;
 if P_SERVCNTRID is not null then
 cur_col_name:='P_SERVCNTRID';
 new_rec.SERVCNTRID:=P_SERVCNTRID; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.ACCACCELERATELOANAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.ACCACCELERATELOANTBL
 ,TRANSID=new_rec.TRANSID
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,PRIORACCELERATN=new_rec.PRIORACCELERATN
 ,STATID=new_rec.STATID
 ,ACCELERATECMNT=new_rec.ACCELERATECMNT
 ,SEMIANDT=new_rec.SEMIANDT
 ,MFINITIALSCRAPEDT=new_rec.MFINITIALSCRAPEDT
 ,MFLASTSCRAPEDT=new_rec.MFLASTSCRAPEDT
 ,MFCURSTATID=new_rec.MFCURSTATID
 ,MFCURCMNT=new_rec.MFCURCMNT
 ,MFINITIALSTATID=new_rec.MFINITIALSTATID
 ,MFINITIALCMNT=new_rec.MFINITIALCMNT
 ,MFLASTUPDTDT=new_rec.MFLASTUPDTDT
 ,MFISSDT=new_rec.MFISSDT
 ,MFLOANMATDT=new_rec.MFLOANMATDT
 ,MFORGLPRINAMT=new_rec.MFORGLPRINAMT
 ,MFINTRTPCT=new_rec.MFINTRTPCT
 ,MFDBENTRAMT=new_rec.MFDBENTRAMT
 ,MFTOBECURAMT=new_rec.MFTOBECURAMT
 ,MFLASTPYMTDT=new_rec.MFLASTPYMTDT
 ,MFESCROWPRIORPYMTAMT=new_rec.MFESCROWPRIORPYMTAMT
 ,MFCDCPAIDTHRUDT=new_rec.MFCDCPAIDTHRUDT
 ,MFCDCMODELNQ=new_rec.MFCDCMODELNQ
 ,MFCDCMOFEEAMT=new_rec.MFCDCMOFEEAMT
 ,MFCDCFEEAMT=new_rec.MFCDCFEEAMT
 ,MFCDCDUEFROMBORRAMT=new_rec.MFCDCDUEFROMBORRAMT
 ,MFCSAPAIDTHRUDT=new_rec.MFCSAPAIDTHRUDT
 ,MFCSAMODELNQ=new_rec.MFCSAMODELNQ
 ,MFCSAMOFEEAMT=new_rec.MFCSAMOFEEAMT
 ,MFCSAFEEAMT=new_rec.MFCSAFEEAMT
 ,MFCSADUEFROMBORRAMT=new_rec.MFCSADUEFROMBORRAMT
 ,PPAENTRYDT=new_rec.PPAENTRYDT
 ,PPAGROUP=new_rec.PPAGROUP
 ,PPACDCNMB=new_rec.PPACDCNMB
 ,PPAORGLPRINAMT=new_rec.PPAORGLPRINAMT
 ,PPADBENTRBALATSEMIANAMT=new_rec.PPADBENTRBALATSEMIANAMT
 ,PPAINTDUEATSEMIANAMT=new_rec.PPAINTDUEATSEMIANAMT
 ,PPADUEATSEMIANAMT=new_rec.PPADUEATSEMIANAMT
 ,SBAENTRYDT=new_rec.SBAENTRYDT
 ,SBALASTTRNSCRPTIMPTDT=new_rec.SBALASTTRNSCRPTIMPTDT
 ,SBADBENTRBALATSEMIANAMT=new_rec.SBADBENTRBALATSEMIANAMT
 ,SBAINTDUEATSEMIANAMT=new_rec.SBAINTDUEATSEMIANAMT
 ,SBACSAFEEAMT=new_rec.SBACSAFEEAMT
 ,SBACDCFEEAMT=new_rec.SBACDCFEEAMT
 ,SBAESCROWPRIORPYMTAMT=new_rec.SBAESCROWPRIORPYMTAMT
 ,SBAACCELAMT=new_rec.SBAACCELAMT
 ,SBADUEDT=new_rec.SBADUEDT
 ,SBANOTERTPCT=new_rec.SBANOTERTPCT
 ,SBALASTNOTEBALAMT=new_rec.SBALASTNOTEBALAMT
 ,SBALASTPYMTDT=new_rec.SBALASTPYMTDT
 ,SBAACCRINTDUEAMT=new_rec.SBAACCRINTDUEAMT
 ,SBAEXCESSAMT=new_rec.SBAEXCESSAMT
 ,MFPREVSTATID=new_rec.MFPREVSTATID
 ,MFPREVCMNT=new_rec.MFPREVCMNT
 ,CDCNMB=new_rec.CDCNMB
 ,SERVCNTRID=new_rec.SERVCNTRID
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'while doing update on STGCSA.ACCACCELERATELOANTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCACCELERATELOANUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TRANSID:=P_TRANSID; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(ACCELERATNID)=('||P_ACCELERATNID||') ';
 select rowid into rec_rowid from STGCSA.ACCACCELERATELOANTBL where 1=1
 and ACCELERATNID=P_ACCELERATNID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID)=('||P_STATID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 and STATID=P_STATID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 p_retval:=0;
 exception when others then
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID =49) '||'';
 begin
 for r2 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =49
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to update one or more rows based on keyset KEYS3
 if p_errval=0 then
 
 
 keystouse:='(SEMIANDT)=('||P_SEMIANDT||') '||
 '(STATID =50) '||'';
 begin
 for r3 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =50
 ) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 4 then 
 -- case to update one or more rows based on keyset KEYS4
 if p_errval=0 then
 
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r4 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r4.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 5 then 
 -- case to update one or more rows based on keyset KEYS5
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r5 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r5.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'Oracle returned error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB; log_TRANSID:=new_rec.TRANSID; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCACCELERATELOANUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'ACCACCELERATELOANUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In ACCACCELERATELOANUPDTSP build 2018-01-23 16:52:03'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO ACCACCELERATELOANUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END ACCACCELERATELOANUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:37. --->
PROCEDURE SOFVFTPODELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_PREPOSTRVWADDENDRECIND NUMBER:=null
 ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
 ,p_PREPOSTRVWCMNT VARCHAR2:=null
 ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
 ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
 ,p_PREPOSTRVWPYMTAMT NUMBER:=null
 ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
 ,p_PREPOSTRVWTRACENMB NUMBER:=null
 ,p_PREPOSTRVWTRANSCD CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTCHKDGT CHAR:=null
 ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
 ) as
 /*
 Created on: 2018-01-23 08:32:44
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:32:44
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVFTPODELTSP performs DELETE on STGCSA.SOFVFTPOTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVFTPOTBL%rowtype;
 new_rec STGCSA.SOFVFTPOTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVFTPOTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVFTPODELTSP build 2018-01-23 08:32:44 '
 ||'Select of STGCSA.SOFVFTPOTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVFTPODELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVFTPOAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVFTPOTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVFTPODELTSP build 2018-01-23 08:32:44 '
 ||'while doing delete on STGCSA.SOFVFTPOTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVFTPODELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVFTPODELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVFTPOTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ';
 select rowid into rec_rowid from STGCSA.SOFVFTPOTBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVFTPODELTSP build 2018-01-23 08:32:44 '
 ||'No STGCSA.SOFVFTPOTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVFTPODELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVFTPODELTSP build 2018-01-23 08:32:44 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVFTPODELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVFTPODELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVFTPODELTSP build 2018-01-23 08:32:44'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVFTPODELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVFTPODELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


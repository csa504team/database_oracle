<!--- Saved 01/28/2018 14:42:23. --->
PROCEDURE PYMT110RPTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCAMT NUMBER:=null
 ,p_CDCRAMT NUMBER:=null
 ,p_CSAAMT NUMBER:=null
 ,p_CSARAMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_LFAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_POSTAMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_PYMT110ID NUMBER:=null
 ,p_SBAAMT NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-27 15:31:24
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-27 15:31:24
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PYMT110RPTUPDTSP performs UPDATE on STGCSA.PYMT110RPTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PYMT110ID
 for P_IDENTIFIER=2 qualification is on PYMT110ID =0 or PYMT110ID<>0
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PYMT110RPTTBL%rowtype;
 new_rec STGCSA.PYMT110RPTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PYMT110RPTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'Select of STGCSA.PYMT110RPTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PYMT110RPTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_PYMT110ID is not null then
 cur_col_name:='P_PYMT110ID';
 new_rec.PYMT110ID:=P_PYMT110ID; 
 end if;
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_POSTAMT is not null then
 cur_col_name:='P_POSTAMT';
 new_rec.POSTAMT:=P_POSTAMT; 
 end if;
 if P_PRINAMT is not null then
 cur_col_name:='P_PRINAMT';
 new_rec.PRINAMT:=P_PRINAMT; 
 end if;
 if P_INTAMT is not null then
 cur_col_name:='P_INTAMT';
 new_rec.INTAMT:=P_INTAMT; 
 end if;
 if P_UNALLOCAMT is not null then
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_CSAAMT is not null then
 cur_col_name:='P_CSAAMT';
 new_rec.CSAAMT:=P_CSAAMT; 
 end if;
 if P_CDCAMT is not null then
 cur_col_name:='P_CDCAMT';
 new_rec.CDCAMT:=P_CDCAMT; 
 end if;
 if P_CSARAMT is not null then
 cur_col_name:='P_CSARAMT';
 new_rec.CSARAMT:=P_CSARAMT; 
 end if;
 if P_CDCRAMT is not null then
 cur_col_name:='P_CDCRAMT';
 new_rec.CDCRAMT:=P_CDCRAMT; 
 end if;
 if P_LFAMT is not null then
 cur_col_name:='P_LFAMT';
 new_rec.LFAMT:=P_LFAMT; 
 end if;
 if P_SBAAMT is not null then
 cur_col_name:='P_SBAAMT';
 new_rec.SBAAMT:=P_SBAAMT; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PYMT110RPTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PYMT110RPTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,TRANSID=new_rec.TRANSID
 ,LOANNMB=new_rec.LOANNMB
 ,POSTAMT=new_rec.POSTAMT
 ,PRINAMT=new_rec.PRINAMT
 ,INTAMT=new_rec.INTAMT
 ,UNALLOCAMT=new_rec.UNALLOCAMT
 ,CSAAMT=new_rec.CSAAMT
 ,CDCAMT=new_rec.CDCAMT
 ,CSARAMT=new_rec.CSARAMT
 ,CDCRAMT=new_rec.CDCRAMT
 ,LFAMT=new_rec.LFAMT
 ,SBAAMT=new_rec.SBAAMT
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'while doing update on STGCSA.PYMT110RPTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PYMT110RPTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PYMT110RPTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TRANSID:=P_TRANSID; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PYMT110ID)=('||P_PYMT110ID||') ';
 select rowid into rec_rowid from STGCSA.PYMT110RPTTBL where 1=1
 and PYMT110ID=P_PYMT110ID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'No STGCSA.PYMT110RPTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(PYMT110ID =0 or PYMT110ID<>0) '||'';
 begin
 for r2 in (select rowid from STGCSA.PYMT110RPTTBL a where 1=1 
 
 and PYMT110ID =0 or PYMT110ID<>0
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO PYMT110RPTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'Oracle returned error fetching STGCSA.PYMT110RPTTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO PYMT110RPTUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'No STGCSA.PYMT110RPTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PYMT110RPTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TRANSID:=new_rec.TRANSID; log_LOANNMB:=new_rec.LOANNMB; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PYMT110RPTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PYMT110RPTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PYMT110RPTUPDTSP build 2018-01-27 15:31:24'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PYMT110RPTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PYMT110RPTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


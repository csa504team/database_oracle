<!--- Saved 01/28/2018 14:41:34. --->
PROCEDURE CHRTOPNGNTYSALLINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CHRTOPNGNTYSALLID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_PREPAYDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_TOTOPENGNTYAMT NUMBER:=null
 ,p_ERROPENGNTYAMT NUMBER:=null
 ,p_REMAINDEROPENGNTYAMT NUMBER:=null
 ,p_OPENGNTYDIFFAMT NUMBER:=null
 ,p_PROOFAMT NUMBER:=null
 ,p_OPNGNTYSCMNT VARCHAR2:=null
 ,p_OPENGNTYCLSDT DATE:=null
 ,p_OPENGNTYPAIDDT DATE:=null
 ,p_TOBECLSFLAG NUMBER:=null
 ,p_TOBEPAIDFLAG NUMBER:=null
 ,p_GNTYAMT NUMBER:=null
 ,p_OPENGNTYVALIDATION NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_UPDTUNALLOCPORTOFGUARANTEAMT NUMBER:=null
 ,p_ACCRINTONDELNQLOANAMT NUMBER:=null
 ,p_UPDTACCRINTDELNQLOANAMT NUMBER:=null
 ,p_ACCRINTPAIDDT DATE:=null
 ,p_REPORTDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:17:14
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:17:14
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CHRTOPNGNTYSALLTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.CHRTOPNGNTYSALLTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint CHRTOPNGNTYSALLINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CHRTOPNGNTYSALLID';
 new_rec.CHRTOPNGNTYSALLID:=runtime.validate_column_value(P_CHRTOPNGNTYSALLID,
 'STGCSA','CHRTOPNGNTYSALLTBL','CHRTOPNGNTYSALLID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','CHRTOPNGNTYSALLTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=runtime.validate_column_value(P_PREPAYDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','PREPAYDT','DATE',7);
 cur_col_name:='P_SEMIANDT';
 new_rec.SEMIANDT:=runtime.validate_column_value(P_SEMIANDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','SEMIANDT','DATE',7);
 cur_col_name:='P_TOTOPENGNTYAMT';
 new_rec.TOTOPENGNTYAMT:=runtime.validate_column_value(P_TOTOPENGNTYAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','TOTOPENGNTYAMT','NUMBER',22);
 cur_col_name:='P_ERROPENGNTYAMT';
 new_rec.ERROPENGNTYAMT:=runtime.validate_column_value(P_ERROPENGNTYAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','ERROPENGNTYAMT','NUMBER',22);
 cur_col_name:='P_REMAINDEROPENGNTYAMT';
 new_rec.REMAINDEROPENGNTYAMT:=runtime.validate_column_value(P_REMAINDEROPENGNTYAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','REMAINDEROPENGNTYAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYDIFFAMT';
 new_rec.OPENGNTYDIFFAMT:=runtime.validate_column_value(P_OPENGNTYDIFFAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','OPENGNTYDIFFAMT','NUMBER',22);
 cur_col_name:='P_PROOFAMT';
 new_rec.PROOFAMT:=runtime.validate_column_value(P_PROOFAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','PROOFAMT','NUMBER',22);
 cur_col_name:='P_OPNGNTYSCMNT';
 new_rec.OPNGNTYSCMNT:=runtime.validate_column_value(P_OPNGNTYSCMNT,
 'STGCSA','CHRTOPNGNTYSALLTBL','OPNGNTYSCMNT','VARCHAR2',255);
 cur_col_name:='P_OPENGNTYCLSDT';
 new_rec.OPENGNTYCLSDT:=runtime.validate_column_value(P_OPENGNTYCLSDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','OPENGNTYCLSDT','DATE',7);
 cur_col_name:='P_OPENGNTYPAIDDT';
 new_rec.OPENGNTYPAIDDT:=runtime.validate_column_value(P_OPENGNTYPAIDDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','OPENGNTYPAIDDT','DATE',7);
 cur_col_name:='P_TOBECLSFLAG';
 new_rec.TOBECLSFLAG:=runtime.validate_column_value(P_TOBECLSFLAG,
 'STGCSA','CHRTOPNGNTYSALLTBL','TOBECLSFLAG','NUMBER',22);
 cur_col_name:='P_TOBEPAIDFLAG';
 new_rec.TOBEPAIDFLAG:=runtime.validate_column_value(P_TOBEPAIDFLAG,
 'STGCSA','CHRTOPNGNTYSALLTBL','TOBEPAIDFLAG','NUMBER',22);
 cur_col_name:='P_GNTYAMT';
 new_rec.GNTYAMT:=runtime.validate_column_value(P_GNTYAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','GNTYAMT','NUMBER',22);
 cur_col_name:='P_OPENGNTYVALIDATION';
 new_rec.OPENGNTYVALIDATION:=runtime.validate_column_value(P_OPENGNTYVALIDATION,
 'STGCSA','CHRTOPNGNTYSALLTBL','OPENGNTYVALIDATION','NUMBER',22);
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=runtime.validate_column_value(P_UNALLOCAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','UNALLOCAMT','NUMBER',22);
 cur_col_name:='P_UPDTUNALLOCPORTOFGUARANTEAMT';
 new_rec.UPDTUNALLOCPORTOFGUARANTEAMT:=runtime.validate_column_value(P_UPDTUNALLOCPORTOFGUARANTEAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','UPDTUNALLOCPORTOFGUARANTEAMT','NUMBER',22);
 cur_col_name:='P_ACCRINTONDELNQLOANAMT';
 new_rec.ACCRINTONDELNQLOANAMT:=runtime.validate_column_value(P_ACCRINTONDELNQLOANAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','ACCRINTONDELNQLOANAMT','NUMBER',22);
 cur_col_name:='P_UPDTACCRINTDELNQLOANAMT';
 new_rec.UPDTACCRINTDELNQLOANAMT:=runtime.validate_column_value(P_UPDTACCRINTDELNQLOANAMT,
 'STGCSA','CHRTOPNGNTYSALLTBL','UPDTACCRINTDELNQLOANAMT','NUMBER',22);
 cur_col_name:='P_ACCRINTPAIDDT';
 new_rec.ACCRINTPAIDDT:=runtime.validate_column_value(P_ACCRINTPAIDDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','ACCRINTPAIDDT','DATE',7);
 cur_col_name:='P_REPORTDT';
 new_rec.REPORTDT:=runtime.validate_column_value(P_REPORTDT,
 'STGCSA','CHRTOPNGNTYSALLTBL','REPORTDT','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.CHRTOPNGNTYSALLAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.CHRTOPNGNTYSALLTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 p_errmsg:='Got dupe key on insert into CHRTOPNGNTYSALLTBL for key '
 ||'(CHRTOPNGNTYSALLID)=('||new_rec.CHRTOPNGNTYSALLID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in CHRTOPNGNTYSALLINSTSP build 2018-01-23 08:17:14';
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CHRTOPNGNTYSALLINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CHRTOPNGNTYSALLINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in CHRTOPNGNTYSALLINSTSP build 2018-01-23 08:17:14'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END CHRTOPNGNTYSALLINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 12/31/2017 09:00:18. --->
PROCEDURE COREACTVTYLOGDelTsp(
 p_identifier number:=0,
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 P_LOGENTRYID NUMBER:=NULL)
 as
 /*
 <!--- generated 2017-09-28 14:03:30 by SYS --->
 Created on: 2017-09-28 14:03:30
 Created by: SYS
 Crerated from genr, stdtbldel_template v 1.0
 Purpose : standard delete by key for table STGCSA.COREACTVTYLOGTBL
 */
 BEGIN 
 savepoint COREACTVTYLOGDelTsp;
 p_errval:=0;
 p_errmsg:=null; 
 if p_identifier=0 then
 delete from STGCSA.COREACTVTYLOGTBL where (
 LOGENTRYID)=
 (select 
 P_LOGENTRYID
 from dual); 
 p_RETVAL := SQL%ROWCOUNT;
 else /* P_IDENTIFIER was unexpected value */
 raise_application_error(-20001,'Unexpected P_IDENTIFIER value: '||p_identifier);
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=SQLERRM(SQLCODE); 
 ROLLBACK TO COREACTVTYLOGDelTsp;
 RAISE;
 /* 
 <cfoutput>
 Nothing to see here! Move along!
 </cfoutput>
 */
 END COREACTVTYLOGDelTsp; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


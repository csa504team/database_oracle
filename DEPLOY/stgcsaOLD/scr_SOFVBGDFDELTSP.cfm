<!--- Saved 01/28/2018 14:42:29. --->
PROCEDURE SOFVBGDFDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DEFRMNTDTCREAT DATE:=null
 ,p_DEFRMNTDTMOD DATE:=null
 ,p_DEFRMNTENDDT1 DATE:=null
 ,p_DEFRMNTENDDT10 DATE:=null
 ,p_DEFRMNTENDDT11 DATE:=null
 ,p_DEFRMNTENDDT12 DATE:=null
 ,p_DEFRMNTENDDT13 DATE:=null
 ,p_DEFRMNTENDDT14 DATE:=null
 ,p_DEFRMNTENDDT15 DATE:=null
 ,p_DEFRMNTENDDT2 DATE:=null
 ,p_DEFRMNTENDDT3 DATE:=null
 ,p_DEFRMNTENDDT4 DATE:=null
 ,p_DEFRMNTENDDT5 DATE:=null
 ,p_DEFRMNTENDDT6 DATE:=null
 ,p_DEFRMNTENDDT7 DATE:=null
 ,p_DEFRMNTENDDT8 DATE:=null
 ,p_DEFRMNTENDDT9 DATE:=null
 ,p_DEFRMNTGPKEY CHAR:=null
 ,p_DEFRMNTREQ10AMT NUMBER:=null
 ,p_DEFRMNTREQ11AMT NUMBER:=null
 ,p_DEFRMNTREQ12AMT NUMBER:=null
 ,p_DEFRMNTREQ13AMT NUMBER:=null
 ,p_DEFRMNTREQ14AMT NUMBER:=null
 ,p_DEFRMNTREQ15AMT NUMBER:=null
 ,p_DEFRMNTREQ1AMT NUMBER:=null
 ,p_DEFRMNTREQ2AMT NUMBER:=null
 ,p_DEFRMNTREQ3AMT NUMBER:=null
 ,p_DEFRMNTREQ4AMT NUMBER:=null
 ,p_DEFRMNTREQ5AMT NUMBER:=null
 ,p_DEFRMNTREQ6AMT NUMBER:=null
 ,p_DEFRMNTREQ7AMT NUMBER:=null
 ,p_DEFRMNTREQ8AMT NUMBER:=null
 ,p_DEFRMNTREQ9AMT NUMBER:=null
 ,p_DEFRMNTSTRTDT1 DATE:=null
 ,p_DEFRMNTSTRTDT10 DATE:=null
 ,p_DEFRMNTSTRTDT11 DATE:=null
 ,p_DEFRMNTSTRTDT12 DATE:=null
 ,p_DEFRMNTSTRTDT13 DATE:=null
 ,p_DEFRMNTSTRTDT14 DATE:=null
 ,p_DEFRMNTSTRTDT15 DATE:=null
 ,p_DEFRMNTSTRTDT2 DATE:=null
 ,p_DEFRMNTSTRTDT3 DATE:=null
 ,p_DEFRMNTSTRTDT4 DATE:=null
 ,p_DEFRMNTSTRTDT5 DATE:=null
 ,p_DEFRMNTSTRTDT6 DATE:=null
 ,p_DEFRMNTSTRTDT7 DATE:=null
 ,p_DEFRMNTSTRTDT8 DATE:=null
 ,p_DEFRMNTSTRTDT9 DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:30:21
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:30:21
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure SOFVBGDFDELTSP performs DELETE on STGCSA.SOFVBGDFTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: DEFRMNTGPKEY
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.SOFVBGDFTBL%rowtype;
 new_rec STGCSA.SOFVBGDFTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.SOFVBGDFTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBGDFDELTSP build 2018-01-23 08:30:21 '
 ||'Select of STGCSA.SOFVBGDFTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBGDFDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.SOFVBGDFAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.SOFVBGDFTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In SOFVBGDFDELTSP build 2018-01-23 08:30:21 '
 ||'while doing delete on STGCSA.SOFVBGDFTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO SOFVBGDFDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVBGDFDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBGDFTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(DEFRMNTGPKEY)=('||P_DEFRMNTGPKEY||') ';
 select rowid into rec_rowid from STGCSA.SOFVBGDFTBL where 1=1
 and DEFRMNTGPKEY=P_DEFRMNTGPKEY;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In SOFVBGDFDELTSP build 2018-01-23 08:30:21 '
 ||'No STGCSA.SOFVBGDFTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO SOFVBGDFDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In SOFVBGDFDELTSP build 2018-01-23 08:30:21 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVBGDFDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'SOFVBGDFDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVBGDFDELTSP build 2018-01-23 08:30:21'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO SOFVBGDFDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END SOFVBGDFDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


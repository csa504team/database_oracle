<!--- Saved 01/28/2018 14:42:18. --->
PROCEDURE PRPMFDUEFROMBORRDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_APPRDT DATE:=null
 ,p_CDCDUEFROMBORRAMT NUMBER:=null
 ,p_CSADUEFROMBORRAMT NUMBER:=null
 ,p_CURRFEEBALAMT NUMBER:=null
 ,p_DUEFROMBORRAMT NUMBER:=null
 ,p_ESCPRIORPYMTAMT NUMBER:=null
 ,p_LATEFEEAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_MATRDT DATE:=null
 ,p_PRPMFDUEFROMBORRID NUMBER:=null
 ,p_SEMIANAMT NUMBER:=null
 ,p_SEMIANDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:28:16
 Created by: GENR
 Crerated from template stdtbldel_template v3.12 16 Jan 2017 on 2018-01-23 08:28:16
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPMFDUEFROMBORRDELTSP performs DELETE on STGCSA.PRPMFDUEFROMBORRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPMFDUEFROMBORRID
 Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
 new_rec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPMFDUEFROMBORRTBL where rowid=rowid_in;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFDUEFROMBORRDELTSP build 2018-01-23 08:28:16 '
 ||'Select of STGCSA.PRPMFDUEFROMBORRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 end;
 -- "new" rec is null since rec to be deleted!
 new_rec:=null;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPMFDUEFROMBORRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 delete STGCSA.PRPMFDUEFROMBORRTBL where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPMFDUEFROMBORRDELTSP build 2018-01-23 08:28:16 '
 ||'while doing delete on STGCSA.PRPMFDUEFROMBORRTBL, on rec with rowid '
 ||rowid_in||' orinally fetched using '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFDUEFROMBORRDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(PRPMFDUEFROMBORRID)=('||P_PRPMFDUEFROMBORRID||') ';
 select rowid into rec_rowid from STGCSA.PRPMFDUEFROMBORRTBL where 1=1
 and PRPMFDUEFROMBORRID=P_PRPMFDUEFROMBORRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPMFDUEFROMBORRDELTSP build 2018-01-23 08:28:16 '
 ||'No STGCSA.PRPMFDUEFROMBORRTBL row to delete with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPMFDUEFROMBORRDELTSP build 2018-01-23 08:28:16 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 log_LOANNMB:=old_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFDUEFROMBORRDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 P_RETVAL:=0;
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPMFDUEFROMBORRDELTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if; EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PRPMFDUEFROMBORRDELTSP build 2018-01-23 08:28:16'
 ||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPMFDUEFROMBORRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


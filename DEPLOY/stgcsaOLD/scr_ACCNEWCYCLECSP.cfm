<!--- Saved 01/29/2018 10:22:46. --->
PROCEDURE ACCNEWCYCLECSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2,
 p_loannmb_list varchar2,
 p_curcycle date,
 p_nxtcycle date,
 p_busday1 date) as
 /*
 ACCNEWCYCLECSP V1.1 Jan 29 18 2018 JL
 */
 pgm varchar2(20):='ACCNEWCYCLE';
 ver varchar2(40):='Ver 1.1 29 Jan2018';
 v_loannmb number;
 c_loannmb char(10);
 reccnt number;
 ctr number;
 loans_updt number:=0;
 loans_nfd number:=0;
 loannmb_cnt number:=0;
 logentryid number;
 work_loannmb_list varchar2(4096);
 accloanstat_49_to_51 number:=0;
 accloanstat_50_to_52 number:=0;
 new_cycid number;
 coretransupd_cnt number:=0;
 ACCPRCSCYCstat_cnt number:=0;
 loggedmsgid number;
 begin
 -- setup
 savepoint ACCNEWCYCLECSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 reccnt:=0;
 -- 
 runtime_logger(logentryid, 'CSPMSG',700,pgm||' '||ver||' started. p_identifier='
 ||p_identifier||' p_loanlist='||p_loannmb_list||' p_curcycle='
 ||to_char(p_curcycle,'yyyy-mm-dd hh24:mi:ss')||' p_nxtcycle='
 ||to_char(p_nxtcycle,'yyyy-mm-dd hh24:mi:ss')||' p_busday1='
 ||to_char(p_busday1,'yyyy-mm-dd hh24:mi:ss'), p_userid,1,
 logtxt1=>p_loannmb_list,logdt1=>p_curcycle,logdt2=>p_nxtcycle,logdt3=>p_busday1);
 --
 case p_identifier
 when 0 then
 -- p_identifier=0 is only valid value, procedure has only one function
 --
 -- parse out loan numbers and update each in coretranstbl
 work_loannmb_list:=p_loannmb_list;
 -- to simplify parsing, add conmma to end of list
 if substr(work_loannmb_list,length(work_loannmb_list))<>',' then
 work_loannmb_list:=work_loannmb_list||',';
 end if; 
 ctr:=instr(work_loannmb_list,',');
 while work_loannmb_list<>',' and ctr<>0 and p_errval=0 loop
 begin
 -- loannmb is fixed char 10 containing nbr... verify
 if ctr<>11 then
 p_errval:=-20001;
 p_errmsg:=pgm||': Next comma delimited token (LOANNMB) on list: '
 ||substr(p_loannmb_list,1,ctr-1)||' is not 10 Characters';
 else 
 -- catch bad number in loannmb list
 v_loannmb:=to_number(substr(work_loannmb_list,1,ctr-1));
 work_loannmb_list:=substr(work_loannmb_list,ctr+1);
 c_loannmb:=substr(work_loannmb_list,1,ctr-1);
 loannmb_cnt:=loannmb_cnt+1;
 end if; 
 exception when others then
 p_errval:=-20001;
 p_errmsg:=pgm||': Next comma delimited token (LOANNMB) on list: '
 ||substr(work_loannmb_list,1,ctr-1)||' failed to validate as numeric. ' 
 ||SQLERRM(sqlcode); 
 end;
 if p_errval = 0 then
 coretransupdtsp(p_retval,p_errval,p_errmsg,2,p_userid,
 p_loannmb=>c_loannmb,p_STATIDCUR=>4);
 if p_errval=0 then 
 if p_retval=0 then
 loans_nfd:=loans_nfd+1;
 else 
 loans_updt:=loans_updt+p_retval;
 end if; 
 end if; 
 end if;
 end loop; 
 -- Did we hit any errors updateing loans on list? If not, proceed. 
 if p_errval = 0 then
 -- update stat in ACCACCELERATELOANTBL
 -- Note qual option 2 for ACCACCELERATELOANUPDTSP is on SEMIANDT
 -- and (static) STATID=49, so a new value can be supplied in P_STATID
 ACCACCELERATELOANUPDTSP(p_retval,p_errval,p_errmsg,2,p_userid,
 P_SEMIANDT=>p_curcycle,p_STATID=>51); 
 accloanstat_49_to_51:=p_retval;
 end if; 
 if p_errval = 0 then
 -- Note qual option 3 for ACCACCELERATELOANUPDTSP is on SEMIANDT
 -- and (static) STATID=50, so a new value can be supplied in P_STATID
 ACCACCELERATELOANUPDTSP(p_retval,p_errval,p_errmsg,2,p_userid,
 P_SEMIANDT=>p_curcycle,p_STATID=>52); 
 accloanstat_50_to_52:=p_retval;
 end if; 
 if p_errval = 0 then
 -- updt CORETRANS based on data from aaaaccelerateloan. 
 -- In order to use STD upd procedure, fetch ACCACCELERATELOAN data
 -- and do discreet updts on CORETRANS
 /* orig converted updt:
 UPDATE stgCSA.CoreTransTbl t2
 SET (statIdCur, LASTUPDTUSERID, LASTUPDTDT)
 = (
 SELECT t1.statId, P_USERID, sysdate
 FROM stgCSA.ACCAccelerateLoanTbl t1
 WHERE t1.transid = t2.transid
 AND t1.SemiAnDt = p_curcycle
 )
 WHERE EXISTS (
 SELECT 1
 FROM stgCSA.ACCAccelerateLoanTbl t1
 WHERE t1.transid = t2.transid
 AND t1.SemiAnDt = p_curcycle);
 */
 for accrec in (select statid, transid 
 from ACCAccelerateLoanTbl t1
 where semiandt=p_curcycle and exists
 (select * from coretranstbl t2 where t1.transid =t2.transid))
 loop
 -- coretransupd option 0 is qual on transid
 if p_errval=0 then
 coretransupdtsp(p_retval,p_errval,p_errmsg,0,p_userid, 
 p_transid=>accrec.transid,p_statidcur=>accrec.statid);
 coretransupd_cnt :=coretransupd_cnt+1;
 end if;
 end loop;
 end if;
 --
 -- add 1 row to accprcscycTBL
 --
 -- new accprcscycstatTBL insert gets new CYCID from ACCPRCSCYCTBL
 -- row to be added, so prefetch new CYCID from sequence cycidseq
 -- and include it in ACCPRCSCYCTBL insert. 
 select cycidseq.nextval into new_cycid from dual;
 --
 If p_errval=0 then 
 accprcscycinstsp(p_retval,p_errval,p_errmsg,0,p_userid, 
 p_cycid=>new_cycid, 
 p_cycyr=>to_number(to_char(p_nxtcycle,'yyyy')),
 p_cycmo=>to_number(to_char(p_nxtcycle,'mm')),
 p_cycmotxt=>to_char(p_nxtcycle,'MON'),
 p_semiandt=>p_nxtcycle,
 p_busday1dt=>p_busday1);
 end if; 
 --
 -- add rows to ACCPrcsCycStat 
 if p_errval=0 then
 -- select PRCSIDs to be added to ACCPrcsCycStat 
 for acp in (select prcsid from ACCcycprcsTbl where CYCPRCSACTV = 1) loop
 if p_errval=0 then
 ACCPRCSCYCstatINSTSP(p_retval,p_errval,p_errmsg,0,p_userid, 
 p_cycid=>new_cycid,
 p_prcsid=>acp.prcsid);
 ACCPRCSCYCstat_cnt:=+1; 
 end if; 
 end loop;
 end if; 
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO ACCNEWCYCLECSP;
 p_errval:=-20002;
 p_errmsg:=' In '||pgm||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 -- all done if nothing went wrong!
 -- if an error occured, rollback and report
 if p_errval<>0 then 
 rollback to ACCNEWCYCLECSP;
 runtime_logger(logentryid,'CSPMSG', 703, p_errmsg, p_userid, 3, 
 LOGTXT1=>p_loannmb_list, logtxt2=>p_loannmb_list); 
 else
 -- no errors!
 runtime_logger(logentryid,'CSPMSG', 701, 
 pgm||' '||ver||' Normal completion. Suplied loan# list in logtxt1.'
 ||'Cnt loan#s passed='||loannmb_cnt||' Loans UPDT='||loans_updt
 ||'Loans NFND='||loans_nfd
 ||' ACCLOANSTATs chnged from 49 to 51='||accloanstat_49_to_51
 ||' ACCLOANSTATs chnged from 50 to 52='||accloanstat_50_to_52
 ||' CORTRANSTBL updt='||coretransupd_cnt||' New CYCID='||new_cycid,
 p_userid, 3,LOGTXT1=>p_loannmb_list); 
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In '||pgm||' '||ver||' '
 ||' outer exception handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 runtime.logger(loggedmsgid,'STDERR',703,p_errmsg,p_userid,
 3);
 runtime_logger(logentryid,'CSPMSG', 702, 
 pgm||' '||ver||' ended with reported errors, updates were roled back.',
 p_userid, 3, LOGTXT1=>'Called with loannmb list='||p_loannmb_list); 
 --
 END ACCNEWCYCLECSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


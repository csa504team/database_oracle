<!--- Saved 01/28/2018 14:41:57. --->
PROCEDURE GENEMAILADRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_DSPLYNM VARCHAR2:=null
 ,p_EMAILACTV NUMBER:=null
 ,p_EMAILADR VARCHAR2:=null
 ,p_EMAILADRID NUMBER:=null
 ,p_GREETINGNM VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:23:02
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:23:02
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure GENEMAILADRUPDTSP performs UPDATE on STGCSA.GENEMAILADRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMAILADRID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.GENEMAILADRTBL%rowtype;
 new_rec STGCSA.GENEMAILADRTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.GENEMAILADRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILADRUPDTSP build 2018-01-23 08:23:02 '
 ||'Select of STGCSA.GENEMAILADRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILADRUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_EMAILADRID is not null then
 cur_col_name:='P_EMAILADRID';
 new_rec.EMAILADRID:=P_EMAILADRID; 
 end if;
 if P_DSPLYNM is not null then
 cur_col_name:='P_DSPLYNM';
 new_rec.DSPLYNM:=P_DSPLYNM; 
 end if;
 if P_GREETINGNM is not null then
 cur_col_name:='P_GREETINGNM';
 new_rec.GREETINGNM:=P_GREETINGNM; 
 end if;
 if P_EMAILADR is not null then
 cur_col_name:='P_EMAILADR';
 new_rec.EMAILADR:=P_EMAILADR; 
 end if;
 if P_EMAILACTV is not null then
 cur_col_name:='P_EMAILACTV';
 new_rec.EMAILACTV:=P_EMAILACTV; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.GENEMAILADRAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENEMAILADRTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,DSPLYNM=new_rec.DSPLYNM
 ,GREETINGNM=new_rec.GREETINGNM
 ,EMAILADR=new_rec.EMAILADR
 ,EMAILACTV=new_rec.EMAILACTV
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In GENEMAILADRUPDTSP build 2018-01-23 08:23:02 '
 ||'while doing update on STGCSA.GENEMAILADRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO GENEMAILADRUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENEMAILADRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(EMAILADRID)=('||P_EMAILADRID||') ';
 select rowid into rec_rowid from STGCSA.GENEMAILADRTBL where 1=1
 and EMAILADRID=P_EMAILADRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In GENEMAILADRUPDTSP build 2018-01-23 08:23:02 '
 ||'No STGCSA.GENEMAILADRTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO GENEMAILADRUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In GENEMAILADRUPDTSP build 2018-01-23 08:23:02 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILADRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'GENEMAILADRUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In GENEMAILADRUPDTSP build 2018-01-23 08:23:02'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO GENEMAILADRUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END GENEMAILADRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


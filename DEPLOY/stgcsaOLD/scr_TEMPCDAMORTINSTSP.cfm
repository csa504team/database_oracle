<!--- Saved 01/28/2018 14:42:53. --->
PROCEDURE TEMPCDAMORTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDAMORTID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_PD DATE:=null
 ,p_PD_BEGIN_PRIN NUMBER:=null
 ,p_PYMT_AMT NUMBER:=null
 ,p_PRIN_AMT NUMBER:=null
 ,p_INT_AMT NUMBER:=null
 ,p_CDC_AMT NUMBER:=null
 ,p_SBA_AMT NUMBER:=null
 ,p_CSA_AMT NUMBER:=null
 ,p_LATE_AMT NUMBER:=null
 ,p_PRIN_BALANCE NUMBER:=null
 ,p_PRIN_NEEDED NUMBER:=null
 ,p_INT_NEEDED NUMBER:=null
 ,p_CDC_FEE_NEEDED NUMBER:=null
 ,p_SBA_FEE_NEEDED NUMBER:=null
 ,p_CSA_FEE_NEEDED NUMBER:=null
 ,p_LATE_FEE_NEEDED NUMBER:=null
 ,p_UNALLOCATED_AMT NUMBER:=null
 ,p_PRIN_PYMT NUMBER:=null
 ,p_INT_PYMT NUMBER:=null
 ,p_TOTAL_PRIN NUMBER:=null
 ,p_STATUS VARCHAR2:=null
 ,p_TYPENM VARCHAR2:=null
 ,p_FIRSTPAYMENTDT DATE:=null
 ,p_LASTPAYMENTDT DATE:=null
 ) as
 /*
 Created on: 2018-01-23 08:36:19
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:36:19
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.TEMPCDAMORTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.TEMPCDAMORTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint TEMPCDAMORTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_LOANNMB:=P_LOANNMB;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CDAMORTID';
 new_rec.CDAMORTID:=runtime.validate_column_value(P_CDAMORTID,
 'STGCSA','TEMPCDAMORTTBL','CDAMORTID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','TEMPCDAMORTTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_PD';
 new_rec.PD:=runtime.validate_column_value(P_PD,
 'STGCSA','TEMPCDAMORTTBL','PD','DATE',7);
 cur_col_name:='P_PD_BEGIN_PRIN';
 new_rec.PD_BEGIN_PRIN:=runtime.validate_column_value(P_PD_BEGIN_PRIN,
 'STGCSA','TEMPCDAMORTTBL','PD_BEGIN_PRIN','NUMBER',22);
 cur_col_name:='P_PYMT_AMT';
 new_rec.PYMT_AMT:=runtime.validate_column_value(P_PYMT_AMT,
 'STGCSA','TEMPCDAMORTTBL','PYMT_AMT','NUMBER',22);
 cur_col_name:='P_PRIN_AMT';
 new_rec.PRIN_AMT:=runtime.validate_column_value(P_PRIN_AMT,
 'STGCSA','TEMPCDAMORTTBL','PRIN_AMT','NUMBER',22);
 cur_col_name:='P_INT_AMT';
 new_rec.INT_AMT:=runtime.validate_column_value(P_INT_AMT,
 'STGCSA','TEMPCDAMORTTBL','INT_AMT','NUMBER',22);
 cur_col_name:='P_CDC_AMT';
 new_rec.CDC_AMT:=runtime.validate_column_value(P_CDC_AMT,
 'STGCSA','TEMPCDAMORTTBL','CDC_AMT','NUMBER',22);
 cur_col_name:='P_SBA_AMT';
 new_rec.SBA_AMT:=runtime.validate_column_value(P_SBA_AMT,
 'STGCSA','TEMPCDAMORTTBL','SBA_AMT','NUMBER',22);
 cur_col_name:='P_CSA_AMT';
 new_rec.CSA_AMT:=runtime.validate_column_value(P_CSA_AMT,
 'STGCSA','TEMPCDAMORTTBL','CSA_AMT','NUMBER',22);
 cur_col_name:='P_LATE_AMT';
 new_rec.LATE_AMT:=runtime.validate_column_value(P_LATE_AMT,
 'STGCSA','TEMPCDAMORTTBL','LATE_AMT','NUMBER',22);
 cur_col_name:='P_PRIN_BALANCE';
 new_rec.PRIN_BALANCE:=runtime.validate_column_value(P_PRIN_BALANCE,
 'STGCSA','TEMPCDAMORTTBL','PRIN_BALANCE','NUMBER',22);
 cur_col_name:='P_PRIN_NEEDED';
 new_rec.PRIN_NEEDED:=runtime.validate_column_value(P_PRIN_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','PRIN_NEEDED','NUMBER',22);
 cur_col_name:='P_INT_NEEDED';
 new_rec.INT_NEEDED:=runtime.validate_column_value(P_INT_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','INT_NEEDED','NUMBER',22);
 cur_col_name:='P_CDC_FEE_NEEDED';
 new_rec.CDC_FEE_NEEDED:=runtime.validate_column_value(P_CDC_FEE_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','CDC_FEE_NEEDED','NUMBER',22);
 cur_col_name:='P_SBA_FEE_NEEDED';
 new_rec.SBA_FEE_NEEDED:=runtime.validate_column_value(P_SBA_FEE_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','SBA_FEE_NEEDED','NUMBER',22);
 cur_col_name:='P_CSA_FEE_NEEDED';
 new_rec.CSA_FEE_NEEDED:=runtime.validate_column_value(P_CSA_FEE_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','CSA_FEE_NEEDED','NUMBER',22);
 cur_col_name:='P_LATE_FEE_NEEDED';
 new_rec.LATE_FEE_NEEDED:=runtime.validate_column_value(P_LATE_FEE_NEEDED,
 'STGCSA','TEMPCDAMORTTBL','LATE_FEE_NEEDED','NUMBER',22);
 cur_col_name:='P_UNALLOCATED_AMT';
 new_rec.UNALLOCATED_AMT:=runtime.validate_column_value(P_UNALLOCATED_AMT,
 'STGCSA','TEMPCDAMORTTBL','UNALLOCATED_AMT','NUMBER',22);
 cur_col_name:='P_PRIN_PYMT';
 new_rec.PRIN_PYMT:=runtime.validate_column_value(P_PRIN_PYMT,
 'STGCSA','TEMPCDAMORTTBL','PRIN_PYMT','NUMBER',22);
 cur_col_name:='P_INT_PYMT';
 new_rec.INT_PYMT:=runtime.validate_column_value(P_INT_PYMT,
 'STGCSA','TEMPCDAMORTTBL','INT_PYMT','NUMBER',22);
 cur_col_name:='P_TOTAL_PRIN';
 new_rec.TOTAL_PRIN:=runtime.validate_column_value(P_TOTAL_PRIN,
 'STGCSA','TEMPCDAMORTTBL','TOTAL_PRIN','NUMBER',22);
 cur_col_name:='P_STATUS';
 new_rec.STATUS:=runtime.validate_column_value(P_STATUS,
 'STGCSA','TEMPCDAMORTTBL','STATUS','VARCHAR2',30);
 cur_col_name:='P_TYPENM';
 new_rec.TYPENM:=runtime.validate_column_value(P_TYPENM,
 'STGCSA','TEMPCDAMORTTBL','TYPENM','VARCHAR2',30);
 cur_col_name:='P_FIRSTPAYMENTDT';
 new_rec.FIRSTPAYMENTDT:=runtime.validate_column_value(P_FIRSTPAYMENTDT,
 'STGCSA','TEMPCDAMORTTBL','FIRSTPAYMENTDT','DATE',7);
 cur_col_name:='P_LASTPAYMENTDT';
 new_rec.LASTPAYMENTDT:=runtime.validate_column_value(P_LASTPAYMENTDT,
 'STGCSA','TEMPCDAMORTTBL','LASTPAYMENTDT','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO TEMPCDAMORTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.TEMPCDAMORTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.TEMPCDAMORTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO TEMPCDAMORTINSTSP;
 p_errmsg:='Got dupe key on insert into TEMPCDAMORTTBL for key '
 ||'(CDAMORTID)=('||new_rec.CDAMORTID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in TEMPCDAMORTINSTSP build 2018-01-23 08:36:19';
 ROLLBACK TO TEMPCDAMORTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO TEMPCDAMORTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPCDAMORTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'TEMPCDAMORTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in TEMPCDAMORTINSTSP build 2018-01-23 08:36:19'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO TEMPCDAMORTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END TEMPCDAMORTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


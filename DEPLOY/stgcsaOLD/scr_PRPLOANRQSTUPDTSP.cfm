<!--- Saved 01/28/2018 14:42:17. --->
PROCEDURE PRPLOANRQSTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACTUALRQST VARCHAR2:=null
 ,p_AMORTCDCAMT NUMBER:=null
 ,p_AMORTCSAAMT NUMBER:=null
 ,p_AMORTSBAAMT NUMBER:=null
 ,p_APPVDT DATE:=null
 ,p_BALAMT NUMBER:=null
 ,p_BALCALC1AMT NUMBER:=null
 ,p_BALCALC2AMT NUMBER:=null
 ,p_BALCALC3AMT NUMBER:=null
 ,p_BALCALC4AMT NUMBER:=null
 ,p_BALCALC5AMT NUMBER:=null
 ,p_BALCALC6AMT NUMBER:=null
 ,p_BALCALCAMT NUMBER:=null
 ,p_BEHINDAMT NUMBER:=null
 ,p_BORRFEEAMT NUMBER:=null
 ,p_BORRNM VARCHAR2:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_BUSDAY6DT DATE:=null
 ,p_CDCBEHINDAMT NUMBER:=null
 ,p_CDCDUEFROMBORRAMT NUMBER:=null
 ,p_CDCFEECALCONLYAMT NUMBER:=null
 ,p_CDCFEEDISBAMT NUMBER:=null
 ,p_CDCFEENEEDEDAMT NUMBER:=null
 ,p_CDCNMB CHAR:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_CDCPCT NUMBER:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCSBAFEEDISBAMT NUMBER:=null
 ,p_CSABEHINDAMT NUMBER:=null
 ,p_CSADUEFROMBORRAMT NUMBER:=null
 ,p_CSAFEECALCAMT NUMBER:=null
 ,p_CSAFEECALCONLYAMT NUMBER:=null
 ,p_CSAFEENEEDEDAMT NUMBER:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CURRFEEBALAMT NUMBER:=null
 ,p_DBENTRBALAMT NUMBER:=null
 ,p_DUEFROMBORRAMT NUMBER:=null
 ,p_ESCROWAMT NUMBER:=null
 ,p_FEECALCAMT NUMBER:=null
 ,p_FEECALCONLYAMT NUMBER:=null
 ,p_FEENEEDEDAMT NUMBER:=null
 ,p_FIVEYRADJ NUMBER:=null
 ,p_FIVEYRADJCDCAMT NUMBER:=null
 ,p_FIVEYRADJCSAAMT NUMBER:=null
 ,p_FIVEYRADJSBAAMT NUMBER:=null
 ,p_GFDAMT NUMBER:=null
 ,p_INT2AMT NUMBER:=null
 ,p_INT3AMT NUMBER:=null
 ,p_INT4AMT NUMBER:=null
 ,p_INT5AMT NUMBER:=null
 ,p_INT6AMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_INTNEEDEDAMT NUMBER:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_ISSDT DATE:=null
 ,p_LATEFEENEEDEDAMT NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_LOANSTAT VARCHAR2:=null
 ,p_MODELNQ NUMBER:=null
 ,p_NOTEBALCALCAMT NUMBER:=null
 ,p_NOTERTPCT NUMBER:=null
 ,p_OPENGNTYAMT NUMBER:=null
 ,p_OPENGNTYERRAMT NUMBER:=null
 ,p_OPENGNTYREGAMT NUMBER:=null
 ,p_OPENGNTYUNALLOCAMT NUMBER:=null
 ,p_PI6MOAMT NUMBER:=null
 ,p_PIAMT NUMBER:=null
 ,p_PIPROJAMT NUMBER:=null
 ,p_POSTINGDT DATE:=null
 ,p_POSTINGMO NUMBER:=null
 ,p_POSTINGYRNMB CHAR:=null
 ,p_PREPAYAMT NUMBER:=null
 ,p_PREPAYCALCDT DATE:=null
 ,p_PREPAYDT DATE:=null
 ,p_PREPAYMO NUMBER:=null
 ,p_PREPAYPCT NUMBER:=null
 ,p_PREPAYPREMAMT NUMBER:=null
 ,p_PREPAYYRNMB CHAR:=null
 ,p_PRIN2AMT NUMBER:=null
 ,p_PRIN3AMT NUMBER:=null
 ,p_PRIN4AMT NUMBER:=null
 ,p_PRIN5AMT NUMBER:=null
 ,p_PRIN6AMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_PRINCURAMT NUMBER:=null
 ,p_PRINNEEDEDAMT NUMBER:=null
 ,p_PRINPROJAMT NUMBER:=null
 ,p_RQSTCD NUMBER:=null
 ,p_RQSTREF NUMBER:=null
 ,p_SEMIANMO NUMBER:=null
 ,p_SEMIANNAMT NUMBER:=null
 ,p_SEMIANNDT DATE:=null
 ,p_SEMIANNPYMTAMT NUMBER:=null
 ,p_THIRDTHURSDAY DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_USERLCK VARCHAR2:=null
 ) as
 /*
 Created on: 2018-01-23 08:27:23
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:27:23
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure PRPLOANRQSTUPDTSP performs UPDATE on STGCSA.PRPLOANRQSTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 new_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.PRPLOANRQSTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTSP build 2018-01-23 08:27:23 '
 ||'Select of STGCSA.PRPLOANRQSTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_RQSTREF is not null then
 cur_col_name:='P_RQSTREF';
 new_rec.RQSTREF:=P_RQSTREF; 
 end if;
 if P_RQSTCD is not null then
 cur_col_name:='P_RQSTCD';
 new_rec.RQSTCD:=P_RQSTCD; 
 end if;
 if P_CDCREGNCD is not null then
 cur_col_name:='P_CDCREGNCD';
 new_rec.CDCREGNCD:=P_CDCREGNCD; 
 end if;
 if P_CDCNMB is not null then
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=P_CDCNMB; 
 end if;
 if P_LOANSTAT is not null then
 cur_col_name:='P_LOANSTAT';
 new_rec.LOANSTAT:=P_LOANSTAT; 
 end if;
 if P_BORRNM is not null then
 cur_col_name:='P_BORRNM';
 new_rec.BORRNM:=P_BORRNM; 
 end if;
 if P_PREPAYAMT is not null then
 cur_col_name:='P_PREPAYAMT';
 new_rec.PREPAYAMT:=P_PREPAYAMT; 
 end if;
 if P_PREPAYDT is not null then
 cur_col_name:='P_PREPAYDT';
 new_rec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_PREPAYMO is not null then
 cur_col_name:='P_PREPAYMO';
 new_rec.PREPAYMO:=P_PREPAYMO; 
 end if;
 if P_PREPAYYRNMB is not null then
 cur_col_name:='P_PREPAYYRNMB';
 new_rec.PREPAYYRNMB:=P_PREPAYYRNMB; 
 end if;
 if P_PREPAYCALCDT is not null then
 cur_col_name:='P_PREPAYCALCDT';
 new_rec.PREPAYCALCDT:=P_PREPAYCALCDT; 
 end if;
 if P_SEMIANNDT is not null then
 cur_col_name:='P_SEMIANNDT';
 new_rec.SEMIANNDT:=P_SEMIANNDT; 
 end if;
 if P_SEMIANMO is not null then
 cur_col_name:='P_SEMIANMO';
 new_rec.SEMIANMO:=P_SEMIANMO; 
 end if;
 if P_SEMIANNAMT is not null then
 cur_col_name:='P_SEMIANNAMT';
 new_rec.SEMIANNAMT:=P_SEMIANNAMT; 
 end if;
 if P_SEMIANNPYMTAMT is not null then
 cur_col_name:='P_SEMIANNPYMTAMT';
 new_rec.SEMIANNPYMTAMT:=P_SEMIANNPYMTAMT; 
 end if;
 if P_PRINCURAMT is not null then
 cur_col_name:='P_PRINCURAMT';
 new_rec.PRINCURAMT:=P_PRINCURAMT; 
 end if;
 if P_BALAMT is not null then
 cur_col_name:='P_BALAMT';
 new_rec.BALAMT:=P_BALAMT; 
 end if;
 if P_BALCALCAMT is not null then
 cur_col_name:='P_BALCALCAMT';
 new_rec.BALCALCAMT:=P_BALCALCAMT; 
 end if;
 if P_NOTEBALCALCAMT is not null then
 cur_col_name:='P_NOTEBALCALCAMT';
 new_rec.NOTEBALCALCAMT:=P_NOTEBALCALCAMT; 
 end if;
 if P_PIAMT is not null then
 cur_col_name:='P_PIAMT';
 new_rec.PIAMT:=P_PIAMT; 
 end if;
 if P_NOTERTPCT is not null then
 cur_col_name:='P_NOTERTPCT';
 new_rec.NOTERTPCT:=P_NOTERTPCT; 
 end if;
 if P_ISSDT is not null then
 cur_col_name:='P_ISSDT';
 new_rec.ISSDT:=P_ISSDT; 
 end if;
 if P_DBENTRBALAMT is not null then
 cur_col_name:='P_DBENTRBALAMT';
 new_rec.DBENTRBALAMT:=P_DBENTRBALAMT; 
 end if;
 if P_PREPAYPREMAMT is not null then
 cur_col_name:='P_PREPAYPREMAMT';
 new_rec.PREPAYPREMAMT:=P_PREPAYPREMAMT; 
 end if;
 if P_POSTINGDT is not null then
 cur_col_name:='P_POSTINGDT';
 new_rec.POSTINGDT:=P_POSTINGDT; 
 end if;
 if P_POSTINGMO is not null then
 cur_col_name:='P_POSTINGMO';
 new_rec.POSTINGMO:=P_POSTINGMO; 
 end if;
 if P_POSTINGYRNMB is not null then
 cur_col_name:='P_POSTINGYRNMB';
 new_rec.POSTINGYRNMB:=P_POSTINGYRNMB; 
 end if;
 if P_BUSDAY6DT is not null then
 cur_col_name:='P_BUSDAY6DT';
 new_rec.BUSDAY6DT:=P_BUSDAY6DT; 
 end if;
 if P_THIRDTHURSDAY is not null then
 cur_col_name:='P_THIRDTHURSDAY';
 new_rec.THIRDTHURSDAY:=P_THIRDTHURSDAY; 
 end if;
 if P_ACTUALRQST is not null then
 cur_col_name:='P_ACTUALRQST';
 new_rec.ACTUALRQST:=P_ACTUALRQST; 
 end if;
 if P_CSAPAIDTHRUDT is not null then
 cur_col_name:='P_CSAPAIDTHRUDT';
 new_rec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT; 
 end if;
 if P_CDCPAIDTHRUDT is not null then
 cur_col_name:='P_CDCPAIDTHRUDT';
 new_rec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT; 
 end if;
 if P_BORRPAIDTHRUDT is not null then
 cur_col_name:='P_BORRPAIDTHRUDT';
 new_rec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT; 
 end if;
 if P_INTPAIDTHRUDT is not null then
 cur_col_name:='P_INTPAIDTHRUDT';
 new_rec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT; 
 end if;
 if P_APPVDT is not null then
 cur_col_name:='P_APPVDT';
 new_rec.APPVDT:=P_APPVDT; 
 end if;
 if P_PRINNEEDEDAMT is not null then
 cur_col_name:='P_PRINNEEDEDAMT';
 new_rec.PRINNEEDEDAMT:=P_PRINNEEDEDAMT; 
 end if;
 if P_INTNEEDEDAMT is not null then
 cur_col_name:='P_INTNEEDEDAMT';
 new_rec.INTNEEDEDAMT:=P_INTNEEDEDAMT; 
 end if;
 if P_MODELNQ is not null then
 cur_col_name:='P_MODELNQ';
 new_rec.MODELNQ:=P_MODELNQ; 
 end if;
 if P_CURRFEEBALAMT is not null then
 cur_col_name:='P_CURRFEEBALAMT';
 new_rec.CURRFEEBALAMT:=P_CURRFEEBALAMT; 
 end if;
 if P_CSAFEENEEDEDAMT is not null then
 cur_col_name:='P_CSAFEENEEDEDAMT';
 new_rec.CSAFEENEEDEDAMT:=P_CSAFEENEEDEDAMT; 
 end if;
 if P_CDCFEENEEDEDAMT is not null then
 cur_col_name:='P_CDCFEENEEDEDAMT';
 new_rec.CDCFEENEEDEDAMT:=P_CDCFEENEEDEDAMT; 
 end if;
 if P_FEENEEDEDAMT is not null then
 cur_col_name:='P_FEENEEDEDAMT';
 new_rec.FEENEEDEDAMT:=P_FEENEEDEDAMT; 
 end if;
 if P_LATEFEENEEDEDAMT is not null then
 cur_col_name:='P_LATEFEENEEDEDAMT';
 new_rec.LATEFEENEEDEDAMT:=P_LATEFEENEEDEDAMT; 
 end if;
 if P_CSADUEFROMBORRAMT is not null then
 cur_col_name:='P_CSADUEFROMBORRAMT';
 new_rec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT; 
 end if;
 if P_CDCDUEFROMBORRAMT is not null then
 cur_col_name:='P_CDCDUEFROMBORRAMT';
 new_rec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT; 
 end if;
 if P_DUEFROMBORRAMT is not null then
 cur_col_name:='P_DUEFROMBORRAMT';
 new_rec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT; 
 end if;
 if P_CSABEHINDAMT is not null then
 cur_col_name:='P_CSABEHINDAMT';
 new_rec.CSABEHINDAMT:=P_CSABEHINDAMT; 
 end if;
 if P_CDCBEHINDAMT is not null then
 cur_col_name:='P_CDCBEHINDAMT';
 new_rec.CDCBEHINDAMT:=P_CDCBEHINDAMT; 
 end if;
 if P_BEHINDAMT is not null then
 cur_col_name:='P_BEHINDAMT';
 new_rec.BEHINDAMT:=P_BEHINDAMT; 
 end if;
 if P_BORRFEEAMT is not null then
 cur_col_name:='P_BORRFEEAMT';
 new_rec.BORRFEEAMT:=P_BORRFEEAMT; 
 end if;
 if P_CDCFEEDISBAMT is not null then
 cur_col_name:='P_CDCFEEDISBAMT';
 new_rec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT; 
 end if;
 if P_AMORTCDCAMT is not null then
 cur_col_name:='P_AMORTCDCAMT';
 new_rec.AMORTCDCAMT:=P_AMORTCDCAMT; 
 end if;
 if P_AMORTSBAAMT is not null then
 cur_col_name:='P_AMORTSBAAMT';
 new_rec.AMORTSBAAMT:=P_AMORTSBAAMT; 
 end if;
 if P_AMORTCSAAMT is not null then
 cur_col_name:='P_AMORTCSAAMT';
 new_rec.AMORTCSAAMT:=P_AMORTCSAAMT; 
 end if;
 if P_CDCPCT is not null then
 cur_col_name:='P_CDCPCT';
 new_rec.CDCPCT:=P_CDCPCT; 
 end if;
 if P_PREPAYPCT is not null then
 cur_col_name:='P_PREPAYPCT';
 new_rec.PREPAYPCT:=P_PREPAYPCT; 
 end if;
 if P_FEECALCAMT is not null then
 cur_col_name:='P_FEECALCAMT';
 new_rec.FEECALCAMT:=P_FEECALCAMT; 
 end if;
 if P_CSAFEECALCAMT is not null then
 cur_col_name:='P_CSAFEECALCAMT';
 new_rec.CSAFEECALCAMT:=P_CSAFEECALCAMT; 
 end if;
 if P_CSAFEECALCONLYAMT is not null then
 cur_col_name:='P_CSAFEECALCONLYAMT';
 new_rec.CSAFEECALCONLYAMT:=P_CSAFEECALCONLYAMT; 
 end if;
 if P_CDCFEECALCONLYAMT is not null then
 cur_col_name:='P_CDCFEECALCONLYAMT';
 new_rec.CDCFEECALCONLYAMT:=P_CDCFEECALCONLYAMT; 
 end if;
 if P_FEECALCONLYAMT is not null then
 cur_col_name:='P_FEECALCONLYAMT';
 new_rec.FEECALCONLYAMT:=P_FEECALCONLYAMT; 
 end if;
 if P_FIVEYRADJ is not null then
 cur_col_name:='P_FIVEYRADJ';
 new_rec.FIVEYRADJ:=P_FIVEYRADJ; 
 end if;
 if P_FIVEYRADJCSAAMT is not null then
 cur_col_name:='P_FIVEYRADJCSAAMT';
 new_rec.FIVEYRADJCSAAMT:=P_FIVEYRADJCSAAMT; 
 end if;
 if P_FIVEYRADJCDCAMT is not null then
 cur_col_name:='P_FIVEYRADJCDCAMT';
 new_rec.FIVEYRADJCDCAMT:=P_FIVEYRADJCDCAMT; 
 end if;
 if P_FIVEYRADJSBAAMT is not null then
 cur_col_name:='P_FIVEYRADJSBAAMT';
 new_rec.FIVEYRADJSBAAMT:=P_FIVEYRADJSBAAMT; 
 end if;
 if P_BALCALC1AMT is not null then
 cur_col_name:='P_BALCALC1AMT';
 new_rec.BALCALC1AMT:=P_BALCALC1AMT; 
 end if;
 if P_BALCALC2AMT is not null then
 cur_col_name:='P_BALCALC2AMT';
 new_rec.BALCALC2AMT:=P_BALCALC2AMT; 
 end if;
 end if;
 if P_BALCALC3AMT is not null then
 cur_col_name:='P_BALCALC3AMT';
 new_rec.BALCALC3AMT:=P_BALCALC3AMT; 
 if P_BALCALC4AMT is not null then
 cur_col_name:='P_BALCALC4AMT';
 new_rec.BALCALC4AMT:=P_BALCALC4AMT; 
 end if;
 if P_BALCALC5AMT is not null then
 cur_col_name:='P_BALCALC5AMT';
 new_rec.BALCALC5AMT:=P_BALCALC5AMT; 
 end if;
 if P_BALCALC6AMT is not null then
 cur_col_name:='P_BALCALC6AMT';
 new_rec.BALCALC6AMT:=P_BALCALC6AMT; 
 end if;
 if P_PRINAMT is not null then
 cur_col_name:='P_PRINAMT';
 new_rec.PRINAMT:=P_PRINAMT; 
 end if;
 if P_PRIN2AMT is not null then
 cur_col_name:='P_PRIN2AMT';
 new_rec.PRIN2AMT:=P_PRIN2AMT; 
 end if;
 if P_PRIN3AMT is not null then
 cur_col_name:='P_PRIN3AMT';
 new_rec.PRIN3AMT:=P_PRIN3AMT; 
 end if;
 if P_PRIN4AMT is not null then
 cur_col_name:='P_PRIN4AMT';
 new_rec.PRIN4AMT:=P_PRIN4AMT; 
 end if;
 if P_PRIN5AMT is not null then
 cur_col_name:='P_PRIN5AMT';
 new_rec.PRIN5AMT:=P_PRIN5AMT; 
 end if;
 if P_PRIN6AMT is not null then
 cur_col_name:='P_PRIN6AMT';
 new_rec.PRIN6AMT:=P_PRIN6AMT; 
 end if;
 if P_PRINPROJAMT is not null then
 cur_col_name:='P_PRINPROJAMT';
 new_rec.PRINPROJAMT:=P_PRINPROJAMT; 
 end if;
 if P_PIPROJAMT is not null then
 cur_col_name:='P_PIPROJAMT';
 new_rec.PIPROJAMT:=P_PIPROJAMT; 
 end if;
 if P_INTAMT is not null then
 cur_col_name:='P_INTAMT';
 new_rec.INTAMT:=P_INTAMT; 
 end if;
 if P_INT2AMT is not null then
 cur_col_name:='P_INT2AMT';
 new_rec.INT2AMT:=P_INT2AMT; 
 end if;
 if P_INT3AMT is not null then
 cur_col_name:='P_INT3AMT';
 new_rec.INT3AMT:=P_INT3AMT; 
 end if;
 if P_INT4AMT is not null then
 cur_col_name:='P_INT4AMT';
 new_rec.INT4AMT:=P_INT4AMT; 
 end if;
 if P_INT5AMT is not null then
 cur_col_name:='P_INT5AMT';
 new_rec.INT5AMT:=P_INT5AMT; 
 end if;
 if P_INT6AMT is not null then
 cur_col_name:='P_INT6AMT';
 new_rec.INT6AMT:=P_INT6AMT; 
 end if;
 if P_ESCROWAMT is not null then
 cur_col_name:='P_ESCROWAMT';
 new_rec.ESCROWAMT:=P_ESCROWAMT; 
 end if;
 if P_PI6MOAMT is not null then
 cur_col_name:='P_PI6MOAMT';
 new_rec.PI6MOAMT:=P_PI6MOAMT; 
 end if;
 if P_GFDAMT is not null then
 cur_col_name:='P_GFDAMT';
 new_rec.GFDAMT:=P_GFDAMT; 
 end if;
 if P_OPENGNTYAMT is not null then
 cur_col_name:='P_OPENGNTYAMT';
 new_rec.OPENGNTYAMT:=P_OPENGNTYAMT; 
 end if;
 if P_OPENGNTYERRAMT is not null then
 cur_col_name:='P_OPENGNTYERRAMT';
 new_rec.OPENGNTYERRAMT:=P_OPENGNTYERRAMT; 
 end if;
 if P_OPENGNTYREGAMT is not null then
 cur_col_name:='P_OPENGNTYREGAMT';
 new_rec.OPENGNTYREGAMT:=P_OPENGNTYREGAMT; 
 end if;
 if P_OPENGNTYUNALLOCAMT is not null then
 cur_col_name:='P_OPENGNTYUNALLOCAMT';
 new_rec.OPENGNTYUNALLOCAMT:=P_OPENGNTYUNALLOCAMT; 
 end if;
 if P_UNALLOCAMT is not null then
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_USERLCK is not null then
 cur_col_name:='P_USERLCK';
 new_rec.USERLCK:=P_USERLCK; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 if P_CDCSBAFEEDISBAMT is not null then
 cur_col_name:='P_CDCSBAFEEDISBAMT';
 new_rec.CDCSBAFEEDISBAMT:=P_CDCSBAFEEDISBAMT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.PRPLOANRQSTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPLOANRQSTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,RQSTREF=new_rec.RQSTREF
 ,RQSTCD=new_rec.RQSTCD
 ,CDCREGNCD=new_rec.CDCREGNCD
 ,CDCNMB=new_rec.CDCNMB
 ,LOANSTAT=new_rec.LOANSTAT
 ,BORRNM=new_rec.BORRNM
 ,PREPAYAMT=new_rec.PREPAYAMT
 ,PREPAYDT=new_rec.PREPAYDT
 ,PREPAYMO=new_rec.PREPAYMO
 ,PREPAYYRNMB=new_rec.PREPAYYRNMB
 ,PREPAYCALCDT=new_rec.PREPAYCALCDT
 ,SEMIANNDT=new_rec.SEMIANNDT
 ,SEMIANMO=new_rec.SEMIANMO
 ,BALAMT=new_rec.BALAMT
 ,SEMIANNAMT=new_rec.SEMIANNAMT
 ,SEMIANNPYMTAMT=new_rec.SEMIANNPYMTAMT
 ,PRINCURAMT=new_rec.PRINCURAMT
 ,BALCALCAMT=new_rec.BALCALCAMT
 ,NOTEBALCALCAMT=new_rec.NOTEBALCALCAMT
 ,PIAMT=new_rec.PIAMT
 ,NOTERTPCT=new_rec.NOTERTPCT
 ,ISSDT=new_rec.ISSDT
 ,DBENTRBALAMT=new_rec.DBENTRBALAMT
 ,PREPAYPREMAMT=new_rec.PREPAYPREMAMT
 ,POSTINGDT=new_rec.POSTINGDT
 ,POSTINGMO=new_rec.POSTINGMO
 ,POSTINGYRNMB=new_rec.POSTINGYRNMB
 ,BUSDAY6DT=new_rec.BUSDAY6DT
 ,THIRDTHURSDAY=new_rec.THIRDTHURSDAY
 ,ACTUALRQST=new_rec.ACTUALRQST
 ,CSAPAIDTHRUDT=new_rec.CSAPAIDTHRUDT
 ,CDCPAIDTHRUDT=new_rec.CDCPAIDTHRUDT
 ,BORRPAIDTHRUDT=new_rec.BORRPAIDTHRUDT
 ,INTPAIDTHRUDT=new_rec.INTPAIDTHRUDT
 ,APPVDT=new_rec.APPVDT
 ,PRINNEEDEDAMT=new_rec.PRINNEEDEDAMT
 ,INTNEEDEDAMT=new_rec.INTNEEDEDAMT
 ,MODELNQ=new_rec.MODELNQ
 ,CURRFEEBALAMT=new_rec.CURRFEEBALAMT
 ,CSAFEENEEDEDAMT=new_rec.CSAFEENEEDEDAMT
 ,CDCFEENEEDEDAMT=new_rec.CDCFEENEEDEDAMT
 ,FEENEEDEDAMT=new_rec.FEENEEDEDAMT
 ,LATEFEENEEDEDAMT=new_rec.LATEFEENEEDEDAMT
 ,CSADUEFROMBORRAMT=new_rec.CSADUEFROMBORRAMT
 ,CDCDUEFROMBORRAMT=new_rec.CDCDUEFROMBORRAMT
 ,DUEFROMBORRAMT=new_rec.DUEFROMBORRAMT
 ,CSABEHINDAMT=new_rec.CSABEHINDAMT
 ,CDCBEHINDAMT=new_rec.CDCBEHINDAMT
 ,BEHINDAMT=new_rec.BEHINDAMT
 ,BORRFEEAMT=new_rec.BORRFEEAMT
 ,CDCFEEDISBAMT=new_rec.CDCFEEDISBAMT
 ,AMORTCDCAMT=new_rec.AMORTCDCAMT
 ,AMORTSBAAMT=new_rec.AMORTSBAAMT
 ,AMORTCSAAMT=new_rec.AMORTCSAAMT
 ,CDCPCT=new_rec.CDCPCT
 ,PREPAYPCT=new_rec.PREPAYPCT
 ,FEECALCAMT=new_rec.FEECALCAMT
 ,CSAFEECALCAMT=new_rec.CSAFEECALCAMT
 ,CSAFEECALCONLYAMT=new_rec.CSAFEECALCONLYAMT
 ,CDCFEECALCONLYAMT=new_rec.CDCFEECALCONLYAMT
 ,FEECALCONLYAMT=new_rec.FEECALCONLYAMT
 ,FIVEYRADJ=new_rec.FIVEYRADJ
 ,FIVEYRADJCSAAMT=new_rec.FIVEYRADJCSAAMT
 ,FIVEYRADJCDCAMT=new_rec.FIVEYRADJCDCAMT
 ,FIVEYRADJSBAAMT=new_rec.FIVEYRADJSBAAMT
 ,BALCALC1AMT=new_rec.BALCALC1AMT
 ,BALCALC2AMT=new_rec.BALCALC2AMT
 ,BALCALC3AMT=new_rec.BALCALC3AMT
 ,BALCALC4AMT=new_rec.BALCALC4AMT
 ,BALCALC5AMT=new_rec.BALCALC5AMT
 ,BALCALC6AMT=new_rec.BALCALC6AMT
 ,PRINAMT=new_rec.PRINAMT
 ,PRIN2AMT=new_rec.PRIN2AMT
 ,PRIN3AMT=new_rec.PRIN3AMT
 ,PRIN4AMT=new_rec.PRIN4AMT
 ,PRIN5AMT=new_rec.PRIN5AMT
 ,PRIN6AMT=new_rec.PRIN6AMT
 ,PRINPROJAMT=new_rec.PRINPROJAMT
 ,PIPROJAMT=new_rec.PIPROJAMT
 ,INTAMT=new_rec.INTAMT
 ,INT2AMT=new_rec.INT2AMT
 ,INT3AMT=new_rec.INT3AMT
 ,INT4AMT=new_rec.INT4AMT
 ,INT5AMT=new_rec.INT5AMT
 ,INT6AMT=new_rec.INT6AMT
 ,ESCROWAMT=new_rec.ESCROWAMT
 ,PI6MOAMT=new_rec.PI6MOAMT
 ,GFDAMT=new_rec.GFDAMT
 ,OPENGNTYAMT=new_rec.OPENGNTYAMT
 ,OPENGNTYERRAMT=new_rec.OPENGNTYERRAMT
 ,OPENGNTYREGAMT=new_rec.OPENGNTYREGAMT
 ,OPENGNTYUNALLOCAMT=new_rec.OPENGNTYUNALLOCAMT
 ,UNALLOCAMT=new_rec.UNALLOCAMT
 ,USERLCK=new_rec.USERLCK
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 ,CDCSBAFEEDISBAMT=new_rec.CDCSBAFEEDISBAMT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In PRPLOANRQSTUPDTSP build 2018-01-23 08:27:23 '
 ||'while doing update on STGCSA.PRPLOANRQSTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPLOANRQSTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(LOANNMB)=('||P_LOANNMB||') ';
 select rowid into rec_rowid from STGCSA.PRPLOANRQSTTBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In PRPLOANRQSTUPDTSP build 2018-01-23 08:27:23 '
 ||'No STGCSA.PRPLOANRQSTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In PRPLOANRQSTUPDTSP build 2018-01-23 08:27:23 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PRPLOANRQSTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In PRPLOANRQSTUPDTSP build 2018-01-23 08:27:23'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END PRPLOANRQSTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


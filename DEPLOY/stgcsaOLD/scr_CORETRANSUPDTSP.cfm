<!--- Saved 01/28/2018 14:41:50. --->
PROCEDURE CORETRANSUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_STATIDCUR NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_TRANSID NUMBER:=null
 ,p_TRANSIND1 NUMBER:=null
 ,p_TRANSTYPID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-24 20:12:26
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-24 20:12:26
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure CORETRANSUPDTSP performs UPDATE on STGCSA.CORETRANSTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: TRANSID
 for P_IDENTIFIER=1 qualification is on TRANSID, TRANSTYPID
 for P_IDENTIFIER=2 qualification is on LOANNMB
 for P_IDENTIFIER=3 qualification is on TRANSID, LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.CORETRANSTBL%rowtype;
 new_rec STGCSA.CORETRANSTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.CORETRANSTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'Select of STGCSA.CORETRANSTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO CORETRANSUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_TRANSID is not null then
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=P_TRANSID; 
 end if;
 if P_TRANSTYPID is not null then
 cur_col_name:='P_TRANSTYPID';
 new_rec.TRANSTYPID:=P_TRANSTYPID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_TRANSIND1 is not null then
 cur_col_name:='P_TRANSIND1';
 new_rec.TRANSIND1:=P_TRANSIND1; 
 end if;
 if P_STATIDCUR is not null then
 cur_col_name:='P_STATIDCUR';
 new_rec.STATIDCUR:=P_STATIDCUR; 
 end if;
 if P_TIMESTAMPFLD is not null then
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.CORETRANSAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CORETRANSTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,TRANSTYPID=new_rec.TRANSTYPID
 ,LOANNMB=new_rec.LOANNMB
 ,TRANSIND1=new_rec.TRANSIND1
 ,STATIDCUR=new_rec.STATIDCUR
 ,TIMESTAMPFLD=new_rec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'while doing update on STGCSA.CORETRANSTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO CORETRANSUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CORETRANSUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_TRANSID:=P_TRANSID; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(TRANSID)=('||P_TRANSID||') ';
 select rowid into rec_rowid from STGCSA.CORETRANSTBL where 1=1
 and TRANSID=P_TRANSID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'No STGCSA.CORETRANSTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 
 
 keystouse:='(TRANSID)=('||P_TRANSID||') '||
 '(TRANSTYPID)=('||P_TRANSTYPID||') '||'';
 begin
 for r1 in (select rowid from STGCSA.CORETRANSTBL a where 1=1 
 and TRANSID=P_TRANSID
 and TRANSTYPID=P_TRANSTYPID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'Oracle returned error fetching STGCSA.CORETRANSTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'No STGCSA.CORETRANSTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||'';
 begin
 for r2 in (select rowid from STGCSA.CORETRANSTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'Oracle returned error fetching STGCSA.CORETRANSTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'No STGCSA.CORETRANSTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to update one or more rows based on keyset KEYS3
 if p_errval=0 then
 
 
 keystouse:='(LOANNMB)=('||P_LOANNMB||') '||
 '(TRANSID)=('||P_TRANSID||') '||'';
 begin
 for r3 in (select rowid from STGCSA.CORETRANSTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'Oracle returned error fetching STGCSA.CORETRANSTBL row(s) with key(s) '
 ||keystouse;
 end;
 if recnum=0 then 
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'No STGCSA.CORETRANSTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CORETRANSUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_TRANSID:=new_rec.TRANSID; log_LOANNMB:=new_rec.LOANNMB; log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CORETRANSUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CORETRANSUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In CORETRANSUPDTSP build 2018-01-24 20:12:26'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CORETRANSUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END CORETRANSUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


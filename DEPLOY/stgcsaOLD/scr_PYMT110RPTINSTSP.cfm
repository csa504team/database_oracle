<!--- Saved 01/28/2018 14:42:23. --->
PROCEDURE PYMT110RPTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PYMT110ID NUMBER:=null
 ,p_TRANSID NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_POSTAMT NUMBER:=null
 ,p_PRINAMT NUMBER:=null
 ,p_INTAMT NUMBER:=null
 ,p_UNALLOCAMT NUMBER:=null
 ,p_CSAAMT NUMBER:=null
 ,p_CDCAMT NUMBER:=null
 ,p_CSARAMT NUMBER:=null
 ,p_CDCRAMT NUMBER:=null
 ,p_LFAMT NUMBER:=null
 ,p_SBAAMT NUMBER:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-01-27 15:31:30
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-27 15:31:30
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PYMT110RPTTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.PYMT110RPTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint PYMT110RPTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 log_TRANSID:=P_TRANSID; log_LOANNMB:=P_LOANNMB; log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_PYMT110ID';
 new_rec.PYMT110ID:=runtime.validate_column_value(P_PYMT110ID,
 'STGCSA','PYMT110RPTTBL','PYMT110ID','NUMBER',22);
 cur_col_name:='P_TRANSID';
 new_rec.TRANSID:=runtime.validate_column_value(P_TRANSID,
 'STGCSA','PYMT110RPTTBL','TRANSID','NUMBER',22);
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=runtime.validate_column_value(P_LOANNMB,
 'STGCSA','PYMT110RPTTBL','LOANNMB','CHAR',10);
 cur_col_name:='P_POSTAMT';
 new_rec.POSTAMT:=runtime.validate_column_value(P_POSTAMT,
 'STGCSA','PYMT110RPTTBL','POSTAMT','NUMBER',22);
 cur_col_name:='P_PRINAMT';
 new_rec.PRINAMT:=runtime.validate_column_value(P_PRINAMT,
 'STGCSA','PYMT110RPTTBL','PRINAMT','NUMBER',22);
 cur_col_name:='P_INTAMT';
 new_rec.INTAMT:=runtime.validate_column_value(P_INTAMT,
 'STGCSA','PYMT110RPTTBL','INTAMT','NUMBER',22);
 cur_col_name:='P_UNALLOCAMT';
 new_rec.UNALLOCAMT:=runtime.validate_column_value(P_UNALLOCAMT,
 'STGCSA','PYMT110RPTTBL','UNALLOCAMT','NUMBER',22);
 cur_col_name:='P_CSAAMT';
 new_rec.CSAAMT:=runtime.validate_column_value(P_CSAAMT,
 'STGCSA','PYMT110RPTTBL','CSAAMT','NUMBER',22);
 cur_col_name:='P_CDCAMT';
 new_rec.CDCAMT:=runtime.validate_column_value(P_CDCAMT,
 'STGCSA','PYMT110RPTTBL','CDCAMT','NUMBER',22);
 cur_col_name:='P_CSARAMT';
 new_rec.CSARAMT:=runtime.validate_column_value(P_CSARAMT,
 'STGCSA','PYMT110RPTTBL','CSARAMT','NUMBER',22);
 cur_col_name:='P_CDCRAMT';
 new_rec.CDCRAMT:=runtime.validate_column_value(P_CDCRAMT,
 'STGCSA','PYMT110RPTTBL','CDCRAMT','NUMBER',22);
 cur_col_name:='P_LFAMT';
 new_rec.LFAMT:=runtime.validate_column_value(P_LFAMT,
 'STGCSA','PYMT110RPTTBL','LFAMT','NUMBER',22);
 cur_col_name:='P_SBAAMT';
 new_rec.SBAAMT:=runtime.validate_column_value(P_SBAAMT,
 'STGCSA','PYMT110RPTTBL','SBAAMT','NUMBER',22);
 cur_col_name:='P_TIMESTAMPFLD';
 new_rec.TIMESTAMPFLD:=runtime.validate_column_value(P_TIMESTAMPFLD,
 'STGCSA','PYMT110RPTTBL','TIMESTAMPFLD','DATE',7);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO PYMT110RPTINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.PYMT110RPTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.PYMT110RPTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO PYMT110RPTINSTSP;
 p_errmsg:='Got dupe key on insert into PYMT110RPTTBL for key '
 ||'(PYMT110ID)=('||new_rec.PYMT110ID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in PYMT110RPTINSTSP build 2018-01-27 15:31:30';
 ROLLBACK TO PYMT110RPTINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO PYMT110RPTINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PYMT110RPTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'PYMT110RPTINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in PYMT110RPTINSTSP build 2018-01-27 15:31:30'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO PYMT110RPTINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END PYMT110RPTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/28/2018 14:42:53. --->
PROCEDURE TEMPCDAMORTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDAMORTID NUMBER:=null
 ,p_CDC_AMT NUMBER:=null
 ,p_CDC_FEE_NEEDED NUMBER:=null
 ,p_CSA_AMT NUMBER:=null
 ,p_CSA_FEE_NEEDED NUMBER:=null
 ,p_FIRSTPAYMENTDT DATE:=null
 ,p_INT_AMT NUMBER:=null
 ,p_INT_NEEDED NUMBER:=null
 ,p_INT_PYMT NUMBER:=null
 ,p_LASTPAYMENTDT DATE:=null
 ,p_LATE_AMT NUMBER:=null
 ,p_LATE_FEE_NEEDED NUMBER:=null
 ,p_LOANNMB CHAR:=null
 ,p_PD DATE:=null
 ,p_PD_BEGIN_PRIN NUMBER:=null
 ,p_PRIN_AMT NUMBER:=null
 ,p_PRIN_BALANCE NUMBER:=null
 ,p_PRIN_NEEDED NUMBER:=null
 ,p_PRIN_PYMT NUMBER:=null
 ,p_PYMT_AMT NUMBER:=null
 ,p_SBA_AMT NUMBER:=null
 ,p_SBA_FEE_NEEDED NUMBER:=null
 ,p_STATUS VARCHAR2:=null
 ,p_TOTAL_PRIN NUMBER:=null
 ,p_TYPENM VARCHAR2:=null
 ,p_UNALLOCATED_AMT NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:36:14
 Created by: GENR
 Crerated from template stdtblupd_template v3.12 16 Jan 2017 on 2018-01-23 08:36:14
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure TEMPCDAMORTUPDTSP performs UPDATE on STGCSA.TEMPCDAMORTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CDAMORTID
 Updates all columns except the qualification keys and columns for which a
 null value is passed. Logging to audit is included if required for this table.
 */
 old_rec STGCSA.TEMPCDAMORTTBL%rowtype;
 new_rec STGCSA.TEMPCDAMORTTBL%rowtype;
 logged_msg_id number;
 audretval number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 -- standard activity log capture vard
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval<>0 then
 return;
 end if;
 begin
 select * into old_rec from STGCSA.TEMPCDAMORTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPCDAMORTUPDTSP build 2018-01-23 08:36:14 '
 ||'Select of STGCSA.TEMPCDAMORTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPCDAMORTUPDTSP;
 end;
 if p_errval=0 then
 -- new rec starts with old rec
 new_rec:=old_rec;
 -- set this as lastupdt
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 -- copy into new rec all table column input parameters that are not null
 runtime.errfound:=false;
 runtime.errstr:=' ';
 if P_CDAMORTID is not null then
 cur_col_name:='P_CDAMORTID';
 new_rec.CDAMORTID:=P_CDAMORTID; 
 end if;
 if P_LOANNMB is not null then
 cur_col_name:='P_LOANNMB';
 new_rec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_PD is not null then
 cur_col_name:='P_PD';
 new_rec.PD:=P_PD; 
 end if;
 if P_PD_BEGIN_PRIN is not null then
 cur_col_name:='P_PD_BEGIN_PRIN';
 new_rec.PD_BEGIN_PRIN:=P_PD_BEGIN_PRIN; 
 end if;
 if P_PYMT_AMT is not null then
 cur_col_name:='P_PYMT_AMT';
 new_rec.PYMT_AMT:=P_PYMT_AMT; 
 end if;
 if P_PRIN_AMT is not null then
 cur_col_name:='P_PRIN_AMT';
 new_rec.PRIN_AMT:=P_PRIN_AMT; 
 end if;
 if P_INT_AMT is not null then
 cur_col_name:='P_INT_AMT';
 new_rec.INT_AMT:=P_INT_AMT; 
 end if;
 if P_CDC_AMT is not null then
 cur_col_name:='P_CDC_AMT';
 new_rec.CDC_AMT:=P_CDC_AMT; 
 end if;
 if P_SBA_AMT is not null then
 cur_col_name:='P_SBA_AMT';
 new_rec.SBA_AMT:=P_SBA_AMT; 
 end if;
 if P_CSA_AMT is not null then
 cur_col_name:='P_CSA_AMT';
 new_rec.CSA_AMT:=P_CSA_AMT; 
 end if;
 if P_LATE_AMT is not null then
 cur_col_name:='P_LATE_AMT';
 new_rec.LATE_AMT:=P_LATE_AMT; 
 end if;
 if P_PRIN_BALANCE is not null then
 cur_col_name:='P_PRIN_BALANCE';
 new_rec.PRIN_BALANCE:=P_PRIN_BALANCE; 
 end if;
 if P_PRIN_NEEDED is not null then
 cur_col_name:='P_PRIN_NEEDED';
 new_rec.PRIN_NEEDED:=P_PRIN_NEEDED; 
 end if;
 if P_INT_NEEDED is not null then
 cur_col_name:='P_INT_NEEDED';
 new_rec.INT_NEEDED:=P_INT_NEEDED; 
 end if;
 if P_CDC_FEE_NEEDED is not null then
 cur_col_name:='P_CDC_FEE_NEEDED';
 new_rec.CDC_FEE_NEEDED:=P_CDC_FEE_NEEDED; 
 end if;
 if P_SBA_FEE_NEEDED is not null then
 cur_col_name:='P_SBA_FEE_NEEDED';
 new_rec.SBA_FEE_NEEDED:=P_SBA_FEE_NEEDED; 
 end if;
 if P_CSA_FEE_NEEDED is not null then
 cur_col_name:='P_CSA_FEE_NEEDED';
 new_rec.CSA_FEE_NEEDED:=P_CSA_FEE_NEEDED; 
 end if;
 if P_LATE_FEE_NEEDED is not null then
 cur_col_name:='P_LATE_FEE_NEEDED';
 new_rec.LATE_FEE_NEEDED:=P_LATE_FEE_NEEDED; 
 end if;
 if P_UNALLOCATED_AMT is not null then
 cur_col_name:='P_UNALLOCATED_AMT';
 new_rec.UNALLOCATED_AMT:=P_UNALLOCATED_AMT; 
 end if;
 if P_PRIN_PYMT is not null then
 cur_col_name:='P_PRIN_PYMT';
 new_rec.PRIN_PYMT:=P_PRIN_PYMT; 
 end if;
 if P_INT_PYMT is not null then
 cur_col_name:='P_INT_PYMT';
 new_rec.INT_PYMT:=P_INT_PYMT; 
 end if;
 if P_TOTAL_PRIN is not null then
 cur_col_name:='P_TOTAL_PRIN';
 new_rec.TOTAL_PRIN:=P_TOTAL_PRIN; 
 end if;
 if P_STATUS is not null then
 cur_col_name:='P_STATUS';
 new_rec.STATUS:=P_STATUS; 
 end if;
 if P_TYPENM is not null then
 cur_col_name:='P_TYPENM';
 new_rec.TYPENM:=P_TYPENM; 
 end if;
 if P_FIRSTPAYMENTDT is not null then
 cur_col_name:='P_FIRSTPAYMENTDT';
 new_rec.FIRSTPAYMENTDT:=P_FIRSTPAYMENTDT; 
 end if;
 if P_LASTPAYMENTDT is not null then
 cur_col_name:='P_LASTPAYMENTDT';
 new_rec.LASTPAYMENTDT:=P_LASTPAYMENTDT; 
 end if;
 cur_col_name:=null;
 end if;
 if p_errval=0 then
 -- allow audit to record changed fields as needed
 stgcsa.TEMPCDAMORTAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.TEMPCDAMORTTBL
 set lastupdtuserid=new_rec.lastupdtuserid,
 lastupdtdt=new_rec.lastupdtdt
 ,LOANNMB=new_rec.LOANNMB
 ,PD=new_rec.PD
 ,PD_BEGIN_PRIN=new_rec.PD_BEGIN_PRIN
 ,PYMT_AMT=new_rec.PYMT_AMT
 ,PRIN_AMT=new_rec.PRIN_AMT
 ,INT_AMT=new_rec.INT_AMT
 ,CDC_AMT=new_rec.CDC_AMT
 ,SBA_AMT=new_rec.SBA_AMT
 ,CSA_AMT=new_rec.CSA_AMT
 ,LATE_AMT=new_rec.LATE_AMT
 ,PRIN_BALANCE=new_rec.PRIN_BALANCE
 ,PRIN_NEEDED=new_rec.PRIN_NEEDED
 ,INT_NEEDED=new_rec.INT_NEEDED
 ,CDC_FEE_NEEDED=new_rec.CDC_FEE_NEEDED
 ,SBA_FEE_NEEDED=new_rec.SBA_FEE_NEEDED
 ,CSA_FEE_NEEDED=new_rec.CSA_FEE_NEEDED
 ,LATE_FEE_NEEDED=new_rec.LATE_FEE_NEEDED
 ,UNALLOCATED_AMT=new_rec.UNALLOCATED_AMT
 ,PRIN_PYMT=new_rec.PRIN_PYMT
 ,INT_PYMT=new_rec.INT_PYMT
 ,TOTAL_PRIN=new_rec.TOTAL_PRIN
 ,STATUS=new_rec.STATUS
 ,TYPENM=new_rec.TYPENM
 ,FIRSTPAYMENTDT=new_rec.FIRSTPAYMENTDT
 ,LASTPAYMENTDT=new_rec.LASTPAYMENTDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:= ' In TEMPCDAMORTUPDTSP build 2018-01-23 08:36:14 '
 ||'while doing update on STGCSA.TEMPCDAMORTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got SQLERR '||sqlerrm(sqlcode);
 ROLLBACK TO TEMPCDAMORTUPDTSP;
 runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint TEMPCDAMORTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 --
 log_LOANNMB:=P_LOANNMB;
 case p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 -- standard caution... only proceed if no error
 if p_errval=0 then
 begin
 keystouse:='(CDAMORTID)=('||P_CDAMORTID||') ';
 select rowid into rec_rowid from STGCSA.TEMPCDAMORTTBL where 1=1
 and CDAMORTID=P_CDAMORTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 p_errmsg:=' In TEMPCDAMORTUPDTSP build 2018-01-23 08:36:14 '
 ||'No STGCSA.TEMPCDAMORTTBL row to update with key(s) '||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO TEMPCDAMORTUPDTSP;
 p_errval:=-20002;
 p_errmsg:=' In TEMPCDAMORTUPDTSP build 2018-01-23 08:36:14 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 log_LOANNMB:=new_rec.LOANNMB;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPCDAMORTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'TEMPCDAMORTUPDTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In TEMPCDAMORTUPDTSP build 2018-01-23 08:36:14'||cur_col_name
 ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO TEMPCDAMORTUPDTSP;
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 --
 END TEMPCDAMORTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


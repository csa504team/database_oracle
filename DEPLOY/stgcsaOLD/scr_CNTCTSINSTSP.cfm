<!--- Saved 01/28/2018 14:41:38. --->
PROCEDURE CNTCTSINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCNMB CHAR:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_LASTNMCNTCT VARCHAR2:=null
 ,p_CNTCTMAINPHNNMB CHAR:=null
 ,p_CNTCTSERVICINGPHNNMB CHAR:=null
 ,p_CNTCTFUNDINGPHNNMB CHAR:=null
 ,p_BOUNCEDCHK VARCHAR2:=null
 ,p_MAILDOCPOC VARCHAR2:=null
 ,p_PROBLEMLOANMTHDOFCNTCT VARCHAR2:=null
 ,p_BUSEMAILADR VARCHAR2:=null
 ,p_CNTCTBUSPHNNMB VARCHAR2:=null
 ,p_CNTCTBUSFAXNMB VARCHAR2:=null
 ,p_CNTCTCDC VARCHAR2:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTSID NUMBER:=null
 ,p_MAINCNTCT VARCHAR2:=null
 ,p_SERVICINGCNTCTNM VARCHAR2:=null
 ,p_FUNDINGCNTCT VARCHAR2:=null
 ,p_CDCID NUMBER:=null
 ) as
 /*
 Created on: 2018-01-23 08:18:11
 Created by: GENR
 Crerated from template stdtblins_template v2.12 16 Jan 2017 on 2018-01-23 08:18:11
 Using SNAP V3.03 20 Dec 2017 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CNTCTSTBLfor P_IDENTIFIER=0
 */
 new_rec STGCSA.CNTCTSTBL%rowtype;
 logged_msg_id number;
 audretval number;
 crossover_active char(1);
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 begin
 -- setup
 savepoint CNTCTSINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 runtime.errfound:=false;
 runtime.errstr:=' ';
 cur_col_name:='P_CDCNMB';
 new_rec.CDCNMB:=runtime.validate_column_value(P_CDCNMB,
 'STGCSA','CNTCTSTBL','CDCNMB','CHAR',8);
 cur_col_name:='P_CNTCTFIRSTNM';
 new_rec.CNTCTFIRSTNM:=runtime.validate_column_value(P_CNTCTFIRSTNM,
 'STGCSA','CNTCTSTBL','CNTCTFIRSTNM','VARCHAR2',80);
 cur_col_name:='P_LASTNMCNTCT';
 new_rec.LASTNMCNTCT:=runtime.validate_column_value(P_LASTNMCNTCT,
 'STGCSA','CNTCTSTBL','LASTNMCNTCT','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAINPHNNMB';
 new_rec.CNTCTMAINPHNNMB:=runtime.validate_column_value(P_CNTCTMAINPHNNMB,
 'STGCSA','CNTCTSTBL','CNTCTMAINPHNNMB','CHAR',1);
 cur_col_name:='P_CNTCTSERVICINGPHNNMB';
 new_rec.CNTCTSERVICINGPHNNMB:=runtime.validate_column_value(P_CNTCTSERVICINGPHNNMB,
 'STGCSA','CNTCTSTBL','CNTCTSERVICINGPHNNMB','CHAR',1);
 cur_col_name:='P_CNTCTFUNDINGPHNNMB';
 new_rec.CNTCTFUNDINGPHNNMB:=runtime.validate_column_value(P_CNTCTFUNDINGPHNNMB,
 'STGCSA','CNTCTSTBL','CNTCTFUNDINGPHNNMB','CHAR',1);
 cur_col_name:='P_BOUNCEDCHK';
 new_rec.BOUNCEDCHK:=runtime.validate_column_value(P_BOUNCEDCHK,
 'STGCSA','CNTCTSTBL','BOUNCEDCHK','VARCHAR2',255);
 cur_col_name:='P_MAILDOCPOC';
 new_rec.MAILDOCPOC:=runtime.validate_column_value(P_MAILDOCPOC,
 'STGCSA','CNTCTSTBL','MAILDOCPOC','VARCHAR2',255);
 cur_col_name:='P_PROBLEMLOANMTHDOFCNTCT';
 new_rec.PROBLEMLOANMTHDOFCNTCT:=runtime.validate_column_value(P_PROBLEMLOANMTHDOFCNTCT,
 'STGCSA','CNTCTSTBL','PROBLEMLOANMTHDOFCNTCT','VARCHAR2',255);
 cur_col_name:='P_BUSEMAILADR';
 new_rec.BUSEMAILADR:=runtime.validate_column_value(P_BUSEMAILADR,
 'STGCSA','CNTCTSTBL','BUSEMAILADR','VARCHAR2',80);
 cur_col_name:='P_CNTCTBUSPHNNMB';
 new_rec.CNTCTBUSPHNNMB:=runtime.validate_column_value(P_CNTCTBUSPHNNMB,
 'STGCSA','CNTCTSTBL','CNTCTBUSPHNNMB','VARCHAR2',30);
 cur_col_name:='P_CNTCTBUSFAXNMB';
 new_rec.CNTCTBUSFAXNMB:=runtime.validate_column_value(P_CNTCTBUSFAXNMB,
 'STGCSA','CNTCTSTBL','CNTCTBUSFAXNMB','VARCHAR2',20);
 cur_col_name:='P_CNTCTCDC';
 new_rec.CNTCTCDC:=runtime.validate_column_value(P_CNTCTCDC,
 'STGCSA','CNTCTSTBL','CNTCTCDC','VARCHAR2',255);
 cur_col_name:='P_CNTCTMAILADRSTR1NM';
 new_rec.CNTCTMAILADRSTR1NM:=runtime.validate_column_value(P_CNTCTMAILADRSTR1NM,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRSTR1NM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRSTR2NM';
 new_rec.CNTCTMAILADRSTR2NM:=runtime.validate_column_value(P_CNTCTMAILADRSTR2NM,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRSTR2NM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRCTYNM';
 new_rec.CNTCTMAILADRCTYNM:=runtime.validate_column_value(P_CNTCTMAILADRCTYNM,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRCTYNM','VARCHAR2',80);
 cur_col_name:='P_CNTCTMAILADRSTCD';
 new_rec.CNTCTMAILADRSTCD:=runtime.validate_column_value(P_CNTCTMAILADRSTCD,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRSTCD','CHAR',2);
 cur_col_name:='P_CNTCTMAILADRZIPCD';
 new_rec.CNTCTMAILADRZIPCD:=runtime.validate_column_value(P_CNTCTMAILADRZIPCD,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRZIPCD','CHAR',5);
 cur_col_name:='P_CNTCTMAILADRZIP4CD';
 new_rec.CNTCTMAILADRZIP4CD:=runtime.validate_column_value(P_CNTCTMAILADRZIP4CD,
 'STGCSA','CNTCTSTBL','CNTCTMAILADRZIP4CD','CHAR',4);
 cur_col_name:='P_CNTCTSID';
 new_rec.CNTCTSID:=runtime.validate_column_value(P_CNTCTSID,
 'STGCSA','CNTCTSTBL','CNTCTSID','NUMBER',22);
 cur_col_name:='P_MAINCNTCT';
 new_rec.MAINCNTCT:=runtime.validate_column_value(P_MAINCNTCT,
 'STGCSA','CNTCTSTBL','MAINCNTCT','VARCHAR2',255);
 cur_col_name:='P_SERVICINGCNTCTNM';
 new_rec.SERVICINGCNTCTNM:=runtime.validate_column_value(P_SERVICINGCNTCTNM,
 'STGCSA','CNTCTSTBL','SERVICINGCNTCTNM','VARCHAR2',80);
 cur_col_name:='P_FUNDINGCNTCT';
 new_rec.FUNDINGCNTCT:=runtime.validate_column_value(P_FUNDINGCNTCT,
 'STGCSA','CNTCTSTBL','FUNDINGCNTCT','VARCHAR2',255);
 cur_col_name:='P_CDCID';
 new_rec.CDCID:=runtime.validate_column_value(P_CDCID,
 'STGCSA','CNTCTSTBL','CDCID','NUMBER',22);
 cur_col_name:=null;
 -- did validatrion find errors?
 if runtime.errfound=true then
 ROLLBACK TO CNTCTSINSTSP;
 p_errval:=-20002;
 p_errmsg:='One or more missing, non-defaultable inputs: '||substr(runtime.errstr,3);
 end if;
 -- allow audit to record changed fields as needed
 if p_errval=0 then
 stgcsa.CNTCTSAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'A','?',NUlL,new_rec);
 end if;
 -- if all still ok do actual delete
 if p_errval=0 then
 begin
 insert into STGCSA.CNTCTSTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception when others then
 p_errval:=sqlcode;
 if p_errval=-100 then
 -- dupe key on insert, app issue, set return
 ROLLBACK TO CNTCTSINSTSP;
 p_errmsg:='Got dupe key on insert into CNTCTSTBL for key '
 ||'(CNTCTSID)=('||new_rec.CNTCTSID||') ';
 else
 p_errmsg:='Got Oracle error:'||SQLERRM(SQLCODE)
 ||' in CNTCTSINSTSP build 2018-01-23 08:18:11';
 ROLLBACK TO CNTCTSINSTSP;
 end if;
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 ROLLBACK TO CNTCTSINSTSP;
 p_errval:=-20002;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CNTCTSINSTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 else
 runtime.logger(logged_msg_id,'STDERR',103,
 'CNTCTSINSTSP returned non-zero, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg,p_userid,
 2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 if cur_col_name is not null then
 cur_col_name:=' moving input parm '||cur_col_name||' to DB Column';
 end if;
 p_errmsg:=' In outer exception handler in CNTCTSINSTSP build 2018-01-23 08:18:11'
 ||cur_col_name||' caught Oracle error:'||SQLERRM(SQLCODE);
 ROLLBACK TO CNTCTSINSTSP;
 runtime.logger(logged_msg_id,'STDERR',20,p_errmsg,p_userid);
 --
 END CNTCTSINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


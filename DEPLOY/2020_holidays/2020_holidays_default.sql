define deploy_name=2020_holidays
define package_name=default
define package_buildtime=20191118135935
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy 2020_holidays_default created on Mon 11/18/2019 13:59:45.62 by johnlow
prompt deploy scripts for deploy 2020_holidays_default created on Mon 11/18/2019 13:59:45.62 by johnlow
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy 2020_holidays_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Misc\2020_insert_holidays.sql 
Zeeshan Muhammad committed 62f52b8 on Mon Nov 18 16:54:17 2019 +0000

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Misc\2020_insert_holidays.sql"
DELETE FROM cdconline.holidaystbl WHERE holidaydt >= TO_DATE('01/01/2020' ,'mm/dd/yyyy');

INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('01/01/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('01/20/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('02/17/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('05/25/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('07/03/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
--INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('07/04/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('09/07/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('10/12/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('11/11/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('11/26/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('12/25/2020','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);

COMMIT;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

 
 --compile std procs and triggers
 prompt ACCACCELERATELOAN                                                       
@@..\..\storedprocs\ACCACCELERATELOANUPDTSP.sql                                 
@@..\..\storedprocs\ACCACCELERATELOANINSTSP.sql                                 
@@..\..\storedprocs\ACCACCELERATELOANDELTSP.sql                                 
@@..\..\storedprocs\ACCACCELERATELOANTRG.sql                                    
/                                                       
                                                                                
 prompt ACCCYCPRCS                                                              
@@..\..\storedprocs\ACCCYCPRCSUPDTSP.sql                                        
@@..\..\storedprocs\ACCCYCPRCSINSTSP.sql                                        
@@..\..\storedprocs\ACCCYCPRCSDELTSP.sql                                        
@@..\..\storedprocs\ACCCYCPRCSTRG.sql                                           
/                                                       
                                                                                
 prompt ACCPRCSCYCSTAT                                                          
@@..\..\storedprocs\ACCPRCSCYCSTATUPDTSP.sql                                    
@@..\..\storedprocs\ACCPRCSCYCSTATINSTSP.sql                                    
@@..\..\storedprocs\ACCPRCSCYCSTATDELTSP.sql                                    
@@..\..\storedprocs\ACCPRCSCYCSTATTRG.sql                                       
/                                                       
                                                                                
 prompt ACCPRCSCYC                                                              
@@..\..\storedprocs\ACCPRCSCYCUPDTSP.sql                                        
@@..\..\storedprocs\ACCPRCSCYCINSTSP.sql                                        
@@..\..\storedprocs\ACCPRCSCYCDELTSP.sql                                        
@@..\..\storedprocs\ACCPRCSCYCTRG.sql                                           
/                                                       
                                                                                
 prompt CHRTOPNGNTYSALL                                                         
@@..\..\storedprocs\CHRTOPNGNTYSALLUPDTSP.sql                                   
@@..\..\storedprocs\CHRTOPNGNTYSALLINSTSP.sql                                   
@@..\..\storedprocs\CHRTOPNGNTYSALLDELTSP.sql                                   
@@..\..\storedprocs\CHRTOPNGNTYSALLTRG.sql                                      
/                                                       
                                                                                
 prompt CNTCTATTR                                                               
@@..\..\storedprocs\CNTCTATTRUPDTSP.sql                                         
@@..\..\storedprocs\CNTCTATTRINSTSP.sql                                         
@@..\..\storedprocs\CNTCTATTRDELTSP.sql                                         
@@..\..\storedprocs\CNTCTATTRTRG.sql                                            
/                                                       
                                                                                
 prompt CNTCTROLE                                                               
@@..\..\storedprocs\CNTCTROLEUPDTSP.sql                                         
@@..\..\storedprocs\CNTCTROLEINSTSP.sql                                         
@@..\..\storedprocs\CNTCTROLEDELTSP.sql                                         
@@..\..\storedprocs\CNTCTROLETRG.sql                                            
/                                                       
                                                                                
 prompt CNTCTS                                                                  
@@..\..\storedprocs\CNTCTSUPDTSP.sql                                            
@@..\..\storedprocs\CNTCTSINSTSP.sql                                            
@@..\..\storedprocs\CNTCTSDELTSP.sql                                            
@@..\..\storedprocs\CNTCTSTRG.sql                                               
/                                                       
                                                                                
 prompt CNTCT                                                                   
@@..\..\storedprocs\CNTCTUPDTSP.sql                                             
@@..\..\storedprocs\CNTCTINSTSP.sql                                             
@@..\..\storedprocs\CNTCTDELTSP.sql                                             
@@..\..\storedprocs\CNTCTTRG.sql                                                
/                                                       
                                                                                
 prompt COMPRPT                                                                 
@@..\..\storedprocs\COMPRPTUPDTSP.sql                                           
@@..\..\storedprocs\COMPRPTINSTSP.sql                                           
@@..\..\storedprocs\COMPRPTDELTSP.sql                                           
@@..\..\storedprocs\COMPRPTTRG.sql                                              
/                                                       
                                                                                
 prompt CORECONTROLVAL                                                          
@@..\..\storedprocs\CORECONTROLVALUPDTSP.sql                                    
@@..\..\storedprocs\CORECONTROLVALINSTSP.sql                                    
@@..\..\storedprocs\CORECONTROLVALDELTSP.sql                                    
@@..\..\storedprocs\CORECONTROLVALTRG.sql                                       
/                                                       
                                                                                
 prompt COREEMPATTR                                                             
@@..\..\storedprocs\COREEMPATTRUPDTSP.sql                                       
@@..\..\storedprocs\COREEMPATTRINSTSP.sql                                       
@@..\..\storedprocs\COREEMPATTRDELTSP.sql                                       
@@..\..\storedprocs\COREEMPATTRTRG.sql                                          
/                                                       
                                                                                
 prompt COREEMPATTRTYP                                                          
@@..\..\storedprocs\COREEMPATTRTYPUPDTSP.sql                                    
@@..\..\storedprocs\COREEMPATTRTYPINSTSP.sql                                    
@@..\..\storedprocs\COREEMPATTRTYPDELTSP.sql                                    
@@..\..\storedprocs\COREEMPATTRTYPTRG.sql                                       
/                                                       
                                                                                
 prompt COREEMP                                                                 
@@..\..\storedprocs\COREEMPUPDTSP.sql                                           
@@..\..\storedprocs\COREEMPINSTSP.sql                                           
@@..\..\storedprocs\COREEMPDELTSP.sql                                           
@@..\..\storedprocs\COREEMPTRG.sql                                              
/                                                       
                                                                                
 prompt CORERPT                                                                 
@@..\..\storedprocs\CORERPTUPDTSP.sql                                           
@@..\..\storedprocs\CORERPTINSTSP.sql                                           
@@..\..\storedprocs\CORERPTDELTSP.sql                                           
@@..\..\storedprocs\CORERPTTRG.sql                                              
/                                                       
                                                                                
 prompt CORETRANSATTRERR                                                        
@@..\..\storedprocs\CORETRANSATTRERRUPDTSP.sql                                  
@@..\..\storedprocs\CORETRANSATTRERRINSTSP.sql                                  
@@..\..\storedprocs\CORETRANSATTRERRDELTSP.sql                                  
@@..\..\storedprocs\CORETRANSATTRERRTRG.sql                                     
/                                                       
                                                                                
 prompt CORETRANSATTR                                                           
@@..\..\storedprocs\CORETRANSATTRUPDTSP.sql                                     
@@..\..\storedprocs\CORETRANSATTRINSTSP.sql                                     
@@..\..\storedprocs\CORETRANSATTRDELTSP.sql                                     
@@..\..\storedprocs\CORETRANSATTRTRG.sql                                        
/                                                       
                                                                                
 prompt CORETRANSMEMOATTR                                                       
@@..\..\storedprocs\CORETRANSMEMOATTRUPDTSP.sql                                 
@@..\..\storedprocs\CORETRANSMEMOATTRINSTSP.sql                                 
@@..\..\storedprocs\CORETRANSMEMOATTRDELTSP.sql                                 
@@..\..\storedprocs\CORETRANSMEMOATTRTRG.sql                                    
/                                                       
                                                                                
 prompt CORETRANSSTAT                                                           
@@..\..\storedprocs\CORETRANSSTATUPDTSP.sql                                     
@@..\..\storedprocs\CORETRANSSTATINSTSP.sql                                     
@@..\..\storedprocs\CORETRANSSTATDELTSP.sql                                     
@@..\..\storedprocs\CORETRANSSTATTRG.sql                                        
/                                                       
                                                                                
 prompt CORETRANS                                                               
@@..\..\storedprocs\CORETRANSUPDTSP.sql                                         
@@..\..\storedprocs\CORETRANSINSTSP.sql                                         
@@..\..\storedprocs\CORETRANSDELTSP.sql                                         
@@..\..\storedprocs\CORETRANSTRG.sql                                            
/                                                       
                                                                                
 prompt ENT                                                                     
@@..\..\storedprocs\ENTUPDTSP.sql                                               
@@..\..\storedprocs\ENTINSTSP.sql                                               
@@..\..\storedprocs\ENTDELTSP.sql                                               
@@..\..\storedprocs\ENTTRG.sql                                                  
/                                                       
                                                                                
 prompt GENAPPV                                                                 
@@..\..\storedprocs\GENAPPVUPDTSP.sql                                           
@@..\..\storedprocs\GENAPPVINSTSP.sql                                           
@@..\..\storedprocs\GENAPPVDELTSP.sql                                           
@@..\..\storedprocs\GENAPPVTRG.sql                                              
/                                                       
                                                                                
 prompt GENATTCH                                                                
@@..\..\storedprocs\GENATTCHUPDTSP.sql                                          
@@..\..\storedprocs\GENATTCHINSTSP.sql                                          
@@..\..\storedprocs\GENATTCHDELTSP.sql                                          
@@..\..\storedprocs\GENATTCHTRG.sql                                             
/                                                       
                                                                                
 prompt GENCNFRM                                                                
@@..\..\storedprocs\GENCNFRMUPDTSP.sql                                          
@@..\..\storedprocs\GENCNFRMINSTSP.sql                                          
@@..\..\storedprocs\GENCNFRMDELTSP.sql                                          
@@..\..\storedprocs\GENCNFRMTRG.sql                                             
/                                                       
                                                                                
 prompt GENDBFNCTN                                                              
@@..\..\storedprocs\GENDBFNCTNUPDTSP.sql                                        
@@..\..\storedprocs\GENDBFNCTNINSTSP.sql                                        
@@..\..\storedprocs\GENDBFNCTNDELTSP.sql                                        
@@..\..\storedprocs\GENDBFNCTNTRG.sql                                           
/                                                       
                                                                                
 prompt GENEMAILADR                                                             
@@..\..\storedprocs\GENEMAILADRUPDTSP.sql                                       
@@..\..\storedprocs\GENEMAILADRINSTSP.sql                                       
@@..\..\storedprocs\GENEMAILADRDELTSP.sql                                       
@@..\..\storedprocs\GENEMAILADRTRG.sql                                          
/                                                       
                                                                                
 prompt GENEMAILARCHV                                                           
@@..\..\storedprocs\GENEMAILARCHVUPDTSP.sql                                     
@@..\..\storedprocs\GENEMAILARCHVINSTSP.sql                                     
@@..\..\storedprocs\GENEMAILARCHVDELTSP.sql                                     
@@..\..\storedprocs\GENEMAILARCHVTRG.sql                                        
/                                                       
                                                                                
 prompt GENEMAILATTCH                                                           
@@..\..\storedprocs\GENEMAILATTCHUPDTSP.sql                                     
@@..\..\storedprocs\GENEMAILATTCHINSTSP.sql                                     
@@..\..\storedprocs\GENEMAILATTCHDELTSP.sql                                     
@@..\..\storedprocs\GENEMAILATTCHTRG.sql                                        
/                                                       
                                                                                
 prompt GENEMAILCAT                                                             
@@..\..\storedprocs\GENEMAILCATUPDTSP.sql                                       
@@..\..\storedprocs\GENEMAILCATINSTSP.sql                                       
@@..\..\storedprocs\GENEMAILCATDELTSP.sql                                       
@@..\..\storedprocs\GENEMAILCATTRG.sql                                          
/                                                       
                                                                                
 prompt GENEMAILDISTROLISTADR                                                   
@@..\..\storedprocs\GENEMAILDISTROLISTADRUPDTSP.sql                             
@@..\..\storedprocs\GENEMAILDISTROLISTADRINSTSP.sql                             
@@..\..\storedprocs\GENEMAILDISTROLISTADRDELTSP.sql                             
@@..\..\storedprocs\GENEMAILDISTROLISTADRTRG.sql                                
/                                                       
                                                                                
 prompt GENEMAILDISTROLIST                                                      
@@..\..\storedprocs\GENEMAILDISTROLISTUPDTSP.sql                                
@@..\..\storedprocs\GENEMAILDISTROLISTINSTSP.sql                                
@@..\..\storedprocs\GENEMAILDISTROLISTDELTSP.sql                                
@@..\..\storedprocs\GENEMAILDISTROLISTTRG.sql                                   
/                                                       
                                                                                
 prompt GENEMAILLOAN                                                            
@@..\..\storedprocs\GENEMAILLOANUPDTSP.sql                                      
@@..\..\storedprocs\GENEMAILLOANINSTSP.sql                                      
@@..\..\storedprocs\GENEMAILLOANDELTSP.sql                                      
@@..\..\storedprocs\GENEMAILLOANTRG.sql                                         
/                                                       
                                                                                
 prompt GENEMAILTEMPLATE                                                        
@@..\..\storedprocs\GENEMAILTEMPLATEUPDTSP.sql                                  
@@..\..\storedprocs\GENEMAILTEMPLATEINSTSP.sql                                  
@@..\..\storedprocs\GENEMAILTEMPLATEDELTSP.sql                                  
@@..\..\storedprocs\GENEMAILTEMPLATETRG.sql                                     
/                                                       
                                                                                
 prompt GENPACSIMPTDTL                                                          
@@..\..\storedprocs\GENPACSIMPTDTLUPDTSP.sql                                    
@@..\..\storedprocs\GENPACSIMPTDTLINSTSP.sql                                    
@@..\..\storedprocs\GENPACSIMPTDTLDELTSP.sql                                    
@@..\..\storedprocs\GENPACSIMPTDTLTRG.sql                                       
/                                                       
                                                                                
 prompt GENPACSIMPT                                                             
@@..\..\storedprocs\GENPACSIMPTUPDTSP.sql                                       
@@..\..\storedprocs\GENPACSIMPTINSTSP.sql                                       
@@..\..\storedprocs\GENPACSIMPTDELTSP.sql                                       
@@..\..\storedprocs\GENPACSIMPTTRG.sql                                          
/                                                       
                                                                                
 prompt IMPTARPRPT                                                              
@@..\..\storedprocs\IMPTARPRPTUPDTSP.sql                                        
@@..\..\storedprocs\IMPTARPRPTINSTSP.sql                                        
@@..\..\storedprocs\IMPTARPRPTDELTSP.sql                                        
@@..\..\storedprocs\IMPTARPRPTTRG.sql                                           
/                                                       
                                                                                
 prompt IMPTCHKMSTR                                                             
@@..\..\storedprocs\IMPTCHKMSTRUPDTSP.sql                                       
@@..\..\storedprocs\IMPTCHKMSTRINSTSP.sql                                       
@@..\..\storedprocs\IMPTCHKMSTRDELTSP.sql                                       
@@..\..\storedprocs\IMPTCHKMSTRTRG.sql                                          
/                                                       
                                                                                
 prompt PRPLOANRQST                                                             
@@..\..\storedprocs\PRPLOANRQSTUPDTSP.sql                                       
@@..\..\storedprocs\PRPLOANRQSTINSTSP.sql                                       
@@..\..\storedprocs\PRPLOANRQSTDELTSP.sql                                       
@@..\..\storedprocs\PRPLOANRQSTTRG.sql                                          
/                                                       
                                                                                
 prompt PRPLOANRQSTUPDT                                                         
@@..\..\storedprocs\PRPLOANRQSTUPDTUPDTSP.sql                                   
@@..\..\storedprocs\PRPLOANRQSTUPDTINSTSP.sql                                   
@@..\..\storedprocs\PRPLOANRQSTUPDTDELTSP.sql                                   
@@..\..\storedprocs\PRPLOANRQSTUPDTTRG.sql                                      
/                                                       
                                                                                
 prompt PRPMFDUEFROMBORR                                                        
@@..\..\storedprocs\PRPMFDUEFROMBORRUPDTSP.sql                                  
@@..\..\storedprocs\PRPMFDUEFROMBORRINSTSP.sql                                  
@@..\..\storedprocs\PRPMFDUEFROMBORRDELTSP.sql                                  
@@..\..\storedprocs\PRPMFDUEFROMBORRTRG.sql                                     
/                                                       
                                                                                
 prompt PRPMFGNTY                                                               
@@..\..\storedprocs\PRPMFGNTYUPDTSP.sql                                         
@@..\..\storedprocs\PRPMFGNTYINSTSP.sql                                         
@@..\..\storedprocs\PRPMFGNTYDELTSP.sql                                         
@@..\..\storedprocs\PRPMFGNTYTRG.sql                                            
/                                                       
                                                                                
 prompt PRPMFWIREPOSTING                                                        
@@..\..\storedprocs\PRPMFWIREPOSTINGUPDTSP.sql                                  
@@..\..\storedprocs\PRPMFWIREPOSTINGINSTSP.sql                                  
@@..\..\storedprocs\PRPMFWIREPOSTINGDELTSP.sql                                  
@@..\..\storedprocs\PRPMFWIREPOSTINGTRG.sql                                     
/                                                       
                                                                                
 prompt PRPRVWSTAT                                                              
@@..\..\storedprocs\PRPRVWSTATUPDTSP.sql                                        
@@..\..\storedprocs\PRPRVWSTATINSTSP.sql                                        
@@..\..\storedprocs\PRPRVWSTATDELTSP.sql                                        
@@..\..\storedprocs\PRPRVWSTATTRG.sql                                           
/                                                       
                                                                                
 prompt PYMT110RPT                                                              
@@..\..\storedprocs\PYMT110RPTUPDTSP.sql                                        
@@..\..\storedprocs\PYMT110RPTINSTSP.sql                                        
@@..\..\storedprocs\PYMT110RPTDELTSP.sql                                        
@@..\..\storedprocs\PYMT110RPTTRG.sql                                           
/                                                       
                                                                                
 prompt PYMTDRFILE                                                              
@@..\..\storedprocs\PYMTDRFILEUPDTSP.sql                                        
@@..\..\storedprocs\PYMTDRFILEINSTSP.sql                                        
@@..\..\storedprocs\PYMTDRFILEDELTSP.sql                                        
@@..\..\storedprocs\PYMTDRFILETRG.sql                                           
/                                                       
                                                                                
 prompt REFSBAOFC                                                               
@@..\..\storedprocs\REFSBAOFCUPDTSP.sql                                         
@@..\..\storedprocs\REFSBAOFCINSTSP.sql                                         
@@..\..\storedprocs\REFSBAOFCDELTSP.sql                                         
@@..\..\storedprocs\REFSBAOFCTRG.sql                                            
/                                                       
                                                                                
 prompt SOEZ                                                                    
@@..\..\storedprocs\SOEZUPDTSP.sql                                              
@@..\..\storedprocs\SOEZINSTSP.sql                                              
@@..\..\storedprocs\SOEZDELTSP.sql                                              
@@..\..\storedprocs\SOEZTRG.sql                                                 
/                                                       
                                                                                
 prompt SOFVBFFA                                                                
@@..\..\storedprocs\SOFVBFFAUPDTSP.sql                                          
@@..\..\storedprocs\SOFVBFFAINSTSP.sql                                          
@@..\..\storedprocs\SOFVBFFADELTSP.sql                                          
@@..\..\storedprocs\SOFVBFFATRG.sql                                             
/                                                       
                                                                                
 prompt SOFVBGDF                                                                
@@..\..\storedprocs\SOFVBGDFUPDTSP.sql                                          
@@..\..\storedprocs\SOFVBGDFINSTSP.sql                                          
@@..\..\storedprocs\SOFVBGDFDELTSP.sql                                          
@@..\..\storedprocs\SOFVBGDFTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVBGW9                                                                
@@..\..\storedprocs\SOFVBGW9UPDTSP.sql                                          
@@..\..\storedprocs\SOFVBGW9INSTSP.sql                                          
@@..\..\storedprocs\SOFVBGW9DELTSP.sql                                          
@@..\..\storedprocs\SOFVBGW9TRG.sql                                             
/                                                       
                                                                                
 prompt SOFVCDCMSTR                                                             
@@..\..\storedprocs\SOFVCDCMSTRUPDTSP.sql                                       
@@..\..\storedprocs\SOFVCDCMSTRINSTSP.sql                                       
@@..\..\storedprocs\SOFVCDCMSTRDELTSP.sql                                       
@@..\..\storedprocs\SOFVCDCMSTRTRG.sql                                          
/                                                       
                                                                                
 prompt SOFVCTCH                                                                
@@..\..\storedprocs\SOFVCTCHUPDTSP.sql                                          
@@..\..\storedprocs\SOFVCTCHINSTSP.sql                                          
@@..\..\storedprocs\SOFVCTCHDELTSP.sql                                          
@@..\..\storedprocs\SOFVCTCHTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVDFPY                                                                
@@..\..\storedprocs\SOFVDFPYUPDTSP.sql                                          
@@..\..\storedprocs\SOFVDFPYINSTSP.sql                                          
@@..\..\storedprocs\SOFVDFPYDELTSP.sql                                          
@@..\..\storedprocs\SOFVDFPYTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVDUEB                                                                
@@..\..\storedprocs\SOFVDUEBUPDTSP.sql                                          
@@..\..\storedprocs\SOFVDUEBINSTSP.sql                                          
@@..\..\storedprocs\SOFVDUEBDELTSP.sql                                          
@@..\..\storedprocs\SOFVDUEBTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVFTPO                                                                
@@..\..\storedprocs\SOFVFTPOUPDTSP.sql                                          
@@..\..\storedprocs\SOFVFTPOINSTSP.sql                                          
@@..\..\storedprocs\SOFVFTPODELTSP.sql                                          
@@..\..\storedprocs\SOFVFTPOTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVGNTY                                                                
@@..\..\storedprocs\SOFVGNTYUPDTSP.sql                                          
@@..\..\storedprocs\SOFVGNTYINSTSP.sql                                          
@@..\..\storedprocs\SOFVGNTYDELTSP.sql                                          
@@..\..\storedprocs\SOFVGNTYTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVGRGC                                                                
@@..\..\storedprocs\SOFVGRGCUPDTSP.sql                                          
@@..\..\storedprocs\SOFVGRGCINSTSP.sql                                          
@@..\..\storedprocs\SOFVGRGCDELTSP.sql                                          
@@..\..\storedprocs\SOFVGRGCTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVLND1                                                                
@@..\..\storedprocs\SOFVLND1UPDTSP.sql                                          
@@..\..\storedprocs\SOFVLND1INSTSP.sql                                          
@@..\..\storedprocs\SOFVLND1DELTSP.sql                                          
@@..\..\storedprocs\SOFVLND1TRG.sql                                             
/                                                       
                                                                                
 prompt SOFVLND2                                                                
@@..\..\storedprocs\SOFVLND2UPDTSP.sql                                          
@@..\..\storedprocs\SOFVLND2INSTSP.sql                                          
@@..\..\storedprocs\SOFVLND2DELTSP.sql                                          
@@..\..\storedprocs\SOFVLND2TRG.sql                                             
/                                                       
                                                                                
 prompt SOFVOTRN                                                                
@@..\..\storedprocs\SOFVOTRNUPDTSP.sql                                          
@@..\..\storedprocs\SOFVOTRNINSTSP.sql                                          
@@..\..\storedprocs\SOFVOTRNDELTSP.sql                                          
@@..\..\storedprocs\SOFVOTRNTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVPYMT                                                                
@@..\..\storedprocs\SOFVPYMTUPDTSP.sql                                          
@@..\..\storedprocs\SOFVPYMTINSTSP.sql                                          
@@..\..\storedprocs\SOFVPYMTDELTSP.sql                                          
@@..\..\storedprocs\SOFVPYMTTRG.sql                                             
/                                                       
                                                                                
 prompt SOFVRTSC                                                                
@@..\..\storedprocs\SOFVRTSCUPDTSP.sql                                          
@@..\..\storedprocs\SOFVRTSCINSTSP.sql                                          
@@..\..\storedprocs\SOFVRTSCDELTSP.sql                                          
@@..\..\storedprocs\SOFVRTSCTRG.sql                                             
/                                                       
                                                                                
 prompt TEMPCDAMORT                                                             
@@..\..\storedprocs\TEMPCDAMORTUPDTSP.sql                                       
@@..\..\storedprocs\TEMPCDAMORTINSTSP.sql                                       
@@..\..\storedprocs\TEMPCDAMORTDELTSP.sql                                       
@@..\..\storedprocs\TEMPCDAMORTTRG.sql                                          
/                                                       
                                                                                
 prompt TEMPGETDT                                                               
@@..\..\storedprocs\TEMPGETDTUPDTSP.sql                                         
@@..\..\storedprocs\TEMPGETDTINSTSP.sql                                         
@@..\..\storedprocs\TEMPGETDTDELTSP.sql                                         
@@..\..\storedprocs\TEMPGETDTTRG.sql                                            
/                                                       
                                                                                
 prompt TEMPGETOGERRRAW                                                         
@@..\..\storedprocs\TEMPGETOGERRRAWUPDTSP.sql                                   
@@..\..\storedprocs\TEMPGETOGERRRAWINSTSP.sql                                   
@@..\..\storedprocs\TEMPGETOGERRRAWDELTSP.sql                                   
@@..\..\storedprocs\TEMPGETOGERRRAWTRG.sql                                      
/                                                       
                                                                                
 prompt TEMPGETOGERR                                                            
@@..\..\storedprocs\TEMPGETOGERRUPDTSP.sql                                      
@@..\..\storedprocs\TEMPGETOGERRINSTSP.sql                                      
@@..\..\storedprocs\TEMPGETOGERRDELTSP.sql                                      
@@..\..\storedprocs\TEMPGETOGERRTRG.sql                                         
/                                                       
                                                                                
 prompt TEMPMACROQUERY                                                          
@@..\..\storedprocs\TEMPMACROQUERYUPDTSP.sql                                    
@@..\..\storedprocs\TEMPMACROQUERYINSTSP.sql                                    
@@..\..\storedprocs\TEMPMACROQUERYDELTSP.sql                                    
@@..\..\storedprocs\TEMPMACROQUERYTRG.sql                                       
/                                                       
                                                                                
 prompt WFCT                                                                    
@@..\..\storedprocs\WFCTUPDTSP.sql                                              
@@..\..\storedprocs\WFCTINSTSP.sql                                              
@@..\..\storedprocs\WFCTDELTSP.sql                                              
@@..\..\storedprocs\WFCTTRG.sql                                                 
/                                                       
                                                                                

68 rows selected.
                                                             
                                                                                

--68 rows selected.

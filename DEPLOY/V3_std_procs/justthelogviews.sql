CREATE OR REPLACE FORCE VIEW STGCSA.STGHISTORY
(
  UPDTDT,
  ACTION,
  TABLENM,
  PK,
  COLNAME,
  CHANGE,
  LOANNMB,
  UPDTUSERID,
  LOGENTRYID,
  CSA_SID
)
BEQUEATH DEFINER
AS
    SELECT l.UPDTDT,
           upd_action
             Action,
           l.TABLENM,
           l.PK,
           d.COLNAME,
           CASE
             WHEN NOT (   d.colname IS NULL
                       OR d.colname = 'NO_CHANGE'
                       OR d.colname = 'Deleted'
                       OR d.colname = 'DELETED')
             THEN
               '"' || d.OLDVALUE || '" > "' || d.NEWVALUE || '"'
             ELSE
               ' '
           END
             change,
           l.LOANNMB,
           l.UPDTUSERID,
           l.UPDTLOGID
             logentryid,
           l.csa_sid
      FROM stgcsa.STGUPDTLOGTBL l, stgcsa.STGUPDTLOGDTLTBL d
     WHERE UPDTLOGID = UPDTLOGFK(+)
  ORDER BY updtdt, UPDTLOGID, colname;


GRANT SELECT ON STGCSA.STGHISTORY TO CSAUPDTROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO LOANCSAADMINROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO LOANCSAANALYSTROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO LOANCSAMANAGERROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO LOANCSAREADALLROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO LOANCSAREVIEWERROLE;

GRANT SELECT ON STGCSA.STGHISTORY TO STGCSAREADALLROLE;
--------

CREATE OR REPLACE FORCE VIEW STGCSA.STGLOG
(
  CSA_SID,
  LOGENTRYID,
  CREATDT,
  CREATUSERID,
  ACTVTYLOGCAT,
  ACTVTYDTLS,
  MSG_SEVERITY
)
BEQUEATH DEFINER
AS
    SELECT csa_sid,
           logentryid,
           creatdt,
           creatuserid,
           actvtylogcat,
           actvtydtls,
           msg_severity
      FROM (SELECT csa_sid,
                   logentryid,
                   creatdt,
                   creatuserid,
                   actvtylogcat,
                   actvtydtls,
                   NVL (msg_severity, 0)   msg_severity
              FROM STGCSA.COREACTVTYLOGTBL
            UNION
            SELECT csa_sid,
                   s.UPDTLOGID,
                   updtdt,
                   updtuserid,
                   DECODE (upd_action,
                           'I', 'ROWINS',
                           'U', 'ROWUPD',
                           'D', 'ROWDEL',
                           'HUH?'),
                      'SQL '
                   || DECODE (upd_action,
                              'I', 'INS',
                              'U', 'UPD',
                              'D', 'DEL',
                              'HUH')
                   || ' on '
                   || tablenm
                   || ' pkvalue: '
                   || pk,
                   NULL
              FROM stgcsa.stgupdtlogTBL s)
  ORDER BY logentryid;


GRANT SELECT ON STGCSA.STGLOG TO CSAUPDTROLE;

GRANT SELECT ON STGCSA.STGLOG TO LOANCSAADMINROLE;

GRANT SELECT ON STGCSA.STGLOG TO LOANCSAANALYSTROLE;

GRANT SELECT ON STGCSA.STGLOG TO LOANCSAMANAGERROLE;

GRANT SELECT ON STGCSA.STGLOG TO LOANCSAREADALLROLE;

GRANT SELECT ON STGCSA.STGLOG TO LOANCSAREVIEWERROLE;

GRANT SELECT ON STGCSA.STGLOG TO STGCSAREADALLROLE;

-----------



CREATE OR REPLACE FORCE VIEW STGCSA.STGLOG_FULL
(
  CSA_SID,
  LOGENTRYID,
  CREATDT,
  CREATUSERID,
  ACTVTYLOGCAT,
  ACTVTYDTLS,
  CCNT,
  TABLENM,
  MSG_SEVERITY
)
BEQUEATH DEFINER
AS
    SELECT csa_sid,
           logentryid,
           creatdt,
           creatuserid,
           actvtylogcat,
           actvtydtls,
           ccnt,
           tablenm,
           msg_severity
      FROM (SELECT csa_sid,
                   logentryid,
                   creatdt,
                   creatuserid,
                   actvtylogcat,
                   actvtydtls,
                   CAST (NULL AS NUMBER)          ccnt,
                   CAST (NULL AS VARCHAR2 (30))   tablenm,
                   NVL (msg_severity, 0)          msg_severity
              FROM STGCSA.COREACTVTYLOGTBL
             WHERE msg_id <> 1
            UNION
            SELECT csa_sid,
                   s.UPDTLOGID,
                   updtdt,
                   updtuserid,
                   DECODE (upd_action,
                           'I', 'ROWINS',
                           'U', 'ROWUPD',
                           'D', 'ROWDEL',
                           'HUH?'),
                      'SQL '
                   || DECODE (upd_action,
                              'I', 'INS',
                              'U', 'UPD',
                              'D', 'DEL',
                              'HUH')
                   || ' on '
                   || tablenm
                   || ' pkvalue: '
                   || pk,
                   colchngs,
                   tablenm,
                   NULL
              FROM stgcsa.stgupdtlogTBL s,
                   (  SELECT COUNT (*) colchngs, updtlogfk
                        FROM STGCSA.STGUPDTLOGDTLTBL
                    GROUP BY updtlogfk) c
             WHERE s.UPDTLOGID = c.updtlogfk(+))
  ORDER BY logentryid;
  
GRANT SELECT ON STGCSA.STGLOG_FULL TO CSAUPDTROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO LOANCSAADMINROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO LOANCSAANALYSTROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO LOANCSAMANAGERROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO LOANCSAREADALLROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON STGCSA.STGLOG_FULL TO STGCSAREADALLROLE;



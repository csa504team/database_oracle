-- v3_std_procs deploy script doit.sql v3.0 7 Nov 2018 JL
accept spoolfile_dir prompt "Please enter a directory name for writing spool files -> "
accept spsfx prompt "suffix for spool file --> "
select to_char(sysdate,'_yyyymmddhh24miss') dt from dual; 
set echo on
spool &&spoolfile_dir\deploy_v3_std_procs&&spsfx
@@Drop_old_logging_trigs
@@drop_old_aud_procs
@@..\..\miscsqlddl\runtime
@@..\..\miscsqlddl\runtime_body
@@stdprocs
@@..\..\views\stghistory
@@..\..\views\stglog
@@..\..\views\stglog_full
@@..\..\views\mycoreactvtylog
@@..\..\views\myhistory
@@..\..\views\mystglog
@@..\..\views\mystglog_full
@@..\..\miscsqlddl\log_indexes
@@..\..\storedprocs\stgloggercsp
DROP PROCEDURE STGCSA.stgcsa_resequence; 
@@..\..\storedprocs\STGCSA_RESEQUENCE
exec STGCSA.stgcsa_resequence


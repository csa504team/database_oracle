define deploy_name=NOV_Ach_Debit
define package_name=check
define package_buildtime=20201110 93806
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy NOV_Ach_Debit_check created on Tue 11/10/2020  9:38:08.55 by Jasleen Gorowada
prompt deploy scripts for deploy NOV_Ach_Debit_check created on Tue 11/10/2020  9:38:08.55 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy NOV_Ach_Debit_check: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_ACH_blank_NOV.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_ACH_blank_NOV.sql"
set echo on
set feedback on
select '*** testing to see if loan exisits with ACH blank***' from dual;
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8362765000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 1                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5126275000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 2                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1719447003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 3                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4997345009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 4                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4317185006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 5                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6433565009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 6                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5886335008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 7                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8606874010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 8                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4325675000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 9                                       
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5155905009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 10                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1530746010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 11                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4448744001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 12                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3204856001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 13                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9206154001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 14                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5165275003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 15                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5656824000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 16                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4570095007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 17                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7527494000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 18                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2917996003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 19                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2923716007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 20                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6246905009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 21                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6620274004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 22                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4432754010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 23                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4591425009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 24                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4586305003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 25                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5159635008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 26                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2045436001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 27                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4341184010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 28                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7988885009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 29                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1207717008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 30                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2916646001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 31                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9723565000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 32                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5223805006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 33                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4667075008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 34                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2701846005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 35                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3411236006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 36                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2273876006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 37                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8444175004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 38                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3077226007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 39                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4841715006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 40                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5367455007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 41                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8828904001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 42                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9047934001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 43                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4848785008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 44                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3079616010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 45                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5421644000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 46                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6200714009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 47                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5374355010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 48                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4059975003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 49                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4542385003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 50                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3186146006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 51                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3560185001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 52                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8054675006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 53                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4461855000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 54                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6317365000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 55                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5307155009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 56                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6387205004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 57                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8520594008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 58                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6160235008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 59                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7161015008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 60                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9769295008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 61                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1539787003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 62                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8142755006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 63                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4683025004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 64                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2752736007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 65                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6433565009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 66                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9306095000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 67                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8103035010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 68                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3937405007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 69                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7623284007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 70                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7561084010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 71                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6370255001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 72                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4276155000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 73                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8645494006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 74                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3409056001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 75                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5301194002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 76                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4275945010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 77                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2744286003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 78                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4275395000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 79                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3451625003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 80                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4309055002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 81                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2684896006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 82                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8417095009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 83                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6198915006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 84                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3255246006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 85                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3485115001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 86                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1304166004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 87                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3071896008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 88                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7663515005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 89                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2254206009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 90                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9099924000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 91                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7582934004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 92                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3498916000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 93                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7813165003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 94                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6368605000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 95                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7656425004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 96                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8224835010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 97                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9295235009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 98                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4168825003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 99                                      
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4131395001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 100                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2435246009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 101                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3110696000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 102                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6286224004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 103                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6440955006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 104                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3362485007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 105                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3097166010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 106                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3591786003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 107                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8953114010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 108                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1188786004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 109                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7294575005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 110                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5368645007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 111                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2528436006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 112                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8874384001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 113                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3509565002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 114                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6139184005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 115                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6139204009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 116                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3076276009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 117                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2424036010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 118                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2836677006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 119                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5088085000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 120                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7836814004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 121                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3078056006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 122                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5005615010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 123                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3989525010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 124                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3690486002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 125                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4733555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 126                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5048245006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 127                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2278567002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 128                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2596356008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 129                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3282786010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 130                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5047405004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 131                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8790624004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 132                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3106826010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 133                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6369504000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 134                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2662256001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 135                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2500026010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 136                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3690555003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 137                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7280104007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 138                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4688915010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 139                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4113875010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 140                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5194175007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 141                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9001634010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 142                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4749635003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 143                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5135315001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 144                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5289455000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 145                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4758865004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 146                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2234617002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 147                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3076276009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 148                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4733555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 149                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5332845006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 150                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4424684002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 151                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8644905003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 152                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5831215010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 153                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6897245005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 154                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4216815006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 155                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2704106001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 156                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7885285003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 157                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5135315001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 158                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5289455000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 159                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2844226004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 160                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4158905010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 161                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4887495007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 162                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3137826002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 163                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2498056008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 164                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8583404005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 165                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9169694010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 166                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3690555003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 167                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9103944010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 168                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5182645009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 169                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3927295007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 170                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7658764009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 171                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3525026000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 172                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4191155001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 173                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4083885000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 174                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8100894008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 175                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3283196001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 176                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2361337004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 177                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3564035010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 178                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5562664010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 179                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7779675009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 180                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3057707008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 181                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3866295002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 182                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5092495008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 183                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5150145007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 184                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4647715002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 185                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5430325007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 186                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3238826001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 187                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3516515007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 188                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3603296007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 189                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4402365006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 190                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2680096005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 191                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2685906009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 192                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3234416010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 193                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6725534006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 194                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3866295002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 195                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9169694010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 196                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5092495008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 197                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4198865005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 198                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2903896007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 199                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6620274004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 200                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3355755007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 201                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2752264005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 202                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3616106006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 203                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3720315003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 204                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4818955010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 205                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6261794004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 206                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2732696000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 207                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7523154006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 208                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8803024001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 209                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6261794004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 210                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8077815003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 211                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4967105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 212                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5425855008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 213                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9032434001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 214                                     
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4898545004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
-- 215                                     
                                                                                                    

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

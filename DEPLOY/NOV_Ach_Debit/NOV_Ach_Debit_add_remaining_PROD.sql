define deploy_name=NOV_Ach_Debit
define package_name=add_remaining_PROD
define package_buildtime=20201110171608
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy NOV_Ach_Debit_add_remaining_PROD created on Tue 11/10/2020 17:16:09.05 by Jasleen Gorowada
prompt deploy scripts for deploy NOV_Ach_Debit_add_remaining_PROD created on Tue 11/10/2020 17:16:09.05 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy NOV_Ach_Debit_add_remaining_PROD: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_records_PROD.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\records_not_updated_PROD.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_records_PROD.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_records_PROD.sql"
select LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL 
where LOANNMB in ('6433565009', '3076276009', '4733555009', '5135315001', '5289455000',
'3690555003', '3866295002', '9169694010', '5092495008', '6620274004', '6261794004');
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\records_not_updated_PROD.sql"
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6238670499       ',LOANDTLACHBNKROUTNMB='036076150      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6433565009';
-- 66                                      
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3524263484       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3076276009';
-- 148  

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3360228786       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4733555009';
-- 149  

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='201005337        ',LOANDTLACHBNKROUTNMB='072414190      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5135315001';
-- 158      

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375008176205     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5289455000';
-- 159  

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010637074        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3690555003';
-- 167   

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8200865080       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3866295002';
-- 195   

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='079135901        ',LOANDTLACHBNKROUTNMB='322077795      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9169694010';
-- 196                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000142087765    ',LOANDTLACHBNKROUTNMB='061000104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5092495008';
-- 197      

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='390010197        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6620274004';
-- 200 

update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6079885486       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6261794004';
-- 210    
commit;                             
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_records_PROD.sql"
select LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL 
where LOANNMB in ('6433565009', '3076276009', '4733555009', '5135315001', '5289455000',
'3690555003', '3866295002', '9169694010', '5092495008', '6620274004', '6261794004');
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

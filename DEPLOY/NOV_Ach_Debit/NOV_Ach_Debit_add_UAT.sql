define deploy_name=NOV_Ach_Debit
define package_name=add_UAT
define package_buildtime=20201110105208
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy NOV_Ach_Debit_add_UAT created on Tue 11/10/2020 10:52:09.00 by Jasleen Gorowada
prompt deploy scripts for deploy NOV_Ach_Debit_add_UAT created on Tue 11/10/2020 10:52:09.00 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy NOV_Ach_Debit_add_UAT: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ACH_info_NOV_UAT.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ACH_info_NOV_UAT.sql"
set echo on
set feedback on
select '*** update ACH information***' from dual;
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6826976778       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8362765000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 1                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='485010346971     ',LOANDTLACHBNKROUTNMB='323070380      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5126275000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 2                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157522316486     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1719447003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 3                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4111060570       ',LOANDTLACHBNKROUTNMB='121140263      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4997345009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 4                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0111717970       ',LOANDTLACHBNKROUTNMB='121140263      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4317185006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 5                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6238670499       ',LOANDTLACHBNKROUTNMB='036076150      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6433565009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 6                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='57905614         ',LOANDTLACHBNKROUTNMB='121107882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5886335008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 7                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0064439856       ',LOANDTLACHBNKROUTNMB='321175261      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8606874010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 8                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='243155177        ',LOANDTLACHBNKROUTNMB='122234149      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4325675000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 9                                       
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='334036870681     ',LOANDTLACHBNKROUTNMB='061000052      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5155905009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 10                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='400541300703     ',LOANDTLACHBNKROUTNMB='322282001      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1530746010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 11                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8201429746       ',LOANDTLACHBNKROUTNMB='321270742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4448744001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 12                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='457335102        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3204856001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 13                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='457334576        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9206154001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 14                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7884696563       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5165275003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 15                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1010787271       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5656824000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 16                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1722170743       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4570095007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 17                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0404019101       ',LOANDTLACHBNKROUTNMB='321270742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7527494000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 18                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325083711033     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2917996003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 19                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='228850522        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2923716007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 20                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0041317066       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6246905009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 21                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='390010197        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6620274004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 22                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325099294599     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4432754010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 23                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005148380629    ',LOANDTLACHBNKROUTNMB='061113415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4591425009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 24                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='003112463        ',LOANDTLACHBNKROUTNMB='121143037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4586305003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 25                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='000230174583     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5159635008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 26                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7011636          ',LOANDTLACHBNKROUTNMB='072413104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2045436001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 27                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0801000496       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4341184010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 28                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001101504602     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7988885009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 29                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='229857898        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1207717008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 30                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='000280773395     ',LOANDTLACHBNKROUTNMB='322281617      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2916646001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 31                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='153457860333     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9723565000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 32                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001670034        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5223805006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 33                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1465274 072      ',LOANDTLACHBNKROUTNMB='122239131      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4667075008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 34                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7782310382       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2701846005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 35                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000050408136    ',LOANDTLACHBNKROUTNMB='061000104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3411236006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 36                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0210008546       ',LOANDTLACHBNKROUTNMB='122244003      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2273876006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 37                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='831869891        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8444175004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 38                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='13136612         ',LOANDTLACHBNKROUTNMB='121181976      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3077226007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 39                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1585090762       ',LOANDTLACHBNKROUTNMB='031000503      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4841715006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 40                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00000493215909   ',LOANDTLACHBNKROUTNMB='321180379      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5367455007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 41                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='339151490        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8828904001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 42                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='527121922        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9047934001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 43                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='03133 72801      ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4848785008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 44                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3260060681       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3079616010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 45                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157516376827     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5421644000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 46                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157516376827     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6200714009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 47                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='150028519        ',LOANDTLACHBNKROUTNMB='061104929      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5374355010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 48                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0053005849       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4059975003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 49                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1208107          ',LOANDTLACHBNKROUTNMB='121143037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4542385003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 50                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='200063758        ',LOANDTLACHBNKROUTNMB='125107037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3186146006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 51                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1385000698       ',LOANDTLACHBNKROUTNMB='121101037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3560185001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 52                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5791036006       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8054675006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 53                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='968508887        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4461855000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 54                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='202401212        ',LOANDTLACHBNKROUTNMB='322271724      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6317365000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 55                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='202005575        ',LOANDTLACHBNKROUTNMB='061020867      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5307155009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 56                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='000343201836     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6387205004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 57                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3402302199       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8520594008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 58                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='031414011        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6160235008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 59                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='100052076        ',LOANDTLACHBNKROUTNMB='122244773      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7161015008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 60                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325091017172     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9769295008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 61                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='279315011        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1539787003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 62                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2290949094       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8142755006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 63                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8623741469       ',LOANDTLACHBNKROUTNMB='031000053      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4683025004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 64                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1570294783       ',LOANDTLACHBNKROUTNMB='231379034      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2752736007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 65                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6238670499       ',LOANDTLACHBNKROUTNMB='036076150      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6433565009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 66                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1189174          ',LOANDTLACHBNKROUTNMB='036002247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9306095000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 67                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='53329016         ',LOANDTLACHBNKROUTNMB='031300135      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8103035010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 68                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='009360599214     ',LOANDTLACHBNKROUTNMB='122037760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3937405007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 69                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2343703827       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7623284007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 70                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0397067360       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7561084010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 71                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2489619          ',LOANDTLACHBNKROUTNMB='031309945      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6370255001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 72                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='009360589921     ',LOANDTLACHBNKROUTNMB='122037760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4276155000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 73                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='659008775        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8645494006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 74                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='064290083        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3409056001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 75                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='030528913        ',LOANDTLACHBNKROUTNMB='121125660      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5301194002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 76                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325052757484     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4275945010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 77                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2117257          ',LOANDTLACHBNKROUTNMB='031907790      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2744286003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 78                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0811098516       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4275395000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 79                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='05107822         ',LOANDTLACHBNKROUTNMB='122042807      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3451625003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 80                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0961442067       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4309055002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 81                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0012072335       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2684896006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 82                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='42020744142      ',LOANDTLACHBNKROUTNMB='321171184      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8417095009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 83                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2304000657       ',LOANDTLACHBNKROUTNMB='122203471      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6198915006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 84                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1340511911       ',LOANDTLACHBNKROUTNMB='121002042      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3255246006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 85                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000811100057    ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3485115001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 86                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4738770825       ',LOANDTLACHBNKROUTNMB='121000248      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1304166004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 87                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='291546661        ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3071896008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 88                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0040106758       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7663515005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 89                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9763713527       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2254206009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 90                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='02327 45052      ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9099924000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 91                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01365 08623      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7582934004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 92                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='991447285        ',LOANDTLACHBNKROUTNMB='121141819      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3498916000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 93                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='765691089        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7813165003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 94                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='220033732        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6368605000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 95                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325000585242     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7656425004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 96                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='041832346        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8224835010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 97                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='048220651        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9295235009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 98                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0811110168       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4168825003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 99                                      
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='59980416032      ',LOANDTLACHBNKROUTNMB='323270300      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4131395001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 100                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='365719830        ',LOANDTLACHBNKROUTNMB='036001808      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2435246009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 101                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='165940417463     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3110696000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 102                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1139812040       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6286224004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 103                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='032966879        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6440955006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 104                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='469963883        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3362485007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 105                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='101040856        ',LOANDTLACHBNKROUTNMB='121144324      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3097166010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 106                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='234169882        ',LOANDTLACHBNKROUTNMB='122234149      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3591786003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 107                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8771787220       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8953114010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 108                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8771787220       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1188786004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 109                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1390016841338    ',LOANDTLACHBNKROUTNMB='031309123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7294575005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 110                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='60216580         ',LOANDTLACHBNKROUTNMB='121143781      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5368645007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 111                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='719388811        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2528436006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 112                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8224239072       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8874384001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 113                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='199070161        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3509565002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 114                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='339151060        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6139184005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 115                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='339151490        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6139204009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 116                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3524263484       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3076276009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 117                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3607755761       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2424036010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 118                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0110251564       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2836677006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 119                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='67700552         ',LOANDTLACHBNKROUTNMB='231372248      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5088085000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 120                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='695216077        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7836814004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 121                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1852359213       ',LOANDTLACHBNKROUTNMB='072000096      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3078056006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 122                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3010514140       ',LOANDTLACHBNKROUTNMB='272479841      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5005615010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 123                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='203573431        ',LOANDTLACHBNKROUTNMB='321171184      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3989525010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 124                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1852359486       ',LOANDTLACHBNKROUTNMB='072000096      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3690486002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 125                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3360228786       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4733555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 126                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7650009961       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5048245006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 127                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0071582951       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2278567002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 128                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='041171904403     ',LOANDTLACHBNKROUTNMB='084301767      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2596356008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 129                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='287256579        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3282786010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 130                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6429340323       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5047405004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 131                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='801000496        ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8790624004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 132                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='641001210        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3106826010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 133                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4866090311       ',LOANDTLACHBNKROUTNMB='123205054      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6369504000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 134                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='087110904        ',LOANDTLACHBNKROUTNMB='322077795      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2662256001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 135                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2020481456       ',LOANDTLACHBNKROUTNMB='122232109      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2500026010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 136                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010637074        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3690555003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 137                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1275001228       ',LOANDTLACHBNKROUTNMB='121101037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7280104007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 138                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='009360705670     ',LOANDTLACHBNKROUTNMB='122037760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4688915010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 139                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2338408591       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4113875010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 140                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='405846374        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5194175007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 141                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3369714419       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9001634010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 142                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0511006379       ',LOANDTLACHBNKROUTNMB='121140263      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4749635003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 143                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='201005337        ',LOANDTLACHBNKROUTNMB='072414190      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5135315001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 144                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375008176205     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5289455000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 145                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001419008932     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4758865004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 146                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325105061755     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2234617002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 147                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3524263484       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3076276009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 148                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3360228786       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4733555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 149                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0064253230       ',LOANDTLACHBNKROUTNMB='051900395      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5332845006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 150                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010103901        ',LOANDTLACHBNKROUTNMB='123205054      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4424684002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 151                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8003139212       ',LOANDTLACHBNKROUTNMB='322070381      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8644905003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 152                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5331863          ',LOANDTLACHBNKROUTNMB='322270518      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5831215010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 153                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2983906120       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6897245005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 154                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='009506160        ',LOANDTLACHBNKROUTNMB='122039360      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4216815006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 155                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7476631325       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2704106001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 156                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3080879362       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7885285003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 157                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='201005337        ',LOANDTLACHBNKROUTNMB='072414190      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5135315001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 158                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375008176205     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5289455000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 159                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0480010453       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2844226004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 160                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01381743001      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4158905010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 161                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='005407024792     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4887495007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 162                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0352162341       ',LOANDTLACHBNKROUTNMB='121140218      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3137826002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 163                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7115272887       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2498056008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 164                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1060068696       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8583404005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 165                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='079135901        ',LOANDTLACHBNKROUTNMB='322077795      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9169694010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 166                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010637074        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3690555003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 167                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='621007041        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9103944010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 168                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='051119150        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5182645009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 169                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01388427951      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3927295007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 170                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0071001500       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7658764009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 171                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2297170108       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3525026000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 172                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='012880971        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4191155001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 173                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7286584979       ',LOANDTLACHBNKROUTNMB='061000227      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4083885000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 174                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7363496295       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8100894008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 175                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2011122971       ',LOANDTLACHBNKROUTNMB='061112843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3283196001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 176                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0365404708       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2361337004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 177                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='40002271         ',LOANDTLACHBNKROUTNMB='122242982      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3564035010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 178                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='040512816        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5562664010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 179                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='040355257        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7779675009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 180                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325097189952     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3057707008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 181                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8200865080       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3866295002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 182                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000142087765    ',LOANDTLACHBNKROUTNMB='061000104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5092495008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 183                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005149901487    ',LOANDTLACHBNKROUTNMB='061113415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5150145007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 184                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005243856118    ',LOANDTLACHBNKROUTNMB='061113415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4647715002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 185                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2016822          ',LOANDTLACHBNKROUTNMB='061120686      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5430325007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 186                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2050164447       ',LOANDTLACHBNKROUTNMB='072410013      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3238826001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 187                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9463114246       ',LOANDTLACHBNKROUTNMB='122239131      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3516515007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 188                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3259805749       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3603296007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 189                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='879314847        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4402365006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 190                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0469432579       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2680096005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 191                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='251228922        ',LOANDTLACHBNKROUTNMB='122234149      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2685906009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 192                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0248001604       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3234416010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 193                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0811062112       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6725534006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 194                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8200865080       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3866295002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 195                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='079135901        ',LOANDTLACHBNKROUTNMB='322077795      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9169694010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 196                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000142087765    ',LOANDTLACHBNKROUTNMB='061000104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5092495008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 197                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5164363          ',LOANDTLACHBNKROUTNMB='322270518      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4198865005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 198                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0146032107       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2903896007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 199                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='390010197        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6620274004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 200                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1017607          ',LOANDTLACHBNKROUTNMB='122238200      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3355755007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 201                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8887021080       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2752264005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 202                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='049264989        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3616106006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 203                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='049264989        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3720315003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 204                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00732002709      ',LOANDTLACHBNKROUTNMB='321170978      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4818955010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 205                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6079885486       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6261794004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 206                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2783575166       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2732696000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 207                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2170115821       ',LOANDTLACHBNKROUTNMB='122232109      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7523154006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 208                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0811065081       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8803024001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 209                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6079885486       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6261794004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 210                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157504338474     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8077815003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 211                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00000094083490   ',LOANDTLACHBNKROUTNMB='321173742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4967105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 212                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='445947           ',LOANDTLACHBNKROUTNMB='061112843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5425855008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 213                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='06237 10336      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9032434001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 214                                     
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0800007767       ',LOANDTLACHBNKROUTNMB='072413735      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4898545004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
-- 215                                     
                                                                                                    
commit;  

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

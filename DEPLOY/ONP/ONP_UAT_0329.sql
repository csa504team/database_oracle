define deploy_name=ONP
define package_name=UAT_0329
define package_buildtime=20210329104119
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy ONP_UAT_0329 created on Mon 03/29/2021 10:41:20.57 by Jasleen Gorowada
prompt deploy scripts for deploy ONP_UAT_0329 created on Mon 03/29/2021 10:41:20.57 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy ONP_UAT_0329: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANNOTEINFOUPDTSP.sql 
Jasleen Gorowada committed fdfef81 on Sat Mar 13 17:14:00 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANNOTEINFOTSP.sql 
Jasleen Gorowada committed fdfef81 on Sat Mar 13 17:14:00 2021 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANNOTEINFOUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANNOTEINFOUPDTSP (
    p_IDENTIFIER           NUMBER := 0,
    p_RETVAL           OUT NUMBER,
    p_LOANNMB           CHAR := NULL,
	p_RECEIVEDDT		DATE := NULL,
	p_TRUSTEENM			VARCHAR2 := NULL,
	p_RELEASEDDT		DATE := NULL,
	p_AIRBILLNMBR		VARCHAR2 := NULL,
	p_MODDT				DATE := sysdate,
	p_CMNTS				VARCHAR2 := NULL,
	p_BORROWER			VARCHAR2 := NULL,
	p_AMOUNT			NUMBER := 0,
	p_BNKNM				VARCHAR2 := NULL,
	p_LOANNOTEINFOCREATUSERID	VARCHAR2 := user,
	p_LOANNOTEINFOCREATDT		DATE := sysdate,
	p_LASTUPDTUSERID	VARCHAR2 := user,
	p_LASTUPDTDT		DATE := sysdate,
    p_NOTETYPE              VARCHAR := NULL,
    p_LOANNOTEID        NUMBER)
AS
BEGIN	
    SAVEPOINT LOANNOTEINFOUPDTSP;

    IF p_IDENTIFIER = 0
    THEN
        BEGIN
			UPDATE LOAN.LOANNOTEINFOTBL 
			SET RECEIVEDDT = p_RECEIVEDDT,
                TRUSTEENM  = p_TRUSTEENM,
                RELEASEDDT = p_RELEASEDDT,
                AIRBILLNMBR = p_AIRBILLNMBR,
                MODDT  = p_MODDT,
                CMNTS = p_CMNTS,
                LOANNOTEINFOCREATUSERID = p_LOANNOTEINFOCREATUSERID,
                LOANNOTEINFOCREATDT = p_LOANNOTEINFOCREATDT,
                LASTUPDTUSERID = p_LASTUPDTUSERID,
                LASTUPDTDT = p_LASTUPDTDT,
                BORROWER = p_BORROWER,
                AMOUNT = p_AMOUNT,
                BNKNM = p_BNKNM,
                NOTETYPE = p_NOTETYPE,
                LOANNMB = p_LOANNMB
                WHERE LOANNOTEID = p_LOANNOTEID;
            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO LOANNOTEINFOUPDTSP;
        END;
END LOANNOTEINFOUPDTSP;
/



GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO UPDLOANROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOUPDTSP TO ONPUPDROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANNOTEINFOTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANNOTEINFOTSP
(
  p_GPNMBR IN CHAR,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		SELECT L.LOANNMB ,
               case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Borrower,
            LP.LOANAPPRQSTAMT,
            PP.LOANAPPPRTNM
			         FROM LOAN.LOANGNTYTBL L
					 left join LOAN.LOANGNTYBORRTBL b on L.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
                     left join LOAN.BUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B'
                     left join LOAN.PERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P'
                     left join LOANAPP.LOANAPPTBL LP on L.LOANAPPNMB = LP.LOANAPPNMB
                     left join LOANAPP.LOANAPPPRTTBL PP on L.LOANAPPNMB = PP.LOANAPPNMB
       WHERE  L.LoanNmb = p_GPNMBR;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO UPDLOANROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOTSP TO ONPREADROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=ONP
define package_name=UAT_0330
define package_buildtime=20210330172338
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy ONP_UAT_0330 created on Tue 03/30/2021 17:23:38.57 by Jasleen Gorowada
prompt deploy scripts for deploy ONP_UAT_0330 created on Tue 03/30/2021 17:23:38.57 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy ONP_UAT_0330: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATELOANNOTEINFOCSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATELOANNOTEINFOCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.VALIDATELOANNOTEINFOCSP
( p_Identifier IN NUMBER := 0 ,
  p_LoanNmb    IN CHAR:= NULL,
  p_RECEIVEDDT		DATE := NULL,
  p_BORROWER			VARCHAR2 := NULL,
  p_AMOUNT			NUMBER := 0,
  p_BNKNM				VARCHAR2 := NULL,
  p_RetVal OUT NUMBER,
  p_ErrSeqNmb OUT NUMBER,
  p_LoanNoteInfoValidErrTxt1 out VARCHAR2,
  p_LoanNoteInfoValidErrTxt2 out VARCHAR2,
  p_LoanNoteInfoValidErrTxt3 out VARCHAR2,
  p_LoanNoteInfoValidErrTxt4 out VARCHAR2

)
AS



BEGIN
	p_ErrSeqNmb := 0;

   /* Check if date is null.*/
   IF p_RECEIVEDDT is null 
   THEN
   
	   BEGIN
	      p_ErrSeqNmb := p_ErrSeqNmb + 1;

		  p_LoanNoteInfoValidErrTxt1:= 'A note requires valid date to be entered.';

	      
	   END;
   END IF;
     --Check if Borrower name is null.
	IF p_BORROWER is null
	   THEN
   
	   BEGIN
	      p_ErrSeqNmb := p_ErrSeqNmb + 1;
	      p_LoanNoteInfoValidErrTxt2:='A note requires Borrower name to be entered.';

	   END;
   END IF;
      --Check if Amount value is null.
   	IF (nvl(p_AMOUNT,0)=0 ) 
	   THEN
   
	   BEGIN
	      p_ErrSeqNmb := p_ErrSeqNmb + 1;
	      p_LoanNoteInfoValidErrTxt3:='A note requires Original note amount to be entered.';

	      
	   END;
   END IF;
    --Check if Bank name is null.
   	IF p_BNKNM is null
	   THEN
   
	   BEGIN
	      p_ErrSeqNmb := p_ErrSeqNmb + 1;

	      p_LoanNoteInfoValidErrTxt4:='A note requires Bank name to be entered.';

	      
	   END;
   END IF;
         
      END;
/




GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO ONPREADROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO ONPUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.VALIDATELOANNOTEINFOCSP TO UPDLOANROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

create or replace PROCEDURE stgcsa.RefSBAOfcSelTSP
(
	p_RetVal OUT number,
	p_ErrVal OUT number,
	p_ErrMsg OUT varchar2,
	p_SelCur OUT SYS_REFCURSOR
)AS
 -- variable declaration
 BEGIN
	 SAVEPOINT RefSBAOfcSelTSP;

	 /* select from REFSBAOFCTBL Table */
	 BEGIN
		 OPEN p_SelCur FOR
		 SELECT DISTINCT(SBAOFCCD) 
		 FROM stgcsa.REFSBAOFCTBL;
		 
		 p_RetVal := SQL%ROWCOUNT;
		 p_ErrVal := SQLCODE;
		 p_ErrMsg := SQLERRM;
	 END;
	 
	 EXCEPTION
		 WHEN OTHERS THEN
		 BEGIN
			 p_RetVal := 0;
			 p_ErrVal := SQLCODE;
			 p_ErrMsg := SQLERRM;
		 
		 ROLLBACK TO RefSBAOfcSelTSP;
		 RAISE;
	 END;
 END RefSBAOfcSelTSP; 

GRANT EXECUTE ON stgcsa.RefSBAOfcSelTSP TO CDCONLINEREADALLROLE;
GRANT EXECUTE ON stgcsa.RefSBAOfcSelTSP TO CSAUPDTROLE;
GRANT EXECUTE ON stgcsa.RefSBAOfcSelTSP TO STGCSA;
GRANT EXECUTE ON stgcsa.RefSBAOfcSelTSP TO CDCONLINEDEVROLE;
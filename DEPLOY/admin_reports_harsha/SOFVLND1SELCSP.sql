---------------------------------------------------- Auditor Report Stored Procedure

create or replace PROCEDURE STGCSA.SOFVLND1SELCSP
(
    p_Identifier IN number := 0,
    p_StartDT1 DATE:=null,
    p_StartDT2 DATE:=null,
    p_EndDT1 DATE:=null,
    p_EndDT2 DATE:=null,
    p_ColNames OUT VARCHAR2,
    p_RetVal OUT number,
    p_ErrVal OUT number,
    p_ErrMsg OUT varchar2,
    p_SelCur OUT SYS_REFCURSOR
)AS
-- variable declaration
BEGIN
    SAVEPOINT SOFVLND1SELCSP;
	-- Returns all report names and their descriptions
	IF p_Identifier = 0
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
    SELECT 1 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Newly Funded Loans' AS RPTNM, 'System generated list from the CSA System of all new loans established (enter date range).' AS RPTDESC FROM DUAL
    UNION
    SELECT 2 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Payment Status Info' AS RPTNM, 'Provide a population of payment information (including all statuses) from the CSA system (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 3 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Prepaid Loans' AS RPTNM, 'System generated list of loans from CSA with status code 31/Prepayments' AS RPTDESC FROM DUAL
    UNION
    SELECT 4 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Prepaid Late Fees' AS RPTNM, 'System generated list of loans from CSA with status code 31/Prepayments Type 3 (Payment Posting); Status 31 Type 3 Payment Posting Loans' AS RPTDESC FROM DUAL
    UNION
    SELECT 5 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Status Change History' AS RPTNM, 'History of status changes (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 6 AS RPTID, 'startdt1,enddt1,startdt2,enddt2' AS SEARCHINPUTS, 'Accelerated Loans' AS RPTNM, 'System generated list of accelerated loans from the CSA system (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 7 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Deferred Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 60/deferral requests, including the date in which the status was, updated since  (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 8 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Deferred Loans History Log' AS RPTNM, 'System generated list of status changes to 60 Defer) from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 9 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'History Log Value Changes' AS RPTNM, 'System generated list modified fields with old and new values from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 10 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Catch Up Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 06/catch up, including the date in which the status was(enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 11 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Catch Up Loans History Log' AS RPTNM, 'System generated list of status changes to 06 (Catch Up) from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION 
    SELECT 12 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'ACH Changes' AS RPTNM, 'Population of ACH changes (accel) received since (enter date range) Note: This should not capture loans that were newly funded' AS RPTDESC FROM DUAL
    UNION
    SELECT 13 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'CDC Information Audit Change Log' AS RPTNM, 'shows recent data changes to CDC Master fields' AS RPTDESC FROM DUAL
	UNION
	SELECT 14 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Accelerations Confirmation' AS RPTNM, 'Accelerations Proc Month Status Report' AS RPTDESC FROM DUAL
	UNION
	SELECT 15 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Cycle Processing Status' AS RPTNM, 'Shows Cycle Processing Status Report' AS RPTDESC FROM DUAL
	UNION
	SELECT 16 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Extract Audit' AS RPTNM, 'Extract Audit report (STG History)' AS RPTDESC FROM DUAL;
    
        p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
-- Newly Funded Loans - System generated list from the CSA System of all new loans established (enter date range).
	ELSIF p_Identifier = 1
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
			L.LOANNMB,
			L.LOANDTLSTATCDOFLOAN,
			L.CREATDT,
			L.LOANDTLNOTEDT
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
		ORDER BY L.LOANNMB
    ;
      p_ColNames := 'Loan Number, Loan Status, Create Date, Loan Detail Note Date';
	  p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
    ELSIF p_Identifier = 20
    THEN
    BEGIN
        OPEN p_SelCur FOR
        SELECT
            1
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_StartDT1)
		ORDER BY L.LOANNMB
    ;
 
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Payment Status Info - Provide a population of payment information (including all statuses) from the CSA system (enter date range)    
  ELSIF p_Identifier = 2
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT T.LOANNMB,
      S.CREATDT,
      R.STATNM,
      S.STATID,
      T.STATIDCUR,
      T.CREATDT AS CREATDT1,
      L.LOANDTLSTATCDOFLOAN
    FROM 
      STGCSA.SOFVLND1TBL L
      LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
      LEFT JOIN STGCSA.CORETRANSSTATTBL S
        ON T.TRANSID = S.TRANSID
      LEFT JOIN STGCSA.REFTRANSSTATTBL R
        ON S.STATID = R.STATID
    WHERE T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
      AND T.TRANSTYPID = 3
    ORDER BY T.LOANNMB,
      S.CREATDT DESC
    ;

      p_ColNames := 'Loan Number, Create Date, Status Name, Status ID, Status ID Cur, Create Date 1, Loan Detail Status';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Loans - System generated list of loans from CSA with status code 31/Prepayments
	ELSIF p_Identifier = 3
    THEN
    BEGIN
        OPEN p_SelCur FOR
	SELECT 
	  L.LOANNMB,
	  L.LOANDTLSTATCDOFLOAN,
	  L.LOANDTLSTATDT
	FROM STGCSA.SOFVLND1TBL L
	WHERE L.LOANDTLSTATCDOFLOAN = '31'
	AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
	;
 
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Late Fees - System generated list of loans from CSA with status code 31/Prepayments Type 3 (Payment Posting)
	ELSIF p_Identifier = 4
    THEN
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
		  T.LOANNMB,
		  S.CREATDT,
		  R.STATNM AS TRANSTAT,
		  S.STATID,
		  L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
		  L.LOANDTLSTATDT
		FROM STGCSA.SOFVLND1TBL L
    LEFT JOIN STGCSA.CORETRANSTBL T ON T.LOANNMB = L.LOANNMB
    LEFT JOIN STGCSA.CORETRANSSTATTBL S ON T.TRANSID = S.TRANSID
    LEFT JOIN STGCSA.REFTRANSSTATTBL R ON S.STATID = R.STATID
		WHERE T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
		  AND T.TRANSTYPID = 3
		  AND L.LOANDTLSTATCDOFLOAN = '31'
		ORDER BY T.LOANNMB,
		  S.CREATDT DESC
    ;
 
      p_ColNames := 'Loan Number, Create Date, Trans Status, Status ID, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Status Change History - History of status changes.
  ELSIF p_Identifier = 5
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      updtdt, 
      pk loannmb, 
      change, 
      updtuserid 
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL'
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    AND colname='LOANDTLSTATCDOFLOAN'
    ORDER BY loannmb, updtdt
    ;
    
      p_ColNames := 'Update Date, Loan Number, Change, Username';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Accelerated Loans - System generated list of accelerated loans from the CSA system (enter date range)
  ELSIF p_Identifier = 6
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      T.LOANNMB,
      T.STATIDCUR AS TRANSSTAT,
      T.CREATDT,
      L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
      L.LOANDTLSTATDT
    FROM 
      STGCSA.SOFVLND1TBL L
      LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
    WHERE (T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    OR T.CREATDT BETWEEN TRUNC(p_StartDT2) AND TRUNC(p_EndDT2))
      AND T.TRANSTYPID = 8
    ORDER BY T.LOANNMB
    ;

      p_ColNames := 'Loan Number, Trans Status, Create Date, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deffered Loans - System generated list of loans from the CSA system with status code 60/deferral requests, including the date in which the status was, updated since (enter date range)
	ELSIF p_Identifier = 7
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
	  L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '60'
    AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deferred Loans History Log - System generated list of status changes to 60 (Defer) from the history logging.
  ELSIF p_Identifier = 8
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      UPDTDT,
      ACTION,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL' 
    AND COLNAME = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%""60""%'
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    ;
    
      p_ColNames := 'Update Date, Action, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- History Log Value Changes - System generated list modified fields with old and new values from the history logging.
	ELSIF p_Identifier = 9
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      l.UPDTDT,
      l.TABLENM,
      l.PK,
      d.COLNAME,
      '""' || d.OLDVALUE || '"" > ""' || d.NEWVALUE || '""' change,
      l.LOANNMB,
      l.UPDTUSERID,
      l.UPDTLOGID
    FROM stgcsa.STGUPDTLOGTBL l, stgcsa.STGUPDTLOGDTLTBL d
    WHERE UPDTLOGID = UPDTLOGFK
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    ORDER BY updtdt, UPDTLOGID, colname
    ;
    
      p_ColNames := 'Update Date, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Update Log ID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up Loans - System generated list of loans from the CSA system with status code 06/catch up, including the date in which the status was(enter date range)
  ELSIF p_Identifier = 10
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '06'
    AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up History Log - System generated list of status changes to 06 (Catch Up) from the history logging (enter date range)
  ELSIF p_Identifier = 11
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT
      UPDTDT,
      ACTION,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL'
    AND COLNAME = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%""06""%'
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
   ;
    
      p_ColNames := 'Update Date, Action, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- ACH Changes - Population of ACH changes received since (enter date range) Note: This should not capture loans that were newly funded
  ELSIF p_Identifier = 12
    THEN
    BEGIN
        OPEN p_SelCur FOR
     SELECT
      PK,
      LOANNMB,
      ACTION,
      COLNAME,
      CHANGE,
      UPDTDT,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory
    WHERE tablenm IN ('SOFVLND1TBL') 
    AND colname IN 
      (
        'LOANDTLACHPRENTIND',                                                           
        'LOANDTLACHBNKNM',                                                              
        'LOANDTLACHBNKMAILADRSTR1NM',                                                   
        'LOANDTLACHBNKMAILADRSTR2NM',                                                   
        'LOANDTLACHBNKMAILADRCTYNM',                                                    
        'LOANDTLACHBNKMAILADRSTCD',                                                     
        'LOANDTLACHBNKMAILADRZIPCD',                                                    
        'LOANDTLACHBNKMAILADRZIP4CD',                                                   
        'LOANDTLACHBNKBR',                                                              
        'LOANDTLACHBNKACTYP',                                                           
        'LOANDTLACHBNKACCT',                                                            
        'LOANDTLACHBNKROUTNMB',                                                         
        'LOANDTLACHBNKTRANS',                                                           
        'LOANDTLACHBNKIDNOORATTEN',                                                     
        'LOANDTLACHDEPNM',                                                              
        'LOANDTLACHSIGNDT',                                                             
        'LOANDTLACHLASTCHNGDT'
      ) 
    AND UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    ;
    
      p_ColNames := 'Primary Key, Loan Number, Action, Column Name, Change, Update Date, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- CDC Information Audit Change Log - shows recent data changes to CDC Master fields
  ELSIF p_Identifier = 13
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
	  LOG.PK,
	  CDC.CDCNM,
	  LOG.UPDTDT,
	  LOG.UPDTUSERID,
	  DET.COLNAME,
	  DET.NEWVALUE,
	  DET.OLDVALUE
	FROM
	  (SELECT * FROM STGCSA.STGUPDTLOGTBL
	  ) LOG
	INNER JOIN
	  (SELECT * FROM STGCSA.STGUPDTLOGDTLTBL
	  ) DET
	ON LOG.UPDTLOGID = DET.UPDTLOGFK
	INNER JOIN
	  (SELECT STGCSA.SOFVCDCMSTRTBL.CDCNM,
		STGCSA.SOFVCDCMSTRTBL.CDCREGNCD,
		STGCSA.SOFVCDCMSTRTBL.CDCCERTNMB,
		STGCSA.SOFVCDCMSTRTBL.CDCREGNCD
		|| STGCSA.SOFVCDCMSTRTBL.CDCCERTNMB AS PK
	  FROM STGCSA.SOFVCDCMSTRTBL
	  ) CDC
	ON LOG.PK = CDC.PK
	WHERE LOG.TABLENM = 'SOFVCDCMSTRTBL'
		-- AND LOG.UPDTDT >= TO_DATE('2018/09/24', 'yyyy/mm/dd')
		-- AND LOG.UPDTDT <= SysDate
		 AND LOG.UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
		ORDER BY
		  LOG.UPDTDT DESC,
		  LOG.UPDTUSERID DESC,
		  DET.COLNAME
	;
    
      p_ColNames := 'CDC Number, CDC Name, Last Date Modified, Last Updated By, Field Name, New Value, Old Value';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
	
-- Accelerations Proc Month Status Report
  ELSIF p_Identifier = 14
    THEN
    BEGIN
        OPEN p_SelCur FOR
	SELECT 
	  CYCPRCS.PRCSID,
	  CYCPRCS.PRCSNM,
	  PRCSCYC.CYCID,
	  PRCSCYC.CYCMOTXT,
	  PRCSCYC.CYCYR,
	  PRCSCYCSTAT.MGRRVWUSER,
	  PRCSCYCSTAT.MGRRVWDT,
	  PRCSCYC.BUSDAY1DT
	  --CYCPRCS.SORTORD
	FROM
	  (SELECT * FROM STGCSA.ACCCYCPRCSTBL
	  ) CYCPRCS
	RIGHT JOIN
	  (SELECT * FROM STGCSA.ACCPRCSCYCSTATTBL
	  ) PRCSCYCSTAT
	ON PRCSCYCSTAT.PRCSID = CYCPRCS.PRCSID
	RIGHT JOIN
	  (SELECT * FROM STGCSA.ACCPRCSCYCTBL
	  ) PRCSCYC
	ON PRCSCYC.CYCID         = PRCSCYCSTAT.CYCID
	WHERE PRCSCYC.BUSDAY1DT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
	AND CYCPRCS.PRCSID      IN (7, 8, 10, 11, 16, 17, 19, 20, 21, 22, 28, 29, 33)
	ORDER BY PRCSCYC.CYCID,
	  CYCPRCS.SORTORD
	;
	
	  p_ColNames := 'Process ID, Process Name, Cycle ID, Cycle Month, Cycle Year, Reviewer, Date Reviewed, Bussiness Day Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
	
-- Cycle Processing Status Report
ELSIF p_Identifier = 15
    THEN
    BEGIN
        OPEN p_SelCur FOR
SELECT CYCL.cycmotxt, 
       CYCL.cycyr, 
       PRCS.prcsnm, 
       CASE compflag 
         WHEN 'DONE' THEN 'COMPLETED' 
         ELSE 
           CASE Nvl(Length(To_char(setupdt)), 0) 
             WHEN 0 THEN 'NOT YET STARTED' 
             ELSE 'PROCESS ACTIVE' 
           END 
       END AS STATUS, 
       STAT.schdldt, 
       STAT.setupdt, 
       STAT.mgrrvwuser, 
       STAT.mgrrvwdt, 
       CYCL.lastupdtdt 
FROM   (SELECT cycid, 
               prcsid, 
               schdldt, 
               setupdt, 
               mgrrvwcmplt, 
               mgrrvwuser, 
               mgrrvwdt,
               CASE mgrrvwcmplt 
                 WHEN 1 THEN 'DONE' 
                 ELSE 'NOT' 
               END AS COMPFLAG 
        FROM   stgcsa.accprcscycstattbl) STAT 
       inner join (SELECT * 
                   FROM   stgcsa.accprcscyctbl) CYCL 
               ON CYCL.cycid = STAT.cycid 
       inner join (SELECT * 
                   FROM   stgcsa.acccycprcstbl) PRCS 
               ON STAT.prcsid = PRCS.prcsid 
WHERE  CYCL.lastupdtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) 
ORDER  BY PRCS.sortord
;
	
	  p_ColNames := 'Cycle Month, Cycle Year, Process Name, Status, Schedule Date, Setup Date, Manager Reviewer, Date Reviwed, Last Updated';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;

-- Extract Audit Report (STG History)
ELSIF p_Identifier = 16
    THEN
    BEGIN
        OPEN p_SelCur FOR
	SELECT
	  PK,
	  UPDTDT,
	  LOANNMB,
	  ACTION,
	  UPDTUSERID,
	  TABLENM,
	  COLNAME,
	  CHANGE,
	  LOGENTRYID,
	  CSA_SID
	FROM STGCSA.STGHISTORY
	WHERE UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) 
	;
	
	  p_ColNames := 'CDC Number, Last Date Modified, Loan Number, Action, Last Updated By, Table Name, Column Name, Change, Log Entry ID, Session ID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;	
		
		
    END IF;
EXCEPTION
    WHEN OTHERS THEN
    BEGIN
        p_RetVal := 0;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
 
        ROLLBACK TO SOFVLND1SELCSP;
        RAISE;
    END;
END SOFVLND1SELCSP;

GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSAREADALLROLE;
GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO CSAUPDTROLE;
GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSA;
-- GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSADEVROLE;
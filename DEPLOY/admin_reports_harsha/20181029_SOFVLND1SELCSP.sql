---------------------------------------------------- Auditor Report Stored Procedures

create or replace PROCEDURE STGCSA.SOFVLND1SELCSP
(
    p_Identifier IN number := 0,
    p_StartDT1 DATE:=null,
    p_StartDT2 DATE:=null,
    p_EndDT1 DATE:=null,
    p_EndDT2 DATE:=null,
    p_ColNames OUT VARCHAR2,
    p_RetVal OUT number,
    p_ErrVal OUT number,
    p_ErrMsg OUT varchar2,
    p_SelCur OUT SYS_REFCURSOR
)AS
-- variable declaration
BEGIN
    SAVEPOINT SOFVLND1SELCSP;
	-- Returns all report names and their descriptions
	IF p_Identifier = 0
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
	SELECT 1 AS RPTID, 'Newly Funded Loans' AS RPTNM, 'System generated list from the CSA System of all new loans established (enter date range).' AS RPTDESC FROM DUAL
    UNION
    SELECT 2 AS RPTID, 'Payment Status Info' AS RPTNM, 'Provide a population of payment information (including all statuses) from the CSA system (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 3 AS RPTID, 'Prepaid Loans' AS RPTNM, 'System generated list of loans from CSA with status code 31/Prepayments' AS RPTDESC FROM DUAL
    UNION
    SELECT 4 AS RPTID, 'Prepaid Late Fees' AS RPTNM, 'System generated list of loans from CSA with status code 31/Prepayments Type 3 (Payment Posting); Status 31 Type 3 Payment Posting Loans' AS RPTDESC FROM DUAL
    UNION
    SELECT 5 AS RPTID, 'Status Change History' AS RPTNM, 'History of status changes (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 6 AS RPTID, 'Accelerated Loans' AS RPTNM, 'System generated list of accelerated loans from the CSA system (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 7 AS RPTID, 'Deferred Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 60/deferral requests, including the date in which the status was, updated since  (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 8 AS RPTID, 'Deferred Loans History Log' AS RPTNM, 'System generated list of status changes to 60 Defer) from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 9 AS RPTID, 'History Log Value Changes' AS RPTNM, 'System generated list modified fields with old and new values from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 10 AS RPTID, 'Catch Up Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 06/catch up, including the date in which the status was(enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 11 AS RPTID, 'Catch Up Loans History Log' AS RPTNM, 'System generated list of status changes to 06 (Catch Up) from the history logging (enter date range)' AS RPTDESC FROM DUAL
    UNION 
    SELECT 12 AS RPTID, 'ACH Changes' AS RPTNM, 'Population of ACH changes (accel) received since (enter date range) Note: This should not capture loans that were newly funded' AS RPTDESC FROM DUAL;
    
		p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
-- Newly Funded Loans - System generated list from the CSA System of all new loans established (enter date range).
	ELSIF p_Identifier = 1
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
			L.LOANNMB,
			L.LOANDTLSTATCDOFLOAN,
			L.CREATDT,
			L.LOANDTLNOTEDT
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_StartDT1)
		ORDER BY L.LOANNMB
    ;
      p_ColNames := 'Loan Number, Loan Status, Create Date, Loan Detail Note Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
    ELSIF p_Identifier = 14
    THEN
    BEGIN
        OPEN p_SelCur FOR
        SELECT
            1
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_StartDT1)
		ORDER BY L.LOANNMB
    ;
 
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Payment Status Info - Provide a population of payment information (including all statuses) from the CSA system (enter date range)    
    ELSIF p_Identifier = 2
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT T.LOANNMB,
      S.CREATDT,
      R.STATNM,
      S.STATID,
      T.STATIDCUR,
      T.CREATDT AS CREATDT1,
      L.LOANDTLSTATCDOFLOAN
    FROM 
      STGCSA.SOFVLND1TBL L
      LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
      LEFT JOIN STGCSA.CORETRANSSTATTBL S
        ON T.TRANSID = S.TRANSID
      LEFT JOIN STGCSA.REFTRANSSTATTBL R
        ON S.STATID = R.STATID
    WHERE T.CREATDT >= TRUNC(p_StartDT1)
      AND T.TRANSTYPID = 3
    ORDER BY T.LOANNMB,
      S.CREATDT DESC
    ;

      p_ColNames := 'Loan Number, Create Date, Status Name, Status ID, Status ID Cur, Create Date 1, Loan Detail Status';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Loans - System generated list of loans from CSA with status code 31/Prepayments
	ELSIF p_Identifier = 3
    THEN
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
      L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
		  L.LOANDTLSTATDT
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.LOANDTLSTATCDOFLOAN = '31'
		AND L.LOANDTLSTATDT >= TRUNC(p_StartDT1)
    ;
 
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Late Fees - System generated list of loans from CSA with status code 31/Prepayments Type 3 (Payment Posting)
	ELSIF p_Identifier = 4
    THEN
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
      T.LOANNMB,
		  S.CREATDT,
		  R.STATNM AS TRANSTAT,
		  S.STATID,
		  L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
		  L.LOANDTLSTATDT
		FROM STGCSA.SOFVLND1TBL L
    LEFT JOIN STGCSA.CORETRANSTBL T ON T.LOANNMB = L.LOANNMB
    LEFT JOIN STGCSA.CORETRANSSTATTBL S ON T.TRANSID = S.TRANSID
    LEFT JOIN STGCSA.REFTRANSSTATTBL R ON S.STATID = R.STATID
		WHERE T.CREATDT >= TRUNC(p_StartDT1)
		  AND T.TRANSTYPID = 3
		  AND L.LOANDTLSTATCDOFLOAN = '31'
		ORDER BY T.LOANNMB,
		  S.CREATDT DESC
    ;
 
      p_ColNames := 'Loan Number, Create Date, Trans Status, Status ID, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Status Change History - History of status changes.
  ELSIF p_Identifier = 5
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      updtdt, 
      pk loannmb, 
      change, 
      updtuserid 
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL' 
    AND colname='LOANDTLSTATCDOFLOAN'
    ORDER BY loannmb, updtdt
    ;
    
      p_ColNames := 'Loan Number, Create Date, Trans Status, Status ID, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Accelerated Loans - System generated list of accelerated loans from the CSA system (enter date range)
  ELSIF p_Identifier = 6
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      T.LOANNMB,
      T.STATIDCUR AS TRANSSTAT,
      T.CREATDT,
      L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
      L.LOANDTLSTATDT
    FROM 
      STGCSA.SOFVLND1TBL L
      LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
    WHERE (T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1)
    OR T.CREATDT BETWEEN TRUNC(p_StartDT2) AND TRUNC(p_EndDT2))
      AND T.TRANSTYPID = 8
    ORDER BY T.LOANNMB
    ;

      p_ColNames := 'Loan Number, Trans Status, Create Date, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deffered Loans - System generated list of loans from the CSA system with status code 60/deferral requests, including the date in which the status was, updated since (enter date range)
	ELSIF p_Identifier = 7
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '60'
    AND L.LOANDTLSTATDT >= TRUNC(p_StartDT1)
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deferred Loans History Log - System generated list of status changes to 60 (Defer) from the history logging.
  ELSIF p_Identifier = 8
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      UPDTDT,
      A,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL' 
    AND COLNAME = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%""60""%'
    ;
    
      p_ColNames := 'Update Date, A, Table Name, PK, Column Name, Change, Loan Number, Update User ID, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- History Log Value Changes - System generated list modified fields with old and new values from the history logging.
	ELSIF p_Identifier = 9
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      l.UPDTDT,
      l.TABLENM,
      l.PK,
      d.COLNAME,
      '""' || d.OLDVALUE || '"" > ""' || d.NEWVALUE || '""' change,
      l.LOANNMB,
      l.UPDTUSERID,
      l.UPDTLOGID
    FROM stgcsa.STGUPDTLOGTBL l, stgcsa.STGUPDTLOGDTLTBL d
    WHERE UPDTLOGID = UPDTLOGFK
    ORDER BY updtdt, UPDTLOGID, colname
    ;
    
      p_ColNames := 'Update Date, Table Name, PK, Column Name, Change, Loan Number, Update User ID, Update Log ID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up Loans - System generated list of loans from the CSA system with status code 06/catch up, including the date in which the status was(enter date range)
  ELSIF p_Identifier = 10
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT 
      L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '06'
    AND L.LOANDTLSTATDT >= TRUNC(p_StartDT1)
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up History Log - System generated list of status changes to 06 (Catch Up) from the history logging (enter date range)
  ELSIF p_Identifier = 11
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT
      UPDTDT,
      A,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL'
    AND COLNAME = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%""06""%'
   ;
    
      p_ColNames := 'Update Date, A, Table Name, PK, Column Name, Change, Loan Number, Update User ID, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- ACH Changes - Population of ACH changes received since (enter date range) Note: This should not capture loans that were newly funded
  ELSIF p_Identifier = 12
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT * 
    FROM stgcsa.stghistory
    WHERE tablenm IN ('SOFVLND1TBL') 
    AND colname IN 
      (
        'LOANDTLACHPRENTIND',                                                           
        'LOANDTLACHBNKNM',                                                              
        'LOANDTLACHBNKMAILADRSTR1NM',                                                   
        'LOANDTLACHBNKMAILADRSTR2NM',                                                   
        'LOANDTLACHBNKMAILADRCTYNM',                                                    
        'LOANDTLACHBNKMAILADRSTCD',                                                     
        'LOANDTLACHBNKMAILADRZIPCD',                                                    
        'LOANDTLACHBNKMAILADRZIP4CD',                                                   
        'LOANDTLACHBNKBR',                                                              
        'LOANDTLACHBNKACTYP',                                                           
        'LOANDTLACHBNKACCT',                                                            
        'LOANDTLACHBNKROUTNMB',                                                         
        'LOANDTLACHBNKTRANS',                                                           
        'LOANDTLACHBNKIDNOORATTEN',                                                     
        'LOANDTLACHDEPNM',                                                              
        'LOANDTLACHSIGNDT',                                                             
        'LOANDTLACHLASTCHNGDT'
      ) 
    AND UPDTDT >= TRUNC(p_StartDT1)
    ;
    
      p_ColNames := 'Update Date, A, Table Name, PK, Column Name, Change, Loan Number, Update User ID, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
    BEGIN
        p_RetVal := 0;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
 
        ROLLBACK TO SOFVLND1SELCSP;
        RAISE;
    END;
END SOFVLND1SELCSP;

GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSAREADALLROLE;
GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO CSAUPDTROLE;
GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSA;
GRANT EXECUTE ON STGCSA.SOFVLND1SELCSP TO STGCSADEVROLE;
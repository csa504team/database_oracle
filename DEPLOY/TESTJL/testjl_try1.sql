-- deploy scripts for deploy testjl_try1 created on Wed 07/03/2019 11:33:09.25 by Jasleen Gorowada
define deploy_name=testjl
define package_name=try1
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors:' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script.sql 
John low committed 73cd99e on Fri Jun 21 11:25:11 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_2.sql 
John low committed 73cd99e on Fri Jun 21 11:25:11 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_3.sql 
Jasleen0605 committed a68afdf on Sun Jun 23 14:55:32 2019 -0400

*/
--
--
--
-- Deploy files start here:
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script.sql"
select sysdate from dual;
select to_char(sysdate,'xxx') from dual;
select 'all done' from dual;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_2.sql"
select 'this is script two' from dual;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\testing\test_deploy_script_3.sql"
select 'this is script three' from dual;
set echo off
select '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

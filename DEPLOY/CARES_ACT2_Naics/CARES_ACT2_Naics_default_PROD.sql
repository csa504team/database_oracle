define deploy_name=CARES_ACT2_Naics
define package_name=default_PROD
define package_buildtime=20210301113050
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CARES_ACT2_Naics_default_PROD created on Mon 03/01/2021 11:30:51.49 by Jasleen Gorowada
prompt deploy scripts for deploy CARES_ACT2_Naics_default_PROD created on Mon 03/01/2021 11:30:51.49 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CARES_ACT2_Naics_default_PROD: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\CARES2NAICSTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CARES2NAICSSELCSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\CARES2NAICSTBL.sql"
CREATE TABLE CDCONLINE.CARES2NAICSTBL
(
  LOAN_NUMBER          VARCHAR2(10 BYTE),
  PROGRAM_CODE         VARCHAR2(5 BYTE),
  PROCESS_METHOD_CODE  VARCHAR2(5 BYTE),
  STATUS_CODE          VARCHAR2(5 BYTE),
  NAICS_CODE           VARCHAR2(10 BYTE),
  CDCREGNCD            VARCHAR2(5 BYTE),
  CDCNMB               VARCHAR2(5 BYTE),
  PROCESSED_DATE       DATE,
  CREATEUSERID         VARCHAR2(32 BYTE),
  CREATDT              DATE                     DEFAULT sysdate,
  LASTUPDTUSERID       VARCHAR2(32 BYTE),
  LASTUPDTDT           DATE                     DEFAULT sysdate,
  APPROVAL_DATE        DATE
)
TABLESPACE CDCONLINEDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


ALTER TABLE CDCONLINE.CARES2NAICSTBL ADD (
  CONSTRAINT SYS_C00210838
  CHECK ("LOAN_NUMBER" IS NOT NULL)
  ENABLE VALIDATE);

GRANT SELECT ON CDCONLINE.CARES2NAICSTBL TO CDCONLINEREADALLROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CARES2NAICSSELCSP.sql"
CREATE OR REPLACE PROCEDURE CDCONLINE.CARES2NAICSSELCSP
(
	p_Identifier    IN  number := 0,
	p_CDCRgnCD      IN  char default null,
	p_CDCNmb        IN  char default null,
	p_SelCur        OUT SYS_REFCURSOR
)AS

 BEGIN

	IF p_Identifier = 0
		THEN

		 BEGIN
			 OPEN p_SelCur FOR
				 SELECT n.LOAN_NUMBER, n.PROGRAM_CODE, n.PROCESS_METHOD_CODE, n.STATUS_CODE, n.APPROVAL_DATE, n.NAICS_CODE, n.CDCREGNCD, n.CDCNMB, n.PROCESSED_DATE, n.CREATEUSERID, n.CREATDT, n.LASTUPDTUSERID, n.LASTUPDTDT, COALESCE(TRIM(L.BORRNM ), TRIM( w9.W9NM)) AS BORRNM
				 FROM 	CDCONLINE.CARES2NAICSTBL n
				        left join CDCONLINE.LOANW9TBL w9 on w9.loannmb = n.LOAN_NUMBER
				        left join CDCONLINE.LOANTBL l on l.loannmb = n.LOAN_NUMBER
				 WHERE ( (n.STATUS_CODE = 1) OR  (n.STATUS_CODE = 3) OR  (n.STATUS_CODE = 2 AND TRIM(l.LOANSTATCD) IN ('ACTIVE','CATCH-UP','DEFERRED')  ) )
				        ;
		 END;

	 ELSIF p_Identifier = 1
		THEN

		BEGIN
			 OPEN p_SelCur FOR
                SELECT 	n.LOAN_NUMBER, n.PROGRAM_CODE, n.PROCESS_METHOD_CODE, n.STATUS_CODE, n.APPROVAL_DATE, n.NAICS_CODE, n.CDCREGNCD, n.CDCNMB, n.PROCESSED_DATE, n.CREATEUSERID, n.CREATDT, n.LASTUPDTUSERID, n.LASTUPDTDT, COALESCE(TRIM(L.BORRNM ), TRIM( w9.W9NM)) AS BORRNM
                FROM 	CDCONLINE.CARES2NAICSTBL n
                        left join CDCONLINE.LOANW9TBL w9 on w9.loannmb = n.LOAN_NUMBER
				        left join CDCONLINE.LOANTBL l on l.loannmb = n.LOAN_NUMBER
                WHERE   TRIM(n.CDCREGNCD) = TRIM(p_CDCRgnCD)
                        AND TRIM(n.CDCNMB) = TRIM(p_CDCNmb)
                       	AND ( (n.STATUS_CODE = 1) OR  (n.STATUS_CODE = 3) OR  (n.STATUS_CODE = 2 AND TRIM(l.LOANSTATCD) IN ('ACTIVE','CATCH-UP','DEFERRED')  ) )
                        ;
		 END;

	 END IF;
END;
/


GRANT EXECUTE ON CDCONLINE.CARES2NAICSSELCSP TO CDCONLINEREADALLROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

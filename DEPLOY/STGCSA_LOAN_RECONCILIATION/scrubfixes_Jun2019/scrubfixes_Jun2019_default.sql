-- deploy scripts for deploy scrubfixes_jun2019_default created on Tue 07/09/2019 14:18:10.26 by johnlow
define deploy_name=scrubfixes_jun2019
define package_name=default
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1098PDFTBL.sql 
John low committed 82a794c on Thu Jun 27 13:10:21 2019 -0400

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099INTPDFTBL.sql 
John low committed 82a794c on Thu Jun 27 13:10:21 2019 -0400

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099MISCPDFTBL.sql 
John low committed 82a794c on Thu Jun 27 13:10:21 2019 -0400

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAXPDFPARSERLOGTBL.sql 
John low committed 82a794c on Thu Jun 27 13:10:21 2019 -0400

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_ANNSTMTRECIPNTTBL.sql 
John low committed 82a794c on Thu Jun 27 13:10:21 2019 -0400

-- C:\CSA\database_oracle\STGCSA\Views\sc_sofvdrtbl.sql 
John low committed 9ead828 on Tue Jul 9 14:04:28 2019 -0400

-- C:\CSA\database_oracle\STGCSA\Views\sc_refsbaofctbl.sql 
John low committed 9ead828 on Tue Jul 9 14:04:28 2019 -0400

*/
--
--
--
-- Deploy files start here:
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1098PDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1098PDFTBL 
  (
CDCREGNCD
,CDCNMB
,TAXYRNMB
,BORRNM
,BORRMAILADRSTR1NM
,BORRMAILADRSTR2NM
,BORRMAILADRCTYSTZIPCD
,LOANNMB
,TAXID
,INTPAIDAMT
,NOTEBALAMT
,SBAFEEAMT
,CDCFEEAMT
,CSAFEEAMT
,RPTBLIND
,TAX1098PDF
,PDFLOADLASTTIME
,BORRDESGNTN
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-27 12:44:39
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
CDCREGNCD
,CDCNMB
,TAXYRNMB
,BORRNM
,BORRMAILADRSTR1NM
,BORRMAILADRSTR2NM
,BORRMAILADRCTYSTZIPCD
,LOANNMB
,TAXID
,INTPAIDAMT
,NOTEBALAMT
,SBAFEEAMT
,CDCFEEAMT
,CSAFEEAMT
,RPTBLIND
/* TAX1098PDF */ ,cast('00' as raw(1))
,PDFLOADLASTTIME
,BORRDESGNTN
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1098PDFTBL ;
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099INTPDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1099INTPDFTBL 
  (
TAXYRNMB
,NAMADDRID
,TAX1099INTPDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-27 12:44:39
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
TAXYRNMB
,NAMADDRID
/* TAX1099INTPDF */ ,cast('00' as raw(1))
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1099INTPDFTBL ;
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099MISCPDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1099MISCPDFTBL 
  (
TAXYRNMB
,NAMADDRID
,TAX1099MISCPDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-27 12:44:39
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
TAXYRNMB
,NAMADDRID
/* TAX1099MISCPDF */ ,cast('00' as raw(1))
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1099MISCPDFTBL ;

-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAXPDFPARSERLOGTBL.sql"
create or replace view CDCONLINE.SC_TAXPDFPARSERLOGTBL 
  (
LOANNMB
,TAXYRNMB
,LASTUPDT
,PDFFILENM
,PDFFILEDT
,PDFTYP
,RESULT
,TAXREF
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-27 12:44:39
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,TAXYRNMB
,LASTUPDT
,PDFFILENM
,PDFFILEDT
,PDFTYP
/* RESULT */ ,cast('00' as raw(1))
,TAXREF
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAXPDFPARSERLOGTBL ;

-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_ANNSTMTRECIPNTTBL.sql "
create or replace view CDCONLINE.SC_ANNSTMTRECIPNTTBL 
  (
LOANNMB
,CDCREGNCD
,CDCNMB
,TAXID
,BORRDESGNTN
,TAXYRNMB
,BORRNM
,BORRMAILADRSTR1NM
,BORRMAILADRSTR2NM
,BORRMAILADRCTYNM
,BORRMAILADRSTCD
,BORRMAILADRZIPCD
,BORRMAILADRZIP4CD
,TAXFORM
,NOTEBALAMT
,CDCNM
,CDCPHNNMB
,LOANSTATCD
,LASTUPDT
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-26 10:33:34
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,CDCREGNCD
,CDCNMB
/* TAXID */ ,decode(ltrim(TAXID),null,TAXID,rownum)
,BORRDESGNTN
,TAXYRNMB
/* BORRNM */ ,decode(ltrim(BORRNM),null,BORRNM,'BORROWER: ' || LOANNMB)
/* BORRMAILADRSTR1NM */ ,decode(ltrim(BORRMAILADRSTR1NM),null,BORRMAILADRSTR1NM,'STREET1: ' || LOANNMB)
/* BORRMAILADRSTR2NM */ ,decode(ltrim(BORRMAILADRSTR2NM),null,BORRMAILADRSTR2NM,'STREET2: ' || LOANNMB)
,BORRMAILADRCTYNM
,BORRMAILADRSTCD
,BORRMAILADRZIPCD
,BORRMAILADRZIP4CD
,TAXFORM
,NOTEBALAMT
,CDCNM
,CDCPHNNMB
,LOANSTATCD
,LASTUPDT
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.ANNSTMTRECIPNTTBL 
 WHERE (LOANNMB, CDCREGNCD, CDCNMB, BORRDESGNTN, TAXYRNMB, ROWID) in 
 (SELECT LOANNMB, CDCREGNCD, CDCNMB, BORRDESGNTN, TAXYRNMB, MAX(ROWID) 
 FROM CDCONLINE.ANNSTMTRECIPNTTBL 
 GROUP BY LOANNMB, CDCREGNCD, CDCNMB, BORRDESGNTN, TAXYRNMB);

-- Deploy component file "C:\CSA\database_oracle\STGCSA\Views\sc_sofvdrtbl.sql"
create or replace view STGCSA.SC_SOFVDRTBL
  as select 
  -- Scrubbing View - Definition generated 2019-06-24 11:59:44
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
  * from STGCSA.SOFVDRTBL;

-- Deploy component file "C:\CSA\database_oracle\STGCSA\Views\sc_refsbaofctbl.sql"
create or replace view STGCSA.SC_REFSBAOFCTBL
  as select 
  -- Scrubbing View - Definition generated 2019-06-24 11:59:44
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
  * from STGCSA.REFSBAOFCTBL;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

/* 
ALTER TABLE CSA.SOFVDRTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE CSA.SOFVDRTBL CASCADE CONSTRAINTS;
*/

CREATE TABLE CSA.SOFVDRTBL
(
  LOANNMB         CHAR(10 BYTE)                 NOT NULL,
  CREATUSERID     VARCHAR2(32 BYTE)             NOT NULL,
  CREATDT         DATE                          DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             NOT NULL,
  LASTUPDTDT      DATE                          DEFAULT sysdate               NOT NULL
)
TABLESPACE CSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX CSA.SOFVDRTBL_PK ON CSA.SOFVDRTBL
(LOANNMB)
TABLESPACE CSAINDTBS;

ALTER TABLE CSA.SOFVDRTBL ADD (
  CONSTRAINT SOFVDRTBL_PK
  PRIMARY KEY
  (LOANNMB)
  USING INDEX CSA.SOFVDRTBL_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVDRTBL TO CSADEVROLE;

GRANT SELECT ON CSA.SOFVDRTBL TO CSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVDRTBL TO CSAUPDT;

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVDRTBL TO CSAUPDTROLE;

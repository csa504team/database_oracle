define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0802
define package_buildtime=20210802160811
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0802 created on Mon 08/02/2021 16:08:12.30 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0802 created on Mon 08/02/2021 16:08:12.30 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0802: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_column_LOANAPPTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANPARTLENDRTBL_TEMP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANCOLLATLIEN_TEMP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_column_LOANGNTYPRETBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_column_LOANGNTYTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\CALCDEBENTURECSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPINFOINSCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSUMTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPRINTSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPUPDCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPUPDTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPREINSCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANPRCSPNDLOANCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANSRVSFINUPDCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPRINTSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSUMTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANSBICCSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_column_LOANAPPTBL.sql"
alter table LoanApp.LOANAPPTBL
add (
LOANAPPCDCOTHCLSCOSTAMT NUMBER(19,4));
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANPARTLENDRTBL_TEMP.sql"
CREATE GLOBAL TEMPORARY TABLE LOANAPP.LOANPARTLENDRTBL_TEMP
(
  LOANAPPNMB          NUMBER(10),
  LOANPARTLENDRTYPCD  CHAR(1 BYTE),
  LOANPARTLENDRAMT    NUMBER(19,4),
  LOCID               NUMBER(7),
  LOANPARTLENDRNM     VARCHAR2(200 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANCOLLATLIEN_TEMP.sql"
CREATE GLOBAL TEMPORARY TABLE LOANAPP.LOANCOLLATLIEN_TEMP
(
  LOANAPPNMB                 NUMBER(10)         NOT NULL,
  LOANCOLLATSEQNMB           NUMBER(5)          NOT NULL,
  LOANCOLLATLIENSEQNMB       NUMBER(5)          NOT NULL,
  LOANCOLLATLIENHLDRNM       VARCHAR2(80 BYTE),
  LOANCOLLATLIENBALAMT       NUMBER(19,4),
  LOANCOLLATLIENPOS          NUMBER(5),
  LOANCOLLATLIENCREATUSERID  VARCHAR2(32 BYTE)  NOT NULL,
  LOANCOLLATLIENCREATDT      DATE               NOT NULL,
  LASTUPDTUSERID             VARCHAR2(32 BYTE)  DEFAULT null                  NOT NULL,
  LASTUPDTDT                 DATE               DEFAULT null                  NOT NULL,
  LOANCOLLATLIENSTATCD       CHAR(1 BYTE),
  LOANCOLLATLIENCMNT         VARCHAR2(255 BYTE),
  ROWNUM_NUM                 NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_column_LOANGNTYPRETBL.sql"
alter table Loan.LOANGNTYPRETBL
add (
LOANAPPCDCOTHCLSCOSTAMT NUMBER(19,4));
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_column_LOANGNTYTBL.sql"
alter table Loan.LOANGNTYTBL
add (
LOANAPPCDCOTHCLSCOSTAMT NUMBER(19,4));
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAUTHDOCCSP (p_LoanAppNMb                             NUMBER := NULL,
                                                    p_CreatuserId                            VARCHAR2 := NULL,
                                                    p_SelCur1                            OUT SYS_REFCURSOR,
                                                    p_SelCur2                            OUT SYS_REFCURSOR,
                                                    p_SelCur3                            OUT SYS_REFCURSOR,
                                                    p_SelCur4                            OUT SYS_REFCURSOR,
                                                    --p_SelCur5                            OUT SYS_REFCURSOR,
                                                    p_SelCur6                            OUT SYS_REFCURSOR,
                                                    --p_SelCur7 out sys_refcursor,
                                                    p_SelCur7                            OUT SYS_REFCURSOR,
                                                    p_SelCur8                            OUT SYS_REFCURSOR,
                                                    p_SelCur9                            OUT SYS_REFCURSOR,
                                                    p_SelCur10                           OUT SYS_REFCURSOR,
                                                    p_SelCur11                           OUT SYS_REFCURSOR,
                                                    p_SelCur12                           OUT SYS_REFCURSOR,
                                                    p_SelCur13                           OUT SYS_REFCURSOR,  
                                                    p_SelCur14                           OUT SYS_REFCURSOR,
                                                    p_TermInYears                        OUT FLOAT,
                                                    p_LoanNmb                            OUT VARCHAR2,
                                                    p_AppvDt                             OUT DATE,
                                                    p_DebentureAmt                       OUT NUMBER,
                                                    p_default_string                     OUT VARCHAR2,
                                                    p_EPCText                            OUT VARCHAR2,
                                                    p_LOANAPPEPCIND                      OUT CHAR,
                                                    p_REFIOPTION                         OUT VARCHAR2,
                                                    --p_ProceedText out varchar2,
                                                    --p_LoanProcdAmt out number,
                                                    p_total_LOANPROCDAMT                 OUT NUMBER,
                                                    p_LoanPartLendrAmt                   OUT NUMBER,
                                                    p_NetDebenturePercent                OUT NUMBER,
                                                    --p_ClosingCosts         OUT NUMBER,
                                                    p_LoanInterimLoanSet_flag            OUT NUMBER,
                                                    p_TotalClosing                       OUT NUMBER,
                                                    p_SubtotalAdminCosts                 OUT NUMBER,
                                                    p_TotalDebentureAdmin                OUT NUMBER,
                                                    p_UnderwriterText                    OUT VARCHAR2,
                                                    p_SubTotalText                       OUT VARCHAR2,
                                                    p_DisburseMonths                     OUT VARCHAR2,
                                                    p_OCtext                             OUT VARCHAR2,
                                                    p_TotalInterimAmt                    OUT NUMBER,
                                                    p_D2A_InterimLoanStart               OUT VARCHAR2,
                                                    p_D2B_InterimLoanRow                 OUT VARCHAR2,
                                                    p_D2C_InterimLoanEnd                 OUT VARCHAR2,
                                                    p_D3_Escrow                          OUT VARCHAR2,
                                                    p_ThirdPartyPercent                  OUT NUMBER,
                                                    p_D4B_PermLoanRow                    OUT VARCHAR2,
                                                    p_D4C_Preexisting3rdPartyLoan        OUT VARCHAR2,
                                                    p_Minimum3rdPartyLoanTerm            OUT VARCHAR2,
                                                    p_AnnualGntyFeePct                   OUT NUMBER,
                                                    p_LoanAppContribAmt                  OUT NUMBER,
                                                    p_LoanAppContribPct                  OUT NUMBER) AS
    origstate                   BOOLEAN := TRUE;
    loannmb_in                  CHAR (10) := NULL;
    loanapp_table_rec           LOANAPP.LOANAPPTBL%ROWTYPE;
    loan_table_rec              LOAN.LOANGNTYTBL%ROWTYPE;
    sbaofcd                     CHAR (5) := NULL;
    InCA                        BOOLEAN := FALSE;
    BusinessRECollateral        BOOLEAN := FALSE;
    PPCollateral                BOOLEAN := FALSE;
    MarineCollateral            BOOLEAN := FALSE;
    LiabilityInsurance          BOOLEAN := FALSE;
    ProductLiabilityInsurance   BOOLEAN := FALSE;
    DramShopInsurance           BOOLEAN := FALSE;
    MalpracticeInsurance        BOOLEAN := FALSE;
    WorkersCompInsurance        BOOLEAN := FALSE;
    OtherInsurance              BOOLEAN := FALSE;
    InsuranceTypes              VARCHAR2 (255) := NULL;
    EnvironmentalRequired       BOOLEAN := FALSE;
    /* count borrowers for H2_CertificationAgreements() */
    NumBorrowers                NUMBER := 0;
    Suffix                      VARCHAR2 (10) := NULL;
    TradeName                   VARCHAR2 (80) := NULL;
    modified_Borr_Address       VARCHAR2 (255) := NULL;
    modified_Borr_Insurance     VARCHAR2 (100) := NULL;
    modified_Guar_Address       VARCHAR2 (255) := NULL;
    modified_Guar_Insurance     VARCHAR2 (100) := NULL;
    p_ProceedText               VARCHAR2 (250) := NULL;
    p_LoanProcdAmt              NUMBER := 0;
    v_TotalInterimAmt           NUMBER :=0;
    ProcFeeToBePaid             NUMBER := 0;
    ClosingCostToBePaid         NUMBER := 0;
    PreExistingLoanAmt          NUMBER := 0;

-- Created 7/15/2021 JK for OPSMDEV 2729


    --LOAN or LOANAPP
    CURSOR OrignationRec 
    IS
        SELECT * FROM LOANAPP.LOANAPPTBL where LOANAPPNMB=p_LoanAppNmb;
        
    CURSOR ServicingRec 
    IS
        SELECT * FROM LOAN.LOANGNTYTBL where LOANAPPNMB=p_LoanAppNmb;
    --Check if OC is Guarantor
    CURSOR OCGuarantor IS
        SELECT g.*,
                  b.BusPhyAddrStr1NM
               || ' '
               || b.BusPhyAddrStr2Nm
               || ' '
               || b.BusPhyAddrCtyNm
               || ' , '
               || b.BusPhyAddrStCd
               || ' '
               || b.BusPhyAddrZipCd
                   AS Address,
               b.BusPhyAddrStCd
                   AS State,
               b.BusNm
                   AS Name,
               b.BusTrdNm
        FROM LoanApp.LoanGuarTbl g
             JOIN LoanApp.LoanBusTbl b
             ON (    g.TaxId = b.TaxId
                 AND g.LoanAppNmb = b.LoanAppNmb)
        WHERE     g.GUARBUSPERIND = 'B'
              AND g.LoanGuarOperCoInd = 'Y'
              AND g.LOANAPPNMB = p_LoanAppNmb;

    CURSOR OCGuarantor1 IS
        SELECT g.*,
                  b.BusPhyAddrStr1NM
               || ' '
               || b.BusPhyAddrStr2Nm
               || ' '
               || b.BusPhyAddrCtyNm
               || ' , '
               || b.BusPhyAddrStCd
               || ' '
               || b.BusPhyAddrZipCd
                   AS Address,
               b.BusPhyAddrStCd
                   AS State,
               b.BusNm
                   AS Name,
               b.BusTrdNm
        FROM Loan.LOANGNTYGUARTBL g
             JOIN Loan.BusTbl b
             ON (g.TaxId = b.TaxId)
        WHERE     g.GUARBUSPERIND = 'B'
              AND g.LoanGuarOperCoInd = 'Y'
              AND g.LOANAPPNMB = p_LoanAppNmb;


    CURSOR LoanAppBorr IS
        SELECT bor.*,
                  bus.BusPhyAddrStr1NM
               || ' '
               || bus.BusPhyAddrStr2Nm
               || ' '
               || bus.BusPhyAddrCtyNm
               || ','
               || bus.BusPhyAddrStCd
               || ' '
               || bus.BusPhyAddrZipCd
                   AS Address,
               bus.BusPhyAddrStCd
                   AS State,
               bus.BusNm
                   AS Name,
               bus.BusTrdNm
        FROM LOANAPP.LoanBorrTbl bor
             JOIN LOANAPP.LoanBusTbl bus
             ON (    bor.TaxId = bus.TaxId
                 AND bor.LoanAppNmb = bus.LoanAppNmb)
        WHERE     bor.BorrBusPerInd = 'B'
              AND bor.LOANAPPNMB = p_LoanAppNmb
        UNION
        --Person type borrower's information if origination=true
        SELECT bor.*,
                  p.PerPhyAddrStr1NM
               || ' '
               || p.PerPhyAddrStr2Nm
               || ' '
               || p.PerPhyAddrCtyNm
               || ' , '
               || p.PerPhyAddrStCd
               || ' '
               || p.PerPhyAddrZipCd
                   AS Address,
               p.PerPhyAddrStCd
                   AS State,
                  ocadataout (p.PerFirstNm)
               || ' '
               || CAST (p.PerInitialNm AS VARCHAR2 (2))
               || ' '
               || ocadataout (p.PerLastNm)
               || ' '
               || CAST (p.PerSfxNm AS VARCHAR2 (5))
                   AS Name,
               NULL
                   AS BusTrdNm
        FROM loanapp.LoanBorrTbl bor
             JOIN loanapp.LoanPerTbl p
             ON (    bor.TAXID = p.TaxId
                 AND bor.LoanAppNmb = p.LoanAppNmb)
        WHERE     bor.BorrBusPerInd = 'P'
              AND bor.LOANAPPNMB = p_LoanAppNmb;

    CURSOR LoanBorr IS
        SELECT bor.*,
                  bus.BusPhyAddrStr1NM
               || ' '
               || bus.BusPhyAddrStr2Nm
               || ' '
               || bus.BusPhyAddrCtyNm
               || ','
               || bus.BusPhyAddrStCd
               || ' '
               || bus.BusPhyAddrZipCd
                   AS Address,
               bus.BusPhyAddrStCd
                   AS State,
               bus.BusNm
                   AS Name,
               bus.BusTrdNm
        FROM LOAN.LoanGntyBorrTbl bor
             JOIN LOAN.BusTbl bus
             ON (bor.TaxId = bus.TaxId)
        WHERE     bor.BorrBusPerInd = 'B'
              AND bor.LOANAPPNMB = p_LoanAppNmb
        UNION
        --Person type borrower's information if origination=false
        SELECT bor.*,
                  p.PerPhyAddrStr1NM
               || ' '
               || p.PerPhyAddrStr2Nm
               || ' '
               || p.PerPhyAddrCtyNm
               || ' , '
               || p.PerPhyAddrStCd
               || ' '
               || p.PerPhyAddrZipCd
                   AS Address,
               p.PerPhyAddrStCd
                   AS State,
                  ocadataout (p.PerFirstNm)
               || ' '
               || CAST (p.PerInitialNm AS VARCHAR2 (2))
               || ' '
               || ocadataout (p.PerLastNm)
               || ' '
               || CAST (p.PerSfxNm AS VARCHAR2 (5))
                   AS Name,
               NULL
                   AS BusTrdNm
        FROM loan.LoanGntyBorrTbl bor
             JOIN loan.PerTbl p
             ON (bor.TAXID = p.TaxId)
        WHERE     bor.BorrBusPerInd = 'P'
              AND bor.LOANAPPNMB = p_LoanAppNmb;

    --Records for LoanSunProceed
    CURSOR LOANSUBPROCD IS
        SELECT *
        FROM LOANAPP.LOANSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD IN ('01',
                                     '02',
                                     '04',
                                     '10',
                                     '15',
                                     '19')
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    CURSOR LOANGNTYSUBPROCD IS
        SELECT *
        FROM LOAN.LOANGNTYSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD IN ('01',
                                     '02',
                                     '04',
                                     '10',
                                     '15',
                                     '19')
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    --Records for LoanSunProceed LOANPROCDTYPCD 15
    CURSOR LOANSUBPROCD_15 IS
        SELECT *
        FROM LOANAPP.LOANSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD = '15'
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    CURSOR LOANGNTYSUBPROCD_15 IS
        SELECT *
        FROM LOAN.LOANGNTYSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD = '15'
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    --LoanProceed Records
    CURSOR LOANPROCD IS
        SELECT *
        FROM LOANAPP.LOANPROCDTBL
        WHERE     LoanAppNmb = p_LoanAppNMb
              AND ProcdTypCd = 'E'
        ORDER BY LoanProcdTypCD;

    CURSOR LOANGNTYPROCD IS
        SELECT *
        FROM LOAN.LOANGNTYPROCDTBL
        WHERE     LoanAppNmb = p_LoanAppNMb
              AND ProcdTypCd = 'E'
        ORDER BY LoanProcdTypCD;
        
        
    --3rd Party Lender
    CURSOR LoanThirdPartySet
    IS
    SELECT LoanPartLendrNm, LoanPartLendrAmt,LOANLENDRGROSSAMT, LoanPartLendrTypCd, LocId
    FROM LoanApp.LoanPartLendrTbl  
    WHERE LoanAppNmb = p_LoanAppNMb;
    
    CURSOR LoanGntyThirdPartySet
    IS
    SELECT LoanPartLendrNm, LoanPartLendrAmt,LOANLENDRGROSSAMT, LoanPartLendrTypCd, LocId
    FROM Loan.LoanGntyPartLendrTbl  
    WHERE LoanAppNmb = p_LoanAppNMb;
    
    --Frank's request
    CURSOR LOANCOLLAT IS  
    SELECT Collat.*,ROWNUM
                FROM LoanApp.LoanCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   LoanApp.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
    CURSOR LOANGNTYCOLLAT IS
    SELECT Collat.*,ROWNUM
                FROM Loan.LoanGntyCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   Loan.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                
    OrignationRec_rec           OrignationRec%ROWTYPE;
    ServicingRec_rec            ServicingRec%ROWTYPE;
    ocguarantor_rec             OCGuarantor%ROWTYPE;
    ocguarantor_rec1            OCGuarantor1%ROWTYPE;
    LoanAppBorr_rec             LoanAppBorr%ROWTYPE;
    LoanBorr_rec                LoanBorr%ROWTYPE;
    LOANSUBPROCD_rec            LOANSUBPROCD%ROWTYPE;
    LOANGNTYSUBPROCD_rec        LOANGNTYSUBPROCD%ROWTYPE;
    LOANSUBPROCD_rec15          LOANSUBPROCD%ROWTYPE;
    LOANGNTYSUBPROCD_rec15      LOANGNTYSUBPROCD%ROWTYPE;
    LOANPROCD_rec               LOANPROCD%ROWTYPE;
    LOANGNTYPROCD_rec           LOANGNTYPROCD%ROWTYPE;
    LOANTHIRDPARTYSET_rec       LOANTHIRDPARTYSET%ROWTYPE;
    LOANGNTYTHIRDPARTYSET_rec   LOANGNTYTHIRDPARTYSET%ROWTYPE;
    LOANINTERIMLOANSET_rec      LOANTHIRDPARTYSET%ROWTYPE;
    LOANGNTYINTERIMLOANSET_rec  LOANGNTYTHIRDPARTYSET%ROWTYPE;
    LOANCOLLAT_rec              LOANCOLLAT%ROWTYPE;
    LOANGNTYCOLLAT_rec          LOANGNTYCOLLAT%ROWTYPE;
BEGIN
    SAVEPOINT LOANAUTHDOCCSP;
    -- to check if loan is in originaiton or servicing
    p_default_string := '[MISSING DATA]';

    SELECT LOANNMB
    INTO loannmb_in
    FROM LOANAPP.LOANAPPTBL
    WHERE LOANAPPNMB = p_LoanAppNmb;

    IF (loannmb_in IS NULL) THEN
        origstate := TRUE;
    ELSE
        origstate := FALSE;
    END IF;
    
        IF origstate=true then
        OPEN OrignationRec;
            LOOP 
                FETCH OrignationRec into OrignationRec_rec;
                EXIT WHEN OrignationRec%notfound;
            END LOOP;
        CLOSE OrignationRec;
    ELSE
        OPEN ServicingRec;
            LOOP 
                FETCH ServicingRec into ServicingRec_rec;
                EXIT WHEN ServicingRec%notfound;
            END LOOP;
        CLOSE ServicingRec;    
    END IF;

    --checks if in origination or servicing
    IF origstate = TRUE THEN
        --cursor to retrieve Loan information
        OPEN p_SelCur1 FOR
            SELECT CAST (NVL (LoanNmb, p_default_string) AS CHAR (25))       AS LOANNMB,
                   CAST (NVL (LoanAppNm, p_default_string) AS CHAR (25))     AS LOANAPPNM
            FROM LOANAPP.LOANAPPTBL
            WHERE LoanappNmb = p_LoanAppNmb;

        SELECT *
        INTO loanapp_table_rec
        FROM LOANAPP.LOANAPPTBL
        WHERE LoanappNmb = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur1 FOR
            SELECT CAST (NVL (LoanNmb, p_default_string) AS CHAR (25))       AS LOANNMB,
                   CAST (NVL (LoanAppNm, p_default_string) AS CHAR (25))     AS LOANAPPNM
            FROM LOAN.LOANGNTYTBL
            WHERE LoanappNmb = p_LoanAppNmb;

        SELECT *
        INTO loan_table_rec
        FROM LOAN.LOANGNTYTBL
        WHERE LoanappNmb = p_LoanAppNmb;
    END IF;



    --cursor to retrieve LOANAPPPRTTBL information
    OPEN p_SelCur2 FOR SELECT CAST (NVL (LoanAppPrtNm, p_default_string) AS VARCHAR2 (200))       AS LOANAPPPRTNM,
                              CAST (NVL (LoanAppFirsNmb, p_default_string) AS CHAR (25))          AS LOANAPPFIRSNMB,
                              CAST (NVL (LoanAppPrtStr1, p_default_string) AS VARCHAR2 (80))      AS LOANAPPPRTSTR1,
                              CAST (NVL (LoanAppPrtStr2, p_default_string) AS VARCHAR2 (80))      AS LOANAPPPRTSTR2,
                              CAST (NVL (LoanAppPrtCtyNm, p_default_string) AS VARCHAR2 (40))     AS LOANAPPPRTCTYNM,
                              CAST (NVL (LoanAppPrtStCd, p_default_string) AS CHAR (25))          AS LOANAPPPRTSTCD,
                              CAST (NVL (LoanAppPrtZipCd, p_default_string) AS CHAR (25))         AS LOANAPPPRTZIPCD
                       FROM LOANAPP.LOANAPPPRTTBL
                       WHERE LoanAppNmb = p_LoanAppNmb;

    -- cursor to retrieve OFCCD information
    IF origstate = TRUE THEN
        sbaofcd := loanapp_table_rec.LOANAPPORIGNTNOFCCD;

        OPEN p_SelCur3 FOR SELECT CAST (NVL (SBAOFC1NM, p_default_string) AS VARCHAR2 (80))        AS SBAOFC1NM,
                                  CAST (NVL (SBAOFCSTRNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCSTRNM,
                                  CAST (NVL (SBAOFCSTR2NM, p_default_string) AS VARCHAR2 (80))     AS SBAOFCSTR2NM,
                                  CAST (NVL (SBAOFCCTYNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCCTYNM,
                                  CAST (NVL (STCD, p_default_string) AS CHAR (2))                  AS STCD,
                                  CAST (NVL (ZIPCD5, p_default_string) AS CHAR (5))                AS ZIPCD5
                           FROM SBAREF.SBAOFCTBL
                           WHERE SBAOFCCD = sbaofcd;
    ELSE
        sbaofcd := loan_table_rec.LOANAPPORIGNTNOFCCD;

        OPEN p_SelCur3 FOR SELECT CAST (NVL (SBAOFC1NM, p_default_string) AS VARCHAR2 (80))        AS SBAOFC1NM,
                                  CAST (NVL (SBAOFCSTRNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCSTRNM,
                                  CAST (NVL (SBAOFCSTR2NM, p_default_string) AS VARCHAR2 (80))     AS SBAOFCSTR2NM,
                                  CAST (NVL (SBAOFCCTYNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCCTYNM,
                                  CAST (NVL (STCD, p_default_string) AS CHAR (2)),
                                  CAST (NVL (ZIPCD5, p_default_string) AS CHAR (5))
                           FROM SBAREF.SBAOFCTBL
                           WHERE SBAOFCCD = sbaofcd;
    END IF;

    --output variable for TermInYears
    IF origstate = TRUE THEN
        p_TermInYears := loanapp_table_rec.LOANAPPRQSTMATMOQTY / 12;
        IF (p_TermInYears = 120) THEN 
            p_Minimum3rdPartyLoanTerm := '7';
        ELSE
            p_Minimum3rdPartyLoanTerm := '10';
        END IF;
    ELSE
        p_TermInYears := loan_table_rec.LOANAPPRQSTMATMOQTY / 12;
        IF (p_TermInYears = 120) THEN 
            p_Minimum3rdPartyLoanTerm := '7';
        ELSE
            p_Minimum3rdPartyLoanTerm := '10';
        END IF;
    END IF;

    --Debenture amount
    IF origstate = TRUE THEN
        p_LoanNmb := NULL;
        p_AppvDt := NULL;
        p_DebentureAmt := loanapp_table_rec.LOANAPPRQSTAMT;
    ELSE
        p_LoanNmb := loan_table_rec.LOANNMB;
        p_AppvDt := loan_table_rec.LOANLASTAPPVDT;
        p_DebentureAmt := loan_table_rec.LOANCURRAPPVAMT;
    END IF;

    --Business type borrower's information if origination=true
    IF origstate = TRUE THEN
        OPEN p_SelCur4 FOR
            SELECT bor.*,
                      bus.BusPhyAddrStr1NM
                   || ' '
                   || bus.BusPhyAddrStr2Nm
                   || ' '
                   || bus.BusPhyAddrCtyNm
                   || ','
                   || bus.BusPhyAddrStCd
                   || ' '
                   || bus.BusPhyAddrZipCd
                       AS Address,
                   bus.BusPhyAddrStCd
                       AS State,
                   bus.BusNm
                       AS Name,
                   bus.BusTrdNm
            FROM LOANAPP.LoanBorrTbl bor
                 JOIN LOANAPP.LoanBusTbl bus
                 ON (    bor.TaxId = bus.TaxId
                     AND bor.LoanAppNmb = bus.LoanAppNmb)
            WHERE     bor.BorrBusPerInd = 'B'
                  AND bor.LOANAPPNMB = p_LoanAppNmb
            UNION
            --Person type borrower's information if origination=true
            SELECT bor.*,
                      p.PerPhyAddrStr1NM
                   || ' '
                   || p.PerPhyAddrStr2Nm
                   || ' '
                   || p.PerPhyAddrCtyNm
                   || ' , '
                   || p.PerPhyAddrStCd
                   || ' '
                   || p.PerPhyAddrZipCd
                       AS Address,
                   p.PerPhyAddrStCd
                       AS State,
                      ocadataout (p.PerFirstNm)
                   || ' '
                   || CAST (p.PerInitialNm AS VARCHAR2 (2))
                   || ' '
                   || ocadataout (p.PerLastNm)
                   || ' '
                   || CAST (p.PerSfxNm AS VARCHAR2 (5))
                       AS Name,
                   NULL
                       AS BusTrdNm
            FROM loanapp.LoanBorrTbl bor
                 JOIN loanapp.LoanPerTbl p
                 ON (    bor.TAXID = p.TaxId
                     AND bor.LoanAppNmb = p.LoanAppNmb)
            WHERE     bor.BorrBusPerInd = 'P'
                  AND bor.LOANAPPNMB = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur4 FOR
            --Business type borrower's information if origination=false
            SELECT bor.*,
                      bus.BusPhyAddrStr1NM
                   || ' '
                   || bus.BusPhyAddrStr2Nm
                   || ' '
                   || bus.BusPhyAddrCtyNm
                   || ','
                   || bus.BusPhyAddrStCd
                   || ' '
                   || bus.BusPhyAddrZipCd
                       AS Address,
                   bus.BusPhyAddrStCd
                       AS State,
                   bus.BusNm
                       AS Name,
                   bus.BusTrdNm
            FROM LOAN.LoanGntyBorrTbl bor
                 JOIN LOAN.BusTbl bus
                 ON (bor.TaxId = bus.TaxId)
            WHERE     bor.BorrBusPerInd = 'B'
                  AND bor.LOANAPPNMB = p_LoanAppNmb
            UNION
            --Person type borrower's information if origination=false
            SELECT bor.*,
                      p.PerPhyAddrStr1NM
                   || ' '
                   || p.PerPhyAddrStr2Nm
                   || ' '
                   || p.PerPhyAddrCtyNm
                   || ' , '
                   || p.PerPhyAddrStCd
                   || ' '
                   || p.PerPhyAddrZipCd
                       AS Address,
                   p.PerPhyAddrStCd
                       AS State,
                      ocadataout (p.PerFirstNm)
                   || ' '
                   || CAST (p.PerInitialNm AS VARCHAR2 (2))
                   || ' '
                   || ocadataout (p.PerLastNm)
                   || ' '
                   || CAST (p.PerSfxNm AS VARCHAR2 (5))
                       AS Name,
                   NULL
                       AS BusTrdNm
            FROM loan.LoanGntyBorrTbl bor
                 JOIN loan.PerTbl p
                 ON (bor.TAXID = p.TaxId)
            WHERE     bor.BorrBusPerInd = 'P'
                  AND bor.LOANAPPNMB = p_LoanAppNmb;
    END IF;


    -- If EPC transaction, then need to check if OC is guarantor. OC will always be a business.
    IF origstate = TRUE THEN
        p_LOANAPPEPCIND := loanapp_table_rec.LOANAPPEPCIND;

        IF (loanapp_table_rec.LOANAPPEPCIND = 'Y') THEN
            OPEN OCGuarantor;

            LOOP
                FETCH OCGuarantor INTO ocguarantor_rec;

                EXIT WHEN OCGuarantor%NOTFOUND;
            END LOOP;

            IF (OCGuarantor%ROWCOUNT > 0) THEN
                p_EPCText := 'Guarantor Operating Company:';
            END IF;

            CLOSE OCGuarantor;
        END IF;
    ELSE
        p_LOANAPPEPCIND := loan_table_rec.LOANAPPEPCIND;

        IF (loan_table_rec.LOANAPPEPCIND = 'Y') THEN
            OPEN OCGuarantor1;

            LOOP
                FETCH OCGuarantor1 INTO ocguarantor_rec1;

                EXIT WHEN OCGuarantor1%NOTFOUND;
            END LOOP;

            IF (OCGuarantor1%ROWCOUNT > 0) THEN
                p_EPCText := 'Guarantor Operating Company:';
            END IF;

            CLOSE OCGuarantor1;
        END IF;
    END IF;



    --List all borrowers identified in p_SelCur4 or p_SelCur5
    IF origstate = TRUE THEN
        DELETE FROM LOANAPP.Borr_Address;

        DELETE FROM LOANAPP.Borr_Insurance;

        OPEN LoanAppBorr;

        LOOP
            FETCH LoanAppBorr INTO LoanAppBorr_rec;

            EXIT WHEN LoanAppBorr%NOTFOUND;

            -- if borrower is in California
            IF (    LoanAppBorr_rec.State = 'CA'
                AND InCA = FALSE) THEN
                InCA := TRUE;
            END IF;

            IF (loanapp_table_rec.LoanAppEPCInd = 'Y') THEN
                IF (LoanAppBorr_rec.ImEPCOperCd = 2) THEN
                    Suffix := '(EPC)';
                ELSIF (LoanAppBorr_rec.ImEPCOperCd = 3) THEN
                    Suffix := '(Operating Concern)';
                END IF;
            END IF;

            IF (LoanAppBorr_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || LoanAppBorr_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Borr_Address :=
                LoanAppBorr_rec.Name || ' ' || TradeName || ' ' || Suffix || ' ' || LoanAppBorr_rec.Address;

            INSERT INTO LOANAPP.Borr_Address (Address)
            VALUES (modified_Borr_Address);

            COMMIT;

            --if any of insurance required for this borrower. Must be business.
            IF (LoanAppBorr_rec.BORRBUSPERIND = 'B') THEN
                --  dbms_output.put_line('Inside loop for BORRBUSPERIND as B'||LoanAppBorr_rec.LOANAPPNMB);
                IF (LoanAppBorr_rec.LiabInsurRqrdInd = 'Y') THEN
                    LiabilityInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                    ProductLiabilityInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                    DramShopInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.MalprctsInsurRqrdInd = 'Y') THEN
                    MalpracticeInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                    WorkersCompInsurance := TRUE;
                END IF;

                --  dbms_output.put_line('LoanAppBorr_rec.OthInsurRqrdInd: '||LoanAppBorr_rec.OthInsurRqrdInd);
                IF (LoanAppBorr_rec.OthInsurRqrdInd = 'Y') THEN
                    OtherInsurance := TRUE;

                    IF (InsuranceTypes IS NULL) THEN
                        InsuranceTypes := LoanAppBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    --   dbms_output.put_line('InsuranceTypes inside if: '||InsuranceTypes);
                    ELSE
                        InsuranceTypes := InsuranceTypes || ', ' || LoanAppBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    --   dbms_output.put_line('InsuranceTypes inside else: '||InsuranceTypes);
                    END IF;
                END IF;
            END IF;

            NumBorrowers := NumBorrowers + 1;
        --   dbms_output.put_line('LoanAppBorr_rec.Address: '||LoanAppBorr_rec.Address);
        --   dbms_output.put_line('NumBorrowers: '||NumBorrowers);
        END LOOP;

        CLOSE LoanAppBorr;

        OPEN p_SelCur6 FOR SELECT Address FROM LOANAPP.Borr_Address;
    --open p_SelCur7 for
    -- select Insurance from LOANAPP.Borr_Insurance;

    ELSE
        DELETE FROM LOANAPP.Borr_Address;

        DELETE FROM LOANAPP.Borr_Insurance;

        OPEN LoanBorr;

        LOOP
            FETCH LoanBorr INTO LoanBorr_rec;

            EXIT WHEN LoanBorr%NOTFOUND;

            --if borrower is in California
            IF (    LoanBorr_rec.State = 'CA'
                AND InCA = FALSE) THEN
                InCA := TRUE;
            END IF;

            IF (loan_table_rec.LoanAppEPCInd = 'Y') THEN
                IF (LoanBorr_rec.ImEPCOperCd = 2) THEN
                    Suffix := '(EPC)';
                ELSIF (LoanBorr_rec.ImEPCOperCd = 3) THEN
                    Suffix := '(Operating Concern)';
                END IF;
            END IF;

            IF (LoanBorr_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || LoanBorr_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Borr_Address :=
                LoanBorr_rec.Name || ' ' || TradeName || ' ' || Suffix || ' ' || LoanBorr_rec.Address;

            INSERT INTO LOANAPP.Borr_Address (Address)
            VALUES (modified_Borr_Address);

            COMMIT;

            -- dbms_output.put_line('LoanBorr_rec.Address: '||LoanBorr_rec.Address);
            --if any of insurance required for this borrower. Must be business.
            IF (LoanBorr_rec.BORRBUSPERIND = 'B') THEN
                IF (LoanBorr_rec.LiabInsurRqrdInd = 'Y') THEN
                    LiabilityInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                    ProductLiabilityInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                    DramShopInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.MalprctsInsurRqrdInd = 'Y') THEN
                    MalpracticeInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                    WorkersCompInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.OthInsurRqrdInd = 'Y') THEN
                    OtherInsurance := TRUE;

                    IF (InsuranceTypes IS NULL) THEN
                        InsuranceTypes := LoanBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    ELSE
                        InsuranceTypes := InsuranceTypes || ', ' || LoanBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    END IF;
                END IF;
            END IF;

            NumBorrowers := NumBorrowers + 1;
        END LOOP;

        CLOSE LoanBorr;

        OPEN p_SelCur6 FOR SELECT Address FROM LOANAPP.Borr_Address;
    --open p_SelCur7 for
    --select Insurance from LOANAPP.Borr_Insurance;
    END IF;

    -- If EPC/OC and if OC is guarantor, OC gets put into a separate column in authorization.
    IF origstate = TRUE THEN
        DELETE FROM LOANAPP.Guar_Address;

        DELETE FROM LOANAPP.Guar_Insurance;

        OPEN OCGuarantor;

        LOOP
            FETCH OCGuarantor INTO ocguarantor_rec;

            EXIT WHEN OCGuarantor%NOTFOUND;

            IF (ocguarantor_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || ocguarantor_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Guar_Address := ocguarantor_rec.Name || ' ' || TradeName || ' Guarantor';

            INSERT INTO LOANAPP.Guar_Address (Address)
            VALUES (modified_Guar_Address);

            COMMIT;

            IF (ocguarantor_rec.LiabInsurRqrdInd = 'Y') THEN
                LiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                ProductLiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                DramShopInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.MalprctsInsurRqrdInd = 'Y') THEN
                MalpracticeInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                WorkersCompInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.OthInsurRqrdInd = 'Y') THEN
                OtherInsurance := TRUE;

                IF (InsuranceTypes IS NULL) THEN
                    InsuranceTypes := ocguarantor_rec.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                ELSE
                    InsuranceTypes := InsuranceTypes || ', ' || ocguarantor_rec.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                END IF;
            END IF;
        -- dbms_output.put_line('ocguarantor_rec.Address: '||ocguarantor_rec.Address);
        END LOOP;

        CLOSE OCGuarantor;

        OPEN p_SelCur7 FOR SELECT Address FROM LOANAPP.Guar_Address;
    --open p_SelCur9 for
    --select Insurance from LOANAPP.Guar_Insurance;
    ELSE
        DELETE FROM LOANAPP.Guar_Address;

        DELETE FROM LOANAPP.Guar_Insurance;

        OPEN OCGuarantor1;

        LOOP
            FETCH OCGuarantor1 INTO ocguarantor_rec1;

            EXIT WHEN OCGuarantor1%NOTFOUND;

            IF (ocguarantor_rec1.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || ocguarantor_rec1.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Guar_Address := ocguarantor_rec1.Name || ' ' || TradeName || ' Guarantor';

            INSERT INTO LOANAPP.Guar_Address (Address)
            VALUES (modified_Guar_Address);

            COMMIT;

            IF (ocguarantor_rec1.LiabInsurRqrdInd = 'Y') THEN
                LiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.ProdLiabInsurRqrdInd = 'Y') THEN
                ProductLiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.LiqLiabInsurRqrdInd = 'Y') THEN
                DramShopInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.MalprctsInsurRqrdInd = 'Y') THEN
                MalpracticeInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.WORKRSCOMPINSRQRDIND = 'Y') THEN
                WorkersCompInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.OthInsurRqrdInd = 'Y') THEN
                OtherInsurance := TRUE;

                IF (InsuranceTypes IS NULL) THEN
                    InsuranceTypes := ocguarantor_rec1.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                ELSE
                    InsuranceTypes := InsuranceTypes || ', ' || ocguarantor_rec1.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                END IF;
            END IF;

            EXIT WHEN OCGuarantor1%NOTFOUND;
        --dbms_output.put_line('ocguarantor_rec1.Address: '||ocguarantor_rec1.Address);
        END LOOP;

        CLOSE OCGuarantor1;

        OPEN p_SelCur7 FOR SELECT Address FROM LOANAPP.Guar_Address;
    --open p_SelCur9 for
    --select Insurance from LOANAPP.Guar_Insurance;
    END IF;

    --Get SubProcdTbl information
    IF origstate = TRUE THEN
        OPEN p_SelCur8 FOR SELECT subprcd.*
                           FROM LOANAPP.LOANSUBPROCDTBL subprcd
                           WHERE subprcd.LOANAPPNMB = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur8 FOR SELECT subprcd.*
                           FROM LOAN.LOANGNTYSUBPROCDTBL subprcd
                           WHERE subprcd.LOANAPPNMB = p_LoanAppNmb;
    END IF;

    -- Determine if any Use of Proceeds are for debt refinancing
    IF origstate = TRUE THEN
        OPEN LOANSUBPROCD_15;

        LOOP
            FETCH LOANSUBPROCD_15 INTO LOANSUBPROCD_rec15;

            EXIT WHEN LOANSUBPROCD_15%NOTFOUND;

            IF LOANSUBPROCD_15%FOUND THEN
                p_REFIOPTION := 'and refinancing';
            END IF;
        END LOOP;

        CLOSE LOANSUBPROCD_15;
    ELSE
        OPEN LOANGNTYSUBPROCD_15;

        LOOP
            FETCH LOANGNTYSUBPROCD_15 INTO LOANGNTYSUBPROCD_rec15;

            EXIT WHEN LOANGNTYSUBPROCD_15%NOTFOUND;

            IF LOANGNTYSUBPROCD_15%FOUND THEN
                p_REFIOPTION := 'and refinancing';
            END IF;
        END LOOP;

        CLOSE LOANSUBPROCD_15;
    END IF;

    --Get SubProcdTbl information for LOANPROCDTYPCD in ('01','02','04','10','15','19')
    IF origstate = TRUE THEN
        OPEN p_SelCur9 FOR SELECT *
                           FROM LOANAPP.LOANSUBPROCDTBL
                           WHERE     LOANAPPNMB = p_LoanAppNMb
                                 AND PROCDTYPCD = 'E'
                                 AND LOANPROCDTYPCD IN ('01',
                                                        '02',
                                                        '04',
                                                        '10',
                                                        '15',
                                                        '19')
                           ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;
    ELSE
        OPEN p_SelCur9 FOR SELECT *
                           FROM LOAN.LOANGNTYSUBPROCDTBL
                           WHERE     LOANAPPNMB = p_LoanAppNMb
                                 AND PROCDTYPCD = 'E'
                                 AND LOANPROCDTYPCD IN ('01',
                                                        '02',
                                                        '04',
                                                        '10',
                                                        '15',
                                                        '19')
                           ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;
    END IF;

    --Check each Procd
    IF origstate = TRUE THEN
        p_total_LOANPROCDAMT := 0;

        OPEN LOANPROCD;

        LOOP
            FETCH LOANPROCD INTO LOANPROCD_rec;

            EXIT WHEN LOANPROCD%NOTFOUND;
            --get total project costs by summing all uses of proceeds
            p_total_LOANPROCDAMT := nvl(p_total_LOANPROCDAMT,0) + nvl(LOANPROCD_rec.LOANPROCDAMT,0);

            IF (   LOANPROCD_rec.LOANPROCDTYPCD = '01'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '02'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '04'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '10') THEN
                /* Purchase Land Only */
                CASE
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '01') THEN
                        p_ProceedText := 'Purchase Land';
                    /* Purchase Land and Existing Building */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '02') THEN
                        p_ProceedText := 'Purchase Land ' || CHR (38) || ' Building';
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '05') THEN
                        p_ProceedText := 'Construction/Remodeling';
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '10') THEN
                        p_ProceedText := 'Purchase/Install Equipment';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANPROCD_rec.LoanProcdAmt;
            /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                open LOANSUBPROCD_15;
                loop
                    fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                    exit when LOANSUBPROCD_15%notfound;
                end loop;
                close LOANSUBPROCD_15;
                p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
            ELSIF (   LOANPROCD_rec.LOANPROCDTYPCD = '15'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '19'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '21'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '22') THEN
                /* Eligible business expenses under Debt Refinancing: New use
                of proceeds introduced with revised 1244 Form.  */

                CASE
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '19') THEN
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '21') THEN
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '22') THEN
                        p_ProceedText := 'Professional Fees';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANPROCD_rec.LoanProcdAmt;
            END IF;
        END LOOP;

        CLOSE LOANPROCD;
    ELSIF origstate = FALSE THEN
        p_total_LOANPROCDAMT := 0;

        OPEN LOANGNTYPROCD;

        LOOP
            FETCH LOANGNTYPROCD INTO LOANGNTYPROCD_rec;

            EXIT WHEN LOANGNTYPROCD%NOTFOUND;
            --get total project costs by summing all uses of proceeds
            p_total_LOANPROCDAMT := nvl(p_total_LOANPROCDAMT,0) + nvl(LOANGNTYPROCD_rec.LOANPROCDAMT,0);

            IF (   LOANGNTYPROCD_rec.LOANPROCDTYPCD = '01'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '02'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '04'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '10') THEN
                /* Purchase Land Only */
                CASE
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '01') THEN
                        p_ProceedText := 'Purchase Land';
                    /* Purchase Land and Existing Building */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '02') THEN
                        p_ProceedText := 'Purchase Land ' || CHR (38) || ' Building';
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '04') THEN
                        p_ProceedText := 'Construction/Remodeling';
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '10') THEN
                        p_ProceedText := 'Purchase/Install Equipment';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANGNTYPROCD_rec.LoanProcdAmt;
            /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                open LOANSUBPROCD_15;
                loop
                    fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                    exit when LOANSUBPROCD_15%notfound;
                end loop;
                close LOANSUBPROCD_15;
                p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
            ELSIF (   LOANGNTYPROCD_rec.LOANPROCDTYPCD = '15'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '19'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '21'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '22') THEN
                /* Eligible business expenses under Debt Refinancing: New use
                of proceeds introduced with revised 1244 Form.  */

                CASE
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '19') THEN
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '21') THEN
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '22') THEN
                        p_ProceedText := 'Professional Fees';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANGNTYPROCD_rec.LoanProcdAmt;
            END IF;
        END LOOP;

        CLOSE LOANGNTYPROCD;
    END IF;


    --Get Text information as per sample
    IF origstate = TRUE THEN
        OPEN p_SelCur10 FOR SELECT CASE
                                       WHEN lp.LOANPROCDTYPCD = '01' THEN
                                           'Purchase Land'
                                       WHEN slp.LOANPROCDTYPCD = '02' THEN
                                           'Purchase Land ' || CHR (38) || ' Building'
                                       WHEN slp.LOANPROCDTYPCD = '04' THEN
                                           'Construction/Remodeling'
                                       WHEN slp.LOANPROCDTYPCD = '10' THEN
                                           'Purchase/Install Equipment'
                                       WHEN slp.LOANPROCDTYPCD = '19' THEN
                                           'Eligible Business Expenses under Debt Refinancing'
                                       WHEN slp.LOANPROCDTYPCD = '21' THEN
                                           'Other Expenses (construction contingencies, interim interest)'
                                       WHEN slp.LOANPROCDTYPCD = '22' THEN
                                           'Professional Fees'
                                       WHEN slp.LOANPROCDTYPCD = '03' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '03')
                                       WHEN slp.LOANPROCDTYPCD = '05' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '05')
                                       WHEN slp.LOANPROCDTYPCD = '06' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '06')
                                       WHEN slp.LOANPROCDTYPCD = '09' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '09')
                                       WHEN slp.LOANPROCDTYPCD = '11' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '11')
                                       WHEN slp.LOANPROCDTYPCD = '15' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '15')
                                   END
                                       AS LOANPROCDTYPDESC,
                                   NVL (lp.LOANPROCDAMT, 0)
                                       LOANPROCDAMT
                            FROM SBAREF.LOANPROCDTYPTBL slp
                                 LEFT OUTER JOIN LOANAPP.LOANPROCDTBL lp
                                 ON     lp.PROCDTYPCD = slp.PROCDTYPCD
                                    AND lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD
                                    AND lp.LOANAPPNMB = p_LoanAppNmb
                            WHERE slp.PROCDTYPCD = 'E';
    ELSE
        OPEN p_SelCur10 FOR SELECT CASE
                                       WHEN lp.LOANPROCDTYPCD = '01' THEN
                                           'Purchase Land'
                                       WHEN slp.LOANPROCDTYPCD = '02' THEN
                                           'Purchase Land ' || CHR (38) || ' Building'
                                       WHEN slp.LOANPROCDTYPCD = '04' THEN
                                           'Construction/Remodeling'
                                       WHEN slp.LOANPROCDTYPCD = '10' THEN
                                           'Purchase/Install Equipment'
                                       WHEN slp.LOANPROCDTYPCD = '19' THEN
                                           'Eligible Business Expenses under Debt Refinancing'
                                       WHEN slp.LOANPROCDTYPCD = '21' THEN
                                           'Other Expenses (construction contingencies, interim interest)'
                                       WHEN slp.LOANPROCDTYPCD = '22' THEN
                                           'Professional Fees'
                                       WHEN slp.LOANPROCDTYPCD = '03' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '03')
                                       WHEN slp.LOANPROCDTYPCD = '05' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '05')
                                       WHEN slp.LOANPROCDTYPCD = '06' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '06')
                                       WHEN slp.LOANPROCDTYPCD = '09' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '09')
                                       WHEN slp.LOANPROCDTYPCD = '11' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '11')
                                       WHEN slp.LOANPROCDTYPCD = '15' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '15')
                                   END
                                       AS LOANPROCDTYPDESC,
                                   NVL (lp.LOANPROCDAMT, 0)
                                       LOANPROCDAMT
                            FROM SBAREF.LOANPROCDTYPTBL slp
                                 LEFT OUTER JOIN LOAN.LOANGNTYPROCDTBL lp
                                 ON     lp.PROCDTYPCD = slp.PROCDTYPCD
                                    AND lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD
                                    AND lp.LOANAPPNMB = p_LoanAppNmb
                            WHERE slp.PROCDTYPCD = 'E';
    
    END IF;
    
    /*Total 3rd loans equals sum of all 3rd party loans that are permanent financing.*/
    IF origstate=true THEN
                p_LoanPartLendrAmt :=0;
                open LoanThirdPartySet;
                LOOP 
                    fetch LoanThirdPartySet INTO LoanThirdPartySet_rec;
                    EXIT WHEN LoanThirdPartySet%notfound;
                    IF LoanThirdPartySet_rec.LoanPartLendrTypCd='P' THEN
                        p_LoanPartLendrAmt := nvl(p_LoanPartLendrAmt,0) + nvl(LoanThirdPartySet_rec.LoanPartLendrAmt,0);
                        PreExistingLoanAmt := nvl(LoanThirdPartySet_rec.LOANLENDRGROSSAMT,0) - nvl(LoanThirdPartySet_rec.LOANPARTLENDRAMT,0);
                        p_D4B_PermLoanRow := 'a.	'||LoanThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanThirdPartySet_rec.LoanPartLendrAmt;
                        IF (PreExistingLoanAmt > 0 ) THEN 
                            p_D4C_Preexisting3rdPartyLoan := 'In addition to this amount, the Third Party Lender mortgage will include a pre-existing non-project debt in the amount of '||PreExistingLoanAmt;
                        END IF;
                    END IF;
                END LOOP;
                CLOSE LoanThirdPartySet;
    ELSE
                p_LoanPartLendrAmt :=0;
                open LoanGntyThirdPartySet;
                LOOP 
                    fetch LoanGntyThirdPartySet INTO LoanGntyThirdPartySet_rec;
                    EXIT WHEN LoanGntyThirdPartySet%notfound;
                    IF LoanGntyThirdPartySet_rec.LoanPartLendrTypCd='P' THEN
                        p_LoanPartLendrAmt := nvl(p_LoanPartLendrAmt,0) + nvl(LoanGntyThirdPartySet_rec.LoanPartLendrAmt,0);
                        PreExistingLoanAmt := nvl(LoanGntyThirdPartySet_rec.LOANLENDRGROSSAMT,0) - nvl(LoanGntyThirdPartySet_rec.LOANPARTLENDRAMT,0);
                        p_D4B_PermLoanRow := 'a.	'||LoanGntyThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanGntyThirdPartySet_rec.LoanPartLendrAmt;
                        IF (PreExistingLoanAmt > 0 ) THEN 
                            p_D4C_Preexisting3rdPartyLoan := 'In addition to this amount, the Third Party Lender mortgage will include a pre-existing non-project debt in the amount of '||PreExistingLoanAmt;
                        END IF;
                    END IF;
                END LOOP;
                CLOSE LoanGntyThirdPartySet;
    END IF;
    IF origstate=true THEN
        p_NetDebenturePercent := round((nvl(OrignationRec_rec.LOANAPPCDCNETDBENTRAMT,0) / nvl(p_total_LOANPROCDAMT,0)) * 100,2);
        --p_ClosingCosts := least(OrignationRec_rec.LOANAPPCDCCLSCOSTAMT, 2500);
        p_TotalClosing := nvl(OrignationRec_rec.LOANAPPCDCCLSCOSTAMT,0) + nvl(OrignationRec_rec.LOANAPPCDCOTHCLSCOSTAMT,0);
        /* Pay Processing Fee upfront. */
        p_SubTotalText := 'Subtotal(b.1 through b.5)';
        ProcFeeToBePaid := OrignationRec_rec.LoanAppCDCPrcsFeeAmt;
        IF (OrignationRec_rec.SEPRATECLOSINGCOSTIND='Y') THEN
            ProcFeeToBePaid := 0;
            p_UnderwriterText := 'If the CDC Processing fee is paid for by the Borrower and the Borrower is not to be reimbursed, 
                                b.3 is added to the sum of a. and b.5 Subtotal when computing the Underwriters Fee.';
            p_SubTotalText := 'Subtotal(b.1, b.2 and b.4 because borrower pays for b.3.)';
        END IF;
        /* Pay Closing Costs upfront */
        ClosingCostToBePaid := p_TotalClosing;
        IF (OrignationRec_rec.SEPRATECLOSINGCOSTIND='Y') THEN
            ClosingCostToBePaid := 0;
            IF (OrignationRec_rec.LOANAPPCDCSEPRTPRCSFEEIND ='Y') THEN
                p_SubTotalText := 'Subtotal(b.1 and b.2 because borrower pays 
                                for b.3 and b.4)';
            ELSE
                p_SubTotalText := 'Subtotal(b.1, b.2 and b.3 because borrower pays for b.4)';
            END IF;
        END IF;
        p_SubtotalAdminCosts := nvl(OrignationRec_rec.LoanAppCDCGntyAmt,0)
                                + nvl(OrignationRec_rec.LoanAppCDCFundFeeAmt,0)
                                + nvl(OrignationRec_rec.LoanAppCDCPrcsFeeAmt,0)
                                + nvl(p_TotalClosing,0);
        p_TotalDebentureAdmin := nvl(p_SubtotalAdminCosts,0) + 
                                nvl(OrignationRec_rec.LOANAPPCDCUNDRWTRFEEAMT,0);
        CASE WHEN (OrignationRec_rec.DISBDEADLNDT is not null and OrignationRec_rec.LOANINITLAPPVDT is not null) THEN
            p_DisburseMonths := to_char(MONTHS_BETWEEN(OrignationRec_rec.DISBDEADLNDT,OrignationRec_rec.LOANINITLAPPVDT));
        WHEN (OrignationRec_rec.PRCSMTHDCD != '5RE' or OrignationRec_rec.PRCSMTHDCD != '5RX') THEN
            p_DisburseMonths := '48';
            p_AnnualGntyFeePct := 0.4865;
        WHEN (OrignationRec_rec.PRCSMTHDCD = '5RE' or OrignationRec_rec.PRCSMTHDCD = '5RX') THEN
            p_DisburseMonths := '9';
            p_AnnualGntyFeePct := 0.4517;
        END CASE;
        OPEN p_SelCur11 for
                    SELECT * FROM LOANAPP.LOANAPPTBL where LOANAPPNMB=p_LoanAppNmb;   
                                



    ELSE
        p_NetDebenturePercent := round((nvl(ServicingRec_rec.LOANAPPCDCNETDBENTRAMT,0) / nvl(p_total_LOANPROCDAMT,0)) * 100,2);
        --p_ClosingCosts := least(ServicingRec_rec.LOANAPPCDCCLSCOSTAMT, 2500);
        p_TotalClosing := nvl(ServicingRec_rec.LOANAPPCDCCLSCOSTAMT,0) + nvl(ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT,0);
        /* Pay Processing Fee upfront. */
        p_SubTotalText := 'Subtotal(b.1 through b.5)';
        ProcFeeToBePaid := ServicingRec_rec.LoanAppCDCPrcsFeeAmt;
        IF (ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT > 0) THEN
            ProcFeeToBePaid := 0;
            p_UnderwriterText := 'If the CDC Processing fee is paid for by the Borrower and the Borrower is not to be reimbursed, 
                                b.3 is added to the sum of a. and b.5 Subtotal when computing the Underwriters Fee.';
            p_SubTotalText := 'Subtotal(b.1, b.2 and b.4 because borrower pays for b.3.)';
        END IF;
        /* Pay Closing Costs upfront */
        ClosingCostToBePaid := p_TotalClosing;
        IF (ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT > 0) THEN
            ClosingCostToBePaid := 0;
            IF (ServicingRec_rec.LOANAPPCDCSEPRTPRCSFEEIND ='Y') THEN
                p_SubTotalText := 'Subtotal(b.1 and b.2 because borrower pays 
                                for b.3 and b.4)';
            ELSE
                p_SubTotalText := 'Subtotal(b.1, b.2 and b.3 because borrower pays for b.4)';
            END IF;
        END IF;


        p_SubtotalAdminCosts := nvl(ServicingRec_rec.LoanAppCDCGntyAmt,0)
                        + nvl(ServicingRec_rec.LoanAppCDCFundFeeAmt,0)
                        + nvl(ServicingRec_rec.LoanAppCDCPrcsFeeAmt,0)
                        + nvl(p_TotalClosing,0);
        p_TotalDebentureAdmin := p_SubtotalAdminCosts 
                        + nvl(ServicingRec_rec.LOANAPPCDCUNDRWTRFEEAMT,0);
                        
        CASE WHEN (ServicingRec_rec.DISBDEADLNDT is not null and ServicingRec_rec.LOANINITLAPPVDT is not null) THEN
            p_DisburseMonths := to_char(MONTHS_BETWEEN(ServicingRec_rec.DISBDEADLNDT,ServicingRec_rec.LOANINITLAPPVDT));
        WHEN (ServicingRec_rec.PRCSMTHDCD != '5RE' or ServicingRec_rec.PRCSMTHDCD != '5RX') THEN
            p_DisburseMonths := '48';
        WHEN (ServicingRec_rec.PRCSMTHDCD = '5RE' or ServicingRec_rec.PRCSMTHDCD = '5RX') THEN
            p_DisburseMonths := '9';
        END CASE;  
        OPEN p_SelCur11 for
            SELECT * FROM LOAN.LOANGNTYTBL where LOANAPPNMB=p_LoanAppNmb;

                        
    END IF;
    
    IF p_EPCText = 'Guarantor Operating Company:' THEN
        p_OCtext := ', Operating Company';
    END IF;
    
       /*Interim Loan */
       p_LoanInterimLoanSet_flag :=0;
    IF origstate=true THEN
                OPEN LoanThirdPartySet;
                LOOP 
                    FETCH LoanThirdPartySet INTO LoanInterimLoanSet_rec;
                    EXIT WHEN LoanThirdPartySet%notfound;
                    IF LoanInterimLoanSet_rec.LoanPartLendrTypCd='I' THEN
                        p_LoanInterimLoanSet_flag :=1;
                        v_TotalInterimAmt := nvl(v_TotalInterimAmt,0) + nvl(LoanInterimLoanSet_rec.LoanPartLendrAmt,0);
                    END IF;
                END LOOP;
                CLOSE LoanThirdPartySet;
    ELSE
                OPEN LoanGntyThirdPartySet;
                LOOP 
                    FETCH LoanGntyThirdPartySet INTO LoanGntyInterimLoanSet_rec;
                    EXIT WHEN LoanGntyThirdPartySet%notfound;
                    IF LoanGntyInterimLoanSet_rec.LoanPartLENDrTypCd='I' THEN
                        p_LoanInterimLoanSet_flag :=1;
                        v_TotalInterimAmt := nvl(v_TotalInterimAmt,0) + nvl(LoanGntyInterimLoanSet_rec.LoanPartLENDrAmt,0);
                    END IF;
                END LOOP;
                CLOSE LoanGntyThirdPartySet;
    END IF;
    
    IF p_LoanInterimLoanSet_flag = 1 THEN
        p_D2A_InterimLoanStart := 'Interim Financing (paid off by the Debenture):
                                 a. Interim Lender: An interim loan in the total principal amount of '||v_TotalInterimAmt||' will be provided by the following lender(s) ("Interim Lender"):';
        p_TotalInterimAmt := v_TotalInterimAmt;
         IF origstate=true THEN
                DELETE FROM LOANAPP.LoanPartLENDrTbl_Temp;
                OPEN LoanThirdPartySet;
                    LOOP
                        FETCH LoanThirdPartySet INTO LoanThirdPartySet_rec;
                        EXIT WHEN LoanThirdPartySet%notfound;
                        IF LoanThirdPartySet_rec.LoanPartLENDrTypCd='I' THEN  
                            p_D2B_InterimLoanRow := '(1)	'||LoanThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanThirdPartySet_rec.LoanPartLendrAmt;
                            
                            
                            insert INTO LOANAPP.LoanPartLENDrTbl_Temp(LOANAPPNMB,LOANPARTLENDRTYPCD,LOANPARTLENDRAMT,LOCID,LOANPARTLENDRNM)
                            VALUES(p_LOANAPPNMB,LoanThirdPartySet_rec.LOANPARTLENDRTYPCD,LoanThirdPartySet_rec.LOANPARTLENDRAMT,
                            LoanThirdPartySet_rec.LOCID,LoanThirdPartySet_rec.LOANPARTLENDRNM);
                            COMMIT;
                        END IF;            
                    END LOOP;
                    CLOSE LoanThirdPartySet;
                    OPEN p_SelCur12 for
                    SELECT * FROM LOANAPP.LoanPartLENDrTbl_Temp;           
            ELSE
                DELETE FROM LOANAPP.LoanPartLENDrTbl_Temp;
                OPEN LoanGntyThirdPartySet;
                    LOOP
                        FETCH LoanGntyThirdPartySet INTO LoanGntyThirdPartySet_rec;
                        EXIT WHEN LoanGntyThirdPartySet%notfound;
                        IF LoanGntyThirdPartySet_rec.LoanPartLENDrTypCd='I' THEN     
                            p_D2B_InterimLoanRow := '(1)	'||LoanGntyThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanGntyThirdPartySet_rec.LoanPartLendrAmt;
                            

                            insert INTO LOANAPP.LoanPartLENDrTbl_Temp(LOANAPPNMB,LOANPARTLENDRTYPCD,LOANPARTLENDRAMT,LOCID,LOANPARTLENDRNM)
                            VALUES(p_LOANAPPNMB,LoanGntyThirdPartySet_rec.LOANPARTLENDRTYPCD,LoanGntyThirdPartySet_rec.LOANPARTLENDRAMT,
                            LoanGntyThirdPartySet_rec.LOCID,LoanGntyThirdPartySet_rec.LOANPARTLENDRNM);
                            COMMIT;
                        END IF;
                      
                    END LOOP;
                    CLOSE LoanGntyThirdPartySet;
                    OPEN p_SelCur12 for
                    SELECT * FROM LOANAPP.LoanPartLENDrTbl_Temp; 
            END IF;
        p_D2C_InterimLoanEnd := 'Application of Net Debenture Proceeds to Interim Loan: Upon sale of the Debenture, the Net Debenture Proceeds (the portion of Debenture Proceeds that finance Project Cost) 
                               will be applied to pay off the balance of the interim loan. If the Interim Lender is also the Third Party Lender, this payment will reduce the total balance owed to Third Party 
                               Lender to the amount specified in Paragraph XX. below.
                               c. Required Certifications Before 504 Loan Closing: Following completion of the Project, but no earlier than the 5th day of the month prior to the month in which the CDC submits 
                               this loan to SBA for debenture funding, CDC must cause Interim Lender to certify the amount of the interim loan disbursed, that the interim loan has been disbursed in reasonable 
                               compliance with this Authorization, and that it has no knowledge of any unremedied substantial adverse change in the condition of the Borrower and Operating Company since the date 
                               of the loan application to the Interim Lender.';
    ELSE
        p_D3_Escrow := 'Escrow Closing (No Interim Financing):
                        The project will not require interim financing. The following will be required for an escrow closing:
                        a. Escrow Account:
                        (1) SBA must approve the escrow agreement. Escrow agent must be approved by CDC and SBA and follow escrow instructions provided by CDC and SBA.
                        (2) Debenture sale funds must be wired directly into the escrow account. 
                        (3) The funds in escrow may not be distributed and the escrow account may not be dissolved until SBA is satisfied that all collateral documents have been properly filed, lien positions 
                            properly perfected, and a final title policy (or other evidence of title if no title policy is required) issued showing required title and lien position.
                        (4) The escrow must close no later than 5 months from the date of the Debenture sale.
                        If it does not, the escrow funds will be used to pay off the Debenture in full.
                        (5) Borrower must deposit an additional 10% of the total project costs. This deposit must be in cash or an irrevocable Letter of Credit.
                        b. Borrower must agree in writing:
                        (1) That the required deposit must be used to make up any deficiency in the escrow account due to costs associated with the Debenture sale;
                        (2) To pay all costs of prepaying the Debenture if any lien position required by this Authorization is not perfected in a reasonable time (not over 5 months) after disbursement of Debenture 
                            proceeds including:
                        (a) Costs associated with the Debenture sale including any CDC processing fee not paid by SBA, CSA fee, CDC attorney fee/closing costs, SBA Guarantee Fee, Funding Fee, and Underwriter�s Fee.
                        (b) The cost of prepaying a Debenture including the prepayment premium and 6 months worth of interest on the original Debenture amount.';


    END IF; 
    p_ThirdPartyPercent := round((nvl(p_LoanPartLendrAmt,0)/ nvl(p_total_LOANPROCDAMT,0)) * 100,2);

    /* Borrower's Cntrobution */
    IF origstate=true THEN
    p_LoanAppContribAmt := OrignationRec_rec.LoanAppContribAmt;
    p_LoanAppContribPct := OrignationRec_rec.LoanAppContribPct;
    ELSE
    p_LoanAppContribAmt := ServicingRec_rec.LoanAppContribAmt;
    p_LoanAppContribPct := ServicingRec_rec.LoanAppContribPct;
    END IF;
    
    --Frank's queries
    IF origstate = TRUE THEN
        OPEN p_SelCur13 FOR SELECT Collat.*,
                        ROWNUM
                FROM LoanApp.LoanCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   LoanApp.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                DELETE FROM LOANAPP.LOANCOLLATLIEN_TEMP;
                OPEN LOANCOLLAT;
                    LOOP
                        FETCH LOANCOLLAT INTO LOANCOLLAT_rec;
                        EXIT WHEN LOANCOLLAT%notfound;                            
                            insert INTO LOANCOLLATLIEN_TEMP
                            select LOANCOLLATLIEN.*,  ROWNUM
                            from LOANAPP.LOANCOLLATLIENTBL LOANCOLLATLIEN
                            where LOANAPPNMB = LOANCOLLAT_rec.LOANAPPNMB and LoanCollatSeqNmb = LOANCOLLAT_rec.LoanCollatSeqNmb;
                            COMMIT;                           
                    END LOOP;
        OPEN p_SelCur14 FOR SELECT * FROM LOANAPP.LOANCOLLATLIEN_TEMP;
    ELSE
        OPEN p_SelCur13 FOR SELECT Collat.*,
                        ROWNUM
                FROM Loan.LoanGntyCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   Loan.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                DELETE FROM LOANAPP.LOANCOLLATLIEN_TEMP;
                OPEN LOANCOLLAT;
                    LOOP
                        FETCH LOANCOLLAT INTO LOANCOLLAT_rec;
                        EXIT WHEN LOANCOLLAT%notfound;                            
                            insert INTO LOANCOLLATLIEN_TEMP
                            select LOANCOLLATLIEN.*,  ROWNUM
                            from LOANAPP.LOANCOLLATLIENTBL LOANCOLLATLIEN
                            where LOANAPPNMB = LOANCOLLAT_rec.LOANAPPNMB and LoanCollatSeqNmb = LOANCOLLAT_rec.LoanCollatSeqNmb;
                            COMMIT;                           
                    END LOOP;
        OPEN p_SelCur14 FOR SELECT * FROM LOANAPP.LOANCOLLATLIEN_TEMP;
    END IF;   
    
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            ROLLBACK TO LOANAUTHDOCCSP;
            RAISE;
        END;
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPSELTSP (p_identifier    IN     NUMBER DEFAULT 0,
                                                   p_loanappnmb    IN     NUMBER DEFAULT 0,
                                                   p_imfundofccd   IN     CHAR DEFAULT NULL,
                                                   p_prtseqnmb     IN     NUMBER DEFAULT NULL,
                                                   p_datefrom      IN     DATE DEFAULT NULL,
                                                   p_dateto        IN     DATE DEFAULT NULL,
                                                   p_RqstSEQNMB    IN     NUMBER DEFAULT NULL,
                                                   p_LocId         IN     NUMBER DEFAULT NULL,
                                                   p_retval           OUT NUMBER,
                                                   p_selcur           OUT SYS_REFCURSOR)
AS
    /* Date         : Jul 10 2000
       Programmer   : Gulam Samdani
       Remarks      :
        Parameters   :  No.   Name        Type     Description
                        1     @p_Identifier  int     Identifies which batch
                                                   to execute.
                        2     @RetVal      int     No. of rows returned/
                                                   affected
                        3    Primary key columns
    ***********************************************************************************
    Revised By      Date        Comments
    ----------     --------    ----------------------------------------------------------
    Sheri Stine     10/01/00    Removed columns that were deleted from table.
    *********************************************************************************
    Sheri Stine     11/04/00    Removed columns LoanAppNatrTxt,LoanAppBusEstbDt
                                LoanAppBusOwnrshpEstbDt
    *********************************************************************************
    Sheri Stine     01/17/01    Renamed RaceCd, SICCd, SICSeceptionSeqNmb, IMRtTypCd
                                BusTypCd, LoanAppPurpsCd and LoanAppAdjPrd
                                Removed LoanAppBaseRtSourc
    *********************************************************************************
    Sheri Stine     01/18/01    Removed LoanAppExptrInd, LoanAppStbyAgrmtInd,
                                LoanAppstbyAgrmtAmt, LoanAppCntctAppvDt
    *********************************************************************************
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP -- 02/28/2013 -- modified to add new columns LOANNMB,LOANAPPFUNDDT,UNDRWRITNGBY,
    COHORTCD,FUNDSFISCLYR,LOANTYPIND in id=0,16
    RSURAPA - 10/23/2013 -- Modified the procedure to add new column LOANAPPINTDTLCD
    RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
    RGG --   11/07/2013 -- Modified to rename the column LoanAppPymtAdjPrdCd to LOANPYMNINSTLMNTFREQCD
    SP  --   11/26/2013 -- Removed refernces to  columns :IMRtTypCd,LoanAppFixVarInd,
    LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd in id= 0
    SP  --   04/07/2014 -- Modified to remove NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd as these are moved to loanappprojtbl
    SB -- Added Identifier 38 for Extract XML
    RGG -- 03/25/2015 -- Added UNDRWRITNGCRDRISKCERTIND  to ID 0.
    NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
    RGG -- 04/01/2016 : Added LOANAGNTINVLVDIND to ID 38
    SS--04/21/2016 Modified to add new column LOANAMTLIMEXMPTIND
    BR -- 08/11/2016 Modified to add new column VNDRTYPIND
    SS--09/29/2016 Modified procedure to add new column LoanPymntSchdlFreq OPSMDEV-1123
    SS--11/29/2016 Added LOANGNTYMATSTIND OPSMDEV  1245
    NK-- 02/06/2017--OPSMDEV-1337-- Added LoanAppLspLastNm,LoanAppLspFirstNm, LoanAppLspInitialNm, LoanAppLspSfxNm, LoanAppLspTitlTxt, LoanAppLspPhn, LoanAppLspFax, LoanAppLspEMail in Id 38
    NK-- 02/27/2017-- OPSMDEV-1372-- Added LoanGntyNoteDt in ID 38
    NK-- 03/16/2017-- OPSMDEV-1385-- Added e.LoanEconDevObjctCd in ID 38
    SS--05/19/2017 --OPSMDEV 1436 Added CDCServFeePct to identifier 0.Added identifier 18
    RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0 and 38
    NK--09/13/2017--OPSMDEV 1529-- Added  b.LoanAppPrtNm as LenderLSPName, b.LoanAppLspHQLocId as LenderLSPLocId
    SS--10/17/2017 --OPSMDEV 1548-- Added LoanAppDisasterCntrlNmb to identifier 16
    JP - 10/26/2017 -- OPSMDEV-1123 -- Restored LoanPymntSchdlFreq column name in Identifier 38
    JP - 11/15/2017 -- CAFSOPER-1219 -- Got lenderlspname in Identifier 38 from partner schema
    SS--11/15/2017--OPSMDEV 1573 --Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm   ,LoanPrtAltCntctLastNm  ,LoanPrtAltCntctInitialNm  ,LoanPrtAltCntctTitlTxt  ,LoanPrtAltCntctPrimPhn  ,LoanPrtAltCntctCellPhn  ,
 LoanPrtAltCntctemail,LendrAltCntctTypCd  to identifier 38
   SS--11/16/2015 --OPSMDEV1569  Added LoanBusEstDt to identifier 38
   SS-- OPSMDEV 1573 Dropped  loanapplsplastnm,b.loanapplspfirstnm,loanapplspinitialnm,loanapplsptitltxt,loanapplspphn,loanapplspemail from identifier 38
   NK--01/12/2017--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0,38
   SS--Opsmdev 1081--01/24/2018-- added identifier 19
   JP -- 05/07/2018 -- OPSMDEV 1716 - for  LoanIntBaseRt and LoanIntRt fields formatted to 5 digit decimal as 'FM990.00000'
  SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to identifier 0 and 38
  SS--11/16/2018--Added function to identifier 38 for encryption OPSMDEV 2043
 SS--06/18/2019--OPSMDEV 2230 Added RqstSeqNmb to identifier 0 and 38
  JP -- 07/29/2019 OPSMDEV 2255 added p_identifier 20 and added parameters p_RqstSEQNMB and p_LocId
  SS-- 08/28/2019  Added identifier 21
  RY:09/04/2019: OPSMDEV-2271:: Added LastUpdtDt in id-19
  SS-03/03/2020 Added identifier 21 OPSMDEV 2406
  SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to identifier 0 and 38
  SL--01/04/2021 SODSTORY-317  Added LOANAPPPROJLMIND to the identerfier 38
  SL--01/05/2021 SODSTORY-318  Added LOANAPPPROJSIZECD to the identerfier 38
  SL--01/07/2021 SODSTORY-360/361.  Added: LOANAPPUSRESIND, PPPLOANNMB, LOANAPPPROJDELQIND, EIDLLOANNMB,LOANAPPPROJGRORCPTAMT1,LOANAPPPROJGRORCPTAMT2
  SL--01/07/2021 SODSTORY-554 Added preprocessing dates to id 18
  SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT  
  BO--06/28/2021--OPSMDEV-2768 -- Added PrtyGrpInd column to the "SELECT" list where identifier is 0 and 18
*/

    v_maxlqdcrseqnmb   NUMBER;
BEGIN
    --SAVEPOINT loanappsel;

    /* Select Row On the basis of Key*/
    --IF p_identifier = 0 THEN
    CASE p_identifier
        WHEN 0
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappnmb,
                                         prgmcd,
                                         prcsmthdcd,
                                         loanappnm,
                                         loanapprecvdt,
                                         loanappentrydt,
                                         loanapprqstamt,
                                         loanappsbagntypct,
                                         loanappsbarcmndamt,
                                         loanapprqstmatmoqty,
                                         loanappepcind,
                                         loanapppymtamt,
                                         loanappfullamortpymtind,
                                         loanappmointqty,
                                         loanapplifinsurrqmtind,
                                         loanapprcnsdrtnind,
                                         loanappinjctnind,
                                         loanappewcpsngltranspostind,
                                         loanappewcpsngltransind,
                                         loanappeligevalind,
                                         loanappnewbuscd,
                                         loanappdisastercntrlnmb,
                                         loancollatind,
                                         recovind,
                                         loanappoutprgrmareaofoperind,
                                         loanappparntloannmb,
                                         loanappmicrolendrid,
                                         statcd,
                                         loanappcdcgntyamt,
                                         loanappcdcnetdbentramt,
                                         loanappcdcfundfeeamt,
                                         loanappcdcseprtprcsfeeind,
                                         loanappcdcprcsfeeamt,
                                         loanappcdcclscostamt,
                                         loanappcdcothclscostamt
                                         loanappcdcundrwtrfeeamt,
                                         loanappcontribpct,
                                         loanappcontribamt,
                                         loandisastrappnmb,
                                         loandisastrappfeecharged,
                                         loandisastrappdcsn,
                                         loanassocdisastrappnmb,
                                         loanassocdisastrloannmb,
                                         loannmb,
                                         loanappfunddt,
                                         undrwritngby,
                                         cohortcd,
                                         fundsfisclyr,
                                         loantypind,
                                         loanappintdtlcd,
                                         loanpymninstlmntfreqcd,
                                         fininstrmnttypind,
                                         undrwritngcrdriskcertind,
                                         loanamtlimexmptind,
                                         loanpymntschdlfreq,
                                         loangntymatstind,
                                         loangntynotedt,
                                         cdcservfeepct,
                                         loanappcdcgrossdbentramt,
                                         loanappbaltoborramt,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanAppExtraServFeeAMT,
                                         LoanAppExtraServFeeInd,
                                         RqstSeqNmb,
                                         LoanAppMonthPayroll,
                                         PrtyGrpInd
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;    
        WHEN 2
        THEN
            /*Check The Existance Of Row*/

            BEGIN
                OPEN p_selcur FOR SELECT 1
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 11
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappeligevalind,
                                         ROUND (TO_NUMBER (loanapprqstamt), 2)     loanapprqstnum,
                                         prgmcd,
                                         prcsmthdcd
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 12
        THEN
            /* Select all the Incomplete Applications for a given PrtSeqNmb */

            BEGIN
                OPEN p_selcur FOR SELECT a.loanappnmb, a.loanappnmb, b.borrseqnmb
                                    FROM loanapptbl a, loanborrtbl b
                                   WHERE a.loanappnmb = b.loanappnmb;
            END;
        WHEN 14
        THEN
            /* Select all the Incomplete Applications */

            BEGIN
                /*  b.BusSeqNmb ,
                              a.LoanAppAppcntFirstNm,
                              a.LoanAppAppcntLastNm*/
                OPEN p_selcur FOR
                    SELECT a.loanappnmb, b.borrseqnmb
                      FROM loanapptbl a, loanborrtbl b
                     WHERE     a.loanappnmb = b.loanappnmb
                           AND RPAD (111, 10, ' ') >= RPAD (TO_CHAR (p_datefrom, 'yyyy/mm/dd'), 10, ' ')
                           AND RPAD (111, 10, ' ') <= RPAD (TO_CHAR (p_dateto, 'yyyy/mm/dd'), 10, ' ');
            END;
        WHEN 15
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappnm
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 16
        THEN
            BEGIN
                OPEN p_selcur FOR
                    SELECT a.loanappnmb,
                           a.loanappnm,
                           ROUND (TO_NUMBER (a.loanapprqstamt), 2)     loanapprqstnum,
                           loanappprojctynm,
                           loanappprojstcd,
                           a.statcd,
                           d.stattxt,
                           e.loannmb,
                           a.loanappfunddt,
                           a.undrwritngby,
                           a.cohortcd,
                           a.fundsfisclyr,
                           a.loantypind,
                           a.loanappdisastercntrlnmb
                      FROM loanapptbl        a,
                           loanappprojtbl    b,
                           loan.loangntytbl  e,
                           sbaref.statcdtbl  d
                     WHERE     a.loanappnmb = p_loanappnmb
                           AND a.loanappnmb = e.loanappnmb(+)
                           AND a.loanappnmb = b.loanappnmb(+)
                           AND a.statcd = d.statcd;
            END;
        WHEN 17
        THEN
            BEGIN
                OPEN p_selcur FOR
                    SELECT a.prcsmthdcd,
                           a.statcd,
                           a.loanappprcsofccd,
                           b.loanrvwruserid     loanapprvwr1userid,
                           c.loanrvwruserid     loanapprvwr2userid,
                           d.loanrvwruserid     loanapprvwr3userid
                      FROM loanapptbl  a
                           LEFT OUTER JOIN loanrvwrtbl b ON (a.loanappnmb = b.loanappnmb AND b.loanrvwrtyp = '1')
                           LEFT OUTER JOIN loanrvwrtbl c ON (a.loanappnmb = c.loanappnmb AND c.loanrvwrtyp = '2')
                           LEFT OUTER JOIN loanrvwrtbl d ON (a.loanappnmb = d.loanappnmb AND d.loanrvwrtyp = '3')
                     WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 38
        THEN
            BEGIN
                SELECT MAX (lqdcrseqnmb)
                  INTO v_maxlqdcrseqnmb
                  FROM loanapp.lqdcrwsctbl a
                 WHERE a.loanappnmb = p_loanappnmb;

                OPEN p_selcur FOR
                    /* 38 = Leet for Extract XML */
                    SELECT a.loanappnmb
                               applicationnumber,
                           p.addtnllocacqlmtind,
                           r.armmartypind,
                           TO_CHAR (r.armmarceilrt, 'FM990.000')
                               armmarceilrt,
                           TO_CHAR (r.armmarfloorrt, 'FM990.000')
                               armmarfloorrt,
                           p.bulksalelawcomplyind,
                           TO_CHAR (p.compslmtamt, 'FM999999999999990.00')
                               compslmtamt,
                           p.compslmtind,
                           e.loanecondevobjctcd
                               AS econdevobjctcd,
                           r.escrowacctrqrdind,
                           TO_CHAR (p.fixassetacqlmtamt, 'FM999999999999990.00')
                               fixassetacqlmtamt,
                           p.fixassetacqlmtind,
                           p.frnchsfeedefind,
                           p.frnchsfeedefmonnmb,
                           p.frnchslendrsameoppind,
                           p.frnchsrecrdaccsind,
                           p.frnchstermnotcind,
                           i.imrttypcd,
                           r.latechgaftdaynmb,
                           TO_CHAR (r.latechgfeepct, 'FM990.000')
                               latechgfeepct,
                           r.latechgind,
                           b.loanagntinvlvdind,                                                 -- RGG added on 04/01/16
                           a.loanamtlimexmptind,
                           i.loanintadjprdcd
                               loanappadjprdcd,
                           a.loanappappvdt,
                           TO_CHAR (i.loanintbasert, 'FM990.00000')
                               loanappbasert,
                           TO_CHAR (a.loanappcdcclscostamt, 'FM999999999999990.00')
                               loanappcdcclscostamt,
                           TO_CHAR (a.loanappcdcothclscostamt, 'FM999999999999990.00')
                               loanappcdcothclscostamt,
                           TO_CHAR (a.loanappcdcfundfeeamt, 'FM999999999999990.00')
                               loanappcdcfundfeeamt,
                           TO_CHAR (a.loanappcdcgntyamt, 'FM999999999999990.00')
                               loanappcdcgntyamt,
                           p.loanappcdcjobrat,
                           TO_CHAR (a.loanappcdcnetdbentramt, 'FM999999999999990.00')
                               loanappcdcnetdbentramt,
                           TO_CHAR (a.loanappcdcprcsfeeamt, 'FM999999999999990.00')
                               loanappcdcprcsfeeamt,
                           a.loanappcdcseprtprcsfeeind,
                           TO_CHAR (a.loanappcdcundrwtrfeeamt, 'FM999999999999990.00')
                               loanappcdcundrwtrfeeamt,
                           ocadataout (b.loanappcntctemail)
                               loanappcntctemail,
                           b.loanappcntctfax,
                           ocadataout (b.loanappcntctfirstnm)
                               loanappcntctfirstnm,
                           b.loanappcntctinitialnm,
                           ocadataout (b.loanappcntctlastnm)
                               loanappcntctlastnm,
                           ocadataout (b.loanappcntctphn)
                               loanappcntctphn,
                           b.loanappcntctsfxnm,
                           b.loanappcntcttitltxt,
                           --   b.loanapplsplastnm,
                           --   b.loanapplspfirstnm,
                           --     b.loanapplspinitialnm,
                           b.loanapplspsfxnm,
                           --   b.loanapplsptitltxt,
                           --   b.loanapplspphn,
                           b.loanapplspfax,
                           --   b.loanapplspemail,
                           TO_CHAR (a.loanappcontribamt, 'FM999999999999990.00')
                               loanappcontribamt,
                           a.loanappcontribpct,
                           p.loanappcurrempqty,
                           a.loanappdisastercntrlnmb,
                           a.loanappeligevalind,
                           a.loanappepcind,
                           a.loanappetranvennm,                                                 --SS added on 06/09/2016
                           a.loanappewcpsngltransind,
                           a.loanappewcpsngltranspostind,
                           i.loanintfixvarind
                               loanappfixvarind,
                           p.loanappfrnchscd,
                           p.loanappfrnchsind,
                           p.loanappfrnchsnm,
                           a.loanappfullamortpymtind,
                           TO_CHAR (i.loanintrt, 'FM990.00000')
                               loanappinitlintpct,
                           TO_CHAR (i.loanintsprdovrpct, 'FM990.000')
                               loanappinitlsprdovrprimepct,
                           a.loanappinjctnind,
                           a.loanappintdtlcd,
                           p.loanappjobcreatqty,
                           p.loanappjobrqmtmetind,
                           p.loanappjobrtnd,
                           a.loanapplifinsurrqmtind,
                           a.loanappmointqty,
                           a.LoanAppMonthPayroll, -- JP added 04/02/2020
                           a.loanappnetearnind,
                           TO_CHAR (p.loanappnetexprtamt, 'FM999999999999990.00')
                               loanappnetexprtamt,
                           a.loanappnewbuscd,
                           RTRIM (a.loanappnm)
                               loanappnm,
                           NULL
                               loanappothadjprdtxt,
                           a.loanappoutprgrmareaofoperind,
                           b.loanapppckgsourcctynm,
                           b.loanapppckgsourcnm,
                           b.loanapppckgsourcstcd,
                           b.loanapppckgsourcstr1nm,
                           b.loanapppckgsourcstr2nm,
                           b.loanapppckgsourczipcd,
                           b.loanapppckgsourczip4cd,
                           p.loanappprojcntycd,                                                  -- NK added on 04/26/16
                           p.loanappprojctynm,
                           p.loanappprojstcd,
                           p.loanappprojstr1nm,
                           p.loanappprojstr2nm,
                           p.loanappprojzipcd,
                           p.loanappprojzip4cd,
                           b.loanappprtappnmb,
                           b.loanappprtloannmb,
                           TO_CHAR (a.loanapppymtamt, 'FM999999999999990.00')
                               loanapppymtamt,
                           a.loanapprecvdt,
                           a.loanapprevlind,
                           a.loanapprcnsdrtnind,
                           TO_CHAR (a.loanapprqstamt, 'FM999999999999990.00')
                               loanapprqstamt,
                           a.loanapprqstmatmoqty,
                           p.loanappruralurbanind,
                           a.loanappsbagntypct,
                           a.loanappservofccd,
                           a.loanassocdisastrloannmb,
                           a.loancollatind,
                           a.loandisastrappdcsn,
                           a.loandisastrappfeecharged,
                           a.loandisastrappnmb,
                           b.loanpckgsourctypcd,                                                 -- NK added on 04/26/16
                           a.loanpymninstlmntfreqcd,
                           --a.LoanPymntSchdlFreq, -- BS added on 07/13/17
                           --REGEXP_REPLACE (a.loanpymntschdlfreq, ','), -- NK 10/04/2017 --CAFSOPER
                           a.loanpymntschdlfreq,                                                        -- JP 10/26/2017
                           a.loanpymtrepayinstltypcd,
                           a.loantypind,
                           w.lqdcrtotscr,
                           p.naicscd,
                           r.netearnclauseind,
                           TO_CHAR (r.netearnpymntoveramt, 'FM999999999999990.00')
                               netearnpymntoveramt,
                           TO_CHAR (r.netearnpymntpct, 'FM990.000')
                               netearnpymntpct,
                           a.prcsmthdcd,
                           a.prgmcd,                                                              --NK added on 04/26/16
                           r.pymntbegnmonmb,
                           r.pymntdaynmb,
                           r.pymntintonlybegnmonmb,
                           r.pymntintonlydaynmb,
                           r.pymntintonlyfreqcd,
                           a.statcd,
                           r.stintrtreductnind,
                           r.stintrtreductnprgmnm,
                           a.undrwritngby,
                           a.loangntymatstind,                                                   --SS added on 12/2/2016
                           (SELECT vndrtypind
                              FROM sbaref.vndrswcdtbl
                             WHERE vndrswnm = a.loanappetranvennm)
                               AS vndrtypind,                                              --BR added on 08/11/2016 #862
                           a.loangntynotedt,                                                            --NK--02/27/2017
                           e.loanecondevobjctcd,
                           a.loanappcdcgrossdbentramt,
                           a.loanappbaltoborramt,
                           --b.loanappprtnm                                              AS lenderlspname,
                           prt.prtlglnm
                               AS lenderlspname,                                                        -- JP 11/15/2017
                           b.loanapplsphqlocid
                               AS lenderlsplocid,
                           ocadataout (LoanPrtCntctCellPhn)
                               AS LoanPrtCntctCellPhn,
                           ocadataout (b.LoanPrtAltCntctFirstNm)
                               AS loanapplspfirstnm,
                           ocadataout (b.LoanPrtAltCntctLastNm)
                               AS loanapplsplastnm,
                           b.LoanPrtAltCntctInitialNm
                               AS loanapplspinitialnm,
                           b.LoanPrtAltCntctTitlTxt
                               AS loanapplsptitltxt,
                           ocadataout (b.LoanPrtAltCntctPrimPhn)
                               AS loanapplspphn,
                           ocadataout (b.LoanPrtAltCntctCellPhn)
                               AS LoanPrtAltCntctCellPhn,
                           ocadataout (b.LoanPrtAltCntctemail)
                               AS loanapplspemail,
                           b.LendrAltCntctTypCd,
                           p.LoanBusEstDt,
                           ocadataout (b.LoanPrtAltCntctFirstNm)
                               AS LoanPrtAltCntctFirstNm,
                           ocadataout (b.LoanPrtAltCntctLastNm)
                               AS LoanPrtAltCntctLastNm,
                           b.LoanPrtAltCntctInitialNm,
                           b.LoanPrtAltCntctTitlTxt,
                           ocadataout (b.LoanPrtAltCntctPrimPhn)
                               AS LoanPrtAltCntctPrimPhn,
                           ocadataout (b.LoanPrtAltCntctemail)
                               AS LoanPrtAltCntctemail,
                           a.LoanAppRevlMoIntQty,
                           a.LoanAppAmortMoIntQty,
                           a.LoanAppExtraServFeeAmt,                                                  -- SS --10/25/2018
                           a.LoanAppExtraServFeeInd,                                                  -- SS --10/25/2018
                           a.RqstSeqNmb,
                           p.LOANAPPPROJLMIND,                                                        -- SL -- 01/04/2021 SOD-317
                           p.LOANAPPPROJSIZECD,                                                       -- SL -- 01/05/2021 SOD-318
                           p.LOANAPPPROJDELQIND,                                                      -- SL -- 01/07/2021 SOD-360/361
                           p.EIDLLOANNMB,                                                             -- SL -- 01/07/2021 SOD-360/361
                           p.LOANAPPPROJGRORCPTAMT1,                                                  -- SL -- 01/07/2021 SOD-360/361
                           p.LOANAPPPROJGRORCPTAMT2,                                                  -- SL -- 01/07/2021 SOD-360/361
                           p.PPPLOANNMB,                                                              -- SL -- 01/07/2021 SOD-360/361
                           LoanAppMonthPayroll,
                           a.LOANAPPUSRESIND,                                                          -- SL -- 01/07/2021 SOD-360/361
                           a.LOANAPPSCHEDCIND,
                           a.LOANAPPSCHEDCYR,
                           a.LOANAPPGROSSINCOMEAMT 
                      FROM loanapp.loanapptbl  a
                           LEFT OUTER JOIN loanapp.loanappprttbl b ON (b.loanappnmb = a.loanappnmb)          -- b = bank
                           LEFT OUTER JOIN loanapp.loanappprojtbl p ON (p.loanappnmb = a.loanappnmb)         -- p = proj
                           LEFT OUTER JOIN loanapp.loanpymnttbl r ON (r.loanappnmb = a.loanappnmb) -- r = repayment #1118
                           LEFT OUTER JOIN loanapp.lqdcrwsctbl w
                               ON ((w.loanappnmb = a.loanappnmb)                                   -- w = weighted score
                                                                 AND (w.lqdcrseqnmb = v_maxlqdcrseqnmb))
                           LEFT OUTER JOIN loanapp.loanintdtltbl i
                               ON ((i.loanappnmb = a.loanappnmb)                                         -- i = interest
                                                                 AND (i.loanintdtlseqnmb = 1))
                           LEFT OUTER JOIN loanapp.loanecondevobjctchldtbl e
                               ON ((e.loanappnmb = a.loanappnmb)                                   -- e = econdevobjctcd
                                                                 AND (e.loanecondevobjctseqnmb = 1))
                           LEFT OUTER JOIN partner.locsrchtbl loc ON (loc.locid = b.loanapplsphqlocid)  -- JP 11/15/2017
                           LEFT OUTER JOIN partner.prtsrchtbl prt ON (prt.prtid = loc.prtid)            -- JP 11/15/2017
                     WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 18
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT a.cdcservfeepct,
                                         a.DspstnStatCd,
                                         dspstn.DspstnStatDescTxt,
                                         prePrcs.STARRECVDT,
                                         prePrcs.STARUPLDDT,
                                         a.PrtyGrpInd
                                    FROM loanapp.loanapptbl a
                                    LEFT JOIN SBAREF.DSPSTNSTATCDTBL dspstn ON dspstn.DspstnStatCd = a.DspstnStatCd
                                    LEFT JOIN loanApp.CARESPrePrcsTbl prePrcs ON a.loanappnmb = prePrcs.loanappnmb
                                   WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 19
        THEN
            OPEN p_selcur FOR SELECT Statcd, LastUpdtDt
                                FROM loanapp.loanapptbl
                               WHERE loanappnmb = p_loanappnmb;
        WHEN 20
        --check if a request id exists under the same location id
        THEN
            OPEN p_selcur FOR SELECT RqstSEQNMB
                                FROM loanapp.Loanapptbl a, loanapp.loanappprttbl b
                               WHERE a.loanappnmb = b.loanappnmb AND a.RqstSEQNMB = p_RqstSeqNmb AND b.LocId = p_LocId;
        WHEN 21
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT a.loanappsourctypcd, loanappsourctypdesctxt
                                    FROM loanapp.LoanappTbl a, loanapp.loanappsourctyptbl b
                                   WHERE loanappnmb = p_loanappnmb AND a.loanappsourctypcd = b.loanappsourctypcd;
            END;
    END CASE;

    p_retval := NVL (p_retval, 0);
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            --   ROLLBACK TO loanappsel;
            RAISE;
        END;
END loanappseltsp;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\CALCDEBENTURECSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.CALCDEBENTURECSP(
p_LoanAppFundDt IN DATE := SYSDATE,
p_LoanAppFirstDisbDt IN DATE := SYSDATE,
p_LoanAppRqstMatMoQty IN NUMBER := 0,
p_LoanAppCDCNetDbentrAmt IN NUMBER := 0,
p_LoanAppCDCClsCostAmt IN NUMBER := 0,
p_LOANAPPCDCOTHCLSCOSTAMT IN NUMBER :=0,
p_LoanAppCDCSeprtPrcsFeeInd IN CHAR := NULL,
p_LoanAppCDCPrcsFeeAmt IN OUT NUMBER,
p_LoanAppCDCGntyAmt OUT NUMBER,
p_LoanAppCDCFundFeeAmt OUT NUMBER,
p_LoanAppCDCUndrwtrFeeAmt OUT NUMBER,
p_LoanAppCDCGrossDbentrAmt OUT NUMBER,
p_LoanAppCDCSubTotAmt OUT NUMBER,
p_LoanAppCDCAdminCostAmt OUT NUMBER,
p_LoanAppBalToBorrAmt OUT NUMBER)
AS
v_CDCGntyFeeMulti NUMBER;
v_CDCFundFeeMulti NUMBER;
v_CDCPrcsFeeMulti NUMBER;
v_CDCGrossDbentrMulti NUMBER;
v_CDCUndrwtrFeeMulti NUMBER;
/* 
This procedure calculates the Gross Debenture Amount
If the user does not enter the processing fee, then it is calculated based on net debentrue amount.
Note the procedure will not reject the processing fee entered by user which is more than 1.5% * net debenture amount. But the processing fee should be rejected by validation procedures.
*/

BEGIN
      SELECT TO_NUMBER(IMPrmtrValTxt)
      INTO v_CDCGntyFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCGntyFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);

      SELECT TO_NUMBER (IMPrmtrValTxt)
      INTO v_CDCFundFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCFundFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);

      SELECT TO_NUMBER (IMPrmtrValTxt)
      INTO v_CDCPrcsFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCPrcsFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
	
    p_LoanAppCDCGntyAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCGntyFeeMulti, 2);
    p_LoanAppCDCFundFeeAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCFundFeeMulti, 2);
    
    IF p_LoanAppCDCPrcsFeeAmt IS NULL THEN
    	p_LoanAppCDCPrcsFeeAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCPrcsFeeMulti, 2);
    END IF;
    
    IF p_LoanAppRqstMatMoQty = 300 THEN
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee25YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee25YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    ELSIF p_LoanAppRqstMatMoQty = 240 THEN
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee20YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee20YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    ELSE
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee10YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee10YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    END IF;
    
    p_LoanAppCDCGrossDbentrAmt := CEIL((p_LoanAppCDCNetDbentrAmt + p_LoanAppCDCGntyAmt + p_LoanAppCDCFundFeeAmt + p_LoanAppCDCPrcsFeeAmt + p_LoanAppCDCClsCostAmt + p_LOANAPPCDCOTHCLSCOSTAMT) / v_CDCGrossDbentrMulti / 1000) * 1000;
    p_LoanAppCDCUndrwtrFeeAmt := ROUND(p_LoanAppCDCGrossDbentrAmt * v_CDCUndrwtrFeeMulti, 2);
    p_LoanAppCDCSubTotAmt := p_LoanAppCDCGntyAmt + p_LoanAppCDCFundFeeAmt + /*CASE WHEN p_LoanAppCDCSeprtPrcsFeeInd = 'Y' THEN 0 ELSE p_LoanAppCDCPrcsFeeAmt END*/ p_LoanAppCDCPrcsFeeAmt + p_LoanAppCDCClsCostAmt + p_LOANAPPCDCOTHCLSCOSTAMT;
    p_LoanAppCDCAdminCostAmt := p_LoanAppCDCSubTotAmt + p_LoanAppCDCUndrwtrFeeAmt;
    p_LoanAppBalToBorrAmt := p_LoanAppCDCGrossDbentrAmt - p_LoanAppCDCNetDbentrAmt - p_LoanAppCDCAdminCostAmt;
    
EXCEPTION
  WHEN NO_DATA_FOUND THEN
          
    p_LoanAppCDCGrossDbentrAmt := 0;
    p_LoanAppCDCUndrwtrFeeAmt  := 0;
    p_LoanAppCDCSubTotAmt      := 0;
    p_LoanAppCDCAdminCostAmt   := 0;
    p_LoanAppBalToBorrAmt      := 0;
    
  WHEN OTHERS THEN
          
      dbms_output.put_line(sqlerrm);
      raise;
    
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPINFOINSCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPINFOINSCSP (p_Identifier                         NUMBER := 0,
                                                       p_RetVal                         OUT NUMBER,
                                                       p_LoanAppNmb                     OUT NUMBER,
                                                       p_StatCd                             CHAR := NULL,
                                                       p_PrgmCd                             CHAR := NULL,
                                                       p_PrcsMthdCd                         CHAR := NULL,
                                                       p_LoanAppNm                          VARCHAR2 := NULL,
                                                       p_LoanAppRecvDt                      DATE := NULL,
                                                       p_LoanAppRqstAmt                     NUMBER := NULL,
                                                       p_LoanAppSBARcmndAmt                 NUMBER := NULL,
                                                       p_LoanAppSBAGntyPct                  NUMBER := NULL,
                                                       p_LoanAppRqstMatMoQty                NUMBER := NULL,
                                                       p_LoanAppEPCInd                      CHAR := NULL,
                                                       p_LoanAppPymtAmt                     NUMBER := NULL,
                                                       p_LoanAppFullAmortPymtInd            CHAR := NULL,
                                                       p_LoanAppMoIntQty                    NUMBER := NULL,
                                                       p_LoanAppLifInsurRqmtInd             CHAR := NULL,
                                                       p_LoanAppRcnsdrtnInd                 CHAR := NULL,
                                                       p_LoanAppInjctnInd                   CHAR := NULL,
                                                       p_LoanAppEWCPSnglTransPostInd        CHAR := NULL,
                                                       p_LoanAppEWCPSnglTransInd            CHAR := NULL,
                                                       p_LoanAppEligEvalInd                 CHAR := NULL,
                                                       p_LoanAppNewBusCd                    CHAR := NULL,
                                                       p_NAICSCd                            NUMBER := NULL,
                                                       p_LoanAppFrnchsInd                   CHAR := NULL,
                                                       p_LoanAppFrnchsCd                    CHAR := NULL,
                                                       p_LoanAppFrnchsNm                    VARCHAR2 := NULL,
                                                       p_LoanAppDisasterCntrlNmb            VARCHAR2 := NULL,
                                                       p_LoanAppProjStr1Nm                  VARCHAR2 := NULL,
                                                       p_LoanAppProjStr2Nm                  VARCHAR2 := NULL,
                                                       p_LoanAppProjCtyNm                   VARCHAR2 := NULL,
                                                       p_LoanAppProjCntyCd                  CHAR := NULL,
                                                       p_LoanAppProjStCd                    CHAR := NULL,
                                                       p_LoanAppProjZipCd                   CHAR := NULL,
                                                       p_LoanAppProjZip4Cd                  CHAR := NULL,
                                                       p_LoanAppRuralUrbanInd               CHAR := NULL,
                                                       p_LoanAppNetExprtAmt                 NUMBER := NULL,
                                                       p_LoanAppCurrEmpQty                  NUMBER := NULL,
                                                       p_LoanAppJobCreatQty                 NUMBER := NULL,
                                                       p_LoanAppJobRtnd                     NUMBER := NULL,
                                                       p_LoanAppJobRqmtMetInd               CHAR := NULL,
                                                       p_LoanAppCDCJobRat                   NUMBER := NULL,
                                                       p_LoanAppCDCGntyAmt                  NUMBER := NULL,
                                                       p_LoanAppCDCNetDbentrAmt             NUMBER := NULL,
                                                       p_LoanAppCDCFundFeeAmt               NUMBER := NULL,
                                                       p_LoanAppCDCSeprtPrcsFeeInd          CHAR := NULL,
                                                       p_LoanAppCDCPrcsFeeAmt               NUMBER := NULL,
                                                       p_LoanAppCDCClsCostAmt               NUMBER := NULL,
                                                       p_LoanAppCDCOthClsCostAmt            NUMBER := NULL,
                                                       p_LoanAppCDCUndrwtrFeeAmt            NUMBER := NULL,
                                                       p_LoanAppContribPct                  NUMBER := NULL,
                                                       p_LoanAppContribAmt                  NUMBER := NULL,
                                                       p_LocId                              NUMBER := NULL,
                                                       p_LoanAppFIRSNmb                     CHAR := NULL,
                                                       p_PrtId                              NUMBER := NULL,
                                                       p_LoanAppPrtAppNmb                   VARCHAR2 := NULL,
                                                       p_LoanAppPrtLoanNmb                  VARCHAR2 := NULL,
                                                       p_LoanAppPrtNm                       VARCHAR2 := NULL,
                                                       p_LoanAppPrtStr1                     VARCHAR2 := NULL,
                                                       p_LoanAppPrtStr2                     VARCHAR2 := NULL,
                                                       p_LoanAppPrtCtyNm                    VARCHAR2 := NULL,
                                                       p_LoanAppPrtStCd                     CHAR := NULL,
                                                       p_LoanAppPrtZipCd                    CHAR := NULL,
                                                       p_LoanAppPrtZip4Cd                   CHAR := NULL,
                                                       p_LoanAppCntctLastNm                 VARCHAR2 := NULL,
                                                       p_LoanAppCntctFirstNm                VARCHAR2 := NULL,
                                                       p_LoanAppCntctInitialNm              CHAR := NULL,
                                                       p_LoanAppCntctSfxNm                  CHAR := NULL,
                                                       p_LoanAppCntctTitlTxt                VARCHAR2 := NULL,
                                                       p_LoanAppCntctPhn                    VARCHAR2 := NULL,
                                                       p_LoanAppCntctFax                    VARCHAR2 := NULL,
                                                       p_LoanAppCntctemail                  VARCHAR2 := NULL,
                                                       p_LoanPckgSourcTypCd                 NUMBER := NULL,
                                                       p_LoanAppPckgSourcNm                 VARCHAR2 := NULL,
                                                       p_LoanAppPckgSourcStr1Nm             VARCHAR2 := NULL,
                                                       p_LoanAppPckgSourcStr2Nm             VARCHAR2 := NULL,
                                                       p_LoanAppPckgSourcCtyNm              VARCHAR2 := NULL,
                                                       p_LoanAppPckgSourcStCd               CHAR := NULL,
                                                       p_LoanAppPckgSourcZipCd              CHAR := NULL,
                                                       p_LoanAppPckgSourcZip4Cd             CHAR := NULL,
                                                       p_LoanAppCreatUserId                 CHAR := NULL,
                                                       p_LoanCollatInd                      CHAR := NULL,
                                                       p_LoanAppSourcTypCd                  NUMBER := NULL,
                                                       p_RecovInd                           CHAR := NULL,
                                                       p_LoanAppOutPrgrmAreaOfOperInd       CHAR := NULL,
                                                       p_LoanAppParntLoanNmb                CHAR := NULL,
                                                       p_LoanAppMicroLendrId                CHAR := NULL,
                                                       p_LoanAppPrtTaxId                    CHAR := NULL,
                                                       p_LOANAPPNETEARNIND                  CHAR := NULL,
                                                       p_LOANAPPSERVOFCCD                   CHAR := NULL,
                                                       p_LoanAppRevlInd                     CHAR := NULL,
                                                       p_LOANDISASTRAPPNMB                  CHAR := NULL,
                                                       p_LOANNMB                            CHAR := NULL,
                                                       p_LOANAPPFUNDDT                      DATE := NULL,
                                                       p_UNDRWRITNGBY                       CHAR := NULL,
                                                       p_COHORTCD                           CHAR := NULL,
                                                       p_FUNDSFISCLYR                       NUMBER := NULL,
                                                       p_LOANTYPIND                         CHAR := NULL,
                                                       p_LoanAppETranSeqNmb                 NUMBER := NULL,
                                                       p_LOANAPPINTDTLCD                    VARCHAR2 := NULL,
                                                       p_LOANAPPLSPHQLOCID                  NUMBER := NULL,
                                                       p_LOANAGNTINVLVDIND                  VARCHAR := NULL,
                                                       p_LoanAmtLimExmptInd                 CHAR := NULL,
                                                       p_LOANAPPETRANVENNM                  VARCHAR2 := NULL,
                                                       p_LOANGNTYMATSTIND                   CHAR := NULL,
                                                       p_FRNCHSTERMNOTCIND                  CHAR := NULL,
                                                       p_FRNCHSRECRDACCSIND                 CHAR := NULL,
                                                       p_FRNCHSLENDRSAMEOPPIND              CHAR := NULL,
                                                       p_FRNCHSFEEDEFMONNMB                 NUMBER := NULL,
                                                       p_FRNCHSFEEDEFIND                    CHAR := NULL,
                                                       p_FIXASSETACQLMTIND                  CHAR := NULL,
                                                       p_FIXASSETACQLMTAMT                  NUMBER := NULL,
                                                       p_COMPSLMTIND                        CHAR := NULL,
                                                       p_COMPSLMTAMT                        NUMBER := NULL,
                                                       p_BULKSALELAWCOMPLYIND               CHAR := NULL,
                                                       p_ADDTNLLOCACQLMTIND                 CHAR := NULL,
                                                       --    p_LoanAppLspLastNm                  VARCHAR2 := NULL,
                                                       --   p_LoanAppLspFirstNm                 VARCHAR2 := NULL,
                                                       --    p_LoanAppLspInitialNm               CHAR := NULL,
                                                       p_LoanAppLspSfxNm                    CHAR := NULL,
                                                       --     p_LoanAppLspTitlTxt                 VARCHAR2 := NULL,
                                                       --     p_LoanAppLspPhn                     VARCHAR2 := NULL,
                                                       p_LoanAppLspFax                      VARCHAR2 := NULL,
                                                       --     p_LoanAppLspEMail                   VARCHAR2 := NULL,
                                                       p_LoanGntyNoteDt                     DATE := NULL,
                                                       p_LoanAppCDCGrossDbentrAmt           NUMBER := NULL,
                                                       p_LoanAppBalToBorrAmt                NUMBER := NULL,
                                                       p_LoanPymnInstlmntFreqCd             CHAR := NULL,
                                                       p_LoanPrtCntctCellPhn                VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctFirstNm             VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctLastNm              VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctInitialNm           CHAR := NULL,
                                                       p_LoanPrtAltCntctTitlTxt             VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctPrimPhn             VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctCellPhn             VARCHAR2 := NULL,
                                                       p_LoanPrtAltCntctemail               VARCHAR2 := NULL,
                                                       p_LendrAltCntctTypCd                 CHAR := NULL,
                                                       p_LOANBUSESTDT                       DATE := NULL,
                                                       p_LoanAppRevlMoIntQty                NUMBER := NULL,
                                                       p_LoanAppAmortMoIntQty               NUMBER := NULL,
                                                       p_LoanPymtRepayInstlTypCd            CHAR := NULL,
                                                       p_LoanAppProjGeoCd                   CHAR := NULL,
                                                       p_LoanAppHubZoneInd                  CHAR := NULL,
                                                       p_LoanAppProjLat                     CHAR := NULL,
                                                       p_LoanAppProjLong                    CHAR := NULL,
                                                       p_LoanAppExtraServFeeAMT             NUMBER := NULL,
                                                       p_LoanAppExtraServFeeInd             CHAR := NULL,
                                                       p_LoanAppCBRSInd                     CHAR := NULL,
                                                       p_LoanAppOppZoneInd                  CHAR := NULL,
                                                       p_ACHAcctNmb                         VARCHAR2 DEFAULT NULL,
                                                       p_ACHAcctTypCd                       CHAR DEFAULT NULL,
                                                       p_ACHRtngNmb                         VARCHAR2 DEFAULT NULL,
                                                       p_ACHTinNmb                          CHAR DEFAULT NULL,
                                                       p_LoanAppMonthPayroll                NUMBER DEFAULT NULL,
                                                       p_LOANAPPPROJLMIND                   CHAR := NULL,
                                                       p_LOANAPPPROJSIZECD                  CHAR := NULL,
                                                       p_LOANAPPPROJDELQIND                 CHAR := NULL,
                                                       p_EIDLLOANNMB                        CHAR := NULL,
                                                       p_LOANAPPPROJGRORCPTAMT1             NUMBER := NULL,
                                                       p_LOANAPPPROJGRORCPTAMT2             NUMBER := NULL,
                                                       p_LOANAPPUSRESIND                    CHAR := NULL,
                                                       p_LOANAPPSCHEDCIND                   CHAR := NULL,
                                                       p_LOANAPPSCHEDCYR                    NUMBER := NULL,
                                                       p_LOANAPPGROSSINCOMEAMT              NUMBER := NULL,
                                                       p_PPPLOANNMB                         CHAR := NULL) AS
    /*
     * created by:
     * Purpose:
     * Parameters:
     * Revision:
     * Modified by -- APK-- added input variable p_LoanAppPrtTaxId to the procedure.
     * RGG --01/03/2012-- modified to add new column LOANAPPNETEARN and LOANAPPSERVOFCCD
     * APK -- 01/04/2011 -- modified rename LOANAPPNETEARN to LOANAPPNETEARNIND
       APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the
       loanapptbl in development.
     * RGG -- Added a case statement at line 222.
     * SP -- 02/28/2013 -- modified to add new columns LOANNMB,LOANAPPFUNDDT,UNDRWRITNGBY,
    COHORTCD,FUNDSFISCLYR,LOANTYPIND
     * RSURAPA -- 10/07/2013 -- Removed references to all the strnmb and strsfxnm columns
     * RGG     -- 11/05/2013 -- Added new column LOANAPPINTDTLCD
     *SP       -- 11/27/2013 -- Removed references to columns :IMRtTypCd,LoanAppFixVarInd,
    LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppAdjPrdCd
    *RGG       -- 12/19/2013 -- Added code to take the status code from user input
    *  SP      -- 04/07/2014 -- Modified to remove NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd as these columns are now present in loanappprojtbl
    SB      -- 09/19/2014 -- Modified the procedure to add new column FININSTRMNTTYPIND
    RY-- 06/04/2016 --- Modified Procedure to add new column LOANAGNTINVLVDIND in loanapp.loanappprttbl insert call
     SS--05/05/16 modified to add new column LoanAmtLimExmptInd
     RGG: 05/16/2016: OPSMDEV - 961 Added LOANAPPETRANVENNM to save the vendor name at the application level
     SS: 12/2/2016 OPSMDEV 1245 ADDED  COLUMN LOANGNTYMATSTIND
     SB - 12/09/2016 FRNCHSTERMNOTCIND,FRNCHSRECRDACCSIND,FRNCHSLENDRSAMEOPPIND,FRNCHSFEEDEFMONNMB,FRNCHSFEEDEFIND,FIXASSETACQLMTIND,FIXASSETACQLMTAMT,COMPSLMTIND,COMPSLMTAMT,BULKSALELAWCOMPLYIND,ADDTNLLOCACQLMTIND added to insert loanapp.LoanAppProjTbl
    --NK-- 02/06/2017--OPSMDEV-1337-- Added LoanAppLspLastNm,LoanAppLspFirstNm, LoanAppLspInitialNm, LoanAppLspSfxNm, LoanAppLspTitlTxt, LoanAppLspPhn, LoanAppLspFax, LoanAppLspEMail
    --NK--03/28/2017-- OPSMDEV 1385-- Removed EconDevObjctCd
    --NK--04/20/2017--CAFSOPER-- Added p_LoanGntyNoteDt
    --  RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to Loanapptbl insertion call
    --  SB--08/30/2017--debenture calculations-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to Loanapptbl insertion call
    --  JP - 09/29/2017 -- CAFSOPER-1201 - Removed columns LoanAppOrigntnOfcCd and LoanAppPrcsOfcCd from insert to LOANAPP.LoanAppTbl. Also added call to LOANAPPUPDTSP procedure
    --SS--11/17/2017--OPSMDEV 1566--Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,LoanPrtAltCntctTitlTxt,LoanPrtAltCntctPrimPh,LoanPrtAltCntctCellPhn,
    --  LoanPrtAltCntctemail,LendrAltCntctTypCd and deleted LoanAppLspLastNm,LoanAppLspFirstNm,LoanAppLspInitialNm,LoanAppLspSfxNm,LoanAppLspTitlTxt,LoanAppLspPhn.
    --SS--11/22/2017--OPSMDEV 1569 Added LOANBUSESTDT
    --NK--01/16/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
    ----RY--04/20/2018--OPSMDEV--1783--Added LoanPymtRepayInstlTypCd
    --BR--09/26/2018--OPSMDEV 1885-- Added LoanAppProjGeoCd, LoanAppHubZoneInd, LoanAppProjLat, LoanAppProjLong
    --SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd to loanapptbl
    --SS--OPSMDEV2043 Added function for encryption
    --SS--02/18/2020 OPSMDEV 2409,2410 Added LoanAppCBRSInd,LoanAppOppZoneInd
    -- -- JP added 04/02/2020 p_ACHAcctNmb,p_ACHAcctTypCd,p_ACHRtngNmb,p_ACHTinNmb,p_LoanAppMonthPayroll
    --SL--01/06/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to insert into LoanAppProjTbl
    --SL--01/07/2021 SODSTORY-360/361.  Added:
           p_LOANAPPPROJDELQIND                 CHAR := NULL,
           p_EIDLLOANNMB                        CHAR := NULL,
           p_LOANAPPPROJGRORCPTAMT1             NUMBER := NULL,
           p_LOANAPPPROJGRORCPTAMT2             NUMBER := NULL,
           p_LOANAPPUSRESIND                    CHAR := NULL,
           p_PPPLOANNMB                         CHAR := NULL
 JP --1/14/2021 SODSTORY-390 Changed P_UNDRWRITNGBY to SBA for PPP/PPS
 SL --03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT
  -- JP OPSMDEV 2561 4/6/2021 added D for FinInstrmntTypInd               
*/
    v_RetVal           NUMBER;
    v_LoanAppRqstAmt   NUMBER;
BEGIN
    IncrmntSeqNmbCSP ('Application', p_LoanAppNmb);

    INSERT INTO LOANAPP.LoanAppTbl (LoanAppNmb,
                                    PrgmCd,
                                    PrcsMthdCd,
                                    LoanAppNm,
                                    LoanAppRecvDt,
                                    LoanAppEntryDt,
                                    LoanAppRqstAmt,
                                    LoanAppSBARcmndAmt,
                                    LoanAppSBAGntyPct,
                                    LoanAppRqstMatMoQty,
                                    LoanAppEPCInd,
                                    LoanAppPymtAmt,
                                    LoanAppFullAmortPymtInd,
                                    LoanAppMoIntQty,
                                    LoanAppLifInsurRqmtInd,
                                    LoanAppRcnsdrtnInd,
                                    LoanAppInjctnInd,
                                    LoanAppEWCPSnglTransInd,
                                    LoanAppEWCPSnglTransPostInd,
                                    LoanAppEligEvalInd,
                                    LoanAppNewBusCd,
                                    LoanAppDisasterCntrlNmb,
                                    LoanCollatInd,
                                    LoanAppSourcTypCd,
                                    StatCd,
                                    LoanAppStatUserId,
                                    LoanAppStatDt,
                                    --LoanAppOrigntnOfcCd,
                                    --LoanAppPrcsOfcCd,
                                    RecovInd,
                                    LoanAppCreatUserId,
                                    LoanAppCreatDt,
                                    LoanAppOutPrgrmAreaOfOperInd,
                                    LoanAppParntLoanNmb,
                                    LoanAppMicroLendrId,
                                    LoanAppCDCGntyAmt,
                                    LoanAppCDCNetDbentrAmt,
                                    LoanAppCDCFundFeeAmt,
                                    LoanAppCDCSeprtPrcsFeeInd,
                                    LoanAppCDCPrcsFeeAmt,
                                    LoanAppCDCClsCostAmt,
                                    LoanAppCDCOthClsCostAmt,
                                    LoanAppCDCUndrwtrFeeAmt,
                                    LoanAppContribPct,
                                    LoanAppContribAmt,
                                    LOANAPPNETEARNIND,
                                    LOANAPPSERVOFCCD,
                                    LoanAppRevlInd,
                                    LOANDISASTRAPPNMB,
                                    LOANNMB,
                                    LOANAPPFUNDDT,
                                    UNDRWRITNGBY,
                                    COHORTCD,
                                    FUNDSFISCLYR,
                                    LOANTYPIND,
                                    LoanAppETranSeqNmb,
                                    LoanAppIntDtlCd,
                                    FinInstrmntTypInd,
                                    LoanAmtLimExmptInd,
                                    LOANAPPETRANVENNM,
                                    LOANGNTYMATSTIND,
                                    LoanGntyNoteDt,
                                    LoanAppCDCGrossDbentrAmt,
                                    LoanAppBalToBorrAmt,
                                    LoanPymnInstlmntFreqCd,
                                    LoanAppRevlMoIntQty,
                                    LoanAppAmortMoIntQty,
                                    LoanPymtRepayInstlTypCd,
                                    LoanAppExtraServFeeAMT,
                                    LoanAppExtraServFeeInd,
                                    LoanAppMonthPayroll,
                                    LOANAPPUSRESIND,
                                    LOANAPPSCHEDCIND,
                                    LOANAPPSCHEDCYR,
                                    LOANAPPGROSSINCOMEAMT)
        SELECT p_LoanAppNmb,
               p_PrgmCd,
               p_PrcsMthdCd,
               p_LoanAppNm,
               p_LoanAppRecvDt,
               SYSDATE,
               p_LoanAppRqstAmt,
               p_LoanAppSBARcmndAmt,
               p_LoanAppSBAGntyPct,
               p_LoanAppRqstMatMoQty,
               p_LoanAppEPCInd,
               p_LoanAppPymtAmt,
               p_LoanAppFullAmortPymtInd,
               p_LoanAppMoIntQty,
               p_LoanAppLifInsurRqmtInd,                                                                 ---sms 10/18/00
               p_LoanAppRcnsdrtnInd,
               p_LoanAppInjctnInd,
               p_LoanAppEWCPSnglTransInd,
               p_LoanAppEWCPSnglTransPostInd,
               p_LoanAppEligEvalInd,
               p_LoanAppNewBusCd,
               p_LoanAppDisasterCntrlNmb,
               p_LoanCollatInd,
               p_LoanAppSourcTypCd,
               NVL (p_StatCd, 'IP'),
               NVL (p_LoanAppCreatUserId, USER),
               SYSDATE,
               /*CASE                                                                   -- 03/26/2012 RGG
                  WHEN p_PrcsMthdCd = 'DPB'
                  THEN
                     '9030'
                  WHEN p_PrcsMthdCd = 'DPH'
                  THEN
                     '9030'
                  WHEN p_PrcsMthdCd = 'DPO'
                  THEN
                     '9030'
                  ELSE
                     (SELECT c.OrignOfcCd
                        FROM sbaref.CntyTbl c
                       WHERE c.CntyCd = p_LoanAppProjCntyCd AND c.StCd = p_LoanAppProjStCd)
               END,                                                                   -- 03/26/2012 RGG
               (SELECT c.ServOfcCd
                  FROM sbaref.CntyTbl c
                 WHERE c.CntyCd = p_LoanAppProjCntyCd AND c.StCd = p_LoanAppProjStCd),*/
               p_RecovInd,
               NVL (p_LoanAppCreatUserId, USER),
               SYSDATE,
               p_LoanAppOutPrgrmAreaOfOperInd,
               p_LoanAppParntLoanNmb,
               p_LoanAppMicroLendrId,
               p_LoanAppCDCGntyAmt,
               p_LoanAppCDCNetDbentrAmt,
               p_LoanAppCDCFundFeeAmt,
               p_LoanAppCDCSeprtPrcsFeeInd,
               p_LoanAppCDCPrcsFeeAmt,
               p_LoanAppCDCClsCostAmt,
               p_LoanAppCDCOthClsCostAmt,
               p_LoanAppCDCUndrwtrFeeAmt,
               p_LoanAppContribPct,
               p_LoanAppContribAmt,
               p_LOANAPPNETEARNIND,
               p_LOANAPPSERVOFCCD,
               p_LoanAppRevlInd,
               p_LOANDISASTRAPPNMB,
               p_LOANNMB,
               p_LOANAPPFUNDDT,
               CASE
                   WHEN P_PrcsMthdCd IN ('PPP', 'PPS') THEN 'SBA'
                   ELSE P_UNDRWRITNGBY
               END,
               --P_UNDRWRITNGBY
               p_COHORTCD,
               p_FUNDSFISCLYR,
               p_LOANTYPIND,
               p_LoanAppETranSeqNmb,
               p_LoanAppIntDtlCd,
               CASE p_prgmcd
                   WHEN 'E' THEN 'E'
                   WHEN 'D' THEN 'G'
                   ELSE 'L'
               END,
               p_LoanAmtLimExmptInd,
               p_LOANAPPETRANVENNM,
               p_LOANGNTYMATSTIND,
               p_LoanGntyNoteDt,
               p_LoanAppCDCGrossDbentrAmt,
               p_LoanAppBalToBorrAmt,
               p_LoanPymnInstlmntFreqCd,
               p_LoanAppRevlMoIntQty,
               p_LoanAppAmortMoIntQty,
               p_LoanPymtRepayInstlTypCd,
               p_LoanAppExtraServFeeAMT,
               p_LoanAppExtraServFeeInd,
               p_LoanAppMonthPayroll,
               p_LOANAPPUSRESIND,
               p_LOANAPPSCHEDCIND,
               p_LOANAPPSCHEDCYR,
               p_LOANAPPGROSSINCOMEAMT
        FROM DUAL;



    p_RetVal := SQL%ROWCOUNT;

    /* Insert into the Loan Incomplete Application Partner Table */
    INSERT INTO LOANAPP.LoanAppPrtTbl (LoanAppNmb,
                                       LocId,
                                       LoanAppFIRSNmb,
                                       PrtId,
                                       LoanAppPrtAppNmb,
                                       LoanAppPrtLoanNmb,
                                       LoanAppPrtNm,
                                       LoanAppPrtStr1,
                                       LoanAppPrtStr2,
                                       LoanAppPrtCtyNm,
                                       LoanAppPrtStCd,
                                       LoanAppPrtZipCd,
                                       LoanAppPrtZip4Cd,
                                       LoanAppCntctLastNm,
                                       LoanAppCntctFirstNm,
                                       LoanAppCntctInitialNm,
                                       LoanAppCntctSfxNm,
                                       LoanAppCntctTitlTxt,
                                       LoanAppCntctPhn,
                                       LoanAppCntctFax,
                                       LoanAppCntctemail,
                                       LoanPckgSourcTypCd,
                                       LoanAppPckgSourcNm,
                                       LoanAppPckgSourcStr1Nm,
                                       LoanAppPckgSourcStr2Nm,
                                       LoanAppPckgSourcCtyNm,
                                       LoanAppPckgSourcStCd,
                                       LoanAppPckgSourcZipCd,
                                       LoanAppPckgSourcZip4Cd,
                                       LoanAppPrtTaxId,
                                       LOANAPPLSPHQLOCID,
                                       LoanAppPrtCreatUserId,
                                       LoanAppPrtCreatDt,
                                       LASTUPDTUSERID,
                                       LASTUPDTDT,
                                       LoanAgntInvlvdInd,
                                       --    LoanAppLspLastNm,
                                       --    LoanAppLspFirstNm,
                                       --    LoanAppLspInitialNm,
                                       LoanAppLspSfxNm,
                                       --    LoanAppLspTitlTxt,
                                       --    LoanAppLspPhn,
                                       LoanAppLspFax,
                                       LoanPrtCntctCellPhn,
                                       LoanPrtAltCntctFirstNm,
                                       LoanPrtAltCntctLastNm,
                                       LoanPrtAltCntctInitialNm,
                                       LoanPrtAltCntctTitlTxt,
                                       LoanPrtAltCntctPrimPhn,
                                       LoanPrtAltCntctCellPhn,
                                       LoanPrtAltCntctemail,
                                       LendrAltCntctTypCd,
                                       ACHAcctNmb,
                                       ACHAcctTypCd,
                                       ACHRtngNmb,
                                       ACHTinNmb)
        --     LoanAppLspEMail)
        SELECT p_LoanAppNmb,
               p_LocId,
               p_LoanAppFIRSNmb,
               p_PrtId,
               p_LoanAppPrtAppNmb,
               p_LoanAppPrtLoanNmb,
               p_LoanAppPrtNm,
               p_LoanAppPrtStr1,
               p_LoanAppPrtStr2,
               p_LoanAppPrtCtyNm,
               p_LoanAppPrtStCd,
               p_LoanAppPrtZipCd,
               p_LoanAppPrtZip4Cd,
               ocadatain (p_LoanAppCntctLastNm),
               ocadatain (p_LoanAppCntctFirstNm),
               p_LoanAppCntctInitialNm,
               p_LoanAppCntctSfxNm,
               p_LoanAppCntctTitlTxt,
               ocadatain (p_LoanAppCntctPhn),
               p_LoanAppCntctFax,
               ocadatain (p_LoanAppCntctemail),
               p_LoanPckgSourcTypCd,
               p_LoanAppPckgSourcNm,
               p_LoanAppPckgSourcStr1Nm,
               p_LoanAppPckgSourcStr2Nm,
               p_LoanAppPckgSourcCtyNm,
               p_LoanAppPckgSourcStCd,
               p_LoanAppPckgSourcZipCd,
               p_LoanAppPckgSourcZip4Cd,
               p_LoanAppPrtTaxId,
               p_LOANAPPLSPHQLOCID,
               NVL (p_LoanAppCreatUserId, USER),
               SYSDATE,
               NVL (p_LoanAppCreatUserId, USER),
               SYSDATE,
               p_LOANAGNTINVLVDIND,
               --   p_LoanAppLspLastNm,
               --   p_LoanAppLspFirstNm,
               --   p_LoanAppLspInitialNm,
               p_LoanAppLspSfxNm,
               --    p_LoanAppLspTitlTxt,
               --    p_LoanAppLspPhn,
               p_LoanAppLspFax,
               --    p_LoanAppLspEMail
               ocadatain (p_LoanPrtCntctCellPhn),
               ocadatain (p_LoanPrtAltCntctFirstNm),
               ocadatain (p_LoanPrtAltCntctLastNm),
               p_LoanPrtAltCntctInitialNm,
               p_LoanPrtAltCntctTitlTxt,
               ocadatain (p_LoanPrtAltCntctPrimPhn),
               ocadatain (p_LoanPrtAltCntctCellPhn),
               ocadatain (p_LoanPrtAltCntctemail),
               p_LendrAltCntctTypCd,
               p_ACHAcctNmb,
               p_ACHAcctTypCd,
               p_ACHRtngNmb,
               p_ACHTinNmb
        FROM DUAL;

    /* Insert into the Loan Incomplete Application Project Table */
    IF p_PrcsMthdCd <> 'SMP' THEN
        BEGIN
            INSERT INTO LOANAPP.LoanAppProjTbl (LoanAppNmb,
                                                LoanAppProjStr1Nm,
                                                LoanAppProjStr2Nm,
                                                LoanAppProjCtyNm,
                                                LoanAppProjCntyCd,
                                                LoanAppProjStCd,
                                                LoanAppProjZipCd,
                                                LoanAppProjZip4Cd,
                                                LoanAppRuralUrbanInd,
                                                LoanAppNetExprtAmt,
                                                LoanAppCurrEmpQty,
                                                LoanAppJobCreatQty,
                                                LoanAppJobRtnd,
                                                LoanAppJobRqmtMetInd,
                                                LoanAppCDCJobRat,
                                                LoanAppProjCreatUserId,
                                                LoanAppProjCreatDt,
                                                LASTUPDTUSERID,
                                                LASTUPDTDT,
                                                NAICSYrNmb,
                                                NAICSCd,
                                                LoanAppFrnchsCd,
                                                LoanAppFrnchsNm,
                                                LoanAppFrnchsInd,
                                                FRNCHSTERMNOTCIND,
                                                FRNCHSRECRDACCSIND,
                                                FRNCHSLENDRSAMEOPPIND,
                                                FRNCHSFEEDEFMONNMB,
                                                FRNCHSFEEDEFIND,
                                                FIXASSETACQLMTIND,
                                                FIXASSETACQLMTAMT,
                                                COMPSLMTIND,
                                                COMPSLMTAMT,
                                                BULKSALELAWCOMPLYIND,
                                                ADDTNLLOCACQLMTIND,
                                                LOANBUSESTDT,
                                                LoanAppProjGeoCd,
                                                LoanAppHubZoneInd,
                                                LoanAppProjLat,
                                                LoanAppProjLong,
                                                LoanAppCBRSInd,
                                                LoanAppOppZoneInd,
                                                LOANAPPPROJLMIND,
                                                LOANAPPPROJSIZECD,
                                                LOANAPPPROJDELQIND,
                                                EIDLLOANNMB,
                                                LOANAPPPROJGRORCPTAMT1,
                                                LOANAPPPROJGRORCPTAMT2,
                                                PPPLOANNMB)
                SELECT p_LoanAppNmb,
                       p_LoanAppProjStr1Nm,
                       p_LoanAppProjStr2Nm,
                       p_LoanAppProjCtyNm,
                       p_LoanAppProjCntyCd,
                       p_LoanAppProjStCd,
                       p_LoanAppProjZipCd,
                       p_LoanAppProjZip4Cd,
                       p_LoanAppRuralUrbanInd,
                       p_LoanAppNetExprtAmt,
                       p_LoanAppCurrEmpQty,
                       p_LoanAppJobCreatQty,
                       p_LoanAppJobRtnd,
                       p_LoanAppJobRqmtMetInd,
                       p_LoanAppCDCJobRat,
                       NVL (p_LoanAppCreatUserId, USER),
                       SYSDATE,
                       NVL (p_LoanAppCreatUserId, USER),
                       SYSDATE,
                       (SELECT MAX (NAICSYrNmb)
                        FROM sbaref.NAICSTbl
                        WHERE NAICSCd = p_NAICSCd),
                       p_NAICSCd,
                       p_LoanAppFrnchsCd,
                       p_LoanAppFrnchsNm,
                       p_LoanAppFrnchsInd,
                       p_FRNCHSTERMNOTCIND,
                       p_FRNCHSRECRDACCSIND,
                       p_FRNCHSLENDRSAMEOPPIND,
                       p_FRNCHSFEEDEFMONNMB,
                       p_FRNCHSFEEDEFIND,
                       p_FIXASSETACQLMTIND,
                       p_FIXASSETACQLMTAMT,
                       p_COMPSLMTIND,
                       p_COMPSLMTAMT,
                       p_BULKSALELAWCOMPLYIND,
                       p_ADDTNLLOCACQLMTIND,
                       p_LOANBUSESTDT,
                       p_LoanAppProjGeoCd,
                       p_LoanAppHubZoneInd,
                       p_LoanAppProjLat,
                       p_LoanAppProjLong,
                       p_LoanAppCBRSInd,
                       p_LoanAppOppZoneInd,
                       p_LOANAPPPROJLMIND,
                       p_LOANAPPPROJSIZECD,
                       p_LOANAPPPROJDELQIND,
                       p_EIDLLOANNMB,
                       p_LOANAPPPROJGRORCPTAMT1,
                       p_LOANAPPPROJGRORCPTAMT2,
                       p_PPPLOANNMB
                FROM DUAL;
        END;
    END IF;

    -- following sets originating and processing office codes.
    LOANAPP.LOANAPPUPDTSP (
        p_Identifier => 16,
        p_RetVal => v_RetVal,                                                                            -- out variable
        p_LoanAppNmb => p_LoanAppNmb,
        p_PrcsMthdCd => p_PrcsMthdCd,
        p_LoanAppRqstAmt => v_LoanAppRqstAmt                                                             -- out variable
                                            );


    p_RetVal := NVL (p_RetVal, 0);
    p_LoanAppNmb := NVL (p_LoanAppNmb, 0);
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDSUMTSP (
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
	p_PROCDTYPCD 				CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANSUBPROCDSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOANAPP.LOANSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD and LOANAPPNMB=p_LoanAppNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOANAPP.LOANPROCDTBL set LOANPROCDAMT = nvl(quan,0) where PROCDTYPCD = p_PROCDTYPCD and LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDSUMTSP;
            RAISE;
        END;
END LOANSUBPROCDSUMTSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPRINTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPRINTSELCSP (
    p_LoanAppNmb   IN     NUMBER DEFAULT NULL,
    p_SelCur1         OUT SYS_REFCURSOR,
    p_SelCur2         OUT SYS_REFCURSOR,
    p_SelCur3         OUT SYS_REFCURSOR,
    p_SelCur4         OUT SYS_REFCURSOR,
    p_SelCur5         OUT SYS_REFCURSOR,
    p_SelCur6         OUT SYS_REFCURSOR,
    p_SelCur7         OUT SYS_REFCURSOR,
    p_SelCur8         OUT SYS_REFCURSOR,
    p_SelCur9         OUT SYS_REFCURSOR,
    p_SelCur10        OUT SYS_REFCURSOR,
    p_SelCur11        OUT SYS_REFCURSOR,
    p_SelCur12        OUT SYS_REFCURSOR,
    p_SelCur13        OUT SYS_REFCURSOR,
    p_SelCur14        OUT SYS_REFCURSOR,
    p_SelCur15        OUT SYS_REFCURSOR,
    p_SelCur16        OUT SYS_REFCURSOR,
    p_SelCur17        OUT SYS_REFCURSOR,
    p_SelCur18        OUT SYS_REFCURSOR,
    p_SelCur19        OUT SYS_REFCURSOR,
    p_SelCur20        OUT SYS_REFCURSOR,
    p_SelCur21        OUT SYS_REFCURSOR,
    p_SelCur22        OUT SYS_REFCURSOR,
    p_SelCur23        OUT SYS_REFCURSOR,
    p_SelCur24        OUT SYS_REFCURSOR,
    p_SelCur25        OUT SYS_REFCURSOR,
    p_SelCur26        OUT SYS_REFCURSOR,
    p_SelCur27        OUT SYS_REFCURSOR,
    p_SelCur28        OUT SYS_REFCURSOR,
    p_SelCur29        OUT SYS_REFCURSOR,
    p_SelCur30        OUT SYS_REFCURSOR,
    p_SelCur31        OUT SYS_REFCURSOR,
    p_SelCur32        OUT SYS_REFCURSOR)
AS
    /*  Date     :
    Programmer   :
    Remarks      :
    ***********************************************************************************
    Revised By      Date        Comments
    ----------     --------    ----------------------------------------------------------
    APK         03/15/2011  Modified to add new result sets 22-25
    APK         03/21/2011  Modified to add proper exception handlers.
    SP          12/7/2011   Modified to remove LOANCOLLATLQDVALAMT.
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP          06/10/2013  Modified to add loanaffilind to result sets 13,15,16
    RSURAPA -- 10/08/2013 -- Removed references to all the strnmb and strsfxnm columns
    RGG     --  11/05/2013  -- Modified to add new column LOANAPPINTDTLCD
    SP -- 11/27/2013 -- Removed references to columns :IMRtTypCd,LoanAppFixVarInd,
    LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd from p_SelCur4  and added cursor p_SelCur27 to return these values from LoanIntDtlTbl
    RGG -- 02/12/2014 -- Added LACGNTYCONDTYPCD to some result sets to fix an issue with Printing
    SP  -- 04/07/2014 -- Modified to get NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd from loanappprojtbl instead of loanapptbl
    RY --- 04/04/2016 --- Added LOANAGNTINVLVDIND in p_SelCur2 as it is added in loanappprttbl
    NK --08/05/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd and LoanCntlIntTyp
    RGG -- 09/12/2016 -- OPSMDEV 1103 Added new columns for making Use of Proceeds support Loan Auth LOANPROCDREFDESCTXT, LOANPROCDPURAGRMTDT,
                         LOANPROCDPURINTANGASSETAMT, LOANPROCDPURINTANGASSETDESC, LOANPROCDPURSTKHLDRNM

    ********************************************************************************
    */
    /*SP:9/13/2012: Modified as LoanPrtCmntSeqNmb is removed
    SS -- 09/14/2016 added new columns to p_SelCur7  for OPSMDEV 1111Print functionality in ETRAN to account for new columns added in collateral
    NK -- 09/28/2016-- OPSMDEV-1163 added ,  LoanDisblInsurRqrdInd   ,  LoanInsurDisblDescTxt  ,  LoanLifeInsurRqrdInd    , LoanPrinNoNCAEvdncInd   ,LoanComptitrNm
                           in   p_SelCur16.
    NK--09/28/2016--OPSMDEV 1179-- Added LOANINJCTNFundTermYrNmb p_SelCur5.
    NK-- 10/03/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in   p_SelCur15, p_SelCur16.
    SS---10/14/2016 OPSMDEV1122 Added LoanPymntSchdlFreq,LoanPymtRepayInstlTypCd,LOANPYMNINSTLMNTFREQCD to cursor 4
    SS---10/18/2016 OPSMDEV-1172 added more columns for Project to cursor 10)
    NK--10/24/2016--OPSMDEV-1174 Added  ,a.LiabInsurRqrdInd,a.ProdLiabInsurRqrdInd,a.LiqLiabInsurRqrdInd,a.MalPrctsInsurRqrdInd ,a.OthInsurRqrdind ,a.OthInsurDescTxt,a.PymtLessCrdtInd ,a.InsurDisblDescTxt
    ,a.RecrdStmtTypCd ,a.RecrdStmtYrEndDayNmb in p_SelCur13
    NK--10/25/2016--OPSMDEV 1171-- added NCAIncldInd,StkPurCorpText in p_SelCur6
    NK--11/08/2016--OPSMDEV 1229-- Added p_SelCur30 for StbyAgrmt
    SS--11/16/2016 --OPSMDEV 1170-- Added  CollatAppOrdDt , LoanCollatValDt,COLLATSTATDESCTXT to Cursor7
    SS--11/16/2016 ---OPSMDEV1172-- Added NAICSDESCTXT to cursor 10
    SS--12/09/2016 --OPSMDEV1289 --Added LOANGNTYMATSTDESCTXT to cursor 4
    NK--01/11/2016 -- OPSMDEV 1287 -- Added VetCertInd in p_SelCur16
    NK-- 01/26/2017--OPSMDEV-1337-- Added LoanAppLspLastNm,LoanAppLspFirstNm, LoanAppLspInitialNm, LoanAppLspSfxNm, LoanAppLspTitlTxt, LoanAppLspPhn, LoanAppLspFax, LoanAppLspEMail in p_SelCur2
    NK--03/28/2017-- OPSMDEV 1385-- added LoanEconDevObjctCd in p_SelCur10
    Nk--05/12/2017-- OPSMDEV 1432--Added LOANINTADJPRDEFFDT  in p_Selcur27
    RY-- 06/07/2017--OPSMDEV 1407 --Added loanpartlendrcntrycd,loanpartlendrpostcd,loanpartlendrstnm to p_Selcur20

    --   BR -- 06/14/2017 -- OPSMDEV - 1359 -- Added p_SelCur31
    RY -- 07/24/2017-- CAFSOPER-805 --Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to p_SelCur4.
    NK--10/11/2017-- OPSMDEV 1548 added DISASTRSTDECLCD, DISASTRDESCTXT,DISASTRSTRTDT to p_selCur10.
    BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
    NK --11/14/2017--OPSMDEV 1551 Added SOP Principal questions to p_SelCur13
    NK--11/14/2017--OPSMDEV 1558 Added SOP Borrower Questions to p_SelCur15
    NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to p_SelCur14
    BR--11/14/2017--opsmdev 1577 Added LoanIntDtlGuarInd p_selCur27
    SS--11/15/2017 OPSMDEV 1573 Added LoanPrtCntctCellPhn, LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn, LoanPrtAltCntctCellPhn,
   LoanPrtAltCntctemail,LendrAltCntctTypCd
   SS--11/15/2017 OPSMDEV 1566 Added LOANBUSESTDT to p_selcur 10 and BusPrimCntctNm,BusPrimCntctEmail to p_selcur 13
   NK-- 11/16/2017 OPSMDEV 1591 Added BusFedAgncyLoanInd, PerFedAgncyLoanInd to p_selcur 13
     --NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to p_selcur 13
   NK--12/14/2017--OPSMDEV 1605 added LOANINTDTLDESCTXT  and LoanGntyNoteDt to p_selcur 27
      NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to p_SelCur4
 RY-- 04/20/2018--OPSMDEV--1786--Added WorkrsCompInsRqrdInd to p_SelCur13
 SS--08/03/2018--OPSMDEV--1882--Added p_SelCur 32
 SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to p_selcur 4
 SS--11/15/2018--Added function for encryption to p_selcur 32
 SS--11/16/2018--Added function to p_selcur 2 for encryption OPSMDEV 2043
 SS--11/20/2018 OPSMDEV 2043 Added function for encryption to p_selcur 18,20
 SS--11/26/2018 OPSMDEV 2043 Added function for encryption
 RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
 SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to p_selcur 4
 SS--04/3/2020 Added LoanAppMonthPayroll  to cursor 4 CARESACT - 63
 SS--04/26/2020 Added ACHAcctNmb,ACHRtngNmb,ACHAcctTypCd,ACHTinNmb to p_selcur 2
 SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to p_selcur 10
 JS--02/10/2021 Loan Authorization Modernization. Added LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to Guarantor - p_SelCur15
 */
    v_ProcdTypCd   CHAR (1);
BEGIN
    BEGIN
        SELECT ProcdTypCd
          INTO v_ProcdTypCd
          FROM LoanAppTbl a, sbaref.PrgrmValidTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.PrgmCd = b.PrgrmCd
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND (   (    b.SpcPurpsLoanCd IS NULL
                        AND NOT EXISTS
                                (SELECT 1
                                   FROM LoanSpcPurpsTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb))
                    OR EXISTS
                           (SELECT 1
                              FROM LoanSpcPurpsTbl z
                             WHERE     LoanAppNmb = p_LoanAppNmb
                                   AND z.SpcPurpsLoanCd = b.SpcPurpsLoanCd))
               AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_ProcdTypCd := NULL;
    END;

    OPEN p_SelCur1 FOR
        SELECT b.SpcPurpsLoanCd, b.SpcPurpsLoanDesc
          FROM LoanSpcPurpsTbl a, sbaref.SpcPurpsLoanTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.SpcPurpsLoanCd = b.SpcPurpsLoanCd;

    OPEN p_SelCur2 FOR
        SELECT LocId,
               LoanAppFIRSNmb,
               PrtId,
               LoanAppPrtAppNmb,
               LoanAppPrtLoanNmb,
               LoanAppPrtNm,
               LoanAppPrtStr1,
               LoanAppPrtStr2,
               LoanAppPrtCtyNm,
               LoanAppPrtStCd,
               LoanAppPrtZipCd,
               LoanAppPrtZip4Cd,
               ocadataout (LoanAppCntctLastNm)
                   AS LoanAppCntctLastNm,
               ocadataout (LoanAppCntctFirstNm)
                   AS LoanAppCntctFirstNm,
               LoanAppCntctInitialNm,
               LoanAppCntctSfxNm,
               LoanAppCntctTitlTxt,
               ocadataout (LoanAppCntctPhn)
                   AS LoanAppCntctPhn,
               LoanAppCntctFax,
               ocadataout (LoanAppCntctemail)
                   AS LoanAppCntctemail,
               b.LoanPckgSourcTypCd,
               b.LoanPckgSourcTypDescTxt,
               LoanAppPckgSourcNm,
               LoanAppPckgSourcStr1Nm,
               LoanAppPckgSourcStr2Nm,
               LoanAppPckgSourcCtyNm,
               LoanAppPckgSourcStCd,
               LoanAppPckgSourcZipCd,
               LoanAppPckgSourcZip4Cd,
               LoanAgntInvlvdInd,
               --  LoanAppLspFirstNm,
               --  LoanAppLspInitialNm,
               LoanAppLspSfxNm,
               --  LoanAppLspTitlTxt,
               --   LoanAppLspPhn,
               LoanAppLspFax,
               --  LoanAppLspEMail,
               ocadataout (LoanPrtCntctCellPhn)
                   AS LoanPrtCntctCellPhn,
               ocadataout (LoanPrtAltCntctFirstNm)
                   AS LoanPrtAltCntctFirstNm,
               ocadataout (LoanPrtAltCntctLastNm)
                   AS LoanPrtAltCntctLastNm,
               LoanPrtAltCntctInitialNm,
               LoanPrtAltCntctTitlTxt,
               ocadataout (LoanPrtAltCntctPrimPhn)
                   AS LoanPrtAltCntctPrimPhn,
               ocadataout (LoanPrtAltCntctCellPhn)
                   AS LoanPrtAltCntctCellPhn,
               ocadataout (LoanPrtAltCntctemail)
                   AS LoanPrtAltCntctemail,
               LendrAltCntctTypCd,
               ACHAcctNmb,
               ACHRtngNmb,
               ACHAcctTypCd,
               ACHTinNmb
          FROM LoanAppPrtTbl a, sbaref.LoanPckgSourcTypTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanPckgSourcTypCd = b.LoanPckgSourcTypCd(+);

    OPEN p_SelCur3 FOR
          SELECT a.LoanCrdtUnavRsnSeqNmb,
                 a.LoanCrdtUnavRsnCd,
                 b.LoanCrdtUnavRsnDescTxt,
                 a.LoanCrdtUnavRsnTxt
            FROM LoanCrdtUnavRsnTbl a, sbaref.LoanCrdtUnavRsnCdTbl b
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCrdtUnavRsnCd = b.LoanCrdtUnavRsnCd
        ORDER BY a.LoanCrdtUnavRsnSeqNmb;

    OPEN p_SelCur4 FOR
        SELECT LoanAppNm,
               a.PrgmCd,
               c.PrgrmDesc,
               a.PrcsMthdCd,
               b.PrcsMthdDesc,
               ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                   LoanAppRqstNum,
               LoanAppSBAGntyPct,
               LoanAppRqstMatMoQty                       --  ,LoanAppFixVarInd
                                  --  ,a.IMRtTypCd
                                  -- ,d.IMRtTypDescTxt
                                  -- ,LoanAppBaseRt
                                  -- ,LoanAppInitlIntPct
                                  -- ,LoanAppInitlSprdOvrPrimePct
                                  -- ,a.LoanAppAdjPrdCd
                                  -- ,e.CalndrPrdDesc
                                  ,
               LoanAppEPCInd,
               ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                   LoanAppPymtNum,
               LoanAppFullAmortPymtInd,
               LoanAppMoIntQty,
               LoanAppLifInsurRqmtInd,
               LoanAppRcnsdrtnInd,
               LoanAppInjctnInd,
               LoanAppEligEvalInd,
               LoanAppNewBusCd,
               BusAgeDesc
                   LoanAppNewBusTxt,
               NVL ((SELECT ROUND (TO_NUMBER (SUM (LoanPartLendrAmt)), 2)
                       FROM LOANAPP.LoanPartLendrTbl
                      WHERE LoanAppNmb = p_LoanAppNmb),
                    0)
                   ThirdPartyLoanAmt,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanCollatInd,
               a.LoanAppCDCGntyAmt,
               a.LoanAppCDCNetDbentrAmt,
               a.LoanAppCDCFundFeeAmt,
               a.LoanAppCDCSeprtPrcsFeeInd,
               a.LoanAppCDCPrcsFeeAmt,
               a.LoanAppCDCClsCostAmt,
               a.LoanAppCDCOthClsCostAmt,
               a.LoanAppCDCUndrwtrFeeAmt,
               a.LoanAppContribPct,
               a.LoanAppContribAmt,
               LoanAppDisasterCntrlNmb,
               LOANDISASTRAPPFEECHARGED,
               LOANDISASTRAPPDCSN,
               LOANASSOCDISASTRAPPNMB,
               LOANASSOCDISASTRLOANNMB,
               LOANDISASTRAPPNMB,
               LOANAPPINTDTLCD,
               LoanPymtRepayInstlTypCd,
               LOANPYMNINSTLMNTFREQCD,
               LoanPymntSchdlFreq,
               (SELECT e.LOAN_GNTY_MAT_ST_DESC_TXT
                  FROM SBAREF.LOANMATSTINDCDTBL e
                 WHERE a.LOANGNTYMATSTIND = e.LOAN_GNTY_MAT_ST_IND)
                   AS LOANGNTYMATSTDESCTXT,
               CASE
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'M' THEN 'Month'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'Quarter'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'S' THEN 'Semi annual'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'A' THEN 'Annual'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'O' THEN 'Other'
                   ELSE 'Not selected yet'
               END
                   AS PYMTINTONLYFREQTXT,
               LoanAppCDCGrossDbentrAmt,
               LoanAppBalToBorrAmt,
               a.LoanAppRevlMoIntQty,
               a.LoanAppAmortMoIntQty,
               a.LoanAppExtraServFeeAMT,
               a.LoanAppExtraServFeeInd,
               a.RqstSeqNmb,
               LoanAppMonthPayroll
          FROM LoanAppTbl          a,
               sbaref.PrcsMthdTbl  b,
               sbaref.PrgrmTbl     c                   -- ,sbaref.IMRtTypTbl d
                                    -- ,sbaref.CalndrPrdTbl e
                                    ,
               sbaref.BusAgeCdTbl  f
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND a.PrgmCd = c.PrgrmCd(+)
               -- AND a.IMRtTypCd       = d.IMRtTypCd(+)
               --  AND a.PrcsMthdCd      = d.PrcsMthdCd(+)
               -- AND a.LoanAppAdjPrdCd = e.CalndrPrdCd(+)
               AND a.LoanAppNewBusCd = f.BusAgeCd(+);

    OPEN p_SelCur5 FOR
        SELECT d.InjctnTypCd,
               r.InjctnTypTxt,
               CASE WHEN d.InjctnTypCd = 'O' THEN '~' ELSE r.InjctnTypCd END
                   DisplayOrder,
               ROUND (TO_NUMBER (d.LoanInjctnAmt), 2)
                   LoanInjctnNum,
               d.LoanInjctnOthDescTxt,
               d.LOANINJCTNFundTermYrNmb       -- NK--OPSMDEV 1179--09/28/2016
          FROM LoanInjctnTbl d, sbaref.InjctnTypCdTbl r
         WHERE d.LoanAppNmb = p_LoanAppNmb AND d.InjctnTypCd = r.InjctnTypCd
        UNION ALL
        SELECT a.InjctnTypCd,
               a.InjctnTypTxt,
               CASE WHEN a.InjctnTypCd = 'O' THEN '~' ELSE a.InjctnTypCd END
                   DisplayOrder,
               NULL,
               NULL,
               NULL
          FROM sbaref.InjctnTypCdTbl a
         WHERE     a.InjctnTypStrtDt <= SYSDATE
               AND (   a.InjctnTypEndDt IS NULL
                    OR a.InjctnTypEndDt + 1 > SYSDATE)
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanInjctnTbl z
                         WHERE     z.LoanAppNmb = p_LoanAppNmb
                               AND z.InjctnTypCd = a.InjctnTypCd)
        ORDER BY DisplayOrder;

    OPEN p_SelCur6 FOR
        SELECT a.ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               ROUND (TO_NUMBER (a.LoanProcdAmt), 2)     LoanProcdNum,
               a.LoanProcdOthTypTxt,
               a.LoanProcdRefDescTxt,
               a.LoanProcdPurAgrmtDt,
               a.LoanProcdPurIntangAssetAmt,
               a.LoanProcdPurIntangAssetDesc,
               a.LoanProcdPurStkHldrNm,
               a.NCAIncldInd,
               a.StkPurCorpText
          FROM LoanProcdTbl a, sbaref.LoanProcdTypTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ProcdTypCd = b.ProcdTypCd
               AND a.LoanProcdTypCd = b.LoanProcdTypCd
        UNION ALL
        SELECT ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL
          FROM sbaref.LoanProcdTypTbl b
         WHERE     ProcdTypCd = v_ProcdTypCd
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanProcdTbl a
                         WHERE     a.LoanAppNmb = p_LoanAppNmb
                               AND a.ProcdTypCd = b.ProcdTypCd
                               AND a.LoanProcdTypCd = b.LoanProcdTypCd)
               AND LoanProcdTypCdStrtDt <= SYSDATE
               AND (   LoanProcdTypCdEndDt IS NULL
                    OR LoanProcdTypCdEndDt + 1 > SYSDATE)
        ORDER BY LoanProcdTypCd;

    OPEN p_SelCur7 FOR
          SELECT LoanCollatSeqNmb,
                 a.LoanCollatTypCd,
                 b.LoanCollatTypDescTxt,
                 a.LoanCollatDesc,
                 ROUND (TO_NUMBER (LoanCollatMrktValAmt), 2)
                     LoanCollatMrktValNum,
                 LoanCollatOwnrRecrd,
                 LoanCollatSBALienPos,
                 a.LoanCollatValSourcCd,
                 c.LoanCollatValSourcDescTxt,
                 LoanCollatValDt,
                 SecurShrPariPassuInd                                     --SS
                                     ,
                 SecurShrPariPassuNonSBAInd,
                 SecurPariPassuLendrNm,
                 SecurPariPassuAmt,
                 SecurLienLimAmt,
                 SecurInstrmntTypCd,
                 SecurWaterRightInd,
                 SecurRentAsgnInd,
                 SecurSellerNm,
                 SecurPurNm,
                 SecurOwedToSellerAmt,
                 SecurCDCDeedInEscrowInd,
                 SecurSellerIntDtlInd,
                 SecurSellerIntDtlTxt,
                 SecurTitlSubjPriorLienInd,
                 SecurPriorLienTxt,
                 SecurPriorLienLimAmt,
                 SecurSubjPriorAsgnInd,
                 SecurPriorAsgnTxt,
                 SecurPriorAsgnLimAmt,
                 SecurALTATltlInsurInd,
                 SecurLeaseTermOverLoanYrNmb,
                 LandlordIntProtWaiverInd,
                 SecurDt,
                 SecurLessorTermNotcDaysNmb,
                 SecurDescTxt,
                 SecurPropAcqWthLoanInd,
                 SecurPropTypTxt,
                 SecurOthPropTxt,
                 SecurLienOnLqorLicnsInd,
                 SecurMadeYrNmb,
                 SecurLocTxt,
                 SecurOwnerNm,
                 SecurMakeNm,
                 SecurAmt,
                 SecurNoteSecurInd,
                 SecurStkShrNmb,
                 SecurLienHldrVrfyInd,
                 SecurTitlVrfyTypCd,
                 SecurTitlVrfyOthTxt,
                 FloodInsurRqrdInd,
                 REHazardInsurRqrdInd,
                 PerHazardInsurRqrdInd,
                 FullMarInsurRqrdInd,
                 EnvInvstgtSBAAppInd,
                 LeasePrmTypCd,
                 ApprTypCd,
                 LoanCollatSubTypCd,
                 CollatAppOrdDt,
                 LoanCollatValDt,
                 (SELECT d.LAC_COND_TYP_DESC_TXT
                    FROM LOAN.LAC_COND_TYPCD_TBL d
                   WHERE a.LoanCollatSubTypCd = d.LAC_COND_TYP_CD)
                     AS LAC_COND_TYP_DESC_TXT,
                 (SELECT e.LAC_ALTA_PLCY_DESC_TXT
                    FROM LOAN.LAC_ALTA_PLCY_CD_TBL e
                   WHERE a.SecurTitlVrfyTypCd = e.LAC_ALTA_PLCY_CD)
                     AS LAC_ALTA_PLCY_DESC_TXT,
                 (SELECT z.leastypdesctxt
                    FROM sbaref.leastypcdtbl z
                   WHERE a.leaseprmtypcd = z.leastypcd)
                     AS LEASDESCTXT,
                 (SELECT g.Propapprsltypdesctxt
                    FROM sbaref.Propapprsltypcdtbl g
                   WHERE a.ApprTypcd = g.Propapprsltypcd)
                     AS APPRSLDESCTXT,
                 (SELECT f.collatstatdesctxt
                    FROM sbaref.collatstatcdtbl f
                   WHERE a.collatstatcd = f.collatstatcd)
                     AS COLLATSTATDESCTXT
            FROM LoanCollatTbl               a,
                 sbaref.LoanCollatTypCdTbl   b,
                 sbaref.LoanCollatValSourcTbl c
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCollatTypCd = b.LoanCollatTypCd
                 AND a.LoanCollatValSourcCd = c.LoanCollatValSourcCd(+)
        ORDER BY LoanCollatSeqNmb;

    OPEN p_SelCur8 FOR
        SELECT LoanCollatSeqNmb,
               LoanCollatLienSeqNmb,
               LoanCollatLienHldrNm,
               LoanCollatLienPos,
               ROUND (TO_NUMBER (LoanCollatLienBalAmt), 2)
                   LoanCollatLienBalNum
          FROM LoanCollatLienTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur9 FOR SELECT LoanPrtCmntTxt
                         FROM LoanPrtCmntTbl
                        WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur10 FOR
        SELECT a.LoanAppNmb,
               NAICSYRNMB,
               NAICSCD,
               LoanAppFrnchsInd,
               LOANAPPFRNCHSCD,
               LOANAPPFRNCHSNM,
               LoanAppProjStr1Nm,
               LoanAppProjStr2Nm,
               LoanAppProjCtyNm,
               LoanAppProjCntyCd,
               LoanAppProjStCd,
               LoanAppProjZipCd,
               LoanAppProjZip4Cd,
               LoanAppRuralUrbanInd,
               ROUND (TO_NUMBER (LoanAppNetExprtAmt), 2)
                   LoanAppNetExprtAmt,
               LoanAppCurrEmpQty,
               LoanAppJobCreatQty,
               LoanAppJobRtnd,
               LoanAppJobRqmtMetInd,
               LoanAppCDCJobRat,
               LOANAPPPROJLMIND,
               LOANAPPPROJSIZECD,
               e.LoanEconDevObjctCd,
               b.EconDevObjctTxt,
               ADDTNLLOCACQLMTIND                                         --SS
                                 ,
               FIXASSETACQLMTIND,
               FIXASSETACQLMTAMT,
               COMPSLMTIND,
               COMPSLMTAMT,
               BULKSALELAWCOMPLYIND,
               FRNCHSRECRDACCSIND,
               FRNCHSFEEDEFMONNMB,
               FRNCHSFEEDEFIND,
               FRNCHSTERMNOTCIND,
               FRNCHSLENDRSAMEOPPIND,
               LOANBUSESTDT,
               (SELECT z.LqdCrTotScr
                  FROM loanapp.LqdCrWSCTbl z
                 WHERE     z.LoanAppNmb = p_LoanAppNmb
                       AND z.LqdCrSeqNmb =
                           (SELECT MAX (LqdCrSeqNmb)
                              FROM loanapp.LqdCrWSCTbl y
                             WHERE y.LoanAppNmb = p_LoanAppNmb)
                       AND creatdt = (SELECT MAX (creatdt)
                                        FROM loanapp.LqdCrWSCTbl x
                                       WHERE z.LoanAppNmb = x.LoanAppNmb))
                   AS LqdCrTotScr,
               (SELECT g.DESCTXT
                  FROM Sbaref.NAICSTBL g
                 WHERE a.NAICSCD = g.NAICSCD AND a.NAICSYRNMB = g.NAICSYRNMB)
                   AS NAICSDESCTXT,
               (SELECT b.DISASTRSTDECLCD
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRSTDECLCD,
               (SELECT b.DISASTRDESCTXT
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRDESCTXT,
               (SELECT b.DISASTRSTRTDT
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRSTRTDT
          FROM LoanAppProjTbl                   a,
               sbaref.EconDevObjctCdTbl         b,
               LOANAPP.LOANECONDEVOBJCTCHLDTBL  e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND e.LoanEconDevObjctCd = b.EconDevObjctCd(+);

    OPEN p_SelCur11 FOR
        SELECT LoanBSDt,
               ROUND (TO_NUMBER (LoanBSCashEqvlntAmt), 2)
                   LoanBSCashEqvlntAmt,
               ROUND (TO_NUMBER (LoanBSNetTrdRecvAmt), 2)
                   LoanBSNetTrdRecvAmt,
               ROUND (TO_NUMBER (LoanBSTotInvtryAmt), 2)
                   LoanBSTotInvtryAmt,
               ROUND (TO_NUMBER (LoanBSOthCurAssetAmt), 2)
                   LoanBSOthCurAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotCurAssetAmt), 2)
                   LoanBSTotCurAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotFixAssetAmt), 2)
                   LoanBSTotFixAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotOthAssetAmt), 2)
                   LoanBSTotOthAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotAssetAmt), 2)
                   LoanBSTotAssetAmt,
               ROUND (TO_NUMBER (LoanBSAcctsPayblAmt), 2)
                   LoanBSAcctsPayblAmt,
               ROUND (TO_NUMBER (LoanBSCurLTDAmt), 2)
                   LoanBSCurLTDAmt,
               ROUND (TO_NUMBER (LoanBSOthCurLiabAmt), 2)
                   LoanBSOthCurLiabAmt,
               ROUND (TO_NUMBER (LoanBSTotCurLiabAmt), 2)
                   LoanBSTotCurLiabAmt,
               ROUND (TO_NUMBER (LoanBSLTDAmt), 2)
                   LoanBSLTDAmt,
               ROUND (TO_NUMBER (LoanBSOthLTLiabAmt), 2)
                   LoanBSOthLTLiabAmt,
               ROUND (TO_NUMBER (LoanBSStbyDbt), 2)
                   LoanBSStbyDbt,
               ROUND (TO_NUMBER (LoanBSTotLiab), 2)
                   LoanBSTotLiab,
               ROUND (TO_NUMBER (LoanBSBusNetWrth), 2)
                   LoanBSBusNetWrth,
               ROUND (TO_NUMBER (LoanBSTngblNetWrth), 2)
                   LoanBSTngblNetWrth,
               LoanBSActlPrfrmaInd,
               a.LoanFinanclStmtSourcCd,
               b.LoanFinanclStmtSourcDescTxt
          FROM LoanBSTbl a, sbaref.LoanFinanclStmtSourcTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd;

    OPEN p_SelCur12 FOR
          SELECT LoanISSeqNmb,
                 ROUND (TO_NUMBER (LoanISNetSalesRevnuAmt), 2)
                     LoanISNetSalesRevnuAmt,
                 ROUND (TO_NUMBER (LoanISCostSalesAmt), 2)
                     LoanISCostSalesAmt,
                 ROUND (TO_NUMBER (LoanISGrsProftAmt), 2)
                     LoanISGrsProftAmt,
                 ROUND (TO_NUMBER (LoanISOperProftAmt), 2)
                     LoanISOperProftAmt,
                 ROUND (TO_NUMBER (LoanISAnnIntExpnAmt), 2)
                     LoanISAnnIntExpnAmt,
                 ROUND (TO_NUMBER (LoanISDprctAmortAmt), 2)
                     LoanISDprctAmortAmt,
                 ROUND (TO_NUMBER (LoanISNetIncBefTaxWthdrlAmt), 2)
                     LoanISNetIncBefTaxWthdrlAmt,
                 ROUND (TO_NUMBER (LoanISOwnrSalaryAmt), 2)
                     LoanISOwnrSalaryAmt,
                 ROUND (TO_NUMBER (LoanISIncTaxAmt), 2)
                     LoanISIncTaxAmt,
                 ROUND (TO_NUMBER (LoanISNetIncAmt), 2)
                     LoanISNetIncAmt,
                 ROUND (TO_NUMBER (LoanISCshflwAmt), 2)
                     LoanISCshflwAmt,
                 a.LoanFinanclStmtSourcCd,
                 b.LoanFinanclStmtSourcDescTxt,
                 LoanISBegnDt,
                 LoanISEndDt
            FROM LoanISTbl a, sbaref.LoanFinanclStmtSourcTbl b
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd
        ORDER BY LoanISSeqNmb;

    OPEN p_SelCur13 FOR
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               b.BusPrimPhnNmb,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusMailAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               b.BusDunsNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb                  -- ,a.InsurDisblDescTxt
                                     ,
               a.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS borrLoanCntlIntTyp                     -- NK--08/29/2016
                                        ,
               a.WorkrsCompInsRqrdInd,
               NULL
                   AS LACGNTYCONDTYPCD,
               NULL
                   AS GntyCondTypTxt,
               BusAltPhnNmb,
               BusPrimEmailAdr,
               BusAltEmailAdr,
               BusFedAgncySDPIEInd,
               BusSexNatrInd,
               BusNonFmrSBAEmpInd,
               BusNonLegBrnchEmpInd,
               BusNonFedEmpInd,
               BusNonGS13EmpInd,
               BusNonSBACEmpInd,
               b.BusPrimCntctNm,
               --     b.BusPrimCntctEmail,
               b.BusFedAgncyLoanInd,
               b.BusOutDbtInd,
               NULL
                   PerOthrCmntTxt
          FROM LoanBusTbl                 b,
               LoanBorrTbl                a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.IMEPCOperCdTbl      e
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND b.TaxId = a.TaxId
               AND BorrBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        UNION ALL
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   PerFirstNm,
               ocadataout (PerLastNm)
                   PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (PerPhyAddrStr1Nm)
                   PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               NULL
                   BusDunsNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb                  -- ,a.InsurDisblDescTxt
                                     ,
               a.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS borrLoanCntlIntTyp                     -- NK--08/29/2016
                                        ,
               a.WorkrsCompInsRqrdInd,
               NULL
                   AS LACGNTYCONDTYPCD,
               NULL
                   AS GntyCondTypTxt,
               NULL
                   BusAltPhnNmb,
               NULL
                   BusPrimEmailAdr,
               NULL
                   BusAltEmailAdr,
               NULL
                   BusFedAgncySDPIEInd,
               NULL
                   BusSexNatrInd,
               NULL
                   BusNonFmrSBAEmpInd,
               NULL
                   BusNonLegBrnchEmpInd,
               NULL
                   BusNonFedEmpInd,
               NULL
                   BusNonGS13EmpInd,
               NULL
                   BusNonSBACEmpInd,
               NULL
                   BusPrimCntctNm,
               --   NULL BusPrimCntctEmail,
               b.PerFedAgncyLoanInd,
               NULL
                   BusOutDbtInd,
               b.PerOthrCmntTxt
          FROM LoanPerTbl                 b,
               LoanBorrTbl                a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.IMEPCOperCdTbl      e,
               LoanPerFinanTbl            f
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = f.TaxId(+)
               AND a.LoanAppNmb = f.LoanAppNmb(+)
               AND b.TaxId = a.TaxId
               AND BorrBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        ORDER BY BorrSeqNmb;

    OPEN p_SelCur14 FOR
          SELECT a.BorrSeqNmb,
                 a.LoanPrevFinanSeqNmb,
                 a.LoanPrevFinanAgencyNm,
                 a.LoanPrevFinanLoanNmb,
                 a.LoanPrevFinanAppvDt,
                 ROUND (TO_NUMBER (a.LoanPrevFinanTotAmt), 2)
                     LoanPrevFinanTotNum,
                 ROUND (TO_NUMBER (a.LoanPrevFinanBalAmt), 2)
                     LoanPrevFinanBalNum,
                 a.LoanPrevFinanStatCd,
                 b.LoanPrevFinanStatDescTxt,
                 a.LoanPrevFinanDelnqCmntTxt
            FROM LoanPrevFinanTbl a, sbaref.LoanPrevFinanStatTbl b
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.LoanPrevFinanStatCd = b.LoanPrevFinanStatCd(+)
        ORDER BY a.BorrSeqNmb, a.LoanPrevFinanSeqNmb;

    OPEN p_SelCur15 FOR
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               b.BusPrimPhnNmb,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusPhyAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/12/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt
          FROM LoanBusTbl                 b,
               LoanGuarTbl                a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND b.TaxId = a.TaxId
               AND GuarBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
        UNION ALL
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   PerFirstNm,
               ocadataout (PerLastNm)
                   PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (PerPhyAddrStr1Nm)
                   PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/12/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt
          FROM LoanPerTbl                 b,
               LoanGuarTbl                a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               LoanPerFinanTbl            f
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = f.TaxId(+)
               AND a.LoanAppNmb = f.LoanAppNmb(+)
               AND b.TaxId = a.TaxId
               AND GuarBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
        ORDER BY GuarSeqNmb;

    OPEN p_SelCur16 FOR
        SELECT j.BorrSeqNmb,
               j.TaxId
                   BorrTaxId,
               p.PrinSeqNmb,
               p.TaxId
                   PrinTaxId,
               PrinBusPerInd,
               p.VetCd,
               VetTxt,
               b.VetCertInd,
               p.GndrCd,
               GndrDesc,
               p.EthnicCd,
               EthnicDesc,
               LoanPrinPrimBusExprnceYrNmb,
               LoanPrinInsurNm,
               LoanPrinInsurAmt,
               LoanPrinGntyInd,
               NVL (LoanBusPrinPctOwnrshpBus, 0)
                   LoanBusPrinPctOwnrshpBus,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusMailAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               j.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               bp.LoanCntlIntInd
                   AS busprinLoanCntlIntInd,
               (SELECT a.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL a
                 WHERE bp.LoanCntlIntTyp = a.CNTLINTTYPCD)
                   AS busprintLoanCntlIntTyp,                -- NK--08/15/2016
               p.LoanDisblInsurRqrdInd,
               p.LoanInsurDisblDescTxt,
               p.LoanLifeInsurRqrdInd,
               p.LoanPrinNoNCAEvdncInd,
               p.LoanComptitrNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = p.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = p.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               p.GntyLmtAmt,
               p.GntyLmtPct,
               p.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               p.GntyLmtYrNmb,
               b.BusPrimPhnNmb,
               b.BusAltPhnNmb,
               b.BusPrimEmailAdr,
               b.BusAltEmailAdr,
               b.BusFedAgncySDPIEInd,
               b.BusCSP60DayDelnqInd,
               b.BusLglActnInd,
               NULL
                   PerCitznShpCntNm
          FROM LoanBusTbl                 b,
               LoanBorrTbl                j,
               LoanPrinTbl                p,
               LoanBusPrinTbl             bp,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.VetTbl              e,
               sbaref.GndrCdTbl           f,
               sbaref.EthnicCdTbl         g
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND j.LoanAppNmb = p_LoanAppNmb
               AND p.LoanAppNmb = p_LoanAppNmb
               AND bp.LoanAppNmb = p_LoanAppNmb
               AND j.BorrSeqNmb = bp.BorrSeqNmb
               AND p.PrinSeqNmb = bp.PrinSeqNmb
               AND b.TaxId = p.TaxId
               AND p.PrinBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND p.VetCd = e.VetCd(+)
               AND p.GndrCd = f.GndrCd(+)
               AND p.EthnicCd = g.EthnicCd(+)
        UNION ALL
        SELECT j.BorrSeqNmb,
               j.TaxId
                   BorrTaxId,
               p.PrinSeqNmb,
               p.TaxId
                   PrinTaxId,
               PrinBusPerInd,
               p.VetCd,
               VetTxt,
               b.VetCertInd,
               p.GndrCd,
               GndrDesc,
               p.EthnicCd,
               EthnicDesc,
               LoanPrinPrimBusExprnceYrNmb,
               LoanPrinInsurNm,
               LoanPrinInsurAmt,
               LoanPrinGntyInd,
               NVL (LoanBusPrinPctOwnrshpBus, 0)
                   LoanBusPrinPctOwnrshpBus,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   AS PerFirstNm,
               ocadataout (PerLastNm)
                   AS PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               j.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               bp.LoanCntlIntInd
                   AS busprinLoanCntlIntInd,
               (SELECT a.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL a
                 WHERE bp.LoanCntlIntTyp = a.CNTLINTTYPCD)
                   AS busprintLoanCntlIntTyp,                -- NK--08/15/2016
               p.LoanDisblInsurRqrdInd,
               p.LoanInsurDisblDescTxt,
               p.LoanLifeInsurRqrdInd,
               p.LoanPrinNoNCAEvdncInd,
               p.LoanComptitrNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = p.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = p.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               p.GntyLmtAmt,
               p.GntyLmtPct,
               p.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               p.GntyLmtYrNmb,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (b.PerAltPhnNmb)
                   AS PerAltPhnNmb,
               ocadataout (b.PerPrimEmailAdr)
                   AS PerPrimEmailAdr,
               ocadataout (b.PerAltEmailAdr)
                   AS PerAltEmailAdr,
               b.PerFedAgncySDPIEInd,
               b.perCSP60DayDelnqInd,
               b.PerLglActnInd,
               (SELECT z.IMCNTRYNM
                  FROM SBAREF.IMCNTRYCDTBL z
                 WHERE z.IMCNTRYCD = b.PerCitznShpCntNm)
                   AS PerCitznShpCntNm
          FROM LoanPerTbl                 b,
               LoanBorrTbl                j,
               LoanPrinTbl                p,
               LoanBusPrinTbl             bp,
               LoanPerFinanTbl            i,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.VetTbl              e,
               sbaref.GndrCdTbl           f,
               sbaref.EthnicCdTbl         g
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND j.LoanAppNmb = p_LoanAppNmb
               AND p.LoanAppNmb = p_LoanAppNmb
               AND bp.LoanAppNmb = p_LoanAppNmb
               AND j.BorrSeqNmb = bp.BorrSeqNmb
               AND p.PrinSeqNmb = bp.PrinSeqNmb
               AND b.TaxId = p.TaxId
               AND b.TaxId = i.TaxId(+)
               AND b.LoanAppNmb = i.LoanAppNmb(+)
               AND PrinBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND p.VetCd = e.VetCd(+)
               AND p.GndrCd = f.GndrCd(+)
               AND p.EthnicCd = g.EthnicCd(+)
        ORDER BY PrinSeqNmb;

    OPEN p_SelCur17 FOR
        SELECT PrinSeqNmb, a.RaceCd, b.RaceTxt
          FROM LoanPrinRaceTbl a, sbaref.RaceCdTbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.RaceCd = b.RaceCd;

    OPEN p_SelCur18 FOR
          SELECT TaxId,
                 LoanFedEmpSeqNmb,
                 ocadataout (FedEmpFirstNm)        AS FedEmpFirstNm,
                 FedEmpInitialNm,
                 ocadataout (FedEmpLastNm)         AS FedEmpLastNm,
                 FedEmpSfxNm,
                 ocadataout (FedEmpAddrStr1Nm)     AS FedEmpAddrStr1Nm,
                 ocadataout (FedEmpAddrStr2Nm)     AS FedEmpAddrStr2Nm,
                 FedEmpAddrCtyNm,
                 FedEmpAddrStCd,
                 FedEmpAddrZipCd,
                 FedEmpAddrZip4Cd,
                 FedEmpAgncyOfc
            FROM LoanFedEmpTbl
           WHERE LoanAppNmb = p_LoanAppNmb
        ORDER BY TaxId, LoanFedEmpSeqNmb;

    OPEN p_SelCur19 FOR
          SELECT d.TaxId,
                 d.LoanIndbtnesSeqNmb,
                 d.LoanIndbtnesPayblToNm,
                 d.LoanIndbtnesPurpsTxt,
                 d.LoanIndbtnesOrglDt,
                 ROUND (TO_NUMBER (d.LoanIndbtnesCurBalAmt), 2)
                     LoanIndbtnesCurBalNum,
                 d.LoanIndbtnesIntPct,
                 d.LoanIndbtnesMatDt,
                 ROUND (TO_NUMBER (d.LoanIndbtnesPymtAmt), 2)
                     LoanIndbtnesPymtNum,
                 d.LoanIndbtnesCollatDescTxt,
                 freq.CalndrPrdDesc,
                 stat.LoanPrevFinanStatDescTxt
            FROM LoanIndbtnesTbl            d,
                 sbaref.CalndrPrdTbl        freq,
                 sbaref.LoanPrevFinanStatTbl stat
           WHERE     d.LoanAppNmb = p_LoanAppNmb
                 AND d.LoanIndbtnesPymtFreqCd = freq.CalndrPrdCd(+)
                 AND d.LoanIndbtnesPrevFinanStatCd =
                     stat.LoanPrevFinanStatCd(+)
        ORDER BY d.TaxId, d.LoanIndbtnesSeqNmb;

    OPEN p_SelCur20 FOR
        SELECT d.LoanPartLendrNm,
               d.LocId,
               d.LoanPartLendrStr1Nm,
               d.LoanPartLendrStr2Nm,
               d.LoanPartLendrCtyNm,
               d.LoanPartLendrStCd,
               d.LoanPartLendrZip5Cd,
               d.LoanPartLendrZip4Cd,
               d.LoanLienPosCd,
               ROUND (TO_NUMBER (d.LoanPartLendrAmt), 2)
                   LoanPartLendrNum,
               ocadataout (d.LoanPartLendrOfcrLastNm)
                   AS LoanPartLendrOfcrLastNm,
               ocadataout (d.LoanPartLendrOfcrFirstNm)
                   AS LoanPartLendrOfcrFirstNm,
               d.LoanPartLendrOfcrInitialNm,
               d.LoanPartLendrOfcrSfxNm,
               d.LoanPartLendrOfcrTitlTxt,
               ocadataout (d.LoanPartLendrOfcrPhnNmb)
                   AS LoanPartLendrOfcrPhnNmb,
               d.loanpartlendrcntrycd,
               d.loanpartlendrpostcd,
               d.loanpartlendrstnm,
               r.LoanPartLendrTypDescTxt,
               LOANLENDRTAXID,
               LOANLENDRGROSSINTPCT,
               LOANLENDRSERVFEEPCT,
               LOANLENDRPARTPCT,
               LOANORIGPARTPCT,
               LOANSBAGNTYBALPCT,
               LOANLENDRPARTAMT,
               LOANORIGPARTAMT,
               LOANLENDRGROSSAMT,
               LOANSBAGNTYBALAMT,
               LOAN504PARTLENDRSEQNMB,
               LOANPOOLAPPNMB
          FROM LoanPartLendrTbl d, sbaref.LoanPartLendrTypTbl r
         WHERE     d.LoanAppNmb = p_LoanAppNmb
               AND d.LoanPartLendrTypCd = r.LoanPartLendrTypCd(+);

    OPEN p_SelCur21 FOR
        SELECT b.ARCLoanRsnCd, b.ARCLoanRsnDescTxt, a.ARCLoanRsnOthDescTxt
          FROM loanapp.LoanAppARCRsnTbl a, sbaref.ARCLoanRsnTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ARCLoanRsnCd = b.ARCLoanRsnCd;

    OPEN p_SelCur22 FOR SELECT LOANAPPNMB,
                               IMASSTAREACD,
                               IMASSTOTHAREATXT,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPASSTAREATBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur23 FOR SELECT LOANAPPNMB,
                               BUSEXISTIND,
                               GROSSREVNUAMT,
                               ASSTRECVIND,
                               CREATUSERID,
                               CREATDT,
                               LASTUPDTUSERID,
                               LASTUPDTDT
                          FROM LOANAPP.LOANAPPCAINFOTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur24 FOR SELECT LOANAPPNMB,
                               IMCNSELNGSRCTYPCD,
                               IMCNSELNGOTHSRCTXT,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPCNSELNGSRCTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur25 FOR SELECT LOANAPPNMB,
                               IMCNSELNGTYPCD,
                               IMCNSELNGHRCD,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPCNSELNGTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur26 FOR SELECT LOANAPPNMB,
                               TOTPYMTAMT,
                               LOAN7APYMTAMT,
                               SELLERFINANFULLSTBYAMT,
                               SELLERFINANNONFULLSTBYAMT,
                               BUYEREQTYCASHAMT,
                               BUYEREQTYCASHDESCTXT,
                               BUYEREQTYBORRAMT,
                               BUYEREQTYOTHAMT,
                               BUYEREQTYOTHDESCTXT,
                               TOTASSETAMT,
                               ACCTRECVASSETAMT,
                               INVTRYASSETAMT,
                               REASSETAMT,
                               REVALTYPIND,
                               EQUIPASSETAMT,
                               EQUIPVALTYPIND,
                               FIXASSETAMT,
                               INTANGASSETAMT,
                               OTHASSETAMT,
                               COVNTASSETAMT,
                               CUSTASSETAMT,
                               LICNSASSETAMT,
                               FRNCHSASSETAMT,
                               GOODWILLASSETAMT,
                               OTHINTANGASSETAMT,
                               OTHINTANGASSETDESCTXT,
                               TOTAPPRAMT,
                               BUSAPPRNM,
                               BUSAPPRFEEAMT,
                               BUSAPPRASAIND,
                               BUSAPPRCBAIND,
                               BUSAPPRABVIND,
                               BUSAPPRCVAIND,
                               BUSAPPRAVAIND,
                               BUSAPPRCPAIND,
                               BUSBRKRCOMISNIND,
                               BUSBRKRNM,
                               BUSBRKRCOMISNAMT,
                               BUSBRKRADR,
                               CREATUSERID,
                               CREATDT,
                               LASTUPDTUSERID,
                               LASTUPDTDT,
                               OTHASSETDESCTXT
                          FROM LOANAPPCHNGBUSOWNRSHPTBL
                         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur27 FOR
        SELECT i.LOANAPPNMB,
               i.LOANINTDTLSEQNMB,
               i.LOANINTLOANPARTPCT,
               i.LOANINTLOANPARTMONMB,
               i.LOANINTFIXVARIND,
               i.LOANINTRT,
               i.IMRTTYPCD,
               d.IMRTTYPDESCTXT,
               i.LOANINTBASERT,
               i.LOANINTSPRDOVRPCT,
               CASE
                   WHEN i.LOANINTADJPRDCD IS NULL
                   THEN
                       ' '
                   ELSE
                       (SELECT CALNDRPRDDESC
                          FROM sbaref.CalndrPrdTbl e
                         WHERE e.CALNDRPRDCD = i.LOANINTADJPRDCD)
               END
                   CALNDRPRDDESC,
               i.LOANINTADJPRDMONMB,
               i.LOANINTADJPRDEFFDT,
               LoanIntDtlGuarInd,
               (SELECT s.LOANINTDTLDESCTXT
                  FROM Sbaref.LOANINTDTLCDTBL s
                 WHERE s.LOANINTDTLCD = i.LOANINTFIXVARIND)
                   AS LOANINTDTLDESCTXT,
               (TRUNC (LoanGntyNoteDt))
                   LoanGntyNoteDt
          FROM LOANAPP.LOANINTDTLTBL  i,
               LOANAPP.LOANAPPTBL     a,
               sbaref.IMRtTypTbl      d
         WHERE     i.LOANAPPNMB = p_LOANAPPNMB
               AND i.LOANAPPNMB = a.LOANAPPNMB
               AND i.IMRTTYPCD = d.IMRTTYPCD
               AND a.PrcsMthdCd = d.PrcsMthdCd;

    OPEN p_SelCur28 FOR
        SELECT LOANAPPPYMTAMT,
               CASE
                   WHEN LOANPYMNINSTLMNTFREQCD = 'M' THEN 'month'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'quarter'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'S' THEN 'semi annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'A' THEN 'annual'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTFREQTXT,
               PYMNTDAYNMB,
               -- PYMNTDAYSAMEIND ,
               PYMNTBEGNMONMB,
               ESCROWACCTRQRDIND,
               NETEARNCLAUSEIND,
               ARMMARTYPIND,
               CASE
                   WHEN ARMMARTYPIND = 'N' THEN 'None'
                   WHEN ARMMARTYPIND = 'F' THEN 'Ceiling and Floor Fluctuate'
                   WHEN ARMMARTYPIND = 'X' THEN 'Ceiling and Floor are Fixed'
                   ELSE NULL
               END
                   AS ARMMARTYPTXT,
               ARMMARCEILRT,
               ARMMARFLOORRT,
               STINTRTREDUCTNIND,
               LATECHGIND,
               LATECHGAFTDAYNMB,
               LATECHGFEEPCT,
               -- LOANAPPMOINTQTY ,
               PYMNTINTONLYBEGNMONMB,
               PYMNTINTONLYFREQCD,
               CASE
                   WHEN PYMNTINTONLYFREQCD = 'M' THEN 'month'
                   WHEN PYMNTINTONLYFREQCD = 'Q' THEN 'quarter'
                   WHEN PYMNTINTONLYFREQCD = 'S' THEN 'semi annual'
                   WHEN PYMNTINTONLYFREQCD = 'A' THEN 'annual'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTINTONLYFREQTXT,
               PYMNTINTONLYDAYNMB,
               --PYMNTINTONLYDAYSAMEIND,
               NetEarnPymntPct,
               NetEarnPymntOverAmt,
               StIntRtReductnPrgmNm,
               a.LOANAPPMOINTQTY,
               a.LoanPymtRepayInstlTypCd
          FROM LOANAPP.LOANPYMNTTBL p, LOANAPP.LoanAppTbl a
         WHERE a.LoanAppNmb = p_LoanAppNmb AND a.LoanAppNmb = p.LoanAppNmb(+);

    OPEN p_SelCur29 FOR
        SELECT LoanCollatSeqNmb,
               TaxId,
               BUSPERIND,
               CASE
                   WHEN BUSPERIND = 'B'
                   THEN
                       (SELECT BusNm
                          FROM loanapp.LOANbustbl z
                         WHERE     z.loanappnmb = p_loanappnmb
                               AND z.taxid = c.taxid)
                   ELSE
                       (SELECT    ocadataout (PerFirstNm)
                               || LTRIM (
                                         ' '
                                      || CASE
                                             WHEN PerInitialNm IS NOT NULL
                                             THEN
                                                 PerInitialNm || '.'
                                             ELSE
                                                 NULL
                                         END)
                               || LTRIM (' ' || ocadataout (PerLastNm))
                               || LTRIM (' ' || PerSfxNm)
                          FROM loanapp.loanpertbl z
                         WHERE     z.loanappnmb = p_loanappnmb
                               AND z.taxid = c.taxid)
               END
                   AS BUSPERNm
          FROM loanapp.LoanLmtGntyCollatTbl c
         WHERE LoanAppNmb = p_LoanAppNmb;

    --StbyAgrmt
    OPEN p_SelCur30 FOR
        SELECT a.LOANSTBYCRDTRNM,
               a.LOANSTBYAMT,
               (SELECT s.STBYPYMTTYPDESCTXT
                  FROM SBAREF.STBYPYMTTYPCDTBL s
                 WHERE a.LOANSTBYREPAYTYPCD = S.STBYPYMTTYPCD)
                   AS LOANSTBYPYMTTYPDESCTXT,
               a.LOANSTBYPYMTINTRT,
               a.LOANSTBYPYMTOTHDESCTXT,
               a.LOANSTBYPYMTAMT,
               a.LOANSTBYREPAYTYPCD,
               a.LOANSTBYPYMTSTRTDT
          FROM LOANAPP.LOANStbyAgrmtDtlTBL a
         WHERE a.LOANAPPNMB = p_LOANAPPNMB;

    --Export Country Codes---
    OPEN p_SelCur31 FOR
          SELECT a.LoanAppNmb,
                 a.LoanExprtCntryCd,
                 b.imcntrynm,
                 a.LoanExprtCntrySeqNmb
            FROM LOANAPP.LOANEXPRTCNTRYTBL a, SBAREF.IMCNTRYCDTBL b
           WHERE     a.LOANAPPNMB = p_loanappnmb
                 AND a.LoanExprtCntryCd = b.ImCntryCd
        ORDER BY LOANEXPRTCNTRYSEQNMB ASC;

    OPEN p_SelCur32 FOR
          SELECT a.LOANAGNTSEQNMB,
                 a.LOANAGNTID,
                 a.LOANAGNTBUSPERIND,
                 a.LOANAGNTNM,
                    ocadataout (a.LOANAGNTCNTCTFIRSTNM)
                 || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                 || ' '
                 || ocadataout (a.LOANAGNTCNTCTLastNm)
                 || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                     AS LoanContactName,
                    ocadataout (a.LOANAGNTADDRSTR1NM)
                 || RTRIM (' ' || ocadataout (a.LOANAGNTADDRSTR2NM))
                 || ' '
                 || a.LOANAGNTADDRCTYNM
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTNM)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIPCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIP4CD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRPOSTCD)
                 || ' '
                 || a.LOANAGNTADDRCNTCD
                     AS LoanContactaddress,
                 a.LOANAGNTTYPCD,
                 a.LOANAGNTDOCUPLDIND,
                 a.LOANCDCTPLFEEIND,
                 a.LOANCDCTPLFEEAMT,
                 b.LOANAGNTSERVTYPCD,
                 b.LOANAGNTSERVOTHTYPTXT,
                 b.LOANAGNTAPPCNTPAIDAMT,
                 b.LOANAGNTSBALENDRPAIDAMT
            FROM LOANAPP.LoanAgntTbl a, loanagntfeedtltbl b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.loanappnmb = b.loanappnmb
                 AND a.loanagntseqnmb = b.loanagntseqnmb
        ORDER BY loanagntseqnmb ASC;
END LOANPRINTSELCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPSELCSP (
   p_Identifier                  IN     NUMBER DEFAULT 0,
   p_LoanAppNmb                  IN     NUMBER DEFAULT 0,
   p_RetVal                         OUT NUMBER,
   p_LoanAppCDCClsCostAmt           OUT NUMBER,
   p_LoanAppCDCOthClsCostAmt        OUT NUMBER,
   p_LoanAppCDCGntyAmt              OUT NUMBER,
   p_LoanAppCDCJobRat               OUT NUMBER,
   p_LoanAppCDCFundFeeAmt           OUT NUMBER,
   p_LoanAppCDCNetDbentrAmt         OUT NUMBER,
   p_LoanAppCDCSeprtPrcsFeeInd      OUT VARCHAR2,
   p_LoanAppCDCPrcsFeeAmt           OUT NUMBER,
   p_LoanAppCDCUndrwtrFeeAmt        OUT NUMBER,
   p_LoanAppContribPct              OUT NUMBER,
   p_LoanAppContribAmt              OUT NUMBER,
   p_LoanAppCurrEmpQty              OUT NUMBER,
   p_LoanAppFrnchsInd               OUT VARCHAR2,
   p_LoanAppFrnchsCd                OUT VARCHAR2,
   p_LoanAppFrnchsNm                OUT VARCHAR2,
   p_LoanAppJobCreatQty             OUT NUMBER,
   p_LoanAppJobRtnd                 OUT NUMBER,
   p_LoanAppJobRqmtMetInd           OUT VARCHAR2,
   p_LoanAppNetExprtAmt             OUT NUMBER,
   p_LoanAppNewBusCd                OUT VARCHAR2,
   p_LoanAppProjCtyNm               OUT VARCHAR2,
   p_LoanAppProjCntyCd              OUT VARCHAR2,
   p_LoanAppProjStr1Nm              OUT VARCHAR2,
   p_LoanAppProjStr2Nm              OUT VARCHAR2,
   p_LoanAppProjStCd                OUT VARCHAR2,
   p_LoanAppProjZipCd               OUT VARCHAR2,
   p_LoanAppProjZip4Cd              OUT VARCHAR2,
   p_LoanAppRqstAmt                 OUT NUMBER,
   p_LoanAppRqstMatMoQty            OUT NUMBER,
   p_LoanAppRuralUrbanInd           OUT VARCHAR2,
   p_LocId                          OUT NUMBER,
   p_NAICSYrNmb                     OUT NUMBER,
   p_NAICSCd                        OUT NUMBER,
   p_PrcsMthdCd                     OUT VARCHAR2,
   p_PrgmCd                         OUT VARCHAR2,
   p_PrtId                          OUT NUMBER,
   p_StatCd                         OUT VARCHAR2,
   p_SumPrtAmt                      OUT NUMBER,
   p_SumLoanInjctnAmt               OUT NUMBER,
   p_LOANAPPDISASTERCNTRLNMB        OUT VARCHAR2,
   p_LOANDISASTRAPPFEECHARGED       OUT NUMBER,
   p_LOANDISASTRAPPDCSN             OUT VARCHAR2,
   p_LOANASSOCDISASTRAPPNMB         OUT VARCHAR2,
   p_LOANASSOCDISASTRLOANNMB        OUT VARCHAR2,
   p_LOANDISASTRAPPNMB              OUT VARCHAR2,
   p_LoanAppCDCGrossDbentrAmt       OUT NUMBER,
   p_LoanAppBalToBorrAmt            OUT NUMBER,
   p_LOANBUSESTDT                   OUT DATE,
   p_LoanAppRevlMoIntQty                OUT NUMBER ,
   p_LoanAppAmortMoIntQty               OUT NUMBER ,
   p_LOANAPPRURALINITIATVIND out VARCHAR2,
   p_PrtyGrpInd                     OUT VARCHAR2 )
AS
/* Date         :
   Programmer   :
   Remarks      :
    Parameters   :
***********************************************************************************
APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
SP  -- 04/07/2014 -- Moved NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd from LoanAppTbl to LoanAppProjTbl
NK--03/28/2017-- OPSMDEV 1385-- Removed EconDevObjctCd
RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0
SS--11/17/2017--OPSMDEV 1566-- Added LOANBUSESTDT
NK--01/16/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
RY--08/07/2018--OPSMDEV-1876:: Added LoanAppRuralInitiatvInd 
BO--06/28/2021--OPSMDEV-2768 -- Added PrtyGrpInd column to the query
*********************************************************************************
*/
BEGIN
   SAVEPOINT LoanAppSel;

   SELECT c.LocId,
          c.PrtId,
          a.PrgmCd,
          SUBSTR (a.PrcsMthdCd, 1, 3),
          d.NAICSYrNmb,
          d.NAICSCd,
          a.LoanAppNewBusCd,
          d.LoanAppNetExprtAmt,
          d.LoanAppCurrEmpQty,
          d.LoanAppJobCreatQty,
          d.LoanAppJobRtnd,
          d.LoanAppJobRqmtMetInd,
          d.LoanAppCDCJobRat,
          d.LoanAppFrnchsInd,
          d.LoanAppFrnchsCd,
          d.LoanAppFrnchsNm,
          d.LoanAppRuralUrbanInd,
          a.LoanAppContribPct,
          a.LoanAppContribAmt,
          a.LoanAppRqstAmt,
          a.LoanAppRqstMatMoQty,
          a.LoanAppCDCGntyAmt,
          a.LoanAppCDCNetDbentrAmt,
          a.LoanAppCDCFundFeeAmt,
          a.LoanAppCDCSeprtPrcsFeeInd,
          a.LoanAppCDCPrcsFeeAmt,
          a.LoanAppCDCClsCostAmt,
          a.LoanAppCDCothClsCostAmt,
          a.LoanAppCDCUndrwtrFeeAmt,
          d.LoanAppProjStr1Nm,
          d.LoanAppProjStr2Nm,
          d.LoanAppProjCtyNm,
          d.LoanAppProjCntyCd,
          d.LoanAppProjStCd,
          d.LoanAppProjZipCd,
          d.LoanAppProjZip4Cd,
          a.StatCd,
          
          NVL ( (SELECT SUM (p.LoanPartLendrAmt)
                   FROM LoanPartLendrTbl p
                  WHERE LoanAppNmb = p_LoanAppNmb),
               0),
          NVL ( (SELECT SUM (i.LoanInjctnAmt)
                   FROM LoanInjctnTbl i
                  WHERE LoanAppNmb = p_LoanAppNmb),
               0),
          LOANAPPDISASTERCNTRLNMB,
          LOANDISASTRAPPFEECHARGED,
          LOANDISASTRAPPDCSN,
          LOANASSOCDISASTRAPPNMB,
          LOANASSOCDISASTRLOANNMB,
          LOANDISASTRAPPNMB,
          LoanAppCDCGrossDbentrAmt,
          LoanAppBalToBorrAmt,
          d.LOANBUSESTDT,
          a.LoanAppRevlMoIntQty, 
          a.LoanAppAmortMoIntQty,
          d.LOANAPPRURALINITIATVIND,
          a.PrtyGrpInd 
     INTO p_LocId,
          p_PrtId,
          p_PrgmCd,
          p_PrcsMthdCd,
          p_NAICSYrNmb,
          p_NAICSCd,
          p_LoanAppNewBusCd,
          p_LoanAppNetExprtAmt,
          p_LoanAppCurrEmpQty,
          p_LoanAppJobCreatQty,
          p_LoanAppJobRtnd,
          p_LoanAppJobRqmtMetInd,
          p_LoanAppCDCJobRat,
          p_LoanAppFrnchsInd,
          p_LoanAppFrnchsCd,
          p_LoanAppFrnchsNm,
          p_LoanAppRuralUrbanInd,
          p_LoanAppContribPct,
          p_LoanAppContribAmt,
          p_LoanAppRqstAmt,
          p_LoanAppRqstMatMoQty,
          p_LoanAppCDCGntyAmt,
          p_LoanAppCDCNetDbentrAmt,
          p_LoanAppCDCFundFeeAmt,
          p_LoanAppCDCSeprtPrcsFeeInd,
          p_LoanAppCDCPrcsFeeAmt,
          p_LoanAppCDCClsCostAmt,
          p_LoanAppCDCothClsCostAmt,
          p_LoanAppCDCUndrwtrFeeAmt,
          p_LoanAppProjStr1Nm,
          p_LoanAppProjStr2Nm,
          p_LoanAppProjCtyNm,
          p_LoanAppProjCntyCd,
          p_LoanAppProjStCd,
          p_LoanAppProjZipCd,
          p_LoanAppProjZip4Cd,
          p_StatCd,         
          p_SumPrtAmt,
          p_SumLoanInjctnAmt,
          p_LOANAPPDISASTERCNTRLNMB,
          p_LOANDISASTRAPPFEECHARGED,
          p_LOANDISASTRAPPDCSN,
          p_LOANASSOCDISASTRAPPNMB,
          p_LOANASSOCDISASTRLOANNMB,
          p_LOANDISASTRAPPNMB,
          p_LoanAppCDCGrossDbentrAmt,
          p_LoanAppBalToBorrAmt,
          p_LOANBUSESTDT,
          p_LoanAppRevlMoIntQty, 
          p_LoanAppAmortMoIntQty,
          p_LOANAPPRURALINITIATVIND,
          p_PrtyGrpInd 
     FROM LoanAppTbl a, LoanAppPrtTbl c, LoanAppProjTbl d
    WHERE     a.LoanAppNmb = p_LoanAppNmb
          AND a.LoanAppNmb = c.LoanAppNmb(+)
          AND a.LoanAppNmb = d.LoanAppNmb(+);

   p_RetVal := SQL%ROWCOUNT;
   p_RetVal := NVL (p_RetVal, 0);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      NULL; 
   WHEN OTHERS
   THEN
      BEGIN
         ROLLBACK TO LoanAppSel;
         RAISE;
      END;
END LOANAPPSELCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCOPYCSP (
    p_LoanAppNmb          NUMBER := NULL,
    p_CreatUserId         VARCHAR2 := NULL,
    p_NEWLoanAppNmb   OUT NUMBER)
AS
    /*
     created by:   BJRychener
     created date: October 31, 2017
     purpose:      Loan Copy Procedure
     Revision:

    */
    /* NK--11/90/2017--OPSMDEV 1551 added PerFedAgncySDPIEInd, PerCSP60DayDelnqInd,PerLglActnInd, PerCitznShpCntNm ,PerAltPhnNmb, PerPrimEmailAdr ,PerAltEmailAdr To LoanPerTbl
       NK--11/13/2017 OPSMDEV 1551  Added BusAltPhnNmb ,  BusPrimEmailAdr ,  BusAltEmailAdr, BusCSP60DayDelnqInd, BusLglActnInd to LoanBusTbl
       NK--11/13/2017 OPSMDEV 1558  Added BusSexNatrInd,BusNonFmrSBAEmpInd,BusNonLegBrnchEmpInd,BusNonFedEmpInd,BusNonGS13EmpInd,BusNonSBACEmpInd to LoanBusTbl
       BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
       NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to LoanPrevFinanTbl
       SS 11/15/2017--OPSMDEV 1573 Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,
       LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn,LoanPrtAltCntctCellPhn,
              LoanPrtAltCntctemail, LendrAltCntctTypCd to LOANAPPPRTTBL
       SS--11/15/2017 --OPSMDEV 1566 Added LOANBUSESTDT to Loanappprojtbl and BusPrimCntctNm  to LoanBustbl
       NK-- 11/15/2017--OPSMDEV 1591 Added PeredAgncyLoanInd to LoanPerTbl
       NK--11/15/2017--OPSMDEV 1591 Added BusedAgncyLoanInd to LoanBusTbl
       NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to LoanBusTbl
       NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to LoanAppTbl
       BJR-- 02/01/2018--CAFSOPER-1407-- added UPPER() to srch names to allow records to be stored in caps and easier to find during searches.
       RY-- 04/20/2018--OPSMDEV--1786 --Added WorkrsCompInsRqrdInd to LoanBorrTbl
       SS--08/06/2018--OPSMDEV--1882-- Added LoanAGNtTbl and LoanAgntfeedtltbl
       SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to loanapptbl
       BR--10/30/2018--OPSMDEV 1885 -- added LOANAPPPROJGEOCD, LOANAPPHUBZONEIND, LOANAPPPROJLAT, LOANAPPPROJLONG
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
       SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to loanapptbl
       SS--02/18/2020 OPSMDEV 2409,2410 Added LoanAppCBRSInd,LoanAppOppZoneInd to LoanAppProjTbl
       SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to loanapptbl
       SS--09/05/2020 CARESACT 595 copying demographics from loan master tables
       SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to LoanAppProjTbl
       JS--02/11/2021 Loan Authorization Modernization. Added 7 new columns LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to LoanApp.LoanGuarTbl.
       SL-04/26/2021--OPSMDEV-2576  Addeed new column PerTAXIDCertInd to LoanPerTbl
    */

    v_NEWLoanAppNmb   NUMBER := NULL;
    v_count           NUMBER;
BEGIN
    BEGIN
        --Generate New Loan App Number using MAX(LoanAppNmb) from LoanApp.LoanAppTbl
        LoanApp.IncrmntSeqNmbCSP ('Application', v_NEWLoanAppNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanAppTbl
        INSERT INTO LoanApp.LoanAppTbl (LoanAppNmb,
                                        PrgmCd,
                                        PrcsMthdCd,
                                        LoanAppNm,
                                        LoanAppRecvDt,
                                        LoanAppEntryDt,
                                        LoanAppRqstAmt,
                                        LoanAppSBAGntyPct,
                                        LoanAppSBARcmndAmt,
                                        LoanAppRqstMatMoQty,
                                        LoanAppEPCInd,
                                        LoanAppPymtAmt,
                                        LoanAppFullAmortPymtInd,
                                        LoanAppMoIntQty,
                                        LoanAppLifInsurRqmtInd,
                                        LoanAppRcnsdrtnInd,
                                        LoanAppInjctnInd,
                                        LoanAppEWCPSnglTransPostInd,
                                        LoanAppEWCPSnglTransInd,
                                        LoanAppEligEvalInd,
                                        LoanAppNewBusCd,
                                        LoanAppDisasterCntrlNmb,
                                        LoanCollatInd,
                                        LoanAppSourcTypCd,
                                        StatCd,
                                        LoanAppStatUserId,
                                        LoanAppStatDt,
                                        LoanAppOrigntnOfcCd,
                                        LoanAppPrcsOfcCd,
                                        LoanAppAppvDt,
                                        RecovInd,
                                        LoanAppMicroLendrId,
                                        LOANDISASTRAPPNMB,
                                        LOANDISASTRAPPFEECHARGED,
                                        LOANDISASTRAPPDCSN,
                                        LOANASSOCDISASTRAPPNMB,
                                        LOANASSOCDISASTRLOANNMB,
                                        LoanAppCreatUserId,
                                        LoanAppCreatDt,
                                        LastUpdtUserId,
                                        LastUpdtDt,
                                        LOANAPPNETEARNIND,
                                        LOANAPPSERVOFCCD,
                                        LoanAppRevlInd,
                                        LOANNMB,
                                        LOANAPPFUNDDT,
                                        UNDRWRITNGBY,
                                        COHORTCD,
                                        FUNDSFISCLYR,
                                        LOANTYPIND,
                                        LOANAPPINTDTLCD,
                                        LOANPYMNINSTLMNTFREQCD,
                                        FININSTRMNTTYPIND,
                                        LOANAMTLIMEXMPTIND,
                                        LoanPymntSchdlFreq,
                                        LoanPymtRepayInstlTypCd,
                                        LOANGNTYMATSTIND,
                                        LoanGntyNoteDt,
                                        CDCServFeePct,
                                        LoanAppContribAmt,
                                        LoanAppContribPct,
                                        LoanAppCDCGntyAmt,
                                        LOANAPPCDCNETDBENTRAMT,
                                        LoanAppCDCFundFeeAmt,
                                        LOANAPPCDCSEPRTPRCSFEEIND,
                                        LoanAppCDCPrcsFeeAmt,
                                        LoanAppCDCClsCostAmt,
                                        LoanAppCDCOthClsCostAmt,
                                        LoanAppCDCUndrwtrFeeAmt,
                                        LoanAppCDCGrossDbentrAmt,
                                        LoanAppBalToBorrAmt,
                                        LoanAppOutPrgrmAreaOfOperInd,
                                        LoanAppRevlMoIntQty,
                                        LoanAppAmortMoIntQty,
                                        LoanAppExtraServFeeAMT,
                                        LoanAppExtraServFeeInd,
                                        RqstSeqNmb,
                                        LoanAppMonthPayroll)
            SELECT v_NEWLoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   LoanAppNm,
                   NULL,                                      --LoanAppRecvDt,
                   SYSDATE,                                  --LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   LoanAppEligEvalInd,
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   LoanCollatInd,
                   LoanAppSourcTypCd,
                   'IP',
                   p_CreatUserId,                         --LoanAppStatUserId,
                   SYSDATE,                                   --LoanAppStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   NULL,
                   RecovInd,
                   LoanAppMicroLendrId,
                   NULL,                                  --LOANDISASTRAPPNMB,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   p_CreatUserId,                        --LoanAppCreatUserId,
                   SYSDATE,                                  --LoanAppCreatDt,
                   p_CreatUserId,                            --LastUpdtUserId,
                   SYSDATE,                                      --LastUpdtDt,
                   LOANAPPNETEARNIND,
                   LOANAPPSERVOFCCD,
                   LoanAppRevlInd,
                   NULL,                                            --LOANNMB,
                   NULL,                                      --LOANAPPFUNDDT,
                   UNDRWRITNGBY,
                   NULL,                                           --COHORTCD,
                   NULL,                                       --FUNDSFISCLYR,
                   LOANTYPIND,
                   LOANAPPINTDTLCD,
                   LOANPYMNINSTLMNTFREQCD,
                   FININSTRMNTTYPIND,
                   LOANAMTLIMEXMPTIND,
                   LoanPymntSchdlFreq,
                   LoanPymtRepayInstlTypCd,
                   LOANGNTYMATSTIND,
                   NULL,                                     --LoanGntyNoteDt,
                   CDCServFeePct,
                   LoanAppContribAmt,
                   LoanAppContribPct,
                   LoanAppCDCGntyAmt,
                   LOANAPPCDCNETDBENTRAMT,
                   LoanAppCDCFundFeeAmt,
                   LOANAPPCDCSEPRTPRCSFEEIND,
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCOthClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppCDCGrossDbentrAmt,
                   LoanAppBalToBorrAmt,
                   LoanAppOutPrgrmAreaOfOperInd,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   RqstSeqNmb,
                   LoanAppMonthPayroll
              FROM LoanApp.LoanAppTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppProjTbl
        INSERT INTO LoanApp.LoanAppProjTbl (LoanAppNmb,
                                            LoanAppProjStr1Nm,
                                            LoanAppProjStr2Nm,
                                            LoanAppProjCtyNm,
                                            LoanAppProjCntyCd,
                                            LoanAppProjStCd,
                                            LoanAppProjZipCd,
                                            LoanAppProjZip4Cd,
                                            LoanAppRuralUrbanInd,
                                            LoanAppNetExprtAmt,
                                            LoanAppCurrEmpQty,
                                            LoanAppJobCreatQty,
                                            LoanAppJobRtnd,
                                            LoanAppJobRqmtMetInd,
                                            LoanAppCDCJobRat,
                                            LoanAppProjCreatUserId,
                                            LoanAppProjCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            NAICSYrNmb,
                                            NAICSCd,
                                            LoanAppFrnchsCd,
                                            LoanAppFrnchsNm,
                                            LoanAppFrnchsInd,
                                            ADDTNLLOCACQLMTIND,
                                            FIXASSETACQLMTIND,
                                            FIXASSETACQLMTAMT,
                                            COMPSLMTIND,
                                            COMPSLMTAMT,
                                            BULKSALELAWCOMPLYIND,
                                            FRNCHSRECRDACCSIND,
                                            FRNCHSFEEDEFMONNMB,
                                            FRNCHSFEEDEFIND,
                                            FRNCHSTERMNOTCIND,
                                            FRNCHSLENDRSAMEOPPIND,
                                            LOANBUSESTDT,
                                            LOANAPPPROJGEOCD,
                                            LOANAPPHUBZONEIND,
                                            LOANAPPPROJLAT,
                                            LOANAPPPROJLONG,
                                            LoanAppCBRSInd,
                                            LoanAppOppZoneInd,
                                            LOANAPPPROJLMIND,
                                            LOANAPPPROJSIZECD)
            SELECT v_NEWLoanAppNmb,
                   LoanAppProjStr1Nm,
                   LoanAppProjStr2Nm,
                   LoanAppProjCtyNm,
                   LoanAppProjCntyCd,
                   LoanAppProjStCd,
                   LoanAppProjZipCd,
                   LoanAppProjZip4Cd,
                   LoanAppRuralUrbanInd,
                   LoanAppNetExprtAmt,
                   LoanAppCurrEmpQty,
                   LoanAppJobCreatQty,
                   LoanAppJobRtnd,
                   LoanAppJobRqmtMetInd,
                   LoanAppCDCJobRat,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NAICSYrNmb,
                   NAICSCd,
                   LoanAppFrnchsCd,
                   LoanAppFrnchsNm,
                   LoanAppFrnchsInd,
                   ADDTNLLOCACQLMTIND,
                   FIXASSETACQLMTIND,
                   FIXASSETACQLMTAMT,
                   COMPSLMTIND,
                   COMPSLMTAMT,
                   BULKSALELAWCOMPLYIND,
                   FRNCHSRECRDACCSIND,
                   FRNCHSFEEDEFMONNMB,
                   FRNCHSFEEDEFIND,
                   FRNCHSTERMNOTCIND,
                   FRNCHSLENDRSAMEOPPIND,
                   LOANBUSESTDT,
                   LOANAPPPROJGEOCD,
                   LOANAPPHUBZONEIND,
                   LOANAPPPROJLAT,
                   LOANAPPPROJLONG,
                   LoanAppCBRSInd,
                   LoanAppOppZoneInd,
                   LOANAPPPROJLMIND,
                   LOANAPPPROJSIZECD
              FROM LoanApp.LoanAppProjTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanInjctnTbl
        INSERT INTO LoanApp.LoanInjctnTbl (LoanAppNmb,
                                           LoanInjctnSeqNmb,
                                           InjctnTypCd,
                                           LoanInjctnOthDescTxt,
                                           LoanInjctnAmt,
                                           LoanInjctnCreatUserId,
                                           LoanInjctnCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanInjctnFundTermYrNmb)
            SELECT v_NEWLoanAppNmb,
                   LoanInjctnSeqNmb,
                   InjctnTypCd,
                   LoanInjctnOthDescTxt,
                   LoanInjctnAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanInjctnFundTermYrNmb
              FROM LoanApp.LoanInjctnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanStbyAgrmtDtlTBL
        INSERT INTO LoanApp.LoanStbyAgrmtDtlTBL (LOANSTBYAGRMTDTLSEQNMB,
                                                 LOANAPPNMB,
                                                 LOANSTBYCRDTRNM,
                                                 LOANSTBYAMT,
                                                 LOANSTBYREPAYTYPCD,
                                                 LOANSTBYPYMTINTRT,
                                                 LOANSTBYPYMTOTHDESCTXT,
                                                 LOANSTBYPYMTAMT,
                                                 LOANSTBYPYMTSTRTDT,
                                                 CREATUSERID,
                                                 CREATDT,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
            SELECT LOANSTBYAGRMTDTLSEQNMB,
                   v_NEWLoanAppNmb,
                   LOANSTBYCRDTRNM,
                   LOANSTBYAMT,
                   LOANSTBYREPAYTYPCD,
                   LOANSTBYPYMTINTRT,
                   LOANSTBYPYMTOTHDESCTXT,
                   LOANSTBYPYMTAMT,
                   LOANSTBYPYMTSTRTDT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanStbyAgrmtDtlTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanSpcPurpsTbl
        INSERT INTO LoanApp.LoanSpcPurpsTbl (LoanAppNmb,
                                             LoanSpcPurpsSeqNmb,
                                             SpcPurpsLoanCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanSpcPurpsSeqNmb,
                   SpcPurpsLoanCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanSpcPurpsTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppARCRsnTbl
        INSERT INTO LoanApp.LoanAppARCRsnTbl (LoanAppNmb,
                                              ARCLoanRsnCd,
                                              ARCLoanRsnOthDescTxt,
                                              CreateUserId,
                                              CreateDt,
                                              LASTUPDTUSERID,
                                              LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   ARCLoanRsnCd,
                   ARCLoanRsnOthDescTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAppARCRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCrdtUnavRsnTbl
        INSERT INTO LoanApp.LoanCrdtUnavRsnTbl (LoanAppNmb,
                                                LoanCrdtUnavRsnSeqNmb,
                                                LoanCrdtUnavRsnCd,
                                                LoanCrdtUnavRsnTxt,
                                                LoanCrdtUnavRsnCreatUserId,
                                                LoanCrdtUnavRsnCreatDt,
                                                LastUpdtUserId,
                                                LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanCrdtUnavRsnSeqNmb,
                   LoanCrdtUnavRsnCd,
                   LoanCrdtUnavRsnTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanCrdtUnavRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanProcdTbl
        INSERT INTO LoanApp.LoanProcdTbl (LoanAppNmb,
                                          LoanProcdSeqNmb,
                                          LoanProcdTypCd,
                                          ProcdTypCd,
                                          LoanProcdOthTypTxt,
                                          LoanProcdAmt,
                                          LoanProcdCreatUserId,
                                          LoanProcdCreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LoanProcdRefDescTxt,
                                          LoanProcdPurAgrmtDt,
                                          LoanProcdPurIntangAssetAmt,
                                          LoanProcdPurIntangAssetDesc,
                                          LoanProcdPurStkHldrNm,
                                          NCAIncldInd,
                                          StkPurCorpText)
            SELECT v_NEWLoanAppNmb,
                   LoanProcdSeqNmb,
                   LoanProcdTypCd,
                   ProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanApp.LoanProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

   BEGIN
        --Copy to LOANAPP.LOANSUBPROCDTBL
        INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
            SELECT  LOANAPP.LOANSUBPROCDIDSEQ.nextval,
                    v_NEWLoanAppNmb,
                    LoanProcdTypCd,
                    PROCDTYPCD,
                    LOANPROCDTXTBLCK,
                    LoanProcdAmt,
		    LoanProcdAddr,
		    LOANSUBPROCDTYPCD,
		    LoanProcdDesc,	
		    Lender
              FROM LoanApp.LOANSUBPROCDTBL
             WHERE  LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppChngBusOwnrshpTbl
        INSERT INTO LoanApp.LoanAppChngBusOwnrshpTbl (
                        LOANAPPNMB,
                        TOTPYMTAMT,
                        LOAN7APYMTAMT,
                        SELLERFINANFULLSTBYAMT,
                        SELLERFINANNONFULLSTBYAMT,
                        BUYEREQTYCASHAMT,
                        BUYEREQTYCASHDESCTXT,
                        BUYEREQTYBORRAMT,
                        BUYEREQTYOTHAMT,
                        BUYEREQTYOTHDESCTXT,
                        TOTASSETAMT,
                        ACCTRECVASSETAMT,
                        INVTRYASSETAMT,
                        REASSETAMT,
                        REVALTYPIND,
                        EQUIPASSETAMT,
                        EQUIPVALTYPIND,
                        FIXASSETAMT,
                        INTANGASSETAMT,
                        OTHASSETAMT,
                        OTHASSETDESCTXT,
                        COVNTASSETAMT,
                        CUSTASSETAMT,
                        LICNSASSETAMT,
                        FRNCHSASSETAMT,
                        GOODWILLASSETAMT,
                        OTHINTANGASSETAMT,
                        OTHINTANGASSETDESCTXT,
                        TOTAPPRAMT,
                        BUSAPPRNM,
                        BUSAPPRFEEAMT,
                        BUSAPPRASAIND,
                        BUSAPPRCBAIND,
                        BUSAPPRABVIND,
                        BUSAPPRCVAIND,
                        BUSAPPRAVAIND,
                        BUSAPPRCPAIND,
                        BUSBRKRCOMISNIND,
                        BUSBRKRNM,
                        BUSBRKRCOMISNAMT,
                        BUSBRKRADR,
                        CREATUSERID,
                        CREATDT,
                        LASTUPDTUSERID,
                        LASTUPDTDT,
                        BUSAPPRLIVIND)
            SELECT v_NEWLoanAppNmb,
                   TOTPYMTAMT,
                   LOAN7APYMTAMT,
                   SELLERFINANFULLSTBYAMT,
                   SELLERFINANNONFULLSTBYAMT,
                   BUYEREQTYCASHAMT,
                   BUYEREQTYCASHDESCTXT,
                   BUYEREQTYBORRAMT,
                   BUYEREQTYOTHAMT,
                   BUYEREQTYOTHDESCTXT,
                   TOTASSETAMT,
                   ACCTRECVASSETAMT,
                   INVTRYASSETAMT,
                   REASSETAMT,
                   REVALTYPIND,
                   EQUIPASSETAMT,
                   EQUIPVALTYPIND,
                   FIXASSETAMT,
                   INTANGASSETAMT,
                   OTHASSETAMT,
                   OTHASSETDESCTXT,
                   COVNTASSETAMT,
                   CUSTASSETAMT,
                   LICNSASSETAMT,
                   FRNCHSASSETAMT,
                   GOODWILLASSETAMT,
                   OTHINTANGASSETAMT,
                   OTHINTANGASSETDESCTXT,
                   TOTAPPRAMT,
                   BUSAPPRNM,
                   BUSAPPRFEEAMT,
                   BUSAPPRASAIND,
                   BUSAPPRCBAIND,
                   BUSAPPRABVIND,
                   BUSAPPRCVAIND,
                   BUSAPPRAVAIND,
                   BUSAPPRCPAIND,
                   BUSBRKRCOMISNIND,
                   BUSBRKRNM,
                   BUSBRKRCOMISNAMT,
                   BUSBRKRADR,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   BUSAPPRLIVIND
              FROM LoanApp.LoanAppChngBusOwnrshpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPrtCmntTbl
        INSERT INTO LoanApp.LoanPrtCmntTbl (LoanAppNmb,
                                            LoanPrtCmntTxt,
                                            LoanPrtCmntCreatUserId,
                                            LoanPrtCmntCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanPrtCmntTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM loanApp.LoanPrtCmntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanEconDevObjctChldTbl
        INSERT INTO LoanApp.LoanEconDevObjctChldTbl (LoanEconDevObjctSeqNmb,
                                                     LoanAppNmb,
                                                     LoanEconDevObjctCd,
                                                     CreatUserId,
                                                     CreatDt,
                                                     LastUpdtUserId,
                                                     LastUpdtDt)
            SELECT LoanEconDevObjctSeqNmb,
                   v_NEWLoanAppNmb,
                   LoanEconDevObjctCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanEconDevObjctChldTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCollatTbl
        INSERT INTO LoanApp.LoanCollatTbl (loanappnmb,
                                           loancollatseqnmb,
                                           loancollattypcd,
                                           loancollatdesc,
                                           loancollatmrktvalamt,
                                           loancollatownrrecrd,
                                           loancollatsbalienpos,
                                           loancollatvalsourccd,
                                           loancollatvaldt,
                                           loancollatcreatuserid,
                                           loancollatcreatdt,
                                           lastupdtuserid,
                                           lastupdtdt,
                                           collatstatcd,
                                           collatsecsrchorddt,
                                           collatsecsrchrcvddt,
                                           collatapporddt,
                                           collatstr1nm,
                                           collatstr2nm,
                                           collatctynm,
                                           collatcntycd,
                                           collatstcd,
                                           collatzipcd,
                                           collatzip4cd,
                                           LoanCollatSubTypCd,
                                           SecurShrPariPassuInd,
                                           SecurShrPariPassuNonSBAInd,
                                           SecurPariPassuLendrNm,
                                           SecurPariPassuAmt,
                                           SecurLienLimAmt,
                                           SecurInstrmntTypCd,
                                           SecurWaterRightInd,
                                           SecurRentAsgnInd,
                                           SecurSellerNm,
                                           SecurPurNm,
                                           SecurOwedToSellerAmt,
                                           SecurCDCDeedInEscrowInd,
                                           SecurSellerIntDtlInd,
                                           SecurSellerIntDtlTxt,
                                           SecurTitlSubjPriorLienInd,
                                           SecurPriorLienTxt,
                                           SecurPriorLienLimAmt,
                                           SecurSubjPriorAsgnInd,
                                           SecurPriorAsgnTxt,
                                           SecurPriorAsgnLimAmt,
                                           SecurALTATltlInsurInd,
                                           SecurLeaseTermOverLoanYrNmb,
                                           LandlordIntProtWaiverInd,
                                           SecurDt,
                                           SecurLessorTermNotcDaysNmb,
                                           SecurDescTxt,
                                           SecurPropAcqWthLoanInd,
                                           SecurPropTypTxt,
                                           SecurOthPropTxt,
                                           SecurLienOnLqorLicnsInd,
                                           SecurMadeYrNmb,
                                           SecurLocTxt,
                                           SecurOwnerNm,
                                           SecurMakeNm,
                                           SecurAmt,
                                           SecurNoteSecurInd,
                                           SecurStkShrNmb,
                                           SecurLienHldrVrfyInd,
                                           SecurTitlVrfyTypCd,
                                           SecurTitlVrfyOthTxt,
                                           FloodInsurRqrdInd,
                                           REHazardInsurRqrdInd,
                                           PerHazardInsurRqrdInd,
                                           FullMarInsurRqrdInd,
                                           EnvInvstgtSBAAppInd,
                                           LeasePrmTypCd,
                                           ApprTypCd)
            SELECT v_NEWLoanAppNmb,
                   loancollatseqnmb,
                   loancollattypcd,
                   loancollatdesc,
                   loancollatmrktvalamt,
                   loancollatownrrecrd,
                   loancollatsbalienpos,
                   loancollatvalsourccd,
                   loancollatvaldt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   collatstatcd,
                   collatsecsrchorddt,
                   collatsecsrchrcvddt,
                   collatapporddt,
                   collatstr1nm,
                   collatstr2nm,
                   collatctynm,
                   collatcntycd,
                   collatstcd,
                   collatzipcd,
                   collatzip4cd,
                   LoanCollatSubTypCd,
                   SecurShrPariPassuInd,
                   SecurShrPariPassuNonSBAInd,
                   SecurPariPassuLendrNm,
                   SecurPariPassuAmt,
                   SecurLienLimAmt,
                   SecurInstrmntTypCd,
                   SecurWaterRightInd,
                   SecurRentAsgnInd,
                   SecurSellerNm,
                   SecurPurNm,
                   SecurOwedToSellerAmt,
                   SecurCDCDeedInEscrowInd,
                   SecurSellerIntDtlInd,
                   SecurSellerIntDtlTxt,
                   SecurTitlSubjPriorLienInd,
                   SecurPriorLienTxt,
                   SecurPriorLienLimAmt,
                   SecurSubjPriorAsgnInd,
                   SecurPriorAsgnTxt,
                   SecurPriorAsgnLimAmt,
                   SecurALTATltlInsurInd,
                   SecurLeaseTermOverLoanYrNmb,
                   LandlordIntProtWaiverInd,
                   SecurDt,
                   SecurLessorTermNotcDaysNmb,
                   SecurDescTxt,
                   SecurPropAcqWthLoanInd,
                   SecurPropTypTxt,
                   SecurOthPropTxt,
                   SecurLienOnLqorLicnsInd,
                   SecurMadeYrNmb,
                   SecurLocTxt,
                   SecurOwnerNm,
                   SecurMakeNm,
                   SecurAmt,
                   SecurNoteSecurInd,
                   SecurStkShrNmb,
                   SecurLienHldrVrfyInd,
                   SecurTitlVrfyTypCd,
                   SecurTitlVrfyOthTxt,
                   FloodInsurRqrdInd,
                   REHazardInsurRqrdInd,
                   PerHazardInsurRqrdInd,
                   FullMarInsurRqrdInd,
                   EnvInvstgtSBAAppInd,
                   LeasePrmTypCd,
                   ApprTypCd
              FROM LoanApp.LoanCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

        --Copy to LoanApp.LoanCollatLienTbl
        INSERT INTO LoanApp.LoanCollatLienTbl (LoanAppNmb,
                                               LoanCollatSeqNmb,
                                               LoanCollatLienSeqNmb,
                                               LoanCollatLienHldrNm,
                                               LoanCollatLienBalAmt,
                                               LoanCollatLienPos,
                                               LoanCollatLienCreatUserId,
                                               LoanCollatLienCreatDt,
                                               LastUpdtUserId,
                                               LastUpdtDt,
                                               LOANCOLLATLIENSTATCD,
                                               LOANCOLLATLIENCMNT)
            SELECT v_NEWLoanAppNmb,
                   LoanCollatSeqNmb,
                   LoanCollatLienSeqNmb,
                   LoanCollatLienHldrNm,
                   LoanCollatLienBalAmt,
                   LoanCollatLienPos,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANCOLLATLIENSTATCD,
                   LOANCOLLATLIENCMNT
              FROM LoanCollatLienTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPartLendrTbl
        INSERT INTO LoanApp.LoanPartLendrTbl (LoanAppNmb,
                                              LoanPartLendrSeqNmb,
                                              LoanPartLendrTypCd,
                                              LoanPartLendrAmt,
                                              LocId,
                                              LoanPartLendrNm,
                                              LoanPartLendrStr1Nm,
                                              LoanPartLendrStr2Nm,
                                              LoanPartLendrCtyNm,
                                              LoanPartLendrStCd,
                                              LoanPartLendrZip5Cd,
                                              LoanPartLendrZip4Cd,
                                              LoanPartLendrOfcrLastNm,
                                              LoanPartLendrOfcrFirstNm,
                                              LoanPartLendrOfcrInitialNm,
                                              LoanPartLendrOfcrSfxNm,
                                              LoanPartLendrOfcrTitlTxt,
                                              LoanPartLendrOfcrPhnNmb,
                                              LoanLienPosCd,
                                              LoanPartLendrSrLienPosFeeAmt,
                                              LoanPoolAppNmb,
                                              LOANLENDRTAXID,
                                              LOANLENDRSERVFEEPCT,
                                              LOANLENDRPARTPCT,
                                              LOANORIGPARTPCT,
                                              LoanSBAGntyBalPct,
                                              LOANLENDRPARTAMT,
                                              LOANORIGPARTAMT,
                                              LOANLENDRGROSSAMT,
                                              LOANSBAGNTYBALAMT,
                                              Loan504PartLendrSeqNmb,
                                              LoanPartLendrCreatUserId,
                                              LoanPartLendrCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              loanpartlendrcntrycd,
                                              loanpartlendrpostcd,
                                              loanpartlendrstnm)
            SELECT v_NEWLoanAppNmb,
                   LoanPartLendrSeqNmb,
                   LoanPartLendrTypCd,
                   LoanPartLendrAmt,
                   LocId,
                   LoanPartLendrNm,
                   LoanPartLendrStr1Nm,
                   LoanPartLendrStr2Nm,
                   LoanPartLendrCtyNm,
                   LoanPartLendrStCd,
                   LoanPartLendrZip5Cd,
                   LoanPartLendrZip4Cd,
                   LoanPartLendrOfcrLastNm,
                   LoanPartLendrOfcrFirstNm,
                   LoanPartLendrOfcrInitialNm,
                   LoanPartLendrOfcrSfxNm,
                   LoanPartLendrOfcrTitlTxt,
                   LoanPartLendrOfcrPhnNmb,
                   LoanLienPosCd,
                   LoanPartLendrSrLienPosFeeAmt,
                   LoanPoolAppNmb,
                   LOANLENDRTAXID,
                   LOANLENDRSERVFEEPCT,
                   LOANLENDRPARTPCT,
                   LOANORIGPARTPCT,
                   LoanSBAGntyBalPct,
                   LOANLENDRPARTAMT,
                   LOANORIGPARTAMT,
                   LOANLENDRGROSSAMT,
                   LOANSBAGNTYBALAMT,
                   Loan504PartLendrSeqNmb,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   loanpartlendrcntrycd,
                   loanpartlendrpostcd,
                   loanpartlendrstnm
              FROM LoanApp.LoanPartLendrTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    --Copy to LoanApp.LoanBusTbl
    INSERT INTO LoanApp.LoanBusTbl (LoanAppNmb,
                                    TaxId,
                                    BusEINCertInd,
                                    BusTypCd,
                                    BusNm,
                                    BusSrchNm,
                                    BusTrdNm,
                                    BusBnkrptInd,
                                    BusLwsuitInd,
                                    BusPriorSBALoanInd,
                                    BusCurBnkNm,
                                    BusChkngBalAmt,
                                    BusCurOwnrshpDt,
                                    BusPrimPhnNmb,
                                    BusPhyAddrStr1Nm,
                                    BusPhyAddrStr2Nm,
                                    BusPhyAddrCtyNm,
                                    BusPhyAddrStCd,
                                    BusPhyAddrStNm,
                                    BusPhyAddrCntCd,
                                    BusPhyAddrZipCd,
                                    BusPhyAddrZip4Cd,
                                    BusPhyAddrPostCd,
                                    BusMailAddrStr1Nm,
                                    BusMailAddrStr2Nm,
                                    BusMailAddrCtyNm,
                                    BusMailAddrStCd,
                                    BusMailAddrStNm,
                                    BusMailAddrCntCd,
                                    BusMailAddrZipCd,
                                    BusMailAddrZip4Cd,
                                    BusMailAddrPostCd,
                                    BusCreatUserId,
                                    BusCreatDt,
                                    BusExprtInd,
                                    BusExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    BusExtrnlCrdtScorNmb,
                                    BusExtrnlCrdtScorDt,
                                    BusDunsNMB,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    BusAltPhnNmb,
                                    BusPrimEmailAdr,
                                    BusAltEmailAdr,
                                    BusCSP60DayDelnqInd,
                                    BusLglActnInd,
                                    BusFedAgncySDPIEInd,
                                    BusSexNatrInd,
                                    BusNonFmrSBAEmpInd,
                                    BusNonLegBrnchEmpInd,
                                    BusNonFedEmpInd,
                                    BusNonGS13EmpInd,
                                    BusNonSBACEmpInd,
                                    BusPrimCntctNm,
                                    -- BusPrimCntctEmail,
                                    BusFedAgncyLoanInd,
                                    BusOutDbtInd)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.BusEINCertInd, a.BusEINCertInd),
               NVL (b.BusTypCd, a.BusTypCd),
               NVL (b.BusNm, a.BusNm),
               NVL (b.BusSrchNm, a.BusSrchNm),
               a.BusTrdNm,
               a.BusBnkrptInd,
               a.BusLwsuitInd,
               a.BusPriorSBALoanInd,
               a.BusCurBnkNm,
               a.BusChkngBalAmt,
               a.BusCurOwnrshpDt,
               a.BusPrimPhnNmb,
               a.BusPhyAddrStr1Nm,
               a.BusPhyAddrStr2Nm,
               a.BusPhyAddrCtyNm,
               a.BusPhyAddrStCd,
               a.BusPhyAddrStNm,
               a.BusPhyAddrCntCd,
               a.BusPhyAddrZipCd,
               a.BusPhyAddrZip4Cd,
               a.BusPhyAddrPostCd,
               a.BusMailAddrStr1Nm,
               a.BusMailAddrStr2Nm,
               a.BusMailAddrCtyNm,
               a.BusMailAddrStCd,
               a.BusMailAddrStNm,
               a.BusMailAddrCntCd,
               a.BusMailAddrZipCd,
               a.BusMailAddrZip4Cd,
               a.BusMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.BusExprtInd,
               a.BusExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.BusExtrnlCrdtScorNmb,
               a.BusExtrnlCrdtScorDt,
               a.BusDunsNMB,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               b.VETCERTIND,
               a.BusAltPhnNmb,
               a.BusPrimEmailAdr,
               a.BusAltEmailAdr,
               a.BusCSP60DayDelnqInd,
               a.BusLglActnInd,
               a.BusFedAgncySDPIEInd,
               a.BusSexNatrInd,
               a.BusNonFmrSBAEmpInd,
               a.BusNonLegBrnchEmpInd,
               a.BusNonFedEmpInd,
               a.BusNonGS13EmpInd,
               a.BusNonSBACEmpInd,
               a.BusPrimCntctNm,
               --   BusPrimCntctEmail,
               a.BusFedAgncyLoanInd,
               a.BusOutDbtInd
          FROM LoanApp.LoanBusTbl a, loan.Bustbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.taxid = b.taxid(+);


    --Copy to LoanApp.LoanPerTbl
    INSERT INTO LoanApp.LoanPerTbl (LoanAppNmb,
                                    TaxId,
                                    PerTAXIDCertInd,
                                    PerFirstNm,
                                    PerFirstSrchNm,
                                    PerLastNm,
                                    PerLastSrchNm,
                                    PerInitialNm,
                                    PerSfxNm,
                                    PerTitlTxt,
                                    PerBrthDt,
                                    PerBrthCtyNm,
                                    PerBrthStCd,
                                    PerBrthCntCd,
                                    PerBrthCntNm,
                                    PerUSCitznInd,
                                    PerAlienRgstrtnNmb,
                                    PerPndngLwsuitInd,
                                    PerAffilEmpFedInd,
                                    PerIOBInd,
                                    PerIndctPrleProbatnInd,
                                    PerCrmnlOffnsInd,
                                    PerCnvctInd,
                                    PerBnkrptcyInd,
                                    PerFngrprntWaivDt,
                                    PerPrimPhnNmb,
                                    PerPhyAddrStr1Nm,
                                    PerPhyAddrStr2Nm,
                                    PerPhyAddrCtyNm,
                                    PerPhyAddrStCd,
                                    PerPhyAddrStNm,
                                    PerPhyAddrCntCd,
                                    PerPhyAddrZipCd,
                                    PerPhyAddrZip4Cd,
                                    PerPhyAddrPostCd,
                                    PerMailAddrStr1Nm,
                                    PerMailAddrStr2Nm,
                                    PerMailAddrCtyNm,
                                    PerMailAddrStCd,
                                    PerMailAddrStNm,
                                    PerMailAddrCntCd,
                                    PerMailAddrZipCd,
                                    PerMailAddrZip4Cd,
                                    PerMailAddrPostCd,
                                    PerCreatUserId,
                                    PerCreatDt,
                                    PerExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    PerExtrnlCrdtScorNmb,
                                    PerExtrnlCrdtScorDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    PerFedAgncySDPIEInd,
                                    PerCSP60DayDelnqInd,
                                    PerLglActnInd,
                                    PerCitznShpCntNm,
                                    PerAltPhnNmb,
                                    PerPrimEmailAdr,
                                    PerAltEmailAdr,
                                    PerFedAgncyLoanInd,
                                    PerOthrCmntTxt)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.PerTAXIDCertInd, a.PerTAXIDCertInd),
               NVL (b.PerFirstNm, a.PerFirstNm),
               NVL (b.PerFirstSrchNm, a.PerFirstSrchNm),
               NVL (b.PerLastNm, a.PerLastNm),
               NVL (b.PerLastSrchNm, a.PerLastSrchNm),
               NVL (b.PerInitialNm, a.PerInitialNm),
               NVL (b.PerSfxNm, a.PerSfxNm),
               a.PerTitlTxt,
               NVL (b.PerBrthDt, a.PerBrthDt),
               NVL (b.PerBrthCtyNm, a.PerBrthCtyNm),
               NVL (b.PerBrthStCd, a.PerBrthStCd),
               NVL (b.PerBrthCntCd, a.PerBrthCntCd),
               NVL (b.PerBrthCntNm, b.PerBrthCntNm),
               a.PerUSCitznInd,
               a.PerAlienRgstrtnNmb,
               a.PerPndngLwsuitInd,
               a.PerAffilEmpFedInd,
               a.PerIOBInd,
               a.PerIndctPrleProbatnInd,
               a.PerCrmnlOffnsInd,
               a.PerCnvctInd,
               a.PerBnkrptcyInd,
               a.PerFngrprntWaivDt,
               a.PerPrimPhnNmb,
               a.PerPhyAddrStr1Nm,
               a.PerPhyAddrStr2Nm,
               a.PerPhyAddrCtyNm,
               a.PerPhyAddrStCd,
               a.PerPhyAddrStNm,
               a.PerPhyAddrCntCd,
               a.PerPhyAddrZipCd,
               a.PerPhyAddrZip4Cd,
               a.PerPhyAddrPostCd,
               a.PerMailAddrStr1Nm,
               a.PerMailAddrStr2Nm,
               a.PerMailAddrCtyNm,
               a.PerMailAddrStCd,
               a.PerMailAddrStNm,
               a.PerMailAddrCntCd,
               a.PerMailAddrZipCd,
               a.PerMailAddrZip4Cd,
               a.PerMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.PerExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.PerExtrnlCrdtScorNmb,
               a.PerExtrnlCrdtScorDt,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (b.VETCERTIND, a.VETCERTIND),
               a.PerFedAgncySDPIEInd,
               a.PerCSP60DayDelnqInd,
               a.PerLglActnInd,
               a.PerCitznShpCntNm,
               a.PerAltPhnNmb,
               a.PerPrimEmailAdr,
               a.PerAltEmailAdr,
               a.PerFedAgncyLoanInd,
               a.PerOthrCmntTxt
          FROM loanapp.loanpertbl a, loan.pertbl b
         WHERE a.taxid = b.taxid(+) AND a.loanappnmb = p_loanappnmb;


    BEGIN
        --Copy to LoanApp.LoanIndbtnesTbl
        INSERT INTO LoanApp.LoanIndbtnesTbl (LoanAppNmb,
                                             TaxId,
                                             LoanIndbtnesSeqNmb,
                                             LoanIndbtnesPayblToNm,
                                             LoanIndbtnesPurpsTxt,
                                             LoanIndbtnesOrglDt,
                                             LoanIndbtnesCurBalAmt,
                                             LoanIndbtnesIntPct,
                                             LoanIndbtnesMatDt,
                                             LoanIndbtnesPymtAmt,
                                             LoanIndbtnesPymtFreqCd,
                                             LoanIndbtnesCollatDescTxt,
                                             LoanIndbtnesPrevFinanStatCd,
                                             LoanIndbtnesCreatUserId,
                                             LoanIndbtnesCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanIndbtnesSeqNmb,
                   LoanIndbtnesPayblToNm,
                   LoanIndbtnesPurpsTxt,
                   LoanIndbtnesOrglDt,
                   LoanIndbtnesCurBalAmt,
                   LoanIndbtnesIntPct,
                   LoanIndbtnesMatDt,
                   LoanIndbtnesPymtAmt,
                   LoanIndbtnesPymtFreqCd,
                   LoanIndbtnesCollatDescTxt,
                   LoanIndbtnesPrevFinanStatCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanIndbtnesTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPerFinanTbl
        INSERT INTO LoanApp.LoanPerFinanTbl (LoanAppNmb,
                                             TaxId,
                                             PerLqdAssetAmt,
                                             PerBusOwnrshpAmt,
                                             PerREAmt,
                                             PerOthAssetAmt,
                                             PerTotAssetAmt,
                                             PerRELiabAmt,
                                             PerCCDbtAmt,
                                             PerInstlDbtAmt,
                                             PerOthLiabAmt,
                                             PerTotLiabAmt,
                                             PerNetWrthAmt,
                                             PerAnnSalaryAmt,
                                             PerOthAnnIncAmt,
                                             PerSourcOfOthIncTxt,
                                             PerResOwnRentOthInd,
                                             PerMoHsngAmt,
                                             PerCreatUserId,
                                             PerCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   PerLqdAssetAmt,
                   PerBusOwnrshpAmt,
                   PerREAmt,
                   PerOthAssetAmt,
                   PerTotAssetAmt,
                   PerRELiabAmt,
                   PerCCDbtAmt,
                   PerInstlDbtAmt,
                   PerOthLiabAmt,
                   PerTotLiabAmt,
                   PerNetWrthAmt,
                   PerAnnSalaryAmt,
                   PerOthAnnIncAmt,
                   PerSourcOfOthIncTxt,
                   PerResOwnRentOthInd,
                   PerMoHsngAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPerFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanFedEmpTbl
        INSERT INTO LOANAPP.LoanFedEmpTbl (LoanAppNmb,
                                           TaxId,
                                           LoanFedEmpSeqNmb,
                                           FedEmpFirstNm,
                                           FedEmpLastNm,
                                           FedEmpInitialNm,
                                           FedEmpSfxNm,
                                           FedEmpAddrStr1Nm,
                                           FedEmpAddrStr2Nm,
                                           FedEmpAddrCtyNm,
                                           FedEmpAddrStCd,
                                           FedEmpAddrZipCd,
                                           FedEmpAddrZip4Cd,
                                           FedEmpAgncyOfc,
                                           FedEmpCreatUserId,
                                           FedEmpCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanFedEmpSeqNmb,
                   FedEmpFirstNm,
                   FedEmpLastNm,
                   FedEmpInitialNm,
                   FedEmpSfxNm,
                   FedEmpAddrStr1Nm,
                   FedEmpAddrStr2Nm,
                   FedEmpAddrCtyNm,
                   FedEmpAddrStCd,
                   FedEmpAddrZipCd,
                   FedEmpAddrZip4Cd,
                   FedEmpAgncyOfc,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanFedEmpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.Pertbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'P'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.bustbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'B'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;



    BEGIN
        --Copy to LoanApp.LoanPrinTbl */
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.pertbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'P'
                   AND a.taxid = b.Taxid(+);
    END;

    BEGIN
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.bustbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'B'
                   AND a.taxid = b.Taxid(+);
    END;



    BEGIN
        --Copy to LoanApp.LoanBusPrinTbl
        INSERT INTO LoanApp.LoanBusPrinTbl (LoanAppNmb,
                                            BorrSeqNmb,
                                            PrinSeqNmb,
                                            LoanBusPrinPctOwnrshpBus,
                                            LoanPrinGntyInd,
                                            LoanBusPrinCreatUserId,
                                            LoanBusPrinCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            LoanCntlIntInd,
                                            LoanCntlIntTyp)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   PrinSeqNmb,
                   LoanBusPrinPctOwnrshpBus,
                   LoanPrinGntyInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanCntlIntInd,
                   LoanCntlIntTyp
              FROM LoanApp.LoanBusPrinTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.perracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'P'
               AND a.taxid = b.taxid;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.busracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'B'
               AND a.taxid = b.taxid;

    --Copy to LoanApp.LoanPrinRaceTbl
    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               RaceCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinRaceTbl a
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND NOT EXISTS
                       (SELECT 1
                          FROM loanapp.LoanPrinRaceTbl z
                         WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                               AND z.PRINSEQNMB = a.PrinSeqNmb);


    BEGIN
        --Copy to LoanApp.LoanBorrRaceTbl
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrTbl a, loan.perracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'P'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanborrTbl a, loan.busracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'B'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrRaceTbl a
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND NOT EXISTS
                           (SELECT 1
                              FROM loanapp.LoanBorrRaceTbl z
                             WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                                   AND z.BorrSEQNMB = a.BorrSeqNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanPrevFinanTbl
        INSERT INTO LoanApp.LoanPrevFinanTbl (LoanAppNmb,
                                              BorrSeqNmb,
                                              LoanPrevFinanSeqNmb,
                                              LoanPrevFinanStatCd,
                                              LoanPrevFinanAgencyNm,
                                              LoanPrevFinanLoanNmb,
                                              LoanPrevFinanAppvDt,
                                              LoanPrevFinanTotAmt,
                                              LoanPrevFinanBalAmt,
                                              LoanPrevFinanCreatUserId,
                                              LoanPrevFinanCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              LoanPrevFinanDelnqCmntTxt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   LoanPrevFinanSeqNmb,
                   LoanPrevFinanStatCd,
                   LoanPrevFinanAgencyNm,
                   LoanPrevFinanLoanNmb,
                   LoanPrevFinanAppvDt,
                   LoanPrevFinanTotAmt,
                   LoanPrevFinanBalAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanPrevFinanDelnqCmntTxt
              FROM LoanApp.LoanPrevFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanGuarTbl
        INSERT INTO LoanApp.LoanGuarTbl (LoanAppNmb,
                                         GuarSeqNmb,
                                         TaxId,
                                         GuarBusPerInd,
                                         LoanGuarOperCoInd,
                                         GuarCreatUserId,
                                         GuarCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         LACGntyCondTypCd,
                                         GntySubCondTypCd,
                                         GntyLmtAmt,
                                         GntyLmtPct,
                                         LoanCollatSeqNmb,
                                         GntyLmtYrNmb,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdInd,
                                         WorkrsCompInsRqrdInd,
                                         OthInsurDescTxt)
            SELECT v_NEWLoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LACGntyCondTypCd,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdInd,
                   WorkrsCompInsRqrdInd,
                   OthInsurDescTxt
              FROM LoanApp.LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanLmtGntyCollatTbl
        INSERT INTO LoanApp.LoanLmtGntyCollatTbl (LOANAPPNMB,
                                                  TAXID,
                                                  BUSPERIND,
                                                  LOANCOLLATSEQNMB,
                                                  CREATUSERID,
                                                  CREATDT,
                                                  LASTUPDTUSERID,
                                                  LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   TAXID,
                   BUSPERIND,
                   LOANCOLLATSEQNMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanLmtGntyCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanIntDtlTbl
        INSERT INTO LoanApp.LoanIntDtlTbl (LOANAPPNMB,
                                           LOANINTDTLSEQNMB,
                                           LOANINTLOANPARTPCT,
                                           LOANINTLOANPARTMONMB,
                                           LOANINTFIXVARIND,
                                           LOANINTRT,
                                           IMRTTYPCD,
                                           LOANINTBASERT,
                                           LOANINTSPRDOVRPCT,
                                           LOANINTADJPRDCD,
                                           LOANINTADJPRDMONMB,
                                           CREATUSERID,
                                           CREATDT,
                                           LASTUPDTUSERID,
                                           LASTUPDTDT,
                                           LOANINTADJPRDEFFDT,
                                           LoanIntDtlGuarInd)
            SELECT v_NEWLoanAppNmb,
                   LOANINTDTLSEQNMB,
                   LOANINTLOANPARTPCT,
                   LOANINTLOANPARTMONMB,
                   LOANINTFIXVARIND,
                   LOANINTRT,
                   IMRTTYPCD,
                   LOANINTBASERT,
                   LOANINTSPRDOVRPCT,
                   LOANINTADJPRDCD,
                   LOANINTADJPRDMONMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANINTADJPRDEFFDT,
                   LoanIntDtlGuarInd
              FROM LoanApp.LoanIntDtlTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanExprtCntryTbl
        INSERT INTO LoanApp.LoanExprtCntryTbl (LOANAPPNMB,
                                               LOANEXPRTCNTRYCD,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT,
                                               LOANEXPRTCNTRYSEQNMB)
            SELECT v_NEWLoanAppNmb,
                   LOANEXPRTCNTRYCD,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANEXPRTCNTRYSEQNMB
              FROM LoanApp.LoanExprtCntryTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBSTbl
        INSERT INTO LoanApp.LoanBSTbl (LoanAppNmb,
                                       LoanBSDt,
                                       LoanBSCashEqvlntAmt,
                                       LoanBSNetTrdRecvAmt,
                                       LoanBSTotInvtryAmt,
                                       LoanBSOthCurAssetAmt,
                                       LoanBSTotCurAssetAmt,
                                       LoanBSTotFixAssetAmt,
                                       LoanBSTotOthAssetAmt,
                                       LoanBSTotAssetAmt,
                                       LoanBSAcctsPayblAmt,
                                       LoanBSCurLTDAmt,
                                       LoanBSOthCurLiabAmt,
                                       LoanBSTotCurLiabAmt,
                                       LoanBSLTDAmt,
                                       LoanBSOthLTLiabAmt,
                                       LoanBSStbyDbt,
                                       LoanBSTotLiab,
                                       LoanBSBusNetWrth,
                                       LoanBSTngblNetWrth,
                                       LoanBSActlPrfrmaInd,
                                       LoanFinanclStmtSourcCd,
                                       LoanBSCreatUserId,
                                       LoanBSCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanBSDt,
                   LoanBSCashEqvlntAmt,
                   LoanBSNetTrdRecvAmt,
                   LoanBSTotInvtryAmt,
                   LoanBSOthCurAssetAmt,
                   LoanBSTotCurAssetAmt,
                   LoanBSTotFixAssetAmt,
                   LoanBSTotOthAssetAmt,
                   LoanBSTotAssetAmt,
                   LoanBSAcctsPayblAmt,
                   LoanBSCurLTDAmt,
                   LoanBSOthCurLiabAmt,
                   LoanBSTotCurLiabAmt,
                   LoanBSLTDAmt,
                   LoanBSOthLTLiabAmt,
                   LoanBSStbyDbt,
                   LoanBSTotLiab,
                   LoanBSBusNetWrth,
                   LoanBSTngblNetWrth,
                   LoanBSActlPrfrmaInd,
                   LoanFinanclStmtSourcCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBSTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanISTbl
        INSERT INTO LoanApp.LoanISTbl (LoanAppNmb,
                                       LoanISSeqNmb,
                                       LoanISNetSalesRevnuAmt,
                                       LoanISCostSalesAmt,
                                       LoanISGrsProftAmt,
                                       LoanISOwnrSalaryAmt,
                                       LoanISDprctAmortAmt,
                                       LoanISNetIncAmt,
                                       LoanISOperProftAmt,
                                       LoanISAnnIntExpnAmt,
                                       LoanISNetIncBefTaxWthdrlAmt,
                                       LoanISIncTaxAmt,
                                       LoanISCshflwAmt,
                                       LoanFinanclStmtSourcCd,
                                       LoanISBegnDt,
                                       LoanISEndDt,
                                       LoanISCreatUserId,
                                       LoanISCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanISSeqNmb,
                   LoanISNetSalesRevnuAmt,
                   LoanISCostSalesAmt,
                   LoanISGrsProftAmt,
                   LoanISOwnrSalaryAmt,
                   LoanISDprctAmortAmt,
                   LoanISNetIncAmt,
                   LoanISOperProftAmt,
                   LoanISAnnIntExpnAmt,
                   LoanISNetIncBefTaxWthdrlAmt,
                   LoanISIncTaxAmt,
                   LoanISCshflwAmt,
                   LoanFinanclStmtSourcCd,
                   LoanISBegnDt,
                   LoanISEndDt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanISTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppPrtTbl
        INSERT INTO LOANAPP.LoanAppPrtTbl (LoanAppNmb,
                                           LocId,
                                           LoanAppFIRSNmb,
                                           PrtId,
                                           LoanAppPrtAppNmb,
                                           LoanAppPrtLoanNmb,
                                           LoanAppPrtNm,
                                           LoanAppPrtStr1,
                                           LoanAppPrtStr2,
                                           LoanAppPrtCtyNm,
                                           LoanAppPrtStCd,
                                           LoanAppPrtZipCd,
                                           LoanAppPrtZip4Cd,
                                           LoanAppCntctLastNm,
                                           LoanAppCntctFirstNm,
                                           LoanAppCntctInitialNm,
                                           LoanAppCntctSfxNm,
                                           LoanAppCntctTitlTxt,
                                           LoanAppCntctPhn,
                                           LoanAppCntctFax,
                                           LoanAppCntctemail,
                                           LoanPckgSourcTypCd,
                                           LoanAppPckgSourcNm,
                                           LoanAppPckgSourcStr1Nm,
                                           LoanAppPckgSourcStr2Nm,
                                           LoanAppPckgSourcCtyNm,
                                           LoanAppPckgSourcStCd,
                                           LoanAppPckgSourcZipCd,
                                           LoanAppPckgSourcZip4Cd,
                                           ACHRtngNmb,
                                           ACHAcctNmb,
                                           ACHAcctTypCd,
                                           ACHTinNmb,
                                           LoanAppPrtTaxId,
                                           LoanAppLSPHQLocId,
                                           LoanAppPrtCreatUserId,
                                           LoanAppPrtCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanAgntInvlvdInd,
                                           --    LoanAppLspLastNm,
                                           --     LoanAppLspFirstNm,
                                           --     LoanAppLspInitialNm,
                                           LoanAppLspSfxNm,
                                           --    LoanAppLspTitlTxt,
                                           --    LoanAppLspPhn,
                                           LoanAppLspFax,
                                           --    LoanAppLspEMail,
                                           LoanPrtCntctCellPhn,
                                           LoanPrtAltCntctFirstNm,
                                           LoanPrtAltCntctLastNm,
                                           LoanPrtAltCntctInitialNm,
                                           LoanPrtAltCntctTitlTxt,
                                           LoanPrtAltCntctPrimPhn,
                                           LoanPrtAltCntctCellPhn,
                                           LoanPrtAltCntctemail,
                                           LendrAltCntctTypCd)
            SELECT v_NEWLoanAppNmb,
                   LocId,
                   LoanAppFIRSNmb,
                   PrtId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   LoanAppPrtNm,
                   LoanAppPrtStr1,
                   LoanAppPrtStr2,
                   LoanAppPrtCtyNm,
                   LoanAppPrtStCd,
                   LoanAppPrtZipCd,
                   LoanAppPrtZip4Cd,
                   LoanAppCntctLastNm,
                   LoanAppCntctFirstNm,
                   LoanAppCntctInitialNm,
                   LoanAppCntctSfxNm,
                   LoanAppCntctTitlTxt,
                   LoanAppCntctPhn,
                   LoanAppCntctFax,
                   LoanAppCntctemail,
                   LoanPckgSourcTypCd,
                   LoanAppPckgSourcNm,
                   LoanAppPckgSourcStr1Nm,
                   LoanAppPckgSourcStr2Nm,
                   LoanAppPckgSourcCtyNm,
                   LoanAppPckgSourcStCd,
                   LoanAppPckgSourcZipCd,
                   LoanAppPckgSourcZip4Cd,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppPrtTaxId,
                   LoanAppLSPHQLocId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanAgntInvlvdInd,
                   --   LoanAppLspLastNm,
                   --   LoanAppLspFirstNm,
                   --   LoanAppLspInitialNm,
                   LoanAppLspSfxNm,
                   --   LoanAppLspTitlTxt,
                   --    LoanAppLspPhn,
                   LoanAppLspFax,
                   --   LoanAppLspEMail,
                   LoanPrtCntctCellPhn,
                   LoanPrtAltCntctFirstNm,
                   LoanPrtAltCntctLastNm,
                   LoanPrtAltCntctInitialNm,
                   LoanPrtAltCntctTitlTxt,
                   LoanPrtAltCntctPrimPhn,
                   LoanPrtAltCntctCellPhn,
                   LoanPrtAltCntctemail,
                   LendrAltCntctTypCd
              FROM LoanApp.LoanAppPrtTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPymntTbl
        INSERT INTO LoanApp.LoanPymntTbl (LOANAPPNMB,
                                          PYMNTDAYNMB,
                                          PYMNTBEGNMONMB,
                                          ESCROWACCTRQRDIND,
                                          NETEARNCLAUSEIND,
                                          ARMMARTYPIND,
                                          ARMMARCEILRT,
                                          ARMMARFLOORRT,
                                          STINTRTREDUCTNIND,
                                          LATECHGIND,
                                          LATECHGAFTDAYNMB,
                                          LATECHGFEEPCT,
                                          PYMNTINTONLYBEGNMONMB,
                                          PYMNTINTONLYFREQCD,
                                          PYMNTINTONLYDAYNMB,
                                          NetEarnPymntPct,
                                          NetEarnPymntOverAmt,
                                          StIntRtReductnPrgmNm,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   PYMNTDAYNMB,
                   PYMNTBEGNMONMB,
                   ESCROWACCTRQRDIND,
                   NETEARNCLAUSEIND,
                   ARMMARTYPIND,
                   ARMMARCEILRT,
                   ARMMARFLOORRT,
                   STINTRTREDUCTNIND,
                   LATECHGIND,
                   LATECHGAFTDAYNMB,
                   LATECHGFEEPCT,
                   PYMNTINTONLYBEGNMONMB,
                   PYMNTINTONLYFREQCD,
                   PYMNTINTONLYDAYNMB,
                   NetEarnPymntPct,
                   NetEarnPymntOverAmt,
                   StIntRtReductnPrgmNm,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPymntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO Loanapp.LoanAgntTbl (LOANAPPNMB,
                                         LoanAgntSeqNmb,
                                         LOANAGNTBUSPERIND,
                                         LOANAGNTNM,
                                         LOANAGNTCNTCTFIRSTNM,
                                         LOANAGNTCNTCTMIDNM,
                                         LOANAGNTCNTCTLASTNM,
                                         LOANAGNTCNTCTSFXNM,
                                         LOANAGNTADDRSTR1NM,
                                         LOANAGNTADDRSTR2NM,
                                         LOANAGNTADDRCTYNM,
                                         LOANAGNTADDRSTCD,
                                         LOANAGNTADDRSTNM,
                                         LOANAGNTADDRZIPCD,
                                         LOANAGNTADDRZIP4CD,
                                         LOANAGNTADDRPOSTCD,
                                         LOANAGNTADDRCNTCD,
                                         LOANAGNTTYPCD,
                                         LOANAGNTDOCUPLDIND,
                                         LOANCDCTPLFEEIND,
                                         LOANCDCTPLFEEAMT,
                                         LOANAGNTID,
                                         CreatUserId,
                                         CreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM,
                   LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LoanAgntId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAgntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO LOANAPP.LOANAGNTFEEDTLTBL (LOANAPPNMB,
                                               LoanAgntSeqNmb,
                                               LOANAGNTSERVTYPCD,
                                               LOANAGNTSERVOTHTYPTXT,
                                               LOANAGNTAPPCNTPAIDAMT,
                                               LOANAGNTSBALENDRPAIDAMT,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   LoanAGNTSeqNmb,
                   LOANAGNTSERVTYPCD,
                   LOANAGNTSERVOTHTYPTXT,
                   LOANAGNTAPPCNTPAIDAMT,
                   LOANAGNTSBALENDRPAIDAMT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LOANAPP.LOANAGNTFEEDTLTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    p_NEWLoanAppNmb := v_NEWLoanAppNmb;
END LOANCOPYCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPUPDCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPUPDCSP (p_Identifier                         NUMBER := NULL,
                                                   p_RetVal                         OUT NUMBER,
                                                   p_LoanAppNmb                         NUMBER := 0,
                                                   p_LoanAppNm                          VARCHAR2 := NULL,
                                                   p_LoanAppRqstAmt                     NUMBER := NULL,
                                                   p_LoanAppSBAGntyPct                  NUMBER := NULL,
                                                   p_LoanAppSBARcmndAmt                 NUMBER := NULL,
                                                   p_LoanAppRqstMatMoQty                NUMBER := NULL,
                                                   p_LoanAppEPCInd                      CHAR := NULL,
                                                   p_LoanAppPymtAmt                     NUMBER := NULL,
                                                   p_LoanAppFullAmortPymtInd            CHAR := NULL,
                                                   p_LoanAppMoIntQty                    NUMBER := NULL,
                                                   p_LoanAppLifInsurRqmtInd             CHAR := NULL,
                                                   p_LoanAppRcnsdrtnInd                 CHAR := NULL,
                                                   p_LoanAppInjctnInd                   CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransPostInd        CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransInd            CHAR := NULL,
                                                   p_LoanAppDisasterCntrlNmb            VARCHAR2 := NULL,
                                                   p_LoanCollatInd                      CHAR := NULL,
                                                   p_SpcPurpsLoanCds                    VARCHAR2 := NULL,
                                                   p_LoanAppCreatUserId                 CHAR := NULL,
                                                   p_RecovInd                           CHAR := NULL,
                                                   p_LoanAppOutPrgrmAreaOfOperInd       CHAR := NULL,
                                                   p_LoanAppParntLoanNmb                CHAR := NULL,
                                                   p_LoanAppCDCGntyAmt                  NUMBER := 0,
                                                   p_LoanAppCDCNetDbentrAmt             NUMBER := 0,
                                                   p_LoanAppCDCFundFeeAmt               NUMBER := 0,
                                                   p_LoanAppCDCSeprtPrcsFeeInd          CHAR := NULL,
                                                   p_LoanAppCDCPrcsFeeAmt               NUMBER := 0,
                                                   p_LoanAppCDCClsCostAmt               NUMBER := 0,
                                                   p_LoanAppCDCOthClsCostAmt            NUMBER := 0,
                                                   p_LoanAppCDCUndrwtrFeeAmt            NUMBER := 0,
                                                   p_LoanAppContribPct                  NUMBER := 0,
                                                   p_LoanAppContribAmt                  NUMBER := 0,
                                                   p_LOANDISASTRAPPFEECHARGED           NUMBER := 0,
                                                   p_LOANDISASTRAPPDCSN                 CHAR := NULL,
                                                   p_LOANASSOCDISASTRAPPNMB             CHAR := NULL,
                                                   p_LOANASSOCDISASTRLOANNMB            CHAR := NULL,
                                                   p_LoanAppPymtDefMoNmb                NUMBER := NULL,
                                                   p_LOANDISASTRAPPNMB                  CHAR := NULL,
                                                   p_LOANPYMNINSTLMNTFREQCD             CHAR := NULL,
                                                   p_LOANAPPINTDTLCD                    VARCHAR2 := NULL,
                                                   p_UNDRWRITNGBY                       CHAR := NULL,
                                                   p_LOANAMTLIMEXMPTIND                 CHAR := NULL,
                                                   p_LoanPymntSchdlFreq                 VARCHAR2 := NULL,
                                                   p_LoanPymtRepayInstlTypCd            CHAR := NULL,
                                                   p_LOANGNTYMATSTIND                   CHAR := NULL,
                                                   p_LoanGntyNoteDt                     DATE := NULL,
                                                   p_LoanAppCDCGrossDbentrAmt           NUMBER := NULL,
                                                   p_LoanAppBalToBorrAmt                NUMBER := NULL,
                                                   p_LoanAppRevlMoIntQty                NUMBER := NULL,
                                                   p_LoanAppAmortMoIntQty               NUMBER := NULL,
                                                   p_LoanAppExtraServFeeInd             CHAR := NULL,
                                                   p_LoanAppExtraServFeeAMT             NUMBER := NULL,
                                                   p_LoanAppNetEarnInd                  CHAR := NULL,
                                                   p_LoanAppMonthPayroll                NUMBER := 0,
                                                   p_LoanAppUSResInd                    CHAR := NULL,
                                                   p_LOANAPPSCHEDCIND                   CHAR := NULL, 
                                                   p_LOANAPPSCHEDCYR                    NUMBER := NULL, 
                                                   p_LOANAPPGROSSINCOMEAMT              NUMBER := NULL) AS
    /* Date         :
    Programmer   :
    Remarks      :
    Parameters   :
    ***********************************************************************************
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
    RGG   -- 11/07/2013 -- Modified to LoanAppPymtAdjPrdCd rename to LOANPYMNINSTLMNTFREQCD
    SB -   10//2014 -- Modified to add parameter and to update UNDRWRITNGBY column
    SS-- 04/27/16 -- Modified to add new column LOANAMTLIMEXMPTIND for 504 refi
    SS--09/30/2016 --Added new column LoanPymntSchdlFreq for OPSMDEV 1116 (repayments)
     SS---10/12/2016 Added LoanPymtRepayInstlTypCd column OPSMDEV1116
     SS--12/8/2016 Added LOANGNTYMATSTIND OPSMDEV 1288
     SS---12/12/2016 Added LoanGntyNoteDt to identifier OPSMDEV1288
     RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0
     NK--01/16/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
     SS--10/30/2018--OPSMDEV 1965  --Added  LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd to ID 0
     SS--02/21/2020 OPSMDEV 2406 Added LoanAppNetEarnInd
     SL--04/01/2020 CAFSCARES35 Added p_LoanAppMonthPayroll
     SL--01/08/2021--SODSTORY-360. Added p_LoanAppUSResInd
     JP --1/14/2021 SODSTORY-390 Changed P_UNDRWRITNGBY to SBA for PPP/PPS
     SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 

    *********************************************************************************
    */
    v_SpcPurpsLoanCd   CHAR (4);
    LEN                NUMBER (10, 0);
    cnt                NUMBER (10, 0);
BEGIN
    /* Update Application Screen into the Loan Incomplete Application Table */
    IF p_Identifier = 0 THEN
        BEGIN
            UPDATE LoanAppTbl
            SET LoanAppNmb = p_LoanAppNmb,
                LoanAppNm = p_LoanAppNm,
                LoanAppRqstAmt = p_LoanAppRqstAmt,
                LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                LoanAppEPCInd = p_LoanAppEPCInd,
                LoanAppPymtAmt = p_LoanAppPymtAmt,
                LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                LoanAppMoIntQty = p_LoanAppMoIntQty,
                LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                LoanAppInjctnInd = p_LoanAppInjctnInd,
                LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                LoanCollatInd = p_LoanCollatInd,
                RecovInd = p_RecovInd,
                LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                LoanAppCDCGntyAmt = p_LoanAppCDCGntyAmt,
                LoanAppCDCNetDbentrAmt = p_LoanAppCDCNetDbentrAmt,
                LoanAppCDCFundFeeAmt = p_LoanAppCDCFundFeeAmt,
                LoanAppCDCSeprtPrcsFeeInd = p_LoanAppCDCSeprtPrcsFeeInd,
                LoanAppCDCPrcsFeeAmt = p_LoanAppCDCPrcsFeeAmt,
                LoanAppCDCClsCostAmt = p_LoanAppCDCClsCostAmt,
                LoanAppCDCOthClsCostAmt = p_LoanAppCDCOthClsCostAmt,
                LoanAppCDCUndrwtrFeeAmt = p_LoanAppCDCUndrwtrFeeAmt,
                LoanAppContribPct = p_LoanAppContribPct,
                LoanAppContribAmt = p_LoanAppContribAmt,
                LOANDISASTRAPPFEECHARGED = p_LOANDISASTRAPPFEECHARGED,
                LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                LOANASSOCDISASTRLOANNMB = p_LOANASSOCDISASTRLOANNMB,
                LoanAppPymtDefMoNmb = p_LoanAppPymtDefMoNmb,
                LoanAppCreatUserId = p_LoanAppCreatUserId,
                LoanAppCreatDt = SYSDATE,
                LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                LASTUPDTDT = SYSDATE,
                LOANDISASTRAPPNMB = p_LOANDISASTRAPPNMB,
                LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                UNDRWRITNGBY = CASE WHEN PrcsMthdCd IN ('PPP', 'PPS') THEN 'SBA' ELSE P_UNDRWRITNGBY END,
                --    p_UNDRWRITNGBY,
                LOANAMTLIMEXMPTIND = p_LOANAMTLIMEXMPTIND,
                LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                LoanPymtRepayInstlTypCd = NVL (p_LoanPymtRepayInstlTypCd, LoanPymtRepayInstlTypCd),
                LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                LoanGntyNoteDt = p_LoanGntyNoteDt,
                LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd,
                LoanAppMonthPayroll = p_LoanAppMonthPayroll,
                LoanAppUSResInd = p_LoanAppUSResInd,
                LOANAPPSCHEDCIND = p_LOANAPPSCHEDCIND, 
                LOANAPPSCHEDCYR = p_LOANAPPSCHEDCYR, 
                LOANAPPGROSSINCOMEAMT = p_LOANAPPGROSSINCOMEAMT,
                LoanAppNetEarnInd = p_LoanAppNetEarnInd
            WHERE LoanAppNmb = p_LoanAppNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    /* Update Special Purpose Table */
    BEGIN
        DELETE FROM LoanSpcPurpsTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    v_SpcPurpsLoanCd := NULL;

    IF p_SpcPurpsLoanCds IS NOT NULL THEN
        BEGIN
            SELECT LENGTH (p_SpcPurpsLoanCds) INTO LEN FROM DUAL;

            cnt := 1;

            WHILE LEN >= cnt
            LOOP
                BEGIN
                    --SELECT substr('NRAL,REVL',1,4)
                    --FROM DUAL;
                    SELECT SUBSTR (p_SpcPurpsLoanCds, cnt, 4) INTO v_SpcPurpsLoanCd FROM DUAL;

                    -- DBMS_OUTPUT.put_line(':'||v_SpcPurpsLoanCd);
                    INSERT INTO LoanSpcPurpsTbl (LoanAppNmb,
                                                 LoanSpcPurpsSeqNmb,
                                                 SpcPurpsLoanCd,
                                                 CreatUserId,
                                                 CreatDt,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
                        SELECT p_LoanAppNmb,
                               (  NVL (
                                      (SELECT MAX (LoanSpcPurpsSeqNmb)
                                       FROM LoanSpcPurpsTbl z
                                       WHERE z.LoanAppNmb = p_LoanAppNmb),
                                      0)
                                + 1)
                                   AS LoanSpcPurpsSeqNmb,
                               v_SpcPurpsLoanCd,
                               p_LoanAppCreatUserId,
                               SYSDATE,
                               NVL (p_LoanAppCreatUserId, USER),
                               SYSDATE
                        FROM DUAL;

                    --LoanSpcPurpsTbl
                    --WHERE LoanAppNmb =p_LoanAppNmb;
                    p_RetVal := SQL%ROWCOUNT;
                    p_RetVal := p_RetVal + 1;
                    cnt := cnt + 5;
                END;
            END LOOP;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAPPUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPUPDTSP (p_Identifier                            NUMBER := 0,
                                                   p_RetVal                            OUT NUMBER,
                                                   p_LoanAppNmb                            NUMBER := 0,
                                                   p_PrgmCd                                CHAR := NULL,
                                                   p_PrcsMthdCd                            CHAR := NULL,
                                                   p_LoanAppNm                             VARCHAR2 := NULL,
                                                   p_LoanAppRecvDt                         DATE := NULL,
                                                   p_LoanAppEntryDt                        DATE := NULL,
                                                   p_LoanAppRqstAmt                 IN OUT NUMBER,
                                                   p_LoanAppSBAGntyPct                     NUMBER := NULL,
                                                   p_LoanAppSBARcmndAmt                    NUMBER := NULL,
                                                   p_LoanAppRqstMatMoQty                   NUMBER := NULL,
                                                   p_LoanAppEPCInd                         CHAR := NULL,
                                                   p_LoanAppPymtAmt                        NUMBER := NULL,
                                                   p_LoanAppFullAmortPymtInd               CHAR := NULL,
                                                   p_LoanAppMoIntQty                       NUMBER := NULL,
                                                   p_LoanAppLifInsurRqmtInd                CHAR := NULL,
                                                   p_LoanAppRcnsdrtnInd                    CHAR := NULL,
                                                   p_LoanAppInjctnInd                      CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransPostInd           CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransInd               CHAR := NULL,
                                                   p_LoanAppEligEvalInd                    CHAR := NULL,
                                                   p_LoanAppNewBusCd                       CHAR := NULL,
                                                   p_NAICSCd                               NUMBER := NULL,
                                                   p_LoanCollatInd                         CHAR := NULL,
                                                   p_LoanAppDisasterCntrlNmb               VARCHAR2 := NULL,
                                                   p_LoanAppCreatUserId                    VARCHAR2 := NULL,
                                                   p_LoanAppOutPrgrmAreaOfOperInd          CHAR := NULL,
                                                   p_LoanAppParntLoanNmb                   CHAR := NULL,
                                                   p_LoanAppMicroLendrId                   CHAR := NULL,
                                                   p_ACHRtngNmb                            VARCHAR2 := NULL,
                                                   p_ACHAcctNmb                            VARCHAR2 := NULL,
                                                   p_ACHAcctTypCd                          CHAR := NULL,
                                                   p_LOANAPPCDCGNTYAMT                     NUMBER := NULL,
                                                   p_LOANAPPCDCNETDBENTRAMT                NUMBER := NULL,
                                                   p_LOANAPPCDCFUNDFEEAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCSEPRTPRCSFEEIND             CHAR := NULL,
                                                   p_LOANAPPCDCPRCSFEEAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCCLSCOSTAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCOTHCLSCOSTAMT               NUMBER := NULL,
                                                   p_LOANAPPCDCUNDRWTRFEEAMT               NUMBER := NULL,
                                                   p_LOANAPPCONTRIBPCT                     NUMBER := NULL,
                                                   p_LOANAPPCONTRIBAMT                     NUMBER := NULL,
                                                   p_LOANDISASTRAPPFEECHARGED              NUMBER := NULL,
                                                   p_LOANDISASTRAPPDCSN                    CHAR := NULL,
                                                   p_LOANASSOCDISASTRAPPNMB                CHAR := NULL,
                                                   p_LOANASSOCDISASTRLOANNMB               CHAR := NULL,
                                                   p_LOANDISASTRAPPNMB                     CHAR := NULL,
                                                   p_LoanAppUndrwritngExprtDt              DATE := NULL,
                                                   p_LOANNMB                               CHAR := NULL,
                                                   p_LOANAPPFUNDDT                         DATE := NULL,
                                                   p_UNDRWRITNGBY                          CHAR := NULL,
                                                   p_COHORTCD                              CHAR := NULL,
                                                   p_FUNDSFISCLYR                          NUMBER := NULL,
                                                   p_LOANTYPIND                            CHAR := NULL,
                                                   p_LoanAppUndrwritngExprtRsn             VARCHAR2 := NULL,
                                                   p_LOANAPPINTDTLCD                       VARCHAR2 := NULL,
                                                   p_LOANPYMNINSTLMNTFREQCD                CHAR := NULL,
                                                   p_UndrwritngCrdRiskCertInd              VARCHAR2 := NULL,
                                                   p_Statcd                                CHAR := NULL,
                                                   p_LoanAmtLimExmptInd                    CHAR := NULL,
                                                   p_LoanAppEtranVenNm                     CHAR := NULL,
                                                   p_LoanPymntSchdlFreq                    VARCHAR := NULL,
                                                   p_LOANGNTYMATSTIND                      CHAR := NULL,
                                                   p_LoanGntyNoteDt                        DATE := NULL,
                                                   p_CDCServFeePct                         NUMBER := NULL,
                                                   p_LoanAppCDCGrossDbentrAmt              NUMBER := NULL,
                                                   p_LoanAppBalToBorrAmt                   NUMBER := NULL,
                                                   p_LoanAppRevlMoIntQty                   NUMBER := NULL,
                                                   p_LoanAppAmortMoIntQty                  NUMBER := NULL,
                                                   p_LoanPymtRepayInstlTypCd               CHAR := NULL,
                                                   p_LoanAppExtraServFeeAMT                NUMBER := NULL,
                                                   p_LoanAppExtraServFeeInd                CHAR := NULL,
                                                   p_Rqstseqnmb                            NUMBER := NULL,
                                                   p_lndrmatchind                          CHAR := NULL,
                                                   p_LoanAppMonthPayroll                   NUMBER := NULL,
                                                   p_LoanAppUSResInd                       CHAR:=NULL,
                                                   p_LOANAPPSCHEDCIND                   CHAR := NULL, 
                                                   p_LOANAPPSCHEDCYR                    NUMBER := NULL, 
                                                   p_LOANAPPGROSSINCOMEAMT              NUMBER := NULL)
AS
    /*
      created by:
      purpose:
      parameters:
      Revision:
               Modified by APK - 09/23/2010- to include LoanSBAGntyBalAmt as part of calculation instead of LoanLendrGrossAmt, in Identifier 14.
                09/28/2010-- APK-- included the FININSTRMNTTYPIND change for PrgrmValidTbl and parameter tables.
      10/05/2010 -- APK -- Removed the FININSTRMNTTYPIND column from parameter tables and
     hence in the procedure.
       10/07/2010 -- APK -- modify identifier 14 in LOANAPP.LOANAPPUPDTSP remove the update
       to  LoanAppSBAGntyPct field..
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     RGG -- 03/26/2012 -- added an IF condition at line 347
     SP  -- 11/16/2012 -- added id=17  to update when a loan application was successfully exported to the Loan Guaranty Processing Center for SBA Underwriting
     SP -- 02/28/2013 -- modified to add new columns LOANNMB,LOANAPPFUNDDT,UNDRWRITNGBY,
    COHORTCD,FUNDSFISCLYR,LOANTYPIND to id=4,18
     SP -- 06/05/2013 -- Added id =19 to update LoanAppRqstAmt,LoanAppSBARcmndAmt as SBA reviewers are allowed to make these changes to the application at the time they render the decision to approve or decline the application
     RGG -- 06/19/2013 -- Changed Id 18 to reflect for Funds pending and added ID 20 for Funded loans.
     RGG -- 06/28/2013 -- Chnged Id 18 to add ,LoanAppAppvDt       = sysdate at line 423 as per Developer's request.
     SP -- 09/02/2013 -- Changed id =17 to update LoanAppUndrwritngExprtRsn
     RSURAPA - 10/23/2013 -- Modified the procedure to add new column LOANAPPINTDTLCD
     RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
     RGG -- 11/01/2013 -- Added new columns to ID 19.
     RGG -- 11/07/2013 -- Renamed LoanAppPymtAdjPrdCd to LOANPYMNINSTLMNTFREQCD
     RGG -- 11/08/2013 -- Added LoanAppInitlSprdOvrPrimePct to ID 19.
     SP  -- 11/27/2013 -- Removed references to columns :IMRtTypCd(id= 0,1,12,19),LoanAppFixVarInd(id= 0,1,12,19),
    LoanAppBaseRt(id= 0,1,12,19),LoanAppInitlIntPct(id= 0,1,12,14,19),LoanAppInitlSprdOvrPrimePct(id= 0,1,12,19),LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd(id= 0,1,12,19)
    SP   -- 01/28/2014  -- Added id =21 to update UnderwritingBy for reconsideration changes
    SP   -- 02/20/2014  -- Added id =22 Update only interest detail code for Underwriting web service
    SP   -- 04/07/2014  -- Modified id = 0,12 to remove NAICSYRNMB,NAICSCD(0,12,19),LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,loanappfrnchsind as these are moved to loanappprojtbl
    SB - 09/03/2014 -- Modified the procedure for (identifier 4) with logic IF PrgrmCd=E then Financial Instrument Ind = `E� else `L�
    PSP - 09/26/2014 -- Modified to add new identifier to update UndrwritngCrdRiskCertInd for lender to update their own underwriting decision, despite LqdCr
    SS-05/05/2016 --Modified to add new column LoanAmtLimExmptInd to identifier 25
    BR - 08/12/2016 -- Modified to add new column LoanAppEtranVenNm to identifier 25
    SS--09/29/2016 --Modified to add new column LoanPymntSchdlFreq  --OPSMDEV 1116
    RY--11/14/16--OPSMDEV-1212 --Added LoanPymntSchdlFreq condition to Identifier12
    SS--11/29/2016 OPSMDEV 1245 --Added LOANGNTYMATSTIND to identifier 0
    SS--01/04/2017 Added identifier 31
    --NK--04/20/2017--CAFSOPER-- Added p_LoanGntyNoteDt in ID 25.
    SS--05/19/2017 --OPSMDEV 1436 Added CDCServFeePct to Identifier 0
    SS--05/23/2017 --OPSMDEV 1436 Added identifier 32
    RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID- 0 and 25
    JP -- 09/29/2017 - CAFSOPER-1201 Modified the identifier 16 code
    NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
    NK--01/18/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 25
    BJR -- 01/25/2018 -- CAFSOPER-1381 -- Removed created by and created dates from the update statements so that the original creator/create date stays intact for the record.
 Ry--04/19/18 --OPSMDEV--1783--Added LoanPymtRepayInstlTypCd in Id-0,1,12,19,25,31
 SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to identifier 0 and 25
 RY--08/15/2019--OPSMDEV--2269:: Added Id-34
 JP -- 04/03/2020 CARESACT-63 added LoanAppMonthPayroll for ID 25 update of LoanAppTbl
 SL--01/08/2021--SODSTORY-360. Added p_LoanAppUSResInd to ID 25
 SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 

 */
    v_LoanAppOrigntnOfcCd   CHAR (4);
    v_LoanAppPrcsOfcCd      CHAR (4);
    v_IMPrmtrValChk         VARCHAR (255);
    v_IMPrmtrId             VARCHAR2 (255);
    v_RecovInd              CHAR (1);
    v_PrcsMthdCd            CHAR (3);
    v_LoanAppRecvDt         DATE;
    v_LoanSBAGntyBalAmt     NUMBER (19, 4);
    v_LOANINTRT             NUMBER (6, 3);
BEGIN
    /* Update All in the Loan Incomplete Application Table */
    CASE p_Identifier
        WHEN 0
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       PrgmCd = TRIM (p_PrgmCd),
                       PrcsMthdCd = TRIM (p_PrcsMthdCd),
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                       LoanAppMicroLendrId = p_LoanAppMicroLendrId,
                       LOANDISASTRAPPFEECHARGED = p_LOANDISASTRAPPFEECHARGED,
                       LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                       LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                       LOANASSOCDISASTRLOANNMB = p_LOANASSOCDISASTRLOANNMB,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       CDCServFeePct = p_CDCServFeePct,
                       LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                       LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                       LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                       LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd,
                       LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                       LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 1
        /* Update Application Screen into the Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       PrgmCd = p_PrgmCd,
                       PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = LoanAppEWCPSnglTransInd,
                       LoanAppDisasterCntrlNmb = LoanAppDisasterCntrlNmb,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 2
        /* Update Eligibility in the Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 3                                       /* Update the Received Date in Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 4                            /* Update the LoanAppNm and PrcsMthdCd in Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       UNDRWRITNGBY = p_UNDRWRITNGBY,
                       LOANTYPIND = p_LOANTYPIND,
                       PrgmCd = p_PrgmCd,
                       FinInstrmntTypInd = (CASE WHEN p_Prgmcd = 'E' THEN 'E' ELSE 'L' END)
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 11                                                                     -- only to update recommended amount
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 12
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET PrgmCd = p_PrgmCd,
                       PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                       LoanAppMicroLendrId = p_LoanAppMicroLendrId,
                       LOANAPPCDCGNTYAMT = p_LOANAPPCDCGNTYAMT,
                       LOANAPPCDCNETDBENTRAMT = p_LOANAPPCDCNETDBENTRAMT,
                       LOANAPPCDCFUNDFEEAMT = p_LOANAPPCDCFUNDFEEAMT,
                       LOANAPPCDCSEPRTPRCSFEEIND = p_LOANAPPCDCSEPRTPRCSFEEIND,
                       LOANAPPCDCPRCSFEEAMT = p_LOANAPPCDCPRCSFEEAMT,
                       LOANAPPCDCCLSCOSTAMT = p_LOANAPPCDCCLSCOSTAMT,
                       LOANAPPCDCOTHCLSCOSTAMT = p_LOANAPPCDCOTHCLSCOSTAMT,
                       LOANAPPCDCUNDRWTRFEEAMT = p_LOANAPPCDCUNDRWTRFEEAMT,
                       LOANAPPCONTRIBPCT = p_LOANAPPCONTRIBPCT,
                       LOANAPPCONTRIBAMT = p_LOANAPPCONTRIBAMT,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 13
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET loanappnm = p_LoanAppNm
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 14
        THEN
            BEGIN
                SELECT LoanIntRt
                  INTO v_LoanIntRt
                  FROM loanapp.LoanIntDtlTbl
                 WHERE loanappnmb = p_LoanAppNmb AND LoanIntDtlSeqNmb = 1;

                SELECT NVL (SUM (LoanSBAGntyBalAmt), 0)
                  INTO v_LoanSBAGntyBalAmt
                  FROM loanapp.LoanPartLendrTbl p
                 WHERE p.LoanAppNmb = p_LoanAppNmb;

                UPDATE loanapptbl a
                   SET LoanAppRqstAmt = v_LoanSBAGntyBalAmt,
                       LoanAppPymtAmt = v_LoanSBAGntyBalAmt * (1 + (v_LoanIntRt / 100)) / LoanAppRqstMatMoQty, /*(1+(LoanAppInitlIntPct/100))*/
                       LoanAppSBARcmndAmt = v_LoanSBAGntyBalAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;

                /*OPEN p_SelCur FOR*/
                SELECT LoanAppRqstAmt
                  INTO p_LoanAppRqstAmt
                  FROM loanapp.LoanAppTbl
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 15
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                       LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 16                                                   -- LoanAppOrigntnOfcCd, LoanAppPrcsOfcCd and LoanAppNm
        THEN
            BEGIN
                /*IF p_PrcsMthdCd IS NULL
                THEN
                    BEGIN
                        SELECT PrcsMthdCd
                          INTO v_PrcsMthdCd
                          FROM LoanAppTbl
                         WHERE LoanAppNmb = p_LoanAppNmb;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            v_PrcsMthdCd := NULL;
                    END;
                ELSE
                    v_PrcsMthdCd := p_PrcsMthdCd;
                END IF;

                IF v_PrcsMthdCd IN ('DPB', 'DPH', 'DPO')
                THEN
                    v_LoanAppOrigntnOfcCd := '9030';
                ELSE
                    BEGIN
                        v_LoanAppRecvDt := SYSDATE;
                        v_RecovInd := NULL;
                        v_IMPrmtrId := 'OrigntOfcCd';
                        v_IMPrmtrValChk :=
                            LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId,
                                                    v_PrcsMthdCd,
                                                    v_LoanAppRecvDt,
                                                    v_RecovInd);

                        IF v_IMPrmtrValChk IS NULL
                        THEN
                            BEGIN
                                SELECT c.OrignOfcCd
                                  INTO v_LoanAppOrigntnOfcCd
                                  FROM loanapp.LoanAppProjTbl p,                                           -- p for proj
                                                                 sbaref.CntyTbl c                          -- c for cnty
                                 WHERE     c.CntyCd = p.LoanAppProjCntyCd
                                       AND c.StCd = p.LoanAppProjStCd
                                       AND p.LoanAppNmb = p_LoanAppNmb;
                            EXCEPTION
                                WHEN NO_DATA_FOUND
                                THEN
                                    v_LoanAppOrigntnOfcCd := NULL;
                            END;
                        ELSE
                            v_LoanAppOrigntnOfcCd := v_IMPrmtrValChk;
                        END IF;
                    END;
                END IF;

                BEGIN
                    v_LoanAppRecvDt := SYSDATE;
                    v_RecovInd := NULL;
                    v_IMPrmtrId := 'SrvcngOfcCd';
                    v_IMPrmtrValChk :=
                        LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId,
                                                v_PrcsMthdCd,
                                                v_LoanAppRecvDt,
                                                v_RecovInd);

                    IF v_IMPrmtrValChk IS NULL
                    THEN
                        BEGIN
                            SELECT c.ServOfcCd
                              INTO v_LoanAppPrcsOfcCd
                              FROM loanapp.LoanAppProjTbl p,                                               -- p for proj
                                                             sbaref.CntyTbl c                              -- c for cnty
                             WHERE     c.CntyCd = p.LoanAppProjCntyCd
                                   AND c.StCd = p.LoanAppProjStCd
                                   AND p.LoanAppNmb = p_LoanAppNmb;
                        EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                                v_LoanAppPrcsOfcCd := NULL;
                        END;
                    ELSE
                        v_LoanAppPrcsOfcCd := v_IMPrmtrValChk;
                    END IF;
                END;*/

                UPDATE LoanAppTbl SET
                   	   --LoanAppOrigntnOfcCd = NVL (v_LoanAppOrigntnOfcCd, LoanAppOrigntnOfcCd),
                       --LoanAppPrcsOfcCd = NVL (v_LoanAppPrcsOfcCd, LoanAppPrcsOfcCd),
                       LoanAppNm = NVL (p_LoanAppNm, LoanAppNm),
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 17 /*update when a loan application was successfully exported to the Loan Guaranty Processing Center for SBA Underwriting*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppUndrwritngExprtDt = p_LoanAppUndrwritngExprtDt,
                       LoanAppUndrwritngExprtRsn = p_LoanAppUndrwritngExprtRsn,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 18                                                                       /*Mark the loan as funds pending*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = 'FP',
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       LoanAppAppvDt = SYSDATE,
                       loanappfunddt = p_loanappfunddt,
                       cohortcd = p_cohortcd,
                       FUNDSFISCLYR = p_FUNDSFISCLYR,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 19 /*Update LoanAppRqstAmt for SBA reviewers at the time of rendering the decision to approve or decline the application */
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;

                UPDATE loanappprojtbl
                   SET NAICSCd = p_NAICSCd, LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER), LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 20                                                                              /*Mark the loan as funded*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = 'FD',
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       loannmb = p_loannmb,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 21                                              /*Update only underwriting by for reconsideration changes*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET UndrwritngBy = p_UndrwritngBy,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 22                                       /*Update only interest detail code for Underwriting web service */
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 23 /*updates UndrwritngCrdRiskCertInd for lender to update their own underwriting decision, despite LqdCr */
        THEN
            BEGIN
                LOANAPP.MOVEAUDTLOANSCSP (P_LOANAPPNMB);

                UPDATE loanapptbl
                   SET UndrwritngCrdRiskCertInd = p_UndrwritngCrdRiskCertInd,
                       StatCd = p_StatCd,
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       LastUpdtUserId = p_LoanAppCreatUserId,
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 24        /* Update the Received Date in Loan Incomplete Application Table use LOANAPP.LOANAPPRECVUPDCSP */
        THEN
            BEGIN
                SELECT LoanAppRecvDt
                  INTO v_LoanAppRecvDt
                  FROM LoanAppTbl
                 WHERE LoanAppNmb = p_LoanAppNmb;

                IF v_LoanAppRecvDt IS NULL
                THEN
                    LOANAPP.LOANAPPRECVUPDCSP (p_LoanAppNmb, p_LoanAppRecvDt, p_LoanAppCreatUserId);
                    p_RetVal := 1;
                END IF;
            END;
        WHEN 25                                  /* Updates required by the OrigUpdate and Underwriting web services. */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LastUpdtDt = SYSDATE,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LoanAppCDCClsCostAmt = p_LoanAppCDCClsCostAmt,
                       LoanAppCDCOthClsCostAmt = p_LoanAppCDCOthClsCostAmt,
                       LoanAppCDCFundFeeAmt = p_LoanAppCDCFundFeeAmt,
                       LoanAppCDCGntyAmt = p_LoanAppCDCGntyAmt,
                       LoanAppCDCNetDbentrAmt = p_LoanAppCDCNetDbentrAmt,
                       LoanAppCDCPrcsFeeAmt = p_LoanAppCDCPrcsFeeAmt,
                       LoanAppCDCSeprtPrcsFeeInd = p_LoanAppCDCSeprtPrcsFeeInd,
                       LoanAppCDCUndrwtrFeeAmt = p_LoanAppCDCUndrwtrFeeAmt,
                       LoanAppContribAmt = p_LoanAppContribAmt,
                       LoanAppContribPct = p_LoanAppContribPct,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppRqstAmt =
                           CASE WHEN StatCd IN ('R1', 'R2', 'R3') THEN LoanAppRqstAmt ELSE p_LoanAppRqstAmt END,
                       LoanAppSBARcmndAmt = p_LoanAppRqstAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAssocDisastrLoanNmb = p_LoanAssocDisastrLoanNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanDisastrAppDcsn = p_LoanDisastrAppDcsn,
                       LoanDisastrAppFeeCharged = p_LoanDisastrAppFeeCharged,
                       LoanDisastrAppNmb = p_LoanDisastrAppNmb,
                       LoanPymnInstlmntFreqCd = p_LoanPymnInstlmntFreqCd,
                       UndrwritngBy = p_UndrwritngBy,
                       LoanAmtLimExmptInd = p_LoanAmtLimExmptInd,
                       LoanAppEtranVenNm = p_LoanAppEtranVenNm,                 -- BR 08/12/2016 added LoanAppEtranVenNm
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                       LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                       LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                       LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd,
                       LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                       LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd,
                       LoanAppMonthPayroll = p_LoanAppMonthPayroll,
                       LoanAppUSResInd= p_LoanAppUSResInd,
                       LOANAPPSCHEDCIND = p_LOANAPPSCHEDCIND, 
                        LOANAPPSCHEDCYR = p_LOANAPPSCHEDCYR, 
                        LOANAPPGROSSINCOMEAMT = p_LOANAPPGROSSINCOMEAMT
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 31
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LoanPymnInstlmntFreqCd = p_LoanPymnInstlmntFreqCd,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LoanPymtRepayInstlTypCd = NVL (p_LoanPymtRepayInstlTypCd, LoanPymtRepayInstlTypCd)
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 32
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET CDCServFeePct = p_CDCServFeePct,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 33
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET Rqstseqnmb = p_rqstseqnmb, lndrmatchind = p_lndrmatchind
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 34
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = p_StatCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
    END CASE;

    IF p_RetVal IS NULL
    THEN
        p_RetVal := SQL%ROWCOUNT;
        p_RetVal := NVL (p_RetVal, 0);
    END IF;
END LOANAPPUPDTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPREINSCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPREINSCSP (p_LoanAppNmb                            NUMBER := 0,
                                                    p_LoanAppPymtAmt                        NUMBER := NULL,
                                                    p_LoanAppMoIntQty                       NUMBER := NULL,
                                                    p_LoanAppInjctnInd                      CHAR := NULL,
                                                    p_LoanCollatInd                         CHAR := NULL,
                                                    p_LoanAppCreatUserId                    CHAR := NULL,
                                                    p_LoanAppCreatDt                        DATE := NULL,
                                                    p_LoanAppMatDt                          DATE := NULL,
                                                    p_LoanAppFirstDisbDt                    DATE := NULL,
                                                    p_LoanTotUndisbAmt                      NUMBER := NULL,
                                                    p_LoanAppOutPrgrmAreaOfOperInd          CHAR := NULL,
                                                    p_LoanAppParntLoanNmb                   CHAR := NULL,
                                                    p_ACHRtngNmb                            VARCHAR2 := NULL,
                                                    p_ACHAcctNmb                            VARCHAR2 := NULL,
                                                    p_ACHAcctTypCd                          CHAR := NULL,
                                                    p_ACHTinNmb                             CHAR := NULL,
                                                    p_ACTNCD                                CHAR := NULL,
                                                    p_LoanAppCDCGntyAmt                     NUMBER := NULL,
                                                    p_LoanAppCDCNetDbentrAmt                NUMBER := NULL,
                                                    p_LoanAppCDCFundFeeAmt                  NUMBER := NULL,
                                                    p_LoanAppCDCSeprtPrcsFeeInd             CHAR := NULL,
                                                    p_LoanAppCDCPrcsFeeAmt                  NUMBER := NULL,
                                                    p_LoanAppCDCClsCostAmt                  NUMBER := NULL,
                                                    p_LoanAppCDCOthClsCostAmt               NUMBER := NULL,
                                                    p_LoanAppCDCUndrwtrFeeAmt               NUMBER := NULL,
                                                    p_LoanAppContribPct                     NUMBER := NULL,
                                                    p_LoanAppContribAmt                     NUMBER := NULL,
                                                    p_LoanAppSBAGntyPct                     NUMBER := NULL,
                                                    p_LoanCurrAppvAmt                       NUMBER := NULL,
                                                    p_LoanAppRqstMatMoQty                   NUMBER := 0,
                                                    p_LoanAppMatExtDt                       DATE := NULL,
                                                    p_LoanStatCd                            NUMBER := NULL,
                                                    p_LOANSTATCMNTCD                        CHAR := NULL,
                                                    p_LOANGNTYDFRTODT                       DATE := NULL,
                                                    p_LOANGNTYDFRDMNTHSNMB                  NUMBER := NULL,
                                                    p_LOANGNTYDFRDGRSAMT                    NUMBER := NULL,
                                                    p_LASTUPDTUSERID                 IN     CHAR := NULL,
                                                    p_LoanPymntSchdlFreq                    VARCHAR2 := NULL,
                                                    p_LoanNextInstlmntDueDt                 DATE := NULL,
                                                    p_Loan1201NotcInd                       NUMBER := NULL,
                                                    p_LOANSERVGRPCD                         VARCHAR2 := NULL,
                                                    p_FREEZETYPCD                           CHAR := NULL,
                                                    p_FREEZERSNCD                           NUMBER := NULL,
                                                    p_FORGVNESFREEZETYPCD                   NUMBER := NULL,
                                                    p_DisbDeadLnDt                          DATE := NULL,
                                                    p_LOANPYMTTRANSCD                       NUMBER := NULL,
                                                    p_SBICLICNSNMB                          CHAR := NULL,
                                                    p_LOANMAILSTR1NM                        VARCHAR2 := NULL --RGG 03/21/2014 Deleted references
                                                                                                            ,
                                                    p_LOANMAILSTR2NM                        VARCHAR2 := NULL --RGG 03/21/2014 Deleted references
                                                                                                            ,
                                                    p_LOANMAILCTYNM                         VARCHAR2 := NULL --RGG 03/21/2014 Deleted references
                                                                                                            ,
                                                    p_LOANMAILSTCD                          CHAR := NULL --RGG 03/21/2014 Deleted references
                                                                                                        ,
                                                    p_LOANMAILZIPCD                         CHAR := NULL --RGG 03/21/2014 Deleted references
                                                                                                        ,
                                                    p_LOANMAILZIP4CD                        CHAR := NULL --RGG 03/21/2014 Deleted references
                                                                                                        ,
                                                    p_LOANMAILCNTRYCD                       CHAR := NULL --RGG 03/21/2014 Deleted references
                                                                                                        ,
                                                    p_LOANMAILSTNM                          VARCHAR2 := NULL --RGG 03/21/2014 Deleted references
                                                                                                            ,
                                                    p_LOANMAILPOSTCD                        VARCHAR2 := NULL --RGG 03/21/2014 Deleted references
                                                                                                            ,
                                                    p_LOANAPPNETEARNIND                     CHAR := NULL,
                                                    p_LOANPYMTINSTLMNTTYPCD                 CHAR := NULL,
                                                    p_LOANPYMNINSTLMNTFREQCD                CHAR := NULL,
                                                    p_LOANTOTDEFNMB                         NUMBER := NULL,
                                                    p_LOANTOTDEFMONMB                       NUMBER := NULL,
                                                    p_LoanAppNm                             VARCHAR2 := NULL,
                                                    p_LOANGNTYNOTEDT                        DATE := NULL,
                                                    p_LOANGNTYMATSTIND                      VARCHAR2 := NULL,
                                                    p_LOANPYMTREPAYINSTLTYPCD               CHAR := NULL,
                                                    p_LOANTRANSSEQNMB                   OUT NUMBER,
                                                    p_LoanAppCDCGrossDbentrAmt              NUMBER := NULL,
                                                    p_LoanAppBalToBorrAmt                   NUMBER := NULL,
                                                    p_LoanGntyNoteIntRt                     NUMBER := NULL,
                                                    p_LoanGntyNoteMntlyPymtAmt              NUMBER := NULL,
                                                    p_LoanAppRevlMoIntQty                   NUMBER := NULL,
                                                    p_LoanAppAmortMoIntQty                  NUMBER := NULL,
                                                    p_LoanGntyFeeBillAdjAmt                 NUMBER := NULL,
                                                    p_LoanGntyCorrespLangCd                 CHAR := NULL,
                                                    p_LoanAppMonthPayroll                   NUMBER := NULL,
                                                    p_LoanGntyPayRollChngInd                CHAR := NULL) AS
/*
   Created By:
   Purpose:
   Revision:
---05/04/2010 APK added new column p_LOANSTATCMNTCD, others commented out for future use.
---10/05/2010 APK added new column FININSTRMNTTYPIND..
---10/13/2010 - APK --Similar to FinInstrmntTypInd we need to copy all the disaster fields from LoanGntyTbl in to pre table. All of these are read-only fields on the screen.
LoanAppDisasterCntrlNmb
LoanDisastrAppDcsn
LoanDisastrAppFeeCharged
LoanAssocDisastrLoanNmb
LoanAssocDisastrAppNmb.. And formatting..
APK -- 03/18/2011 -- Modified to add LoanAppMatExtDt
APK -- 03/22/2011 -- Removed the LOANGNTYFEEREBATEAMT   changes that were added as they there not needed.
 Modified by : APK 05/06/2011 -- modified to remove column OrigntnLocId as it was removed from the table.
 APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. 
 also added Loan1201NotcInd and LOANSERVGRPCD previously added to the Gnty table.
 APK -- 10/19/2011 -- Modified to change Loan1201NotcInd data type from Char  to Number.
 APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
 APK -- 02/19/2012 -- added LOANPYMTTRANSCD as its added to the table
 SP  -- 3/5/2012  -- Modified to add new column LOANCOFSEFFDT
 SP  -- 4/18/2012 -- Modified to add new column LoanAppNetEarnInd
 SP  -- 4/26/2012 -- Modified as the column ACTIONCD was renamed to ACTNCD
 SP --- 05/01/2012 -- Modified  to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
 RGG -- 06/11/2012 -- Modified to add p_LoanAppNm and update it based on logic "if input parameter loannm passed and not null, then update based on loanappnmb, otherwise get
                        from  loan.loangntytbl"
RSURAPA -- 05/15/2013 -- New column NOTEDT( note date) added to table loangntypretbl, appling the required changed to the add the column to the procedure.
RGG    -- 05/22/2013 -- Removed the NVL for NOTEDT as per the request of developer.
RSURAPA -- 05/22/2013 -- Renamed the NOTEDT column in the loangntypretbl to LOANGNTYNOTEDT.
RSURAPA -- 06/17/2013 -- New Column LOANGNTYMATSTIND(Loan maturity start date indicator for notes) added to the loangntypre table.
SP -- 06/28/2013 -- Modified  to make sure that LOANGNTYMATSTIND  will not be overwritten for non MLD and ILP
RGG -- 03/20/2014 -- Added REPAYINSTLTYPCD
RGG -- 03/21/2014 -- Deleted references to LOANMAILSTR1NM,LOANMAILSTR2NM,LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,
                      LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD
SB  --04/04/2014  -- Removed NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND columns
* RGG -- 09/22/2014 -- Removed NVL for the userid values.
PSP --09/02/2015  -- Modified to get the value for LoanAppIntDtlCd from LoanGntyTbl and insert into pretbl
RGG -- 04/05/2016 -- Added LOANLNDRASSGNLTRDT
NK -- 07/19/2017--CAFSOPER 805 -- Added LoanAppBalToBorrAmt,LoanAppCDCGrossDbentrAmt(should be updated once front end development is completed)
NK--07/20/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt
RY--01/16/2018--OPSMDEV--1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
NK--02/05/2018--OPSMDEV --1507 -- Added LoanGntyFeeBillAdjAmt
RY--07/08/2019--OPSMDEV--2240--Added LoanGntycorrespLangCd
SL--02/01/2021 SODSTORY 446 added column LoanGntyPayRollChngInd
SL --03/04/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT
*/
BEGIN
    SELECT NVL (MAX (loantransseqnmb), 0) + 1
    INTO p_LOANTRANSSEQNMB
    FROM loan.loantranstbl
    WHERE loanappnmb = p_loanappnmb;

    INSERT INTO Loan.LoanGntyPreTbl (LoanAppNmb,
                                     PrgmCd,
                                     PrcsMthdCd,
                                     CohortCd,
                                     LoanAppNm,
                                     LoanAppRecvDt,
                                     LoanAppEntryDt,
                                     LoanAppRqstAmt,
                                     LoanAppSBAGntyPct,
                                     LoanInitlAppvAmt,
                                     LoanCurrAppvAmt,
                                     LoanInitlAppvDt,
                                     LoanLastAppvDt,
                                     LoanAppRqstMatMoQty,
                                     LoanAppEPCInd,
                                     LoanAppPymtAmt,
                                     LoanAppFullAmortPymtInd,
                                     LoanAppMoIntQty,
                                     LoanAppLifInsurRqmtInd,
                                     LoanAppRcnsdrtnInd,
                                     LoanAppInjctnInd,
                                     LoanCollatInd,
                                     LoanAppEWCPSnglTransPostInd,
                                     LoanAppEWCPSnglTransInd,
                                     LoanAppEligEvalInd,
                                     LoanAppNewBusCd,
                                     LoanAppDisasterCntrlNmb,
                                     LoanStatCd,
                                     LoanStatDt,
                                     LoanAppOrigntnOfcCd,
                                     LoanAppPrcsOfcCd,
                                     LoanAppServOfcCd,
                                     LoanAppFundDt,
                                     LoanNmb,
                                     LoanGntyFeeAmt,
                                     LocId,
                                     PrtId,
                                     LoanAppPrtAppNmb,
                                     LoanAppPrtLoanNmb,
                                     LoanOutBalAmt,
                                     LoanOutBalDt,
                                     LoanSoldScndMrktInd,
                                     LoanAppCreatUserId,
                                     LoanAppCreatDt,
                                     LoanTypInd,
                                     LoanSrvsLocId,
                                     LoanAppMatDt,
                                     LoanAppFirstDisbDt,
                                     LoanNextInstlmntDueDt,
                                     LoanTotUndisbAmt,
                                     LoanDisbInd,
                                     RecovInd,
                                     LoanAppOutPrgrmAreaOfOperInd,
                                     LoanAppParntLoanNmb,
                                     LoanAppMicroLendrId,
                                     ACHRtngNmb,
                                     ACHAcctNmb,
                                     ACHAcctTypCd,
                                     LoanIntPaidAmt,
                                     LoanIntPaidDt,
                                     ACHTinNmb,
                                     ACTNCD,
                                     LoanAppCDCGntyAmt,
                                     LoanAppCDCNetDbentrAmt,
                                     LoanAppCDCFundFeeAmt,
                                     LoanAppCDCSeprtPrcsFeeInd,
                                     LoanAppCDCPrcsFeeAmt,
                                     LoanAppCDCClsCostAmt,
                                     LoanAppCDCOthClsCostAmt,
                                     LoanAppCDCUndrwtrFeeAmt,
                                     LoanAppContribPct,
                                     LoanAppContribAmt,
                                     LOANSTATCMNTCD,
                                     LOANGNTYDFRTODT,
                                     LOANGNTYDFRDMNTHSNMB,
                                     LOANGNTYDFRDGRSAMT,
                                     LOANDISASTRAPPFEECHARGED,
                                     LOANDISASTRAPPDCSN,
                                     LOANASSOCDISASTRAPPNMB,
                                     LOANASSOCDISASTRLOANNMB,
                                     LoanPymntSchdlFreq,
                                     FININSTRMNTTYPIND,
                                     LASTUPDTUSERID,
                                     LASTUPDTDT,
                                     LoanAppMatExtDt,
                                     LOAN1201NOTCIND,
                                     LOANSERVGRPCD,
                                     FREEZETYPCD,
                                     FREEZERSNCD,
                                     FORGVNESFREEZETYPCD,
                                     DisbDeadLnDt,
                                     LOANPYMTTRANSCD,
                                     LOANCOFSEFFDT,
                                     SBICLICNSNMB,
                                     LOANAPPNETEARNIND,
                                     LOANPYMTINSTLMNTTYPCD,
                                     LOANPYMNINSTLMNTFREQCD,
                                     LOANTOTDEFNMB,
                                     LOANTOTDEFMONMB,
                                     LOANGNTYNOTEDT,
                                     LOANGNTYMATSTIND,
                                     LOANTRANSSEQNMB,
                                     LOANPYMTREPAYINSTLTYPCD,
                                     LoanAppIntDtlCd,
                                     LOANLNDRASSGNLTRDT,
                                     LoanAppBalToBorrAmt,
                                     LoanAppCDCGrossDbentrAmt,
                                     LoanGntyNoteIntRt,
                                     LoanGntyNoteMntlyPymtAmt,
                                     LoanAppRevlMoIntQty,
                                     LoanAppAmortMoIntQty,
                                     LoanGntyFeeBillAdjAmt,
                                     LoanGntyCorrespLangCd,
                                     LoanAppMonthPayroll,
                                     LoanGntyPayRollChngInd,
                                     LOANAPPSCHEDCYR,
                                     LOANAPPGROSSINCOMEAMT)
        SELECT LoanAppNmb,
               PrgmCd,
               PrcsMthdCd,
               CohortCd,
               NVL (p_LoanAppNm, LoanAppNm),
               LoanAppRecvDt,
               LoanAppEntryDt,
               LoanAppRqstAmt,
               p_LoanAppSBAGntyPct,
               LoanInitlAppvAmt,
               p_LoanCurrAppvAmt,
               LoanInitlAppvDt,
               SYSDATE,
               p_LoanAppRqstMatMoQty,
               LoanAppEPCInd,
               p_LoanAppPymtAmt,
               LoanAppFullAmortPymtInd,
               p_LoanAppMoIntQty,
               LoanAppLifInsurRqmtInd,
               LoanAppRcnsdrtnInd,
               p_LoanAppInjctnInd,
               p_LoanCollatInd,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanAppEligEvalInd,
               LoanAppNewBusCd,
               LoanAppDisasterCntrlNmb,
               p_LoanStatCd,
               LoanStatDt,
               LoanAppOrigntnOfcCd,
               LoanAppPrcsOfcCd,
               LoanAppServOfcCd,
               LoanAppFundDt,
               LoanNmb,
               LoanGntyFeeAmt,
               LocId,
               PrtId,
               LoanAppPrtAppNmb,
               LoanAppPrtLoanNmb,
               LoanOutBalAmt,
               LoanOutBalDt,
               LoanSoldScndMrktInd,
               p_LoanAppCreatUserId,
               p_LoanAppCreatDt,
               LoanTypInd,
               LoanSrvsLocId,
               p_LoanAppMatDt,
               p_LoanAppFirstDisbDt,
               p_LoanNextInstlmntDueDt,
               p_LoanTotUndisbAmt,
               LoanDisbInd,
               RecovInd,
               p_LoanAppOutPrgrmAreaOfOperInd,
               p_LoanAppParntLoanNmb,
               LoanAppMicroLendrId,
               p_ACHRtngNmb,
               p_ACHAcctNmb,
               p_ACHAcctTypCd,
               LoanIntPaidAmt,
               LoanIntPaidDt,
               p_ACHTinNmb,
               p_ACTNCD,
               p_LoanAppCDCGntyAmt,
               p_LoanAppCDCNetDbentrAmt,
               p_LoanAppCDCFundFeeAmt,
               p_LoanAppCDCSeprtPrcsFeeInd,
               p_LoanAppCDCPrcsFeeAmt,
               p_LoanAppCDCClsCostAmt,
               p_LoanAppCDCOthClsCostAmt,
               p_LoanAppCDCUndrwtrFeeAmt,
               p_LoanAppContribPct,
               p_LoanAppContribAmt,
               p_LOANSTATCMNTCD,
               p_LOANGNTYDFRTODT,
               p_LOANGNTYDFRDMNTHSNMB,
               p_LOANGNTYDFRDGRSAMT,
               LOANDISASTRAPPFEECHARGED,
               LOANDISASTRAPPDCSN,
               LOANASSOCDISASTRAPPNMB,
               LOANASSOCDISASTRLOANNMB,
               p_LoanPymntSchdlFreq,
               FININSTRMNTTYPIND,
               p_LASTUPDTUSERID,
               SYSDATE,
               p_LoanAppMatExtDt,
               p_LOAN1201NOTCIND,
               p_LOANSERVGRPCD,
               p_FREEZETYPCD,
               p_FREEZERSNCD,
               p_FORGVNESFREEZETYPCD,
               p_DisbDeadLnDt,
               p_LOANPYMTTRANSCD,
               LOANCOFSEFFDT,
               NVL (p_SBICLICNSNMB, SBICLICNSNMB),
               p_LOANAPPNETEARNIND,
               p_LOANPYMTINSTLMNTTYPCD,
               p_LOANPYMNINSTLMNTFREQCD,
               p_LOANTOTDEFNMB,
               p_LOANTOTDEFMONMB,
               p_LOANGNTYNOTEDT,
               p_LOANGNTYMATSTIND,
               p_LOANTRANSSEQNMB,
               p_LOANPYMTREPAYINSTLTYPCD,
               LoanAppIntDtlCd,
               LOANLNDRASSGNLTRDT,
               p_LoanAppBalToBorrAmt,
               p_LoanAppCDCGrossDbentrAmt,
               p_LoanGntyNoteIntRt,
               p_LoanGntyNoteMntlyPymtAmt,
               p_LoanAppRevlMoIntQty,
               p_LoanAppAmortMoIntQty,
               p_LoanGntyFeeBillAdjAmt,
               p_LoanGntyCorrespLangCd,
               p_LoanAppMonthPayroll,
               p_LoanGntyPayRollChngInd,
               LOANAPPSCHEDCYR,
               LOANAPPGROSSINCOMEAMT
        FROM LoanGntyTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANPRCSPNDLOANCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANPRCSPNDLOANCSP (
   p_LoanAppNmb    NUMBER := NULL,
   p_LoanAppNm     VARCHAR2 := NULL)
AS
   /*
   Date     :
   Programmer   :
   Remarks      :
   ***********************************************************************************
   Revised By      Date        Comments
   ----------     --------    ----------------------------------------------------------
   APK -- 01/14/2011 -- Modified the procedure to make the called procedure Loan.LoanSrvsFinUpdCSP, so that the parameters being passed and the actual parameters that the procedure Loan.LoanSrvsFinUpdCSP ecpects matches. The parameters were coded incorrectly.
   APK -- 03/21/2011 -- Modified to add new column LoanAppMatExtDt..
   APK     03/22/2011  Removed the LOANGNTYFEEREBATEAMT   changes that were added as
                             they there not needed.
   APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. also added Loan1201NotcInd , LOANSERVGRPCD previously added to the Gnty table. added to the call to LoanGntyTbl update statement.
   APK -- 10/19/2011 -- Modified to change Loan1201NotcInd data type from Char  to Number.
   APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
   SP  -- 1/9/2012   --  MODIFIED TO ADD NEW COLUMN LOANPYMTTRANSCD
   APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
   SP  -- 3/5/2012   -- Modified to add new column loancofseffdt
   SP  -- 4/18/2012  -- Modified to add new column LoanAppNetEarnInd
   SP  -- 04/26/2012 -- Modified as ACTIONCD was renamed to ACTNCD
   SP --- 05/01/2012 -- Modified  to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
   LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
   APK -- 05/16/2012 -- Modified as per the below requirement, to add 'RETURN' statement to abort when there is no record in the LoanGntyPreTbl:
   'In the case of disaster disburements, servicing web service is not writing to the PreTables, as there is not a rule of two. Therefore, we need to have loan.LOANPRCSPNDLOANCSP modified to check to see if there exists a record in the LoanGntyPreTbl prior to processing. If there is no data in the LoanGntyPreTbl, the procedure should exit.'
   RGG -- 06/11/2012 -- Modified to add p_LoanAppNm and update it based on logic "if input parameter loannm passed and not null, then update based on loanappnmb, otherwise get
                           from  loan.loangntytbl"
   SP -- 06/12/2012  -- Modified  to retrieve LoanOutBalAmt and LoanOutBalDt from LoanGntyPreTbl and pass them on the parameter list in the call to LoanSrvsFinUpdCSP.
   RSURAPA -- 05/16/2013 -- Added a variable v_NOTEDT, retrived the NOTEDT value from the LoanGntyPreTbl and added the variable as a parameter for the call to the LoanSrvsFinUpdCSP procedure.
   RSURAPA -- 05/22/2013 -- The notedt column was changed to loangntynotedt in the loangntytbl, changed all the reference of notedt in the procedure to refelect the change.
   RGG -- 03/21/2014 -- Deleted references to LOANMAILSTR1NM,LOANMAILSTR2NM,LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,
                         LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD
   SP  -- 04/10/2014 -- Added REPAYINSTLTYPCD
   RGG -- 04/05/2016 -- Added LOANLNDRASSGNLTRDT
   NK  -- 09/07/2016--OPMSDEV 1099-- added LoanProcdRefDescTxt,LoanProcdPurAgrmtDt,LoanProcdPurIntangAssetAmt,LoanProcdPurIntangAssetDesc,LoanProcdPurStkHldrNm
   NK-- 10/25/2016 --OPMSDEV 1216---Added NCAIncldInd,StkPurCorpText
   NK--07/20/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt
   BR--12/05/2018--CAFSOPER 2235-- corrected LastUpdtUserId update for LoanGntyTbl and LoanAudtLogTbl (set by trigger)
   SL--02/01/2021 SODSTORY 447/448 added column LoanGntyPayRollChngInd

   ********************************************************************************
   */
   v_CohortCd                       CHAR (20);
   v_RetVal                         NUMBER (10, 0);
   v_RetTxt                         VARCHAR2 (255);
   v_LoanAppSBAGntyPct              NUMBER (6, 3);
   v_LoanCurrAppvAmt                NUMBER (19, 4);
   v_LoanCurrAppvAmt_old            NUMBER (19, 4);
   v_LoanLastAppvDt                 DATE;
   v_LoanAppRqstMatMoQty            NUMBER (3, 0);
   v_LoanAppRecvDt                  DATE;
   v_LoanAppFundDt                  DATE;
   v_LoanStatCd                     NUMBER (3, 1);
   v_LoanStatCd_old                 NUMBER (3, 1);
   v_LoanStatDt                     DATE;
   v_FyLoanTransChngLoanAmt         NUMBER (19, 4);
   v_LoanTransFisclYrNmb            NUMBER (4, 0);
   v_LastUpdtUserId                 CHAR (15);
   v_LoanAppAdjPrdCd                CHAR (1);
   v_LoanAppPymtAmt                 NUMBER (19, 4);
   v_LoanAppInjctnInd               CHAR (1);
   v_LoanCollatInd                  CHAR (1);
   v_LoanAppDisasterCntrlNmb        VARCHAR2 (20);
   v_LoanAppNetExprtAmt             NUMBER (19, 4);
   v_LoanAppCDCGntyAmt              NUMBER (19, 4);
   v_LoanAppCDCNetDbentrAmt         NUMBER (19, 4);
   v_LoanAppCDCFundFeeAmt           NUMBER (19, 4);
   v_LoanAppCDCSeprtPrcsFeeInd      CHAR (1);
   v_LoanAppCDCPrcsFeeAmt           NUMBER (19, 4);
   v_LoanAppCDCClsCostAmt           NUMBER (19, 4);
   v_LoanAppCDCOthClsCostAmt           NUMBER (19, 4);
   v_LoanAppCDCUndrwtrFeeAmt        NUMBER (19, 4);
   v_LoanAppContribPct              NUMBER (6, 3);              --number(5,2);
   v_LoanAppContribAmt              NUMBER (19, 4);
   v_LoanAppMoIntQty                NUMBER (10, 0);
   v_LoanAppMatDt                   DATE;
   v_LoanAppOutPrgrmAreaOfOperInd   CHAR (1);
   v_LoanAppParntLoanNmb            CHAR (10);
   v_ACHRtngNmb                     VARCHAR2 (25);
   v_ACHAcctNmb                     VARCHAR2 (25);
   v_ACHAcctTypCd                   CHAR (1);
   v_ACHTinNmb                      CHAR (10);
   v_LoanAppFirstDisbDt             DATE;
   v_LoanTotUndisbAmt               NUMBER (19, 4);
   v_LoanProcdSeqNmb                NUMBER (3, 0);
   v_ProcdTypCd                     CHAR (1);
   v_LoanProcdTypCd                 CHAR (2);
   v_LoanProcdOthTypTxt             VARCHAR2 (80);
   v_LoanProcdAmt                   NUMBER (19, 4);
   v_LoanProcdCreatUserId           CHAR (15);
   v_LoanProcdCreatDt               DATE;
   v_ProcdSeqNmb                    NUMBER (3, 0);
   v_LoanHistrySeqNmb               NUMBER (10, 0);
   v_LoanTransSeqNmb                NUMBER;
   v_LoanCancelFyNmb                NUMBER;
   v_RetStat                        NUMBER;
   v_temp                           NUMBER;
   v_temp1                          NUMBER;
   v_temp2                          NUMBER;
   v_LOANSTATCMNTCD                 CHAR (2);
   v_LOANGNTYDFRTODT                DATE;
   v_LOANGNTYDFRDMNTHSNMB           NUMBER (5);
   v_LOANGNTYDFRDGRSAMT             NUMBER (19, 4);
   v_LoanNextInstlmntDueDt          DATE;
   v_LoanPymntSchdlFreq             VARCHAR2 (24);
   v_LOANDISASTRAPPFEECHARGED       NUMBER (19, 4);
   v_LOANDISASTRAPPDCSN             CHAR (15);
   v_LOANASSOCDISASTRAPPNMB         CHAR (30);
   v_LOANASSOCDISASTRLOANNMB        CHAR (10);
   v_LoanAppMatExtDt                DATE;
   v_loanhighappvamt                NUMBER (15, 2);
   v_reinstateind                   CHAR (1);
   v_increaseind                    CHAR (1);
   v_Loan1201NotcInd                NUMBER (3, 0);
   v_LOANSERVGRPCD                  CHAR (1);
   v_FREEZETYPCD                    CHAR (1);
   v_FREEZERSNCD                    NUMBER (3, 0);
   v_FORGVNESFREEZETYPCD            NUMBER (3, 0);
   v_DisbDeadLnDt                   DATE;
   v_LOANPYMTTRANSCD                NUMBER (3);
   v_LOANDISASTRAPPNMB              CHAR (30);
   v_LOANCOFSEFFDT                  DATE;
   v_SBICLICNSNMB                   CHAR (8);
   v_LOANAPPNETEARNIND              CHAR (1);
   v_LOANPYMTINSTLMNTTYPCD          CHAR (1);
   v_LOANPYMNINSTLMNTFREQCD         CHAR (1);
   v_LOANTOTDEFNMB                  NUMBER (3, 0);
   v_LOANTOTDEFMONMB                NUMBER (3, 0);
   v_LoanAppNm                      VARCHAR2 (80);
   v_LOANOUTBALAMT                  NUMBER (19, 4);
   v_LOANOUTBALDT                   DATE;
   v_EIDLAmt                        NUMBER (15, 2);
   v_PHYAmt                         NUMBER (15, 2);
   v_PrgmCd                         CHAR (1);
   v_LOANGNTYNOTEDT                 DATE;
   v_LOANGNTYMATSTIND               VARCHAR2 (1);
   v_LOANPYMTREPAYINSTLTYPCD        CHAR (1);
   v_LOANAPPINTDTLCD                VARCHAR2 (2);
   v_LOANLNDRASSGNLTRDT             DATE;
   v_LoanGntyNoteIntRt              NUMBER (6, 3);
   v_LoanGntyNoteMntlyPymtAmt       NUMBER (19, 4);
   v_LoanAppRevlMoIntQty            NUMBER;
   v_LoanAppAmortMoIntQty           NUMBER;
   v_LoanAppMonthPayroll            NUMBER;
   v_LoanGntyPayRollChngInd         CHAR;
/*
---05/04/2010 APK added new variable v_LOANSTATCMNTCD, and this is retrieved and passed to the procedure LOANSRVSFINUPDCSP.
RY--01/24/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
*/
BEGIN
   IF p_LoanAppNm IS NULL
   THEN
      SELECT LoanAppNm
        INTO v_LoanAppNm
        FROM loangntytbl a
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   ELSE
      v_LoanAppNm := p_LoanAppNm;
   END IF;

   BEGIN
      SELECT LoanStatCd, LoanCurrAppvAmt, PrgmCd
        INTO v_LoanStatCd_old, v_LoanCurrAppvAmt_old, v_PrgmCd
        FROM LoanGntyTbl a
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   BEGIN
      SELECT LoanAppSBAGntyPct,
             LoanCurrAppvAmt,
             LoanLastAppvDt,
             LoanAppRqstMatMoQty,
             LoanAppFundDt,
             LoanAppFundDt,
             LoanStatCd,
             LoanStatDt,
             a.LastUpdtUserId,                      --last update to pre table
             CohortCd,
             LoanAppCDCGntyAmt,
             LoanAppCDCNetDbentrAmt,
             LoanAppCDCFundFeeAmt,
             LoanAppCDCSeprtPrcsFeeInd,
             LoanAppCDCPrcsFeeAmt,
             LoanAppCDCClsCostAmt,
             LoanAppCDCOthClsCostAmt,
             LoanAppCDCUndrwtrFeeAmt,
             LoanAppContribPct,
             LoanAppContribAmt,
             LoanAppMatDt,
             LoanCurrAppvAmt - v_LoanCurrAppvAmt_old,
               EXTRACT (YEAR FROM LoanLastAppvDt)
             + CASE
                  WHEN EXTRACT (MONTH FROM LoanLastAppvDt) > 9 THEN 1
                  ELSE 0
               END,
             LoanTotUndisbAmt,
             LoanAppPymtAmt,
             LoanAppInjctnInd,
             LoanCollatInd,
             LoanAppDisasterCntrlNmb,
             LoanAppNetExprtAmt,
             LoanAppMoIntQty,
             LoanAppOutPrgrmAreaOfOperInd,
             LoanAppParntLoanNmb,
             ACHRtngNmb,
             ACHAcctNmb,
             ACHAcctTypCd,
             ACHTinNmb,
             LoanAppFirstDisbDt,
             a.LOANSTATCMNTCD,
             a.LOANGNTYDFRTODT,
             a.LOANGNTYDFRDMNTHSNMB,
             a.LOANGNTYDFRDGRSAMT,
             LoanNextInstlmntDueDt,
             LoanPymntSchdlFreq,
             LOANDISASTRAPPFEECHARGED,
             LOANDISASTRAPPDCSN,
             LOANASSOCDISASTRAPPNMB,
             LOANASSOCDISASTRLOANNMB,
             LoanAppMatExtDt,
             Loan1201NotcInd,
             LOANSERVGRPCD,
             FREEZETYPCD,
             FREEZERSNCD,
             FORGVNESFREEZETYPCD,
             DisbDeadLnDt,
             LOANPYMTTRANSCD,
             LOANDISASTRAPPNMB,
             LOANCOFSEFFDT,
             SBICLICNSNMB,
             LOANAPPNETEARNIND,
             LOANPYMTINSTLMNTTYPCD,
             LOANPYMNINSTLMNTFREQCD,
             LOANTOTDEFNMB,
             LOANTOTDEFMONMB,
             LOANOUTBALAMT,
             LOANOUTBALDT,
             LOANGNTYNOTEDT,
             LOANGNTYMATSTIND,
             LOANPYMTREPAYINSTLTYPCD,
             LOANAPPINTDTLCD,
             LOANLNDRASSGNLTRDT,
             LoanGntyNoteIntRt,
             LoanGntyNoteMntlyPymtAmt,
             LoanAppRevlMoIntQty,
             LoanAppAmortMoIntQty,
             LoanAppMonthPayroll,
             LoanGntyPayRollChngInd
        INTO v_LoanAppSBAGntyPct,
             v_LoanCurrAppvAmt,
             v_LoanLastAppvDt,
             v_LoanAppRqstMatMoQty,
             v_LoanAppRecvDt,
             v_LoanAppFundDt,
             v_LoanStatCd,
             v_LoanStatDt,
             v_LastUpdtUserId,
             v_CohortCd,
             v_LoanAppCDCGntyAmt,
             v_LoanAppCDCNetDbentrAmt,
             v_LoanAppCDCFundFeeAmt,
             v_LoanAppCDCSeprtPrcsFeeInd,
             v_LoanAppCDCPrcsFeeAmt,
             v_LoanAppCDCClsCostAmt,
             v_LoanAppCDCOthClsCostAmt,
             v_LoanAppCDCUndrwtrFeeAmt,
             v_LoanAppContribPct,
             v_LoanAppContribAmt,
             v_LoanAppMatDt,
             v_FyLoanTransChngLoanAmt,
             v_LoanTransFisclYrNmb,
             v_LoanTotUndisbAmt,
             v_LoanAppPymtAmt,
             v_LoanAppInjctnInd,
             v_LoanCollatInd,
             v_LoanAppDisasterCntrlNmb,
             v_LoanAppNetExprtAmt,
             v_LoanAppMoIntQty,
             v_LoanAppOutPrgrmAreaOfOperInd,
             v_LoanAppParntLoanNmb,
             v_ACHRtngNmb,
             v_ACHAcctNmb,
             v_ACHAcctTypCd,
             v_ACHTinNmb,
             v_LoanAppFirstDisbDt,
             v_LOANSTATCMNTCD,
             v_LOANGNTYDFRTODT,
             v_LOANGNTYDFRDMNTHSNMB,
             v_LOANGNTYDFRDGRSAMT,
             v_LoanNextInstlmntDueDt,
             v_LoanPymntSchdlFreq,
             v_LOANDISASTRAPPFEECHARGED,
             v_LOANDISASTRAPPDCSN,
             v_LOANASSOCDISASTRAPPNMB,
             v_LOANASSOCDISASTRLOANNMB,
             v_LoanAppMatExtDt,
             v_Loan1201NotcInd,
             v_LOANSERVGRPCD,
             v_FREEZETYPCD,
             v_FREEZERSNCD,
             v_FORGVNESFREEZETYPCD,
             v_DisbDeadLnDt,
             v_LOANPYMTTRANSCD,
             v_LOANDISASTRAPPNMB,
             v_LOANCOFSEFFDT,
             v_SBICLICNSNMB,
             v_LOANAPPNETEARNIND,
             v_LOANPYMTINSTLMNTTYPCD,
             v_LOANPYMNINSTLMNTFREQCD,
             v_LOANTOTDEFNMB,
             v_LOANTOTDEFMONMB,
             v_LOANOUTBALAMT,
             v_LOANOUTBALDT,
             v_LOANGNTYNOTEDT,
             v_LOANGNTYMATSTIND,
             v_LOANPYMTREPAYINSTLTYPCD,
             v_LOANAPPINTDTLCD,
             v_LOANLNDRASSGNLTRDT,
             v_LoanGntyNoteIntRt,
             v_LoanGntyNoteMntlyPymtAmt,
             v_LoanAppRevlMoIntQty,
             v_LoanAppAmortMoIntQty,
             v_LoanAppMonthPayroll,
             v_LoanGntyPayRollChngInd
        --from LoanGntyPreTbl where LoanAppNmb = v_LoanAppNmb
        FROM LoanGntyPreTbl a
             LEFT OUTER JOIN LoanGntyProjTbl b
                ON (a.LoanAppNmb = b.LoanAppNmb)
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN;
   END;

   Loan.LoanSrvsFinUpdCSP (p_LoanAppNmb,
                           v_LoanStatCd,
                           v_LoanCurrAppvAmt,
                           v_LoanAppSBAGntyPct,
                           v_LoanAppRqstMatMoQty,
                           v_LoanAppAdjPrdCd,
                           v_LoanAppPymtAmt,
                           v_LoanAppInjctnInd,
                           v_LoanCollatInd,
                           v_LoanAppDisasterCntrlNmb,
                           v_LoanAppNetExprtAmt,
                           v_LoanAppCDCGntyAmt,
                           v_LoanAppCDCNetDbentrAmt,
                           v_LoanAppCDCFundFeeAmt,
                           v_LoanAppCDCSeprtPrcsFeeInd,
                           v_LoanAppCDCPrcsFeeAmt,
                           v_LoanAppCDCClsCostAmt,
                           v_LoanAppCDCOthClsCostAmt,
                           v_LoanAppCDCUndrwtrFeeAmt,
                           v_LoanAppContribPct,
                           v_LoanAppContribAmt,
                           v_LoanAppMoIntQty,
                           v_LoanAppMatDt,
                           v_LastUpdtUserId,
                           v_LoanAppOutPrgrmAreaOfOperInd,
                           v_LoanAppParntLoanNmb,
                           v_ACHRtngNmb,
                           v_ACHAcctNmb,
                           v_ACHAcctTypCd,
                           v_ACHTinNmb,
                           v_LoanAppFirstDisbDt,
                           v_LoanTotUndisbAmt,
                           v_LOANSTATCMNTCD,
                           v_LOANGNTYDFRTODT,
                           v_LOANGNTYDFRDMNTHSNMB,
                           v_LOANGNTYDFRDGRSAMT,
                           v_LOANDISASTRAPPFEECHARGED,
                           v_LOANDISASTRAPPDCSN,
                           v_LOANASSOCDISASTRAPPNMB,
                           v_LOANASSOCDISASTRLOANNMB,
                           v_LoanNextInstlmntDueDt,
                           v_LoanPymntSchdlFreq,
                           v_LoanAppMatExtDt,
                           v_Loan1201NotcInd,
                           v_LOANSERVGRPCD,
                           v_FREEZETYPCD,
                           v_FREEZERSNCD,
                           v_FORGVNESFREEZETYPCD,
                           v_DisbDeadLnDt,
                           v_LOANPYMTTRANSCD,
                           v_LOANDISASTRAPPNMB,
                           v_LOANCOFSEFFDT,
                           v_SBICLICNSNMB,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           v_LOANAPPNETEARNIND,
                           v_LOANPYMTINSTLMNTTYPCD,
                           v_LOANPYMNINSTLMNTFREQCD,
                           v_LOANTOTDEFNMB,
                           v_LOANTOTDEFMONMB,
                           v_LoanAppNm,
                           v_LOANOUTBALAMT,
                           v_LOANOUTBALDT,
                           v_LOANGNTYNOTEDT,
                           v_LOANGNTYMATSTIND,
                           v_LOANPYMTREPAYINSTLTYPCD,
                           v_LOANAPPINTDTLCD,
                           v_LOANLNDRASSGNLTRDT,
                           v_LoanGntyNoteIntRt,
                           v_LoanGntyNoteMntlyPymtAmt,
                           v_LoanAppRevlMoIntQty,
                           v_LoanAppAmortMoIntQty,
                           v_LoanAppMonthPayroll,
                           v_LoanGntyPayRollChngInd);


   SELECT COUNT (*)
     INTO v_temp1
     FROM LoanGntyPreProcdTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF v_temp1 > 0
   THEN                                        --or v_LoanCurrAppvAmt = 0 then
      BEGIN
         --create history for proceed update
         Loan.LoanGntyProcdUpdCSP (p_LoanAppNmb, v_LastUpdtUserId);

         DELETE LoanGntyProcdTbl
          WHERE LoanAppNmb = p_LoanAppNmb;

         INSERT INTO LoanGntyProcdTbl (LoanProcdSeqNmb,
                                       LoanAppNmb,
                                       ProcdTypCd,
                                       LoanProcdTypCd,
                                       LoanProcdOthTypTxt,
                                       LoanProcdAmt,
                                       LoanProcdCreatUserId,
                                       LoanProcdCreatDt,
                                       LoanProcdRefDescTxt,
                                       LoanProcdPurAgrmtDt,
                                       LoanProcdPurIntangAssetAmt,
                                       LoanProcdPurIntangAssetDesc,
                                       LoanProcdPurStkHldrNm,
                                       NCAIncldInd,
                                       StkPurCorpText)
            SELECT LoanProcdSeqNmb,
                   LoanAppNmb,
                   ProcdTypCd,
                   LoanProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   LoanProcdCreatUserId,
                   LoanProcdCreatDt,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanGntyPreProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

         DELETE LoanGntyPreProcdTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      END;
   END IF;

   DELETE LoanGntyPreTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   SELECT PrgmCd, LoanStatCd
     INTO v_PrgmCd, v_LoanStatCd
     FROM loan.loangntytbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF v_PrgmCd = 'H'
   THEN
      SELECT SUM (
                CASE WHEN LOANPROCDTYPCD != '04' THEN LOANPROCDAMT ELSE 0 END),
             SUM (
                CASE WHEN LOANPROCDTYPCD = '04' THEN LOANPROCDAMT ELSE 0 END)
        INTO v_PhyAmt, v_EIDLAmt
        FROM loan.loangntyprocdtbl
       WHERE LoanAppNmb = p_LoanAppNmb;

      UPDATE loan.loangntytbl
         SET LoanGntyEIDLAmt =
                CASE
                   WHEN LoanGntyEIDLAmt > v_EIDLAmt THEN LoanGntyEIDLAmt
                   ELSE v_EIDLAmt
                END,
             LoanGntyEIDLCnclAmt =
                  CASE
                     WHEN LoanGntyEIDLAmt > v_EIDLAmt THEN LoanGntyEIDLAmt
                     ELSE v_EIDLAmt
                  END
                - CASE WHEN v_LoanStatCd = 4 THEN 0 ELSE v_EIDLAmt END,
             LoanGntyPhyAmt =
                CASE
                   WHEN LoanGntyPhyAmt > v_PhyAmt THEN LoanGntyPhyAmt
                   ELSE v_PhyAmt
                END,
             LoanGntyPhyCnclAmt =
                  CASE
                     WHEN LoanGntyPhyAmt > v_PhyAmt THEN LoanGntyPhyAmt
                     ELSE v_PhyAmt
                  END
                - CASE WHEN v_LoanStatCd = 4 THEN 0 ELSE v_PhyAmt END,
             LastUpdtUserId = v_LastUpdtUserId,
             LastUpdtDt = SYSDATE
       WHERE LoanAppNmb = p_LoanAppNmb;
   END IF;

   LoanChngLogInsCSP (p_LoanAppNmb,
                      'S',
                      v_LastUpdtUserId,
                      NULL,
                      NULL);
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANSRVSFINUPDCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANSRVSFINUPDCSP (
   p_LoanAppNmb                     NUMBER,
   p_LoanStatCd                     NUMBER,
   p_LoanCurrAppvAmt                NUMBER := NULL,
   p_LoanAppSBAGntyPct              NUMBER := NULL,
   p_LoanAppRqstMatMoQty            NUMBER := NULL,
   p_LoanAppAdjPrdCd                CHAR := NULL,
   p_LoanAppPymtAmt                 NUMBER := NULL,
   p_LoanAppInjctnInd               CHAR := NULL,
   p_LoanCollatInd                  CHAR := NULL,
   p_LoanAppDisasterCntrlNmb        VARCHAR2 := NULL,
   p_LoanAppNetExprtAmt             NUMBER := NULL,
   p_LoanAppCDCGntyAmt              NUMBER := NULL,
   p_LoanAppCDCNetDbentrAmt         NUMBER := NULL,
   p_LoanAppCDCFundFeeAmt           NUMBER := NULL,
   p_LoanAppCDCSeprtPrcsFeeInd      CHAR := NULL,
   p_LoanAppCDCPrcsFeeAmt           NUMBER := NULL,
   p_LoanAppCDCClsCostAmt           NUMBER := NULL,
   p_LoanAppCDCOthClsCostAmt        NUMBER := NULL,
   p_LoanAppCDCUndrwtrFeeAmt        NUMBER := NULL,
   p_LoanAppContribPct              NUMBER := NULL,
   p_LoanAppContribAmt              NUMBER := NULL,
   p_LoanAppMoIntQty                NUMBER := NULL,
   p_LoanAppMatDt                   DATE := NULL,
   p_LoanAppCreatUserId             CHAR := NULL,
   p_LoanAppOutPrgrmAreaOfOperInd   CHAR := NULL,
   p_LoanAppParntLoanNmb            CHAR := NULL,
   p_ACHRtngNmb                     VARCHAR2 := NULL,
   p_ACHAcctNmb                     VARCHAR2 := NULL,
   p_ACHAcctTypCd                   CHAR := NULL,
   p_ACHTinNmb                      CHAR := NULL,
   p_LoanAppFirstDisbDt             DATE := NULL,
   p_LoanTotUndisbAmt               NUMBER := NULL,
   p_LOANSTATCMNTCD                 CHAR := NULL,
   p_LOANGNTYDFRTODT                DATE := NULL,
   p_LOANGNTYDFRDMNTHSNMB           NUMBER := NULL,
   p_LOANGNTYDFRDGRSAMT             NUMBER := NULL,
   p_LOANDISASTRAPPFEECHARGED       NUMBER := NULL,
   p_LOANDISASTRAPPDCSN             CHAR := NULL,
   p_LOANASSOCDISASTRAPPNMB         CHAR := NULL,
   p_LOANASSOCDISASTRLOANNMB        CHAR := NULL,
   p_LoanNextInstlmntDueDt          DATE := NULL,
   p_LoanPymntSchdlFreq             VARCHAR2 := NULL,
   p_LoanAppMatExtDt                DATE := NULL,
   p_Loan1201NotcInd                NUMBER := NULL,
   p_LOANSERVGRPCD                  VARCHAR2 := NULL,
   p_FREEZETYPCD                    CHAR := NULL,
   p_FREEZERSNCD                    NUMBER := NULL,
   p_FORGVNESFREEZETYPCD            NUMBER := NULL,
   p_DisbDeadLnDt                   DATE := NULL,
   p_LOANPYMTTRANSCD                NUMBER := NULL,
   p_LOANDISASTRAPPNMB              CHAR := NULL,
   p_LOANCOFSEFFDT                  DATE := NULL,
   p_SBICLICNSNMB                   CHAR := NULL,
   p_LOANMAILSTR1NM                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILSTR2NM                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILCTYNM                  VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILSTCD                   CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILZIPCD                  CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILZIP4CD                 CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILCNTRYCD                CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILSTNM                   VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILPOSTCD                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANAPPNETEARNIND              CHAR := NULL,
   p_LOANPYMTINSTLMNTTYPCD          CHAR := NULL,
   p_LOANPYMNINSTLMNTFREQCD         CHAR := NULL,
   p_LOANTOTDEFNMB                  NUMBER := NULL,
   p_LOANTOTDEFMONMB                NUMBER := NULL,
   p_LoanAppNm                      VARCHAR2 := NULL,
   p_LOANOUTBALAMT                  NUMBER := NULL,
   p_LOANOUTBALDT                   DATE := NULL,
   p_LOANGNTYNOTEDT                 DATE := NULL,
   p_LOANGNTYMATSTIND               VARCHAR2 := NULL,
   p_LOANPYMTREPAYINSTLTYPCD        CHAR := NULL,
   p_LOANAPPINTDTLCD                VARCHAR2 := NULL,
   p_LOANLNDRASSGNLTRDT             DATE := NULL,
   p_LoanGntyNoteIntRt              NUMBER := NULL,
   p_LoanGntyNoteMntlyPymtAmt       NUMBER := NULL,
   p_LoanAppRevlMoIntQty            NUMBER := NULL,
   p_LoanAppAmortMoIntQty           NUMBER := NULL,
   p_LoanAppMonthPayroll            NUMBER := NULL,
   p_LoanGntyPayRollChngInd         CHAR := NULL)
AS
   /*
   Date     :
   Programmer   :
   Remarks      :
   ***********************************************************************************
   Revised By      Date        Comments
   ----------     --------    ----------------------------------------------------------
   APK         05/04/2010   added new input variable v_LOANSTATCMNTCD
   APK         03/21/2011   Modified to add new column LoanAppMatExtDt..
   APK         03/22/2011  Removed the LOANGNTYFEEREBATEAMT   changes that were added as
                             they there not needed.
   APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. also added Loan1201NotcInd and LOANSERVGRPCD previously added to the Gnty table. added to the call to LoanGntyTbl update statement.
   APK -- 10/19/2011 -- Modified to change Loan1201NotcInd data type from Char  to Number.
   APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
   SP  -- 1/9/2012   --  MODIFIED TO ADD NEW COLUMN LOANPYMTTRANSCD
   APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
   SP --- 3/5/2012   -- Modified to add new column loancofseffdt
   SP --- 4/18/2012  -- Modified to add new column LoanAppNetEarnInd
   SP --- 05/01/2012 -- Modified to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
   LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
   RGG -- 06/11/2012 -- Modified to add p_LoanAppNm and update LoanGntyTbl. It is passed from LoanSrvsPrcsPndLoanCSP
   SP  -- 06/12/2012 -- Modified  to accept p_LoanOutBalAmt and p_LoanOutBalDt from the parameter list and to propagate those values to LoanGntyTbl.
   RSURAPA -- 05/16/2013  --  Added a new parameter p_nodedt and added nodedt = p_notedt to the update statement of the loangntytbl table.
   RSURAPA -- 05/22/2013  -- The notedt column was changed to loangntynote date in the loangntytbl.
   RSURAPA -- 06/17/2013 -- Added new column LOANGNTYMATSTIND in the loangntytbl, modified procedure to include the column as parameter and in the update statement.
   RGG -- 03/21/2014 -- Deleted references to LOANMAILSTR1NM,LOANMAILSTR2NM,LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,
                         LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD
   SP  -- 04/10/2014 -- Added REPAYINSTLTYPCD
   --RGG - 04/04/2016 Added LOANLNDRASSGNLTRDT
   NK--07/20/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt
   SS--12/04/2017 --OPSMDEV 1589-- Added code for voluntary termination and reinstatement of disbursed current loans
   RY--01/24/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
   SL--02/01/2021 SODSTORY 447/448 added column LoanGntyPayRollChngInd
********************************************************************************
   */
   v_TotDisbAmt               NUMBER;
   v_LoanStatDt               DATE;
   v_LoanLastAppvDt           DATE;
   v_OldFixVarInd             CHAR (1);
   v_LoanStatCd               NUMBER;
   v_OldStatCd                NUMBER;
   v_OldLoanAmt               NUMBER;
   v_OldInitlPct              NUMBER;
   v_OldExprtAmt              NUMBER;
   v_OldMatDt                 DATE;
   v_OldFirstDisbDt           DATE;
   v_LoanTypInd               CHAR (1);
   v_OldDisbInd               CHAR (1);
   v_Loan1502Ind              CHAR (1);
   v_DefaultDt                DATE;
   v_OldRtTypCd               CHAR (3);
   v_LoanIntPctChngInd        CHAR (1);
   v_RetVal                   NUMBER;
   v_RetTxt                   VARCHAR2 (255);
   v_LoanAppNm                VARCHAR2 (80);
   v_LoanTotUndisbAmt         NUMBER (15, 2);
   v_LOANAPPCDCGROSSDBENTRAMT NUMBER;
   v_LOANAPPBALTOBORRAMT      NUMBER;
   v_LoanOutBalAmt            NUMBER;
   v_LoanNmb                  CHAR (10);
BEGIN
   v_DefaultDt         := '01-JAN-1800';
   v_LoanStatCd        := p_LoanStatCd;
   v_LoanTotUndisbAmt  := p_LoanTotUndisbAmt;

   BEGIN
      SELECT LOANAPPCDCGROSSDBENTRAMT, LOANAPPBALTOBORRAMT
      INTO   v_LOANAPPCDCGROSSDBENTRAMT, v_LOANAPPBALTOBORRAMT
      FROM   LoanGntyPreTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN OTHERS THEN
         SELECT LOANAPPCDCGROSSDBENTRAMT, LOANAPPBALTOBORRAMT
         INTO   v_LOANAPPCDCGROSSDBENTRAMT, v_LOANAPPBALTOBORRAMT
         FROM   LoanGntyTbl
         WHERE  LoanAppNmb = p_LoanAppNmb;
   END;

   --v_DefaultDt := '01/01/1800';
   IF p_LoanAppNm IS NULL THEN
      SELECT LoanAppNm
      INTO   v_LoanAppNm
      FROM   loangntytbl a
      WHERE  a.LoanAppNmb = p_LoanAppNmb;
   ELSE
      v_LoanAppNm  := p_LoanAppNm;
   END IF;

   BEGIN
      SELECT LoanStatCd,
             LoanStatDt,
             LoanLastAppvDt,
             LoanTypInd,
             LoanDisbInd,
             LoanCurrAppvAmt,
             LoanAppMatDt,
             LoanNmb
      INTO   v_OldStatCd,
             v_LoanStatDt,
             v_LoanLastAppvDt,
             v_LoanTypInd,
             v_OldDisbInd,
             v_OldLoanAmt,
             v_OldMatDt,
             v_LoanNmb
      FROM   LOAN.LoanGntyTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   END;

   BEGIN
      SELECT NVL (LoanAppNetExprtAmt, 0)
      INTO   v_OldExprtAmt
      FROM   LoanGntyProjTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   END;

   v_LoanOutBalAmt     := p_LoanOutBalAmt;

   IF     p_LoanStatCd = 10
      AND v_OldStatCd != 10 THEN
      INSERT INTO gntyloanrpt.loantbl@cael1 (LoanNmb,
                                             LoadDt,
                                             DataSourcId,
                                             PrcsDt,
                                             LoanOutBalAmt,
                                             ETRANEXTDT,
                                             ErrTxt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
      VALUES      (v_LoanNmb,
                   SYSDATE,
                   'Colson',
                   TO_CHAR (SYSDATE, 'yyyymmdd'),
                   p_LoanOutBalAmt,
                   SYSDATE,
                   '<errors><validationerrors><error>1004</error></validationerrors></errors>',
                   p_LoanAppCreatUserId,
                   SYSDATE);

      --Voluntary Termination
      v_LoanOutBalAmt  := 0;
   END IF;

   IF     v_OldStatCd = 10
      AND p_LoanStatCd != 10 THEN
      --Reinstatement
      BEGIN
         SELECT LOANOUTBALAMT
         INTO   v_LoanOutBalAmt
         FROM   GNTYLOANRPT.LoanTbl@CAEL1
         WHERE      LoanNmb = v_LoanNmb
                AND ROWNUM = 1
                AND LOANOUTBALAMT > 0
                AND DATASOURCID = 'Colson'
                AND LoadDt = (SELECT MAX (LoadDt)
                              FROM   GNTYLOANRPT.LoanTbl@CAEL1 y
                              WHERE      y.LoanNmb = v_LoanNmb
                                     AND LOANOUTBALAMT > 0
                                     AND DATASOURCID = 'Colson');
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
   END IF;



   IF     p_LoanCurrAppvAmt = 0
      AND v_LoanStatCd = 4 THEN                                                         --Cancelation
      BEGIN
         v_LoanStatCd  := 4;
   		 v_LoanOutBalAmt := 0;
   		 
   		 IF v_OldLoanAmt > 0 THEN
   		 	v_LoanTotUndisbAmt := v_OldLoanAmt;
		 END IF;
      END;
   ELSIF     p_LoanCurrAppvAmt > 0
         AND v_LoanStatCd = 4 THEN                                                    --Reinstatement
      BEGIN
         v_LoanStatCd        := 1;
         v_LoanTotUndisbAmt  := p_LoanCurrAppvAmt;
      END;
   /*ELSIF p_LoanCurrAppvAmt > 0 and v_LoanStatCd = 2
     THEN                                                     --Voluntarily termination
        BEGIN
           v_LoanStatCd := 10;
           v_LOANOUTBALAMT :=0;
        END;

    ELSIF p_LoanCurrAppvAmt > 0 AND v_LoanStatCd = 10
     THEN                                                     --Reinstatement for Voluntarily termination
        BEGIN
           v_LoanStatCd := 2;
           v_LoanTotUndisbAmt := p_LoanCurrAppvAmt;
        END;*/


   ELSIF NVL (v_LoanTypInd, 'D') != 'G' THEN
      BEGIN
         --Retrieve total disbursement amount
         BEGIN
            SELECT NVL (SUM (LoanDisbAmt), 0)
            INTO   v_TotDisbAmt
            FROM   LoanDisbTbl
            WHERE  LoanAppNmb = p_LoanAppNmb;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;

         /* Check if loan status got changed based on disbursement amount and loan amount*/
         loan.LoanDisbStatCSP (v_TotDisbAmt, p_LoanCurrAppvAmt, v_LoanStatCd);
      /*if @error != 0
      begin
          IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
          RETURN @error
      end*/
      END;
   END IF;

   -- END IF;

   IF v_OldStatCd <> v_LoanStatCd THEN
      BEGIN
         v_LoanStatDt  := SYSDATE;
      END;
   END IF;


   /* v_LoanLastAppvDt : for increase/decrease, use current date */
   IF v_OldLoanAmt <> p_LoanCurrAppvAmt THEN
      BEGIN
         v_LoanLastAppvDt  := SYSDATE;
      END;
   END IF;

   loan.UpdtLoanFinCSP (
                        p_LoanAppNmb,
                        p_LoanAppSBAGntyPct,
                        p_LoanCurrAppvAmt,
                        v_LoanLastAppvDt,
                        p_LoanAppRqstMatMoQty,
                        NULL,
                        v_LoanStatCd,
                        v_LoanStatDt,
                        p_LoanAppCreatUserId,
                        v_RetVal,
                        v_RetTxt,
                        v_LoanAppNm
                       );

   /*IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/
   UPDATE LoanGntyTbl
   SET                                                         --LoanAppAdjPrdCd = p_LoanAppAdjPrdCd,
      LoanAppPymtAmt                    = p_LoanAppPymtAmt,
          LoanAppInjctnInd              = p_LoanAppInjctnInd,
          LoanCollatInd                 = p_LoanCollatInd,
          LoanAppDisasterCntrlNmb       = p_LoanAppDisasterCntrlNmb,
          LoanAppMoIntQty               = p_LoanAppMoIntQty,
          LoanAppMatDt                  = p_LoanAppMatDt,
          LastUpdtUserId                = p_LoanAppCreatUserId,
          LastUpdtDt                    = SYSDATE,
          LoanAppOutPrgrmAreaOfOperInd  = p_LoanAppOutPrgrmAreaOfOperInd,
          LoanAppParntLoanNmb           = p_LoanAppParntLoanNmb,
          ACHRtngNmb                    = p_ACHRtngNmb,
          ACHAcctNmb                    = p_ACHAcctNmb,
          ACHAcctTypCd                  = p_ACHAcctTypCd,
          ACHTinNmb                     = p_ACHTinNmb,
          LoanAppFirstDisbDt            = p_LoanAppFirstDisbDt,
          LoanTotUndisbAmt              = v_LoanTotUndisbAmt,
          LoanAppCDCGntyAmt             = p_LoanAppCDCGntyAmt,
          LoanAppCDCNetDbentrAmt        = p_LoanAppCDCNetDbentrAmt,
          LoanAppCDCFundFeeAmt          = p_LoanAppCDCFundFeeAmt,
          LoanAppCDCSeprtPrcsFeeInd     = p_LoanAppCDCSeprtPrcsFeeInd,
          LoanAppCDCPrcsFeeAmt          = p_LoanAppCDCPrcsFeeAmt,
          LoanAppCDCClsCostAmt          = p_LoanAppCDCClsCostAmt,
          LoanAppCDCOthClsCostAmt       = p_LoanAppCDCOthClsCostAmt,
          LoanAppCDCUndrwtrFeeAmt       = p_LoanAppCDCUndrwtrFeeAmt,
          LoanAppContribPct             = p_LoanAppContribPct,
          LoanAppContribAmt             = p_LoanAppContribAmt,
          LOANSTATCMNTCD                = p_LOANSTATCMNTCD,
          LOANGNTYDFRTODT               = p_LOANGNTYDFRTODT,
          LOANGNTYDFRDMNTHSNMB          = p_LOANGNTYDFRDMNTHSNMB,
          LOANGNTYDFRDGRSAMT            = p_LOANGNTYDFRDGRSAMT,
          LOANDISASTRAPPFEECHARGED      = p_LOANDISASTRAPPFEECHARGED,
          LOANDISASTRAPPDCSN            = p_LOANDISASTRAPPDCSN,
          LOANASSOCDISASTRAPPNMB        = p_LOANASSOCDISASTRAPPNMB,
          LOANASSOCDISASTRLOANNMB       = p_LOANASSOCDISASTRLOANNMB,
          LoanNextInstlmntDueDt         = p_LoanNextInstlmntDueDt,
          LoanPymntSchdlFreq            = p_LoanPymntSchdlFreq,
          LoanAppMatExtDt               = p_LoanAppMatExtDt,
          Loan1201NotcInd               = p_Loan1201NotcInd,
          LOANSERVGRPCD                 = p_LOANSERVGRPCD,
          FREEZETYPCD                   = p_FREEZETYPCD,
          FREEZERSNCD                   = p_FREEZERSNCD,
          FORGVNESFREEZETYPCD           = p_FORGVNESFREEZETYPCD,
          DisbDeadLnDt                  = p_DisbDeadLnDt,
          LOANPYMTTRANSCD               = p_LOANPYMTTRANSCD,
          LOANDISASTRAPPNMB             = p_LOANDISASTRAPPNMB,
          LOANCOFSEFFDT                 = p_LOANCOFSEFFDT,
          SBICLICNSNMB                  = p_SBICLICNSNMB,
          LOANAPPNETEARNIND             = p_LOANAPPNETEARNIND,
          LOANPYMTINSTLMNTTYPCD         = p_LOANPYMTINSTLMNTTYPCD,
          LOANPYMNINSTLMNTFREQCD        = p_LOANPYMNINSTLMNTFREQCD,
          LOANTOTDEFNMB                 = p_LOANTOTDEFNMB,
          LOANTOTDEFMONMB               = p_LOANTOTDEFMONMB,
          LOANSTATCD                    = v_LOANSTATCD,
          LOANSTATDT                    = v_LOANSTATDT,
          LOANAPPNM                     = v_LoanAppNm,                               --RGG 06-11-2012
          LOANOUTBALAMT                 = v_LOANOUTBALAMT,
          LOANOUTBALDT                  =
             CASE
                WHEN NVL (LOANOUTBALAMT, 0) = NVL (v_LOANOUTBALAMT, 0) THEN LOANOUTBALDT
                ELSE SYSDATE
             END,
          LOANGNTYNOTEDT                = p_LOANGNTYNOTEDT,
          LOANGNTYMATSTIND              = p_LOANGNTYMATSTIND,
          LOANPYMTREPAYINSTLTYPCD       = p_LOANPYMTREPAYINSTLTYPCD,
          LOANAPPINTDTLCD               = p_LOANAPPINTDTLCD,
          LOANLNDRASSGNLTRDT            = p_LOANLNDRASSGNLTRDT,
          LoanGntyNoteIntRt             = p_LoanGntyNoteIntRt,
          LoanGntyNoteMntlyPymtAmt      = p_LoanGntyNoteMntlyPymtAmt,
          LOANAPPCDCGROSSDBENTRAMT      = v_LOANAPPCDCGROSSDBENTRAMT,
          LOANAPPBALTOBORRAMT           = v_LOANAPPBALTOBORRAMT,
          LoanAppRevlMoIntQty           = p_LoanAppRevlMoIntQty,
          LoanAppAmortMoIntQty          = p_LoanAppAmortMoIntQty,
          LoanAppMonthPayroll           = p_LoanAppMonthPayroll,
          LoanGntyPayRollChngInd        = p_LoanGntyPayRollChngInd
   WHERE  LoanAppNmb = p_LoanAppNmb;

   /*SELECT @error = @@error
   IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/

   UPDATE LoanGntyProjTbl
   SET    LoanAppNetExprtAmt = p_LoanAppNetExprtAmt, LastUpdtUserId = p_LoanAppCreatUserId, LastUpdtDt = SYSDATE
   WHERE  LoanAppNmb = p_LoanAppNmb;

   /*SELECT @error = @@error
   IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/

   loan.LoanChngLogInsCSP (
                           p_LoanAppNmb,
                           'S',
                           p_LoanAppCreatUserId,
                           v_LoanIntPctChngInd
                          );

   IF    (    v_OldStatCd != 10
          AND p_LoanStatCd = 10)
      OR (    v_OldStatCd = 10
          AND p_LoanStatCd != 10) THEN
      --voluntary termination
      Loan.LoanChngLogInsCSP (
                              p_LoanAppNmb,
                              'R',
                              p_LoanAppCreatUserId,
                              NULL,
                              NULL
                             );
   END IF;
/*if @error != 0
begin
    IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
    RETURN @error
end*/
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPRINTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPRINTSELCSP (
    p_LoanAppNmb   IN     NUMBER := NULL,
    p_LoanNmb      IN     CHAR := NULL,
    p_SelCur1         OUT SYS_REFCURSOR,
    p_SelCur2         OUT SYS_REFCURSOR,
    p_SelCur3         OUT SYS_REFCURSOR,
    p_SelCur4         OUT SYS_REFCURSOR,
    p_SelCur5         OUT SYS_REFCURSOR,
    p_SelCur6         OUT SYS_REFCURSOR,
    p_SelCur7         OUT SYS_REFCURSOR,
    p_SelCur8         OUT SYS_REFCURSOR,
    p_SelCur9         OUT SYS_REFCURSOR,
    p_SelCur10        OUT SYS_REFCURSOR,                                    --
    p_SelCur11        OUT SYS_REFCURSOR,                                    --
    p_SelCur12        OUT SYS_REFCURSOR,                                    --
    p_SelCur13        OUT SYS_REFCURSOR,
    p_SelCur14        OUT SYS_REFCURSOR,                                    --
    p_SelCur15        OUT SYS_REFCURSOR,
    p_SelCur16        OUT SYS_REFCURSOR,
    p_SelCur17        OUT SYS_REFCURSOR,
    p_SelCur18        OUT SYS_REFCURSOR,
    p_selCur19        OUT SYS_REFCURSOR,
    p_SelCur20        OUT SYS_REFCURSOR,
    p_Selcur21        OUT SYS_REFCURSOR,
    p_Selcur22        OUT SYS_REFCURSOR,
    p_Selcur23        OUT SYS_REFCURSOR,
    p_Selcur24        OUT SYS_REFCURSOR)
AS
    /*
     Created by:
     Created on:
     Purpose:
     Parameters:
     Modified by: APK - 10/08/2010-- Removed column LOANLENDRSERVFEEPCT from the LoanGntyPartLendrTbl and LoanPartLendrHistryTbl table;
     Modified by: APK - 11/10/2010-- Modifed the query for cursor result set 2 as it was returing incorrect result set.
     APK         03/18/2011      Modified to add LoanAppMatExtDt
     APK          12/07/2011      Modified to comment the field "CAST(LoanCollatLqdValAmt AS NUMBER(15, 2)) LoanCollatLqdValNum" as LoanCollatLqdValAmt has been deleted from the underlying table.
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     SP :9/13/2012: Modified as LOANPRTCMNTSEQNMB is removed
     SP -- 11/26/2013 -- Removed references to columns :IMRtTypCd,LoanAppFixVarInd,
                         LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd frpm p_selcur2 and added it in p_selcur18
     RG -- 02/04/2014 -- Added new fields to result sets to address an issue with printing.
     RG -- 02/18/2014 -- Added p_SelCur19 to return the Associate Information.
     SB  --04/04/2014  -- Removed NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM columns
     SB  --04/08/2014  -- Added NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND columns
     NK--08/08/2016--OPSMDEV-1030-- Replaced LOANAFFILIND with LoanCntlIntInd and LoanCntlIntTyp
     RGG -- 09/12/2016 -- OPSMDEV 1103 Added new columns for making Use of Proceeds support Loan Auth LOANPROCDREFDESCTXT, LOANPROCDPURAGRMTDT,
                          LOANPROCDPURINTANGASSETAMT, LOANPROCDPURINTANGASSETDESC, LOANPROCDPURSTKHLDRNM
     NK--09/28/2016-- OPSMDEV-1181 added ,  LoanDisblInsurRqrdInd   ,  LoanInsurDisblDescTxt  ,  LoanLifeInsurRqrdInd    ,  LOANPRININSURAMT   ,  LoanPrinNoNCAEvdncInd   ,LoanComptitrNm
                            In Result set 7
     NK-- 10/03/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in   Result set 6 and 7.
     BR-- 10/24/2016 --OPSMDEV-1147 added GntySubCondTypCd to p_selcur11,  GntySubCondTypCd and LACGNTYCONDTYPCD to p_selcur12
     NK--10/27/2016--OPSMDEV 1171-- added NCAIncldInd,StkPurCorpText in result set 3.2
     NK-- 11/02/2016-- OPSMDEV 1180-- added ,a.LiabInsurRqrdInd,a.ProdLiabInsurRqrdInd,a.LiqLiabInsurRqrdInd,a.MalPrctsInsurRqrdInd ,a.OthInsurRqrdind ,a.OthInsurDescTxt,a.PymtLessCrdtInd ,a.InsurDisblDescTxt
                                             ,a.RecrdStmtTypCd ,a.RecrdStmtYrEndDayNmb in result set 5.1.
     NK--09/28/2016--OPSMDEV 1179-- added LoanInjctnFundTermYrNmb
     NK--11/10/2016--OPSMDEV 1229-- Added p_SelCur20 for Stand By Agreement.
     SS--11/17/16--OPSMDEV 1239 Added new columns for additional conditions collateral to cursor 5
     SS--11/21/2016--OPSMDEV     Added cursor 21 for repayment
     NK--12/09/2016-- OPSMDEV 1253 -- added LoanPrinInsurNm in result set 7
     SS--12/29/2016 Added Cohortcd,Undrwritngby,RecovInd,SBAOFC1NM to cursor 2
     BR--01/11/2017-- OPSMDEV-1286 -- added VetCertInd as selection from bus/per tbls
     RY--03/06/2017--OPSMDEV-1400-- Added LOANGNTYCSAAMTRCVD to p_SelCur2
     RY--03/06/2017--OPSMDEV-1400-- Added LoanPartLendrAcctNm,LoanPartLendrAcctNmb,LoanPartLendrTranCd,LoanPartLendrAttn,LndrLoanNmb to p_SelCur14
     BR--03/13/2017--OPSMDEV-1395-- Added  AdverChngInd  to p_SelCur10
     RY-- 03/28/2017-OPSMDEV1401--Removed EconDevObjctCd and changed the reference for EconDevObjctCd in  p_SelCur8
     RY -- 05/16/17 -- OPSMDEV-1433:: Added LOANINTADJPRDEFFDT in p_SelCur18
     SS--06/06/2017--OPSMDEV--1359 --Put outer join between LoanGntyProjTbl and loan.LoanGntyEconDevObjctChldTbl in p_selcur8
     SS--06/08/2017 OPSMDEV 1408 --Added ,LOANPARTLENDRPHYADDRCNTRYCD ,LOANPARTLENDRPHYADDRPOSTCD ,LOANPARTLENDRPHYADDRSTNM  to p_selcur 14
     BR -- 06/14/2017 -- OPSMDEV - 1359 -- Added p_SelCur23
     NK -- 07/19/2017--CAFSOPER 805 -- Added LoanAppBalToBorrAmt,LoanAppCDCGrossDbentrAmt in p_SelCur2
     NK--07/19/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt,LoanGntyNoteDt in p_SelCur2
     BR --11/14/2017--OPSMDEV 1577 -- Added LoanIntDtlGuarInd from Loan.loanintdtltbl p_SelCur18
     NK--11/14/2017--OPSMDEV 1558 Added BusAltPhnNmb, BusPrimEmailAdr,BusAltEmailAdr to borrower section
    NK-- 11/14/2017--OPSMDEV 1551 Added perAltPhnNmb ,perPrimEmailAdr,perAltEmailAdr,BusAltPhnNmb, BusPrimEmailAdr,BusAltEmailAdr and PerCitznShpCntNm p_selcur 12
    SS--11/15/2017--OPSMDEV 1567 Added LOANBUSESTDT to p_selcur 8 and BusPrimCntctNm,BusPrimCntctEmail to p_selcur 10;
    NK--12/13/2017--OPSMDEV 1605 Added LOANMAILSTR1NM, LOANMAILSTR2NM, LOANMAILCTYNM,LOANMAILSTCD,
    LOANMAILZIPCD, LOANMAILZIP4CD, LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD, LoanMailAddrsInvldInd to p_SelCur2
    RY--01/16/2018--OPSMDEV--1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty to p_SelCur2
    SS--03/23/2018 --OPSMDEV--1728--Added LoanGntyDbentrIntRt to p_SelCur2
    RY--04/20/2018--OPSMDEV-1786--Added WorkrsCompInsRqrdInd to p_SelCur10
    NK--07/31/2018-- CAFSOPER 1971 -- Added condition loanpartlendrtypcd = 'P'(participating lender) in getting sum ThirdPartyLoanAmt
    SS--08/06/2018--OPSMDEV 1882--Added p_SelCur24
    SS--10/26/2018--OPSMDEV 1966 Added LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd to p_selcur 2
     --RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns in LOANAGNTTBL
      RY--11/19/2018-OPSMDEV-2044:: Added OCADATAOUT to PII columns in LOANGNTYPARTLENDRTBL
       -- RY-11/27/2018--OPSMDEV-2044:: Added OCADATAOUT for PII columns in PERTBL
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt
       BR -- 02/01/2019--CAFSOPER2459-- Added  OcaDataOut() to missed Fields in p_SelCur11
 --RY--07/09/2019--OPSMDEV--2240--Added LoanGntycorrespLangCd to p_selcur 2
 SS--04/03/2020 Added LoanAppMonthPayroll to p_selcur 2 CARESACT - 66
 SS--04/26/2020 Added ACHAcctNmb,ACHRtngNmb,ACHAcctTypCd,ACHTinNmb to p_selcur 2
 SS--4/30/2020 Added Loan1502CertInd to p_selcur 2
 SS--5/7/2020 Added  lendrRebateFeeAmt,  LendrRebatefeePaidDt   to p_selcur 2 CARESACT 205
 SL--06/24/2020 CARESACT-595 Updated to use LoanGntyBusPrinTbl where it was BusPrinTbl
 SL--07/31/2020 CARESACT-595 Modifed to use LoanGntyBusTbl data instead of BusTbl when get loan level bus data
SS--08/03/2020 CARESACT 595 Modified P_selcur 10,11,12,19 to add loangntypertbl
JP -- 10/19/2020 - CAGSOPER 4001-- Added  and  a.LoanAppNmb = f.LoanAppNmb in Qry 11
SL--02/01/2021 SODSTORY 446 added column LoanGntyPayRollChngInd
JS--02/15/2021 Loan Authorization Modernization. Added LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to Guarantor - p_SelCur11
SL --03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 
*/
    v_LoanAppNmb   NUMBER := p_LoanAppNmb;
    p_sys_error    NUMBER := 0;
    p_error        NUMBER;
    p_ProcdTypCd   CHAR (1);
    p_SelCur       SYS_REFCURSOR;
BEGIN
    IF p_LoanAppNmb IS NULL
    THEN
        BEGIN
            SELECT LoanAppNmb
              INTO v_LoanAppNmb
              FROM LoanGntyTbl
             WHERE LoanNmb = p_LoanNmb;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                raise_application_error (-20002,
                                         '99999:Loan Number doesnt exist');
        END;
    END IF;

    BEGIN
        SELECT ProcdTypCd
          INTO p_ProcdTypCd
          FROM LoanGntyTbl a, sbaref.PrgrmValidTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.PrgmCd = b.PrgrmCd
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            p_ProcdTypCd := NULL;
    END;

    OPEN p_SelCur1 FOR                                        -- result set 1:
        /* Special purpose */
        SELECT b.SpcPurpsLoanCd, b.SpcPurpsLoanDesc
          FROM LoanGntySpcPurpsTbl a, sbaref.SpcPurpsLoanTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.SpcPurpsLoanCd = b.SpcPurpsLoanCd;

    OPEN p_SelCur2 FOR                                        -- result set 3:
        /* B. Loan Summary */
        /*APK -- 11/10/2010 modified below query*/
        SELECT LoanAppNm,
               a.PrgmCd,
               c.PrgrmDesc,
               a.PrcsMthdCd,
               b.PrcsMthdDesc,
               CAST (LoanInitlAppvAmt AS NUMBER (15, 2))
                   LoanInitlAppvAmt,
               CAST (LoanCurrAppvAmt AS NUMBER (15, 2))
                   LoanCurrAppvAmt,
               CAST (LoanAppRqstAmt AS NUMBER (15, 2))
                   LoanAppRqstNum,
               h.LoanTransMatMoQty
                   AS LoanInitMatMoQty,
               CAST (h.LoanTransChngGntyFeeAmt AS NUMBER (15, 2))
                   LoanInitGntyFeeAmt,
               h.LoanTransSBAGntyPct
                   AS LoanInitSBAGntyPct,
               LoanAppSBAGntyPct,
               LoanAppRqstMatMoQty,
               LoanAppEPCInd,
               CAST (LoanAppPymtAmt AS NUMBER (15, 2))
                   AS LoanAppPymtNum,
               LoanAppFullAmortPymtInd,
               LoanAppMoIntQty,
               LoanAppLifInsurRqmtInd,
               LoanAppRcnsdrtnInd,
               LoanAppInjctnInd,
               LoanAppEligEvalInd,
               LoanAppNewBusCd,
               BusAgeDesc
                   AS LoanAppNewBusTxt,
               LoanAppDisasterCntrlNmb,
               LOANDISASTRAPPFEECHARGED,
               LOANDISASTRAPPDCSN,
               LOANASSOCDISASTRAPPNMB,
               LOANASSOCDISASTRLOANNMB,
               LOANPYMNTSCHDLFREQ,
               NVL (
                   (SELECT CAST (SUM (LoanPartLendrAmt) AS NUMBER (15, 2))
                      FROM loan.LoanGntyPartLendrTbl
                     WHERE     LoanAppNmb = p_LoanAppNmb
                           AND LOANPARTLENDRTYPCD = 'P'),
                   0)
                   AS ThirdPartyLoanAmt,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanAppRecvDt,
               LoanAppEntryDt,
               CAST (LoanInitlAppvAmt AS NUMBER (15, 2))
                   AS LoanAppSBAAppvNum,
               LoanAppDisasterCntrlNmb,
               CAST (LoanOutBalAmt AS NUMBER (15, 2))
                   AS LoanOutBalNum,
               LoanOutBalDt,
               LoanInitlAppvDt,
               LoanLastAppvDt,
               g.LoanStatCd,
               g.LoanStatDescTxt,
               LoanStatDt,
               LoanAppOrigntnOfcCd,
               LoanAppPrcsOfcCd,
               LoanAppServOfcCd,
               LoanAppFundDt,
               LoanNmb,
               LoanGntyFeeAmt,
               a.PrtId,
               a.LocId,
               LoanAppPrtAppNmb,
               LoanAppPrtLoanNmb,
               LoanCollatInd,
               LoanAppFirstDisbDt,
               LoanAppMatDt,
               LoanSrvsLocId,
               PrtLocNm,
               LoanTotUndisbAmt,
               LoanNextInstlmntDueDt,
               LoanDisbInd,
               LoanOutBalAmt,
               LoanSoldScndMrktInd,
               CAST (LoanAppCDCGntyAmt AS NUMBER (15, 2))
                   AS LoanAppCDCGntyAmt,
               CAST (LoanAppCDCNetDbentrAmt AS NUMBER (15, 2))
                   LoanAppCDCNetDbentrAmt,
               CAST (LoanAppCDCFundFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCFundFeeAmt,
               LoanAppCDCSeprtPrcsFeeInd,
               CAST (LoanAppCDCPrcsFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCPrcsFeeAmt,
               CAST (LoanAppCDCClsCostAmt AS NUMBER (15, 2))
                   LoanAppCDCClsCostAmt,
               CAST (LoanAppCDCOthClsCostAmt AS NUMBER (15, 2))
                   LoanAppCDCOthClsCostAmt,
               CAST (LoanAppCDCUndrwtrFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCUndrwtrFeeAmt,
               LoanAppContribPct,
               CAST (LoanAppContribAmt AS NUMBER (15, 2))
                   LoanAppContribAmt,
               a.FININSTRMNTTYPIND,
               a.NOTEPARENTLOANNMB,
               a.NOTEGENNEDIND,
               a.LOANPURCHSDIND,
               a.LoanAppMatExtDt,
               a.LOANDISASTRAPPNMB,
               a.LoanAppLSPHQLocId,
               a.LOANGNTYMATSTIND,
               a.Cohortcd,
               a.Undrwritngby,
               a.RecovInd,
               (SELECT INITCAP (d.Sbaofc1Nm)
                  FROM SBAREF.SBAOFCTBL d
                 WHERE a.LoanAppServOfcCd = d.SBAOFCCD)
                   AS SBAOFC1NM,
               (SELECT e.LOAN_GNTY_MAT_ST_DESC_TXT
                  FROM SBAREF.LOANMATSTINDCDTBL e
                 WHERE a.LOANGNTYMATSTIND = e.LOAN_GNTY_MAT_ST_IND)
                   AS LOANGNTYMATSTDESCTXT,
               (SELECT P.PRTLOCNM
                  FROM PARTNER.PRTLOCTBL p
                 WHERE P.LOCID = a.LoanAppLSPHQLocId)
                   LSPLocNm,
               LOANGNTYCSAAMTRCVD,
               a.LoanAppBalToBorrAmt,
               a.LoanAppCDCGrossDbentrAmt,
               a.LoanGntyNoteDt,
               a.LoanGntyNoteIntRt,
               a.LoanGntyNoteMntlyPymtAmt,
               a.LOANMAILSTR1NM,
               a.LOANMAILSTR2NM,
               a.LOANMAILCTYNM,
               a.LOANMAILSTCD,
               a.LOANMAILZIPCD,
               a.LOANMAILZIP4CD,
               a.LOANMAILCNTRYCD,
               a.LOANMAILSTNM,
               a.LOANMAILPOSTCD,
               a.LoanMailAddrsInvldInd,
               a.LoanAppRevlMoIntQty,
               a.LoanAppAmortMoIntQty,
               a.LoanGntyDbentrIntRt,
               a.LoanAgntInvlvdInd,
               a.LoanAppExtraServFeeAMT,
               a.LoanAppExtraServFeeInd,
               a.LoanGntycorrespLangCd,
               a.LoanAppMonthPayroll,
               a.LoanGntyPayRollChngInd,
               a.ACHAcctNmb,
               a.ACHRtngNmb,
               a.ACHAcctTypCd,
               a.ACHTinNmb,
               a.Loan1502CertInd,
               a.lendrRebateFeeAmt,
               a.LendrRebatefeePaidDt,
               a.LOANAPPSCHEDCYR, 
               a.LOANAPPGROSSINCOMEAMT
          FROM loan.LoanGntyTbl  a
               LEFT OUTER JOIN sbaref.PrcsMthdTbl b
                   ON (a.PrcsMthdCd = b.PrcsMthdCd)
               LEFT OUTER JOIN sbaref.PrgrmTbl c ON (a.PrgmCd = c.PrgrmCd)
               LEFT OUTER JOIN sbaref.BusAgeCdTbl f
                   ON (a.LoanAppNewBusCd = f.BusAgeCd)
               LEFT OUTER JOIN sbaref.LoanStatCdTbl g
                   ON (a.LoanStatCd = g.LoanStatCd)
               LEFT OUTER JOIN loan.LoanTransTbl h
                   ON (a.LoanAppNmb = h.LoanAppNmb AND h.LoanTransTypCd = 1)
               LEFT OUTER JOIN partner.LocSrchTbl l
                   ON (a.LoanSrvsLocId = l.LocId)
         WHERE a.LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur3 FOR                                      -- result set 3.1:
                                                               /* Injection */
        SELECT D.InjctnTypCd,
               r.InjctnTypTxt,
               CASE WHEN D.InjctnTypCd = 'O' THEN '~' ELSE r.InjctnTypCd END
                   DisplayOrder,
               CAST (D.LoanInjctnAmt AS NUMBER (12, 2))
                   LoanInjctnNum,
               D.LoanInjctnOthDescTxt,
               D.LoanInjctnFundTermYrNmb
          FROM LoanGntyInjctnTbl D, sbaref.InjctnTypCdTbl r
         WHERE D.LoanAppNmb = p_LoanAppNmb AND D.InjctnTypCd = r.InjctnTypCd
        UNION ALL
        SELECT a.InjctnTypCd,
               a.InjctnTypTxt,
               CASE WHEN a.InjctnTypCd = 'O' THEN '~' ELSE a.InjctnTypCd END
                   DisplayOrder,
               NULL,
               NULL,
               NULL
          FROM sbaref.InjctnTypCdTbl a
         WHERE     a.InjctnTypStrtDt <= SYSDATE
               AND (a.InjctnTypEndDt IS NULL OR a.InjctnTypEndDt >= SYSDATE)
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanGntyInjctnTbl z
                         WHERE     z.LoanAppNmb = p_LoanAppNmb
                               AND z.InjctnTypCd = a.InjctnTypCd)
        ORDER BY DisplayOrder;

    OPEN p_SelCur4 FOR                                      -- result set 3.2:
        /* C. Use of Proceeds */
        SELECT a.ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               CAST (a.LoanProcdAmt AS NUMBER (12, 2))     LoanProcdNum,
               a.LoanProcdOthTypTxt,
               a.LOANPROCDREFDESCTXT,
               a.LOANPROCDPURAGRMTDT,
               a.LOANPROCDPURINTANGASSETAMT,
               a.LOANPROCDPURINTANGASSETDESC,
               a.LOANPROCDPURSTKHLDRNM,
               a.NCAIncldInd,
               a.StkPurCorpText
          FROM Loan.LoanGntyProcdTbl a, sbaref.LoanProcdTypTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ProcdTypCd = b.ProcdTypCd
               AND a.LoanProcdTypCd = b.LoanProcdTypCd
        UNION ALL
        SELECT p_ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL
          FROM sbaref.LoanProcdTypTbl b
         WHERE     ProcdTypCd = p_ProcdTypCd
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanGntyProcdTbl a
                         WHERE     a.LoanAppNmb = p_LoanAppNmb
                               AND a.ProcdTypCd = b.ProcdTypCd
                               AND a.LoanProcdTypCd = b.LoanProcdTypCd)
               AND LoanProcdTypCdStrtDt <= SYSDATE
               AND (   LoanProcdTypCdEndDt IS NULL
                    OR LoanProcdTypCdEndDt + 1 > SYSDATE)
        ORDER BY LoanProcdTypCd;

    OPEN p_SelCur5 FOR                                        -- result set 4:
          /* D. Collateral */
          SELECT LoanCollatSeqNmb,
                 a.LoanCollatTypCd,
                 b.LoanCollatTypDescTxt,
                 a.LoanCollatDesc,
                 CAST (LoanCollatMrktValAmt AS NUMBER (15, 2))
                     LoanCollatMrktValNum,
                 LoanCollatOwnrRecrd,
                 LoanCollatSBALienPos,
                 a.LoanCollatValSourcCd,
                 c.LoanCollatValSourcDescTxt,
                 LoanCollatValDt,
                 LOANCOLLATSUBTYPCD,
                 SecurShrPariPassuInd                                     --SS
                                     ,
                 SecurShrPariPassuNonSBAInd,
                 SecurPariPassuLendrNm,
                 SecurPariPassuAmt,
                 SecurLienLimAmt,
                 SecurInstrmntTypCd,
                 SecurWaterRightInd,
                 SecurRentAsgnInd,
                 SecurSellerNm,
                 SecurPurNm,
                 SecurOwedToSellerAmt,
                 SecurCDCDeedInEscrowInd,
                 SecurSellerIntDtlInd,
                 SecurSellerIntDtlTxt,
                 SecurTitlSubjPriorLienInd,
                 SecurPriorLienTxt,
                 SecurPriorLienLimAmt,
                 SecurSubjPriorAsgnInd,
                 SecurPriorAsgnTxt,
                 SecurPriorAsgnLimAmt,
                 SecurALTATltlInsurInd,
                 SecurLeaseTermOverLoanYrNmb,
                 LandlordIntProtWaiverInd,
                 SecurDt,
                 SecurLessorTermNotcDaysNmb,
                 SecurDescTxt,
                 SecurPropAcqWthLoanInd,
                 SecurPropTypTxt,
                 SecurOthPropTxt,
                 SecurLienOnLqorLicnsInd,
                 SecurMadeYrNmb,
                 SecurLocTxt,
                 SecurOwnerNm,
                 SecurMakeNm,
                 SecurAmt,
                 SecurNoteSecurInd,
                 SecurStkShrNmb,
                 SecurLienHldrVrfyInd,
                 SecurTitlVrfyTypCd,
                 SecurTitlVrfyOthTxt,
                 FloodInsurRqrdInd,
                 REHazardInsurRqrdInd,
                 PerHazardInsurRqrdInd,
                 FullMarInsurRqrdInd,
                 EnvInvstgtSBAAppInd,
                 LeasePrmTypCd,
                 ApprTypCd,
                 CollatAppOrdDt,
                 (SELECT d.LAC_COND_TYP_DESC_TXT
                    FROM LOAN.LAC_COND_TYPCD_TBL d
                   WHERE a.LoanCollatSubTypCd = d.LAC_COND_TYP_CD)
                     AS LAC_COND_TYP_DESC_TXT,
                 (SELECT e.LAC_ALTA_PLCY_DESC_TXT
                    FROM LOAN.LAC_ALTA_PLCY_CD_TBL e
                   WHERE a.SecurTitlVrfyTypCd = e.LAC_ALTA_PLCY_CD)
                     AS LAC_ALTA_PLCY_DESC_TXT,
                 (SELECT z.leastypdesctxt
                    FROM sbaref.leastypcdtbl z
                   WHERE a.leaseprmtypcd = z.leastypcd)
                     AS LEASDESCTXT,
                 (SELECT g.Propapprsltypdesctxt
                    FROM sbaref.Propapprsltypcdtbl g
                   WHERE a.ApprTypcd = g.Propapprsltypcd)
                     AS APPRSLDESCTXT,
                 (SELECT f.collatstatdesctxt
                    FROM sbaref.collatstatcdtbl f
                   WHERE a.collatstatcd = f.collatstatcd)
                     AS COLLATSTATDESCTXT
            FROM LoanGntyCollatTbl           a,
                 sbaref.LoanCollatTypCdTbl   b,
                 sbaref.LoanCollatValSourcTbl c
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCollatTypCd = b.LoanCollatTypCd
                 AND a.LoanCollatValSourcCd = c.LoanCollatValSourcCd(+)
        ORDER BY LoanCollatSeqNmb;

    OPEN p_SelCur6 FOR                                      -- result set 4.1:
        /* Collateral Lien*/
        SELECT LoanCollatSeqNmb,
               LoanCollatLienSeqNmb,
               LoanCollatLienHldrNm,
               LoanCollatLienPos,
               CAST (LoanCollatLienBalAmt AS NUMBER (15, 2))
                   LoanCollatLienBalNum
          FROM LoanGntyCollatLienTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur7 FOR                                      -- result set 4.2:
                       /* E. Lender Comments */
                       SELECT LoanPrtCmntTxt
                         FROM LoanGntyPrtCmntTbl
                        WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur8 FOR                                        -- result set 5:
        /* G. Project Information*/
        SELECT a.LoanAppNmb,
               LoanAppProjStr1Nm,
               LoanAppProjStr2Nm,
               LoanAppProjCtyNm,
               LoanAppProjCntyCd,
               LoanAppProjStCd,
               LoanAppProjZipCd,
               LoanAppProjZip4Cd,
               LoanAppRuralUrbanInd,
               CAST (LoanAppNetExprtAmt AS NUMBER (15, 2))
                   LoanAppNetExprtAmt,
               LoanAppCurrEmpQty,
               LoanAppJobRtnd,
               LoanAppJobRqmtMetInd,
               LoanAppCDCJobRat,
               a.NAICSYRNMB,
               a.NAICSCD,
               a.LOANAPPFRNCHSCD,
               a.LOANAPPFRNCHSNM,
               a.LOANAPPFRNCHSIND,
               b.EconDevObjctTxt,
               ADDTNLLOCACQLMTIND                                         --SS
                                 ,
               FIXASSETACQLMTIND,
               FIXASSETACQLMTAMT,
               COMPSLMTIND,
               COMPSLMTAMT,
               BULKSALELAWCOMPLYIND,
               FRNCHSRECRDACCSIND,
               FRNCHSFEEDEFMONNMB,
               FRNCHSFEEDEFIND,
               FRNCHSTERMNOTCIND,
               FRNCHSLENDRSAMEOPPIND,
               LoanAppJobCreatQty,
               LOANBUSESTDT,
               (SELECT z.LqdCrTotScr
                  FROM loanapp.LqdCrWSCTbl z
                 WHERE     z.LoanAppNmb = p_LoanAppNmb
                       AND z.TaxId = o.TaxId
                       AND z.LqdCrSeqNmb =
                           (SELECT MAX (LqdCrSeqNmb)
                              FROM loanapp.LqdCrWSCTbl y
                             WHERE     y.LoanAppNmb = p_LoanAppNmb
                                   AND y.TaxId = o.TaxId))
                   AS LqdCrTotScr,
               (SELECT g.DESCTXT
                  FROM Sbaref.NAICSTBL g
                 WHERE a.NAICSCD = g.NAICSCD AND a.NAICSYRNMB = g.NAICSYRNMB)
                   AS NAICSDESCTXT
          FROM LoanGntyProjTbl                   a,
               LoanGntyBorrTbl                   o,
               sbaref.EconDevObjctCdTbl          b,
               loan.LoanGntyEconDevObjctChldTbl  c
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND o.LoanAppNmb = p_LoanAppNmb
               AND o.LOANBUSPRIMBORRIND = 'Y'
               AND o.LOANBORRACTVINACTIND = 'A'
               AND a.LoanAppNmb = c.LoanAppNmb(+)
               AND c.LoanEconDevObjctCd = b.EconDevObjctCd(+);

    OPEN p_SelCur9 FOR                                        -- result set 6:
        /* H. Financial Statements*/
        SELECT LoanBSDt,
               CAST (LoanBSCashEqvlntAmt AS NUMBER (15, 2))
                   LoanBSCashEqvlntAmt,
               CAST (LoanBSNetTrdRecvAmt AS NUMBER (15, 2))
                   LoanBSNetTrdRecvAmt,
               CAST (LoanBSTotInvtryAmt AS NUMBER (15, 2))
                   LoanBSTotInvtryAmt,
               CAST (LoanBSOthCurAssetAmt AS NUMBER (15, 2))
                   LoanBSOthCurAssetAmt,
               CAST (LoanBSTotCurAssetAmt AS NUMBER (15, 2))
                   LoanBSTotCurAssetAmt,
               CAST (LoanBSTotFixAssetAmt AS NUMBER (15, 2))
                   LoanBSTotFixAssetAmt,
               CAST (LoanBSTotOthAssetAmt AS NUMBER (15, 2))
                   LoanBSTotOthAssetAmt,
               CAST (LoanBSTotAssetAmt AS NUMBER (15, 2))
                   LoanBSTotAssetAmt,
               CAST (LoanBSAcctsPayblAmt AS NUMBER (15, 2))
                   LoanBSAcctsPayblAmt,
               CAST (LoanBSCurLTDAmt AS NUMBER (15, 2))
                   LoanBSCurLTDAmt,
               CAST (LoanBSOthCurLiabAmt AS NUMBER (15, 2))
                   LoanBSOthCurLiabAmt,
               CAST (LoanBSTotCurLiabAmt AS NUMBER (15, 2))
                   LoanBSTotCurLiabAmt,
               CAST (LoanBSLTDAmt AS NUMBER (15, 2))
                   LoanBSLTDAmt,
               CAST (LoanBSOthLTLiabAmt AS NUMBER (15, 2))
                   LoanBSOthLTLiabAmt,
               CAST (LoanBSStbyDbt AS NUMBER (15, 2))
                   LoanBSStbyDbt,
               CAST (LoanBSTotLiab AS NUMBER (15, 2))
                   LoanBSTotLiab,
               CAST (LoanBSBusNetWrth AS NUMBER (15, 2))
                   LoanBSBusNetWrth,
               CAST (LoanBSTngblNetWrth AS NUMBER (15, 2))
                   LoanBSTngblNetWrth,
               LoanBSActlPrfrmaInd,
               a.LoanFinanclStmtSourcCd,
               b.LoanFinanclStmtSourcDescTxt
          FROM LoanGntyBSTbl a, sbaref.LoanFinanclStmtSourcTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd;


    OPEN p_SelCur10 FOR                                     -- result set 5.1:
                                                              /* I.Borrower */
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm                            -- Pranesh 06/05/06
                               ,
               f.BusPhyAddrCntCd                               -- Pks 05/11/06
                                ,
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd                          -- Pranesh 06/05/06
                                 ,
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm                           -- Pranesh 06/05/06
                                ,
               f.BusMailAddrCntCd                              -- Pks 05/11/06
                                 ,
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd                         -- Pranesh 06/05/06
                                  ,
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               f.BusDUNSNmb,
               a.LoanCntlIntInd                              -- RGG 02/03/2014
                               ,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                           --NK 08/30/2016
                                    ,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb,
               a.WorkrsCompInsRqrdInd,
               b.VetCertInd,
               a.AdverChngInd,
               f.BusAltPhnNmb,
               f.BusPrimEmailAdr,
               f.BusAltEmailAdr                        --   , b.BusPrimCntctNm
                               ,
               f.BusPrimPhnNmb,
               NULL
                   PerOthrCmntTxt
          FROM BusTbl                     b,
               Loan.LoanGntyBusTbl        f,
               LoanGntyBorrTbl            a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D,
               sbaref.IMEPCOperCdTbl      e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND f.LoanAppNmb = a.LoanAppNmb
               AND a.TaxId = b.TaxId
               AND BorrBusPerInd = 'B'
               AND LoanBorrActvInactInd = 'A'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        UNION ALL
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               f.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               f.PerAlienRgstrtnNmb,
               f.PerAffilEmpFedInd,
               f.PerIOBInd,
               f.PerIndctPrleProbatnInd,
               f.PerCrmnlOffnsInd,
               f.PerCnvctInd,
               NULL
                   BusExprtInd,
               f.PerBnkrptcyInd,
               f.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (f.PerPrimPhnNmb)
                   AS PerPrimPhnNmb                            -- pks 05/03/06
                                   ,
               OcaDataOut (f.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcaDataOut (f.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               f.PerPhyAddrCtyNm,
               f.PerPhyAddrStCd,
               f.PerPhyAddrStNm                            -- Pranesh 06/05/06
                               ,
               f.PerPhyAddrCntCd,
               f.PerPhyAddrZipCd,
               f.PerPhyAddrZip4Cd,
               f.PerPhyAddrPostCd                          -- Pranesh 06/05/06
                                 ,
               OcaDataOut (f.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcaDataOut (f.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               f.PerMailAddrCtyNm,
               f.PerMailAddrStCd,
               f.PerMailAddrStNm                           -- Pranesh 06/05/06
                                ,
               f.PerMailAddrCntCd,
               f.PerMailAddrZipCd,
               f.PerMailAddrZip4Cd,
               f.PerMailAddrPostCd                         -- Pranesh 06/05/06
                                  ,
               f.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.PerExtrnlCrdtScorNmb,
               f.PerExtrnlCrdtScorDt,
               NULL
                   AS BusDUNSNmb,
               a.LoanCntlIntInd                              -- RGG 02/03/2014
                               ,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                           --NK 08/30/2016
                                    ,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb,
               a.WorkrsCompInsRqrdInd,
               b.VetCertInd,
               a.AdverChngInd,
               NULL
                   BusAltPhnNmb,
               NULL
                   BusPrimEmailAdr,
               NULL
                   BusAltEmailAdr                    --  , NULL BusPrimCntctNm
                                 ,
               NULL
                   BusPrimPhnNmb,
               B.PerOthrCmntTxt
          FROM PerTbl                     b,
               Loangntypertbl             f,
               LoanGntyBorrTbl            a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D,
               sbaref.IMEPCOperCdTbl      e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND f.loanappnmb = a.loanappnmb
               AND BorrBusPerInd = 'P'
               AND LoanBorrActvInactInd = 'A'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        ORDER BY BorrSeqNmb;

    OPEN p_SelCur11 FOR                                       -- result set 6:
        /* J. Guarantor */
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,                                -- pks 05/03/06
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm,                           -- Pranesh 06/05/06
               f.BusPhyAddrCntCd,                          -- Pranesh 05/03/06
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd,                         -- Pranesh 06/05/06
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm,                          -- Pranesh 06/05/06
               f.BusMailAddrCntCd,                             -- pks 05/03/06
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd,                        -- Pranesh 06/05/06
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/03/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.GntySubCondTypCd,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt,
               b.VetCertInd,                                   --BR 10/24/2016
               NULL
                   PerOthrCmntTxt
          FROM BusTbl                     b,
               Loan.LoanGntyBusTbl        f,
               LoanGntyGuarTbl            a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND a.loanappnmb = f.loanappnmb          -- JP added 10/19/2020
               AND GuarBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LoanGuarActvInactInd = 'A'
        UNION ALL
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               e.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               e.PerAlienRgstrtnNmb,
               e.PerAffilEmpFedInd,
               e.PerIOBInd,
               e.PerIndctPrleProbatnInd,
               e.PerCrmnlOffnsInd,
               e.PerCnvctInd,
               NULL
                   BusExprtInd,
               e.PerBnkrptcyInd,
               e.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (e.PerPrimPhnNmb)
                   AS PerPrimPhnNmb,                           -- pks 05/03/06
               OcadataOut (e.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcadataOut (e.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               e.PerPhyAddrCtyNm,
               e.PerPhyAddrStCd,
               e.PerPhyAddrStNm,                           -- Pranesh 06/05/06
               e.PerPhyAddrCntCd,                              -- pks 05/03/06
               e.PerPhyAddrZipCd,
               e.PerPhyAddrZip4Cd,
               e.PerPhyAddrPostCd,                         -- Pranesh 06/05/06
               OcadataOut (e.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcadataOut (e.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               e.PerMailAddrCtyNm,
               e.PerMailAddrStCd,
               e.PerMailAddrStNm,                          -- Pranesh 06/05/06
               e.PerMailAddrCntCd,                             -- pks 05/03/06
               e.PerMailAddrZipCd,
               e.PerMailAddrZip4Cd,
               e.PerMailAddrPostCd,                        -- Pranesh 06/05/06
               e.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               e.PerExtrnlCrdtScorNmb,
               e.PerExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                            --RGG 02/04/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.GntySubCondTypCd,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt,
               b.VetCertInd,
               B.PerOthrCmntTxt
          FROM PerTbl                     b,
               Loangntypertbl             e,
               LoanGntyGuarTbl            a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND e.loanappnmb = a.loanappnmb
               AND GuarBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LoanGuarActvInactInd = 'A'
        ORDER BY GuarSeqNmb;


    OPEN p_SelCur12 FOR                                       -- result set 7:
        /* K. Principal */
        SELECT a.BorrSeqNmb,
               a.TaxId
                   BorrTaxId,
               i.PrinTaxId,
               i.PrinBusPerInd,
               b.VetCd,
               e.VetTxt,
               b.GndrCd,
               f.GndrDesc,
               b.EthnicCd,
               g.EthnicDesc,
               h.LoanPrinGntyInd,
               NVL (i.BusPrinOwnrshpPct, 0)
                   LoanBusPrinPctOwnrshpBus,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               j.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               j.BusExprtInd,
               j.BusBnkrptInd,
               j.BusLwsuitInd,
               j.BusPriorSBALoanInd,
               j.BusPrimPhnNmb,                                -- pks 05/03/06
               j.BusPhyAddrStr1Nm,
               j.BusPhyAddrStr2Nm,
               j.BusPhyAddrCtyNm,
               j.BusPhyAddrStCd,
               j.BusPhyAddrStNm,                           -- Pranesh 06/05/06
               j.BusPhyAddrCntCd,                              -- pks 05/03/06
               j.BusPhyAddrZipCd,
               j.BusPhyAddrZip4Cd,
               j.BusPhyAddrPostCd,                         -- Pranesh 06/05/06
               j.BusMailAddrStr1Nm,
               j.BusMailAddrStr2Nm,
               j.BusMailAddrCtyNm,
               j.BusMailAddrStCd,
               j.BusMailAddrStNm,                          -- Pranesh 06/05/06
               j.BusMailAddrCntCd,                             -- pks 05/03/06
               j.BusMailAddrZipCd,
               j.BusMailAddrZip4Cd,
               j.BusMailAddrPostCd,                        -- Pranesh 06/05/06
               j.BusExtrnlCrdtScorInd,
               j.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               j.BusExtrnlCrdtScorNmb,
               j.BusExtrnlCrdtScorDt,
               j.BusPrimBusExprnceYrNmb
                   AS LoanPrinPrimBusExprnceYrNmb,
               h.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE h.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp,                          --NK 08/15/2016
               h.LoanDisblInsurRqrdInd,
               h.LoanInsurDisblDescTxt,
               h.LoanLifeInsurRqrdInd,
               h.LOANPRININSURAMT,
               h.LoanPrinNoNCAEvdncInd,
               h.LoanComptitrNm,
               h.LoanPrinInsurNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = h.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = h.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               h.GntyLmtAmt,
               h.GntyLmtPct,
               h.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               h.GntyLmtYrNmb,
               GntySubCondTypCd,
               LACGNTYCONDTYPCD,
               b.VetCertInd,
               NULL
                   PerCitznShpCntNm,
               j.BusAltPhnNmb,
               j.BusPrimEmailAdr,
               j.BusAltEmailAdr
          FROM loan.LoanGntyBorrTbl  a
               LEFT OUTER JOIN loan.LoanGntyPrinTbl h
                   ON (a.BorrSeqNmb = h.BorrSeqNmb ---(--h.LoanAppNmb = 7630--a.LoanAppNmb= h.LoanAppNmb--and )
                                                   AND h.PrinBusPerInd = 'B')
               LEFT OUTER JOIN loan.LoanGntyBusPrinTbl i
                   ON (    a.TaxId = i.BusTaxId
                       AND h.PrinTaxId = i.PrinTaxId
                       AND i.LoanAppNmb = p_LoanAppNmb
                       AND i.PrinBusPerInd = 'B'
                       AND i.BusPrinActvInactInd = 'A')
               LEFT OUTER JOIN loan.BusTbl b ON (i.PrinTaxId = b.TaxId)
               LEFT OUTER JOIN Loan.LoanGntyBusTbl j
                   ON b.taxid = j.taxid AND i.LOANAPPNMB = j.LoanAppNmb
               LEFT OUTER JOIN sbaref.BusTypTbl c
                   ON (b.BusTypCd = c.BusTypCd)
               LEFT OUTER JOIN sbaref.IMCrdtScorSourcTbl d
                   ON (b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd)
               LEFT OUTER JOIN sbaref.VetTbl e ON (b.VetCd = e.VetCd)
               LEFT OUTER JOIN sbaref.GndrCdTbl f ON (b.GndrCd = f.GndrCd)
               LEFT OUTER JOIN sbaref.EthnicCdTbl g
                   ON (b.EthnicCd = g.EthnicCd)
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND h.LoanAppNmb = p_LoanAppNmb
               AND a.LoanBorrActvInactInd = 'A'
        /*FROM BusTbl b,
                  LoanGntyBorrTbl a,
                  sbaref.BusTypTbl c,
                  sbaref.IMCrdtScorSourcTbl D,
                  sbaref.VetTbl e,
                  sbaref.GndrCdTbl f,
                  sbaref.EthnicCdTbl g,
                  LoanGntyPrinTbl h,
                  BusPrinTbl i
              WHERE a.LoanAppNmb = p_LoanAppNmb
                      AND h.LoanAppNmb = p_LoanAppNmb
                      AND a.BorrSeqNmb =  h.BorrSeqNmb(+)
                      AND a.TaxId = i.BusTaxId
                      AND i.PrinTaxId =h.PrinTaxId(+)
                      AND i.PrinBusPerInd = 'B'
                      AND i.PrinTaxId = b.TaxId
                      AND h.PrinBusPerInd = 'B'
                      AND b.BusTypCd =c.BusTypCd(+)
                      AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
                      AND b.VetCd =e.VetCd(+)
                      AND b.GndrCd =f.GndrCd(+)
                      AND b.EthnicCd =g.EthnicCd(+)
                      AND i.BusPrinActvInactInd = 'A'
                      AND a.LoanBorrActvInactInd = 'A'*/
        UNION ALL
        SELECT a.BorrSeqNmb,
               a.TaxId
                   BorrTaxId,
               i.PrinTaxId,
               i.PrinBusPerInd,
               b.VetCd,
               e.VetTxt,
               b.GndrCd,
               f.GndrDesc,
               b.EthnicCd,
               g.EthnicDesc,
               h.LoanPrinGntyInd,
               NVL (BusPrinOwnrshpPct, 0)
                   LoanBusPrinPctOwnrshpBus,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               s.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               s.PerUSCitznInd,
               c.CitznshipTxt,
               s.PerAlienRgstrtnNmb,
               s.PerAffilEmpFedInd,
               s.PerIOBInd,
               s.PerIndctPrleProbatnInd,
               s.PerCrmnlOffnsInd,
               s.PerCnvctInd,
               NULL
                   BusExprtInd,
               s.PerBnkrptcyInd,
               s.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (s.PerPrimPhnNmb)
                   AS PerPrimPhnNmb,                       -- Pranesh 05/03/06
               OcaDataOut (s.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcaDataOut (s.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               s.PerPhyAddrCtyNm,
               s.PerPhyAddrStCd,
               s.PerPhyAddrStNm,                           -- Pranesh 06/05/06
               s.PerPhyAddrCntCd,
               s.PerPhyAddrZipCd,
               s.PerPhyAddrZip4Cd,
               s.PerPhyAddrPostCd,                         -- Pranesh 06/05/06
               OcaDataOut (s.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcaDataOut (s.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               s.PerMailAddrCtyNm,
               s.PerMailAddrStCd,
               s.PerMailAddrStNm,                          -- Pranesh 06/05/06
               s.PerMailAddrCntCd,
               s.PerMailAddrZipCd,
               s.PerMailAddrZip4Cd,
               s.PerMailAddrPostCd,                        -- Pranesh 06/05/06
               s.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               s.PerExtrnlCrdtScorNmb,
               s.PerExtrnlCrdtScorDt,
               s.PerPrimBusExprnceYrNmb
                   LoanPrinPrimBusExprnceYrNmb,
               h.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE h.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp,                         --NK--08/15/2016
               h.LoanDisblInsurRqrdInd,
               h.LoanInsurDisblDescTxt,
               h.LoanLifeInsurRqrdInd,
               h.LOANPRININSURAMT,
               h.LoanPrinNoNCAEvdncInd,
               h.LoanComptitrNm,
               h.LoanPrinInsurNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = h.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = h.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               h.GntyLmtAmt,
               h.GntyLmtPct,
               h.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               h.GntyLmtYrNmb,
               GntySubCondTypCd,                               --BR 10/24/2016
               LACGNTYCONDTYPCD,                              --BR 10/24/2016,
               b.VetCertInd,
               (SELECT z.IMCNTRYNM
                  FROM SBAREF.IMCNTRYCDTBL z
                 WHERE z.IMCNTRYCD = s.PerCitznShpCntNm)
                   AS PerCitznShpCntNm,
               OcaDataOut (s.PerAltPhnNmb)
                   AS PerAltPhnNmb,
               OcaDataOut (s.PerPrimEmailAdr)
                   AS PerPrimEmailAdr,
               OcaDataOut (s.PerAltEmailAdr)
                   AS PerAltEmailAdr
          FROM loan.LoanGntyBorrTbl  a
               LEFT OUTER JOIN loan.LoanGntyPrinTbl h
                   ON (a.BorrSeqNmb = h.BorrSeqNmb AND h.PrinBusPerInd = 'P')
               ---h.LoanAppNmb = p_LoanAppNmb--a.LoanAppNmb = h.LoanAppNmb--and
               LEFT OUTER JOIN loan.LoanGntyBusPrinTbl i
                   ON (    a.TaxId = i.BusTaxId
                       AND i.loanAppNmb = p_LoanAppNmb
                       AND h.PrinTaxId = i.PrinTaxId
                       AND i.PrinBusPerInd = 'P'
                       AND i.BusPrinActvInactInd = 'A')
               LEFT OUTER JOIN loan.PerTbl b ON (i.PrinTaxId = b.TaxId)
               LEFT OUTER JOIN loan.Loangntypertbl s
                   ON (s.loanappnmb = a.loanappnmb)
               LEFT OUTER JOIN sbaref.CitznshipCdTbl c
                   ON (b.PerUSCitznInd = c.CitznshipCd)
               LEFT OUTER JOIN sbaref.IMCrdtScorSourcTbl d
                   ON (b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd)
               LEFT OUTER JOIN sbaref.VetTbl e ON (b.VetCd = e.VetCd)
               LEFT OUTER JOIN sbaref.GndrCdTbl f ON (b.GndrCd = f.GndrCd)
               LEFT OUTER JOIN sbaref.EthnicCdTbl g
                   ON (b.EthnicCd = g.EthnicCd)
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND h.LoanAppNmb = p_LoanAppNmb
               AND a.LoanBorrActvInactInd = 'A'
        ORDER BY BorrSeqNmb, PrinTaxId;

    /* FROM PerTbl b,
              LoanGntyBorrTbl a,
              LoanGntyPrinTbl h,
              sbaref.CitznshipCdTbl c,
              sbaref.IMCrdtScorSourcTbl D,
              sbaref.VetTbl e,
              sbaref.GndrCdTbl f,
              sbaref.EthnicCdTbl g,
              BusPrinTbl i
          WHERE a.LoanAppNmb = p_LoanAppNmb
                  AND h.LoanAppNmb = p_LoanAppNmb
                  AND a.BorrSeqNmb =h.BorrSeqNmb(+)
                  AND a.TaxId = i.BusTaxId
                  AND i.PrinTaxId =h.PrinTaxId(+)
                  AND i.PrinBusPerInd = 'P'
                  AND i.PrinTaxId = b.TaxId
                  AND h.PrinBusPerInd = 'P'
                  AND b.PerUSCitznInd =c.CitznshipCd(+)
                  AND b.IMCrdtScorSourcCd =D.IMCrdtScorSourcCd(+)
                  AND b.VetCd =               e.VetCd(+)
                  AND b.GndrCd =               f.GndrCd(+)
                  AND b.EthnicCd =               g.EthnicCd(+)
                  AND i.BusPrinActvInactInd = 'A'
                  AND a.LoanBorrActvInactInd = 'A'
                  ORDER BY BorrSeqNmb, PrinTaxId;*/
    OPEN p_SelCur13 FOR                                       -- result set 8:
        /* Principal Race */
        SELECT BorrSeqNmb,
               PrinTaxId,
               c.RaceCd,
               b.RaceTxt,
               a.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                          -- NK 08/15/2016
          FROM LoanGntyPrinTbl a, BusRaceTbl c, sbaref.RaceCdTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrinBusPerInd = 'B'
               AND a.PrinTaxId = c.TaxId
               AND c.RaceCd = b.RaceCd
        UNION ALL
        SELECT BorrSeqNmb,
               PrinTaxId,
               c.RaceCd,
               b.RaceTxt,
               a.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                          -- NK 08/15/2016
          FROM LoanGntyPrinTbl a, PerRaceTbl c, sbaref.RaceCdTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrinBusPerInd = 'P'
               AND a.PrinTaxId = c.TaxId
               AND c.RaceCd = b.RaceCd
        ORDER BY BorrSeqNmb, PrinTaxId;

    OPEN p_SelCur14 FOR                   --Resultset 11, participating lender
        SELECT D.LoanPartLendrNm,
               D.LocId,
               D.LoanPartLendrStr1Nm,
               D.LoanPartLendrStr2Nm,
               D.LoanPartLendrCtyNm,
               D.LoanPartLendrStCd,
               D.LoanPartLendrZip5Cd,
               D.LoanPartLendrZip4Cd,
               D.LoanLienPosCd,
               CAST (D.LoanPartLendrAmt AS NUMBER (12, 2))
                   LoanPartLendrNum,
               OcaDataOut (D.LoanPartLendrOfcrLastNm)
                   AS LoanPartLendrOfcrLastNm,
               OcaDataOut (D.LoanPartLendrOfcrFirstNm)
                   AS LoanPartLendrOfcrFirstNm,
               D.LoanPartLendrOfcrInitialNm,
               D.LoanPartLendrOfcrSfxNm,
               D.LoanPartLendrOfcrTitlTxt,
               OcaDataOut (D.LoanPartLendrOfcrPhnNmb)
                   AS LoanPartLendrOfcrPhnNmb,
               D.LOANPARTLENDRCNTRYCD,
               D.LOANPARTLENDRPOSTCD,
               D.LOANPARTLENDRSTNM,
               r.LoanPartLendrTypDescTxt,
               LOANLENDRTAXID,
               LOANLENDRGROSSINTPCT,
               LOANLENDRPARTPCT,
               LOANORIGPARTPCT,
               LOANSBAGNTYBALPCT,
               LOANLENDRPARTAMT,
               LOANORIGPARTAMT,
               LOANLENDRGROSSAMT,
               LOANSBAGNTYBALAMT,
               LOAN503POOLAPPNMB,
               LoanPartLendrAcctNm,
               LoanPartLendrAcctNmb,
               LoanPartLendrTranCd,
               LoanPartLendrAttn,
               LndrLoanNmb
          FROM LoanGntyPartLendrTbl        D,
               sbaref.LoanPartLendrTypTbl  r,
               Loan.LoanGntyTbl            G
         WHERE     D.LoanAppNmb = p_LoanAppNmb
               AND D.LoanAppNmb = G.LOANAPPNMB
               AND D.LoanPartLendrTypCd = r.LoanPartLendrTypCd;

    OPEN p_SelCur15 FOR                      --Resultset 12, loan disbursement
        SELECT LoanAppNmb,
               LoanDisbSeqNmb,
               LoanDisbDt,
               CAST (LoanDisbAmt AS NUMBER (15, 2))     LoanDisbAmt
          FROM LoanDisbTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    -- result set 12 : Companion list
    LoanTransCmpnSelCSP (p_LoanAppNmb, 'Y', p_SelCur16);

    -- result set 13:
    /* ARC Reason Text */
    /*06/17/2009 -APK- Added new result set 13*/
    OPEN p_SelCur17 FOR
        SELECT b.ARCLoanRsnCd, b.ARCLoanRsnDescTxt
          FROM loanapp.LoanAppARCRsnTbl a, sbaref.ARCLoanRsnTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ARCLoanRsnCd = b.ARCLoanRsnCd;

    -- get values from loanintdtltbl
    OPEN p_SelCur18 FOR
          SELECT a.LOANAPPNMB,
                 a.LOANINTDTLSEQNMB,
                 a.LOANINTLOANPARTPCT,
                 a.LOANINTLOANPARTMONMB,
                 a.LOANINTFIXVARIND,
                 a.LOANINTRT,
                 a.IMRTTYPCD,
                 D.IMRtTypDescTxt,
                 a.LOANINTBASERT,
                 a.LOANINTSPRDOVRPCT,
                 CASE
                     WHEN a.LOANINTADJPRDCD IS NULL
                     THEN
                         ' '
                     ELSE
                         (SELECT CALNDRPRDDESC
                            FROM sbaref.CalndrPrdTbl e
                           WHERE e.CALNDRPRDCD = a.LOANINTADJPRDCD)
                 END
                     CALNDRPRDDESC,
                 (SELECT b.Loanintdtldesctxt
                    FROM SBAREF.LOANINTDTLCDTBL b
                   WHERE g.Loanappintdtlcd = b.Loanintdtlcd)
                     AS Loanintdtldesctxt,
                 a.LOANINTADJPRDMONMB,
                 a.LOANINTADJPRDEFFDT,
                 a.LoanIntDtlGuarInd
            FROM LOAN.LOANINTDTLTBL a, LOAN.LOANGNTYTBL g, sbaref.IMRtTypTbl D
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.loanappnmb = g.loanappnmb
                 AND a.IMRtTypCd = D.IMRtTypCd
                 AND g.PrcsMthdCd = D.PrcsMthdCd
        ORDER BY a.LOANINTDTLSEQNMB;

    --Associate Information
    OPEN p_SelCur19 FOR
        SELECT a.LOANASSOCSEQNMB,
               a.TAXID,
               a.LOANASSOCTYPCD,
               e.LOANASSOCDESCTXT,
               a.LOANASSOCBUSPERIND,
               a.LOANASSOCCMNTTXT,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL     PerFirstNm,
               NULL     PerLastNm,
               NULL     PerInitialNm,
               NULL     PerSfxNm,
               NULL     PerTitlTxt,
               NULL     PerBrthDt,
               NULL     PerBrthCtyNm,
               NULL     PerBrthStCd,
               NULL     PerBrthCntCd,
               NULL     PerBrthCntNm,
               NULL     PerUSCitznInd,
               NULL     CitznshipTxt,
               NULL     PerAlienRgstrtnNmb,
               NULL     PerAffilEmpFedInd,
               NULL     PerIOBInd,
               NULL     PerIndctPrleProbatnInd,
               NULL     PerCrmnlOffnsInd,
               NULL     PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm,
               f.BusPhyAddrCntCd,
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd,
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm,
               f.BusMailAddrCntCd,
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd,
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               f.BusDUNSNmb,
               b.VetCertInd,
               NULL     PerOthrCmntTxt
          FROM LOAN.BusTbl                b,
               Loan.LoanGntyBusTbl        f,
               LOAN.LOANASSOCTBL          a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D,
               LOAN.LOANASSOCTYPTBL       e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = f.LoanAppNmb
               AND a.TaxId = b.TaxId
               AND a.LOANASSOCBUSPERIND = 'B'
               AND a.LOANASSOCACTVINACTIND = 'A'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LOANASSOCTYPCD = e.LOANASSOCTYPCD(+)
        UNION ALL
        SELECT a.LOANASSOCSEQNMB,
               a.TAXID,
               a.LOANASSOCTYPCD,
               e.LOANASSOCDESCTXT,
               a.LOANASSOCBUSPERIND,
               a.LOANASSOCCMNTTXT,
               NULL                                 BusTypCd,
               NULL                                 BusTypTxt,
               NULL                                 BusNm,
               NULL                                 BusTrdNm,
               OcaDataOut (b.PerFirstNm)            AS PerFirstNm,
               OcaDataOut (b.PerLastNm)             AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               f.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               f.PerAlienRgstrtnNmb,
               f.PerAffilEmpFedInd,
               f.PerIOBInd,
               f.PerIndctPrleProbatnInd,
               f.PerCrmnlOffnsInd,
               f.PerCnvctInd,
               NULL                                 BusExprtInd,
               f.PerBnkrptcyInd,
               f.PerPndngLwsuitInd,
               NULL                                 BusPriorSBALoanInd,
               OcadataOut (f.PerPrimPhnNmb)         AS PerPrimPhnNmb,
               OcaDataOut (f.PerPhyAddrStr1Nm)      AS PerPhyAddrStr1Nm,
               OcaDataOut (f.PerPhyAddrStr2Nm)      AS PerPhyAddrStr2Nm,
               f.PerPhyAddrCtyNm,
               f.PerPhyAddrStCd,
               f.PerPhyAddrStNm,
               f.PerPhyAddrCntCd,
               f.PerPhyAddrZipCd,
               f.PerPhyAddrZip4Cd,
               f.PerPhyAddrPostCd,
               OcaDataOut (f.PerMailAddrStr1Nm)     AS PerMailAddrStr1Nm,
               OcaDataOut (f.PerMailAddrStr2Nm)     AS PerMailAddrStr2Nm,
               f.PerMailAddrCtyNm,
               f.PerMailAddrStCd,
               f.PerMailAddrStNm,
               f.PerMailAddrCntCd,
               f.PerMailAddrZipCd,
               f.PerMailAddrZip4Cd,
               f.PerMailAddrPostCd,
               f.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.PerExtrnlCrdtScorNmb,
               f.PerExtrnlCrdtScorDt,
               NULL                                 AS BusDUNSNmb,
               b.VetCertInd,
               b.PerOthrCmntTxt
          FROM LOAN.PerTbl                b,
               loan.loangntypertbl        f,
               LOAN.LOANASSOCTBL          a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D,
               LOAN.LOANASSOCTYPTBL       e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND f.loanappnmb = a.loanappnmb
               AND a.LOANASSOCBUSPERIND = 'P'
               AND a.LOANASSOCACTVINACTIND = 'A'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LOANASSOCTYPCD = e.LOANASSOCTYPCD(+)
        ORDER BY LOANASSOCSEQNMB;

    --Stand By Agreement---
    OPEN p_SelCur20 FOR
        SELECT a.LOANSTBYCRDTRNM,
               a.LOANSTBYAMT,
               (SELECT s.STBYPYMTTYPDESCTXT
                  FROM SBAREF.STBYPYMTTYPCDTBL s
                 WHERE a.LOANSTBYREPAYTYPCD = S.STBYPYMTTYPCD)
                   AS LOANSTBYPYMTTYPDESCTXT,
               a.LOANSTBYPYMTINTRT,
               a.LOANSTBYPYMTOTHDESCTXT,
               a.LOANSTBYPYMTAMT,
               a.LOANSTBYREPAYTYPCD,
               a.LOANSTBYPYMTSTRTDT
          FROM LOAN.LOANGNTYStbyAgrmtDtlTBL a
         WHERE a.LOANAPPNMB = p_LOANAPPNMB;


    OPEN p_SelCur21 FOR
        SELECT LOANAPPPYMTAMT,
               CASE
                   WHEN LOANPYMNINSTLMNTFREQCD = 'M' THEN 'Monthly'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'Quarterly'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'S' THEN 'Semi Annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'A' THEN 'Annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'O' THEN 'Other'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTFREQTXT,
               PYMNTDAYNMB,
               PYMNTBEGNMONMB,
               ESCROWACCTRQRDIND,
               NETEARNCLAUSEIND,
               ARMMARTYPIND,
               CASE
                   WHEN ARMMARTYPIND = 'N' THEN 'None'
                   WHEN ARMMARTYPIND = 'F' THEN 'Ceiling and Floor Fluctuate'
                   WHEN ARMMARTYPIND = 'X' THEN 'Ceiling and Floor are Fixed'
                   ELSE NULL
               END
                   AS ARMMARTYPTXT,
               ARMMARCEILRT,
               ARMMARFLOORRT,
               STINTRTREDUCTNIND,
               LATECHGIND,
               LATECHGAFTDAYNMB,
               LATECHGFEEPCT,
               PYMNTINTONLYBEGNMONMB,
               PYMNTINTONLYFREQCD,
               CASE
                   WHEN PYMNTINTONLYFREQCD = 'M' THEN 'Monthly'
                   WHEN PYMNTINTONLYFREQCD = 'Q' THEN 'Quarterly'
                   WHEN PYMNTINTONLYFREQCD = 'S' THEN 'Semi Annual'
                   WHEN PYMNTINTONLYFREQCD = 'A' THEN 'Annual'
                   WHEN PYMNTINTONLYFREQCD = 'O' THEN 'Other'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTINTONLYFREQTXT,
               PYMNTINTONLYDAYNMB,
               NetEarnPymntPct,
               NetEarnPymntOverAmt,
               StIntRtReductnPrgmNm,
               a.LOANAPPMOINTQTY,
               a.LoanPymtRepayInstlTypCd,
               a.LOANPYMNINSTLMNTFREQCD,
               RTRIM (
                      SUBSTR (LoanPymntSchdlFreq, 1, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 3, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 5, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 7, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 9, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 11, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 13, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 15, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 17, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 19, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 21, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 23, 2),
                   ', ')
                   AS freq,
               a.LOANAPPINTDTLCD
          FROM LOAN.LOANGNTYPYMNTTBL p, LOAN.LoanGNTYTbl a
         WHERE a.LoanAppNmb = p_LoanAppNmb AND a.LoanAppNmb = p.LoanAppNmb(+);

    OPEN p_SelCur22 FOR
        SELECT LoanCollatSeqNmb,
               TaxId,
               BUSPERIND,
               CASE
                   WHEN BUSPERIND = 'B'
                   THEN
                       (SELECT BusNm
                          FROM loan.bustbl z
                         WHERE z.taxid = c.taxid AND ROWNUM <= 1)
                   ELSE
                       (SELECT    OcaDataOut (PerFirstNm)
                               || LTRIM (
                                         ' '
                                      || CASE
                                             WHEN PerInitialNm IS NOT NULL
                                             THEN
                                                 PerInitialNm || '.'
                                             ELSE
                                                 NULL
                                         END)
                               || LTRIM (' ' || OcaDataOut (PerLastNm))
                               || LTRIM (' ' || PerSfxNm)
                          FROM loan.pertbl z
                         WHERE z.taxid = c.taxid)
               END
                   AS BUSPERNm
          FROM loan.LoanLmtGntyCollatTbl c
         WHERE LoanAppNmb = p_LoanAppNmb;

    --Export Country Codes---
    OPEN p_SelCur23 FOR
          SELECT a.LoanAppNmb,
                 a.LoanExprtCntryCd,
                 b.imcntrynm,
                 a.LoanExprtCntrySeqNmb
            FROM LOAN.LOANEXPRTCNTRYTBL a, SBAREF.IMCNTRYCDTBL b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.LoanExprtCntryCd = b.ImCntryCd
        ORDER BY LOANEXPRTCNTRYSEQNMB ASC;

    OPEN p_SelCur24 FOR
          SELECT a.LOANAGNTSEQNMB,
                 a.LOANAGNTID,
                 a.LOANAGNTBUSPERIND,
                 a.LOANAGNTNM,
                    OcaDataOut (a.LOANAGNTCNTCTFIRSTNM)
                 || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                 || ' '
                 || OcaDataOut (a.LOANAGNTCNTCTLastNm)
                 || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                     AS LoanContactName,
                    OcaDataOut (a.LOANAGNTADDRSTR1NM)
                 || RTRIM (' ' || OcaDataOut (a.LOANAGNTADDRSTR2NM))
                 || ' '
                 || a.LOANAGNTADDRCTYNM
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTNM)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIPCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIP4CD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRPOSTCD)
                 || ' '
                 || a.LOANAGNTADDRCNTCD
                     AS LoanContactaddress,
                 a.LOANAGNTTYPCD,
                 a.LOANAGNTDOCUPLDIND,
                 a.LOANCDCTPLFEEIND,
                 a.LOANCDCTPLFEEAMT,
                 b.LOANAGNTSERVTYPCD,
                 b.LOANAGNTSERVOTHTYPTXT,
                 b.LOANAGNTAPPCNTPAIDAMT,
                 b.LOANAGNTSBALENDRPAIDAMT
            FROM LoanAgntTbl a, loanagntfeedtltbl b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.loanappnmb = b.loanappnmb
                 AND a.loanagntseqnmb = b.loanagntseqnmb
        ORDER BY loanagntseqnmb ASC;
END LOANGNTYPRINTSELCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.MOVELOANGNTYCSP (p_Identifier            IN     NUMBER := 0,
                                                  p_LoanAppNmb            IN     NUMBER := 0,
                                                  p_LoanNmb               IN     CHAR := NULL,
                                                  p_LoanGntyFeeAmt        IN     NUMBER := NULL,
                                                  p_LoanAppServOfcCd      IN     CHAR := NULL,
                                                  p_CohortCd              IN     CHAR := NULL,
                                                  p_RetVal                   OUT NUMBER,
                                                  p_RetStat                  OUT NUMBER,
                                                  p_RetTxt                   OUT VARCHAR2,
                                                  p_LoanOrigntnFeeInd            CHAR := NULL,
                                                  p_LOANONGNGFEECOLLIND          CHAR := NULL) AS
    /*
        Parameters   :  No.   Name        Type     Description
                        1     p_Identifier  int     Identifies which batch
                                                   to execute.
                        2     p_LoanAppNmb  int     LoanApplication Number to be moved
                        3     p_CreatUserId char(15)User Id
                        4     p_RetVal      int     No. of rows returned/
                                                   affected
                        5     p_RetStat     int     The return value for the update
                        6     p_RetTxt      varchar return value text
    *********************************************************************************
    MODIFIED BY: APK - 11/02/2010 - Modified for 503 changes
                Need to modify the LOAN.MOVELOANGNTYCSP to add the following additional
                fields for process method SMP. For non-SMP set to null.All the �v_� come
                from LOANAPP.POOLAPPPRTPNTINFOCSP for given 503 LoanAppNmb
                PoolGrossInitlIntPct = g.LoanAppInitlIntPct, -- Gross Interest at the time
                of funding,PoolLendrGrossInitlAmt, = v_PoolLendrGrossInitlAmt, --Balance at
                the time of funding,PoolLendrPartInitlAmt = v_PoolLendrPartInitlAmt, --
                lender�s participation amount at time of  funding,PoolOrigPartInitlAmt =
                v_PoolOrigPartInitlAmt, -- originator�s participation
                amount at time of  funding,PoolSBAGntyBalInitlAmt =
                v_PoolSBAGntyBalInitlAmt, -- SBA�s participation amount at time of  funding.
    APK - 11/16/2010 - Modified to add the Financial instrument indicator.
    APK - 05/10/2011 - Modified to add the purchase indicator.
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP  -- 04/18/2012 -- Modified to add new column LoanAppNetEarnInd
    SP  -- 04/04/2013 -- modified to add UNDRWRITNGBY
    RGG -- 06/23/2013 -- Added the new column LOANGNTYMATSTIND to decide whether LoanGntyNoteDt or First disturbment date should be used.
    SP  -- 07/29/2013 -- Modified to remove check for prgrmcd = X for setting LOANGNTYMATSTIND
    RGG -- 07/29/2013 -- Added the new column LOANGNTYLCKOUTUPDIND.
    RGG -- 09/05/2013 -- Removed references for Street Number and Street Suffix Columns
    SB -- 04/04/2014 -- Removed references for NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND
    PSP -- 08/04/2014 -- Modified to update fininstrmnttypind to E where prgmcd =  E
    SB -- 09/03/2014 --  Modified to replace logic to set loan.LoanGntyTbl. FinInstrmntTypInd logic with loan.LoanGntyTbl. FinInstrmntTypInd = loanapp.LoanAppTbl. FinInstrmntTypInd
    RY-- 12/20/16 OPSMDEV 1293,1294 -- Commented out the code for LOANGNTYMATSTIND as added a column in loanapptbl.
    RY --12/27/16 OPSMDEV 1293,1294-- Added the  column'LOANGNTYNOTEDT' to movefrom origination to servicing.
    RY--12/28/16 OPSMDEV1314--Added NoteDt
    SS--06/05/2017 OPSMDEV 1456--Added exception to two begin statements
    SS--10/25/2017 OPSMDEV 1541--Added LOANPYMNINSTLMNTFREQCD,LoanPymntSchdlFreq
    RY--01/17/18 OPSMDEV-1644 --Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
    RY--04/23/2018--OPSMDEV-1783--Added LoanPymtRepayInstlTypCd in loangntytbl call
    SS--10/26/2018--OPSMDEV 1965 Added LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd
    RY--11/27/2018--OPSMDEV-2044--Added OCADATAOut for PII columns in PERtbl
    JP--11/14/2019 -- CAFSOPER 3213 -- added LoanAgntInvlvdInd in the insert to LoanGntyTbl table
    SS--04/03/2020 Added LoanAppMonthPayroll to loangntytbl insert CARESACT - 66
    JP -- 06/12/2020 added LOAN1502CERTIND
    JP--1/9/2021  - SODSTORY 376 added column LoanAppUsrEsInd
    SL--03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 
*/
    v_POOLLOANSBAGNTYBALAMT      NUMBER (19, 4);
    v_POOLLOANLENDRGROSSAMT      NUMBER (19, 4);
    v_POOLLOANORIGPARTAMT        NUMBER (19, 4);
    v_POOLLOANLENDRPARTAMT       NUMBER (19, 4);
    v_PrcsMthdCd                 CHAR (4);
    v_PoolSBAGntyFeeInitlPct     NUMBER (6, 3);
    v_LOANLENDRSERVFEEPCT        NUMBER (6, 3);
    v_POOLLOANLENDRGROSSINTPCT   NUMBER (6, 3);
    v_LOANLENDRNETINTPCT         NUMBER (6, 3);
    v_PoolSBAGntyFeePct          NUMBER (6, 3);
    v_LOANLENDRPARTPCT           NUMBER (6, 3);
    v_LOANORIGPARTPCT            NUMBER (6, 3);
    v_LOANSBAGNTYBALPCT          NUMBER (6, 3);
    --v_FININSTRMNTTYPIND         CHAR(1);
    v_LoanMailStr1Nm             VARCHAR2 (80);
    v_LoanMailStr2Nm             VARCHAR2 (80);
    v_LoanMailCtyNm              VARCHAR2 (40);
    v_LoanMailStCd               CHAR (2);
    v_LoanMailZipCd              CHAR (5);
    v_LoanMailZip4Cd             CHAR (4);
    v_LoanMailCntryCd            CHAR (2);
    v_LoanMailStNm               VARCHAR2 (60);
    v_LoanMailPostCd             VARCHAR2 (20);
    v_PrgmCd                     CHAR (1);
    v_TaxId                      CHAR (10);
    v_BorrBusPerInd              CHAR (1);
    v_LOANGNTYEIDLAMT            NUMBER (15, 2);
    v_LOANGNTYPHYAMT             NUMBER (15, 2);
    v_LOANGNTYMATSTIND           CHAR (1);
    v_POOLGROSSINITLINTPCT       NUMBER (6, 3);
    v_LoanGntyFeeDscntRt         NUMBER;
    v_LoanGntyFeeBillAmt         NUMBER;
BEGIN
    /* Insert into LoanGntyTbl Table */
    BEGIN
        SELECT PrgmCd, PrcsMthdCd
        INTO v_PrgmCd, v_PrcsMthdCd
        FROM LOANAPP.LOANAPPTBL
        WHERE LoanAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    BEGIN
        SELECT LOANINTLOANPARTPCT
        INTO v_POOLGROSSINITLINTPCT
        FROM LOANAPP.LOANINTDTLTBL
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND LOANINTDTLSEQNMB = 1;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    BEGIN
        SELECT LoanGntyFeeDscntRt,
               ROUND (LoanGntyFeeAmt * (100 - NVL (LoanGntyFeeDscntRt, 0)) / 100, 2)     AS LoanGntyFeeBillAmt
        INTO v_LoanGntyFeeDscntRt, v_LoanGntyFeeBillAmt
        FROM loan.LoanGntyCmpnLoanCalcTbl
        WHERE     LoanMainAppNmb = p_LoanAppNmb
              AND LoanAppNmb = p_LoanAppNmb
              AND LoanCmpnAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    /* IF v_PrgmCd = 'E'
     THEN
      v_FININSTRMNTTYPIND := 'E';
     ELSE
      v_FININSTRMNTTYPIND := 'L';
     END IF;  */

    IF p_Identifier = 0 THEN
        IF v_PrcsMthdCd = 'SMP' THEN
            LOANAPP.POOLAPPPRTPNTINFOCSP (
                p_LoanAppNmb,
                v_POOLLOANSBAGNTYBALAMT,
                v_POOLLOANLENDRGROSSAMT,
                v_POOLLOANORIGPARTAMT,
                v_POOLLOANLENDRPARTAMT,
                v_PoolSBAGntyFeeInitlPct,
                v_LOANLENDRSERVFEEPCT,
                v_POOLLOANLENDRGROSSINTPCT,
                v_LOANLENDRNETINTPCT,
                v_PoolSBAGntyFeePct,
                v_LOANLENDRPARTPCT,
                v_LOANORIGPARTPCT,
                v_LOANSBAGNTYBALPCT);
        END IF;

        IF v_PrgmCd = 'H' THEN
            SELECT TaxId, BORRBUSPERIND
            INTO v_TaxId, v_BORRBUSPERIND
            FROM loanapp.LoanBorrTbl
            WHERE     loanappnmb = p_LoanAppNmb
                  AND LOANBUSPRIMBORRIND = 'Y';

            IF v_BorrBusPerInd = 'B' THEN
                SELECT BUSPHYADDRSTR1NM,
                       BUSPHYADDRSTR2NM,
                       BUSPHYADDRCTYNM,
                       BUSPHYADDRSTCD,
                       BUSPHYADDRZIPCD,
                       BUSPHYADDRZIP4CD,
                       BUSPHYADDRCNTCD,
                       BUSPHYADDRSTNM,
                       BUSPHYADDRPOSTCD
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd,
                     v_LoanMailStNm,
                     v_LoanMailPostCd
                FROM loanapp.LoanBusTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND BUSMAILADDRSTR1NM IS NULL
                UNION ALL
                SELECT BUSMAILADDRSTR1NM,
                       BUSMAILADDRSTR2NM,
                       BUSMAILADDRCTYNM,
                       BUSMAILADDRSTCD,
                       BUSMAILADDRZIPCD,
                       BUSMAILADDRZIP4CD,
                       BUSMAILADDRCNTCD,
                       BUSMAILADDRSTNM,
                       BUSMAILADDRPOSTCD
                FROM loanapp.LoanBusTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND BUSMAILADDRSTR1NM IS NOT NULL;
            ELSE
                SELECT OcaDataOut (PERPHYADDRSTR1NM),
                       OcaDataOut (PERPHYADDRSTR2NM),
                       PERPHYADDRCTYNM,
                       PERPHYADDRSTCD,
                       PERPHYADDRZIPCD,
                       PERPHYADDRZIP4CD,
                       PERPHYADDRCNTCD,
                       PERPHYADDRSTNM,
                       PERPHYADDRPOSTCD
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd,
                     v_LoanMailStNm,
                     v_LoanMailPostCd
                FROM loanapp.LoanPERTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND PERMAILADDRSTR1NM IS NULL
                UNION ALL
                SELECT OcaDataOut (PERMAILADDRSTR1NM)     AS PERMAILADDRSTR1NM,
                       OcaDataOut (PERMAILADDRSTR2NM)     AS PERMAILADDRSTR2NM,
                       PERMAILADDRCTYNM,
                       PERMAILADDRSTCD,
                       PERMAILADDRZIPCD,
                       PERMAILADDRZIP4CD,
                       PERMAILADDRCNTCD,
                       PERMAILADDRSTNM,
                       PERMAILADDRPOSTCD
                FROM loanapp.LoanPERTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND PERMAILADDRSTR1NM IS NOT NULL;
            END IF;
        ELSE
            BEGIN
                SELECT LoanAppProjStr1Nm,
                       LOANAPPPROJSTR2NM,
                       LOANAPPPROJCTYNM,
                       LOANAPPPROJSTCD,
                       LOANAPPPROJZIPCD,
                       LOANAPPPROJZIP4CD,
                       'US'
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd
                FROM loanapp.LoanAppProjTbl
                WHERE LoanAppNmb = p_LoanAppNmb;
            EXCEPTION
                WHEN OTHERS THEN
                    NULL;
            END;
        END IF;

        IF v_PrgmCd = 'H' THEN
            SELECT SUM (CASE WHEN LOANPROCDTYPCD = '04' THEN LOANPROCDAMT ELSE 0 END),
                   SUM (CASE WHEN LOANPROCDTYPCD != '04' THEN LOANPROCDAMT ELSE 0 END)
            INTO v_LOANGNTYEIDLAMT, v_LOANGNTYPHYAMT
            FROM loanapp.LoanProcdTbl
            WHERE loanappnmb = p_Loanappnmb;
        END IF;

        /* --RGG 06/23/2013 Loan maturity start date indicator changes
         IF --(v_PrcsMthdCd = 'ILP' OR v_PrcsMthdCd = 'MLD') THEN
             v_PrgmCd <> 'C' THEN
             v_LOANGNTYMATSTIND := 'N';
         ELSE
             v_LOANGNTYMATSTIND := 'F';
         END IF;
         --RGG 06/23/2013 Loan maturity start date indicator changes*/

        INSERT INTO LoanGntyTbl (LoanAppNmb,
                                 PrgmCd,
                                 PrcsMthdCd,
                                 CohortCd,
                                 LoanAppNm,
                                 LoanAppRecvDt,
                                 LoanAppEntryDt,
                                 LoanAppRqstAmt,
                                 LoanAppSBAGntyPct,
                                 LoanInitlAppvAmt,
                                 LoanCurrAppvAmt,
                                 LoanInitlAppvDt,
                                 LoanLastAppvDt,
                                 LoanAppRqstMatMoQty,
                                 LoanAppEPCInd,
                                 LoanAppPymtAmt,
                                 LoanAppFullAmortPymtInd,
                                 LoanAppMoIntQty,
                                 LoanAppLifInsurRqmtInd,
                                 LoanAppRcnsdrtnInd,
                                 LoanAppInjctnInd,
                                 LoanCollatInd,
                                 LoanAppEWCPSnglTransPostInd,
                                 LoanAppEWCPSnglTransInd,
                                 LoanAppEligEvalInd,
                                 LoanAppNewBusCd,
                                 LoanAppDisasterCntrlNmb,
                                 LoanStatCd,
                                 LoanStatDt,
                                 LoanAppOrigntnOfcCd,
                                 LoanAppPrcsOfcCd,
                                 LoanAppServOfcCd,
                                 LoanAppFundDt,
                                 LoanAppMatDt,
                                 LoanNmb,
                                 LoanGntyFeeAmt,
                                 LoanGntyFeeBillAmt,
                                 LocId,
                                 LoanAppLSPHQLocId,
                                 PrtId,
                                 LoanSrvsLocId,
                                 LoanAppPrtAppNmb,
                                 LoanAppPrtLoanNmb,
                                 LoanOutBalAmt,
                                 LoanOutBalDt,
                                 LoanTypInd,
                                 RecovInd,
                                 LoanAppOutPrgrmAreaOfOperInd,
                                 LoanAppParntLoanNmb,
                                 LoanAppMicroLendrId,
                                 LoanTotUndisbAmt,
                                 ACHRtngNmb,
                                 ACHAcctNmb,
                                 ACHAcctTypCd,
                                 ACHTinNmb,
                                 LoanAppCDCGntyAmt,
                                 LoanAppCDCNetDbentrAmt,
                                 LoanAppCDCFundFeeAmt,
                                 LoanAppCDCSeprtPrcsFeeInd,
                                 LoanAppCDCPrcsFeeAmt,
                                 LoanAppCDCClsCostAmt,
                                 LoanAppCDCOthClsCostAmt,
                                 LoanAppCDCUndrwtrFeeAmt,
                                 LoanAppContribPct,
                                 LoanAppContribAmt,
                                 LoanAppPrtTaxId,
                                 LOANDISASTRAPPFEECHARGED,
                                 LOANDISASTRAPPDCSN,
                                 LOANASSOCDISASTRAPPNMB,
                                 LOANASSOCDISASTRLOANNMB,
                                 LoanAppCreatUserId,
                                 LoanAppCreatDt,
                                 POOLGROSSINITLINTPCT,
                                 POOLSBAGNTYBALINITLAMT,
                                 POOLLENDRGROSSINITLAMT,
                                 POOLORIGPARTINITLAMT,
                                 POOLLENDRPARTINITLAMT,
                                 FININSTRMNTTYPIND,
                                 LOANPURCHSDIND,
                                 LOANGNTYDFRDMNTHSNMB,
                                 LOANGNTYDFRTODT,
                                 LOANDISASTRAPPNMB,
                                 LoanMailStr1Nm,
                                 LoanMailStr2Nm,
                                 LoanMailCtyNm,
                                 LoanMailStCd,
                                 LoanMailZipCd,
                                 LoanMailZip4Cd,
                                 LoanMailCntryCd,
                                 LoanMailStNm,
                                 LoanMailPostCd,
                                 LoanAppNetEarnInd,
                                 LOANGNTYEIDLAMT,
                                 LOANGNTYEIDLCNCLAMT,
                                 LOANGNTYPHYAMT,
                                 LOANGNTYPHYCNCLAMT,
                                 UNDRWRITNGBY,
                                 LOANGNTYMATSTIND,
                                 LOANGNTYLCKOUTUPDIND,
                                 LOANAPPINTDTLCD,
                                 LoanOrigntnFeeInd,
                                 LOANONGNGFEECOLLIND,
                                 LoanGntyFeeDscntRt,
                                 loangntynotedt,
                                 LOANAPPCDCGROSSDBENTRAMT,
                                 LOANAPPBALTOBORRAMT,
                                 LOANPYMNINSTLMNTFREQCD,
                                 LoanPymntSchdlFreq,
                                 LoanAppRevlMoIntQty,
                                 LoanAppAmortMoIntQty,
                                 LoanPymtRepayInstlTypCd,
                                 LoanAppExtraServFeeAMT,
                                 LoanAppExtraServFeeInd,
                                 LoanAgntInvlvdInd,
                                 LoanAppMonthPayroll,
                                 LOAN1502CERTIND,
                                 LoanAppUsrEsInd,
                                 LOANAPPSCHEDCIND, 
                                 LOANAPPSCHEDCYR, 
                                 LOANAPPGROSSINCOMEAMT )
            SELECT p_LoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   p_CohortCd,
                   LoanAppNm,
                   LoanAppRecvDt,
                   LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,                                                     /* Initial Approval Amount */
                   LoanAppSBARcmndAmt,                                                     /* Current Approval Amount */
                   LoanAppAppvDt,                                                            /* Initial Approval Date */
                   LoanAppAppvDt,                                                            /* Current Approval Date */
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanCollatInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   'Y',
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   1
                       AS LoanStatCd,
                   SYSDATE
                       AS LoanStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   p_LoanAppServOfcCd,                                                            /* Servicing Office */
                   SYSDATE
                       AS LoanFundDt,
                   ADD_MONTHS (SYSDATE, TRUNC (LoanAppRqstMatMoQty))
                       AS LoanAppMatDt,
                   p_LoanNmb,
                   p_LoanGntyFeeAmt,
                   v_LoanGntyFeeBillAmt,
                   LocId,
                   LoanAppLSPHQLocId,
                   PrtId,
                   LocId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   0,
                   NULL,
                   (SELECT LoanTypInd
                    FROM sbaref.PrcsMthdTbl z
                    WHERE z.PrcsMthdCd = a.PrcsMthdCd),
                   RecovInd,
                   NVL (LoanAppOutPrgrmAreaOfOperInd, 'N'),
                   LoanAppParntLoanNmb,
                   LoanAppMicroLendrId,
                   LoanAppSBARcmndAmt,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppCDCGntyAmt,
                   LoanAppCDCNetDbentrAmt,
                   LoanAppCDCFundFeeAmt,
                   NVL (LoanAppCDCSeprtPrcsFeeInd, 'Y'),
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCOthClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppContribPct,
                   LoanAppContribAmt,
                   b.LoanAppPrtTaxId,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   LoanAppCreatUserId,
                   LoanAppCreatDt,
                   CASE WHEN v_PrcsMthdCd = 'SMP' THEN v_POOLGROSSINITLINTPCT ELSE NULL END,
                   v_POOLLOANSBAGNTYBALAMT,
                   v_POOLLOANLENDRGROSSAMT,
                   v_POOLLOANORIGPARTAMT,
                   v_POOLLOANLENDRPARTAMT,
                   FININSTRMNTTYPIND,
                   (CASE
                        WHEN (SELECT LoanTypInd
                              FROM sbaref.PrcsMthdTbl z
                              WHERE z.PrcsMthdCd = a.PrcsMthdCd) != 'D' THEN
                            'LL'
                        ELSE
                            'SS'
                    END),
                   LOANAPPPYMTDEFMONMB,
                   ADD_MONTHS (SYSDATE, LOANAPPPYMTDEFMONMB),
                   LOANDISASTRAPPNMB,
                   v_LoanMailStr1Nm,
                   v_LoanMailStr2Nm,
                   v_LoanMailCtyNm,
                   v_LoanMailStCd,
                   v_LoanMailZipCd,
                   v_LoanMailZip4Cd,
                   v_LoanMailCntryCd,
                   v_LoanMailStNm,
                   v_LoanMailPostCd,
                   LoanAppNetEarnInd,
                   NVL (v_LOANGNTYEIDLAMT, 0),
                   0,
                   NVL (v_LOANGNTYPHYAMT, 0),
                   0,
                   CASE WHEN PrcsMthdCd IN ('PPP', 'PPS') THEN 'LNDR' ELSE UNDRWRITNGBY END
                       UNDRWRITNGBY,
                   LOANGNTYMATSTIND,
                   'M',
                   LOANAPPINTDTLCD,
                   p_LoanOrigntnFeeInd,
                   p_LOANONGNGFEECOLLIND,
                   v_LoanGntyFeeDscntRt,
                   loangntynotedt,
                   LOANAPPCDCGROSSDBENTRAMT,
                   LOANAPPBALTOBORRAMT,
                   LOANPYMNINSTLMNTFREQCD,
                   LoanPymntSchdlFreq,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanPymtRepayInstlTypCd,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   LoanAgntInvlvdInd,
                   LoanAppMonthPayroll,
                   -- JP 06/12/2020 added LOAN1502CERTIND
                   (SELECT 'Y'     LOAN1502CERTIND
                    FROM DUAL
                    WHERE EXISTS
                              (SELECT 1
                               FROM PARTNER.PRTLOCACHACCTTBL p
                               WHERE     b.locid = p.locid
                                     AND p.VALIDPRTDISBTYP = '1102'
                                     AND p.LNDRCERTIND = 'Y'))
                       AS LOAN1502CERTIND,
                   LoanAppUsrEsInd,
                   LOANAPPSCHEDCIND, 
                   LOANAPPSCHEDCYR, 
                   LOANAPPGROSSINCOMEAMT
            FROM loanapp.LoanAppTbl a, loanapp.LoanAppPrtTbl b
            WHERE     a.LoanAppNmb = p_LoanAppNmb
                  AND a.LoanAppNmb = b.LoanAppNmb(+);

        p_RetStat := SQLCODE;
        p_RetVal := SQL%ROWCOUNT;

        IF v_PrcsMthdCd = 'SMP' THEN
            LOAN.LOANCHNGLOGINSCSP (p_LoanAppNmb, 'P', USER, NULL, NULL);
        END IF;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
    p_RetStat := NVL (p_RetStat, 0);
END MOVELOANGNTYCSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDSUMTSP (
    p_LOANAPPNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
	p_PROCDTYPCD 				CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOAN.LOANGNTYSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD and LOANAPPNMB=p_LOANAPPNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOAN.LOANGNTYPROCDTBL set LOANPROCDAMT = nvl(quan,0) where PROCDTYPCD = p_PROCDTYPCD and LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LOANAPPNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDSUMTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDSUMTSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSELTSP (p_Identifier   IN     NUMBER := 0,
                                                 p_LoanAppNmb   IN     NUMBER := 0,
                                                 p_LoanNmb      IN     CHAR := NULL,
                                                 p_LocId        IN     NUMBER := NULL,
                                                 p_PrtId        IN     NUMBER := NULL,
                                                 p_FromDate     IN     DATE := NULL,
                                                 p_ToDate       IN     DATE := NULL,
                                                 p_FisclYrNmb   IN     NUMBER := NULL,
                                                 p_PrcsMthdCd          CHAR := NULL,
                                                 p_RetVal          OUT NUMBER,
                                                 p_SelCur          OUT SYS_REFCURSOR) AS
    /*
      Created by:
      Created on :
      Purpose    :
      Parameters:
      Modified by: APK 09/22/2010- modified Identifier 18 to use proper left outer join as the query was not returning data when there is no data in LoanTransTbl.
     APK         03/18/2011      Modified to add LoanAppMatExtDt
     APK 05/06/2011 -- modified to remove column OrigntnLocId as it was removed from the table.
     APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes.
     APK -- 11/03/2011-- modified to add new column GtyFeeUnCollAmt as the fee was not correctly calculated from elips.
     THis column will now be synched with elips.
     APk -- 11/15/2011 -- ID = 22, modified to remove the condition ' LoanStatCd  NOT IN (4, 5)' so as to display irrespective of the status.
     APK -- 11/18/2011 -- modified ID = 22 to add additional field PrtId.
     APK -- 11/29/2011-- modifed ID = 25 to use proper schema for sbaref.loanstatcdtbl instead of Loan.
     SP-----1/5/2011-----Modified ID=0 to add new column LOANPYMTTRANSCD
     SP-----2/2/2012-----Modified ID=18  to add new column LOANPYMTTRANSCD
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     SP --- 03/05/2012 -- Modified ID=0,18 to add new column LOANCOFSEFFDT
     SP --- 04/18/2012 -- Modified ID=0,18 to add new column LoanAppNetEarnInd
     SP --- 05/01/2012 -- Modified ID=0,18 to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
    LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
     RGG --- 10/01/2012 -- Changes ID = 0,17,18 to add LoanAppActPymtAmt field.
    SP -- 04/04/2013 -- ADDED UNDERWRITINGBY FOR DISPLAY PURPOSE
    RSURAPA -- 05/16/2013 Added selection of notedt from the loangntytbl for p_Identifier 18
    RSURAPA -- 05/22/2013 The column name notedt was changed to loangntynotedt in the loangntytbl.
    SP --- 05/29/2013 -- removed Changes ID = 0,17,18 to add LoanAppActPymtAmt field.
    RSURAPA -- 05/17/2013 -- Added select of new column LOANGNTYMATSTIND from the loangntytbl for p_Identifier 18
    RGG -- 09/19/2013 -- aded identifier 26 to Return the PMT close date and close reason codes for a given loan number
    SP---10/07/2013---Removed street number and street suffix name as part of appending these to street name changes
    SP -- 10/29/2013 -- Modified to add new column LOANAPPINTDTLCD to id=0
    SP -- 10/30/2013 -- Modified to add LOANDISASTRAPPNMB to id =0 as these values are passed into LoanGntyPreInsTsp in the case of cancel and reinstate web screens.
    RGG -- 11/05/2013 -- Added Id 27 to return the current LOANAPPINTDTLCD and LOANINTDTLDESCTXT.
    RSURAPA -- 11/09/13 -- modified identifier 18 to return additional column LoanAppIntDtlCd
    sp -- 11/27/2013 --Removed refernces to  columns :IMRtTypCd(0,17,18,24),LoanAppFixVarInd(0,17,18,24),
    LoanAppBaseRt(0,17,18,24),LoanAppInitlIntPct(0,17,18,24),LoanAppInitlSprdOvrPrimePct(0,17,18,24),LoanAppAdjPrdCd(0,18,24)
    SP -- 02/05/2014 Modified id=18 as the field name LoanGntyFeeWavrInd  was changed to LOANOrigntnFeeInd
    SP -- 04/03/2014 Modified id =0,18 to add LOANGNTYPURISSIND,LOANGNTYPURISSDT to alert a Loan Officer performing a purchase review in GPTS of possible guaranty issues
    RGG -- 04/04/2014 -- Removed NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM
    SB -- 04/04/2014 removed Loanappfrnchsind
    PSP -- 04/17/2014 -- Added license type,draw type,SBIC pooling date to id = 0,18 and added id = 28 to get all draw downs for a commitment
    SB -- Added Identifier 38 for Extract XML
    PSP -- 04/11/2014 -- Added draw type description to id = 28
    RGG -- 01/26/2015 -- Added LOANAUTOAMSSTOPIND to Id 18.
    PSP -- 02/25/2015 -- Modified id = 18,39 to add loanpurofccd to capture purchasing office using office code
    of the recommending official so would not lose count of the purchase once the file is assigned to the appropriate servicing office
    SB -- 03/24/2015 -- Added extra column a.LoanAppDisasterCntrlNmb for idnetifier 38
    RGG -- 05/19/2015 -- In order to allow lender updates to disbursed loans if they were lender-underwritten,UndrwritngBy is added to ID 18.
    NK --03/21/2016 --TO_CHAR (ColumnName, '990.999')  to TO_CHAR (ColumnName, 'FM990.000')
    NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
    RGG -- 04/04/2016 -- Added LOANLNDRASSGNLTRDT wherever LOANCOFSEFFDT is present.
    MZ -- 09/22/2016 -- Added identifier 40 to return micro lender with 8 digit loan #
    RGG -- 12/20/2016 -- OPSMDEV 1275 Added Interest Rate, Status Description to ID 18
    JP -- 2/27/2017 -- OPSMDEV-1341 Added NAICSYrNmb, 3 LoanFinanclStmt columns, LoanGntyNoteDt, LoanTotUndisbAmt and LoanAppProjCntyCd to Identifier 38
    RY --03/02/2017 -- OPSMDEV 1400--Added new column LoanGntyCsaAmtRcvd to ID 0,18,28,38
    JP -- 3/06/2017 -- OPSMDEV-1341 Added LoanAppFundDt to Identifier 38
    RY -- 03/17/2017 -- OPSMDEV 1401 -- Changed the references for 'EconDevObjctCd' in ID 38.
    SS--03/27/2017 --OPSMDEV 1329 Added identifier 29
    MZ -- 03/30/2017, Identifier 38, added LoanOutBalAmt and LoanOutBalDt
    JP -- 04/06/2017 -- Added LoanAppFirstDisbDt and LoanDisbInd to identifier 38
    RY--04/10/2017 --CAFSOPER518--Added LoanTypInd  to ID 0
    BJR -- 04/25/2017 -- CAFSOPER719 -- Added p_Identifier = 3 to Return LoanGntyTypInd from Loan.LoanGntyTbl
    SS--05/19/2017 --OPSMDEV1436 Added CDCServFeePct to identifier 0 and created identifier 41
    RY--06/07/2017-- OPSMDEV 1440--Added LOANGNTY1201ELCTRNCSTMTIND to ID 0 and 38
    NK -- 07/19/2017--CAFSOPER 805 -- Added LoanAppBalToBorrAmt,LoanAppCDCGrossDbentrAmt in ID 18,38
    NK--07/19/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt in ID 0, 18
    NK --08/08/2017-- Added LOANGNTY1201ELCTRNCSTMTIND to ID 18
    NK-- 08/22/2017--OPSMDEV-1439--Added  LOANPURCHSDIND and LOANGNTY1201ELCTRNCSTMTIND to ID 41
    SS--11/16/2017--OPSMDEV- 1568--Added LOANBUSESTDT to identifier 38
    NK--12/01/2017--OPSMDEV-- 1598--Added LoanMailAddrsInvldInd to Id 0, 38
    RY--01/16/2018--OPSMDEV--1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty to Id 0,17,18,24,28 and 38
    NK--02/05/2018--OPSMDEV --1507 -- Added LoanGntyFeeBillAdjAmt to ID 0, 18
    SS--03/23/2018--OPSMDEV 1728 --Added LoanGntyDbentrIntRt to identifier 0 and 18
    JP -- 05/07/2018 -- OPSMDEV 1717 -  LoanIntBaseRt and LoanIntRt fields formatted to 5 digit decimal as 'FM990.00000'
   SS--0/13/2018 --OPSMDEV 1861-- Added LoanAgntInvlvdInd to ID 0,18,41 and 38
   NK--08/15/2018--CAFSOPER 2021--added new id 42
   SS--10/26/2018--OPSMDEV 1965 added LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  to identifier 0and 38
   RY--07/08/2019--opsmdev-2240:: Added LoanGntycorrespLangCd  to 0,18 and 38
   SS--04/03/2020 Added LoanAppMonthPayroll to id 0 and 38 CARESACT - 66
   SS--04/30/2020 Added Loan1502CertInd to identifier 0 and 18
   SS--05/06/2020 Added   lendrRebateFeeAmt,LendrRebatefeePaidDt  CARESACT 105
   JP -- 5/22/2020 Rewrote the query for id 20
   JP -- 06/02/2020 id 18 modified to get ACH info from PIMS for PrcsMthdCd PPP
   JP -- 07/23/2020 CAFSOPER-3775 added DspstnStatCd to ID 0
   SL -- 08/28/2020 CARESACT-625 added DspstnStatCd to ID 38
   JP -- 09/17/2020 CAFSOPER-3937 added DspstnStatDescTxt to id 18
   JP--1/9/2021  - SODSTORY 376 added column LoanAppUsrEsInd to id 0,18,38
   SL--02/01/2021 SODSTORY 446 added column LoanGntyPayRollChngInd
   SL --03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT
   JP -- 05/11/2021 SODSTORY-857 ID 20   -- Removed TRUNC from LoanAppFundDt and LOANSTATDT where clause
   JP -- 05/27/2021 CAFSOPER-4119 ID 21   -- Removed TRUNC from LoanAppFundDt and LOANSTATDT where clause
   */
    v_FromDate   DATE;
    v_ToDate     DATE;
    v_fy         NUMBER;
BEGIN
    /* Select Row On the basis of Key*/
    --IF p_Identifier = 0 THEN
    CASE p_Identifier
        WHEN 0 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)              LoanAppRqstAmt,
                                         LoanAppSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)            LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         LoanAppRqstMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)              LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)              LoanGntyFeeAmt,
                                         LocId,
                                         LoanAppLSPHQLocId,
                                         PrtId,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)               LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanTotUndisbAmt,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanDisbInd,
                                         LoanSoldScndMrktInd,
                                         ACHRTNGNMB,
                                         ACHACCTNMB,
                                         ACHACCTTYPCD,
                                         ACHTinNmb,
                                         LoanIntPaidAmt,
                                         LoanIntPaidDt,
                                         RecovInd,
                                         LoanIntPaidAmt,
                                         LoanIntPaidDt,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)           LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)      LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)        LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)        LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)        LoanAppCDCClsCostAmt,
                                         ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)        LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)     LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)           LoanAppContribAmt,
                                         LoanAppOutPrgrmAreaOfOperInd,
                                         LoanAppParntLoanNmb,
                                         LoanAppPrtTaxId,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         POOLGROSSINITLINTPCT,
                                         POOLLENDRGROSSINITLAMT,
                                         POOLLENDRPARTINITLAMT,
                                         POOLORIGPARTINITLAMT,
                                         POOLSBAGNTYBALINITLAMT,
                                         POOLLENDRPARTCURRAMT,
                                         POOLORIGPARTCURRAMT,
                                         POOLLENDRGROSSCURRAMT,
                                         LoanPurchsdInd,
                                         LoanGntyFeeRebateAmt,
                                         LoanAppMatExtDt,
                                         Loan1201NotcInd,
                                         LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         LOANPYMTTRANSCD,
                                         LOANCOFSEFFDT,
                                         SBICLICNSNMB,
                                         LOANMAILSTR1NM,
                                         LOANMAILSTR2NM,
                                         LOANMAILCTYNM,
                                         LOANMAILSTCD,
                                         LOANMAILZIPCD,
                                         LOANMAILZIP4CD,
                                         LOANMAILCNTRYCD,
                                         LOANMAILSTNM,
                                         LOANMAILPOSTCD,
                                         LOANAPPNETEARNIND,
                                         LOANPYMTINSTLMNTTYPCD,
                                         LOANPYMNINSTLMNTFREQCD,
                                         LOANTOTDEFNMB,
                                         LOANTOTDEFMONMB,
                                         UNDRWRITNGBY,
                                         LOANGNTYNOTEDT,
                                         LOANGNTYMATSTIND,
                                         LOANAPPINTDTLCD,
                                         LOANDISASTRAPPNMB,
                                         LOANPYMTREPAYINSTLTYPCD,
                                         LOANGNTYPURISSIND,
                                         LOANGNTYPURISSDT,
                                         LOANSBICLICNSTYP,
                                         loansbicdrawtyp,
                                         loansbicschdlpooldt,
                                         LOANLNDRASSGNLTRDT,
                                         LoanGntyCsaAmtRcvd,
                                         LoanTypInd,
                                         CDCServFeePct,
                                         LOANGNTY1201ELCTRNCSTMTIND,
                                         LoanGntyNoteIntRt,
                                         LoanGntyNoteMntlyPymtAmt,
                                         LoanMailAddrsInvldInd,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanGntyFeeBillAdjAmt,
                                         LoanGntyFeeBillAmt,
                                         LoanGntyDbentrIntRt,
                                         LoanAgntInvlvdInd,
                                         LoanAppExtraServFeeAMT,
                                         LoanAppExtraServFeeInd,
                                         LoanGntycorrespLangCd,
                                         LoanAppMonthPayroll,
                                         LoanGntyPayRollChngInd,
                                         LOANAPPSCHEDCIND,
                                         LOANAPPSCHEDCYR,
                                         LOANAPPGROSSINCOMEAMT,
                                         Loan1502CertInd,
                                         LendrRebateFeeAmt,
                                         LendrRebatefeePaidDt,
                                         DspstnStatCd,
                                         LoanAppUsrEsInd
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 2 THEN
            /*Check The Existance Of Row*/
            BEGIN
                OPEN p_SelCur FOR SELECT 1
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 3 THEN
            /*Return LoanGntyTypInd*/
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppSBAGntyPct, loantypind AS LoanGntyTypInd
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 11 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppEligEvalInd,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)     LoanAppRqstNum,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         LoanAppPrtTaxId
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            --p_RetVal := SQL%ROWCOUNT;
            END;
        WHEN 15 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNm
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 16 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT DISTINCT LoanNmb,
                                                  LoanAppNmb,
                                                  PrgmCd,
                                                  PrcsMthdCd,
                                                  PrtId,
                                                  LocId,
                                                  LoanAppPrcsOfcCd,
                                                  LoanStatCd
                                  FROM Loan.LoanGntyTbl
                                  WHERE     LoanNmb = p_LoanNmb
                                        AND LocId = NVL (p_LocId, LocId)
                                        AND PrtId = NVL (p_PrtId, PrtId);
            END;
        WHEN 17 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNm,
                                         a.PrtId,
                                         a.LocId,
                                         e.PrtLglNm,
                                         PhyAddrStr1Txt,
                                         PhyAddrStr2Txt                                       PhyAddrCtyNm,
                                         PhyAddrStCd,
                                         PhyAddrPostCd,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)               LoanCurrAppvAmt,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)              LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)                LoanCurrGntyFeeAmt,
                                         ROUND (TO_NUMBER (c.LoanTransChngGntyFeeAmt), 2)     LoanInitlGntyFeeAmt,
                                         LoanAppSBAGntyPct                                    LoanCurrSBAGntyPct,
                                         c.LoanTransSBAGntyPct                                LoanInitlSBAGntyPct,
                                         LoanAppRqstMatMoQty                                  LoanCurrRqstMatMoQty,
                                         c.LoanTransMatMoQty                                  LoanInitlRqstMatMoQty,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)                LoanAppPymtAmt,
                                         LoanAppMoIntQty,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppEPCInd,
                                         LoanCollatInd,
                                         LoanAppInjctnInd,
                                         LoanAppEntryDt,
                                         LoanAppRecvDt,
                                         LoanLastAppvDt,
                                         LoanInitlAppvDt,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)                 LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppPrtTaxId,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty
                                  FROM Loan.LoanGntyTbl a,
                                       partner.PhyAddrTbl b,
                                       Loan.LoanTransTbl c,
                                       sbaref.IMRtTypTbl d,
                                       partner.PrtTbl e
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.LoanAppNmb = c.LoanAppNmb
                                        AND c.LoanTransTypCd = 1
                                        AND a.LocId = b.LocId
                                        AND a.PrtId = e.PrtId
                                        AND PhyAddrSeqNmb = (SELECT MAX (PhyAddrSeqNmb)
                                                             FROM partner.PhyAddrTbl z
                                                             WHERE     z.LocId = a.LocId
                                                                   AND z.PrtCntctNmb IS NULL
                                                                   AND ValidAddrTyp = 'Phys');
            END;
        WHEN 18                        /* RSURAPA - 05/16/2013 added selection of loangntynotedt from the loangntytbl */
                THEN                /* RSURAPA -- 06/17/2013 added selection of LOANGNTYMATSTIND from the loangntytbl */
            BEGIN
                OPEN p_SelCur FOR SELECT a.LoanAppNmb,
                                         PrgmCd,
                                         CASE
                                             WHEN a.PrgmCd != 'A' THEN 'N'
                                             WHEN a.LoanAppFundDt < TO_DATE ('20131001', 'yyyymmdd') THEN 'Y'
                                             ELSE a.LoanOngngFeeCollInd
                                         END
                                             AS LoanOngngFeeCollInd,
                                         NVL (LOANOrigntnFeeInd, 'Y')
                                             AS LoanOrigntnFeeInd,
                                         a.PrcsMthdCd,
                                         b.PrcsMthdDesc,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                                             LoanAppRqstAmt,
                                         a.LoanAppSBAGntyPct
                                             LoanCurrSBAGntyPct,
                                         e.LoanTransSBAGntyPct
                                             LoanOrigSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)
                                             LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)
                                             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         a.LoanAppRqstMatMoQty
                                             LoanCurrMatMoQty,
                                         e.LoanTransMatMoQty
                                             LoanOrigMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                                             LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         a.LoanAppOrigntnOfcCd,
                                         c.SBAOfc1Nm
                                             LoanOrigntnOfcNm,
                                         a.LoanAppPrcsOfcCd,
                                         d.SBAOfc1Nm
                                             LoanPrcsOfcNm,
                                         a.LoanAppServOfcCd,
                                         g.SBAOfc1Nm
                                             LoanServOfcNm,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)
                                             LoanCurrGntyFeeAmt,
                                         ROUND (
                                               (SELECT SUM (LOANTRANSGNTYFEEBILLCHNGAMT)
                                                FROM loan.loantranstbl z
                                                WHERE z.loanappnmb = a.loanappnmb)
                                             + NVL (a.LoanGntyFeeBillAdjAmt, 0),
                                             2)
                                             LoanGntyFeeBilledAmt,
                                         --   ROUND((a.LoanGntyFeeBillAmt + a.LoanGntyFeeBillAdjAmt),2)LoanGntyFeeBilledAmt,
                                         a.LoanGntyFeeBillAdjAmt,
                                         ROUND (TO_NUMBER (e.LoanTransChngGntyFeeAmt), 2)
                                             LoanOrigGntyFeeAmt,
                                         a.LocId,
                                         a.LoanAppLSPHQLocId,
                                         a.PrtId,
                                         a.LoanSrvsLocId,
                                         p.PrtLglNm
                                             LoanAppPrtNm,
                                         p.PrtLocFIRSNmb
                                             LoanAppFIRSNmb,
                                         p.PhyAddrStr1Txt
                                             LoanAppPrtStr1,
                                         p.PhyAddrCtyNm
                                             LoanAppPrtCtyNm,
                                         p.PhyAddrStCd
                                             LoanAppPrtStCd,
                                         p.PhyAddrPostCd,
                                         l.PrtLocNm,
                                         a.LoanAppPrtAppNmb,
                                         a.LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)
                                             LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanSoldScndMrktInd,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanTotUndisbAmt,
                                         LoanDisbInd,
                                         RecovInd,
                                         /*,  (select LoanAppInitlIntPct from loanapp.LoanAppTbl where LoanAppNmb =:p_LoanAppNmb) LoanOrignIntPct,*/
                                         CASE
                                             WHEN (SELECT COUNT (*)
                                                   FROM LoanGntyCollatTbl
                                                   WHERE LoanAppNmb = p_LoanAppNmb) > 0 THEN
                                                 'Y'
                                             ELSE
                                                 'N'
                                         END
                                             CollatExistInd,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         a.LoanAppParntLoanNmb,
                                         a.LoanAppMicroLendrId,
                                         (SELECT MAX (ACHROUTINGNMB)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHRtngNmb,
                                         (SELECT MAX (ACHACCTNMB)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHAcctNmb,
                                         (SELECT MAX (
                                                     CASE VALIDACHACCTTYP
                                                         WHEN 'Ck' THEN 'C'
                                                         WHEN 'GL' THEN 'G'
                                                         WHEN 'Sv' THEN 'S'
                                                         ELSE VALIDACHACCTTYP
                                                     END)
                                                     VALIDACHACCTTYP
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHAcctTypCd,
                                         (SELECT MAX (TaxId)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHTinNmb,
                                         a.LoanIntPaidAmt,
                                         a.LoanIntPaidDt,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)
                                             LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)
                                             LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)
                                             LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)
                                             LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)
                                             LoanAppCDCClsCostAmt,
                                             ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)
                                             LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)
                                             LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)
                                             LoanAppContribAmt,
                                         LOANSTATCMNTCD,
                                         a.LoanGntyFeePaidAmt,
                                         a.LOANGNTYDFRTODT,
                                         a.LOANGNTYDFRDMNTHSNMB,
                                         a.LOANGNTYDFRDGRSAMT,
                                         LOANDISASTRAPPFEECHARGED,
                                         LOANDISASTRAPPDCSN,
                                         LOANASSOCDISASTRAPPNMB,
                                         LOANASSOCDISASTRLOANNMB,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LOANPYMTTRANSCD,
                                         a.LOANDISASTRAPPNMB,
                                         a.loancofseffdt,
                                         a.SBICLICNSNMB,
                                         a.LOANMAILSTR1NM,
                                         a.LOANMAILSTR2NM,
                                         a.LOANMAILCTYNM,
                                         a.LOANMAILSTCD,
                                         a.LOANMAILZIPCD,
                                         a.LOANMAILZIP4CD,
                                         a.LOANMAILCNTRYCD,
                                         a.LOANMAILSTNM,
                                         a.LOANMAILPOSTCD,
                                         a.LOANAPPNETEARNIND,
                                         a.LOANPYMTINSTLMNTTYPCD,
                                         a.LOANPYMNINSTLMNTFREQCD,
                                         a.LOANTOTDEFNMB,
                                         a.LOANTOTDEFMONMB,
                                         a.LOANGNTYNOTEDT,
                                         a.LOANGNTYMATSTIND,
                                         a.LoanAppIntDtlCd,
                                         a.LOANPYMTREPAYINSTLTYPCD,
                                         a.LOANGNTYPURISSIND,
                                         a.LOANGNTYPURISSDT,
                                         a.LOANSBICLICNSTYP,
                                         a.loansbicdrawtyp,
                                         a.loansbicschdlpooldt,
                                         a.LoanGntyFeeDscntRt,
                                         ROUND (a.LoanGntyFeeAmt * (100 - NVL (a.LoanGntyFeeDscntRt, 0)) / 100, 2)
                                             AS LoanGntyDscntFeeAmt,
                                         a.LOANAUTOAMSSTOPIND,
                                         a.loanpurofccd,
                                         (SELECT SBAOfc1Nm
                                          FROM sbaref.SBAOfcTbl o
                                          WHERE o.SBAOfcCd = a.loanpurofccd)
                                             AS LoanPurOfcNm,
                                         a.UndrwritngBy,
                                         a.LOANLNDRASSGNLTRDT,
                                         LoanGntyCsaAmtRcvd,
                                         i.loanintrt,
                                         (SELECT LOANSTATDESCTXT
                                          FROM SBAREF.LOANSTATCDTBL z
                                          WHERE Z.LOANSTATCD = a.loanstatcd)
                                             AS status,
                                         a.LoanAppCDCGrossDbentrAmt,
                                         a.LoanAppBalToBorrAmt,
                                         a.LoanGntyNoteIntRt,
                                         a.LoanGntyNoteMntlyPymtAmt,
                                         a.LoanGnty1201ElctrncStmtInd,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         a.LoanGntyFeeBillAmt,
                                         a.LoanGntyDbentrIntRt,
                                         a.LoanAgntInvlvdInd,
                                         a.LoanGntycorrespLangCd,
                                         a.Loan1502CertInd,
                                         a.lendrRebateFeeAmt,
                                         a.LoanGntyPayRollChngInd,
                                         a.LOANAPPSCHEDCIND,
                                         a.LOANAPPSCHEDCYR,
                                         a.LOANAPPGROSSINCOMEAMT,
                                         a.LendrRebatefeePaidDt,
                                         (SELECT dspstn.DspstnStatDescTxt
                                          FROM SBAREF.DSPSTNSTATCDTBL dspstn
                                          WHERE dspstn.DspstnStatCd = a.DspstnStatCd)
                                             DspstnStatDescTxt,
                                         LoanAppUsrEsInd
                                  FROM sbaref.PrcsMthdTbl b,
                                       sbaref.SBAOfcTbl c,
                                       sbaref.SBAOfcTbl d,
                                       sbaref.SBAOfcTbl g,
                                       loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.LoanTransTbl e
                                       ON (    e.LoanAppNmb = a.LoanAppNmb
                                           AND e.LoanAppNmb = p_LoanAppNmb
                                           AND e.LoanTransTypCd = 1)
                                       LEFT OUTER JOIN partner.PrtSrchTbl p
                                       ON a.PrtId = p.PrtId
                                       LEFT OUTER JOIN partner.LocSrchTbl l
                                       ON a.LOANSRVSLOCID = l.LOCID
                                       LEFT OUTER JOIN LOAN.LOANINTDTLTBL i
                                       ON a.loanappnmb = i.loanappnmb
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.PrcsMthdCd = b.PrcsMthdCd
                                        AND a.LoanAppOrigntnOfcCd = c.SBAOfcCd
                                        AND a.LoanAppPrcsOfcCd = d.SBAOfcCd
                                        AND a.LoanAppServOfcCd = g.SBAOfcCd;
            END;
        WHEN 19 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT SBAOfc1Nm,
                                         SBAOfc2Nm,
                                         --                      DECODE (SBAOfcStrNmb, NULL, '', SBAOfcStrNmb || ' ')
                                         --                   || NVL (SBAOfcStrNm, '')
                                         SBAOfcStrNm,
                                         SBAOfcStr2Nm,
                                         SBAOfcCtyNm,
                                         SBAOfcTypCd,
                                         StCd,
                                         ZipCd4,
                                         ZipCd5
                                  FROM Loan.LoanGntyTbl a, sbaref.SBAOfcTbl b
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.LoanAppServOfcCd = b.SBAOfcCd;
            END;
        WHEN 20 THEN
            /* This report gives the total number of loans funded and the amount funded for the current Fiscal year
                Current month and Current day for each cohort  -- JP 5/22/2020 rewrote the query*/
            BEGIN
                -- get current Fiscal Year
                SELECT   EXTRACT (YEAR FROM SYSDATE)
                       + (CASE
                              WHEN EXTRACT (MONTH FROM SYSDATE) > 9 THEN 1
                              ELSE 0
                          END)
                           fy
                INTO v_fy
                FROM DUAL;

                SELECT TO_DATE ('10/01/' || (v_fy - 1), 'MM/DD/YYYY')                     v_FromDate,
                       TO_DATE ('09/30/' || (v_fy) || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS')     v_ToDate
                INTO v_FromDate, v_ToDate
                FROM DUAL;

                OPEN p_SelCur FOR
                    SELECT CohortCd,
                           'Approved'
                               StatusOfLoan,
                           COUNT (*)
                               cnt_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN 1
                                   ELSE 0
                               END)
                               cnt_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN 1
                                   ELSE 0
                               END)
                               cnt_dd,
                           SUM (LoanCurrAppvAmt)
                               sum_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN
                                       LoanCurrAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN
                                       LoanCurrAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_dd
                    FROM Loan.LoanGntyTbl
                    -- JP 5/11/2021 removed TRUNC(LoanAppFundDt)
                    WHERE     LoanAppFundDt BETWEEN v_FromDate AND v_ToDate
                          AND LoanStatCd <> 4
                    GROUP BY CohortCd
                    UNION ALL
                    SELECT CohortCd,
                           'Cancelled'
                               StatusOfLoan,
                           COUNT (*)
                               cnt_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN 1
                                   ELSE 0
                               END)
                               cnt_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN 1
                                   ELSE 0
                               END)
                               cnt_dd,
                           SUM (LoanInitlAppvAmt)
                               sum_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN
                                       LoanInitlAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN
                                       LoanInitlAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_dd
                    FROM Loan.LoanGntyTbl
                    -- JP 5/11/2021 removed TRUNC(LOANSTATDT)
                    WHERE     LOANSTATDT BETWEEN v_FromDate AND v_ToDate
                          AND LoanStatCd = 4
                    GROUP BY CohortCd;
            /* Replaced LoanAppFundDt >= v_FromDate  AND LoanAppFundDt < v_ToDate*/
            /* Total number of Funded loans for the current Fiscal Year */
            /* Total number of Funded loans for the current month */
            /* Total number of Funded loans for today */
            /* Total Amount Funded for the current Fiscal Year */
            /* Total Amount Funded for the current Month */
            /* Total Amount Funded for today */

            END;
        WHEN 21 THEN
            BEGIN
                IF p_FisclYrNmb IS NOT NULL THEN
                    BEGIN
                        --  v_FromDate := TO_DATE ('10/01/' || RPAD (p_FisclYrNmb - 1, 4, ' '), 'MM/DD/YYYY');
                        --   v_ToDate := TO_DATE ('10/01/' || RPAD (p_FisclYrNmb, 4, ' '), 'MM/DD/YYYY');

                        SELECT TO_DATE ('10/01/' || (p_FisclYrNmb - 1), 'MM/DD/YYYY')                     v_FromDate,
                               TO_DATE ('09/30/' || (p_FisclYrNmb) || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS')     v_ToDate
                        INTO v_FromDate, v_ToDate
                        FROM DUAL;
                    END;
                END IF;

                OPEN p_SelCur FOR SELECT CohortCd,
                                         'Approved'                           StatusOfLoan,
                                         COUNT (*)                            cnt_yy,
                                         ROUND (SUM (LoanCurrAppvAmt), 2)     sum_yy
                                  FROM loan.LoanGntyTbl
                                  -- JP 5/27/2021 removed TRUNC(LoanAppFundDt)
                                  WHERE     LoanAppFundDt BETWEEN v_FromDate AND v_ToDate
                                        AND LoanStatCd <> 4
                                  GROUP BY CohortCd
                                  UNION ALL
                                  SELECT CohortCd,
                                         'Cancelled'                           StatusOfLoan,
                                         COUNT (*)                             cnt_yy,
                                         ROUND (SUM (LoanInitlAppvAmt), 2)     sum_yy
                                  FROM loan.LoanGntyTbl
                                  -- JP 5/27/2021 removed TRUNC(LOANSTATDT)
                                  WHERE     LOANSTATDT BETWEEN v_FromDate AND v_ToDate
                                        AND LoanStatCd = 4
                                  GROUP BY CohortCd;
            /* IAC 05/21/2020 Replaced LoanAppFundDt with LOANSTATDT */
            /* Total number of Funded loans*/
            /* Total Amount Funded*/
            /* Total number of Funded loans*/
            /* Total Amount Funded*/
            END;
        WHEN 22 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         LoanNmb,
                                         LoanAppNm,
                                         PrcsMthdCd,
                                         LoanStatCd,
                                         LocId,
                                         LoanSrvsLocId,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)     LoanCurrAppvAmt,
                                         PrtId
                                  FROM LoanGntyTbl
                                  WHERE (   LocId = p_LocId
                                         OR LoanSrvsLocId = p_LocId);
            --AND LocId IS NOT NULL
            /*do not include purchased loans...DJ07/20/2010*/
            /*Need to display all loans, undid previous changes.APK 07/21/2010*/
            --and LoanStatCd  NOT IN (4, 5);
            END;
        WHEN 23 THEN
            BEGIN
                OPEN p_SelCur FOR
                    SELECT SUM (
                               CASE
                                   WHEN     EXTRACT (YEAR FROM LoanAppFundDt) = EXTRACT (YEAR FROM v_FromDate)
                                        AND EXTRACT (MONTH FROM LoanAppFundDt) = EXTRACT (MONTH FROM v_FromDate) THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurMonNmb,
                           SUM (
                               CASE
                                   WHEN (  EXTRACT (YEAR FROM LoanAppFundDt)
                                         + (CASE
                                                WHEN EXTRACT (MONTH FROM LoanAppFundDt) > 9 THEN 1
                                                ELSE 0
                                            END)) = (  EXTRACT (YEAR FROM v_FromDate)
                                                     + (CASE
                                                            WHEN EXTRACT (MONTH FROM v_FromDate) > 9 THEN 1
                                                            ELSE 0
                                                        END)) THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurFyNmb,
                           SUM (
                               CASE
                                   WHEN     EXTRACT (YEAR FROM LoanAppFundDt) = EXTRACT (YEAR FROM v_FromDate)
                                        AND TO_CHAR (LoanAppFundDt, 'IW') = TO_CHAR (v_FromDate, 'IW') THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurWeekNmb
                    FROM LoanGntyTbl
                    WHERE     PrtId = p_PrtId
                          AND PrcsMthdCd = p_PrcsMthdCd
                          AND LoanStatCd != 4;
            END;
        WHEN 24 THEN
            /*Retrun row based on LoanNmb*/
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)              LoanAppRqstAmt,
                                         LoanAppSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)            LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         LoanAppRqstMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)              LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)              LoanGntyFeeAmt,
                                         LocId,
                                         PrtId,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)               LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanTotUndisbAmt,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanDisbInd,
                                         LoanSoldScndMrktInd,
                                         ACHRtngNmb,
                                         ACHAcctNmb,
                                         ACHAcctTypCd,
                                         ACHTinNmb,
                                         LoanAppPrtTaxId,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)           LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)      LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)        LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)        LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)        LoanAppCDCClsCostAmt,
                                         ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)        LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)     LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)           LoanAppContribAmt,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanIntPaidDt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         POOLLENDRGROSSINITLAMT,
                                         POOLLENDRPARTINITLAMT,
                                         POOLORIGPARTINITLAMT,
                                         POOLSBAGNTYBALINITLAMT,
                                         POOLLENDRPARTCURRAMT,
                                         POOLORIGPARTCURRAMT,
                                         POOLLENDRGROSSCURRAMT,
                                         LoanAppMatExtDt,
                                         Loan1201NotcInd,
                                         LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl
                                  WHERE LoanNmb = p_LoanNmb;
            END;
        WHEN 25 THEN
            BEGIN
                OPEN p_SelCur FOR /* to classify according to collateral types for liquidation loans -date created:7/28/2011*/
                                  SELECT a.loanappnmb,
                                         c.loannmb,
                                         a.loancollattypcd,
                                         b.loancollattypdesctxt,
                                         a.loancollatseqnmb,
                                         a.loancollatdesc,
                                         a.loancollatmrktvalamt,
                                         d.lqdstatcd,
                                         e.lqdstatdesctxt,
                                         c.loanstatcd,
                                         f.loanstatdesctxt,
                                         'classification'     AS classification
                                  FROM loan.loangntycollattbl a,
                                       sbaref.LOANCOLLATTYPCDTBL b,
                                       loan.loangntytbl c,
                                       loan.lqdtbl d,
                                       sbaref.lqdstatcdtbl e,
                                       sbaref.loanstatcdtbl f
                                  WHERE     c.loannmb = p_LoanNmb
                                        AND a.loancollattypcd = b.loancollattypcd(+)
                                        AND a.loanappnmb = c.loanappnmb
                                        AND a.loanappnmb = d.loanappnmb
                                        AND d.lqdstatcd = e.lqdstatcd(+)
                                        AND c.loanstatcd = f.loanstatcd(+)
                                        AND d.lqdseqnmb = (SELECT MAX (g.lqdseqnmb)
                                                           FROM lqdtbl g
                                                           WHERE g.loanappnmb = c.loanappnmb)
                                  ORDER BY a.loancollatseqnmb;
            END;
        WHEN 26 THEN
            BEGIN
                OPEN p_SelCur FOR         /* Return the PMT close date and close reason codes for a given loan number:*/
                                  SELECT a.loanappnmb,
                                         a.loannmb,
                                         a.LOANSTATCMNTCD,
                                         a.CURREFCLOSDT,
                                         a.CURREFSTATCD
                                  FROM loan.loangntytbl a
                                  WHERE a.loannmb = p_LoanNmb;
            END;
        WHEN 27 THEN
            BEGIN
                OPEN p_SelCur FOR         /* Return the PMT close date and close reason codes for a given loan number:*/
                                  SELECT a.loanappnmb, a.LOANAPPINTDTLCD, b.LOANINTDTLDESCTXT
                                  FROM loan.loangntytbl a, sbaref.LOANINTDTLCDTBL b
                                  WHERE     a.loanappnmb = p_loanappnmb
                                        AND a.LOANAPPINTDTLCD = b.LOANINTDTLCD;
            END;
        WHEN 28 THEN
            BEGIN
                OPEN p_SelCur FOR                                     /* get all draw downs for commitment loan number*/
                                  SELECT a.LoanAppNmb,
                                         PrgmCd,
                                         CASE
                                             WHEN a.PrgmCd != 'A' THEN 'N'
                                             WHEN a.LoanAppFundDt < TO_DATE ('20131001', 'yyyymmdd') THEN 'Y'
                                             ELSE a.LoanOngngFeeCollInd
                                         END
                                             AS LoanOngngFeeCollInd,
                                         NVL (LOANOrigntnFeeInd, 'Y')
                                             AS LoanOrigntnFeeInd,
                                         a.PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                                             LoanAppRqstAmt,
                                         a.LoanAppSBAGntyPct
                                             LoanCurrSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)
                                             LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)
                                             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         a.LoanAppRqstMatMoQty
                                             LoanCurrMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                                             LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         a.LoanStatCd,
                                         LoanStatDt,
                                         a.LoanAppOrigntnOfcCd,
                                         a.LoanAppPrcsOfcCd,
                                         a.LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)
                                             LoanCurrGntyFeeAmt,
                                         ROUND (
                                             (SELECT SUM (LOANTRANSGNTYFEEBILLCHNGAMT)
                                              FROM loan.loantranstbl z
                                              WHERE z.loanappnmb = a.loanappnmb),
                                             2)
                                             LoanGntyFeeBilledAmt,
                                         a.LocId,
                                         a.PrtId,
                                         a.LoanSrvsLocId,
                                         p.PrtLglNm
                                             LoanAppPrtNm,
                                         p.PrtLocFIRSNmb
                                             LoanAppFIRSNmb,
                                         p.PhyAddrStr1Txt
                                             LoanAppPrtStr1,
                                         p.PhyAddrCtyNm
                                             LoanAppPrtCtyNm,
                                         p.PhyAddrStCd
                                             LoanAppPrtStCd,
                                         p.PhyAddrPostCd,
                                         l.PrtLocNm,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)
                                             LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanSoldScndMrktInd,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanTotUndisbAmt,
                                         LoanDisbInd,
                                         RecovInd,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         a.LoanAppParntLoanNmb,
                                         a.LoanAppMicroLendrId,
                                         a.LoanIntPaidAmt,
                                         a.LoanIntPaidDt,
                                         LOANSTATCMNTCD,
                                         a.LoanGntyFeePaidAmt,
                                         a.LOANGNTYDFRTODT,
                                         a.LOANGNTYDFRDMNTHSNMB,
                                         a.LOANGNTYDFRDGRSAMT,
                                         LOANDISASTRAPPFEECHARGED,
                                         LOANDISASTRAPPDCSN,
                                         LOANASSOCDISASTRAPPNMB,
                                         LOANASSOCDISASTRLOANNMB,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LOANPYMTTRANSCD,
                                         a.LOANDISASTRAPPNMB,
                                         a.loancofseffdt,
                                         a.SBICLICNSNMB,
                                         a.LOANMAILSTR1NM,
                                         a.LOANMAILSTR2NM,
                                         a.LOANMAILCTYNM,
                                         a.LOANMAILSTCD,
                                         a.LOANMAILZIPCD,
                                         a.LOANMAILZIP4CD,
                                         a.LOANMAILCNTRYCD,
                                         a.LOANMAILSTNM,
                                         a.LOANMAILPOSTCD,
                                         a.LOANAPPNETEARNIND,
                                         a.LOANPYMTINSTLMNTTYPCD,
                                         a.LOANPYMNINSTLMNTFREQCD,
                                         a.LOANTOTDEFNMB,
                                         a.LOANTOTDEFMONMB,
                                         a.LOANGNTYNOTEDT,
                                         a.LOANGNTYMATSTIND,
                                         a.LoanAppIntDtlCd,
                                         a.LOANPYMTREPAYINSTLTYPCD,
                                         a.LOANGNTYPURISSIND,
                                         a.LOANGNTYPURISSDT,
                                         a.LOANSBICLICNSTYP,
                                         a.loansbicdrawtyp,
                                         a.loansbicschdlpooldt,
                                         i.loanintrt,
                                         LOANSTATDESCTXT
                                             AS status,
                                         LOANSBICDRAWTYPDESC,
                                         a.LOANLNDRASSGNLTRDT,
                                         a.LoanGntyCsaAmtRcvd,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.loanintdtltbl i
                                       ON (    a.loanappnmb = i.loanappnmb
                                           AND i.loanintdtlseqnmb = 1)
                                       LEFT OUTER JOIN sbaref.loanstatcdtbl s
                                       ON (a.loanstatcd = s.loanstatcd)
                                       LEFT OUTER JOIN SBAREF.LOANSBICDRAWTYPTBL d
                                       ON (a.LOANSBICDRAWTYP = d.LOANSBICDRAWTYP)
                                       LEFT OUTER JOIN partner.PrtSrchTbl p
                                       ON (a.PrtId = p.PrtId)
                                       LEFT OUTER JOIN partner.LocSrchTbl l
                                       ON (a.LOANSRVSLOCID = l.LOCID)
                                  WHERE     a.LOANAPPPARNTLOANNMB = p_LOANNMB
                                        AND FININSTRMNTTYPIND = 'D';
            END;
        WHEN 29 THEN
            BEGIN
                OPEN p_selcur FOR SELECT LoanAppNmb, Locid
                                  FROM LOAN.LOANGNTYTBL
                                  WHERE LoanNmb = p_Loannmb;
            END;
        WHEN 30 THEN
            BEGIN
                OPEN p_SelCur FOR
                    SELECT LoanGntyFeePct,
                           LoanEfctvGntyFeePct,
                           LoanGntyFeeAmt,
                           LoanGntyFeeDscntRt,
                           ROUND (LoanGntyFeeAmt * (100 - NVL (LoanGntyFeeDscntRt, 0)) / 100, 2)
                               AS LoanGntyFeeBillAmt
                    FROM Loan.LoanGntyCmpnLoanCalcTbl
                    WHERE     LoanMainAppNmb = p_LoanAppNmb
                          AND LoanAppNmb = p_LoanAppNmb
                          AND LOANCMPNAPPNMB = p_LoanAppNmb;
            END;
        /* 38 = Leet for Extract XML */
        WHEN 38 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT a.LoanAppNmb,
                                         a.LoanNmb,
                                         p.AddtnlLocAcqLmtInd,
                                         TO_CHAR (r.ARMMarCeilRt, 'FM990.000')
                                             ARMMarCeilRt,
                                         TO_CHAR (r.ARMMarFloorRt, 'FM990.000')
                                             ARMMarFloorRt,
                                         r.ARMMarTypInd,
                                         p.BulkSaleLawComplyInd,
                                         TO_CHAR (p.CompsLmtAmt, 'FM999999999999990.00')
                                             CompsLmtAmt,
                                         p.CompsLmtInd,
                                         (SELECT COUNT (*)
                                          FROM loan.LoanGntyPreTbl
                                          WHERE LoanAppNmb = a.LoanAppNmb)
                                             AS CountPreTbl,
                                         e.LoanEconDevObjctCd
                                             AS EconDevObjctCd,
                                         r.EscrowAcctRqrdInd,
                                         a.FININSTRMNTTYPIND,
                                         TO_CHAR (p.FixAssetAcqLmtAmt, 'FM999999999999990.00')
                                             FixAssetAcqLmtAmt,
                                         p.FixAssetAcqLmtInd,
                                         p.FrnchsFeeDefInd,
                                         p.FrnchsFeeDefMonNmb,
                                         p.FrnchsLendrSameOppInd,
                                         p.FrnchsRecrdAccsInd,
                                         p.FrnchsTermNotcInd,
                                         i.IMRtTypCd,
                                         r.LateChgAftDayNmb,
                                         r.LateChgInd,
                                         TO_CHAR (r.LateChgFeePct, 'FM990.000')
                                             LateChgFeePct,
                                         i.LoanIntAdjPrdCd
                                             LoanAppAdjPrdCd,
                                         TO_CHAR (i.LoanIntBaseRt, 'FM990.00000')
                                             LoanAppBaseRt,
                                         TO_CHAR (a.LoanAppCDCClsCostAmt, 'FM999999999999990.00')
                                             LoanAppCDCClsCostAmt,
                                         TO_CHAR (a.LOANAPPCDCOTHCLSCOSTAMT, 'FM999999999999990.00')
                                             LOANAPPCDCOTHCLSCOSTAMT,
                                         p.LoanAppCDCJobRat,
                                         TO_CHAR (a.LoanAppCDCNetDbentrAmt, 'FM999999999999990.00')
                                             LoanAppCDCNetDbentrAmt,
                                         TO_CHAR (a.LoanAppCDCPrcsFeeAmt, 'FM999999999999990.00')
                                             LoanAppCDCPrcsFeeAmt,
                                         a.LoanAppCDCSeprtPrcsFeeInd,
                                         TO_CHAR (a.LoanAppContribAmt, 'FM999999999999990.00')
                                             LoanAppContribAmt,
                                         TO_CHAR (a.LoanAppContribPct, 'FM990.00')
                                             LoanAppContribPct,
                                         p.LoanAppCurrEmpQty,
                                         a.LoanAppDisasterCntrlNmb,
                                         TO_CHAR (a.LoanAppFirstDisbDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppFirstDisbDt,
                                         i.LoanIntFixVarInd
                                             LoanAppFixVarInd,
                                         p.LoanAppFrnchsCd,
                                         p.LoanAppFrnchsInd,
                                         p.LoanAppFrnchsNm,
                                         TO_CHAR (a.LoanAppFundDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppFundDt,
                                         TO_CHAR (i.LoanIntRt, 'FM990.00000')
                                             LoanAppInitlIntPct,
                                         TO_CHAR (i.LoanIntSprdOvrPct, 'FM990.000')
                                             LoanAppInitlSprdOvrPrimePct,
                                         a.LoanAppInjctnInd,
                                         a.LoanAppIntDtlCd,
                                         p.LoanAppJobCreatQty,
                                         p.LoanAppJobRtnd,
                                         p.LoanAppJobRqmtMetInd,
                                         TO_CHAR (a.LoanAppMatDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppMatDt,
                                         TO_CHAR (a.LoanAppMatExtDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppMatExtDt,
                                         a.LoanAppMoIntQty,
                                         a.LoanAppNetEarnInd,
                                         TO_CHAR (p.LoanAppNetExprtAmt, 'FM999999999999990.00')
                                             LoanAppNetExprtAmt,
                                         a.LoanAppNewBusCd,
                                         a.LoanAppNm,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         p.LoanAppProjCntyCd,
                                         p.LoanAppProjCtyNm,
                                         p.LoanAppProjStCd,
                                         p.LoanAppProjStr1Nm,
                                         p.LoanAppProjStr2Nm,
                                         p.LoanAppProjZipCd,
                                         p.LoanAppProjZip4Cd,
                                         a.LoanAppPrtLoanNmb,
                                         TO_CHAR (a.LoanAppPymtAmt, 'FM999999999999990.00')
                                             LoanAppPymtAmt,
                                         a.LoanAppRecvDt,
                                         a.LoanAppRqstMatMoQty,
                                         p.LoanAppRuralUrbanInd,
                                         a.LoanAppSBAGntyPct,
                                         a.LoanAppServOfcCd,
                                         a.LoanCollatInd,
                                         TO_CHAR (a.LoanCurrAppvAmt, 'FM999999999999990.00')
                                             LoanCurrAppvAmt,
                                         a.LoanDisbInd,
                                         p.LoanFinanclStmtDueDt,
                                         p.LoanFinanclStmtFreqCd,
                                         p.LoanFinanclStmtFreqDtlDesc,
                                         a.LoanGntyMatStInd,
                                         a.LoanGntyNoteDt,
                                         a.LoanMailCtyNm,
                                         a.LoanMailCntryCd,
                                         a.LoanMailPostCd,
                                         a.LoanMailStCd,
                                         a.LoanMailStNm,
                                         a.LoanMailStr1Nm,
                                         a.LoanMailStr2Nm,
                                         a.LoanMailZipCd,
                                         a.LoanMailZip4Cd,
                                         a.LoanNextInstlmntDueDt,
                                         a.LoanOutBalAmt,
                                         a.LoanOutBalDt,
                                         a.LoanPymnInstlmntFreqCd,
                                         a.LoanPymtRepayInstlTypCd,
                                         a.LoanPymntSchdlFreq,
                                         a.LoanPurchsdInd,
                                         a.LoanServGrpCd,
                                         a.LoanStatCd,
                                         a.LoanTotUndisbAmt,
                                         a.LocId,
                                         a.LoanGntyCsaAmtRcvd,
                                         p.NAICSCd,
                                         p.NAICSYrNmb,
                                         r.NetEarnClauseInd,
                                         TO_CHAR (r.NetEarnPymntOverAmt, 'FM999999999999990.00')
                                             NetEarnPymntOverAmt,
                                         TO_CHAR (r.NetEarnPymntPct, 'FM990.000')
                                             NetEarnPymntPct,
                                         a.PrcsMthdCd,
                                         a.PrgmCd,
                                         a.LOANGNTY1201ELCTRNCSTMTIND,
                                         r.PymntBegnMoNmb,
                                         r.PymntDayNmb,
                                         r.PymntIntOnlyBegnMoNmb,
                                         r.PymntIntOnlyDayNmb,
                                         r.PymntIntOnlyFreqCd,
                                         r.StIntRtReductnInd,
                                         r.StIntRtReductnPrgmNm,
                                         p.LoanBusEstDt,
                                         TO_CHAR (a.LoanAppCDCGrossDbentrAmt, 'FM999999999999990.00')
                                             LoanAppCDCGrossDbentrAmt,
                                         TO_CHAR (a.LoanAppBalToBorrAmt, 'FM999999999999990.00')
                                             LoanAppBalToBorrAmt,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         a.LoanAgntInvlvdInd,
                                         a.LoanAppExtraServFeeAMT,
                                         a.LoanAppExtraServFeeInd,
                                         a.Undrwritngby,
                                         a.LoanGntycorrespLangCd,
                                         a.LoanAppMonthPayroll,
                                         a.LoanGntyPayRollChngInd,
                                         a.LOANAPPSCHEDCIND,
                                         a.LOANAPPSCHEDCYR,
                                         a.LOANAPPGROSSINCOMEAMT,
                                         a.DspstnStatCd,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.LoanIntDtlTbl i                            -- i for interest
                                       ON (    i.LoanAppNmb = a.LoanAppNmb
                                           AND i.LoanIntDtlSeqNmb = 1)
                                       LEFT OUTER JOIN loan.LoanGntyProjTbl p                              -- p for proj
                                       ON (p.LoanAppNmb = a.LoanAppNmb)
                                       LEFT OUTER JOIN loan.LoanGntyPymntTbl r                        -- r for repayment
                                       ON (r.LoanAppNmb = a.LoanAppNmb)
                                       LEFT OUTER JOIN loan.LoanGntyEconDevObjctChldTbl e        -- e for econdevobjctcd
                                       ON (    e.LoanAppNmb = a.LoanAppNmb
                                           AND e.LoanEconDevObjctSeqNmb = 1)
                                  WHERE a.LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 39 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loanappnmb, loanpurofccd, SBAOfc1Nm AS LoanPurOfcNm
                                  FROM loan.loangntytbl g
                                       LEFT OUTER JOIN sbaref.SBAOfcTbl o
                                       ON (g.loanpurofccd = o.SBAOfcCd)
                                  WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 40 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT PRGMCD, LOANAPPMICROLENDRID
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanNmb LIKE (RTRIM (p_LoanNmb) || '%');
            END;
        WHEN 41 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT CDCServFeePct,
                                         LOANPURCHSDIND,
                                         LOANGNTY1201ELCTRNCSTMTIND,
                                         LoanAgntInvlvdInd,
                                         LoanNmb,
                                         loantypind                                                -- NK-- CAFSOPER 2021
                                  FROM Loan.LoanGntyTbl
                                  WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 42 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loanappnmb,
                                         loanstatcd,
                                         prgmcd,
                                         loantypind
                                  FROM loan.loangntytbl
                                  WHERE LoanNmb = p_LoanNmb;
            END;
        WHEN 43 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loannmb
                                  FROM loan.loangntytbl
                                  WHERE loannmb = p_loannmb;
            END;
    END CASE;

    p_RetVal := SQL%ROWCOUNT;
    p_RetVal := NVL (p_RetVal, 0);
END LOANGNTYSELTSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANSBICCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.MOVELOANSBICCSP
(
	p_Identifier         NUMBER   := NULL,
    p_LoanAppNmb         NUMBER   := NULL,
    p_LoanNmb 	         CHAR     := NULL,
    p_LoanAppServOfcCd 	 CHAR     := NULL,
	p_LoanAppCreatUserId VARCHAR2 := NULL,
    p_CohortCd 	         CHAR     := NULL,
    p_RetVal         OUT NUMBER
    
)AS
/*
 PSP :06/10/2014 :Used while creating SBIC loans
 RGG : 06/20/2014 : Changed as per requirements to keep GntyPct = 100 and Outstanding Balance = 0. 
 --BR--10/18/2017--OPSMDEV 1541-- Fixed the Rollback to match the Savepoint name
 
 */
v_LoanNmb char(10);
v_LoanNmbNew char(10);
v_PRGMCD char(1);
v_maxnt number(1); 
v_RetVal NUMBER(10);
v_RetVal1 number(10);
p_RetTxt VARCHAR2(255);
BEGIN
    
  If p_Identifier = 0 then
   -- SAVEPOINT MOVELOANSBICCSP ;
    
   -- create entry in LoanGntyTbl
   INSERT INTO Loan.LoanGntyTbl
        (   LOANAPPNMB,
            PRGMCD,
            PRCSMTHDCD,
            COHORTCD,
            LOANAPPNM,
            LOANAPPRECVDT,
            LOANAPPENTRYDT,
            LOANAPPRQSTAMT,
            LOANAPPSBAGNTYPCT,
            LOANINITLAPPVAMT,
            LOANCURRAPPVAMT,
            LOANINITLAPPVDT,
            LOANLASTAPPVDT,
            LOANAPPRQSTMATMOQTY,
            LOANAPPEPCIND,
            LOANAPPPYMTAMT,
            LOANAPPFULLAMORTPYMTIND,
            LOANAPPMOINTQTY,
            LOANAPPLIFINSURRQMTIND,
            LOANAPPRCNSDRTNIND,
            LOANAPPINJCTNIND,
            LOANCOLLATIND,
            LOANAPPEWCPSNGLTRANSPOSTIND,
            LOANAPPEWCPSNGLTRANSIND,
            LOANAPPELIGEVALIND,
            LOANAPPNEWBUSCD,
            LOANAPPDISASTERCNTRLNMB,
            LOANSTATCD,
            LOANSTATDT,
            LOANAPPORIGNTNOFCCD,
            LOANAPPPRCSOFCCD,
            LOANAPPSERVOFCCD,
            LOANAPPFUNDDT,
            LOANNMB,
            LOANGNTYFEEAMT,
            LOCID,
            PRTID,
            LOANAPPPRTAPPNMB,
            LOANAPPPRTLOANNMB,
            LOANOUTBALAMT,
            LOANOUTBALDT,
            LOANSOLDSCNDMRKTIND,
            LOANAPPCREATUSERID,
            LOANAPPCREATDT,
            LOANTYPIND,
            LOANSRVSLOCID,
            LOANAPPMATDT,
            LOANAPPFIRSTDISBDT,
            LOANNEXTINSTLMNTDUEDT,
            LOANTOTUNDISBAMT,
            LOANDISBIND,
            RECOVIND,
            LOANAPPOUTPRGRMAREAOFOPERIND,
            LOANAPPPARNTLOANNMB,
            LOANAPPMICROLENDRID,
            ACHRTNGNMB,
            ACHACCTNMB,
            ACHACCTTYPCD,
            LOANINTPAIDAMT,
            LOANINTPAIDDT,
            ACHTINNMB,
            LOANAPPCDCGNTYAMT,
            LOANAPPCDCNETDBENTRAMT,
            LOANAPPCDCFUNDFEEAMT,
            LOANAPPCDCSEPRTPRCSFEEIND,
            LOANAPPCDCPRCSFEEAMT,
            LOANAPPCDCCLSCOSTAMT,
            LOANAPPCDCOthCLSCOSTAMT,
            LOANAPPCDCUNDRWTRFEEAMT,
            LOANAPPCONTRIBPCT,
            LOANAPPCONTRIBAMT,
            LOANAPPPRTTAXID,
            LOANSTATCMNTCD,
            LOANGNTYDFRTODT,
            LOANGNTYFEEPAIDAMT,
            LOANGNTYDFRDMNTHSNMB,
            LOANGNTYDFRDGRSAMT,
            LOANDISASTRAPPNMB,
            LOANDISASTRAPPFEECHARGED,
            LOANDISASTRAPPDCSN,
            LOANASSOCDISASTRAPPNMB,
            LOANASSOCDISASTRLOANNMB,
            LOANPYMNTSCHDLFREQ,
            FININSTRMNTTYPIND,
            LOANGNTYFEEREBATEAMT,
            LOANAPPMATEXTDT,
            LOAN1201NOTCIND,
            LOANSERVGRPCD,
            FREEZETYPCD,
            FREEZERSNCD,
            FORGVNESFREEZETYPCD,
            DISBDEADLNDT,
            LOANPYMTTRANSCD,
            LOANCOFSEFFDT,
            LOANPURCHSDIND,
            SBICLICNSNMB,
            LOANMAILSTR1NM,
            LOANMAILSTR2NM,
            LOANMAILCTYNM,
            LOANMAILSTCD,
            LOANMAILZIPCD,
            LOANMAILZIP4CD,
            LOANMAILCNTRYCD,
            LOANMAILSTNM,
            LOANMAILPOSTCD,
            LOANAPPNETEARNIND,
            LOANPYMTINSTLMNTTYPCD,
            LOANPYMNINSTLMNTFREQCD,
            LOANTOTDEFNMB,
            LOANTOTDEFMONMB,
            LOANGNTYMATSTIND,
            LOANPYMTREPAYINSTLTYPCD,
            LOANGNTYEIDLAMT,
			LOANGNTYEIDLCNCLAMT,
			LOANGNTYPHYAMT,
			LOANGNTYPHYCNCLAMT,
            LOANGNTYLCKOUTUPDIND,
            LOANSBICLICNSTYP,
            LASTUPDTUSERID,
            LASTUPDTDT
        )
    select LOANAPPNMB,
            PRGMCD,
            PRCSMTHDCD,
            p_COHORTCD,
            LOANAPPNM,
            LOANAPPRECVDT,
            LOANAPPENTRYDT,
            LOANAPPRQSTAMT,
            100,--LOANAPPSBAGNTYPCT,
            LOANINITLAPPVAMT,
            LOANCURRAPPVAMT,
            sysdate,
            sysdate,
            LOANAPPRQSTMATMOQTY,
            LOANAPPEPCIND,
            LOANAPPPYMTAMT,
            LOANAPPFULLAMORTPYMTIND,
            LOANAPPMOINTQTY,
            LOANAPPLIFINSURRQMTIND,
            LOANAPPRCNSDRTNIND,
            LOANAPPINJCTNIND,
            LOANCOLLATIND,
            LOANAPPEWCPSNGLTRANSPOSTIND,
            LOANAPPEWCPSNGLTRANSIND,
            LOANAPPELIGEVALIND,
            LOANAPPNEWBUSCD,
            LOANAPPDISASTERCNTRLNMB,
            LOANSTATCD,
            sysdate,
            LOANAPPORIGNTNOFCCD,
            LOANAPPPRCSOFCCD,
            p_LOANAPPSERVOFCCD,
            sysdate,
            p_LoanNmb,
            LOANGNTYFEEAMT,
            LOCID,
            PRTID,
            LOANAPPPRTAPPNMB,
            LOANAPPPRTLOANNMB,
            0,--LOANOUTBALAMT,
            LOANOUTBALDT,
            LOANSOLDSCNDMRKTIND,
            LOANAPPCREATUSERID,
            LOANAPPCREATDT,
            LOANTYPIND,
            LOANSRVSLOCID,
            LOANAPPMATDT,
            LOANAPPFIRSTDISBDT,
            LOANNEXTINSTLMNTDUEDT,
            LOANTOTUNDISBAMT,
            LOANDISBIND,
            RECOVIND,
            LOANAPPOUTPRGRMAREAOFOPERIND,
            LOANAPPPARNTLOANNMB,
            LOANAPPMICROLENDRID,
            ACHRTNGNMB,
            ACHACCTNMB,
            ACHACCTTYPCD,
            LOANINTPAIDAMT,
            LOANINTPAIDDT,
            ACHTINNMB,
            LOANAPPCDCGNTYAMT,
            LOANAPPCDCNETDBENTRAMT,
            LOANAPPCDCFUNDFEEAMT,
            LOANAPPCDCSEPRTPRCSFEEIND,
            LOANAPPCDCPRCSFEEAMT,
            LOANAPPCDCCLSCOSTAMT,
            LOANAPPCDCOthCLSCOSTAMT,
            LOANAPPCDCUNDRWTRFEEAMT,
            LOANAPPCONTRIBPCT,
            LOANAPPCONTRIBAMT,
            LOANAPPPRTTAXID,
            LOANSTATCMNTCD,
            LOANGNTYDFRTODT,
            LOANGNTYFEEPAIDAMT,
            LOANGNTYDFRDMNTHSNMB,
            LOANGNTYDFRDGRSAMT,
            LOANDISASTRAPPNMB,
            LOANDISASTRAPPFEECHARGED,
            LOANDISASTRAPPDCSN,
            LOANASSOCDISASTRAPPNMB,
            LOANASSOCDISASTRLOANNMB,
            LOANPYMNTSCHDLFREQ,
            FININSTRMNTTYPIND,
            LOANGNTYFEEREBATEAMT,
            LOANAPPMATEXTDT,
            LOAN1201NOTCIND,
            LOANSERVGRPCD,
            FREEZETYPCD,
            FREEZERSNCD,
            FORGVNESFREEZETYPCD,
            DISBDEADLNDT,
            LOANPYMTTRANSCD,
            LOANCOFSEFFDT,
            LOANPURCHSDIND,
            SBICLICNSNMB,
            LOANMAILSTR1NM,
            LOANMAILSTR2NM,
            LOANMAILCTYNM,
            LOANMAILSTCD,
            LOANMAILZIPCD,
            LOANMAILZIP4CD,
            LOANMAILCNTRYCD,
            LOANMAILSTNM,
            LOANMAILPOSTCD,
            LOANAPPNETEARNIND,
            LOANPYMTINSTLMNTTYPCD,
            LOANPYMNINSTLMNTFREQCD,
            LOANTOTDEFNMB,
            LOANTOTDEFMONMB,
            LOANGNTYMATSTIND,
            LOANPYMTREPAYINSTLTYPCD,
            0,
			0,
			0,
			0,
            null,
            LOANSBICLICNSTYP,
            p_LoanAppCreatUserId,
            sysdate
            from loan.loangntypretbl
            where loanappnmb = p_loanappnmb;
    -- create entry in LoanGntyProcdTbl 
    INSERT INTO loan.LOANGNTYSPCPURPSTBL
           (LOANAPPNMB,
            LOANSPCPURPSSEQNMB,
            SPCPURPSLOANCD,
            CREATUSERID,
            CREATDT,
            LASTUPDTUSERID,
            LASTUPDTDT
           )
    VALUES(
            p_LoanAppNmb,
            1,
            'NOSP',
            p_LoanAppCreatUserId,
            SYSDATE,
            p_LoanAppCreatUserId,
            SYSDATE
           );
           
      INSERT INTO LoanGntyProcdTbl
    (
        LOANPROCDSEQNMB,   
        LOANAPPNMB,  
        PROCDTYPCD,      
        LOANPROCDTYPCD,      
        LOANPROCDOTHTYPTXT,
        LOANPROCDAMT,
        LOANPROCDCREATUSERID,     
        LOANPROCDCREATDT,      
        LASTUPDTUSERID,    
        LASTUPDTDT 
     )
    SELECT
	    LOANPROCDSEQNMB,   
        p_LoanAppNmb,      
        PROCDTYPCD,      
        LOANPROCDTYPCD,      
        LOANPROCDOTHTYPTXT,
        LOANPROCDAMT,
        LOANPROCDCREATUSERID,     
        LOANPROCDCREATDT,      
        p_LoanAppCreatUserId,
        sysdate         
    FROM LoanGntyPreProcdTbl
	WHERE LoanAppNmb = p_LoanAppNmb;
           
           
		p_RetVal  :=  SQL%ROWCOUNT;
    END IF;
 
 
LOAN.LOANGNTYPREDELTSP(0,v_RetVal,p_LoanAppNmb,null);
 LOAN.LOANGNTYPREPROCDDELTSP(1,v_RetVal1,p_LoanAppNmb,null);
LOAN.LOANCHNGLOGINSCSP(p_LoanAppNmb,'O',p_LoanAppCreatUserId,null,null);

/*exception when others then
begin
    raise;
    rollback to MOVELOANSBICCSP;
end;*/
end  MOVELOANSBICCSP;
/

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

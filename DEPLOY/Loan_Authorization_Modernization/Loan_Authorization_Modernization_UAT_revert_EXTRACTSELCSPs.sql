define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_revert_EXTRACTSELCSPs
define package_buildtime=20210514115232
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_revert_EXTRACTSELCSPs created on Fri 05/14/2021 11:52:33.01 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_revert_EXTRACTSELCSPs created on Fri 05/14/2021 11:52:33.01 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_revert_EXTRACTSELCSPs: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.EXTRACTSELCSP (p_LoanAppNmb   IN     NUMBER := NULL,
                                                p_LoanNmb      IN     VARCHAR2 := NULL,
                                                p_RetVal          OUT NUMBER,
                                                p_SelCur1         OUT SYS_REFCURSOR,
                                                p_SelCur2         OUT SYS_REFCURSOR,
                                                p_SelCur3         OUT SYS_REFCURSOR,
                                                p_SelCur4         OUT SYS_REFCURSOR,
                                                p_SelCur5         OUT SYS_REFCURSOR,
                                                p_SelCur6         OUT SYS_REFCURSOR,
                                                p_SelCur7         OUT SYS_REFCURSOR,
                                                p_SelCur8         OUT SYS_REFCURSOR,
                                                p_SelCur9         OUT SYS_REFCURSOR,
                                                p_SelCur10        OUT SYS_REFCURSOR,
                                                p_SelCur11        OUT SYS_REFCURSOR,
                                                p_SelCur12        OUT SYS_REFCURSOR,
                                                p_SelCur13        OUT SYS_REFCURSOR,
                                                p_SelCur14        OUT SYS_REFCURSOR,
                                                p_SelCur15        OUT SYS_REFCURSOR,
                                                p_SelCur16        OUT SYS_REFCURSOR,
                                                p_SelCur17        OUT SYS_REFCURSOR,
                                                p_SelCur18        OUT SYS_REFCURSOR,
                                                p_SelCur19        OUT SYS_REFCURSOR,
                                                p_SelCur20        OUT SYS_REFCURSOR,
                                                p_SelCur21        OUT SYS_REFCURSOR,
                                                p_SelCur22        OUT SYS_REFCURSOR,
                                                p_SelCur23        OUT SYS_REFCURSOR,
                                                p_SelCur24        OUT SYS_REFCURSOR,
                                                p_SelCur25        OUT SYS_REFCURSOR,
                                                p_SelCur26        OUT SYS_REFCURSOR,
                                                p_SelCur27        OUT SYS_REFCURSOR) AS
    /*SB -- This procedure created for Extract XML */
    /*SB -- Added p_selcur18 on 02/25/2015 */
    /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met, Replaced the initialization block to check for correct loannmb and loanappnmb*/
    /*NK -- 01/12/2017 Added  p_SELCUR20 and p_SELCUR21*/
    /*JP -- 01/30/2017 CAFSOPER-1341 Changed p_SelCur19�s procedure from LOANINFOEXTRACTCSP to LOANINFOEXTRACTSELCSP. (Added "SEL".)
    RY -- 03/17/2017 --OPSMDEV --1401-- Added P_SELCUR22
    BR -- 05/26/2017 --OPSMDEV-1434 -- Added P_SelCur23 Loan.LoanExprtCntrySelTSP
    SS--07/19/2018--OPSMDEV--1861-- Added  P_SELCUR24 and  P_SELCUR25 for Agents
    JP - CAfsoper-2988 08/08/2019 - Modified call to LoanGntyGuarSelTSP
    RY:: 12/16/2019- OPSMDEV-2347 :: Added LOAN.DEFRMNTHISTRYSELTSP to the list
    -- JP 8/21/2020 CARESACT 621 added call to LOAN.LoanHldStatSelTSP
    */
    --
    v_LoanNmb      CHAR (10);
    v_LoanAppNmb   NUMBER;
    v_RetVal       NUMBER;
BEGIN
    -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
    BEGIN
        IF (   p_LoanAppNmb IS NOT NULL
            OR p_LoanNmb IS NOT NULL) THEN
            SELECT LoanAppNmb, LoanNmb
            INTO v_LoanAppNmb, v_LoanNmb
            FROM loan.LoanGntyTbl
            WHERE     LoanAppNmb = NVL (p_LoanAppNmb, LoanAppNmb)
                  AND LoanNmb = NVL (p_LoanNmb, LoanNmb);
        ELSE
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
    END;



    IF v_LoanAppNmb IS NULL THEN
        BEGIN
            p_RetVal := 0;

            OPEN p_SelCur1 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur2 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur3 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur4 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur5 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur6 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur7 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur8 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur9 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur10 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur11 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur12 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur13 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur14 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur15 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur16 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur17 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur18 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur19 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur20 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur21 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur22 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur23 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur24 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur25 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;
            
        END;
    ELSE
        BEGIN
            LoanGntySelTSP (
                38,
                v_LoanAppNmb,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                P_RetVal,
                p_SelCur1);
            LoanAssocSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur2);
            LoanGntyBorrSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur3);
            LoanGntyBSSelTSP (38, v_RetVal, v_LoanAppNmb, NULL, p_SelCur4);
            LoanGntyCollatSelTSP (38, v_LoanAppNmb, NULL, p_SelCur5);
            LoanGntyCollatLienSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur6);
            LoanDisbSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur7);
            LoanDisbPymtSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur8);
            /*LoanGntyGuarSelTSP (38,
                                v_LoanAppNmb,
                                NULL,
                                NULL,
                                v_RetVal,
                                p_SelCur9);*/
            -- JP modified for CAFSOPER - 2988
            LoanGntyGuarSelTSP (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SelCur9);

            LoanGntyInjctnSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur10);
            LoanIntDtlSelTSP (38, v_LoanAppNmb, p_SelCur11);
            LoanGntyPartLendrSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur12);

            LoanGntyPrinSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur13);
            LoanGntyRaceSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
            LoanGntyProcdSelTSP (38, NULL, v_LoanAppNmb, NULL, v_RetVal, p_SelCur15);
            LoanGntyPrtCmntSelTSP (38, v_LoanAppNmb, v_RetVal, p_SelCur16);
            LoanGntySpcPurpsSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur17);
            LoanGntyStbyAgrmtDtlSelTSP (38, v_LoanAppNmb, NULL, p_SELCUR20, v_RETVAL);
            LoanLmtGntyCollatSelTSP (38, v_LOANAPPNMB, NULL, NULL, NULL, NULL, P_SELCUR21, v_RETVAL);

            LoanGntyEconDevObjctChldSelTsp (38, v_LOANAPPNMB, NULL, P_SELCUR22, v_RETVAL);
            LoanExprtCntrySelTsp (
                p_IDENTIFIER => 38,
                p_LOANAPPNMB => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SELCUR => P_SELCUR23);
            --(38, v_LOANAPPNMB, P_SELCUR23, v_RETVAL);
            LoanAgntSelTsp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR24);
            --(38, v_LoanAppNmb, NULL, p_SELCUR24, v_RetVal);


            LoanAgntfeeDtlSelTSp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR25);
            --(38, v_LoanAppNmb, NULL, p_SELCUR25, v_RetVal);

            LOAN.DEFRMNTHISTRYSELTSP (38, v_loanappnmb, NULL, v_RetVal, p_SELCUR26);
            -- JP 8/21/2020 CARESACT 621
            LOAN.LoanHldStatSelTSP (p_Identifier => 38, p_LoanAppNmb => v_LoanAppNmb, p_SelCur => p_SELCUR27);

            OPEN p_SELCUR18 FOR SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.BusRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'B'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                UNION
                                SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.PerRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'P'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                ORDER BY 1, 2;                                             /*BorrTaxId, BorrBusPerInd */

            LOAN.LOANINFOEXTRACTSELCSP (v_LoanAppNmb, v_LoanNmb, p_SelCur19);
        END;
    END IF;
END EXTRACTSELCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.EXTRACTSELCSP (p_LoanAppNmb       IN     NUMBER := NULL,
                                                   p_PrtId                   NUMBER := 0,
                                                   p_RetVal              OUT NUMBER,
                                                   p_SelCur1             OUT SYS_REFCURSOR,
                                                   p_SelCur2             OUT SYS_REFCURSOR,
                                                   p_SelCur3             OUT SYS_REFCURSOR,
                                                   p_SelCur4             OUT SYS_REFCURSOR,
                                                   p_SelCur5             OUT SYS_REFCURSOR,
                                                   p_SelCur6             OUT SYS_REFCURSOR,
                                                   p_SelCur7             OUT SYS_REFCURSOR,
                                                   p_SelCur8             OUT SYS_REFCURSOR,
                                                   p_SelCur9             OUT SYS_REFCURSOR,
                                                   p_SelCur10            OUT SYS_REFCURSOR,
                                                   p_SelCur11            OUT SYS_REFCURSOR,
                                                   p_SelCur12            OUT SYS_REFCURSOR,
                                                   p_SelCur13            OUT SYS_REFCURSOR,
                                                   p_SelCur14            OUT SYS_REFCURSOR,
                                                   p_SelCur15            OUT SYS_REFCURSOR,
                                                   p_SelCur16            OUT SYS_REFCURSOR,
                                                   p_SelCur17            OUT SYS_REFCURSOR,
                                                   p_SelCur18            OUT SYS_REFCURSOR,
                                                   p_SelCur19            OUT SYS_REFCURSOR,
                                                   p_SelCur20            OUT SYS_REFCURSOR,
                                                   p_SelCur21            OUT SYS_REFCURSOR,
                                                   p_SelCur22            OUT SYS_REFCURSOR,
                                                   p_SelCur23            OUT SYS_REFCURSOR,
                                                   p_SelCur24            OUT SYS_REFCURSOR,
                                                   p_SelCur25            OUT SYS_REFCURSOR,
                                                   p_SelCur26            OUT SYS_REFCURSOR,
                                                   p_SelCur27            OUT SYS_REFCURSOR,
                                                   p_SelCur28            OUT SYS_REFCURSOR,
                                                   p_SelCur29            OUT SYS_REFCURSOR,
                                                   p_SelCur30            OUT SYS_REFCURSOR,
                                                   p_SelCur31            OUT SYS_REFCURSOR,
                                                   p_SelCur32            OUT SYS_REFCURSOR,
                                                   p_SelCur33            OUT SYS_REFCURSOR,
                                                   p_SelCur34            OUT SYS_REFCURSOR,
                                                   p_SelCur35            OUT SYS_REFCURSOR,
                                                   p_SelCur36            OUT SYS_REFCURSOR,
                                                   p_SelCur37            OUT SYS_REFCURSOR,
                                                   p_SelCur38            OUT SYS_REFCURSOR,
                                                   p_SelCur39            OUT SYS_REFCURSOR,
                                                   p_SelCur40            OUT SYS_REFCURSOR,
                                                   p_SelCur41            OUT SYS_REFCURSOR,
                                                   p_SelCur42            OUT SYS_REFCURSOR,
                                                   p_SelCur43            OUT SYS_REFCURSOR)
AS
   /*SB -- 07/07/2014 This procedure created for Extract XML */
   /*SB -- 02/24/2015 Added p_selcur35  */
   /*SB -- 03/27/2015 Added p_selcur36  */
   /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met*/
   /*SB -- 04/07/2016 Added p_selcur37  */
   /*BR -- 10/21/2016 Added p_selcur38, p_selcur39 */
   /*Nk -- 03/10/2017 OPSMDEV 1385 Added p_selcur40*/
   --SS--07/12/2018 OPSMDEV 1860 Added p_selcur 42 and 43
   -- JP 08/06/2019 OPSMDEV 2255 modified call to LoanAppSelTSP 
   v_RetVal   NUMBER;
   v_PrtId    NUMBER (7);
BEGIN
   -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
   SELECT COUNT (*)
     INTO p_RetVal
     FROM loanapp.LoanAppTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF NVL (p_PrtId, 0) = 0
   THEN
      BEGIN
         SELECT PrtId
           INTO v_PrtId
           FROM loanapp.LoanAppPrtTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PrtId := NULL;
      END;
   END IF;

   IF p_RetVal = 0
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur2 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur3 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur4 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur5 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur6 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur7 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur8 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur9 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur10 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur11 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur12 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur13 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur14 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur15 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur16 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur17 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur18 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur19 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur20 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur21 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur22 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur23 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur24 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur25 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur26 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur27 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur28 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur29 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur30 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur31 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur32 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur33 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur34 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur35 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur36 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur37 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur38 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur39 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur40 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur41 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur42 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur43 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
             
      END;
   ELSE
      BEGIN
         --LoanAppSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur1);
         -- JP Modifed LoanAppSelTSP for 2255 
        LoanAppSelTSP (p_identifier => 38, 
                       p_loanappnmb => p_LoanAppNmb, 
                       p_retval => v_RetVal, 
                       p_selcur => p_SelCur1);
         LoanAppARCRsnSelTSP (38, v_RetVal, p_LoanAppNmb, NULL, p_SelCur2);
         LoanAppAsstAreaSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur3);
         LoanAppCAInfoSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur4);
         LoanAppChngBusOwnrshpSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur5);
         LoanAppCnselngSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur6);
         LoanAppCnselngSrcSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur7);
         LoanAppEligSelTSP (38, p_LoanAppNmb, NULL, p_SelCur8);
         LoanAppPrtSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur9);
         LoanBorrSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur10);
         LoanBSSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur11);
         LoanCollatSelTSP (38, p_LoanAppNmb, NULL, p_SelCur12);
         LoanCollatLienSelTSP (38, p_LoanAppNmb, NULL, NULL, p_SelCur13);
         LoanCrdtUnavRsnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
         LoanFedEmpSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur15);
         LoanGuarSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur16);
         LoanIndbtnesSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur17);
         LoanInjctnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur18);
         LoanIntDtlSelTSP (38, p_LoanAppNmb, p_SelCur19);
         LoanISSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur20);
         LoanPartLendrSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur21);
         LoanPerFinanSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur22);
         LoanPrevFinanSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur23);
         LoanPrinSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur24);
         LoanPrinRaceSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur25);
         LoanProcdSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur26);
         LoanPrtCmntSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur27);
         LoanSpcPurpsSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur28);
         LqdCrSelCSP (38, p_LoanAppNmb, v_RetVal, p_SelCur29, p_SelCur30, p_SelCur31, p_SelCur32, p_SelCur33);
         partner.PrtAgrmtSelTSP (38, v_RetVal, v_prtid, NULL, NULL, NULL, NULL, p_SelCur34);
         loanapp.LoanBorrRaceSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur35);
         LOANAPP.LoanAppBusApprSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur36);
         LOANAPP.LoanAppCAUndrServMrktSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur37);
         LOANAPP.LoanLmtGntyCollatSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, p_SelCur38, v_RetVal);
         LOANAPP.LoanStbyAgrmtDtlSelTSP (38, p_LoanAppNmb, NULL, p_SelCur39, v_RetVal);
         LOANAPP.LoanEconDevObjctChldSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR40, v_RetVal);
         LOANAPP.LoanExprtCntrySelTsp (38, p_LoanAppNmb, p_SELCUR41, v_RetVal);
         LoanApp.LoanAgntSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR42, v_RetVal);
         LoanApp.LoanAgntfeeDtlSelTSp (38, p_LoanAppNmb, NULL, p_SELCUR43, v_RetVal);
      END;
   END IF;
END ExtractSelCSP;
/

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

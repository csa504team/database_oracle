define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0602_fix_errors
define package_buildtime=20210602113357
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0602_fix_errors created on Wed 06/02/2021 11:33:58.59 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0602_fix_errors created on Wed 06/02/2021 11:33:58.59 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0602_fix_errors: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDINSTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
     p_PROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDSELTSP;

    /* Select from LOANSUBPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	 
                LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM LOAN.LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANSUBPROCDID, LOANAPPNMB,/* LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD AND  PROCDTYPCD=p_PROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDSELTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDSELTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select nvl(LOANPROCDSEQNMB,0) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
	cursor procdtbl_max is select max(nvl(LOANPROCDSEQNMB,0)) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
	v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    v_LASTUPDTUSERID    varchar2(50):=NULL;
    max_LOANPROCDSEQNMB number :=0;
    
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> '04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc1]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc2]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB=0  then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if  v_LOANPROCDSEQNMB=0 then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDINSTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDINSTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select LOANPROCDSEQNMB from LOANAPP.LOANPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
    v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    
BEGIN
    SAVEPOINT LOANSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> '04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc1]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc2]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDINSTSP;
            RAISE;
        END;
END LOANSUBPROCDINSTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_PROCDTYPCD   CHAR:=NULL,
    p_RetVal       OUT NUMBER,
    p_SelCur       OUT SYS_REFCURSOR)
AS
BEGIN
    SAVEPOINT LOANSUBPROCDSELTSP;

    /* Select from LOANSUBPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	 
                LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM LOANAPP.LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD AND  PROCDTYPCD=p_PROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDSELTSP;
            RAISE;
        END;
END LOANSUBPROCDSELTSP;
/

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Loan_Authorization_Modernization
define package_name=PROD_0203
define package_buildtime=20210203211803
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_PROD_0203 created on Wed 02/03/2021 21:18:04.11 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_PROD_0203 created on Wed 02/03/2021 21:18:04.11 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_PROD_0203: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_columns_LOANGUARTBL.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_columns_LOANGUARHISTRYTBL.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_columns_LOANGNTYGUARTBL.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARUPDTSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARSELTSP.sql 
Jasleen Gorowada committed 456f138 on Fri Jan 22 14:34:18 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARINSTSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARUPDCSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARUPDTSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARINSCSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARSELTSP.sql 
Jasleen Gorowada committed 456f138 on Fri Jan 22 14:34:18 2021 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_columns_LOANGUARTBL.sql"
alter table LoanApp.LoanGuarTbl
add (
LIABINSURRQRDIND CHAR(1),
PRODLIABINSURRQRDIND CHAR(1),
LIQLIABINSURRQRDIND CHAR(1),
MALPRCTSINSURRQRDIND CHAR(1),
OTHINSURRQRDIND CHAR(1),
WORKRSCOMPINSRQRDIND CHAR(1),
OTHINSURDESCTXT		VARCHAR2(255));
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_columns_LOANGUARHISTRYTBL.sql"
alter table Loan.LOANGUARHISTRYTBL
add (
LIABINSURRQRDIND CHAR(1),
PRODLIABINSURRQRDIND CHAR(1),
LIQLIABINSURRQRDIND CHAR(1),
MALPRCTSINSURRQRDIND CHAR(1),
OTHINSURRQRDIND CHAR(1),
WORKRSCOMPINSRQRDIND CHAR(1),
OTHINSURDESCTXT		VARCHAR2(255));
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_columns_LOANGNTYGUARTBL.sql"
alter table Loan.LOANGNTYGUARTBL
add (
LIABINSURRQRDIND CHAR(1),
PRODLIABINSURRQRDIND CHAR(1),
LIQLIABINSURRQRDIND CHAR(1),
MALPRCTSINSURRQRDIND CHAR(1),
OTHINSURRQRDIND CHAR(1),
WORKRSCOMPINSRQRDIND CHAR(1),
OTHINSURDESCTXT		VARCHAR2(255));
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARUPDTSP(
  p_Identifier      	NUMBER:=0,
  p_RetVal       	OUT NUMBER,
  p_LoanAppNmb      	NUMBER:=0,
  p_GuarSeqNmb      	NUMBER:=0,
  p_TaxId 	          CHAR:=NULL,
  p_GuarBusPerInd 	  CHAR:=NULL,
  p_LoanGuarOperCoInd CHAR:=NULL,
  p_GuarCreatUserId 	CHAR:=NULL,
  p_LastUpdtUserId    CHAR := null,
  p_LACGntyCondTypCd  NUMBER := NULL, 
  p_GntySubCondTypCd  NUMBER := NULL,
  p_GntyLmtAmt        NUMBER := NULL,
  p_GntyLmtPct        NUMBER := NULL,
  p_LoanCollatSeqNmb  NUMBER := NULL,
  p_GntyLmtYrNmb      NUMBER := NULL,
  p_LIABINSURRQRDIND               CHAR:=NULL,
  p_PRODLIABINSURRQRDIND           CHAR:=NULL,
  p_LIQLIABINSURRQRDIND            CHAR:=NULL,
  p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
  p_OTHINSURRQRDIND                CHAR:=NULL,
  p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
  p_OTHINSURDESCTXT                VARCHAR2:=NULL
)
AS
/*  Date         : October 1, 2000
    Programmer   : Sheri Stine
    Remarks      :
    Parameters   :  No.   Name        Type     Description
                    1     @p_Identifier  int     Identifies which batch
                                               to execute.
                    2     @p_RetVal      int     No. of rows returned/
                                               affected
                    3    table columns to be inserted
***************************************************************************************
Priya Varigala 07/17/01   LoanGuarOperCoInd new column
SP:09/26/2012: Modified as column LoanAffilInd is added to the table for determining companion relationship 
SP:02/07/2014: Modified to add  LACGntyCondTypCd to support LADS
NK--08/05/2016--OPSMDEV1018-- Removed LoanCntlIntInd 
BR -- 09/21/2016 -- OPSMDEV??? -- added GntySubCondTypCd,GntyLmtAmt,GntyLmtPct,LoanCollatSeqNmb,GntyLmtYrNmb to 0, 11, 12, 38                    number(4);
BJR -- 01/25/2018 -- CAFSOPER-1381 -- Removed created by and created dates from the update statements so that the original creator/create date stays intact for the record.
*/
	BEGIN
					SAVEPOINT LoanGuarUpd;
/* Insert into LoanGuar Table */
		IF  p_Identifier = 0 THEN
		BEGIN
		       UPDATE LoanGuarTbl
		        	SET LoanAppNmb        = p_LoanAppNmb,
			            GuarSeqNmb        = p_GuarSeqNmb,
                  TaxId             = p_TaxId,
			          	GuarBusPerInd     = p_GuarBusPerInd,
			           	LoanGuarOperCoInd = p_LoanGuarOperCoInd,
                  LastUpdtUserId    = nvl(p_LastUpdtUserId,user),
                  LastUpdtDt        = sysdate,             
                  LACGntyCondTypCd  = p_LACGntyCondTypCd, 
                  GntySubCondTypCd  = p_GntySubCondTypCd,
                  GntyLmtAmt        = p_GntyLmtAmt,
                  GntyLmtPct        = p_GntyLmtPct,
                  LoanCollatSeqNmb  = p_LoanCollatSeqNmb,
                  GntyLmtYrNmb      = p_GntyLmtYrNmb,
                  LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                  PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                  LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                  MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                  OTHINSURRQRDIND = p_OTHINSURRQRDIND,
                  WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                  OTHINSURDESCTXT = p_OTHINSURDESCTXT
			    	WHERE LoanAppNmb = p_LoanAppNmb
			      	AND GuarSeqNmb = p_GuarSeqNmb;
p_RetVal := SQL%ROWCOUNT;
     END;  
     END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
          ROLLBACK TO LOANGUARUPD;
          RAISE;
        END;
END LOANGUARUPDTSP;
/


GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARSELTSP(
p_Identifier     IN NUMBER  DEFAULT 0,
p_LoanAppNmb     IN NUMBER  DEFAULT 0,
p_GuarSeqNmb     IN NUMBER  DEFAULT 0,
p_TaxId         IN CHAR  DEFAULT NULL,
p_GuarBusPerInd IN CHAR  DEFAULT NULL,
p_RetVal         OUT NUMBER,
p_SelCur         OUT sys_refcursor)
AS
/* Date         : Feb 11 2004
   Programmer   : Prasad Mente
   Remarks      :
   Parameters   :  No.   Name        Type     Description
                    1    @p_Identifier  int     Identifies which batch
                                              to execute.
                    2    @RetVal      int     No. of rows returned/
                                              affected
                    3    Primary key columns
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
APK            07/20/2011   Modified ID=12 to add an additional condition using BusPerInd.
SP             9/26/2012    Modified as column LoanAffilInd is
                            added FOR determining companion relationship
SP             02/04/2014   Modified to add column LACGntyCondTypCd to support LADS
*/
/* Variable Declaration */
/* End of Variable Declaration */
/*[SPCONV-ERR(26)]:('@TRANCOUNT') Global Variable treated as variable
SB -- Added Identifier 38 for Extract XML */
/*NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
NK -- 04/06/2016-- included p.PerBrthCntCd in Identifier 38 
NK --04/19/2016-- included m.GuarSeqNmb in Identifier 38*/
--NK--09/09/2016--OPSMDEV1018--Removed LoanCntlIntInd  ID 0,11,38
--BR--09/30/2016--OPSMDEV    -- Added new columns GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb  
--NK--10/11/2016-- OPSMDEV1141-- added GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb in ID 11 for Copy function.
--NK--12/29/2016-- Added GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb in ID 38.
BEGIN--SS--11/26/2018--OPSMDEV 2043
    
            SAVEPOINT LoanGuarSel;
    
        IF  p_Identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LACGntyCondTypCd,
                   GntySubCondTypCd, 
                   GntyLmtAmt, 
                   GntyLmtPct, 
                   LoanCollatSeqNmb, 
                   GntyLmtYrNmb,
                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT
              FROM LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb
               AND GuarSeqNmb = p_GuarSeqNmb;    
        END;

        ELSIF  p_Identifier = 2 THEN
            BEGIN
                OPEN p_SelCur FOR
                SELECT 1
                  FROM LoanGuarTbl
                 WHERE LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb; 
            END;

            ELSIF  p_Identifier = 11 THEN
                BEGIN
                    OPEN p_SelCur FOR
                    SELECT LoanAppNmb,
                           GuarSeqNmb,
                           TaxId,
                           GuarBusPerInd,
                           LoanGuarOperCoInd,
                           LACGntyCondTypCd,
                           GntySubCondTypCd, 
                           GntyLmtAmt, 
                           GntyLmtPct, 
                           LoanCollatSeqNmb, 
                           GntyLmtYrNmb  
                      FROM LoanGuarTbl
                     WHERE LoanAppNmb = p_LoanAppNmb;
                END;

                    
/*Check The Existance Of Row based on LoanAppNmb and TaxId*/
                ELSIF  p_Identifier = 12 THEN
                    BEGIN
                        OPEN p_SelCur FOR
                        SELECT GuarSeqNmb
                          FROM LoanGuarTbl
                         WHERE LoanAppNmb   = p_LoanAppNmb
                           AND TaxId         = p_TaxId
                           AND GuarBusPerInd = p_GuarBusPerInd;
                     END;
                  
                 ELSIF  p_Identifier = 14 THEN
                    BEGIN
                            OPEN p_SelCur FOR
                            SELECT GuarSeqNmb,
                                   GuarBusPerInd,
                                   a.TaxId,
                                   BusNm,
                                   NULL PerFirstNm,
                                   NULL PerLastNm,
                                   NULL PerInitialNm,
                                   NULL PerSfxNm
                              FROM LoanBusTbl b, LoanGuarTbl a
                             WHERE a.LoanAppNmb = p_LoanAppNmb
                               and b.LoanAppNmb = p_LoanAppNmb
                               and a.TaxId = b.TaxId
                               and GuarBusPerInd = 'B'
                              UNION
                            SELECT GuarSeqNmb,
                                   GuarBusPerInd,
                                   a.TaxId,
                                   NULL BusNm,
                                   ocadataout(PerFirstNm) PerFirstNm,
                                  ocadataout( PerLastNm)  PerLastNm,
                                   PerInitialNm,
                                   PerSfxNm
                              FROM LoanPerTbl b, LoanGuarTbl a
                             WHERE a.LoanAppNmb = p_LoanAppNmb
                               and b.LoanAppNmb = p_LoanAppNmb
                               and a.TaxId = b.TaxId
                               and GuarBusPerInd = 'P'
                             ORDER BY GuarSeqNmb ;
                            
                        END;
    ELSIF  p_Identifier = 38 THEN
            BEGIN
                OPEN p_SelCur FOR
        /* 38 = Leet for Extract XML, initially indented for union */
    select                                                               m.TaxId,
                                                                         m.GuarBusPerInd,
                                                                         m.GuarSeqNmb,
                                                                         m.LACGntyCondTypCd,
                                                                         m.LoanGuarOperCoInd,
                                                                         null  LoanAffilInd,--NK--10/12/2016--OPSMDEV 1018
                                                                         m.GntyLmtAmt,
                                                                         m.GntySubCondTypCd,
                                                                         m.LoanCollatSeqNmb,
                                                                         m.GntyLmtPct,
                                                                         m.GntyLmtYrNmb,
                                                                         
                -- Both Bus and Per:
                b.BusBnkrptInd                                             BnkrptcyInd,
                m.GuarBusPerInd                                            BusPerInd,
        to_char(b.BusExtrnlCrdtScorDt,            'Mon dd yyyy hh:miAM')   ExtrnlCrdtScorDt,
                b.BusExtrnlCrdtScorInd                                     ExtrnlCrdtScorInd,
                b.BusExtrnlCrdtScorNmb                                     ExtrnlCrdtScorNmb,
                                                                         b.IMCrdtScorSourcCd,
                b.BusLwsuitInd                                             LawsuitInd,
                b.BusMailAddrCtyNm                                         MailAddrCtyNm,
                b.BusMailAddrStCd                                          MailAddrStCd,
                b.BusMailAddrStNm                                          MailAddrStNm,
                b.BusMailAddrCntCd                                         MailAddrCntCd,
                b.BusMailAddrPostCd                                        MailAddrPostCd,
                b.BusMailAddrStr1Nm                                        MailAddrStr1Nm,
                b.BusMailAddrStr2Nm                                        MailAddrStr2Nm,
                b.BusMailAddrZip4Cd                                        MailAddrZip4Cd,
                b.BusMailAddrZipCd                                         MailAddrZipCd,
                b.BusPhyAddrCtyNm                                          PhyAddrCtyNm,
                b.BusPhyAddrStCd                                           PhyAddrStCd,
                b.BusPhyAddrStNm                                           PhyAddrStNm,
                b.BusPhyAddrCntCd                                          PhyAddrCntCd,
                b.BusPhyAddrPostCd                                         PhyAddrPostCd,
                b.BusPhyAddrStr1Nm                                         PhyAddrStr1Nm,
                b.BusPhyAddrStr2Nm                                         PhyAddrStr2Nm,
                b.BusPhyAddrZip4Cd                                         PhyAddrZip4Cd,
                b.BusPhyAddrZipCd                                          PhyAddrZipCd,
                b.BusPrimPhnNmb                                            PrimPhnNmb,
                -- Unique to Bus:
        to_char(b.BusChkngBalAmt,                 'FM999999999999990.00')    BusChkngBalAmt,
                                                                         b.BusCurBnkNm,
        to_char(b.BusCurOwnrshpDt,                'Mon dd yyyy hh:miAM')   BusCurOwnrshpDt,
                                                                         b.BusDUNSNmb,
                                                                         b.BusEINCertInd,
                                                                         b.BusExprtInd,
                                                                         b.BusNm,
                                                                         b.BusPriorSBALoanInd,
                                                                         b.BusTrdNm,
                                                                         b.BusTypCd,
                null                                                       LqdCrCBDInfo,
                null                                                       LqdCrBBADunBradCrRiskScr,
                -- Unique to Per:
                null                                                       PerAffilEmpFedInd,
                null                                                       PerAlienRgstrtnNmb,
                null                                                       PerBrthCntCd,
                null                                                       PerBrthCntNm,
                null                                                       PerBrthCtyNm,
                null                                                       PerBrthDt,
                null                                                       PerBrthStCd,
                null                                                       PerCnvctInd,
                null                                                       PerCrmnlOffnsInd,
                null                                                       PerFirstNm,
                null                                                       PerFngrprntWaivDt,
                null                                                       PerIndctPrleProbatnInd,
                null                                                       PerInitialNm,
                null                                                       PerIOBInd,
                null                                                       PerLastNm,
                null                                                       PerSfxNm,
                null                                                       PerTitlTxt,
                null                                                       PerUSCitznInd,
                null                                                       LIABINSURRQRDIND,
                null                                                       PRODLIABINSURRQRDIND,
                null                                                       LIQLIABINSURRQRDIND,
                null                                                       MALPRCTSINSURRQRDIND,
                null                                                       OTHINSURRQRDIND,
                null                                                       WORKRSCOMPINSRQRDIND                                                     
    from        loanapp.LoanGuarTbl               m
    inner join  loanapp.LoanBusTbl                b on (   (m.LoanAppNmb   = b.LoanAppNmb)
                                                       and (m.TaxId        = b.TaxId))
    where       m.LoanAppNmb                      = p_LoanAppNmb
    and         m.GuarBusPerInd                   = 'B'
union
    select                                                               m.TaxId,
                                                                         m.GuarBusPerInd,
                                                                         m.GuarSeqNmb,
                                                                         m.LACGntyCondTypCd,
                                                                         m.LoanGuarOperCoInd,
                                                                         null  LoanAffilInd,
                                                                         m.GntyLmtAmt,
                                                                         m.GntySubCondTypCd,
                                                                         m.LoanCollatSeqNmb,
                                                                         m.GntyLmtPct,
                                                                         m.GntyLmtYrNmb,
                                                                         
                -- Both Bus and Per:
                p.PerBnkrptcyInd                                           BnkrptcyInd,
                m.GuarBusPerInd                                            BusPerInd,
        to_char(p.PerExtrnlCrdtScorDt,            'Mon dd yyyy hh:miAM')   ExtrnlCrdtScorDt,
                p.PerExtrnlCrdtScorInd                                     ExtrnlCrdtScorInd,
                p.PerExtrnlCrdtScorNmb                                     ExtrnlCrdtScorNmb,
                                                                         p.IMCrdtScorSourcCd,
                p.PerPndngLwsuitInd                                        LawsuitInd,
                p.PerMailAddrCtyNm                                         MailAddrCtyNm,
                p.PerMailAddrStCd                                          MailAddrStCd,
                p.PerMailAddrStNm                                          MailAddrStNm,
                p.PerMailAddrCntCd                                         MailAddrCntCd,
                p.PerMailAddrPostCd                                        MailAddrPostCd,
                ocadataout(p.PerMailAddrStr1Nm )                                       MailAddrStr1Nm,
                ocadataout(p.PerMailAddrStr2Nm)                                        MailAddrStr2Nm,
                p.PerMailAddrZip4Cd                                        MailAddrZip4Cd,
                p.PerMailAddrZipCd                                         MailAddrZipCd,
                p.PerPhyAddrCtyNm                                          PhyAddrCtyNm,
                p.PerPhyAddrStCd                                           PhyAddrStCd,
                p.PerPhyAddrStNm                                           PhyAddrStNm,
                p.PerPhyAddrCntCd                                          PhyAddrCntCd,
                p.PerPhyAddrPostCd                                         PhyAddrPostCd,
               ocadataout(p.PerPhyAddrStr1Nm )                                        PhyAddrStr1Nm,
               ocadataout(p.PerPhyAddrStr2Nm)                                         PhyAddrStr2Nm,
                p.PerPhyAddrZip4Cd                                         PhyAddrZip4Cd,
                p.PerPhyAddrZipCd                                          PhyAddrZipCd,
                 ocadataout(p.PerPrimPhnNmb )                                           PrimPhnNmb,
                -- Unique to Bus:
                null                                                       BusChkngBalAmt,
                null                                                       BusCurBnkNm,
                null                                                       BusCurOwnrshpDt,
                null                                                       BusDUNSNmb,
                null                                                       BusEINCertInd,
                null                                                       BusExprtInd,
                null                                                       BusNm,
                null                                                       BusPriorSBALoanInd,
                null                                                       BusTrdNm,
                null                                                       BusTypCd,
                null                                                       LqdCrCBDInfo,
                null                                                       LqdCrBBADunBradCrRiskScr,
                -- Unique to Per:
                                                                         p.PerAffilEmpFedInd,
                                                                         p.PerAlienRgstrtnNmb,
                                                                         p.PerBrthCntCd,
                                                                         p.PerBrthCntNm,
                                                                         p.PerBrthCtyNm,
        to_char(p.PerBrthDt,                      'Mon dd yyyy hh:miAM')   PerBrthDt,
                                                                         p.PerBrthStCd,
                                                                         p.PerCnvctInd,
                                                                         p.PerCrmnlOffnsInd,
                                                                         ocadataout(p.PerFirstNm)  PerFirstNm,
        to_char(p.PerFngrprntWaivDt,              'Mon dd yyyy hh:miAM')   PerFngrprntWaivDt,
                                                                         p.PerIndctPrleProbatnInd,
                                                                         p.PerInitialNm,
                                                                         p.PerIOBInd,
                                                                         ocadataout(p.PerLastNm)  PerLastNm,
                                                                         p.PerSfxNm,
                                                                         p.PerTitlTxt,
                                                                         p.PerUSCitznInd,
        null                                                             LIABINSURRQRDIND,
        null                                                             PRODLIABINSURRQRDIND,
        null                                                             LIQLIABINSURRQRDIND,
        null                                                             MALPRCTSINSURRQRDIND,
        null                                                             OTHINSURRQRDIND,
        null                                                             WORKRSCOMPINSRQRDIND   
    from        loanapp.LoanGuarTbl               m
    inner join  loanapp.LoanPerTbl                p on (   (m.LoanAppNmb = p.LoanAppNmb)
                                                       and (m.TaxId      = p.TaxId))
    where       m.LoanAppNmb                      = p_LoanAppNmb
    and         m.GuarBusPerInd                   = 'P'
order by        1,2; -- TaxId, GuarBusPerInd

        END;
   END IF;
p_RetVal := nvl(p_RetVal,0);      
  EXCEPTION
 WHEN OTHERS THEN
                    BEGIN
                    ROLLBACK TO LoanGuarSel;
                    RAISE;
                    END;
   
END LOANGUARSELTSP;
/



GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANGUARINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARINSTSP(
p_Identifier         NUMBER:=0 ,
p_RetVal         OUT NUMBER,
p_LoanAppNmb         NUMBER:=0 ,
p_GuarSeqNmb     OUT NUMBER,
p_TaxId              CHAR:=NULL,
p_GuarBusPerInd      CHAR:=NULL,
p_LoanGuarOperCoInd  CHAR:=NULL,
p_GuarCreatUserId    CHAR:=NULL,
p_LACGntyCondTypCd   NUMBER := NULL, 
p_GntySubCondTypCd   NUMBER := NULL, 
p_GntyLmtAmt         NUMBER := NULL, 
p_GntyLmtPct         NUMBER := NULL, 
p_LoanCollatSeqNmb   NUMBER := NULL, 
p_GntyLmtYrNmb       NUMBER := NULL,
p_LIABINSURRQRDIND               CHAR:=NULL,
p_PRODLIABINSURRQRDIND           CHAR:=NULL,
p_LIQLIABINSURRQRDIND            CHAR:=NULL,
p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
p_OTHINSURRQRDIND                CHAR:=NULL,
p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
p_OTHINSURDESCTXT                VARCHAR2:=NULL
)
AS
/*  Date         : October 1, 2000
    Programmer   : Sheri Stine
    Remarks      :
    Parameters   :  No.   Name        Type     Description
                    1     p_Identifier  int     Identifies which batch
                                               to execute.
                    2     p_RetVal      int     No. of rows returned/
                                               affected
                    3    table columns to be inserted
*********
* *********************************************************************************
Revised By      Date        Commments
SP              9/26/2012   Modified as column LoanAffilInd is added to the table 
                            for determining companion relationship
SP              02/04/2014  Modified to add column LACGntyCondTypCd to support LADS
*********************************************************************************
*/
--NK--09/09/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd 
--BR--09/30/2016--OPSMDEV    -- Added new columns GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb  

   BEGIN
        
            SAVEPOINT LOANGUARINSTSP;
        
/* Insert into LoanGuar Table */
        IF  p_Identifier = 0 THEN
        BEGIN
        
            
    INSERT INTO LOANAPP.LoanGuarTbl(LoanAppNmb, 
                                    GuarSeqNmb,
                                    TaxId, 
                                    GuarBusPerInd,
                                    LoanGuarOperCoInd,
                                    GuarCreatUserId,
                                    GuarCreatDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    LACGntyCondTypCd,
                                    GntySubCondTypCd,
                                    GntyLmtAmt,
                                    GntyLmtPct,
                                    LoanCollatSeqNmb,
                                    GntyLmtYrNmb,
                                    LIABINSURRQRDIND,
                                    PRODLIABINSURRQRDIND,
                                    LIQLIABINSURRQRDIND,
                                    MALPRCTSINSURRQRDIND,
                                    OTHINSURRQRDIND,
                                    WORKRSCOMPINSRQRDIND,
                                    OTHINSURDESCTXT
)
                            SELECT  p_LoanAppNmb, 
                                    NVL((select MAX(GuarSeqNmb)
                                         from loanapp.LoanGuarTbl z
                                         where z.LoanAppNmb = p_LoanAppNmb), 0) + 1, 
                                    p_TaxId, 
                                    p_GuarBusPerInd,
                                    p_LoanGuarOperCoInd, 
                                    nvl(p_GuarCreatUserId,user),
                                    SYSDATE,
                                    nvl(p_GuarCreatUserId,user),
                                    sysdate,
                                    p_LACGntyCondTypCd,
                                    p_GntySubCondTypCd,
                                    p_GntyLmtAmt,
                                    p_GntyLmtPct,
                                    p_LoanCollatSeqNmb,
                                    p_GntyLmtYrNmb,
                                    p_LIABINSURRQRDIND,
                                    p_PRODLIABINSURRQRDIND,
                                    p_LIQLIABINSURRQRDIND,
                                    p_MALPRCTSINSURRQRDIND,
                                    p_OTHINSURRQRDIND,
                                    p_WORKRSCOMPINSRQRDIND,
                                    p_OTHINSURDESCTXT
                           FROM DUAL;
                
               /* LoanGuarTbl
            WHERE LoanAppNmb = p_LoanAppNmb;*/
            
            p_RetVal  := SQL%ROWCOUNT;
            SELECT   MAX(GuarSeqNmb)INTO p_GuarSeqNmb
             FROM LoanGuarTbl
            WHERE LoanAppNmb = p_LoanAppNmb;
            END;
            END IF;
p_RetVal := nvl(p_RetVal,0);    
p_GuarSeqNmb := nvl(p_GuarSeqNmb,0);
  EXCEPTION
    WHEN OTHERS THEN
    BEGIN
    ROLLBACK TO LOANGUARINSTSP;
    RAISE;
    END ;
    END;
/


GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARUPDCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARUPDCSP
(
    p_LoanAppNmb            number := 0 ,
    p_CreatUserId char := NULL
)as
/*
 Revision:
 APK -- 12/2/2011-- modified to add last update userid and last update date during insert into LoanHistryInfoTbl as this was missing. 
 APK -- 12/2/2011-- modified to add LastUpdtUserId,LastUpdtDt additionally to the insert call to LoanSpcPurpsHistryTbl as it was taking default USER from DB side.
 SP:9/26/2012: Modified as column LoanAffilInd is added to the table for determining companion relationship
 SP:11/15/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd
  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .
 */
v_LoanHistrySeqNmb number;
v_temp number;
v_temp1 number;
begin  
	--Get the history number based on the loan number and create user id
	loan.LoanHistrySelCSP(p_LoanAppNmb,p_CreatUserId,v_LoanHistrySeqNmb,'LoanGuarHistryTbl');
	
    --select count(*) into v_temp from LoanGuarHistryTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb;
    --if v_temp = 0 then
    --begin
        --Insert into history if history not exists
         INSERT INTO LoanGuarHistryTbl
           ( LoanHistrySeqNmb, GuarSeqNmb, TaxId, GuarBusPerInd, LoanGuarOperCoInd, LoanGuarHistryCreatUserId, LoanGuarHistryCreatDt,OUTLAWCD, OUTLAWOTHRDSCTXT,LastUpdtUserId,
			    LastUpdtDt,LACGNTYCONDTYPCD, GntySubCondTypCd  , 
               GntyLmtAmt , 
               GntyLmtPct, 
               LoanCollatSeqNmb , 
               GntyLmtYrNmb ,                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT   )
           SELECT v_LoanHistrySeqNmb,
                    GuarSeqNmb,
                    TaxId,
                    GuarBusPerInd,
                    LoanGuarOperCoInd,
                    LoanGuarLastUpdtUserId,
                    LoanGuarLastUpdtDt,
                    OUTLAWCD, 
                    OUTLAWOTHRDSCTXT,
                    LoanGuarLastUpdtUserId,
                    LoanGuarLastUpdtDt,
                    LACGNTYCONDTYPCD,
                     GntySubCondTypCd  , 
                     GntyLmtAmt , 
                     GntyLmtPct, 
                     LoanCollatSeqNmb , 
                     GntyLmtYrNmb  ,
                    LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT 
                    
             FROM LoanGntyGuarTbl a
                WHERE a.LoanAppNmb = p_LoanAppNmb;
				--AND a.LoanGuarActvInactInd = 'A' );
	--END;
    --end if;
	
	/*select count(*) into v_temp from LoanGuarHistryTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb;
    select count(*) into v_temp1 from LoanHistryInfoTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb and LoanHistryTblNm = 'LoanGuarHistryTbl';
	
    if v_temp = 0 AND v_temp1 = 0 then
	begin
		--Insert into history if history not exists
		insert into LoanHistryInfoTbl
		(
			LoanHistrySeqNmb,
			LoanHistryTblNm,
            LastUpdtUserId,
			    LastUpdtDt
		)
		values
		(
			v_LoanHistrySeqNmb,
			'LoanGuarHistryTbl',
            p_CreatUserId,
			SYSDATE
		);		
	end;
    end if;*/
		
	--loan.LoanChngLogInsCSP(p_LoanAppNmb,'S',p_CreatUserId,null,null);	
end;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARUPDTSP (p_IDENTIFIER                      NUMBER := 0,
                                                     p_RETVAL                      OUT NUMBER,
                                                     p_LOANAPPNMB                      NUMBER := 0,
                                                     p_GUARSEQNMB                      NUMBER := NULL,
                                                     p_TAXID                           VARCHAR2 := NULL,
                                                     p_GUARBUSPERIND                   VARCHAR2 := NULL,
                                                     p_LOANGUAROPERCOIND               CHAR := NULL,
                                                     p_LOANGUARACTVINACTIND            VARCHAR2 := NULL,
                                                     p_LOANGUARLASTUPDTUSERID          VARCHAR2 := NULL,
                                                     p_OUTLAWCD                        CHAR := NULL,
                                                     p_OUTLAWOTHRDSCTXT                VARCHAR2 := NULL,
                                                     p_TAXIDCHNGIND             IN     CHAR := NULL,
                                                     p_NEWTAXID                 IN     CHAR := NULL,
                                                     p_LACGNTYCONDTYPCD                NUMBER := NULL,
                                                     p_GntySubCondTypCd                NUMBER := NULL,
                                                     p_GntyLmtAmt                      NUMBER := NULL,
                                                     p_GntyLmtPct                      NUMBER := NULL,
                                                     p_LoanCollatSeqNmb                NUMBER := NULL,
                                                     p_GntyLmtYrNmb                    NUMBER := NULL,
                                                     p_LIABINSURRQRDIND               CHAR:=NULL,
                                                     p_PRODLIABINSURRQRDIND           CHAR:=NULL,
                                                     p_LIQLIABINSURRQRDIND            CHAR:=NULL,
                                                     p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
                                                     p_OTHINSURRQRDIND                CHAR:=NULL,
                                                     p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
                                                     p_OTHINSURDESCTXT                VARCHAR2:=NULL) AS
/*
  SP:11/12/2013:Created to support LADS screens
 *NK--08/08/2016--OPSMDEV-1030--  Removed LoanCntlIntInd  */
--  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .
--   NK -- 02/14/2018 --OPSMDEV 1672-- Added ID 14

BEGIN
    SAVEPOINT LOANGNTYGUARUPD;

    IF p_IDENTIFIER = 0 THEN
        BEGIN
            UPDATE LOAN.LOANGNTYGUARTBL
            SET TAXID = p_TAXID,
                GUARBUSPERIND = p_GUARBUSPERIND,
                LOANGUAROPERCOIND = p_LOANGUAROPERCOIND,
                LOANGUARACTVINACTIND = p_LOANGUARACTVINACTIND,
                OUTLAWCD = p_OUTLAWCD,
                OUTLAWOTHRDSCTXT = p_OUTLAWOTHRDSCTXT,
                LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
                LOANGUARLASTUPDTDT = SYSDATE,
                GntySubCondTypCd = p_GntySubCondTypCd,
                GntyLmtAmt = p_GntyLmtAmt,
                GntyLmtPct = p_GntyLmtPct,
                LoanCollatSeqNmb = p_LoanCollatSeqNmb,
                GntyLmtYrNmb = p_GntyLmtYrNmb,
                LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                OTHINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                OTHINSURDESCTXT = p_OTHINSURDESCTXT
            WHERE     LOANAPPNMB = p_LOANAPPNMB
                  AND GUARSEQNMB = p_GUARSEQNMB;

            p_RETVAL := SQL%ROWCOUNT;
        END;
    ELSIF p_IDENTIFIER = 11 THEN
        -- Update LACGNTYCONDTYPCD for LADS screens
        BEGIN
            UPDATE LOAN.LOANGNTYGUARTBL
            SET LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
                LOANGUARLASTUPDTDT = SYSDATE
            WHERE     LOANAPPNMB = p_LOANAPPNMB
                  AND GUARSEQNMB = p_GUARSEQNMB;

            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;

    IF p_Identifier = 14 THEN
        UPDATE Loan.LoanGntyGuarTbl
        SET LoanGuarOperCoInd = p_LoanGuarOperCoInd
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND TaxId = p_TaxId;
    END IF;

    IF p_Identifier = 15 THEN
        UPDATE Loan.LoanGntyGuarTbl
        SET LOANGUARACTVINACTIND = p_LOANGUARACTVINACTIND,
            LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
            LOANGUARLASTUPDTDT = SYSDATE
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND TaxId = p_TaxId;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            ROLLBACK TO LOANGNTYGUARUPD;
            RAISE;
        END;
END;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARINSCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARINSCSP (
   p_LoanAppNmb                      NUMBER := 0,
   p_TaxId                           CHAR := NULL,
   p_GuarBusPerInd                   CHAR := NULL,
   p_LoanGuarOperCoInd               VARCHAR2 := NULL,
   p_LoanGuarLastUpdtUserId          CHAR := NULL,
   p_OUTLAWCD                        CHAR := NULL,
   p_OUTLAWOTHRDSCTXT                VARCHAR2 := NULL,
   p_TaxIdChngInd             IN     CHAR := NULL,
   p_NewTaxId                 IN     CHAR := NULL,
   p_LACGNTYCONDTYPCD                NUMBER := NULL,
   p_GuarSeqNmb                  OUT NUMBER,
   p_GntySubCondTypCd                NUMBER := NULL,
   p_GntyLmtAmt                      NUMBER := NULL,
   p_GntyLmtPct                      NUMBER := NULL,
   p_LoanCollatSeqNmb                NUMBER := NULL,
   p_GntyLmtYrNmb                    NUMBER := NULL,
   p_LIABINSURRQRDIND               CHAR:=NULL,
   p_PRODLIABINSURRQRDIND           CHAR:=NULL,
   p_LIQLIABINSURRQRDIND            CHAR:=NULL,
   p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
   p_OTHINSURRQRDIND                CHAR:=NULL,
   p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
   p_OTHINSURDESCTXT                VARCHAR2:=NULL)
AS
   /*SP:06/05/2013: Modified to insert/update LoanAffilInd in LoanGntyGuarTbl for caluculating gnty fee
     SP:11/12/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
     NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd
    NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .

    */
   v_count1   NUMBER;
BEGIN
   SAVEPOINT LoanGntyGuarInsCSP;

   BEGIN
      SELECT COUNT (*)
        INTO v_count1
        FROM LoanGntyGuarTbl
       WHERE     LoanAppNmb = p_LoanAppNmb
             AND TaxId = p_TaxId
             AND GuarBusPerInd = p_GuarBusPerInd;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF (V_COUNT1 = 0)
   THEN
      BEGIN
         INSERT INTO LoanGntyGuarTbl (LoanAppNmb,
                                      GuarSeqNmb,
                                      TaxId,
                                      GuarBusPerInd,
                                      LoanGuarOperCoInd,
                                      LoanGuarActvInactInd,
                                      LoanGuarCreatDt,
                                      LoanGuarLastUpdtUserId,
                                      LoanGuarLastUpdtDt,
                                      OUTLAWCD,
                                      OUTLAWOTHRDSCTXT,
                                      LACGNTYCONDTYPCD,
                                      GntySubCondTypCd,
                                      GntyLmtAmt,
                                      GntyLmtPct,
                                      LoanCollatSeqNmb,
                                      GntyLmtYrNmb,
                                      LIABINSURRQRDIND,
                                      PRODLIABINSURRQRDIND,
                                      LIQLIABINSURRQRDIND,
                                      MALPRCTSINSURRQRDIND,
                                      OTHINSURRQRDIND,
                                      WORKRSCOMPINSRQRDIND,
                                      OTHINSURDESCTXT)
            SELECT p_LoanAppNmb,
                   (  NVL ( (SELECT MAX (z.GuarSeqNmb)
                               FROM LoanGntyGuarTbl z
                              WHERE z.LoanAppNmb = p_LoanAppNmb),
                           0)
                    + 1)
                      AS GuarSeqNmb,
                   p_TaxId,
                   p_GuarBusPerInd,
                   p_LoanGuarOperCoInd,
                   'A',
                   SYSDATE,
                   p_LoanGuarLastUpdtUserId,
                   SYSDATE,
                   p_OUTLAWCD,
                   p_OUTLAWOTHRDSCTXT,
                   p_LACGNTYCONDTYPCD,
                   p_GntySubCondTypCd,
                   p_GntyLmtAmt,
                   p_GntyLmtPct,
                   p_LoanCollatSeqNmb,
                   p_GntyLmtYrNmb,
                   p_LIABINSURRQRDIND,
                   p_PRODLIABINSURRQRDIND,
                   p_LIQLIABINSURRQRDIND,
                   p_MALPRCTSINSURRQRDIND,
                   p_OTHINSURRQRDIND,
                   p_WORKRSCOMPINSRQRDIND,
                   p_OTHINSURDESCTXT
              FROM DUAL;

         --LoanGntyGuarTbl
         --where LoanAppNmb = p_LoanAppNmb;


         BEGIN
            SELECT MAX (GuarSeqNmb)
              INTO p_GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;
      END;
   ELSE
      BEGIN
         BEGIN
            SELECT GuarSeqNmb
              INTO p_GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND TaxId = p_TaxId
                   AND GuarBusPerInd = p_GuarBusPerInd;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         UPDATE LoanGntyGuarTbl
            SET TaxId =
                   (CASE
                       WHEN p_TaxIdChngInd = 'Y' THEN p_NewTaxId
                       ELSE p_TaxId
                    END),
                LoanGuarActvInactInd = 'A',
                LoanGuarLastUpdtUserId = p_LoanGuarLastUpdtUserId,
                LoanGuarLastUpdtDt = SYSDATE,
                OUTLAWCD = p_OUTLAWCD,
                OUTLAWOTHRDSCTXT = p_OUTLAWOTHRDSCTXT,
                LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                GntySubCondTypCd = p_GntySubCondTypCd,
                GntyLmtAmt = p_GntyLmtAmt,
                GntyLmtPct = p_GntyLmtPct,
                LoanCollatSeqNmb = p_LoanCollatSeqNmb,
                GntyLmtYrNmb = p_GntyLmtYrNmb,
                LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                OTHINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                OTHINSURDESCTXT = p_OTHINSURDESCTXT
          WHERE LoanAppNmb = p_LoanAppNmb AND GuarSeqNmb = p_GuarSeqNmb;
      END;
   END IF;

 

   p_GuarSeqNmb := NVL (p_GuarSeqNmb, 0);
END LOANGNTYGUARINSCSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYGUARSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARSELTSP (p_Identifier          IN     NUMBER := 0,
                                                     p_LoanAppNmb          IN     NUMBER := 0,
                                                     p_GuarSeqNmb          IN     NUMBER := 0,
                                                     p_TaxId               IN     CHAR := NULL,
                                                     p_GuarBusPerInd       IN     CHAR := NULL,
                                                     p_RetVal                 OUT NUMBER,
                                                     p_SelCur                 OUT SYS_REFCURSOR)
AS
/*SP:9/26/2012 : Modified as column LoanAffilInd is added to the table for determining companion relationship
 *SP:11/12/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
  RSURAPS : 11/21/2013 : add LACGNTYCONDTYPCD to identifier 14 to support LADS screens
  SB -- Added Identifier 38 for Extract XML
  NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd  in ID 0 and 38
  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in ID 0 and 38.
  RY--11/27/2018--OPSMDEV-2044:: Added Ocadataout for PII columns of PERTBL
  SL--07/31/2020--CARESACT-595: Modifed to use LoanGntyBusTbl data instead of BusTbl to get loan level bus data
  --SS--07/31/2020-- CARESACT 595 Replaced pertbl with loan level loangntypertbl
*/
BEGIN
   /* Select Row On the basis of Key*/
   IF p_Identifier = 0
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LoanGuarCreatDt,
                   OutLawCd,
                   OutLawOthrDscTxt,
                   LACGNTYCONDTYPCD,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 2
   THEN
      /*Check The Existance Of Row*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT 1
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 11
   THEN
      /* return all guarantors of the loan*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LoanGuarCreatDt
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 12
   THEN
      /*Check The Existance Of Row based on LoanAppNmb and TaxId*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND TaxId = p_TaxId
                   AND GuarBusPerInd = p_GuarBusPerInd
                   AND LoanGuarActvInactInd = 'A';


         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 14
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT GuarSeqNmb,
                   GuarBusPerInd,
                   a.TaxId,
                   BusNm,
                   NULL PerFirstNm,
                   NULL PerLastNm,
                   NULL PerInitialNm,
                   NULL PerSfxNm,
                   LACGNTYCONDTYPCD
              FROM Loan.BusTbl b, LoanGntyGuarTbl a
             WHERE     a.LoanAppNmb = p_LoanAppNmb
                   AND a.TaxId = b.TaxId
                   AND GuarBusPerInd = 'B'
                   AND LoanGuarActvInactInd = 'A'
            UNION
            SELECT GuarSeqNmb,
                   GuarBusPerInd,
                   a.TaxId,
                   NULL BusNm,
                   OcaDataOut (PerFirstNm),
                   OcaDataOut (PerLastNm),
                   PerInitialNm,
                   PerSfxNm,
                   LACGNTYCONDTYPCD
              FROM PerTbl b, LoanGntyGuarTbl a
             WHERE     a.LoanAppNmb = p_LoanAppNmb
                   AND a.TaxId = b.TaxId
                   AND GuarBusPerInd = 'P'
                   AND LoanGuarActvInactInd = 'A'
            ORDER BY GuarSeqNmb;


         p_RetVal := SQL%ROWCOUNT;
      END;
   /* 38 = Leet for Extract XML, initially indented for union */
   ELSIF p_Identifier = 38
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT m.TaxId,
                   m.GuarBusPerInd,
                   m.GuarSeqNmb,                                                                       --NK-- 01/12/2017
                   m.LoanGuarOperCoInd,
                   m.LACGntyCondTypCd,
                   NULL LoanAffilInd,                                                     --NK--10/12/2016--OPSMDEV 1018
                   m.OutlawCd,
                   m.OutlawOthrDscTxt,
                   m.GntySubCondTypCd,
                   --m.GntyLmtAmt ,
                   TO_CHAR (m.GntyLmtAmt, 'FM999999999999990.00') GntyLmtAmt,
                   --m.GntyLmtPct,
                   TO_CHAR (m.GntyLmtPct, 'FM990.000') GntyLmtPct,
                   m.LoanCollatSeqNmb,
                   m.GntyLmtYrNmb,
                   -- Both Bus and Per
                   bnk.ACHAcctNmb,
                   bnk.ACHAcctTypCd,
                   bnk.ACHRoutingNmb,
                   c.BusAltEmailAdr AltEmailAdr,
                   c.BusAltPhnNmb AltPhnNmb,
                   m.GuarBusPerInd BusPerInd,
                   b.EthnicCd,
                   TO_CHAR (c.BusExtrnlCrdtScorDt, 'Mon dd yyyy hh:miAM') ExtrnlCrdtScorDt,
                   c.BusExtrnlCrdtScorInd ExtrnlCrdtScorInd,
                   c.BusExtrnlCrdtScorNmb ExtrnlCrdtScorNmb,
                   b.GndrCd,
                   c.IMCrdtScorSourcCd,
                   c.BusMailAddrCntCd MailAddrCntCd,
                   c.BusMailAddrCtyNm MailAddrCtyNm,
                   c.BusMailAddrPostCd MailAddrPostCd,
                   c.BusMailAddrStCd MailAddrStCd,
                   c.BusMailAddrStNm MailAddrStNm,
                   c.BusMailAddrStr1Nm MailAddrStr1Nm,
                   c.BusMailAddrStr2Nm MailAddrStr2Nm,
                   c.BusMailAddrZip4Cd MailAddrZip4Cd,
                   c.BusMailAddrZipCd MailAddrZipCd,
                   c.BusPhyAddrCntCd PhyAddrCntCd,
                   c.BusPhyAddrCtyNm PhyAddrCtyNm,
                   c.BusPhyAddrPostCd PhyAddrPostCd,
                   c.BusPhyAddrStCd PhyAddrStCd,
                   c.BusPhyAddrStNm PhyAddrStNm,
                   c.BusPhyAddrStr1Nm PhyAddrStr1Nm,
                   c.BusPhyAddrStr2Nm PhyAddrStr2Nm,
                   c.BusPhyAddrZip4Cd PhyAddrZip4Cd,
                   c.BusPhyAddrZipCd PhyAddrZipCd,
                   c.BusPrimEmailAdr PrimEmailAdr,
                   c.BusPrimPhnNmb PrimPhnNmb,
                   b.VetCd,
                   -- Unique to Bus
                   c.BusDUNSNmb,
                   b.BusEINCertInd,
                   b.BusNm,
                   c.BusPriorSBALoanInd,
                   c.BusTrdNm,
                   b.BusTypCd,
                   -- Unique to Per
                   NULL PerFirstNm,
                   NULL PerInitialNm,
                   NULL PerLastNm,
                   NULL PerSfxNm,
                   NULL PerTitlTxt,
                   NULL PerUSCitznInd,
                   null LIABINSURRQRDIND,
                   null PRODLIABINSURRQRDIND,
                   null LIQLIABINSURRQRDIND,
                   null MALPRCTSINSURRQRDIND,
                   null OTHINSURRQRDIND,
                   null WORKRSCOMPINSRQRDIND 
              FROM loan.LoanGntyGuarTbl m                                  -- main, keeps code similar to borr/guar/prin
                   INNER JOIN loan.BusTbl b ON (m.TaxId = b.TaxId)
                   INNER JOIN Loan.LoanGntyBusTbl c ON m.Taxid = c.Taxid AND m.LoanAppNmb = c.LoanAppNmb
                   LEFT JOIN loan.BnkTbl bnk
                      ON (    (bnk.TaxId = b.TaxId)
                          AND (bnk.BusPerInd = 'B'))
             WHERE     m.LoanAppNmb = p_LoanAppNmb
                   AND m.LoanGuarActvInactInd = 'A'
                   AND m.GuarBusPerInd = 'B'
            UNION
            SELECT m.TaxId,
                   m.GuarBusPerInd,
                   m.GuarSeqNmb,
                   m.LoanGuarOperCoInd,
                   m.LACGntyCondTypCd,
                   NULL LoanAffilInd,
                   m.OutlawCd,
                   m.OutlawOthrDscTxt,
                   m.GntySubCondTypCd,
                   --m.GntyLmtAmt ,
                   TO_CHAR (m.GntyLmtAmt, 'FM999999999999990.00') GntyLmtAmt,
                   --m.GntyLmtPct,
                   TO_CHAR (m.GntyLmtPct, 'FM990.000') GntyLmtPct,
                   m.LoanCollatSeqNmb,
                   m.GntyLmtYrNmb,
                   -- Both Bus and Per
                   bnk.ACHAcctNmb,
                   bnk.ACHAcctTypCd,
                   bnk.ACHRoutingNmb,
                   OcaDataOut (p.PerAltEmailAdr) AS AltEmailAdr,
                   OcaDataOut (p.PerAltPhnNmb) AS AltPhnNmb,
                   m.GuarBusPerInd BusPerInd,
                   pt.EthnicCd,
                   TO_CHAR (p.PerExtrnlCrdtScorDt, 'Mon dd yyyy hh:miAM') ExtrnlCrdtScorDt,
                   p.PerExtrnlCrdtScorInd ExtrnlCrdtScorInd,
                   p.PerExtrnlCrdtScorNmb ExtrnlCrdtScorNmb,
                   pt.GndrCd,
                   p.IMCrdtScorSourcCd,
                   p.PerMailAddrCntCd MailAddrCntCd,
                   p.PerMailAddrCtyNm MailAddrCtyNm,
                   p.PerMailAddrPostCd MailAddrPostCd,
                   p.PerMailAddrStCd MailAddrStCd,
                   p.PerMailAddrStNm MailAddrStNm,
                   OcaDataOut (p.PerMailAddrStr1Nm) AS MailAddrStr1Nm,
                   OcaDataOut (p.PerMailAddrStr2Nm) AS MailAddrStr2Nm,
                   p.PerMailAddrZip4Cd MailAddrZip4Cd,
                   p.PerMailAddrZipCd MailAddrZipCd,
                   p.PerPhyAddrCntCd PhyAddrCntCd,
                   p.PerPhyAddrCtyNm PhyAddrCtyNm,
                   p.PerPhyAddrPostCd PhyAddrPostCd,
                   p.PerPhyAddrStCd PhyAddrStCd,
                   p.PerPhyAddrStNm PhyAddrStNm,
                   OcaDataOut (p.PerPhyAddrStr1Nm) AS PhyAddrStr1Nm,
                   OcaDataOut (p.PerPhyAddrStr2Nm) AS PhyAddrStr2Nm,
                   p.PerPhyAddrZip4Cd PhyAddrZip4Cd,
                   p.PerPhyAddrZipCd PhyAddrZipCd,
                   OcaDataOut (p.PerPrimEmailAdr) AS PrimEmailAdr,
                   OcaDataOut (p.PerPrimPhnNmb) AS PrimPhnNmb,
                   pt.VetCd,
                   -- Unique to Bus
                   NULL BusDUNSNmb,
                   NULL BusEINCertInd,
                   NULL BusNm,
                   NULL BusPriorSBALoanInd,
                   NULL BusTrdNm,
                   NULL BusTypCd,
                   -- Unique to Per
                   OcaDataOut (pt.PerFirstNm) AS PerFirstNm,
                   pt.PerInitialNm,
                   OcaDataOut (pt.PerLastNm) AS PerLastNm,
                   pt.PerSfxNm,
                   p.PerTitlTxt,
                   p.PerUSCitznInd,
                   null LIABINSURRQRDIND,
                   null PRODLIABINSURRQRDIND,
                   null LIQLIABINSURRQRDIND,
                   null MALPRCTSINSURRQRDIND,
                   null OTHINSURRQRDIND,
                   null WORKRSCOMPINSRQRDIND 
              FROM loan.LoanGntyGuarTbl m                                  -- main, keeps code similar to borr/guar/prin
                   INNER JOIN loan.PerTbl pt ON (m.TaxId = pt.TaxId)
                   INNER JOIN loan.loangntyPerTbl p ON (m.TaxId = p.TaxId)
                   LEFT JOIN loan.BnkTbl bnk
                      ON (    (bnk.TaxId = p.TaxId)
                          AND (bnk.BusPerInd = 'P'))
             WHERE     m.LoanAppNmb = p_LoanAppNmb
                   AND m.LoanGuarActvInactInd = 'A'
                   AND m.GuarBusPerInd = 'P'
                   
            ORDER BY 1, 2;                                                                           -- TaxId, BusPerInd


         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
END LOANGNTYGUARSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLANAREADROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLOOKUPUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO READLOANROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO UPDLOANROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0607
define package_buildtime=20210607164322
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0607 created on Mon 06/07/2021 16:43:22.86 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0607 created on Mon 06/07/2021 16:43:22.86 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0607: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\update_FORMLAYOUT_0607.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\update_record_VALIDATIONEXCPTNTBL_0607.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATESUBPROCDCSP.sql 
Jasleen Gorowada committed 1447a73 on Wed Jun 2 16:11:42 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\update_FORMLAYOUT_0607.sql"
update SBAREF.FORMLAYOUTTBL set FORMNM='LoanAuth Proceed SubType Lookup' where FORMID=485;
update SBAREF.FORMLAYOUTTBL set FORMNM='LoanAuth Proceed Text Block' where FORMID=484;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\update_record_VALIDATIONEXCPTNTBL_0607.sql"
update LOANAPP.LOANVALIDATIONERRTBL set ERRTXT ='For application, sum of sub proceeds for proceed type ^1 must equal the proceed amount ^3.' where ERRCD=4289;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATESUBPROCDCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATESUBPROCDCSP(
   p_RetVal             OUT NUMBER,
   p_LoanAppNmb      IN     NUMBER DEFAULT NULL,
   p_ErrSeqNmb       IN OUT NUMBER,
   p_TransInd        IN     NUMBER DEFAULT NULL,
   p_LoanAppRecvDt   IN     DATE DEFAULT NULL)
AS
cursor fetch_procd_rec is
select LOANPROCDSEQNMB,LoanProcdAmt,ProcdTypcd,LoanProcdTypCd  FROM LOANAPP.LOANPROCDTBL WHERE LoanAppNmb = p_LoanAppNmb;

sub_recs_sum number:=0;          

     	
BEGIN
    SAVEPOINT VALIDATESUBPROCDCSP;
    
    for rec in fetch_procd_rec
    loop
        select nvl(sum(LOANPROCDAMT),0) into sub_recs_sum from LOANAPP.LOANSUBPROCDTBL where 
        LOANPROCDTYPCD=rec.LOANPROCDTYPCD and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= rec.PROCDTYPCD;         	
             IF rec.PROCDTYPCD = 'E' AND sub_recs_sum != rec.LoanProcdAmt THEN
                p_ErrSeqNmb := ADD_ERROR (p_LoanAppNmb,
                                          p_ErrSeqNmb,
                                          4289,
                                          p_TransInd,
                                          TO_CHAR (rec.PROCDTYPCD)||TO_CHAR (rec.LOANPROCDTYPCD),
                                          TO_CHAR (rec.LOANPROCDSEQNMB),
                                          rec.LoanProcdAmt,
                                          null,
                                          NULL,
                                          null);
             END IF;
    end loop;
            p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
END;
/

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO UPDLOANROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0603
define package_buildtime=20210603100102
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0603 created on Thu 06/03/2021 10:01:03.24 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0603 created on Thu 06/03/2021 10:01:03.24 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0603: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOAN.LOANGNTYPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOAN.LOANGNTYSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANGNTYPROCDTOTAMTSELTSP;

    /* Select from LOAN.LOANGNTYPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDTOTAMTSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO UPDLOANROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOANAPP.LOANPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOANAPP.LOANSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANPROCDTOTAMTSELTSP;

    /* Select from LOANAPP.LOANPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANPROCDTOTAMTSELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO POOLSECADMINROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0601_fix_errors
define package_buildtime=20210601151113
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0601_fix_errors created on Tue 06/01/2021 15:11:14.19 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0601_fix_errors created on Tue 06/01/2021 15:11:14.19 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0601_fix_errors: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\drop_objects.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANSUBPROCDIDSEQ.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATELOANCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\drop_objects.sql"
drop table LOANAPP.LOANPROCDSUBTYPTBL;
drop table LOAN.LOANGNTYPROCDSUBTYPTBL;
drop table SBAREF.PROCTXTBLCK;
drop table SBAREF.LOANPROCDSUBTYPLKUPTBL;
drop sequence LOANAPP.LOANPROCDSUBTYPIDSEQ;
drop sequence LOAN.LOANGNTYPROCDSUBTYPID_SEQ;
drop procedure LOAN.LOANGNTYPROCDSUBTYPDELTSP;
drop procedure LOAN.LOANGNTYPROCDSUBTYPINSTSP;
drop procedure LOAN.LOANGNTYPROCDSUBTYPSELTSP;
drop procedure LOAN.LOANGNTYPROCDSUBTYPSUMTSP;
drop procedure LOAN.LOANGNTYPROCDTOTAMTSELTSP;
drop procedure LOANAPP.LOANPROCDSUBTYPDELTSP;
drop procedure LOANAPP.LOANPROCDSUBTYPINSTSP;
drop procedure LOANAPP.LOANPROCDSUBTYPSELTSP;
drop procedure LOANAPP.LOANPROCDSUBTYPSUMTSP;
drop procedure LOANAPP.LOANPROCDTOTAMTSELTSP;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANSUBPROCDIDSEQ.sql"
CREATE SEQUENCE LOANAPP.LOANSUBPROCDIDSEQ
  START WITH 133
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  ORDER
  NOKEEP
  GLOBAL;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATELOANCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATELOANCSP (p_retval            OUT NUMBER,
                                                     p_loanappnmb     IN     NUMBER DEFAULT NULL,
                                                     p_transind       IN     NUMBER DEFAULT NULL,
                                                     p_cleardataind   IN     VARCHAR DEFAULT 'Y',
                                                     p_selcur1           OUT SYS_REFCURSOR,
                                                     p_selcur2           OUT SYS_REFCURSOR) AS
    /*
     Stored Procedure: LOANAPP.VALIDATELOANCSP
     Database        : LOANAPP
     Purpose         : To check for all validations.
     Date            :
     Author          :
     Authors Note    :
     Parameters      :
     -- --.
    ********************* REVISION HISTORY ****************************************
     Revised By   Date             Description
     ----------  ----------     -----------------------------------------------
    APK          02/08/2011     Modified to add new validation VALIDATEELIGCSP
                                exceptions
    APK         02/07/2012     Modified to add new procedure VALIDATEAPPCACSP
    APK         02/08/2012     Modified to exclude VALIDATEAPPCACSP if process method is
                                not    CAI
    APK         05/22/2012    Please create following two new validations for LAI loan.
                              1.    Credit Score is Mandatory.
                              2.    Credit Score needs to be greater than 140.
                              We already have parameters for the processing methods and
                              credit score limits from ARC loans implementation.
                              They are:CrdScrThrshld:  This holds the lower limit and will
                              have to be set to 140.
    APK         05/22/2012    Modified to change the PrcsMthdCd from SLA to LAI as it was determined that the correct code is LAI.
    SP          05/02/2013    Commented the code for error 1037 to allow poor score loans
    SP          12/26/2013    Added call to LOANAPP.VALIDATEINTDTLCSP
    SP          01/08/2014    Modified error 1036 to check for PrcsMthdReqLqdCr value
    RY          05/16/2016    Added new validation 'ValidateLoanChrgofCsp'--OPSM DEV-835
    RY          14/11/2016 --OPSM DEV 1245   Added new validation 'validateloanpymntcsp'
    SS-         07/25/2018 OPSMDEV 1862 Added Validateagntcsp
    RY-- 08/12/2018--OPSMDEV-1876-- Added new Sp call LOANAPP.VALIDATERURALINITIATVCSP
    SB - 07/19/2020 Added v_PrcsMthdCd IN ('CAI','CRL')
    SL - 01/04/2021 SODSTORY-344.  Added new sp call to loanApp.VALIDATELENRCMNTCSP to validate lender comments
    **************** END OF COMMENTS ******************************************
    */

    v_errseqnmb             NUMBER (10, 0);
    v_loanapprecvdt         DATE;
    v_prcsmthdcd            CHAR (3);
    v_recovind              CHAR (1);
    v_crdscrthrshldmin      NUMBER (10, 0);
    v_lqdcrtotscr           CHAR (3);
    v_imprmtrvalchk         VARCHAR (255);
    v_imprmtrid             VARCHAR2 (255);
    v_loanapprqstamt        NUMBER (19, 4);
    v_prcsmthdreqlqdcramt   NUMBER (19, 4);
    v_loanagntseqnmb        NUMBER (10);
BEGIN
    p_retval := 0;
    v_loanapprecvdt := SYSDATE;
    v_errseqnmb := 1;

    IF    p_cleardataind IS NULL
       OR p_cleardataind = 'Y' THEN
        BEGIN
            DELETE FROM loanerrtbl
            WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    BEGIN
        SELECT NVL (MAX (errseqnmb), 0) + 1
        INTO v_errseqnmb
        FROM loanapp.loanerrtbl e
        WHERE e.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_errseqnmb := 1;
    END;

    BEGIN
        SELECT prcsmthdcd,
               NVL (loanapprecvdt, SYSDATE),
               recovind,
               loanapprqstamt
        INTO v_prcsmthdcd,
             v_loanapprecvdt,
             v_recovind,
             v_loanapprqstamt
        FROM loanapptbl a
        WHERE a.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_prcsmthdcd := NULL;
    END;

    validateappcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateappprtcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateintdtlcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateprocdcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validatespcpurpscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateeligcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);

    IF v_prcsmthdcd != 'SMP' THEN
		validateappprojcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateborrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebuscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebusprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepercsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprinracecsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    END IF;
    
    if v_prcsmthdcd NOT IN('PPP','PPS') THEN
    	validatelenrcmntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    	validatebscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecrdtunavrsncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatliencsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateiscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateguarcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatefedempcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateindbtnescsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateinjctncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateperfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprevfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepartlendrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateloanchrgofcsp (p_loanappnmb, v_errseqnmb, p_transind);
		validateloanpymntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateloanexprtcntrycsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateagntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
    END IF;

    IF v_prcsmthdcd IN ('CAI', 'CRL') THEN
        loanapp.validateappcacsp (p_loanappnmb, p_transind, v_loanapprecvdt, v_errseqnmb, p_retval);
    END IF;
    
    if v_prcsmthdcd='504' THEN
        loanapp.validatesubprocdcsp  (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    end if;

    OPEN p_selcur1 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'F'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    OPEN p_selcur2 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'W'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    p_retval := NVL (p_retval, 0);
--dbms_output.put_line(29);
END;
/


GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRFHINES;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRILIBERMAN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNNAMILAE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNSALATOVA;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSJUSTIN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSMANIAM;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERUPDCDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSREADROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBACDUPDTROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFUPDCDROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select LOANPROCDSEQNMB from LOANAPP.LOANPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
    v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    
BEGIN
    SAVEPOINT LOANSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDINSTSP;
            RAISE;
        END;
END LOANSUBPROCDINSTSP;
/
GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCOPYCSP (
    p_LoanAppNmb          NUMBER := NULL,
    p_CreatUserId         VARCHAR2 := NULL,
    p_NEWLoanAppNmb   OUT NUMBER)
AS
    /*
     created by:   BJRychener
     created date: October 31, 2017
     purpose:      Loan Copy Procedure
     Revision:

    */
    /* NK--11/90/2017--OPSMDEV 1551 added PerFedAgncySDPIEInd, PerCSP60DayDelnqInd,PerLglActnInd, PerCitznShpCntNm ,PerAltPhnNmb, PerPrimEmailAdr ,PerAltEmailAdr To LoanPerTbl
       NK--11/13/2017 OPSMDEV 1551  Added BusAltPhnNmb ,  BusPrimEmailAdr ,  BusAltEmailAdr, BusCSP60DayDelnqInd, BusLglActnInd to LoanBusTbl
       NK--11/13/2017 OPSMDEV 1558  Added BusSexNatrInd,BusNonFmrSBAEmpInd,BusNonLegBrnchEmpInd,BusNonFedEmpInd,BusNonGS13EmpInd,BusNonSBACEmpInd to LoanBusTbl
       BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
       NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to LoanPrevFinanTbl
       SS 11/15/2017--OPSMDEV 1573 Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,
       LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn,LoanPrtAltCntctCellPhn,
              LoanPrtAltCntctemail, LendrAltCntctTypCd to LOANAPPPRTTBL
       SS--11/15/2017 --OPSMDEV 1566 Added LOANBUSESTDT to Loanappprojtbl and BusPrimCntctNm  to LoanBustbl
       NK-- 11/15/2017--OPSMDEV 1591 Added PeredAgncyLoanInd to LoanPerTbl
       NK--11/15/2017--OPSMDEV 1591 Added BusedAgncyLoanInd to LoanBusTbl
       NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to LoanBusTbl
       NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to LoanAppTbl
       BJR-- 02/01/2018--CAFSOPER-1407-- added UPPER() to srch names to allow records to be stored in caps and easier to find during searches.
       RY-- 04/20/2018--OPSMDEV--1786 --Added WorkrsCompInsRqrdInd to LoanBorrTbl
       SS--08/06/2018--OPSMDEV--1882-- Added LoanAGNtTbl and LoanAgntfeedtltbl
       SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to loanapptbl
       BR--10/30/2018--OPSMDEV 1885 -- added LOANAPPPROJGEOCD, LOANAPPHUBZONEIND, LOANAPPPROJLAT, LOANAPPPROJLONG
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
       SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to loanapptbl
       SS--02/18/2020 OPSMDEV 2409,2410 Added LoanAppCBRSInd,LoanAppOppZoneInd to LoanAppProjTbl
       SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to loanapptbl
       SS--09/05/2020 CARESACT 595 copying demographics from loan master tables
       SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to LoanAppProjTbl
       JS--02/11/2021 Loan Authorization Modernization. Added 7 new columns LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to LoanApp.LoanGuarTbl.
       SL-04/26/2021--OPSMDEV-2576  Addeed new column PerTAXIDCertInd to LoanPerTbl
    */

    v_NEWLoanAppNmb   NUMBER := NULL;
    v_count           NUMBER;
BEGIN
    BEGIN
        --Generate New Loan App Number using MAX(LoanAppNmb) from LoanApp.LoanAppTbl
        LoanApp.IncrmntSeqNmbCSP ('Application', v_NEWLoanAppNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanAppTbl
        INSERT INTO LoanApp.LoanAppTbl (LoanAppNmb,
                                        PrgmCd,
                                        PrcsMthdCd,
                                        LoanAppNm,
                                        LoanAppRecvDt,
                                        LoanAppEntryDt,
                                        LoanAppRqstAmt,
                                        LoanAppSBAGntyPct,
                                        LoanAppSBARcmndAmt,
                                        LoanAppRqstMatMoQty,
                                        LoanAppEPCInd,
                                        LoanAppPymtAmt,
                                        LoanAppFullAmortPymtInd,
                                        LoanAppMoIntQty,
                                        LoanAppLifInsurRqmtInd,
                                        LoanAppRcnsdrtnInd,
                                        LoanAppInjctnInd,
                                        LoanAppEWCPSnglTransPostInd,
                                        LoanAppEWCPSnglTransInd,
                                        LoanAppEligEvalInd,
                                        LoanAppNewBusCd,
                                        LoanAppDisasterCntrlNmb,
                                        LoanCollatInd,
                                        LoanAppSourcTypCd,
                                        StatCd,
                                        LoanAppStatUserId,
                                        LoanAppStatDt,
                                        LoanAppOrigntnOfcCd,
                                        LoanAppPrcsOfcCd,
                                        LoanAppAppvDt,
                                        RecovInd,
                                        LoanAppMicroLendrId,
                                        LOANDISASTRAPPNMB,
                                        LOANDISASTRAPPFEECHARGED,
                                        LOANDISASTRAPPDCSN,
                                        LOANASSOCDISASTRAPPNMB,
                                        LOANASSOCDISASTRLOANNMB,
                                        LoanAppCreatUserId,
                                        LoanAppCreatDt,
                                        LastUpdtUserId,
                                        LastUpdtDt,
                                        LOANAPPNETEARNIND,
                                        LOANAPPSERVOFCCD,
                                        LoanAppRevlInd,
                                        LOANNMB,
                                        LOANAPPFUNDDT,
                                        UNDRWRITNGBY,
                                        COHORTCD,
                                        FUNDSFISCLYR,
                                        LOANTYPIND,
                                        LOANAPPINTDTLCD,
                                        LOANPYMNINSTLMNTFREQCD,
                                        FININSTRMNTTYPIND,
                                        LOANAMTLIMEXMPTIND,
                                        LoanPymntSchdlFreq,
                                        LoanPymtRepayInstlTypCd,
                                        LOANGNTYMATSTIND,
                                        LoanGntyNoteDt,
                                        CDCServFeePct,
                                        LoanAppContribAmt,
                                        LoanAppContribPct,
                                        LoanAppCDCGntyAmt,
                                        LOANAPPCDCNETDBENTRAMT,
                                        LoanAppCDCFundFeeAmt,
                                        LOANAPPCDCSEPRTPRCSFEEIND,
                                        LoanAppCDCPrcsFeeAmt,
                                        LoanAppCDCClsCostAmt,
                                        LoanAppCDCUndrwtrFeeAmt,
                                        LoanAppCDCGrossDbentrAmt,
                                        LoanAppBalToBorrAmt,
                                        LoanAppOutPrgrmAreaOfOperInd,
                                        LoanAppRevlMoIntQty,
                                        LoanAppAmortMoIntQty,
                                        LoanAppExtraServFeeAMT,
                                        LoanAppExtraServFeeInd,
                                        RqstSeqNmb,
                                        LoanAppMonthPayroll)
            SELECT v_NEWLoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   LoanAppNm,
                   NULL,                                      --LoanAppRecvDt,
                   SYSDATE,                                  --LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   LoanAppEligEvalInd,
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   LoanCollatInd,
                   LoanAppSourcTypCd,
                   'IP',
                   p_CreatUserId,                         --LoanAppStatUserId,
                   SYSDATE,                                   --LoanAppStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   NULL,
                   RecovInd,
                   LoanAppMicroLendrId,
                   NULL,                                  --LOANDISASTRAPPNMB,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   p_CreatUserId,                        --LoanAppCreatUserId,
                   SYSDATE,                                  --LoanAppCreatDt,
                   p_CreatUserId,                            --LastUpdtUserId,
                   SYSDATE,                                      --LastUpdtDt,
                   LOANAPPNETEARNIND,
                   LOANAPPSERVOFCCD,
                   LoanAppRevlInd,
                   NULL,                                            --LOANNMB,
                   NULL,                                      --LOANAPPFUNDDT,
                   UNDRWRITNGBY,
                   NULL,                                           --COHORTCD,
                   NULL,                                       --FUNDSFISCLYR,
                   LOANTYPIND,
                   LOANAPPINTDTLCD,
                   LOANPYMNINSTLMNTFREQCD,
                   FININSTRMNTTYPIND,
                   LOANAMTLIMEXMPTIND,
                   LoanPymntSchdlFreq,
                   LoanPymtRepayInstlTypCd,
                   LOANGNTYMATSTIND,
                   NULL,                                     --LoanGntyNoteDt,
                   CDCServFeePct,
                   LoanAppContribAmt,
                   LoanAppContribPct,
                   LoanAppCDCGntyAmt,
                   LOANAPPCDCNETDBENTRAMT,
                   LoanAppCDCFundFeeAmt,
                   LOANAPPCDCSEPRTPRCSFEEIND,
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppCDCGrossDbentrAmt,
                   LoanAppBalToBorrAmt,
                   LoanAppOutPrgrmAreaOfOperInd,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   RqstSeqNmb,
                   LoanAppMonthPayroll
              FROM LoanApp.LoanAppTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppProjTbl
        INSERT INTO LoanApp.LoanAppProjTbl (LoanAppNmb,
                                            LoanAppProjStr1Nm,
                                            LoanAppProjStr2Nm,
                                            LoanAppProjCtyNm,
                                            LoanAppProjCntyCd,
                                            LoanAppProjStCd,
                                            LoanAppProjZipCd,
                                            LoanAppProjZip4Cd,
                                            LoanAppRuralUrbanInd,
                                            LoanAppNetExprtAmt,
                                            LoanAppCurrEmpQty,
                                            LoanAppJobCreatQty,
                                            LoanAppJobRtnd,
                                            LoanAppJobRqmtMetInd,
                                            LoanAppCDCJobRat,
                                            LoanAppProjCreatUserId,
                                            LoanAppProjCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            NAICSYrNmb,
                                            NAICSCd,
                                            LoanAppFrnchsCd,
                                            LoanAppFrnchsNm,
                                            LoanAppFrnchsInd,
                                            ADDTNLLOCACQLMTIND,
                                            FIXASSETACQLMTIND,
                                            FIXASSETACQLMTAMT,
                                            COMPSLMTIND,
                                            COMPSLMTAMT,
                                            BULKSALELAWCOMPLYIND,
                                            FRNCHSRECRDACCSIND,
                                            FRNCHSFEEDEFMONNMB,
                                            FRNCHSFEEDEFIND,
                                            FRNCHSTERMNOTCIND,
                                            FRNCHSLENDRSAMEOPPIND,
                                            LOANBUSESTDT,
                                            LOANAPPPROJGEOCD,
                                            LOANAPPHUBZONEIND,
                                            LOANAPPPROJLAT,
                                            LOANAPPPROJLONG,
                                            LoanAppCBRSInd,
                                            LoanAppOppZoneInd,
                                            LOANAPPPROJLMIND,
                                            LOANAPPPROJSIZECD)
            SELECT v_NEWLoanAppNmb,
                   LoanAppProjStr1Nm,
                   LoanAppProjStr2Nm,
                   LoanAppProjCtyNm,
                   LoanAppProjCntyCd,
                   LoanAppProjStCd,
                   LoanAppProjZipCd,
                   LoanAppProjZip4Cd,
                   LoanAppRuralUrbanInd,
                   LoanAppNetExprtAmt,
                   LoanAppCurrEmpQty,
                   LoanAppJobCreatQty,
                   LoanAppJobRtnd,
                   LoanAppJobRqmtMetInd,
                   LoanAppCDCJobRat,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NAICSYrNmb,
                   NAICSCd,
                   LoanAppFrnchsCd,
                   LoanAppFrnchsNm,
                   LoanAppFrnchsInd,
                   ADDTNLLOCACQLMTIND,
                   FIXASSETACQLMTIND,
                   FIXASSETACQLMTAMT,
                   COMPSLMTIND,
                   COMPSLMTAMT,
                   BULKSALELAWCOMPLYIND,
                   FRNCHSRECRDACCSIND,
                   FRNCHSFEEDEFMONNMB,
                   FRNCHSFEEDEFIND,
                   FRNCHSTERMNOTCIND,
                   FRNCHSLENDRSAMEOPPIND,
                   LOANBUSESTDT,
                   LOANAPPPROJGEOCD,
                   LOANAPPHUBZONEIND,
                   LOANAPPPROJLAT,
                   LOANAPPPROJLONG,
                   LoanAppCBRSInd,
                   LoanAppOppZoneInd,
                   LOANAPPPROJLMIND,
                   LOANAPPPROJSIZECD
              FROM LoanApp.LoanAppProjTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanInjctnTbl
        INSERT INTO LoanApp.LoanInjctnTbl (LoanAppNmb,
                                           LoanInjctnSeqNmb,
                                           InjctnTypCd,
                                           LoanInjctnOthDescTxt,
                                           LoanInjctnAmt,
                                           LoanInjctnCreatUserId,
                                           LoanInjctnCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanInjctnFundTermYrNmb)
            SELECT v_NEWLoanAppNmb,
                   LoanInjctnSeqNmb,
                   InjctnTypCd,
                   LoanInjctnOthDescTxt,
                   LoanInjctnAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanInjctnFundTermYrNmb
              FROM LoanApp.LoanInjctnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanStbyAgrmtDtlTBL
        INSERT INTO LoanApp.LoanStbyAgrmtDtlTBL (LOANSTBYAGRMTDTLSEQNMB,
                                                 LOANAPPNMB,
                                                 LOANSTBYCRDTRNM,
                                                 LOANSTBYAMT,
                                                 LOANSTBYREPAYTYPCD,
                                                 LOANSTBYPYMTINTRT,
                                                 LOANSTBYPYMTOTHDESCTXT,
                                                 LOANSTBYPYMTAMT,
                                                 LOANSTBYPYMTSTRTDT,
                                                 CREATUSERID,
                                                 CREATDT,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
            SELECT LOANSTBYAGRMTDTLSEQNMB,
                   v_NEWLoanAppNmb,
                   LOANSTBYCRDTRNM,
                   LOANSTBYAMT,
                   LOANSTBYREPAYTYPCD,
                   LOANSTBYPYMTINTRT,
                   LOANSTBYPYMTOTHDESCTXT,
                   LOANSTBYPYMTAMT,
                   LOANSTBYPYMTSTRTDT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanStbyAgrmtDtlTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanSpcPurpsTbl
        INSERT INTO LoanApp.LoanSpcPurpsTbl (LoanAppNmb,
                                             LoanSpcPurpsSeqNmb,
                                             SpcPurpsLoanCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanSpcPurpsSeqNmb,
                   SpcPurpsLoanCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanSpcPurpsTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppARCRsnTbl
        INSERT INTO LoanApp.LoanAppARCRsnTbl (LoanAppNmb,
                                              ARCLoanRsnCd,
                                              ARCLoanRsnOthDescTxt,
                                              CreateUserId,
                                              CreateDt,
                                              LASTUPDTUSERID,
                                              LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   ARCLoanRsnCd,
                   ARCLoanRsnOthDescTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAppARCRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCrdtUnavRsnTbl
        INSERT INTO LoanApp.LoanCrdtUnavRsnTbl (LoanAppNmb,
                                                LoanCrdtUnavRsnSeqNmb,
                                                LoanCrdtUnavRsnCd,
                                                LoanCrdtUnavRsnTxt,
                                                LoanCrdtUnavRsnCreatUserId,
                                                LoanCrdtUnavRsnCreatDt,
                                                LastUpdtUserId,
                                                LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanCrdtUnavRsnSeqNmb,
                   LoanCrdtUnavRsnCd,
                   LoanCrdtUnavRsnTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanCrdtUnavRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanProcdTbl
        INSERT INTO LoanApp.LoanProcdTbl (LoanAppNmb,
                                          LoanProcdSeqNmb,
                                          LoanProcdTypCd,
                                          ProcdTypCd,
                                          LoanProcdOthTypTxt,
                                          LoanProcdAmt,
                                          LoanProcdCreatUserId,
                                          LoanProcdCreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LoanProcdRefDescTxt,
                                          LoanProcdPurAgrmtDt,
                                          LoanProcdPurIntangAssetAmt,
                                          LoanProcdPurIntangAssetDesc,
                                          LoanProcdPurStkHldrNm,
                                          NCAIncldInd,
                                          StkPurCorpText)
            SELECT v_NEWLoanAppNmb,
                   LoanProcdSeqNmb,
                   LoanProcdTypCd,
                   ProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanApp.LoanProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

   BEGIN
        --Copy to LOANAPP.LOANSUBPROCDTBL
        INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
            SELECT  LOANAPP.LOANSUBPROCDIDSEQ.nextval,
                    v_NEWLoanAppNmb,
                    LoanProcdTypCd,
                    PROCDTYPCD,
                    LOANPROCDTXTBLCK,
                    LoanProcdAmt,
		    LoanProcdAddr,
		    LOANSUBPROCDTYPCD,
		    LoanProcdDesc,	
		    Lender
              FROM LoanApp.LOANSUBPROCDTBL
             WHERE  LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppChngBusOwnrshpTbl
        INSERT INTO LoanApp.LoanAppChngBusOwnrshpTbl (
                        LOANAPPNMB,
                        TOTPYMTAMT,
                        LOAN7APYMTAMT,
                        SELLERFINANFULLSTBYAMT,
                        SELLERFINANNONFULLSTBYAMT,
                        BUYEREQTYCASHAMT,
                        BUYEREQTYCASHDESCTXT,
                        BUYEREQTYBORRAMT,
                        BUYEREQTYOTHAMT,
                        BUYEREQTYOTHDESCTXT,
                        TOTASSETAMT,
                        ACCTRECVASSETAMT,
                        INVTRYASSETAMT,
                        REASSETAMT,
                        REVALTYPIND,
                        EQUIPASSETAMT,
                        EQUIPVALTYPIND,
                        FIXASSETAMT,
                        INTANGASSETAMT,
                        OTHASSETAMT,
                        OTHASSETDESCTXT,
                        COVNTASSETAMT,
                        CUSTASSETAMT,
                        LICNSASSETAMT,
                        FRNCHSASSETAMT,
                        GOODWILLASSETAMT,
                        OTHINTANGASSETAMT,
                        OTHINTANGASSETDESCTXT,
                        TOTAPPRAMT,
                        BUSAPPRNM,
                        BUSAPPRFEEAMT,
                        BUSAPPRASAIND,
                        BUSAPPRCBAIND,
                        BUSAPPRABVIND,
                        BUSAPPRCVAIND,
                        BUSAPPRAVAIND,
                        BUSAPPRCPAIND,
                        BUSBRKRCOMISNIND,
                        BUSBRKRNM,
                        BUSBRKRCOMISNAMT,
                        BUSBRKRADR,
                        CREATUSERID,
                        CREATDT,
                        LASTUPDTUSERID,
                        LASTUPDTDT,
                        BUSAPPRLIVIND)
            SELECT v_NEWLoanAppNmb,
                   TOTPYMTAMT,
                   LOAN7APYMTAMT,
                   SELLERFINANFULLSTBYAMT,
                   SELLERFINANNONFULLSTBYAMT,
                   BUYEREQTYCASHAMT,
                   BUYEREQTYCASHDESCTXT,
                   BUYEREQTYBORRAMT,
                   BUYEREQTYOTHAMT,
                   BUYEREQTYOTHDESCTXT,
                   TOTASSETAMT,
                   ACCTRECVASSETAMT,
                   INVTRYASSETAMT,
                   REASSETAMT,
                   REVALTYPIND,
                   EQUIPASSETAMT,
                   EQUIPVALTYPIND,
                   FIXASSETAMT,
                   INTANGASSETAMT,
                   OTHASSETAMT,
                   OTHASSETDESCTXT,
                   COVNTASSETAMT,
                   CUSTASSETAMT,
                   LICNSASSETAMT,
                   FRNCHSASSETAMT,
                   GOODWILLASSETAMT,
                   OTHINTANGASSETAMT,
                   OTHINTANGASSETDESCTXT,
                   TOTAPPRAMT,
                   BUSAPPRNM,
                   BUSAPPRFEEAMT,
                   BUSAPPRASAIND,
                   BUSAPPRCBAIND,
                   BUSAPPRABVIND,
                   BUSAPPRCVAIND,
                   BUSAPPRAVAIND,
                   BUSAPPRCPAIND,
                   BUSBRKRCOMISNIND,
                   BUSBRKRNM,
                   BUSBRKRCOMISNAMT,
                   BUSBRKRADR,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   BUSAPPRLIVIND
              FROM LoanApp.LoanAppChngBusOwnrshpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPrtCmntTbl
        INSERT INTO LoanApp.LoanPrtCmntTbl (LoanAppNmb,
                                            LoanPrtCmntTxt,
                                            LoanPrtCmntCreatUserId,
                                            LoanPrtCmntCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanPrtCmntTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM loanApp.LoanPrtCmntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanEconDevObjctChldTbl
        INSERT INTO LoanApp.LoanEconDevObjctChldTbl (LoanEconDevObjctSeqNmb,
                                                     LoanAppNmb,
                                                     LoanEconDevObjctCd,
                                                     CreatUserId,
                                                     CreatDt,
                                                     LastUpdtUserId,
                                                     LastUpdtDt)
            SELECT LoanEconDevObjctSeqNmb,
                   v_NEWLoanAppNmb,
                   LoanEconDevObjctCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanEconDevObjctChldTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCollatTbl
        INSERT INTO LoanApp.LoanCollatTbl (loanappnmb,
                                           loancollatseqnmb,
                                           loancollattypcd,
                                           loancollatdesc,
                                           loancollatmrktvalamt,
                                           loancollatownrrecrd,
                                           loancollatsbalienpos,
                                           loancollatvalsourccd,
                                           loancollatvaldt,
                                           loancollatcreatuserid,
                                           loancollatcreatdt,
                                           lastupdtuserid,
                                           lastupdtdt,
                                           collatstatcd,
                                           collatsecsrchorddt,
                                           collatsecsrchrcvddt,
                                           collatapporddt,
                                           collatstr1nm,
                                           collatstr2nm,
                                           collatctynm,
                                           collatcntycd,
                                           collatstcd,
                                           collatzipcd,
                                           collatzip4cd,
                                           LoanCollatSubTypCd,
                                           SecurShrPariPassuInd,
                                           SecurShrPariPassuNonSBAInd,
                                           SecurPariPassuLendrNm,
                                           SecurPariPassuAmt,
                                           SecurLienLimAmt,
                                           SecurInstrmntTypCd,
                                           SecurWaterRightInd,
                                           SecurRentAsgnInd,
                                           SecurSellerNm,
                                           SecurPurNm,
                                           SecurOwedToSellerAmt,
                                           SecurCDCDeedInEscrowInd,
                                           SecurSellerIntDtlInd,
                                           SecurSellerIntDtlTxt,
                                           SecurTitlSubjPriorLienInd,
                                           SecurPriorLienTxt,
                                           SecurPriorLienLimAmt,
                                           SecurSubjPriorAsgnInd,
                                           SecurPriorAsgnTxt,
                                           SecurPriorAsgnLimAmt,
                                           SecurALTATltlInsurInd,
                                           SecurLeaseTermOverLoanYrNmb,
                                           LandlordIntProtWaiverInd,
                                           SecurDt,
                                           SecurLessorTermNotcDaysNmb,
                                           SecurDescTxt,
                                           SecurPropAcqWthLoanInd,
                                           SecurPropTypTxt,
                                           SecurOthPropTxt,
                                           SecurLienOnLqorLicnsInd,
                                           SecurMadeYrNmb,
                                           SecurLocTxt,
                                           SecurOwnerNm,
                                           SecurMakeNm,
                                           SecurAmt,
                                           SecurNoteSecurInd,
                                           SecurStkShrNmb,
                                           SecurLienHldrVrfyInd,
                                           SecurTitlVrfyTypCd,
                                           SecurTitlVrfyOthTxt,
                                           FloodInsurRqrdInd,
                                           REHazardInsurRqrdInd,
                                           PerHazardInsurRqrdInd,
                                           FullMarInsurRqrdInd,
                                           EnvInvstgtSBAAppInd,
                                           LeasePrmTypCd,
                                           ApprTypCd)
            SELECT v_NEWLoanAppNmb,
                   loancollatseqnmb,
                   loancollattypcd,
                   loancollatdesc,
                   loancollatmrktvalamt,
                   loancollatownrrecrd,
                   loancollatsbalienpos,
                   loancollatvalsourccd,
                   loancollatvaldt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   collatstatcd,
                   collatsecsrchorddt,
                   collatsecsrchrcvddt,
                   collatapporddt,
                   collatstr1nm,
                   collatstr2nm,
                   collatctynm,
                   collatcntycd,
                   collatstcd,
                   collatzipcd,
                   collatzip4cd,
                   LoanCollatSubTypCd,
                   SecurShrPariPassuInd,
                   SecurShrPariPassuNonSBAInd,
                   SecurPariPassuLendrNm,
                   SecurPariPassuAmt,
                   SecurLienLimAmt,
                   SecurInstrmntTypCd,
                   SecurWaterRightInd,
                   SecurRentAsgnInd,
                   SecurSellerNm,
                   SecurPurNm,
                   SecurOwedToSellerAmt,
                   SecurCDCDeedInEscrowInd,
                   SecurSellerIntDtlInd,
                   SecurSellerIntDtlTxt,
                   SecurTitlSubjPriorLienInd,
                   SecurPriorLienTxt,
                   SecurPriorLienLimAmt,
                   SecurSubjPriorAsgnInd,
                   SecurPriorAsgnTxt,
                   SecurPriorAsgnLimAmt,
                   SecurALTATltlInsurInd,
                   SecurLeaseTermOverLoanYrNmb,
                   LandlordIntProtWaiverInd,
                   SecurDt,
                   SecurLessorTermNotcDaysNmb,
                   SecurDescTxt,
                   SecurPropAcqWthLoanInd,
                   SecurPropTypTxt,
                   SecurOthPropTxt,
                   SecurLienOnLqorLicnsInd,
                   SecurMadeYrNmb,
                   SecurLocTxt,
                   SecurOwnerNm,
                   SecurMakeNm,
                   SecurAmt,
                   SecurNoteSecurInd,
                   SecurStkShrNmb,
                   SecurLienHldrVrfyInd,
                   SecurTitlVrfyTypCd,
                   SecurTitlVrfyOthTxt,
                   FloodInsurRqrdInd,
                   REHazardInsurRqrdInd,
                   PerHazardInsurRqrdInd,
                   FullMarInsurRqrdInd,
                   EnvInvstgtSBAAppInd,
                   LeasePrmTypCd,
                   ApprTypCd
              FROM LoanApp.LoanCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

        --Copy to LoanApp.LoanCollatLienTbl
        INSERT INTO LoanApp.LoanCollatLienTbl (LoanAppNmb,
                                               LoanCollatSeqNmb,
                                               LoanCollatLienSeqNmb,
                                               LoanCollatLienHldrNm,
                                               LoanCollatLienBalAmt,
                                               LoanCollatLienPos,
                                               LoanCollatLienCreatUserId,
                                               LoanCollatLienCreatDt,
                                               LastUpdtUserId,
                                               LastUpdtDt,
                                               LOANCOLLATLIENSTATCD,
                                               LOANCOLLATLIENCMNT)
            SELECT v_NEWLoanAppNmb,
                   LoanCollatSeqNmb,
                   LoanCollatLienSeqNmb,
                   LoanCollatLienHldrNm,
                   LoanCollatLienBalAmt,
                   LoanCollatLienPos,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANCOLLATLIENSTATCD,
                   LOANCOLLATLIENCMNT
              FROM LoanCollatLienTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPartLendrTbl
        INSERT INTO LoanApp.LoanPartLendrTbl (LoanAppNmb,
                                              LoanPartLendrSeqNmb,
                                              LoanPartLendrTypCd,
                                              LoanPartLendrAmt,
                                              LocId,
                                              LoanPartLendrNm,
                                              LoanPartLendrStr1Nm,
                                              LoanPartLendrStr2Nm,
                                              LoanPartLendrCtyNm,
                                              LoanPartLendrStCd,
                                              LoanPartLendrZip5Cd,
                                              LoanPartLendrZip4Cd,
                                              LoanPartLendrOfcrLastNm,
                                              LoanPartLendrOfcrFirstNm,
                                              LoanPartLendrOfcrInitialNm,
                                              LoanPartLendrOfcrSfxNm,
                                              LoanPartLendrOfcrTitlTxt,
                                              LoanPartLendrOfcrPhnNmb,
                                              LoanLienPosCd,
                                              LoanPartLendrSrLienPosFeeAmt,
                                              LoanPoolAppNmb,
                                              LOANLENDRTAXID,
                                              LOANLENDRSERVFEEPCT,
                                              LOANLENDRPARTPCT,
                                              LOANORIGPARTPCT,
                                              LoanSBAGntyBalPct,
                                              LOANLENDRPARTAMT,
                                              LOANORIGPARTAMT,
                                              LOANLENDRGROSSAMT,
                                              LOANSBAGNTYBALAMT,
                                              Loan504PartLendrSeqNmb,
                                              LoanPartLendrCreatUserId,
                                              LoanPartLendrCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              loanpartlendrcntrycd,
                                              loanpartlendrpostcd,
                                              loanpartlendrstnm)
            SELECT v_NEWLoanAppNmb,
                   LoanPartLendrSeqNmb,
                   LoanPartLendrTypCd,
                   LoanPartLendrAmt,
                   LocId,
                   LoanPartLendrNm,
                   LoanPartLendrStr1Nm,
                   LoanPartLendrStr2Nm,
                   LoanPartLendrCtyNm,
                   LoanPartLendrStCd,
                   LoanPartLendrZip5Cd,
                   LoanPartLendrZip4Cd,
                   LoanPartLendrOfcrLastNm,
                   LoanPartLendrOfcrFirstNm,
                   LoanPartLendrOfcrInitialNm,
                   LoanPartLendrOfcrSfxNm,
                   LoanPartLendrOfcrTitlTxt,
                   LoanPartLendrOfcrPhnNmb,
                   LoanLienPosCd,
                   LoanPartLendrSrLienPosFeeAmt,
                   LoanPoolAppNmb,
                   LOANLENDRTAXID,
                   LOANLENDRSERVFEEPCT,
                   LOANLENDRPARTPCT,
                   LOANORIGPARTPCT,
                   LoanSBAGntyBalPct,
                   LOANLENDRPARTAMT,
                   LOANORIGPARTAMT,
                   LOANLENDRGROSSAMT,
                   LOANSBAGNTYBALAMT,
                   Loan504PartLendrSeqNmb,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   loanpartlendrcntrycd,
                   loanpartlendrpostcd,
                   loanpartlendrstnm
              FROM LoanApp.LoanPartLendrTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    --Copy to LoanApp.LoanBusTbl
    INSERT INTO LoanApp.LoanBusTbl (LoanAppNmb,
                                    TaxId,
                                    BusEINCertInd,
                                    BusTypCd,
                                    BusNm,
                                    BusSrchNm,
                                    BusTrdNm,
                                    BusBnkrptInd,
                                    BusLwsuitInd,
                                    BusPriorSBALoanInd,
                                    BusCurBnkNm,
                                    BusChkngBalAmt,
                                    BusCurOwnrshpDt,
                                    BusPrimPhnNmb,
                                    BusPhyAddrStr1Nm,
                                    BusPhyAddrStr2Nm,
                                    BusPhyAddrCtyNm,
                                    BusPhyAddrStCd,
                                    BusPhyAddrStNm,
                                    BusPhyAddrCntCd,
                                    BusPhyAddrZipCd,
                                    BusPhyAddrZip4Cd,
                                    BusPhyAddrPostCd,
                                    BusMailAddrStr1Nm,
                                    BusMailAddrStr2Nm,
                                    BusMailAddrCtyNm,
                                    BusMailAddrStCd,
                                    BusMailAddrStNm,
                                    BusMailAddrCntCd,
                                    BusMailAddrZipCd,
                                    BusMailAddrZip4Cd,
                                    BusMailAddrPostCd,
                                    BusCreatUserId,
                                    BusCreatDt,
                                    BusExprtInd,
                                    BusExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    BusExtrnlCrdtScorNmb,
                                    BusExtrnlCrdtScorDt,
                                    BusDunsNMB,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    BusAltPhnNmb,
                                    BusPrimEmailAdr,
                                    BusAltEmailAdr,
                                    BusCSP60DayDelnqInd,
                                    BusLglActnInd,
                                    BusFedAgncySDPIEInd,
                                    BusSexNatrInd,
                                    BusNonFmrSBAEmpInd,
                                    BusNonLegBrnchEmpInd,
                                    BusNonFedEmpInd,
                                    BusNonGS13EmpInd,
                                    BusNonSBACEmpInd,
                                    BusPrimCntctNm,
                                    -- BusPrimCntctEmail,
                                    BusFedAgncyLoanInd,
                                    BusOutDbtInd)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.BusEINCertInd, a.BusEINCertInd),
               NVL (b.BusTypCd, a.BusTypCd),
               NVL (b.BusNm, a.BusNm),
               NVL (b.BusSrchNm, a.BusSrchNm),
               a.BusTrdNm,
               a.BusBnkrptInd,
               a.BusLwsuitInd,
               a.BusPriorSBALoanInd,
               a.BusCurBnkNm,
               a.BusChkngBalAmt,
               a.BusCurOwnrshpDt,
               a.BusPrimPhnNmb,
               a.BusPhyAddrStr1Nm,
               a.BusPhyAddrStr2Nm,
               a.BusPhyAddrCtyNm,
               a.BusPhyAddrStCd,
               a.BusPhyAddrStNm,
               a.BusPhyAddrCntCd,
               a.BusPhyAddrZipCd,
               a.BusPhyAddrZip4Cd,
               a.BusPhyAddrPostCd,
               a.BusMailAddrStr1Nm,
               a.BusMailAddrStr2Nm,
               a.BusMailAddrCtyNm,
               a.BusMailAddrStCd,
               a.BusMailAddrStNm,
               a.BusMailAddrCntCd,
               a.BusMailAddrZipCd,
               a.BusMailAddrZip4Cd,
               a.BusMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.BusExprtInd,
               a.BusExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.BusExtrnlCrdtScorNmb,
               a.BusExtrnlCrdtScorDt,
               a.BusDunsNMB,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               b.VETCERTIND,
               a.BusAltPhnNmb,
               a.BusPrimEmailAdr,
               a.BusAltEmailAdr,
               a.BusCSP60DayDelnqInd,
               a.BusLglActnInd,
               a.BusFedAgncySDPIEInd,
               a.BusSexNatrInd,
               a.BusNonFmrSBAEmpInd,
               a.BusNonLegBrnchEmpInd,
               a.BusNonFedEmpInd,
               a.BusNonGS13EmpInd,
               a.BusNonSBACEmpInd,
               a.BusPrimCntctNm,
               --   BusPrimCntctEmail,
               a.BusFedAgncyLoanInd,
               a.BusOutDbtInd
          FROM LoanApp.LoanBusTbl a, loan.Bustbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.taxid = b.taxid(+);


    --Copy to LoanApp.LoanPerTbl
    INSERT INTO LoanApp.LoanPerTbl (LoanAppNmb,
                                    TaxId,
                                    PerTAXIDCertInd,
                                    PerFirstNm,
                                    PerFirstSrchNm,
                                    PerLastNm,
                                    PerLastSrchNm,
                                    PerInitialNm,
                                    PerSfxNm,
                                    PerTitlTxt,
                                    PerBrthDt,
                                    PerBrthCtyNm,
                                    PerBrthStCd,
                                    PerBrthCntCd,
                                    PerBrthCntNm,
                                    PerUSCitznInd,
                                    PerAlienRgstrtnNmb,
                                    PerPndngLwsuitInd,
                                    PerAffilEmpFedInd,
                                    PerIOBInd,
                                    PerIndctPrleProbatnInd,
                                    PerCrmnlOffnsInd,
                                    PerCnvctInd,
                                    PerBnkrptcyInd,
                                    PerFngrprntWaivDt,
                                    PerPrimPhnNmb,
                                    PerPhyAddrStr1Nm,
                                    PerPhyAddrStr2Nm,
                                    PerPhyAddrCtyNm,
                                    PerPhyAddrStCd,
                                    PerPhyAddrStNm,
                                    PerPhyAddrCntCd,
                                    PerPhyAddrZipCd,
                                    PerPhyAddrZip4Cd,
                                    PerPhyAddrPostCd,
                                    PerMailAddrStr1Nm,
                                    PerMailAddrStr2Nm,
                                    PerMailAddrCtyNm,
                                    PerMailAddrStCd,
                                    PerMailAddrStNm,
                                    PerMailAddrCntCd,
                                    PerMailAddrZipCd,
                                    PerMailAddrZip4Cd,
                                    PerMailAddrPostCd,
                                    PerCreatUserId,
                                    PerCreatDt,
                                    PerExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    PerExtrnlCrdtScorNmb,
                                    PerExtrnlCrdtScorDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    PerFedAgncySDPIEInd,
                                    PerCSP60DayDelnqInd,
                                    PerLglActnInd,
                                    PerCitznShpCntNm,
                                    PerAltPhnNmb,
                                    PerPrimEmailAdr,
                                    PerAltEmailAdr,
                                    PerFedAgncyLoanInd,
                                    PerOthrCmntTxt)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.PerTAXIDCertInd, a.PerTAXIDCertInd),
               NVL (b.PerFirstNm, a.PerFirstNm),
               NVL (b.PerFirstSrchNm, a.PerFirstSrchNm),
               NVL (b.PerLastNm, a.PerLastNm),
               NVL (b.PerLastSrchNm, a.PerLastSrchNm),
               NVL (b.PerInitialNm, a.PerInitialNm),
               NVL (b.PerSfxNm, a.PerSfxNm),
               a.PerTitlTxt,
               NVL (b.PerBrthDt, a.PerBrthDt),
               NVL (b.PerBrthCtyNm, a.PerBrthCtyNm),
               NVL (b.PerBrthStCd, a.PerBrthStCd),
               NVL (b.PerBrthCntCd, a.PerBrthCntCd),
               NVL (b.PerBrthCntNm, b.PerBrthCntNm),
               a.PerUSCitznInd,
               a.PerAlienRgstrtnNmb,
               a.PerPndngLwsuitInd,
               a.PerAffilEmpFedInd,
               a.PerIOBInd,
               a.PerIndctPrleProbatnInd,
               a.PerCrmnlOffnsInd,
               a.PerCnvctInd,
               a.PerBnkrptcyInd,
               a.PerFngrprntWaivDt,
               a.PerPrimPhnNmb,
               a.PerPhyAddrStr1Nm,
               a.PerPhyAddrStr2Nm,
               a.PerPhyAddrCtyNm,
               a.PerPhyAddrStCd,
               a.PerPhyAddrStNm,
               a.PerPhyAddrCntCd,
               a.PerPhyAddrZipCd,
               a.PerPhyAddrZip4Cd,
               a.PerPhyAddrPostCd,
               a.PerMailAddrStr1Nm,
               a.PerMailAddrStr2Nm,
               a.PerMailAddrCtyNm,
               a.PerMailAddrStCd,
               a.PerMailAddrStNm,
               a.PerMailAddrCntCd,
               a.PerMailAddrZipCd,
               a.PerMailAddrZip4Cd,
               a.PerMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.PerExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.PerExtrnlCrdtScorNmb,
               a.PerExtrnlCrdtScorDt,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (b.VETCERTIND, a.VETCERTIND),
               a.PerFedAgncySDPIEInd,
               a.PerCSP60DayDelnqInd,
               a.PerLglActnInd,
               a.PerCitznShpCntNm,
               a.PerAltPhnNmb,
               a.PerPrimEmailAdr,
               a.PerAltEmailAdr,
               a.PerFedAgncyLoanInd,
               a.PerOthrCmntTxt
          FROM loanapp.loanpertbl a, loan.pertbl b
         WHERE a.taxid = b.taxid(+) AND a.loanappnmb = p_loanappnmb;


    BEGIN
        --Copy to LoanApp.LoanIndbtnesTbl
        INSERT INTO LoanApp.LoanIndbtnesTbl (LoanAppNmb,
                                             TaxId,
                                             LoanIndbtnesSeqNmb,
                                             LoanIndbtnesPayblToNm,
                                             LoanIndbtnesPurpsTxt,
                                             LoanIndbtnesOrglDt,
                                             LoanIndbtnesCurBalAmt,
                                             LoanIndbtnesIntPct,
                                             LoanIndbtnesMatDt,
                                             LoanIndbtnesPymtAmt,
                                             LoanIndbtnesPymtFreqCd,
                                             LoanIndbtnesCollatDescTxt,
                                             LoanIndbtnesPrevFinanStatCd,
                                             LoanIndbtnesCreatUserId,
                                             LoanIndbtnesCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanIndbtnesSeqNmb,
                   LoanIndbtnesPayblToNm,
                   LoanIndbtnesPurpsTxt,
                   LoanIndbtnesOrglDt,
                   LoanIndbtnesCurBalAmt,
                   LoanIndbtnesIntPct,
                   LoanIndbtnesMatDt,
                   LoanIndbtnesPymtAmt,
                   LoanIndbtnesPymtFreqCd,
                   LoanIndbtnesCollatDescTxt,
                   LoanIndbtnesPrevFinanStatCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanIndbtnesTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPerFinanTbl
        INSERT INTO LoanApp.LoanPerFinanTbl (LoanAppNmb,
                                             TaxId,
                                             PerLqdAssetAmt,
                                             PerBusOwnrshpAmt,
                                             PerREAmt,
                                             PerOthAssetAmt,
                                             PerTotAssetAmt,
                                             PerRELiabAmt,
                                             PerCCDbtAmt,
                                             PerInstlDbtAmt,
                                             PerOthLiabAmt,
                                             PerTotLiabAmt,
                                             PerNetWrthAmt,
                                             PerAnnSalaryAmt,
                                             PerOthAnnIncAmt,
                                             PerSourcOfOthIncTxt,
                                             PerResOwnRentOthInd,
                                             PerMoHsngAmt,
                                             PerCreatUserId,
                                             PerCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   PerLqdAssetAmt,
                   PerBusOwnrshpAmt,
                   PerREAmt,
                   PerOthAssetAmt,
                   PerTotAssetAmt,
                   PerRELiabAmt,
                   PerCCDbtAmt,
                   PerInstlDbtAmt,
                   PerOthLiabAmt,
                   PerTotLiabAmt,
                   PerNetWrthAmt,
                   PerAnnSalaryAmt,
                   PerOthAnnIncAmt,
                   PerSourcOfOthIncTxt,
                   PerResOwnRentOthInd,
                   PerMoHsngAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPerFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanFedEmpTbl
        INSERT INTO LOANAPP.LoanFedEmpTbl (LoanAppNmb,
                                           TaxId,
                                           LoanFedEmpSeqNmb,
                                           FedEmpFirstNm,
                                           FedEmpLastNm,
                                           FedEmpInitialNm,
                                           FedEmpSfxNm,
                                           FedEmpAddrStr1Nm,
                                           FedEmpAddrStr2Nm,
                                           FedEmpAddrCtyNm,
                                           FedEmpAddrStCd,
                                           FedEmpAddrZipCd,
                                           FedEmpAddrZip4Cd,
                                           FedEmpAgncyOfc,
                                           FedEmpCreatUserId,
                                           FedEmpCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanFedEmpSeqNmb,
                   FedEmpFirstNm,
                   FedEmpLastNm,
                   FedEmpInitialNm,
                   FedEmpSfxNm,
                   FedEmpAddrStr1Nm,
                   FedEmpAddrStr2Nm,
                   FedEmpAddrCtyNm,
                   FedEmpAddrStCd,
                   FedEmpAddrZipCd,
                   FedEmpAddrZip4Cd,
                   FedEmpAgncyOfc,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanFedEmpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.Pertbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'P'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.bustbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'B'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;



    BEGIN
        --Copy to LoanApp.LoanPrinTbl */
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.pertbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'P'
                   AND a.taxid = b.Taxid(+);
    END;

    BEGIN
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.bustbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'B'
                   AND a.taxid = b.Taxid(+);
    END;



    BEGIN
        --Copy to LoanApp.LoanBusPrinTbl
        INSERT INTO LoanApp.LoanBusPrinTbl (LoanAppNmb,
                                            BorrSeqNmb,
                                            PrinSeqNmb,
                                            LoanBusPrinPctOwnrshpBus,
                                            LoanPrinGntyInd,
                                            LoanBusPrinCreatUserId,
                                            LoanBusPrinCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            LoanCntlIntInd,
                                            LoanCntlIntTyp)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   PrinSeqNmb,
                   LoanBusPrinPctOwnrshpBus,
                   LoanPrinGntyInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanCntlIntInd,
                   LoanCntlIntTyp
              FROM LoanApp.LoanBusPrinTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.perracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'P'
               AND a.taxid = b.taxid;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.busracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'B'
               AND a.taxid = b.taxid;

    --Copy to LoanApp.LoanPrinRaceTbl
    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               RaceCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinRaceTbl a
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND NOT EXISTS
                       (SELECT 1
                          FROM loanapp.LoanPrinRaceTbl z
                         WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                               AND z.PRINSEQNMB = a.PrinSeqNmb);


    BEGIN
        --Copy to LoanApp.LoanBorrRaceTbl
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrTbl a, loan.perracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'P'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanborrTbl a, loan.busracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'B'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrRaceTbl a
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND NOT EXISTS
                           (SELECT 1
                              FROM loanapp.LoanBorrRaceTbl z
                             WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                                   AND z.BorrSEQNMB = a.BorrSeqNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanPrevFinanTbl
        INSERT INTO LoanApp.LoanPrevFinanTbl (LoanAppNmb,
                                              BorrSeqNmb,
                                              LoanPrevFinanSeqNmb,
                                              LoanPrevFinanStatCd,
                                              LoanPrevFinanAgencyNm,
                                              LoanPrevFinanLoanNmb,
                                              LoanPrevFinanAppvDt,
                                              LoanPrevFinanTotAmt,
                                              LoanPrevFinanBalAmt,
                                              LoanPrevFinanCreatUserId,
                                              LoanPrevFinanCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              LoanPrevFinanDelnqCmntTxt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   LoanPrevFinanSeqNmb,
                   LoanPrevFinanStatCd,
                   LoanPrevFinanAgencyNm,
                   LoanPrevFinanLoanNmb,
                   LoanPrevFinanAppvDt,
                   LoanPrevFinanTotAmt,
                   LoanPrevFinanBalAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanPrevFinanDelnqCmntTxt
              FROM LoanApp.LoanPrevFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanGuarTbl
        INSERT INTO LoanApp.LoanGuarTbl (LoanAppNmb,
                                         GuarSeqNmb,
                                         TaxId,
                                         GuarBusPerInd,
                                         LoanGuarOperCoInd,
                                         GuarCreatUserId,
                                         GuarCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         LACGntyCondTypCd,
                                         GntySubCondTypCd,
                                         GntyLmtAmt,
                                         GntyLmtPct,
                                         LoanCollatSeqNmb,
                                         GntyLmtYrNmb,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdInd,
                                         WorkrsCompInsRqrdInd,
                                         OthInsurDescTxt)
            SELECT v_NEWLoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LACGntyCondTypCd,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdInd,
                   WorkrsCompInsRqrdInd,
                   OthInsurDescTxt
              FROM LoanApp.LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanLmtGntyCollatTbl
        INSERT INTO LoanApp.LoanLmtGntyCollatTbl (LOANAPPNMB,
                                                  TAXID,
                                                  BUSPERIND,
                                                  LOANCOLLATSEQNMB,
                                                  CREATUSERID,
                                                  CREATDT,
                                                  LASTUPDTUSERID,
                                                  LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   TAXID,
                   BUSPERIND,
                   LOANCOLLATSEQNMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanLmtGntyCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanIntDtlTbl
        INSERT INTO LoanApp.LoanIntDtlTbl (LOANAPPNMB,
                                           LOANINTDTLSEQNMB,
                                           LOANINTLOANPARTPCT,
                                           LOANINTLOANPARTMONMB,
                                           LOANINTFIXVARIND,
                                           LOANINTRT,
                                           IMRTTYPCD,
                                           LOANINTBASERT,
                                           LOANINTSPRDOVRPCT,
                                           LOANINTADJPRDCD,
                                           LOANINTADJPRDMONMB,
                                           CREATUSERID,
                                           CREATDT,
                                           LASTUPDTUSERID,
                                           LASTUPDTDT,
                                           LOANINTADJPRDEFFDT,
                                           LoanIntDtlGuarInd)
            SELECT v_NEWLoanAppNmb,
                   LOANINTDTLSEQNMB,
                   LOANINTLOANPARTPCT,
                   LOANINTLOANPARTMONMB,
                   LOANINTFIXVARIND,
                   LOANINTRT,
                   IMRTTYPCD,
                   LOANINTBASERT,
                   LOANINTSPRDOVRPCT,
                   LOANINTADJPRDCD,
                   LOANINTADJPRDMONMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANINTADJPRDEFFDT,
                   LoanIntDtlGuarInd
              FROM LoanApp.LoanIntDtlTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanExprtCntryTbl
        INSERT INTO LoanApp.LoanExprtCntryTbl (LOANAPPNMB,
                                               LOANEXPRTCNTRYCD,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT,
                                               LOANEXPRTCNTRYSEQNMB)
            SELECT v_NEWLoanAppNmb,
                   LOANEXPRTCNTRYCD,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANEXPRTCNTRYSEQNMB
              FROM LoanApp.LoanExprtCntryTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBSTbl
        INSERT INTO LoanApp.LoanBSTbl (LoanAppNmb,
                                       LoanBSDt,
                                       LoanBSCashEqvlntAmt,
                                       LoanBSNetTrdRecvAmt,
                                       LoanBSTotInvtryAmt,
                                       LoanBSOthCurAssetAmt,
                                       LoanBSTotCurAssetAmt,
                                       LoanBSTotFixAssetAmt,
                                       LoanBSTotOthAssetAmt,
                                       LoanBSTotAssetAmt,
                                       LoanBSAcctsPayblAmt,
                                       LoanBSCurLTDAmt,
                                       LoanBSOthCurLiabAmt,
                                       LoanBSTotCurLiabAmt,
                                       LoanBSLTDAmt,
                                       LoanBSOthLTLiabAmt,
                                       LoanBSStbyDbt,
                                       LoanBSTotLiab,
                                       LoanBSBusNetWrth,
                                       LoanBSTngblNetWrth,
                                       LoanBSActlPrfrmaInd,
                                       LoanFinanclStmtSourcCd,
                                       LoanBSCreatUserId,
                                       LoanBSCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanBSDt,
                   LoanBSCashEqvlntAmt,
                   LoanBSNetTrdRecvAmt,
                   LoanBSTotInvtryAmt,
                   LoanBSOthCurAssetAmt,
                   LoanBSTotCurAssetAmt,
                   LoanBSTotFixAssetAmt,
                   LoanBSTotOthAssetAmt,
                   LoanBSTotAssetAmt,
                   LoanBSAcctsPayblAmt,
                   LoanBSCurLTDAmt,
                   LoanBSOthCurLiabAmt,
                   LoanBSTotCurLiabAmt,
                   LoanBSLTDAmt,
                   LoanBSOthLTLiabAmt,
                   LoanBSStbyDbt,
                   LoanBSTotLiab,
                   LoanBSBusNetWrth,
                   LoanBSTngblNetWrth,
                   LoanBSActlPrfrmaInd,
                   LoanFinanclStmtSourcCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBSTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanISTbl
        INSERT INTO LoanApp.LoanISTbl (LoanAppNmb,
                                       LoanISSeqNmb,
                                       LoanISNetSalesRevnuAmt,
                                       LoanISCostSalesAmt,
                                       LoanISGrsProftAmt,
                                       LoanISOwnrSalaryAmt,
                                       LoanISDprctAmortAmt,
                                       LoanISNetIncAmt,
                                       LoanISOperProftAmt,
                                       LoanISAnnIntExpnAmt,
                                       LoanISNetIncBefTaxWthdrlAmt,
                                       LoanISIncTaxAmt,
                                       LoanISCshflwAmt,
                                       LoanFinanclStmtSourcCd,
                                       LoanISBegnDt,
                                       LoanISEndDt,
                                       LoanISCreatUserId,
                                       LoanISCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanISSeqNmb,
                   LoanISNetSalesRevnuAmt,
                   LoanISCostSalesAmt,
                   LoanISGrsProftAmt,
                   LoanISOwnrSalaryAmt,
                   LoanISDprctAmortAmt,
                   LoanISNetIncAmt,
                   LoanISOperProftAmt,
                   LoanISAnnIntExpnAmt,
                   LoanISNetIncBefTaxWthdrlAmt,
                   LoanISIncTaxAmt,
                   LoanISCshflwAmt,
                   LoanFinanclStmtSourcCd,
                   LoanISBegnDt,
                   LoanISEndDt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanISTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppPrtTbl
        INSERT INTO LOANAPP.LoanAppPrtTbl (LoanAppNmb,
                                           LocId,
                                           LoanAppFIRSNmb,
                                           PrtId,
                                           LoanAppPrtAppNmb,
                                           LoanAppPrtLoanNmb,
                                           LoanAppPrtNm,
                                           LoanAppPrtStr1,
                                           LoanAppPrtStr2,
                                           LoanAppPrtCtyNm,
                                           LoanAppPrtStCd,
                                           LoanAppPrtZipCd,
                                           LoanAppPrtZip4Cd,
                                           LoanAppCntctLastNm,
                                           LoanAppCntctFirstNm,
                                           LoanAppCntctInitialNm,
                                           LoanAppCntctSfxNm,
                                           LoanAppCntctTitlTxt,
                                           LoanAppCntctPhn,
                                           LoanAppCntctFax,
                                           LoanAppCntctemail,
                                           LoanPckgSourcTypCd,
                                           LoanAppPckgSourcNm,
                                           LoanAppPckgSourcStr1Nm,
                                           LoanAppPckgSourcStr2Nm,
                                           LoanAppPckgSourcCtyNm,
                                           LoanAppPckgSourcStCd,
                                           LoanAppPckgSourcZipCd,
                                           LoanAppPckgSourcZip4Cd,
                                           ACHRtngNmb,
                                           ACHAcctNmb,
                                           ACHAcctTypCd,
                                           ACHTinNmb,
                                           LoanAppPrtTaxId,
                                           LoanAppLSPHQLocId,
                                           LoanAppPrtCreatUserId,
                                           LoanAppPrtCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanAgntInvlvdInd,
                                           --    LoanAppLspLastNm,
                                           --     LoanAppLspFirstNm,
                                           --     LoanAppLspInitialNm,
                                           LoanAppLspSfxNm,
                                           --    LoanAppLspTitlTxt,
                                           --    LoanAppLspPhn,
                                           LoanAppLspFax,
                                           --    LoanAppLspEMail,
                                           LoanPrtCntctCellPhn,
                                           LoanPrtAltCntctFirstNm,
                                           LoanPrtAltCntctLastNm,
                                           LoanPrtAltCntctInitialNm,
                                           LoanPrtAltCntctTitlTxt,
                                           LoanPrtAltCntctPrimPhn,
                                           LoanPrtAltCntctCellPhn,
                                           LoanPrtAltCntctemail,
                                           LendrAltCntctTypCd)
            SELECT v_NEWLoanAppNmb,
                   LocId,
                   LoanAppFIRSNmb,
                   PrtId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   LoanAppPrtNm,
                   LoanAppPrtStr1,
                   LoanAppPrtStr2,
                   LoanAppPrtCtyNm,
                   LoanAppPrtStCd,
                   LoanAppPrtZipCd,
                   LoanAppPrtZip4Cd,
                   LoanAppCntctLastNm,
                   LoanAppCntctFirstNm,
                   LoanAppCntctInitialNm,
                   LoanAppCntctSfxNm,
                   LoanAppCntctTitlTxt,
                   LoanAppCntctPhn,
                   LoanAppCntctFax,
                   LoanAppCntctemail,
                   LoanPckgSourcTypCd,
                   LoanAppPckgSourcNm,
                   LoanAppPckgSourcStr1Nm,
                   LoanAppPckgSourcStr2Nm,
                   LoanAppPckgSourcCtyNm,
                   LoanAppPckgSourcStCd,
                   LoanAppPckgSourcZipCd,
                   LoanAppPckgSourcZip4Cd,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppPrtTaxId,
                   LoanAppLSPHQLocId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanAgntInvlvdInd,
                   --   LoanAppLspLastNm,
                   --   LoanAppLspFirstNm,
                   --   LoanAppLspInitialNm,
                   LoanAppLspSfxNm,
                   --   LoanAppLspTitlTxt,
                   --    LoanAppLspPhn,
                   LoanAppLspFax,
                   --   LoanAppLspEMail,
                   LoanPrtCntctCellPhn,
                   LoanPrtAltCntctFirstNm,
                   LoanPrtAltCntctLastNm,
                   LoanPrtAltCntctInitialNm,
                   LoanPrtAltCntctTitlTxt,
                   LoanPrtAltCntctPrimPhn,
                   LoanPrtAltCntctCellPhn,
                   LoanPrtAltCntctemail,
                   LendrAltCntctTypCd
              FROM LoanApp.LoanAppPrtTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPymntTbl
        INSERT INTO LoanApp.LoanPymntTbl (LOANAPPNMB,
                                          PYMNTDAYNMB,
                                          PYMNTBEGNMONMB,
                                          ESCROWACCTRQRDIND,
                                          NETEARNCLAUSEIND,
                                          ARMMARTYPIND,
                                          ARMMARCEILRT,
                                          ARMMARFLOORRT,
                                          STINTRTREDUCTNIND,
                                          LATECHGIND,
                                          LATECHGAFTDAYNMB,
                                          LATECHGFEEPCT,
                                          PYMNTINTONLYBEGNMONMB,
                                          PYMNTINTONLYFREQCD,
                                          PYMNTINTONLYDAYNMB,
                                          NetEarnPymntPct,
                                          NetEarnPymntOverAmt,
                                          StIntRtReductnPrgmNm,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   PYMNTDAYNMB,
                   PYMNTBEGNMONMB,
                   ESCROWACCTRQRDIND,
                   NETEARNCLAUSEIND,
                   ARMMARTYPIND,
                   ARMMARCEILRT,
                   ARMMARFLOORRT,
                   STINTRTREDUCTNIND,
                   LATECHGIND,
                   LATECHGAFTDAYNMB,
                   LATECHGFEEPCT,
                   PYMNTINTONLYBEGNMONMB,
                   PYMNTINTONLYFREQCD,
                   PYMNTINTONLYDAYNMB,
                   NetEarnPymntPct,
                   NetEarnPymntOverAmt,
                   StIntRtReductnPrgmNm,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPymntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO Loanapp.LoanAgntTbl (LOANAPPNMB,
                                         LoanAgntSeqNmb,
                                         LOANAGNTBUSPERIND,
                                         LOANAGNTNM,
                                         LOANAGNTCNTCTFIRSTNM,
                                         LOANAGNTCNTCTMIDNM,
                                         LOANAGNTCNTCTLASTNM,
                                         LOANAGNTCNTCTSFXNM,
                                         LOANAGNTADDRSTR1NM,
                                         LOANAGNTADDRSTR2NM,
                                         LOANAGNTADDRCTYNM,
                                         LOANAGNTADDRSTCD,
                                         LOANAGNTADDRSTNM,
                                         LOANAGNTADDRZIPCD,
                                         LOANAGNTADDRZIP4CD,
                                         LOANAGNTADDRPOSTCD,
                                         LOANAGNTADDRCNTCD,
                                         LOANAGNTTYPCD,
                                         LOANAGNTDOCUPLDIND,
                                         LOANCDCTPLFEEIND,
                                         LOANCDCTPLFEEAMT,
                                         LOANAGNTID,
                                         CreatUserId,
                                         CreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM,
                   LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LoanAgntId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAgntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO LOANAPP.LOANAGNTFEEDTLTBL (LOANAPPNMB,
                                               LoanAgntSeqNmb,
                                               LOANAGNTSERVTYPCD,
                                               LOANAGNTSERVOTHTYPTXT,
                                               LOANAGNTAPPCNTPAIDAMT,
                                               LOANAGNTSBALENDRPAIDAMT,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   LoanAGNTSeqNmb,
                   LOANAGNTSERVTYPCD,
                   LOANAGNTSERVOTHTYPTXT,
                   LOANAGNTAPPCNTPAIDAMT,
                   LOANAGNTSBALENDRPAIDAMT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LOANAPP.LOANAGNTFEEDTLTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    p_NEWLoanAppNmb := v_NEWLoanAppNmb;
END LOANCOPYCSP;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

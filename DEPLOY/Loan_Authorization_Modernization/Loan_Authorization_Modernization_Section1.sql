define deploy_name=Loan_Authorization_Modernization
define package_name=Section1
define package_buildtime=20201023114500
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_Section1 created on Fri 10/23/2020 11:45:02.13 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_Section1 created on Fri 10/23/2020 11:45:02.13 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_Section1: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql 
Jasleen Gorowada committed 741beb5 on Tue Oct 20 13:20:22 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql"

CREATE OR REPLACE procedure LOANAPP.LOANAUTHDOCCSP
(
p_LoanAppNMb number := null,
p_CreatuserId varchar2:=null,
p_SelCur1 out sys_refcursor,
p_SelCur2 out sys_refcursor,
p_SelCur3 out sys_refcursor,
p_TermInYears out float,
p_LoanNmb out varchar2,
p_AppvDt out date,
p_DebentureAmt out number,
p_default_string out char
)
as
origstate boolean := true;
loannmb_in char(10) := null;
loanapp_table_rec LOANAPP.LOANAPPTBL%rowtype;
loan_table_rec LOAN.LOANGNTYTBL%rowtype;
sbaofcd char(5):= null;

begin
    SAVEPOINT LOANAUTHDOCCSP;
    -- to check if loan is in originaiton or servicing
    p_default_string:='[MISSING DATA]';
    select LOANNMB into loannmb_in from LOANAPP.LOANAPPTBL
    where LOANAPPNMB=p_LoanAppNmb;
    if(loannmb_in is null) then
        origstate:=true;
    else
        origstate:= false;
    end if;  
    
    --checks if in origination or servicing
    if origstate=true then 
    --cursor to retrieve Loan information  
        open p_SelCur1 for 
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOANAPP.LOANAPPTBL
            where LoanappNmb=p_LoanAppNmb;
        select * into loanapp_table_rec from LOANAPP.LOANAPPTBL  where LoanappNmb=p_LoanAppNmb;
            
    else
        open p_SelCur1 for 
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOAN.LOANGNTYTBL
            where LoanappNmb=p_LoanAppNmb;  
        select * into loan_table_rec from LOAN.LOANGNTYTBL  where LoanappNmb=p_LoanAppNmb;    
    end if;
        

    
    --cursor to retrieve LOANAPPPRTTBL information
    open p_SelCur2 for
        select cast(nvl(LoanAppPrtNm,p_default_string)as VARCHAR2(200)) as LOANAPPPRTNM,
        cast(nvl(LoanAppFirsNmb,p_default_string)as CHAR(25)) as LOANAPPFIRSNMB,
        cast(nvl(LoanAppPrtStr1,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR1,
        cast(nvl(LoanAppPrtStr2,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR2,
        cast(nvl(LoanAppPrtCtyNm,p_default_string) as VARCHAR2(40)) as LOANAPPPRTCTYNM, 
        cast(nvl(LoanAppPrtStCd,p_default_string) as CHAR(25)) as LOANAPPPRTSTCD,
        cast(nvl(LoanAppPrtZipCd,p_default_string) as CHAR(25)) as LOANAPPPRTZIPCD
        from LOANAPP.LOANAPPPRTTBL	where LoanAppNmb=p_LoanAppNmb;
        
    -- cursor to retrieve OFCCD information
    if origstate=true then
        sbaofcd:=loanapp_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM, 
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM , 
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)) as STCD, 
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) as ZIPCD5
            from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;
    else
        sbaofcd:=loan_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM, 
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM , 
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)), 
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;    
    end if;   
    --output variable for TermInYears 
    if origstate=true then
        p_TermInYears:=loanapp_table_rec.LOANAPPRQSTMATMOQTY/12; 
    else
        p_TermInYears:=loan_table_rec.LOANAPPRQSTMATMOQTY/12;  
    end if;       
    
    --Debenture amount
    if origstate=true then   
        p_LoanNmb:=null;
        p_AppvDt:=null;
        p_DebentureAmt:=loanapp_table_rec.LOANAPPRQSTAMT;
    else
        p_LoanNmb:=loan_table_rec.LOANNMB;
        p_AppvDt:=loan_table_rec.LOANLASTAPPVDT;
        p_DebentureAmt:=loan_table_rec.LOANCURRAPPVAMT;
    end if;   
        
        
EXCEPTION
WHEN OTHERS THEN
					BEGIN
					ROLLBACK TO LOANAUTHDOCCSP;
					RAISE;
					END;

end;
/


GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LMSDEV;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPOSTSERVSU;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPRTUPDT;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANSERVLOANPRTUPLOAD;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANSERVSBICGOV;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANUPDT;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO POOLSECADMINROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

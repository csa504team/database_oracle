define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0712
define package_buildtime=20210712162435
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0712 created on Mon 07/12/2021 16:24:36.46 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0712 created on Mon 07/12/2021 16:24:36.46 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0712: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\updates_0630.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\updates_0630.sql"
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=1 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='01';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=2 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='02';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=3 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='03';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=4 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='04';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=5 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='05';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=6 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='06';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=9 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='09';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=10 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='10';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=11 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='11';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=15 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='15';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=19 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='19';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=21 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='21';                                                                           
update SBAREF.LOANPROCDTYPTBL set LOANPROCDTYPORD=22 where PROCDTYPCD = 'E'
and LOANPROCDTYPCD='22';  
commit;                                                                         
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAUTHDOCCSP.sql"
CREATE OR REPLACE procedure LOANAPP.LOANAUTHDOCCSP
(
p_LoanAppNMb number := null,
p_CreatuserId varchar2:=null,
p_SelCur1 out sys_refcursor,
p_SelCur2 out sys_refcursor,
p_SelCur3 out sys_refcursor,
p_SelCur4 out sys_refcursor,
p_SelCur5 out sys_refcursor,
p_SelCur6 out sys_refcursor,
--p_SelCur7 out sys_refcursor,
p_SelCur7 out sys_refcursor,
p_SelCur8 out sys_refcursor,
p_SelCur9 out sys_refcursor,
p_SelCur10 out sys_refcursor,
p_TermInYears out float,
p_LoanNmb out varchar2,
p_AppvDt out date,
p_DebentureAmt out number,
p_default_string out varchar2,
p_EPCText  out varchar2 ,
p_LOANAPPEPCIND out char,
p_REFIOPTION out varchar2,
--p_ProceedText out varchar2,
--p_LoanProcdAmt out number,
p_total_LOANPROCDAMT out number
)
as
origstate boolean := true;
loannmb_in char(10) := null;
loanapp_table_rec LOANAPP.LOANAPPTBL%rowtype;
loan_table_rec LOAN.LOANGNTYTBL%rowtype;
sbaofcd char(5):= null;
InCA boolean := false;
BusinessRECollateral boolean := false;
PPCollateral boolean := false;
MarineCollateral boolean := false;
LiabilityInsurance boolean := false;
ProductLiabilityInsurance boolean := false;
DramShopInsurance boolean := false;
MalpracticeInsurance boolean := false;
WorkersCompInsurance boolean := false;
OtherInsurance boolean := false;
InsuranceTypes varchar2(255) :=null;
EnvironmentalRequired boolean :=false;
/* count borrowers for H2_CertificationAgreements() */
NumBorrowers number := 0;
Suffix varchar2(10):=null;
TradeName  varchar2(80) :=null;
modified_Borr_Address varchar2(255) :=null;
modified_Borr_Insurance varchar2(100) :=null;
modified_Guar_Address varchar2(255) :=null;
modified_Guar_Insurance varchar2(100) :=null;
p_ProceedText varchar2(250) := null;
p_LoanProcdAmt number :=null;





--Check if OC is Guarantor
Cursor OCGuarantor
is
    Select g.*, 
    b.BusPhyAddrStr1NM||' '||b.BusPhyAddrStr2Nm||' '||b.BusPhyAddrCtyNm||' , '||b.BusPhyAddrStCd||' '||b.BusPhyAddrZipCd as Address,
    b.BusPhyAddrStCd as State,
    b.BusNm as Name,
    b.BusTrdNm  
    From LoanApp.LoanGuarTbl g Join LoanApp.LoanBusTbl b 
    On (g.TaxId = b.TaxId And g.LoanAppNmb = b.LoanAppNmb)
    Where g.GUARBUSPERIND = 'B'
    And g.LoanGuarOperCoInd = 'Y'
    and g.LOANAPPNMB=p_LoanAppNmb;
    
Cursor OCGuarantor1
is
    Select g.*, 
    b.BusPhyAddrStr1NM||' '||b.BusPhyAddrStr2Nm||' '||b.BusPhyAddrCtyNm||' , '||b.BusPhyAddrStCd||' '||b.BusPhyAddrZipCd as Address,
    b.BusPhyAddrStCd as State,
    b.BusNm as Name,
    b.BusTrdNm  
    From Loan.LOANGNTYGUARTBL g Join Loan.BusTbl b 
    On (g.TaxId = b.TaxId)
    Where g.GUARBUSPERIND = 'B'
    And g.LoanGuarOperCoInd = 'Y'
    and g.LOANAPPNMB=p_LoanAppNmb;
    
    
Cursor LoanAppBorr
is
    Select bor.*, 
    bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
    bus.BusPhyAddrStCd as State, 
    bus.BusNm as Name,
    bus.BusTrdNm 
    From LOANAPP.LoanBorrTbl bor Join LOANAPP.LoanBusTbl bus 
    On (bor.TaxId = bus.TaxId And bor.LoanAppNmb = bus.LoanAppNmb)
    Where bor.BorrBusPerInd = 'B'
    and bor.LOANAPPNMB=p_LoanAppNmb
    UNION
    --Person type borrower's information if origination=true
    Select bor.*, 
    p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
    p.PerPhyAddrStCd as State, 
    UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Name,
    null as BusTrdNm  
    From loanapp.LoanBorrTbl bor Join loanapp.LoanPerTbl p 
    On (bor.TAXID = p.TaxId And bor.LoanAppNmb = p.LoanAppNmb)
    Where bor.BorrBusPerInd = 'P' 
    and bor.LOANAPPNMB=p_LoanAppNmb; 
    
Cursor LoanBorr
is
    Select bor.*, 
    bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
    bus.BusPhyAddrStCd as State, 
    bus.BusNm as Name,
    bus.BusTrdNm  
    From LOAN.LoanGntyBorrTbl bor Join LOAN.BusTbl bus 
    On (bor.TaxId = bus.TaxId)
    Where bor.BorrBusPerInd = 'B'
    and bor.LOANAPPNMB=p_LoanAppNmb
    UNION
--Person type borrower's information if origination=false
    Select bor.*, 
    p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
    p.PerPhyAddrStCd as State, 
    UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Name,
    null as BusTrdNm  
    From loan.LoanGntyBorrTbl bor Join loan.PerTbl p 
    On (bor.TAXID = p.TaxId)
    Where bor.BorrBusPerInd = 'P' 
    and bor.LOANAPPNMB=p_LoanAppNmb;    

--Records for LoanSunProceed
Cursor LOANSUBPROCD
is 
	select * from LOANAPP.LOANSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
	and LOANPROCDTYPCD in ('01','02','04','10','15','19') order by LOANPROCDADDR desc, 
	LOANPROCDTYPCD asc;
	
Cursor LOANGNTYSUBPROCD
is 
	select * from LOAN.LOANGNTYSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
	and LOANPROCDTYPCD in ('01','02','04','10','15','19') order by LOANPROCDADDR desc, 
	LOANPROCDTYPCD asc;
	
--Records for LoanSunProceed LOANPROCDTYPCD 15
Cursor LOANSUBPROCD_15
is 
	select * from LOANAPP.LOANSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
	and LOANPROCDTYPCD='15' order by LOANPROCDADDR desc, 
	LOANPROCDTYPCD asc;
	
Cursor LOANGNTYSUBPROCD_15
is 
	select * from LOAN.LOANGNTYSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
	and LOANPROCDTYPCD='15' order by LOANPROCDADDR desc, 
	LOANPROCDTYPCD asc;

--LoanProceed Records
Cursor LOANPROCD
is
    Select *
    From LOANAPP.LOANPROCDTBL
    Where LoanAppNmb = p_LoanAppNMb
    And ProcdTypCd = 'E'
    order by LoanProcdTypCD;

Cursor LOANGNTYPROCD
is
    Select *
    From LOAN.LOANGNTYPROCDTBL
    Where LoanAppNmb = p_LoanAppNMb
    And ProcdTypCd = 'E'
    order by LoanProcdTypCD;


ocguarantor_rec OCGuarantor%rowtype;
ocguarantor_rec1 OCGuarantor1%rowtype;
LoanAppBorr_rec LoanAppBorr%rowtype;
LoanBorr_rec LoanBorr%rowtype;
LOANSUBPROCD_rec LOANSUBPROCD%rowtype;
LOANGNTYSUBPROCD_rec LOANGNTYSUBPROCD%rowtype;
LOANSUBPROCD_rec15 LOANSUBPROCD%rowtype;
LOANGNTYSUBPROCD_rec15 LOANGNTYSUBPROCD%rowtype;
LOANPROCD_rec LOANPROCD%rowtype;
LOANGNTYPROCD_rec LOANGNTYPROCD%rowtype;
begin
    SAVEPOINT LOANAUTHDOCCSP;
    -- to check if loan is in originaiton or servicing
    p_default_string:='[MISSING DATA]';
    select LOANNMB into loannmb_in from LOANAPP.LOANAPPTBL
    where LOANAPPNMB=p_LoanAppNmb;
    if(loannmb_in is null) then
        origstate:=true;
    else
        origstate:= false;
    end if;

    --checks if in origination or servicing
    if origstate=true then
    --cursor to retrieve Loan information
        open p_SelCur1 for
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOANAPP.LOANAPPTBL
            where LoanappNmb=p_LoanAppNmb;
        select * into loanapp_table_rec from LOANAPP.LOANAPPTBL  where LoanappNmb=p_LoanAppNmb;

    else
        open p_SelCur1 for
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOAN.LOANGNTYTBL
            where LoanappNmb=p_LoanAppNmb;
        select * into loan_table_rec from LOAN.LOANGNTYTBL  where LoanappNmb=p_LoanAppNmb;
    end if;



    --cursor to retrieve LOANAPPPRTTBL information
    open p_SelCur2 for
        select cast(nvl(LoanAppPrtNm,p_default_string)as VARCHAR2(200)) as LOANAPPPRTNM,
        cast(nvl(LoanAppFirsNmb,p_default_string)as CHAR(25)) as LOANAPPFIRSNMB,
        cast(nvl(LoanAppPrtStr1,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR1,
        cast(nvl(LoanAppPrtStr2,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR2,
        cast(nvl(LoanAppPrtCtyNm,p_default_string) as VARCHAR2(40)) as LOANAPPPRTCTYNM,
        cast(nvl(LoanAppPrtStCd,p_default_string) as CHAR(25)) as LOANAPPPRTSTCD,
        cast(nvl(LoanAppPrtZipCd,p_default_string) as CHAR(25)) as LOANAPPPRTZIPCD
        from LOANAPP.LOANAPPPRTTBL	where LoanAppNmb=p_LoanAppNmb;

    -- cursor to retrieve OFCCD information
    if origstate=true then
        sbaofcd:=loanapp_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM,
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM ,
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)) as STCD,
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) as ZIPCD5
            from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;
    else
        sbaofcd:=loan_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM,
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM ,
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)),
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;
    end if;
    --output variable for TermInYears
    if origstate=true then
        p_TermInYears:=loanapp_table_rec.LOANAPPRQSTMATMOQTY/12;
    else
        p_TermInYears:=loan_table_rec.LOANAPPRQSTMATMOQTY/12;
    end if;

    --Debenture amount
    if origstate=true then
        p_LoanNmb:=null;
        p_AppvDt:=null;
        p_DebentureAmt:=loanapp_table_rec.LOANAPPRQSTAMT;
    else
        p_LoanNmb:=loan_table_rec.LOANNMB;
        p_AppvDt:=loan_table_rec.LOANLASTAPPVDT;
        p_DebentureAmt:=loan_table_rec.LOANCURRAPPVAMT;
    end if;
    --Business type borrower's information if origination=true
    if origstate=true then
        open p_SelCur4 for
            Select bor.*, 
            bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
            bus.BusPhyAddrStCd as State, 
            bus.BusNm as Name,
            bus.BusTrdNm 
            From LOANAPP.LoanBorrTbl bor Join LOANAPP.LoanBusTbl bus 
            On (bor.TaxId = bus.TaxId And bor.LoanAppNmb = bus.LoanAppNmb)
            Where bor.BorrBusPerInd = 'B'
            and bor.LOANAPPNMB=p_LoanAppNmb
            UNION
            --Person type borrower's information if origination=true
            Select bor.*, 
            p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
            p.PerPhyAddrStCd as State, 
            UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Name,
            null as BusTrdNm  
            From loanapp.LoanBorrTbl bor Join loanapp.LoanPerTbl p 
            On (bor.TAXID = p.TaxId And bor.LoanAppNmb = p.LoanAppNmb)
            Where bor.BorrBusPerInd = 'P' 
            and bor.LOANAPPNMB=p_LoanAppNmb;  
    else
        open p_SelCur5 for
            --Business type borrower's information if origination=false
            Select bor.*, 
            bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
            bus.BusPhyAddrStCd as State, 
            bus.BusNm as Name,
            bus.BusTrdNm  
            From LOAN.LoanGntyBorrTbl bor Join LOAN.BusTbl bus 
            On (bor.TaxId = bus.TaxId)
            Where bor.BorrBusPerInd = 'B'
            and bor.LOANAPPNMB=p_LoanAppNmb
            UNION
        --Person type borrower's information if origination=false
            Select bor.*, 
            p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
            p.PerPhyAddrStCd as State, 
            UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Name,
            null as BusTrdNm  
            From loan.LoanGntyBorrTbl bor Join loan.PerTbl p 
            On (bor.TAXID = p.TaxId)
            Where bor.BorrBusPerInd = 'P' 
            and bor.LOANAPPNMB=p_LoanAppNmb; 
    end if;
        
       
    -- If EPC transaction, then need to check if OC is guarantor. OC will always be a business. 
    if origstate = true then
		p_LOANAPPEPCIND := loanapp_table_rec.LOANAPPEPCIND;
        if(loanapp_table_rec.LOANAPPEPCIND = 'Y') then 
            open OCGuarantor;
                loop
                    fetch OCGuarantor into ocguarantor_rec;
                    exit when OCGuarantor%notfound;
                end loop;
                if(OCGuarantor%rowcount >0) then
                    p_EPCText:= 'Guarantor Operating Company:';
                end if; 
            close OCGuarantor;
        end if;
    else
		p_LOANAPPEPCIND := loan_table_rec.LOANAPPEPCIND;
        if(loan_table_rec.LOANAPPEPCIND='Y') then
            open OCGuarantor1;
                loop
                    fetch OCGuarantor1 into ocguarantor_rec1;
                    exit when OCGuarantor1%notfound;
                end loop;
                if(OCGuarantor1%rowcount >0) then
                    p_EPCText:= 'Guarantor Operating Company:';
                end if; 
            close OCGuarantor1;
        end if;
    end if;
                
                
            

    
    --List all borrowers identified in p_SelCur4 or p_SelCur5 
    if origstate=true then
        delete from LOANAPP.Borr_Address;
        delete from LOANAPP.Borr_Insurance;
        open LoanAppBorr;
            loop
                FETCH LoanAppBorr INTO LoanAppBorr_rec;
                exit when LoanAppBorr%notfound;
               -- if borrower is in California 
                if( LoanAppBorr_rec.State= 'CA' and InCA=false) then 
                    InCA := true;
                end if;
                if(loanapp_table_rec.LoanAppEPCInd ='Y') then 
                    if(LoanAppBorr_rec.ImEPCOperCd =2 ) then
                        Suffix := '(EPC)';
                    elsif(LoanAppBorr_rec.ImEPCOperCd  = 3) then
                        Suffix := '(Operating Concern)';
                    end if;
                end if;
                if( LoanAppBorr_rec.BusTrdNm is not null) then
                    TradeName := 'dba '|| LoanAppBorr_rec.BusTrdNm;
                else
                    TradeName := null;
                end if;
                modified_Borr_Address := LoanAppBorr_rec.Name||' '||TradeName||' '||Suffix||' '||LoanAppBorr_rec.Address;
                insert into LOANAPP.Borr_Address(Address)values(modified_Borr_Address);
                commit;
                --if any of insurance required for this borrower. Must be business. 
                if(LoanAppBorr_rec.BORRBUSPERIND = 'B') then
                  --  dbms_output.put_line('Inside loop for BORRBUSPERIND as B'||LoanAppBorr_rec.LOANAPPNMB);
                    if(LoanAppBorr_rec.LiabInsurRqrdInd = 'Y') then
                        LiabilityInsurance := true;
                    end if;
                    if(LoanAppBorr_rec.ProdLiabInsurRqrdInd  = 'Y') then
                        ProductLiabilityInsurance := true;
                    end if;
                    If(LoanAppBorr_rec.LiqLiabInsurRqrdInd = 'Y') then 
                        DramShopInsurance := true;
                    end if;
                    If(LoanAppBorr_rec.MalprctsInsurRqrdInd = 'Y') then 
                        MalpracticeInsurance := true;
                    end if;
                    If(LoanAppBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') then 
                        WorkersCompInsurance := true;
                    end if;
                  --  dbms_output.put_line('LoanAppBorr_rec.OthInsurRqrdInd: '||LoanAppBorr_rec.OthInsurRqrdInd);
                    If(LoanAppBorr_rec.OthInsurRqrdInd = 'Y') then
                        OtherInsurance := true;
                        if(InsuranceTypes is null) then
                            InsuranceTypes := LoanAppBorr_rec.OthInsurDescTxt;
                            insert into LOANAPP.Borr_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                         --   dbms_output.put_line('InsuranceTypes inside if: '||InsuranceTypes);
                        else
                            InsuranceTypes := InsuranceTypes||', '||LoanAppBorr_rec.OthInsurDescTxt;
                            insert into LOANAPP.Borr_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                         --   dbms_output.put_line('InsuranceTypes inside else: '||InsuranceTypes);
                        end if;
                    end if;
                end if;
                NumBorrowers := NumBorrowers+1; 
             --   dbms_output.put_line('LoanAppBorr_rec.Address: '||LoanAppBorr_rec.Address);
             --   dbms_output.put_line('NumBorrowers: '||NumBorrowers);
            end loop;
            close LoanAppBorr;
            open p_SelCur6 for
            select Address from LOANAPP.Borr_Address;
            --open p_SelCur7 for
           -- select Insurance from LOANAPP.Borr_Insurance;
            
    else
        delete from LOANAPP.Borr_Address;
        delete from LOANAPP.Borr_Insurance;
        open LoanBorr;
            loop
                fetch LoanBorr into LoanBorr_rec;
                exit when LoanBorr%notfound;
                --if borrower is in California 
                if( LoanBorr_rec.State= 'CA' and InCA=false) then 
                    InCA := true;
                end if;
                if(loan_table_rec.LoanAppEPCInd ='Y') then 
                    if(LoanBorr_rec.ImEPCOperCd =2 ) then
                        Suffix := '(EPC)';
                    elsif(LoanBorr_rec.ImEPCOperCd  = 3) then
                        Suffix := '(Operating Concern)';
                    end if;
                end if;
                if( LoanBorr_rec.BusTrdNm is not null) then
                    TradeName := 'dba '|| LoanBorr_rec.BusTrdNm;
                else
                    TradeName := null;
                end if;
                modified_Borr_Address := LoanBorr_rec.Name||' '||TradeName||' '||Suffix||' '||LoanBorr_rec.Address;
                insert into LOANAPP.Borr_Address(Address)values(modified_Borr_Address);
                commit;
               -- dbms_output.put_line('LoanBorr_rec.Address: '||LoanBorr_rec.Address);
                --if any of insurance required for this borrower. Must be business. 
                if(LoanBorr_rec.BORRBUSPERIND = 'B') then
                    if(LoanBorr_rec.LiabInsurRqrdInd = 'Y') then
                        LiabilityInsurance := true;
                    end if;
                    if(LoanBorr_rec.ProdLiabInsurRqrdInd  = 'Y') then
                        ProductLiabilityInsurance := true;
                    end if;
                    If(LoanBorr_rec.LiqLiabInsurRqrdInd = 'Y') then 
                        DramShopInsurance := true;
                    end if;
                    If(LoanBorr_rec.MalprctsInsurRqrdInd = 'Y') then 
                        MalpracticeInsurance := true;
                    end if;
                    If(LoanBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') then 
                        WorkersCompInsurance := true;
                    end if;
                    If(LoanBorr_rec.OthInsurRqrdInd = 'Y') then
                        OtherInsurance := true;
                        if(InsuranceTypes is null) then
                            InsuranceTypes := LoanBorr_rec.OthInsurDescTxt;
                            insert into LOANAPP.Borr_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        else
                            InsuranceTypes := InsuranceTypes||', '||LoanBorr_rec.OthInsurDescTxt;
                            insert into LOANAPP.Borr_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        end if;
                    end if;
                end if;
                NumBorrowers := NumBorrowers+1;
            end loop;
            close LoanBorr;
            open p_SelCur6 for
            select Address from LOANAPP.Borr_Address;
            --open p_SelCur7 for
            --select Insurance from LOANAPP.Borr_Insurance;
    end if; 
    
    -- If EPC/OC and if OC is guarantor, OC gets put into a separate column in authorization.       
    if origstate=true then
            delete from LOANAPP.Guar_Address;
            delete from LOANAPP.Guar_Insurance;
            open OCGuarantor;
                loop
                    fetch OCGuarantor into ocguarantor_rec;
                    exit when OCGuarantor%notfound;  
                    if(ocguarantor_rec.BusTrdNm  is not null) then
                        TradeName := 'dba '||ocguarantor_rec.BusTrdNm;
                    else
                        TradeName :=null;
                    end if;
                    modified_Guar_Address := ocguarantor_rec.Name||' '||TradeName||' Guarantor';
                    insert into LOANAPP.Guar_Address(Address)values(modified_Guar_Address);
                    commit;
                    if(ocguarantor_rec.LiabInsurRqrdInd = 'Y') then
                        LiabilityInsurance := true;
                    end if;
                    if(ocguarantor_rec.ProdLiabInsurRqrdInd  = 'Y') then
                        ProductLiabilityInsurance := true;
                    end if;
                    If(ocguarantor_rec.LiqLiabInsurRqrdInd = 'Y') then 
                        DramShopInsurance := true;
                    end if;
                    If(ocguarantor_rec.MalprctsInsurRqrdInd = 'Y') then 
                        MalpracticeInsurance := true;
                    end if;
                    If(ocguarantor_rec.WORKRSCOMPINSRQRDIND = 'Y') then 
                        WorkersCompInsurance := true;
                    end if;
                    If(ocguarantor_rec.OthInsurRqrdInd = 'Y') then
                        OtherInsurance := true;
                        if(InsuranceTypes is null) then
                            InsuranceTypes := ocguarantor_rec.OthInsurDescTxt;
                            insert into LOANAPP.Guar_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        else
                            InsuranceTypes := InsuranceTypes||', '||ocguarantor_rec.OthInsurDescTxt;
                            insert into LOANAPP.Guar_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        end if;
                    end if;
                   -- dbms_output.put_line('ocguarantor_rec.Address: '||ocguarantor_rec.Address);
                end loop;
            close OCGuarantor;  
            open p_SelCur7 for
            select Address from LOANAPP.Guar_Address;
            --open p_SelCur9 for
            --select Insurance from LOANAPP.Guar_Insurance;  
    else
            delete from LOANAPP.Guar_Address;
            delete from LOANAPP.Guar_Insurance;
            open OCGuarantor1;
                loop
                    fetch OCGuarantor1 into ocguarantor_rec1;
                    exit when OCGuarantor1%notfound;
                    if(ocguarantor_rec1.BusTrdNm  is not null) then
                        TradeName := 'dba '||ocguarantor_rec1.BusTrdNm;
                    else
                        TradeName :=null;
                    end if;
                    modified_Guar_Address := ocguarantor_rec1.Name||' '||TradeName||' Guarantor'; 
                    insert into LOANAPP.Guar_Address(Address)values(modified_Guar_Address);
                    commit;
                    if(ocguarantor_rec1.LiabInsurRqrdInd = 'Y') then
                        LiabilityInsurance := true;
                    end if;
                    if(ocguarantor_rec1.ProdLiabInsurRqrdInd  = 'Y') then
                        ProductLiabilityInsurance := true;
                    end if;
                    If(ocguarantor_rec1.LiqLiabInsurRqrdInd = 'Y') then 
                        DramShopInsurance := true;
                    end if;
                    If(ocguarantor_rec1.MalprctsInsurRqrdInd = 'Y') then 
                        MalpracticeInsurance := true;
                    end if;
                    If(ocguarantor_rec1.WORKRSCOMPINSRQRDIND = 'Y') then 
                        WorkersCompInsurance := true;
                    end if;
                    If(ocguarantor_rec1.OthInsurRqrdInd = 'Y') then
                        OtherInsurance := true;
                        if(InsuranceTypes is null) then
                            InsuranceTypes := ocguarantor_rec1.OthInsurDescTxt;
                            insert into LOANAPP.Guar_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        else
                            InsuranceTypes := InsuranceTypes||', '||ocguarantor_rec1.OthInsurDescTxt;
                            insert into LOANAPP.Guar_Insurance(Insurance)values(InsuranceTypes);
                            commit;
                        end if;
                    end if;             
                    exit when OCGuarantor1%notfound;
                    --dbms_output.put_line('ocguarantor_rec1.Address: '||ocguarantor_rec1.Address);
                end loop;
            close OCGuarantor1;
            open p_SelCur7 for
            select Address from LOANAPP.Guar_Address;
            --open p_SelCur9 for
            --select Insurance from LOANAPP.Guar_Insurance;  
    end if;     
    
    --Get SubProcdTbl information
    if origstate=true then 
        open p_SelCur8 for
            Select subprcd.* 
            From LOANAPP.LOANSUBPROCDTBL subprcd 
            where subprcd.LOANAPPNMB=p_LoanAppNmb;
    else
        open p_SelCur8 for
        Select subprcd.* 
        From LOAN.LOANGNTYSUBPROCDTBL subprcd 
        where subprcd.LOANAPPNMB=p_LoanAppNmb; 
    end if;
             
    -- Determine if any Use of Proceeds are for debt refinancing
    if origstate=true then
            open LOANSUBPROCD_15;
            loop 
                fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                exit when LOANSUBPROCD_15%notfound;
                if LOANSUBPROCD_15%found then
                    p_REFIOPTION := 'and refinancing';
                end if;
            end loop;
            close LOANSUBPROCD_15;
    else
            open LOANGNTYSUBPROCD_15;
            loop 
                fetch LOANGNTYSUBPROCD_15 into LOANGNTYSUBPROCD_rec15;
                exit when LOANGNTYSUBPROCD_15%notfound;
                if LOANGNTYSUBPROCD_15%found then
                    p_REFIOPTION := 'and refinancing';
                end if;
            end loop;
            close LOANSUBPROCD_15;
    end if;
    
        --Get SubProcdTbl information for LOANPROCDTYPCD in ('01','02','04','10','15','19')
    if origstate=true then 
        open p_SelCur9 for
            select * from LOANAPP.LOANSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
            and LOANPROCDTYPCD in ('01','02','04','10','15','19') order by LOANPROCDADDR desc, 
            LOANPROCDTYPCD asc;
    else
        open p_SelCur9 for
        	select * from LOAN.LOANGNTYSUBPROCDTBL where LOANAPPNMB=p_LoanAppNMb and PROCDTYPCD='E'
            and LOANPROCDTYPCD in ('01','02','04','10','15','19') order by LOANPROCDADDR desc, 
            LOANPROCDTYPCD asc;
    end if;

    --Check each Procd
    if origstate=true then
            p_total_LOANPROCDAMT :=0;
            open LOANPROCD;
            loop 
                fetch LOANPROCD into LOANPROCD_rec;
                exit when LOANPROCD%notfound;
                --get total project costs by summing all uses of proceeds
                p_total_LOANPROCDAMT := p_total_LOANPROCDAMT + LOANPROCD_rec.LOANPROCDAMT;
                if (LOANPROCD_rec.LOANPROCDTYPCD='01' or LOANPROCD_rec.LOANPROCDTYPCD='02' or LOANPROCD_rec.LOANPROCDTYPCD='04'or LOANPROCD_rec.LOANPROCDTYPCD='10') then
                    /* Purchase Land Only */
                    case when (LOANPROCD_rec.LOANPROCDTYPCD = '01') then
                        p_ProceedText := 'Purchase Land';
                        
                    /* Purchase Land and Existing Building */
                    when (LOANPROCD_rec.LOANPROCDTYPCD='02') then
                        p_ProceedText := 'Purchase Land '|| chr(38) ||' Building';
                    
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    when (LOANPROCD_rec.LOANPROCDTYPCD='05') then
                        p_ProceedText := 'Construction/Remodeling';
                    
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    when (LOANPROCD_rec.LOANPROCDTYPCD='10') then
                        p_ProceedText := 'Purchase/Install Equipment';
                    else null;
                    end case; 
                    p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;
                /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                    open LOANSUBPROCD_15;
                    loop 
                        fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                        exit when LOANSUBPROCD_15%notfound;
                    end loop;
                    close LOANSUBPROCD_15;
                    p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
                elsif (LOANPROCD_rec.LOANPROCDTYPCD='15' or LOANPROCD_rec.LOANPROCDTYPCD='19' or LOANPROCD_rec.LOANPROCDTYPCD='21'or LOANPROCD_rec.LOANPROCDTYPCD='22') then
                    /* Eligible business expenses under Debt Refinancing: New use 
                    of proceeds introduced with revised 1244 Form.  */

                    case when (LOANPROCD_rec.LOANPROCDTYPCD = '19') then
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                        
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    when (LOANPROCD_rec.LOANPROCDTYPCD='21') then
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    when (LOANPROCD_rec.LOANPROCDTYPCD='22') then
                        p_ProceedText := 'Professional Fees';
                    else null;
                     end case;
                    p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;
                end if;
            end loop;
            close LOANPROCD;
    elsif origstate=false then
            p_total_LOANPROCDAMT :=0;
            open LOANGNTYPROCD;
            loop 
                fetch LOANGNTYPROCD into LOANGNTYPROCD_rec;
                exit when LOANGNTYPROCD%notfound;
                --get total project costs by summing all uses of proceeds
                p_total_LOANPROCDAMT := p_total_LOANPROCDAMT + LOANGNTYPROCD_rec.LOANPROCDAMT;
                if (LOANGNTYPROCD_rec.LOANPROCDTYPCD='01' or LOANGNTYPROCD_rec.LOANPROCDTYPCD='02' or LOANGNTYPROCD_rec.LOANPROCDTYPCD='04'or LOANGNTYPROCD_rec.LOANPROCDTYPCD='10') then
                    /* Purchase Land Only */
                    case when (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '01') then
                        p_ProceedText := 'Purchase Land';
                        
                    /* Purchase Land and Existing Building */
                    when (LOANGNTYPROCD_rec.LOANPROCDTYPCD='02') then
                        p_ProceedText := 'Purchase Land '|| chr(38) ||' Building';
                    
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    when (LOANGNTYPROCD_rec.LOANPROCDTYPCD='04') then
                        p_ProceedText := 'Construction/Remodeling';
                    
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    when (LOANGNTYPROCD_rec.LOANPROCDTYPCD='10') then
                        p_ProceedText := 'Purchase/Install Equipment';
                    else null;
                    end case; 
                    p_LoanProcdAmt :=  LOANGNTYPROCD_rec.LoanProcdAmt;
                /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                    open LOANSUBPROCD_15;
                    loop 
                        fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                        exit when LOANSUBPROCD_15%notfound;
                    end loop;
                    close LOANSUBPROCD_15;
                    p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
                elsif (LOANGNTYPROCD_rec.LOANPROCDTYPCD='15' or LOANGNTYPROCD_rec.LOANPROCDTYPCD='19' or LOANGNTYPROCD_rec.LOANPROCDTYPCD='21'or LOANGNTYPROCD_rec.LOANPROCDTYPCD='22') then
                    /* Eligible business expenses under Debt Refinancing: New use 
                    of proceeds introduced with revised 1244 Form.  */

                    case when (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '19') then
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                        
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    when (LOANGNTYPROCD_rec.LOANPROCDTYPCD='21') then
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    when (LOANGNTYPROCD_rec.LOANPROCDTYPCD='22') then
                        p_ProceedText := 'Professional Fees';
                    else null;
                     end case;
                    p_LoanProcdAmt :=  LOANGNTYPROCD_rec.LoanProcdAmt;
                end if;
            end loop;
            close LOANGNTYPROCD;
    end if;
    
    
    --Get Text information as per sample
    if origstate=true then 
        open p_SelCur10 for
            select  case when lp.LOANPROCDTYPCD='01' then 'Purchase Land' 
             when slp.LOANPROCDTYPCD='02' then 'Purchase Land '|| chr(38) ||' Building'
             when slp.LOANPROCDTYPCD='04' then 'Construction/Remodeling'
             when slp.LOANPROCDTYPCD='10' then 'Purchase/Install Equipment'
             when slp.LOANPROCDTYPCD='19' then 'Eligible Business Expenses under Debt Refinancing'
             when slp.LOANPROCDTYPCD='21' then 'Other Expenses (construction contingencies, interim interest)'
             when slp.LOANPROCDTYPCD='22' then 'Professional Fees'
             when slp.LOANPROCDTYPCD='03' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='03')
             when slp.LOANPROCDTYPCD='05' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='05')
             when slp.LOANPROCDTYPCD='06' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='06')
             when slp.LOANPROCDTYPCD='09' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='09')
             when slp.LOANPROCDTYPCD='11' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='11')
             when slp.LOANPROCDTYPCD='15' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='15')
              end as LOANPROCDTYPDESC ,
             nvl(lp.LOANPROCDAMT,0) LOANPROCDAMT from  SBAREF.LOANPROCDTYPTBL slp  left outer join LOANAPP.LOANPROCDTBL lp
             on  lp.PROCDTYPCD=slp.PROCDTYPCD and lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD and
             lp.LOANAPPNMB=p_LoanAppNmb where slp.PROCDTYPCD='E';
            
    else
        open p_SelCur10 for
            select  case when lp.LOANPROCDTYPCD='01' then 'Purchase Land' 
             when slp.LOANPROCDTYPCD='02' then 'Purchase Land '|| chr(38) ||' Building'
             when slp.LOANPROCDTYPCD='04' then 'Construction/Remodeling'
             when slp.LOANPROCDTYPCD='10' then 'Purchase/Install Equipment'
             when slp.LOANPROCDTYPCD='19' then 'Eligible Business Expenses under Debt Refinancing'
             when slp.LOANPROCDTYPCD='21' then 'Other Expenses (construction contingencies, interim interest)'
             when slp.LOANPROCDTYPCD='22' then 'Professional Fees'
             when slp.LOANPROCDTYPCD='03' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='03')
             when slp.LOANPROCDTYPCD='05' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='05')
             when slp.LOANPROCDTYPCD='06' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='06')
             when slp.LOANPROCDTYPCD='09' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='09')
             when slp.LOANPROCDTYPCD='11' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='11')
             when slp.LOANPROCDTYPCD='15' then (select LOANPROCDTYPDESCTXT from SBAREF.LOANPROCDTYPTBL where PROCDTYPCD='E' and LOANPROCDTYPCD='15')
              end as LOANPROCDTYPDESC ,
             nvl(lp.LOANPROCDAMT,0) LOANPROCDAMT from  SBAREF.LOANPROCDTYPTBL slp  left outer join LOAN.LOANGNTYPROCDTBL lp
             on  lp.PROCDTYPCD=slp.PROCDTYPCD and lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD and
             lp.LOANAPPNMB=p_LoanAppNmb where slp.PROCDTYPCD='E';
    end if;
    
    

                
                         
                    
                                             
EXCEPTION
WHEN OTHERS THEN
					BEGIN
					ROLLBACK TO LOANAUTHDOCCSP;
					RAISE;
					END;

end;
/



GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPOSTSERVSU;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANPRTUPDT;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANSERVLOANPRTUPLOAD;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANSERVSBICGOV;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO LOANUPDT;

GRANT EXECUTE ON LOANAPP.LOANAUTHDOCCSP TO POOLSECADMINROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

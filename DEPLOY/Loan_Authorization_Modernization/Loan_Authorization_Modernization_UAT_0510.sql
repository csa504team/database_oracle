define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0510
define package_buildtime=20210510190744
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0510 created on Mon 05/10/2021 19:07:46.05 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0510 created on Mon 05/10/2021 19:07:46.05 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0510: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANPROCDSUBTYPTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\LOANGNTYPROCDSUBTYPTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\PROCTXTBLCK.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\LOANPROCDSUBTYPLKUPTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANPROCDSUBTYPIDSEQ.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\LOANGNTYPROCDSUBTYPID_SEQ.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPDELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPINSTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPSUMTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYPROCDCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPDELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPINSTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPSUMTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANPROCDSUBTYPTBL.sql"
CREATE TABLE LOANAPP.LOANPROCDSUBTYPTBL
(
  LOANPROCDSUBTYPID   NUMBER                    NOT NULL,
  LOANAPPNMB          NUMBER(10)                NOT NULL,
  LOANPROCDTYPCD      CHAR(3 BYTE)              NOT NULL,
  LOANPROCDTEXTBLOCK  VARCHAR2(500 BYTE),
  LOANPROCDAMT        NUMBER(19,4),
  LOANPROCDADDR       VARCHAR2(500 BYTE),
  PROCDSUBTYLKUPID    NUMBER(10),
  LOANPROCDDESC       VARCHAR2(200 BYTE),
  CREATUSERID         CHAR(15 BYTE)             DEFAULT user,
  CREATDT             DATE                      DEFAULT sysdate,
  LASTUPDTUSERID      CHAR(15 BYTE)             DEFAULT user,
  LASTUPDTDT          DATE                      DEFAULT sysdate
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


--  There is no statement for index LOANAPP.SYS_C00212673.
--  The object is created when the parent object is created.

ALTER TABLE LOANAPP.LOANPROCDSUBTYPTBL ADD (
  PRIMARY KEY
  (LOANPROCDSUBTYPID)
  USING INDEX
    TABLESPACE LOANDATATBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);


GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LANAREAD;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LANAUPDATE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOAN;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANACCT;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANAPPLOOKUPUPDROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANAPPUPDAPPROLE;


GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANLOOKUPUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANREADALLROLE;

GRANT INSERT, SELECT, UPDATE ON LOANAPP.LOANPROCDSUBTYPTBL TO LOANUPDROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOANAPP.LOANPROCDSUBTYPTBL TO SBACDTBLUPDT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\LOANGNTYPROCDSUBTYPTBL.sql"
CREATE TABLE LOAN.LOANGNTYPROCDSUBTYPTBL
(
  LOANAPPNMB             NUMBER(10)             NOT NULL,
  LOANPROCDTYPCD         CHAR(3 BYTE)           NOT NULL,
  LOANGNTYPROCDSUBTYPID  NUMBER                 NOT NULL,
  LOANPROCDTEXTBLOCK     VARCHAR2(500 BYTE),
  LOANPROCDAMT           NUMBER(19,4),
  LOANPROCDADDR          VARCHAR2(500 BYTE),
  PROCDSUBTYLKUPID       NUMBER(10),
  LOANPROCDDESC          VARCHAR2(200 BYTE),
  CREATUSERID            CHAR(15 BYTE)          DEFAULT user,
  CREATDT                DATE                   DEFAULT sysdate,
  LASTUPDTUSERID         CHAR(15 BYTE)          DEFAULT user,
  LASTUPDTDT             DATE                   DEFAULT sysdate
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


--  There is no statement for index LOAN.SYS_C00215261.
--  The object is created when the parent object is created.

ALTER TABLE LOAN.LOANGNTYPROCDSUBTYPTBL ADD (
  PRIMARY KEY
  (LOANGNTYPROCDSUBTYPID)
  USING INDEX
    TABLESPACE LOANDATATBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);


GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANORIGHQOVERRIDE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANSERVSBICGOV;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\PROCTXTBLCK.sql"
CREATE TABLE SBAREF.PROCTXTBLCK
(
  LOANPROCDTYPCD  CHAR(3 BYTE)                  NOT NULL,
  TEXTBLOCK       VARCHAR2(255 BYTE)            NOT NULL
)
TABLESPACE SBAREFDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


--  There is no statement for index SBAREF.SYS_C00212676.
--  The object is created when the parent object is created.

ALTER TABLE SBAREF.PROCTXTBLCK ADD (
  PRIMARY KEY
  (LOANPROCDTYPCD)
  USING INDEX
    TABLESPACE SBAREFDATATBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCTXTBLCK TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO GPTS;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOAN;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANACCTREADALLROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANAPP;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANAPPREADALLROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANAPPUPDAPPROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANORIGBORROWER;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANPOSTSERVSUROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANREADALLROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO LOANUPDROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO PERSONNELREADROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO POOLSECADMINROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO PUBLICREADROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCTXTBLCK TO SBACDUPDTROLE;

GRANT SELECT ON SBAREF.PROCTXTBLCK TO SBAREFREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCTXTBLCK TO SBAREFUPDCDROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCTXTBLCK TO UPDCDROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\LOANPROCDSUBTYPLKUPTBL.sql"
CREATE TABLE SBAREF.LOANPROCDSUBTYPLKUPTBL
(
  PROCDSUBTYLKUPID    NUMBER(10)                NOT NULL,
  PROCDTYPCD          CHAR(3 BYTE)              NOT NULL,
  PROCDSUBTYPDESCTXT  VARCHAR2(80 BYTE)         NOT NULL
)
TABLESPACE SBAREFDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX SBAREF.LOANPROCDSUBTYPLKUPCDUCIND ON SBAREF.LOANPROCDSUBTYPLKUPTBL
(PROCDSUBTYLKUPID, PROCDTYPCD, PROCDSUBTYPDESCTXT)
LOGGING
TABLESPACE SBAREFINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO GPTS;

GRANT REFERENCES, SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOAN;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANACCTREADALLROLE;

GRANT REFERENCES, SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANAPP;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANORIGBORROWER;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANREADALLROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO LOANUPDROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO PERSONNELREADROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO POOLSECADMINROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO PUBLICREADROLE;

GRANT SELECT, UPDATE ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO SBACDUPDTROLE;

GRANT SELECT ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO SBAREFREADALLROLE;

GRANT SELECT, UPDATE ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO SBAREFUPDCDROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.LOANPROCDSUBTYPLKUPTBL TO UPDCDROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANPROCDSUBTYPIDSEQ.sql"
CREATE SEQUENCE LOANAPP.LOANPROCDSUBTYPIDSEQ
  START WITH 172
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  ORDER
  NOKEEP
  GLOBAL;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\LOANGNTYPROCDSUBTYPID_SEQ.sql"
CREATE SEQUENCE LOAN.LOANGNTYPROCDSUBTYPID_SEQ
  START WITH 4
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER
  NOKEEP
  GLOBAL;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPDELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDSUBTYPDELTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanProcdSubTypId 	NUMBER:=0
)
AS
v_LOANAPPNMB NUMBER :=0;
v_LOANPROCDTYPCD CHAR(3) := NULL;

BEGIN
	SAVEPOINT LOANGNTYPROCDSUBTYPDELTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
        select LOANAPPNMB, LOANPROCDTYPCD into v_LOANAPPNMB, v_LOANPROCDTYPCD from LOAN.LOANGNTYPROCDSUBTYPTBL where LOANGNTYPROCDSUBTYPID = p_LoanProcdSubTypId;
		DELETE  Loan.LOANGNTYPROCDSUBTYPTBL
		WHERE LOANGNTYPROCDSUBTYPID = p_LoanProcdSubTypId;
	LOANGNTYPROCDSUBTYPSUMTSP(v_LOANAPPNMB,v_LOANPROCDTYPCD);
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANGNTYPROCDSUBTYPDELTSP;
RAISE;
END;
END LOANGNTYPROCDSUBTYPDELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPDELTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDSUBTYPINSTSP(
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LOANPROCDSUBTYPID       OUT  NUMBER,
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_ProcdSubTyLkupId		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS

    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCTXTBLCK where LOANPROCDTYPCD=p_LoanProcdTypcd; --template
    cursor desc_cur is select PROCDSUBTYPDESCTXT from SBAREF.LOANPROCDSUBTYPLKUPTBL where PROCDSUBTYLKUPID = p_ProcdSubTyLkupId; --insert into template
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_PROCDSUBTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;

BEGIN
SAVEPOINT LOANGNTYPROCDSUBTYPINSTSP;
/* Insert into LOANGNTYPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_PROCDSUBTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_PROCDSUBTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_PROCDSUBTYPDESCTXT, 0, INSTR(v_PROCDSUBTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_PROCDSUBTYPDESCTXT,INSTR(v_PROCDSUBTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
        
        BEGIN
            select LOAN.LOANGNTYPROCDSUBTYPID_SEQ.nextval into p_LOANPROCDSUBTYPID from dual;
            INSERT INTO Loan.LOANGNTYPROCDSUBTYPTBL (LOANGNTYPROCDSUBTYPID,
                                            LOANAPPNMB,
                                            LOANPROCDTYPCD,
                                            LOANPROCDTEXTBLOCK,
                                            LOANPROCDAMT,
					    LoanProcdAddr,	
					    LoanProcdDesc,	
					    ProcdSubTyLkupId,
					    Lender)
                SELECT 	p_LOANPROCDSUBTYPID,
                       	p_LoanAppNmb,
                       	p_LoanProcdTypcd,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LoanProcdDesc,
			p_ProcdSubTyLkupId,
			p_Lender
                  FROM DUAL;
            LOANGNTYPROCDSUBTYPSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDSUBTYPINSTSP;
            RAISE;
        END;
END LOANGNTYPROCDSUBTYPINSTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPINSTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDSUBTYPSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANPROCDSUBTYPSELTSP;

    /* Select from LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANGNTYPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANGNTYPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                AND LOANPROCDTYPCD=p_LOANPROCDTYPCD 
                order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        TO_CHAR (LOANPROCDAMT, 'FM999999999999990.00') LOANPROCDAMT,
                        LoanProcdAmt ,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        Lender,
                        ProcdSubTyLkupId 
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;        
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDSUBTYPSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDSUBTYPSELTSP;
/

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDSUBTYPSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDSUBTYPSUMTSP (
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANGNTYPROCDSUBTYPSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOAN.LOANGNTYPROCDSUBTYPTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOAN.LOANGNTYPROCDTBL set LOANPROCDAMT = quan where PROCDTYPCD = SUBSTR(p_LoanProcdTypcd,1,1) and LOANPROCDTYPCD=SUBSTR(p_LoanProcdTypcd,2,2) and LOANAPPNMB=p_LoanAppNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDSUBTYPSUMTSP;
            RAISE;
        END;
END LOANGNTYPROCDSUBTYPSUMTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSUMTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSUMTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSUMTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSUMTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSUMTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOAN.LOANGNTYPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = SUBSTR(p_LoanProcdTypcd,1,1) and LOANPROCDTYPCD=SUBSTR(p_LoanProcdTypcd,2,2) ;        
                        	
cursor check_rec_1
    is
        SELECT sum(LOANPROCDAMT) As TotalAmount from LOAN.LOANGNTYPROCDSUBTYPTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;         	
                        	
BEGIN
    SAVEPOINT LOANGNTYPROCDTOTAMTSELTSP;

    /* Select from LOAN.LOANGNTYPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDTOTAMTSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYPROCDCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.MOVELOANGNTYPROCDCSP(
p_Identifier 	IN NUMBER  := 0,
p_LoanAppNmb 	IN NUMBER  := 0,
p_RetVal  	OUT NUMBER,
p_RetStat 	OUT NUMBER,
p_RetTxt 	OUT VARCHAR2)
AS
/*     Parameters   :  No.   Name        Type     Description
                    1     p_Identifier  int     Identifies which batch
                                               to execute.
                    2     p_LoanAppNmb  int     LoanApplication Number to be moved
                    3     p_CreatUserId char(15)User Id
                    4     p_RetVal      int     No. of rows returned/
                                               affected
                    5     p_RetStat     int     The return value for the update
                    6     p_RetTxt      varchar return value text
*******************
*/
--NK  -- 08/29/2016--OPMSDEV 1099-- added LoanProcdRefDescTxt,LoanProcdPurAgrmtDt,LoanProcdPurIntangAssetAmt,LoanProcdPurIntangAssetDesc,LoanProcdPurStkHldrNm
--RY-- 10/03/2016 ---OPSMDEV 1177---Added NCAIncldInd,StkPurCorpText
BEGIN
--	savepoint moveloangntyprocdcsp;
/* Insert into LoanGntyProcd Table */
		IF  p_Identifier = 0 
		THEN
		
			BEGIN
				
				INSERT INTO LoanGntyProcdTbl (LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText    
                )
				SELECT  LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,
                LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText     

				FROM loanapp.LoanProcdTbl
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				
	/* Insert into LOANGNTYPROCDSUBTYPTBL Table */			
		INSERT INTO LOAN.LOANGNTYPROCDSUBTYPTBL (
                	LOANAPPNMB, LOANPROCDTYPCD, LOANGNTYPROCDSUBTYPID, LOANPROCDTEXTBLOCK, LOANPROCDAMT, LoanProcdAddr, LoanProcdDesc, ProcdSubTyLkupId) 
				SELECT  LOANAPPNMB, LOANPROCDTYPCD, LOANPROCDSUBTYPID, LOANPROCDTEXTBLOCK, LOANPROCDAMT, LoanProcdAddr, LoanProcdDesc, ProcdSubTyLkupId     

				FROM loanapp.LOANPROCDSUBTYPTBL
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				p_RetStat := SQLCODE;
				p_RetVal :=  SQL%ROWCOUNT;
	
				IF  p_RetStat <> 0 
				THEN
					BEGIN
		
						p_RetTxt :=  'LoanGntyProcdTbl not copied';
		
					END;
				END IF;
		        END;
		END IF;
p_RetVal := nvl(p_RetVal,0);
        p_RetStat := nvl(p_RetStat,0);		
/*exception when others then
begin
    raise;
    rollback to moveloangntyprocdcsp;
end;	*/
END MOVELOANGNTYPROCDCSP;
/


GRANT EXECUTE ON LOAN.MOVELOANGNTYPROCDCSP TO CDCONLINEREADALLROLE;


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.EXTRACTSELCSP (p_LoanAppNmb   IN     NUMBER := NULL,
                                                p_LoanNmb      IN     VARCHAR2 := NULL,
                                                p_RetVal          OUT NUMBER,
                                                p_SelCur1         OUT SYS_REFCURSOR,
                                                p_SelCur2         OUT SYS_REFCURSOR,
                                                p_SelCur3         OUT SYS_REFCURSOR,
                                                p_SelCur4         OUT SYS_REFCURSOR,
                                                p_SelCur5         OUT SYS_REFCURSOR,
                                                p_SelCur6         OUT SYS_REFCURSOR,
                                                p_SelCur7         OUT SYS_REFCURSOR,
                                                p_SelCur8         OUT SYS_REFCURSOR,
                                                p_SelCur9         OUT SYS_REFCURSOR,
                                                p_SelCur10        OUT SYS_REFCURSOR,
                                                p_SelCur11        OUT SYS_REFCURSOR,
                                                p_SelCur12        OUT SYS_REFCURSOR,
                                                p_SelCur13        OUT SYS_REFCURSOR,
                                                p_SelCur14        OUT SYS_REFCURSOR,
                                                p_SelCur15        OUT SYS_REFCURSOR,
                                                p_SelCur16        OUT SYS_REFCURSOR,
                                                p_SelCur17        OUT SYS_REFCURSOR,
                                                p_SelCur18        OUT SYS_REFCURSOR,
                                                p_SelCur19        OUT SYS_REFCURSOR,
                                                p_SelCur20        OUT SYS_REFCURSOR,
                                                p_SelCur21        OUT SYS_REFCURSOR,
                                                p_SelCur22        OUT SYS_REFCURSOR,
                                                p_SelCur23        OUT SYS_REFCURSOR,
                                                p_SelCur24        OUT SYS_REFCURSOR,
                                                p_SelCur25        OUT SYS_REFCURSOR,
                                                p_SelCur26        OUT SYS_REFCURSOR,
                                                p_SelCur27        OUT SYS_REFCURSOR,
                                                p_SelCur28        OUT SYS_REFCURSOR) AS
    /*SB -- This procedure created for Extract XML */
    /*SB -- Added p_selcur18 on 02/25/2015 */
    /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met, Replaced the initialization block to check for correct loannmb and loanappnmb*/
    /*NK -- 01/12/2017 Added  p_SELCUR20 and p_SELCUR21*/
    /*JP -- 01/30/2017 CAFSOPER-1341 Changed p_SelCur19�s procedure from LOANINFOEXTRACTCSP to LOANINFOEXTRACTSELCSP. (Added "SEL".)
    RY -- 03/17/2017 --OPSMDEV --1401-- Added P_SELCUR22
    BR -- 05/26/2017 --OPSMDEV-1434 -- Added P_SelCur23 Loan.LoanExprtCntrySelTSP
    SS--07/19/2018--OPSMDEV--1861-- Added  P_SELCUR24 and  P_SELCUR25 for Agents
    JP - CAfsoper-2988 08/08/2019 - Modified call to LoanGntyGuarSelTSP
    RY:: 12/16/2019- OPSMDEV-2347 :: Added LOAN.DEFRMNTHISTRYSELTSP to the list
    -- JP 8/21/2020 CARESACT 621 added call to LOAN.LoanHldStatSelTSP
    */
    --
    v_LoanNmb      CHAR (10);
    v_LoanAppNmb   NUMBER;
    v_RetVal       NUMBER;
BEGIN
    -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
    BEGIN
        IF (   p_LoanAppNmb IS NOT NULL
            OR p_LoanNmb IS NOT NULL) THEN
            SELECT LoanAppNmb, LoanNmb
            INTO v_LoanAppNmb, v_LoanNmb
            FROM loan.LoanGntyTbl
            WHERE     LoanAppNmb = NVL (p_LoanAppNmb, LoanAppNmb)
                  AND LoanNmb = NVL (p_LoanNmb, LoanNmb);
        ELSE
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
    END;



    IF v_LoanAppNmb IS NULL THEN
        BEGIN
            p_RetVal := 0;

            OPEN p_SelCur1 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur2 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur3 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur4 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur5 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur6 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur7 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur8 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur9 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur10 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur11 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur12 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur13 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur14 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur15 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur16 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur17 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur18 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur19 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur20 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur21 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur22 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur23 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur24 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur25 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;
            
            OPEN p_SelCur28 FOR
                SELECT v_RetVal
                  FROM DUAL
                 WHERE 1 = 0;
        END;
    ELSE
        BEGIN
            LoanGntySelTSP (
                38,
                v_LoanAppNmb,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                P_RetVal,
                p_SelCur1);
            LoanAssocSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur2);
            LoanGntyBorrSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur3);
            LoanGntyBSSelTSP (38, v_RetVal, v_LoanAppNmb, NULL, p_SelCur4);
            LoanGntyCollatSelTSP (38, v_LoanAppNmb, NULL, p_SelCur5);
            LoanGntyCollatLienSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur6);
            LoanDisbSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur7);
            LoanDisbPymtSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur8);
            /*LoanGntyGuarSelTSP (38,
                                v_LoanAppNmb,
                                NULL,
                                NULL,
                                v_RetVal,
                                p_SelCur9);*/
            -- JP modified for CAFSOPER - 2988
            LoanGntyGuarSelTSP (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SelCur9);

            LoanGntyInjctnSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur10);
            LoanIntDtlSelTSP (38, v_LoanAppNmb, p_SelCur11);
            LoanGntyPartLendrSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur12);

            LoanGntyPrinSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur13);
            LoanGntyRaceSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
            LoanGntyProcdSelTSP (38, NULL, v_LoanAppNmb, NULL, v_RetVal, p_SelCur15);
            LoanGntyProcdSubTypSelTSP (38, p_LoanAppNmb, NULL, p_SelCur28, v_RetVal);
            LoanGntyPrtCmntSelTSP (38, v_LoanAppNmb, v_RetVal, p_SelCur16);
            LoanGntySpcPurpsSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur17);
            LoanGntyStbyAgrmtDtlSelTSP (38, v_LoanAppNmb, NULL, p_SELCUR20, v_RETVAL);
            LoanLmtGntyCollatSelTSP (38, v_LOANAPPNMB, NULL, NULL, NULL, NULL, P_SELCUR21, v_RETVAL);

            LoanGntyEconDevObjctChldSelTsp (38, v_LOANAPPNMB, NULL, P_SELCUR22, v_RETVAL);
            LoanExprtCntrySelTsp (
                p_IDENTIFIER => 38,
                p_LOANAPPNMB => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SELCUR => P_SELCUR23);
            --(38, v_LOANAPPNMB, P_SELCUR23, v_RETVAL);
            LoanAgntSelTsp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR24);
            --(38, v_LoanAppNmb, NULL, p_SELCUR24, v_RetVal);


            LoanAgntfeeDtlSelTSp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR25);
            --(38, v_LoanAppNmb, NULL, p_SELCUR25, v_RetVal);

            LOAN.DEFRMNTHISTRYSELTSP (38, v_loanappnmb, NULL, v_RetVal, p_SELCUR26);
            -- JP 8/21/2020 CARESACT 621
            LOAN.LoanHldStatSelTSP (p_Identifier => 38, p_LoanAppNmb => v_LoanAppNmb, p_SelCur => p_SELCUR27);

            OPEN p_SELCUR18 FOR SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.BusRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'B'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                UNION
                                SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.PerRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'P'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                ORDER BY 1, 2;                                             /*BorrTaxId, BorrBusPerInd */

            LOAN.LOANINFOEXTRACTSELCSP (v_LoanAppNmb, v_LoanNmb, p_SelCur19);
        END;
    END IF;
END ExtractSelCSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPDELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPDELTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanProcdSubTypId 	NUMBER:=0
)
AS
v_LOANAPPNMB NUMBER :=0;
v_LOANPROCDTYPCD CHAR(3) := NULL;

BEGIN
	SAVEPOINT LOANPROCDSUBTYPDELTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
        select LOANAPPNMB, LOANPROCDTYPCD into v_LOANAPPNMB, v_LOANPROCDTYPCD from LOANAPP.LOANPROCDSUBTYPTBL where LOANPROCDSUBTYPID = p_LoanProcdSubTypId;
		DELETE  LOANPROCDSUBTYPTBL
		WHERE LoanProcdSubTypId = p_LoanProcdSubTypId;
		
        LOANPROCDSUBTYPSUMTSP(v_LOANAPPNMB,v_LOANPROCDTYPCD);
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANPROCDSUBTYPDELTSP;
RAISE;
END;
END LOANPROCDSUBTYPDELTSP;
/


GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPDELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LOANPROCDSUBTYPID       OUT  NUMBER,
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_ProcdSubTyLkupId		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCTXTBLCK where LOANPROCDTYPCD=p_LoanProcdTypcd; --template
    cursor desc_cur is select PROCDSUBTYPDESCTXT from SBAREF.LOANPROCDSUBTYPLKUPTBL where PROCDSUBTYLKUPID = p_ProcdSubTyLkupId; --insert into template
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_PROCDSUBTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
    
BEGIN
    SAVEPOINT LOANPROCDSUBTYPINSTSP;

    /* Insert into LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_PROCDSUBTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_PROCDSUBTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_PROCDSUBTYPDESCTXT, 0, INSTR(v_PROCDSUBTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_PROCDSUBTYPDESCTXT,INSTR(v_PROCDSUBTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
            
        BEGIN
            select LOANAPP.LOANPROCDSUBTYPIDSEQ.nextval into p_LOANPROCDSUBTYPID from dual;
            INSERT INTO LOANPROCDSUBTYPTBL (LOANPROCDSUBTYPID,
                                            LOANAPPNMB,
                                            LOANPROCDTYPCD,
                                            LOANPROCDTEXTBLOCK,
                                            LOANPROCDAMT,
					    LoanProcdAddr,	
					    LoanProcdDesc,	
					    ProcdSubTyLkupId,
					    Lender)
                SELECT 	p_LOANPROCDSUBTYPID,
                       	p_LoanAppNmb,
                       	p_LoanProcdTypcd,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			v_LoanProcdDesc,
			p_ProcdSubTyLkupId,
			p_Lender
                  FROM DUAL;
            LOANPROCDSUBTYPSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDSUBTYPINSTSP;
            RAISE;
        END;
END LOANPROCDSUBTYPINSTSP;
/



GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANPROCDSUBTYPSELTSP;

    /* Select from LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LoanProcdAddr,
                        LoanProcdDesc,                        
                        LOANPROCDTEXTBLOCK,
                        to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt,
                        Lender,
                        ProcdSubTyLkupId 
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDSUBTYPSELTSP;
            RAISE;
        END;
END LOANPROCDSUBTYPSELTSP;
/




GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDSUBTYPSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPSUMTSP (
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANPROCDSUBTYPSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOANAPP.LOANPROCDSUBTYPTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOANAPP.LOANPROCDTBL set LOANPROCDAMT = quan where PROCDTYPCD = SUBSTR(p_LoanProcdTypcd,1,1) and LOANPROCDTYPCD=SUBSTR(p_LoanProcdTypcd,2,2) and LOANAPPNMB=p_LoanAppNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDSUBTYPSUMTSP;
            RAISE;
        END;
END LOANPROCDSUBTYPSUMTSP;
/



GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSUMTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOANAPP.LOANPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = SUBSTR(p_LoanProcdTypcd,1,1) and LOANPROCDTYPCD=SUBSTR(p_LoanProcdTypcd,2,2) ;        
                        	
cursor check_rec_1
    is
        SELECT sum(LOANPROCDAMT) As TotalAmount from LOANAPP.LOANPROCDSUBTYPTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;         	
                        	
BEGIN
    SAVEPOINT LOANPROCDTOTAMTSELTSP;

    /* Select from LOANAPP.LOANPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANPROCDTOTAMTSELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCOPYCSP (
    p_LoanAppNmb          NUMBER := NULL,
    p_CreatUserId         VARCHAR2 := NULL,
    p_NEWLoanAppNmb   OUT NUMBER)
AS
    /*
     created by:   BJRychener
     created date: October 31, 2017
     purpose:      Loan Copy Procedure
     Revision:

    */
    /* NK--11/90/2017--OPSMDEV 1551 added PerFedAgncySDPIEInd, PerCSP60DayDelnqInd,PerLglActnInd, PerCitznShpCntNm ,PerAltPhnNmb, PerPrimEmailAdr ,PerAltEmailAdr To LoanPerTbl
       NK--11/13/2017 OPSMDEV 1551  Added BusAltPhnNmb ,  BusPrimEmailAdr ,  BusAltEmailAdr, BusCSP60DayDelnqInd, BusLglActnInd to LoanBusTbl
       NK--11/13/2017 OPSMDEV 1558  Added BusSexNatrInd,BusNonFmrSBAEmpInd,BusNonLegBrnchEmpInd,BusNonFedEmpInd,BusNonGS13EmpInd,BusNonSBACEmpInd to LoanBusTbl
       BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
       NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to LoanPrevFinanTbl
       SS 11/15/2017--OPSMDEV 1573 Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,
       LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn,LoanPrtAltCntctCellPhn,
              LoanPrtAltCntctemail, LendrAltCntctTypCd to LOANAPPPRTTBL
       SS--11/15/2017 --OPSMDEV 1566 Added LOANBUSESTDT to Loanappprojtbl and BusPrimCntctNm  to LoanBustbl
       NK-- 11/15/2017--OPSMDEV 1591 Added PeredAgncyLoanInd to LoanPerTbl
       NK--11/15/2017--OPSMDEV 1591 Added BusedAgncyLoanInd to LoanBusTbl
       NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to LoanBusTbl
       NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to LoanAppTbl
       BJR-- 02/01/2018--CAFSOPER-1407-- added UPPER() to srch names to allow records to be stored in caps and easier to find during searches.
       RY-- 04/20/2018--OPSMDEV--1786 --Added WorkrsCompInsRqrdInd to LoanBorrTbl
       SS--08/06/2018--OPSMDEV--1882-- Added LoanAGNtTbl and LoanAgntfeedtltbl
       SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to loanapptbl
       BR--10/30/2018--OPSMDEV 1885 -- added LOANAPPPROJGEOCD, LOANAPPHUBZONEIND, LOANAPPPROJLAT, LOANAPPPROJLONG
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
       SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to loanapptbl
       SS--02/18/2020 OPSMDEV 2409,2410 Added LoanAppCBRSInd,LoanAppOppZoneInd to LoanAppProjTbl
       SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to loanapptbl
       SS--09/05/2020 CARESACT 595 copying demographics from loan master tables
       SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to LoanAppProjTbl
       JS--02/11/2021 Loan Authorization Modernization. Added 7 new columns LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to LoanApp.LoanGuarTbl.
       SL-04/26/2021--OPSMDEV-2576  Addeed new column PerTAXIDCertInd to LoanPerTbl
    */

    v_NEWLoanAppNmb   NUMBER := NULL;
    v_count           NUMBER;
BEGIN
    BEGIN
        --Generate New Loan App Number using MAX(LoanAppNmb) from LoanApp.LoanAppTbl
        LoanApp.IncrmntSeqNmbCSP ('Application', v_NEWLoanAppNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanAppTbl
        INSERT INTO LoanApp.LoanAppTbl (LoanAppNmb,
                                        PrgmCd,
                                        PrcsMthdCd,
                                        LoanAppNm,
                                        LoanAppRecvDt,
                                        LoanAppEntryDt,
                                        LoanAppRqstAmt,
                                        LoanAppSBAGntyPct,
                                        LoanAppSBARcmndAmt,
                                        LoanAppRqstMatMoQty,
                                        LoanAppEPCInd,
                                        LoanAppPymtAmt,
                                        LoanAppFullAmortPymtInd,
                                        LoanAppMoIntQty,
                                        LoanAppLifInsurRqmtInd,
                                        LoanAppRcnsdrtnInd,
                                        LoanAppInjctnInd,
                                        LoanAppEWCPSnglTransPostInd,
                                        LoanAppEWCPSnglTransInd,
                                        LoanAppEligEvalInd,
                                        LoanAppNewBusCd,
                                        LoanAppDisasterCntrlNmb,
                                        LoanCollatInd,
                                        LoanAppSourcTypCd,
                                        StatCd,
                                        LoanAppStatUserId,
                                        LoanAppStatDt,
                                        LoanAppOrigntnOfcCd,
                                        LoanAppPrcsOfcCd,
                                        LoanAppAppvDt,
                                        RecovInd,
                                        LoanAppMicroLendrId,
                                        LOANDISASTRAPPNMB,
                                        LOANDISASTRAPPFEECHARGED,
                                        LOANDISASTRAPPDCSN,
                                        LOANASSOCDISASTRAPPNMB,
                                        LOANASSOCDISASTRLOANNMB,
                                        LoanAppCreatUserId,
                                        LoanAppCreatDt,
                                        LastUpdtUserId,
                                        LastUpdtDt,
                                        LOANAPPNETEARNIND,
                                        LOANAPPSERVOFCCD,
                                        LoanAppRevlInd,
                                        LOANNMB,
                                        LOANAPPFUNDDT,
                                        UNDRWRITNGBY,
                                        COHORTCD,
                                        FUNDSFISCLYR,
                                        LOANTYPIND,
                                        LOANAPPINTDTLCD,
                                        LOANPYMNINSTLMNTFREQCD,
                                        FININSTRMNTTYPIND,
                                        LOANAMTLIMEXMPTIND,
                                        LoanPymntSchdlFreq,
                                        LoanPymtRepayInstlTypCd,
                                        LOANGNTYMATSTIND,
                                        LoanGntyNoteDt,
                                        CDCServFeePct,
                                        LoanAppContribAmt,
                                        LoanAppContribPct,
                                        LoanAppCDCGntyAmt,
                                        LOANAPPCDCNETDBENTRAMT,
                                        LoanAppCDCFundFeeAmt,
                                        LOANAPPCDCSEPRTPRCSFEEIND,
                                        LoanAppCDCPrcsFeeAmt,
                                        LoanAppCDCClsCostAmt,
                                        LoanAppCDCUndrwtrFeeAmt,
                                        LoanAppCDCGrossDbentrAmt,
                                        LoanAppBalToBorrAmt,
                                        LoanAppOutPrgrmAreaOfOperInd,
                                        LoanAppRevlMoIntQty,
                                        LoanAppAmortMoIntQty,
                                        LoanAppExtraServFeeAMT,
                                        LoanAppExtraServFeeInd,
                                        RqstSeqNmb,
                                        LoanAppMonthPayroll)
            SELECT v_NEWLoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   LoanAppNm,
                   NULL,                                      --LoanAppRecvDt,
                   SYSDATE,                                  --LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   LoanAppEligEvalInd,
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   LoanCollatInd,
                   LoanAppSourcTypCd,
                   'IP',
                   p_CreatUserId,                         --LoanAppStatUserId,
                   SYSDATE,                                   --LoanAppStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   NULL,
                   RecovInd,
                   LoanAppMicroLendrId,
                   NULL,                                  --LOANDISASTRAPPNMB,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   p_CreatUserId,                        --LoanAppCreatUserId,
                   SYSDATE,                                  --LoanAppCreatDt,
                   p_CreatUserId,                            --LastUpdtUserId,
                   SYSDATE,                                      --LastUpdtDt,
                   LOANAPPNETEARNIND,
                   LOANAPPSERVOFCCD,
                   LoanAppRevlInd,
                   NULL,                                            --LOANNMB,
                   NULL,                                      --LOANAPPFUNDDT,
                   UNDRWRITNGBY,
                   NULL,                                           --COHORTCD,
                   NULL,                                       --FUNDSFISCLYR,
                   LOANTYPIND,
                   LOANAPPINTDTLCD,
                   LOANPYMNINSTLMNTFREQCD,
                   FININSTRMNTTYPIND,
                   LOANAMTLIMEXMPTIND,
                   LoanPymntSchdlFreq,
                   LoanPymtRepayInstlTypCd,
                   LOANGNTYMATSTIND,
                   NULL,                                     --LoanGntyNoteDt,
                   CDCServFeePct,
                   LoanAppContribAmt,
                   LoanAppContribPct,
                   LoanAppCDCGntyAmt,
                   LOANAPPCDCNETDBENTRAMT,
                   LoanAppCDCFundFeeAmt,
                   LOANAPPCDCSEPRTPRCSFEEIND,
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppCDCGrossDbentrAmt,
                   LoanAppBalToBorrAmt,
                   LoanAppOutPrgrmAreaOfOperInd,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   RqstSeqNmb,
                   LoanAppMonthPayroll
              FROM LoanApp.LoanAppTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppProjTbl
        INSERT INTO LoanApp.LoanAppProjTbl (LoanAppNmb,
                                            LoanAppProjStr1Nm,
                                            LoanAppProjStr2Nm,
                                            LoanAppProjCtyNm,
                                            LoanAppProjCntyCd,
                                            LoanAppProjStCd,
                                            LoanAppProjZipCd,
                                            LoanAppProjZip4Cd,
                                            LoanAppRuralUrbanInd,
                                            LoanAppNetExprtAmt,
                                            LoanAppCurrEmpQty,
                                            LoanAppJobCreatQty,
                                            LoanAppJobRtnd,
                                            LoanAppJobRqmtMetInd,
                                            LoanAppCDCJobRat,
                                            LoanAppProjCreatUserId,
                                            LoanAppProjCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            NAICSYrNmb,
                                            NAICSCd,
                                            LoanAppFrnchsCd,
                                            LoanAppFrnchsNm,
                                            LoanAppFrnchsInd,
                                            ADDTNLLOCACQLMTIND,
                                            FIXASSETACQLMTIND,
                                            FIXASSETACQLMTAMT,
                                            COMPSLMTIND,
                                            COMPSLMTAMT,
                                            BULKSALELAWCOMPLYIND,
                                            FRNCHSRECRDACCSIND,
                                            FRNCHSFEEDEFMONNMB,
                                            FRNCHSFEEDEFIND,
                                            FRNCHSTERMNOTCIND,
                                            FRNCHSLENDRSAMEOPPIND,
                                            LOANBUSESTDT,
                                            LOANAPPPROJGEOCD,
                                            LOANAPPHUBZONEIND,
                                            LOANAPPPROJLAT,
                                            LOANAPPPROJLONG,
                                            LoanAppCBRSInd,
                                            LoanAppOppZoneInd,
                                            LOANAPPPROJLMIND,
                                            LOANAPPPROJSIZECD)
            SELECT v_NEWLoanAppNmb,
                   LoanAppProjStr1Nm,
                   LoanAppProjStr2Nm,
                   LoanAppProjCtyNm,
                   LoanAppProjCntyCd,
                   LoanAppProjStCd,
                   LoanAppProjZipCd,
                   LoanAppProjZip4Cd,
                   LoanAppRuralUrbanInd,
                   LoanAppNetExprtAmt,
                   LoanAppCurrEmpQty,
                   LoanAppJobCreatQty,
                   LoanAppJobRtnd,
                   LoanAppJobRqmtMetInd,
                   LoanAppCDCJobRat,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NAICSYrNmb,
                   NAICSCd,
                   LoanAppFrnchsCd,
                   LoanAppFrnchsNm,
                   LoanAppFrnchsInd,
                   ADDTNLLOCACQLMTIND,
                   FIXASSETACQLMTIND,
                   FIXASSETACQLMTAMT,
                   COMPSLMTIND,
                   COMPSLMTAMT,
                   BULKSALELAWCOMPLYIND,
                   FRNCHSRECRDACCSIND,
                   FRNCHSFEEDEFMONNMB,
                   FRNCHSFEEDEFIND,
                   FRNCHSTERMNOTCIND,
                   FRNCHSLENDRSAMEOPPIND,
                   LOANBUSESTDT,
                   LOANAPPPROJGEOCD,
                   LOANAPPHUBZONEIND,
                   LOANAPPPROJLAT,
                   LOANAPPPROJLONG,
                   LoanAppCBRSInd,
                   LoanAppOppZoneInd,
                   LOANAPPPROJLMIND,
                   LOANAPPPROJSIZECD
              FROM LoanApp.LoanAppProjTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanInjctnTbl
        INSERT INTO LoanApp.LoanInjctnTbl (LoanAppNmb,
                                           LoanInjctnSeqNmb,
                                           InjctnTypCd,
                                           LoanInjctnOthDescTxt,
                                           LoanInjctnAmt,
                                           LoanInjctnCreatUserId,
                                           LoanInjctnCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanInjctnFundTermYrNmb)
            SELECT v_NEWLoanAppNmb,
                   LoanInjctnSeqNmb,
                   InjctnTypCd,
                   LoanInjctnOthDescTxt,
                   LoanInjctnAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanInjctnFundTermYrNmb
              FROM LoanApp.LoanInjctnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanStbyAgrmtDtlTBL
        INSERT INTO LoanApp.LoanStbyAgrmtDtlTBL (LOANSTBYAGRMTDTLSEQNMB,
                                                 LOANAPPNMB,
                                                 LOANSTBYCRDTRNM,
                                                 LOANSTBYAMT,
                                                 LOANSTBYREPAYTYPCD,
                                                 LOANSTBYPYMTINTRT,
                                                 LOANSTBYPYMTOTHDESCTXT,
                                                 LOANSTBYPYMTAMT,
                                                 LOANSTBYPYMTSTRTDT,
                                                 CREATUSERID,
                                                 CREATDT,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
            SELECT LOANSTBYAGRMTDTLSEQNMB,
                   v_NEWLoanAppNmb,
                   LOANSTBYCRDTRNM,
                   LOANSTBYAMT,
                   LOANSTBYREPAYTYPCD,
                   LOANSTBYPYMTINTRT,
                   LOANSTBYPYMTOTHDESCTXT,
                   LOANSTBYPYMTAMT,
                   LOANSTBYPYMTSTRTDT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanStbyAgrmtDtlTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanSpcPurpsTbl
        INSERT INTO LoanApp.LoanSpcPurpsTbl (LoanAppNmb,
                                             LoanSpcPurpsSeqNmb,
                                             SpcPurpsLoanCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanSpcPurpsSeqNmb,
                   SpcPurpsLoanCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanSpcPurpsTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppARCRsnTbl
        INSERT INTO LoanApp.LoanAppARCRsnTbl (LoanAppNmb,
                                              ARCLoanRsnCd,
                                              ARCLoanRsnOthDescTxt,
                                              CreateUserId,
                                              CreateDt,
                                              LASTUPDTUSERID,
                                              LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   ARCLoanRsnCd,
                   ARCLoanRsnOthDescTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAppARCRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCrdtUnavRsnTbl
        INSERT INTO LoanApp.LoanCrdtUnavRsnTbl (LoanAppNmb,
                                                LoanCrdtUnavRsnSeqNmb,
                                                LoanCrdtUnavRsnCd,
                                                LoanCrdtUnavRsnTxt,
                                                LoanCrdtUnavRsnCreatUserId,
                                                LoanCrdtUnavRsnCreatDt,
                                                LastUpdtUserId,
                                                LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanCrdtUnavRsnSeqNmb,
                   LoanCrdtUnavRsnCd,
                   LoanCrdtUnavRsnTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanCrdtUnavRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanProcdTbl
        INSERT INTO LoanApp.LoanProcdTbl (LoanAppNmb,
                                          LoanProcdSeqNmb,
                                          LoanProcdTypCd,
                                          ProcdTypCd,
                                          LoanProcdOthTypTxt,
                                          LoanProcdAmt,
                                          LoanProcdCreatUserId,
                                          LoanProcdCreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LoanProcdRefDescTxt,
                                          LoanProcdPurAgrmtDt,
                                          LoanProcdPurIntangAssetAmt,
                                          LoanProcdPurIntangAssetDesc,
                                          LoanProcdPurStkHldrNm,
                                          NCAIncldInd,
                                          StkPurCorpText)
            SELECT v_NEWLoanAppNmb,
                   LoanProcdSeqNmb,
                   LoanProcdTypCd,
                   ProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanApp.LoanProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;


    
BEGIN
        --Copy to LoanApp.LoanProcdSubTypTbl
        INSERT INTO LoanApp.LoanProcdSubTypTbl    (    LoanProcdSubTypId,
                                                       LoanAppNmb,
                                                       LoanProcdTypCd,
                                                       LoanProcdTextBlock,
                                                       LoanProcdAmt,
						       LoanProcdAddr,
							LoanProcdDesc,	
						       ProcdSubTyLkupId)  
            SELECT  LoanApp.LoanProcdSubTypIdSeq.nextval,
                    v_NEWLoanAppNmb,
                    LoanProcdTypCd,
                    LoanProcdTextBlock,
                    LoanProcdAmt,
		    LoanProcdAddr,
		    LoanProcdDesc,	
		    ProcdSubTyLkupId
              FROM LoanApp.LoanProcdSubTypTbl
             WHERE  LoanAppNmb = p_LoanAppNmb;
    END;



    BEGIN
        --Copy to LoanApp.LoanAppChngBusOwnrshpTbl
        INSERT INTO LoanApp.LoanAppChngBusOwnrshpTbl (
                        LOANAPPNMB,
                        TOTPYMTAMT,
                        LOAN7APYMTAMT,
                        SELLERFINANFULLSTBYAMT,
                        SELLERFINANNONFULLSTBYAMT,
                        BUYEREQTYCASHAMT,
                        BUYEREQTYCASHDESCTXT,
                        BUYEREQTYBORRAMT,
                        BUYEREQTYOTHAMT,
                        BUYEREQTYOTHDESCTXT,
                        TOTASSETAMT,
                        ACCTRECVASSETAMT,
                        INVTRYASSETAMT,
                        REASSETAMT,
                        REVALTYPIND,
                        EQUIPASSETAMT,
                        EQUIPVALTYPIND,
                        FIXASSETAMT,
                        INTANGASSETAMT,
                        OTHASSETAMT,
                        OTHASSETDESCTXT,
                        COVNTASSETAMT,
                        CUSTASSETAMT,
                        LICNSASSETAMT,
                        FRNCHSASSETAMT,
                        GOODWILLASSETAMT,
                        OTHINTANGASSETAMT,
                        OTHINTANGASSETDESCTXT,
                        TOTAPPRAMT,
                        BUSAPPRNM,
                        BUSAPPRFEEAMT,
                        BUSAPPRASAIND,
                        BUSAPPRCBAIND,
                        BUSAPPRABVIND,
                        BUSAPPRCVAIND,
                        BUSAPPRAVAIND,
                        BUSAPPRCPAIND,
                        BUSBRKRCOMISNIND,
                        BUSBRKRNM,
                        BUSBRKRCOMISNAMT,
                        BUSBRKRADR,
                        CREATUSERID,
                        CREATDT,
                        LASTUPDTUSERID,
                        LASTUPDTDT,
                        BUSAPPRLIVIND)
            SELECT v_NEWLoanAppNmb,
                   TOTPYMTAMT,
                   LOAN7APYMTAMT,
                   SELLERFINANFULLSTBYAMT,
                   SELLERFINANNONFULLSTBYAMT,
                   BUYEREQTYCASHAMT,
                   BUYEREQTYCASHDESCTXT,
                   BUYEREQTYBORRAMT,
                   BUYEREQTYOTHAMT,
                   BUYEREQTYOTHDESCTXT,
                   TOTASSETAMT,
                   ACCTRECVASSETAMT,
                   INVTRYASSETAMT,
                   REASSETAMT,
                   REVALTYPIND,
                   EQUIPASSETAMT,
                   EQUIPVALTYPIND,
                   FIXASSETAMT,
                   INTANGASSETAMT,
                   OTHASSETAMT,
                   OTHASSETDESCTXT,
                   COVNTASSETAMT,
                   CUSTASSETAMT,
                   LICNSASSETAMT,
                   FRNCHSASSETAMT,
                   GOODWILLASSETAMT,
                   OTHINTANGASSETAMT,
                   OTHINTANGASSETDESCTXT,
                   TOTAPPRAMT,
                   BUSAPPRNM,
                   BUSAPPRFEEAMT,
                   BUSAPPRASAIND,
                   BUSAPPRCBAIND,
                   BUSAPPRABVIND,
                   BUSAPPRCVAIND,
                   BUSAPPRAVAIND,
                   BUSAPPRCPAIND,
                   BUSBRKRCOMISNIND,
                   BUSBRKRNM,
                   BUSBRKRCOMISNAMT,
                   BUSBRKRADR,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   BUSAPPRLIVIND
              FROM LoanApp.LoanAppChngBusOwnrshpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPrtCmntTbl
        INSERT INTO LoanApp.LoanPrtCmntTbl (LoanAppNmb,
                                            LoanPrtCmntTxt,
                                            LoanPrtCmntCreatUserId,
                                            LoanPrtCmntCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanPrtCmntTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM loanApp.LoanPrtCmntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanEconDevObjctChldTbl
        INSERT INTO LoanApp.LoanEconDevObjctChldTbl (LoanEconDevObjctSeqNmb,
                                                     LoanAppNmb,
                                                     LoanEconDevObjctCd,
                                                     CreatUserId,
                                                     CreatDt,
                                                     LastUpdtUserId,
                                                     LastUpdtDt)
            SELECT LoanEconDevObjctSeqNmb,
                   v_NEWLoanAppNmb,
                   LoanEconDevObjctCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanEconDevObjctChldTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCollatTbl
        INSERT INTO LoanApp.LoanCollatTbl (loanappnmb,
                                           loancollatseqnmb,
                                           loancollattypcd,
                                           loancollatdesc,
                                           loancollatmrktvalamt,
                                           loancollatownrrecrd,
                                           loancollatsbalienpos,
                                           loancollatvalsourccd,
                                           loancollatvaldt,
                                           loancollatcreatuserid,
                                           loancollatcreatdt,
                                           lastupdtuserid,
                                           lastupdtdt,
                                           collatstatcd,
                                           collatsecsrchorddt,
                                           collatsecsrchrcvddt,
                                           collatapporddt,
                                           collatstr1nm,
                                           collatstr2nm,
                                           collatctynm,
                                           collatcntycd,
                                           collatstcd,
                                           collatzipcd,
                                           collatzip4cd,
                                           LoanCollatSubTypCd,
                                           SecurShrPariPassuInd,
                                           SecurShrPariPassuNonSBAInd,
                                           SecurPariPassuLendrNm,
                                           SecurPariPassuAmt,
                                           SecurLienLimAmt,
                                           SecurInstrmntTypCd,
                                           SecurWaterRightInd,
                                           SecurRentAsgnInd,
                                           SecurSellerNm,
                                           SecurPurNm,
                                           SecurOwedToSellerAmt,
                                           SecurCDCDeedInEscrowInd,
                                           SecurSellerIntDtlInd,
                                           SecurSellerIntDtlTxt,
                                           SecurTitlSubjPriorLienInd,
                                           SecurPriorLienTxt,
                                           SecurPriorLienLimAmt,
                                           SecurSubjPriorAsgnInd,
                                           SecurPriorAsgnTxt,
                                           SecurPriorAsgnLimAmt,
                                           SecurALTATltlInsurInd,
                                           SecurLeaseTermOverLoanYrNmb,
                                           LandlordIntProtWaiverInd,
                                           SecurDt,
                                           SecurLessorTermNotcDaysNmb,
                                           SecurDescTxt,
                                           SecurPropAcqWthLoanInd,
                                           SecurPropTypTxt,
                                           SecurOthPropTxt,
                                           SecurLienOnLqorLicnsInd,
                                           SecurMadeYrNmb,
                                           SecurLocTxt,
                                           SecurOwnerNm,
                                           SecurMakeNm,
                                           SecurAmt,
                                           SecurNoteSecurInd,
                                           SecurStkShrNmb,
                                           SecurLienHldrVrfyInd,
                                           SecurTitlVrfyTypCd,
                                           SecurTitlVrfyOthTxt,
                                           FloodInsurRqrdInd,
                                           REHazardInsurRqrdInd,
                                           PerHazardInsurRqrdInd,
                                           FullMarInsurRqrdInd,
                                           EnvInvstgtSBAAppInd,
                                           LeasePrmTypCd,
                                           ApprTypCd)
            SELECT v_NEWLoanAppNmb,
                   loancollatseqnmb,
                   loancollattypcd,
                   loancollatdesc,
                   loancollatmrktvalamt,
                   loancollatownrrecrd,
                   loancollatsbalienpos,
                   loancollatvalsourccd,
                   loancollatvaldt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   collatstatcd,
                   collatsecsrchorddt,
                   collatsecsrchrcvddt,
                   collatapporddt,
                   collatstr1nm,
                   collatstr2nm,
                   collatctynm,
                   collatcntycd,
                   collatstcd,
                   collatzipcd,
                   collatzip4cd,
                   LoanCollatSubTypCd,
                   SecurShrPariPassuInd,
                   SecurShrPariPassuNonSBAInd,
                   SecurPariPassuLendrNm,
                   SecurPariPassuAmt,
                   SecurLienLimAmt,
                   SecurInstrmntTypCd,
                   SecurWaterRightInd,
                   SecurRentAsgnInd,
                   SecurSellerNm,
                   SecurPurNm,
                   SecurOwedToSellerAmt,
                   SecurCDCDeedInEscrowInd,
                   SecurSellerIntDtlInd,
                   SecurSellerIntDtlTxt,
                   SecurTitlSubjPriorLienInd,
                   SecurPriorLienTxt,
                   SecurPriorLienLimAmt,
                   SecurSubjPriorAsgnInd,
                   SecurPriorAsgnTxt,
                   SecurPriorAsgnLimAmt,
                   SecurALTATltlInsurInd,
                   SecurLeaseTermOverLoanYrNmb,
                   LandlordIntProtWaiverInd,
                   SecurDt,
                   SecurLessorTermNotcDaysNmb,
                   SecurDescTxt,
                   SecurPropAcqWthLoanInd,
                   SecurPropTypTxt,
                   SecurOthPropTxt,
                   SecurLienOnLqorLicnsInd,
                   SecurMadeYrNmb,
                   SecurLocTxt,
                   SecurOwnerNm,
                   SecurMakeNm,
                   SecurAmt,
                   SecurNoteSecurInd,
                   SecurStkShrNmb,
                   SecurLienHldrVrfyInd,
                   SecurTitlVrfyTypCd,
                   SecurTitlVrfyOthTxt,
                   FloodInsurRqrdInd,
                   REHazardInsurRqrdInd,
                   PerHazardInsurRqrdInd,
                   FullMarInsurRqrdInd,
                   EnvInvstgtSBAAppInd,
                   LeasePrmTypCd,
                   ApprTypCd
              FROM LoanApp.LoanCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

        --Copy to LoanApp.LoanCollatLienTbl
        INSERT INTO LoanApp.LoanCollatLienTbl (LoanAppNmb,
                                               LoanCollatSeqNmb,
                                               LoanCollatLienSeqNmb,
                                               LoanCollatLienHldrNm,
                                               LoanCollatLienBalAmt,
                                               LoanCollatLienPos,
                                               LoanCollatLienCreatUserId,
                                               LoanCollatLienCreatDt,
                                               LastUpdtUserId,
                                               LastUpdtDt,
                                               LOANCOLLATLIENSTATCD,
                                               LOANCOLLATLIENCMNT)
            SELECT v_NEWLoanAppNmb,
                   LoanCollatSeqNmb,
                   LoanCollatLienSeqNmb,
                   LoanCollatLienHldrNm,
                   LoanCollatLienBalAmt,
                   LoanCollatLienPos,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANCOLLATLIENSTATCD,
                   LOANCOLLATLIENCMNT
              FROM LoanCollatLienTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPartLendrTbl
        INSERT INTO LoanApp.LoanPartLendrTbl (LoanAppNmb,
                                              LoanPartLendrSeqNmb,
                                              LoanPartLendrTypCd,
                                              LoanPartLendrAmt,
                                              LocId,
                                              LoanPartLendrNm,
                                              LoanPartLendrStr1Nm,
                                              LoanPartLendrStr2Nm,
                                              LoanPartLendrCtyNm,
                                              LoanPartLendrStCd,
                                              LoanPartLendrZip5Cd,
                                              LoanPartLendrZip4Cd,
                                              LoanPartLendrOfcrLastNm,
                                              LoanPartLendrOfcrFirstNm,
                                              LoanPartLendrOfcrInitialNm,
                                              LoanPartLendrOfcrSfxNm,
                                              LoanPartLendrOfcrTitlTxt,
                                              LoanPartLendrOfcrPhnNmb,
                                              LoanLienPosCd,
                                              LoanPartLendrSrLienPosFeeAmt,
                                              LoanPoolAppNmb,
                                              LOANLENDRTAXID,
                                              LOANLENDRSERVFEEPCT,
                                              LOANLENDRPARTPCT,
                                              LOANORIGPARTPCT,
                                              LoanSBAGntyBalPct,
                                              LOANLENDRPARTAMT,
                                              LOANORIGPARTAMT,
                                              LOANLENDRGROSSAMT,
                                              LOANSBAGNTYBALAMT,
                                              Loan504PartLendrSeqNmb,
                                              LoanPartLendrCreatUserId,
                                              LoanPartLendrCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              loanpartlendrcntrycd,
                                              loanpartlendrpostcd,
                                              loanpartlendrstnm)
            SELECT v_NEWLoanAppNmb,
                   LoanPartLendrSeqNmb,
                   LoanPartLendrTypCd,
                   LoanPartLendrAmt,
                   LocId,
                   LoanPartLendrNm,
                   LoanPartLendrStr1Nm,
                   LoanPartLendrStr2Nm,
                   LoanPartLendrCtyNm,
                   LoanPartLendrStCd,
                   LoanPartLendrZip5Cd,
                   LoanPartLendrZip4Cd,
                   LoanPartLendrOfcrLastNm,
                   LoanPartLendrOfcrFirstNm,
                   LoanPartLendrOfcrInitialNm,
                   LoanPartLendrOfcrSfxNm,
                   LoanPartLendrOfcrTitlTxt,
                   LoanPartLendrOfcrPhnNmb,
                   LoanLienPosCd,
                   LoanPartLendrSrLienPosFeeAmt,
                   LoanPoolAppNmb,
                   LOANLENDRTAXID,
                   LOANLENDRSERVFEEPCT,
                   LOANLENDRPARTPCT,
                   LOANORIGPARTPCT,
                   LoanSBAGntyBalPct,
                   LOANLENDRPARTAMT,
                   LOANORIGPARTAMT,
                   LOANLENDRGROSSAMT,
                   LOANSBAGNTYBALAMT,
                   Loan504PartLendrSeqNmb,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   loanpartlendrcntrycd,
                   loanpartlendrpostcd,
                   loanpartlendrstnm
              FROM LoanApp.LoanPartLendrTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    --Copy to LoanApp.LoanBusTbl
    INSERT INTO LoanApp.LoanBusTbl (LoanAppNmb,
                                    TaxId,
                                    BusEINCertInd,
                                    BusTypCd,
                                    BusNm,
                                    BusSrchNm,
                                    BusTrdNm,
                                    BusBnkrptInd,
                                    BusLwsuitInd,
                                    BusPriorSBALoanInd,
                                    BusCurBnkNm,
                                    BusChkngBalAmt,
                                    BusCurOwnrshpDt,
                                    BusPrimPhnNmb,
                                    BusPhyAddrStr1Nm,
                                    BusPhyAddrStr2Nm,
                                    BusPhyAddrCtyNm,
                                    BusPhyAddrStCd,
                                    BusPhyAddrStNm,
                                    BusPhyAddrCntCd,
                                    BusPhyAddrZipCd,
                                    BusPhyAddrZip4Cd,
                                    BusPhyAddrPostCd,
                                    BusMailAddrStr1Nm,
                                    BusMailAddrStr2Nm,
                                    BusMailAddrCtyNm,
                                    BusMailAddrStCd,
                                    BusMailAddrStNm,
                                    BusMailAddrCntCd,
                                    BusMailAddrZipCd,
                                    BusMailAddrZip4Cd,
                                    BusMailAddrPostCd,
                                    BusCreatUserId,
                                    BusCreatDt,
                                    BusExprtInd,
                                    BusExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    BusExtrnlCrdtScorNmb,
                                    BusExtrnlCrdtScorDt,
                                    BusDunsNMB,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    BusAltPhnNmb,
                                    BusPrimEmailAdr,
                                    BusAltEmailAdr,
                                    BusCSP60DayDelnqInd,
                                    BusLglActnInd,
                                    BusFedAgncySDPIEInd,
                                    BusSexNatrInd,
                                    BusNonFmrSBAEmpInd,
                                    BusNonLegBrnchEmpInd,
                                    BusNonFedEmpInd,
                                    BusNonGS13EmpInd,
                                    BusNonSBACEmpInd,
                                    BusPrimCntctNm,
                                    -- BusPrimCntctEmail,
                                    BusFedAgncyLoanInd,
                                    BusOutDbtInd)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.BusEINCertInd, a.BusEINCertInd),
               NVL (b.BusTypCd, a.BusTypCd),
               NVL (b.BusNm, a.BusNm),
               NVL (b.BusSrchNm, a.BusSrchNm),
               a.BusTrdNm,
               a.BusBnkrptInd,
               a.BusLwsuitInd,
               a.BusPriorSBALoanInd,
               a.BusCurBnkNm,
               a.BusChkngBalAmt,
               a.BusCurOwnrshpDt,
               a.BusPrimPhnNmb,
               a.BusPhyAddrStr1Nm,
               a.BusPhyAddrStr2Nm,
               a.BusPhyAddrCtyNm,
               a.BusPhyAddrStCd,
               a.BusPhyAddrStNm,
               a.BusPhyAddrCntCd,
               a.BusPhyAddrZipCd,
               a.BusPhyAddrZip4Cd,
               a.BusPhyAddrPostCd,
               a.BusMailAddrStr1Nm,
               a.BusMailAddrStr2Nm,
               a.BusMailAddrCtyNm,
               a.BusMailAddrStCd,
               a.BusMailAddrStNm,
               a.BusMailAddrCntCd,
               a.BusMailAddrZipCd,
               a.BusMailAddrZip4Cd,
               a.BusMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.BusExprtInd,
               a.BusExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.BusExtrnlCrdtScorNmb,
               a.BusExtrnlCrdtScorDt,
               a.BusDunsNMB,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               b.VETCERTIND,
               a.BusAltPhnNmb,
               a.BusPrimEmailAdr,
               a.BusAltEmailAdr,
               a.BusCSP60DayDelnqInd,
               a.BusLglActnInd,
               a.BusFedAgncySDPIEInd,
               a.BusSexNatrInd,
               a.BusNonFmrSBAEmpInd,
               a.BusNonLegBrnchEmpInd,
               a.BusNonFedEmpInd,
               a.BusNonGS13EmpInd,
               a.BusNonSBACEmpInd,
               a.BusPrimCntctNm,
               --   BusPrimCntctEmail,
               a.BusFedAgncyLoanInd,
               a.BusOutDbtInd
          FROM LoanApp.LoanBusTbl a, loan.Bustbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.taxid = b.taxid(+);


    --Copy to LoanApp.LoanPerTbl
    INSERT INTO LoanApp.LoanPerTbl (LoanAppNmb,
                                    TaxId,
                                    PerTAXIDCertInd,
                                    PerFirstNm,
                                    PerFirstSrchNm,
                                    PerLastNm,
                                    PerLastSrchNm,
                                    PerInitialNm,
                                    PerSfxNm,
                                    PerTitlTxt,
                                    PerBrthDt,
                                    PerBrthCtyNm,
                                    PerBrthStCd,
                                    PerBrthCntCd,
                                    PerBrthCntNm,
                                    PerUSCitznInd,
                                    PerAlienRgstrtnNmb,
                                    PerPndngLwsuitInd,
                                    PerAffilEmpFedInd,
                                    PerIOBInd,
                                    PerIndctPrleProbatnInd,
                                    PerCrmnlOffnsInd,
                                    PerCnvctInd,
                                    PerBnkrptcyInd,
                                    PerFngrprntWaivDt,
                                    PerPrimPhnNmb,
                                    PerPhyAddrStr1Nm,
                                    PerPhyAddrStr2Nm,
                                    PerPhyAddrCtyNm,
                                    PerPhyAddrStCd,
                                    PerPhyAddrStNm,
                                    PerPhyAddrCntCd,
                                    PerPhyAddrZipCd,
                                    PerPhyAddrZip4Cd,
                                    PerPhyAddrPostCd,
                                    PerMailAddrStr1Nm,
                                    PerMailAddrStr2Nm,
                                    PerMailAddrCtyNm,
                                    PerMailAddrStCd,
                                    PerMailAddrStNm,
                                    PerMailAddrCntCd,
                                    PerMailAddrZipCd,
                                    PerMailAddrZip4Cd,
                                    PerMailAddrPostCd,
                                    PerCreatUserId,
                                    PerCreatDt,
                                    PerExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    PerExtrnlCrdtScorNmb,
                                    PerExtrnlCrdtScorDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    PerFedAgncySDPIEInd,
                                    PerCSP60DayDelnqInd,
                                    PerLglActnInd,
                                    PerCitznShpCntNm,
                                    PerAltPhnNmb,
                                    PerPrimEmailAdr,
                                    PerAltEmailAdr,
                                    PerFedAgncyLoanInd,
                                    PerOthrCmntTxt)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.PerTAXIDCertInd, a.PerTAXIDCertInd),
               NVL (b.PerFirstNm, a.PerFirstNm),
               NVL (b.PerFirstSrchNm, a.PerFirstSrchNm),
               NVL (b.PerLastNm, a.PerLastNm),
               NVL (b.PerLastSrchNm, a.PerLastSrchNm),
               NVL (b.PerInitialNm, a.PerInitialNm),
               NVL (b.PerSfxNm, a.PerSfxNm),
               a.PerTitlTxt,
               NVL (b.PerBrthDt, a.PerBrthDt),
               NVL (b.PerBrthCtyNm, a.PerBrthCtyNm),
               NVL (b.PerBrthStCd, a.PerBrthStCd),
               NVL (b.PerBrthCntCd, a.PerBrthCntCd),
               NVL (b.PerBrthCntNm, b.PerBrthCntNm),
               a.PerUSCitznInd,
               a.PerAlienRgstrtnNmb,
               a.PerPndngLwsuitInd,
               a.PerAffilEmpFedInd,
               a.PerIOBInd,
               a.PerIndctPrleProbatnInd,
               a.PerCrmnlOffnsInd,
               a.PerCnvctInd,
               a.PerBnkrptcyInd,
               a.PerFngrprntWaivDt,
               a.PerPrimPhnNmb,
               a.PerPhyAddrStr1Nm,
               a.PerPhyAddrStr2Nm,
               a.PerPhyAddrCtyNm,
               a.PerPhyAddrStCd,
               a.PerPhyAddrStNm,
               a.PerPhyAddrCntCd,
               a.PerPhyAddrZipCd,
               a.PerPhyAddrZip4Cd,
               a.PerPhyAddrPostCd,
               a.PerMailAddrStr1Nm,
               a.PerMailAddrStr2Nm,
               a.PerMailAddrCtyNm,
               a.PerMailAddrStCd,
               a.PerMailAddrStNm,
               a.PerMailAddrCntCd,
               a.PerMailAddrZipCd,
               a.PerMailAddrZip4Cd,
               a.PerMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.PerExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.PerExtrnlCrdtScorNmb,
               a.PerExtrnlCrdtScorDt,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (b.VETCERTIND, a.VETCERTIND),
               a.PerFedAgncySDPIEInd,
               a.PerCSP60DayDelnqInd,
               a.PerLglActnInd,
               a.PerCitznShpCntNm,
               a.PerAltPhnNmb,
               a.PerPrimEmailAdr,
               a.PerAltEmailAdr,
               a.PerFedAgncyLoanInd,
               a.PerOthrCmntTxt
          FROM loanapp.loanpertbl a, loan.pertbl b
         WHERE a.taxid = b.taxid(+) AND a.loanappnmb = p_loanappnmb;


    BEGIN
        --Copy to LoanApp.LoanIndbtnesTbl
        INSERT INTO LoanApp.LoanIndbtnesTbl (LoanAppNmb,
                                             TaxId,
                                             LoanIndbtnesSeqNmb,
                                             LoanIndbtnesPayblToNm,
                                             LoanIndbtnesPurpsTxt,
                                             LoanIndbtnesOrglDt,
                                             LoanIndbtnesCurBalAmt,
                                             LoanIndbtnesIntPct,
                                             LoanIndbtnesMatDt,
                                             LoanIndbtnesPymtAmt,
                                             LoanIndbtnesPymtFreqCd,
                                             LoanIndbtnesCollatDescTxt,
                                             LoanIndbtnesPrevFinanStatCd,
                                             LoanIndbtnesCreatUserId,
                                             LoanIndbtnesCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanIndbtnesSeqNmb,
                   LoanIndbtnesPayblToNm,
                   LoanIndbtnesPurpsTxt,
                   LoanIndbtnesOrglDt,
                   LoanIndbtnesCurBalAmt,
                   LoanIndbtnesIntPct,
                   LoanIndbtnesMatDt,
                   LoanIndbtnesPymtAmt,
                   LoanIndbtnesPymtFreqCd,
                   LoanIndbtnesCollatDescTxt,
                   LoanIndbtnesPrevFinanStatCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanIndbtnesTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPerFinanTbl
        INSERT INTO LoanApp.LoanPerFinanTbl (LoanAppNmb,
                                             TaxId,
                                             PerLqdAssetAmt,
                                             PerBusOwnrshpAmt,
                                             PerREAmt,
                                             PerOthAssetAmt,
                                             PerTotAssetAmt,
                                             PerRELiabAmt,
                                             PerCCDbtAmt,
                                             PerInstlDbtAmt,
                                             PerOthLiabAmt,
                                             PerTotLiabAmt,
                                             PerNetWrthAmt,
                                             PerAnnSalaryAmt,
                                             PerOthAnnIncAmt,
                                             PerSourcOfOthIncTxt,
                                             PerResOwnRentOthInd,
                                             PerMoHsngAmt,
                                             PerCreatUserId,
                                             PerCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   PerLqdAssetAmt,
                   PerBusOwnrshpAmt,
                   PerREAmt,
                   PerOthAssetAmt,
                   PerTotAssetAmt,
                   PerRELiabAmt,
                   PerCCDbtAmt,
                   PerInstlDbtAmt,
                   PerOthLiabAmt,
                   PerTotLiabAmt,
                   PerNetWrthAmt,
                   PerAnnSalaryAmt,
                   PerOthAnnIncAmt,
                   PerSourcOfOthIncTxt,
                   PerResOwnRentOthInd,
                   PerMoHsngAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPerFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanFedEmpTbl
        INSERT INTO LOANAPP.LoanFedEmpTbl (LoanAppNmb,
                                           TaxId,
                                           LoanFedEmpSeqNmb,
                                           FedEmpFirstNm,
                                           FedEmpLastNm,
                                           FedEmpInitialNm,
                                           FedEmpSfxNm,
                                           FedEmpAddrStr1Nm,
                                           FedEmpAddrStr2Nm,
                                           FedEmpAddrCtyNm,
                                           FedEmpAddrStCd,
                                           FedEmpAddrZipCd,
                                           FedEmpAddrZip4Cd,
                                           FedEmpAgncyOfc,
                                           FedEmpCreatUserId,
                                           FedEmpCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanFedEmpSeqNmb,
                   FedEmpFirstNm,
                   FedEmpLastNm,
                   FedEmpInitialNm,
                   FedEmpSfxNm,
                   FedEmpAddrStr1Nm,
                   FedEmpAddrStr2Nm,
                   FedEmpAddrCtyNm,
                   FedEmpAddrStCd,
                   FedEmpAddrZipCd,
                   FedEmpAddrZip4Cd,
                   FedEmpAgncyOfc,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanFedEmpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.Pertbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'P'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.bustbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'B'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;



    BEGIN
        --Copy to LoanApp.LoanPrinTbl */
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.pertbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'P'
                   AND a.taxid = b.Taxid(+);
    END;

    BEGIN
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.bustbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'B'
                   AND a.taxid = b.Taxid(+);
    END;



    BEGIN
        --Copy to LoanApp.LoanBusPrinTbl
        INSERT INTO LoanApp.LoanBusPrinTbl (LoanAppNmb,
                                            BorrSeqNmb,
                                            PrinSeqNmb,
                                            LoanBusPrinPctOwnrshpBus,
                                            LoanPrinGntyInd,
                                            LoanBusPrinCreatUserId,
                                            LoanBusPrinCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            LoanCntlIntInd,
                                            LoanCntlIntTyp)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   PrinSeqNmb,
                   LoanBusPrinPctOwnrshpBus,
                   LoanPrinGntyInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanCntlIntInd,
                   LoanCntlIntTyp
              FROM LoanApp.LoanBusPrinTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.perracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'P'
               AND a.taxid = b.taxid;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.busracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'B'
               AND a.taxid = b.taxid;

    --Copy to LoanApp.LoanPrinRaceTbl
    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               RaceCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinRaceTbl a
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND NOT EXISTS
                       (SELECT 1
                          FROM loanapp.LoanPrinRaceTbl z
                         WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                               AND z.PRINSEQNMB = a.PrinSeqNmb);


    BEGIN
        --Copy to LoanApp.LoanBorrRaceTbl
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrTbl a, loan.perracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'P'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanborrTbl a, loan.busracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'B'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrRaceTbl a
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND NOT EXISTS
                           (SELECT 1
                              FROM loanapp.LoanBorrRaceTbl z
                             WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                                   AND z.BorrSEQNMB = a.BorrSeqNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanPrevFinanTbl
        INSERT INTO LoanApp.LoanPrevFinanTbl (LoanAppNmb,
                                              BorrSeqNmb,
                                              LoanPrevFinanSeqNmb,
                                              LoanPrevFinanStatCd,
                                              LoanPrevFinanAgencyNm,
                                              LoanPrevFinanLoanNmb,
                                              LoanPrevFinanAppvDt,
                                              LoanPrevFinanTotAmt,
                                              LoanPrevFinanBalAmt,
                                              LoanPrevFinanCreatUserId,
                                              LoanPrevFinanCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              LoanPrevFinanDelnqCmntTxt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   LoanPrevFinanSeqNmb,
                   LoanPrevFinanStatCd,
                   LoanPrevFinanAgencyNm,
                   LoanPrevFinanLoanNmb,
                   LoanPrevFinanAppvDt,
                   LoanPrevFinanTotAmt,
                   LoanPrevFinanBalAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanPrevFinanDelnqCmntTxt
              FROM LoanApp.LoanPrevFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanGuarTbl
        INSERT INTO LoanApp.LoanGuarTbl (LoanAppNmb,
                                         GuarSeqNmb,
                                         TaxId,
                                         GuarBusPerInd,
                                         LoanGuarOperCoInd,
                                         GuarCreatUserId,
                                         GuarCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         LACGntyCondTypCd,
                                         GntySubCondTypCd,
                                         GntyLmtAmt,
                                         GntyLmtPct,
                                         LoanCollatSeqNmb,
                                         GntyLmtYrNmb,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdInd,
                                         WorkrsCompInsRqrdInd,
                                         OthInsurDescTxt)
            SELECT v_NEWLoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LACGntyCondTypCd,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdInd,
                   WorkrsCompInsRqrdInd,
                   OthInsurDescTxt
              FROM LoanApp.LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanLmtGntyCollatTbl
        INSERT INTO LoanApp.LoanLmtGntyCollatTbl (LOANAPPNMB,
                                                  TAXID,
                                                  BUSPERIND,
                                                  LOANCOLLATSEQNMB,
                                                  CREATUSERID,
                                                  CREATDT,
                                                  LASTUPDTUSERID,
                                                  LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   TAXID,
                   BUSPERIND,
                   LOANCOLLATSEQNMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanLmtGntyCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanIntDtlTbl
        INSERT INTO LoanApp.LoanIntDtlTbl (LOANAPPNMB,
                                           LOANINTDTLSEQNMB,
                                           LOANINTLOANPARTPCT,
                                           LOANINTLOANPARTMONMB,
                                           LOANINTFIXVARIND,
                                           LOANINTRT,
                                           IMRTTYPCD,
                                           LOANINTBASERT,
                                           LOANINTSPRDOVRPCT,
                                           LOANINTADJPRDCD,
                                           LOANINTADJPRDMONMB,
                                           CREATUSERID,
                                           CREATDT,
                                           LASTUPDTUSERID,
                                           LASTUPDTDT,
                                           LOANINTADJPRDEFFDT,
                                           LoanIntDtlGuarInd)
            SELECT v_NEWLoanAppNmb,
                   LOANINTDTLSEQNMB,
                   LOANINTLOANPARTPCT,
                   LOANINTLOANPARTMONMB,
                   LOANINTFIXVARIND,
                   LOANINTRT,
                   IMRTTYPCD,
                   LOANINTBASERT,
                   LOANINTSPRDOVRPCT,
                   LOANINTADJPRDCD,
                   LOANINTADJPRDMONMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANINTADJPRDEFFDT,
                   LoanIntDtlGuarInd
              FROM LoanApp.LoanIntDtlTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanExprtCntryTbl
        INSERT INTO LoanApp.LoanExprtCntryTbl (LOANAPPNMB,
                                               LOANEXPRTCNTRYCD,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT,
                                               LOANEXPRTCNTRYSEQNMB)
            SELECT v_NEWLoanAppNmb,
                   LOANEXPRTCNTRYCD,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANEXPRTCNTRYSEQNMB
              FROM LoanApp.LoanExprtCntryTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBSTbl
        INSERT INTO LoanApp.LoanBSTbl (LoanAppNmb,
                                       LoanBSDt,
                                       LoanBSCashEqvlntAmt,
                                       LoanBSNetTrdRecvAmt,
                                       LoanBSTotInvtryAmt,
                                       LoanBSOthCurAssetAmt,
                                       LoanBSTotCurAssetAmt,
                                       LoanBSTotFixAssetAmt,
                                       LoanBSTotOthAssetAmt,
                                       LoanBSTotAssetAmt,
                                       LoanBSAcctsPayblAmt,
                                       LoanBSCurLTDAmt,
                                       LoanBSOthCurLiabAmt,
                                       LoanBSTotCurLiabAmt,
                                       LoanBSLTDAmt,
                                       LoanBSOthLTLiabAmt,
                                       LoanBSStbyDbt,
                                       LoanBSTotLiab,
                                       LoanBSBusNetWrth,
                                       LoanBSTngblNetWrth,
                                       LoanBSActlPrfrmaInd,
                                       LoanFinanclStmtSourcCd,
                                       LoanBSCreatUserId,
                                       LoanBSCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanBSDt,
                   LoanBSCashEqvlntAmt,
                   LoanBSNetTrdRecvAmt,
                   LoanBSTotInvtryAmt,
                   LoanBSOthCurAssetAmt,
                   LoanBSTotCurAssetAmt,
                   LoanBSTotFixAssetAmt,
                   LoanBSTotOthAssetAmt,
                   LoanBSTotAssetAmt,
                   LoanBSAcctsPayblAmt,
                   LoanBSCurLTDAmt,
                   LoanBSOthCurLiabAmt,
                   LoanBSTotCurLiabAmt,
                   LoanBSLTDAmt,
                   LoanBSOthLTLiabAmt,
                   LoanBSStbyDbt,
                   LoanBSTotLiab,
                   LoanBSBusNetWrth,
                   LoanBSTngblNetWrth,
                   LoanBSActlPrfrmaInd,
                   LoanFinanclStmtSourcCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBSTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanISTbl
        INSERT INTO LoanApp.LoanISTbl (LoanAppNmb,
                                       LoanISSeqNmb,
                                       LoanISNetSalesRevnuAmt,
                                       LoanISCostSalesAmt,
                                       LoanISGrsProftAmt,
                                       LoanISOwnrSalaryAmt,
                                       LoanISDprctAmortAmt,
                                       LoanISNetIncAmt,
                                       LoanISOperProftAmt,
                                       LoanISAnnIntExpnAmt,
                                       LoanISNetIncBefTaxWthdrlAmt,
                                       LoanISIncTaxAmt,
                                       LoanISCshflwAmt,
                                       LoanFinanclStmtSourcCd,
                                       LoanISBegnDt,
                                       LoanISEndDt,
                                       LoanISCreatUserId,
                                       LoanISCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanISSeqNmb,
                   LoanISNetSalesRevnuAmt,
                   LoanISCostSalesAmt,
                   LoanISGrsProftAmt,
                   LoanISOwnrSalaryAmt,
                   LoanISDprctAmortAmt,
                   LoanISNetIncAmt,
                   LoanISOperProftAmt,
                   LoanISAnnIntExpnAmt,
                   LoanISNetIncBefTaxWthdrlAmt,
                   LoanISIncTaxAmt,
                   LoanISCshflwAmt,
                   LoanFinanclStmtSourcCd,
                   LoanISBegnDt,
                   LoanISEndDt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanISTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppPrtTbl
        INSERT INTO LOANAPP.LoanAppPrtTbl (LoanAppNmb,
                                           LocId,
                                           LoanAppFIRSNmb,
                                           PrtId,
                                           LoanAppPrtAppNmb,
                                           LoanAppPrtLoanNmb,
                                           LoanAppPrtNm,
                                           LoanAppPrtStr1,
                                           LoanAppPrtStr2,
                                           LoanAppPrtCtyNm,
                                           LoanAppPrtStCd,
                                           LoanAppPrtZipCd,
                                           LoanAppPrtZip4Cd,
                                           LoanAppCntctLastNm,
                                           LoanAppCntctFirstNm,
                                           LoanAppCntctInitialNm,
                                           LoanAppCntctSfxNm,
                                           LoanAppCntctTitlTxt,
                                           LoanAppCntctPhn,
                                           LoanAppCntctFax,
                                           LoanAppCntctemail,
                                           LoanPckgSourcTypCd,
                                           LoanAppPckgSourcNm,
                                           LoanAppPckgSourcStr1Nm,
                                           LoanAppPckgSourcStr2Nm,
                                           LoanAppPckgSourcCtyNm,
                                           LoanAppPckgSourcStCd,
                                           LoanAppPckgSourcZipCd,
                                           LoanAppPckgSourcZip4Cd,
                                           ACHRtngNmb,
                                           ACHAcctNmb,
                                           ACHAcctTypCd,
                                           ACHTinNmb,
                                           LoanAppPrtTaxId,
                                           LoanAppLSPHQLocId,
                                           LoanAppPrtCreatUserId,
                                           LoanAppPrtCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanAgntInvlvdInd,
                                           --    LoanAppLspLastNm,
                                           --     LoanAppLspFirstNm,
                                           --     LoanAppLspInitialNm,
                                           LoanAppLspSfxNm,
                                           --    LoanAppLspTitlTxt,
                                           --    LoanAppLspPhn,
                                           LoanAppLspFax,
                                           --    LoanAppLspEMail,
                                           LoanPrtCntctCellPhn,
                                           LoanPrtAltCntctFirstNm,
                                           LoanPrtAltCntctLastNm,
                                           LoanPrtAltCntctInitialNm,
                                           LoanPrtAltCntctTitlTxt,
                                           LoanPrtAltCntctPrimPhn,
                                           LoanPrtAltCntctCellPhn,
                                           LoanPrtAltCntctemail,
                                           LendrAltCntctTypCd)
            SELECT v_NEWLoanAppNmb,
                   LocId,
                   LoanAppFIRSNmb,
                   PrtId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   LoanAppPrtNm,
                   LoanAppPrtStr1,
                   LoanAppPrtStr2,
                   LoanAppPrtCtyNm,
                   LoanAppPrtStCd,
                   LoanAppPrtZipCd,
                   LoanAppPrtZip4Cd,
                   LoanAppCntctLastNm,
                   LoanAppCntctFirstNm,
                   LoanAppCntctInitialNm,
                   LoanAppCntctSfxNm,
                   LoanAppCntctTitlTxt,
                   LoanAppCntctPhn,
                   LoanAppCntctFax,
                   LoanAppCntctemail,
                   LoanPckgSourcTypCd,
                   LoanAppPckgSourcNm,
                   LoanAppPckgSourcStr1Nm,
                   LoanAppPckgSourcStr2Nm,
                   LoanAppPckgSourcCtyNm,
                   LoanAppPckgSourcStCd,
                   LoanAppPckgSourcZipCd,
                   LoanAppPckgSourcZip4Cd,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppPrtTaxId,
                   LoanAppLSPHQLocId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanAgntInvlvdInd,
                   --   LoanAppLspLastNm,
                   --   LoanAppLspFirstNm,
                   --   LoanAppLspInitialNm,
                   LoanAppLspSfxNm,
                   --   LoanAppLspTitlTxt,
                   --    LoanAppLspPhn,
                   LoanAppLspFax,
                   --   LoanAppLspEMail,
                   LoanPrtCntctCellPhn,
                   LoanPrtAltCntctFirstNm,
                   LoanPrtAltCntctLastNm,
                   LoanPrtAltCntctInitialNm,
                   LoanPrtAltCntctTitlTxt,
                   LoanPrtAltCntctPrimPhn,
                   LoanPrtAltCntctCellPhn,
                   LoanPrtAltCntctemail,
                   LendrAltCntctTypCd
              FROM LoanApp.LoanAppPrtTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPymntTbl
        INSERT INTO LoanApp.LoanPymntTbl (LOANAPPNMB,
                                          PYMNTDAYNMB,
                                          PYMNTBEGNMONMB,
                                          ESCROWACCTRQRDIND,
                                          NETEARNCLAUSEIND,
                                          ARMMARTYPIND,
                                          ARMMARCEILRT,
                                          ARMMARFLOORRT,
                                          STINTRTREDUCTNIND,
                                          LATECHGIND,
                                          LATECHGAFTDAYNMB,
                                          LATECHGFEEPCT,
                                          PYMNTINTONLYBEGNMONMB,
                                          PYMNTINTONLYFREQCD,
                                          PYMNTINTONLYDAYNMB,
                                          NetEarnPymntPct,
                                          NetEarnPymntOverAmt,
                                          StIntRtReductnPrgmNm,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   PYMNTDAYNMB,
                   PYMNTBEGNMONMB,
                   ESCROWACCTRQRDIND,
                   NETEARNCLAUSEIND,
                   ARMMARTYPIND,
                   ARMMARCEILRT,
                   ARMMARFLOORRT,
                   STINTRTREDUCTNIND,
                   LATECHGIND,
                   LATECHGAFTDAYNMB,
                   LATECHGFEEPCT,
                   PYMNTINTONLYBEGNMONMB,
                   PYMNTINTONLYFREQCD,
                   PYMNTINTONLYDAYNMB,
                   NetEarnPymntPct,
                   NetEarnPymntOverAmt,
                   StIntRtReductnPrgmNm,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPymntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO Loanapp.LoanAgntTbl (LOANAPPNMB,
                                         LoanAgntSeqNmb,
                                         LOANAGNTBUSPERIND,
                                         LOANAGNTNM,
                                         LOANAGNTCNTCTFIRSTNM,
                                         LOANAGNTCNTCTMIDNM,
                                         LOANAGNTCNTCTLASTNM,
                                         LOANAGNTCNTCTSFXNM,
                                         LOANAGNTADDRSTR1NM,
                                         LOANAGNTADDRSTR2NM,
                                         LOANAGNTADDRCTYNM,
                                         LOANAGNTADDRSTCD,
                                         LOANAGNTADDRSTNM,
                                         LOANAGNTADDRZIPCD,
                                         LOANAGNTADDRZIP4CD,
                                         LOANAGNTADDRPOSTCD,
                                         LOANAGNTADDRCNTCD,
                                         LOANAGNTTYPCD,
                                         LOANAGNTDOCUPLDIND,
                                         LOANCDCTPLFEEIND,
                                         LOANCDCTPLFEEAMT,
                                         LOANAGNTID,
                                         CreatUserId,
                                         CreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM,
                   LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LoanAgntId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAgntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO LOANAPP.LOANAGNTFEEDTLTBL (LOANAPPNMB,
                                               LoanAgntSeqNmb,
                                               LOANAGNTSERVTYPCD,
                                               LOANAGNTSERVOTHTYPTXT,
                                               LOANAGNTAPPCNTPAIDAMT,
                                               LOANAGNTSBALENDRPAIDAMT,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   LoanAGNTSeqNmb,
                   LOANAGNTSERVTYPCD,
                   LOANAGNTSERVOTHTYPTXT,
                   LOANAGNTAPPCNTPAIDAMT,
                   LOANAGNTSBALENDRPAIDAMT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LOANAPP.LOANAGNTFEEDTLTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    p_NEWLoanAppNmb := v_NEWLoanAppNmb;
END LOANCOPYCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.EXTRACTSELCSP (p_LoanAppNmb       IN     NUMBER := NULL,
                                                   p_PrtId                   NUMBER := 0,
                                                   p_RetVal              OUT NUMBER,
                                                   p_SelCur1             OUT SYS_REFCURSOR,
                                                   p_SelCur2             OUT SYS_REFCURSOR,
                                                   p_SelCur3             OUT SYS_REFCURSOR,
                                                   p_SelCur4             OUT SYS_REFCURSOR,
                                                   p_SelCur5             OUT SYS_REFCURSOR,
                                                   p_SelCur6             OUT SYS_REFCURSOR,
                                                   p_SelCur7             OUT SYS_REFCURSOR,
                                                   p_SelCur8             OUT SYS_REFCURSOR,
                                                   p_SelCur9             OUT SYS_REFCURSOR,
                                                   p_SelCur10            OUT SYS_REFCURSOR,
                                                   p_SelCur11            OUT SYS_REFCURSOR,
                                                   p_SelCur12            OUT SYS_REFCURSOR,
                                                   p_SelCur13            OUT SYS_REFCURSOR,
                                                   p_SelCur14            OUT SYS_REFCURSOR,
                                                   p_SelCur15            OUT SYS_REFCURSOR,
                                                   p_SelCur16            OUT SYS_REFCURSOR,
                                                   p_SelCur17            OUT SYS_REFCURSOR,
                                                   p_SelCur18            OUT SYS_REFCURSOR,
                                                   p_SelCur19            OUT SYS_REFCURSOR,
                                                   p_SelCur20            OUT SYS_REFCURSOR,
                                                   p_SelCur21            OUT SYS_REFCURSOR,
                                                   p_SelCur22            OUT SYS_REFCURSOR,
                                                   p_SelCur23            OUT SYS_REFCURSOR,
                                                   p_SelCur24            OUT SYS_REFCURSOR,
                                                   p_SelCur25            OUT SYS_REFCURSOR,
                                                   p_SelCur26            OUT SYS_REFCURSOR,
                                                   p_SelCur27            OUT SYS_REFCURSOR,
                                                   p_SelCur28            OUT SYS_REFCURSOR,
                                                   p_SelCur29            OUT SYS_REFCURSOR,
                                                   p_SelCur30            OUT SYS_REFCURSOR,
                                                   p_SelCur31            OUT SYS_REFCURSOR,
                                                   p_SelCur32            OUT SYS_REFCURSOR,
                                                   p_SelCur33            OUT SYS_REFCURSOR,
                                                   p_SelCur34            OUT SYS_REFCURSOR,
                                                   p_SelCur35            OUT SYS_REFCURSOR,
                                                   p_SelCur36            OUT SYS_REFCURSOR,
                                                   p_SelCur37            OUT SYS_REFCURSOR,
                                                   p_SelCur38            OUT SYS_REFCURSOR,
                                                   p_SelCur39            OUT SYS_REFCURSOR,
                                                   p_SelCur40            OUT SYS_REFCURSOR,
                                                   p_SelCur41            OUT SYS_REFCURSOR,
                                                   p_SelCur42            OUT SYS_REFCURSOR,
                                                   p_SelCur43            OUT SYS_REFCURSOR,
                                                   p_SelCur44            OUT SYS_REFCURSOR)
AS
   /*SB -- 07/07/2014 This procedure created for Extract XML */
   /*SB -- 02/24/2015 Added p_selcur35  */
   /*SB -- 03/27/2015 Added p_selcur36  */
   /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met*/
   /*SB -- 04/07/2016 Added p_selcur37  */
   /*BR -- 10/21/2016 Added p_selcur38, p_selcur39 */
   /*Nk -- 03/10/2017 OPSMDEV 1385 Added p_selcur40*/
   --SS--07/12/2018 OPSMDEV 1860 Added p_selcur 42 and 43
   -- JP 08/06/2019 OPSMDEV 2255 modified call to LoanAppSelTSP 
   v_RetVal   NUMBER;
   v_PrtId    NUMBER (7);
BEGIN
   -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
   SELECT COUNT (*)
     INTO p_RetVal
     FROM loanapp.LoanAppTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF NVL (p_PrtId, 0) = 0
   THEN
      BEGIN
         SELECT PrtId
           INTO v_PrtId
           FROM loanapp.LoanAppPrtTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PrtId := NULL;
      END;
   END IF;

   IF p_RetVal = 0
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur2 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur3 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur4 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur5 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur6 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur7 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur8 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur9 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur10 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur11 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur12 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur13 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur14 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur15 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur16 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur17 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur18 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur19 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur20 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur21 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur22 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur23 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur24 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur25 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur26 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur27 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur28 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur29 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur30 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur31 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur32 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur33 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur34 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur35 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur36 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur37 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur38 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur39 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur40 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur41 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur42 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur43 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
        OPEN p_SelCur44 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
      END;
   ELSE
      BEGIN
         --LoanAppSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur1);
         -- JP Modifed LoanAppSelTSP for 2255 
        LoanAppSelTSP (p_identifier => 38, 
                       p_loanappnmb => p_LoanAppNmb, 
                       p_retval => v_RetVal, 
                       p_selcur => p_SelCur1);
         LoanAppARCRsnSelTSP (38, v_RetVal, p_LoanAppNmb, NULL, p_SelCur2);
         LoanAppAsstAreaSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur3);
         LoanAppCAInfoSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur4);
         LoanAppChngBusOwnrshpSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur5);
         LoanAppCnselngSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur6);
         LoanAppCnselngSrcSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur7);
         LoanAppEligSelTSP (38, p_LoanAppNmb, NULL, p_SelCur8);
         LoanAppPrtSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur9);
         LoanBorrSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur10);
         LoanBSSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur11);
         LoanCollatSelTSP (38, p_LoanAppNmb, NULL, p_SelCur12);
         LoanCollatLienSelTSP (38, p_LoanAppNmb, NULL, NULL, p_SelCur13);
         LoanCrdtUnavRsnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
         LoanFedEmpSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur15);
         LoanGuarSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur16);
         LoanIndbtnesSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur17);
         LoanInjctnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur18);
         LoanIntDtlSelTSP (38, p_LoanAppNmb, p_SelCur19);
         LoanISSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur20);
         LoanPartLendrSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur21);
         LoanPerFinanSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur22);
         LoanPrevFinanSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur23);
         LoanPrinSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur24);
         LoanPrinRaceSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur25);
         LoanProcdSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur26);
         LoanPrtCmntSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur27);
         LoanSpcPurpsSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur28);
         LqdCrSelCSP (38, p_LoanAppNmb, v_RetVal, p_SelCur29, p_SelCur30, p_SelCur31, p_SelCur32, p_SelCur33);
         partner.PrtAgrmtSelTSP (38, v_RetVal, v_prtid, NULL, NULL, NULL, NULL, p_SelCur34);
         loanapp.LoanBorrRaceSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur35);
         LOANAPP.LoanAppBusApprSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur36);
         LOANAPP.LoanAppCAUndrServMrktSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur37);
         LOANAPP.LoanLmtGntyCollatSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, p_SelCur38, v_RetVal);
         LOANAPP.LoanStbyAgrmtDtlSelTSP (38, p_LoanAppNmb, NULL, p_SelCur39, v_RetVal);
         LOANAPP.LoanEconDevObjctChldSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR40, v_RetVal);
         LOANAPP.LoanExprtCntrySelTsp (38, p_LoanAppNmb, p_SELCUR41, v_RetVal);
         LoanApp.LoanAgntSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR42, v_RetVal);
         LoanApp.LoanAgntfeeDtlSelTSp (38, p_LoanAppNmb, NULL, p_SELCUR43, v_RetVal);
         LoanApp.LoanProcdSubTypSelTSP (38, p_LoanAppNmb, NULL, p_SelCur44,v_RetVal);
      END;
   END IF;
END ExtractSelCSP;
/


set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Loan_Authorization_Modernization
define package_name=UAT_0601
define package_buildtime=20210601141324
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Loan_Authorization_Modernization_UAT_0601 created on Tue 06/01/2021 14:13:25.65 by Jasleen Gorowada
prompt deploy scripts for deploy Loan_Authorization_Modernization_UAT_0601 created on Tue 06/01/2021 14:13:25.65 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Loan_Authorization_Modernization_UAT_0601: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\drop_objects.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANSUBPROCDTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\LOANGNTYSUBPROCDTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\PROCDTXTBLCKTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\LOANSUBPROCDTYPTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANPROCDSUBTYPIDSEQ.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\LOANGNTYSUBPROCDIDSEQ.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\insert_PROCDTXTBLCKTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\insert_LOANSUBPROCDTYPCDTBL.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\inserts_FORMLAYOUT.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_4289_LOANVALIDATIONERRTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATELOANCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATESUBPROCDCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSUMTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDDELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDDELALLTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSUMTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDINSTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDDELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDDELALLTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYPROCDCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Procedures\PROCDTXTBLCKSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Procedures\LOANSUBPROCDTYPSELTSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\drop_objects.sql"
drop LOANAPP.LOANPROCDSUBTYPTBL;
drop LOAN.LOANGNTYPROCDSUBTYPTBL;
drop SBAREF.PROCTXTBLCK;
drop SBAREF.LOANPROCDSUBTYPLKUPTBL;
drop LOANAPP.LOANPROCDSUBTYPIDSEQ;
drop LOAN.LOANGNTYPROCDSUBTYPID_SEQ;
drop LOAN.LOANGNTYPROCDSUBTYPDELTSP;
drop LOAN.LOANGNTYPROCDSUBTYPINSTSP;
drop LOAN.LOANGNTYPROCDSUBTYPSELTSP;
drop LOAN.LOANGNTYPROCDSUBTYPSUMTSP;
drop LOAN.LOANGNTYPROCDTOTAMTSELTSP;
drop LOANAPP.LOANPROCDSUBTYPDELTSP;
drop LOANAPP.LOANPROCDSUBTYPINSTSP;
drop LOANAPP.LOANPROCDSUBTYPSELTSP;
drop LOANAPP.LOANPROCDSUBTYPSUMTSP;
drop LOANAPP.LOANPROCDTOTAMTSELTSP;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Tables\LOANSUBPROCDTBL.sql"
CREATE TABLE LOANAPP.LOANSUBPROCDTBL
(
  LOANSUBPROCDID     NUMBER                     NOT NULL,
  LOANAPPNMB         NUMBER(10)                 NOT NULL,
  LOANPROCDTYPCD     CHAR(2 BYTE)               NOT NULL,
  PROCDTYPCD         CHAR(1 BYTE),
  LOANPROCDTXTBLCK   VARCHAR2(500 BYTE),
  LOANPROCDAMT       NUMBER(19,4),
  LOANPROCDADDR      VARCHAR2(500 BYTE),
  LOANSUBPROCDTYPCD  NUMBER(10)                 NOT NULL,
  LOANPROCDDESC      VARCHAR2(200 BYTE),
  LENDER             VARCHAR2(80 BYTE)          DEFAULT NULL,
  CREATUSERID        CHAR(15 BYTE)              DEFAULT user,
  CREATDT            DATE                       DEFAULT sysdate,
  LASTUPDTUSERID     CHAR(15 BYTE)              DEFAULT user,
  LASTUPDTDT         DATE                       DEFAULT sysdate
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOANAPP.LOANAPPSUBPROCDSEQNMBUCIND ON LOANAPP.LOANSUBPROCDTBL
(LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOANAPP.LOANSUBPROCDTBL ADD (
  CONSTRAINT LOANAPPSUBPROCDSEQNMBUCIND
  PRIMARY KEY
  (LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD)
  USING INDEX LOANAPP.LOANAPPSUBPROCDSEQNMBUCIND
  ENABLE VALIDATE);

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LANAREAD;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LANAUPDATE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOAN;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANACCT;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANAPPLOOKUPUPDROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANAPPUPDAPPROLE;


GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANLOOKUPUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOANAPP.LOANSUBPROCDTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO LOANREADALLROLE;

GRANT INSERT, SELECT, UPDATE ON LOANAPP.LOANSUBPROCDTBL TO LOANUPDROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOANAPP.LOANSUBPROCDTBL TO SBACDTBLUPDT;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\LOANGNTYSUBPROCDTBL.sql"
CREATE TABLE LOAN.LOANGNTYSUBPROCDTBL
(
  LOANSUBPROCDID     NUMBER                     NOT NULL,
  LOANAPPNMB         NUMBER(10)                 NOT NULL,
  LOANPROCDTYPCD     CHAR(2 BYTE)               NOT NULL,
  PROCDTYPCD         CHAR(1 BYTE),
  LOANPROCDTXTBLCK   VARCHAR2(500 BYTE),
  LOANPROCDAMT       NUMBER(19,4),
  LOANPROCDADDR      VARCHAR2(500 BYTE),
  LOANSUBPROCDTYPCD  NUMBER(10)                 NOT NULL,
  LOANPROCDDESC      VARCHAR2(200 BYTE),
  LENDER             VARCHAR2(80 BYTE)          DEFAULT NULL,
  CREATUSERID        CHAR(15 BYTE)              DEFAULT user,
  CREATDT            DATE                       DEFAULT sysdate,
  LASTUPDTUSERID     CHAR(15 BYTE)              DEFAULT user,
  LASTUPDTDT         DATE                       DEFAULT sysdate
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.LOANAPPSUBPROCDSEQNMBUCIND ON LOAN.LOANGNTYSUBPROCDTBL
(LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANGNTYSUBPROCDTBL ADD (
  CONSTRAINT LOANAPPSUBPROCDSEQNMBUCIND
  PRIMARY KEY
  (LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD)
  USING INDEX LOAN.LOANAPPSUBPROCDSEQNMBUCIND
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANORIGHQOVERRIDE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYSUBPROCDTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANSERVSBICGOV;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYSUBPROCDTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYSUBPROCDTBL TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\PROCDTXTBLCKTBL.sql"
CREATE TABLE SBAREF.PROCDTXTBLCKTBL
(
  LOANPROCDTYPCD      CHAR(3 BYTE)              NOT NULL,
  TEXTBLOCK           VARCHAR2(255 BYTE)        NOT NULL,
  CREATUSERID         VARCHAR2(32 BYTE),
  CREATDT             DATE,
  LASTUPDTUSERID      VARCHAR2(32 BYTE),
  LASTUPDTDT          DATE,
  PROCDTYPCD          CHAR(1 BYTE)              NOT NULL,
  PROCDTXTBLCKSTRTDT  DATE                      DEFAULT sysdate,
  PROCDTXTBLCKENDDT   DATE                      DEFAULT null
)
TABLESPACE SBAREFDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCDTXTBLCKTBL TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO GPTS;

GRANT REFERENCES, SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOAN;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANACCTREADALLROLE;

GRANT REFERENCES, SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANAPP;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANORIGBORROWER;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANREADALLROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO LOANUPDROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO PERSONNELREADROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO POOLSECADMINROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO PUBLICREADROLE;

GRANT SELECT, UPDATE ON SBAREF.PROCDTXTBLCKTBL TO SBACDUPDTROLE;

GRANT SELECT ON SBAREF.PROCDTXTBLCKTBL TO SBAREFREADALLROLE;

GRANT SELECT, UPDATE ON SBAREF.PROCDTXTBLCKTBL TO SBAREFUPDCDROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.PROCDTXTBLCKTBL TO UPDCDROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Tables\LOANSUBPROCDTYPTBL.sql"
CREATE TABLE SBAREF.LOANSUBPROCDTYPTBL
(
  LOANSUBPROCDTYPCD      NUMBER(10)             NOT NULL,
  PROCDTYPCD             CHAR(1 BYTE)           NOT NULL,
  LOANPROCDTYPCD         CHAR(2 BYTE)           NOT NULL,
  SUBPROCDTYPDESCTXT     VARCHAR2(80 BYTE)      NOT NULL,
  LOANPROCDSUBTYPSTRTDT  DATE,
  LOANPROCDSUBTYPENDDT   DATE,
  CREATUSERID            VARCHAR2(32 BYTE)      DEFAULT user,
  CREATDT                DATE                   DEFAULT sysdate,
  LASTUPDTUSERID         VARCHAR2(32 BYTE),
  LASTUPDTDT             DATE
)
TABLESPACE SBAREFDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX SBAREF.LOANSUBPROCDTYPCDUCIND ON SBAREF.LOANSUBPROCDTYPTBL
(LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD)
LOGGING
TABLESPACE SBAREFINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE SBAREF.LOANSUBPROCDTYPTBL ADD (
  CONSTRAINT LOANSUBPROCDTYPCDUCIND
  PRIMARY KEY
  (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD)
  USING INDEX SBAREF.LOANSUBPROCDTYPCDUCIND
  ENABLE VALIDATE);

ALTER TABLE SBAREF.LOANSUBPROCDTYPTBL ADD (
  CONSTRAINT LOANSUBPROCDTYPCDFK 
  FOREIGN KEY (PROCDTYPCD) 
  REFERENCES SBAREF.PROCDTYPCDTBL (PROCDTYPCD)
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.LOANSUBPROCDTYPTBL TO CDCONLINECORPGOVPRTUPDATE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO GPTS;

GRANT REFERENCES, SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOAN;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANACCTREADALLROLE;

GRANT REFERENCES, SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANAPP;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANORIGBORROWER;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANREADALLROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO LOANUPDROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO PERSONNELREADROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO POOLSECADMINROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO PUBLICREADROLE;

GRANT SELECT, UPDATE ON SBAREF.LOANSUBPROCDTYPTBL TO SBACDUPDTROLE;

GRANT SELECT ON SBAREF.LOANSUBPROCDTYPTBL TO SBAREFREADALLROLE;

GRANT SELECT, UPDATE ON SBAREF.LOANSUBPROCDTYPTBL TO SBAREFUPDCDROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SBAREF.LOANSUBPROCDTYPTBL TO UPDCDROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Sequences\LOANPROCDSUBTYPIDSEQ.sql"
CREATE SEQUENCE LOANAPP.LOANPROCDSUBTYPIDSEQ
  START WITH 172
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  ORDER
  NOKEEP
  GLOBAL;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\LOANGNTYSUBPROCDIDSEQ.sql"
CREATE SEQUENCE LOAN.LOANGNTYSUBPROCDIDSEQ
  START WITH 4
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  ORDER
  NOKEEP
  GLOBAL;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\insert_PROCDTXTBLCKTBL.sql"
SET DEFINE OFF;
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('00 ', 'the purchase of land described as [Description] and located at [Address]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('01 ', 'the purchase of land described as [Description] and located at [Address]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('02 ', 'the purchase of land and existing improvements described as [Description] and located at [Address]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('04 ', '[ProcdSubTypDesc2] described as [Description] and [ProcdSubTypDesc1] located at [Address]'||CHR(13)||'', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('10 ', 'the [ProcdSubTypDesc] of machinery and equipment described as [Description] and located at [Address]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('15 ', 'the refinance of debt owed to [Lender] and secured by [ProcdSubTypDesc] described as [Description]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('19 ', 'eligible business expenses under Debt Refinancing  described as [Description]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('21 ', 'other Expenses described as [Description]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
Insert into SBAREF.PROCDTXTBLCKTBL
   (LOANPROCDTYPCD, TEXTBLOCK, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT, PROCDTYPCD, PROCDTXTBLCKSTRTDT, PROCDTXTBLCKENDDT)
 Values
   ('22 ', 'professional fees described as [Description]', user, sysdate, NULL, 
    NULL, 'E', TO_DATE('5/18/2021 2:35:36 PM', 'MM/DD/YYYY HH:MI:SS AM'), NULL);
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\insert_LOANSUBPROCDTYPCDTBL.sql"
SET DEFINE OFF;
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (1, 'E', '02', 'purchase land', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (2, 'E', '02', 'purchase existing building', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (3, 'E', '04', 'on real estate, new improvements or additions ', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (4, 'E', '04', 'on real estate, renovations', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (5, 'E', '04', 'on real estate, new improvements or additions, and renovations', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (6, 'E', '04', 'on leased real estate, new improvements or additions', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (7, 'E', '04', 'on leased real estate, renovations', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (8, 'E', '04', 'on leased real estate, new improvements or additions', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, sysdate, NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (9, 'E', '10', 'purchase', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (10, 'E', '10', 'installation', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (11, 'E', '10', 'purchase and installation', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (12, 'E', '15', 'real estate', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (13, 'E', '15', 'machinery and equipment', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
Insert into SBAREF.LOANSUBPROCDTYPTBL
   (LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, 
    LOANPROCDSUBTYPENDDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
 Values
   (14, 'E', '15', 'real estate and machinery and equipment', TO_DATE('5/17/2021 11:56:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 
    NULL, user, TO_DATE('5/17/2021 11:55:30 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, NULL);
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\inserts_FORMLAYOUT.sql"
SET DEFINE OFF;
delete from SBAREF.FORMLAYOUTCOLUMNTBL where FORMID in (select FORMID from SBAREF.FORMLAYOUTTBL  where TBLNM in ('PROCTXTBLCK','LOANPROCDSUBTYPLKUPTBL'));

delete from SBAREF.FORMLAYOUTTBL  where TBLNM in ('PROCTXTBLCK','LOANPROCDSUBTYPLKUPTBL');
commit;
Insert into SBAREF.FORMLAYOUTTBL
   (FORMID, TBLNM, FORMNM, FORMINSTRCTN, ORDBY, 
    CGIFORM, APPSLCTSTMT, FORMTYPNMB, DBNM, INSPROCNM, 
    SELPROCNM, SELPROCIDNMB, UPDTPROCNM, DELPROCNM, MAXROWNMB, 
    LASTTBLUPDTDT, UPDTCOLUMNNM, DFLTQUERYNM, DATABASENM, CREATUSERID, 
    CREATDT, DATAQUERYDESC)
 Values
   (484, 'PROCDTXTBLCKTBL', 'Proceed Text Block', NULL, NULL, 
    NULL, NULL, 2, 'Oracle_Transaction', NULL, 
    'PROCDTXTBLCKSELTSP', NULL, NULL, NULL, NULL, 
    NULL, NULL, NULL, 'SBAREF', user, 
    sysdate, NULL);
Insert into SBAREF.FORMLAYOUTTBL
   (FORMID, TBLNM, FORMNM, FORMINSTRCTN, ORDBY, 
    CGIFORM, APPSLCTSTMT, FORMTYPNMB, DBNM, INSPROCNM, 
    SELPROCNM, SELPROCIDNMB, UPDTPROCNM, DELPROCNM, MAXROWNMB, 
    LASTTBLUPDTDT, UPDTCOLUMNNM, DFLTQUERYNM, DATABASENM, CREATUSERID, 
    CREATDT, DATAQUERYDESC)
 Values
   (485, 'LOANSUBPROCDTYPTBL', 'Proceed SubType Lookup', NULL, NULL, 
    NULL, NULL, 2, 'Oracle_Transaction', 'LOANSUBPROCDTYPINSTSP', 
    'LOANSUBPROCDTYPSELTSP', NULL, 'LOANSUBPROCDTYPUPDTSP', NULL, NULL, 
    NULL, NULL, NULL, 'SBAREF', user, 
    sysdate, NULL);
COMMIT;

Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (484, 1, 'LOANPROCDTYPCD', 'Loan Proceed Type Code', 0, 
    1, 1, 1, 0, 1, 
    1, 'T', 3, 3, 1, 
    0, 0, NULL, NULL, 1, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (484, 2, 'TEXTBLOCK', 'Proceed Text Block', 0, 
    1, 1, 1, 0, 2, 
    2, 'T', 35, 255, 1, 
    0, 0, NULL, NULL, 2, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (484, 7, 'PROCDTYPCD', 'Proceed Type Code', 0, 
    1, 1, 1, 0, 7, 
    7, 'T', 1, 1, 1, 
    0, 0, NULL, NULL, 7, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (485, 1, 'LOANSUBPROCDTYPCD', 'Loan Sub Proceed Type Code', 1, 
    0, 1, 1, 0, 1, 
    1, 'N', 10, 10, 1, 
    0, 0, NULL, NULL, 1, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (485, 2, 'PROCDTYPCD', 'Proceed Type Code', 1, 
    0, 1, 1, 0, 2, 
    2, 'T', 1, 1, 1, 
    0, 0, NULL, NULL, 2, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (485, 3, 'LOANPROCDTYPCD', 'Loan Proceed Type Code', 1, 
    0, 1, 1, 0, 3, 
    3, 'T', 2, 2, 1, 
    0, 0, NULL, NULL, 3, 
    user, sysdate, NULL, NULL, NULL);
Insert into SBAREF.FORMLAYOUTCOLUMNTBL
   (FORMID, ORDNMB, COLUMNNM, COLUMNLBLNM, ISKEYCOLUMNNMB, 
    EDTCOLUMNNMB, SHOWADD, SHOWQUERY, SHOWQBE, UPDTPROCNMB, 
    INSPROCNMB, DATATYP, COLUMNSIZENMB, COLUMNMAXNMB, MANDCOLUMNVAL, 
    UPPRCASE, AUTOASGNVAL, SLCTSTMT, DFLTVAR, SHOWORDNMB, 
    CREATUSERID, CREATDT, ISDRPDWN, ISDRPDWNSEL, PRCSMTHDEXPRTIND)
 Values
   (485, 4, 'SUBPROCDTYPDESCTXT', 'Loan Sub Proceed Type Text', 0, 
    1, 1, 1, 0, 4, 
    4, 'T', 35, 80, 1, 
    0, 0, NULL, NULL, 4, 
    user, sysdate, NULL, NULL, NULL);
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_4289_LOANVALIDATIONERRTBL.sql"
SET DEFINE OFF;
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   (4289, 1, 'For application, sum of sub proceeds for proceed type ^1 must equal the proceed amount.', user, sysdate, 
    user, sysdate, NULL);
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   (4289, 2, 'For application, sum of sub proceeds for proceed type ^1 must equal the proceed amount.', user, sysdate, 
    user, sysdate, NULL);
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATELOANCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATELOANCSP (p_retval            OUT NUMBER,
                                                     p_loanappnmb     IN     NUMBER DEFAULT NULL,
                                                     p_transind       IN     NUMBER DEFAULT NULL,
                                                     p_cleardataind   IN     VARCHAR DEFAULT 'Y',
                                                     p_selcur1           OUT SYS_REFCURSOR,
                                                     p_selcur2           OUT SYS_REFCURSOR) AS
    /*
     Stored Procedure: LOANAPP.VALIDATELOANCSP
     Database        : LOANAPP
     Purpose         : To check for all validations.
     Date            :
     Author          :
     Authors Note    :
     Parameters      :
     -- --.
    ********************* REVISION HISTORY ****************************************
     Revised By   Date             Description
     ----------  ----------     -----------------------------------------------
    APK          02/08/2011     Modified to add new validation VALIDATEELIGCSP
                                exceptions
    APK         02/07/2012     Modified to add new procedure VALIDATEAPPCACSP
    APK         02/08/2012     Modified to exclude VALIDATEAPPCACSP if process method is
                                not    CAI
    APK         05/22/2012    Please create following two new validations for LAI loan.
                              1.    Credit Score is Mandatory.
                              2.    Credit Score needs to be greater than 140.
                              We already have parameters for the processing methods and
                              credit score limits from ARC loans implementation.
                              They are:CrdScrThrshld:  This holds the lower limit and will
                              have to be set to 140.
    APK         05/22/2012    Modified to change the PrcsMthdCd from SLA to LAI as it was determined that the correct code is LAI.
    SP          05/02/2013    Commented the code for error 1037 to allow poor score loans
    SP          12/26/2013    Added call to LOANAPP.VALIDATEINTDTLCSP
    SP          01/08/2014    Modified error 1036 to check for PrcsMthdReqLqdCr value
    RY          05/16/2016    Added new validation 'ValidateLoanChrgofCsp'--OPSM DEV-835
    RY          14/11/2016 --OPSM DEV 1245   Added new validation 'validateloanpymntcsp'
    SS-         07/25/2018 OPSMDEV 1862 Added Validateagntcsp
    RY-- 08/12/2018--OPSMDEV-1876-- Added new Sp call LOANAPP.VALIDATERURALINITIATVCSP
    SB - 07/19/2020 Added v_PrcsMthdCd IN ('CAI','CRL')
    SL - 01/04/2021 SODSTORY-344.  Added new sp call to loanApp.VALIDATELENRCMNTCSP to validate lender comments
    **************** END OF COMMENTS ******************************************
    */

    v_errseqnmb             NUMBER (10, 0);
    v_loanapprecvdt         DATE;
    v_prcsmthdcd            CHAR (3);
    v_recovind              CHAR (1);
    v_crdscrthrshldmin      NUMBER (10, 0);
    v_lqdcrtotscr           CHAR (3);
    v_imprmtrvalchk         VARCHAR (255);
    v_imprmtrid             VARCHAR2 (255);
    v_loanapprqstamt        NUMBER (19, 4);
    v_prcsmthdreqlqdcramt   NUMBER (19, 4);
    v_loanagntseqnmb        NUMBER (10);
BEGIN
    p_retval := 0;
    v_loanapprecvdt := SYSDATE;
    v_errseqnmb := 1;

    IF    p_cleardataind IS NULL
       OR p_cleardataind = 'Y' THEN
        BEGIN
            DELETE FROM loanerrtbl
            WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    BEGIN
        SELECT NVL (MAX (errseqnmb), 0) + 1
        INTO v_errseqnmb
        FROM loanapp.loanerrtbl e
        WHERE e.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_errseqnmb := 1;
    END;

    BEGIN
        SELECT prcsmthdcd,
               NVL (loanapprecvdt, SYSDATE),
               recovind,
               loanapprqstamt
        INTO v_prcsmthdcd,
             v_loanapprecvdt,
             v_recovind,
             v_loanapprqstamt
        FROM loanapptbl a
        WHERE a.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_prcsmthdcd := NULL;
    END;

    validateappcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateappprtcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateintdtlcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateprocdcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validatespcpurpscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateeligcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);

    IF v_prcsmthdcd != 'SMP' THEN
		validateappprojcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateborrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebuscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebusprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepercsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprinracecsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    END IF;
    
    if v_prcsmthdcd NOT IN('PPP','PPS') THEN
    	validatelenrcmntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    	validatebscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecrdtunavrsncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatliencsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateiscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateguarcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatefedempcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateindbtnescsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateinjctncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateperfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprevfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepartlendrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateloanchrgofcsp (p_loanappnmb, v_errseqnmb, p_transind);
		validateloanpymntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateloanexprtcntrycsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateagntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
    END IF;

    IF v_prcsmthdcd IN ('CAI', 'CRL') THEN
        loanapp.validateappcacsp (p_loanappnmb, p_transind, v_loanapprecvdt, v_errseqnmb, p_retval);
    END IF;
    
    if v_prcsmthdcd='504' THEN
        loanapp.validatesubprocdcsp  (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    end if;

    OPEN p_selcur1 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'F'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    OPEN p_selcur2 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'W'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    p_retval := NVL (p_retval, 0);
--dbms_output.put_line(29);
END;
/


GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRFHINES;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRILIBERMAN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNNAMILAE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNSALATOVA;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSJUSTIN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSMANIAM;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERUPDCDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSREADROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBACDUPDTROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFUPDCDROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATESUBPROCDCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATESUBPROCDCSP(
   p_RetVal             OUT NUMBER,
   p_LoanAppNmb      IN     NUMBER DEFAULT NULL,
   p_ErrSeqNmb       IN OUT NUMBER,
   p_TransInd        IN     NUMBER DEFAULT NULL,
   p_LoanAppRecvDt   IN     DATE DEFAULT NULL)
AS
cursor fetch_procd_rec is
select LOANPROCDSEQNMB,LoanProcdAmt,ProcdTypcd,LoanProcdTypCd  FROM LOANAPP.LOANPROCDTBL WHERE LoanAppNmb = p_LoanAppNmb;

sub_recs_sum number:=0;          

     	
BEGIN
    SAVEPOINT VALIDATESUBPROCDCSP;
    
    for rec in fetch_procd_rec
    loop
        select nvl(sum(LOANPROCDAMT),0) into sub_recs_sum from LOANAPP.LOANSUBPROCDTBL where 
        LOANPROCDTYPCD=rec.LOANPROCDTYPCD and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= rec.PROCDTYPCD;         	
             IF rec.PROCDTYPCD = 'E' AND sub_recs_sum != rec.LoanProcdAmt THEN
                p_ErrSeqNmb := ADD_ERROR (p_LoanAppNmb,
                                          p_ErrSeqNmb,
                                          4289,
                                          p_TransInd,
                                          TO_CHAR (rec.PROCDTYPCD)||TO_CHAR (rec.LOANPROCDTYPCD),
                                          TO_CHAR (rec.LOANPROCDSEQNMB),
                                          null,
                                          null,
                                          NULL,
                                          null);
             END IF;
    end loop;
            p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
END;
/

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOANAPP.LOANPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOANAPP.LOANSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANPROCDTOTAMTSELTSP;

    /* Select from LOANAPP.LOANPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANPROCDTOTAMTSELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDSUMTSP (
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
	p_PROCDTYPCD 				CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANSUBPROCDSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOANAPP.LOANSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD and LOANAPPNMB=p_LoanAppNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOANAPP.LOANPROCDTBL set LOANPROCDAMT = quan where PROCDTYPCD = p_PROCDTYPCD and LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDSUMTSP;
            RAISE;
        END;
END LOANSUBPROCDSUMTSP;
/

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSUMTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_PROCDTYPCD   CHAR:=NULL,
    p_RetVal       OUT NUMBER,
    p_SelCur       OUT SYS_REFCURSOR)
AS
BEGIN
    SAVEPOINT LOANSUBPROCDSELTSP;

    /* Select from LOANSUBPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	 
                LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM LOANAPP.LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD AND  PROCDTYPCD=p_PROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDSELTSP;
            RAISE;
        END;
END LOANSUBPROCDSELTSP;
/
GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDSELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select LOANPROCDSEQNMB from LOANAPP.LOANPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
    v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    
BEGIN
    SAVEPOINT LOANSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB =0 then 
            LOANAPP.LOANPROCDINSTSP(0,v_RetVal,p_LoanAppNmb,v1_LoanProcdSeqNmb,p_LoanProcdTypCd,p_PROCDTYPCD,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,v_LoanProcdCreatUserId,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOANAPP.LOANSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANSUBPROCDINSTSP;
            RAISE;
        END;
END LOANSUBPROCDINSTSP;
/
GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDINSTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDDELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDDELTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanSubProcdId 	NUMBER:=0
)
AS
v_LOANAPPNMB NUMBER :=0;
v_LOANPROCDTYPCD CHAR(3) := NULL;
v_PROCDTYPCD CHAR(1) := NULL;

BEGIN
	SAVEPOINT LOANSUBPROCDDELTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
        select LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD into v_LOANAPPNMB, v_LOANPROCDTYPCD, v_PROCDTYPCD from LOANAPP.LOANSUBPROCDTBL where LOANSUBPROCDID = p_LOANSUBPROCDID;
		DELETE  LOANSUBPROCDTBL
		WHERE LOANSUBPROCDID = p_LOANSUBPROCDID;
		
        LOANSUBPROCDSUMTSP(v_LOANAPPNMB,v_LOANPROCDTYPCD,v_PROCDTYPCD);
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANSUBPROCDDELTSP;
RAISE;
END;
END LOANSUBPROCDDELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANSUBPROCDDELALLTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDDELALLTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanAppNmb 	NUMBER:=0
)
AS

BEGIN
	SAVEPOINT LOANSUBPROCDDELALLTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
		DELETE  LoanSubProcdTbl
		WHERE LoanAppNmb = p_LoanAppNmb
		and LOANPROCDTYPCD not in (select LOANPROCDTYPCD from LOANAPP.LoanProcdTbl where LOANAPPNMB=p_LoanAppNmb) and PROCDTYPCD='E';
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANSUBPROCDDELALLTSP;
RAISE;
END;
END LOANSUBPROCDDELALLTSP;
/

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.EXTRACTSELCSP (p_LoanAppNmb       IN     NUMBER := NULL,
                                                   p_PrtId                   NUMBER := 0,
                                                   p_RetVal              OUT NUMBER,
                                                   p_SelCur1             OUT SYS_REFCURSOR,
                                                   p_SelCur2             OUT SYS_REFCURSOR,
                                                   p_SelCur3             OUT SYS_REFCURSOR,
                                                   p_SelCur4             OUT SYS_REFCURSOR,
                                                   p_SelCur5             OUT SYS_REFCURSOR,
                                                   p_SelCur6             OUT SYS_REFCURSOR,
                                                   p_SelCur7             OUT SYS_REFCURSOR,
                                                   p_SelCur8             OUT SYS_REFCURSOR,
                                                   p_SelCur9             OUT SYS_REFCURSOR,
                                                   p_SelCur10            OUT SYS_REFCURSOR,
                                                   p_SelCur11            OUT SYS_REFCURSOR,
                                                   p_SelCur12            OUT SYS_REFCURSOR,
                                                   p_SelCur13            OUT SYS_REFCURSOR,
                                                   p_SelCur14            OUT SYS_REFCURSOR,
                                                   p_SelCur15            OUT SYS_REFCURSOR,
                                                   p_SelCur16            OUT SYS_REFCURSOR,
                                                   p_SelCur17            OUT SYS_REFCURSOR,
                                                   p_SelCur18            OUT SYS_REFCURSOR,
                                                   p_SelCur19            OUT SYS_REFCURSOR,
                                                   p_SelCur20            OUT SYS_REFCURSOR,
                                                   p_SelCur21            OUT SYS_REFCURSOR,
                                                   p_SelCur22            OUT SYS_REFCURSOR,
                                                   p_SelCur23            OUT SYS_REFCURSOR,
                                                   p_SelCur24            OUT SYS_REFCURSOR,
                                                   p_SelCur25            OUT SYS_REFCURSOR,
                                                   p_SelCur26            OUT SYS_REFCURSOR,
                                                   p_SelCur27            OUT SYS_REFCURSOR,
                                                   p_SelCur28            OUT SYS_REFCURSOR,
                                                   p_SelCur29            OUT SYS_REFCURSOR,
                                                   p_SelCur30            OUT SYS_REFCURSOR,
                                                   p_SelCur31            OUT SYS_REFCURSOR,
                                                   p_SelCur32            OUT SYS_REFCURSOR,
                                                   p_SelCur33            OUT SYS_REFCURSOR,
                                                   p_SelCur34            OUT SYS_REFCURSOR,
                                                   p_SelCur35            OUT SYS_REFCURSOR,
                                                   p_SelCur36            OUT SYS_REFCURSOR,
                                                   p_SelCur37            OUT SYS_REFCURSOR,
                                                   p_SelCur38            OUT SYS_REFCURSOR,
                                                   p_SelCur39            OUT SYS_REFCURSOR,
                                                   p_SelCur40            OUT SYS_REFCURSOR,
                                                   p_SelCur41            OUT SYS_REFCURSOR,
                                                   p_SelCur42            OUT SYS_REFCURSOR,
                                                   p_SelCur43            OUT SYS_REFCURSOR,
                                                   p_SelCur44            OUT SYS_REFCURSOR)
AS
   /*SB -- 07/07/2014 This procedure created for Extract XML */
   /*SB -- 02/24/2015 Added p_selcur35  */
   /*SB -- 03/27/2015 Added p_selcur36  */
   /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met*/
   /*SB -- 04/07/2016 Added p_selcur37  */
   /*BR -- 10/21/2016 Added p_selcur38, p_selcur39 */
   /*Nk -- 03/10/2017 OPSMDEV 1385 Added p_selcur40*/
   --SS--07/12/2018 OPSMDEV 1860 Added p_selcur 42 and 43
   -- JP 08/06/2019 OPSMDEV 2255 modified call to LoanAppSelTSP 
   v_RetVal   NUMBER;
   v_PrtId    NUMBER (7);
BEGIN
   -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
   SELECT COUNT (*)
     INTO p_RetVal
     FROM loanapp.LoanAppTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF NVL (p_PrtId, 0) = 0
   THEN
      BEGIN
         SELECT PrtId
           INTO v_PrtId
           FROM loanapp.LoanAppPrtTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PrtId := NULL;
      END;
   END IF;

   IF p_RetVal = 0
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur2 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur3 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur4 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur5 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur6 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur7 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur8 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur9 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur10 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur11 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur12 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur13 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur14 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur15 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur16 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur17 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur18 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur19 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur20 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur21 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur22 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur23 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur24 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur25 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur26 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur27 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur28 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur29 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur30 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur31 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur32 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur33 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur34 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur35 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur36 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur37 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur38 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur39 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur40 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur41 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur42 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur43 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
        OPEN p_SelCur44 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
      END;
   ELSE
      BEGIN
         --LoanAppSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur1);
         -- JP Modifed LoanAppSelTSP for 2255 
        LoanAppSelTSP (p_identifier => 38, 
                       p_loanappnmb => p_LoanAppNmb, 
                       p_retval => v_RetVal, 
                       p_selcur => p_SelCur1);
         LoanAppARCRsnSelTSP (38, v_RetVal, p_LoanAppNmb, NULL, p_SelCur2);
         LoanAppAsstAreaSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur3);
         LoanAppCAInfoSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur4);
         LoanAppChngBusOwnrshpSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur5);
         LoanAppCnselngSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur6);
         LoanAppCnselngSrcSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur7);
         LoanAppEligSelTSP (38, p_LoanAppNmb, NULL, p_SelCur8);
         LoanAppPrtSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur9);
         LoanBorrSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur10);
         LoanBSSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur11);
         LoanCollatSelTSP (38, p_LoanAppNmb, NULL, p_SelCur12);
         LoanCollatLienSelTSP (38, p_LoanAppNmb, NULL, NULL, p_SelCur13);
         LoanCrdtUnavRsnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
         LoanFedEmpSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur15);
         LoanGuarSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur16);
         LoanIndbtnesSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur17);
         LoanInjctnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur18);
         LoanIntDtlSelTSP (38, p_LoanAppNmb, p_SelCur19);
         LoanISSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur20);
         LoanPartLendrSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur21);
         LoanPerFinanSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur22);
         LoanPrevFinanSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur23);
         LoanPrinSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur24);
         LoanPrinRaceSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur25);
         LoanProcdSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur26);
         LoanPrtCmntSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur27);
         LoanSpcPurpsSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur28);
         LqdCrSelCSP (38, p_LoanAppNmb, v_RetVal, p_SelCur29, p_SelCur30, p_SelCur31, p_SelCur32, p_SelCur33);
         partner.PrtAgrmtSelTSP (38, v_RetVal, v_prtid, NULL, NULL, NULL, NULL, p_SelCur34);
         loanapp.LoanBorrRaceSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur35);
         LOANAPP.LoanAppBusApprSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur36);
         LOANAPP.LoanAppCAUndrServMrktSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur37);
         LOANAPP.LoanLmtGntyCollatSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, p_SelCur38, v_RetVal);
         LOANAPP.LoanStbyAgrmtDtlSelTSP (38, p_LoanAppNmb, NULL, p_SelCur39, v_RetVal);
         LOANAPP.LoanEconDevObjctChldSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR40, v_RetVal);
         LOANAPP.LoanExprtCntrySelTsp (38, p_LoanAppNmb, p_SELCUR41, v_RetVal);
         LoanApp.LoanAgntSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR42, v_RetVal);
         LoanApp.LoanAgntfeeDtlSelTSp (38, p_LoanAppNmb, NULL, p_SELCUR43, v_RetVal);
         LoanApp.LoanSubProcdSelTSP (38, p_LoanAppNmb, NULL,NULL,v_RetVal,p_SelCur44);
      END;
   END IF;
END ExtractSelCSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANCOPYCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCOPYCSP (
    p_LoanAppNmb          NUMBER := NULL,
    p_CreatUserId         VARCHAR2 := NULL,
    p_NEWLoanAppNmb   OUT NUMBER)
AS
    /*
     created by:   BJRychener
     created date: October 31, 2017
     purpose:      Loan Copy Procedure
     Revision:

    */
    /* NK--11/90/2017--OPSMDEV 1551 added PerFedAgncySDPIEInd, PerCSP60DayDelnqInd,PerLglActnInd, PerCitznShpCntNm ,PerAltPhnNmb, PerPrimEmailAdr ,PerAltEmailAdr To LoanPerTbl
       NK--11/13/2017 OPSMDEV 1551  Added BusAltPhnNmb ,  BusPrimEmailAdr ,  BusAltEmailAdr, BusCSP60DayDelnqInd, BusLglActnInd to LoanBusTbl
       NK--11/13/2017 OPSMDEV 1558  Added BusSexNatrInd,BusNonFmrSBAEmpInd,BusNonLegBrnchEmpInd,BusNonFedEmpInd,BusNonGS13EmpInd,BusNonSBACEmpInd to LoanBusTbl
       BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
       NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to LoanPrevFinanTbl
       SS 11/15/2017--OPSMDEV 1573 Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,
       LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn,LoanPrtAltCntctCellPhn,
              LoanPrtAltCntctemail, LendrAltCntctTypCd to LOANAPPPRTTBL
       SS--11/15/2017 --OPSMDEV 1566 Added LOANBUSESTDT to Loanappprojtbl and BusPrimCntctNm  to LoanBustbl
       NK-- 11/15/2017--OPSMDEV 1591 Added PeredAgncyLoanInd to LoanPerTbl
       NK--11/15/2017--OPSMDEV 1591 Added BusedAgncyLoanInd to LoanBusTbl
       NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to LoanBusTbl
       NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to LoanAppTbl
       BJR-- 02/01/2018--CAFSOPER-1407-- added UPPER() to srch names to allow records to be stored in caps and easier to find during searches.
       RY-- 04/20/2018--OPSMDEV--1786 --Added WorkrsCompInsRqrdInd to LoanBorrTbl
       SS--08/06/2018--OPSMDEV--1882-- Added LoanAGNtTbl and LoanAgntfeedtltbl
       SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to loanapptbl
       BR--10/30/2018--OPSMDEV 1885 -- added LOANAPPPROJGEOCD, LOANAPPHUBZONEIND, LOANAPPPROJLAT, LOANAPPPROJLONG
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
       SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to loanapptbl
       SS--02/18/2020 OPSMDEV 2409,2410 Added LoanAppCBRSInd,LoanAppOppZoneInd to LoanAppProjTbl
       SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to loanapptbl
       SS--09/05/2020 CARESACT 595 copying demographics from loan master tables
       SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to LoanAppProjTbl
       JS--02/11/2021 Loan Authorization Modernization. Added 7 new columns LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to LoanApp.LoanGuarTbl.
       SL-04/26/2021--OPSMDEV-2576  Addeed new column PerTAXIDCertInd to LoanPerTbl
    */

    v_NEWLoanAppNmb   NUMBER := NULL;
    v_count           NUMBER;
BEGIN
    BEGIN
        --Generate New Loan App Number using MAX(LoanAppNmb) from LoanApp.LoanAppTbl
        LoanApp.IncrmntSeqNmbCSP ('Application', v_NEWLoanAppNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanAppTbl
        INSERT INTO LoanApp.LoanAppTbl (LoanAppNmb,
                                        PrgmCd,
                                        PrcsMthdCd,
                                        LoanAppNm,
                                        LoanAppRecvDt,
                                        LoanAppEntryDt,
                                        LoanAppRqstAmt,
                                        LoanAppSBAGntyPct,
                                        LoanAppSBARcmndAmt,
                                        LoanAppRqstMatMoQty,
                                        LoanAppEPCInd,
                                        LoanAppPymtAmt,
                                        LoanAppFullAmortPymtInd,
                                        LoanAppMoIntQty,
                                        LoanAppLifInsurRqmtInd,
                                        LoanAppRcnsdrtnInd,
                                        LoanAppInjctnInd,
                                        LoanAppEWCPSnglTransPostInd,
                                        LoanAppEWCPSnglTransInd,
                                        LoanAppEligEvalInd,
                                        LoanAppNewBusCd,
                                        LoanAppDisasterCntrlNmb,
                                        LoanCollatInd,
                                        LoanAppSourcTypCd,
                                        StatCd,
                                        LoanAppStatUserId,
                                        LoanAppStatDt,
                                        LoanAppOrigntnOfcCd,
                                        LoanAppPrcsOfcCd,
                                        LoanAppAppvDt,
                                        RecovInd,
                                        LoanAppMicroLendrId,
                                        LOANDISASTRAPPNMB,
                                        LOANDISASTRAPPFEECHARGED,
                                        LOANDISASTRAPPDCSN,
                                        LOANASSOCDISASTRAPPNMB,
                                        LOANASSOCDISASTRLOANNMB,
                                        LoanAppCreatUserId,
                                        LoanAppCreatDt,
                                        LastUpdtUserId,
                                        LastUpdtDt,
                                        LOANAPPNETEARNIND,
                                        LOANAPPSERVOFCCD,
                                        LoanAppRevlInd,
                                        LOANNMB,
                                        LOANAPPFUNDDT,
                                        UNDRWRITNGBY,
                                        COHORTCD,
                                        FUNDSFISCLYR,
                                        LOANTYPIND,
                                        LOANAPPINTDTLCD,
                                        LOANPYMNINSTLMNTFREQCD,
                                        FININSTRMNTTYPIND,
                                        LOANAMTLIMEXMPTIND,
                                        LoanPymntSchdlFreq,
                                        LoanPymtRepayInstlTypCd,
                                        LOANGNTYMATSTIND,
                                        LoanGntyNoteDt,
                                        CDCServFeePct,
                                        LoanAppContribAmt,
                                        LoanAppContribPct,
                                        LoanAppCDCGntyAmt,
                                        LOANAPPCDCNETDBENTRAMT,
                                        LoanAppCDCFundFeeAmt,
                                        LOANAPPCDCSEPRTPRCSFEEIND,
                                        LoanAppCDCPrcsFeeAmt,
                                        LoanAppCDCClsCostAmt,
                                        LoanAppCDCUndrwtrFeeAmt,
                                        LoanAppCDCGrossDbentrAmt,
                                        LoanAppBalToBorrAmt,
                                        LoanAppOutPrgrmAreaOfOperInd,
                                        LoanAppRevlMoIntQty,
                                        LoanAppAmortMoIntQty,
                                        LoanAppExtraServFeeAMT,
                                        LoanAppExtraServFeeInd,
                                        RqstSeqNmb,
                                        LoanAppMonthPayroll)
            SELECT v_NEWLoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   LoanAppNm,
                   NULL,                                      --LoanAppRecvDt,
                   SYSDATE,                                  --LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   LoanAppEligEvalInd,
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   LoanCollatInd,
                   LoanAppSourcTypCd,
                   'IP',
                   p_CreatUserId,                         --LoanAppStatUserId,
                   SYSDATE,                                   --LoanAppStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   NULL,
                   RecovInd,
                   LoanAppMicroLendrId,
                   NULL,                                  --LOANDISASTRAPPNMB,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   p_CreatUserId,                        --LoanAppCreatUserId,
                   SYSDATE,                                  --LoanAppCreatDt,
                   p_CreatUserId,                            --LastUpdtUserId,
                   SYSDATE,                                      --LastUpdtDt,
                   LOANAPPNETEARNIND,
                   LOANAPPSERVOFCCD,
                   LoanAppRevlInd,
                   NULL,                                            --LOANNMB,
                   NULL,                                      --LOANAPPFUNDDT,
                   UNDRWRITNGBY,
                   NULL,                                           --COHORTCD,
                   NULL,                                       --FUNDSFISCLYR,
                   LOANTYPIND,
                   LOANAPPINTDTLCD,
                   LOANPYMNINSTLMNTFREQCD,
                   FININSTRMNTTYPIND,
                   LOANAMTLIMEXMPTIND,
                   LoanPymntSchdlFreq,
                   LoanPymtRepayInstlTypCd,
                   LOANGNTYMATSTIND,
                   NULL,                                     --LoanGntyNoteDt,
                   CDCServFeePct,
                   LoanAppContribAmt,
                   LoanAppContribPct,
                   LoanAppCDCGntyAmt,
                   LOANAPPCDCNETDBENTRAMT,
                   LoanAppCDCFundFeeAmt,
                   LOANAPPCDCSEPRTPRCSFEEIND,
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppCDCGrossDbentrAmt,
                   LoanAppBalToBorrAmt,
                   LoanAppOutPrgrmAreaOfOperInd,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   RqstSeqNmb,
                   LoanAppMonthPayroll
              FROM LoanApp.LoanAppTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppProjTbl
        INSERT INTO LoanApp.LoanAppProjTbl (LoanAppNmb,
                                            LoanAppProjStr1Nm,
                                            LoanAppProjStr2Nm,
                                            LoanAppProjCtyNm,
                                            LoanAppProjCntyCd,
                                            LoanAppProjStCd,
                                            LoanAppProjZipCd,
                                            LoanAppProjZip4Cd,
                                            LoanAppRuralUrbanInd,
                                            LoanAppNetExprtAmt,
                                            LoanAppCurrEmpQty,
                                            LoanAppJobCreatQty,
                                            LoanAppJobRtnd,
                                            LoanAppJobRqmtMetInd,
                                            LoanAppCDCJobRat,
                                            LoanAppProjCreatUserId,
                                            LoanAppProjCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            NAICSYrNmb,
                                            NAICSCd,
                                            LoanAppFrnchsCd,
                                            LoanAppFrnchsNm,
                                            LoanAppFrnchsInd,
                                            ADDTNLLOCACQLMTIND,
                                            FIXASSETACQLMTIND,
                                            FIXASSETACQLMTAMT,
                                            COMPSLMTIND,
                                            COMPSLMTAMT,
                                            BULKSALELAWCOMPLYIND,
                                            FRNCHSRECRDACCSIND,
                                            FRNCHSFEEDEFMONNMB,
                                            FRNCHSFEEDEFIND,
                                            FRNCHSTERMNOTCIND,
                                            FRNCHSLENDRSAMEOPPIND,
                                            LOANBUSESTDT,
                                            LOANAPPPROJGEOCD,
                                            LOANAPPHUBZONEIND,
                                            LOANAPPPROJLAT,
                                            LOANAPPPROJLONG,
                                            LoanAppCBRSInd,
                                            LoanAppOppZoneInd,
                                            LOANAPPPROJLMIND,
                                            LOANAPPPROJSIZECD)
            SELECT v_NEWLoanAppNmb,
                   LoanAppProjStr1Nm,
                   LoanAppProjStr2Nm,
                   LoanAppProjCtyNm,
                   LoanAppProjCntyCd,
                   LoanAppProjStCd,
                   LoanAppProjZipCd,
                   LoanAppProjZip4Cd,
                   LoanAppRuralUrbanInd,
                   LoanAppNetExprtAmt,
                   LoanAppCurrEmpQty,
                   LoanAppJobCreatQty,
                   LoanAppJobRtnd,
                   LoanAppJobRqmtMetInd,
                   LoanAppCDCJobRat,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NAICSYrNmb,
                   NAICSCd,
                   LoanAppFrnchsCd,
                   LoanAppFrnchsNm,
                   LoanAppFrnchsInd,
                   ADDTNLLOCACQLMTIND,
                   FIXASSETACQLMTIND,
                   FIXASSETACQLMTAMT,
                   COMPSLMTIND,
                   COMPSLMTAMT,
                   BULKSALELAWCOMPLYIND,
                   FRNCHSRECRDACCSIND,
                   FRNCHSFEEDEFMONNMB,
                   FRNCHSFEEDEFIND,
                   FRNCHSTERMNOTCIND,
                   FRNCHSLENDRSAMEOPPIND,
                   LOANBUSESTDT,
                   LOANAPPPROJGEOCD,
                   LOANAPPHUBZONEIND,
                   LOANAPPPROJLAT,
                   LOANAPPPROJLONG,
                   LoanAppCBRSInd,
                   LoanAppOppZoneInd,
                   LOANAPPPROJLMIND,
                   LOANAPPPROJSIZECD
              FROM LoanApp.LoanAppProjTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanInjctnTbl
        INSERT INTO LoanApp.LoanInjctnTbl (LoanAppNmb,
                                           LoanInjctnSeqNmb,
                                           InjctnTypCd,
                                           LoanInjctnOthDescTxt,
                                           LoanInjctnAmt,
                                           LoanInjctnCreatUserId,
                                           LoanInjctnCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanInjctnFundTermYrNmb)
            SELECT v_NEWLoanAppNmb,
                   LoanInjctnSeqNmb,
                   InjctnTypCd,
                   LoanInjctnOthDescTxt,
                   LoanInjctnAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanInjctnFundTermYrNmb
              FROM LoanApp.LoanInjctnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanStbyAgrmtDtlTBL
        INSERT INTO LoanApp.LoanStbyAgrmtDtlTBL (LOANSTBYAGRMTDTLSEQNMB,
                                                 LOANAPPNMB,
                                                 LOANSTBYCRDTRNM,
                                                 LOANSTBYAMT,
                                                 LOANSTBYREPAYTYPCD,
                                                 LOANSTBYPYMTINTRT,
                                                 LOANSTBYPYMTOTHDESCTXT,
                                                 LOANSTBYPYMTAMT,
                                                 LOANSTBYPYMTSTRTDT,
                                                 CREATUSERID,
                                                 CREATDT,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
            SELECT LOANSTBYAGRMTDTLSEQNMB,
                   v_NEWLoanAppNmb,
                   LOANSTBYCRDTRNM,
                   LOANSTBYAMT,
                   LOANSTBYREPAYTYPCD,
                   LOANSTBYPYMTINTRT,
                   LOANSTBYPYMTOTHDESCTXT,
                   LOANSTBYPYMTAMT,
                   LOANSTBYPYMTSTRTDT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanStbyAgrmtDtlTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanSpcPurpsTbl
        INSERT INTO LoanApp.LoanSpcPurpsTbl (LoanAppNmb,
                                             LoanSpcPurpsSeqNmb,
                                             SpcPurpsLoanCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanSpcPurpsSeqNmb,
                   SpcPurpsLoanCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanSpcPurpsTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppARCRsnTbl
        INSERT INTO LoanApp.LoanAppARCRsnTbl (LoanAppNmb,
                                              ARCLoanRsnCd,
                                              ARCLoanRsnOthDescTxt,
                                              CreateUserId,
                                              CreateDt,
                                              LASTUPDTUSERID,
                                              LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   ARCLoanRsnCd,
                   ARCLoanRsnOthDescTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAppARCRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCrdtUnavRsnTbl
        INSERT INTO LoanApp.LoanCrdtUnavRsnTbl (LoanAppNmb,
                                                LoanCrdtUnavRsnSeqNmb,
                                                LoanCrdtUnavRsnCd,
                                                LoanCrdtUnavRsnTxt,
                                                LoanCrdtUnavRsnCreatUserId,
                                                LoanCrdtUnavRsnCreatDt,
                                                LastUpdtUserId,
                                                LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanCrdtUnavRsnSeqNmb,
                   LoanCrdtUnavRsnCd,
                   LoanCrdtUnavRsnTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanCrdtUnavRsnTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanProcdTbl
        INSERT INTO LoanApp.LoanProcdTbl (LoanAppNmb,
                                          LoanProcdSeqNmb,
                                          LoanProcdTypCd,
                                          ProcdTypCd,
                                          LoanProcdOthTypTxt,
                                          LoanProcdAmt,
                                          LoanProcdCreatUserId,
                                          LoanProcdCreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LoanProcdRefDescTxt,
                                          LoanProcdPurAgrmtDt,
                                          LoanProcdPurIntangAssetAmt,
                                          LoanProcdPurIntangAssetDesc,
                                          LoanProcdPurStkHldrNm,
                                          NCAIncldInd,
                                          StkPurCorpText)
            SELECT v_NEWLoanAppNmb,
                   LoanProcdSeqNmb,
                   LoanProcdTypCd,
                   ProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanApp.LoanProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

   BEGIN
        --Copy to LOANAPP.LOANSUBPROCDTBL
        INSERT INTO LOANAPP.LOANSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
            SELECT  LOANAPP.LOANSUBPROCDIDSEQ.nextval,
                    v_NEWLoanAppNmb,
                    LoanProcdTypCd,
                    PROCDTYPCD,
                    LOANPROCDTXTBLCK,
                    LoanProcdAmt,
		    LoanProcdAddr,
		    LOANSUBPROCDTYPCD,
		    LoanProcdDesc,	
		    Lender
              FROM LoanApp.LOANSUBPROCDTBL
             WHERE  LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppChngBusOwnrshpTbl
        INSERT INTO LoanApp.LoanAppChngBusOwnrshpTbl (
                        LOANAPPNMB,
                        TOTPYMTAMT,
                        LOAN7APYMTAMT,
                        SELLERFINANFULLSTBYAMT,
                        SELLERFINANNONFULLSTBYAMT,
                        BUYEREQTYCASHAMT,
                        BUYEREQTYCASHDESCTXT,
                        BUYEREQTYBORRAMT,
                        BUYEREQTYOTHAMT,
                        BUYEREQTYOTHDESCTXT,
                        TOTASSETAMT,
                        ACCTRECVASSETAMT,
                        INVTRYASSETAMT,
                        REASSETAMT,
                        REVALTYPIND,
                        EQUIPASSETAMT,
                        EQUIPVALTYPIND,
                        FIXASSETAMT,
                        INTANGASSETAMT,
                        OTHASSETAMT,
                        OTHASSETDESCTXT,
                        COVNTASSETAMT,
                        CUSTASSETAMT,
                        LICNSASSETAMT,
                        FRNCHSASSETAMT,
                        GOODWILLASSETAMT,
                        OTHINTANGASSETAMT,
                        OTHINTANGASSETDESCTXT,
                        TOTAPPRAMT,
                        BUSAPPRNM,
                        BUSAPPRFEEAMT,
                        BUSAPPRASAIND,
                        BUSAPPRCBAIND,
                        BUSAPPRABVIND,
                        BUSAPPRCVAIND,
                        BUSAPPRAVAIND,
                        BUSAPPRCPAIND,
                        BUSBRKRCOMISNIND,
                        BUSBRKRNM,
                        BUSBRKRCOMISNAMT,
                        BUSBRKRADR,
                        CREATUSERID,
                        CREATDT,
                        LASTUPDTUSERID,
                        LASTUPDTDT,
                        BUSAPPRLIVIND)
            SELECT v_NEWLoanAppNmb,
                   TOTPYMTAMT,
                   LOAN7APYMTAMT,
                   SELLERFINANFULLSTBYAMT,
                   SELLERFINANNONFULLSTBYAMT,
                   BUYEREQTYCASHAMT,
                   BUYEREQTYCASHDESCTXT,
                   BUYEREQTYBORRAMT,
                   BUYEREQTYOTHAMT,
                   BUYEREQTYOTHDESCTXT,
                   TOTASSETAMT,
                   ACCTRECVASSETAMT,
                   INVTRYASSETAMT,
                   REASSETAMT,
                   REVALTYPIND,
                   EQUIPASSETAMT,
                   EQUIPVALTYPIND,
                   FIXASSETAMT,
                   INTANGASSETAMT,
                   OTHASSETAMT,
                   OTHASSETDESCTXT,
                   COVNTASSETAMT,
                   CUSTASSETAMT,
                   LICNSASSETAMT,
                   FRNCHSASSETAMT,
                   GOODWILLASSETAMT,
                   OTHINTANGASSETAMT,
                   OTHINTANGASSETDESCTXT,
                   TOTAPPRAMT,
                   BUSAPPRNM,
                   BUSAPPRFEEAMT,
                   BUSAPPRASAIND,
                   BUSAPPRCBAIND,
                   BUSAPPRABVIND,
                   BUSAPPRCVAIND,
                   BUSAPPRAVAIND,
                   BUSAPPRCPAIND,
                   BUSBRKRCOMISNIND,
                   BUSBRKRNM,
                   BUSBRKRCOMISNAMT,
                   BUSBRKRADR,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   BUSAPPRLIVIND
              FROM LoanApp.LoanAppChngBusOwnrshpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPrtCmntTbl
        INSERT INTO LoanApp.LoanPrtCmntTbl (LoanAppNmb,
                                            LoanPrtCmntTxt,
                                            LoanPrtCmntCreatUserId,
                                            LoanPrtCmntCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanPrtCmntTxt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM loanApp.LoanPrtCmntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanEconDevObjctChldTbl
        INSERT INTO LoanApp.LoanEconDevObjctChldTbl (LoanEconDevObjctSeqNmb,
                                                     LoanAppNmb,
                                                     LoanEconDevObjctCd,
                                                     CreatUserId,
                                                     CreatDt,
                                                     LastUpdtUserId,
                                                     LastUpdtDt)
            SELECT LoanEconDevObjctSeqNmb,
                   v_NEWLoanAppNmb,
                   LoanEconDevObjctCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanEconDevObjctChldTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanCollatTbl
        INSERT INTO LoanApp.LoanCollatTbl (loanappnmb,
                                           loancollatseqnmb,
                                           loancollattypcd,
                                           loancollatdesc,
                                           loancollatmrktvalamt,
                                           loancollatownrrecrd,
                                           loancollatsbalienpos,
                                           loancollatvalsourccd,
                                           loancollatvaldt,
                                           loancollatcreatuserid,
                                           loancollatcreatdt,
                                           lastupdtuserid,
                                           lastupdtdt,
                                           collatstatcd,
                                           collatsecsrchorddt,
                                           collatsecsrchrcvddt,
                                           collatapporddt,
                                           collatstr1nm,
                                           collatstr2nm,
                                           collatctynm,
                                           collatcntycd,
                                           collatstcd,
                                           collatzipcd,
                                           collatzip4cd,
                                           LoanCollatSubTypCd,
                                           SecurShrPariPassuInd,
                                           SecurShrPariPassuNonSBAInd,
                                           SecurPariPassuLendrNm,
                                           SecurPariPassuAmt,
                                           SecurLienLimAmt,
                                           SecurInstrmntTypCd,
                                           SecurWaterRightInd,
                                           SecurRentAsgnInd,
                                           SecurSellerNm,
                                           SecurPurNm,
                                           SecurOwedToSellerAmt,
                                           SecurCDCDeedInEscrowInd,
                                           SecurSellerIntDtlInd,
                                           SecurSellerIntDtlTxt,
                                           SecurTitlSubjPriorLienInd,
                                           SecurPriorLienTxt,
                                           SecurPriorLienLimAmt,
                                           SecurSubjPriorAsgnInd,
                                           SecurPriorAsgnTxt,
                                           SecurPriorAsgnLimAmt,
                                           SecurALTATltlInsurInd,
                                           SecurLeaseTermOverLoanYrNmb,
                                           LandlordIntProtWaiverInd,
                                           SecurDt,
                                           SecurLessorTermNotcDaysNmb,
                                           SecurDescTxt,
                                           SecurPropAcqWthLoanInd,
                                           SecurPropTypTxt,
                                           SecurOthPropTxt,
                                           SecurLienOnLqorLicnsInd,
                                           SecurMadeYrNmb,
                                           SecurLocTxt,
                                           SecurOwnerNm,
                                           SecurMakeNm,
                                           SecurAmt,
                                           SecurNoteSecurInd,
                                           SecurStkShrNmb,
                                           SecurLienHldrVrfyInd,
                                           SecurTitlVrfyTypCd,
                                           SecurTitlVrfyOthTxt,
                                           FloodInsurRqrdInd,
                                           REHazardInsurRqrdInd,
                                           PerHazardInsurRqrdInd,
                                           FullMarInsurRqrdInd,
                                           EnvInvstgtSBAAppInd,
                                           LeasePrmTypCd,
                                           ApprTypCd)
            SELECT v_NEWLoanAppNmb,
                   loancollatseqnmb,
                   loancollattypcd,
                   loancollatdesc,
                   loancollatmrktvalamt,
                   loancollatownrrecrd,
                   loancollatsbalienpos,
                   loancollatvalsourccd,
                   loancollatvaldt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   collatstatcd,
                   collatsecsrchorddt,
                   collatsecsrchrcvddt,
                   collatapporddt,
                   collatstr1nm,
                   collatstr2nm,
                   collatctynm,
                   collatcntycd,
                   collatstcd,
                   collatzipcd,
                   collatzip4cd,
                   LoanCollatSubTypCd,
                   SecurShrPariPassuInd,
                   SecurShrPariPassuNonSBAInd,
                   SecurPariPassuLendrNm,
                   SecurPariPassuAmt,
                   SecurLienLimAmt,
                   SecurInstrmntTypCd,
                   SecurWaterRightInd,
                   SecurRentAsgnInd,
                   SecurSellerNm,
                   SecurPurNm,
                   SecurOwedToSellerAmt,
                   SecurCDCDeedInEscrowInd,
                   SecurSellerIntDtlInd,
                   SecurSellerIntDtlTxt,
                   SecurTitlSubjPriorLienInd,
                   SecurPriorLienTxt,
                   SecurPriorLienLimAmt,
                   SecurSubjPriorAsgnInd,
                   SecurPriorAsgnTxt,
                   SecurPriorAsgnLimAmt,
                   SecurALTATltlInsurInd,
                   SecurLeaseTermOverLoanYrNmb,
                   LandlordIntProtWaiverInd,
                   SecurDt,
                   SecurLessorTermNotcDaysNmb,
                   SecurDescTxt,
                   SecurPropAcqWthLoanInd,
                   SecurPropTypTxt,
                   SecurOthPropTxt,
                   SecurLienOnLqorLicnsInd,
                   SecurMadeYrNmb,
                   SecurLocTxt,
                   SecurOwnerNm,
                   SecurMakeNm,
                   SecurAmt,
                   SecurNoteSecurInd,
                   SecurStkShrNmb,
                   SecurLienHldrVrfyInd,
                   SecurTitlVrfyTypCd,
                   SecurTitlVrfyOthTxt,
                   FloodInsurRqrdInd,
                   REHazardInsurRqrdInd,
                   PerHazardInsurRqrdInd,
                   FullMarInsurRqrdInd,
                   EnvInvstgtSBAAppInd,
                   LeasePrmTypCd,
                   ApprTypCd
              FROM LoanApp.LoanCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

        --Copy to LoanApp.LoanCollatLienTbl
        INSERT INTO LoanApp.LoanCollatLienTbl (LoanAppNmb,
                                               LoanCollatSeqNmb,
                                               LoanCollatLienSeqNmb,
                                               LoanCollatLienHldrNm,
                                               LoanCollatLienBalAmt,
                                               LoanCollatLienPos,
                                               LoanCollatLienCreatUserId,
                                               LoanCollatLienCreatDt,
                                               LastUpdtUserId,
                                               LastUpdtDt,
                                               LOANCOLLATLIENSTATCD,
                                               LOANCOLLATLIENCMNT)
            SELECT v_NEWLoanAppNmb,
                   LoanCollatSeqNmb,
                   LoanCollatLienSeqNmb,
                   LoanCollatLienHldrNm,
                   LoanCollatLienBalAmt,
                   LoanCollatLienPos,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANCOLLATLIENSTATCD,
                   LOANCOLLATLIENCMNT
              FROM LoanCollatLienTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPartLendrTbl
        INSERT INTO LoanApp.LoanPartLendrTbl (LoanAppNmb,
                                              LoanPartLendrSeqNmb,
                                              LoanPartLendrTypCd,
                                              LoanPartLendrAmt,
                                              LocId,
                                              LoanPartLendrNm,
                                              LoanPartLendrStr1Nm,
                                              LoanPartLendrStr2Nm,
                                              LoanPartLendrCtyNm,
                                              LoanPartLendrStCd,
                                              LoanPartLendrZip5Cd,
                                              LoanPartLendrZip4Cd,
                                              LoanPartLendrOfcrLastNm,
                                              LoanPartLendrOfcrFirstNm,
                                              LoanPartLendrOfcrInitialNm,
                                              LoanPartLendrOfcrSfxNm,
                                              LoanPartLendrOfcrTitlTxt,
                                              LoanPartLendrOfcrPhnNmb,
                                              LoanLienPosCd,
                                              LoanPartLendrSrLienPosFeeAmt,
                                              LoanPoolAppNmb,
                                              LOANLENDRTAXID,
                                              LOANLENDRSERVFEEPCT,
                                              LOANLENDRPARTPCT,
                                              LOANORIGPARTPCT,
                                              LoanSBAGntyBalPct,
                                              LOANLENDRPARTAMT,
                                              LOANORIGPARTAMT,
                                              LOANLENDRGROSSAMT,
                                              LOANSBAGNTYBALAMT,
                                              Loan504PartLendrSeqNmb,
                                              LoanPartLendrCreatUserId,
                                              LoanPartLendrCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              loanpartlendrcntrycd,
                                              loanpartlendrpostcd,
                                              loanpartlendrstnm)
            SELECT v_NEWLoanAppNmb,
                   LoanPartLendrSeqNmb,
                   LoanPartLendrTypCd,
                   LoanPartLendrAmt,
                   LocId,
                   LoanPartLendrNm,
                   LoanPartLendrStr1Nm,
                   LoanPartLendrStr2Nm,
                   LoanPartLendrCtyNm,
                   LoanPartLendrStCd,
                   LoanPartLendrZip5Cd,
                   LoanPartLendrZip4Cd,
                   LoanPartLendrOfcrLastNm,
                   LoanPartLendrOfcrFirstNm,
                   LoanPartLendrOfcrInitialNm,
                   LoanPartLendrOfcrSfxNm,
                   LoanPartLendrOfcrTitlTxt,
                   LoanPartLendrOfcrPhnNmb,
                   LoanLienPosCd,
                   LoanPartLendrSrLienPosFeeAmt,
                   LoanPoolAppNmb,
                   LOANLENDRTAXID,
                   LOANLENDRSERVFEEPCT,
                   LOANLENDRPARTPCT,
                   LOANORIGPARTPCT,
                   LoanSBAGntyBalPct,
                   LOANLENDRPARTAMT,
                   LOANORIGPARTAMT,
                   LOANLENDRGROSSAMT,
                   LOANSBAGNTYBALAMT,
                   Loan504PartLendrSeqNmb,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   loanpartlendrcntrycd,
                   loanpartlendrpostcd,
                   loanpartlendrstnm
              FROM LoanApp.LoanPartLendrTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    --Copy to LoanApp.LoanBusTbl
    INSERT INTO LoanApp.LoanBusTbl (LoanAppNmb,
                                    TaxId,
                                    BusEINCertInd,
                                    BusTypCd,
                                    BusNm,
                                    BusSrchNm,
                                    BusTrdNm,
                                    BusBnkrptInd,
                                    BusLwsuitInd,
                                    BusPriorSBALoanInd,
                                    BusCurBnkNm,
                                    BusChkngBalAmt,
                                    BusCurOwnrshpDt,
                                    BusPrimPhnNmb,
                                    BusPhyAddrStr1Nm,
                                    BusPhyAddrStr2Nm,
                                    BusPhyAddrCtyNm,
                                    BusPhyAddrStCd,
                                    BusPhyAddrStNm,
                                    BusPhyAddrCntCd,
                                    BusPhyAddrZipCd,
                                    BusPhyAddrZip4Cd,
                                    BusPhyAddrPostCd,
                                    BusMailAddrStr1Nm,
                                    BusMailAddrStr2Nm,
                                    BusMailAddrCtyNm,
                                    BusMailAddrStCd,
                                    BusMailAddrStNm,
                                    BusMailAddrCntCd,
                                    BusMailAddrZipCd,
                                    BusMailAddrZip4Cd,
                                    BusMailAddrPostCd,
                                    BusCreatUserId,
                                    BusCreatDt,
                                    BusExprtInd,
                                    BusExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    BusExtrnlCrdtScorNmb,
                                    BusExtrnlCrdtScorDt,
                                    BusDunsNMB,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    BusAltPhnNmb,
                                    BusPrimEmailAdr,
                                    BusAltEmailAdr,
                                    BusCSP60DayDelnqInd,
                                    BusLglActnInd,
                                    BusFedAgncySDPIEInd,
                                    BusSexNatrInd,
                                    BusNonFmrSBAEmpInd,
                                    BusNonLegBrnchEmpInd,
                                    BusNonFedEmpInd,
                                    BusNonGS13EmpInd,
                                    BusNonSBACEmpInd,
                                    BusPrimCntctNm,
                                    -- BusPrimCntctEmail,
                                    BusFedAgncyLoanInd,
                                    BusOutDbtInd)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.BusEINCertInd, a.BusEINCertInd),
               NVL (b.BusTypCd, a.BusTypCd),
               NVL (b.BusNm, a.BusNm),
               NVL (b.BusSrchNm, a.BusSrchNm),
               a.BusTrdNm,
               a.BusBnkrptInd,
               a.BusLwsuitInd,
               a.BusPriorSBALoanInd,
               a.BusCurBnkNm,
               a.BusChkngBalAmt,
               a.BusCurOwnrshpDt,
               a.BusPrimPhnNmb,
               a.BusPhyAddrStr1Nm,
               a.BusPhyAddrStr2Nm,
               a.BusPhyAddrCtyNm,
               a.BusPhyAddrStCd,
               a.BusPhyAddrStNm,
               a.BusPhyAddrCntCd,
               a.BusPhyAddrZipCd,
               a.BusPhyAddrZip4Cd,
               a.BusPhyAddrPostCd,
               a.BusMailAddrStr1Nm,
               a.BusMailAddrStr2Nm,
               a.BusMailAddrCtyNm,
               a.BusMailAddrStCd,
               a.BusMailAddrStNm,
               a.BusMailAddrCntCd,
               a.BusMailAddrZipCd,
               a.BusMailAddrZip4Cd,
               a.BusMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.BusExprtInd,
               a.BusExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.BusExtrnlCrdtScorNmb,
               a.BusExtrnlCrdtScorDt,
               a.BusDunsNMB,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               b.VETCERTIND,
               a.BusAltPhnNmb,
               a.BusPrimEmailAdr,
               a.BusAltEmailAdr,
               a.BusCSP60DayDelnqInd,
               a.BusLglActnInd,
               a.BusFedAgncySDPIEInd,
               a.BusSexNatrInd,
               a.BusNonFmrSBAEmpInd,
               a.BusNonLegBrnchEmpInd,
               a.BusNonFedEmpInd,
               a.BusNonGS13EmpInd,
               a.BusNonSBACEmpInd,
               a.BusPrimCntctNm,
               --   BusPrimCntctEmail,
               a.BusFedAgncyLoanInd,
               a.BusOutDbtInd
          FROM LoanApp.LoanBusTbl a, loan.Bustbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.taxid = b.taxid(+);


    --Copy to LoanApp.LoanPerTbl
    INSERT INTO LoanApp.LoanPerTbl (LoanAppNmb,
                                    TaxId,
                                    PerTAXIDCertInd,
                                    PerFirstNm,
                                    PerFirstSrchNm,
                                    PerLastNm,
                                    PerLastSrchNm,
                                    PerInitialNm,
                                    PerSfxNm,
                                    PerTitlTxt,
                                    PerBrthDt,
                                    PerBrthCtyNm,
                                    PerBrthStCd,
                                    PerBrthCntCd,
                                    PerBrthCntNm,
                                    PerUSCitznInd,
                                    PerAlienRgstrtnNmb,
                                    PerPndngLwsuitInd,
                                    PerAffilEmpFedInd,
                                    PerIOBInd,
                                    PerIndctPrleProbatnInd,
                                    PerCrmnlOffnsInd,
                                    PerCnvctInd,
                                    PerBnkrptcyInd,
                                    PerFngrprntWaivDt,
                                    PerPrimPhnNmb,
                                    PerPhyAddrStr1Nm,
                                    PerPhyAddrStr2Nm,
                                    PerPhyAddrCtyNm,
                                    PerPhyAddrStCd,
                                    PerPhyAddrStNm,
                                    PerPhyAddrCntCd,
                                    PerPhyAddrZipCd,
                                    PerPhyAddrZip4Cd,
                                    PerPhyAddrPostCd,
                                    PerMailAddrStr1Nm,
                                    PerMailAddrStr2Nm,
                                    PerMailAddrCtyNm,
                                    PerMailAddrStCd,
                                    PerMailAddrStNm,
                                    PerMailAddrCntCd,
                                    PerMailAddrZipCd,
                                    PerMailAddrZip4Cd,
                                    PerMailAddrPostCd,
                                    PerCreatUserId,
                                    PerCreatDt,
                                    PerExtrnlCrdtScorInd,
                                    IMCrdtScorSourcCd,
                                    PerExtrnlCrdtScorNmb,
                                    PerExtrnlCrdtScorDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    VETCERTIND,
                                    PerFedAgncySDPIEInd,
                                    PerCSP60DayDelnqInd,
                                    PerLglActnInd,
                                    PerCitznShpCntNm,
                                    PerAltPhnNmb,
                                    PerPrimEmailAdr,
                                    PerAltEmailAdr,
                                    PerFedAgncyLoanInd,
                                    PerOthrCmntTxt)
        SELECT v_NEWLoanAppNmb,
               a.TaxId,
               NVL (b.PerTAXIDCertInd, a.PerTAXIDCertInd),
               NVL (b.PerFirstNm, a.PerFirstNm),
               NVL (b.PerFirstSrchNm, a.PerFirstSrchNm),
               NVL (b.PerLastNm, a.PerLastNm),
               NVL (b.PerLastSrchNm, a.PerLastSrchNm),
               NVL (b.PerInitialNm, a.PerInitialNm),
               NVL (b.PerSfxNm, a.PerSfxNm),
               a.PerTitlTxt,
               NVL (b.PerBrthDt, a.PerBrthDt),
               NVL (b.PerBrthCtyNm, a.PerBrthCtyNm),
               NVL (b.PerBrthStCd, a.PerBrthStCd),
               NVL (b.PerBrthCntCd, a.PerBrthCntCd),
               NVL (b.PerBrthCntNm, b.PerBrthCntNm),
               a.PerUSCitznInd,
               a.PerAlienRgstrtnNmb,
               a.PerPndngLwsuitInd,
               a.PerAffilEmpFedInd,
               a.PerIOBInd,
               a.PerIndctPrleProbatnInd,
               a.PerCrmnlOffnsInd,
               a.PerCnvctInd,
               a.PerBnkrptcyInd,
               a.PerFngrprntWaivDt,
               a.PerPrimPhnNmb,
               a.PerPhyAddrStr1Nm,
               a.PerPhyAddrStr2Nm,
               a.PerPhyAddrCtyNm,
               a.PerPhyAddrStCd,
               a.PerPhyAddrStNm,
               a.PerPhyAddrCntCd,
               a.PerPhyAddrZipCd,
               a.PerPhyAddrZip4Cd,
               a.PerPhyAddrPostCd,
               a.PerMailAddrStr1Nm,
               a.PerMailAddrStr2Nm,
               a.PerMailAddrCtyNm,
               a.PerMailAddrStCd,
               a.PerMailAddrStNm,
               a.PerMailAddrCntCd,
               a.PerMailAddrZipCd,
               a.PerMailAddrZip4Cd,
               a.PerMailAddrPostCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               a.PerExtrnlCrdtScorInd,
               a.IMCrdtScorSourcCd,
               a.PerExtrnlCrdtScorNmb,
               a.PerExtrnlCrdtScorDt,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (b.VETCERTIND, a.VETCERTIND),
               a.PerFedAgncySDPIEInd,
               a.PerCSP60DayDelnqInd,
               a.PerLglActnInd,
               a.PerCitznShpCntNm,
               a.PerAltPhnNmb,
               a.PerPrimEmailAdr,
               a.PerAltEmailAdr,
               a.PerFedAgncyLoanInd,
               a.PerOthrCmntTxt
          FROM loanapp.loanpertbl a, loan.pertbl b
         WHERE a.taxid = b.taxid(+) AND a.loanappnmb = p_loanappnmb;


    BEGIN
        --Copy to LoanApp.LoanIndbtnesTbl
        INSERT INTO LoanApp.LoanIndbtnesTbl (LoanAppNmb,
                                             TaxId,
                                             LoanIndbtnesSeqNmb,
                                             LoanIndbtnesPayblToNm,
                                             LoanIndbtnesPurpsTxt,
                                             LoanIndbtnesOrglDt,
                                             LoanIndbtnesCurBalAmt,
                                             LoanIndbtnesIntPct,
                                             LoanIndbtnesMatDt,
                                             LoanIndbtnesPymtAmt,
                                             LoanIndbtnesPymtFreqCd,
                                             LoanIndbtnesCollatDescTxt,
                                             LoanIndbtnesPrevFinanStatCd,
                                             LoanIndbtnesCreatUserId,
                                             LoanIndbtnesCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanIndbtnesSeqNmb,
                   LoanIndbtnesPayblToNm,
                   LoanIndbtnesPurpsTxt,
                   LoanIndbtnesOrglDt,
                   LoanIndbtnesCurBalAmt,
                   LoanIndbtnesIntPct,
                   LoanIndbtnesMatDt,
                   LoanIndbtnesPymtAmt,
                   LoanIndbtnesPymtFreqCd,
                   LoanIndbtnesCollatDescTxt,
                   LoanIndbtnesPrevFinanStatCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanIndbtnesTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPerFinanTbl
        INSERT INTO LoanApp.LoanPerFinanTbl (LoanAppNmb,
                                             TaxId,
                                             PerLqdAssetAmt,
                                             PerBusOwnrshpAmt,
                                             PerREAmt,
                                             PerOthAssetAmt,
                                             PerTotAssetAmt,
                                             PerRELiabAmt,
                                             PerCCDbtAmt,
                                             PerInstlDbtAmt,
                                             PerOthLiabAmt,
                                             PerTotLiabAmt,
                                             PerNetWrthAmt,
                                             PerAnnSalaryAmt,
                                             PerOthAnnIncAmt,
                                             PerSourcOfOthIncTxt,
                                             PerResOwnRentOthInd,
                                             PerMoHsngAmt,
                                             PerCreatUserId,
                                             PerCreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   PerLqdAssetAmt,
                   PerBusOwnrshpAmt,
                   PerREAmt,
                   PerOthAssetAmt,
                   PerTotAssetAmt,
                   PerRELiabAmt,
                   PerCCDbtAmt,
                   PerInstlDbtAmt,
                   PerOthLiabAmt,
                   PerTotLiabAmt,
                   PerNetWrthAmt,
                   PerAnnSalaryAmt,
                   PerOthAnnIncAmt,
                   PerSourcOfOthIncTxt,
                   PerResOwnRentOthInd,
                   PerMoHsngAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPerFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanFedEmpTbl
        INSERT INTO LOANAPP.LoanFedEmpTbl (LoanAppNmb,
                                           TaxId,
                                           LoanFedEmpSeqNmb,
                                           FedEmpFirstNm,
                                           FedEmpLastNm,
                                           FedEmpInitialNm,
                                           FedEmpSfxNm,
                                           FedEmpAddrStr1Nm,
                                           FedEmpAddrStr2Nm,
                                           FedEmpAddrCtyNm,
                                           FedEmpAddrStCd,
                                           FedEmpAddrZipCd,
                                           FedEmpAddrZip4Cd,
                                           FedEmpAgncyOfc,
                                           FedEmpCreatUserId,
                                           FedEmpCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   TaxId,
                   LoanFedEmpSeqNmb,
                   FedEmpFirstNm,
                   FedEmpLastNm,
                   FedEmpInitialNm,
                   FedEmpSfxNm,
                   FedEmpAddrStr1Nm,
                   FedEmpAddrStr2Nm,
                   FedEmpAddrCtyNm,
                   FedEmpAddrStCd,
                   FedEmpAddrZipCd,
                   FedEmpAddrZip4Cd,
                   FedEmpAgncyOfc,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanFedEmpTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.Pertbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'P'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBorrTbl
        INSERT INTO LoanApp.LoanBorrTbl (LoanAppNmb,
                                         BorrSeqNmb,
                                         TaxId,
                                         LoanBusPrimBorrInd,
                                         BorrBusPerInd,
                                         IMEPCOperCd,
                                         BorrCreatUserId,
                                         BorrCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LoanCntlIntInd,
                                         LoanCntlIntTyp,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdind,
                                         OthInsurDescTxt,
                                         PymtLessCrdtInd,
                                         RecrdStmtTypCd,
                                         RecrdStmtYrEndDayNmb,
                                         WorkrsCompInsRqrdInd)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   a.TaxId,
                   LoanBusPrimBorrInd,
                   BorrBusPerInd,
                   IMEPCOperCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.Ethniccd),
                   NVL (b.GndrCd, a.Gndrcd),
                   LoanCntlIntInd,
                   LoanCntlIntTyp,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdind,
                   OthInsurDescTxt,
                   PymtLessCrdtInd,
                   RecrdStmtTypCd,
                   RecrdStmtYrEndDayNmb,
                   WorkrsCompInsRqrdInd
              FROM LoanApp.LoanBorrTbl a, Loan.bustbl b
             WHERE     A.taxid = b.taxid(+)
                   AND borrbusperind = 'B'
                   AND LoanAppNmb = p_LoanAppNmb;
    END;



    BEGIN
        --Copy to LoanApp.LoanPrinTbl */
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.pertbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'P'
                   AND a.taxid = b.Taxid(+);
    END;

    BEGIN
        INSERT INTO LoanApp.LoanPrinTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         TaxId,
                                         PrinBusPerInd,
                                         PrinCreatUserId,
                                         PrinCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         EthnicCd,
                                         GndrCd,
                                         LACGntyCondTypCd,
                                         VetCd,
                                         LoanDisblInsurRqrdInd,
                                         LoanLifeInsurRqrdInd,
                                         LoanPrinNonCAEvdncInd,
                                         GntyLmtAmt,
                                         GntySubCondTypCd,
                                         LoanPrinInsurAmt,
                                         LoanInsurDisblDescTxt,
                                         LoanPrinInsurNm,
                                         LoanComptitrNm,
                                         LoanPrinPrimBusExprnceYrNmb)
            SELECT v_NEWLoanAppNmb,
                   PrinSeqNmb,
                   a.TaxId,
                   PrinBusPerInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (b.EthnicCd, a.ETHNICCD),
                   NVL (b.GndrCd, a.Gndrcd),
                   LACGntyCondTypCd,
                   NVL (b.VetCd, a.Vetcd),
                   LoanDisblInsurRqrdInd,
                   LoanLifeInsurRqrdInd,
                   LoanPrinNonCAEvdncInd,
                   GntyLmtAmt,
                   GntySubCondTypCd,
                   LoanPrinInsurAmt,
                   LoanInsurDisblDescTxt,
                   LoanPrinInsurNm,
                   LoanComptitrNm,
                   LoanPrinPrimBusExprnceYrNmb
              FROM LoanApp.LoanPrinTbl a, Loan.bustbl b
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND prinbusperind = 'B'
                   AND a.taxid = b.Taxid(+);
    END;



    BEGIN
        --Copy to LoanApp.LoanBusPrinTbl
        INSERT INTO LoanApp.LoanBusPrinTbl (LoanAppNmb,
                                            BorrSeqNmb,
                                            PrinSeqNmb,
                                            LoanBusPrinPctOwnrshpBus,
                                            LoanPrinGntyInd,
                                            LoanBusPrinCreatUserId,
                                            LoanBusPrinCreatDt,
                                            LastUpdtUserId,
                                            LastUpdtDt,
                                            LoanCntlIntInd,
                                            LoanCntlIntTyp)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   PrinSeqNmb,
                   LoanBusPrinPctOwnrshpBus,
                   LoanPrinGntyInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanCntlIntInd,
                   LoanCntlIntTyp
              FROM LoanApp.LoanBusPrinTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.perracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'P'
               AND a.taxid = b.taxid;

    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               b.Racecd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinTbl a, loan.busracetbl b
         WHERE     a.LoanAppNmb = p_loanappnmb
               AND a.PRINBUSPERIND = 'B'
               AND a.taxid = b.taxid;

    --Copy to LoanApp.LoanPrinRaceTbl
    INSERT INTO LoanApp.LoanPrinRaceTbl (LoanAppNmb,
                                         PrinSeqNmb,
                                         RaceCd,
                                         PrinRaceCreatUserId,
                                         PrinRaceCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
        SELECT v_NEWLoanAppNmb,
               PrinSeqNmb,
               RaceCd,
               NVL (p_CreatUserId, USER),
               SYSDATE,
               NVL (p_CreatUserId, USER),
               SYSDATE
          FROM LoanApp.LoanPrinRaceTbl a
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND NOT EXISTS
                       (SELECT 1
                          FROM loanapp.LoanPrinRaceTbl z
                         WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                               AND z.PRINSEQNMB = a.PrinSeqNmb);


    BEGIN
        --Copy to LoanApp.LoanBorrRaceTbl
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrTbl a, loan.perracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'P'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   b.RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanborrTbl a, loan.busracetbl b
             WHERE     a.LoanAppNmb = p_loanappnmb
                   AND a.BorrBUSPERIND = 'B'
                   AND a.taxid = b.taxid;
    END;

    BEGIN
        INSERT INTO LoanApp.LoanBorrRaceTbl (LoanAppNmb,
                                             BorrSeqNmb,
                                             RaceCd,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   RaceCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBorrRaceTbl a
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND NOT EXISTS
                           (SELECT 1
                              FROM loanapp.LoanBorrRaceTbl z
                             WHERE     z.LoanAppNmb = v_NEWLoanAppNmb
                                   AND z.BorrSEQNMB = a.BorrSeqNmb);
    END;

    BEGIN
        --Copy to LoanApp.LoanPrevFinanTbl
        INSERT INTO LoanApp.LoanPrevFinanTbl (LoanAppNmb,
                                              BorrSeqNmb,
                                              LoanPrevFinanSeqNmb,
                                              LoanPrevFinanStatCd,
                                              LoanPrevFinanAgencyNm,
                                              LoanPrevFinanLoanNmb,
                                              LoanPrevFinanAppvDt,
                                              LoanPrevFinanTotAmt,
                                              LoanPrevFinanBalAmt,
                                              LoanPrevFinanCreatUserId,
                                              LoanPrevFinanCreatDt,
                                              LastUpdtUserId,
                                              LastUpdtDt,
                                              LoanPrevFinanDelnqCmntTxt)
            SELECT v_NEWLoanAppNmb,
                   BorrSeqNmb,
                   LoanPrevFinanSeqNmb,
                   LoanPrevFinanStatCd,
                   LoanPrevFinanAgencyNm,
                   LoanPrevFinanLoanNmb,
                   LoanPrevFinanAppvDt,
                   LoanPrevFinanTotAmt,
                   LoanPrevFinanBalAmt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanPrevFinanDelnqCmntTxt
              FROM LoanApp.LoanPrevFinanTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanGuarTbl
        INSERT INTO LoanApp.LoanGuarTbl (LoanAppNmb,
                                         GuarSeqNmb,
                                         TaxId,
                                         GuarBusPerInd,
                                         LoanGuarOperCoInd,
                                         GuarCreatUserId,
                                         GuarCreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt,
                                         LACGntyCondTypCd,
                                         GntySubCondTypCd,
                                         GntyLmtAmt,
                                         GntyLmtPct,
                                         LoanCollatSeqNmb,
                                         GntyLmtYrNmb,
                                         LiabInsurRqrdInd,
                                         ProdLiabInsurRqrdInd,
                                         LiqLiabInsurRqrdInd,
                                         MalPrctsInsurRqrdInd,
                                         OthInsurRqrdInd,
                                         WorkrsCompInsRqrdInd,
                                         OthInsurDescTxt)
            SELECT v_NEWLoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LACGntyCondTypCd,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LiabInsurRqrdInd,
                   ProdLiabInsurRqrdInd,
                   LiqLiabInsurRqrdInd,
                   MalPrctsInsurRqrdInd,
                   OthInsurRqrdInd,
                   WorkrsCompInsRqrdInd,
                   OthInsurDescTxt
              FROM LoanApp.LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanLmtGntyCollatTbl
        INSERT INTO LoanApp.LoanLmtGntyCollatTbl (LOANAPPNMB,
                                                  TAXID,
                                                  BUSPERIND,
                                                  LOANCOLLATSEQNMB,
                                                  CREATUSERID,
                                                  CREATDT,
                                                  LASTUPDTUSERID,
                                                  LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   TAXID,
                   BUSPERIND,
                   LOANCOLLATSEQNMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanLmtGntyCollatTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanIntDtlTbl
        INSERT INTO LoanApp.LoanIntDtlTbl (LOANAPPNMB,
                                           LOANINTDTLSEQNMB,
                                           LOANINTLOANPARTPCT,
                                           LOANINTLOANPARTMONMB,
                                           LOANINTFIXVARIND,
                                           LOANINTRT,
                                           IMRTTYPCD,
                                           LOANINTBASERT,
                                           LOANINTSPRDOVRPCT,
                                           LOANINTADJPRDCD,
                                           LOANINTADJPRDMONMB,
                                           CREATUSERID,
                                           CREATDT,
                                           LASTUPDTUSERID,
                                           LASTUPDTDT,
                                           LOANINTADJPRDEFFDT,
                                           LoanIntDtlGuarInd)
            SELECT v_NEWLoanAppNmb,
                   LOANINTDTLSEQNMB,
                   LOANINTLOANPARTPCT,
                   LOANINTLOANPARTMONMB,
                   LOANINTFIXVARIND,
                   LOANINTRT,
                   IMRTTYPCD,
                   LOANINTBASERT,
                   LOANINTSPRDOVRPCT,
                   LOANINTADJPRDCD,
                   LOANINTADJPRDMONMB,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANINTADJPRDEFFDT,
                   LoanIntDtlGuarInd
              FROM LoanApp.LoanIntDtlTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanExprtCntryTbl
        INSERT INTO LoanApp.LoanExprtCntryTbl (LOANAPPNMB,
                                               LOANEXPRTCNTRYCD,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT,
                                               LOANEXPRTCNTRYSEQNMB)
            SELECT v_NEWLoanAppNmb,
                   LOANEXPRTCNTRYCD,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LOANEXPRTCNTRYSEQNMB
              FROM LoanApp.LoanExprtCntryTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanBSTbl
        INSERT INTO LoanApp.LoanBSTbl (LoanAppNmb,
                                       LoanBSDt,
                                       LoanBSCashEqvlntAmt,
                                       LoanBSNetTrdRecvAmt,
                                       LoanBSTotInvtryAmt,
                                       LoanBSOthCurAssetAmt,
                                       LoanBSTotCurAssetAmt,
                                       LoanBSTotFixAssetAmt,
                                       LoanBSTotOthAssetAmt,
                                       LoanBSTotAssetAmt,
                                       LoanBSAcctsPayblAmt,
                                       LoanBSCurLTDAmt,
                                       LoanBSOthCurLiabAmt,
                                       LoanBSTotCurLiabAmt,
                                       LoanBSLTDAmt,
                                       LoanBSOthLTLiabAmt,
                                       LoanBSStbyDbt,
                                       LoanBSTotLiab,
                                       LoanBSBusNetWrth,
                                       LoanBSTngblNetWrth,
                                       LoanBSActlPrfrmaInd,
                                       LoanFinanclStmtSourcCd,
                                       LoanBSCreatUserId,
                                       LoanBSCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanBSDt,
                   LoanBSCashEqvlntAmt,
                   LoanBSNetTrdRecvAmt,
                   LoanBSTotInvtryAmt,
                   LoanBSOthCurAssetAmt,
                   LoanBSTotCurAssetAmt,
                   LoanBSTotFixAssetAmt,
                   LoanBSTotOthAssetAmt,
                   LoanBSTotAssetAmt,
                   LoanBSAcctsPayblAmt,
                   LoanBSCurLTDAmt,
                   LoanBSOthCurLiabAmt,
                   LoanBSTotCurLiabAmt,
                   LoanBSLTDAmt,
                   LoanBSOthLTLiabAmt,
                   LoanBSStbyDbt,
                   LoanBSTotLiab,
                   LoanBSBusNetWrth,
                   LoanBSTngblNetWrth,
                   LoanBSActlPrfrmaInd,
                   LoanFinanclStmtSourcCd,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanBSTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanISTbl
        INSERT INTO LoanApp.LoanISTbl (LoanAppNmb,
                                       LoanISSeqNmb,
                                       LoanISNetSalesRevnuAmt,
                                       LoanISCostSalesAmt,
                                       LoanISGrsProftAmt,
                                       LoanISOwnrSalaryAmt,
                                       LoanISDprctAmortAmt,
                                       LoanISNetIncAmt,
                                       LoanISOperProftAmt,
                                       LoanISAnnIntExpnAmt,
                                       LoanISNetIncBefTaxWthdrlAmt,
                                       LoanISIncTaxAmt,
                                       LoanISCshflwAmt,
                                       LoanFinanclStmtSourcCd,
                                       LoanISBegnDt,
                                       LoanISEndDt,
                                       LoanISCreatUserId,
                                       LoanISCreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanISSeqNmb,
                   LoanISNetSalesRevnuAmt,
                   LoanISCostSalesAmt,
                   LoanISGrsProftAmt,
                   LoanISOwnrSalaryAmt,
                   LoanISDprctAmortAmt,
                   LoanISNetIncAmt,
                   LoanISOperProftAmt,
                   LoanISAnnIntExpnAmt,
                   LoanISNetIncBefTaxWthdrlAmt,
                   LoanISIncTaxAmt,
                   LoanISCshflwAmt,
                   LoanFinanclStmtSourcCd,
                   LoanISBegnDt,
                   LoanISEndDt,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanISTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanAppPrtTbl
        INSERT INTO LOANAPP.LoanAppPrtTbl (LoanAppNmb,
                                           LocId,
                                           LoanAppFIRSNmb,
                                           PrtId,
                                           LoanAppPrtAppNmb,
                                           LoanAppPrtLoanNmb,
                                           LoanAppPrtNm,
                                           LoanAppPrtStr1,
                                           LoanAppPrtStr2,
                                           LoanAppPrtCtyNm,
                                           LoanAppPrtStCd,
                                           LoanAppPrtZipCd,
                                           LoanAppPrtZip4Cd,
                                           LoanAppCntctLastNm,
                                           LoanAppCntctFirstNm,
                                           LoanAppCntctInitialNm,
                                           LoanAppCntctSfxNm,
                                           LoanAppCntctTitlTxt,
                                           LoanAppCntctPhn,
                                           LoanAppCntctFax,
                                           LoanAppCntctemail,
                                           LoanPckgSourcTypCd,
                                           LoanAppPckgSourcNm,
                                           LoanAppPckgSourcStr1Nm,
                                           LoanAppPckgSourcStr2Nm,
                                           LoanAppPckgSourcCtyNm,
                                           LoanAppPckgSourcStCd,
                                           LoanAppPckgSourcZipCd,
                                           LoanAppPckgSourcZip4Cd,
                                           ACHRtngNmb,
                                           ACHAcctNmb,
                                           ACHAcctTypCd,
                                           ACHTinNmb,
                                           LoanAppPrtTaxId,
                                           LoanAppLSPHQLocId,
                                           LoanAppPrtCreatUserId,
                                           LoanAppPrtCreatDt,
                                           LastUpdtUserId,
                                           LastUpdtDt,
                                           LoanAgntInvlvdInd,
                                           --    LoanAppLspLastNm,
                                           --     LoanAppLspFirstNm,
                                           --     LoanAppLspInitialNm,
                                           LoanAppLspSfxNm,
                                           --    LoanAppLspTitlTxt,
                                           --    LoanAppLspPhn,
                                           LoanAppLspFax,
                                           --    LoanAppLspEMail,
                                           LoanPrtCntctCellPhn,
                                           LoanPrtAltCntctFirstNm,
                                           LoanPrtAltCntctLastNm,
                                           LoanPrtAltCntctInitialNm,
                                           LoanPrtAltCntctTitlTxt,
                                           LoanPrtAltCntctPrimPhn,
                                           LoanPrtAltCntctCellPhn,
                                           LoanPrtAltCntctemail,
                                           LendrAltCntctTypCd)
            SELECT v_NEWLoanAppNmb,
                   LocId,
                   LoanAppFIRSNmb,
                   PrtId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   LoanAppPrtNm,
                   LoanAppPrtStr1,
                   LoanAppPrtStr2,
                   LoanAppPrtCtyNm,
                   LoanAppPrtStCd,
                   LoanAppPrtZipCd,
                   LoanAppPrtZip4Cd,
                   LoanAppCntctLastNm,
                   LoanAppCntctFirstNm,
                   LoanAppCntctInitialNm,
                   LoanAppCntctSfxNm,
                   LoanAppCntctTitlTxt,
                   LoanAppCntctPhn,
                   LoanAppCntctFax,
                   LoanAppCntctemail,
                   LoanPckgSourcTypCd,
                   LoanAppPckgSourcNm,
                   LoanAppPckgSourcStr1Nm,
                   LoanAppPckgSourcStr2Nm,
                   LoanAppPckgSourcCtyNm,
                   LoanAppPckgSourcStCd,
                   LoanAppPckgSourcZipCd,
                   LoanAppPckgSourcZip4Cd,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppPrtTaxId,
                   LoanAppLSPHQLocId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   LoanAgntInvlvdInd,
                   --   LoanAppLspLastNm,
                   --   LoanAppLspFirstNm,
                   --   LoanAppLspInitialNm,
                   LoanAppLspSfxNm,
                   --   LoanAppLspTitlTxt,
                   --    LoanAppLspPhn,
                   LoanAppLspFax,
                   --   LoanAppLspEMail,
                   LoanPrtCntctCellPhn,
                   LoanPrtAltCntctFirstNm,
                   LoanPrtAltCntctLastNm,
                   LoanPrtAltCntctInitialNm,
                   LoanPrtAltCntctTitlTxt,
                   LoanPrtAltCntctPrimPhn,
                   LoanPrtAltCntctCellPhn,
                   LoanPrtAltCntctemail,
                   LendrAltCntctTypCd
              FROM LoanApp.LoanAppPrtTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        --Copy to LoanApp.LoanPymntTbl
        INSERT INTO LoanApp.LoanPymntTbl (LOANAPPNMB,
                                          PYMNTDAYNMB,
                                          PYMNTBEGNMONMB,
                                          ESCROWACCTRQRDIND,
                                          NETEARNCLAUSEIND,
                                          ARMMARTYPIND,
                                          ARMMARCEILRT,
                                          ARMMARFLOORRT,
                                          STINTRTREDUCTNIND,
                                          LATECHGIND,
                                          LATECHGAFTDAYNMB,
                                          LATECHGFEEPCT,
                                          PYMNTINTONLYBEGNMONMB,
                                          PYMNTINTONLYFREQCD,
                                          PYMNTINTONLYDAYNMB,
                                          NetEarnPymntPct,
                                          NetEarnPymntOverAmt,
                                          StIntRtReductnPrgmNm,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   PYMNTDAYNMB,
                   PYMNTBEGNMONMB,
                   ESCROWACCTRQRDIND,
                   NETEARNCLAUSEIND,
                   ARMMARTYPIND,
                   ARMMARCEILRT,
                   ARMMARFLOORRT,
                   STINTRTREDUCTNIND,
                   LATECHGIND,
                   LATECHGAFTDAYNMB,
                   LATECHGFEEPCT,
                   PYMNTINTONLYBEGNMONMB,
                   PYMNTINTONLYFREQCD,
                   PYMNTINTONLYDAYNMB,
                   NetEarnPymntPct,
                   NetEarnPymntOverAmt,
                   StIntRtReductnPrgmNm,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanPymntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO Loanapp.LoanAgntTbl (LOANAPPNMB,
                                         LoanAgntSeqNmb,
                                         LOANAGNTBUSPERIND,
                                         LOANAGNTNM,
                                         LOANAGNTCNTCTFIRSTNM,
                                         LOANAGNTCNTCTMIDNM,
                                         LOANAGNTCNTCTLASTNM,
                                         LOANAGNTCNTCTSFXNM,
                                         LOANAGNTADDRSTR1NM,
                                         LOANAGNTADDRSTR2NM,
                                         LOANAGNTADDRCTYNM,
                                         LOANAGNTADDRSTCD,
                                         LOANAGNTADDRSTNM,
                                         LOANAGNTADDRZIPCD,
                                         LOANAGNTADDRZIP4CD,
                                         LOANAGNTADDRPOSTCD,
                                         LOANAGNTADDRCNTCD,
                                         LOANAGNTTYPCD,
                                         LOANAGNTDOCUPLDIND,
                                         LOANCDCTPLFEEIND,
                                         LOANCDCTPLFEEAMT,
                                         LOANAGNTID,
                                         CreatUserId,
                                         CreatDt,
                                         LastUpdtUserId,
                                         LastUpdtDt)
            SELECT v_NEWLoanAppNmb,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM,
                   LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LoanAgntId,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LoanApp.LoanAgntTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    BEGIN
        INSERT INTO LOANAPP.LOANAGNTFEEDTLTBL (LOANAPPNMB,
                                               LoanAgntSeqNmb,
                                               LOANAGNTSERVTYPCD,
                                               LOANAGNTSERVOTHTYPTXT,
                                               LOANAGNTAPPCNTPAIDAMT,
                                               LOANAGNTSBALENDRPAIDAMT,
                                               CREATUSERID,
                                               CREATDT,
                                               LASTUPDTUSERID,
                                               LASTUPDTDT)
            SELECT v_NEWLoanAppNmb,
                   LoanAGNTSeqNmb,
                   LOANAGNTSERVTYPCD,
                   LOANAGNTSERVOTHTYPTXT,
                   LOANAGNTAPPCNTPAIDAMT,
                   LOANAGNTSBALENDRPAIDAMT,
                   NVL (p_CreatUserId, USER),
                   SYSDATE,
                   NVL (p_CreatUserId, USER),
                   SYSDATE
              FROM LOANAPP.LOANAGNTFEEDTLTBL
             WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    p_NEWLoanAppNmb := v_NEWLoanAppNmb;
END LOANCOPYCSP;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYPROCDTOTAMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOAN.LOANGNTYPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOAN.LOANGNTYSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANGNTYPROCDTOTAMTSELTSP;

    /* Select from LOAN.LOANGNTYPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDTOTAMTSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO UPDLOANROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSUMTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDSUMTSP (
    p_LOANAPPNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
	p_PROCDTYPCD 				CHAR)
AS
        quan NUMBER(10);
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDSUMTSP;

        BEGIN
            select sum(LOANPROCDAMT) into quan from LOAN.LOANGNTYSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD and LOANAPPNMB=p_LOANAPPNmb;
  --DBMS_OUTPUT.PUT_LINE('quantity value =' || quan);
  update LOAN.LOANGNTYPROCDTBL set LOANPROCDAMT = quan where PROCDTYPCD = p_PROCDTYPCD and LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LOANAPPNmb;
END;
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDSUMTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDSUMTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSUMTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSUMTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSUMTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSUMTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSUMTSP TO UPDLOANROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
     p_PROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDSELTSP;

    /* Select from LOANSUBPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	 
                LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM LOAN.LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANSUBPROCDID, LOANAPPNMB,/* LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD AND  PROCDTYPCD=p_PROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
                LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
                to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
                LOANPROCDDESC, LENDER, CREATUSERID, 
                CREATDT, LASTUPDTUSERID, LASTUPDTDT
                FROM    LOANGNTYSUBPROCDTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDSELTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDSELTSP TO UPDLOANROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select nvl(LOANPROCDSEQNMB,0) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
	cursor procdtbl_max is select max(nvl(LOANPROCDSEQNMB,0)) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
	v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    v_LASTUPDTUSERID    varchar2(50):=NULL;
    max_LOANPROCDSEQNMB number :=0;
    
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB=0  then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if  v_LOANPROCDSEQNMB=0 then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			v_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDINSTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDINSTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDDELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDDELTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanSubProcdId 	NUMBER:=0
)
AS
v_LOANAPPNMB NUMBER :=0;
v_LOANPROCDTYPCD CHAR(3) := NULL;
v_PROCDTYPCD CHAR(1) := NULL;

BEGIN
	SAVEPOINT LOANGNTYSUBPROCDDELTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
        select LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD into v_LOANAPPNMB, v_LOANPROCDTYPCD, v_PROCDTYPCD from LOAN.LOANGNTYSUBPROCDTBL where LOANSUBPROCDID = p_LOANSUBPROCDID;
		DELETE  LOANGNTYSUBPROCDTBL
		WHERE LOANSUBPROCDID = p_LOANSUBPROCDID;
		
        LOANGNTYSUBPROCDSUMTSP(v_LOANAPPNMB,v_LOANPROCDTYPCD,v_PROCDTYPCD);
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANGNTYSUBPROCDDELTSP;
RAISE;
END;
END LOANGNTYSUBPROCDDELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELTSP TO UPDLOANROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANGNTYSUBPROCDDELALLTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDDELALLTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanAppNmb 	NUMBER:=0
)
AS

BEGIN
	SAVEPOINT LOANGNTYSUBPROCDDELALLTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
		DELETE  LoanGntySubProcdTbl
		WHERE LoanAppNmb = p_LoanAppNmb
		and LOANPROCDTYPCD not in (select LOANPROCDTYPCD from LOAN.LoanGntyProcdTbl where LOANAPPNMB=p_LoanAppNmb) and PROCDTYPCD='E';
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANGNTYSUBPROCDDELALLTSP;
RAISE;
END;
END LOANGNTYSUBPROCDDELALLTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELALLTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELALLTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELALLTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELALLTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDDELALLTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\EXTRACTSELCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.EXTRACTSELCSP (p_LoanAppNmb   IN     NUMBER := NULL,
                                                p_LoanNmb      IN     VARCHAR2 := NULL,
                                                p_RetVal          OUT NUMBER,
                                                p_SelCur1         OUT SYS_REFCURSOR,
                                                p_SelCur2         OUT SYS_REFCURSOR,
                                                p_SelCur3         OUT SYS_REFCURSOR,
                                                p_SelCur4         OUT SYS_REFCURSOR,
                                                p_SelCur5         OUT SYS_REFCURSOR,
                                                p_SelCur6         OUT SYS_REFCURSOR,
                                                p_SelCur7         OUT SYS_REFCURSOR,
                                                p_SelCur8         OUT SYS_REFCURSOR,
                                                p_SelCur9         OUT SYS_REFCURSOR,
                                                p_SelCur10        OUT SYS_REFCURSOR,
                                                p_SelCur11        OUT SYS_REFCURSOR,
                                                p_SelCur12        OUT SYS_REFCURSOR,
                                                p_SelCur13        OUT SYS_REFCURSOR,
                                                p_SelCur14        OUT SYS_REFCURSOR,
                                                p_SelCur15        OUT SYS_REFCURSOR,
                                                p_SelCur16        OUT SYS_REFCURSOR,
                                                p_SelCur17        OUT SYS_REFCURSOR,
                                                p_SelCur18        OUT SYS_REFCURSOR,
                                                p_SelCur19        OUT SYS_REFCURSOR,
                                                p_SelCur20        OUT SYS_REFCURSOR,
                                                p_SelCur21        OUT SYS_REFCURSOR,
                                                p_SelCur22        OUT SYS_REFCURSOR,
                                                p_SelCur23        OUT SYS_REFCURSOR,
                                                p_SelCur24        OUT SYS_REFCURSOR,
                                                p_SelCur25        OUT SYS_REFCURSOR,
                                                p_SelCur26        OUT SYS_REFCURSOR,
                                                p_SelCur27        OUT SYS_REFCURSOR,
                                                p_SelCur28        OUT SYS_REFCURSOR) AS
    /*SB -- This procedure created for Extract XML */
    /*SB -- Added p_selcur18 on 02/25/2015 */
    /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met, Replaced the initialization block to check for correct loannmb and loanappnmb*/
    /*NK -- 01/12/2017 Added  p_SELCUR20 and p_SELCUR21*/
    /*JP -- 01/30/2017 CAFSOPER-1341 Changed p_SelCur19�s procedure from LOANINFOEXTRACTCSP to LOANINFOEXTRACTSELCSP. (Added "SEL".)
    RY -- 03/17/2017 --OPSMDEV --1401-- Added P_SELCUR22
    BR -- 05/26/2017 --OPSMDEV-1434 -- Added P_SelCur23 Loan.LoanExprtCntrySelTSP
    SS--07/19/2018--OPSMDEV--1861-- Added  P_SELCUR24 and  P_SELCUR25 for Agents
    JP - CAfsoper-2988 08/08/2019 - Modified call to LoanGntyGuarSelTSP
    RY:: 12/16/2019- OPSMDEV-2347 :: Added LOAN.DEFRMNTHISTRYSELTSP to the list
    -- JP 8/21/2020 CARESACT 621 added call to LOAN.LoanHldStatSelTSP
    */
    --
    v_LoanNmb      CHAR (10);
    v_LoanAppNmb   NUMBER;
    v_RetVal       NUMBER;
BEGIN
    -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
    BEGIN
        IF (   p_LoanAppNmb IS NOT NULL
            OR p_LoanNmb IS NOT NULL) THEN
            SELECT LoanAppNmb, LoanNmb
            INTO v_LoanAppNmb, v_LoanNmb
            FROM loan.LoanGntyTbl
            WHERE     LoanAppNmb = NVL (p_LoanAppNmb, LoanAppNmb)
                  AND LoanNmb = NVL (p_LoanNmb, LoanNmb);
        ELSE
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
    END;



    IF v_LoanAppNmb IS NULL THEN
        BEGIN
            p_RetVal := 0;

            OPEN p_SelCur1 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur2 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur3 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur4 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur5 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur6 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur7 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur8 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur9 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur10 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur11 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur12 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur13 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur14 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur15 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur16 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur17 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur18 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur19 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur20 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur21 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur22 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur23 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur24 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur25 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;
            
            OPEN p_SelCur28 FOR
                SELECT v_RetVal
                  FROM DUAL
                 WHERE 1 = 0;
        END;
    ELSE
        BEGIN
            LoanGntySelTSP (
                38,
                v_LoanAppNmb,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                P_RetVal,
                p_SelCur1);
            LoanAssocSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur2);
            LoanGntyBorrSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur3);
            LoanGntyBSSelTSP (38, v_RetVal, v_LoanAppNmb, NULL, p_SelCur4);
            LoanGntyCollatSelTSP (38, v_LoanAppNmb, NULL, p_SelCur5);
            LoanGntyCollatLienSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur6);
            LoanDisbSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur7);
            LoanDisbPymtSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur8);
            /*LoanGntyGuarSelTSP (38,
                                v_LoanAppNmb,
                                NULL,
                                NULL,
                                v_RetVal,
                                p_SelCur9);*/
            -- JP modified for CAFSOPER - 2988
            LoanGntyGuarSelTSP (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SelCur9);

            LoanGntyInjctnSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur10);
            LoanIntDtlSelTSP (38, v_LoanAppNmb, p_SelCur11);
            LoanGntyPartLendrSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur12);

            LoanGntyPrinSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur13);
            LoanGntyRaceSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
            LoanGntyProcdSelTSP (38, NULL, v_LoanAppNmb, NULL, v_RetVal, p_SelCur15);
            LoanGntySubProcdSelTSP (38, p_LoanAppNmb, NULL,NULL, p_SelCur28, v_RetVal);
            LoanGntyPrtCmntSelTSP (38, v_LoanAppNmb, v_RetVal, p_SelCur16);
            LoanGntySpcPurpsSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur17);
            LoanGntyStbyAgrmtDtlSelTSP (38, v_LoanAppNmb, NULL, p_SELCUR20, v_RETVAL);
            LoanLmtGntyCollatSelTSP (38, v_LOANAPPNMB, NULL, NULL, NULL, NULL, P_SELCUR21, v_RETVAL);

            LoanGntyEconDevObjctChldSelTsp (38, v_LOANAPPNMB, NULL, P_SELCUR22, v_RETVAL);
            LoanExprtCntrySelTsp (
                p_IDENTIFIER => 38,
                p_LOANAPPNMB => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SELCUR => P_SELCUR23);
            --(38, v_LOANAPPNMB, P_SELCUR23, v_RETVAL);
            LoanAgntSelTsp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR24);
            --(38, v_LoanAppNmb, NULL, p_SELCUR24, v_RetVal);


            LoanAgntfeeDtlSelTSp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR25);
            --(38, v_LoanAppNmb, NULL, p_SELCUR25, v_RetVal);

            LOAN.DEFRMNTHISTRYSELTSP (38, v_loanappnmb, NULL, v_RetVal, p_SELCUR26);
            -- JP 8/21/2020 CARESACT 621
            LOAN.LoanHldStatSelTSP (p_Identifier => 38, p_LoanAppNmb => v_LoanAppNmb, p_SelCur => p_SELCUR27);

            OPEN p_SELCUR18 FOR SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.BusRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'B'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                UNION
                                SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.PerRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'P'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                ORDER BY 1, 2;                                             /*BorrTaxId, BorrBusPerInd */

            LOAN.LOANINFOEXTRACTSELCSP (v_LoanAppNmb, v_LoanNmb, p_SelCur19);
        END;
    END IF;
END ExtractSelCSP;
/



-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\MOVELOANGNTYPROCDCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.MOVELOANGNTYPROCDCSP(
p_Identifier 	IN NUMBER  := 0,
p_LoanAppNmb 	IN NUMBER  := 0,
p_RetVal  	OUT NUMBER,
p_RetStat 	OUT NUMBER,
p_RetTxt 	OUT VARCHAR2)
AS
/*     Parameters   :  No.   Name        Type     Description
                    1     p_Identifier  int     Identifies which batch
                                               to execute.
                    2     p_LoanAppNmb  int     LoanApplication Number to be moved
                    3     p_CreatUserId char(15)User Id
                    4     p_RetVal      int     No. of rows returned/
                                               affected
                    5     p_RetStat     int     The return value for the update
                    6     p_RetTxt      varchar return value text
*******************
*/
--NK  -- 08/29/2016--OPMSDEV 1099-- added LoanProcdRefDescTxt,LoanProcdPurAgrmtDt,LoanProcdPurIntangAssetAmt,LoanProcdPurIntangAssetDesc,LoanProcdPurStkHldrNm
--RY-- 10/03/2016 ---OPSMDEV 1177---Added NCAIncldInd,StkPurCorpText
BEGIN
--	savepoint moveloangntyprocdcsp;
/* Insert into LoanGntyProcd Table */
		IF  p_Identifier = 0 
		THEN
		
			BEGIN
				
				INSERT INTO LoanGntyProcdTbl (LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText    
                )
				SELECT  LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,
                LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText     

				FROM loanapp.LoanProcdTbl
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				
	/* Insert into LOANGNTYPROCDSUBTYPTBL Table */			
		INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
				SELECT  LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, LOANPROCDAMT, LoanProcdAddr,LOANSUBPROCDTYPCD, LoanProcdDesc, Lender     

				FROM loanapp.LOANSUBPROCDTBL
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				p_RetStat := SQLCODE;
				p_RetVal :=  SQL%ROWCOUNT;
	
				IF  p_RetStat <> 0 
				THEN
					BEGIN
		
						p_RetTxt :=  'LoanGntyProcdTbl not copied';
		
					END;
				END IF;
		        END;
		END IF;
p_RetVal := nvl(p_RetVal,0);
        p_RetStat := nvl(p_RetStat,0);		
/*exception when others then
begin
    raise;
    rollback to moveloangntyprocdcsp;
end;	*/
END MOVELOANGNTYPROCDCSP;
/


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Procedures\PROCDTXTBLCKSELTSP.sql"
CREATE OR REPLACE PROCEDURE SBAREF.PROCDTXTBLCKSELTSP
(
	p_Identifier number := 0,
	p_RetVal OUT number,
	p_LOANPROCDTYPCD char := NULL,
	p_PROCDTYPCD char := NULL,
	p_SelCur OUT SYS_REFCURSOR
)AS
BEGIN
	SAVEPOINT PROCDTXTBLCKSELTSP;

  IF p_Identifier = 0 or p_Identifier IS NULL
  THEN
                                           /* Select Row On the basis of Key*/
    BEGIN
      OPEN p_SelCur FOR
      SELECT
      PROCDTYPCD,
	LOANPROCDTYPCD,
	TEXTBLOCK
      from PROCDTXTBLCKTBL
      Where LOANPROCDTYPCD=p_LOANPROCDTYPCD and PROCDTYPCD = p_PROCDTYPCD;

      p_RetVal := SQL%ROWCOUNT;
    END;
      END IF;
EXCEPTION
	WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO PROCDTXTBLCKSELTSP;
		RAISE;
	END;
END;
/


GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO CDCONLINECORPGOVPRTUPDATE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO ILPERSLENDERROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO PUBLICREADROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO SBACDUPDTROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO SBAREFREADALLROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO SBAREFUPDCDROLE;

GRANT EXECUTE ON SBAREF.PROCDTXTBLCKSELTSP TO UPDCDROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Procedures\LOANSUBPROCDTYPSELTSP.sql"
CREATE OR REPLACE PROCEDURE SBAREF.LOANSUBPROCDTYPSELTSP (p_Identifier               NUMBER := 0,
                                                     p_RetVal               OUT NUMBER,
                                                    p_LOANPROCDTYPCD     CHAR := NULL,    
                                                    p_PROCDTYPCD         CHAR :=NULL,
                                                    p_SelCur      OUT SYS_REFCURSOR)
AS
BEGIN
   SAVEPOINT LOANSUBPROCDTYPSELTSP;
   /*All Active records*/
   IF p_Identifier = 0
   THEN
     
      BEGIN
        OPEN p_SelCur FOR
            SELECT 
            LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, 
            SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, LOANPROCDSUBTYPENDDT, 
            CREATUSERID, CREATDT, LASTUPDTUSERID, 
            LASTUPDTDT
            FROM SBAREF.LOANSUBPROCDTYPTBL where LOANPROCDTYPCD=p_LOANPROCDTYPCD and  PROCDTYPCD=p_PROCDTYPCD and LOANPROCDSUBTYPSTRTDT <= sysdate
   AND    (LOANPROCDSUBTYPENDDT is null
           or LOANPROCDSUBTYPENDDT > sysdate);   
         p_RetVal := SQL%ROWCOUNT;
      END;
      /*All active records for 504 loan type */
    ELSIF p_Identifier = 1
       THEN
     
      BEGIN
        OPEN p_SelCur FOR
            SELECT 
            LOANSUBPROCDTYPCD, PROCDTYPCD, LOANPROCDTYPCD, 
            SUBPROCDTYPDESCTXT, LOANPROCDSUBTYPSTRTDT, LOANPROCDSUBTYPENDDT, 
            CREATUSERID, CREATDT, LASTUPDTUSERID, 
            LASTUPDTDT
            FROM SBAREF.LOANSUBPROCDTYPTBL where LOANPROCDTYPCD=p_LOANPROCDTYPCD and  PROCDTYPCD=p_PROCDTYPCD and   LOANPROCDSUBTYPSTRTDT <= sysdate
   AND    (LOANPROCDSUBTYPENDDT is null
           or LOANPROCDSUBTYPENDDT > sysdate) and PROCDTYPCD='E';   
         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         ROLLBACK TO LOANSUBPROCDTYPSELTSP;
         RAISE;
      END;
END;
/


GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO CDCONLINECORPGOVPRTUPDATE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO ILPERSLENDERROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO PUBLICREADROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO SBACDUPDTROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO SBAREFREADALLROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO SBAREFUPDCDROLE;

GRANT EXECUTE ON SBAREF.LOANSUBPROCDTYPSELTSP TO UPDCDROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

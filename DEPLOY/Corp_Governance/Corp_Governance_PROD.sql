define deploy_name=Corp_Governance
define package_name=PROD
define package_buildtime=20201228110625
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Corp_Governance_PROD created on Mon 12/28/2020 11:06:26.16 by Jasleen Gorowada
prompt deploy scripts for deploy Corp_Governance_PROD created on Mon 12/28/2020 11:06:26.16 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Corp_Governance_PROD: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\Misc\Corp_Governance.sql 
Jasleen Gorowada committed e51738e on Mon Dec 21 15:30:56 2020 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\Misc\Corp_Governance.sql"
UPDATE CDCDOCS.DLVRTBL
SET    
       DLVRENDDT     = to_date('31-12-19','DD-MM-YY'),
       LASTUPDUSERID = user,
       LASTUPDDT     = sysdate
WHERE  DLVRENDDT        IS NOT NULL;
commit;


INSERT INTO CDCDOCS.DLVRTBL (
   DLVRID, DLVRNM, DLVRSTARTDT, 
   DLVRENDDT, ACTVIND, CREATUSERID, 
   CREATDT, LASTUPDUSERID, LASTUPDDT) 
VALUES ( (select max(DLVRID)+1 from CDCDOCS.DLVRTBL where DLVRNM='Annual Report'),
 'Annual Report',
 to_date('01-01-20','DD-MM-YY'),
 null,
 'Y',
 user,
 sysdate,
 user,
 sysdate );
 commit;
 
 
INSERT INTO CdcDocs.DLVRReqDocTypTbl 
select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and DLVRSTARTDT=to_date('01-01-20','DD-MM-YY')),
DocTypCd,user,sysdate,user,sysdate FROM CDCDOCS.DLVRReqDocTypTbl where 
DLVRID in (select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and trunc(DLVRSTARTDT)=to_date('01-01-19','DD-MM-YY'));
commit;


insert into  CdcDocs.DLVRReqDocTypTbl
select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and DLVRSTARTDT=to_date('01-01-20','DD-MM-YY')),
D.DocTypCd,
user,sysdate,user,sysdate
FROM SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     p.BusPrcsTypCd = (SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
AND     (p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     D.DocTypCd=1203;
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

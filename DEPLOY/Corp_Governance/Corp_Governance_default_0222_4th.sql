define deploy_name=Corp_Governance
define package_name=default_0222
define package_buildtime=20210222144234
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Corp_Governance_default_0222 created on Mon 02/22/2021 14:42:35.95 by Jasleen Gorowada
prompt deploy scripts for deploy Corp_Governance_default_0222 created on Mon 02/22/2021 14:42:35.95 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Corp_Governance_default_0222: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\CDCDOCS\Misc\Final_Qry_ALP_PCLP_Deliverables_Docs.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\CDCDOCS\Misc\Final_Qry_ALP_PCLP_Deliverables_Docs.sql"
ALTER TABLE CdcDocs.DLVRReqDocTypTbl ADD ReqDoc CHAR(1) DEFAULT 'Y';
COMMIT;

UPDATE CdcDocs.DLVRReqDocTypTbl SET ReqDoc = 'N' WHERE DLVRREQDOCTYPID = 76;
COMMIT;


create or replace PROCEDURE         cdcdocs.dlvrreqdoctypselcsp (
    p_identifier       NUMBER := NULL,
    p_dlvrid           NUMBER := NULL,
    p_SelCur       OUT SYS_REFCURSOR)
AS
BEGIN
    -- returns deliverable requirements set
    IF p_identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR
                  SELECT req.dlvrreqdoctypid      AS dlvrreqdoctypid,
                         req.dlvrid               AS dlvrid,
                         req.doctypcd             AS doctypcd,
                         doctyp.doctypdesctxt     AS doctypdesctxt,
                         dlvr.dlvrnm              AS dlvrnm,
                         req.reqdoc               AS reqDoc
                    FROM cdcdocs.dlvrreqdoctyptbl req
                         LEFT JOIN sbaref.doctyptbl doctyp
                             ON doctyp.doctypcd = req.doctypcd
                         LEFT JOIN cdcdocs.dlvrtbl dlvr
                             ON dlvr.dlvrid = req.dlvrid
                   WHERE req.dlvrid = p_dlvrid
                ORDER BY dlvr.dlvrnm ASC;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
        END;
END;
/
COMMIT;



set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

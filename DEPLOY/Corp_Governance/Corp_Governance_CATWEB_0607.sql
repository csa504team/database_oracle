define deploy_name=Corp_Governance
define package_name=CATWEB_0607
define package_buildtime=20210607163245
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Corp_Governance_CATWEB_0607 created on Mon 06/07/2021 16:32:45.82 by Jasleen Gorowada
prompt deploy scripts for deploy Corp_Governance_CATWEB_0607 created on Mon 06/07/2021 16:32:45.82 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Corp_Governance_CATWEB_0607: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCDOCS\update_dlvrreqdoctyptbl_0607.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCDOCS\update_dlvrreqdoctyptbl_0607.sql"
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1259 where DOCTYPCD=62 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1166 where DOCTYPCD=63 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1260 where DOCTYPCD=64 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1261 where DOCTYPCD=65 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1203 where DOCTYPCD=67 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1201 where DOCTYPCD=68 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1204 where DOCTYPCD=69 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1263 where DOCTYPCD=70 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1168 where DOCTYPCD=71 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1167 where DOCTYPCD=72 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1264 where DOCTYPCD=73 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1245 where DOCTYPCD=74 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1265 where DOCTYPCD=75 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1200, REQDOC='Y' where DOCTYPCD=76 and DLVRID=3;
update CDCDOCS.DlvrReqDocTypTbl set DOCTYPCD=1266,REQDOC='N'  where DOCTYPCD=77 and DLVRID=3;
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

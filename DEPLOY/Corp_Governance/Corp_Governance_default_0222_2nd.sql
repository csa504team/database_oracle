define deploy_name=Corp_Governance
define package_name=default_0222
define package_buildtime=20210222144234
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Corp_Governance_default_0222 created on Mon 02/22/2021 14:42:35.95 by Jasleen Gorowada
prompt deploy scripts for deploy Corp_Governance_default_0222 created on Mon 02/22/2021 14:42:35.95 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Corp_Governance_default_0222: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\CDCDOCS\Misc\Final_Qry_ALP_PCLP_Deliverables_Docs.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\CDCDOCS\Misc\Final_Qry_ALP_PCLP_Deliverables_Docs.sql"
-- Add new deliverable "ALP/PCLP Renewal" - DONE on Dev db
INSERT INTO CDCDOCS.DLVRTBL 
(
	DLVRID, DLVRNM, DLVRSTARTDT, 
	DLVRENDDT, ACTVIND, CREATUSERID, 
	CREATDT, LASTUPDUSERID, LASTUPDDT
) 
VALUES 
( 
	(select max(DLVRID)+1 from CDCDOCS.DLVRTBL ),
	 'ALP/PCLP Renewal',
	to_date('01-01-21','DD-MM-YY'),
	null,
	'Y',
	user,
	sysdate,
	user,
	sysdate 
);
 commit;



--===========Insert new doctypes into CdcDocs.DLVRReqDocTypTbl for new deliverable "ALP/PCLP Renewal" =======================================

--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "ALP/PCLP Renewal"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%ALP/PCLP Renewal%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Bylaws and Amendments"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Bylaws and Amendments%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Internal Control Policy & Board Approval"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Internal Control Policy '||CHR(38)||' Board Approval%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Policies: Credit Approval & Loan Servicing"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Policies: Credit Approval '||CHR(38)||' Loan Servicing%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Policies: Credit Approval & Loan Servicing"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Policies: Credit Approval '||CHR(38)||' Loan Servicing%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Organizational Chart"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Organizational Chart%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Board/Committee Members & Staff List"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Board/Committee Members '||CHR(38)||' Staff List%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Professional Services Contracts"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Professional Services Contracts%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Quarterly Delinquency Reports"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Quarterly Delinquency Reports%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "E&O Insurance Binder"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%E'||CHR(38)||'O Insurance Binder%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "D&O Insurance Binder"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%D'||CHR(38)||'O Insurance Binder%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Designated Attorney List"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Designated Attorney List%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Board Meeting Details & Minutes"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Board Meeting Details '||CHR(38)||' Minutes%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Independent Auditor Engagement Letter"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Independent Auditor Engagement Letter%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Economic Development Report"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Economic Development Report%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)%';
COMMIT;
----------------------------------------------------------------------------------------------
--==================== Set EmailSent = 'Y' Using CDCDOCS.SetDlvrCDCEMailSent stored proc=========================================================================

create or replace PROCEDURE         CDCDOCS.SetDlvrCDCEMailSent( 
p_identifier    NUMBER := NULL,
p_cdcdlvrid	NUMBER := NULL,
p_dlvrid	NUMBER := NULL,
p_cdcregncd     VARCHAR2,
p_cdcnmb        VARCHAR2,
p_creatuserid    VARCHAR2,	
	
p_SelCur         OUT SYS_REFCURSOR)
AS
BEGIN
    
    -- Retrieve all active deliverable names and their associated years
    IF p_identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            UPDATE
                CDCDOCS.CDCDLVRTBL
	    SET
		EmailSent = 'Y'	
            WHERE
                dlvrid = p_dlvrid
	    AND cdcdlvrid = p_cdcdlvrid
	    AND TRIM (cdcnmb) = TRIM (p_cdcnmb)
            AND TRIM (cdcregncd) = TRIM (p_cdcregncd)
            AND TRIM (creatuserid) = TRIM (p_creatuserid);
        END;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    BEGIN
        RAISE;
    END;
END;
/

--==================== Check for EmailSent CDCDOCS.CheckDlvrCDCEMailSent stored proc=========================================================================

create or replace PROCEDURE         CDCDOCS.CheckDlvrCDCEMailSent( 
p_identifier    NUMBER := NULL,
p_cdcdlvrid	NUMBER := NULL,
p_dlvrid	NUMBER := NULL,
p_cdcregncd     VARCHAR2,
p_cdcnmb        VARCHAR2,
p_creatuserid    VARCHAR2,	
	
p_SelCur         OUT SYS_REFCURSOR)
AS
BEGIN
    
    -- Retrieve all active deliverable names and their associated years
    IF p_identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            SELECT
                *
            FROM
                CDCDOCS.CDCDLVRTBL
            WHERE
                dlvrid = p_dlvrid
	    AND cdcdlvrid = p_cdcdlvrid
	    AND TRIM (cdcnmb) = TRIM (p_cdcnmb)
            AND TRIM (cdcregncd) = TRIM (p_cdcregncd)
            AND TRIM (creatuserid) = TRIM (p_creatuserid);
        END;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    BEGIN
        RAISE;
    END;
END;
/

GRANT EXECUTE ON CDCDOCS.CheckDlvrCDCEMailSent TO CDCDOCSREADALLROLE;

GRANT EXECUTE ON CDCDOCS.CheckDlvrCDCEMailSent TO CDCONLINEPRTREAD;


set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=OCT_Ach_Debit
define package_name=add
define package_buildtime=20201019213442
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy OCT_Ach_Debit_add created on Mon 10/19/2020 21:34:43.52 by Jasleen Gorowada
prompt deploy scripts for deploy OCT_Ach_Debit_add created on Mon 10/19/2020 21:34:43.52 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy OCT_Ach_Debit_add: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ACH_info.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ACH_info.sql"
set echo on
set feedback on
select '*** update ACH information***' from dual;
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6863688831       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9585635009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1200027805       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4407475003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1390010259467    ',LOANDTLACHBNKROUTNMB='031309123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4502095005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='22261614         ',LOANDTLACHBNKROUTNMB='261170876      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4280995008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001156707881     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8572535010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001064548336     ',LOANDTLACHBNKROUTNMB='122037760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4787215007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3860120401       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3686626004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005242184647    ',LOANDTLACHBNKROUTNMB='061113415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5265705009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='08782 06535      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7990564010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3944886677       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3209896001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4516303786       ',LOANDTLACHBNKROUTNMB='241070417      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4071245010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='21682 31395      ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4299875007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7599429466       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1284126005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1006401          ',LOANDTLACHBNKROUTNMB='061020197      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3257906007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01478301246      ',LOANDTLACHBNKROUTNMB='041215016      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4281235005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4010700088       ',LOANDTLACHBNKROUTNMB='084201294      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4381235002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4583498          ',LOANDTLACHBNKROUTNMB='081000676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4518215009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='41455510         ',LOANDTLACHBNKROUTNMB='061207839      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3054506005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1380413011       ',LOANDTLACHBNKROUTNMB='121002042      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4958725002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7102115826       ',LOANDTLACHBNKROUTNMB='125200934      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4973475008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='80577737         ',LOANDTLACHBNKROUTNMB='031318745      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5517325010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0806603738       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6740684003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='123321790        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4261145005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0162435738       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3566055009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2342003893       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4185965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='508851305        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7196385003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='508851305        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7196685010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='508851305        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6401375006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='691113026        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7440425005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='700433337        ',LOANDTLACHBNKROUTNMB='231372248      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3626746003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='229681002197     ',LOANDTLACHBNKROUTNMB='072401048      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3336376001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0202359812       ',LOANDTLACHBNKROUTNMB='061101375      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3396345009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0838342049       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2535886010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157513315216     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2560036004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='153400920770     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5042865003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='95048186         ',LOANDTLACHBNKROUTNMB='043318092      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5498705010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5943376011       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9360464009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325079855903     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8626885006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0087007761       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1121476000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6978049077       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4381665002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7490006065       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4605405005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='003203049        ',LOANDTLACHBNKROUTNMB='121143037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4256985005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8000034046       ',LOANDTLACHBNKROUTNMB='322285781      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5474125008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325103429470     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2439017003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1390000999810    ',LOANDTLACHBNKROUTNMB='031309123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2786406000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='291203286        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2623907010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3094682313       ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7169895008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1003785944       ',LOANDTLACHBNKROUTNMB='261170290      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3452885002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3967763578       ',LOANDTLACHBNKROUTNMB='123205054      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4853585001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8811476897       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1619867003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1250047171       ',LOANDTLACHBNKROUTNMB='121002042      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6702454003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='020101044        ',LOANDTLACHBNKROUTNMB='121142287      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5350595007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3522016629       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7053554002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1018817          ',LOANDTLACHBNKROUTNMB='061020197      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3480265001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2427610343       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4133435004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='352004279        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5021445007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='520773339        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2478676009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01381577222      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3863185009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1883132498       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2682986005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7087703 7        ',LOANDTLACHBNKROUTNMB='322276855      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2910106002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0105927277       ',LOANDTLACHBNKROUTNMB='061101375      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3932965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='002778788        ',LOANDTLACHBNKROUTNMB='322281507      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4097345003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3210104319       ',LOANDTLACHBNKROUTNMB='122003396      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2682996008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='259829973        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2086577000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5841045650       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3601306004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157507620936     ',LOANDTLACHBNKROUTNMB='121122676      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8075165005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='741252167        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2786216002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375008842597     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4898905005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='03109615         ',LOANDTLACHBNKROUTNMB='122042807      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3547065006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1139972916       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1776676002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3265173835       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2498186001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='06530604         ',LOANDTLACHBNKROUTNMB='061192274      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4116795005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375004269831     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3787725002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='223108926        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9747825000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01381866506      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4696875000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='041 045912       ',LOANDTLACHBNKROUTNMB='121102036      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3349256010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01381990951      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4461565007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='588665259        ',LOANDTLACHBNKROUTNMB='272471852      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4979195008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='454852513        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5160635005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0893670618       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3888015000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='57801009731      ',LOANDTLACHBNKROUTNMB='323371076      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3647165009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='844636746        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2809736009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1065970901       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3547665009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='046152039        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9089355009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2048753855       ',LOANDTLACHBNKROUTNMB='061201754      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3160356001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7968522313       ',LOANDTLACHBNKROUTNMB='061000227      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4881315009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0758691208       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9057474009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='012529248        ',LOANDTLACHBNKROUTNMB='121100782      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3906334000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='601068783        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9812964008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='106011649629     ',LOANDTLACHBNKROUTNMB='272480678      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5313415000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='06709 43478      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3520256000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='12147050         ',LOANDTLACHBNKROUTNMB='061120851      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5300705005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0592671630       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5411554004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0091464          ',LOANDTLACHBNKROUTNMB='061104123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5096435009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0094153          ',LOANDTLACHBNKROUTNMB='061104123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5132745006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2303807065       ',LOANDTLACHBNKROUTNMB='061112843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4829005002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4445267561       ',LOANDTLACHBNKROUTNMB='122238420      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5014544008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2138879040       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6177465003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1433607833       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4280535010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='334032864464     ',LOANDTLACHBNKROUTNMB='061000052      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4097375001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='050263771        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9548375004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325052703166     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7414765009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='135799879        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9393425000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000258652       ',LOANDTLACHBNKROUTNMB='322283767      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2880196005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2424132662       ',LOANDTLACHBNKROUTNMB='122239131      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3167806008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3800060830       ',LOANDTLACHBNKROUTNMB='122210406      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5159625005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3189822111       ',LOANDTLACHBNKROUTNMB='321270742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2221106007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='001064123899     ',LOANDTLACHBNKROUTNMB='122037760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3939405000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0811064646       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1301816003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0466604267       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3668045007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='131026940        ',LOANDTLACHBNKROUTNMB='061202672      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4315375000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6412394401       ',LOANDTLACHBNKROUTNMB='121108441      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2145806002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6410258001       ',LOANDTLACHBNKROUTNMB='121108441      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6990314000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6410258001       ',LOANDTLACHBNKROUTNMB='121108441      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3722195008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6410258001       ',LOANDTLACHBNKROUTNMB='121108441      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3096646010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='11200009009686   ',LOANDTLACHBNKROUTNMB='272481583      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1831776001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0094340145       ',LOANDTLACHBNKROUTNMB='061101375      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5138335002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3164645511       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8290395007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0131010492       ',LOANDTLACHBNKROUTNMB='122243635      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9314504007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0103305967       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4639005001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8828092505       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4121505004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0242017274       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7214045000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2100031380       ',LOANDTLACHBNKROUTNMB='122243761      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9424015010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='270123511        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7975145000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3688662414       ',LOANDTLACHBNKROUTNMB='055003201      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5096115007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='040763625        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8197225008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='103467858        ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201605010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0348500752       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6063134004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='212761961        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2806416001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='037170464        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5296345000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6835160802       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2947576007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3098587086       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9331415002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='325037610627     ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6821305003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4556676          ',LOANDTLACHBNKROUTNMB='322270518      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3406746001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7214112976       ',LOANDTLACHBNKROUTNMB='322271724      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2965866004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000080810       ',LOANDTLACHBNKROUTNMB='062206295      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4636635007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='334034731216     ',LOANDTLACHBNKROUTNMB='061000052      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4409865006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='334026456129     ',LOANDTLACHBNKROUTNMB='061000052      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4619415009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2004976          ',LOANDTLACHBNKROUTNMB='061121122      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5427855001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6370933753       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8660205005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='80001829325      ',LOANDTLACHBNKROUTNMB='321081669      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2714936007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='153499211552     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6218815009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='611067136        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3231526002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0344526488       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3675446003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8015008538       ',LOANDTLACHBNKROUTNMB='322070381      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5150325002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='006202782        ',LOANDTLACHBNKROUTNMB='121143037      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4006645005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2020327881       ',LOANDTLACHBNKROUTNMB='122232109      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2627206002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='411999807        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7120435009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='02057 11225      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5287605004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2041965241       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1868726001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00000259829973   ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8223195006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01586 60217      ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7041094009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5795111243       ',LOANDTLACHBNKROUTNMB='122232109      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2959546006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9867698616       ',LOANDTLACHBNKROUTNMB='123205054      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7119975010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00000073710138253',LOANDTLACHBNKROUTNMB='082907273      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2506296000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2648270222       ',LOANDTLACHBNKROUTNMB='122000661      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4051015003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0648065571       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5035895003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='291294650        ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2239926004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8250883124       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3646095010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='392073018        ',LOANDTLACHBNKROUTNMB='322271627      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5041175006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1700017854812    ',LOANDTLACHBNKROUTNMB='325272021      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5124495003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='601019906        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3460645000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='035724351        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9032475009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='031406827        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6139115002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='383012499423     ',LOANDTLACHBNKROUTNMB='031202084      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7982895000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5407008548       ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3364436003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0030122311       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4558635000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0495405243       ',LOANDTLACHBNKROUTNMB='121000358      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6209424001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5712392660       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2636006003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='06301238         ',LOANDTLACHBNKROUTNMB='321170978      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9169704000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1484507791       ',LOANDTLACHBNKROUTNMB='122239131      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1792346008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1390003774003    ',LOANDTLACHBNKROUTNMB='031309123      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7665134006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9014341827       ',LOANDTLACHBNKROUTNMB='031300012      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3566705003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='027740332        ',LOANDTLACHBNKROUTNMB='122242843      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5335035005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8616853585       ',LOANDTLACHBNKROUTNMB='031000053      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8969304002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='10007243         ',LOANDTLACHBNKROUTNMB='121144861      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4678835006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='63724207         ',LOANDTLACHBNKROUTNMB='321070450      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4512725008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='153497478021     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4673475006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='712008192        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4989034003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4350000386       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5289785005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='366069466        ',LOANDTLACHBNKROUTNMB='036001808      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4677285005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='200936375        ',LOANDTLACHBNKROUTNMB='322271724      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4040255010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8378627338       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4745085007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0063168355       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9155764006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01158740         ',LOANDTLACHBNKROUTNMB='122201198      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9904865006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0201092639       ',LOANDTLACHBNKROUTNMB='122228003      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1147657000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5714759817       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4634895000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6410528137       ',LOANDTLACHBNKROUTNMB='061101375      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4895825010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='004966655919     ',LOANDTLACHBNKROUTNMB='122400724      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6138884001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01381990731      ',LOANDTLACHBNKROUTNMB='072403473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4895505008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='165910011338     ',LOANDTLACHBNKROUTNMB='122235821      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2296857010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7166283254       ',LOANDTLACHBNKROUTNMB='122000247      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9581784004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='80001684936      ',LOANDTLACHBNKROUTNMB='321081669      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2678376007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0418814401       ',LOANDTLACHBNKROUTNMB='121108441      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7336094006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1115010239       ',LOANDTLACHBNKROUTNMB='121133416      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5474235006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='432703851        ',LOANDTLACHBNKROUTNMB='122016066      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2820066010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='003301389        ',LOANDTLACHBNKROUTNMB='122243062      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3656825008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0393947577       ',LOANDTLACHBNKROUTNMB='122000496      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3082636006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
commit;  

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=OCT_Ach_Debit
define package_name=check
define package_buildtime=20201019213145
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy OCT_Ach_Debit_check created on Mon 10/19/2020 21:31:46.71 by Jasleen Gorowada
prompt deploy scripts for deploy OCT_Ach_Debit_check created on Mon 10/19/2020 21:31:46.71 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy OCT_Ach_Debit_check: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_ACH_blank.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_ACH_blank.sql"
set echo on
set feedback on
select '*** testing to see if loan exisits with ACH blank***' from dual;
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9585635009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4407475003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4502095005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7041094009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4280995008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4281235005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4280535010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8572535010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4787215007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3686626004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5265705009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7990564010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7975145000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3209896001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4071245010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4299875007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1284126005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1301816003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3257906007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4381235002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4381665002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4518215009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3054506005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4958725002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4973475008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5517325010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6740684003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4261145005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4256985005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3566055009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4185965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7196385003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7196685010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6401375006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7440425005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3626746003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3336376001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3396345009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2535886010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2560036004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5042865003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5498705010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9360464009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8626885006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1121476000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4605405005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4619415009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5474125008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2439017003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2786406000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2786216002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2623907010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2627206002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7169895008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3452885002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4853585001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1619867003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6702454003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5350595007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7053554002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3480265001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4133435004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5021445007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2478676009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3863185009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2682986005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2682996008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2910106002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3932965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3939405000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4097345003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4097375001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2086577000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3601306004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8075165005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4898905005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3547065006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3547665009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1776676002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2498186001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4116795005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4121505004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3787725002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9747825000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4696875000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3349256010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4461565007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4979195008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5160635005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5159625005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3888015000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3647165009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2809736009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2806416001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9089355009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3160356001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4881315009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9057474009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3906334000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9812964008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5313415000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3520256000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5300705005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5296345000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5411554004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5096435009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5096115007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5132745006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5138335002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4829005002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5014544008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6177465003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9548375004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7414765009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9393425000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2880196005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3167806008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2221106007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3668045007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3675446003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4315375000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2145806002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6990314000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3722195008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3096646010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1831776001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8290395007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9314504007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4639005001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4636635007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7214045000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9424015010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8197225008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4201605010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6063134004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2947576007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9331415002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6821305003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3406746001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2965866004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4409865006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5427855001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8660205005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2714936007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6218815009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3231526002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5150325002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4006645005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7120435009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5287605004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1868726001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8223195006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2959546006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7119975010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2506296000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5035895003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3646095010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5041175006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5124495003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7982895000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6209424001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1792346008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3566705003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4989034003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5289785005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4634895000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4895825010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4895505008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9581784004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5474235006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3082636006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4051015003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2239926004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3460645000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9032475009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6139115002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='6138884001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3364436003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4558635000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2636006003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9169704000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9155764006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7665134006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='5335035005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='8969304002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4678835006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4677285005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4512725008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4673475006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4040255010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='4745085007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='9904865006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='1147657000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2296857010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2678376007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='7336094006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='2820066010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select count(*) from stgcsa.sofvlnd1tbl where LOANNMB='3656825008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

CREATE OR replace PROCEDURE cdconline.ltrtranscthistaudtseltsp ( 
p_identifier  NUMBER := NULL,  
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns all archived versions, irrespective of input parameters; should be used by admins only, never cdcs
    IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, pptranscthist
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrtranscthistaudttbl
			ORDER BY
				loannmb ASC, rqstcd ASC, prepaydt ASC, creatdt DESC;
		END;
	-- returns all archived versions, filtered by input parameters, including p_cdcregncd and p_cdcnmb; should be used by cdcs
	ELSIF p_Identifier = 1 then
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				rqstref
				, rqstdt
				, loannmb
				, rqstcd
				, prepaydt
				, cdcregncd
				, cdcnmb
				, pptranscthist
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.ltrtranscthistaudttbl
			WHERE
				TRUNC(prepaydt) = TRUNC(p_prepaydt)
			AND
				loannmb = p_loannmb
			AND
				rqstcd = p_rqstcd
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			ORDER BY
				creatdt DESC;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinereadallrole,cdconlineprtread;
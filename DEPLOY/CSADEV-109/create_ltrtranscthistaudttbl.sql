CREATE TABLE CDCONLINE.LtrTransctHistAudtTbl
(
	RQSTREF NUMBER(10, 0) NOT NULL 
	, RQSTDT DATE NOT NULL 
	, LOANNMB CHAR(10 CHAR) NOT NULL 
	, RQSTCD NUMBER(3, 0) NOT NULL 
	, PREPAYDT DATE 
	, CDCREGNCD CHAR(2 BYTE) NOT NULL 
	, CDCNMB CHAR(4 BYTE) NOT NULL 
	, PPTRANSCTHIST CLOB
	, CREATUSERID VARCHAR2(32 BYTE) NOT NULL 
	, CREATDT DATE DEFAULT SYSDATE NOT NULL 
	, LASTUPDTUSERID VARCHAR2(32 BYTE) NOT NULL 
	, LASTUPDTDT DATE DEFAULT SYSDATE NOT NULL
);
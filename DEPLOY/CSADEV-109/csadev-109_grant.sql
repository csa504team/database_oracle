-- deploy scripts for deploy csadev-109_grant created on Fri 08/02/2019 12:10:11.65 by johnlow
define deploy_name=csadev-109
define package_name=grant
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Misc\ad_hoc_grant_fix.sql 
zeeshan9000 committed 94a24af on Fri Aug 2 12:04:27 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Misc\ad_hoc_grant_fix.sql"
GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlinegovread;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

CREATE OR replace PROCEDURE cdconline.ltrtranscthistaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_pptranscthist CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrtranscthistaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrtranscthistaudttbl
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       pptranscthist, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_pptranscthist, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE ); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrtranscthistaudttbl; 

                 RAISE; 
             END; 
END; 
define deploy_name=SOFVLND1SELCSP
define package_name=UAT_0629
define package_buildtime=20210629105038
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy SOFVLND1SELCSP_UAT_0629 created on Tue 06/29/2021 10:50:39.34 by Jasleen Gorowada
prompt deploy scripts for deploy SOFVLND1SELCSP_UAT_0629 created on Tue 06/29/2021 10:50:39.34 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy SOFVLND1SELCSP_UAT_0629: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Procedures\sofvlnd1selcsp.sql 
Jasleen0605 committed 0316399 on Fri Feb 7 17:00:19 2020 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Procedures\sofvlnd1selcsp.sql"
CREATE OR REPLACE PROCEDURE STGCSA.SOFVLND1SELCSP
(
    p_Identifier IN number := 0,
    p_StartDT1 DATE:=null,
    p_StartDT2 DATE:=null,
    p_EndDT1 DATE:=null,
    p_EndDT2 DATE:=null,
    p_ColNames OUT VARCHAR2,
    p_RetVal OUT number,
    p_ErrVal OUT number,
    p_ErrMsg OUT varchar2,
    p_SelCur OUT SYS_REFCURSOR
)AS
-- variable declaration
BEGIN
    SAVEPOINT SOFVLND1SELCSP;
	-- Returns all report names and their descriptions
	IF p_Identifier = 0
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
    SELECT 1 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Newly Funded Loans' AS RPTNM, 'System generated list from the CSA System of all new loans established.' AS RPTDESC FROM DUAL
    UNION
    SELECT 2 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Payment Posting Status Info' AS RPTNM, 'Provides a population of payment information (including all statuses) from the CSA system.' AS RPTDESC FROM DUAL
    UNION
    SELECT 3 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Prepaid Loans' AS RPTNM, 'System generated list of loans from CSA with status code 31 (Prepayments).' AS RPTDESC FROM DUAL
    UNION
    SELECT 4 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Prepaid Status Change Info' AS RPTNM, 'System generated list of loans from CSA with status code 31 (Prepayments) Type 3 (Payment Posting).' AS RPTDESC FROM DUAL
    UNION
    SELECT 5 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Loan Status Change History' AS RPTNM, 'History of status changes.' AS RPTDESC FROM DUAL
    UNION
    SELECT 6 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Accelerated Loans' AS RPTNM, 'System generated list of accelerated loans from the CSA system.' AS RPTDESC FROM DUAL
    UNION
    SELECT 7 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Deferred Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 60 (Defer) requests, including the date in which the status was, updated since  (enter date range)' AS RPTDESC FROM DUAL
    UNION
    SELECT 8 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Deferred Loans History Log' AS RPTNM, 'System generated list of status changes to 60 (Defer) from the history log.' AS RPTDESC FROM DUAL
    UNION
    SELECT 9 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'History Log Value Changes' AS RPTNM, 'System generated list modified fields with old and new values from the history log.' AS RPTDESC FROM DUAL
    UNION
    SELECT 10 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Catch Up Loans' AS RPTNM, 'System generated list of loans from the CSA system with status code 06 (Catch Up), including the date in which the status was.' AS RPTDESC FROM DUAL
    UNION
    SELECT 11 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Catch Up Loans History Log' AS RPTNM, 'System generated list of status changes to 06 (Catch Up) from the history log.' AS RPTDESC FROM DUAL
    UNION 
    SELECT 12 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'ACH Changes' AS RPTNM, 'Population of ACH changes (Accl) received since; this should not capture loans that were newly funded.' AS RPTDESC FROM DUAL
    UNION
    SELECT 13 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'CDC Information Audit Change Log' AS RPTNM, 'Displays recent data changes to CDC Master fields' AS RPTDESC FROM DUAL
	UNION
	SELECT 14 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Accelerations Confirmation' AS RPTNM, 'Accelerations Proc Month Status Report' AS RPTDESC FROM DUAL
	UNION
	SELECT 15 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Accelerations Cycle Processing Status' AS RPTNM, 'Shows Cycle Processing Status Report' AS RPTDESC FROM DUAL
	UNION
	SELECT 16 AS RPTID, 'startdt1,enddt1' AS SEARCHINPUTS, 'Extract Audit' AS RPTNM, 'Extract Audit report (STG History)' AS RPTDESC FROM DUAL;
    
        p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
-- Newly Funded Loans - System generated list from the CSA System of all new loans established (enter date range).
	ELSIF p_Identifier = 1
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
		SELECT DISTINCT 
			L.LOANNMB,
			L.LOANDTLSTATCDOFLOAN,
			L.CREATDT,
			L.LOANDTLNOTEDT
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.LOANDTLISSDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
		ORDER BY L.LOANNMB
    ;
      p_ColNames := 'Loan Number, Loan Status, Create Date, Loan Detail Note Date';
	  p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
    ELSIF p_Identifier = 20
    THEN
    BEGIN
        OPEN p_SelCur FOR
        SELECT
            1
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_StartDT1)
		ORDER BY L.LOANNMB
    ;
 
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Payment Status Info - Provide a population of payment information (including all statuses) from the CSA system (enter date range)    
  ELSIF p_Identifier = 2
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT
      T.TRANSID,
      T.LOANNMB,
      --S.CREATDT,
      TO_CHAR(S.CREATDT, 'MM/DD/YYYY HH:MI:SS') AS CREATDT,
      R.STATNM,
      S.STATID,
      T.STATIDCUR,
      --T.CREATDT AS CREATDT1,
      TO_CHAR(T.CREATDT , 'MM/DD/YYYY HH:MI:SS') AS CREATDT1,
      L.LOANDTLSTATCDOFLOAN,
      S.LASTUPDTUSERID || ' - ' || EMP.FULLNM AS USERFULLNAME
FROM STGCSA.SOFVLND1TBL L
LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
LEFT JOIN STGCSA.CORETRANSSTATTBL S
        ON T.TRANSID = S.TRANSID
LEFT JOIN STGCSA.REFTRANSSTATTBL R
        ON S.STATID = R.STATID
LEFT JOIN STGCSA.COREEMPTBL EMP
       ON S.LASTUPDTUSERID = EMP.NTWKID
WHERE T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
AND T.TRANSTYPID = 3
ORDER BY T.LOANNMB, CREATDT DESC;


      p_ColNames := 'Transaction ID, Loan Number, Create Date, Status Name, Status ID, Status ID Cur, Create Date 1, Loan Detail Status, User ID/Name ';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Loans - System generated list of loans from CSA with status code 31/Prepayments
	ELSIF p_Identifier = 3
    THEN
    BEGIN
        OPEN p_SelCur FOR
	SELECT DISTINCT 
	  L.LOANNMB,
	  L.LOANDTLSTATCDOFLOAN,
	  L.LOANDTLSTATDT
	FROM STGCSA.SOFVLND1TBL L
	WHERE L.LOANDTLSTATCDOFLOAN = '31'
	AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
	;
 
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Prepaid Status Change Info - System generated list of loans from CSA with status code 31/Prepayments Type 3 (Payment Posting)
	ELSIF p_Identifier = 4
    THEN
    BEGIN
        OPEN p_SelCur FOR
		SELECT DISTINCT 
		  T.LOANNMB,
		  S.CREATDT,
		  R.STATNM AS TRANSTAT,
		  S.STATID,
		  L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
		  L.LOANDTLSTATDT
		FROM STGCSA.SOFVLND1TBL L
    LEFT JOIN STGCSA.CORETRANSTBL T ON T.LOANNMB = L.LOANNMB
    LEFT JOIN STGCSA.CORETRANSSTATTBL S ON T.TRANSID = S.TRANSID
    LEFT JOIN STGCSA.REFTRANSSTATTBL R ON S.STATID = R.STATID
		WHERE T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
		  AND T.TRANSTYPID = 3
		  AND L.LOANDTLSTATCDOFLOAN = '31'
		ORDER BY T.LOANNMB,
		  S.CREATDT DESC
    ;
 
      p_ColNames := 'Loan Number, Create Date, Trans Status, Status ID, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Loan Status Change History - History of status changes.
  ELSIF p_Identifier = 5
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT
      updtdt,
      pk loannmb, 
      change, 
      updtuserid 
    FROM stgcsa.stghistory 
    WHERE tablenm='SOFVLND1TBL'
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
    AND colname='LOANDTLSTATCDOFLOAN'
    ORDER BY loannmb, updtdt
    ;
    
      p_ColNames := 'Update Date, Loan Number, Change, Username';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Accelerated Loans - System generated list of accelerated loans from the CSA system (enter date range)
  ELSIF p_Identifier = 6
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT 
      T.LOANNMB,
      T.STATIDCUR AS TRANSSTAT,
      T.CREATDT,
      L.LOANDTLSTATCDOFLOAN AS LOANSTAT,
      L.LOANDTLSTATDT
    FROM 
      STGCSA.SOFVLND1TBL L
      LEFT JOIN STGCSA.CORETRANSTBL T
        ON T.LOANNMB = L.LOANNMB
    WHERE (T.CREATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY)
    --OR T.CREATDT BETWEEN TRUNC(p_StartDT2) AND TRUNC(p_EndDT2))
      AND T.TRANSTYPID = 8
	  AND L.LOANDTLSTATCDOFLOAN = 41
    ORDER BY T.LOANNMB
    ;

      p_ColNames := 'Loan Number, Trans Status, Create Date, Loan Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deferred Loans - System generated list of loans from the CSA system with status code 60/deferral requests, including the date in which the status was, updated since (enter date range)
	ELSIF p_Identifier = 7
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT 
	  L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '60'
    AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Deferred Loans History Log - System generated list of status changes to 60 (Defer) from the history logging.
  ELSIF p_Identifier = 8
    THEN
    BEGIN
        OPEN p_SelCur FOR
   SELECT DISTINCT
      UPDTDT,
      ACTION,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE Upper(tablenm) = 'SOFVLND1TBL'
    AND Upper(COLNAME) = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%"01" > "60"%'
    AND UPDTUSERID = 'MFUPDATE'
    AND TRUNC(updtdt) BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
    AND ACTION IS NOT NULL
    ;
    
      p_ColNames := 'Update Date, Action, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- History Log Value Changes - System generated list modified fields with old and new values from the history logging.
	ELSIF p_Identifier = 9
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT 
      l.UPDTDT,
      l.TABLENM,
      l.PK,
      d.COLNAME,
      '""' || d.OLDVALUE || '"" > ""' || d.NEWVALUE || '""' change,
      l.LOANNMB,
      l.UPDTUSERID,
      l.UPDTLOGID
    FROM stgcsa.STGUPDTLOGTBL l, stgcsa.STGUPDTLOGDTLTBL d
    WHERE UPDTLOGID = UPDTLOGFK
	AND updtdt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
	AND d.COLNAME != 'NO CHANGE'
    ORDER BY updtdt, UPDTLOGID, colname
    ;
    
      p_ColNames := 'Update Date, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Update Log ID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up Loans - System generated list of loans from the CSA system with status code 06/catch up, including the date in which the status was(enter date range)
  ELSIF p_Identifier = 10
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT 
      L.LOANNMB,
      L.LOANDTLSTATCDOFLOAN,
      L.LOANDTLSTATDT
    FROM STGCSA.SOFVLND1TBL L
    WHERE L.LOANDTLSTATCDOFLOAN = '06'
    AND L.LOANDTLSTATDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
    ;
    
      p_ColNames := 'Loan Number, Loan Detail Status, Loan Detail Status Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- Catch Up Loans History Log - System generated list of status changes to 06 (Catch Up) from the history logging (enter date range)
  ELSIF p_Identifier = 11
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT
      UPDTDT,
      ACTION,
      TABLENM,
      PK,
      COLNAME,
      CHANGE,
      LOANNMB,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory 
    WHERE tablenm = 'SOFVLND1TBL'
    AND COLNAME = 'LOANDTLSTATCDOFLOAN'
    AND CHANGE LIKE '%"60" > "06"%'
    AND UPDTUSERID = 'MFUPDATE'
    AND trunc(UPDTDT) BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
    AND ACTION IS NOT NULL
   ;
    
      p_ColNames := 'Update Date, Action, Table Name, Primary Key, Column Name, Change, Loan Number, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- ACH Changes - Population of ACH changes received since (enter date range) Note: This should not capture loans that were newly funded
  ELSIF p_Identifier = 12
    THEN
    BEGIN
        OPEN p_SelCur FOR
     SELECT DISTINCT
      PK,
      LOANNMB,
      ACTION,
      COLNAME,
      CHANGE,
      UPDTDT,
      UPDTUSERID,
      LOGENTRYID,
      CSA_SID
    FROM stgcsa.stghistory
    WHERE tablenm IN ('SOFVLND1TBL') 
    AND colname IN 
      (
        'LOANDTLACHPRENTIND',                                                           
        'LOANDTLACHBNKNM',                                                              
        'LOANDTLACHBNKMAILADRSTR1NM',                                                   
        'LOANDTLACHBNKMAILADRSTR2NM',                                                   
        'LOANDTLACHBNKMAILADRCTYNM',                                                    
        'LOANDTLACHBNKMAILADRSTCD',                                                     
        'LOANDTLACHBNKMAILADRZIPCD',                                                    
        'LOANDTLACHBNKMAILADRZIP4CD',                                                   
        'LOANDTLACHBNKBR',                                                              
        'LOANDTLACHBNKACTYP',                                                           
        'LOANDTLACHBNKACCT',                                                            
        'LOANDTLACHBNKROUTNMB',                                                         
        'LOANDTLACHBNKTRANS',                                                           
        'LOANDTLACHBNKIDNOORATTEN',                                                     
        'LOANDTLACHDEPNM',                                                              
        'LOANDTLACHSIGNDT',                                                             
        'LOANDTLACHLASTCHNGDT'
      ) 
    AND UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
	-- AND ACTION IS NOT NULL
    ;
    
      p_ColNames := 'Primary Key, Loan Number, Action, Column Name, Change, Update Date, Username, Log Entry ID, CSA SID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
-- CDC Information Audit Change Log - shows recent data changes to CDC Master fields
  ELSIF p_Identifier = 13
    THEN
    BEGIN
        OPEN p_SelCur FOR
    SELECT DISTINCT 
	  LOG.PK,
	  CDC.CDCNM,
	  LOG.UPDTDT,
	  LOG.UPDTUSERID,
	  DET.COLNAME,
	  DET.NEWVALUE,
	  DET.OLDVALUE
	FROM
	  (SELECT * FROM STGCSA.STGUPDTLOGTBL
	  ) LOG
	INNER JOIN
	  (SELECT * FROM STGCSA.STGUPDTLOGDTLTBL
	   WHERE COLNAME IN('CDCNM','CDCMAILADRSTR1NM','CDCMAILADRSTR2NM','CDCMAILADRCTYNM','CDCMAILADRSTCD','CDCMAILADRZIPCD','CDCMAILADRZIP4CD','CDCACHBNK','CDCACHBR','CDCACHMAILADRSTR1NM','CDCACHMAILADRSTR2NM','CDCACHMAILADRCTYNM','CDCACHMAILADRSTCD','CDCACHMAILADRZIPCD','CDCACHMAILADRZIP4CD','CDCACHROUTSYM','CDCACHTRANSCD','CDCACHACCTNMB','CDCACHSIGNDT')
	  ) DET
	ON LOG.UPDTLOGID = DET.UPDTLOGFK
	INNER JOIN
	  (SELECT STGCSA.SOFVCDCMSTRTBL.CDCNM,
		STGCSA.SOFVCDCMSTRTBL.CDCREGNCD,
		STGCSA.SOFVCDCMSTRTBL.CDCCERTNMB,
		STGCSA.SOFVCDCMSTRTBL.CDCREGNCD
		|| STGCSA.SOFVCDCMSTRTBL.CDCCERTNMB AS PK
	  FROM STGCSA.SOFVCDCMSTRTBL
	  ) CDC
	ON LOG.PK = CDC.PK
	WHERE LOG.TABLENM = 'SOFVCDCMSTRTBL'
	AND NVL(TRIM(DET.NEWVALUE),'$NULL$') <> NVL(TRIM(DET.OLDVALUE), '$NULL$')
	AND LOG.UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
		ORDER BY
		  LOG.UPDTDT DESC,
		  LOG.UPDTUSERID DESC,
		  DET.COLNAME
	;
    
      p_ColNames := 'CDC Number, CDC Name, Last Date Modified, Last Updated By, Field Name, New Value, Old Value';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
	
-- Accelerations Confirmation
  ELSIF p_Identifier = 14
    THEN
    BEGIN
        OPEN p_SelCur FOR	
	SELECT
	  PRCSID,
	  PRCSNM,
	  CYCID,
	  CYCMOTXT,
	  CYCYR,
	  MGRRVWUSER,
	  MGRRVWDT,
	  BUSDAY1DT
  FROM (
  SELECT DISTINCT 
	  CYCPRCS.PRCSID,
	  CYCPRCS.PRCSNM,
	  PRCSCYC.CYCID,
	  PRCSCYC.CYCMOTXT,
	  PRCSCYC.CYCYR,
	  PRCSCYCSTAT.MGRRVWUSER,
	  PRCSCYCSTAT.MGRRVWDT,
	  PRCSCYC.BUSDAY1DT,
      CYCPRCS.SORTORD
	FROM
	  (SELECT * FROM STGCSA.ACCCYCPRCSTBL
	  ) CYCPRCS
	RIGHT JOIN
	  (SELECT * FROM STGCSA.ACCPRCSCYCSTATTBL
	  ) PRCSCYCSTAT
	ON PRCSCYCSTAT.PRCSID = CYCPRCS.PRCSID
	RIGHT JOIN
	  (SELECT * FROM STGCSA.ACCPRCSCYCTBL
	  ) PRCSCYC
	ON PRCSCYC.CYCID         = PRCSCYCSTAT.CYCID
	WHERE PRCSCYC.BUSDAY1DT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
	AND CYCPRCS.PRCSID      IN (7, 8, 10, 11, 16, 17, 19, 20, 21, 22, 28, 29, 33)
	ORDER BY PRCSCYC.CYCID,
	  CYCPRCS.SORTORD )
	;
	
	  p_ColNames := 'Process ID, Process Name, Cycle ID, Cycle Month, Cycle Year, Analyst, Date Reviewed, Business Day Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;
	
-- Accelerations Cycle Processing Status
ELSIF p_Identifier = 15
    THEN
    BEGIN
        OPEN p_SelCur FOR	
	SELECT
		cycmotxt, 
		cycyr, 
		prcsnm, 
		STATUS, 
		setupdt, 
		mgrrvwuser, 
		mgrrvwdt
   FROM (
   SELECT DISTINCT 
	   CYCL.cycmotxt, 
       CYCL.cycyr, 
       PRCS.prcsnm, 
       CASE compflag 
         WHEN 'DONE' THEN 'COMPLETED' 
         ELSE 
           CASE Nvl(Length(To_char(setupdt)), 0) 
             WHEN 0 THEN 'NOT YET STARTED' 
             ELSE 'PROCESS ACTIVE' 
           END 
       END AS STATUS, 
       STAT.schdldt, 
       STAT.setupdt, 
       STAT.mgrrvwuser, 
       STAT.mgrrvwdt, 
       CYCL.lastupdtdt,
       PRCS.sortord
FROM   (SELECT cycid, 
               prcsid, 
               schdldt, 
               setupdt, 
               mgrrvwcmplt, 
               mgrrvwuser, 
               mgrrvwdt,
               CASE mgrrvwcmplt 
                 WHEN 1 THEN 'DONE' 
                 ELSE 'NOT' 
               END AS COMPFLAG 
        FROM   stgcsa.accprcscycstattbl) STAT 
       inner join (SELECT * 
                   FROM   stgcsa.accprcscyctbl) CYCL 
               ON CYCL.cycid = STAT.cycid 
       inner join (SELECT * 
                   FROM   stgcsa.acccycprcstbl) PRCS 
               ON STAT.prcsid = PRCS.prcsid 
WHERE  STAT.schdldt BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY 
ORDER  BY PRCS.sortord )
;
	
	  p_ColNames := 'Cycle Month, Cycle Year, Process Name, Status, Schedule Date, Analyst, Completed Date';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;

-- Extract Audit Report (STG History)
ELSIF p_Identifier = 16
    THEN
    BEGIN
        OPEN p_SelCur FOR
	SELECT DISTINCT
	  PK,
	  UPDTDT,
	  LOANNMB,
	  ACTION,
	  UPDTUSERID,
	  TABLENM,
	  COLNAME,
	  CHANGE,
	  LOGENTRYID,
	  CSA_SID
	FROM STGCSA.STGHISTORY
	WHERE UPDTDT BETWEEN TRUNC(p_StartDT1) AND TRUNC(p_EndDT1) + INTERVAL '1' DAY
	AND COLNAME != 'NO CHANGE'
	-- AND ACTION IS NOT NULL
	;
	
	  p_ColNames := 'CDC Number, Last Date Modified, Loan Number, Action, Last Updated By, Table Name, Column Name, Change, Log Entry ID, Session ID';
      p_RetVal := SQL%ROWCOUNT;
      p_ErrVal := SQLCODE;
      p_ErrMsg := SQLERRM;
    END;	
		
		
    END IF;
EXCEPTION
    WHEN OTHERS THEN
    BEGIN
        p_RetVal := 0;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
 
        ROLLBACK TO SOFVLND1SELCSP;
        RAISE;
    END;
END SOFVLND1SELCSP;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

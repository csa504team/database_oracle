declare
-- Script ensure certian prereqs for an STGCSA schema 
--   all users/roles granted to STGCSA, and that
--   STGCSA grants to are created.  
--
-- DOIT_DONT_COMPLAIN executes the statements but ignores errors
--   that will occur if objects already do/dont exist.
-- John Low
-- 
  sqltxt varchar2(256);
  gn varchar2(256);
  cnt number;
procedure doit_dont_complain is
begin
  execute immediate sqltxt;
exception when others then
  null;
end;
begin
--
-- First, make sure script is not being run against Mars!!!!
select max(global_name), count(*) into gn, cnt from global_name
  where global_name like '%.SBA.GOV';
if cnt>0 then
  raise_application_error(-20001,'Oops, looks like you are '
    ||'running on real SBA data base... dont do that!');
end if;
--
-- part 1, put roles, etc in place so grants to them will succeed
sqltxt:='create role LOANCSAREVIEWERROLE'; doit_dont_complain;                                                
sqltxt:='create role LOANCSAREADALLROLE'; doit_dont_complain;                                                 
sqltxt:='create role LOANCSAANALYSTROLE'; doit_dont_complain;                                                 
sqltxt:='create role LOANCSAMANAGERROLE'; doit_dont_complain;                                                 
sqltxt:='create role LOANCSAADMIN'; doit_dont_complain;                                                       
sqltxt:='create role LOANCSAADMINROLE'; doit_dont_complain;                                                   
sqltxt:='create user CDCONLINE identified by abc123'; doit_dont_complain;                                                          
sqltxt:='create role STGCSADEVROLE'; doit_dont_complain;                                                      
sqltxt:='create role CSAUPDTROLE'; doit_dont_complain;     
sqltxt:='create role CSADBROLE'; doit_dont_complain; 
sqltxt:='grant ALTER ANY TRIGGER to csadbrole'; doit_dont_complain;             
sqltxt:='grant CREATE ANY PROCEDURE to csadbrole'; doit_dont_complain;          
sqltxt:='grant CREATE VIEW to csadbrole'; doit_dont_complain;                   
sqltxt:='grant CREATE ANY TRIGGER to csadbrole'; doit_dont_complain;            
sqltxt:='grant ALTER ANY INDEX to csadbrole'; doit_dont_complain;               
sqltxt:='grant CREATE PROCEDURE to csadbrole'; doit_dont_complain;              
sqltxt:='grant CREATE SEQUENCE to csadbrole'; doit_dont_complain;               
sqltxt:='grant CREATE ANY TABLE to csadbrole'; doit_dont_complain;              
sqltxt:='grant CREATE ANY INDEX to csadbrole'; doit_dont_complain;              
sqltxt:='grant CREATE TRIGGER to csadbrole'; doit_dont_complain;                
sqltxt:='grant CREATE ANY VIEW to csadbrole'; doit_dont_complain;               
sqltxt:='grant ALTER ANY TABLE to csadbrole'; doit_dont_complain;               
sqltxt:='grant CREATE TABLE to csadbrole'; doit_dont_complain;                  
sqltxt:='grant CREATE ANY SEQUENCE to csadbrole'; doit_dont_complain;       
end;
/
    
select 'Special update of stgcsa runtime and DOIT_DONT_COMPLAIN (DDC)' from dual;
select 'Because this is updating DDC it cant use it, so maybe complaints' from dual;
accept pwd prompt "STGCSA password? " hide
connect stgcsa/&&pwd  
-- special recreate of runtime, DOIT_DONT_COMPLAIN
@&&3/MiscSQLDDL/runtime
@&&3/StoredProcs/runtime_doit_dont_complain
select 'All Done!' from dual;

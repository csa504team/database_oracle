CREATE OR REPLACE PROCEDURE STGCSA.AcclTrnscrptGen2TBLCSPdbg
    (p_retval out number, 
    p_errval out number,  
    p_errmsg out varchar2, 
    p_identifier number:=0, 
    p_userid varchar2, 
    p_rpt_start_dt_in date:=null, 
    p_rpt_end_dt_in date:=null) AS
  --
/*
  Writen by John Low,m Binary Frond, Select computing.
  This progrqam was derived from CDCONLINE.ACCLTRNSCRPTGENDEL4CSP.
  Logic to produce data is unchanged from that program.
  Changes are...
  1) Original PGM returned results as a weakly typed cursor variable, and 
     one of two different cursors/result sets was returned.  This version 
     populates temporary tables stgcsa.TTtranscriptrpt1 and stgcsa.TTtranscriptrpt2 
     with the result set previously returned by the cursors.  Both result tables
     are populated.
  2) p_rpt_start_dt and p_rpt_start_dt date now default to midnight of the first 
     day of the current month and midnight of the first day of the next month.
     (These dates may still be explicitly specified as parameters as well.)   
  3) Original PGM is part of CDCONLINE schema, this PGM is in STGCSA schema.
     Changing schema required prefixing table references with "CDCONLINE.", 
     and required CDCONLINE to execute the following grants to STGCSA:
      grant select, insert, update, delete on TTTransTbl to cdconline with grant option;
      grant select, insert, update, delete on TTPymtHistTmpTbl to cdconline with grant option;
      grant select, insert, update, delete on TTPymtHistTbl to cdconline with grant option;
      grant select, insert, update, delete on TTPymtMoTbl to cdconline with grant option;
      grant select, insert, update, delete on TTPymtNoTbl to cdconline with grant option;
      grant select, insert, update, delete on TTAccrTbl to cdconline with grant option;
      grant select, insert, update, delete on TTMoTbl to cdconline with grant option;
      grant select, insert, update, delete on TT5YrTbl to cdconline with grant option;
      grant select, insert, update, delete on TTEscrowTbl to cdconline with grant option;
      grant select, insert, update, delete on TTAdjTbl to cdconline with grant option;
      grant select on AcclLoanEntryTbl to stgcsa with grant option;
      grant select on LoanTbl to stgcsa with grant option;
      grant select on RtTbl to stgcsa with grant option;
      grant select on CDC5YrBreakTbl to stgcsa with grant option;
      grant select on DueFromBorrTbl to stgcsa with grant option;
      grant select on PortflTbl to stgcsa with grant option;
      grant select on DbentrPayoutTbl to stgcsa with grant option;
      grant select on PartcpntTbl to stgcsa with grant option;
      grant select on PartcpntAdrTbl to stgcsa with grant option;
      grant select on WireInstrctnTbl to stgcsa with grant option;
      grant select on PymtHistryTbl to stgcsa with grant option;
      grant execute on fn_BizDateAdd to stgcsa with grant option;  
  


*/  
  wrk1 number;
  wrk0 number;
  pgm varchar2(30):='AcclTrnscrptGen2TBLCSP';
  ver varchar2(40):='V1.1 22 Dec 2017';  
  dtfmt varchar2(40):='YYYY-MM-DD HH24:MI:SS';
  log_LOANNMB char(10):=null;
  log_TRANSID number:=null;
  log_dbmdl varchar2(10):=null;
  log_entrydt date:=null;
  log_userid  varchar2(32):=null;
  log_usernm  varchar2(199):=null;
  log_timestampfld date:=null;  
  logged_msg_id number;
  p_rpt_start_dt date;
  p_rpt_end_dt date; 
  --  accl_transcript_generate_r 2   --  'cdccsRV1'
   v_sys_lasttime DATE;
   v_ws_sba_loan_no CHAR(10);
   v_issue_date_char CHAR(10);
   v_issue_date DATE;
   v_payment_type CHAR(2);
   v_posting_date DATE;
   v_sba_fee NUMBER(19,4);
   v_csa_fee NUMBER(19,4);
   v_cdc_fee NUMBER(19,4);
   v_interest_amount NUMBER(19,4);
   v_principal_amount NUMBER(19,4);
   v_unalloc_amount NUMBER(19,4);
   v_balance NUMBER(19,4);
   v_late_fee NUMBER(19,4);
   v_dfb_amount NUMBER(19,4);
   v_accr_sba_fee NUMBER(19,4);
   v_accr_csa_fee NUMBER(19,4);
   v_accr_cdc_fee NUMBER(19,4);
   v_accr_late_fee NUMBER(19,4);
   v_accr_int NUMBER(19,4);
   v_accr_prin NUMBER(19,4);
   v_m_sba_fee -- monthly fee starts with m_
    NUMBER(19,4);
   v_m_csa_fee NUMBER(19,4);
   v_m_cdc_fee NUMBER(19,4);
   v_m_late_fee NUMBER(19,4);
   v_m_int NUMBER(19,4);
   v_m_prin NUMBER(19,4);
   v_pymt_no NUMBER(10,0);
   v_ws_pymt_no NUMBER(10,0);
   v_month_diff NUMBER(10,0);
   v_balance_used NUMBER(19,4);
   v_cdc_pct NUMBER(8,5);
   v_sba_pct NUMBER(8,5);
   v_csa_pct NUMBER(8,5);
   v_late_pct NUMBER(8,5);
   v_note_rate NUMBER(8,5);
   v_monthly_escrow NUMBER(19,4);
   v_ws_balance NUMBER(19,4);
   v_ws_posting_date DATE;
   v_semi_annual_date DATE;
   v_last_pymt_date DATE;
   v_max_pymt_no NUMBER(10,0);
   v_int_due_days NUMBER(5,0);
   v_int_paid_days NUMBER(5,0);
   v_loan_status CHAR(8);
   v_late_fee_paid_date -- last date of late fee posting
    DATE;
   v_pymt_amt NUMBER(19,4);
   v_m1_int_days NUMBER(3,0);
   -- accl_transcript_generate_r
   -- CSTitl, CSPhn
   v_cs_title VARCHAR2(30);
   v_cs_phone VARCHAR2(25);
   -- resultset + wire info
   v_trustee_name VARCHAR2(80);
   v_aba_number VARCHAR2(20);
   v_account_number VARCHAR2(20);
   v_account_name VARCHAR2(50);
   v_wire_re VARCHAR2(100);
   v_recip_name VARCHAR2(80);
   v_recip_name_1 VARCHAR2(80);
   v_recip_salutation VARCHAR2(10);
   v_recip_contact VARCHAR2(50);
   v_recip_street_1 VARCHAR2(100);
   v_recip_street_2 VARCHAR2(100);
   v_recip_city VARCHAR2(40);
   v_recip_state CHAR(10);
   v_recip_zip CHAR(10);
   v_sender_name VARCHAR2(80);
   v_sender_street_1 VARCHAR2(100);
   v_sender_street_2 VARCHAR2(100);
   v_sender_city VARCHAR2(40);
   v_sender_state CHAR(10);
   v_sender_zip CHAR(10);
   v_trustee_finacial_street_1 -- newly added for ALM 1049
    VARCHAR2(100);
   v_trustee_finacial_street_2 VARCHAR2(100);
   v_trustee_finacial_city VARCHAR2(40);
   v_trustee_finacial_state CHAR(10);
   v_trustee_finacial_zip CHAR(10);
   CURSOR cur_trans
     IS SELECT DISTINCT LoanNmb ,
   IssDt ,
   CDCPct ,
   SBAPct ,
   CSAPct ,
   LatePct ,
   NoteRtPct ,
   MoEscrowAmt ,
   LastPymtDt ,
   IntDueDays ,
   IntPaidDays ,
   LoanStatCd ,
   SemiAnnDt
     FROM cdconline.TTTransTbl ;

BEGIN
  dbms_output.enable(1000000);
  dbms_output.put_line('$$A.1: started'); 
  p_errval:=0;
  p_retval:=0;
  p_errmsg:=pgm||' '||ver||': Normal completion';
  --
  -- default report dates if not passed
  if p_rpt_start_dt_in is null then
    p_rpt_start_dt:=to_date(to_char(trunc(sysdate,'MM'),'yyyymmdd'),'yyyymmdd');
  else 
    p_rpt_start_dt:=p_rpt_start_dt_in;  
  end if;
  if p_rpt_end_dt is null then  
    p_rpt_end_dt:=to_date(to_char(trunc(p_rpt_start_dt+32,'MM'),'yyyymmdd'),'yyyymmdd');
  else
    p_rpt_end_dt:=p_rpt_end_dt_in;
  end if;  
  stgcsa.runtime_logger(logged_msg_id,'STDLOG',100,
    pgm||' '||ver||' entered, P_ID='||P_IDENTIFIER|| 
    ' Start_dt='||to_char(p_rpt_start_dt,dtfmt)||' End_dt='||to_char(p_rpt_end_dt,dtfmt),
    p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
  if p_identifier<> 0 then
    p_errval:=-20001;
    p_errmsg:=PGM||' '||ver||' called with invalid p_identifier: '||P_IDENTIFIER
      ||' Only supported value is 0';
  end if;
  if p_errval=0 then    
    begin  
     ----prod
     v_sys_lasttime := SYSDATE ;-- '5/18/09'
     DELETE FROM cdconline.TTTransTbl;
     DELETE FROM cdconline.TTPymtHistTmpTbl;
     DELETE FROM cdconline.TTPymtHistTbl;
     DELETE FROM cdconline.TTPymtMoTbl;
     DELETE FROM cdconline.TTPymtNoTbl;
     DELETE FROM cdconline.TTAccrTbl;
     DELETE FROM cdconline.TTMoTbl;
     DELETE FROM cdconline.TT5YrTbl;
     DELETE FROM cdconline.TTEscrowTbl;
     DELETE FROM cdconline.TTAdjTbl;
     delete from stgcsa.TTtranscriptrpt1;
     delete from stgcsa.TTtranscriptrpt2;
     dbms_output.put_line('$$A.2: cleared working tables');
     ----test
     ---select@SYSLastTime= '10/26/2012'
     -- CDCTTAdjTbl to calculate the accrued Int if there is an adjustment after the last pymt date
     -- get loan for the process month
     INSERT INTO cdconline.TTTransTbl
       ( RefID, RqstDt, LoanNmb, SemiAnnDt, AccrIntASOFDt, DbentrPrinBalAmt, DbentrIntDueAmt, CreatUserID, LastUpdtUserID )
       ( SELECT e.CDCAcclLoanEntryTblPK ,
                e.RqstDt ,
                e.LoanNmb ,
                e.SemiAnnDt ,
                CASE EXTRACT(MONTH FROM e.SemiAnnDt) WHEN 3 THEN
  TRUNC(e.SemiAnnDt) - INTERVAL '1' DAY
                ELSE
  TO_DATE(EXTRACT(MONTH FROM ADD_MONTHS(e.SemiAnnDt, -1)) || '/30/' || EXTRACT(YEAR FROM ADD_MONTHS(e.SemiAnnDt, -1)), 'MM/DD/YYYY')
                   END AccrIntASOFDt  ,
                e.DbentrPrinBalAmt ,
                e.DbentrIntDueAmt ,
    USER ,
    USER
         FROM cdconline.AcclLoanEntryTbl e,
              cdconline.LoanTbl l
          WHERE  e.LoanNmb = l.LoanNmb
                   AND l.LoanStatCd NOT IN ( 'PREPAID' )

                   AND e.RqstDt BETWEEN p_rpt_start_dt AND p_rpt_end_dt );
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.1: After first insert, cdconline.TTTransTbl cnt: '||wrk1);
     ---and   datediff(m, e.RqstDt, @SYSLastTime) = 0  --prod
     -------    datediff(m, e.RqstDt, @SYSLastTime) <= 1-- 11/17/09 display this month and last month's acceleration
     --and    datediff(m, e.RqstDt, @SYSLastTime) = 1
     -- (l.LoanStatnot in ('ACCELER.', 'PREPAID') or
     --    datediff(m, e.RqstDt, @SYSLastTime) = 1--show prior month
     --)
     /*
     ande.LoanNmb in ('2518106008', '6496144002', '2708366006', '7772614003',
     '2617406010', '9496384008', '3155806000',  '2862576008', '2994156002', '8808714006'-- UAT test loans
     , '6207804004', '2658536010', '6048223002', '2627294004')-- not part of UAT
     */
     -- accl_transcript_generate_r
     -- ande.LoanNmb = '2304304000'--'2500284004'
     -- get loan information
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, l.SmllBusCons, l.SmllBusCons AS SmllBusCons2, l.CDCRegnCd, l.CDCNmb, l.IssDt, l.MatDt, l.NotePrinAmt, l.NoteRtPct, NVL(l.AmortIntAmt, 0) + NVL(l.AmortPrinAmt, 0) AS pos_10, l.IntNeededAmt, l.PrinNeededAmt,
     l.CDCFeeNeededAmt, l.SBAFeeNeededAmt, l.CSAFeeNeededAmt, l.LateFeeNeededAmt, l.CDCPct, l.SBAPct, l.LoanStatCd
     FROM cdconline.TTTransTbl t ,cdconline.LoanTbl l
      WHERE t.LoanNmb = l.LoanNmb) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET SmllBusCons = src.SmllBusCons,
                                  StmtNm = src.SmllBusCons2,
                                  CDCRegnCd = src.CDCRegnCd,
                                  CDCNmb = src.CDCNmb,
                                  IssDt = src.IssDt,
                                  MatDt = src.MatDt,
                                  NotePrinAmt = src.NotePrinAmt,
                                  NoteRtPct = src.NoteRtPct,
                                  MoEscrowAmt = src.pos_10,
                                  IntNeededAmt = src.IntNeededAmt,
                                  PrinNeededAmt = src.PrinNeededAmt,
                                  CDCFeeNeededAmt = src.CDCFeeNeededAmt,
                                  SBAFeeNeededAmt = src.SBAFeeNeededAmt,
                                  CSAFeeNeededAmt = src.CSAFeeNeededAmt,
                                  LateFeeNeededAmt = src.LateFeeNeededAmt,
                                  CDCPct = src.CDCPct,
                                  SBAPct = src.SBAPct,
                                  LoanStatCd = src.LoanStatCd;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.2: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
    /*
     -- for deferred loans, use the amounts needed from the portfolio
     updateCDCTTTransTbl
     setIntNeeded= p.IntDueAmt,
     PrinNeeded= p.PrinDueAmt,
     CDCFeeNeeded= p.CDCFee,
     SBAFeeNeeded= p.SBAFee,
     CSAFeeNeeded= p.CSAFee,
     LateFeeNeeded= p.LateFeeDueAmt
     fromCDCTTTransTblt,
     CDCPortflTblp
     wheret.LoanNmb= p.LoanNmb
     andt.LoanStat= 'DEFERRED'
     */
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, l.BorrNm, l.BorrNm AS BorrNm2
     FROM cdconline.TTTransTbl t ,cdconline.LoanTbl l
      WHERE t.LoanNmb = l.LoanNmb
       AND ( LTRIM(RTRIM(t.SmllBusCons)) = ''
       OR LTRIM(RTRIM(t.SmllBusCons)) IS NULL )) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET SmllBusCons = src.BorrNm,
                                  StmtNm = src.BorrNm2;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.3: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- CSA pct
     MERGE INTO cdconline.TTTransTbl
     USING (SELECT TTTransTbl.ROWID row_id, CDCRtPct
     FROM cdconline.TTTransTbl ,cdconline.RtTbl
      WHERE CDCRtCd = 6) src
     ON ( TTTransTbl.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CSAPct = CDCRtPct;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.4: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- late pct
     MERGE INTO cdconline.TTTransTbl
     USING (SELECT TTTransTbl.ROWID row_id, CDCRtPct
     FROM cdconline.TTTransTbl ,cdconline.RtTbl
      WHERE CDCRtCd = 7) src
     ON ( TTTransTbl.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET LatePct = CDCRtPct;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.5: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- debenture rate
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
     FROM cdconline.TTTransTbl t ,( SELECT CDC5YrBreakTbl.LoanNmb ,
                               MAX(CDC5YrBreakTbl.PymtDt)  PymtDt
                        FROM cdconline.CDC5YrBreakTbl
                         WHERE  CDC5YrBreakTbl.PymtDt IS NOT NULL
                          GROUP BY CDC5YrBreakTbl.LoanNmb ) M ,cdconline.CDC5YrBreakTbl b
      WHERE t.LoanNmb = M.LoanNmb
       AND M.LoanNmb = b.LoanNmb
       AND M.PymtDt = b.PymtDt) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
                                  IntDueDays = src.IntDueDays,
                                  IntPaidDays = src.IntPaidDays;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.6: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- debenture rate for loans without any payments
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
     FROM cdconline.TTTransTbl t ,cdconline.CDC5YrBreakTbl b
      WHERE t.LoanNmb = b.LoanNmb
       AND t.DbentrRtPct IS NULL) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
                                  IntDueDays = src.IntDueDays,
                                  IntPaidDays = src.IntPaidDays;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$B.7: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- insert from pymt (with date posted prior to current run date) for the loans in CDCTTTransTbl
     INSERT INTO cdconline.TTPymtHistTmpTbl
       ( SELECT p.*
         FROM cdconline.TTTransTbl t,
              cdconline.PymtHistryTbl p
          WHERE  t.LoanNmb = p.LoanNmb
                   AND ( p.PymtTyp IN ( 'A','C','W','X' )

                   OR p.PymtTyp LIKE '%R%' )
                   AND TRUNC(v_sys_lasttime) - TRUNC(p.PostingDt) >= 0 );
     -- add fees if there is due from borrower receivable
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTPymtHistTmpTbl;               
    dbms_output.put_line('$$C.1: After initial insert to cdconline.TTPymtHistTmpTbl cnt: '||wrk1);
     MERGE INTO cdconline.TTPymtHistTmpTbl h
     USING (SELECT h.ROWID row_id, h.SBAFeeAmt + b.PostingAmt AS SBAFee
     FROM cdconline.TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
                                       b1.DtCls ,
                                       SUM(b1.PostingAmt)  PostingAmt
                                FROM cdconline.DueFromBorrTbl b1,
                                     cdconline.TTPymtHistTmpTbl h1
                                 WHERE  b1.LoanNmb = h1.LoanNmb
                                          AND h1.PostingDt = b1.DtCls
                                          AND h1.DueFromBorrAmt > 0
                                          AND b1.RecrdTyp = 7 -- sba code

                                          AND b1.StatCd = 9
                                  GROUP BY b1.LoanNmb,b1.DtCls ) b
      WHERE h.LoanNmb = b.LoanNmb
       AND h.PostingDt = b.DtCls
       AND h.DueFromBorrAmt > 0) src
     ON ( h.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET SBAFeeAmt = src.SBAFee;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTPymtHistTmpTbl;               
    dbms_output.put_line('$$C.2: After merge, sqlcount: '||wrk0||' cdconline.TTPymtHistTmpTbl cnt: '||wrk1);
     MERGE INTO cdconline.TTPymtHistTmpTbl h
     USING (SELECT h.ROWID row_id, h.CSAFeeAmt + b.PostingAmt AS CSAFee
     FROM cdconline.TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
                                       b1.DtCls ,
                                       SUM(b1.PostingAmt)  PostingAmt
                                FROM cdconline.DueFromBorrTbl b1,
                                     cdconline.TTPymtHistTmpTbl h1
                                 WHERE  b1.LoanNmb = h1.LoanNmb
                                          AND h1.PostingDt = b1.DtCls
                                          AND h1.DueFromBorrAmt > 0
                                          AND b1.RecrdTyp = 1 -- csa code

                                          AND b1.StatCd = 9
                                  GROUP BY b1.LoanNmb,b1.DtCls ) b
      WHERE h.LoanNmb = b.LoanNmb
       AND h.PostingDt = b.DtCls
       AND h.DueFromBorrAmt > 0) src
     ON ( h.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CSAFeeAmt = src.CSAFee;
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTPymtHistTmpTbl;               
    dbms_output.put_line('$$C.3: After merge, sqlcount: '||wrk0||' cdconline.TTPymtHistTmpTbl cnt: '||wrk1);
     MERGE INTO cdconline.TTPymtHistTmpTbl h
     USING (SELECT h.ROWID row_id, h.CDCFeeAmt + b.PostingAmt AS CDCFeeAmt
     FROM cdconline.TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
                                       b1.DtCls ,
                                       SUM(b1.PostingAmt)  PostingAmt
                                FROM cdconline.DueFromBorrTbl b1,
                                     cdconline.TTPymtHistTmpTbl h1
                                 WHERE  b1.LoanNmb = h1.LoanNmb
                                          AND h1.PostingDt = b1.DtCls
                                          AND h1.DueFromBorrAmt > 0
                                          AND b1.RecrdTyp = 2 -- cdc code

                                          AND b1.StatCd = 9
                                  GROUP BY b1.LoanNmb,b1.DtCls ) b
      WHERE h.LoanNmb = b.LoanNmb
       AND h.PostingDt = b.DtCls
       AND h.DueFromBorrAmt > 0) src
     ON ( h.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CDCFeeAmt = src.CDCFeeAmt;
     -- last pymt date
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTPymtHistTmpTbl;               
    dbms_output.put_line('$$C.4: After merge, sqlcount: '||wrk0||' cdconline.TTPymtHistTmpTbl cnt: '||wrk1);
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, h.LastPymtDt
     FROM cdconline.TTTransTbl t ,( SELECT TTPymtHistTmpTbl.LoanNmb ,
                               MAX(TTPymtHistTmpTbl.PostingDt)  LastPymtDt
                        FROM cdconline.TTPymtHistTmpTbl
                         WHERE  TTPymtHistTmpTbl.PymtTyp IN ( 'A','C','W' )

                          GROUP BY TTPymtHistTmpTbl.LoanNmb ) h
      WHERE t.LoanNmb = h.LoanNmb) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET LastPymtDt = src.LastPymtDt;
     -- last Bal
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$D.1: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, h.BalAmt
     FROM cdconline.TTTransTbl t ,cdconline.TTPymtHistTmpTbl h ,( SELECT TTPymtHistTmpTbl.LoanNmb ,
                                                   MAX(TTPymtHistTmpTbl.PostingDt)  PostingDt
                                            FROM cdconline.TTPymtHistTmpTbl
                                             WHERE  TTPymtHistTmpTbl.PymtTyp IN ( 'A','C','W','X' )

                                              GROUP BY TTPymtHistTmpTbl.LoanNmb ) D
      WHERE t.LoanNmb = D.LoanNmb
       AND t.LoanNmb = h.LoanNmb
       AND h.PostingDt = D.PostingDt) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET LastBalAmt = src.BalAmt;
     -- for loans without any payments
    wrk0:=sql%rowcount;                              
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$D.2: After merge, sqlcount: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET LastBalAmt = NotePrinAmt
      WHERE  LastBalAmt IS NULL;
    wrk0:=sql%rowcount;                              
    dbms_output.put_line('$$D.3: After update, sqlcount: '||wrk0);
     select count(*) into wrk1 from cdconline.TTTransTbl;
     wrk0:=0;
     dbms_output.put_line('$$E.1 Before big loop on cdconline.TTTransTbl, rows: '||wrk1);
     OPEN cur_trans;
     FETCH cur_trans INTO v_ws_sba_loan_no,v_issue_date_char,v_cdc_pct,
       v_sba_pct,v_csa_pct,v_late_pct,v_note_rate,
       v_monthly_escrow,v_last_pymt_date,v_int_due_days,v_int_paid_days,
       v_loan_status,v_semi_annual_date;

     WHILE cur_trans%FOUND
     LOOP
        DECLARE
           CURSOR cur_pymt
             IS SELECT PymtTyp ,
           PostingDt ,
           SBAFeeAmt ,
           CSAFeeAmt ,
           CDCFeeAmt ,
           IntAmt ,
           PrinAmt ,
           UnallocAmt ,
           LateFeeAmt ,
           DueFromBorrAmt
             FROM cdconline.TTPymtHistTmpTbl
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND ( PymtTyp IN ( 'A','C','W','X' )

             OR ( PymtTyp LIKE '%R%'
             AND TRUNC(MONTHS_BETWEEN(LAST_DAY(PostingDt), LAST_DAY(v_last_pymt_date))) > 0 ) )
             ORDER BY PostingDt;

        BEGIN
           -- insert loan funded record
           IF v_issue_date_char IS NOT NULL THEN

           BEGIN
              v_issue_date := TRUNC(TO_DATE(v_issue_date_char)) ;
              -- Mainframe added 1 day to Int basis day to calculate
              -- the first month Int and turn into production
              -- approx. on 9/30/1999.
              -- update the cutoff date to 8/10/1999 due to production for loan 2500284004 - updated on 11/17/09
              IF v_issue_date - TO_DATE('08/10/1999', 'mm/dd/yyyy') >= 1 THEN
               v_m1_int_days := 30 - EXTRACT(DAY FROM v_issue_date) + 1 ;
              ELSE
                 v_m1_int_days := 30 - EXTRACT(DAY FROM v_issue_date) ;
              END IF;

           END;

           -- accl_transcript_generate_r
           ELSE

           BEGIN
              v_issue_date := NULL ;

           END;
           END IF;
           IF v_last_pymt_date IS NULL THEN

           BEGIN
              v_last_pymt_date := v_issue_date ;

           END;
           END IF;
           INSERT INTO cdconline.TTPymtHistTbl
             ( LoanNmb, PostingDt, PymtNmb, CreatUserID, LastUpdtUserID )
             ( SELECT DISTINCT v_ws_sba_loan_no LoanNmb  ,
                               v_issue_date PostingDt  ,
                               0 pymt_no  ,
   USER ,
   USER
               FROM cdconline.TTTransTbl  );
           SELECT 0 ,
                  NotePrinAmt

             INTO v_ws_pymt_no,
                  v_ws_balance
             FROM cdconline.TTTransTbl
            WHERE  LoanNmb = v_ws_sba_loan_no;
            wrk0:=wrk0+sql%rowcount;                              
 
           -- initial monthly Int and Prin
           v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * v_m1_int_days, 2) ;
           SELECT MoEscrowAmt - v_m_int

             INTO v_m_prin

             --,@PymtAmt= Pymt160
             FROM cdconline.TTTransTbl
            WHERE  LoanNmb = v_ws_sba_loan_no;
           v_accr_sba_fee := 0 ;
           v_accr_csa_fee := 0 ;
           v_accr_cdc_fee := 0 ;
           v_accr_int := 0 ;
           v_accr_prin := 0 ;
           /*
           -- initial monthly Int, Prin and fees
           select@balance_used= NotePrin,
           fromCDCTTTransTbl
           whereLoanNmb= @ws_sba_loan_no
           */
           SELECT SUM(PostingAmt)  ,
                  MAX(PostingDt)

             INTO v_accr_late_fee,
                  v_late_fee_paid_date
             FROM cdconline.DueFromBorrTbl
            WHERE  LoanNmb = v_ws_sba_loan_no
                     AND RecrdTyp = 3
                     AND StatCd = 1;
           v_accr_late_fee := NVL(v_accr_late_fee, 0) ;
           v_late_fee_paid_date := NVL(v_late_fee_paid_date, v_issue_date) ;
           OPEN cur_pymt;
           FETCH cur_pymt INTO v_payment_type,v_posting_date,v_sba_fee,v_csa_fee,v_cdc_fee,v_interest_amount,v_principal_amount,v_unalloc_amount,v_late_fee,v_dfb_amount;
           -- pyment cursor
           WHILE cur_pymt%FOUND
           LOOP

              BEGIN
                 v_pymt_no := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_issue_date)));

                 -- find missing pymt months between 2 received pymt months
                 WHILE v_pymt_no - v_ws_pymt_no > 1
                 LOOP

                    BEGIN
                       v_ws_pymt_no := v_ws_pymt_no + 1 ;
                       /*
                       if @ws_pymt_no= 1
                       begin

                       -- initial monthly Int and Prin
                       select@m_int= NotePrin * @NoteRt/100 / 360 * @m1_int_days,
                       fromCDCTTTransTbl
                       whereLoanNmb= @ws_sba_loan_no

                       select@accr_sba_fee= 0,

                       end
                       */
                       -- missing pymt falls on 5 year break
                       IF MOD(v_ws_pymt_no, 60) = 1 THEN

                       BEGIN
                          v_balance_used := v_ws_balance ;
                          v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
                          v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
                          v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
                          v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
                          UPDATE cdconline.TTTransTbl
                             SET Pymt160Amt = v_pymt_amt
                           WHERE  LoanNmb = v_ws_sba_loan_no
                            AND v_ws_pymt_no = 1;
                          wrk0:=wrk0+sql%rowcount;  
                          UPDATE cdconline.TTTransTbl
                             SET Pymt61120Amt = v_pymt_amt
                           WHERE  LoanNmb = v_ws_sba_loan_no
                            AND v_ws_pymt_no = 61;
                          wrk0:=wrk0+sql%rowcount;  
                          UPDATE cdconline.TTTransTbl
                             SET Pymt121180Amt = v_pymt_amt
                           WHERE  LoanNmb = v_ws_sba_loan_no
                            AND v_ws_pymt_no = 121;
                          wrk0:=wrk0+sql%rowcount;  
                          UPDATE cdconline.TTTransTbl
                             SET Pymt181240Amt = v_pymt_amt
                           WHERE  LoanNmb = v_ws_sba_loan_no
                            AND v_ws_pymt_no = 181;
                          wrk0:=wrk0+sql%rowcount;  

                       END;
                       END IF;
                       /*
                       select@PymtAmt= case
                       when@pymt_no between 1 and 60 then Pymt160
                       when@pymt_no between 61 and 120then Pymt61120
                       when@pymt_no between 121 and 180then Pymt121180
                       when@pymt_no between 181 and 240then Pymt181240
                       end
                       fromCDCTTTransTbl
                       whereLoanNmb= @ws_sba_loan_no
                       */
                       -- monthly prin uses prior month accrued Int as part of the calculation
                       IF v_ws_pymt_no = 1 THEN

                       BEGIN
                          SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
                                 MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2)

                            INTO v_m_int,
                                 v_m_prin
                            FROM cdconline.TTTransTbl
                           WHERE  LoanNmb = v_ws_sba_loan_no;

                       END;
                       ELSE

                       BEGIN
                          v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;
                          v_m_prin := v_monthly_escrow - ROUND((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30, 2) ;

                       END;
                       END IF;
                       -- calc. accr. int and prin first since they use prior month accr. bal
                       v_accr_int := v_accr_int + v_m_int ;
                       v_accr_prin := v_accr_prin + v_m_prin ;
                       v_balance := v_ws_balance ;
                       -- calculate accr. fees
                       v_accr_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) + v_accr_sba_fee ;
                       v_accr_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) + v_accr_csa_fee ;
                       v_accr_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) + v_accr_cdc_fee ;
                       v_ws_posting_date := ADD_MONTHS(v_issue_date, v_ws_pymt_no);
                       v_ws_posting_date := TRUNC(v_ws_posting_date, 'MONTH') ;
                       -- calculate accr. late fee for non-deferred loan with
                       -- posting date later than last pymt date and
                       -- posting month <> s/a month
                       IF v_loan_status <> 'DEFERRED'
                         AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_ws_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
                         AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_ws_posting_date))) > 0 THEN

                       BEGIN
                          v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
                          IF v_m_late_fee < 100 THEN
                           v_m_late_fee := 100 ;
                          END IF;
                          v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;

                       END;
                       END IF;
                       -- insert payment hist table
                       INSERT INTO cdconline.TTPymtHistTbl
                         ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
                         ( SELECT v_ws_sba_loan_no LoanNmb  ,
                                  v_ws_pymt_no pymt_no  ,
                                  'DUE' STATUS  ,
                                  'D' PymtTyp  ,
                                  v_ws_posting_date PostingDt  ,
                                  NULL SBAFee  ,
                                  NULL CSAFee  ,
                                  NULL CDCFeeAmt  ,
                                  NULL IntAmt  ,
                                  NULL PrinAmt  ,
                                  NULL UnallocAmt  ,
                                  v_balance Bal  ,
                                  NULL LateFee  ,
                                  NULL PrepayAmt  ,
                                  NULL DFBAmt  ,
  USER ,
  USER
                             FROM DUAL  );
                        wrk0:=wrk0+sql%rowcount;     
                       -- insert to CDCTTMoTbl (the accrued table)
                       -- ifdatediff(m, @LastPymtDtx, @ws_posting_date) > 0
                       IF v_accr_int <> 0 THEN

                       BEGIN
                          INSERT INTO cdconline.TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
                            ( SELECT v_ws_sba_loan_no LoanNmb  ,
                                     v_ws_posting_date PostingDt  ,
                                     v_accr_int AccrInt  ,
                                     v_accr_cdc_fee CDCFeeAmt  ,
                                     v_accr_sba_fee SBAFee  ,
                                     v_accr_csa_fee CSAFee  ,
                                     v_accr_late_fee LateFee  ,
     USER ,
     USER
                                FROM DUAL  );
                         wrk0:=wrk0+sql%rowcount;
                       END;
                       END IF;

                    END;
                 END LOOP;
                 /*
                 selectpymt_no= @ws_pymt_no,
                 PostingDt= @ws_posting_date,
                 late_fee_paid_date = @late_fee_paid_date,
                 prior_bal= @ws_balance,
                 accr_int = @accr_int,
                 accr_prin= @accr_prin,
                 accr_sba_fee= @accr_sba_fee,
                 accr_csa_fee= @accr_csa_fee,
                 accr_cdc_fee= @accr_cdc_fee,
                 accr_late_fee= @accr_late_fee,
                 balance_used= @balance_used,
                 m_int= @m_int,
                 m_prin= @m_prin,
                 m_sba_fee= @m_sba_fee,
                 m_csa_fee= @m_csa_fee,
                 m_cdc_fee= @m_cdc_fee,
                 m_late_fee= @m_late_fee
                 -- accl_transcript_generate_r
                 */
                 -- while @pymt_no - @ws_pymt_no > 1
                 -- pymt received for the month
                 IF v_payment_type LIKE '%R%' THEN

                 BEGIN
                    v_sba_fee := 0 ;
                    v_csa_fee := 0 ;
                    v_cdc_fee := 0 ;
                    v_interest_amount := 0 ;
                    v_principal_amount := 0 ;
                    v_unalloc_amount := 0 ;

                 END;
                 END IF;
                 IF ( MOD(v_pymt_no, 60) = 1
                   AND v_pymt_no <> v_ws_pymt_no ) THEN

                  -- every 5 year break
                 BEGIN
                    v_balance_used := v_ws_balance ;
                    v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
                    v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
                    v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
                    v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
                    UPDATE cdconline.TTTransTbl
                       SET Pymt160Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_pymt_no = 1;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt61120Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_pymt_no = 61;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt121180Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_pymt_no = 121;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt181240Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_pymt_no = 181;
                    wrk0:=wrk0+sql%rowcount;  

                 END;
                 END IF;
                 IF v_pymt_no = 1
                   AND v_pymt_no <> v_ws_pymt_no THEN

                 BEGIN
                    -- very first pymt initialization
                    -- initial monthly Int and Prin
                    SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
                           MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2)

                      INTO v_m_int,
                           v_m_prin
                      FROM cdconline.TTTransTbl
                     WHERE  LoanNmb = v_ws_sba_loan_no;
                    v_m_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) ;
                    v_m_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) ;
                    v_m_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) ;
                    v_accr_sba_fee := 0 ;
                    v_accr_csa_fee := 0 ;
                    v_accr_cdc_fee := 0 ;
                    v_accr_int := 0 ;
                    v_accr_prin := 0 ;
                    v_balance := v_ws_balance ;

                 END;
                 ELSE

                 BEGIN
                    -- monthly prin uses prior month accrued Int as part of the calculation
                    IF v_pymt_no = v_ws_pymt_no THEN

                    BEGIN
                       v_m_prin := 0 ;
                       v_m_int := 0 ;
                       v_m_sba_fee := 0 ;
                       v_m_cdc_fee := 0 ;
                       v_m_csa_fee := 0 ;

                    END;
                    ELSE

                    BEGIN
                       v_m_prin := v_monthly_escrow - ROUND(((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30), 2) ;
                       v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;
                       v_m_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) ;
                       v_m_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) ;
                       v_m_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) ;

                    END;
                    END IF;

                 END;
                 END IF;
                 -- calc. accr. int and prin first since they use prior month accr. bal
                 v_accr_int := v_accr_int + v_m_int - v_interest_amount ;
                 v_accr_prin := v_accr_prin + v_m_prin - v_principal_amount ;
                 -- calculate accr. fees
                 v_accr_sba_fee := v_m_sba_fee + v_accr_sba_fee - v_sba_fee ;
                 v_accr_csa_fee := v_m_csa_fee + v_accr_csa_fee - v_csa_fee ;
                 v_accr_cdc_fee := v_m_cdc_fee + v_accr_cdc_fee - v_cdc_fee ;
                 v_balance := v_ws_balance - v_principal_amount ;
                 -- insert payment hist table
                 IF v_payment_type LIKE '%R%' THEN

                 BEGIN
                    -- calculate accr. late fee for non-deferred loan with
                    -- posting date later than last pymt date and
                    -- posting month <> s/a month
                    IF v_loan_status <> 'DEFERRED'
                      AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
                      AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_posting_date))) > 0 THEN

                    BEGIN
                       v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
                       IF v_m_late_fee < 100 THEN
                        v_m_late_fee := 100 ;
                       END IF;
                       v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;

                    END;
                    END IF;
                    INSERT INTO cdconline.TTPymtHistTbl
                      ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
                      ( SELECT v_ws_sba_loan_no LoanNmb  ,
                               v_pymt_no pymt_no  ,
                               'REJECTED' STATUS  ,
                               v_payment_type PymtTyp  ,
                               v_posting_date PostingDt  ,
                               NULL SBAFee  ,
                               NULL CSAFee  ,
                               NULL CDCFeeAmt  ,
                               NULL IntAmt  ,
                               NULL PrinAmt  ,
                               NULL UnallocAmt  ,
                               v_balance Bal  ,
                               v_late_fee LateFee  ,
                               NULL PrepayAmt  ,
                               v_dfb_amount DueFromBorrAmt  ,
   USER ,
   USER
                          FROM DUAL  );
                    wrk0:=wrk0+sql%rowcount;      

                 END;
                 ELSE

                 BEGIN
                    INSERT INTO cdconline.TTPymtHistTbl
                      ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
                      ( SELECT v_ws_sba_loan_no LoanNmb  ,
                               CASE v_payment_type
                                                  WHEN 'X' THEN NULL
                               ELSE v_pymt_no
                                  END pymt_no  ,
                               CASE v_payment_type
                                                  WHEN 'X' THEN 'ADJ'
                               ELSE 'REC' || CHR(39) || 'D'
                                  END STATUS  ,
                               v_payment_type PymtTyp  ,
                               v_posting_date PostingDt  ,
                               v_sba_fee SBAFee  ,
                               v_csa_fee CSAFee  ,
                               v_cdc_fee CDCFeeAmt  ,
                               v_interest_amount IntAmt  ,
                               v_principal_amount PrinAmt  ,
                               v_unalloc_amount UnallocAmt  ,
                               v_balance Bal  ,
                               v_late_fee LateFeeAmt  ,
                               NULL PrepayAmt  ,
                               v_dfb_amount DueFromBorrAmt  ,
   USER ,
   USER
                          FROM DUAL  );

                   wrk0:=wrk0+sql%rowcount;
                 END;
                 END IF;
                 -- insert to CDCTTMoTbl (the accrued table)
                 IF v_accr_int <> 0 THEN

                 BEGIN
                    INSERT INTO cdconline.TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
                      ( SELECT v_ws_sba_loan_no LoanNmb  ,
                               v_posting_date PostingDt  ,
                               v_accr_int AccrInt  ,
                               v_accr_cdc_fee CDCFeeAmt  ,
                               v_accr_sba_fee SBAFee  ,
                               v_accr_csa_fee CSAFee  ,
                               v_accr_late_fee LateFeeAmt  ,
   USER ,
   USER
                          FROM DUAL  );
                  wrk0:=wrk0+sql%rowcount;
                 END;
                 END IF;
                 /*
                 selectws_pymt_no= @ws_pymt_no,
                 late_fee_paid_date = @late_fee_paid_date,
                 pymt_no= @pymt_no,
                 PostingDt= @PostingDt,
                 prior_bal= @ws_balance,
                 accr_int = @accr_int,
                 accr_prin= @accr_prin,
                 accr_sba_fee= @accr_sba_fee,
                 accr_csa_fee= @accr_csa_fee,
                 accr_cdc_fee= @accr_cdc_fee,
                 accr_late_fee= @accr_late_fee,
                 balance_used= @balance_used,
                 m_int= @m_int,
                 m_prin= @m_prin,
                 m_sba_fee= @m_sba_fee,
                 m_csa_fee= @m_csa_fee,
                 m_cdc_fee= @m_cdc_fee,
                 m_late_fee= @m_late_fee,
                 IntAmt= @IntAmt,
                 PrinAmt= @PrinAmt
                 */
                 -- accl_transcript_generate_r
                 IF TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_last_pymt_date))) > 1 THEN

                 BEGIN
                    -- insert to CDCTTMoTbl (the accrued table)
                    INSERT INTO cdconline.TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
                      ( SELECT v_ws_sba_loan_no LoanNmb  ,
                               v_posting_date PostingDt  ,
                               v_accr_int AccrInt  ,
                               v_accr_cdc_fee CDCFeeAmt  ,
                               v_accr_sba_fee SBAFee  ,
                               v_accr_csa_fee CSAFee  ,
                               v_accr_late_fee LateFeeAmt  ,
   USER ,
   USER
                          FROM DUAL  );
                   wrk0:=wrk0+sql%rowcount;
                 END;
                 END IF;
                 v_ws_balance := v_balance ;
                 --if@PymtTyp<> 'X'
                 v_ws_pymt_no := v_pymt_no ;
                 FETCH cur_pymt INTO v_payment_type,v_posting_date,v_sba_fee,v_csa_fee,v_cdc_fee,v_interest_amount,v_principal_amount,v_unalloc_amount,v_late_fee,v_dfb_amount;

              END;
           END LOOP;
           CLOSE cur_pymt;
           -- missing pymt after last pymt date, until next s/a date
           v_max_pymt_no := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_issue_date))) ;
           WHILE v_max_pymt_no > v_ws_pymt_no
           LOOP

              BEGIN
                 v_ws_pymt_no := v_ws_pymt_no + 1 ;
                 v_ws_posting_date := ADD_MONTHS(v_issue_date, v_ws_pymt_no);
                 v_ws_posting_date := TRUNC(v_ws_posting_date, 'MONTH') ;
                 IF v_ws_pymt_no = 1 THEN

                 BEGIN
                    -- initial monthly Int and Prin
                    SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
                           MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
                           Pymt160Amt

                      INTO v_m_int,
                           v_m_prin,
                           v_pymt_amt
                      FROM cdconline.TTTransTbl
                     WHERE  LoanNmb = v_ws_sba_loan_no;
                    v_accr_sba_fee := 0 ;
                    v_accr_csa_fee := 0 ;
                    v_accr_cdc_fee := 0 ;
                    v_accr_int := 0 ;
                    v_accr_prin := 0 ;

                 END;
                 ELSE

                 BEGIN
                    -- monthly prin uses prior month accrued Int as part of the calculation
                    v_m_prin := v_monthly_escrow - ROUND(((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30), 2) ;
                    v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;

                 END;
                 END IF;
                 -- missing pymt falls on 5 year break
                 IF MOD(v_ws_pymt_no, 60) = 1 THEN

                 BEGIN
                    v_balance_used := v_ws_balance ;
                    v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
                    v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
                    v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
                    v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
                    UPDATE cdconline.TTTransTbl
                       SET Pymt160Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_ws_pymt_no = 1;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt61120Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_ws_pymt_no = 61;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt121180Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_ws_pymt_no = 121;
                    wrk0:=wrk0+sql%rowcount;  
                    UPDATE cdconline.TTTransTbl
                       SET Pymt181240Amt = v_pymt_amt
                     WHERE  LoanNmb = v_ws_sba_loan_no
                      AND v_ws_pymt_no = 181;
                    wrk0:=wrk0+sql%rowcount;
                 END;
                 END IF;
                 /*
                 select@PymtAmt= case
                 when@ws_pymt_no between 1 and 60 then Pymt160Amt
                 when@ws_pymt_no between 61 and 120then Pymt61120Amt
                 when@ws_pymt_no between 121 and 180then Pymt121180
                 when@ws_pymt_no between 181 and 240then Pymt181240Amt
                 end
                 fromCDCTTTransTbl
                 whereLoanNmb= @ws_sba_loan_no
                 */
                 -- calc. accr. int and prin first since they use prior month accr. bal
                 --if@ws_pymt_no - @pymt_no = 1
                 --select@accr_int= @accr_int + @m_int + round(@m_int / 30 * (@IntDueDays - @IntPaidDays), 2)
                 --else
                 v_accr_int := v_accr_int + v_m_int ;
                 v_accr_prin := v_accr_prin + v_m_prin ;
                 v_balance := v_ws_balance ;
                 -- calculate accr. fees
                 v_accr_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) + v_accr_sba_fee ;
                 v_accr_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) + v_accr_csa_fee ;
                 v_accr_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) + v_accr_cdc_fee ;
                 -- calculate accr. late fee for non-deferred loan with
                 -- posting date later than last pymt date and
                 -- posting month <> s/a month
                 IF v_loan_status <> 'DEFERRED'
                   AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_ws_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
                   AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_ws_posting_date))) > 0 THEN

                 BEGIN
                    v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
                    IF v_m_late_fee < 100 THEN
                     v_m_late_fee := 100 ;
                    END IF;
                    v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;

                 END;
                 END IF;
                 /*

                 selectPostingDt= @ws_posting_date,
                 late_fee_paid_date = @late_fee_paid_date,
                 prior_bal= @ws_balance,
                 accr_int = @accr_int,
                 accr_prin= @accr_prin,
                 accr_sba_fee= @accr_sba_fee,
                 accr_csa_fee= @accr_csa_fee,
                 accr_cdc_fee= @accr_cdc_fee,
                 accr_late_fee= @accr_late_fee,
                 balance_used= @balance_used,
                 m_int= @m_int,
                 m_prin= @m_prin,
                 m_sba_fee= @m_sba_fee,
                 m_csa_fee= @m_csa_fee,
                 m_cdc_fee= @m_cdc_fee,
                 m_late_fee= @m_late_fee
                 -- accl_transcript_generate_r
                 */
                 -- insert payment hist table
                 INSERT INTO cdconline.TTPymtHistTbl
                   ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
                   ( SELECT v_ws_sba_loan_no LoanNmb  ,
                            v_ws_pymt_no pymt_no  ,
                            'DUE' STATUS  ,
                            'D' PymtTyp  ,
                            v_ws_posting_date PostingDt  ,
                            NULL SBAFee  ,
                            NULL CSAFee  ,
                            NULL CDCFeeAmt  ,
                            NULL IntAmt  ,
                            NULL PrinAmt  ,
                            NULL UnallocAmt  ,
                            v_balance Bal  ,
                            NULL LateFeeAmt  ,
                            NULL PrepayAmt  ,
                            NULL DueFromBorrAmt  ,
    USER ,
    USER
                       FROM DUAL  );
                 wrk0:=wrk0+sql%rowcount;      
                 -- insert to CDCTTMoTbl (the accrued table)
                 IF v_accr_int <> 0 THEN

                 BEGIN
                    INSERT INTO cdconline.TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
                      ( SELECT v_ws_sba_loan_no LoanNmb  ,
                               v_ws_posting_date PostingDt  ,
                               v_accr_int AccrInt  ,
                               v_accr_cdc_fee CDCFeeAmt  ,
                               v_accr_sba_fee SBAFee  ,
                               v_accr_csa_fee CSAFee  ,
                               v_accr_late_fee LateFeeAmt  ,
   USER ,
   USER
                          FROM DUAL  );

                   wrk0:=wrk0+sql%rowcount;
                 END;
                 END IF;

              END;
           END LOOP;
           -- while @max_pymt_no > @ws_pymt_no
           UPDATE cdconline.TTTransTbl
              SET PrinAmt = v_accr_prin,
                  IntAmt = v_accr_int,
                  SBAFeeAmt = v_accr_sba_fee,
                  CDCFeeAmt = v_accr_cdc_fee,
                  CSAFeeAmt = v_accr_csa_fee,
                  LateFeeAmt = v_accr_late_fee
            WHERE  LoanNmb = v_ws_sba_loan_no;
           wrk0:=wrk0+sql%rowcount; 
           UPDATE cdconline.TTTransTbl
              SET LastBalAmt = v_balance
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND LastBalAmt <> v_balance;
           wrk0:=wrk0+sql%rowcount;  
           -- update the rest of the 5 year break pymt after the s/a date
           UPDATE cdconline.TTTransTbl
              SET Pymt160Amt = v_pymt_amt
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND v_ws_pymt_no < 1
             AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 1;
           wrk0:=wrk0+sql%rowcount;  
           UPDATE cdconline.TTTransTbl
              SET Pymt61120Amt = v_pymt_amt
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND v_ws_pymt_no < 61
             AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 61;
           wrk0:=wrk0+sql%rowcount;  
           UPDATE cdconline.TTTransTbl
              SET Pymt121180Amt = v_pymt_amt
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND v_ws_pymt_no < 121
             AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 121;
           wrk0:=wrk0+sql%rowcount;  
           UPDATE cdconline.TTTransTbl
              SET Pymt181240Amt = v_pymt_amt
            WHERE  LoanNmb = v_ws_sba_loan_no
             AND v_ws_pymt_no < 181
             AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 181;
           wrk0:=wrk0+sql%rowcount;  
           FETCH cur_trans INTO v_ws_sba_loan_no,v_issue_date_char,v_cdc_pct,v_sba_pct,v_csa_pct,
             v_late_pct,v_note_rate,v_monthly_escrow,v_last_pymt_date,v_int_due_days,
             v_int_paid_days,v_loan_status,v_semi_annual_date;

        END;
     END LOOP;
     CLOSE cur_trans;
    select count(*) into wrk1 from cdconline.TTTransTbl;               
    dbms_output.put_line('$$E.2: After big loop, insert/update count: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);

     --=================================================== new stop
     -- get info from the portfolio
     --setCDCNm= isnull(ltrim(rtrim(p.CDCNm)), '') + isnull(' ' + ltrim(rtrim(p.CDCNm1)), '')
     
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, NVL(LTRIM(RTRIM(p.CDCNm)), ' ') AS CDCNm
     FROM cdconline.TTTransTbl t ,cdconline.PortflTbl p
      WHERE t.LoanNmb = p.LoanNmb) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from cdconline.TTTransTbl; 
    dbms_output.put_line('$$F.1: After merge, %rowcnt: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- debenture rate
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
     FROM cdconline.TTTransTbl t ,( SELECT CDC5YrBreakTbl.LoanNmb ,
                               MAX(CDC5YrBreakTbl.PymtDt)  PymtDt
                        FROM cdconline.CDC5YrBreakTbl
                         WHERE  CDC5YrBreakTbl.PymtDt IS NOT NULL
                          GROUP BY CDC5YrBreakTbl.LoanNmb ) M ,cdconline.CDC5YrBreakTbl b
      WHERE t.LoanNmb = M.LoanNmb
       AND M.LoanNmb = b.LoanNmb
       AND M.PymtDt = b.PymtDt) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
                                  IntDueDays = src.IntDueDays,
                                  IntPaidDays = src.IntPaidDays;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from cdconline.TTTransTbl; 
    dbms_output.put_line('$$F.2: After merge, %rowcnt: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- debenture rate for loans without any payments
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
     FROM cdconline.TTTransTbl t ,cdconline.CDC5YrBreakTbl b
      WHERE t.LoanNmb = b.LoanNmb
       AND t.DbentrRtPct IS NULL) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
                                  IntDueDays = src.IntDueDays,
                                  IntPaidDays = src.IntPaidDays;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from cdconline.TTTransTbl; 
    dbms_output.put_line('$$F.3: After merge, %rowcnt: '||wrk0||' cdconline.TTTransTbl cnt: '||wrk1);
     -- escrow Bal
     INSERT INTO cdconline.TTEscrowTbl (LoanNmb,SemiAnnDt,EscrowBalAmt,EscrowDt,DbentrPoBalAmt,DbentrPoDt,CalcEscrowBalAmt,CalcDt, CreatUserID, LastUpdtUserID)
       ( SELECT LoanNmb ,
                SemiAnnDt ,
                NULL EscrowBal  ,
                NULL EscrowDt  ,
                NULL DbentrPoBal  ,
                NULL DbentrPoDt  ,
                NULL CalcEscrowBal  ,
                NULL CalcDt  ,
    USER ,
    USER
         FROM cdconline.TTTransTbl  );
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.1: After insert, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
         
  -- ZM: this had duplicate values
     MERGE INTO cdconline.TTEscrowTbl t1
     USING (SELECT DISTINCT t1.ROWID row_id, y1.EscrowBalAmt, y1.EscrowDt
     FROM cdconline.TTEscrowTbl t1 ,cdconline.CDC5YrBreakTbl y1
      WHERE t1.LoanNmb = y1.LoanNmb
       AND y1.PymtDt IS NOT NULL) src
     ON ( t1.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET EscrowBalAmt = src.EscrowBalAmt,
                                  EscrowDt = src.EscrowDt;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.2: After Merge, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     /*
     updateCDCTTEscrowTbl
     setDbentrPoBal= d.AvailAmt - d.PaidAmt,
     DbentrPoDt= d.PymtDt
     fromCDCTTEscrowTble,
     CDCDbentrPayoutTbld,
       (selectLoanNmb,
     PymtDt= max(PymtDt)
     fromCDCDbentrPayoutTbl
     wherestatus= 1
     group by LoanNmb) m
     wheree.LoanNmb= d.LoanNmb
     andd.LoanNmb= m.LoanNmb
     andd.PymtDt= m.PymtDt
     andd.status= 1
     */
     -- ZM: this has duplicate row ids and amount values; very troublesome
     MERGE INTO cdconline.TTEscrowTbl e
     USING (SELECT e.ROWID row_id, MIN(D.AvailAmt - D.PaidAmt) AS pos_2, D.PymtDt
     FROM cdconline.TTEscrowTbl e ,cdconline.DbentrPayoutTbl D ,( SELECT d2.LoanNmb ,
                                                        d2.PymtDt ,
                                                        MAX(d1.StatCd)  status
                                                 FROM cdconline.DbentrPayoutTbl d1,
                                                      ( SELECT DbentrPayoutTbl.LoanNmb ,
                                                               MAX(DbentrPayoutTbl.PymtDt)  PymtDt
                                                        FROM cdconline.DbentrPayoutTbl
                                                          GROUP BY DbentrPayoutTbl.LoanNmb ) d2
                                                  WHERE  d1.LoanNmb = d2.LoanNmb
                                                           AND TRUNC(d1.PymtDt) = TRUNC(d2.PymtDt)
                                                           AND d1.StatCd IN ( 1,2 -- payout and guarantee
                                                          )

                                                   GROUP BY d2.LoanNmb,d2.PymtDt ) M
      WHERE e.LoanNmb = D.LoanNmb
       AND D.LoanNmb = M.LoanNmb
       AND TRUNC(D.PymtDt) = TRUNC(M.PymtDt)
       AND D.StatCd = M.STATUS GROUP BY e.ROWID, D.PymtDt) src
     ON ( e.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET DbentrPoBalAmt = pos_2,
                                  DbentrPoDt = src.PymtDt;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.3: After merge, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     UPDATE cdconline.TTEscrowTbl
        SET DbentrPoBalAmt = 0
      WHERE  DbentrPoBalAmt IS NULL;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.4: After update, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     -- take the later date and Bal between EscrowDt and DbentrPoDt
     UPDATE cdconline.TTEscrowTbl
        SET CalcEscrowBalAmt = DbentrPoBalAmt,
            CalcDt = DbentrPoDt
      WHERE  DbentrPoDt > EscrowDt
       OR EscrowDt IS NULL;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.5: After update, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     UPDATE cdconline.TTEscrowTbl
        SET CalcEscrowBalAmt = EscrowBalAmt,
            CalcDt = EscrowDt
      WHERE  DbentrPoDt <= EscrowDt
       OR DbentrPoDt IS NULL;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.6: After update, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     UPDATE cdconline.TTEscrowTbl
        SET CalcEscrowBalAmt = 0
      WHERE  CalcEscrowBalAmt IS NULL;
     -- calculate escrow Bal
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.7: After update, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     MERGE INTO cdconline.TTEscrowTbl e
     USING (SELECT e.ROWID row_id, CalcEscrowBalAmt + NVL(p.PymtAmt, 0) AS CalcEscrowBal
     FROM cdconline.TTEscrowTbl e ,( SELECT p1.LoanNmb ,
                                SUM(p1.IntAmt)  + SUM(p1.PrinAmt)  + SUM(p1.UnallocAmt)  PymtAmt
                         FROM cdconline.TTEscrowTbl e1,
                              cdconline.TTPymtHistTbl p1
                          WHERE  e1.LoanNmb = p1.LoanNmb
                                   AND p1.PostingDt > e1.CalcDt
                           GROUP BY p1.LoanNmb ) p
      WHERE e.LoanNmb = p.LoanNmb) src
     ON ( e.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CalcEscrowBalAmt = src.CalcEscrowBal;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTEscrowTbl; 
    dbms_output.put_line('$$G.8: After merge, %rowcnt: '||wrk0||'  cdconline.TTEscrowTbl: '||wrk1);
     -- select * from CDCTTEscrowTbl  -- accl_transcript_generate_r
     /*
     -- when process date is close to semi-annual, and the debenture payout has not
     -- been posted yet on the process, but a few days later, the escrow will be zero
     updateCDCTTEscrowTbl
     setCalcEscrowBal= 0
     where@SYSLastTime between CalcDt and SemiAnnDt
     anddatediff(m, @SYSLastTime, SemiAnnDt) <= 1
     */
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, e.CalcEscrowBalAmt
     FROM cdconline.TTTransTbl t ,cdconline.TTEscrowTbl e
      WHERE t.LoanNmb = e.LoanNmb) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET EscrowBalAmt = src.CalcEscrowBalAmt;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.1: After merge, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     -- if escrow Bal is less than $1 due to rounding, change to zero 10/30/09
     UPDATE cdconline.TTTransTbl
        SET EscrowBalAmt = 0
      WHERE  EscrowBalAmt BETWEEN 0 AND 1;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.2: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     -- amortization scheduled Bal
     UPDATE cdconline.TTTransTbl
        SET AmortBalAmt = LastBalAmt - PrinAmt;
     -- cdc fees owed
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.3: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, D.CDCFeeOwed
     FROM cdconline.TTTransTbl t ,( SELECT DueFromBorrTbl.LoanNmb ,
                               SUM(DueFromBorrTbl.PostingAmt)  CDCFeeOwed
                        FROM cdconline.DueFromBorrTbl
                         WHERE  DueFromBorrTbl.RecrdTyp = 2
                                  AND DueFromBorrTbl.StatCd = 1
                          GROUP BY DueFromBorrTbl.LoanNmb ) D
      WHERE t.LoanNmb = D.LoanNmb) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CDCFeeOwedAmt = src.CDCFeeOwed;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.4: After merge, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET CDCFeeOwedAmt = 0
      WHERE  CDCFeeOwedAmt IS NULL;
     -- CSA Fees Due
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.5: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET CSAFeeDueAmt = CSAFeeAmt
      WHERE  EscrowBalAmt >= CSAFeeAmt;
     -- if EscrowBal only covers partial CSAFee, cover the full month fee starting from the beginning of the missing payment month
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.6: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     MERGE INTO cdconline.TTTransTbl t
     USING (SELECT t.ROWID row_id, M.CSAFee
     FROM cdconline.TTTransTbl t ,( SELECT m1.LoanNmb ,
                               MAX(m1.CSAFeeAmt)  CSAFee
                        FROM cdconline.TTTransTbl t1,
                             cdconline.TTMoTbl m1
                         WHERE  m1.LoanNmb = t1.LoanNmb
                                  AND m1.CSAFeeAmt <= t1.EscrowBalAmt
                          GROUP BY m1.LoanNmb ) M
      WHERE t.LoanNmb = M.LoanNmb
       AND t.EscrowBalAmt < t.CSAFeeAmt) src
     ON ( t.ROWID = src.row_id )
     WHEN MATCHED THEN UPDATE SET CSAFeeDueAmt = src.CSAFee;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.7: After Merge, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     -- for partial cover of CSAFee, if escrow bal cannot cover even 1 pymt, show full csa fee 10/30/09
     -- request on 3/1/2010, CSAFeeDue s/b 0 instead of CSAFee
     UPDATE cdconline.TTTransTbl
        SET CSAFeeDueAmt = 0 -- CSAFee

      WHERE  EscrowBalAmt < CSAFeeAmt
       AND CSAFeeDueAmt IS NULL;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.8: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET CSAFeeDueAmt = 0
      WHERE  EscrowBalAmt <= 0;
     -- due Amt
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.9: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET DueAmt = DbentrPrinBalAmt + DbentrIntDueAmt - (EscrowBalAmt - CSAFeeDueAmt);
     -- DueDt, 2 business days prior to semi-annual date
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.9: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     UPDATE cdconline.TTTransTbl
        SET DueDt = cdconline.fn_BizDateAdd(SemiAnnDt, -2);
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.10: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     SELECT pa.CntctTitl ,
            pa.CntctPhnNmb

       INTO v_cs_title,
            v_cs_phone
       FROM cdconline.PartcpntTbl p,
            cdconline.PartcpntAdrTbl pa
      WHERE  p.PrgrmCd = pa.PrgrmCd
               AND p.PrgrmCd = 10
               AND p.PartcpntCd = 20 -- sender

               AND pa.PartcpntAdrCd = 101 AND ROWNUM <= 1;-- primary contact
     UPDATE cdconline.TTTransTbl
        SET CSTitl = v_cs_title,
            CSPhnNmb = v_cs_phone,
            RlseDt = v_sys_lasttime
            --  use RqstDt 11/18/09
             -- AccelASOFDt= convert(smalldatetime, convert(varchar, month(dateadd(m, 1, @SYSLastTime))) + '/01/' +  convert(varchar, year(dateadd(m, 1, @SYSLastTime))) )
            ,
            AccelASOFDt = TRUNC(ADD_MONTHS(RqstDt, 1), 'MONTH');
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  cdconline.TTTransTbl; 
    dbms_output.put_line('$$H.11: After update, %rowcnt: '||wrk0||'  cdconline.TTTransTbl: '||wrk1);
     -- get trustee info
     SELECT PartcpntNm ,
            ABANmb ,
            AcctNmb ,
            AcctNm ,
            WireRE ,
            a.PartcpntMailAdrStr1Nm ,-- newly added for ALM 1049

            a.PartcpntMailAdrStr2Nm ,
            a.PartcpntMailAdrCtyNm ,
            a.PartcpntSt ,
            a.PartcpntMailAdrZipCd -- newly added for ALM 1049


       INTO v_trustee_name,
            v_aba_number,
            v_account_number,
            v_account_name,
            v_wire_re,
            v_trustee_finacial_street_1,
            v_trustee_finacial_street_2,
            v_trustee_finacial_city,
            v_trustee_finacial_state,
            v_trustee_finacial_zip
       FROM cdconline.PartcpntTbl p,
            cdconline.WireInstrctnTbl W,
            cdconline.PartcpntAdrTbl a
      WHERE  p.PrgrmCd = W.PrgrmCd
               AND p.PartcpntID = W.PartcpntID
               AND p.PrgrmCd = 10
               AND p.PartcpntCd = 30
               AND p.PartcpntID = a.PartcpntID;
     -- get recipient info
     SELECT p.PartcpntNm ,
            p.PartcpntNm ,
            a.CntctSalutation ,
            a.CntctNm ,
            a.PartcpntMailAdrStr1Nm ,
            a.PartcpntMailAdrStr2Nm ,
            a.PartcpntMailAdrCtyNm ,
            a.PartcpntSt ,
            a.PartcpntMailAdrZipCd

       INTO v_recip_name,
            v_recip_name_1,
            v_recip_salutation,
            v_recip_contact,
            v_recip_street_1,
            v_recip_street_2,
            v_recip_city,
            v_recip_state,
            v_recip_zip
       FROM cdconline.PartcpntTbl p,
            cdconline.PartcpntAdrTbl a
      WHERE  p.PrgrmCd = a.PrgrmCd
               AND p.PartcpntID = a.PartcpntID
               AND a.PartcpntAdrCd = 101
               AND p.PartcpntCd = 10;
     -- get sender info
     SELECT p.PartcpntNm ,
            a.PartcpntMailAdrStr1Nm ,
            a.PartcpntMailAdrStr2Nm ,
            a.PartcpntMailAdrCtyNm ,
            a.PartcpntSt ,
            a.PartcpntMailAdrZipCd

       INTO v_sender_name,
            v_sender_street_1,
            v_sender_street_2,
            v_sender_city,
            v_sender_state,
            v_sender_zip
       FROM cdconline.PartcpntTbl p,
            cdconline.PartcpntAdrTbl a
      WHERE  p.PrgrmCd = a.PrgrmCd
               AND p.PartcpntID = a.PartcpntID
               AND a.PartcpntAdrCd = 101
               AND p.PartcpntCd = 20;
      exception when others then
      p_errval:=sqlcode;
      p_errmsg:=pgm||' '||ver||': Oracle error occured while preparing data.'
        ||' SQLCODE='||sqlcode||' '||sqlerrm(sqlcode);          
    end;           
  end if;
  if p_errval=0 then 
    BEGIN
      insert into stgcsa.TTtranscriptrpt1
         SELECT DISTINCT t.RefID ,
                         t.RqstDt ,
                         t.SmllBusCons ,
                         t.StmtNm ,
                         t.CDCRegnCd ,
                         t.CDCNmb ,
                         t.CDCNm ,
                         t.LoanNmb ,
                         t.IssDt ,
                         t.MatDt ,
                         t.SemiAnnDt ,
                         t.NotePrinAmt ,
                         t.DbentrRtPct ,
                         t.NoteRtPct ,
                         t.LastPymtDt ,
                         t.LastBalAmt ,
                         t.MoEscrowAmt ,
                         t.Pymt160Amt ,
                         t.Pymt61120Amt ,
                         t.Pymt121180Amt ,
                         t.Pymt181240Amt ,
                         t.PrinAmt AS tPrinAmt,
                         t.IntAmt AS tIntAmt,
                         t.SBAFeeAmt  AS tSBAFeeAmt,
                         t.CDCFeeAmt AS tCDCFeeAmt,
                         t.CSAFeeAmt  AS tCSAFeeAmt,
                         t.LateFeeAmt AS tLateFeeAmt,
                         t.AmortBalAmt ,
                         t.CDCFeeOwedAmt ,
                         t.DbentrPrinBalAmt ,
                         t.DbentrIntDueAmt ,
                         t.EscrowBalAmt ,
                         t.EscrowDt ,
                         t.CSAFeeDueAmt ,
                         t.DueAmt ,
                         t.DueDt ,
                         t.RlseDt ,
                         t.AccelASOFDt ,
                         t.CSREP ,
                         t.CSTitl ,
                         t.CSPhnNmb ,
                         h.PymtNmb ,
                         h.LoanStatCd ,
                         h.PymtTyp ,
                         h.PostingDt ,
                         h.SBAFeeAmt ,
                         h.CSAFeeAmt ,
                         h.CDCFeeAmt ,
                         h.IntAmt ,
                         h.PrinAmt ,
                         h.UnallocAmt ,
                         h.BalAmt ,
                         h.LateFeeAmt ,
                         h.PrepayAmt ,
                         h.DueFromBorrAmt ,
                         CASE h.PymtTyp
                                            WHEN 'X' THEN NULL
                         ELSE h.IntAmt + h.PrinAmt + h.SBAFeeAmt + h.CSAFeeAmt + h.CDCFeeAmt + h.LateFeeAmt + h.UnallocAmt
                            END PymtAmt  ,
                         M.AccrIntAmt ,
                         v_trustee_name trustee_name  ,
                         v_aba_number ABANmb  ,
                         v_account_number AcctNmb  ,
                         v_account_name AcctNm  ,
                         v_wire_re WireRE  ,
                         v_recip_name recip_name  ,
                         v_recip_name_1 recip_name_1  ,
                         v_recip_salutation recip_salutation  ,
                         v_recip_contact recip_contact  ,
                         v_recip_street_1 recip_street_1  ,
                         v_recip_street_2 recip_street_2  ,
                         v_recip_state recip_state  ,
                         v_recip_zip recip_zip  ,
                         v_sender_name sender_name  ,
                         v_sender_street_1 sender_street_1  ,
                         v_sender_street_2 sender_street_2  ,
                         v_sender_state sender_state  ,
                         v_sender_zip sender_zip  ,
                         v_sender_city sender_city  ,
                         v_recip_city recip_city  ,
                         t.AccrIntASOFDt ,
                         v_trustee_finacial_street_1
                          trustee_street_1 ,-- Newly added for ALM 1048

                         v_trustee_finacial_street_2 trustee_street_2  ,
                         v_trustee_finacial_state trustee_state  ,
                         v_trustee_finacial_zip trustee_zip  ,
                         v_trustee_finacial_city trustee_city
           FROM cdconline.TTTransTbl t
                  LEFT JOIN cdconline.TTPymtHistTbl h   ON t.LoanNmb = h.LoanNmb
                  LEFT JOIN cdconline.TTMoTbl M   ON t.LoanNmb = M.LoanNmb
                  AND h.PostingDt = M.PostingDt
           ORDER BY t.SemiAnnDt,
                    t.LoanNmb,
                    h.PostingDt,
                    h.PymtNmb ;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  stgcsa.TTtranscriptrpt1; 
    dbms_output.put_line('$$I.1: After loading, %rowcnt: '||wrk0||'  stgcsa.TTtranscriptrpt1: '||wrk1);

      -- debenture recap report
      Insert into stgcsa.TTtranscriptrpt2
         SELECT SmllBusCons ,
                StmtNm ,
                CDCRegnCd ,
                CDCNmb ,
                CDCNm ,
                LoanNmb ,
                SemiAnnDt ,
                DueAmt ,
                AccelASOFDt ,
                CSREP ,
                CSTitl ,
                CSPhnNmb ,
                v_trustee_name trustee_name  ,
                v_aba_number ABANmb  ,
                v_account_number AcctNmb  ,
                v_account_name AcctNm  ,
                v_wire_re WireRE  ,
                v_recip_name recip_name  ,
                v_recip_name_1 recip_name_1  ,
                v_recip_salutation recip_salutation  ,
                v_recip_contact recip_contact  ,
                v_recip_street_1 recip_street_1  ,
                v_recip_street_2 recip_street_2  ,
                v_recip_state recip_state  ,
                v_recip_zip recip_zip  ,
                v_sender_name sender_name  ,
                v_sender_street_1 sender_street_1  ,
                v_sender_street_2 sender_street_2  ,
                v_sender_state sender_state  ,
                v_sender_zip sender_zip  ,
                v_sender_city sender_city  ,
                v_recip_city recip_city  ,
                RlseDt ,
                DueDt ,
                v_trustee_finacial_street_1
                 trustee_street_1 ,-- Newly added for ALM 1048

                v_trustee_finacial_street_2 trustee_street_2  ,
                v_trustee_finacial_state trustee_state  ,
                v_trustee_finacial_zip trustee_zip  ,
                v_trustee_finacial_city trustee_city
           FROM cdconline.TTTransTbl
           ORDER BY SemiAnnDt,
                    LoanNmb ;
    wrk0:=sql%rowcount; 
    select count(*) into wrk1 from  stgcsa.TTtranscriptrpt2; 
    dbms_output.put_line('$$I.1: After loading, %rowcnt: '||wrk0||'  stgcsa.TTtranscriptrpt2: '||wrk1);
    exception when others then
      p_errval:=sqlcode;
      p_errmsg:=pgm||' '||ver||': Oracle error occured while saving results into temp tables.'
        ||' SQLCODE='||sqlcode||' '||sqlerrm(sqlcode);     
      raise;      
    end;
  end if;
  if p_errval=0 then
      dbms_output.put_line('$$J.1: At end, No errors');
      stgcsa.runtime_logger(logged_msg_id,'STDLOG',101,p_errmsg,
      p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);    
  else
      dbms_output.put_line('$$J.2: At end, ERROR: '||P_ERRMSG);
    stgcsa.runtime_logger(logged_msg_id,'STDERR',103,p_errmsg,
      p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);    
  end if;                
END AcclTrnscrptGen2TBLCSPDBG;
/
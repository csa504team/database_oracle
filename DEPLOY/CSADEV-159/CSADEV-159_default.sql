define deploy_name=CSADEV-159
define package_name=default
define package_buildtime=20200717135712
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-159_default created on Fri 07/17/2020 13:57:13.02 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-159_default created on Fri 07/17/2020 13:57:13.02 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-159_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\CARESACTACHRPTTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CARESACTACHRPTSELTSP.sql 
Jasleenkaur Gorowada committed 28aeeaf on Mon Jul 6 09:35:36 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\CARESACTACHRPTTBL.sql"

CREATE TABLE CDCONLINE.CARESACTACHRPTTBL
(
  LOANNMB         CHAR(10 BYTE)                 NOT NULL,
  CDCREGNCD       CHAR(2 BYTE)                  NOT NULL,
  CDCNMB          CHAR(4 BYTE)                  NOT NULL,
  BORRNM          VARCHAR2(80 BYTE)             NOT NULL,
  SCHDDBTINDDT    VARCHAR2(3 BYTE)              NOT NULL,
  LOANSTATCD      VARCHAR2(80 BYTE)             NOT NULL,
  PYMTSTATCD      VARCHAR2(80 BYTE)             NOT NULL,
  SCHDTODBTDT     DATE,
  BNKNM           VARCHAR2(80 BYTE),
  BORRRESPYMTDT   DATE,
  ACHROUTINGNMB   CHAR(15 BYTE),
  ACHACCT         VARCHAR2(30 BYTE),
  ISCURRENT       CHAR(1 BYTE)                  NOT NULL,
  CREATUSERID     VARCHAR2(32 BYTE)             NOT NULL,
  CREATDT         DATE                          DEFAULT SYSDATE               NOT NULL,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             NOT NULL,
  LASTUPDTDT      DATE                          DEFAULT SYSDATE               NOT NULL
)
TABLESPACE CDCONLINEDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON CDCONLINE.CARESACTACHRPTTBL TO CDCONLINEGOVREAD;

GRANT SELECT ON CDCONLINE.CARESACTACHRPTTBL TO CDCONLINEPRTREAD;

GRANT SELECT ON CDCONLINE.CARESACTACHRPTTBL TO CDCONLINEREADALLROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CARESACTACHRPTSELTSP.sql"
 -- variable declaration
create or replace PROCEDURE           CDCONLINE.CARESACTACHRPTSELTSP
(
    p_Identifier IN number := 0,
	p_CDCRgnCD IN char default null,
	p_CDCNmb IN char default null,
	p_RetVal OUT number,
	p_ErrVal OUT number,
	p_ErrMsg OUT varchar2,
	p_SelCur OUT SYS_REFCURSOR
) AS
 /*
  Created on: 2020-06-16 11:18:45
  Created by: GENR
  Crerated from template stdtblread_template v1.2 20 Feb 2020 on 2020-06-16 11:18:46
    Using SNAP V4.1.1 9 Mar 2020, J. Low Binary Frond, Select Computing
*/
-- Main body begins here
BEGIN
    SAVEPOINT CARESACTACHRPTSELTSP;

-- CDC's will see all records	
IF p_Identifier = 0
		THEN
		/* select from CARESACTACHRPTTBL Table */
	BEGIN
		OPEN p_SelCur FOR
			SELECT    
				LOANNMB,
				CDCREGNCD,
				CDCNMB,
				BORRNM,
				SCHDDBTINDDT,
				LOANSTATCD,
				PYMTSTATCD,
				SCHDTODBTDT,
				BNKNM,
				BORRRESPYMTDT,
				ACHROUTINGNMB,
				ACHACCT,
				ISCURRENT,
				CREATUSERID,
				CREATDT,
				LASTUPDTUSERID,
				LASTUPDTDT
			FROM CDCONLINE.CARESACTACHRPTTBL
			;
			
		p_RetVal := SQL%ROWCOUNT;
		p_ErrVal := SQLCODE;
		p_ErrMsg := SQLERRM;
	 END;

-- CDC's will see their own records
ELSIF p_Identifier = 1
		THEN
		/* select from CARESACTACHRPTTBL Table */
	BEGIN
		 OPEN p_SelCur FOR
			SELECT    
				LOANNMB,
				CDCREGNCD,
				CDCNMB,
				BORRNM,
				SCHDDBTINDDT,
				LOANSTATCD,
				PYMTSTATCD,
				SCHDTODBTDT,
				BNKNM,
				BORRRESPYMTDT,
				ACHROUTINGNMB,
				ACHACCT,
				ISCURRENT,
				CREATUSERID,
				CREATDT,
				LASTUPDTUSERID,
				LASTUPDTDT
			FROM CDCONLINE.CARESACTACHRPTTBL
			WHERE TRIM(CDCREGNCD) = TRIM(p_CDCRgnCD)
			AND TRIM(CDCNMB) = TRIM(p_CDCNmb)
			;
		
		p_RetVal := SQL%ROWCOUNT;
		p_ErrVal := SQLCODE;
		p_ErrMsg := SQLERRM;
	 END;
	 
END IF;
		EXCEPTION
		WHEN OTHERS THEN
				BEGIN
					p_RetVal := 0;
					p_ErrVal := SQLCODE;
					p_ErrMsg := SQLERRM;
					
				ROLLBACK TO CARESACTACHRPTSELTSP;
				RAISE;
			END;
		END;
-- CARESACTACHRPTSELTSP
/
GRANT EXECUTE ON CDCONLINE.CARESACTACHRPTSELTSP TO CDCONLINECORPGOVPRTUPDATE;

GRANT EXECUTE ON CDCONLINE.CARESACTACHRPTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON CDCONLINE.CARESACTACHRPTSELTSP TO CSAUPDTROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

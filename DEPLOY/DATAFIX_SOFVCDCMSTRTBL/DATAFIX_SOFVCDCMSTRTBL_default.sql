define deploy_name=DATAFIX_SOFVCDCMSTRTBL
define package_name=default
define package_buildtime=20200311143708
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy DATAFIX_SOFVCDCMSTRTBL_default created on Wed 03/11/2020 14:37:10.38 by Jasleen Gorowada
prompt deploy scripts for deploy DATAFIX_SOFVCDCMSTRTBL_default created on Wed 03/11/2020 14:37:10.40 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy DATAFIX_SOFVCDCMSTRTBL_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\datafix_SOFVCDCMSTRTBL.sql 
Jasleen0605 committed 066749c on Wed Mar 11 14:28:51 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\datafix_SOFVCDCMSTRTBL.sql"
-- verify online DB is active
ON SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;

---resultset before data fix
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where cdcsetupdt=to_date('01011900','ddmmyyyy');


--- data fix steps
update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=TO_DATE('3/4/2020', 'MM/DD/YYYY'),
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='09' and CDCCERTNMB='713S';
                             
update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=CREATDT,
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='05' and CDCCERTNMB='038';
                             

update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=CREATDT,
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='03'and CDCCERTNMB='718';
commit;
                             
--resultset after data fix
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where cdcsetupdt=to_date('01011900','ddmmyyyy');
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='09' and CDCCERTNMB ='713S';
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='05' and CDCCERTNMB ='038';
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='03' and CDCCERTNMB ='718';


set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

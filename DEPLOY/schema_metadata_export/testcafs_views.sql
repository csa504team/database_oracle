create or replace view TESTCAFS_objects as 
  select * from DBA_OBJECTS 
    where owner in ('CSA','STGCSA','CDCONLINE','LOAN');
create or replace view TESTCAFS_tables as 
  select * from DBA_tables 
    where owner in ('CSA','STGCSA','CDCONLINE');    
create or replace view TESTCAFS_tab_columns as 
  select OWNER,                                                                  
        TABLE_NAME,                                                             
        COLUMN_NAME,                                                            
        DATA_TYPE,                                                              
        DATA_TYPE_MOD,                                                          
        DATA_TYPE_OWNER,                                                        
        DATA_LENGTH,                                                            
        DATA_PRECISION,                                                         
        DATA_SCALE,                                                             
        NULLABLE,                                                               
        COLUMN_ID,                                                              
        DEFAULT_LENGTH,                                                         
        NUM_DISTINCT,                                                           
        LOW_VALUE,                                                              
        HIGH_VALUE,                                                             
        DENSITY,                                                                
        NUM_NULLS,                                                              
        NUM_BUCKETS,                                                            
        LAST_ANALYZED,                                                          
        SAMPLE_SIZE,                                                            
        CHARACTER_SET_NAME,                                                     
        CHAR_COL_DECL_LENGTH,                                                   
        GLOBAL_STATS,                                                           
        USER_STATS,                                                             
        AVG_COL_LEN,                                                            
        CHAR_LENGTH,                                                            
        CHAR_USED,                                                              
        V80_FMT_IMAGE,                                                          
        DATA_UPGRADED,                                                          
        HISTOGRAM,                                                              
        DEFAULT_ON_NULL,                                                        
        IDENTITY_COLUMN,                                                        
        SENSITIVE_COLUMN,                                                       
        EVALUATION_EDITION,                                                     
        UNUSABLE_BEFORE,                                                        
        UNUSABLE_BEGINNING,                                                     
        COLLATION from DBA_tab_columns
    where owner in ('CSA','STGCSA','CDCONLINE','LOAN');    
create or replace view TESTCAFS_indexes as 
  select * from DBA_indexes 
    where table_owner in ('CSA','STGCSA','CDCONLINE');    
create or replace view TESTCAFS_ind_columns as 
  select * from DBA_ind_columns 
    where table_owner in ('CSA','STGCSA','CDCONLINE');    
create or replace view TESTCAFS_constraints as 
  select OWNER,                                                                  
        CONSTRAINT_NAME,                                                        
        CONSTRAINT_TYPE,                                                        
        TABLE_NAME,                                                             
        SEARCH_CONDITION_VC,                                                    
        R_OWNER,                                                                
        R_CONSTRAINT_NAME,                                                      
        DELETE_RULE,                                                            
        STATUS,                                                                 
        DEFERRABLE,                                                             
        DEFERRED,                                                               
        VALIDATED,                                                              
        GENERATED,                                                              
        BAD,                                                                    
        RELY,                                                                   
        LAST_CHANGE,                                                            
        INDEX_OWNER,                                                            
        INDEX_NAME,                                                             
        INVALID,                                                                
        VIEW_RELATED,                                                           
        ORIGIN_CON_ID from DBA_constraints 
    where owner in ('CSA','STGCSA','CDCONLINE');
create or replace view TESTCAFS_cons_columns as 
  select * from DBA_cons_columns 
    where owner in ('CSA','STGCSA','CDCONLINE');
create or replace view TESTCAFS_triggers as 
  select OWNER,                                                                  
        TRIGGER_NAME,                                                           
        TRIGGER_TYPE,                                                           
        TRIGGERING_EVENT,                                                       
        TABLE_OWNER,                                                            
        BASE_OBJECT_TYPE,                                                       
        TABLE_NAME,                                                             
        COLUMN_NAME,                                                            
        REFERENCING_NAMES,                                                      
        WHEN_CLAUSE,                                                            
        STATUS,                                                                 
        DESCRIPTION,                                                            
        ACTION_TYPE,                                                            
        CROSSEDITION,                                                           
        BEFORE_STATEMENT,                                                       
        BEFORE_ROW,                                                             
        AFTER_ROW,                                                              
        AFTER_STATEMENT,                                                        
        INSTEAD_OF_ROW,                                                         
        FIRE_ONCE,                                                              
        APPLY_SERVER_ONLY from DBA_triggers 
    where owner in ('CSA','STGCSA','CDCONLINE');
create or replace view TESTCAFS_views as 
  select OWNER,                                                                  
        VIEW_NAME,                                                              
        TEXT_LENGTH,                                                            
        TEXT_VC,                                                                
        TYPE_TEXT_LENGTH,                                                       
        TYPE_TEXT,                                                              
        OID_TEXT_LENGTH,                                                        
        OID_TEXT,                                                               
        VIEW_TYPE_OWNER,                                                        
        VIEW_TYPE,                                                              
        SUPERVIEW_NAME,                                                         
        EDITIONING_VIEW,                                                        
        READ_ONLY,                                                              
        CONTAINER_DATA,                                                         
        BEQUEATH,                                                               
        ORIGIN_CON_ID,                                                          
        DEFAULT_COLLATION,                                                      
        CONTAINERS_DEFAULT,                                                     
        CONTAINER_MAP,                                                          
        EXTENDED_DATA_LINK,                                                     
        EXTENDED_DATA_LINK_MAP from DBA_views 
    where owner in ('CSA','STGCSA','CDCONLINE');
create or replace view TESTCAFS_source as 
  select * from DBA_source 
    where owner in ('CSA','STGCSA','CDCONLINE');
    

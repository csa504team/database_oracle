define deploy_name=DebtRefinance
define package_name=Enhancement_0707
define package_buildtime=20210707105718
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy DebtRefinance_Enhancement_0707 created on Wed 07/07/2021 10:57:19.76 by Jasleen Gorowada
prompt deploy scripts for deploy DebtRefinance_Enhancement_0707 created on Wed 07/07/2021 10:57:19.76 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy DebtRefinance_Enhancement_0707: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\MISC\insert_DOCTYPBUSPRCSMTHDTBL.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\MISC\insert_DOCTYPBUSPRCSMTHDTBL.sql"
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1096,2,'5RX', to_date('06-15-17','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1016,2,'5RX', to_date('05-01-17','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1023,2,'5RX', to_date('05-01-17','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1001,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1002,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1003,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1004,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1005,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1006,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1007,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1008,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1009,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1010,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1011,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1012,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1013,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1014,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1015,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1018,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1019,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1020,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1021,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1022,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1024,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1025,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1026,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1027,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1028,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1029,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1030,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1031,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1032,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1033,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1034,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1035,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1036,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1037,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1038,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1039,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1040,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1041,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1042,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1043,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1044,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1045,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1046,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1047,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1048,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1049,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1050,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1051,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1052,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1053,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1054,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1055,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1056,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1057,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1058,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1059,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1060,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1061,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1062,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1063,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1064,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1065,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1066,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1067,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1068,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1069,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1070,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1071,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1072,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1073,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1074,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1075,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1076,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1077,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1078,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1079,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1080,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1081,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1082,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1083,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1084,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1085,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1086,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1087,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1088,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1089,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1090,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1091,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1092,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1093,2,'5RX', to_date('09-30-16','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1101,2,'5RX', to_date('08-21-17','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1196,2,'5RX', to_date('04-08-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1000,2,'5RX', to_date('04-01-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1197,2,'5RX', to_date('05-01-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1198,2,'5RX', to_date('05-01-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1199,2,'5RX', to_date('05-01-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1094,2,'5RX', to_date('05-01-19','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1162,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1161,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1160,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1159,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1158,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1157,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1156,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1155,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1154,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1153,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1152,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1151,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1150,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1149,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1148,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1147,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1146,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1145,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1144,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1143,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1142,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1141,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1140,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1139,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1138,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1137,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1136,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1135,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1134,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1133,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1132,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1131,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1130,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1164,2,'5RX', to_date('01-01-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1194,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1193,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1192,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1191,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1190,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1172,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1171,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1170,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1189,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1188,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1187,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1186,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1185,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1184,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1183,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1182,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1181,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1180,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1179,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1173,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1194,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1193,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1192,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1191,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1190,13,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1172,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1171,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1170,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1169,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1168,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1167,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1166,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1165,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1189,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1188,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1187,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1186,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1185,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1184,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1183,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1182,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1181,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1180,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1179,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1173,2,'5RX', to_date('10-15-18','mm-dd-yy'),null,user,sysdate,'O','Y');                          
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1101,13,'5RX', to_date('03-22-20','mm-dd-yy'),null,user,sysdate,'O','Y');                         
                                                                                                    
                                                                                                    
insert into SBAREF.DOCTYPBUSPRCSMTHDTBL(DOCTYPCD,BUSPRCSTYPCD,PRCSMTHDCD,DOCTYPBUSPRCSMTHDSTRTDT,   
DOCTYPBUSPRCSMTHDENDDT,CREATUSERID,CREATDT,DOCTYPVALIDTYPCD,ALLOWMULTIDOCIND)
values
(1102,13,'5RX', to_date('03-22-20','mm-dd-yy'),null,user,sysdate,'O','Y');
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=FTAWiki
define package_name=FTAWiki_0721
define package_buildtime=20210721 95935
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FTAWiki_FTAWiki_0721 created on Wed 07/21/2021  9:59:35.88 by Jasleen Gorowada
prompt deploy scripts for deploy FTAWiki_FTAWiki_0721 created on Wed 07/21/2021  9:59:35.88 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FTAWiki_FTAWiki_0721: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADUPDTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADSELTSP.sql 
Jasleen Gorowada committed 713c487 on Tue Jun 29 16:53:03 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLDSELTSP.sql 
Jasleen Gorowada committed 713c487 on Tue Jun 29 16:53:03 2021 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\DOWNLOADSRESOURCESUPDTSP.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADUPDTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLOADUPDTSP
(
p_Identifier number := 0,
p_RetVal out number,
p_PGID  number:=0,
p_PGCATID number:=0,
p_DOCDATA blob :=null,
p_DOCID  number:=0
)
as
begin
    savepoint FTAWIKIDOCFILEUPLOADUPDTSP;
    if p_Identifier = 0 then
    begin
        update FTAWIKI.FTAWIKIDOCFILEUPLOADTBL set
        PGID = p_PGID,
        PGCATID = p_PGCATID,
        DOCDATA = UTL_COMPRESS.lz_compress(p_DOCData),
        LASTUPDUSERID = user,
        LASTUPDDT = sysdate
        where DocId = p_DocId;        
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
exception when others then
begin
    rollback to FTAWIKIDOCFILEUPLOADUPDTSP;
    raise;
end;
end;
/


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADSELTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLOADSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_PGID number := null,
p_PGCATID number := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select  DOCID,
             UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            CREATUSERID,
            CREATDT,
            PGID,
            PGCATID,
            DOCNM,
            LASTUPDUSERID,
            LASTUPDDT    
            from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
            where    CREATDT in(select max (CREATDT) from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
        where PGID = p_PGID and PGCATID = p_PGCATID );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
/


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLDSELTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLDSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCNM varchar2 := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select  DOCID,
            UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            CREATUSERID,
            CREATDT,
            PGID,
            PGCATID,
            DOCNM,
            LASTUPDUSERID,
            LASTUPDDT    
            from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
            where    CREATDT in(select max (CREATDT) from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
        where DOCNM = p_DOCNM );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
/


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\DOWNLOADSRESOURCESUPDTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.DOWNLOADSRESOURCESUPDTSP (
    p_IDENTIFIER           NUMBER := 0,
    p_RETVAL               OUT NUMBER,
p_DOWNLOADSRESOURCESID            NUMBER,
p_DOWNLOADSRESOURCESCATID         NUMBER,
p_DOWNLOADSRESOURCESCATSUBID      NUMBER,
p_DOWNLOADSRESOURCESDATE          DATE,
p_DOWNLOADSRESOURCESTITLE         VARCHAR2,
p_DOWNLOADSRESOURCESCONTENT       VARCHAR2,
p_DOWNLOADSRESOURCESFILENAME      VARCHAR2,
p_DOWNLOADSRESOURCESFILELOCATION  VARCHAR2,
p_CREATDT                     DATE,
p_CREATUSERID                       VARCHAR2,
p_LASTUPDTDT                       VARCHAR2,
p_LASTUPDTUSERID               DATE,
p_ATTACHMENTS BLOB)
AS
BEGIN	
    SAVEPOINT DOWNLOADSRESOURCESUPDTSP;

    IF p_IDENTIFIER = 0
    THEN
    BEGIN
        UPDATE FTAWIKI.DOWNLOADSRESOURCESTBL
SET    
       DOWNLOADSRESOURCESCATID        = p_DOWNLOADSRESOURCESCATID,
       DOWNLOADSRESOURCESCATSUBID     = p_DOWNLOADSRESOURCESCATSUBID,
       DOWNLOADSRESOURCESDATE         = p_DOWNLOADSRESOURCESDATE,
       DOWNLOADSRESOURCESTITLE        = p_DOWNLOADSRESOURCESTITLE,
       DOWNLOADSRESOURCESCONTENT      = p_DOWNLOADSRESOURCESCONTENT,
       DOWNLOADSRESOURCESFILENAME     = nvl(p_DOWNLOADSRESOURCESFILENAME,DOWNLOADSRESOURCESFILENAME),
       DOWNLOADSRESOURCESFILELOCATION = p_DOWNLOADSRESOURCESFILELOCATION,
       CREATDT                    = p_CREATDT,
       CREATUSERID                      = p_CREATUSERID,
       LASTUPDTDT                      = p_LASTUPDTDT,
       LASTUPDTUSERID              = p_LASTUPDTUSERID,
       ATTACHMENTS               = nvl(p_ATTACHMENTS,ATTACHMENTS)
WHERE  DOWNLOADSRESOURCESID           = p_DOWNLOADSRESOURCESID;


            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO DOWNLOADSRESOURCESUPDTSP;
        END;
END DOWNLOADSRESOURCESUPDTSP;
/

commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=FTAWiki
define package_name=FTAWiki_0604
define package_buildtime=20210604123629
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FTAWiki_FTAWiki_0604 created on Fri 06/04/2021 12:36:30.99 by Jasleen Gorowada
prompt deploy scripts for deploy FTAWiki_FTAWiki_0604 created on Fri 06/04/2021 12:36:30.99 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FTAWiki_FTAWiki_0604: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\DOWNLOADSRESOURCESCATSUBSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADSELTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\NEWSSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\NEWSUPDTSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\PROGRAMINFORMATIONSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\MISC\FTAWIKI_grants_0604.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\DOWNLOADSRESOURCESCATSUBSELCSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP
(
  p_ISACTIVE IN NUMBER,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		 SELECT 
        dr.downloadsResourcesID,
        dr.downloadsResourcesCatID,
        dr.downloadsResourcesDate,
        dr.downloadsResourcesTitle,
        dr.downloadsResourcesContent,
        drc.downloadsResourcesCatDesc,
        dr.downloadsResourcesFileName,
        dr.downloadsResourcesFileLocation,
        drcs.downloadsResourcesCatSubDesc  ,
        dr.attachments,
       drcs.downloadsResourcesCatSubID  
        from FTAWIKI.DOWNLOADSRESOURCESTBL dr
      left join FTAWIKI.DOWNLOADSRESOURCESCATTBL drc on dr.downloadsResourcesCatID = drc.downloadsResourcesCatID
      left join FTAWIKI.DOWNLOADSRESOURCESCATSUBTBL drcs on dr.downloadsResourcesCatSubID = drcs.downloadsResourcesCatSubID
      where dr.isactive = p_ISACTIVE ORDER BY dr.downloadsResourcesDate DESC;

               

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/


GRANT EXECUTE ON FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP TO FTAPORTALWIKIMANAGERROLE;

GRANT EXECUTE ON FTAWIKI.DOWNLOADSRESOURCESCATSUBSELCSP TO FTAWIKIREADALLROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\FTAWIKIDOCFILEUPLOADSELTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.FTAWIKIDOCFILEUPLOADSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_PGID number := null,
p_PGCATID number := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select  DOCID,
            UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            CREATUSERID,
            CREATDT,
            PGID,
            PGCATID,
            DOCNM,
            LASTUPDUSERID,
            LASTUPDDT    
            from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
            where    CREATDT in(select max (CREATDT) from FTAWIKI.FTAWIKIDOCFILEUPLOADTBL 
        where PGID = p_PGID and PGCATID = p_PGCATID );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
/


GRANT EXECUTE ON FTAWIKI.FTAWIKIDOCFILEUPLOADSELTSP TO FTAPORTALWIKIMANAGERROLE;

GRANT EXECUTE ON FTAWIKI.FTAWIKIDOCFILEUPLOADSELTSP TO FTAWIKIREADALLROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\NEWSSELCSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.NEWSSELCSP
(
  p_ISACTIVE IN NUMBER,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
       SELECT 
NEWSID, NEWSDATE, NEWSTITLE, 
   NEWSCONTENT, NEWSFILENAME, NEWSFILELOCATION, 
   DISPLAYONTOP, CREATDT, CREATUSERID, 
   LASTUPDTUSERID, LASTUPDTDT, ISACTIVE, ATTACHMENTS
FROM FTAWIKI.NEWSTBL where ISACTIVE = p_ISACTIVE order by NEWSDATE desc;
               

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/



GRANT EXECUTE ON FTAWIKI.NEWSSELCSP TO FTAPORTALWIKIMANAGERROLE;

GRANT EXECUTE ON FTAWIKI.NEWSSELCSP TO FTAWIKIREADALLROLE;


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\NEWSUPDTSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.NEWSUPDTSP
(  p_IDENTIFIER           NUMBER := 0,
  p_RETVAL           OUT NUMBER,
  p_NEWSID             NUMBER,
  p_NEWSDATE           DATE,
  p_NEWSCONTENT        VARCHAR2,
  p_NEWSTITLE          VARCHAR2,
  p_NEWSFILENAME       VARCHAR2,
  p_CREATDT        DATE,
  p_CREATUSERID          NUMBER,
  p_LASTUPDTUSERID          NUMBER,
  p_LASTUPDTDT  DATE,
  p_ATTACHMENTS BLOB
)
AS

BEGIN
    SAVEPOINT NEWSUPDTSP;

    IF p_IDENTIFIER = 0
    THEN
        BEGIN
        UPDATE FTAWIKI.NEWSTBL
SET    NEWSDATE          = p_NEWSDATE,
       NEWSCONTENT       = p_NEWSCONTENT,
       NEWSTITLE         = p_NEWSTITLE,
       NEWSFILENAME      =p_NEWSFILENAME,
       CREATDT       = p_CREATDT,
       CREATUSERID         = p_CREATUSERID,
       LASTUPDTUSERID         = p_LASTUPDTUSERID,
       LASTUPDTDT = p_LASTUPDTDT,
       ATTACHMENTS = p_ATTACHMENTS 
WHERE  NEWSID            = p_NEWSID
;
			
            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO NEWSUPDTSP;
        END;
END NEWSUPDTSP;
/


GRANT EXECUTE ON FTAWIKI.NEWSUPDTSP TO FTAWIKIREADALLROLE;

GRANT EXECUTE ON FTAWIKI.NEWSUPDTSP TO FTAPORTALWIKIMANAGERROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Procedures\PROGRAMINFORMATIONSELCSP.sql"
CREATE OR REPLACE PROCEDURE FTAWIKI.PROGRAMINFORMATIONSELCSP
(
  p_ISACTIVE IN NUMBER,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
       SELECT 
PROGRAMINFORMATIONID, PROGRAMINFORMATIONDATE, PROGRAMINFORMATIONTITLE, 
   PROGRAMINFORMATIONCONTENT, DISPLAYONTOP, ISACTIVE, 
   CREATDT, CREATUSERID, LASTUPDTUSERID, 
   LASTUPDTDT
FROM FTAWIKI.PROGRAMINFORMATIONTBL where ISACTIVE = p_ISACTIVE order by PROGRAMINFORMATIONDATE desc;
               

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/



GRANT EXECUTE ON FTAWIKI.PROGRAMINFORMATIONSELCSP TO FTAPORTALWIKIMANAGERROLE;

GRANT EXECUTE ON FTAWIKI.PROGRAMINFORMATIONSELCSP TO FTAWIKIREADALLROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\FTAWIKI_grants_0604.sql"
GRANT FTAPORTALWIKIMANAGERROLE TO FTAPORTALINTERNALWIKIMGR;
GRANT FTAWIKIREADALLROLE TO FTAPORTALINTERNALWIKIMGR;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

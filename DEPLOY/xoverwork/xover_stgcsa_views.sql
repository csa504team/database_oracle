-- crossover export views for app tables (stgcsa side)
--    View only get changes since the last crossover import on this DB 

create or replace view stgcsa.XOVER_SOFVPYMTTBL_view as                   
  select * from stgcsa.SOFVPYMTTBL
     WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);   
grant select on stgcsa.XOVER_SOFVPYMTTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVCDCMSTRTBL_view as                
  select * from stgcsa.SOFVCDCMSTRTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);               
grant select on stgcsa.XOVER_SOFVCDCMSTRTBL_view to stgcsadevrole;              
                                                                                
create or replace view stgcsa.XOVER_SOFVAUDTTBL_view as                   
  select * from stgcsa.SOFVAUDTTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);
grant select on stgcsa.XOVER_SOFVAUDTTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVLND2TBL_view as                   
  select * from stgcsa.SOFVLND2TBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVLND2TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVRTSCTBL_view as                   
  select * from stgcsa.SOFVRTSCTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                 
grant select on stgcsa.XOVER_SOFVRTSCTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVCTCHTBL_view as                   
  select * from stgcsa.SOFVCTCHTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                  
grant select on stgcsa.XOVER_SOFVCTCHTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVFTPOTBL_view as                   
  select * from stgcsa.SOFVFTPOTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVFTPOTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVGNTYTBL_view as                   
  select * from stgcsa.SOFVGNTYTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVGNTYTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVGRGCTBL_view as                   
  select * from stgcsa.SOFVGRGCTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);             
grant select on stgcsa.XOVER_SOFVGRGCTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVLND1TBL_view as                   
  select * from stgcsa.SOFVLND1TBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVLND1TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBGDFTBL_view as                   
  select * from stgcsa.SOFVBGDFTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVBGDFTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVDUEBTBL_view as                   
  select * from stgcsa.SOFVDUEBTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVDUEBTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVDFPYTBL_view as                   
  select * from stgcsa.SOFVDFPYTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                 
grant select on stgcsa.XOVER_SOFVDFPYTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBFFATBL_view as                   
  select * from stgcsa.SOFVBFFATBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                 
grant select on stgcsa.XOVER_SOFVBFFATBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBGW9TBL_view as                   
  select * from stgcsa.SOFVBGW9TBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVBGW9TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVOTRNTBL_view as                   
  select * from stgcsa.SOFVOTRNTBL WHERE lastupdtdt>=
      (select STGCSA_MERGE_END_DT from stgcsa.xover_status);                   
grant select on stgcsa.XOVER_SOFVOTRNTBL_view to stgcsadevrole;  
                                                                                

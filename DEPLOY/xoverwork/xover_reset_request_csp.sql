CREATE OR REPLACE procedure STGCSA.xover_reset_request_csp
   (p_retval out number,
    p_errval out number,
    p_errmsg out varchar2,
    p_identifier number:=0,
	p_what_to_do varchar2:='curr_startdate',      
    p_userid varchar2,
    p_resetdt date:=null
	) as
  prog varchar2(40):='XOVER_RESET_REQUEST_CSP';
  ver  varchar2(40):='V1.5.3 18 Mar 2019, JG';
  -- V1.5 JL 11 Feb - first version
  -- V1.5 JL 11 Feb - 
  reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  msgtxt varchar2(4000);
  statusrec stgcsa.xover_status%rowtype;
begin
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  if (lower(p_what_to_do)='full_reset' or lower(p_what_to_do)='curr_startdate' or lower(p_what_to_do)='as_of_now') then
    select * into statusrec from stgcsa.xover_status;
    msgtxt:='Reset request started';
    runtime.logger(logentryid,'STDXVR',235,msgtxt,user,4,
    logdt1=>p_resetdt,
    program_name=>prog||' '||ver,force_log_entry=>true);
	if lower(p_what_to_do)='full_reset' then
		-- STGCSA merge end date is set to reset_date i.e. 01/01/1900
		update stgcsa.xover_status set xover_last_event='SM',
		stgcsa_merge_end_dt=reset_date,err_step=null,err_dt=null,err_msg=null;
	elsif lower(p_what_to_do)='curr_startdate' then
		-- STGCSA merge end date is set to today's date i.e. sysdate. If p_what_to_do is not explicitly stated, this is also the default value.
		update stgcsa.xover_status set xover_last_event='SM',
		stgcsa_merge_end_dt=sysdate,err_step=null,err_dt=null,err_msg=null;
	elsif lower(p_what_to_do)='as_of_now' then
		--when p_what_to_do is set to 'as_of_now', data is to be resynchronized manually between the two DBs hence STGCSA merge end date is not updated.
		update stgcsa.xover_status set xover_last_event='SM',
		err_step=null,err_dt=null,err_msg=null;
	end if;
    delete from STGCSA.JOBQUETBL;
    delete from STGCSA.MFJOBDETAILTBL;
    delete from STGCSA.MFJOBTBL;
    commit;
    if lower(p_what_to_do)='full_reset' then
		MSGTXT:='XOVER RESET REQUEST RECORDED (STGCSA_MERGE_END_DT set to "1900".) '
		||'Tables MFJOBTBL, MFJOBDETAILTBL, JOBQUETBL emptied.  Normal Completion';
		runtime.logger(logentryid,'STDXVR',236,MSGTXT,p_userid,5,
		program_name=>prog||' '||ver);
		--dbms_output.put_line(MSGTXT);
    elsif lower(p_what_to_do)='curr_startdate' then
        MSGTXT:='XOVER RESET REQUEST RECORDED (STGCSA_MERGE_END_DT changed from "'||statusrec.STGCSA_MERGE_END_DT||' to '||sysdate||'".) '
		||'Tables MFJOBTBL, MFJOBDETAILTBL, JOBQUETBL emptied.  Normal Completion';
		runtime.logger(logentryid,'STDXVR',236,MSGTXT,p_userid,5,
		program_name=>prog||' '||ver);
		--dbms_output.put_line(MSGTXT);
    elsif lower(p_what_to_do)='as_of_now' then
		MSGTXT:='XOVER RESET REQUEST RECORDED (STGCSA_MERGE_END_DT remains "'||statusrec.STGCSA_MERGE_END_DT||'".) '
		||'Tables MFJOBTBL, MFJOBDETAILTBL, JOBQUETBL emptied.  Normal Completion';
		runtime.logger(logentryid,'STDXVR',236,MSGTXT,p_userid,5,
		program_name=>prog||' '||ver);
		--dbms_output.put_line(MSGTXT);
    end if;
   else
    --dbms_output.put_line(p_what_to_do);
    p_errmsg:= 'Unexpected value entered for p_what_to_do. 
    It can only take values from full_reset,curr_startdate, as_of_now, FULL_RESET, CURR_STARTDATE or AS_OF_NOW';
    runtime.logger(logentryid,'STDERR',104,p_errmsg,user,3,
    logdt1=>p_resetdt,
    program_name=>prog||' '||ver,force_log_entry=>true);
    --dbms_output.put_line(p_errmsg); 
   end if;
end;
/
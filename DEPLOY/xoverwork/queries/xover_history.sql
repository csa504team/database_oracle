-- xover_timings v1.1 13 Mar 2019, J. Gorowada, J. Low
-- configure connect information in xover_status_datafetch!
--
@@xover_status_datafetch
define history_period=7
SET SERVEROUT ON FORMAT WRAPPED
set termout on 
set heading on
set pagesize 100
column Duration format 9999.99
column outbound_time format 9999.99
column BATCH_time format 9999.99
column inbound_time format 9999.99
column xover_start format a17
set heading off
set pagesize 0
select '                                                                   ',
  case 
    when '&&xover_last_event'='SM' then
      '@&&now CROSSOVER is not running, Online DB open since &&x_done_fdt'  
    else  
      '@&&now CROSSOVER is running since &&x_init_fdt'  
  end,
  'Recent completed Crossovers (past &&history_period days):                          '
from dual;    
set heading on  
set pagesize 1000
with
mergetask as
  (select csa_sid mt_csa_sid, creatdt mt_sm, logentryid mt_logentryid      
    from stgcsa.coreactvtylogtbl where creatdt>(sysdate-&&history_period)
      and msg_id in (221) order by logentryid),
mt_xtimes0 as
  (select mt_csa_sid, MT_SM , creatdt M,  --  Time of merge (start) from this start log rec
    to_date(substr(actvtydtls,instr(actvtydtls,'SI:')+3,14),'mm-dd hh24:mi:ss') SI,
    creatdt,
   to_date(substr(actvtydtls,instr(actvtydtls,'SX:')+3,14),'mm-dd hh24:mi:ss') SX,
    to_date(substr(actvtydtls,instr(actvtydtls,'SM:')+3,14),'mm-dd hh24:mi:ss') SM,
    to_date(substr(actvtydtls,instr(actvtydtls,'CI:')+3,14),'mm-dd hh24:mi:ss') CI,
    to_date(substr(actvtydtls,instr(actvtydtls,'CX:')+3,14),'mm-dd hh24:mi:ss') CX,
    to_date(substr(actvtydtls,instr(actvtydtls,'CM:')+3,14),'mm-dd hh24:mi:ss') CM,
    substr(actvtydtls,instr(actvtydtls,'SI:')+3,14) SI_STRING
    from stgcsa.coreactvtylogtbl, mergetask
    where csa_sid=mt_csa_sid and msg_id=228),
mt_xtimes as
  (select mt_csa_sid, si, (mt_sm-si)*1440 x_dur, sx, (sx-si)*1440 sx_dur,
    cm, (cm-sx)*1440 cm_dur,
    (cm-si)*1440 out_dur,
    ci, (ci-cm)*1440 csa_dur,
    cx, (cx-ci)*1440 cx_dur,
    M, (m-cx)*1440 imp_dur,      
    MT_SM SM, (MT_SM-M)*1440 SM_DUR,
    (MT_SM-CI)*1440 In_dur,
    mt_xtimes0.creatdt creatdt,
    si_string
    from stgcsa.coreactvtylogtbl, mt_xtimes0
    where csa_sid=mt_csa_sid and msg_id=228)    
select si_string XOVER_START, 
  X_DUR Duration  , OUT_DUR OUTBOUND_TIME,
  csa_dur Batch_TIME, in_dur INBOUND_TIME
  from mt_xtimes where x_dur<1000 order by 1;

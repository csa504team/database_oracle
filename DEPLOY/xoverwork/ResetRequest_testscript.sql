-- Testscript for what_to_do field.
--It can only take values from full_reset,curr_startdate, as_of_now, FULL_RESET, CURR_STARTDATE or AS_OF_NOW'. 
-- Any other values entered for this field will throw an error.
set serveroutput on;
declare
    p_retval number;
    p_errval number;
    p_errmsg varchar2(4000);
begin
STGCSA.xover_reset_request_csp(p_retval,p_errval,p_errmsg,p_identifier=>0,p_what_to_do=>'as_of_now',p_userid=>'TESTER',p_resetdt=>null);
end;


select * from stgcsa.xover_status;

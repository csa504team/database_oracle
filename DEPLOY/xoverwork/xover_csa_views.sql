-- crossover export views for app tables (csa.side)
--    View only get changes since the last crossover import on this DB 

create or replace view csa.XOVER_SOFVPYMTTBL_view as                   
  select * from csa.SOFVPYMTTBL
     WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);   
                                                                                
create or replace view csa.XOVER_SOFVCDCMSTRTBL_view as                
  select * from csa.SOFVCDCMSTRTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);               
                                                                                
create or replace view csa.XOVER_SOFVLND2TBL_view as                   
  select * from csa.SOFVLND2TBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVRTSCTBL_view as                   
  select * from csa.SOFVRTSCTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                 
                                                                                
create or replace view csa.XOVER_SOFVCTCHTBL_view as                   
  select * from csa.SOFVCTCHTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                  
                                                                                
create or replace view csa.XOVER_SOFVGNTYTBL_view as                   
  select * from csa.SOFVGNTYTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVGRGCTBL_view as                   
  select * from csa.SOFVGRGCTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);             
                                                                                
create or replace view csa.XOVER_SOFVLND1TBL_view as                   
  select * from csa.SOFVLND1TBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVBGDFTBL_view as                   
  select * from csa.SOFVBGDFTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVDUEBTBL_view as                   
  select * from csa.SOFVDUEBTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVDFPYTBL_view as                   
  select * from csa.SOFVDFPYTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                 
                                                                                
create or replace view csa.XOVER_SOFVBFFATBL_view as                   
  select * from csa.SOFVBFFATBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                 
                                                                                
create or replace view csa.XOVER_SOFVBGW9TBL_view as                   
  select * from csa.SOFVBGW9TBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVOTRNTBL_view as                   
  select * from csa.SOFVOTRNTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   
                                                                                
create or replace view csa.XOVER_SOFVPROPTBL_view as                   
  select * from csa.SOFVPROPTBL WHERE lastupdtdt>=
      (select CSA_MERGE_END_DT from csa.xover_status);                   

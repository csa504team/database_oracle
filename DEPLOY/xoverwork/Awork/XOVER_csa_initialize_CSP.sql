CREATE OR REPLACE procedure CSA.xover_csa_initialize_csp as
  prog varchar2(40):='XOVER_CSA_INITIALIZE_CSP';
  ver  varchar2(40):='V1.5 11 Feb 2019, JL';
  Reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  msgtxt varchar2(4000);
  statusrec csa.xover_status%rowtype;
begin
  select * into statusrec from csa.xover_status;
  If statusrec.xover_last_event<>'CM' then
    msgtxt:='Error - Crossover(return) is initiating on CSA, but '
      ||'XOVER_LAST_EVENT='||statusrec.xover_last_event||', not "CM" as expected.'; 
    update csa.xover_status 
      set err_step='CI', err_dt=sysdate, err_msg=msgtxt;
    commit;  
    raise_application_error(-20234,'csa '||msgtxt);
  end if;
  update csa.xover_status set xover_last_event='CI',
    csa_init_dt=sysdate,err_step=null,err_dt=null,err_msg=null;
  commit;
end;
/
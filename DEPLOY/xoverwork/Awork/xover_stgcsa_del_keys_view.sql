CREATE OR REPLACE VIEW STGCSA.XOVER_STGCSA_DEL_KEYS_VIEW
(
  TABLENM,
  PK
)
BEQUEATH DEFINER
AS
    SELECT DISTINCT tablenm, CAST (pk AS VARCHAR2 (200)) pk
      FROM stgcsa.stgupdtlogtbl
     WHERE     upd_action = 'D'
           AND updtdt >
               (SELECT DECODE (
                         stgcsa_merge_end_dt,
                         TO_DATE ('19000101', 'yyyymmdd'), TO_DATE ('29000101',
                                                                    'yyyymmdd'),
                         stgcsa_merge_end_dt)
                  FROM stgcsa.xover_status)
           AND tablenm IN ('SOFVBGW9TBL',
                           'SOFVCDCMSTRTBL',
                           'SOFVCTCHTBL',
                           'SOFVDFPYTBL',
                           'SOFVDUEBTBL',
                           'SOFVGNTYTBL',
                           'SOFVGRGCTBL',
                           'SOFVLND1TBL',
                           'SOFVLND2TBL',
                           'SOFVOTRNTBL',
                           'SOFVPYMTTBL',
                           'SOFVRTSCTBL',
                           'SOFVBFFATBL',
                           'SOFVBGDFTBL',
                           'SOFVFTPOTBL',
                           'SOFVDRTBL')
  ORDER BY tablenm;       

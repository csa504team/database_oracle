create index stgcsa.SOFVBGW9TBL_lastupdt_idx                   
  on stgcsa.SOFVBGW9TBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVCDCMSTRTBL_lastupdt_idx                
  on stgcsa.SOFVCDCMSTRTBL(lastupdtdt) tablespace stgcsaindtbs;                
                                                                                
create index stgcsa.SOFVCTCHTBL_lastupdt_idx                   
  on stgcsa.SOFVCTCHTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVDFPYTBL_lastupdt_idx                   
  on stgcsa.SOFVDFPYTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVDUEBTBL_lastupdt_idx                   
  on stgcsa.SOFVDUEBTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVFTPOTBL_lastupdt_idx                   
  on stgcsa.SOFVFTPOTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVGNTYTBL_lastupdt_idx                   
  on stgcsa.SOFVGNTYTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVGRGCTBL_lastupdt_idx                   
  on stgcsa.SOFVGRGCTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVLND1TBL_lastupdt_idx                   
  on stgcsa.SOFVLND1TBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVLND2TBL_lastupdt_idx                   
  on stgcsa.SOFVLND2TBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVOTRNTBL_lastupdt_idx                   
  on stgcsa.SOFVOTRNTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVPYMTTBL_lastupdt_idx                   
  on stgcsa.SOFVPYMTTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVRTSCTBL_lastupdt_idx                   
  on stgcsa.SOFVRTSCTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVAUDTTBL_lastupdt_idx                   
  on stgcsa.SOFVAUDTTBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVBFFATBL_lastupdt_idx                   
  on stgcsa.SOFVBFFATBL(lastupdtdt) tablespace stgcsaindtbs;                   
                                                                                
create index stgcsa.SOFVBGDFTBL_lastupdt_idx                   
  on stgcsa.SOFVBGDFTBL(lastupdtdt) tablespace stgcsaindtbs;  
                                                                                
create index stgcsa.SOFVDRTBL_lastupdt_idx                   
  on stgcsa.SOFVDRTBL(lastupdtdt) tablespace stgcsaindtbs;  
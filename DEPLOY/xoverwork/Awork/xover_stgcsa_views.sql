-- crossover export views for app tables (stgcsa side)
--    View only get changes since the last crossover import on this DB 

CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVAUDTTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVBFFATBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVBGDFTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVBGW9TBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVCDCMSTRTBL_VIEW as                
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVCTCHTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVDFPYTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVDUEBTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVDRTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVDRTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
    
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVFTPOTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVGNTYTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVGRGCTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVLND1TBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVLND2TBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVOTRNTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVPYMTTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW STGCSA.XOVER_SOFVRTSCTBL_VIEW as                   
  with merge_end as  (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
  select * FROM stgcsa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= STGCSA_MERGE_END_DT;
                                                                                

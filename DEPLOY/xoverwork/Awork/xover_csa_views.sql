-- crossover export views for app tables (csa.side)
--    View only get changes since the last crossover import on this DB 

CREATE OR REPLACE VIEW CSA.XOVER_SOFVBFFATBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVBGDFTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVBGW9TBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVCDCMSTRTBL_VIEW as                
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVCTCHTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVDFPYTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVDRTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVDRTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVDUEBTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVGNTYTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVGRGCTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVLND1TBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVLND2TBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVOTRNTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVPYMTTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
                                                                                
CREATE OR REPLACE VIEW CSA.XOVER_SOFVRTSCTBL_VIEW as                   
  with merge_end as  (SELECT CSA_MERGE_END_DT FROM csa.xover_status)
  select * FROM csa.SOFVPYMTTBL, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;
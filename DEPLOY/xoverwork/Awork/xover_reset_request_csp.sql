CREATE OR REPLACE procedure STGCSA.xover_reset_request_csp 
   (p_retval out number, 
    p_errval out number,  
    p_errmsg out varchar2, 
    p_identifier number:=0, 
    p_userid varchar2) as
  prog varchar2(40):='XOVER_RESET_REQUEST_CSP';
  ver  varchar2(40):='V1.5 11 Feb 2019, JL';
  Reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  msgtxt varchar2(4000);
  statusrec stgcsa.xover_status%rowtype;
begin
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  select * into statusrec from stgcsa.xover_status;
  msgtxt:='STGCSA.XOVER_RESET_REQUEST_CSP started';
  runtime.logger(logentryid,'STDXVR',235,msgtxt,user,4,
    program_name=>prog||' '||ver,force_log_entry=>true);  
  update stgcsa.xover_status set xover_last_event='SM',
    stgcsa_merge_end_dt=reset_date,err_step=null,err_dt=null,err_msg=null;
  delete from STGCSA.JOBQUETBL;
  delete from STGCSA.MFJOBDETAILTBL;
  delete from STGCSA.MFJOBTBL;  
  commit;
  MSGTXT:='XOVER RESET REQUEST RECORDED (STGCSA_MERGE_END_DT set to "1900".) '
    ||'Tables MFJOBTBL, MFJOBDETAILTBL, JOBQUETBL emptied.  Normal Completion';
  runtime.logger(logentryid,'STDXVR',236,MSGTXT,p_userid,5,
    program_name=>prog||' '||ver);  
end;
/
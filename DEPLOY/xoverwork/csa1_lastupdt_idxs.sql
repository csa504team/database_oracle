create index csa.SOFVBGW9TBL_lastupdt_idx                   
  on csa.SOFVBGW9TBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVCDCMSTRTBL_lastupdt_idx                
  on csa.SOFVCDCMSTRTBL(lastupdtdt) tablespace csaindtbs;                
                                                                                
create index csa.SOFVCTCHTBL_lastupdt_idx                   
  on csa.SOFVCTCHTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVDFPYTBL_lastupdt_idx                   
  on csa.SOFVDFPYTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVDUEBTBL_lastupdt_idx                   
  on csa.SOFVDUEBTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVFTPOTBL_lastupdt_idx                   
  on csa.SOFVFTPOTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVGNTYTBL_lastupdt_idx                   
  on csa.SOFVGNTYTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVGRGCTBL_lastupdt_idx                   
  on csa.SOFVGRGCTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVLND1TBL_lastupdt_idx                   
  on csa.SOFVLND1TBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVLND2TBL_lastupdt_idx                   
  on csa.SOFVLND2TBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVOTRNTBL_lastupdt_idx                   
  on csa.SOFVOTRNTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVPYMTTBL_lastupdt_idx                   
  on csa.SOFVPYMTTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVRTSCTBL_lastupdt_idx                   
  on csa.SOFVRTSCTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVAUDTTBL_lastupdt_idx                   
  on csa.SOFVAUDTTBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVBFFATBL_lastupdt_idx                   
  on csa.SOFVBFFATBL(lastupdtdt) tablespace csaindtbs;                   
                                                                                
create index csa.SOFVBGDFTBL_lastupdt_idx                   
  on csa.SOFVBGDFTBL(lastupdtdt) tablespace csaindtbs;  

CREATE OR REPLACE TRIGGER STGCSA.SOFVDRTRG
    before insert or update or delete ON STGCSA.SOFVDRTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2018-10-12 12:06:42
  Created by: GENR
  Crerated from template csatrig_template v3.1 11 sep 2018 on 2018-10-12 12:06:42
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVDRTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and
         (oldval<>newval
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )
  then
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then
      logdtl(logdtl_ctr).newvalue:=newval;
    else
      logdtl(logdtl_ctr).newvalue:=null;
    end if;
    if updating or deleting then
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;
    end if;
  end if;
end;
--
--
--
begin
  --
  -- History logging (HST)... initialize
  SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate
    into hst.updtlogid, hst.updtdt FROM DUAL;
  -- set aside table pk, set managed variables
  hst.csa_sid:=runtime.csa_session_id;
  hst.loannmb:=null;
  hst.tablenm:='SOFVDRTBL';
  hst.loannmb:=nvl(:old.loannmb,:new.loannmb);

  hst.updtuserid:=nvl(:new.lastupdtuserid,:old.lastupdtuserid);
  if hst.updtuserid is null then hst.updtuserid:=runtime.csa_userid; end if;
  if hst.updtuserid is null then hst.updtuserid:=user; end if;
  case
    when inserting=true then hst.upd_action:='I';
    when updating=true then hst.upd_action:='U';
    when deleting=true then hst.upd_action:='D';
  end case;
  -- now set composit PK for row being updated
  hst.pk:=null
    ||nvl(:new.LOANNMB,:old.LOANNMB);
  -- insert log main row
  INSERT INTO stgupdtlogtbl values HST;
  --
  -- Now column value logging for changed columns
  --   Initialize logdtl collection.
  logdtl.extend;
  logdtl_ctr:=1;
  logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
  logdtl(logdtl_ctr).oldvalue:=null;
  logdtl(logdtl_ctr).newvalue:=null;
  histlog_col_if_changed('LOANNMB',
    :old.LOANNMB,:new.LOANNMB);
  histlog_col_if_changed('CREATUSERID',
    :old.CREATUSERID,:new.CREATUSERID);
  histlog_col_if_changed('CREATDT',
    to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
  histlog_col_if_changed('LASTUPDTUSERID',
    :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
  histlog_col_if_changed('LASTUPDTDT',
    to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
  -- write out any accumulated logdtl recs with bulk insert
  if logdtl_ctr>1 then
    forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
  end if;
  -- end of history logging section
end;
/

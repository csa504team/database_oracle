CREATE OR REPLACE procedure CSA.xover_CSA_merge_csp(do_validation boolean:=true) as
  prog varchar2(30):='XOVER_CSA_MERGE_CSP';
  ver  varchar2(30):='v1.1 31 Jan 2019';
  -- v1.1 31 Jan jl- increase offset by 3 in PK after a timestamp field
  --  (affected PK extraction for SOFVAUDTTBL, SOFVOTRNTBL only)
  -- V1.0 jl 16 Jan 2019
  -- Program to update local copy of crossover tables with recent updates 
  -- that were delivered by the crossover process, and to perfom validation
  -- as much as practical.  Also maintains the single crossover status record 
  -- used to control the process.
  --
  -- There are two versions of this program which pretty much mirror each
  -- other, one on STGCSA to update STGCSA tables with data CSA, the other
  -- on CSA to update CSA tables with updates sent from STGCSA.  The two
  -- versions differ slightly.  In general only STGCSA deletes rows from the
  -- crossover tables, so STGCSA sends to CSA a table of keys of deleted rows,
  -- and the CSA version of this program processes the list of keys to delete
  -- these rows on the CSA side. 
  --  
  -- There are 17 application crossover tables.  Three have special 
  -- requirements.  SOFVPROPTBL is only updated on the CSA side, so it 
  -- is not included in the export from STGCSA to CSA.  Likewise SOFVAUDTTBL
  -- is only updated by STGCSA, so it is not included in the export of CSA.
  -- SOFVFTPOTBL Is always emptied by CSA processing, so it is only passed
  -- from STGCSA to CSA.  The STGCSA version of this program therefore always
  -- deletes all rows from SOFVFTPOTBL. 
  --   
  starttime date;
  dtfmt constant varchar2(30):='yyyy-mm-dd hh24:mi:ss' ; 
  logentryid number;
  msgtxt varchar2(4000);
  -- vars for checking row counts
  statusrec CSA.XOVER_STATUS%rowtype;
  errtxt varchar2(4096);  
  Blackout_start_dt date;
  blackout_error_count number:=0; 
  blackout_error_table_count number:=0;
  dovalidation varchar2(5):='TRUE'; 
  -- vars for key columns of crossover tables... 
  -- delete step extracts key values into these from PK fields of log recs
  SOFVAUDTTBL_pk1 CHAR(15);
  SOFVAUDTTBL_pk2 TIMESTAMP(6);
  SOFVAUDTTBL_pk3 NUMBER;
  SOFVBFFATBL_pk1 CHAR(10);
  SOFVBFFATBL_pk2 DATE;
  SOFVBGDFTBL_pk1 CHAR(10);
  SOFVBGW9TBL_pk1 CHAR(10);
  SOFVCDCMSTRTBL_pk1 CHAR(2);
  SOFVCDCMSTRTBL_pk2 CHAR(4);
  SOFVCTCHTBL_pk1 CHAR(10);
  SOFVDFPYTBL_pk1 CHAR(10);
  SOFVDUEBTBL_pk1 CHAR(10);
  SOFVDUEBTBL_pk2 NUMBER;
  SOFVDUEBTBL_pk3 DATE;
  SOFVFTPOTBL_pk1 CHAR(10);
  SOFVGNTYTBL_pk1 CHAR(10);
  SOFVGNTYTBL_pk2 DATE;
  SOFVGNTYTBL_pk3 CHAR(1);
  SOFVGRGCTBL_pk1 DATE;
  SOFVLND1TBL_pk1 CHAR(10);
  SOFVLND2TBL_pk1 CHAR(10);
  SOFVOTRNTBL_pk1 CHAR(10);
  SOFVOTRNTBL_pk2 TIMESTAMP(6);
  SOFVOTRNTBL_pk3 CHAR(4);
  SOFVOTRNTBL_pk4 CHAR(4);
  SOFVPROPTBL_pk1 CHAR(10);
  SOFVPYMTTBL_pk1 CHAR(10);
  SOFVPYMTTBL_pk2 DATE;
  SOFVRTSCTBL_pk1 CHAR(10);
  SOFVRTSCTBL_pk2 DATE;
  SOFVDRTBL_pk1 CHAR(10 BYTE);  
  --
  -- procedure debug_putline... 
  -- since no logging for CSA side, little write procedure for testing
  procedure debug_putline(n1 out number, x1 varchar, msgid number, 
    msgtxt varchar, x2 varchar, sev number, program_name varchar) is
  begin
    dbms_output.put_line('ID='||msgid||' sev-'||sev||': '||msgtxt);
  end;    
Begin
  --  
  -- initialize... get time, log start
  select sysdate into starttime from dual;
  dovalidation:='TRUE';
  if do_validation=false then 
    dovalidation:='FALSE';  
  end if;  
  msgtxt:=prog||' started.  DO_VALIDITION='||dovalidation;
  debug_putline(logentryid,'STDXVR',220,msgtxt,user,4,
    program_name=>prog||' '||ver);
  -- get status rec forwarded from other side, write data to log...
  select * into statusrec from csa.xover_status;
  msgtxt:='Sent/expected rowcounts for xover exp run: '
    ||' SOFVBGW9TBL:'||statusrec.stgcnt_SOFVBGW9TBL                                 
    ||' SOFVCDCMSTRTBL:'||statusrec.stgcnt_SOFVCDCMSTRTBL                           
    ||' SOFVCTCHTBL:'||statusrec.stgcnt_SOFVCTCHTBL                                 
    ||' SOFVDFPYTBL:'||statusrec.stgcnt_SOFVDFPYTBL                                 
    ||' SOFVDUEBTBL:'||statusrec.stgcnt_SOFVDUEBTBL                                 
    ||' SOFVFTPOTBL:'||statusrec.stgcnt_SOFVFTPOTBL                                 
    ||' SOFVGNTYTBL:'||statusrec.stgcnt_SOFVGNTYTBL                                 
    ||' SOFVGRGCTBL:'||statusrec.stgcnt_SOFVGRGCTBL                                 
    ||' SOFVLND1TBL:'||statusrec.stgcnt_SOFVLND1TBL                                 
    ||' SOFVLND2TBL:'||statusrec.stgcnt_SOFVLND2TBL                                 
    ||' SOFVOTRNTBL:'||statusrec.stgcnt_SOFVOTRNTBL                                 
    ||' SOFVPYMTTBL:'||statusrec.stgcnt_SOFVPYMTTBL                                 
    ||' SOFVRTSCTBL:'||statusrec.stgcnt_SOFVRTSCTBL                                 
    ||' SOFVAUDTTBL:'||statusrec.stgcnt_SOFVAUDTTBL                                 
    ||' SOFVBFFATBL:'||statusrec.stgcnt_SOFVBFFATBL                                 
    ||' SOFVBGDFTBL:'||statusrec.stgcnt_SOFVBGDFTBL
	||' SOFVDRTBL:'||statusrec.stgcnt_SOFVDRTBL;
  debug_putline(logentryid,'STDXVR',224,msgtxt,user,0,
    program_name=>prog||' '||ver);  
  select * into statusrec from xover_status;
  --
   --
   msgtxt:='Prior events from the last completed crossover: '
    ||' CI:'||to_char(statusrec.CSA_INIT_dt,'mm-dd hh24:mi:ss')
    ||' CM:'||to_char(statusrec.CSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
	||' CX:'||to_char(statusrec.CSA_EXPORT_START_dt,'mm-dd hh24:mi:ss')
	||' SI:'||to_char(statusrec.STGCSA_INIT_dt,'mm-dd hh24:mi:ss')
	||' SM:'||to_char(statusrec. STGCSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
	||' SX:'||to_char(statusrec.STGCSA_EXPORT_START_dt,'mm-dd hh24:mi:ss');
  debug_putline(logentryid,'STDXVR',228,msgtxt,user,0,
    program_name=>prog||' '||ver);  
  -- perform (initial) validation unless explicitly turned off
  if do_validation=true then
    --
    -- integrity check 1:
    -- Verify the local DB state is correct for this program to run.
    -- Correct state is that last completed action (XOVER_LAST_EVENT) is 'SX',
    -- LASTUPDTDT matches STGCSA_EXPORT_START_DT, STGCSA_EXPORT_START_DT > 
    -- STGCSA_MERGE_END_DT, STGCSA_MERGE_END_DT > CSA_EXPORT_START_DT, and
    -- CSA_EXPORT_START_DT > CSA_MERGE_END_DT.  
    select * into statusrec from csa.xover_status;
    Errtxt:=null;
    If not statusrec.xover_last_event='SX' then
      Errtxt:=errtxt||', Last event is '||statusrec.xover_last_event
        ||' not SX as expected.'||' Last event dates are:  '
		||' CI:'||to_char(statusrec.CSA_INIT_dt,'mm-dd hh24:mi:ss')
		||' CM:'||to_char(statusrec.CSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
		||' CX:'||to_char(statusrec.CSA_EXPORT_START_dt,'mm-dd hh24:mi:ss')
		||' SI:'||to_char(statusrec.STGCSA_INIT_dt,'mm-dd hh24:mi:ss')
		||' SM:'||to_char(statusrec. STGCSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
		||' SX:'||to_char(statusrec.STGCSA_EXPORT_START_dt,'mm-dd hh24:mi:ss');
    end if;    
    /*if not statusrec.LASTUPDTDT=statusrec.STGCSA_EXPORT_START_DT then 
      Errtxt:=errtxt||', LASTUPDTDT ('||to_char(statusrec.lastupdtdt,dtfmt)
        ||') does not match STGCSA_EXPORT_START_DT ('
        ||to_char(statusrec.STGCSA_EXPORT_START_DT,dtfmt)||')';
    end if;    
    if not statusrec.STGCSA_EXPORT_START_DT>statusrec.STGCSA_MERGE_END_DT then 
      Errtxt:=errtxt||', STGCSA_EXPORT_START_DT ('
        ||to_char(statusrec.STGCSA_EXPORT_START_DT,dtfmt)
        ||') is not after last STGCSA_MERGE_END_DT ('
        ||to_char(statusrec.STGCSA_MERGE_END_DT,dtfmt)||')';
    end if;    
    if not statusrec.STGCSA_MERGE_END_DT>statusrec.CSA_EXPORT_START_DT then 
      Errtxt:=errtxt||', STGCSA_MERGE_END_DT ('
        ||to_char(statusrec.STGCSA_MERGE_END_DT,dtfmt)
        ||') is not after last CSA_EXPORT_START_DT ('
        ||to_char(statusrec.CSA_EXPORT_START_DT,dtfmt)||')';
    end if;    
    if not statusrec.CSA_EXPORT_START_DT>statusrec.CSA_MERGE_END_DT then 
      Errtxt:=errtxt||', CSA_EXPORT_START_DT ('
        ||to_char(statusrec.CSA_EXPORT_START_DT,dtfmt)
        ||') is not after last CCSA_MERGE_END_DT  ('
        ||to_char(statusrec.CSA_MERGE_END_DT ,dtfmt)||')';
    end if;   */ 
    -- catch anything?  
    if errtxt is not null then
      msgtxt:='Error: '||substr(errtxt,3)
        ||'.  This suggests that last crossover step did not complete correctly'
        ||' or this is not the correct "next" crossover step.'
        ||'  No updates were performed.';
      debug_putline(logentryid,'STDXVR',224,msgtxt,user,3,
        program_name=>prog||' '||ver);
      -- since no updating has been done, it is ok to commit messages, then abort
      commit;
      raise_application_error(-20224,msgtxt);
    end if;    
    --  If still here, we passed last check
    --
    -- Integrity check two:  
    -- The crossover tables should never be updated on this side while the 
    -- other side is live.  As a double-check that this is adhered to,
    -- look for any rows that have been updated on this side since 
    -- this side was last "crossed over" to the other side.  
    -- Alarm and abort if any such updates are found.  
    errtxt:=null;
    blackout_start_dt:=statusrec.CSA_export_start_DT;
/*  -- SOFVAUDTTBL is not updated by CSA so it is excluded from this check       
    Select count(*) into blackout_error_count from CSA.SOFVAUDTTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVAUDTTBL('||blackout_error_count||')';                   
    end if;    
*/                         
    Select count(*) into blackout_error_count from CSA.SOFVBFFATBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVBFFATBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVBGDFTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVBGDFTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVBGW9TBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVBGW9TBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVCDCMSTRTBL               
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVCDCMSTRTBL('||blackout_error_count||')';                
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVCTCHTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVCTCHTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVDFPYTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVDFPYTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVDUEBTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVDUEBTBL('||blackout_error_count||')';                   
    end if;   
/*  SOFVFTPOTBL is always emptied after CSA processing, so dont check for updts                          
    Select count(*) into blackout_error_count from CSA.SOFVFTPOTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVFTPOTBL('||blackout_error_count||')';                   
    end if;        
*/                     
    Select count(*) into blackout_error_count from CSA.SOFVGNTYTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVGNTYTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVGRGCTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVGRGCTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVLND1TBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVLND1TBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVLND2TBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVLND2TBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVOTRNTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVOTRNTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVPYMTTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVPYMTTBL('||blackout_error_count||')';                   
    end if;                         
    Select count(*) into blackout_error_count from CSA.SOFVRTSCTBL                  
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVRTSCTBL('||blackout_error_count||')';                   
    end if;         
	Select count(*) into blackout_error_count from CSA.SOFVDRTBL               
      where lastupdtdt>blackout_start_dt;
    If blackout_error_count>0 then                    
      blackout_error_table_count:=blackout_error_table_count+1;
      errtxt:=errtxt
        ||', SOFVDRTBL('||blackout_error_count||')';                
    end if;	
    -- find any updates during last local blackout?  
    if errtxt is not null then
      msgtxt:='Error: There were local updates to CSA crossover tables (LASTUPDTDT)'
        ||' since current blackout started ('
        ||to_char(blackout_start_dt,dtfmt)||').  '||substr(errtxt,3)
        ||'.  This suggests that last crossover step did not complete correctly'
        ||' or this is not the correct "next" crossover step.'
        ||'  No updates were performed.';
      debug_putline(logentryid,'STDXVR',225,msgtxt,user,3,
        program_name=>prog||' '||ver);
      -- since no updating has been done, it is ok to commit messages, then abort
      commit;
      raise_application_error(-20225,msgtxt);
    end if; 
  end if /* end of this optional validation block */;   
  --
  --
  -- delete processing...
  -- STGCSA (other side) may delete rows. Table xover_stgcsa_delete_keys
  -- identifies rows deleted by stgcsa since last crossover.  Now delete 
  -- locally all such rows.  
  -- Note, if a row (e.g. a loan) was both created and deleted on stgcsa during 
  -- latest cycle then it won't be found on CSA side to delete. 
  -- STGCSA deletes are expected to be low volume.
  for delrec in (select * from CSA.xover_stgcsa_del_keys) loop 
    case delrec.tablenm
/*  STGCSA.SOFVAUDTTBL should not be deleted from, and no trigger exists */
/*    to record the deletes (XOVER_STGCSA_DEL_KEYS) if it happened, */
/*    so dont look for sofvaudttbl deletes */
/*  when 'SOFVAUDTTBL' then
      -- extract key values for SOFVAUDTTBL from log rec 
      SOFVAUDTTBL_pk1:=
         substr(delrec.pk,1,15);
      SOFVAUDTTBL_pk2:=
         to_timestamp(substr(delrec.pk,16,26),'YYYY-MM-DD HH24:MI:SS.ff');
      SOFVAUDTTBL_pk3:=
        to_number(substr(delrec.pk,45,22));
      delete from csa.SOFVAUDTTBL where 
        AUDTRECRDKEY=SOFVAUDTTBL_pk1 and
        AUDTTRANSDT=SOFVAUDTTBL_pk2 and
        AUDTSEQNMB=SOFVAUDTTBL_pk3 and
        1=1;
*/        
    when 'SOFVBFFATBL' then
      -- extract key values for SOFVBFFATBL from log rec 
      SOFVBFFATBL_pk1:=
         substr(delrec.pk,1,10);
      SOFVBFFATBL_pk2:=
         to_date(substr(delrec.pk,11,19),'YYYY-MM-DD HH24:MI:SS');
      delete from csa.SOFVBFFATBL where 
        ASSUMPKEYLOANNMB=SOFVBFFATBL_pk1 and
        ASSUMPLASTEFFDT=SOFVBFFATBL_pk2 and
        1=1;
    when 'SOFVBGDFTBL' then
      -- extract key values for SOFVBGDFTBL from log rec 
      SOFVBGDFTBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVBGDFTBL where 
        DEFRMNTGPKEY=SOFVBGDFTBL_pk1 and
        1=1;
    when 'SOFVBGW9TBL' then
      -- extract key values for SOFVBGW9TBL from log rec 
      SOFVBGW9TBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVBGW9TBL where 
        LOANNMB=SOFVBGW9TBL_pk1 and
        1=1;
    when 'SOFVCDCMSTRTBL' then
      -- extract key values for SOFVCDCMSTRTBL from log rec 
      SOFVCDCMSTRTBL_pk1:=
         substr(delrec.pk,1,2);
      SOFVCDCMSTRTBL_pk2:=
         substr(delrec.pk,3,4);
      delete from csa.SOFVCDCMSTRTBL where 
        CDCREGNCD=SOFVCDCMSTRTBL_pk1 and
        CDCCERTNMB=SOFVCDCMSTRTBL_pk2 and
        1=1;
    when 'SOFVCTCHTBL' then
      -- extract key values for SOFVCTCHTBL from log rec 
      SOFVCTCHTBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVCTCHTBL where 
        CATCHUPPLANSBANMB=SOFVCTCHTBL_pk1 and
        1=1;
    when 'SOFVDFPYTBL' then
      -- extract key values for SOFVDFPYTBL from log rec 
      SOFVDFPYTBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVDFPYTBL where 
        LOANNMB=SOFVDFPYTBL_pk1 and
        1=1;
    when 'SOFVDUEBTBL' then
      -- extract key values for SOFVDUEBTBL from log rec 
      SOFVDUEBTBL_pk1:=
         substr(delrec.pk,1,10);
      SOFVDUEBTBL_pk2:=
        to_number(substr(delrec.pk,11,22));
      SOFVDUEBTBL_pk3:=
         to_date(substr(delrec.pk,33,19),'YYYY-MM-DD HH24:MI:SS');
      delete from csa.SOFVDUEBTBL where 
        LOANNMB=SOFVDUEBTBL_pk1 and
        DUEFROMBORRRECRDTYP=SOFVDUEBTBL_pk2 and
        DUEFROMBORRPOSTINGDT=SOFVDUEBTBL_pk3 and
        1=1;
    when 'SOFVFTPOTBL' then
      -- extract key values for SOFVFTPOTBL from log rec 
      SOFVFTPOTBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVFTPOTBL where 
        LOANNMB=SOFVFTPOTBL_pk1 and
        1=1;
    when 'SOFVGNTYTBL' then
      -- extract key values for SOFVGNTYTBL from log rec 
      SOFVGNTYTBL_pk1:=
        substr(delrec.pk,1,10);
      SOFVGNTYTBL_pk2:=
         to_date(substr(delrec.pk,11,19),'YYYY-MM-DD HH24:MI:SS');
      SOFVGNTYTBL_pk3:=
         substr(delrec.pk,30,1);
      delete from csa.SOFVGNTYTBL where 
        LOANNMB=SOFVGNTYTBL_pk1 and
        GNTYDT=SOFVGNTYTBL_pk2 and
        GNTYTYP=SOFVGNTYTBL_pk3 and
        1=1;
    when 'SOFVGRGCTBL' then
      -- extract key values for SOFVGRGCTBL from log rec 
      SOFVGRGCTBL_pk1:=
         to_date(substr(delrec.pk,1,19),'YYYY-MM-DD HH24:MI:SS');
      delete from csa.SOFVGRGCTBL where 
        CALNDRKEYDT=SOFVGRGCTBL_pk1 and
        1=1;
    when 'SOFVLND1TBL' then
      -- extract key values for SOFVLND1TBL from log rec 
      SOFVLND1TBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVLND1TBL where 
        LOANNMB=SOFVLND1TBL_pk1 and
        1=1;
    when 'SOFVLND2TBL' then
      -- extract key values for SOFVLND2TBL from log rec 
      SOFVLND2TBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVLND2TBL where 
        LOANNMB=SOFVLND2TBL_pk1 and
        1=1;
    when 'SOFVOTRNTBL' then
      -- extract key values for SOFVOTRNTBL from log rec 
      SOFVOTRNTBL_pk1:=
         substr(delrec.pk,1,10);
      SOFVOTRNTBL_pk2:=
         to_timestamp(substr(delrec.pk,11,26),'YYYY-MM-DD HH24:MI:SS.ff');
      SOFVOTRNTBL_pk3:=
         substr(delrec.pk,40,4);
      SOFVOTRNTBL_pk4:=
         substr(delrec.pk,44,4);
      delete from csa.SOFVOTRNTBL where 
        LOANNMB=SOFVOTRNTBL_pk1 and
        ONLNTRANSTRANSDT=SOFVOTRNTBL_pk2 and
        ONLNTRANSTRMNLID=SOFVOTRNTBL_pk3 and
        ONLNTRANSOPRTRINITIAL=SOFVOTRNTBL_pk4 and
        1=1;
    when 'SOFVPYMTTBL' then
      -- extract key values for SOFVPYMTTBL from log rec 
      SOFVPYMTTBL_pk1:=
         substr(delrec.pk,1,10);
      SOFVPYMTTBL_pk2:=
         to_date(substr(delrec.pk,11,19),'YYYY-MM-DD HH24:MI:SS');
      delete from csa.SOFVPYMTTBL where 
        LOANNMB=SOFVPYMTTBL_pk1 and
        PYMTPOSTPYMTDT=SOFVPYMTTBL_pk2 and
        1=1;
    when 'SOFVRTSCTBL' then
      -- extract key values for SOFVRTSCTBL from log rec 
      SOFVRTSCTBL_pk1:=
         substr(delrec.pk,1,10);
      SOFVRTSCTBL_pk2:=
         to_date(substr(delrec.pk,11,19),'YYYY-MM-DD HH24:MI:SS');
      delete from csa.SOFVRTSCTBL where 
        PRGMRTFEEDESC=SOFVRTSCTBL_pk1 and
        PRGMRTENDDT=SOFVRTSCTBL_pk2 and
        1=1;
	when 'SOFVDRTBL' then
      -- extract key values for SOFVDRTBL from log rec 
      SOFVDRTBL_pk1:=
         substr(delrec.pk,1,10);
      delete from csa.SOFVDRTBL where 
        LOANNMB=SOFVDRTBL_pk1 and
        1=1;
    else
      raise_application_error(-20001,
        'XOVER_CSA_MERGE, processing log records for deletes, found rec for'
        ||' unexpected table name: '||delrec.tablenm);     
    end case;   
    msgtxt:='Delete executed, rowcnt='||sql%rowcount
      ||'  tbl='||delrec.tablenm||', pk='||delrec.pk;  
    debug_putline(logentryid,'STDXVR',227,msgtxt,user,0,
    program_name=>prog||' '||ver);  
  end loop;
  --
  --
  -- now merge new/updated recs into the permanent tables
    --                                                            
  -- SOFVAUDTTBL                                                 
  merge into CSA.SOFVAUDTTBL master                                          
    using (select * from CSA.XOVER_SOFVAUDTTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.AUDTRECRDKEY=returned.AUDTRECRDKEY
      and master.AUDTTRANSDT=returned.AUDTTRANSDT
      and master.AUDTSEQNMB=returned.AUDTSEQNMB)                                  
    when matched then update set                           
      master.AUDTOPRTRINITIAL=returned.AUDTOPRTRINITIAL,                              
      master.AUDTTRMNLID=returned.AUDTTRMNLID,                                        
      master.AUDTCHNGFILE=returned.AUDTCHNGFILE,                                      
      master.AUDTACTNCD=returned.AUDTACTNCD,                                          
      master.AUDTBEFCNTNTS=returned.AUDTBEFCNTNTS,                                    
      master.AUDTAFTCNTNTS=returned.AUDTAFTCNTNTS,                                    
      master.AUDTFLDNM=returned.AUDTFLDNM,                                            
      master.AUDTEDTCD=returned.AUDTEDTCD,                                            
      master.AUDTUSERID=returned.AUDTUSERID,                                          
      master.CREATUSERID=returned.CREATUSERID,                                        
      master.CREATDT=returned.CREATDT,                                                
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                  
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (AUDTRECRDKEY,                                                                   
      AUDTTRANSDT,                                                                    
      AUDTSEQNMB,                                                                     
      AUDTOPRTRINITIAL,                                                               
      AUDTTRMNLID,                                                                    
      AUDTCHNGFILE,                                                                   
      AUDTACTNCD,                                                                     
      AUDTBEFCNTNTS,                                                                  
      AUDTAFTCNTNTS,                                                                  
      AUDTFLDNM,                                                                      
      AUDTEDTCD,                                                                      
      AUDTUSERID,                                                                     
      CREATUSERID,                                                                    
      CREATDT,                                                                        
      LASTUPDTUSERID,                                                                 
      LASTUPDTDT)
    VALUES (returned.AUDTRECRDKEY,                                                    
      returned.AUDTTRANSDT,                                                     
      returned.AUDTSEQNMB,                                                      
      returned.AUDTOPRTRINITIAL,                                                
      returned.AUDTTRMNLID,                                                     
      returned.AUDTCHNGFILE,                                                    
      returned.AUDTACTNCD,                                                      
      returned.AUDTBEFCNTNTS,                                                   
      returned.AUDTAFTCNTNTS,                                                   
      returned.AUDTFLDNM,                                                       
      returned.AUDTEDTCD,                                                       
      returned.AUDTUSERID,                                                      
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                    
  msgtxt:=prog||' Finished SOFVAUDTTBL, rowcount='||sql%rowcount||          
    ' Next SOFVPYMTTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  --
  -- PYMTS
  merge into CSA.SOFVPYMTTBL master
    using (select * from CSA.XOVER_SOFVPYMTTBL 
      where lastupdtdt > 
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.loannmb=returned.loannmb and master.pymtpostpymtdt=returned.pymtpostpymtdt)
    when matched then update set 
      master.PYMTPOSTPYMTTYP=returned.PYMTPOSTPYMTTYP,                                
      master.PYMTPOSTDUEAMT=returned.PYMTPOSTDUEAMT,                                  
      master.PYMTPOSTREMITTEDAMT=returned.PYMTPOSTREMITTEDAMT,                        
      master.PYMTPOSTPYMTNMB=returned.PYMTPOSTPYMTNMB,                                
      master.PYMTPOSTCSAPAIDSTARTDTX=returned.PYMTPOSTCSAPAIDSTARTDTX,                
      master.PYMTPOSTCSAPAIDTHRUDTX=returned.PYMTPOSTCSAPAIDTHRUDTX,                  
      master.PYMTPOSTCSADUEAMT=returned.PYMTPOSTCSADUEAMT,                            
      master.PYMTPOSTCSAAPPLAMT=returned.PYMTPOSTCSAAPPLAMT,                          
      master.PYMTPOSTCSAACTUALINITFEEAMT=returned.PYMTPOSTCSAACTUALINITFEEAMT,        
      master.PYMTPOSTCSAREPDAMT=returned.PYMTPOSTCSAREPDAMT,                          
      master.PYMTPOSTSBAFEEREPDAMT=returned.PYMTPOSTSBAFEEREPDAMT,                    
      master.PYMTPOSTCDCPAIDSTARTDTX=returned.PYMTPOSTCDCPAIDSTARTDTX,                
      master.PYMTPOSTCDCPAIDTHRUDTX=returned.PYMTPOSTCDCPAIDTHRUDTX,                  
      master.PYMTPOSTCDCDUEAMT=returned.PYMTPOSTCDCDUEAMT,                            
      master.PYMTPOSTCDCAPPLAMT=returned.PYMTPOSTCDCAPPLAMT,                          
      master.PYMTPOSTCDCREPDAMT=returned.PYMTPOSTCDCREPDAMT,                          
      master.PYMTPOSTSBAFEEAMT=returned.PYMTPOSTSBAFEEAMT,                            
      master.PYMTPOSTSBAFEEAPPLAMT=returned.PYMTPOSTSBAFEEAPPLAMT,                    
      master.PYMTPOSTINTPAIDSTARTDTX=returned.PYMTPOSTINTPAIDSTARTDTX,                
      master.PYMTPOSTINTPAIDTHRUDTX=returned.PYMTPOSTINTPAIDTHRUDTX,                  
      master.PYMTPOSTINTBASISCALC=returned.PYMTPOSTINTBASISCALC,                      
      master.PYMTPOSTINTRTUSEDPCT=returned.PYMTPOSTINTRTUSEDPCT,                      
      master.PYMTPOSTINTBALUSEDAMT=returned.PYMTPOSTINTBALUSEDAMT,                    
      master.PYMTPOSTINTDUEAMT=returned.PYMTPOSTINTDUEAMT,                            
      master.PYMTPOSTINTAPPLAMT=returned.PYMTPOSTINTAPPLAMT,                          
      master.PYMTPOSTINTACTUALBASISDAYS=returned.PYMTPOSTINTACTUALBASISDAYS,          
      master.PYMTPOSTINTACCRADJAMT=returned.PYMTPOSTINTACCRADJAMT,                    
      master.PYMTPOSTINTRJCTAMT=returned.PYMTPOSTINTRJCTAMT,                          
      master.PYMTPOSTSBAFEESTARTDTX=returned.PYMTPOSTSBAFEESTARTDTX,                  
      master.PYMTPOSTPRINDUEAMT=returned.PYMTPOSTPRINDUEAMT,                          
      master.PYMTPOSTPRINBALUSEDAMT=returned.PYMTPOSTPRINBALUSEDAMT,                  
      master.PYMTPOSTPRINAPPLAMT=returned.PYMTPOSTPRINAPPLAMT,                        
      master.PYMTPOSTPRINBALLEFTAMT=returned.PYMTPOSTPRINBALLEFTAMT,                  
      master.PYMTPOSTPRINRJCTAMT=returned.PYMTPOSTPRINRJCTAMT,                        
      master.PYMTPOSTGNTYREPAYAMT=returned.PYMTPOSTGNTYREPAYAMT,                      
      master.PYMTPOSTSBAFEEENDDTX=returned.PYMTPOSTSBAFEEENDDTX,                      
      master.PYMTPOSTPAIDBYCOLSONDTX=returned.PYMTPOSTPAIDBYCOLSONDTX,                
      master.PYMTPOSTLPYMTAMT=returned.PYMTPOSTLPYMTAMT,                              
      master.PYMTPOSTESCRCPTSTDAMT=returned.PYMTPOSTESCRCPTSTDAMT,                    
      master.PYMTPOSTLATEFEEASSESSAMT=returned.PYMTPOSTLATEFEEASSESSAMT,              
      master.PYMTPOSTCDCDISBAMT=returned.PYMTPOSTCDCDISBAMT,                          
      master.PYMTPOSTCDCSBADISBAMT=returned.PYMTPOSTCDCSBADISBAMT,                    
      master.PYMTPOSTXADJCD=returned.PYMTPOSTXADJCD,                                  
      master.PYMTPOSTLEFTOVRAMT=returned.PYMTPOSTLEFTOVRAMT,                          
      master.PYMTPOSTRJCTCD=returned.PYMTPOSTRJCTCD,                                  
      master.PYMTPOSTCMNT=returned.PYMTPOSTCMNT,                                      
      master.PYMTPOSTREMITTEDRJCTAMT=returned.PYMTPOSTREMITTEDRJCTAMT,                
      master.PYMTPOSTLEFTOVRRJCTAMT=returned.PYMTPOSTLEFTOVRRJCTAMT,                  
      master.PYMTPOSTRJCTDT=returned.PYMTPOSTRJCTDT,                                  
      master.PYMTPOSTLATEFEEPAIDAMT=returned.PYMTPOSTLATEFEEPAIDAMT,                  
      master.PYMTPOSTINTWAIVEDAMT=returned.PYMTPOSTINTWAIVEDAMT,                      
      master.PYMTPOSTPREPAYAMT=returned.PYMTPOSTPREPAYAMT,                            
      master.CREATUSERID=returned.CREATUSERID,                                        
      master.CREATDT=returned.CREATDT,                                                
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                  
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      PYMTPOSTPYMTDT,                                                           
      PYMTPOSTPYMTTYP,                                                          
      PYMTPOSTDUEAMT,                                                           
      PYMTPOSTREMITTEDAMT,                                                      
      PYMTPOSTPYMTNMB,                                                          
      PYMTPOSTCSAPAIDSTARTDTX,                                                  
      PYMTPOSTCSAPAIDTHRUDTX,                                                   
      PYMTPOSTCSADUEAMT,                                                        
      PYMTPOSTCSAAPPLAMT,                                                       
      PYMTPOSTCSAACTUALINITFEEAMT,                                              
      PYMTPOSTCSAREPDAMT,                                                       
      PYMTPOSTSBAFEEREPDAMT,                                                    
      PYMTPOSTCDCPAIDSTARTDTX,                                                  
      PYMTPOSTCDCPAIDTHRUDTX,                                                   
      PYMTPOSTCDCDUEAMT,                                                        
      PYMTPOSTCDCAPPLAMT,                                                       
      PYMTPOSTCDCREPDAMT,                                                       
      PYMTPOSTSBAFEEAMT,                                                        
      PYMTPOSTSBAFEEAPPLAMT,                                                    
      PYMTPOSTINTPAIDSTARTDTX,                                                  
      PYMTPOSTINTPAIDTHRUDTX,                                                   
      PYMTPOSTINTBASISCALC,                                                     
      PYMTPOSTINTRTUSEDPCT,                                                     
      PYMTPOSTINTBALUSEDAMT,                                                    
      PYMTPOSTINTDUEAMT,                                                        
      PYMTPOSTINTAPPLAMT,                                                       
      PYMTPOSTINTACTUALBASISDAYS,                                               
      PYMTPOSTINTACCRADJAMT,                                                    
      PYMTPOSTINTRJCTAMT,                                                       
      PYMTPOSTSBAFEESTARTDTX,                                                   
      PYMTPOSTPRINDUEAMT,                                                       
      PYMTPOSTPRINBALUSEDAMT,                                                   
      PYMTPOSTPRINAPPLAMT,                                                      
      PYMTPOSTPRINBALLEFTAMT,                                                   
      PYMTPOSTPRINRJCTAMT,                                                      
      PYMTPOSTGNTYREPAYAMT,                                                     
      PYMTPOSTSBAFEEENDDTX,                                                     
      PYMTPOSTPAIDBYCOLSONDTX,                                                  
      PYMTPOSTLPYMTAMT,                                                         
      PYMTPOSTESCRCPTSTDAMT,                                                    
      PYMTPOSTLATEFEEASSESSAMT,                                                 
      PYMTPOSTCDCDISBAMT,                                                       
      PYMTPOSTCDCSBADISBAMT,                                                    
      PYMTPOSTXADJCD,                                                           
      PYMTPOSTLEFTOVRAMT,                                                       
      PYMTPOSTRJCTCD,                                                           
      PYMTPOSTCMNT,                                                             
      PYMTPOSTREMITTEDRJCTAMT,                                                  
      PYMTPOSTLEFTOVRRJCTAMT,                                                   
      PYMTPOSTRJCTDT,                                                           
      PYMTPOSTLATEFEEPAIDAMT,                                                   
      PYMTPOSTINTWAIVEDAMT,                                                     
      PYMTPOSTPREPAYAMT,                                                        
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.PYMTPOSTPYMTDT,                                                  
      returned.PYMTPOSTPYMTTYP,                                                 
      returned.PYMTPOSTDUEAMT,                                                  
      returned.PYMTPOSTREMITTEDAMT,                                             
      returned.PYMTPOSTPYMTNMB,                                                 
      returned.PYMTPOSTCSAPAIDSTARTDTX,                                         
      returned.PYMTPOSTCSAPAIDTHRUDTX,                                          
      returned.PYMTPOSTCSADUEAMT,                                               
      returned.PYMTPOSTCSAAPPLAMT,                                              
      returned.PYMTPOSTCSAACTUALINITFEEAMT,                                     
      returned.PYMTPOSTCSAREPDAMT,                                              
      returned.PYMTPOSTSBAFEEREPDAMT,                                           
      returned.PYMTPOSTCDCPAIDSTARTDTX,                                         
      returned.PYMTPOSTCDCPAIDTHRUDTX,                                          
      returned.PYMTPOSTCDCDUEAMT,                                               
      returned.PYMTPOSTCDCAPPLAMT,                                              
      returned.PYMTPOSTCDCREPDAMT,                                              
      returned.PYMTPOSTSBAFEEAMT,                                               
      returned.PYMTPOSTSBAFEEAPPLAMT,                                           
      returned.PYMTPOSTINTPAIDSTARTDTX,                                         
      returned.PYMTPOSTINTPAIDTHRUDTX,                                          
      returned.PYMTPOSTINTBASISCALC,                                            
      returned.PYMTPOSTINTRTUSEDPCT,                                            
      returned.PYMTPOSTINTBALUSEDAMT,                                           
      returned.PYMTPOSTINTDUEAMT,                                               
      returned.PYMTPOSTINTAPPLAMT,                                              
      returned.PYMTPOSTINTACTUALBASISDAYS,                                      
      returned.PYMTPOSTINTACCRADJAMT,                                           
      returned.PYMTPOSTINTRJCTAMT,                                              
      returned.PYMTPOSTSBAFEESTARTDTX,                                          
      returned.PYMTPOSTPRINDUEAMT,                                              
      returned.PYMTPOSTPRINBALUSEDAMT,                                          
      returned.PYMTPOSTPRINAPPLAMT,                                             
      returned.PYMTPOSTPRINBALLEFTAMT,                                          
      returned.PYMTPOSTPRINRJCTAMT,                                             
      returned.PYMTPOSTGNTYREPAYAMT,                                            
      returned.PYMTPOSTSBAFEEENDDTX,                                            
      returned.PYMTPOSTPAIDBYCOLSONDTX,                                         
      returned.PYMTPOSTLPYMTAMT,                                                
      returned.PYMTPOSTESCRCPTSTDAMT,                                           
      returned.PYMTPOSTLATEFEEASSESSAMT,                                        
      returned.PYMTPOSTCDCDISBAMT,                                              
      returned.PYMTPOSTCDCSBADISBAMT,                                           
      returned.PYMTPOSTXADJCD,                                                  
      returned.PYMTPOSTLEFTOVRAMT,                                              
      returned.PYMTPOSTRJCTCD,                                                  
      returned.PYMTPOSTCMNT,                                                    
      returned.PYMTPOSTREMITTEDRJCTAMT,                                         
      returned.PYMTPOSTLEFTOVRRJCTAMT,                                          
      returned.PYMTPOSTRJCTDT,                                                  
      returned.PYMTPOSTLATEFEEPAIDAMT,                                          
      returned.PYMTPOSTINTWAIVEDAMT,                                            
      returned.PYMTPOSTPREPAYAMT,                                               
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);
  msgtxt:=prog||' Finished SOFVPYMTTBL, rowcount='||sql%rowcount||' Next SOFVLND1TBL';
  debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);
  --                                                            
  -- SOFVLND1TBL                                                 
  merge into CSA.SOFVLND1TBL master                                          
    using (select * from CSA.XOVER_SOFVLND1TBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on ( master.loannmb=returned.loannmb)                                  
    when matched then update set                           
      master.LOANDTLSBAOFC=returned.LOANDTLSBAOFC,                              
      master.LOANDTLBORRNM=returned.LOANDTLBORRNM,                              
      master.LOANDTLBORRMAILADRSTR1NM=returned.LOANDTLBORRMAILADRSTR1NM,        
      master.LOANDTLBORRMAILADRSTR2NM=returned.LOANDTLBORRMAILADRSTR2NM,        
      master.LOANDTLBORRMAILADRCTYNM=returned.LOANDTLBORRMAILADRCTYNM,          
      master.LOANDTLBORRMAILADRSTCD=returned.LOANDTLBORRMAILADRSTCD,            
      master.LOANDTLBORRMAILADRZIPCD=returned.LOANDTLBORRMAILADRZIPCD,          
      master.LOANDTLBORRMAILADRZIP4CD=returned.LOANDTLBORRMAILADRZIP4CD,        
      master.LOANDTLSBCNM=returned.LOANDTLSBCNM,                                
      master.LOANDTLSBCMAILADRSTR1NM=returned.LOANDTLSBCMAILADRSTR1NM,          
      master.LOANDTLSBCMAILADRSTR2NM=returned.LOANDTLSBCMAILADRSTR2NM,          
      master.LOANDTLSBCMAILADRCTYNM=returned.LOANDTLSBCMAILADRCTYNM,            
      master.LOANDTLSBCMAILADRSTCD=returned.LOANDTLSBCMAILADRSTCD,              
      master.LOANDTLSBCMAILADRZIPCD=returned.LOANDTLSBCMAILADRZIPCD,            
      master.LOANDTLSBCMAILADRZIP4CD=returned.LOANDTLSBCMAILADRZIP4CD,          
      master.LOANDTLTAXID=returned.LOANDTLTAXID,                                
      master.LOANDTLCDCREGNCD=returned.LOANDTLCDCREGNCD,                        
      master.LOANDTLCDCCERT=returned.LOANDTLCDCCERT,                            
      master.LOANDTLDEFPYMT=returned.LOANDTLDEFPYMT,                            
      master.LOANDTLDBENTRPRINAMT=returned.LOANDTLDBENTRPRINAMT,                
      master.LOANDTLDBENTRCURBALAMT=returned.LOANDTLDBENTRCURBALAMT,            
      master.LOANDTLDBENTRINTRTPCT=returned.LOANDTLDBENTRINTRTPCT,              
      master.LOANDTLISSDT=returned.LOANDTLISSDT,                                
      master.LOANDTLDEFPYMTDTX=returned.LOANDTLDEFPYMTDTX,                      
      master.LOANDTLDBENTRPYMTDTTRM=returned.LOANDTLDBENTRPYMTDTTRM,            
      master.LOANDTLDBENTRPROCDSAMT=returned.LOANDTLDBENTRPROCDSAMT,            
      master.LOANDTLDBENTRPROCDSESCROWAMT=returned.LOANDTLDBENTRPROCDSESCROWAMT,
      master.LOANDTLSEMIANNPYMTAMT=returned.LOANDTLSEMIANNPYMTAMT,              
      master.LOANDTLNOTEPRINAMT=returned.LOANDTLNOTEPRINAMT,                    
      master.LOANDTLNOTECURBALAMT=returned.LOANDTLNOTECURBALAMT,                
      master.LOANDTLNOTEINTRTPCT=returned.LOANDTLNOTEINTRTPCT,                  
      master.LOANDTLNOTEDT=returned.LOANDTLNOTEDT,                              
      master.LOANDTLACCTCD=returned.LOANDTLACCTCD,                              
      master.LOANDTLACCTCHNGCD=returned.LOANDTLACCTCHNGCD,                      
      master.LOANDTLREAMIND=returned.LOANDTLREAMIND,                            
      master.LOANDTLNOTEPYMTDTTRM=returned.LOANDTLNOTEPYMTDTTRM,                
      master.LOANDTLRESRVDEPAMT=returned.LOANDTLRESRVDEPAMT,                    
      master.LOANDTLUNDRWTRFEEPCT=returned.LOANDTLUNDRWTRFEEPCT,                
      master.LOANDTLUNDRWTRFEEMTDAMT=returned.LOANDTLUNDRWTRFEEMTDAMT,          
      master.LOANDTLUNDRWTRFEEYTDAMT=returned.LOANDTLUNDRWTRFEEYTDAMT,          
      master.LOANDTLUNDRWTRFEEPTDAMT=returned.LOANDTLUNDRWTRFEEPTDAMT,          
      master.LOANDTLCDCFEEPCT=returned.LOANDTLCDCFEEPCT,                        
      master.LOANDTLCDCFEEMTDAMT=returned.LOANDTLCDCFEEMTDAMT,                  
      master.LOANDTLCDCFEEYTDAMT=returned.LOANDTLCDCFEEYTDAMT,                  
      master.LOANDTLCDCFEEPTDAMT=returned.LOANDTLCDCFEEPTDAMT,                  
      master.LOANDTLCSAFEEPCT=returned.LOANDTLCSAFEEPCT,                        
      master.LOANDTLCSAFEEMTDAMT=returned.LOANDTLCSAFEEMTDAMT,                  
      master.LOANDTLCSAFEEYTDAMT=returned.LOANDTLCSAFEEYTDAMT,                  
      master.LOANDTLCSAFEEPTDAMT=returned.LOANDTLCSAFEEPTDAMT,                  
      master.LOANDTLATTORNEYFEEAMT=returned.LOANDTLATTORNEYFEEAMT,              
      master.LOANDTLINITFEEPCT=returned.LOANDTLINITFEEPCT,                      
      master.LOANDTLINITFEEDOLLRAMT=returned.LOANDTLINITFEEDOLLRAMT,            
      master.LOANDTLFUNDFEEPCT=returned.LOANDTLFUNDFEEPCT,                      
      master.LOANDTLFUNDFEEDOLLRAMT=returned.LOANDTLFUNDFEEDOLLRAMT,            
      master.LOANDTLCBNKNM=returned.LOANDTLCBNKNM,                              
      master.LOANDTLCBNKMAILADRSTR1NM=returned.LOANDTLCBNKMAILADRSTR1NM,        
      master.LOANDTLCBNKMAILADRSTR2NM=returned.LOANDTLCBNKMAILADRSTR2NM,        
      master.LOANDTLCBNKMAILADRCTYNM=returned.LOANDTLCBNKMAILADRCTYNM,          
      master.LOANDTLCBNKMAILADRSTCD=returned.LOANDTLCBNKMAILADRSTCD,            
      master.LOANDTLCBNKMAILADRZIPCD=returned.LOANDTLCBNKMAILADRZIPCD,          
      master.LOANDTLCBNKMAILADRZIP4CD=returned.LOANDTLCBNKMAILADRZIP4CD,        
      master.LOANDTLCACCTNM=returned.LOANDTLCACCTNM,                            
      master.LOANDTLCOLOANDTLACCT=returned.LOANDTLCOLOANDTLACCT,                
      master.LOANDTLCROUTSYM=returned.LOANDTLCROUTSYM,                          
      master.LOANDTLCTRANSCD=returned.LOANDTLCTRANSCD,                          
      master.LOANDTLCATTEN=returned.LOANDTLCATTEN,                              
      master.LOANDTLRBNKNM=returned.LOANDTLRBNKNM,                              
      master.LOANDTLRBNKMAILADRSTR1NM=returned.LOANDTLRBNKMAILADRSTR1NM,        
      master.LOANDTLRBNKMAILADRSTR2NM=returned.LOANDTLRBNKMAILADRSTR2NM,        
      master.LOANDTLRBNKMAILADRCTYNM=returned.LOANDTLRBNKMAILADRCTYNM,          
      master.LOANDTLRBNKMAILADRSTCD=returned.LOANDTLRBNKMAILADRSTCD,            
      master.LOANDTLRBNKMAILADRZIPCD=returned.LOANDTLRBNKMAILADRZIPCD,          
      master.LOANDTLRBNKMAILADRZIP4CD=returned.LOANDTLRBNKMAILADRZIP4CD,        
      master.LOANDTLRACCTNM=returned.LOANDTLRACCTNM,                            
      master.LOANDTLROLOANDTLACCT=returned.LOANDTLROLOANDTLACCT,                
      master.LOANDTLRROUTSYM=returned.LOANDTLRROUTSYM,                          
      master.LOANDTLTRANSCD=returned.LOANDTLTRANSCD,                            
      master.LOANDTLRATTEN=returned.LOANDTLRATTEN,                              
      master.LOANDTLPYMT1AMT=returned.LOANDTLPYMT1AMT,                          
      master.LOANDTLPYMTDT1X=returned.LOANDTLPYMTDT1X,                          
      master.LOANDTLPRIN1AMT=returned.LOANDTLPRIN1AMT,                          
      master.LOANDTLINT1AMT=returned.LOANDTLINT1AMT,                            
      master.LOANDTLCSA1PCT=returned.LOANDTLCSA1PCT,                            
      master.LOANDTLCSADOLLR1AMT=returned.LOANDTLCSADOLLR1AMT,                  
      master.LOANDTLCDC1PCT=returned.LOANDTLCDC1PCT,                            
      master.LOANDTLCDCDOLLR1AMT=returned.LOANDTLCDCDOLLR1AMT,                  
      master.LOANDTLSTMTNM=returned.LOANDTLSTMTNM,                              
      master.LOANDTLCURCSAFEEAMT=returned.LOANDTLCURCSAFEEAMT,                  
      master.LOANDTLCURCDCFEEAMT=returned.LOANDTLCURCDCFEEAMT,                  
      master.LOANDTLCURRESRVAMT=returned.LOANDTLCURRESRVAMT,                    
      master.LOANDTLCURFEEBALAMT=returned.LOANDTLCURFEEBALAMT,                  
      master.LOANDTLAMPRINLEFTAMT=returned.LOANDTLAMPRINLEFTAMT,                
      master.LOANDTLAMTHRUDTX=returned.LOANDTLAMTHRUDTX,                        
      master.LOANDTLAPPIND=returned.LOANDTLAPPIND,                              
      master.LOANDTLSPPIND=returned.LOANDTLSPPIND,                              
      master.LOANDTLSPPLQDAMT=returned.LOANDTLSPPLQDAMT,                        
      master.LOANDTLSPPAUTOPYMTAMT=returned.LOANDTLSPPAUTOPYMTAMT,              
      master.LOANDTLSPPAUTOPCT=returned.LOANDTLSPPAUTOPCT,                      
      master.LOANDTLSTMTINT1AMT=returned.LOANDTLSTMTINT1AMT,                    
      master.LOANDTLSTMTINT2AMT=returned.LOANDTLSTMTINT2AMT,                    
      master.LOANDTLSTMTINT3AMT=returned.LOANDTLSTMTINT3AMT,                    
      master.LOANDTLSTMTINT4AMT=returned.LOANDTLSTMTINT4AMT,                    
      master.LOANDTLSTMTINT5AMT=returned.LOANDTLSTMTINT5AMT,                    
      master.LOANDTLSTMTINT6AMT=returned.LOANDTLSTMTINT6AMT,                    
      master.LOANDTLSTMTINT7AMT=returned.LOANDTLSTMTINT7AMT,                    
      master.LOANDTLSTMTINT8AMT=returned.LOANDTLSTMTINT8AMT,                    
      master.LOANDTLSTMTINT9AMT=returned.LOANDTLSTMTINT9AMT,                    
      master.LOANDTLSTMTINT10AMT=returned.LOANDTLSTMTINT10AMT,                  
      master.LOANDTLSTMTINT11AMT=returned.LOANDTLSTMTINT11AMT,                  
      master.LOANDTLSTMTINT12AMT=returned.LOANDTLSTMTINT12AMT,                  
      master.LOANDTLSTMTINT13AMT=returned.LOANDTLSTMTINT13AMT,                  
      master.LOANDTLSTMTAVGMOBALAMT=returned.LOANDTLSTMTAVGMOBALAMT,            
      master.LOANDTLAMPRINAMT=returned.LOANDTLAMPRINAMT,                        
      master.LOANDTLAMINTAMT=returned.LOANDTLAMINTAMT,                          
      master.LOANDTLREFIIND=returned.LOANDTLREFIIND,                            
      master.LOANDTLSETUPIND=returned.LOANDTLSETUPIND,                          
      master.LOANDTLCREATDT=returned.LOANDTLCREATDT,                            
      master.LOANDTLLMAINTNDT=returned.LOANDTLLMAINTNDT,                        
      master.LOANDTLCONVDT=returned.LOANDTLCONVDT,                              
      master.LOANDTLPRGRM=returned.LOANDTLPRGRM,                                
      master.LOANDTLNOTEMOPYMTAMT=returned.LOANDTLNOTEMOPYMTAMT,                
      master.LOANDTLDBENTRMATDT=returned.LOANDTLDBENTRMATDT,                    
      master.LOANDTLNOTEMATDT=returned.LOANDTLNOTEMATDT,                        
      master.LOANDTLRESRVDEPPCT=returned.LOANDTLRESRVDEPPCT,                    
      master.LOANDTLCDCPFEEPCT=returned.LOANDTLCDCPFEEPCT,                      
      master.LOANDTLCDCPFEEDOLLRAMT=returned.LOANDTLCDCPFEEDOLLRAMT,            
      master.LOANDTLDUEBORRAMT=returned.LOANDTLDUEBORRAMT,                      
      master.LOANDTLAPPVDT=returned.LOANDTLAPPVDT,                              
      master.LOANDTLLTFEEIND=returned.LOANDTLLTFEEIND,                          
      master.LOANDTLACHPRENTIND=returned.LOANDTLACHPRENTIND,                    
      master.LOANDTLAMSCHDLTYP=returned.LOANDTLAMSCHDLTYP,                      
      master.LOANDTLACHBNKNM=returned.LOANDTLACHBNKNM,                          
      master.LOANDTLACHBNKMAILADRSTR1NM=returned.LOANDTLACHBNKMAILADRSTR1NM,    
      master.LOANDTLACHBNKMAILADRSTR2NM=returned.LOANDTLACHBNKMAILADRSTR2NM,    
      master.LOANDTLACHBNKMAILADRCTYNM=returned.LOANDTLACHBNKMAILADRCTYNM,      
      master.LOANDTLACHBNKMAILADRSTCD=returned.LOANDTLACHBNKMAILADRSTCD,        
      master.LOANDTLACHBNKMAILADRZIPCD=returned.LOANDTLACHBNKMAILADRZIPCD,      
      master.LOANDTLACHBNKMAILADRZIP4CD=returned.LOANDTLACHBNKMAILADRZIP4CD,    
      master.LOANDTLACHBNKBR=returned.LOANDTLACHBNKBR,                          
      master.LOANDTLACHBNKACTYP=returned.LOANDTLACHBNKACTYP,                    
      master.LOANDTLACHBNKACCT=returned.LOANDTLACHBNKACCT,                      
      master.LOANDTLACHBNKROUTNMB=returned.LOANDTLACHBNKROUTNMB,                
      master.LOANDTLACHBNKTRANS=returned.LOANDTLACHBNKTRANS,                    
      master.LOANDTLACHBNKIDNOORATTEN=returned.LOANDTLACHBNKIDNOORATTEN,        
      master.LOANDTLACHDEPNM=returned.LOANDTLACHDEPNM,                          
      master.LOANDTLACHSIGNDT=returned.LOANDTLACHSIGNDT,                        
      master.LOANDTLBORRNM2=returned.LOANDTLBORRNM2,                            
      master.LOANDTLSBCNM2=returned.LOANDTLSBCNM2,                              
      master.LOANDTLCACCTNMB=returned.LOANDTLCACCTNMB,                          
      master.LOANDTLRACCTNMB=returned.LOANDTLRACCTNMB,                          
      master.LOANDTLINTLENDRIND=returned.LOANDTLINTLENDRIND,                    
      master.LOANDTLINTRMLENDR=returned.LOANDTLINTRMLENDR,                      
      master.LOANDTLPYMTRECV=returned.LOANDTLPYMTRECV,                          
      master.LOANDTLTRMYR=returned.LOANDTLTRMYR,                                
      master.LOANDTLLOANTYP=returned.LOANDTLLOANTYP,                            
      master.LOANDTLMLACCTNMB=returned.LOANDTLMLACCTNMB,                        
      master.LOANDTLEXCESSDUEBORRAMT=returned.LOANDTLEXCESSDUEBORRAMT,          
      master.LOANDTL503SUBTYP=returned.LOANDTL503SUBTYP,                        
      master.LOANDTLLPYMTDT=returned.LOANDTLLPYMTDT,                            
      master.LOANDTLPOOLSERS=returned.LOANDTLPOOLSERS,                          
      master.LOANDTLSBAFEEPCT=returned.LOANDTLSBAFEEPCT,                        
      master.LOANDTLSBAPAIDTHRUDTX=returned.LOANDTLSBAPAIDTHRUDTX,              
      master.LOANDTLSBAFEEAMT=returned.LOANDTLSBAFEEAMT,                        
      master.LOANDTLPRMAMT=returned.LOANDTLPRMAMT,                              
      master.LOANDTLLENDRSBAFEEAMT=returned.LOANDTLLENDRSBAFEEAMT,              
      master.LOANDTLWITHHELOANDTLIND=returned.LOANDTLWITHHELOANDTLIND,          
      master.LOANDTLPRGRMTYP=returned.LOANDTLPRGRMTYP,                          
      master.LOANDTLLATEIND=returned.LOANDTLLATEIND,                            
      master.LOANDTLCMNT1=returned.LOANDTLCMNT1,                                
      master.LOANDTLCMNT2=returned.LOANDTLCMNT2,                                
      master.LOANDTLSOD=returned.LOANDTLSOD,                                    
      master.LOANDTLTOBECURAMT=returned.LOANDTLTOBECURAMT,                      
      master.LOANDTLGNTYREPAYPTDAMT=returned.LOANDTLGNTYREPAYPTDAMT,            
      master.LOANDTLCDCPAIDTHRUDTX=returned.LOANDTLCDCPAIDTHRUDTX,              
      master.LOANDTLCSAPAIDTHRUDTX=returned.LOANDTLCSAPAIDTHRUDTX,              
      master.LOANDTLINTPAIDTHRUDTX=returned.LOANDTLINTPAIDTHRUDTX,              
      master.LOANDTLSTRTBAL1AMT=returned.LOANDTLSTRTBAL1AMT,                    
      master.LOANDTLSTRTBAL2AMT=returned.LOANDTLSTRTBAL2AMT,                    
      master.LOANDTLSTRTBAL3AMT=returned.LOANDTLSTRTBAL3AMT,                    
      master.LOANDTLSTRTBAL4AMT=returned.LOANDTLSTRTBAL4AMT,                    
      master.LOANDTLSTRTBAL5AMT=returned.LOANDTLSTRTBAL5AMT,                    
      master.LOANDTLSBCIDNMB=returned.LOANDTLSBCIDNMB,                          
      master.LOANDTLACHLASTCHNGDT=returned.LOANDTLACHLASTCHNGDT,                
      master.LOANDTLPRENOTETESTDT=returned.LOANDTLPRENOTETESTDT,                
      master.LOANDTLPYMTNMBDUE=returned.LOANDTLPYMTNMBDUE,                      
      master.LOANDTLRESRVAMT=returned.LOANDTLRESRVAMT,                          
      master.LOANDTLFEEBASEAMT=returned.LOANDTLFEEBASEAMT,                      
      master.LOANDTLSTATCDOFLOAN=returned.LOANDTLSTATCDOFLOAN,                  
      master.LOANDTLACTUALCSAINITFEEAMT=returned.LOANDTLACTUALCSAINITFEEAMT,    
      master.LOANDTLSTATDT=returned.LOANDTLSTATDT,                              
      master.LOANDTLSTMTCLSBALAMT=returned.LOANDTLSTMTCLSBALAMT,                
      master.LOANDTLSTMTCLSDT=returned.LOANDTLSTMTCLSDT,                        
      master.LOANDTLLASTDBPAYOUTAMT=returned.LOANDTLLASTDBPAYOUTAMT,            
      master.LOANDTLCHEMICALBASISDAYS=returned.LOANDTLCHEMICALBASISDAYS,        
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      LOANDTLSBAOFC,                                                            
      LOANDTLBORRNM,                                                            
      LOANDTLBORRMAILADRSTR1NM,                                                 
      LOANDTLBORRMAILADRSTR2NM,                                                 
      LOANDTLBORRMAILADRCTYNM,                                                  
      LOANDTLBORRMAILADRSTCD,                                                   
      LOANDTLBORRMAILADRZIPCD,                                                  
      LOANDTLBORRMAILADRZIP4CD,                                                 
      LOANDTLSBCNM,                                                             
      LOANDTLSBCMAILADRSTR1NM,                                                  
      LOANDTLSBCMAILADRSTR2NM,                                                  
      LOANDTLSBCMAILADRCTYNM,                                                   
      LOANDTLSBCMAILADRSTCD,                                                    
      LOANDTLSBCMAILADRZIPCD,                                                   
      LOANDTLSBCMAILADRZIP4CD,                                                  
      LOANDTLTAXID,                                                             
      LOANDTLCDCREGNCD,                                                         
      LOANDTLCDCCERT,                                                           
      LOANDTLDEFPYMT,                                                           
      LOANDTLDBENTRPRINAMT,                                                     
      LOANDTLDBENTRCURBALAMT,                                                   
      LOANDTLDBENTRINTRTPCT,                                                    
      LOANDTLISSDT,                                                             
      LOANDTLDEFPYMTDTX,                                                        
      LOANDTLDBENTRPYMTDTTRM,                                                   
      LOANDTLDBENTRPROCDSAMT,                                                   
      LOANDTLDBENTRPROCDSESCROWAMT,                                             
      LOANDTLSEMIANNPYMTAMT,                                                    
      LOANDTLNOTEPRINAMT,                                                       
      LOANDTLNOTECURBALAMT,                                                     
      LOANDTLNOTEINTRTPCT,                                                      
      LOANDTLNOTEDT,                                                            
      LOANDTLACCTCD,                                                            
      LOANDTLACCTCHNGCD,                                                        
      LOANDTLREAMIND,                                                           
      LOANDTLNOTEPYMTDTTRM,                                                     
      LOANDTLRESRVDEPAMT,                                                       
      LOANDTLUNDRWTRFEEPCT,                                                     
      LOANDTLUNDRWTRFEEMTDAMT,                                                  
      LOANDTLUNDRWTRFEEYTDAMT,                                                  
      LOANDTLUNDRWTRFEEPTDAMT,                                                  
      LOANDTLCDCFEEPCT,                                                         
      LOANDTLCDCFEEMTDAMT,                                                      
      LOANDTLCDCFEEYTDAMT,                                                      
      LOANDTLCDCFEEPTDAMT,                                                      
      LOANDTLCSAFEEPCT,                                                         
      LOANDTLCSAFEEMTDAMT,                                                      
      LOANDTLCSAFEEYTDAMT,                                                      
      LOANDTLCSAFEEPTDAMT,                                                      
      LOANDTLATTORNEYFEEAMT,                                                    
      LOANDTLINITFEEPCT,                                                        
      LOANDTLINITFEEDOLLRAMT,                                                   
      LOANDTLFUNDFEEPCT,                                                        
      LOANDTLFUNDFEEDOLLRAMT,                                                   
      LOANDTLCBNKNM,                                                            
      LOANDTLCBNKMAILADRSTR1NM,                                                 
      LOANDTLCBNKMAILADRSTR2NM,                                                 
      LOANDTLCBNKMAILADRCTYNM,                                                  
      LOANDTLCBNKMAILADRSTCD,                                                   
      LOANDTLCBNKMAILADRZIPCD,                                                  
      LOANDTLCBNKMAILADRZIP4CD,                                                 
      LOANDTLCACCTNM,                                                           
      LOANDTLCOLOANDTLACCT,                                                     
      LOANDTLCROUTSYM,                                                          
      LOANDTLCTRANSCD,                                                          
      LOANDTLCATTEN,                                                            
      LOANDTLRBNKNM,                                                            
      LOANDTLRBNKMAILADRSTR1NM,                                                 
      LOANDTLRBNKMAILADRSTR2NM,                                                 
      LOANDTLRBNKMAILADRCTYNM,                                                  
      LOANDTLRBNKMAILADRSTCD,                                                   
      LOANDTLRBNKMAILADRZIPCD,                                                  
      LOANDTLRBNKMAILADRZIP4CD,                                                 
      LOANDTLRACCTNM,                                                           
      LOANDTLROLOANDTLACCT,                                                     
      LOANDTLRROUTSYM,                                                          
      LOANDTLTRANSCD,                                                           
      LOANDTLRATTEN,                                                            
      LOANDTLPYMT1AMT,                                                          
      LOANDTLPYMTDT1X,                                                          
      LOANDTLPRIN1AMT,                                                          
      LOANDTLINT1AMT,                                                           
      LOANDTLCSA1PCT,                                                           
      LOANDTLCSADOLLR1AMT,                                                      
      LOANDTLCDC1PCT,                                                           
      LOANDTLCDCDOLLR1AMT,                                                      
      LOANDTLSTMTNM,                                                            
      LOANDTLCURCSAFEEAMT,                                                      
      LOANDTLCURCDCFEEAMT,                                                      
      LOANDTLCURRESRVAMT,                                                       
      LOANDTLCURFEEBALAMT,                                                      
      LOANDTLAMPRINLEFTAMT,                                                     
      LOANDTLAMTHRUDTX,                                                         
      LOANDTLAPPIND,                                                            
      LOANDTLSPPIND,                                                            
      LOANDTLSPPLQDAMT,                                                         
      LOANDTLSPPAUTOPYMTAMT,                                                    
      LOANDTLSPPAUTOPCT,                                                        
      LOANDTLSTMTINT1AMT,                                                       
      LOANDTLSTMTINT2AMT,                                                       
      LOANDTLSTMTINT3AMT,                                                       
      LOANDTLSTMTINT4AMT,                                                       
      LOANDTLSTMTINT5AMT,                                                       
      LOANDTLSTMTINT6AMT,                                                       
      LOANDTLSTMTINT7AMT,                                                       
      LOANDTLSTMTINT8AMT,                                                       
      LOANDTLSTMTINT9AMT,                                                       
      LOANDTLSTMTINT10AMT,                                                      
      LOANDTLSTMTINT11AMT,                                                      
      LOANDTLSTMTINT12AMT,                                                      
      LOANDTLSTMTINT13AMT,                                                      
      LOANDTLSTMTAVGMOBALAMT,                                                   
      LOANDTLAMPRINAMT,                                                         
      LOANDTLAMINTAMT,                                                          
      LOANDTLREFIIND,                                                           
      LOANDTLSETUPIND,                                                          
      LOANDTLCREATDT,                                                           
      LOANDTLLMAINTNDT,                                                         
      LOANDTLCONVDT,                                                            
      LOANDTLPRGRM,                                                             
      LOANDTLNOTEMOPYMTAMT,                                                     
      LOANDTLDBENTRMATDT,                                                       
      LOANDTLNOTEMATDT,                                                         
      LOANDTLRESRVDEPPCT,                                                       
      LOANDTLCDCPFEEPCT,                                                        
      LOANDTLCDCPFEEDOLLRAMT,                                                   
      LOANDTLDUEBORRAMT,                                                        
      LOANDTLAPPVDT,                                                            
      LOANDTLLTFEEIND,                                                          
      LOANDTLACHPRENTIND,                                                       
      LOANDTLAMSCHDLTYP,                                                        
      LOANDTLACHBNKNM,                                                          
      LOANDTLACHBNKMAILADRSTR1NM,                                               
      LOANDTLACHBNKMAILADRSTR2NM,                                               
      LOANDTLACHBNKMAILADRCTYNM,                                                
      LOANDTLACHBNKMAILADRSTCD,                                                 
      LOANDTLACHBNKMAILADRZIPCD,                                                
      LOANDTLACHBNKMAILADRZIP4CD,                                               
      LOANDTLACHBNKBR,                                                          
      LOANDTLACHBNKACTYP,                                                       
      LOANDTLACHBNKACCT,                                                        
      LOANDTLACHBNKROUTNMB,                                                     
      LOANDTLACHBNKTRANS,                                                       
      LOANDTLACHBNKIDNOORATTEN,                                                 
      LOANDTLACHDEPNM,                                                          
      LOANDTLACHSIGNDT,                                                         
      LOANDTLBORRNM2,                                                           
      LOANDTLSBCNM2,                                                            
      LOANDTLCACCTNMB,                                                          
      LOANDTLRACCTNMB,                                                          
      LOANDTLINTLENDRIND,                                                       
      LOANDTLINTRMLENDR,                                                        
      LOANDTLPYMTRECV,                                                          
      LOANDTLTRMYR,                                                             
      LOANDTLLOANTYP,                                                           
      LOANDTLMLACCTNMB,                                                         
      LOANDTLEXCESSDUEBORRAMT,                                                  
      LOANDTL503SUBTYP,                                                         
      LOANDTLLPYMTDT,                                                           
      LOANDTLPOOLSERS,                                                          
      LOANDTLSBAFEEPCT,                                                         
      LOANDTLSBAPAIDTHRUDTX,                                                    
      LOANDTLSBAFEEAMT,                                                         
      LOANDTLPRMAMT,                                                            
      LOANDTLLENDRSBAFEEAMT,                                                    
      LOANDTLWITHHELOANDTLIND,                                                  
      LOANDTLPRGRMTYP,                                                          
      LOANDTLLATEIND,                                                           
      LOANDTLCMNT1,                                                             
      LOANDTLCMNT2,                                                             
      LOANDTLSOD,                                                               
      LOANDTLTOBECURAMT,                                                        
      LOANDTLGNTYREPAYPTDAMT,                                                   
      LOANDTLCDCPAIDTHRUDTX,                                                    
      LOANDTLCSAPAIDTHRUDTX,                                                    
      LOANDTLINTPAIDTHRUDTX,                                                    
      LOANDTLSTRTBAL1AMT,                                                       
      LOANDTLSTRTBAL2AMT,                                                       
      LOANDTLSTRTBAL3AMT,                                                       
      LOANDTLSTRTBAL4AMT,                                                       
      LOANDTLSTRTBAL5AMT,                                                       
      LOANDTLSBCIDNMB,                                                          
      LOANDTLACHLASTCHNGDT,                                                     
      LOANDTLPRENOTETESTDT,                                                     
      LOANDTLPYMTNMBDUE,                                                        
      LOANDTLRESRVAMT,                                                          
      LOANDTLFEEBASEAMT,                                                        
      LOANDTLSTATCDOFLOAN,                                                      
      LOANDTLACTUALCSAINITFEEAMT,                                               
      LOANDTLSTATDT,                                                            
      LOANDTLSTMTCLSBALAMT,                                                     
      LOANDTLSTMTCLSDT,                                                         
      LOANDTLLASTDBPAYOUTAMT,                                                   
      LOANDTLCHEMICALBASISDAYS,                                                 
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.LOANDTLSBAOFC,                                                   
      returned.LOANDTLBORRNM,                                                   
      returned.LOANDTLBORRMAILADRSTR1NM,                                        
      returned.LOANDTLBORRMAILADRSTR2NM,                                        
      returned.LOANDTLBORRMAILADRCTYNM,                                         
      returned.LOANDTLBORRMAILADRSTCD,                                          
      returned.LOANDTLBORRMAILADRZIPCD,                                         
      returned.LOANDTLBORRMAILADRZIP4CD,                                        
      returned.LOANDTLSBCNM,                                                    
      returned.LOANDTLSBCMAILADRSTR1NM,                                         
      returned.LOANDTLSBCMAILADRSTR2NM,                                         
      returned.LOANDTLSBCMAILADRCTYNM,                                          
      returned.LOANDTLSBCMAILADRSTCD,                                           
      returned.LOANDTLSBCMAILADRZIPCD,                                          
      returned.LOANDTLSBCMAILADRZIP4CD,                                         
      returned.LOANDTLTAXID,                                                    
      returned.LOANDTLCDCREGNCD,                                                
      returned.LOANDTLCDCCERT,                                                  
      returned.LOANDTLDEFPYMT,                                                  
      returned.LOANDTLDBENTRPRINAMT,                                            
      returned.LOANDTLDBENTRCURBALAMT,                                          
      returned.LOANDTLDBENTRINTRTPCT,                                           
      returned.LOANDTLISSDT,                                                    
      returned.LOANDTLDEFPYMTDTX,                                               
      returned.LOANDTLDBENTRPYMTDTTRM,                                          
      returned.LOANDTLDBENTRPROCDSAMT,                                          
      returned.LOANDTLDBENTRPROCDSESCROWAMT,                                    
      returned.LOANDTLSEMIANNPYMTAMT,                                           
      returned.LOANDTLNOTEPRINAMT,                                              
      returned.LOANDTLNOTECURBALAMT,                                            
      returned.LOANDTLNOTEINTRTPCT,                                             
      returned.LOANDTLNOTEDT,                                                   
      returned.LOANDTLACCTCD,                                                   
      returned.LOANDTLACCTCHNGCD,                                               
      returned.LOANDTLREAMIND,                                                  
      returned.LOANDTLNOTEPYMTDTTRM,                                            
      returned.LOANDTLRESRVDEPAMT,                                              
      returned.LOANDTLUNDRWTRFEEPCT,                                            
      returned.LOANDTLUNDRWTRFEEMTDAMT,                                         
      returned.LOANDTLUNDRWTRFEEYTDAMT,                                         
      returned.LOANDTLUNDRWTRFEEPTDAMT,                                         
      returned.LOANDTLCDCFEEPCT,                                                
      returned.LOANDTLCDCFEEMTDAMT,                                             
      returned.LOANDTLCDCFEEYTDAMT,                                             
      returned.LOANDTLCDCFEEPTDAMT,                                             
      returned.LOANDTLCSAFEEPCT,                                                
      returned.LOANDTLCSAFEEMTDAMT,                                             
      returned.LOANDTLCSAFEEYTDAMT,                                             
      returned.LOANDTLCSAFEEPTDAMT,                                             
      returned.LOANDTLATTORNEYFEEAMT,                                           
      returned.LOANDTLINITFEEPCT,                                               
      returned.LOANDTLINITFEEDOLLRAMT,                                          
      returned.LOANDTLFUNDFEEPCT,                                               
      returned.LOANDTLFUNDFEEDOLLRAMT,                                          
      returned.LOANDTLCBNKNM,                                                   
      returned.LOANDTLCBNKMAILADRSTR1NM,                                        
      returned.LOANDTLCBNKMAILADRSTR2NM,                                        
      returned.LOANDTLCBNKMAILADRCTYNM,                                         
      returned.LOANDTLCBNKMAILADRSTCD,                                          
      returned.LOANDTLCBNKMAILADRZIPCD,                                         
      returned.LOANDTLCBNKMAILADRZIP4CD,                                        
      returned.LOANDTLCACCTNM,                                                  
      returned.LOANDTLCOLOANDTLACCT,                                            
      returned.LOANDTLCROUTSYM,                                                 
      returned.LOANDTLCTRANSCD,                                                 
      returned.LOANDTLCATTEN,                                                   
      returned.LOANDTLRBNKNM,                                                   
      returned.LOANDTLRBNKMAILADRSTR1NM,                                        
      returned.LOANDTLRBNKMAILADRSTR2NM,                                        
      returned.LOANDTLRBNKMAILADRCTYNM,                                         
      returned.LOANDTLRBNKMAILADRSTCD,                                          
      returned.LOANDTLRBNKMAILADRZIPCD,                                         
      returned.LOANDTLRBNKMAILADRZIP4CD,                                        
      returned.LOANDTLRACCTNM,                                                  
      returned.LOANDTLROLOANDTLACCT,                                            
      returned.LOANDTLRROUTSYM,                                                 
      returned.LOANDTLTRANSCD,                                                  
      returned.LOANDTLRATTEN,                                                   
      returned.LOANDTLPYMT1AMT,                                                 
      returned.LOANDTLPYMTDT1X,                                                 
      returned.LOANDTLPRIN1AMT,                                                 
      returned.LOANDTLINT1AMT,                                                  
      returned.LOANDTLCSA1PCT,                                                  
      returned.LOANDTLCSADOLLR1AMT,                                             
      returned.LOANDTLCDC1PCT,                                                  
      returned.LOANDTLCDCDOLLR1AMT,                                             
      returned.LOANDTLSTMTNM,                                                   
      returned.LOANDTLCURCSAFEEAMT,                                             
      returned.LOANDTLCURCDCFEEAMT,                                             
      returned.LOANDTLCURRESRVAMT,                                              
      returned.LOANDTLCURFEEBALAMT,                                             
      returned.LOANDTLAMPRINLEFTAMT,                                            
      returned.LOANDTLAMTHRUDTX,                                                
      returned.LOANDTLAPPIND,                                                   
      returned.LOANDTLSPPIND,                                                   
      returned.LOANDTLSPPLQDAMT,                                                
      returned.LOANDTLSPPAUTOPYMTAMT,                                           
      returned.LOANDTLSPPAUTOPCT,                                               
      returned.LOANDTLSTMTINT1AMT,                                              
      returned.LOANDTLSTMTINT2AMT,                                              
      returned.LOANDTLSTMTINT3AMT,                                              
      returned.LOANDTLSTMTINT4AMT,                                              
      returned.LOANDTLSTMTINT5AMT,                                              
      returned.LOANDTLSTMTINT6AMT,                                              
      returned.LOANDTLSTMTINT7AMT,                                              
      returned.LOANDTLSTMTINT8AMT,                                              
      returned.LOANDTLSTMTINT9AMT,                                              
      returned.LOANDTLSTMTINT10AMT,                                             
      returned.LOANDTLSTMTINT11AMT,                                             
      returned.LOANDTLSTMTINT12AMT,                                             
      returned.LOANDTLSTMTINT13AMT,                                             
      returned.LOANDTLSTMTAVGMOBALAMT,                                          
      returned.LOANDTLAMPRINAMT,                                                
      returned.LOANDTLAMINTAMT,                                                 
      returned.LOANDTLREFIIND,                                                  
      returned.LOANDTLSETUPIND,                                                 
      returned.LOANDTLCREATDT,                                                  
      returned.LOANDTLLMAINTNDT,                                                
      returned.LOANDTLCONVDT,                                                   
      returned.LOANDTLPRGRM,                                                    
      returned.LOANDTLNOTEMOPYMTAMT,                                            
      returned.LOANDTLDBENTRMATDT,                                              
      returned.LOANDTLNOTEMATDT,                                                
      returned.LOANDTLRESRVDEPPCT,                                              
      returned.LOANDTLCDCPFEEPCT,                                               
      returned.LOANDTLCDCPFEEDOLLRAMT,                                          
      returned.LOANDTLDUEBORRAMT,                                               
      returned.LOANDTLAPPVDT,                                                   
      returned.LOANDTLLTFEEIND,                                                 
      returned.LOANDTLACHPRENTIND,                                              
      returned.LOANDTLAMSCHDLTYP,                                               
      returned.LOANDTLACHBNKNM,                                                 
      returned.LOANDTLACHBNKMAILADRSTR1NM,                                      
      returned.LOANDTLACHBNKMAILADRSTR2NM,                                      
      returned.LOANDTLACHBNKMAILADRCTYNM,                                       
      returned.LOANDTLACHBNKMAILADRSTCD,                                        
      returned.LOANDTLACHBNKMAILADRZIPCD,                                       
      returned.LOANDTLACHBNKMAILADRZIP4CD,                                      
      returned.LOANDTLACHBNKBR,                                                 
      returned.LOANDTLACHBNKACTYP,                                              
      returned.LOANDTLACHBNKACCT,                                               
      returned.LOANDTLACHBNKROUTNMB,                                            
      returned.LOANDTLACHBNKTRANS,                                              
      returned.LOANDTLACHBNKIDNOORATTEN,                                        
      returned.LOANDTLACHDEPNM,                                                 
      returned.LOANDTLACHSIGNDT,                                                
      returned.LOANDTLBORRNM2,                                                  
      returned.LOANDTLSBCNM2,                                                   
      returned.LOANDTLCACCTNMB,                                                 
      returned.LOANDTLRACCTNMB,                                                 
      returned.LOANDTLINTLENDRIND,                                              
      returned.LOANDTLINTRMLENDR,                                               
      returned.LOANDTLPYMTRECV,                                                 
      returned.LOANDTLTRMYR,                                                    
      returned.LOANDTLLOANTYP,                                                  
      returned.LOANDTLMLACCTNMB,                                                
      returned.LOANDTLEXCESSDUEBORRAMT,                                         
      returned.LOANDTL503SUBTYP,                                                
      returned.LOANDTLLPYMTDT,                                                  
      returned.LOANDTLPOOLSERS,                                                 
      returned.LOANDTLSBAFEEPCT,                                                
      returned.LOANDTLSBAPAIDTHRUDTX,                                           
      returned.LOANDTLSBAFEEAMT,                                                
      returned.LOANDTLPRMAMT,                                                   
      returned.LOANDTLLENDRSBAFEEAMT,                                           
      returned.LOANDTLWITHHELOANDTLIND,                                         
      returned.LOANDTLPRGRMTYP,                                                 
      returned.LOANDTLLATEIND,                                                  
      returned.LOANDTLCMNT1,                                                    
      returned.LOANDTLCMNT2,                                                    
      returned.LOANDTLSOD,                                                      
      returned.LOANDTLTOBECURAMT,                                               
      returned.LOANDTLGNTYREPAYPTDAMT,                                          
      returned.LOANDTLCDCPAIDTHRUDTX,                                           
      returned.LOANDTLCSAPAIDTHRUDTX,                                           
      returned.LOANDTLINTPAIDTHRUDTX,                                           
      returned.LOANDTLSTRTBAL1AMT,                                              
      returned.LOANDTLSTRTBAL2AMT,                                              
      returned.LOANDTLSTRTBAL3AMT,                                              
      returned.LOANDTLSTRTBAL4AMT,                                              
      returned.LOANDTLSTRTBAL5AMT,                                              
      returned.LOANDTLSBCIDNMB,                                                 
      returned.LOANDTLACHLASTCHNGDT,                                            
      returned.LOANDTLPRENOTETESTDT,                                            
      returned.LOANDTLPYMTNMBDUE,                                               
      returned.LOANDTLRESRVAMT,                                                 
      returned.LOANDTLFEEBASEAMT,                                               
      returned.LOANDTLSTATCDOFLOAN,                                             
      returned.LOANDTLACTUALCSAINITFEEAMT,                                      
      returned.LOANDTLSTATDT,                                                   
      returned.LOANDTLSTMTCLSBALAMT,                                            
      returned.LOANDTLSTMTCLSDT,                                                
      returned.LOANDTLLASTDBPAYOUTAMT,                                          
      returned.LOANDTLCHEMICALBASISDAYS,                                        
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                   
  msgtxt:=prog||' Finished SOFVLND1TBL, rowcount='||sql%rowcount||          
    ' Next SOFVBGDFTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVBGDFTBL                                                 
  merge into CSA.SOFVBGDFTBL master                                          
    using (select * from CSA.XOVER_SOFVBGDFTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on ( master.DEFRMNTGPKEY=returned.DEFRMNTGPKEY )                                  
    when matched then update set                           
      master.DEFRMNTSTRTDT1=returned.DEFRMNTSTRTDT1,                            
      master.DEFRMNTENDDT1=returned.DEFRMNTENDDT1,                              
      master.DEFRMNTREQ1AMT=returned.DEFRMNTREQ1AMT,                            
      master.DEFRMNTSTRTDT2=returned.DEFRMNTSTRTDT2,                            
      master.DEFRMNTENDDT2=returned.DEFRMNTENDDT2,                              
      master.DEFRMNTREQ2AMT=returned.DEFRMNTREQ2AMT,                            
      master.DEFRMNTSTRTDT3=returned.DEFRMNTSTRTDT3,                            
      master.DEFRMNTENDDT3=returned.DEFRMNTENDDT3,                              
      master.DEFRMNTREQ3AMT=returned.DEFRMNTREQ3AMT,                            
      master.DEFRMNTSTRTDT4=returned.DEFRMNTSTRTDT4,                            
      master.DEFRMNTENDDT4=returned.DEFRMNTENDDT4,                              
      master.DEFRMNTREQ4AMT=returned.DEFRMNTREQ4AMT,                            
      master.DEFRMNTSTRTDT5=returned.DEFRMNTSTRTDT5,                            
      master.DEFRMNTENDDT5=returned.DEFRMNTENDDT5,                              
      master.DEFRMNTREQ5AMT=returned.DEFRMNTREQ5AMT,                            
      master.DEFRMNTSTRTDT6=returned.DEFRMNTSTRTDT6,                            
      master.DEFRMNTENDDT6=returned.DEFRMNTENDDT6,                              
      master.DEFRMNTREQ6AMT=returned.DEFRMNTREQ6AMT,                            
      master.DEFRMNTSTRTDT7=returned.DEFRMNTSTRTDT7,                            
      master.DEFRMNTENDDT7=returned.DEFRMNTENDDT7,                              
      master.DEFRMNTREQ7AMT=returned.DEFRMNTREQ7AMT,                            
      master.DEFRMNTSTRTDT8=returned.DEFRMNTSTRTDT8,                            
      master.DEFRMNTENDDT8=returned.DEFRMNTENDDT8,                              
      master.DEFRMNTREQ8AMT=returned.DEFRMNTREQ8AMT,                            
      master.DEFRMNTSTRTDT9=returned.DEFRMNTSTRTDT9,                            
      master.DEFRMNTENDDT9=returned.DEFRMNTENDDT9,                              
      master.DEFRMNTREQ9AMT=returned.DEFRMNTREQ9AMT,                            
      master.DEFRMNTSTRTDT10=returned.DEFRMNTSTRTDT10,                          
      master.DEFRMNTENDDT10=returned.DEFRMNTENDDT10,                            
      master.DEFRMNTREQ10AMT=returned.DEFRMNTREQ10AMT,                          
      master.DEFRMNTSTRTDT11=returned.DEFRMNTSTRTDT11,                          
      master.DEFRMNTENDDT11=returned.DEFRMNTENDDT11,                            
      master.DEFRMNTREQ11AMT=returned.DEFRMNTREQ11AMT,                          
      master.DEFRMNTSTRTDT12=returned.DEFRMNTSTRTDT12,                          
      master.DEFRMNTENDDT12=returned.DEFRMNTENDDT12,                            
      master.DEFRMNTREQ12AMT=returned.DEFRMNTREQ12AMT,                          
      master.DEFRMNTSTRTDT13=returned.DEFRMNTSTRTDT13,                          
      master.DEFRMNTENDDT13=returned.DEFRMNTENDDT13,                            
      master.DEFRMNTREQ13AMT=returned.DEFRMNTREQ13AMT,                          
      master.DEFRMNTSTRTDT14=returned.DEFRMNTSTRTDT14,                          
      master.DEFRMNTENDDT14=returned.DEFRMNTENDDT14,                            
      master.DEFRMNTREQ14AMT=returned.DEFRMNTREQ14AMT,                          
      master.DEFRMNTSTRTDT15=returned.DEFRMNTSTRTDT15,                          
      master.DEFRMNTENDDT15=returned.DEFRMNTENDDT15,                            
      master.DEFRMNTREQ15AMT=returned.DEFRMNTREQ15AMT,                          
      master.DEFRMNTDTCREAT=returned.DEFRMNTDTCREAT,                            
      master.DEFRMNTDTMOD=returned.DEFRMNTDTMOD,                                
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (DEFRMNTGPKEY,                                                             
      DEFRMNTSTRTDT1,                                                           
      DEFRMNTENDDT1,                                                            
      DEFRMNTREQ1AMT,                                                           
      DEFRMNTSTRTDT2,                                                           
      DEFRMNTENDDT2,                                                            
      DEFRMNTREQ2AMT,                                                           
      DEFRMNTSTRTDT3,                                                           
      DEFRMNTENDDT3,                                                            
      DEFRMNTREQ3AMT,                                                           
      DEFRMNTSTRTDT4,                                                           
      DEFRMNTENDDT4,                                                            
      DEFRMNTREQ4AMT,                                                           
      DEFRMNTSTRTDT5,                                                           
      DEFRMNTENDDT5,                                                            
      DEFRMNTREQ5AMT,                                                           
      DEFRMNTSTRTDT6,                                                           
      DEFRMNTENDDT6,                                                            
      DEFRMNTREQ6AMT,                                                           
      DEFRMNTSTRTDT7,                                                           
      DEFRMNTENDDT7,                                                            
      DEFRMNTREQ7AMT,                                                           
      DEFRMNTSTRTDT8,                                                           
      DEFRMNTENDDT8,                                                            
      DEFRMNTREQ8AMT,                                                           
      DEFRMNTSTRTDT9,                                                           
      DEFRMNTENDDT9,                                                            
      DEFRMNTREQ9AMT,                                                           
      DEFRMNTSTRTDT10,                                                          
      DEFRMNTENDDT10,                                                           
      DEFRMNTREQ10AMT,                                                          
      DEFRMNTSTRTDT11,                                                          
      DEFRMNTENDDT11,                                                           
      DEFRMNTREQ11AMT,                                                          
      DEFRMNTSTRTDT12,                                                          
      DEFRMNTENDDT12,                                                           
      DEFRMNTREQ12AMT,                                                          
      DEFRMNTSTRTDT13,                                                          
      DEFRMNTENDDT13,                                                           
      DEFRMNTREQ13AMT,                                                          
      DEFRMNTSTRTDT14,                                                          
      DEFRMNTENDDT14,                                                           
      DEFRMNTREQ14AMT,                                                          
      DEFRMNTSTRTDT15,                                                          
      DEFRMNTENDDT15,                                                           
      DEFRMNTREQ15AMT,                                                          
      DEFRMNTDTCREAT,                                                           
      DEFRMNTDTMOD,                                                             
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.DEFRMNTGPKEY,                                                    
      returned.DEFRMNTSTRTDT1,                                                  
      returned.DEFRMNTENDDT1,                                                   
      returned.DEFRMNTREQ1AMT,                                                  
      returned.DEFRMNTSTRTDT2,                                                  
      returned.DEFRMNTENDDT2,                                                   
      returned.DEFRMNTREQ2AMT,                                                  
      returned.DEFRMNTSTRTDT3,                                                  
      returned.DEFRMNTENDDT3,                                                   
      returned.DEFRMNTREQ3AMT,                                                  
      returned.DEFRMNTSTRTDT4,                                                  
      returned.DEFRMNTENDDT4,                                                   
      returned.DEFRMNTREQ4AMT,                                                  
      returned.DEFRMNTSTRTDT5,                                                  
      returned.DEFRMNTENDDT5,                                                   
      returned.DEFRMNTREQ5AMT,                                                  
      returned.DEFRMNTSTRTDT6,                                                  
      returned.DEFRMNTENDDT6,                                                   
      returned.DEFRMNTREQ6AMT,                                                  
      returned.DEFRMNTSTRTDT7,                                                  
      returned.DEFRMNTENDDT7,                                                   
      returned.DEFRMNTREQ7AMT,                                                  
      returned.DEFRMNTSTRTDT8,                                                  
      returned.DEFRMNTENDDT8,                                                   
      returned.DEFRMNTREQ8AMT,                                                  
      returned.DEFRMNTSTRTDT9,                                                  
      returned.DEFRMNTENDDT9,                                                   
      returned.DEFRMNTREQ9AMT,                                                  
      returned.DEFRMNTSTRTDT10,                                                 
      returned.DEFRMNTENDDT10,                                                  
      returned.DEFRMNTREQ10AMT,                                                 
      returned.DEFRMNTSTRTDT11,                                                 
      returned.DEFRMNTENDDT11,                                                  
      returned.DEFRMNTREQ11AMT,                                                 
      returned.DEFRMNTSTRTDT12,                                                 
      returned.DEFRMNTENDDT12,                                                  
      returned.DEFRMNTREQ12AMT,                                                 
      returned.DEFRMNTSTRTDT13,                                                 
      returned.DEFRMNTENDDT13,                                                  
      returned.DEFRMNTREQ13AMT,                                                 
      returned.DEFRMNTSTRTDT14,                                                 
      returned.DEFRMNTENDDT14,                                                  
      returned.DEFRMNTREQ14AMT,                                                 
      returned.DEFRMNTSTRTDT15,                                                 
      returned.DEFRMNTENDDT15,                                                  
      returned.DEFRMNTREQ15AMT,                                                 
      returned.DEFRMNTDTCREAT,                                                  
      returned.DEFRMNTDTMOD,                                                    
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                     
  msgtxt:=prog||' Finished SOFVBGDFTBL, rowcount='||sql%rowcount||          
    ' Next SOFVOTRNTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVOTRNTBL                                                 
  merge into CSA.SOFVOTRNTBL master                                          
    using (select * from CSA.XOVER_SOFVOTRNTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB 
      and master.ONLNTRANSTRANSDT=returned.ONLNTRANSTRANSDT
      and master.ONLNTRANSTRMNLID=returned.ONLNTRANSTRMNLID
      and master.ONLNTRANSOPRTRINITIAL=returned.ONLNTRANSOPRTRINITIAL)                                  
    when matched then update set                           
      master.ONLNTRANSFILENM=returned.ONLNTRANSFILENM,                          
      master.ONLNTRANSSCN=returned.ONLNTRANSSCN,                                
      master.ONLNTRANSFIL10=returned.ONLNTRANSFIL10,                            
      master.ONLNTRANSDETPYMTDT=returned.ONLNTRANSDETPYMTDT,                    
      master.ONLNTRANSDTLREST=returned.ONLNTRANSDTLREST,                        
      master.ONLNTRANSDTLREST5=returned.ONLNTRANSDTLREST5,                      
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      ONLNTRANSTRANSDT,                                                         
      ONLNTRANSTRMNLID,                                                         
      ONLNTRANSOPRTRINITIAL,                                                    
      ONLNTRANSFILENM,                                                          
      ONLNTRANSSCN,                                                             
      ONLNTRANSFIL10,                                                           
      ONLNTRANSDETPYMTDT,                                                       
      ONLNTRANSDTLREST,                                                         
      ONLNTRANSDTLREST5,                                                        
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.ONLNTRANSTRANSDT,                                                
      returned.ONLNTRANSTRMNLID,                                                
      returned.ONLNTRANSOPRTRINITIAL,                                           
      returned.ONLNTRANSFILENM,                                                 
      returned.ONLNTRANSSCN,                                                    
      returned.ONLNTRANSFIL10,                                                  
      returned.ONLNTRANSDETPYMTDT,                                              
      returned.ONLNTRANSDTLREST,                                                
      returned.ONLNTRANSDTLREST5,                                               
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                    
  msgtxt:=prog||' Finished SOFVOTRNTBL, rowcount='||sql%rowcount||          
    ' Next SOFVLND2TBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVLND2TBL                                                 
  merge into CSA.SOFVLND2TBL master                                          
    using (select * from CSA.XOVER_SOFVLND2TBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB)                                  
    when matched then update set                           
      master.LD2ACHBNKACCT=returned.LD2ACHBNKACCT,                              
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      LD2ACHBNKACCT,                                                            
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.LD2ACHBNKACCT,                                                   
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                     
  msgtxt:=prog||' Finished SOFVLND2TBL, rowcount='||sql%rowcount||          
    ' Next SOFVRTSCTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVRTSCTBL                                                 
  merge into CSA.SOFVRTSCTBL master                                          
    using (select * from CSA.XOVER_SOFVRTSCTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.PRGMRTFEEDESC=returned.PRGMRTFEEDESC
      and master.PRGMRTENDDT=returned.PRGMRTENDDT)                                  
    when matched then update set                           
      master.PRGMRTBASIS=returned.PRGMRTBASIS,                                  
      master.PRGMRTSTARTDT=returned.PRGMRTSTARTDT,                              
      master.PRGMRTPCT=returned.PRGMRTPCT,                                      
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (PRGMRTBASIS,                                                              
      PRGMRTFEEDESC,                                                            
      PRGMRTENDDT,                                                              
      PRGMRTSTARTDT,                                                            
      PRGMRTPCT,                                                                
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.PRGMRTBASIS,                                                     
      returned.PRGMRTFEEDESC,                                                   
      returned.PRGMRTENDDT,                                                     
      returned.PRGMRTSTARTDT,                                                   
      returned.PRGMRTPCT,                                                       
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                     
  msgtxt:=prog||' Finished SOFVRTSCTBL, rowcount='||sql%rowcount||          
    ' Next SOFVCTCHTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVCTCHTBL                                                 
  merge into CSA.SOFVCTCHTBL master                                          
    using (select * from CSA.XOVER_SOFVCTCHTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.CATCHUPPLANSBANMB=returned.CATCHUPPLANSBANMB)                                  
    when matched then update set                           
      master.CATCHUPPLANSTRTDT1=returned.CATCHUPPLANSTRTDT1,                    
      master.CATCHUPPLANENDDT1=returned.CATCHUPPLANENDDT1,                      
      master.CATCHUPPLANRQ1AMT=returned.CATCHUPPLANRQ1AMT,                      
      master.CATCHUPPLANSTRTDT2=returned.CATCHUPPLANSTRTDT2,                    
      master.CATCHUPPLANENDDT2=returned.CATCHUPPLANENDDT2,                      
      master.CATCHUPPLANRQ2AMT=returned.CATCHUPPLANRQ2AMT,                      
      master.CATCHUPPLANSTRTDT3=returned.CATCHUPPLANSTRTDT3,                    
      master.CATCHUPPLANENDDT3=returned.CATCHUPPLANENDDT3,                      
      master.CATCHUPPLANRQ3AMT=returned.CATCHUPPLANRQ3AMT,                      
      master.CATCHUPPLANSTRTDT4=returned.CATCHUPPLANSTRTDT4,                    
      master.CATCHUPPLANENDDT4=returned.CATCHUPPLANENDDT4,                      
      master.CATCHUPPLANRQ4AMT=returned.CATCHUPPLANRQ4AMT,                      
      master.CATCHUPPLANSTRTDT5=returned.CATCHUPPLANSTRTDT5,                    
      master.CATCHUPPLANENDDT5=returned.CATCHUPPLANENDDT5,                      
      master.CATCHUPPLANRQ5AMT=returned.CATCHUPPLANRQ5AMT,                      
      master.CATCHUPPLANSTRTDT6=returned.CATCHUPPLANSTRTDT6,                    
      master.CATCHUPPLANENDDT6=returned.CATCHUPPLANENDDT6,                      
      master.CATCHUPPLANRQ6AMT=returned.CATCHUPPLANRQ6AMT,                      
      master.CATCHUPPLANSTRTDT7=returned.CATCHUPPLANSTRTDT7,                    
      master.CATCHUPPLANENDDT7=returned.CATCHUPPLANENDDT7,                      
      master.CATCHUPPLANRQ7AMT=returned.CATCHUPPLANRQ7AMT,                      
      master.CATCHUPPLANSTRTDT8=returned.CATCHUPPLANSTRTDT8,                    
      master.CATCHUPPLANENDDT8=returned.CATCHUPPLANENDDT8,                      
      master.CATCHUPPLANRQ8AMT=returned.CATCHUPPLANRQ8AMT,                      
      master.CATCHUPPLANSTRTDT9=returned.CATCHUPPLANSTRTDT9,                    
      master.CATCHUPPLANENDDT9=returned.CATCHUPPLANENDDT9,                      
      master.CATCHUPPLANRQ9AMT=returned.CATCHUPPLANRQ9AMT,                      
      master.CATCHUPPLANSTRTDT10=returned.CATCHUPPLANSTRTDT10,                  
      master.CATCHUPPLANENDDT10=returned.CATCHUPPLANENDDT10,                    
      master.CATCHUPPLANRQ10AMT=returned.CATCHUPPLANRQ10AMT,                    
      master.CATCHUPPLANSTRTDT11=returned.CATCHUPPLANSTRTDT11,                  
      master.CATCHUPPLANENDDT11=returned.CATCHUPPLANENDDT11,                    
      master.CATCHUPPLANRQ11AMT=returned.CATCHUPPLANRQ11AMT,                    
      master.CATCHUPPLANSTRTDT12=returned.CATCHUPPLANSTRTDT12,                  
      master.CATCHUPPLANENDDT12=returned.CATCHUPPLANENDDT12,                    
      master.CATCHUPPLANRQ12AMT=returned.CATCHUPPLANRQ12AMT,                    
      master.CATCHUPPLANSTRTDT13=returned.CATCHUPPLANSTRTDT13,                  
      master.CATCHUPPLANENDDT13=returned.CATCHUPPLANENDDT13,                    
      master.CATCHUPPLANRQ13AMT=returned.CATCHUPPLANRQ13AMT,                    
      master.CATCHUPPLANSTRTDT14=returned.CATCHUPPLANSTRTDT14,                  
      master.CATCHUPPLANENDDT14=returned.CATCHUPPLANENDDT14,                    
      master.CATCHUPPLANRQ14AMT=returned.CATCHUPPLANRQ14AMT,                    
      master.CATCHUPPLANSTRTDT15=returned.CATCHUPPLANSTRTDT15,                  
      master.CATCHUPPLANENDDT15=returned.CATCHUPPLANENDDT15,                    
      master.CATCHUPPLANRQ15AMT=returned.CATCHUPPLANRQ15AMT,                    
      master.CATCHUPPLANCREDT=returned.CATCHUPPLANCREDT,                        
      master.CATCHUPPLANMODDT=returned.CATCHUPPLANMODDT,                        
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (CATCHUPPLANSBANMB,                                                        
      CATCHUPPLANSTRTDT1,                                                       
      CATCHUPPLANENDDT1,                                                        
      CATCHUPPLANRQ1AMT,                                                        
      CATCHUPPLANSTRTDT2,                                                       
      CATCHUPPLANENDDT2,                                                        
      CATCHUPPLANRQ2AMT,                                                        
      CATCHUPPLANSTRTDT3,                                                       
      CATCHUPPLANENDDT3,                                                        
      CATCHUPPLANRQ3AMT,                                                        
      CATCHUPPLANSTRTDT4,                                                       
      CATCHUPPLANENDDT4,                                                        
      CATCHUPPLANRQ4AMT,                                                        
      CATCHUPPLANSTRTDT5,                                                       
      CATCHUPPLANENDDT5,                                                        
      CATCHUPPLANRQ5AMT,                                                        
      CATCHUPPLANSTRTDT6,                                                       
      CATCHUPPLANENDDT6,                                                        
      CATCHUPPLANRQ6AMT,                                                        
      CATCHUPPLANSTRTDT7,                                                       
      CATCHUPPLANENDDT7,                                                        
      CATCHUPPLANRQ7AMT,                                                        
      CATCHUPPLANSTRTDT8,                                                       
      CATCHUPPLANENDDT8,                                                        
      CATCHUPPLANRQ8AMT,                                                        
      CATCHUPPLANSTRTDT9,                                                       
      CATCHUPPLANENDDT9,                                                        
      CATCHUPPLANRQ9AMT,                                                        
      CATCHUPPLANSTRTDT10,                                                      
      CATCHUPPLANENDDT10,                                                       
      CATCHUPPLANRQ10AMT,                                                       
      CATCHUPPLANSTRTDT11,                                                      
      CATCHUPPLANENDDT11,                                                       
      CATCHUPPLANRQ11AMT,                                                       
      CATCHUPPLANSTRTDT12,                                                      
      CATCHUPPLANENDDT12,                                                       
      CATCHUPPLANRQ12AMT,                                                       
      CATCHUPPLANSTRTDT13,                                                      
      CATCHUPPLANENDDT13,                                                       
      CATCHUPPLANRQ13AMT,                                                       
      CATCHUPPLANSTRTDT14,                                                      
      CATCHUPPLANENDDT14,                                                       
      CATCHUPPLANRQ14AMT,                                                       
      CATCHUPPLANSTRTDT15,                                                      
      CATCHUPPLANENDDT15,                                                       
      CATCHUPPLANRQ15AMT,                                                       
      CATCHUPPLANCREDT,                                                         
      CATCHUPPLANMODDT,                                                         
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.CATCHUPPLANSBANMB,                                               
      returned.CATCHUPPLANSTRTDT1,                                              
      returned.CATCHUPPLANENDDT1,                                               
      returned.CATCHUPPLANRQ1AMT,                                               
      returned.CATCHUPPLANSTRTDT2,                                              
      returned.CATCHUPPLANENDDT2,                                               
      returned.CATCHUPPLANRQ2AMT,                                               
      returned.CATCHUPPLANSTRTDT3,                                              
      returned.CATCHUPPLANENDDT3,                                               
      returned.CATCHUPPLANRQ3AMT,                                               
      returned.CATCHUPPLANSTRTDT4,                                              
      returned.CATCHUPPLANENDDT4,                                               
      returned.CATCHUPPLANRQ4AMT,                                               
      returned.CATCHUPPLANSTRTDT5,                                              
      returned.CATCHUPPLANENDDT5,                                               
      returned.CATCHUPPLANRQ5AMT,                                               
      returned.CATCHUPPLANSTRTDT6,                                              
      returned.CATCHUPPLANENDDT6,                                               
      returned.CATCHUPPLANRQ6AMT,                                               
      returned.CATCHUPPLANSTRTDT7,                                              
      returned.CATCHUPPLANENDDT7,                                               
      returned.CATCHUPPLANRQ7AMT,                                               
      returned.CATCHUPPLANSTRTDT8,                                              
      returned.CATCHUPPLANENDDT8,                                               
      returned.CATCHUPPLANRQ8AMT,                                               
      returned.CATCHUPPLANSTRTDT9,                                              
      returned.CATCHUPPLANENDDT9,                                               
      returned.CATCHUPPLANRQ9AMT,                                               
      returned.CATCHUPPLANSTRTDT10,                                             
      returned.CATCHUPPLANENDDT10,                                              
      returned.CATCHUPPLANRQ10AMT,                                              
      returned.CATCHUPPLANSTRTDT11,                                             
      returned.CATCHUPPLANENDDT11,                                              
      returned.CATCHUPPLANRQ11AMT,                                              
      returned.CATCHUPPLANSTRTDT12,                                             
      returned.CATCHUPPLANENDDT12,                                              
      returned.CATCHUPPLANRQ12AMT,                                              
      returned.CATCHUPPLANSTRTDT13,                                             
      returned.CATCHUPPLANENDDT13,                                              
      returned.CATCHUPPLANRQ13AMT,                                              
      returned.CATCHUPPLANSTRTDT14,                                             
      returned.CATCHUPPLANENDDT14,                                              
      returned.CATCHUPPLANRQ14AMT,                                              
      returned.CATCHUPPLANSTRTDT15,                                             
      returned.CATCHUPPLANENDDT15,                                              
      returned.CATCHUPPLANRQ15AMT,                                              
      returned.CATCHUPPLANCREDT,                                                
      returned.CATCHUPPLANMODDT,                                                
      returned.CREATUSERID,                                             
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                     
  msgtxt:=prog||' Finished SOFVCTCHTBL, rowcount='||sql%rowcount||          
    ' Next SOFVFTPOTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);                                                                                   
  --                                                            
  -- SOFVFTPOTBL                                                 
  merge into CSA.SOFVFTPOTBL master                                          
    using (select * from CSA.XOVER_SOFVFTPOTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB)                                  
    when matched then update set                           
      master.PREPOSTRVWRECRDTYPCD=returned.PREPOSTRVWRECRDTYPCD,                                    
      master.PREPOSTRVWTRANSCD=returned.PREPOSTRVWTRANSCD,                                          
      master.PREPOSTRVWTRANSITROUTINGNMB=returned.PREPOSTRVWTRANSITROUTINGNMB,                      
      master.PREPOSTRVWTRANSITROUTINGCHKDGT=returned.PREPOSTRVWTRANSITROUTINGCHKDGT,                
      master.PREPOSTRVWBNKACCTNMB=returned.PREPOSTRVWBNKACCTNMB,                                    
      master.PREPOSTRVWPYMTAMT=returned.PREPOSTRVWPYMTAMT,                                          
      master.PREPOSTRVWINDVLNM=returned.PREPOSTRVWINDVLNM,                                          
      master.PREPOSTRVWCOMPBNKDISC=returned.PREPOSTRVWCOMPBNKDISC,                                  
      master.PREPOSTRVWADDENDRECIND=returned.PREPOSTRVWADDENDRECIND,                                
      master.PREPOSTRVWTRACENMB=returned.PREPOSTRVWTRACENMB,                                        
      master.PREPOSTRVWCMNT=returned.PREPOSTRVWCMNT,                                                
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
      WHEN NOT MATCHED THEN
    INSERT (PREPOSTRVWRECRDTYPCD,                                                     
      PREPOSTRVWTRANSCD,                                                        
      PREPOSTRVWTRANSITROUTINGNMB,                                              
      PREPOSTRVWTRANSITROUTINGCHKDGT,                                           
      PREPOSTRVWBNKACCTNMB,                                                     
      PREPOSTRVWPYMTAMT,                                                        
      LOANNMB,                                                                  
      PREPOSTRVWINDVLNM,                                                        
      PREPOSTRVWCOMPBNKDISC,                                                    
      PREPOSTRVWADDENDRECIND,                                                   
      PREPOSTRVWTRACENMB,                                                       
      PREPOSTRVWCMNT,                                                           
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.PREPOSTRVWRECRDTYPCD,                                            
      returned.PREPOSTRVWTRANSCD,                                               
      returned.PREPOSTRVWTRANSITROUTINGNMB,                                     
      returned.PREPOSTRVWTRANSITROUTINGCHKDGT,                                  
      returned.PREPOSTRVWBNKACCTNMB,                                            
      returned.PREPOSTRVWPYMTAMT,                                               
      returned.LOANNMB,                                                         
      returned.PREPOSTRVWINDVLNM,                                               
      returned.PREPOSTRVWCOMPBNKDISC,                                           
      returned.PREPOSTRVWADDENDRECIND,                                          
      returned.PREPOSTRVWTRACENMB,                                              
      returned.PREPOSTRVWCMNT,                                                  
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);    
  msgtxt:=prog||' Finished SOFVFTPOTBL, rowcount='||sql%rowcount||          
    ' Next SOFVGRGCTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVGRGCTBL                                                 
  merge into CSA.SOFVGRGCTBL master                                          
    using (select * from CSA.XOVER_SOFVGRGCTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.CALNDRKEYDT=returned.CALNDRKEYDT)                                  
    when matched then update set                           
      master.CALNDRDAY=returned.CALNDRDAY,                                                          
      master.CALNDRHOLIDAYIND=returned.CALNDRHOLIDAYIND,                                            
      master.CALNDRREMITTANCEIND=returned.CALNDRREMITTANCEIND,                                      
      master.CALNDRCUTOFFIND=returned.CALNDRCUTOFFIND,                                              
      master.CALNDRSUSPROLLIND=returned.CALNDRSUSPROLLIND,                                          
      master.CALNDRPAYOUTIND=returned.CALNDRPAYOUTIND,                                              
      master.CALNDRDESC=returned.CALNDRDESC,                                                        
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (CALNDRKEYDT,                                                              
      CALNDRDAY,                                                                
      CALNDRHOLIDAYIND,                                                         
      CALNDRREMITTANCEIND,                                                      
      CALNDRCUTOFFIND,                                                          
      CALNDRSUSPROLLIND,                                                        
      CALNDRPAYOUTIND,                                                          
      CALNDRDESC,                                                               
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.CALNDRKEYDT,                                                     
      returned.CALNDRDAY,                                                       
      returned.CALNDRHOLIDAYIND,                                                
      returned.CALNDRREMITTANCEIND,                                             
      returned.CALNDRCUTOFFIND,                                                 
      returned.CALNDRSUSPROLLIND,                                               
      returned.CALNDRPAYOUTIND,                                                 
      returned.CALNDRDESC,                                                      
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                      
  msgtxt:=prog||' Finished SOFVGRGCTBL, rowcount='||sql%rowcount||          
    ' Next SOFVBFFATBL';                 
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVBFFATBL                                                 
  merge into CSA.SOFVBFFATBL master                                          
    using (select * from CSA.XOVER_SOFVBFFATBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.ASSUMPKEYLOANNMB=returned.ASSUMPKEYLOANNMB
      and master.ASSUMPLASTEFFDT=returned.ASSUMPLASTEFFDT)       
    when matched then update set                           
      master.LOANNMB=returned.LOANNMB,                                                              
      master.ASSUMPTAXID=returned.ASSUMPTAXID,                                                      
      master.ASSUMPNM=returned.ASSUMPNM,                                                            
      master.ASSUMPMAILADRSTR1NM=returned.ASSUMPMAILADRSTR1NM,                                      
      master.ASSUMPMAILADRSTR2NM=returned.ASSUMPMAILADRSTR2NM,                                      
      master.ASSUMPMAILADRCTYNM=returned.ASSUMPMAILADRCTYNM,                                        
      master.ASSUMPMAILADRSTCD=returned.ASSUMPMAILADRSTCD,                                          
      master.ASSUMPMAILADRZIPCD=returned.ASSUMPMAILADRZIPCD,                                        
      master.ASSUMPMAILADRZIP4CD=returned.ASSUMPMAILADRZIP4CD,                                      
      master.ASSUMPOFC=returned.ASSUMPOFC,                                                          
      master.ASSUMPPRGRM=returned.ASSUMPPRGRM,                                                      
      master.ASSUMPSTATCDOFLOAN=returned.ASSUMPSTATCDOFLOAN,                                        
      master.ASSUMPVERIFICATIONIND=returned.ASSUMPVERIFICATIONIND,                                  
      master.ASSUMPVERIFICATIONDT=returned.ASSUMPVERIFICATIONDT,                                    
      master.ASSUMPNMMAILADRCHNGIND=returned.ASSUMPNMMAILADRCHNGIND,                                
      master.ASSUMPNMADRCHNGDT=returned.ASSUMPNMADRCHNGDT,                                          
      master.ASSUMPTAXFORMFLD=returned.ASSUMPTAXFORMFLD,                                            
      master.ASSUMPTAXMAINTNDT=returned.ASSUMPTAXMAINTNDT,                                          
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      ASSUMPTAXID,                                                              
      ASSUMPNM,                                                                 
      ASSUMPMAILADRSTR1NM,                                                      
      ASSUMPMAILADRSTR2NM,                                                      
      ASSUMPMAILADRCTYNM,                                                       
      ASSUMPMAILADRSTCD,                                                        
      ASSUMPMAILADRZIPCD,                                                       
      ASSUMPMAILADRZIP4CD,                                                      
      ASSUMPOFC,                                                                
      ASSUMPPRGRM,                                                              
      ASSUMPSTATCDOFLOAN,                                                       
      ASSUMPVERIFICATIONIND,                                                    
      ASSUMPVERIFICATIONDT,                                                     
      ASSUMPNMMAILADRCHNGIND,                                                   
      ASSUMPNMADRCHNGDT,                                                        
      ASSUMPTAXFORMFLD,                                                         
      ASSUMPTAXMAINTNDT,                                                        
      ASSUMPKEYLOANNMB,                                                         
      ASSUMPLASTEFFDT,                                                          
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.ASSUMPTAXID,                                                     
      returned.ASSUMPNM,                                                        
      returned.ASSUMPMAILADRSTR1NM,                                             
      returned.ASSUMPMAILADRSTR2NM,                                             
      returned.ASSUMPMAILADRCTYNM,                                              
      returned.ASSUMPMAILADRSTCD,                                               
      returned.ASSUMPMAILADRZIPCD,                                              
      returned.ASSUMPMAILADRZIP4CD,                                             
      returned.ASSUMPOFC,                                                       
      returned.ASSUMPPRGRM,                                                     
      returned.ASSUMPSTATCDOFLOAN,                                              
      returned.ASSUMPVERIFICATIONIND,                                           
      returned.ASSUMPVERIFICATIONDT,                                            
      returned.ASSUMPNMMAILADRCHNGIND,                                          
      returned.ASSUMPNMADRCHNGDT,                                               
      returned.ASSUMPTAXFORMFLD,                                                
      returned.ASSUMPTAXMAINTNDT,                                               
      returned.ASSUMPKEYLOANNMB,                                                
      returned.ASSUMPLASTEFFDT,                                                 
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                     
  msgtxt:=prog||' Finished SOFVBFFATBL, rowcount='||sql%rowcount||          
    ' Next SOFVBGW9TBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVBGW9TBL                                                 
  merge into CSA.SOFVBGW9TBL master                                          
    using (select * from CSA.XOVER_SOFVBGW9TBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB)                                  
    when matched then update set                           
      master.W9TAXID=returned.W9TAXID,                                                              
      master.W9NM=returned.W9NM,                                                                    
      master.W9MAILADRSTR1NM=returned.W9MAILADRSTR1NM,                                              
      master.W9MAILADRSTR2NM=returned.W9MAILADRSTR2NM,                                              
      master.W9MAILADRCTYNM=returned.W9MAILADRCTYNM,                                                
      master.W9MAILADRSTCD=returned.W9MAILADRSTCD,                                                  
      master.W9MAILADRZIPCD=returned.W9MAILADRZIPCD,                                                
      master.W9MAILADRZIP4CD=returned.W9MAILADRZIP4CD,                                              
      master.W9VERIFICATIONIND=returned.W9VERIFICATIONIND,                                          
      master.W9VERIFICATIONDT=returned.W9VERIFICATIONDT,                                            
      master.W9NMMAILADRCHNGIND=returned.W9NMMAILADRCHNGIND,                                        
      master.W9NMADRCHNGDT=returned.W9NMADRCHNGDT,                                                  
      master.W9TAXFORMFLD=returned.W9TAXFORMFLD,                                                    
      master.W9TAXMAINTNDT=returned.W9TAXMAINTNDT,                                                  
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      W9TAXID,                                                                  
      W9NM,                                                                     
      W9MAILADRSTR1NM,                                                          
      W9MAILADRSTR2NM,                                                          
      W9MAILADRCTYNM,                                                           
      W9MAILADRSTCD,                                                            
      W9MAILADRZIPCD,                                                           
      W9MAILADRZIP4CD,                                                          
      W9VERIFICATIONIND,                                                        
      W9VERIFICATIONDT,                                                         
      W9NMMAILADRCHNGIND,                                                       
      W9NMADRCHNGDT,                                                            
      W9TAXFORMFLD,                                                             
      W9TAXMAINTNDT,                                                            
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.W9TAXID,                                                         
      returned.W9NM,                                                            
      returned.W9MAILADRSTR1NM,                                                 
      returned.W9MAILADRSTR2NM,                                                 
      returned.W9MAILADRCTYNM,                                                  
      returned.W9MAILADRSTCD,                                                   
      returned.W9MAILADRZIPCD,                                                  
      returned.W9MAILADRZIP4CD,                                                 
      returned.W9VERIFICATIONIND,                                               
      returned.W9VERIFICATIONDT,                                                
      returned.W9NMMAILADRCHNGIND,                                              
      returned.W9NMADRCHNGDT,                                                   
      returned.W9TAXFORMFLD,                                                    
      returned.W9TAXMAINTNDT,                                                   
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                      
  msgtxt:=prog||' Finished SOFVBGW9TBL, rowcount='||sql%rowcount||          
    ' Next SOFVDFPYTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVDFPYTBL                                                 
  merge into CSA.SOFVDFPYTBL master                                          
    using (select * from CSA.XOVER_SOFVDFPYTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB)                                  
    when matched then update set                           
      master.DEFPLANNEWMOPYMTAMT=returned.DEFPLANNEWMOPYMTAMT,                                      
      master.DEFPLANOPNDT=returned.DEFPLANOPNDT,                                                    
      master.DEFPLANDTCLS=returned.DEFPLANDTCLS,                                                    
      master.DEFPLANLASTMAINTNDT=returned.DEFPLANLASTMAINTNDT,                                      
      master.DEFPLANCMNT1=returned.DEFPLANCMNT1,                                                    
      master.DEFPLANCMNT2=returned.DEFPLANCMNT2,                                                    
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      DEFPLANNEWMOPYMTAMT,                                                      
      DEFPLANOPNDT,                                                             
      DEFPLANDTCLS,                                                             
      DEFPLANLASTMAINTNDT,                                                      
      DEFPLANCMNT1,                                                             
      DEFPLANCMNT2,                                                             
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.DEFPLANNEWMOPYMTAMT,                                             
      returned.DEFPLANOPNDT,                                                    
      returned.DEFPLANDTCLS,                                                    
      returned.DEFPLANLASTMAINTNDT,                                             
      returned.DEFPLANCMNT1,                                                    
      returned.DEFPLANCMNT2,                                                    
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                   
  msgtxt:=prog||' Finished SOFVDFPYTBL, rowcount='||sql%rowcount||          
    ' Next SOFVGNTYTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVGNTYTBL                                                 
  merge into CSA.SOFVGNTYTBL master                                          
    using (select * from CSA.XOVER_SOFVGNTYTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB
      and master.GNTYDT=returned.GNTYDT
      and master.GNTYTYP=returned.GNTYTYP)                                  
    when matched then update set                           
      master.GNTYGNTYAMT=returned.GNTYGNTYAMT,                                                      
      master.GNTYGNTYORGLDT=returned.GNTYGNTYORGLDT,                                                
      master.GNTYSTATCD=returned.GNTYSTATCD,                                                        
      master.GNTYCLSDT=returned.GNTYCLSDT,                                                          
      master.GNTYCREATDT=returned.GNTYCREATDT,                                                      
      master.GNTYLASTMAINTNDT=returned.GNTYLASTMAINTNDT,                                            
      master.GNTYCMNT=returned.GNTYCMNT,                                                            
      master.GNTYCMNT2=returned.GNTYCMNT2,                                                          
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
      WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      GNTYDT,                                                                   
      GNTYTYP,                                                                  
      GNTYGNTYAMT,                                                              
      GNTYGNTYORGLDT,                                                           
      GNTYSTATCD,                                                               
      GNTYCLSDT,                                                                
      GNTYCREATDT,                                                              
      GNTYLASTMAINTNDT,                                                         
      GNTYCMNT,                                                                 
      GNTYCMNT2,                                                                
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.GNTYDT,                                                          
      returned.GNTYTYP,                                                         
      returned.GNTYGNTYAMT,                                                     
      returned.GNTYGNTYORGLDT,                                                  
      returned.GNTYSTATCD,                                                      
      returned.GNTYCLSDT,                                                       
      returned.GNTYCREATDT,                                                     
      returned.GNTYLASTMAINTNDT,                                                
      returned.GNTYCMNT,                                                        
      returned.GNTYCMNT2,                                                       
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                      
  msgtxt:=prog||' Finished SOFVGNTYTBL rowcount='||sql%rowcount||          
    ' Next SOFVCDCMSTRTBL';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVCDCMSTRTBL                                              
  merge into CSA.SOFVCDCMSTRTBL master                                       
    using (select * from CSA.XOVER_SOFVCDCMSTRTBL                
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.CDCREGNCD=returned.CDCREGNCD
      and master.CDCCERTNMB=returned.CDCCERTNMB)                                  
    when matched then update set                           
      master.CDCPOLKNMB=returned.CDCPOLKNMB,                                                        
      master.CDCNM=returned.CDCNM,                                                                  
      master.CDCMAILADRSTR1NM=returned.CDCMAILADRSTR1NM,                                            
      master.CDCMAILADRCTYNM=returned.CDCMAILADRCTYNM,                                              
      master.CDCMAILADRSTCD=returned.CDCMAILADRSTCD,                                                
      master.CDCMAILADRZIPCD=returned.CDCMAILADRZIPCD,                                              
      master.CDCMAILADRZIP4CD=returned.CDCMAILADRZIP4CD,                                            
      master.CDCCNTCT=returned.CDCCNTCT,                                                            
      master.CDCAREACD=returned.CDCAREACD,                                                          
      master.CDCEXCH=returned.CDCEXCH,                                                              
      master.CDCEXTN=returned.CDCEXTN,                                                              
      master.CDCSTATCD=returned.CDCSTATCD,                                                          
      master.CDCTAXID=returned.CDCTAXID,                                                            
      master.CDCSETUPDT=returned.CDCSETUPDT,                                                        
      master.CDCLMAINTNDT=returned.CDCLMAINTNDT,                                                    
      master.CDCTOTDOLLRDBENTRAMT=returned.CDCTOTDOLLRDBENTRAMT,                                    
      master.CDCTOTNMBPART=returned.CDCTOTNMBPART,                                                  
      master.CDCTOTPAIDDBENTRAMT=returned.CDCTOTPAIDDBENTRAMT,                                      
      master.CDCHLDPYMT=returned.CDCHLDPYMT,                                                        
      master.CDCDIST=returned.CDCDIST,                                                              
      master.CDCMAILADRSTR2NM=returned.CDCMAILADRSTR2NM,                                            
      master.CDCACHBNK=returned.CDCACHBNK,                                                          
      master.CDCACHMAILADRSTR1NM=returned.CDCACHMAILADRSTR1NM,                                      
      master.CDCACHMAILADRSTR2NM=returned.CDCACHMAILADRSTR2NM,                                      
      master.CDCACHMAILADRCTYNM=returned.CDCACHMAILADRCTYNM,                                        
      master.CDCACHMAILADRSTCD=returned.CDCACHMAILADRSTCD,                                          
      master.CDCACHMAILADRZIPCD=returned.CDCACHMAILADRZIPCD,                                        
      master.CDCACHMAILADRZIP4CD=returned.CDCACHMAILADRZIP4CD,                                      
      master.CDCACHBR=returned.CDCACHBR,                                                            
      master.CDCACHACCTTYP=returned.CDCACHACCTTYP,                                                  
      master.CDCACHACCTNMB=returned.CDCACHACCTNMB,                                                  
      master.CDCACHROUTSYM=returned.CDCACHROUTSYM,                                                  
      master.CDCACHTRANSCD=returned.CDCACHTRANSCD,                                                  
      master.CDCACHBNKIDNMB=returned.CDCACHBNKIDNMB,                                                
      master.CDCACHDEPSTR=returned.CDCACHDEPSTR,                                                    
      master.CDCACHSIGNDT=returned.CDCACHSIGNDT,                                                    
      master.CDCPRENOTETESTDT=returned.CDCPRENOTETESTDT,                                            
      master.CDCACHPRENTIND=returned.CDCACHPRENTIND,                                                
      master.CDCCLSBALAMT=returned.CDCCLSBALAMT,                                                    
      master.CDCCLSBALDT=returned.CDCCLSBALDT,                                                      
      master.CDCNETPREPAYAMT=returned.CDCNETPREPAYAMT,                                              
      master.CDCFAXAREACD=returned.CDCFAXAREACD,                                                    
      master.CDCFAXEXCH=returned.CDCFAXEXCH,                                                        
      master.CDCFAXEXTN=returned.CDCFAXEXTN,                                                        
      master.CDCPREPAYCLSBALAMT=returned.CDCPREPAYCLSBALAMT,                                        
      master.CDCNAMADDRID=returned.CDCNAMADDRID,                                                    
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
      WHEN NOT MATCHED THEN
    INSERT (CDCREGNCD,                                                                
      CDCCERTNMB,                                                               
      CDCPOLKNMB,                                                               
      CDCNM,                                                                    
      CDCMAILADRSTR1NM,                                                         
      CDCMAILADRCTYNM,                                                          
      CDCMAILADRSTCD,                                                           
      CDCMAILADRZIPCD,                                                          
      CDCMAILADRZIP4CD,                                                         
      CDCCNTCT,                                                                 
      CDCAREACD,                                                                
      CDCEXCH,                                                                  
      CDCEXTN,                                                                  
      CDCSTATCD,                                                                
      CDCTAXID,                                                                 
      CDCSETUPDT,                                                               
      CDCLMAINTNDT,                                                             
      CDCTOTDOLLRDBENTRAMT,                                                     
      CDCTOTNMBPART,                                                            
      CDCTOTPAIDDBENTRAMT,                                                      
      CDCHLDPYMT,                                                               
      CDCDIST,                                                                  
      CDCMAILADRSTR2NM,                                                         
      CDCACHBNK,                                                                
      CDCACHMAILADRSTR1NM,                                                      
      CDCACHMAILADRSTR2NM,                                                      
      CDCACHMAILADRCTYNM,                                                       
      CDCACHMAILADRSTCD,                                                        
      CDCACHMAILADRZIPCD,                                                       
      CDCACHMAILADRZIP4CD,                                                      
      CDCACHBR,                                                                 
      CDCACHACCTTYP,                                                            
      CDCACHACCTNMB,                                                            
      CDCACHROUTSYM,                                                            
      CDCACHTRANSCD,                                                            
      CDCACHBNKIDNMB,                                                           
      CDCACHDEPSTR,                                                             
      CDCACHSIGNDT,                                                             
      CDCPRENOTETESTDT,                                                         
      CDCACHPRENTIND,                                                           
      CDCCLSBALAMT,                                                             
      CDCCLSBALDT,                                                              
      CDCNETPREPAYAMT,                                                          
      CDCFAXAREACD,                                                             
      CDCFAXEXCH,                                                               
      CDCFAXEXTN,                                                               
      CDCPREPAYCLSBALAMT,                                                       
      CDCNAMADDRID,                                                             
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.CDCREGNCD,                                                       
      returned.CDCCERTNMB,                                                      
      returned.CDCPOLKNMB,                                                      
      returned.CDCNM,                                                           
      returned.CDCMAILADRSTR1NM,                                                
      returned.CDCMAILADRCTYNM,                                                 
      returned.CDCMAILADRSTCD,                                                  
      returned.CDCMAILADRZIPCD,                                                 
      returned.CDCMAILADRZIP4CD,                                                
      returned.CDCCNTCT,                                                        
      returned.CDCAREACD,                                                       
      returned.CDCEXCH,                                                         
      returned.CDCEXTN,                                                         
      returned.CDCSTATCD,                                                       
      returned.CDCTAXID,                                                        
      returned.CDCSETUPDT,                                                      
      returned.CDCLMAINTNDT,                                                    
      returned.CDCTOTDOLLRDBENTRAMT,                                            
      returned.CDCTOTNMBPART,                                                   
      returned.CDCTOTPAIDDBENTRAMT,                                             
      returned.CDCHLDPYMT,                                                      
      returned.CDCDIST,                                                         
      returned.CDCMAILADRSTR2NM,                                                
      returned.CDCACHBNK,                                                       
      returned.CDCACHMAILADRSTR1NM,                                             
      returned.CDCACHMAILADRSTR2NM,                                             
      returned.CDCACHMAILADRCTYNM,                                              
      returned.CDCACHMAILADRSTCD,                                               
      returned.CDCACHMAILADRZIPCD,                                              
      returned.CDCACHMAILADRZIP4CD,                                             
      returned.CDCACHBR,                                                        
      returned.CDCACHACCTTYP,                                                   
      returned.CDCACHACCTNMB,                                                   
      returned.CDCACHROUTSYM,                                                   
      returned.CDCACHTRANSCD,                                                   
      returned.CDCACHBNKIDNMB,                                                  
      returned.CDCACHDEPSTR,                                                    
      returned.CDCACHSIGNDT,                                                    
      returned.CDCPRENOTETESTDT,                                                
      returned.CDCACHPRENTIND,                                                  
      returned.CDCCLSBALAMT,                                                    
      returned.CDCCLSBALDT,                                                     
      returned.CDCNETPREPAYAMT,                                                 
      returned.CDCFAXAREACD,                                                    
      returned.CDCFAXEXCH,                                                      
      returned.CDCFAXEXTN,                                                      
      returned.CDCPREPAYCLSBALAMT,                                              
      returned.CDCNAMADDRID,                                                    
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                    
  msgtxt:=prog||' Finished SOFVCDCMSTRTBL, rowcount='||sql%rowcount||       
    ' Next SOFVDUEBTBL';                
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
                                                                                
  --                                                            
  -- SOFVDUEBTBL                                                 
  merge into CSA.SOFVDUEBTBL master                                          
    using (select * from CSA.XOVER_SOFVDUEBTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB
      and master.DUEFROMBORRRECRDTYP=returned.DUEFROMBORRRECRDTYP
      and master.DUEFROMBORRPOSTINGDT=returned.DUEFROMBORRPOSTINGDT)                                  
    when matched then update set                           
      master.DUEFROMBORRDOLLRAMT=returned.DUEFROMBORRDOLLRAMT,                                      
      master.DUEFROMBORRRJCTDT=returned.DUEFROMBORRRJCTDT,                                          
      master.DUEFROMBORRRJCTCD=returned.DUEFROMBORRRJCTCD,                                          
      master.DUEFROMBORRRECRDSTATCD=returned.DUEFROMBORRRECRDSTATCD,                                
      master.DUEFROMBORRCLSDT=returned.DUEFROMBORRCLSDT,                                            
      master.DUEFROMBORRCMNT1=returned.DUEFROMBORRCMNT1,                                            
      master.DUEFROMBORRCMNT2=returned.DUEFROMBORRCMNT2,                                            
      master.DUEFROMBORRRPTDAILY=returned.DUEFROMBORRRPTDAILY,                                      
      master.DUEFROMBORRDISBIND=returned.DUEFROMBORRDISBIND,                                        
      master.DUEFROMBORRDELACH=returned.DUEFROMBORRDELACH,                                          
      master.CREATUSERID=returned.CREATUSERID,                                                      
      master.CREATDT=returned.CREATDT,                                                              
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                                                
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                  
      DUEFROMBORRRECRDTYP,                                                      
      DUEFROMBORRPOSTINGDT,                                                     
      DUEFROMBORRDOLLRAMT,                                                      
      DUEFROMBORRRJCTDT,                                                        
      DUEFROMBORRRJCTCD,                                                        
      DUEFROMBORRRECRDSTATCD,                                                   
      DUEFROMBORRCLSDT,                                                         
      DUEFROMBORRCMNT1,                                                         
      DUEFROMBORRCMNT2,                                                         
      DUEFROMBORRRPTDAILY,                                                      
      DUEFROMBORRDISBIND,                                                       
      DUEFROMBORRDELACH,                                                        
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                         
      returned.DUEFROMBORRRECRDTYP,                                             
      returned.DUEFROMBORRPOSTINGDT,                                            
      returned.DUEFROMBORRDOLLRAMT,                                             
      returned.DUEFROMBORRRJCTDT,                                               
      returned.DUEFROMBORRRJCTCD,                                               
      returned.DUEFROMBORRRECRDSTATCD,                                          
      returned.DUEFROMBORRCLSDT,                                                
      returned.DUEFROMBORRCMNT1,                                                
      returned.DUEFROMBORRCMNT2,                                                
      returned.DUEFROMBORRRPTDAILY,                                             
      returned.DUEFROMBORRDISBIND,                                              
      returned.DUEFROMBORRDELACH,                                               
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                    
  msgtxt:=prog||' Finished SOFVDUEBTBL, rowcount='||sql%rowcount||          
    ' Next SOFVDRTBL';                   
  debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
  --
    -- SOFVDRTBL                                                 
  merge into CSA.SOFVDRTBL master                                          
    using (select * from CSA.XOVER_SOFVDRTBL                   
      where lastupdtdt >                           
        (select stgcsa_merge_end_dt
           from CSA.XOVER_status)) returned
    on (master.LOANNMB=returned.LOANNMB )                                  
    when matched then update set                                                
      master.CREATUSERID=returned.CREATUSERID,                                  
      master.CREATDT=returned.CREATDT,                                          
      master.LASTUPDTUSERID=returned.LASTUPDTUSERID,                            
      master.LASTUPDTDT=returned.LASTUPDTDT
    WHEN NOT MATCHED THEN
    INSERT (LOANNMB,                                                                                                                         
      CREATUSERID,                                                              
      CREATDT,                                                                  
      LASTUPDTUSERID,                                                           
      LASTUPDTDT)
    VALUES (returned.LOANNMB,                                                                                                       
      returned.CREATUSERID,                                                     
      returned.CREATDT,                                                         
      returned.LASTUPDTUSERID,                                                  
      returned.LASTUPDTDT);                                                    
  msgtxt:=prog||' Finished SOFVDRTBL, rowcount='||sql%rowcount||          
    ' Last table';                   
 debug_putline(logentryid,'STDXVR',223,msgtxt,user,0,
    program_name=>prog||' '||ver);   
  -- if doing validation, check CSA, STGCSA row counts match
  if do_validation=true then
    -- at this point csa copies of crossover tables should have same counts 
    -- as the exported STGCSA tables... verify
    --  collect old and new row counts, check
    select cnt_SOFVBGW9TBL,
           cnt_SOFVCDCMSTRTBL,
           cnt_SOFVCTCHTBL,
           cnt_SOFVDFPYTBL,
           cnt_SOFVDUEBTBL,
           cnt_SOFVFTPOTBL,
           cnt_SOFVGNTYTBL,
           cnt_SOFVGRGCTBL,
           cnt_SOFVLND1TBL,
           cnt_SOFVLND2TBL,
           cnt_SOFVOTRNTBL,
           cnt_SOFVPYMTTBL,
           cnt_SOFVRTSCTBL,
           cnt_SOFVAUDTTBL,
           cnt_SOFVBFFATBL,
           cnt_SOFVBGDFTBL,
		   cnt_SOFVDRTBL
      into   
           statusrec.csacnt_SOFVBGW9TBL,
           statusrec.csacnt_SOFVCDCMSTRTBL,
           statusrec.csacnt_SOFVCTCHTBL,
           statusrec.csacnt_SOFVDFPYTBL,
           statusrec.csacnt_SOFVDUEBTBL,
           statusrec.csacnt_SOFVFTPOTBL,
           statusrec.csacnt_SOFVGNTYTBL,
           statusrec.csacnt_SOFVGRGCTBL,
           statusrec.csacnt_SOFVLND1TBL,
           statusrec.csacnt_SOFVLND2TBL,
           statusrec.csacnt_SOFVOTRNTBL,
           statusrec.csacnt_SOFVPYMTTBL,
           statusrec.csacnt_SOFVRTSCTBL,
           statusrec.csacnt_SOFVAUDTTBL,
           statusrec.csacnt_SOFVBFFATBL,
           statusrec.csacnt_SOFVBGDFTBL,
		   statusrec.csacnt_SOFVDRTBL
      FROM (SELECT COUNT(*) cnt_SOFVBGW9TBL FROM csa.SOFVBGW9TBL),
           (SELECT COUNT(*) cnt_SOFVCDCMSTRTBL FROM csa.SOFVCDCMSTRTBL),
           (SELECT COUNT(*) cnt_SOFVCTCHTBL FROM csa.SOFVCTCHTBL),
           (SELECT COUNT(*) cnt_SOFVDFPYTBL FROM csa.SOFVDFPYTBL),
           (SELECT COUNT(*) cnt_SOFVDUEBTBL FROM csa.SOFVDUEBTBL),
           (SELECT COUNT(*) cnt_SOFVFTPOTBL FROM csa.SOFVFTPOTBL),
           (SELECT COUNT(*) cnt_SOFVGNTYTBL FROM csa.SOFVGNTYTBL),
           (SELECT COUNT(*) cnt_SOFVGRGCTBL FROM csa.SOFVGRGCTBL),
           (SELECT COUNT(*) cnt_SOFVLND1TBL FROM csa.SOFVLND1TBL),
           (SELECT COUNT(*) cnt_SOFVLND2TBL FROM csa.SOFVLND2TBL),
           (SELECT COUNT(*) cnt_SOFVOTRNTBL FROM csa.SOFVOTRNTBL),
           (SELECT COUNT(*) cnt_SOFVPYMTTBL FROM csa.SOFVPYMTTBL),
           (SELECT COUNT(*) cnt_SOFVRTSCTBL FROM csa.SOFVRTSCTBL),
           (SELECT COUNT(*) cnt_SOFVAUDTTBL FROM csa.SOFVAUDTTBL),
           (select count(*) cnt_SOFVBFFATBL FROM csa.SOFVBFFATBL),
           (SELECT COUNT(*) cnt_SOFVBGDFTBL FROM csa.SOFVBGDFTBL),
		   (SELECT COUNT(*) cnt_SOFVDRTBL FROM csa.SOFVDRTBL);
    -- 
	
    -- check that row counts match
    errtxt:=null;
    if statusrec.stgcnt_SOFVAUDTTBL<>statusrec.csacnt_SOFVAUDTTBL then                  
      errtxt:=errtxt||' SOFVAUDTTBL:'                   
        ||statusrec.stgcnt_SOFVAUDTTBL||':'||statusrec.csacnt_SOFVAUDTTBL;              
    end if;
    if statusrec.stgcnt_SOFVFTPOTBL<>statusrec.csacnt_SOFVFTPOTBL then                  
      errtxt:=errtxt||' SOFVFTPOTBL:'                   
        ||statusrec.stgcnt_SOFVFTPOTBL||':'||statusrec.csacnt_SOFVFTPOTBL;              
    end if;
    if statusrec.stgcnt_SOFVBGW9TBL<>statusrec.csacnt_SOFVBGW9TBL then                  
      errtxt:=errtxt||' SOFVBGW9TBL:'                   
        ||statusrec.stgcnt_SOFVBGW9TBL||':'||statusrec.csacnt_SOFVBGW9TBL;              
    end if;
    if statusrec.stgcnt_SOFVCDCMSTRTBL<>statusrec.csacnt_SOFVCDCMSTRTBL then            
      errtxt:=errtxt||' SOFVCDCMSTRTBL:'                
        ||statusrec.stgcnt_SOFVCDCMSTRTBL||':'||statusrec.csacnt_SOFVCDCMSTRTBL;        
    end if;
    if statusrec.stgcnt_SOFVCTCHTBL<>statusrec.csacnt_SOFVCTCHTBL then                  
      errtxt:=errtxt||' SOFVCTCHTBL:'                   
        ||statusrec.stgcnt_SOFVCTCHTBL||':'||statusrec.csacnt_SOFVCTCHTBL;              
    end if;
    if statusrec.stgcnt_SOFVDFPYTBL<>statusrec.csacnt_SOFVDFPYTBL then                  
      errtxt:=errtxt||' SOFVDFPYTBL:'                   
        ||statusrec.stgcnt_SOFVDFPYTBL||':'||statusrec.csacnt_SOFVDFPYTBL;              
    end if;
    if statusrec.stgcnt_SOFVDUEBTBL<>statusrec.csacnt_SOFVDUEBTBL then                  
      errtxt:=errtxt||' SOFVDUEBTBL:'                   
        ||statusrec.stgcnt_SOFVDUEBTBL||':'||statusrec.csacnt_SOFVDUEBTBL;              
    end if;
    if statusrec.stgcnt_SOFVGNTYTBL<>statusrec.csacnt_SOFVGNTYTBL then                  
      errtxt:=errtxt||' SOFVGNTYTBL:'                   
        ||statusrec.stgcnt_SOFVGNTYTBL||':'||statusrec.csacnt_SOFVGNTYTBL;              
    end if;
    if statusrec.stgcnt_SOFVGRGCTBL<>statusrec.csacnt_SOFVGRGCTBL then                  
      errtxt:=errtxt||' SOFVGRGCTBL:'                   
        ||statusrec.stgcnt_SOFVGRGCTBL||':'||statusrec.csacnt_SOFVGRGCTBL;              
    end if;
    if statusrec.stgcnt_SOFVLND1TBL<>statusrec.csacnt_SOFVLND1TBL then                  
      errtxt:=errtxt||' SOFVLND1TBL:'                   
        ||statusrec.stgcnt_SOFVLND1TBL||':'||statusrec.csacnt_SOFVLND1TBL;              
    end if;
    if statusrec.stgcnt_SOFVLND2TBL<>statusrec.csacnt_SOFVLND2TBL then                  
      errtxt:=errtxt||' SOFVLND2TBL:'                   
        ||statusrec.stgcnt_SOFVLND2TBL||':'||statusrec.csacnt_SOFVLND2TBL;              
    end if;
    if statusrec.stgcnt_SOFVOTRNTBL<>statusrec.csacnt_SOFVOTRNTBL then                  
      errtxt:=errtxt||' SOFVOTRNTBL:'                   
        ||statusrec.stgcnt_SOFVOTRNTBL||':'||statusrec.csacnt_SOFVOTRNTBL;              
    end if;
    if statusrec.stgcnt_SOFVPYMTTBL<>statusrec.csacnt_SOFVPYMTTBL then                  
      errtxt:=errtxt||' SOFVPYMTTBL:'                   
        ||statusrec.stgcnt_SOFVPYMTTBL||':'||statusrec.csacnt_SOFVPYMTTBL;              
    end if;
    if statusrec.stgcnt_SOFVRTSCTBL<>statusrec.csacnt_SOFVRTSCTBL then                  
      errtxt:=errtxt||' SOFVRTSCTBL:'                   
        ||statusrec.stgcnt_SOFVRTSCTBL||':'||statusrec.csacnt_SOFVRTSCTBL;              
    end if;
    if statusrec.stgcnt_SOFVBFFATBL<>statusrec.csacnt_SOFVBFFATBL then                  
      errtxt:=errtxt||' SOFVBFFATBL:'                   
        ||statusrec.stgcnt_SOFVBFFATBL||':'||statusrec.csacnt_SOFVBFFATBL;              
    end if;
    if statusrec.stgcnt_SOFVBGDFTBL<>statusrec.csacnt_SOFVBGDFTBL then                  
      errtxt:=errtxt||' SOFVBGDFTBL:'                   
        ||statusrec.stgcnt_SOFVBGDFTBL||':'||statusrec.csacnt_SOFVBGDFTBL;              
    end if;
    if statusrec.stgcnt_SOFVDRTBL<>statusrec.csacnt_SOFVDRTBL then                  
      errtxt:=errtxt||' SOFVBGDFTBL:'                   
        ||statusrec.stgcnt_SOFVDRTBL||':'||statusrec.csacnt_SOFVDRTBL;              
    end if;
    If errtxt is not null then
      errtxt:='Error, STGCSA:CSA row counts not ok: '||errtxt;
      debug_putline(logentryid,'STDXVR',225,errtxt,user,3,
        program_name=>prog||' '||ver);  
      raise_application_error(-20225,prog||' '||errtxt);
    end if;      
  end if;
  --
  -- update status rec to show that last step was merge here
  update csa.xover_status set xover_last_event='CM',
   csa_merge_end_dt=sysdate;
  --
  -- all done, commit and record completion  
  commit;
  debug_putline(logentryid,'STDXVR',221,prog||' Normal completion',user,5,
    program_name=>prog||' '||ver); 
  commit;       
end;
/

CREATE OR REPLACE PROCEDURE STGCSA.SOFVDRINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_CREATUSERID VARCHAR2:=null
   ,p_CREATDT DATE:=null
   ,p_LASTUPDTUSERID VARCHAR2:=null
   ,p_LASTUPDTDT DATE:=null
) as
 /*
  Created on: 2019-02-05 15:27:39
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2019-02-05 15:27:39
    Using SNAP V3.08 6 Sept 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVDRTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVDRTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';  
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVDRINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVDRINSTSP',p_userid,4,
    logtxt1=>'SOFVDRINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVDRINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVDRTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin            
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;   
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then 
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else 
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_CREATUSERID';
      cur_colvalue:=P_CREATUSERID;
      if P_CREATUSERID is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else 
        new_rec.CREATUSERID:=P_CREATUSERID;
      end if;
      cur_colname:='P_CREATDT';
      cur_colvalue:=P_CREATDT;
      if P_CREATDT is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else 
        new_rec.CREATDT:=P_CREATDT;
      end if;
      cur_colname:='P_LASTUPDTUSERID';
      cur_colvalue:=P_LASTUPDTUSERID;
      if P_LASTUPDTUSERID is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else 
        new_rec.LASTUPDTUSERID:=P_LASTUPDTUSERID;
      end if;
      cur_colname:='P_LASTUPDTDT';
      cur_colvalue:=P_LASTUPDTDT;
      if P_LASTUPDTDT is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else 
        new_rec.LASTUPDTDT:=P_LASTUPDTDT;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';      
    end;  
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVDRTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVDRINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVDRINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVDRINSTSP Record Key '||keystouse
      ||' Build: 2019-02-05 15:27:38';
    ROLLBACK TO SOFVDRINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVDRINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVDRINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2019-02-05 15:27:38';
  ROLLBACK TO SOFVDRINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVDRINSTSP',force_log_entry=>TRUE);
--
END SOFVDRINSTSP;
/

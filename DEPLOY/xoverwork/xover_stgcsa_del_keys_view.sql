create or replace view stgcsa.xover_stgcsa_del_keys_view as
select distinct tablenm, cast(pk as varchar2(200)) pk from stgcsa.stgupdtlogtbl 
  where upd_action='D' 
    and updtdt>(select stgcsa_merge_end_dt from stgcsa.xover_status)  
    and tablenm in
      ('SOFVBGW9TBL','SOFVCDCMSTRTBL','SOFVCTCHTBL', 'SOFVDFPYTBL',                   
       'SOFVDUEBTBL','SOFVGNTYTBL','SOFVGRGCTBL','SOFVLND1TBL',                   
       'SOFVLND2TBL','SOFVOTRNTBL','SOFVPYMTTBL','SOFVRTSCTBL',                   
       'SOFVBFFATBL','SOFVBGDFTBL','SOFVFTPOTBL','SOFVDRTBL')
  order by tablenm; 
       

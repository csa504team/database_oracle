CREATE OR REPLACE PACKAGE BODY STGCSA.runtime as
-- Version 3.1 3 Feb 2019 John Low Binary Frond, Select Computing
--
-- V1.04 goes with expanded COREACTVTYLOGTBL
-- V1.05 changes...
-- I had previously changed many procedures to declare/receive numeric parms
--   as varchar2 so that calls passing bad numbers could still make it into
--   the procedure and I could catch the conversion error and report which
--   numeric column had been passed a bad number.  But... this means that for
--   overloaded function VALIDATE_COLUMN_VALUE (which defaults columns passed as
--   null) the varchar2 version of VALIDATE_COLUMN_VALUE was getting invoked
--   and the varchar2 version of VALIDATE_COLUMN_VALUE was not initializing numbers
--   correctly and not initializing number keys from sequences!  This change fixes
--   the varchar2 version of VALIDATE_COLUMN_VALUE to default number columns correctly.
-- Second 1.5 change... added private globals instead of defining common vars redundently.
-- V1.6 16 May JL... add program_name as an optional parm for logger,
--   expose current program info in public package var CSA_PROGRAM.
-- V3.0 29 May 2018 - add FORCE_LOG_ENTRY as LOGGER option, jumped ver# to
--   match new STD procs version.
-- V3.01 added function mysid to expose csa_session_id.
-- V3.02 13 jul 18 jl - fixed bug that PROGRAM_NAME was not passed to DB.
-- V3.1 3 Feb 2019 - add switches for toggling logging action in trigs
--   adds on/off procedure and status check function for audit logging
--   and data history logging.
--   includes 3.05 fix to cover varchar2 in VALIDATE_COLUMN_VALUE  
--
-- Private globals:
  ver varchar2(40):='V3.1 3 Feb 2019';
  fetchval varchar2(4000);
  ans number;
  sqltxt varchar2(200);
  ora_login_user varchar2(30);
  ora_client_host varchar2(80);
  ora_module varchar2(256);
  ora_os_user varchar2(80);
  ora_sid number;
  ora_terminal varchar2(256);
  newlogentryid number;
  audit_logging_on char(1):='Y';
  datahist_logging_on CHAR(1):='Y';
  Y constant char(1):='Y';
  N constant char(1):='N';
--
-- Following switches, procedures, functions provide a stateful switch
--  for trigger audit and data history logging.  
-- All sessions default to true (do all logging) - If either switch
--  is changed, new value persists for remainder of session or changed back 
--
procedure TOGGLE_AUDITING_ON_YN(audit_yn char) is
begin
  if upper(audit_YN)=y then
    audit_logging_on:=y;
  elsif upper(audit_yn)=n then
    audit_logging_on:=n;
  else raise_application_error(-20001,
    'Error - Call to RUNTIME.TOGGLE_AUDITING_ON_YN passed: '||audit_yn
      ||' only Y or N allowed.');  
  end if;  
end;  
function audit_logging_on_YN return CHAR is
begin
  return audit_logging_on;
end;  
--
procedure TOGGLE_DATAHIST_LOGGING_ON_YN(datahist_yn char) is
begin
  if upper(datahist_yn)=y then
    datahist_logging_on:=y;
  elsif upper(datahist_yn)=n then
    datahist_logging_on:=n;
  else raise_application_error(-20001,
    'Error - Call to RUNTIME.TOGGLE_DATAHIST_LOGGING_ON_YN passed: '||datahist_yn
      ||' only Y or N allowed.');  
  end if;  
end;  
function datahist_logging_on_YN return CHAR is
begin
  return datahist_logging_on;
end;  
--
--
function mycsasid return number as
begin
  return csa_session_id;
end;
--
--
function stgkeys(tbl varchar, opt char) return number is
  seqval number;
  seqname varchar2(30);
begin
  case trim(upper(tbl))
  when 'ACCACCELERATELOANTBL' then seqname:='ACCELERATNIDSEQ';
  when 'ACCCYCPRCSTBL' then seqname:='PRCSIDSEQ';
  when 'ACCPRCSCYCSTATTBL' then seqname:='CYCPRCSIDSEQ';
  when 'ACCPRCSCYCTBL' then seqname:='CYCIDSEQ';
  when 'CHRTOPNGNTYSALLTBL' then seqname:='CHRTOPNGNTYSALLIDSEQ';
  when 'CNTCTATTRTBL' then seqname:='CDCCNTCTATTRIDSEQ';
  when 'CNTCTROLETBL' then seqname:='CDCCNTCTROLEIDSEQ';
  when 'CNTCTSTBL' then seqname:='CNTCTSIDSEQ';
  when 'CNTCTTBL' then seqname:='CNTCTIDSEQ';
  when 'COMPRPTTBL' then seqname:='COMPRPTIDSEQ';
  when 'COREACTVTYLOGTBL' then seqname:='LOGENTRYIDSEQ';
  when 'CORECONTROLVALTBL' then seqname:='CONTROLIDSEQ';
  when 'COREEMPATTRTBL' then seqname:='EMPATTRIDSEQ';
  when 'COREEMPATTRTYPTBL' then seqname:='ATTRTYPIDSEQ';
  when 'COREEMPTBL' then seqname:='EMPIDSEQ';
  when 'CORERPTTBL' then seqname:='REPIDSEQ';
  when 'CORETRANSATTRERRTBL' then seqname:='TRANSERRIDSEQ';
  when 'CORETRANSATTRTBL' then seqname:='TRANSATTRIDSEQ';
  when 'CORETRANSMEMOATTRTBL' then seqname:='TRANSMEMOATTRIDSEQ';
  when 'CORETRANSSTATTBL' then seqname:='TRANSSTATIDSEQ';
  when 'CORETRANSTBL' then seqname:='TRANSIDSEQ';
  when 'ENTATTRTBL' then seqname:='ENTATTRIDSEQ';
  when 'ENTSTATTBL' then seqname:='ENTSTATIDSEQ';
  when 'ENTTBL' then seqname:='ENTIDSEQ';
  when 'GENAPPVTBL' then seqname:='APPVIDSEQ';
  when 'GENATTCHTBL' then seqname:='ATTCHIDSEQ';
  when 'GENCNFRMTBL' then seqname:='RVWIDSEQ';
  when 'GENDBFNCTNTBL' then seqname:='FNCTNIDSEQ';
  when 'GENEMAILADRTBL' then seqname:='EMAILADRIDSEQ';
  when 'GENEMAILARCHVTBL' then seqname:='EMAILIDSEQ';
  when 'GENEMAILATTCHTBL' then seqname:='EMAILATTCHIDSEQ';
  when 'GENEMAILCATTBL' then seqname:='EMAILCATIDSEQ';
  when 'GENEMAILDISTROLISTADRTBL' then seqname:='DISTROLISTADRIDSEQ';
  when 'GENEMAILDISTROLISTTBL' then seqname:='DISTROLISTIDSEQ';
  when 'GENEMAILLOANTBL' then seqname:='EMAILLOANIDSEQ';
  when 'GENEMAILTEMPLATETBL' then seqname:='EMAILTEMPLATEIDSEQ';
  when 'GENPACSIMPTDTLTBL' then seqname:='PACSIMPTDTLIDSEQ';
  when 'GENPACSIMPTTBL' then seqname:='PACSIMPTIDSEQ';
  when 'IMPTARPRPTTBL' then seqname:='IMPTARPRPTIDSEQ';
  when 'IMPTCHKMSTRTBL' then seqname:='CHKIMPTCHKMSTRIDSEQ';
  when 'MACROTBL' then seqname:='MACROIDSEQ';
  when 'PRPLOANRQSTUPDTTBL' then seqname:='PRPCDCUPDTIDSEQ';
  when 'PRPMFDUEFROMBORRTBL' then seqname:='PRPMFDUEFROMBORRIDSEQ';
  when 'PRPMFGNTYTBL' then seqname:='PRPMFGNTYIDSEQ';
  when 'PRPMFWIREPOSTINGTBL' then seqname:='WIREPOSTINGIDSEQ';
  when 'PRPRVWSTATTBL' then seqname:='PRPRVWSTATIDSEQ';
  when 'PYMT110RPTTBL' then seqname:='PYMT110IDSEQ';
  when 'PYMTBTCHTBL' then seqname:='BTCHIDSEQ';
  when 'PYMTDRFILETBL' then seqname:='PYMTDRFILEIDSEQ';
  when 'QUERYTBL' then seqname:='QUERYIDSEQ';
  when 'REFACHCHNGPRDTBL' then seqname:='CHGPRDIDSEQ';
  when 'REFADRPRCSTYPTBL' then seqname:='PRCSTYPIDSEQ';
  when 'REFAPPVTYPTBL' then seqname:='APPVTYPIDSEQ';
  when 'REFATTCHTYPTBL' then seqname:='ATTCHTYPIDSEQ';
  when 'REFCALNDRBUSDAYTBL' then seqname:='CALNDRBUSDAYIDSEQ';
  when 'REFCALNDRHOLIDAYTBL' then seqname:='HOLIDAYIDSEQ';
  when 'REFCDPRDTYPTBL' then seqname:='CUDPDTYPIDSEQ';
  when 'REFCNTCTATTRTBL' then seqname:='REFCNTCTATTRIDSEQ';
  when 'REFCNTCTROLETBL' then seqname:='REFCNTCTROLEIDSEQ';
  when 'REFENTATTRTBL' then seqname:='REFENTATTRIDSEQ';
  when 'REFENTSTATTBL' then seqname:='REFENTSTATIDSEQ';
  when 'REFENTTYPTBL' then seqname:='REFENTTYPIDSEQ';
  when 'REFPYMTTRNSFRGROUPTBL' then seqname:='TRNSFRGROUPIDSEQ';
  when 'REFPYMTTYPTBL' then seqname:='PYMTTYPIDSEQ';
  when 'REFRVWTYPTBL' then seqname:='RVWTYPIDSEQ';
  when 'REFTRANSATTRTBL' then seqname:='REFTRANSATTRIDSEQ';
  when 'REFTRANSMEMOATTRTBL' then seqname:='REFTRANSMEMOATTRIDSEQ';
  when 'REFTRANSTYPATTCHPATHTBL' then seqname:='TRANSTYPATTCHPATHIDSEQ';
  when 'REFTRANSTYPRPTTBL' then seqname:='TRANSTYPREPIDSEQ';
  when 'REFTRANSTYPSTATTBL' then seqname:='REFTRANSTYPSTATIDSEQ';
  when 'REFTRANSTYPTBL' then seqname:='TRANSTYPIDSEQ';
  when 'REFTRANSTYPTRANSATTRTBL' then seqname:='TRANSTYPTRANSATTRIDSEQ';
  when 'REFTRANSTYPTRANSMEMOATTRTBL' then seqname:='TRANSTYPMEMOATTRIDSEQ';
  when 'SOEZTBL' then seqname:='SOEZIDSEQ';
  when 'TEMPCDAMORTTBL' then seqname:='TEMPCDAMORTSEQ';
  when 'TEMPGETOGERRRAWTBL' then seqname:='GETOPENGNTYERRRAWIDSEQ';
  when 'TEMPGETOGERRTBL' then seqname:='GETOPENGNTYERRIDSEQ';
  when 'TEMPMACROQUERYTBL' then seqname:='TEMPMACROQUERYSEQ';
  when 'WFCTTBL' then seqname:='WFCTIDSEQ';
  ELSE
    Raise_application_error(-20001,'Key sequence for table '||upper(tbl)
      ||' unknown in stgkeys functions');
  end case;
  if opt='N' then
    execute immediate 'select stgcsa.'||seqname||'.nextval from dual' into seqval;
  else
    execute immediate 'select stgcsa.'||seqname||'.currval from dual' into seqval;
  end if;
  return seqval;
end stgkeys;
function stgnextkey(tbl varchar) return number is
begin
  return stgkeys(tbl,'N');
end;
function stgcurrkey(tbl varchar) return number is
begin
  return stgkeys(tbl,'C');
end;
--
-- Note DEFAULT_COLUMN_VALUE depricated - 
--   not used by 3.x version of STD procedures
-- Procedure DEFAULT_COLUMN_VALUE does core function of defaulting a column.
--   If column is nopt eligable to be defaulted, flag got_defaulted is set false.
--    It is up to caller to handle "got_defaulted" result
-- Overloaded procedure, varchar version
--
procedure DEFAULT_COLUMN_VALUE(inout_column in out varchar2,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL') is
begin
  got_defaulted:=true;
  begin
    select override_value into fetchval from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
    if fetchval is null then
      inout_column:=lpad(' ',collength_in);
    elsif fetchval='$NULL$' then
      inout_column:=null;
    end if;
  exception when no_data_found then
    got_defaulted:=false;
  -- always return useable value even if defaulting prohibited
    inout_column:=lpad(' ',collength_in);
  end;
end;
--
-- Overloaded procedure, number version
procedure DEFAULT_COLUMN_VALUE(inout_column in out number,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL') is
begin
  got_defaulted:=true;
  begin
    select override_value into fetchval from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
    if inout_column is null then
      inout_column:=0;
    elsif fetchval='$NULL$' then
      inout_column:=null;
    elsif substr(fetchval,1,5)='$SEQ:' then
      sqltxt:='select '||substr(fetchval,6)||'.nextval from dual';
      begin
        execute immediate sqltxt into inout_column;
      exception when others then
        inout_column:=0;
        got_defaulted:=false;
      end;
    end if;
  exception when no_data_found then
    got_defaulted:=false;
  -- always return useable value even if defaulting prohibited
    inout_column:=0;
  end;
end;

--
-- Overloaded procedure, date version
procedure DEFAULT_COLUMN_VALUE(inout_column in out date,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL') is
begin
  got_defaulted:=true;
  begin
    select to_date(override_value,'yyyymmddhh24miss')  into inout_column
      from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
    if inout_column is null then
      inout_column:=to_date('19000101000000','yyyymmddhh24miss');
    elsif fetchval='$NULL$' then
      inout_column:=null;
    end if;
  exception when no_data_found then
    got_defaulted:=false;
  -- always return useable value even if defaulting prohibited
    inout_column:=to_date('19000101000000','yyyymmddhh24miss');
  end;
end;

--
-- Overloaded procedure, timestamp version
procedure DEFAULT_COLUMN_VALUE(inout_column in out timestamp,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL') is
begin
  got_defaulted:=true;
  begin
    select to_date(override_value,'yyyymmddhh24miss')  into inout_column
      from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
    if inout_column is null then
      inout_column:=to_timestamp('19000101000000','yyyymmddhh24miss');
    elsif fetchval='$NULL$' then
      inout_column:=null;
    end if;
  exception when no_data_found then
    got_defaulted:=false;
  -- always return useable value even if defaulting prohibited
    inout_column:=to_timestamp('19000101000000','yyyymmddhh24miss');
  end;
end;

--
--
procedure logger_safewrite(
  logentryid number,
  msg_cat varchar2,
  msg_id number,
  msg_txt varchar2,
  userid varchar2,
  log_sev number:=1,
  usernm varchar2:=null,
  loannmb char:=null,
  transid number:=null,
  dbmdl varchar:=null,
  entrydt date:=null,
  timestampfield date:=null,
  logtxt1 varchar2:=null,
  logtxt2 varchar2:=null,
  logtxt3 varchar2:=null,
  logtxt4 varchar2:=null,
  logtxt5 varchar2:=null,
  lognmb1 number:=null,
  lognmb2 number:=null,
  lognmb3 number:=null,
  lognmb4 number:=null,
  lognmb5 number:=null,
  logdt1 date:=null,
  logdt2 date:=null,
  logdt3 date:=null,
  logdt4 date:=null,
  logdt5 date:=null,
  program_name varchar2:=null) as
pragma autonomous_transaction;
begin
  insert into stgcsa.coreACTVTYlogTBL values(logentryid, loannmb, transid,
      dbmdl, msg_cat, msg_txt, sysdate, userid, usernm, timestampfield,
      userid, sysdate, userid, sysdate,msg_id,log_sev,program_name||'/'||ora_module,
      ora_login_user, ora_sid, null,null, ora_os_user,
      null,csa_session_logon_time,CSA_session_id,
      logtxt1, logtxt2, logtxt3, logtxt4, logtxt5,
      lognmb1, lognmb2, lognmb3, lognmb4, lognmb5,
      logdt1, logdt2, logdt3, logdt4, logdt5);
  commit;
  return;
end;

/*fetchval varchar2(4000);
  ans number;
  sqltxt varchar2(200);
  ora_login_user varchar2(30);
  ora_client_host varchar2(80);
  ora_module varchar2(256);
  ora_os_user varchar2(80);
  ora_sid number;
  ora_terminal varchar2(256);
  newlogentryid number; */
--
--
procedure logger(
  logentryid out number,
  msg_cat varchar2,
  msg_id number,
  msg_txt varchar2,
  userid varchar2,
  log_sev number:=1,
  usernm varchar2:=null,
  loannmb char:=null,
  transid number:=null,
  dbmdl varchar:=null,
  entrydt date:=null,
  timestampfield date:=null,
  logtxt1 varchar2:=null,
  logtxt2 varchar2:=null,
  logtxt3 varchar2:=null,
  logtxt4 varchar2:=null,
  logtxt5 varchar2:=null,
  lognmb1 number:=null,
  lognmb2 number:=null,
  lognmb3 number:=null,
  lognmb4 number:=null,
  lognmb5 number:=null,
  logdt1 date:=null,
  logdt2 date:=null,
  logdt3 date:=null,
  logdt4 date:=null,
  logdt5 date:=null,
  program_name varchar2:=null,
  force_log_entry boolean:=false) as
begin
  -- currently program start messages are ignored
  -- future enhancement will be to stack PGM start events and retain timestamp
  -- and other mertics and log them as part of PGM end.
  if msg_id=100 or log_sev=4 then
    return;
  end if;
  select stgcsa.logentryidseq.nextval into logentryid from dual;
  if program_name is not null then
    csa_program:=program_name;
  else
    csa_program:=null;
  end if;
  if csa_userid is null then
    csa_userid:=userid;
  end if;
  if force_log_entry=TRUE then
    logger_safewrite(logentryid, msg_cat, msg_id, msg_txt, userid, log_sev,
        usernm, loannmb, transid, dbmdl, entrydt, timestampfield,
        logtxt1, logtxt2, logtxt3, logtxt4, logtxt5,
        lognmb1, lognmb2, lognmb3, lognmb4, lognmb5,
        logdt1, logdt2, logdt3, logdt4, logdt5,csa_program);
  else
    insert into stgcsa.coreACTVTYlogTBL values(logentryid, loannmb, transid,
      dbmdl, msg_cat, msg_txt, sysdate, userid, usernm, timestampfield,
      userid, sysdate, userid, sysdate,msg_id,log_sev,program_name||'/'||ora_module,
      ora_login_user, ora_sid, null,null, ora_os_user,null,csa_session_logon_time,CSA_session_id,
      logtxt1, logtxt2, logtxt3, logtxt4, logtxt5,
      lognmb1, lognmb2, lognmb3, lognmb4, lognmb5,
      logdt1, logdt2, logdt3, logdt4, logdt5);
  end if;
  return;
end;
--
-- Procedure VALIDATE_COLUMN_VALUE does same function as DEFAULT_COLUMN_VALUE, but...
--   it tests for nulls and it accumulates errors in a defined way.
-- Caller generally processes all columns then tests errfound to see if any had errors.
-- Intendeded side effects: can set errfound true, append diag info to errstr
--
-- Overloaded procedure, varchar version
function VALIDATE_COLUMN_VALUE(input_value varchar2,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return varchar2 is
begin
  if input_value is not null then
    return input_value;
  end if;
  begin
    select override_value into fetchval
    from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
      if fetchval is null then
        if datatype_in='NUMBER' then fetchval:=0; end if;
        if datatype_in='CHAR' then fetchval:=lpad(' ',collength_in); end if;
        if datatype_in='VARCHAR' then fetchval:=' '; end if;
        if datatype_in='VARCHAR2' then fetchval:=' '; end if;
        if datatype_in='DATE' then fetchval:=to_date('01011900','ddmmyyyy'); end if;
        if datatype_in='TIMESTAMP' then fetchval:=to_date('01011900','ddmmyyyy'); end if;
      end if;
      if fetchval='$NULL$' then
        fetchval:=null;
      elsif substr(fetchval,1,5)='$SEQ:' then
        -- v1.5 fix, an (intended) numeric parm could be processed by this (varchar2)
        -- version of procedure, handle defaulting from a sequence!
        sqltxt:='select '||substr(fetchval,6)||'.nextval from dual';
        begin
          execute immediate sqltxt into ans;
          fetchval:=to_char(ans);
        exception when others then
          if length(errstr)<(errstrmax-200) then
            errstr:=errstr||' ,Error '||sqlcode||' defaulting Number Col: '||COLUMN_NAME_IN
              ||' using sequence '||substr(fetchval,6)||', ORA MSG: '||SQLERRM(SQLCODE);
          end if;
        end;
      end if;
    return fetchval;
  exception when no_data_found then
    -- this column is not eligable to be defaulted
    errfound:=true;
    if length(errstr)<errstrmax then
      errstr:=errstr||', '||column_name_in;
    end if;
  -- always return useable value even if defaulting prohibited
    if datatype_in='NUMBER' then
      return '0';
    else
      return lpad(' ',collength_in);
    end if;
  end;
end;
--
-- Overloaded procedure, date version
function VALIDATE_COLUMN_VALUE(input_value date,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return date is
  ansdate date;
begin
  if input_value is not null then
    return input_value;
  end if;
  begin
    select nvl(override_value, '19000101000000') into fetchval
      from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
      if fetchval='$NULL$' then
        ansdate:=null;
      else
        begin
          ansdate:=to_date(fetchval,'YYYYMMDDHH24MISS');
        exception when others then
          if length(errstr)<errstrmax then
            errstr:=errstr||' ,Error defaulting Date Col: '||COLUMN_NAME_IN
              ||' with value from REFCOLSTBL: '||fetchval;
          end if;
          -- always return useable value even if defaulting prohibited
          return to_date('19000101','yyyymmdd');
        end;
      end if;
    return ansdate;
  exception when no_data_found then
    -- this column is not eligable to be defaulted
    errfound:=true;
    if length(errstr)<errstrmax then
      errstr:=errstr||', '||column_name_in;
    end if;
  -- always return useable value even if defaulting prohibited
    return to_date('19000101','yyyymmdd');
  end;
end;
--
-- Overloaded procedure, timestamp version
function VALIDATE_COLUMN_VALUE(input_value timestamp,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return timestamp is
  anstsp timestamp;
begin
  if input_value is not null then
    return input_value;
  end if;
  begin
    select nvl(override_value, '19000101000000') into fetchval
      from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
      if fetchval='$NULL$' then
        anstsp:=null;
      else
        begin
          anstsp:=to_timestamp(fetchval,'YYYYMMDDHH24MISS');
        exception when others then
          if length(errstr)<errstrmax then
            errstr:=errstr||' ,Error defaulting Timestamp Col: '||COLUMN_NAME_IN
              ||' with value from REFCOLSTBL: '||fetchval;
          end if;
          -- always return useable value even if defaulting prohibited
          return to_timestamp('19000101','yyyymmdd');
        end;
      end if;
    return anstsp;
  exception when no_data_found then
    -- this column is not eligable to be defaulted
    errfound:=true;
    if length(errstr)<errstrmax then
      errstr:=errstr||', '||column_name_in;
    end if;
  -- always return useable value even if defaulting prohibited
    return to_timestamp('19000101','yyyymmdd');
  end;
end;

--
-- Overloaded procedure, varchar version
function VALIDATE_COLUMN_VALUE(input_value number,
    table_owner_in varchar2, table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return number is
begin
  if input_value is not null then
    return input_value;
  end if;
  begin
    select nvl(override_value, '0') into fetchval
      from stgcsa.DEFAULT_COLUMN_VALUES
      where process_scope=upper(process_scope) and table_owner=upper(table_owner_in)
        and table_name=upper(table_name_in) and column_name=upper(column_name_in);
  dbms_output.put_line('in VALIDATE_COLUMN_VALUE Number... tab='||table_name_in||' col='||column_name_in||' fetchval='||fetchval);
      if fetchval='$NULL$' then
        ans:=null;
      elsif substr(fetchval,1,5)='$SEQ:' then
        sqltxt:='select '||substr(fetchval,6)||'.nextval from dual';
        begin
          execute immediate sqltxt into ans;
          exception when others then
            if length(errstr)<(errstrmax-200) then
              errstr:=errstr||' ,Error '||sqlcode||' defaulting Number Col: '||COLUMN_NAME_IN
                ||' using sequence '||substr(fetchval,6)||', ORA MSG: '||SQLERRM(SQLCODE);
            end if;
            -- always return useable value even if defaulting prohibited
          return 0;
        end;
      else
        begin
          ans:=to_number(fetchval);
        exception when others then
          if length(errstr)<errstrmax then
            errstr:=errstr||' ,Error defaulting Number Col: '||COLUMN_NAME_IN
              ||' with value from REFCOLSTBL: '||fetchval;
          end if;
          -- always return useable value even if defaulting prohibited
          return 0;
        end;
      end if;
    return ans;
  exception when no_data_found then
    -- this column is not eligable to be defaulted
    errfound:=true;
    if length(errstr)<errstrmax then
      errstr:=errstr||', '||column_name_in;
    end if;
  -- always return useable value even if defaulting prohibited
    return 0;
  end;
end;
--
-- package initialization routine
begin
  audit_logging_on:=y;
  datahist_logging_on:=y;
  SELECT sysdate, stgcsa.csalogsidseq.nextval,
       SYS_CONTEXT('USERENV', 'SESSION_USER'),
       SYS_CONTEXT ('USERENV', 'HOST'),
       SYS_CONTEXT ('USERENV', 'MODULE'),
       SYS_CONTEXT ('USERENV', 'OS_USER'),
       to_number(SYS_CONTEXT ('USERENV', 'SID')),
       SYS_CONTEXT ('USERENV', 'TERMINAL'),
       stgcsa.logentryidseq.nextval
  into csa_session_logon_time, CSA_session_id,
    ora_login_user, ora_client_host, ora_module, ora_os_user,
    ora_sid, ora_terminal, newlogentryid
  FROM DUAL;
     logger_safewrite(newlogentryid, 'STGCSASESS', 1,
      'Runtime '||ver||' initialized for session '||CSA_SESSION_ID||' for Oracle user '||ora_login_user,
       ora_login_user, 0,ora_login_user, null,null,null,csa_session_logon_time);
--
--
end runtime;
/
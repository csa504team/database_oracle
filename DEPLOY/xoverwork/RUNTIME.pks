CREATE OR REPLACE PACKAGE STGCSA.runtime as
--
-- Version 3.1 3 Feb 2019 JL
-- public pkg vars
errfound boolean;
errstr varchar2(4000):=' ';
errstrmax number:=3800;
CSA_session_id number;
csa_userid varchar2(60);
csa_session_logon_time date;
CSA_program varchar2(80);
function mycsasid return number;
function stgnextkey(tbl varchar) return number;
function stgcurrkey(tbl varchar) return number;
procedure toggle_auditing_on_YN(audit_yn char);
function audit_logging_on_YN return char;
procedure toggle_datahist_logging_on_yn(datahist_yn char);
function datahist_logging_on_YN return char;
--
-- Procedure DEFAULT_COLUMN_VALUE -- overloaded
procedure DEFAULT_COLUMN_VALUE(inout_column in out varchar2,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL');
--
procedure DEFAULT_COLUMN_VALUE(inout_column in out date,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL');
--
procedure DEFAULT_COLUMN_VALUE(inout_column in out number,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL');
--
procedure DEFAULT_COLUMN_VALUE(inout_column in out timestamp,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, got_defaulted out boolean,
    process_scope varchar2:='GLOBAL');
--
-- Function VALIDATE_COLUMN_VALUE -- overloaded
function VALIDATE_COLUMN_VALUE(input_value varchar2,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return varchar2;
--
function VALIDATE_COLUMN_VALUE(input_value date,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return date;
--
function VALIDATE_COLUMN_VALUE(input_value number,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return number;
--
function VALIDATE_COLUMN_VALUE(input_value timestamp,
    table_owner_in varchar2,
    table_name_in varchar2, column_name_in varchar2,
    datatype_in varchar2, collength_in number, process_scope varchar2:='GLOBAL')
  return timestamp;
    --
    --
procedure logger(
  logentryid out number,
  msg_cat varchar2,
  msg_id number,
  msg_txt varchar2,
  userid varchar2,
  log_sev number:=1,
  usernm varchar2:=null,
  loannmb char:=null,
  transid number:=null,
  dbmdl varchar:=null,
  entrydt date:=null,
  timestampfield date:=null,
  logtxt1 varchar2:=null,
  logtxt2 varchar2:=null,
  logtxt3 varchar2:=null,
  logtxt4 varchar2:=null,
  logtxt5 varchar2:=null,
  lognmb1 number:=null,
  lognmb2 number:=null,
  lognmb3 number:=null,
  lognmb4 number:=null,
  lognmb5 number:=null,
  logdt1 date:=null,
  logdt2 date:=null,
  logdt3 date:=null,
  logdt4 date:=null,
  logdt5 date:=null,
  program_name varchar2:=null,
  force_log_entry boolean:=false);
--
end runtime;
/
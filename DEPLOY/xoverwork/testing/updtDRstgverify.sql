connect jasleen/Li1lly_Tree@stgcsa2
set pagesize 1000
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss';
set pagesize 1000
set echo on
spool &1 append
prompt $$$ validating queries on STGCSA after merge
set pagesize 100
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss';
                          
select 'SOFVDRTBL', count(*) from stgcsa.SOFVDRTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                              
exit
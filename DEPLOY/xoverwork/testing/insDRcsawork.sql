connect jasleen/Li1lly_Tree@csa1
set pagesize 1000
alter session set nls_date_format='YYYY-MM-DD hh24:mi:ss';
spool &1 append
prompt $$$ csa simulated work before crossover
prompt first select row counts for new updates brought in
                                                       
select 'SOFVDRTBL', count(*) from csa.SOFVDRTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);

--prompt now add different new rows into the csa tables

insert into csa.SOFVDRTBL                  
   (LOANNMB, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT)
 Values
   ('1000000006', 'CSA_LOAD', TO_DATE('3/27/2017 12:06:50 PM', 'MM/DD/YYYY HH:MI:SS AM'), 'CSA_LOAD', 
    (select TO_DATE(TO_CHAR(sysdate, 'MM/DD/YYYY HH:MI:SS AM'),'MM/DD/YYYY HH:MI:SS AM') from dual));
COMMIT;

prompt csa always empties ftpo table!
delete from csa.sofvftpotbl;
exit
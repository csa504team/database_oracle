connect jasleen/Li1lly_Tree@csa1
set pagesize 1000
alter session set nls_date_format='YYYY-MM-DD hh24:mi:ss';
spool &1 append
prompt $$$ csa simulated work before crossover
prompt first select row counts for new updates brought in

                                                                                
select 'SOFVDRTBL', count(*) from csa.SOFVDRTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);



prompt now do some updates to send back


update csa.SOFVDRTBL                   
 set lastupdtuserid='YOU', lastupdtdt=sysdate where loannmb='1000000000';
 
prompt csa always empties ftpo table!
delete from csa.sofvftpotbl;
exit
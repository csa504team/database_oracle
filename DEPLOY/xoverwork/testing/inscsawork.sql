connect jasleen/Li1lly_Tree@csa1
set pagesize 1000
alter session set nls_date_format='YYYY-MM-DD hh24:mi:ss';
spool &1 append
prompt $$$ csa simulated work before crossover
prompt first select row counts for new updates brought in
select 'SOFVAUDTTBL', count(*) from csa.SOFVAUDTTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBFFATBL', count(*) from csa.SOFVBFFATBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBGDFTBL', count(*) from csa.SOFVBGDFTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBGW9TBL', count(*) from csa.SOFVBGW9TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVCDCMSTRTBL', count(*) from csa.SOFVCDCMSTRTBL                       
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVCTCHTBL', count(*) from csa.SOFVCTCHTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVDFPYTBL', count(*) from csa.SOFVDFPYTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVDUEBTBL', count(*) from csa.SOFVDUEBTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVFTPOTBL', count(*) from csa.SOFVFTPOTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVGNTYTBL', count(*) from csa.SOFVGNTYTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVGRGCTBL', count(*) from csa.SOFVGRGCTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVLND1TBL', count(*) from csa.SOFVLND1TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVLND2TBL', count(*) from csa.SOFVLND2TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVOTRNTBL', count(*) from csa.SOFVOTRNTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVPYMTTBL', count(*) from csa.SOFVPYMTTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVRTSCTBL', count(*) from csa.SOFVRTSCTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);

select max(lastupdtdt) from csa.sofvlnd1tbl;
select * from csa.xover_status;

prompt now add different new rows into the csa tables
update stgcsa.newrows_SOFVBGW9TBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVBGW9TBL                   
  select * from stgcsa.newrows_SOFVBGW9TBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVCDCMSTRTBL@stgcsa2                
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVCDCMSTRTBL                
  select * from stgcsa.newrows_SOFVCDCMSTRTBL@stgcsa2                
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVCTCHTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVCTCHTBL                   
  select * from stgcsa.newrows_SOFVCTCHTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVDFPYTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVDFPYTBL                   
  select * from stgcsa.newrows_SOFVDFPYTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVDUEBTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVDUEBTBL                   
  select * from stgcsa.newrows_SOFVDUEBTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVFTPOTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVFTPOTBL                   
  select * from stgcsa.newrows_SOFVFTPOTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVGNTYTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVGNTYTBL                   
  select * from stgcsa.newrows_SOFVGNTYTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVGRGCTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVGRGCTBL                   
  select * from stgcsa.newrows_SOFVGRGCTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVLND1TBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVLND1TBL                   
  select * from stgcsa.newrows_SOFVLND1TBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVLND2TBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVLND2TBL                   
  select * from stgcsa.newrows_SOFVLND2TBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVOTRNTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVOTRNTBL                   
  select * from stgcsa.newrows_SOFVOTRNTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVPYMTTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVPYMTTBL                   
  select * from stgcsa.newrows_SOFVPYMTTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVRTSCTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVRTSCTBL                   
  select * from stgcsa.newrows_SOFVRTSCTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVBFFATBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVBFFATBL                   
  select * from stgcsa.newrows_SOFVBFFATBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;
                                                                                
update stgcsa.newrows_SOFVBGDFTBL@stgcsa2                   
 set lastupdtdt=sysdate where lastupdtdt<sysdate-2/1440;
insert into csa.SOFVBGDFTBL                   
  select * from stgcsa.newrows_SOFVBGDFTBL@stgcsa2                   
     where lastupdtdt>=sysdate-10/86400;

prompt csa always empties ftpo table!
delete from csa.sofvftpotbl;
exit
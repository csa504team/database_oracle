List file markers (to aid finding steps in lst file)
------------------------------------------------------
$$$0 starting now  - (init, starts with showing xover_status from both sides)
$$$1 run STGWORK before crossover - ready to run ...stgwork script
$$$2 run STGCSA export
$$$3 run CSA import
$$$4 query state info on csa after import before merge 
$$$5 run merge on csa 
$$$6 query state info on csa after merge  
$$$7 run CSAWORK before crossover ...csawork script
$$$8 run CSA export
$$$9 run STGCSA import
$$$10 query state info, log entries on stg after import before merge 
$$$11 run merge on stgcsa
$$$12 query state info on stgcsa after merge 
$$$13 run STGVERIFY � queries to show expected data changes 



Test cases - each test case "xxx" must have a xxxstgwork.sql
             and xxxcsawork.sql in "testing" directory
------------------------------------------------------------------
TEST0 - No updateing is done in work steps, should run xover with 
        no updateing action
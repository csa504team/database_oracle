connect jasleen/Li1lly_Tree@stgcsa2
set pagesize 1000
spool &1 append
prompt $$$ STGCSA2 simulated work before crossover
prompt delete 9 rows from each crossover table
delete from stgcsa.SOFVBGW9TBL where rownum<10;                   
delete from stgcsa.SOFVCDCMSTRTBL where rownum<10;                
delete from stgcsa.SOFVCTCHTBL where rownum<10;                   
delete from stgcsa.SOFVDFPYTBL where rownum<10;                   
delete from stgcsa.SOFVDUEBTBL where rownum<10;                   
delete from stgcsa.SOFVFTPOTBL where rownum<10;                   
delete from stgcsa.SOFVGNTYTBL where rownum<10;                   
delete from stgcsa.SOFVGRGCTBL where rownum<10;                   
delete from stgcsa.SOFVLND1TBL where rownum<10;                   
delete from stgcsa.SOFVLND2TBL where rownum<10;                   
delete from stgcsa.SOFVOTRNTBL where rownum<10;                   
delete from stgcsa.SOFVPYMTTBL where rownum<10;                   
delete from stgcsa.SOFVRTSCTBL where rownum<10;                   
delete from stgcsa.SOFVBFFATBL where rownum<10;                   
delete from stgcsa.SOFVBGDFTBL where rownum<10;    
exit
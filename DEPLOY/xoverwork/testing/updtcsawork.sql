connect jasleen/Li1lly_Tree@csa1
set pagesize 1000
alter session set nls_date_format='YYYY-MM-DD hh24:mi:ss';
spool &1 append
prompt $$$ csa simulated work before crossover
prompt first select row counts for new updates brought in
select 'SOFVAUDTTBL', count(*) from csa.SOFVAUDTTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBFFATBL', count(*) from csa.SOFVBFFATBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBGDFTBL', count(*) from csa.SOFVBGDFTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVBGW9TBL', count(*) from csa.SOFVBGW9TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVCDCMSTRTBL', count(*) from csa.SOFVCDCMSTRTBL                       
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVCTCHTBL', count(*) from csa.SOFVCTCHTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVDFPYTBL', count(*) from csa.SOFVDFPYTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVDUEBTBL', count(*) from csa.SOFVDUEBTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVFTPOTBL', count(*) from csa.SOFVFTPOTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVGNTYTBL', count(*) from csa.SOFVGNTYTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVGRGCTBL', count(*) from csa.SOFVGRGCTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVLND1TBL', count(*) from csa.SOFVLND1TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVLND2TBL', count(*) from csa.SOFVLND2TBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVOTRNTBL', count(*) from csa.SOFVOTRNTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVPYMTTBL', count(*) from csa.SOFVPYMTTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);
                                                                                
select 'SOFVRTSCTBL', count(*) from csa.SOFVRTSCTBL                             
  where lastupdtdt>                           
    (select stgcsa_merge_end_dt from csa.xover_status);

select max(lastupdtdt) from csa.sofvlnd1tbl;
select * from csa.xover_status;

prompt now do some updates to send back

update csa.SOFVBGW9TBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVCDCMSTRTBL                
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVCTCHTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVDFPYTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVDUEBTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVGNTYTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVGRGCTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVLND1TBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVLND2TBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVOTRNTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVPYMTTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVRTSCTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVBFFATBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
                                                                                
update csa.SOFVBGDFTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<2;
 
prompt csa always empties ftpo table!
delete from csa.sofvftpotbl;
exit
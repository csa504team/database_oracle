directory=dp2
dumpfile=xover_baseline_from_stgcsa1
table_exists_action=truncate
remap_schema=stgcsa:csa
remap_tablespace=stgcsadata:csadata
tables=stgcsa.SOFVBFFATBL,   
  stgcsa.SOFVBGDFTBL,                   
  stgcsa.SOFVBGW9TBL,                   
  stgcsa.SOFVCDCMSTRTBL,                
  stgcsa.SOFVCTCHTBL,                   
  stgcsa.SOFVDFPYTBL,                   
  stgcsa.SOFVDUEBTBL,                   
  stgcsa.SOFVGNTYTBL,                   
  stgcsa.SOFVGRGCTBL,                   
  stgcsa.SOFVLND1TBL,                   
  stgcsa.SOFVLND2TBL,                   
  stgcsa.SOFVOTRNTBL,                   
  stgcsa.SOFVRTSCTBL,
  stgcsa.SOFVPYMTtbl,
  stgcsa.SOFVAUDTTBL,
  stgcsa.sofvftpotbl

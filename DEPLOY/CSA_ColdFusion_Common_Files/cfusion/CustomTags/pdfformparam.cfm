<!--- Make sure we have NAME and VALUE --->
<cfif NOT IsDefined("ATTRIBUTES.name")>
	<cfthrow message="NAME is required">
</cfif>
<cfif NOT IsDefined("ATTRIBUTES.value")>
	<cfthrow message="VALUE is required">
</cfif>

<!--- Just pass 'em up to the parent --->
<cfassociate basetag="cf_pdfform">

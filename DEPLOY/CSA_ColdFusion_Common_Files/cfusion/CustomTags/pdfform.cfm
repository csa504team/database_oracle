<!--- Save XML form fields to PDF --->
<cffunction name="SetPDFFormFields" output="no" returntype="void">
	<!--- Original form --->
	<cfargument name="PDFForm" type="string" required="yes">
	<!--- Destination form form --->
	<cfargument name="PDFOutput" type="string" required="yes">
	<!--- XML data (form field values) --->
	<cfargument name="XMLData" type="xml" required="yes">

	<cfscript>
	// Init all vars
	var formIS="";
	var PDFfactory="";
	var PDFdoc="";
	var bytes="";
	var byteArrayIS="";
	var outputIS="";
	var byteClass="";
	var PDFBytes="";
	var outFile="";
	var outFileStream="";

	// PDF form input stream
	formIS=CreateObject("java", "java.io.FileInputStream");
	formIS.init(ARGUMENTS.PDFForm);
	
	// Get PDF document object
	PDFfactory=CreateObject("java", "com.adobe.pdf.PDFFactory");
	PDFdoc=PDFfactory.openDocument(formIS);
	
	// Convert raw XML data to byte array
	bytes=ARGUMENTS.XMLData.getBytes("UTF-8");
	byteArrayIS=CreateObject("java", "java.io.ByteArrayInputStream");
	byteArrayIS.init(bytes);
	
	// Perform actual import
	PDFdoc.importFormData(byteArrayIS);
	
	//Call the PDFDocument object's save method
	outputIS=PDFdoc.save();
	
	// Read bytes
	byteClass=CreateObject("java", "java.lang.Byte").TYPE;
	PDFBytes=CreateObject("java", "java.lang.reflect.Array").newInstance(byteClass, outputIS.available());
	
	// And store them
	outputIS.read(PDFBytes);
	
	// Initialize output file
	outFile=CreateObject("java", "java.io.File");
	outFile.init(ARGUMENTS.PDFOutput);
	
	// Write output file
	outFileStream=CreateObject("java", "java.io.FileOutputStream");
	outFileStream.init(outFile);
	outFileStream.write(PDFBytes);
	
	// All done
	outFileStream.close();
	</cfscript>

	<cfreturn>
</cffunction>


<!--- Extract XML form fields from PDF --->
<cffunction name="GetPDFFormFields" output="no" returntype="xml">
	<cfargument name="PDFForm" type="string" required="yes">

	<cfscript>
	// Init all vars
	var formIS="";
	var PDFfactory="";
	var PDFdoc="";
	var formType="";
	var formDataFormat="";
	var formDataIS="";
	var byteClass="";
	var XMLBytes="";
	// Initialize return var
	var result="";
	
	// PDF form input stream
	formIS=CreateObject("java", "java.io.FileInputStream");
	formIS.init(ARGUMENTS.PDFForm);
	
	// Get PDF document object
	PDFfactory=CreateObject("java", "com.adobe.pdf.PDFFactory");
	PDFdoc=PDFfactory.openDocument(formIS);
	
	// Get formtype object
	formType=PDFDoc.getFormType();
	
	// Get a formdataformat object
	formDataFormat=CreateObject("java", "com.adobe.pdf.FormDataFormat");
	
	// If have form data, read it
	if ((formType EQ FormType.XML_FORM)
		OR (formType EQ FormType.ACROFORM))
	{
		// Extract data (XFA or XFDF, as needed)
		if (formType EQ FormType.XML_FORM)
			formDataIS=PDFdoc.exportFormData(formDataFormat.XFA);
		else
			formDataIS=PDFdoc.exportFormData(formDataFormat.XFDF);
	
		// Read bytes
		byteClass=CreateObject("java", "java.lang.Byte").TYPE;
		XMLBytes=CreateObject("java", "java.lang.reflect.Array").newInstance(byteClass, formDataIS.available());
		
		// Read data
		formDataIS.read(XMLBytes);
		
		// Convert XML to string
		result=ToString(XMLBytes);
	}
	</cfscript>

	<cfreturn result>
</cffunction>


<!--- What type of form? --->
<cffunction name="GetFormType" output="no" returntype="numeric">
	<cfargument name="PDFForm" type="string" required="yes">

	<cfscript>
	// Init all vars
	var formIS="";
	var PDFfactory="";
	var PDFdoc="";
	var formType="";
	var result="";
	
	// PDF form input stream
	formIS=CreateObject("java", "java.io.FileInputStream");
	formIS.init(ARGUMENTS.PDFForm);
	
	// Get PDF document object
	PDFfactory=CreateObject("java", "com.adobe.pdf.PDFFactory");
	PDFdoc=PDFfactory.openDocument(formIS);
	
	// Get formtype object
	formType=PDFDoc.getFormType();	

	// Determine type
	if (formType EQ FormType.XML_FORM)
		result=1; // XFA
	else
		result=2; // XFDF
	</cfscript>

	<cfreturn result>
</cffunction>


<!--- Generate XFA XML from name=value pairs --->
<cffunction name="CreateXFA" returntype="string" access="private" output="no">
	<cfargument name="Fields" type="array" required="yes">

	<cfset var PDFFields=XmlNew()>
	<cfset var i=0>

	<cfscript>
	// Create XML root
	PDFFields.Root=XmlElemNew(PDFFields,"Root");
	// Loop through fields
	for (i=1; i LTE ArrayLen(ARGUMENTS.Fields); i=i+1)
	{
		// And add each
		PDFFields.Root.XmlChildren[i]=XmlElemNew(PDFFields,"#ARGUMENTS.Fields[i].name#");
		PDFFields.Root.XmlChildren[i].XmlText=ARGUMENTS.Fields[i].value;
	}
	</cfscript>

	<cfreturn ToString(PDFFields)>

</cffunction>


<!--- Generate XFDF XML from name=value pairs --->
<cffunction name="CreateXFDF" returntype="string" access="private" output="no">
	<cfargument name="Fields" type="array" required="yes">

	<cfset var XFDFFields=XmlNew()>
	<cfset var i=0>

	<cfscript>
	// Create XML root
	XFDFFields.xfdf=XmlElemNew(XFDFFields, "xfdf");

	// Create fields child	
	XFDFFields.xfdf.XmlChildren[1]=XmlElemNew(XFDFFields, "fields");

	// Loop through fields
	for (i=1; i LTE ArrayLen(ARGUMENTS.Fields); i=i+1)
	{
		// And add each
		XFDFFields.xfdf.fields.XmlChildren[i]=XmlElemNew(XFDFFields, "field");
		XFDFFields.xfdf.fields.XmlChildren[i].XmlAttributes["name"]=ARGUMENTS.Fields[i].name;
		XFDFFields.xfdf.fields.XmlChildren[i].value=XmlElemNew(XFDFFields, "value");
		XFDFFields.xfdf.fields.XmlChildren[i].value.XmlText=ARGUMENTS.Fields[i].value;
	}
	</cfscript>

	<cfreturn ToString(XFDFFields)>

</cffunction>


<!--- Extract XFA XML form data into CF structure --->
<cffunction name="ExtractXFA" returntype="struct" access="private" output="no">
	<cfargument name="Data" type="string" required="yes">

	<cfscript>
	var result=StructNew();
	var XMLData="";
	var fields="";
	var f="";

	// Parse returned XML
	XMLData=XMLParse(XMLFields);

	// Find all nodes without children
	fields=XMLSearch(XMLData, "//*[not(*)]");
	
	// Loop through fields to populate result structure
	for (f=1; f LTE ArrayLen(fields); f=f+1)
	{
		// Strip PageCount and CurrentPage if they exist
		if (fields[f].XmlName IS NOT "PageCount"
			AND fields[f].XmlName IS NOT "CurrentPage")
		{
			// Write to result structure
			StructInsert(result,
						fields[f].XmlName,
						fields[f].XmlText,
						true);
		}
	}
	</cfscript>

	<cfreturn result>

</cffunction>


<!--- Extract XFDF XML form data into CF structure --->
<cffunction name="ExtractXFDF" returntype="struct" access="private" output="no">
	<cfargument name="Data" type="string" required="yes">

	<cfscript>
	var result=StructNew();
	var XMLData="";
	var f="";

	// Parse returned XML
	XMLData=XMLParse(XMLFields);

	// Loop through fields to populate result structure
	for (f=1; f LTE ArrayLen(XMLData.xfdf.fields.XmlChildren); f=f+1)
	{
		// Check if has value
		if (ArrayLen(XMLData.xfdf.fields.XmlChildren[f].XmlChildren))
		{
			// Write to result structure
			StructInsert(result,
						XMLData.xfdf.fields.XmlChildren[f].XmlAttributes.name,
						XMLData.xfdf.fields.XmlChildren[f].XmlChildren[1].XmlText,
						true);
		}
	}
	</cfscript>

	<cfreturn result>

</cffunction>


<!--- Get XFA field names --->
<cffunction name="GetXFAFields" returntype="array" access="private" output="no">
	<cfargument name="Data" type="string" required="yes">

	<cfscript>
	var result=ArrayNew(1);
	var XMLData="";
	var fields="";
	var f="";

	// Parse returned XML
	XMLData=XMLParse(XMLFields);

	// Find all nodes without children
	fields=XMLSearch(XMLData, "//*[not(*)]");
	
	// Loop through fields to populate result structure
	for (f=1; f LTE ArrayLen(fields); f=f+1)
	{
		// Strip PageCount and CurrentPage if they exist
		if (fields[f].XmlName IS NOT "PageCount"
			AND fields[f].XmlName IS NOT "CurrentPage")
		{
			// Save field name
			ArrayAppend(result, fields[f].XmlName);
		}
	}
	</cfscript>

	<cfreturn result>

</cffunction>


<!--- Get XFDF field names --->
<cffunction name="GetXFDFFields" returntype="array" access="private" output="no">
	<cfargument name="Data" type="string" required="yes">

	<cfscript>
	var result=ArrayNew(1);
	var XMLData="";
	var f="";

	// Parse returned XML
	XMLData=XMLParse(XMLFields);

	// Loop through fields to populate result structure
	for (f=1; f LTE ArrayLen(XMLData.xfdf.fields.XmlChildren); f=f+1)
	{
		// Save field name
		ArrayAppend(result, XMLData.xfdf.fields.XmlChildren[f].XmlAttributes.name);
	}
	</cfscript>

	<cfreturn result>

</cffunction>


<!---
Here starts tag processing:
Tag is used with an end tag for ACTION="set",
and without an end tag for ACTION="get" and ACTION="fields".
--->


<!--- ACTION is required --->
<cfif NOT IsDefined("ATTRIBUTES.action")>
	<cfthrow message="ACTION is required">
</cfif>

<!--- Which ACTION? --->
<cfswitch expression="#Trim(ATTRIBUTES.action)#">

<!--- Process SET --->
<cfcase value="set">

	<!--- Check that there is an end tag --->
	<cfif NOT ThisTag.HasEndTag>
		<cfthrow message="No end tag found">
	</cfif>
	<!--- Check for required attributes --->
	<cfif NOT IsDefined("ATTRIBUTES.form")>
		<cfthrow message="FORM is required">
	</cfif>
	<cfif NOT IsDefined("ATTRIBUTES.destination")>
		<cfthrow message="DESTINATION is required">
	</cfif>
	<!--- Check attributes are valid --->
	<cfif NOT FileExists(ATTRIBUTES.form)>
		<cfthrow message="File #ATTRIBUTES.form# not found">
	</cfif>
	<cfif Trim(ATTRIBUTES.destination) EQ "">
		<cfthrow message="DESTINATION must be a valid fully qualified file name">
	</cfif>
	<cfif Trim(ATTRIBUTES.form) EQ Trim(ATTRIBUTES.destination)>
		<cfthrow message="FORM and DESTINATION must be different">
	</cfif>

	<!--- Only process in END mode --->
	<cfif ThisTag.HasEndTag AND ThisTag.ExecutionMode IS "end">
	
		<!--- Make sure we have form fields to process --->
		<cfif NOT IsDefined("ThisTag.AssocAttribs")>
			<cfthrow message="No form fields and values specified">
		</cfif>

		<!--- Build XML based on passed child parameters --->
		<cfif GetFormType(ATTRIBUTES.form) IS 1>
			<cfset PDFData=CreateXFA(ThisTag.AssocAttribs)>
		<cfelse>
			<cfset PDFData=CreateXFDF(ThisTag.AssocAttribs)>
		</cfif>

		<!--- Data is ready, do it --->
		<cfset SetPDFFormFields(ATTRIBUTES.form,
								ATTRIBUTES.destination,
								PDFData)>
	</cfif>

</cfcase>
<!--- End of process SET --->

<!--- Process GET --->
<cfcase value="get">

	<!--- No end mode for GET --->
	<cfif NOT ThisTag.HasEndTag OR ThisTag.ExecutionMode IS "end">

		<!--- Check for required attributes --->
		<cfif NOT IsDefined("ATTRIBUTES.form")>
			<cfthrow message="FORM is required">
		</cfif>
		<cfif NOT IsDefined("ATTRIBUTES.result")>
			<cfthrow message="RESULT is required">
		</cfif>
		<!--- Check attributes are valid --->
		<cfif NOT FileExists(ATTRIBUTES.form)>
			<cfthrow message="File #ATTRIBUTES.form# not found">
		</cfif>
		<cfif NOT IsValid("variableName", ATTRIBUTES.result)>
			<cfthrow message="#ATTRIBUTES.result# is not a valid variable name">
		</cfif>
		
		<!--- Do it --->
		<cfset XMLFields=GetPDFFormFields(ATTRIBUTES.form)>

		<!--- Now convert XML data and save to result --->
		<cfif GetFormType(ATTRIBUTES.form) IS 1>
			<cfset CALLER[ATTRIBUTES.result]=ExtractXFA(XMLFields)>
		<cfelse>
			<cfset CALLER[ATTRIBUTES.result]=ExtractXFDF(XMLFields)>
		</cfif>

	</cfif>

</cfcase>
<!--- End of process GET --->

<!--- Process FIELDS --->
<cfcase value="fields">

	<!--- No end mode for GET --->
	<cfif NOT ThisTag.HasEndTag OR ThisTag.ExecutionMode IS "end">

		<!--- Check for required attributes --->
		<cfif NOT IsDefined("ATTRIBUTES.form")>
			<cfthrow message="FORM is required">
		</cfif>
		<cfif NOT IsDefined("ATTRIBUTES.result")>
			<cfthrow message="RESULT is required">
		</cfif>
		<!--- Check attributes are valid --->
		<cfif NOT FileExists(ATTRIBUTES.form)>
			<cfthrow message="File #ATTRIBUTES.form# not found">
		</cfif>
		<cfif NOT IsValid("variableName", ATTRIBUTES.result)>
			<cfthrow message="#ATTRIBUTES.result# is not a valid variable name">
		</cfif>
		
		<!--- Do it --->
		<cfset XMLFields=GetPDFFormFields(ATTRIBUTES.form)>

		<!--- Now convert XML data and save to result --->
		<cfif GetFormType(ATTRIBUTES.form) IS 1>
			<cfset CALLER[ATTRIBUTES.result]=GetXFAFields(XMLFields)>
		<cfelse>
			<cfset CALLER[ATTRIBUTES.result]=GetXFDFFields(XMLFields)>
		</cfif>

	</cfif>

</cfcase>
<!--- End of process FIELDS --->

<!--- Any other ACTION is invalid --->
<cfdefaultcase>
	<cfthrow message="Valid ACTION values are FIELDS, GET, SET">
</cfdefaultcase>

</cfswitch>

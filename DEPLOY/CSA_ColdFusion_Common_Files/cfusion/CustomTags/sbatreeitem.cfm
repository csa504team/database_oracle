<!---
AUTHOR:				Steve Seaquist
DATE:				11/04/2004
DESCRIPTION:		Dynamic HTML substitute for CFTREEITEM. Subtag of cf_sbatree, where the real work is done. 
NOTES:

	Attributes.img, although certainly allowed, is no longer used. If other nodes have identified the current node as its 
	parent, it's a Folder. Otherwise, it's a Document. No need to say so anymore. On the other hand, it DOES make your 
	code easier to read (self-documenting), and maintains compatibility with the standard ColdFusion CFTREEITEM tag. 

	In most cases, you won't have to code Attributes.onclick unless you want to override the behavior of the generated 
	onclick. If you do that, your Attributes.onclick JavaScript completely replaces the generated onclick code. 

	Valid values for Attributes.target: 
		"AppData"	Loads Attributes.href into the AppData frame. (Use this feature ONLY when AppData is a frame.) 
		"_blank"	Opens a new window. The icon also changes to a shortcut icon (indicating an Internet short cut) 
					and the tooltip will have " (opens a new window)" appended to it. 
		"_top"		Jumps out of frames. (Typically used when AppNav is a frame. Pretty meaningless otherwise.) 
		not given	Don't generate a target attribute on the tree's hotlinks. 9 times out of 10, that's what you want, 
					especially if you're using the JavaScript way of handling tree clicks. 

	Folders clearly HAVE to have Attributes.value, or else you wouldn't have any way to match up the folder's contents 
	by giving it in Attributes.parent. But Documents no longer have to have Attributes.value if all you want is a plain 
	hotlink using the Attributes.target and Attributes.href attributes. If there isn't any Attributes.value or 
	Attributes.onclick, no onclick will be generated. That means that your hotlink just jumps immediately, without 
	having to go through the cf_sbatree function="..." function. 

INPUTS:				Mandatory:	Attributes.display, Attributes.img ("Document" or "Folder") and Attributes.value. 
					Optional:	Attributes.expand (ignored) and Attributes.parent. 
OUTPUT:				Base tag cf_sbatree will display a DHTML equivalent of CFTREE. 
REVISION HISTORY:	11/19/2010, SRS:	Added new attributes disabled and highlighted, both defaulting to "No". These 
										can be used to identify the page you're currently on, by setting both to "Yes". 
										But generally, it's easier to use sbatree's new attribute currentpagevalue, 
										which also appends " (current page)" to the title/tooltip. 
					10/18-21/2010, SRS:	Added new attributes href, onclick and target as part of the sbatree rewrite for 
										Section 508 compliance. 
					05/03/2007, SRS:	Added new attribute value cf_sbatree ExpandAll="Auto" for upward compatability. 
										Got cf_sbatree ExpandAll="Auto" to honor cf_sbatreeitem Expand="Yes". (For years now, 
										it has simply been ignored.) This implied the need to default it to "No" and 
										removing the comment that it was not currently in use. 
					11/04/2004, SRS:	Original implementation. 
--->

<cfparam name="Attributes.display">					<!--- Mandatory. --->
<cfparam name="Attributes.displaytoggles"	default=""><!--- Affects only folders. Default set in sbatree. --->
<cfparam name="Attributes.disabled"			default="No"><!--- Affects only documents (clicking does nothing). --->
<cfparam name="Attributes.expand"			default="No"><!--- Affects only folders. --->
<cfparam name="Attributes.highlighted"		default="No"><!--- Affects only documents (pre-highlights). --->
<cfparam name="Attributes.href"				default="">
<cfparam name="Attributes.img"				default=""><!--- Ignored. Inferred from parent/child relationships now. --->
<cfparam name="Attributes.onclick"			default="">
<cfparam name="Attributes.parent"			default=""><!--- Will be root level if parent is "". --->
<cfparam name="Attributes.target"			default=""><!--- "_top"  --->
<cfparam name="Attributes.value"			default=""><!--- Documents no longer require a value if you're using href. --->
<cfassociate basetag="cf_sbatree">

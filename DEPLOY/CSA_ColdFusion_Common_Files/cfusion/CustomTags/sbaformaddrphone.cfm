<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, for the US Small Business Administration. 
DATE:				04/17/2014. 
DESCRIPTION:		Custom tag to present address and phone number in a multiline format, some rows containing multiple 
					form elements horizontally. Gets its data from nested cf_sbaformelt calls. 
NOTES:

	WHEN FULLY WRITTEN, THE PROTOTYPE FOR THIS PARENT TAG WILL BE /library/cfincludes/userprofile/dsp_formdata_contact.cfm. '
	That's what appears in the GLS Profile, our model for all other addresses/phones that could be foreign or domestic. 

	Although the 14 form elements of this custom tag strongly interact with each other, there's no implication that you 
	should segregate them into their own fieldset or anything like that. It simply generates multiple adjacent rows. 
	How you choose to present those rows compared to other rows is totally dependent on the needs of the calling page. 

	There could be, and probably will be, cf_sbaformaddr and cf_sbaformphone. When and if that happens, the difference 
	will be the absence of strong interaction between ColAddrCountry and ColPhoneCountry, with associated shows and hides. 
	If you want the strong interaction, use this parent tag. If you don't, if you want them to behave independently of 
	one another, use the cf_sbaformaddr and cf_sbaformphone custom tags when they become available. 

	Also, since a subtag not given is not rendered on the page, it's possible to use this same tag twice, once with only 
	addr subtags (functioning in effect like a cf_sbaformaddr) and again with only phone subtags (like a cf_sbaformphone). 
	That may in fact be a preferrable alternative to creating cf_sbaformaddr and cf_sbaformphone. Learn it once, use it a 
	lot, single point of maintenance for all addr and phone rows. (There may not ever be cf_sbaformaddr and cf_sbaformphone.) 

INPUT:				See comment header at the top of cf_sbaformelt for explanations of Attributes and Request.Sfe variables. 
INPUT:				thisTag.AssocAttribs.GeneratedData. 
OUTPUT:				HTML. 
REVISION HISTORY:	04/17/2014, SRS:	Original implementation. Cannibalized cf_sbaformperson, which is working. If you 
										want to see a parent tag in action, go to /library/experiments/testerfiles
										/sbaformelt/dsp_SBAFormEltCustomTag.cfm, cf_sbaformperson label="Point of Contact". 
--->

<!--- Configuration Parameters: --->

<cfset Variables.CrashMessage						= Request.SlafEOL
													& "INTERNAL ERROR: Please report the following error to the "
													& "SBA's Office of the Chief Information Officer. "
													& Request.SlafEOL
													& "The following information may help: ">

<!--- Custom tag control: --->

<cfswitch expression="#thisTag.ExecutionMode#">
<cfcase value="start">
	<!--- Only attributes to cf_sbaformparent: --->
	<cfparam name="Attributes.ColAddrCountry"		default="">
	<cfparam name="Attributes.ColAddrZip5"			default="">
	<cfparam name="Attributes.ColAddrZip4"			default="">
	<cfparam name="Attributes.ColAddrLookup"		default="">
	<cfparam name="Attributes.ColAddrDropdown"		default="">
	<cfparam name="Attributes.ColAddrPostal"		default="">
	<cfparam name="Attributes.ColAddrStateName"		default="">
	<cfparam name="Attributes.ColAddrStreet1"		default="">
	<cfparam name="Attributes.ColAddrStreet2"		default="">
	<cfparam name="Attributes.ColAddrStateCode"		default="">
	<cfparam name="Attributes.ColPhoneCountry"		default="">
	<cfparam name="Attributes.ColPhoneArea"			default="">
	<cfparam name="Attributes.ColPhoneBody"			default="">
	<cfparam name="Attributes.ColPhoneExtension"	default="">
	<cfset Request.SfeParent						= "cf_sbaformaddrphone">
	<cfparam name="Attributes.HiGrp"				default="">
	<cfparam name="Attributes.Indent"				default="">
	<cfparam name="Attributes.IndentChars"			default="">
	<cfparam name="Attributes.Label"				default=""><!--- MANDATORY --->
	<cfparam name="Attributes.MandOpt"				default="">					<!--- Defaulted to "opt", below. --->
	<cfparam name="Request.SfeRowClass"				default="">
	<cfparam name="Attributes.RowClass"				default="">
	<cfif Len(Attributes.Label) is 0>
		<cfoutput>#Variables.CrashMessage#
&lt;cf_sbaformparent&gt; was called without the "label" attribute.</cfoutput>
		<cfabort>
	</cfif>
</cfcase>
<cfcase value="end">
	<cfset Request.SfeParent						= "">
	<!--- From here on out, thisTag.ExecutionMode "end" is assumed. So decrease indention: --->

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

<!--- Initialize Layout Variables: --->

<cfinclude template="/library/cfincludes/sbalookandfeel/bld_SBAFormEltLayoutVars.cfm">

<cfset Variables.DefaultColNames					=	(Len(Attributes.ColAddrCountry)		is 0)
													and	(Len(Attributes.ColAddrZip5)		is 0)
													and	(Len(Attributes.ColAddrZip4)		is 0)
													and	(Len(Attributes.ColAddrLookup)		is 0)
													and	(Len(Attributes.ColAddrDropdown)	is 0)
													and	(Len(Attributes.ColAddrPostal)		is 0)
													and	(Len(Attributes.ColAddrStateName)	is 0)
													and	(Len(Attributes.ColAddrStreet1)		is 0)
													and	(Len(Attributes.ColAddrStreet2)		is 0)
													and	(Len(Attributes.ColAddrStateCode)	is 0)
													and	(Len(Attributes.ColPhoneCountry)	is 0)
													and	(Len(Attributes.ColPhoneArea)		is 0)
													and	(Len(Attributes.ColPhoneBody)		is 0)
													and	(Len(Attributes.ColPhoneExtension)	is 0)
													and	(IsDefined("thisTag.AssocAttribs"))
													and	(ArrayLen(thisTag.AssocAttribs)		is 14)>

<!--- THIS IS AS FAR AS I GOT ON IMPLEMENTING THIS PARENT TAG. SRS, 04/17/2014. --->
<!--- THIS IS AS FAR AS I GOT ON IMPLEMENTING THIS PARENT TAG. SRS, 04/17/2014. --->
<!--- THIS IS AS FAR AS I GOT ON IMPLEMENTING THIS PARENT TAG. SRS, 04/17/2014. --->
<!--- THIS IS AS FAR AS I GOT ON IMPLEMENTING THIS PARENT TAG. SRS, 04/17/2014. --->
<!--- THIS IS AS FAR AS I GOT ON IMPLEMENTING THIS PARENT TAG. SRS, 04/17/2014. --->

<!--- BUILD UP THE REST FROM /library/cfincludes/userprofile/dsp_formdata_contact.cfm!!! --->
<!--- BUILD UP THE REST FROM /library/cfincludes/userprofile/dsp_formdata_contact.cfm!!! --->
<!--- BUILD UP THE REST FROM /library/cfincludes/userprofile/dsp_formdata_contact.cfm!!! --->
<!--- BUILD UP THE REST FROM /library/cfincludes/userprofile/dsp_formdata_contact.cfm!!! --->
<!--- BUILD UP THE REST FROM /library/cfincludes/userprofile/dsp_formdata_contact.cfm!!! --->

<cfset Variables.RowClass							= ListAppend(Attributes.RowClass, Request.SfeRowClass, " ")>
<cfoutput>#Request.SlafEOL#
#Variables.IndentPlus0#<div class="tbl_row<cfif Len(Variables.RowClass) GT 0> #Variables.RowClass#</cfif>">
#Variables.IndentPlus1#<div class="tbl_cell formlabel">
#Variables.IndentPlus2#<span class="#Attributes.MandOpt#label#HiGrpLab#">#Attributes.Label#</span>
#Variables.IndentPlus1#</div>
#Variables.IndentPlus1#<div class="tbl_cell formdata">
#Variables.IndentPlus2#<div class="tbl"><div class="tbl_row compound_row"><!-- Side-by-Side --></cfoutput>

<cfset Variables.SubtagCtr							= 0>
<cfset Variables.SubtagIdx							= 0>
<cfloop index="EltIdx" list="ColFirst,ColMI,ColLast,ColSuffix">
	<cfif NOT IsDefined("thisTag.AssocAttribs")>
		<cfcontinue>
	<cfelseif Variables.DefaultColNames>
		<cfset Variables.SubtagCtr					= Variables.SubtagCtr + 1>
		<cfset Variables.SubtagIdx					= Variables.SubtagIdx + 1>
		<cfset Variables.AttribStruct				= thisTag.AssocAttribs[Variables.SubtagIdx]>
		<cfset Variables.GennedStruct				= Variables.AttribStruct.GeneratedData>
	<cfelse>
		<cfset Variables.EltFound					= "No">
		<cfloop index="Variables.SubtagIdx" from="1" to="#ArrayLen(thisTag.AssocAttribs)#">
			<cfset Variables.AttribStruct			= thisTag.AssocAttribs[Variables.SubtagIdx]>
			<cfset Variables.GennedStruct			= Variables.AttribStruct.GeneratedData>
			<cfif Variables.AttribStruct.Col		is Attributes[EltIdx]>
				<cfset Variables.EltFound			= "Yes">
				<cfset Variables.SubtagCtr			= Variables.SubtagCtr + 1>
				<cfbreak>
			</cfif>
		</cfloop>
		<cfif NOT Variables.EltFound>
			<cfcontinue>
		</cfif>
	</cfif>
	<cfswitch expression="#EltIdx#">
	<cfcase value="ColFirst">						<cfset Variables.SubtagLabel = "first"></cfcase>
	<cfcase value="ColMI">							<cfset Variables.SubtagLabel = "m.i."></cfcase>
	<cfcase value="ColLast">						<cfset Variables.SubtagLabel = "last"></cfcase>
	<cfcase value="ColSuffix">						<cfset Variables.SubtagLabel = "suffix"></cfcase>
	</cfswitch>
	<cfoutput>#Request.SlafEOL#
#Variables.IndentPlus3#<div class="tbl_cell"><div class="tbl"><!-- Vertical -->
#Variables.IndentPlus4#<div class="tbl_row"><div class="tbl_cell formdatadata #AttribStruct.MandOpt#data#HiGrpBox#"><!-- First row -->
#Variables.IndentPlus4##Replace(Variables.GennedStruct.Tag, Request.SlafEOL, Request.SlafEOL & Variables.IndentPlus4, "ALL")#
#Variables.IndentPlus4#</div></div><!-- /First row -->
#Variables.IndentPlus4#<div class="tbl_row"><div class="tbl_cell formdatalabel"><!-- Second row --><cfif AttribStruct.Type is "lookup">#Request.SlafEOL#
#Variables.IndentPlus4#<span id="#AttribStruct.name#_msg" style="font-size:6pt;"></span><select id="#SubtagStruct.name#_menu" class="hide"></select><cfelse>#Request.SlafEOL#
#Variables.IndentPlus4#<label for="#AttribStruct.Id#" class="#AttribStruct.MandOpt#label#Variables.HiGrpLab#">#Variables.SubtagLabel#</label></cfif>#Request.SlafEOL#
#Variables.IndentPlus4#</div></div><!-- /Second row -->
#Variables.IndentPlus3#</div></div><!-- /Vertical --></cfoutput>
	</cfloop>

<cfif Variables.SubtagCtr is 0>
	<cfoutput>#Request.SlafEOL#
#Variables.IndentPlus3#<div class="tbl_cell formdata">
#Variables.IndentPlus4#<div class="optdata">
#Variables.IndentPlus4#(Error. cf_sbaformperson had no recognized subtags.)
#Variables.IndentPlus4#</div><!-- optdata -->
#Variables.IndentPlus3#</div><!-- formdata --></cfoutput>
</cfif>

	<cfoutput>#Request.SlafEOL#
#Variables.IndentPlus2#</div></div><!-- /Side-by-Side -->
#Variables.IndentPlus1#</div><!-- /formdata -->
#Variables.IndentPlus1#<div class="tbl_cell formerr"></div>#Request.SlafEOL#
#Variables.IndentPlus0#</div><!-- /tbl_row -->
</cfoutput>

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

	<!---        /thisTag.ExecutionMode "end" having decreased indention --->
</cfcase><!---   /thisTag.ExecutionMode "end" --->
</cfswitch><!--- /thisTag.ExecutionMode --->

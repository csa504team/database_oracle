<!---
AUTHOR:				Steve Seaquist. 
DATE:				09/23/2004, 
DESCRIPTION:		Custom tag to present SBA standard look-and-feel main navigation buttons. 

NOTES:
	Although the standard button set's names are predefined, there is nothing to prevent an application from defining more, 
	with arbitrary names, provided that the graphics exist in /images/button_name_b.gif and /images/button_name_w.gif, and 
	a JavaScript with a DoButton_Name function exists in /#RootURL#/javascripts/DoButton_Name.js. The Action page must also 
	handle all allowed button_name passoffs, based on Form.SubmitButton, which will contain button_name, in case the user 
	has JavaScript turned off. 

	If you pass AppData, AppNav and/or MainNav URLs, then you must have a <body onLoad> script that sets the associated 
	gFrameIsFullyLoaded variable and calls SetReadyIfAllFullyLoaded. For example, if you specified AppData="dsp_routing.cfm", 
	then dsp_routing.cfm (and all other dsp pages it may pass off to in the AppData frame) must do this onLoad: 

					top.gFrameIsFullyLoadedAppData	= true;
					top.SetReadyIfAllFullyLoaded();

	Don't do this for AppHidden or AppInfo frames. The gFrameIsFullyLoaded variables won't even exist. 

INPUT (Attributes scope):
	ActionURL		Button-handler URL, relative to the calling page (example: Action="act_mainnav.cfm"). 
					See Notes, above. Passed through to cf_mainnav if MainNav attribute not given. 
	Configs			If given, it will be cfincluded after configuration parameters are defined allowing you to do overrides. 
	Debug			"Yes" or "No" (default). Obvious. 
	SlafHTML5		Obsolete. Used to distingush quirks mode and strict mode. Now always "Yes" (strict mode). 
	InFrame			"Yes" or "No". If "Yes", generates html, head, javascripts, body, etc. 
					Generally InFrame will only be "No" if cf_mainnav is being called by cf_sbalookandfeel. 
	LibURL			If not given, "/library" is assumed. 
	MainNavHiddens	If given, HTML to be included immediately after <form name="FormMainNav". Usually contains hidden fields 
					to be passed to ActionURL. 
	MainNavJSURL	If given, URL of JavaScripts subdirectory to support each button in MainNav area/frame. 
					Example, MainNavJSURL="/elend/javascripts/sbalookandfeel": DoSubmit.js, etc, are in that directory. 
	MainNavMenuURL	If given, URL of cfinclude that defines Request.SlafShowButtonsInMainNavMenu, a structure-of-structures 
					that maps Show to the MainNavMenu. The cfinclude file is allowed not to exist, so that switching over to 
					menus can be externally configurable. First example is /security/dsp_mainnav_steve.MainNavMenu.cfm. 
	ReadyLight		"Yes" or "No". If "Yes" (default), generates loading/ready indicator. 
	Show			Which buttons to show in the MainNav area (for example: Show="Admin,Help,Exit"). 
	TextOnly		Obsolete. Nowadays this is supposed to be handled using "theming" (specifically, theme 6). 
INPUT (Request scope):
	SlafHTML5							Obsolete. Now always "Yes" (strict mode). 
	SlafShowButtonOverrides				Array of show structures that affect button generation. Mainly used by "external system" 
										buttons in loan-associated systems. See /library/cfincludes/bld_useraccesslist.cfm, 
										then /library/cfincludes/bld_buttonslist.cfm, then this custom tag's dsp_DefineButtons. 
		Allowed keys of each show structure: 
			external					Display silver button if button. Display in external systems menu if menu. 
			href						Standard meaning for HTML links. 
			show						"Packed" format of Show name. Example: "PostServicing" is packed "Post Servicing". 
			target						Standard meaning for HTML links. Examples: _blank, _top, AppData, etc. 
			title						Tooltip. 
	SlafShowButtonsInMainNavMenu		Structure of show structures that affect MainNavMenu generation. 
		Allowed keys of each show structure: 
			callback					URL to use with PageSegmentsGrabber to build the menu. 
			function					Function to call instead of the default "Do#show#(document.FormMainNav);". 
			line						"Before/after/both", whether and where to inject <hr/>. 
			menu						Name of the control that appears in the MainNavMenu bar. "Name of the menu". 
			show						"Packed" format of Show name. Example: "PostServicing" for Show name "Post Servicing". 
	((SlafShow interactions))
		If SlafShowButtonsInMainNavMenu exists, MainNavMenuURL will NOT be cfincluded to define it. (No need to.) 
		Both SlafShowButtonOverrides and SlafShowButtonsInMainNavMenu may be used in the same call, but some Overrides 
		keys won't be used if you're generating a menu, obviously. 
	SlafTextOnly						Obsolete. Now always "" (handled by theme 6). 
OUTPUT:				Web page with SBA standard look-and-feel main navigation buttons. 
REVISION HISTORY:	12/28/2015, SRS:	Added the ability to display buttons as menu items, which required a new configuration 
										structure, preprocessing, etc. Added Attributes.MainNavMenuURL to allow different 
										callers to build the new configuration structure differently (external control). 
										(See also last sentence of 10/06/2015 revision.) 
					10/06/2015, SRS:	Reworked lots of stuff to be "themeable" and simpler because we no longer have 
										to support MSIE 8: Got rid of files for quirks mode, which we haven't generated 
										in over 4 years, to relieve that maintenance burden. 
					03/04/2015, SRS:	As part of making the color scheme considerably lighter, ditched bluesteel.
					09/04/2012, SRS:	Set default for SlafHTML5 to "Yes" to turn on Strict Mode, even in production.
					08/10/2011 -
					06/28/2012, SRS:	In preparation for MSIE 10, which will TOTALLY mess us up with regard to QuirksMode,
										started getting really serious about HTML5 (strict mode) and fixes for strict mode
										related problems. Stratified out gradients (supported in IE 10) from display tables
										(supported in IE 8), so using "lte IE 9" on IE-only mainnav css files.
					07/20/2011, SRS:	Minor change to support "external system" buttons in main navigation.
					11/09/2010, SRS:	Now that CSS buttons are proven to work, made code more efficient by moving hover
										JavaScript to mainnav.js.
					07/16-20/2010, SRS:	"New Look-and-Feel". Took a checkpoint at 07/20/2010.
					03/22/2010, SRS:	Got rid of HTML tables. Section 508 related.
					07/27/2006, SRS:	Numerous minor changes to adapt to a Microsoft Internet Explorer 6.x bug that started
										to appear when AppInfoExists and InFrame is "No". Ultimately didn't solve the
										problem, but at least got rid of vertical scroll bars (for BotMost) in MSIE.
					08/16/2005, SRS:	Because we no longer have to support Netscape 4, we now have the innerHTML property
										available on all browsers, with which we can make the ReadyLight text-only. Also
										added the Width attribute to deal with anomalies in TextOnly mode.
					07/22/2005, SRS:	Because some users have started coding </cfmodule> and/or </cf_mainnav>, we were
										getting double pages. Added a test to ignore end tag to prevent this.
					05/24/2005, SRS:	Allowed for TextOnly attribute, which will be used in cfincluded files.
					01/21/2005,    :	Changed ImageWidth configuration parameter to 75 for new images.
					09/23/2004, SRS:	Original implementation.
--->

<!--- If user codes </cfmodule> or </cf_mainnav>, ignore it: --->

<cfif thisTag.ExecutionMode IS "end">
	<cfexit>
</cfif>

<!--- Attributes: --->

<cfparam name="Attributes.LibURL"							default="/library"><!--- Has to be done out of alphabetical order. --->

<cfif NOT IsDefined("Request.SlafEOL")>
	<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbashared_variables.cfm">
</cfif>

<cfparam name="Attributes.ActionURL"						default="">
<cfparam name="Attributes.Configs"							default="">
<cfparam name="Attributes.Debug"							default="No">
<cfset Attributes.HTML5										= "Yes"><!--- We no longer support quirks mode. Prevent accidents. --->
<cfset Request.SlafHTML5									= "Yes"><!--- We no longer support quirks mode. Prevent accidents. --->
<cfparam name="Attributes.InFrame"							default="Yes">
<cfparam name="Attributes.JSInline"							default="">
<cfparam name="Attributes.MainNavHiddens"					default="">
<cfparam name="Attributes.MainNavJSURL"						default=".">
<cfparam name="Attributes.MainNavMenuURL"					default="">
<cfparam name="Attributes.ReadyLight"						default="Yes">
<cfparam name="Attributes.Show"								default="">
<cfset Attributes.TextOnly									= ""><!--- Ignore what's on the cf_mainnav call. --->

<!--- Configuration Parameters: --->

<cfset Variables.SelfName									= "sbalookandfeel">
<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_ConfigStdButtons.cfm">
<cftry>
	<cfif	(NOT IsDefined("Request.SlafShowButtonsInMainNavMenu"))
		and	(Len(Attributes.MainNavMenuURL)					gt 0)
		and	FileExists(ExpandPath(Attributes.MainNavMenuURL))><!--- Makes MainNavMenus externally configurable. --->
		<cfinclude template="#Attributes.MainNavMenuURL#">
	</cfif>
	<cfcatch type="Any">
		<!--- If an error occurs, ignore it. It's okay for the Attributes.MainNavMenuURL file not to exist. --->
	</cfcatch>
</cftry><!--- After the cfif, if SlafShowButtonsInMainNavMenu is still not defined, menuing is turned off: --->
<cfset Variables.DoingMainNavMenus							= IsDefined("Request.SlafShowButtonsInMainNavMenu")>
<cfif  Variables.DoingMainNavMenus>
	<cfif NOT IsDefined("ShowButtonIsInMainNavMenu")>
		<cfinclude template="/library/udf/bld_SlafUDFs.cfm"><!--- Defines ShowButtonHasOverrides(), and ShowButtonInMainNavMenu(). --->
	</cfif>
	<cfloop index="Variables.Button" list="#Attributes.Show#">
		<cfset ShowButtonIsInMainNavMenu(Variables.Button, "Yes")><!--- "Yes" = "building MainNavMenuNames array". --->
	</cfloop>
</cfif>

<!--- Other attribute initializations: --->

<cfif Len(Attributes.ActionURL) is 0>
	<cfset Attributes.ActionURL								= "javascript:void();">
</cfif>
<cfif Attributes.Debug>
	<cfset Variables.DebugString							= "cf_mainnav called with ">
	<cfloop index="Att" list="#StructKeyList(Attributes)#">
		<cfset Variables.DebugString						= ListAppend(Variables.DebugString, "#Att#=#Evaluate('Attributes.#Att#')#")>
	</cfloop>
	<cfset Variables.DebugString							= Replace(Variables.DebugString, ",", ", ", "ALL") & ". ">
</cfif><!--- Add other junk to the end of DebugString as needed. --->

<!--- Overrides of Configuration Parameters: --->

<cfif Len(Attributes.Configs) gt 0>
	<cfinclude template="#Attributes.Configs#">
</cfif>
<cfif Attributes.InFrame>
	<cfset indent											= Chr(9)&Chr(9)>
<cfelse>
	<cfset indent											= Chr(9)&Chr(9)&Chr(9)>
</cfif>
<cfset Variables.ListShowBtnsFull							= Attributes.Show>
<cfset Variables.ListShowBtnsPacked							= Replace(Replace(Variables.ListShowBtnsFull," ","","ALL"),"-","","ALL")>

<!--- Prepare to build output Web page: --->

<cfif Attributes.InFrame>
	<cfif NOT IsDefined("Request.SlafTopOfHead")>
		<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbalookandfeel_topofhead.cfm">
	</cfif>
	<cfif Attributes.LibURL is not "/library">
		<!--- Cachebusters meaningless, because different Attributes.LibURL probably won't be in MSIE cache anyway. --->
		<cfsavecontent variable="Request.SlafTopOfHeadCSSMainNavStrict">
			<cfoutput>#Request.SlafEOL#
<link  href="#Attributes.LibURL#/css/mainnav.strict.css?CachedAsOf=2015-11-09T16:18"                           rel="stylesheet" type="text/css" media="all"/></cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="Request.SlafTopOfHeadMainNav">
			<cfoutput>#Request.SlafEOL#
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/mainnav.js?CachedAsOf=2010-11-09T17:12"></script></cfoutput>
		</cfsavecontent>
	<cfelse>
		<!--- Use same-name Request.Slaf variables from get_sbalookandfeel_topofhead. --->
	</cfif>
	<cfoutput>#Request.SlafHead#
<title>Main Navigation Frame</title>
#Request.SlafTopOfHead#
<!-- Begin MainNav1 -->
#Request.SlafTopOfHeadCSSMainNavStrict#
<!--[if lte IE 9]><cfif Request.SlafButtons3D>
<link  href="#Attributes.LibURL#/css/mainnav.msie.pie3d.css?CachedAsOf=2012-06-29T13:24"             rel="stylesheet" type="text/css" media="all"/><cfelse>
<link  href="#Attributes.LibURL#/css/mainnav.msie.plain.css?CachedAsOf=2011-08-09T16:33"             rel="stylesheet" type="text/css" media="all"/></cfif>
<![endif]-->
#Request.SlafTopOfHeadMainNav#
<!-- End MainNav1 -->

#Request.SlafTopOfHeadjQuery#
<script>

// The following is provided only for backward compatability: 
var gThisPageIsFullyLoaded									= false;

</script>
<!-- Begin MainNav2 -->
</cfoutput>
	<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_DefineJavaScriptsForButtons.cfm">
	<cfif Len(Attributes.JSInline) GT 0>
		<cfoutput>
#Attributes.JSInline#</cfoutput>
	</cfif>
	<cfoutput>
<!-- End MainNav2 -->
</head>
<body link="Blue" vlink="Maroon" alink="Red" topmargin="0" onLoad="MainNavDoThisOnLoad();">
<!-- Begin MainNav3 -->
<div class="headernav" style="height:77px; width:100%; position:relative; top:0px; left:0px;"></cfoutput>
	<cfif Variables.DoingMainNavMenus>
		<!--- Add class="contains_menus" to get overflow:visible, because menus must pop up over DivAppData. --->
		<cfoutput>
	<div id="DivMainNavButtons" class="contains_menus"></cfoutput>
	<cfelse>
		<!--- Since there are only buttons, go with the default (overflow:hidden). --->
		<cfoutput>
	<div id="DivMainNavButtons"></cfoutput>
	</cfif>
</cfif>

<cfif Attributes.ReadyLight>
	<cfoutput>#Request.SlafEOL#
#indent#<div id="DivReadyLight" class="ReadyLightLoading">
#indent#	<span title="Please wait. Parts of this page are still loading.">Loading</span>
#indent#</div></cfoutput>
</cfif>
<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_DefineButtons.cfm">
<cfif Attributes.InFrame>
	<cfoutput>#Request.SlafEOL#
	</div><!-- /DivMainNavButtons -->
</div>
<!-- End MainNav3 -->
</body>
</html>
</cfoutput>
</cfif>

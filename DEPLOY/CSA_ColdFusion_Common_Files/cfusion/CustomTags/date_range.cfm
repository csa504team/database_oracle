<!--- 
Author:			Daniel Lee
Date:			5/4/2000
Purpose:		This custom tag find the begin and end date of the specify periods.
Input Variables:	cf_type (daily/weekly/monthly/yearly)
					cf_period
Ouput Variables:	cf_begin_date (a list of begin date)
					cf_end_date	(a list of end date)
					cf_title
					cf_labels
Usage:				<cf_date_ranage cf_type="weekly" cf_period="5"
Notes:				I wrote this tag because I have to pass the date list to
					a SQL statement to get the total records created on the certain
					period and then pass the label list to the quickbar.cfm
					to create a bar chart.
					
Modification Log:
Date		Author			Modification
-----------	--------------- ---------------------------

--->

<CFIF ThisTag.ExecutionMode IS "start">
	<CFPARAM NAME="Attributes.cf_type" DEFAULT="weekly">
	<CFSET Attributes.cf_period = Attributes.cf_period - 1>
	
	<!--- get the date period --->
	<CFIF Attributes.cf_type IS "daily">
	
		<!--- set date/time object of the first day of the period --->
		<CFSET caller.cf_begin_date = CreateDate(Year(Now()), Month(Now()), Day(Now()))>
		<CFSET caller.cf_end_date	= CreateDateTime(Year(Now()), Month(Now()), Day(Now()), 23, 59, 59)>
		
		<!--- set up the title and labels --->
		<CFSET caller.cf_title = "Daily Reports">
		<CFSET caller.cf_labels = DateFormat(caller.cf_begin_date, "MM/DD/YYYY")>
						   
		<!--- get the begin and end date of the report period --->
		<CFLOOP INDEX="x" FROM="1" TO="#Attributes.cf_period#">
			<CFSET result1 = DateAdd('d', -x, caller.cf_begin_date)>
			<CFSET result2 = DateAdd('d', -x, caller.cf_end_date)>
										
			<CFSET caller.cf_begin_date	= ListAppend(caller.cf_begin_date, result1)>
			<CFSET caller.cf_end_date		= ListAppend(caller.cf_end_date, 	result2)>
			<CFSET caller.cf_labels 		= ListAppend(caller.cf_labels, DateFormat(result1, "MM/DD/YYYY"))>
		</CFLOOP>
	
	<CFELSEIF Attributes.cf_type IS "weekly">
		<!--- find the first date of the week --->
		<CFSET caller.cf_begin_date = DateAdd('d', -DayOfWeek(now()-1), now())>
		
		<!--- set the time of the first date to 12:00 am --->
		<CFSET caller.cf_begin_date = CreateDate(Year(caller.cf_begin_date), Month(caller.cf_begin_date), 
														Day(caller.cf_begin_date))>
		
		<!--- set up the title and labels --->
		<CFSET caller.cf_title = "Weekly Reports">
		<CFSET caller.cf_end_date = Now()>
		<CFSET caller.cf_labels = DateFormat(caller.cf_begin_date, "MM/DD/YYYY") & " - " &
									   DateFormat(caller.cf_end_date, "MM/DD/YYYY")>
						   
		<!--- get the first and last date of the report period --->
		<CFLOOP INDEX="x" FROM="1" TO="#Attributes.cf_period#">
			<CFSET result1 = DateAdd('d', - 7, ListLast(caller.cf_begin_date))>
			<CFSET result2 = DateAdd('s', -1, DateAdd('d', 7, result1))>
			
			<CFSET caller.cf_begin_date	= ListAppend(caller.cf_begin_date, result1)>
			<CFSET caller.cf_end_date	= ListAppend(caller.cf_end_date, 	result2)>
			<CFSET caller.cf_labels 	= ListAppend(caller.cf_labels, DateFormat(result1, "MM/DD/YYYY") & 
															" - " & DateFormat(result2, "MM/DD/YYYY"))>
		</CFLOOP>
	
	<CFELSEIF Attributes.cf_type IS "monthly">
	
		<!--- find the first date of this month --->
		<CFSET caller.cf_begin_date = #CreateDate(Year(Now()), Month(Now()), 1)#>
		<CFSET caller.cf_end_date	 = Now()>
		
		<!--- set up the title and labels --->
		<CFSET caller.cf_title = "Monthly Reports">
		<CFSET caller.cf_labels = DateFormat(caller.cf_begin_date, "MMM YYYY")>
		
		<!--- get the first and last date of the report period --->
		<CFLOOP INDEX="x" FROM="1" TO="#Attributes.cf_period#">
			<CFSET result1 = DateAdd('M', -1, ListLast(caller.cf_begin_date))>
			<CFSET result2 = DateAdd('S', -1, ListLast(caller.cf_begin_date))>
			
			<CFSET caller.cf_begin_date	= ListAppend(caller.cf_begin_date, 	result1)>
			<CFSET caller.cf_end_date	= ListAppend(caller.cf_end_date, 	result2)>
			<CFSET caller.cf_labels 	= ListAppend(caller.cf_labels, DateFormat(result1, "MMM YYYY"))>
		</CFLOOP>
	
	<CFELSEIF Attributes.cf_type IS "yearly">
		<CFSET caller.cf_begin_date = CreateDate(Year(Now()), 1, 1)>
		<CFSET caller.cf_end_date = Now()>
	
		<!--- set up the title and labels --->
		<CFSET caller.cf_title = "Yearly Reports">	
		<CFSET caller.cf_labels = DateFormat(caller.cf_begin_date, "YYYY")>
		
		<!--- get the first and last date of the report period --->
		<CFLOOP INDEX="x" FROM="1" TO="#Attributes.cf_period#">
			<CFSET result1 = DateAdd('YYYY', -1, ListLast(caller.cf_begin_date))>
			<CFSET result2 = DateAdd('S', -1, ListLast(caller.cf_begin_date))>
			
			<CFSET caller.cf_begin_date	= ListAppend(caller.cf_begin_date, result1)>
			<CFSET caller.cf_end_date	= ListAppend(caller.cf_end_date, 	result2)>
			<CFSET caller.cf_labels 	= ListAppend(caller.cf_labels, DateFormat(ListLast(caller.cf_begin_date), "YYYY"))>
		</CFLOOP>
	</CFIF>
</CFIF>

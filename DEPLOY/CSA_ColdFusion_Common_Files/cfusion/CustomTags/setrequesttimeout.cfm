<!---
AUTHOR:				Steve Seaquist
DATE:				01/18/2006
DESCRIPTION:		Sets RequestTimeout in the manner appropriate to the current CF version. Assumes CFMX or higher. 
					(Earlier versions couldn't set RequestTimeout except on the URL, so if the version is less than CFMX, 
					this custom tag does nothing.) 
NOTES:				Requires prior cfinclude of get_sbalookandfeel_variables or get_sbashared_variables. 
INPUT:				Attributes.Seconds. 
OUTPUT:				None. 
REVISION HISTORY:	01/18/2006, SRS:	Original implementation. 
--->

<cfparam name="Attributes.Seconds" type="numeric"><!--- Crashes if not given. --->
<cfset Variables.RoundedSeconds			= Round(Attributes.Seconds)><!--- Throw away fractional part, if given. --->

<cfif		Request.SlafVersionMajor IS 6><cfset Variables.RequestTimeout	= Variables.RoundedSeconds>
<cfelseif	Request.SlafVersionMajor GE 7><cfsetting RequestTimeout="#Variables.RoundedSeconds#"></cfif>

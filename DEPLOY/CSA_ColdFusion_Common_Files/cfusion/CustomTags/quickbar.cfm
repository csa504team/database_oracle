<!--- QuickBar Cold Fusion Custom Tag --->

<!--- 
Created by Terry Schmitt
tschmitt@checkserv.com
4/7/99 

Modified by Daniel Lee on 4/15/2000
	Notes: Display a image file for the bars instead of bgcolor
		   because IE browser cannot print the bars.
		   I only modified the part of "Horizontal Bar Graph". 
		   If need, we also need to modify the "Vertical Bar Graph".
--->

<!--- **********************************************************
This Cold Fusion custom tag will accept a list of data and create either
vertical or horizontal bar graphs using tables for the display.
Only 2 Parameters are required but many more are optional.
********************************************************** --->

<!--- Parameters
VALUES**
LABELS**
ShowURL
LABELURL
GRAPHSIZE
SCALE
DISPLAYVALUE
TITLE
BARCOLOR
BORDERCOLOR
BORDERSIZE
TYPE
TITLEFONT
TITLEFONTSIZE
DATAFONT
DATAFONTSIZE
--->

<CFIF ThisTag.ExecutionMode IS "start">
	<!--- Initialize variables ************************************************************************--->
	<CFSET varProceed="Yes">
	<CFSET varErrorMsg="">
	<CFSET varValuesListLen="">
	<CFSET varValue="">
	<CFSET varLabels="">
	<CFSET varShowURL="No">
	<CFSET varLabelURL="">
	<CFSET varSize=400>
	<CFSET varDisplayValue="Yes">
	<CFSET varValueLrg="">
	<CFSET varScale=1>
	<CFSET varScaledValue=0>
	<CFSET varMaxValue=0>
	<CFSET varFactor=0>
	<CFSET varValueExcess=0>
	<CFSET varTitle="">
	<CFSET varBarColor="FF0000">
	<CFSET varType="Horiz">
	<CFSET varBGColor="FFFFFF">
	<CFSET varBorderColor="C0C0C0">
	<CFSET varBorderSize=1>
	<CFSET varTitleFont="Arial">
	<CFSET varGraphSize=400>
	<CFSET varTitleFontSize=2>
	<CFSET varDataFont="Arial">
	<CFSET varDataFontSize="1">
	
	<!--- Check for required fields *******************************************************************--->
	<!--- VALUES --->
	<CFIF (#IsDefined("ATTRIBUTES.Values")# IS "No")>
		<CFSET varProceed="No">
		<CFSET varErrorMsg="Values not set!">
	<CFELSE>
		<!--- check values for numbers --->
		<CFSET varListLen=#ListLen(ATTRIBUTES.Values)#>
		<CFSET varItem=1>
		<CFLOOP INDEX="i" FROM="1" TO="#varListLen#">
			<CFSET varListValue=#ListGetAt(ATTRIBUTES.Values,varItem)#>
			<CFIF NOT #IsNumeric(varListValue)#>
				<CFSET varProceed="No">
				<CFSET varErrorMsg="Value #i# is invalid.<BR>&quot;#varListValue#&quot;: is not a number!">
			</CFIF>
			<CFSET varItem=varItem+1>
		</CFLOOP>
	</CFIF>
	
	<CFIF #varProceed# IS "Yes">
		<CFIF #ListLen(ATTRIBUTES.Values)# LT 1>
			<CFSET varProceed="No">
			<CFSET varErrorMsg="Values is empty!">
		</CFIF>
	</CFIF>
	
	<!--- LABELS --->
	<CFIF (#IsDefined("ATTRIBUTES.Labels")# IS "No")>
		<CFSET varProceed="No">
		<CFSET varErrorMsg="Labels not set!">
	</CFIF>
	
	<CFIF #varProceed# IS "Yes">
		<CFIF #ListLen(ATTRIBUTES.Labels)# LT 1>
			<CFSET varProceed="No">
			<CFSET varErrorMsg="Labels is empty!">
		</CFIF>
	</CFIF>
	
	<!--- ITEMS = VALUES? --->
	<CFIF #varProceed# IS "Yes">
		<CFIF #ListLen(ATTRIBUTES.Values)# IS NOT #ListLen(ATTRIBUTES.Labels)#>
			<CFSET varProceed="No">
			<CFSET varErrorMsg="The number of values must equal the number of labels!<BR>Values:#ListLen(ATTRIBUTES.Values)#<BR>Labels:#ListLen(ATTRIBUTES.Labels)#">
		</CFIF>
	</CFIF>
	
	<!--- Check for optional fields******************************************************************* --->
	<!--- URL --->
	<CFIF (#IsDefined("ATTRIBUTES.ShowURL")# IS "Yes")>
		<CFIF #ATTRIBUTES.ShowURL# IS "Yes">
			<CFSET varShowURL="Yes">
			
			<!--- LABELURL --->
			<CFIF (#IsDefined("ATTRIBUTES.LabelURL")# IS "Yes")>
				<CFIF #varProceed# IS "Yes">
					<CFIF #ListLen(ATTRIBUTES.Labels)# IS NOT #ListLen(ATTRIBUTES.LabelURL)#>
						<CFSET varProceed="No">
						<CFSET varShowURL="No">
						<CFSET varErrorMsg="The number of label URL's must equal the number of labels when the ShowURL attribute is set to &quot;Yes&quot;!<BR>Label URL's:#ListLen(ATTRIBUTES.LabelURL)#<BR>Labels:#ListLen(ATTRIBUTES.Labels)#">
					</CFIF>
				</CFIF>
			<CFELSE>
				<CFSET varProceed="No">
				<CFSET varShowURL="No">
				<CFSET varErrorMsg="The LabelURL attribute is required when the ShowURL attribute is set to &quot;Yes&quot;!">
			</CFIF>
		</CFIF>
	</CFIF>
	
	<!--- DISPLAYVALUE --->
	<CFIF (#IsDefined("ATTRIBUTES.DisplayValue")# IS "Yes")>
		<CFIF #ATTRIBUTES.DisplayValue# IS "No">
			<CFSET varDisplayValue="No">
		</CFIF>
	</CFIF>
	
	<!--- GRAPHSIZE --->
	<CFIF (#IsDefined("ATTRIBUTES.GraphSize")# IS "Yes")>
		<CFIF (#IsNumeric(ATTRIBUTES.GraphSize)# IS "Yes") AND (#ATTRIBUTES.GraphSize# LT 2000) AND (#ATTRIBUTES.GraphSize# GT 99)>
			<CFSET varGraphSize=#ATTRIBUTES.GraphSize#>
		</CFIF>
	</CFIF>
	
	<!--- SCALE --->
	<CFIF (#IsDefined("ATTRIBUTES.Scale")# IS "Yes")>
		<CFIF (#IsNumeric(ATTRIBUTES.Scale)# IS "Yes")>
			<CFIF (#ATTRIBUTES.Scale# GT 0) AND (#ATTRIBUTES.Scale# LTE 100)>
				<CFSET varScale=#ATTRIBUTES.Scale#>
			</CFIF>
		</CFIF>
	</CFIF>
	
	<!--- TITLE --->
	<CFIF (#IsDefined("ATTRIBUTES.Title")# IS "Yes")>
		<CFIF #Len(ATTRIBUTES.Title)# GT 0>
			<CFSET varTitle=#ATTRIBUTES.Title#>
		</CFIF>
	</CFIF>
	
	<!--- BARCOLOR --->
	<CFIF (#IsDefined("ATTRIBUTES.BarColor")# IS "Yes")>
		<CFIF #Len(SpanIncluding(ATTRIBUTES.BarColor, "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F"))# IS 6>
			<CFSET varBarColor=#ATTRIBUTES.BarColor#>
		<CFELSE>
			<CFSET varBarColor="FF0000">
		</CFIF>
	</CFIF>
	
	<!--- TYPE --->
	<CFIF (#IsDefined("ATTRIBUTES.Type")# IS "Yes")>
		<CFIF #ATTRIBUTES.Type# IS "Vert">
			<CFSET varType="Vert">
		<CFELSE>
			<CFSET varType="Horiz">
		</CFIF>
	</CFIF>
	
	<!--- BGCOLOR --->
	<CFIF (#IsDefined("ATTRIBUTES.BGColor")# IS "Yes")>
		<CFIF #Len(SpanIncluding(ATTRIBUTES.BGColor, "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F"))# IS 6>
			<CFSET varBGColor=#ATTRIBUTES.BGColor#>
		<CFELSE>
			<CFSET varBGColor="FFFFFF">
		</CFIF>
	</CFIF>
	
	<!--- BORDERCOLOR --->
	<CFIF (#IsDefined("ATTRIBUTES.BorderColor")# IS "Yes")>
		<CFIF #Len(SpanIncluding(ATTRIBUTES.BorderColor, "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F"))# IS 6>
			<CFSET varBorderColor=#ATTRIBUTES.BorderColor#>
		<CFELSE>
			<CFSET varBorderColor="C0C0C0">
		</CFIF>
	</CFIF>
	
	<!--- BORDERSIZE --->
	<CFIF (#IsDefined("ATTRIBUTES.BorderSize")# IS "Yes")>
		<CFIF (#IsNumeric(ATTRIBUTES.BorderSize)# IS "Yes")>
			<CFIF (#ATTRIBUTES.BorderSize# GTE 0) AND (#ATTRIBUTES.BorderSize# LTE 10)>
				<CFSET varBorderSize=#int(ATTRIBUTES.BorderSize)#>
			</CFIF>
		</CFIF>
	</CFIF>
	
	<!--- TITLEFONT --->
	<CFIF (#IsDefined("ATTRIBUTES.TitleFont")# IS "Yes")>
		<CFIF #Len(ATTRIBUTES.TitleFont)# GT 0>
			<CFSET varTitleFont="#ATTRIBUTES.TitleFont#">
		<CFELSE>
			<CFSET varTitleFont="Arial">
		</CFIF>
	</CFIF>
	
	<!--- TITLEFONTSIZE--->
	<CFIF (#IsDefined("ATTRIBUTES.TitleFontSize")# IS "Yes")>
		<CFIF #IsNumeric(ATTRIBUTES.TitleFontSize)#>
			<CFSET varTitleFontSize="#ATTRIBUTES.TitleFontSize#">
		<CFELSE>
			<CFSET varTitleFontSize=2>
		</CFIF>
	</CFIF>
	
	<!--- DATAFONT --->
	<CFIF (#IsDefined("ATTRIBUTES.DataFont")# IS "Yes")>
		<CFIF #Len(ATTRIBUTES.DataFont)# GT 0>
			<CFSET varDataFont="#ATTRIBUTES.DataFont#">
		<CFELSE>
			<CFSET varDataFont="Arial">
		</CFIF>
	</CFIF>
	
	<!--- DATAFONTSIZE--->
	<CFIF (#IsDefined("ATTRIBUTES.DataFontSize")# IS "Yes")>
		<CFIF #IsNumeric(ATTRIBUTES.DataFontSize)#>
			<CFSET varDataFontSize="#ATTRIBUTES.DataFontSize#">
		<CFELSE>
			<CFSET varDataFontSize=1>
		</CFIF>
	</CFIF>

	<!--- Unit --->	
	<CFIF IsDefined("attributes.cf_unit")>
		<CFSET varUnit = "#attributes.cf_unit#">
	<CFELSE>
		<CFSET varUnit = "Number Of Records">
	</CFIF>
	
	<!--- If OK, continue *****************************************************************************--->
	<CFIF #varProceed# IS "Yes">
		<!--- Horizontal Bar Graph************************************************************************* --->
		<CFIF varType IS "Horiz">
			<CFSET varValuesListLen=#ListLen(ATTRIBUTES.Values)#>
			<TABLE ALIGN="center" BORDER="0" CELLSPACING="0" CELLPADDING="<CFOUTPUT>#varBorderSize#</CFOUTPUT>" BGCOLOR="#<CFOUTPUT>#varBorderColor#</CFOUTPUT>"><TR><TD>
			<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="0" BGCOLOR="#<CFOUTPUT>#varBGColor#</CFOUTPUT>">
			<TR>
				<!--- Title --->
				<TD COLSPAN="2" ALIGN="CENTER"><FONT FACE="<CFOUTPUT>#varTitleFont#" SIZE="#varTitleFontSize#"><B>#varTitle#</CFOUTPUT></B></FONT></TD>
			</TR>
			
			<TR>
			    <TD ALIGN="LEFT" VALIGN="TOP">
					<!--- Bars --->

					<!--- Find max value --->
					<CFLOOP INDEX="ii" FROM="1" TO="#varValuesListLen#">
						<CFIF #ListGetAt(ATTRIBUTES.Values, ii)# GT #varMaxValue#>
							<CFSET varMaxValue=#ListGetAt(ATTRIBUTES.Values, ii)#>
						</CFIF>
					</CFLOOP>
	
					<!--- Set too large factor --->
					<CFSET varFactor = #varMaxValue# / 40>
					<!--- display bars --->
					<TABLE  BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="400">
						<TR>
							<TD ALIGN="center">
								<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">Date</FONT>
							</TD>
							<TD ALIGN="center">
								<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">
									<CFOUTPUT>#varUnit#</CFOUTPUT>
								</FONT>
							</TD>
						</TR>
						
					<CFLOOP INDEX="ii" FROM="1" TO="#varValuesListLen#">
						<CFSET varLabels="#ListGetAt(ATTRIBUTES.Labels, ii)#">
						<CFIF #varShowURL# IS "Yes">			
							<CFSET varLabelURL="#ListGetAt(ATTRIBUTES.LabelURL, ii)#">
						</CFIF>
						
						<CFSET varValue="#ListGetAt(ATTRIBUTES.Values, ii)#">
						<CFSET varScaledValue=#varValue#*#varScale#>
						<CFOUTPUT>

						<TR>
							<TD WIDTH="20%" NOWRAP>						
								<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">
								<CFIF #varShowURL# IS "Yes">
									<A HREF="#varLabelURL#">#varLabels#&nbsp;</A>
								<CFELSE>
									#varLabels#&nbsp;
								</CFIF>
								</FONT>
							</TD>
							
						    <!--- If value is larger then set size --->
							<CFIF (#varScaledValue# GT (#varGraphSize# - 200))>
								<CFSET varValueLrg=(#varGraphSize# - 200)>
								<CFSET varValueExcess=#varValue#/#varFactor#>

								<TD NOWRAP>
									<IMG SRC="../images/redbar.gif" WIDTH="#varValueLrg#" HEIGHT="12">
									<!---IMG SRC="../images/plus.gif" WIDTH="#varValueLrg#" HEIGHT="12"--->
									<FONT SIZE="1">&nbsp;//&nbsp;</FONT>
									<IMG SRC="../images/redbar.gif" WIDTH="#varValueExcess#" HEIGHT="12">
									<!---IMG SRC="../images/plus.gif" WIDTH="#varValueExcess#" HEIGHT="12"--->
									<FONT FACE="" SIZE="1">&nbsp;</FONT>
									<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">
									<CFIF #varDisplayValue# IS "Yes">
										#varValue#
									<CFELSE>
										&nbsp;
									</CFIF>
									</FONT>
								</TD>
								
							<!--- If value is zero --->
							<CFELSEIF #varValue# IS 0>
								<TD>
									<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">
									<CFIF #varDisplayValue# IS "Yes">
										#varValue#
									<CFELSE>
										&nbsp;
									</CFIF>
									</FONT>
								</TD>

							<!--- Any other value --->
							<CFELSE>
								<TD NOWRAP>
									<IMG SRC="../images/redbar.gif" WIDTH="#varScaledValue#" HEIGHT="12">
									<!---IMG SRC="../images/plus.gif" WIDTH="#varScaledValue#" HEIGHT="12"--->
									&nbsp;
									<FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">
									<CFIF #varDisplayValue# IS "Yes">
										#varValue#
									<CFELSE>
										&nbsp;
									</CFIF>
									</FONT>
								</TD>
							</CFIF>
						</TR>
						
						</CFOUTPUT>
					</CFLOOP>
				</TABLE>
				</TD>
			</TR>
			</TABLE>
			</TD></TR></TABLE>

		</CFIF>
		
		<!--- Vertical Bar Graph************************************************************************* --->
		<CFIF varType IS "Vert">
			<CFSET varValuesListLen=#ListLen(ATTRIBUTES.Values)#>
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="<CFOUTPUT>#varBorderSize#</CFOUTPUT>" BGCOLOR="#<CFOUTPUT>#varBorderColor#</CFOUTPUT>"><TR><TD>
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" BGCOLOR="#<CFOUTPUT>#varBGColor#</CFOUTPUT>">
			<TR>
				<CFOUTPUT>
				<TD COLSPAN="#varValuesListLen#" ALIGN="CENTER"><FONT FACE="#varTitleFont#" SIZE="#varTitleFontSize#"><B>#varTitle#</B></FONT></TD>
				</CFOUTPUT>
			</TR>
			<TR>
				<CFLOOP INDEX="ii" FROM="1" TO="#varValuesListLen#">
					<CFSET varLabels="#ListGetAt(ATTRIBUTES.Labels, ii)#">
					<CFSET varValue="#ListGetAt(ATTRIBUTES.Values, ii)#">
					<CFSET varScaledValue=#varValue#*#varScale#>
					<!--- Find max value --->
					<CFLOOP INDEX="ii" FROM="1" TO="#varValuesListLen#">
						<CFIF #ListGetAt(ATTRIBUTES.Values, ii)# GT #varMaxValue#>
							<CFSET varMaxValue=#ListGetAt(ATTRIBUTES.Values, ii)#>
						</CFIF>
					</CFLOOP>
					<CFOUTPUT>
					<!--- Set too large factor --->
					<CFSET varFactor=#varMaxValue#/40>
				    <TD ALIGN="CENTER" VALIGN="BOTTOM">
						<TABLE CELLSPACING="2" CELLPADDING="2" BORDER="0">
							<TR>
							    <TD ALIGN="CENTER"><FONT FACE="#varDataFont#" SIZE="#varDataFontSize#"><CFIF #varDisplayValue# IS "Yes">#varValue#</CFIF></FONT></TD>
							</TR>
							<CFIF #varValue# IS NOT 0>
							<TR>
							    <TD ALIGN="CENTER">	
								<!--- If value is larger then set size --->
								<CFIF (#varScaledValue# GT (#varGraphSize#-100))>
									<CFSET varValueLrg=(#varGraphSize#-99)>
									<CFSET varValueExcess=#varValue#/#varFactor#>
									<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
										<TR>
											<TD WIDTH="10" HEIGHT="#varValueExcess#" BGCOLOR="#varBarColor#">&nbsp;</TD>
										</TR>
										<TR>
											<TD ALIGN="CENTER" VALIGN="MIDDLE"><FONT FACE="Arial" SIZE="1">//</FONT></TD>
										</TR>
										<TR>
										    <TD WIDTH="10" HEIGHT="#varValueLrg#" BGCOLOR="#varBarColor#"><TABLE CELLSPACING="2" CELLPADDING="2" BORDER="0"><TR><TD></TD></TR></TABLE></TD>
										</TR>
									</TABLE>
								<CFELSEIF #varValue# IS 0>
									<!--- If value is zero --->
									<!--- Do not display anything --->		
								<CFELSE>
									<!--- Any other value --->
									<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
										<TR>
										    <TD WIDTH="10" HEIGHT="#varScaledValue#" BGCOLOR="#varBarColor#"><TABLE CELLSPACING="2" CELLPADDING="2" BORDER="0"><TR><TD></TD></TR></TABLE></TD>
										</TR>
									</TABLE>
								</CFIF>
								</TD>
							</TR>
							</CFIF>
							<TR>
							    <TD><FONT FACE="#varDataFont#" SIZE="#varDataFontSize#">#varLabels#</FONT></TD>
							</TR>
						</TABLE>
					</TD>
					</CFOUTPUT>
				</CFLOOP>
			</TR>
			</TABLE>
			</TD></TR></TABLE>
		</CFIF>
	
	<CFELSE>
		<!--- Error occurred --->
		<TABLE WIDTH="400" BORDER="0" CELLSPACING="2" CELLPADDING="2" BGCOLOR="#C0C0C0" HEIGHT="200">
		<TR>
			<TD ALIGN="CENTER" VALIGN="TOP"><FONT FACE="Arial" SIZE="3"><B>Graphing Error!</B></FONT></TD>
		</TR>
		<TR>
		    <TD ALIGN="CENTER" VALIGN="TOP"><CFOUTPUT><FONT FACE="Arial" SIZE="3">#varErrorMsg#</FONT></CFOUTPUT></TD>
		</TR>
		</TABLE>
	</CFIF>
</CFIF>
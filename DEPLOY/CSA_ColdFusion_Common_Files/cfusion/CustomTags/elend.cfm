<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				11/20/2014. 
DESCRIPTION:		Stepping-stone custom tag into the new elend.class parser. 
NOTES:

	The old CustomTag interface had its own custom Request and Response objects which behaved much like HashMaps. 
	But Request.getAttribute always required an uppercase key. For source code compatibility, pass keys to the 
	HashMap implementation in uppercase too. That's the reason for setting ListRequestKeys to UCase(...). 

INPUT:				Same attributes as the old cfx_etran parser. 
OUTPUT:				Same variables  as the old cfx_etran parser. 
REVISION HISTORY:	12/11/2014, SRS:	Made it capable of calling the old cfx too (for enile and eweb). 
					11/20/2014, SRS:	Original implementation. 
--->


<cfswitch expression="#thisTag.ExecutionMode#">
<cfcase value="start">
	<!--- Okay to use simple names in Variables scope, because custom tags have their own Variables scope: --->
	<cfset Variables.Keys									= "CfxAppAttributes,"
															& "CfxErrMsg,"
															& "CfxETranVersion,"
															& "CfxSucceeded,"
															& "CfxTrace,"
															& "CfxVersion,"
															& "CfxXlateColNames,"
															& "CfxXlateTblNames,"
															& "CfxXmlFieldData">
	<cfset Variables.CfxWorks								= "No">
	<cflock scope="Server" type="readonly" timeout="30">
		<cfset Variables.CfxWorks							= (ListFirst(Server.Coldfusion.ProductVersion) is "9")>
	</cflock>
	<cfif Variables.CfxWorks>
		<cfset Request.SBALogText							= "cf_elend old">
		<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
		<cfx_etran
			xml												= "#Caller.Inputs.RequestData#"
			trace											= "#Caller.Inputs.ParserSetDebug#"
			validating										= "#Caller.Inputs.ParserSetValidating#"
			xlatedir										= "#ExpandPath(Caller.SysXmlPrURL)#/config"
			db												= "#Caller.OracleSchemaName#">
		<cfset Variables.Key								= "">
		<cfloop index="Variables.Key" list="#Variables.Keys#">
			<cfset Caller[Variables.Key]					= Variables[Variables.Key]>
		</cfloop>
	<cfelse><!--- Default is to use elend.class instead: --->
		<cfset Request.SBALogText							= "cf_elend new">
		<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
		<cfset Variables.ELendParser						= CreateObject("java", "elend").init()>
		<cfset Request.SBALogText							= "created">
		<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
		<cfset Variables.ListRequestKeys					= UCase(StructKeyList(Attributes))>
		<cfset Variables.ListResponseKeys					= "">
		<cfset Variables.Request							= CreateObject("java", "java.util.HashMap").init()>
		<cfset Variables.Response							= CreateObject("java", "java.util.HashMap").init()>
		<!--- CF11 likes to cast "Yes" and "No" to java.lang.Boolean, but elend.class expects "Yes" or "No", so: --->
		<cfloop index="Variables.Key" list="#Variables.ListRequestKeys#">
			<cfif IsBoolean(Attributes[Variables.Key])>
				<cfif Attributes[Variables.Key]>
					<cfset Variables.Request.put(Variables.Key, JavaCast("string", "Yes"))><!--- Be extra sure. --->
				<cfelse>
					<cfset Variables.Request.put(Variables.Key, JavaCast("string", "No"))> <!--- Be extra sure. --->
				</cfif>
			<cfelse>
				<cfset Variables.Request.put(Variables.Key, Attributes[Variables.Key])>
			</cfif>
		</cfloop>
		<cfset Request.SBALogText							= "before">
		<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
		<!---
		REST web services lose CFCatch behavior in caller's cftry, resulting in a loss of Outputs.ResponseData. 
		(Execution gets interrupted by the CFTry, but its CFCatch clause doesn't get executed. As a result, 
		CFCatch.Message and CFCatch.Detail don't get returned to user.) Therefore, we have to do our own error 
		recovery here, using our own "Cfx" variables interface. It's up to the caller to complain in a manner 
		appropriate to the call. Part 1 is the cftry/cfcatch itself that sets CfxErrMsg if we catch an error: 
		--->
		<cfset Variables.CfxErrMsg							= ""><!--- Nullstring = no error occurred. --->
		<cftry>
			<cfset Variables.ELendParser.processRequest(Variables.Request, Variables.Response)>
			<cfcatch type="any">
				<cfset Variables.CfxErrMsg					= "#CFCatch.Message# #CFCatch.Detail#"><!--- Non-nullstring --->
				<cfset Request.SBALogText					= Variables.CfxErrMsg>
				<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
			</cfcatch>
		</cftry>
		<cfset Request.SBALogText							= "after">
		<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
		<cfif Len(Variables.CfxErrMsg) is 0>
			<!--- No error occurred in the call, great. Pass back data, which is for-sure all strings: --->
			<cfset Variables.ResponseKeySet					= Variables.Response.keySet() />
			<cfset Variables.ResponseIterator				= Variables.ResponseKeySet.iterator() />
			<cfloop condition="Variables.ResponseIterator.hasNext()">
				<cfset Variables.Key						= Variables.ResponseIterator.next() />
				<cfset Caller[Variables.Key]				= Variables.Response.get(Variables.Key)>
			</cfloop>
		<cfelse>
			<!--- Part 2 of cftry/cfcatch. Len(CfxErrMsg) gt 0, therefore, we caught an error. Pass it back to caller: --->
			<cfloop index="Variables.Key" list="#Variables.Keys#">
				<cfswitch expression="#Variables.Key#">
				<cfcase value="CfxErrMsg">
					<cfset Caller[Variables.Key]			= Variables.CfxErrMsg>
				</cfcase>
				<cfcase value="CfxSucceeded">
					<cfset Caller[Variables.Key]			= "No">
				</cfcase>
				<cfdefaultcase>
					<cfset Caller[Variables.Key]			= "">
				</cfdefaultcase>
				</cfswitch>
			</cfloop>
		</cfif>
	</cfif>
	<cfset Request.SBALogText								= "cf_elend exit">
	<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
</cfcase><!--- End of processing when thisTag.ExecutionMode is "start". --->
<cfcase value="end">
</cfcase>
</cfswitch>

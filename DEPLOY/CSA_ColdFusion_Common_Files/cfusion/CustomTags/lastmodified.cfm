<!---
AUTHOR:				Steve Seaquist
DATE:				10/04/2004
DESCRIPTION:		Custom tag to display date/time, even when used in a custom tag, and even when used under Unix. 
NOTES:				None
INPUT:				None
OUTPUT:				"Last modified: MM/DD/YYYY hh:mm:ss AM/PM" sent to cfoutput. 
REVISION HISTORY:	06/17/2008, SRS:	Added support for Variables.SlafPrevTickCounts. 
					11/24/2005, SRS:	Made processing time go to another line for new look-and-feel. 
					09/02/2005, SRS:	Tightened up whitespace management. 
					12/06/2004, DYL:	Added Version option. 
					10/04/2004, SRS:	Original implementation.
--->

<!--- Get date/time base template was modified: --->

<cfdirectory
	directory										= "#GetDirectoryFromPath(GetBaseTemplatePath())#"
	filter											= "#GetFileFromPath		(GetBaseTemplatePath())#"
	name											= "getCaller"
	sort											= "datelastmodified desc">
	
<cfset Variables.DateLastModified					= getCaller.datelastmodified>

<cfif	(Left(Variables.DateLastModified, 6) IS    "Sunday")
	OR	(Left(Variables.DateLastModified, 6) IS    "Monday")
	OR	(Left(Variables.DateLastModified, 7) IS   "Tuesday")
	OR	(Left(Variables.DateLastModified, 9) IS "Wednesday")
	OR	(Left(Variables.DateLastModified, 8) IS  "Thursday")
	OR	(Left(Variables.DateLastModified, 6) IS    "Friday")
	OR	(Left(Variables.DateLastModified, 8) IS  "Saturday")>
	<cfset Variables.DateLastModified				= ListDeleteAt(Variables.DateLastModified, 1)>
</cfif>
<cfset Variables.DateLastModified					= DateFormat(Variables.DateLastModified, "MM/DD/YYYY")>
<cfset Variables.TimeLastModified					= TimeFormat(Variables.DateLastModified, "h:MM:SS tt")>

<!--- Get processing time for this page and the action page(s) that preceeded it: --->

<cfset Variables.TotalTickCount						= 0>
<cftry>
	<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
		<cfif IsDefined("Session.SlafActPageVariables.SlafPrevTickCounts")><!--- From put_act_page_variables. --->
			<cfset Variables.TotalTickCount			= Variables.TotalTickCount
													+ Session.SlafActPageVariables.SlafPrevTickCounts>
			<cfset StructClear(Session.SlafActPageVariables)><!--- Intentionally destructive. Okay in display page. --->
			<cfset StructDelete(Session,"SlafActPageVariables")>
		</cfif>
	</cflock>
	<cfcatch type="Any"><!--- Session scope might not exist. If not, ignore the error: ---></cfcatch>
</cftry>
<cfif IsDefined("Request.RequestStartTickCount")>
	<cfset Variables.TotalTickCount					= Variables.TotalTickCount
													+ (GetTickCount() - Request.RequestStartTickCount)>
</cfif>
<cfset Variables.TotalTickCount						= Variables.TotalTickCount /1000>

<!--- Output to the Web page: --->

<cfoutput>Last modified:&nbsp;#Variables.DateLastModified#&nbsp;#Variables.TimeLastModified#&nbsp;&nbsp;</cfoutput>

<cfif Variables.TotalTickCount GT 0>
	<cfif IsDefined("Request.SlafAppDataIsInline") AND Request.SlafAppDataIsInline><!--- From cf_sbalookandfeel --->
		<cfoutput></span><span class="SpanLastModifiedR"></cfoutput>
	</cfif>
	<cfoutput>SBA Processing:&nbsp; #Variables.TotalTickCount#&nbsp;seconds</cfoutput>
<cfelse>
	<cfoutput>&nbsp;...</cfoutput>
</cfif>

<cfif IsDefined("Attributes.Version")>
	<cfoutput>&nbsp;Version:&nbsp;#Attributes.Version#</cfoutput>
<cfelseif IsDefined("Request.Version")>
	<cfoutput>&nbsp;Version:&nbsp;#Request.Version#</cfoutput>
</cfif>

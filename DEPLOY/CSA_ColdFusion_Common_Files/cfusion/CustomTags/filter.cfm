<!---
NAME: CF_Filter

AUTHOR: Ali Taleb

DATE: 22 March 2000

Modification Log:
Date		Author			Modification
-----------	--------------- ---------------------------
5/8/00		Daniel Lee		Added an option to ouput the illegal strings
							and number of illegal string.
7/1/2002	Daniel Lee		Commented out "fart" because a user has the word in her email.
8/29/2002	Daniel Lee		Added "suck" and "a s s" on the illegal string list
							
DESCRIPTION:
A flexible ColdFusion custom tag to replace characters in illegal words 
passed as form field values with defined symbols. It also gives the user
the ability to either destroy HTML and ColdFusion tags passed as form field
values or just replace the angled brackets with round ones to neutralise
their possible effect.

ATTRIBUTES:
FIELDS              - (optional) a comma-delimited list of the form field names to be 
                                 filtered. Defaults to all the form fields.
REPLACEMENT         - (optional) character or symbol to replace the letters in the
                                 illegal word or tag word. Defaults to '*'
PRESERVEFIRSTLETTER - (optional) Determines wether to preserve the first letter in the
                                 replaceable word or not. valid values are "YES" or "NO". 
                                 e.g. 'fucking' could be changed to 'f***ing' if the 
                                 attribute is set to "YES", or it could change to '****ing'
                                 if the attribute is set to "NO". The default value is "No"
TAGS                - (optional) Takes two possible values: 
                                 "MODIFY"  - Replaces angle brackets with round ones.
                                 "DESTROY" - Preserves the angled brackets but kills the HTML
                                             or CF tag words in between. This option will take
                                             more computational time. It is considearbly
                                             faster and neeter looking to use the defaulted
                                             'Modify' attribute.                                   

USAGE:
Place this tag in the page receiving the form. It will then change
the values of the form fields containing the illegal words.
The easiest syntax is <CF_Filter> as all the attributes are optional with pre-defined default values.

SOME USAGE EXAMPLES:
<CF_Filter>
<CF_Filter Fields="Subject,Comments">
<CF_Filter Fields="Subject,Comments", PreserveFirstLetter="Yes">
<CF_Filter Replacement="*" Tags="DESTROY">


NOTE:
The illegal words have been divided into two lists named 'DeffinitelyIllegal' and 'ProbablyIllegal'.
You are free to add or remove items form the two lists but if adding, the most important point to bear in 
mind is which list to add to.
'DeffinitelyIllegal' contains words which if they occur as fully or as part of another word, cause the result
to be illegal as well. e.g: 'fuck', 'fucking', 'fucked' are all words containing the string 'fuck', and
therefore, 'fuck' is part of the 'DeffinitelyIllegal' list.
However, the word 'dick' is illegal but only if it occurs independently in a sentence. If it is part of
another word, e.d 'Dickinson' then it becomes acceptable. This kind of words are part of the 'ProbablyIllegal' list.

The tag has been fully tested in CF 4.0.1. Please email me at a.t@england.com with any problems or comments.
Please also notify me of any HTML tag words I have missed out in the 'IllegalTagWords' list. 
--->


<cfsetting enablecfoutputonly="Yes">

<!--- <cfset DeffinitelyIllegal = "masturbate,fuck,dickhead,dick-head,bollock,pisshead,piss-head,shithead,shit-head,bastard,arsehole,arse-hole,asshole,pimpster,penis,fart,bong"> --->
<cfset DeffinitelyIllegal = "masturbate,fuck,dickhead,dick-head,bollock,pisshead,piss-head,shithead,shit-head,bastard,arsehole,arse-hole,asshole,pimpster,penis,bong,suck,a s s">
<cfset ProbablyIllegal = "dick,cunt,bitch,pussy,piss,shit,shite,cum,penis,vagina,fanny,clits,clit,clitories,dildo,arse,ass,pimp">
<cfset IllegalTagWords = "doctype,a,acronym,address,applet,area,b,base,basefont,bgsound,big,blink,blockquote,body,br,button,caption,center,cite,code,col,colgroup,comment,dd,del,dfn,dir,div,dl,dt,em,embed,em,fieldset,font,form,frame,frameset,head,hr,html,hx,i,iframe,ilayer,img,input,ins,isindex,kbd,keygen,label,layer,legend,li,link,listing,map,marquee,menu,meta,multicol,nextid,nobr,noembed,noframes,object,ol,option,p,plaintext,pre,q,s,samp,select,small,sound,spacer,span,strike,strong,style,sub,sup,table,tbody,td,textarea,tfoot,th,thead,title,tr,tt,u,ul,var,wbr,xmp">
<cfset IllegalCharacters = "&gt;,&lt;">


<cfif Not IsDefined("Form.FieldNames")>
  <cfoutput><b>Error!</b> There are no form fields passed to this page for the custom tag to filter.</cfoutput>
  <cfabort>
  
<cfelse>
  <cfif IsDefined("Attributes.Fields")>
    <cfloop list="#Attributes.Fields#" index="f">
     <cfset f = #Trim(f)#>
     <cfif ListFindNoCase(#Form.FieldNames#, #f#) eq 0>
       <cfoutput><b>Error!</b> There is no form field named '#f#' passed to this page. Please check your spelling.</cfoutput>
       <cfabort>
     </cfif>
    </cfloop>
  </cfif>   
</cfif>  

<cfparam name="Attributes.Fields" default="#Form.FieldNames#">
<cfparam name="Attributes.Replacement" default="">
<cfparam name="Attributes.PreserveFirstLetter" default="No">
<cfparam name="Attributes.Tags" default="Modify">

<CFSET caller.num_illegal_string = 0>
<CFSET caller.illegal_list = "">

<cfif (Attributes.PreserveFirstLetter) is "Yes">
  <cfset StartPoint = 2>
<cfelse>  
  <cfset StartPoint = 1>
</cfif>  

<cfloop list="#Attributes.Fields#" index="i">
	<cfset i = #Trim(i)#>
	<cfset CurrentFieldValue = Evaluate("Form." & #i#)>

	<cfloop List="#DeffinitelyIllegal#" index="d">
	
		<!--- create a star list of DeffinitelyIllegal --->
		<cfset StarList = "">
		<cfset WordLength = Len(#d#)>
    
		<cfloop from="#StartPoint#" to="#WordLength#" index="w">
			<cfset StarList = ListAppend(StarList, "#Attributes.Replacement#",",")>
		</cfloop>
		
		<cfset StarList = Replace(StarList,",","","All")>
   		
		<!--- Add the PreserveFirstLetter on the star list e.g. f***,d******* --->
	    <cfif (Attributes.PreserveFirstLetter) is "Yes">
			<cfset FirstLetter = Left(#d#,1)>
			<cfset StarList = ListPrepend(StarList,"#FirstLetter#","��")>
			<cfset StarList = Replace(StarList,"�","","All")>
	    </cfif> 
    	
		<!--- replace the DeffinitelyIllegal word --->
		<cfset CurrentFieldValue = ReplaceNoCase(CurrentFieldValue, #d#, #StarList#, "All")>
		
		<cfset CurrentFieldValue = Replace(CurrentFieldValue, "<","&lt;","All")>
		<cfset CurrentFieldValue = Replace(CurrentFieldValue, ">","&gt;","All")>
		
		<!--- Replace the IllegalTagWords --->
	    <cfif (Attributes.Tags) is "Destroy">
			<!--- replace the angle bracket string e.g. <TABLE> to <*> --->
			<cfloop list="#IllegalTagWords#" index="t">
		        <cfif (FindNoCase("&lt;#t# ", #CurrentFieldValue#) gt 0) 
		           OR (FindNoCase("&lt;#t#&gt;", #CurrentFieldValue#) gt 0)
		           OR (FindNoCase("&lt;/#t#&gt;", #CurrentFieldValue#) gt 0)
		           OR (FindNoCase("&lt;/#t# ", #CurrentFieldValue#) gt 0)>
				   		<cfset CurrentFieldValue = Replace(CurrentFieldValue, #t#, #Attributes.Replacement#, "All")>
		        </cfif>
		        <cfif (FindNoCase("&lt;cf", #CurrentFieldValue#) gt 0)
		           OR (FindNoCase("&lt;/cf", #CurrentFieldValue#) gt 0)>
						<cfset CurrentFieldValue = ReplaceNoCase(CurrentFieldValue, "cf", #Attributes.Replacement#, "All")>
				</cfif>
			</cfloop> 
	     <cfelse>
			<!--- change the <> to () e.g. <table> to (table) --->
			<cfset ListToReplace="),("> 
			<cfset CurrentFieldValue = ReplaceList(CurrentFieldValue, #IllegalCharacters#, #ListToReplace#)>		
	     </cfif> 
	</cfloop>

	<CFSET num_probably_illegal = 0>
	<cfloop list="#ProbablyIllegal#" index="p">
		<cfset ListCounter = 0>
		<cfloop delimiters=" " list="#CurrentFieldValue#" index="u">
			<cfset StarList = "">
			<cfset WordLength = Len(#p#)>
			<cfif u eq p>
				<CFSET num_probably_illegal = num_probably_illegal + 1>
				
				<cfset BadWordPosition = (#ListCounter#+1)>
				<cfloop from="#StartPoint#" to="#WordLength#" index="w">
					<cfset StarList = ListAppend(StarList, "#Attributes.Replacement#",",")>
				</cfloop>
	    		<cfset StarList = Replace(StarList,",","","All")>
        
		        <cfif (Attributes.PreserveFirstLetter) is "Yes">
					<cfset FirstLetter = Left(#u#,1)>
					<cfset StarList = ListPrepend(StarList,"#FirstLetter#","��")>
					<cfset StarList = Replace(StarList,"�","","All")>
		        </cfif> 
				<cfset CurrentFieldValue = ListSetAt(CurrentFieldValue, #BadWordPosition#, #StarList#, " ")>
			</cfif>
			<cfset ListCounter = IncrementValue(ListCounter)>
		</cfloop>
	</cfloop>

	<!--- count the number of illegal strings --->
	<CFIF Evaluate("Caller.Form.#i#") IS NOT CurrentFieldValue>
		<CFSET Caller.num_illegal_string = Caller.num_illegal_string + 1>
		
		<!--- change the < and > to &lt; and &gt; --->
		<CFIF FindOneOf("<>", Evaluate("Form.#i#"))>
			<cfset ListToReplace="&gt;,&lt;">
			<cfset temp = ReplaceList(Evaluate("Form.#i#"), ">,<", #ListToReplace#)>
			<CFSET temp = SetVariable("Form.#i#", temp)>
		</CFIF>
		
		<CFSET caller.illegal_list = ListAppend(caller.illegal_list, Evaluate("Form.#i#"))>
	</CFIF>
	
	<!--- if Replacement is not "", update the input variables --->
	<CFIF Attributes.Replacement IS NOT "">
		<cfset Temp = SetVariable("Caller.Form.#i#" , CurrentFieldValue)>
	</CFIF>
	
</cfloop>

<cfsetting enablecfoutputonly="No">
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				10/20/2015.
DESCRIPTION:		Outputs new "button bar" with variable number of icons.
NOTES:

	Requires the caller to have established the "offset parent" element. If that isn't done, the browser will probably use the
	window as the offset parent, which may not be what you want. All positioning calculations are done in ColdFusion, not using
	CSS's calc() function, to prevent crashes in IE 9.

	There currently isn't any checking to make sure that the same icon name doesn't appear twice, either both in leftside, or
	both in rightside, or once in leftside and once in rightside. If that happens, the same element id would be generated more
	than once, which will create problems referencing one of the two elements. The reason why is, it's possible to call this
	custom tag more than once in the same page, and there wouldn't any mechanism to prevent collisions in that situation. So it
	might as well be documented up-front as a pitfall to be avoided.

	The output HTML could theoretically be captured by cfsavecontent in the caller, but no promises that that trick would work.

INPUT:

	AppName			For pages other than the Home page, overrides "Application Help" to Attributes.AppName & " Help".
	IconColor		Override the color normally used by the user-selected theme. Default is "##69c".
	IconSize		Size specification in pixels. Applied to both height and width because our icons are square. Default is "24".
	Indent			Indention to be applied to every line generated. Default is 1 tab. Used to beautify View Source listings.
	LeftSide		Comma-delimited list of icons to output on the left side of screen.
	LibURL			Which document root directory to use as Library. Default "/library".
	RightSide		Comma-delimited list of icons to output on the right side of screen.
	SLAF			Is the caller SBA Look-and-Feel? Default "Yes".
	Spacing			Spacing between icons in pixels. Default is "2".

OUTPUT:				HTML, output to the web page, which will appear TO THE USER in the order specified (left to right). However, the
					icons might not appear in that same order in the View Source. Also, if an icon name is unknown, it will result in
					a hole in the button bar, as if the icon went missing somehow. This makes the error evident and the cause obvious.
REVISION HISTORY:	02/02/2017, NS:		(OPSMDEV-1277) Changes file for LINC help.
					02/23/2016 -
					05/06/2016, SRS:	OPSMDEV-909: Changed filenames for 2 PDFs in the help menu. Made the email icon point to
										/cls/dsp_contactus.cfm. Made hotlink icons into actual hotlinks (a.icon, not div.icon) for
										Section 508 reasons (correct screen reader behaviors). Moved class names of FontAwesome icons
										from div/a to span, which allowed combining phone/email with 2 spans. (See buttonbar.css.)
					01/07/2016, SRS:	Added 2 more PDFs to Help menu. Made menus "stateful" for greater reliability. Revised this
										comment header and Attributes defaults to include AppName.
					12/16/2015, SMJ:	Added the ability to set theme from the home page.
					12/04/2015, SRS:	Added th1 as an override of default, now called Home Page Theme.
					11/20/2015, SMJ:	Moved MainNav buttons DoExit, DoHelp, and DoPrint functionality to the button bar.
					10/20/2015, SRS:	Original implementation. Cannibalized parts of sbalookandfeel_dtv.cfm.
--->

<!--- If developer codes </cf_sbabuttonbar> (or </cfmodule>), ignore it: --->

<cfif thisTag.ExecutionMode IS "end">
	<cfexit>
</cfif>
<cfsetting enablecfoutputonly="Yes">

<!--- Configuration Parameters, which must match static no-theme default values in /library/css/buttonbar.css: --->

<cfset Variables.DefaultIcons								= "settings,help,messages,printer,email,home,logout">
<cfset Variables.DefaultIconColor							= "">
<cfset Variables.DefaultIconSize							= 24>
<cfset Variables.DefaultOffsetLeft							= 3>
<cfset Variables.DefaultOffsetRight							= 3>
<cfset Variables.DefaultOffsetTop							= 3>
<cfset Variables.DefaultSpacing								= 2>
<cfset Variables.IconWidthFudgeFactor						= 9><!--- Completely baffling as to why a fudge factor is necessary. --->

<!--- Attributes: --->

<cfparam name="Attributes.AppName"							default="">
<cfparam name="Attributes.IconColor"						default="">
<cfparam name="Attributes.IconSize"							default="24">
<cfparam name="Attributes.Indent"							default="    ">
<cfparam name="Attributes.LeftSide"							default="">
<cfparam name="Attributes.LibURL"							default="/library">
<cfparam name="Attributes.OffsetLeft"						default="3">
<cfparam name="Attributes.OffsetRight"						default="3">
<cfparam name="Attributes.OffsetTop"						default="3">
<cfparam name="Attributes.RightSide"						default="">
<cfparam name="Attributes.SLAF"								default="Yes">
<cfparam name="Attributes.Spacing"							default="2">
<cfif NOT IsDefined("Request.SlafEOL")><!--- Will probably already be defined, because Request scope extends into custom tags. --->
	<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbalookandfeel_variables.cfm">
</cfif>
<cfif NOT IsDefined("Request.SlafIPAddrIsOISS")><!--- And it should be, because get_sbalookandfeel_variables calls get_sbashared_variables. --->
	<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbashared_variables.cfm">
</cfif>
<!---
<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
--->
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.GetThemes								= "">
</cflock>

<!--- Calculate Variables-scope stuff based on Attributes-scope inputs: --->

<cfset Variables.TotalIconWidth								= Attributes.IconSize + Attributes.Spacing + Variables.IconWidthFudgeFactor>
<cfif	(Len(Attributes.LeftSide)							is 0)
	and	(Len(Attributes.RightSide)							is 0)>
	<cfset Attributes.RightSide								= Variables.DefaultIcons>
</cfif>

<cfsavecontent variable="Variables.CustomStyleSheet">
	<cfif Attributes.IconColor								is not Variables.DefaultIconColor>
		<cfoutput>
div.buttonbar					div.icon					span.icon
	{/* Make the following !important just for here, in this non-defaults call. (In other words, to override *ALL* themes temporarily.) */
	color:													#Attributes.IconColor# !important;
	}</cfoutput>
	</cfif>
	<cfif Attributes.IconSize								is not Variables.DefaultIconSize>
		<cfoutput>
div.buttonbar					div.icon.spacer
	{
	height:													#Attributes.IconSize#px;</cfoutput>
		<cfif Attributes.Spacing							is not Variables.DefaultSpacing>
			<cfoutput>
	width:													#Attributes.Spacing#px;</cfoutput>
		</cfif>
		<!--- Default font-size 16px is exactly 2/3rds of default height/width 24px. Hence the formula below: --->
		<cfoutput>
	}
div.buttonbar					div.icon:not(.spacer)
	{
	height:													#Attributes.IconSize#px;
	width:													#Attributes.IconSize#px;
	}
body:not(.th6)	div.buttonbar	div.icon:not(.spacer)		span.icon
	{
	font-size:												#Attributes.IconSize#px;
	}
div.buttonbar					div.menu
	{
	top:													#Round((Attributes.IconSize * 11) / 12)#px;
	}
body.th6	div.buttonbar		div.icon
	{/* th6 is Text-Only */
	font-size:												#Round((Attributes.Spacing * 2) / 3)#px;
	}</cfoutput>
	<cfelseif Attributes.Spacing							is not Variables.DefaultSpacing>
		<cfoutput>
div.buttonbar					div.icon.spacer
	{
	width:													#Attributes.Spacing#px;
	}</cfoutput>
	</cfif>
	<cfif Attributes.OffsetTop								is not Variables.DefaultOffsetTop>
		<cfoutput>
div.buttonbar,
div.buttonbar.left,		/* for specificity */
div.buttonbar:not(.left)/* for specificity */
	{
	top:													#Attributes.OffsetTop#px;
	}</cfoutput>
	</cfif>
	<cfif Attributes.OffsetLeft								is not Variables.DefaultOffsetLeft>
		<cfoutput>
div.buttonbar.left
	{
	left:													#Attributes.OffsetLeft#px;
	}</cfoutput>
	</cfif>
	<cfif Attributes.OffsetRight							is not Variables.DefaultOffsetRight>
		<cfoutput>
div.buttonbar:not(.left)
	{
	right:													#Attributes.OffsetRight#px;
	}</cfoutput>
	</cfif>
</cfsavecontent>

<cfset Variables["Indent0"]									= Attributes.Indent>
<cfloop index="i" from="1" to="6"><!--- Only 3 currently needed, however. Not too expensive to plan ahead. --->
	<cfset Variables["Indent#i#"]							= Attributes.Indent & RepeatString("  ", i)>
</cfloop>

<!--- Initializations for External Conditions: --->

<cfset Variables.CLS.CLSAuthorized							= "No">
<cftry>
	<cflock scope="Session" type="ReadOnly" timeout="30">
		<cfif IsDefined("Session.CLS.CLSAuthorized")>
			<cfset Variables.CLS.CLSAuthorized				= Session.CLS.CLSAuthorized>
		</cfif>
	</cflock>
	<cfcatch type="any"><!--- Ignore if user isn't in a session (Scheduled Task directories, for example). ---></cfcatch>
</cftry>

<!--- Check unread messages --->
<cfif Variables.CLS.CLSAuthorized>
	
	<cfparam name="Variables.ImUserNm"							default="" />
	<cfparam name="Variables.db"								default="" />
	<cfparam name="Variables.username"							default="" />
	<cfparam name="Variables.password"							default="" />
		
	<cflock scope="Session" type="ReadOnly" timeout="30">
		<cfif IsDefined("Session.ImUserTbl.ImUserNm")>
			<cfset Variables.ImUserNm				= Session.ImUserTbl.ImUserNm />
		</cfif>
	</cflock>
	
	<cfset Variables.tempdb										= Variables.db />
	<cfset Variables.db											= "oracle_spc_caob1" />

	<cfset Variables.Identifier									= 3 />
	<cfset Variables.CLSMsgToUserNm								= Variables.ImUserNm />
	<cfset Variables.cfprname									= "getUnread" />
	<cfset Variables.LogAct										= "get unread message count" />
	<cfinclude template = "/cfincludes/oracle/securitydocs/spc_CLSMSGSELTSP.cfm">
		
	<cfset Variables.db											= Variables.tempdb />
</cfif>

<cfif IsDefined("getUnread") and IsDefined("getUnread.Count") and getUnread.Count>
	<cfset Variables.unread									= "unread" />
	<cfset Variables.UnreadCount							= getUnread.Count />
<cfelse>
	<cfset Variables.unread									= "" />
	<cfset Variables.UnreadCount							= 0 />
</cfif>	

<!--- AJAX Callback for theme Changes on the home page--->

<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/cls/dsp_home.cfm,/cls/dsp_choosefunction.cfm,/cls/dsp_login.cfm">
	<cfoutput>
<script>
gTrimRE														= /^(\s|\u00A0)+|(\s|\u00A0)+$/g; // Note: "g" prefix means global in prefix notation.
function SlafSetThemeForAllFrames							(pThemeName)
	{
	$.ajax("/library/callbacks/jquery/act_ManagePreferences.jquery.cfm?Action=SetTheme&ThemeName=" + pThemeName,
		{
		accepts:	"text/plain",
		async:		true,
		cache:		false,
		complete:	function(pXHR, pStatus)
						{
						var	sResponseTextTrimmed;
						if	(pStatus !== "success")
							alert("Unable to make theme display persistently. Status: " + pStatus);
						else
							{
							sResponseTextTrimmed			= pXHR.responseText.replace(gTrimRE, "");
							if	(sResponseTextTrimmed === "Success!")
								gSlafThemeName				= pThemeName;
							else
								alert(sResponseTextTrimmed);
							}
						},
		dataType:	"text",
		type:		"GET",
		timeout:	30000 // 30 seconds
		});
	return false;// In menu link, href is bogus. Don't follow it.
	}
</script>

</cfoutput>
</cfcase>
</cfswitch>
<cfif Variables.UnreadCount>
	<cfoutput>
	<style>
		div.buttonbar					span.icon.messages.unread:after
			{
			content: "#Variables.UnreadCount#";
			}
	</style>
	</cfoutput>
</cfif>
<!--- Display the buttonbar: --->
<cfoutput>#Request.SlafEOL#
#Variables.Indent0##Request.SlafTopOfHeadCSSFontAwesome##Request.SlafEOL#
#Variables.Indent0#<link  href="#Request.SlafTopOfHeadLibURL#/css/buttonbar.css?CachedAsOf=2017-02-23T14:06" rel="stylesheet" type="text/css" media="all"/>
#Variables.Indent0#<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/buttonbar.js?CachedAsOf=2017-02-23T21:34"></script>
#Variables.Indent0#<form name="GiveBBMenuControlsAForm" action="javascript:;" onsubmit="return false;"></cfoutput>
<cfset Variables.ButtonBarCount								= 0>
<cfset Variables.ButtonBarName								= "">
<cfloop index="Variables.Side"								list="Left,Right">
	<cfset Variables.SideLC									= LCase(Variables.Side)>
	<cfset Variables.IconsForThisSide						= Attributes[Variables.Side & "Side"]>
	<cfif Len(Variables.IconsForThisSide) is 0>
		<cfcontinue>
	</cfif>
	<cfset Variables.ButtonBarCount							+= 1>
	<cfset Variables.ButtonBarName							= "DivButtonBar#Variables.Side#">
	<cfset Variables.IconIdxHelp							= 0>
	<cfset Variables.IconIdxSettings						= 0>
	<cfoutput>#Request.SlafEOL#
#Variables.Indent0#<div class="buttonbar #Variables.SideLC#"></cfoutput>
	<!--- Generate Requested Icons: --->
	<!--- Generate Requested Icons: --->
	<!--- Generate Requested Icons: --->
	<cfset Variables.IconIdx								= 0>
	<cfloop index="Variables.Icon" list="#Variables.IconsForThisSide#">
		<cfset Variables.IconIdx							= Variables.IconIdx + 1><!--- Don't bump IconIdx unless found by cfcase. --->
		<cfif (Variables.IconIdx gt 1) and (Attributes.Spacing is not "0px")>
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<div class="icon spacer"></div></cfoutput>
		</cfif>
		<cfset Variables.MenuId								= "#Variables.SideLC#_#Variables.Icon#">
		<cfswitch expression="#Variables.Icon#"><!--- cfcases are alphabetical: --->
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="email">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<a class="icon" href="/cls/dsp_contactus.cfm" title="Go to the 'Contact Us' form"
#Variables.Indent1#><span class="icon phone"></span></a></cfoutput>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="help">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<input  id="#Variables.MenuId#" class="menu" type="checkbox" name="BBMenuControl" value="#Variables.MenuId#" style="display:none;">
#Variables.Indent1#<label for="#Variables.MenuId#" class="menu help" tabindex="-1"
#Variables.Indent1#><div class="icon" title="Help Menu"><span class="icon help"></span
#Variables.Indent1#><div class="menu">
#Variables.Indent2#<a class="menuitem" target="_blank" title="(Opens a new window)"
#Variables.Indent3#href="#Attributes.LibURL#/html/sbalookandfeel_help.html"
#Variables.Indent2#>"SBA Look-and-Feel" Help</a></cfoutput>
			<cfswitch expression="#CGI.Script_Name#">
			<cfcase value="/cls/dsp_home.cfm,/cls/dsp_choosefunction.cfm,/cls/dsp_login.cfm">
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/cafs-documentation.pdf"
#Variables.Indent3#title="CAFS Home Instructions (opens a new window)"
#Variables.Indent2#>CAFS Home Instructions</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS AO APPROVAL 21 November 2015FINAL.pdf"
#Variables.Indent3#title="Approving Users (opens a new window)"
#Variables.Indent2#>Instructions for Approving Users</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS Borrower.pdf"
#Variables.Indent3#title="Borrowers (opens a new window)"
#Variables.Indent2#>Instructions for Borrowers</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS AO Account Certification.pdf"
#Variables.Indent3#title="CLS Account Recertification (opens a new window)"
#Variables.Indent2#>Instructions for CLS Account Certification</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS CONTACT FLOW March 2016.pdf"
#Variables.Indent3#title="Customer Support (opens a new window)"
#Variables.Indent2#>Instructions for Customer Support</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS AO 21 November 2015FINAL.pdf"
#Variables.Indent3#title="Lender Authorizing Officials (LAO) Sign Up (opens a new window)"
#Variables.Indent2#>Instructions for Lender Authorizing Officials (LAO) Sign Up</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS_LINC_LENDER_21_February_2017FINAL.pdf"
#Variables.Indent3#title="LINC Sign Up (opens a new window)"
#Variables.Indent2#>Instructions for LINC Sign Up</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS NEW ACCOUNT 21 November 2015FINAL.pdf"
#Variables.Indent3#title="Partner Account Sign Up (opens a new window)"
#Variables.Indent2#>Instructions for Partner Account Sign Up</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS SBA One 19FEB2016.doc.pdf"
#Variables.Indent3#title="SBA One Sign Up (opens a new window)"
#Variables.Indent2#>Instructions for SBA One Sign Up</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS ROLES 21 November 2015FINAL.pdf"
#Variables.Indent3#title="Setting Up Roles (opens a new window)"
#Variables.Indent2#>Instructions for Setting Up Roles</a>
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/cls/help/CAFS Update Profile 21 November 2015FINAL.pdf"
#Variables.Indent3#title="Updating Profile (opens a new window)"
#Variables.Indent2#>Instructions for Updating Profile</a></cfoutput>
			</cfcase>
			<cfdefaultcase>
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" title="Application Help"
#Variables.Indent3#href="##" onclick="BBMenuSelect('HelpClick');"
#Variables.Indent2#><cfif Len(Attributes.AppName) gt 0>#Attributes.AppName#<cfelse>Application</cfif> Help</a></cfoutput>
			</cfdefaultcase>
			</cfswitch>
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#</div><!-- /menu --></div><!-- /icon --></label></cfoutput>
			<cfset Variables.IconIdxHelp					= Variables.IconIdx>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="home">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<a class="icon" href="javascript:BBMenuSelect('Home');" title="Home"><span class="icon home"></span></a></cfoutput>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="logout">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<a class="icon" href="/cls/dsp_logout.cfm" title="Log Out of All CAFS Systems"><span class="icon logout"></span></a></cfoutput>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="messages">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<a class="icon" href="/cls/messages/dsp_messages.cfm" title="Messages"><span class="icon messages #Variables.unread#"></span></a></cfoutput>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="printer">
			<cfswitch expression="#ListFirst(CGI.Script_Name, '/')#">
				<cfcase value="loanborrowsearch,dss1apps,elipsapps,microlender">
					<cfset Variables.PrintJS = "BBMenuSelect('PrintDirect');" />
				</cfcase>
				<cfdefaultcase>
					<cfset Variables.PrintJS = "BBMenuSelect('Print');" />
				</cfdefaultcase>
			</cfswitch>
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<a class="icon" href="javascript:#Variables.PrintJS#" title="Print this page"><span class="icon printer"></span></a></cfoutput>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfcase value="settings">
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<input  id="#Variables.MenuId#" class="menu" type="checkbox" name="BBMenuControl" value="#Variables.MenuId#" style="display:none;">
#Variables.Indent1#<label for="#Variables.MenuId#" class="menu settings" tabindex="-1"
#Variables.Indent1#><div class="icon" title="Settings Menu"><span class="icon settings"></span
#Variables.Indent1#><div class="menu"></cfoutput>
			<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" href="/cls/dsp_profile.cfm" onclick="
#Variables.Indent3#BBMenuHideSettings(); return true;// Follow href.
#Variables.Indent2#">Update Profile</a>
#Variables.Indent2#<a class="menuitem" href="/cls/dsp_chgpass.cfm" onclick="
#Variables.Indent3#BBMenuHideSettings(); return true;// Follow href.
#Variables.Indent2#">Change Password</a>
#Variables.Indent2#<a class="menuitem" href="/cls/dsp_access.cfm" onclick="
#Variables.Indent3#BBMenuHideSettings(); return true;// Follow href.
#Variables.Indent2#">Request Access to CAFS Systems</a></cfoutput>
			<cfif IsDefined("Request.SlafApplicationName")>
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" target="_blank"
#Variables.Indent3#href="/library/callbacks/dsp_preferences.cfm?SessionGroupId=#Hash(UCase(Request.SlafApplicationName))#" onclick="
#Variables.Indent3#BBMenuHideSettings(); return true;// Follow href.
#Variables.Indent2#">Accessibility and Other Preferences</a></cfoutput>
			</cfif>
			<cfif Attributes.SLAF>
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" title="Skip Navigation (moves focus to data entry region of page)" href="##DivAppData" onclick="
#Variables.Indent3#top.document.getElementById('DivAppData').focus();	// Focusable because of the tabindex='-1' hack. See DivAppData.
#Variables.Indent3#BBMenuHideSettings(); return false;					// Setting focus resets tab order, so don't follow JS-off href.
#Variables.Indent2#">Skip Navigation</a></cfoutput>
			</cfif><!--- /Attributes.SLAF --->
			<cfswitch expression="#CGI.Script_Name#">
			<cfcase value="/cls/dsp_home.cfm,/cls/dsp_choosefunction.cfm"><!--- Just pages that COULD have the App menu. Don't bother user with hotlink if inapplicable. --->
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" href="javascript:;" onclick="
#Variables.Indent3#if  (window.SwitchWidths) // In case not logged in.
#Variables.Indent4#window.SwitchWidths('Global');
#Variables.Indent3#BBMenuHideSettings(); return false;// Don't follow href.
#Variables.Indent2#">Change Application Menu (Wide / Traditional)</a></cfoutput>
			</cfcase>
			</cfswitch>
			<cfif Attributes.SLAF and "No"><!--- If we turn this back on, also set higher z-index on div.menu elements to make them stay on-screen. --->
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" href="javascript:;" onclick="
#Variables.Indent3#BBMenuSelect('Show/Hide');
#Variables.Indent2#">Show/Hide Navigation</a></cfoutput>
			</cfif><!--- /Attributes.SLAF and "No" --->
			<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<hr/></cfoutput>
			<!---<cfloop query="Variables.GetThemes">
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<a class="menuitem" href="javascript:;" onclick="
#Variables.Indent3#top.SlafSetThemeForAllFrames('th#Variables.GetThemes.code#');
#Variables.Indent3#BBMenuHideSettings(); return false; // Don't follow href.
#Variables.Indent2#">#Variables.GetThemes.description#</a></cfoutput>
			</cfloop>
			--->
			<cfif Request.SlafIPAddrIsOISS>
				<cfset Variables.WhereAmI					= "#Request.SlafDevTestProd#, "
															& "#Request.SlafServerGroup#, "
															& "#Request.SlafServerName#, "
															& "#Request.SlafServerInstanceName#">
				<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<hr/>
#Variables.Indent2#<a class="menuitem" href="javascript:BBMenuSelect('CFDebug');">Developers-Only Debug: Show/Hide *ALL* (except icons)</a>
#Variables.Indent2#<span class="menuitem">Developers-Only Debug: #Variables.WhereAmI#</span></cfoutput>
				<cfif	(NOT Variables.CLS.CLSAuthorized)
					and	(Request.SlafDevTestProd			is not "Prod")>
					<cfif Attributes.Slaf>
						<cfset Variables.OtherLoginPage		= "dsp_home.cfm">
						<cfset Variables.OtherLoginPageIsNewOld	= "New">
					<cfelse>
						<cfset Variables.OtherLoginPage		= "dsp_login.cfm?Home=Old">
						<cfset Variables.OtherLoginPageIsNewOld	= "Old">
					</cfif>
					<cfoutput>#Request.SlafEOL#
#Variables.Indent2#<hr/>
#Variables.Indent2#<a class="menuitem" href="#Variables.OtherLoginPage#">Dev/Test and OCA Only: Use #Variables.OtherLoginPageIsNewOld# Login Page</a></cfoutput>
				</cfif>
			</cfif><!--- /SlafIPAddrIsOISS --->
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#</div><!-- /menu --></div><!-- /icon --></label></cfoutput>
			<cfset Variables.IconIdxSettings				= Variables.IconIdx>
		</cfcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		<cfdefaultcase>
			<cfoutput>#Request.SlafEOL#
#Variables.Indent1#<div class="icon" title="Unknown Icon Name"><span class="icon unknown"></span></div></cfoutput>
		</cfdefaultcase>
		<!--- --------------------------------------------------------------------------------------------------- --->
		</cfswitch>
	</cfloop>
	<cfoutput>#Request.SlafEOL#
#Variables.Indent0#</div><!-- /class="buttonbar #Variables.SideLC#" --></cfoutput>
</cfloop>
<cfoutput>#Request.SlafEOL#
#Variables.Indent0#</form><!-- /GiveBBMenuControlsAForm --></cfoutput>
<!--- Output a custom StyleSheet to implement the caller's Attribute overrides: --->
<!--- Output a custom StyleSheet to implement the caller's Attribute overrides: --->
<!--- Output a custom StyleSheet to implement the caller's Attribute overrides: --->
<cfif Len(Variables.CustomStyleSheet) gt 0>
	<cfoutput>#Request.SlafEOL#
<style>
/*<cfif Variables.ButtonBarCount is 1>
The #Variables.ButtonBarName# element, above, was<cfelse>
The DivButtonBar elements, above, were</cfif> custom generated, which requires overriding the static CSS in buttonbar.css.
The following override CSS can be saved to a file to take advantage of browser caching.
The new CSS file should be included AFTER the buttonbar.
*/#Variables.CustomStyleSheet#
</style></cfoutput>
</cfif>
<cfsetting enablecfoutputonly="No"><!--- Per cfdocs, balance Yes with No because enablecfoutputonly behaves like a stack. --->

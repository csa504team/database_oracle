<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, for the US Small Business Administration. 
DATE:				Original implementation started 10/28/2009, completed ________. 
DESCRIPTION:		Custom tag to present SBA form elements with proper look-and-feel. 
NOTES:

	The following paragraph was found not to be true!! In fact, $("input[type=date]") DOES find <input type="date">. 
	It's just that, once you get there, this.type is "text". (To find out what was originally coded in the HTML, you 
	have to use this.getAttribute("type") instead.) Therefore, to make this custom tag more HTML 5 compliant, I'm going 
	to have to rewrite it and its associated JavaScripts. I'll probably add the TCJ code at that time. So this is a 
	major "heads up" that this custom tag is going to be reworked. (Don't use yet!! Don't use yet!! Don't use yet!!) 

	If we were to use HTML 5's new <input type="date"> syntax, for example, jQuery 1.3.2 wouldn't find the elements with 
	$("input[type=date]"). Presumably, the browser converts any input type it doesn't recognize to <input type="Text">, 
	and then the DOM doesn't give jQuery the original type attribute. So instead of using <input type="date">, we have to 
	use <input type="Text" data-type="date">. So /library/javascripts/sbaformelt.js instantiates the onchange handlers 
	with $("input[data-type]").etc. 

	Soon this custom tag will make use of "Table Columns in JSON" files ("TCJ files"), which will have the "tcj_" prefix. 
	This will get rid of the need to specify size and maxlength, or even a logical data type that means "Integer" or 
	"Percentage". Also, "NOT NULL" columns will automatically pick up MandOpt="reqd". 

	The Col attribute is what allows the cf_sbaformelt to pick up data validation criteria from the TCJ files. The 
	typical progression of defaults is Col > Name > Id, but Name and Id, if given, override this defaults hierarchy. 
	You would give Name if you didn't want the Col appearing in the View Source. You would give Id if you had multiples 
	occurrences of the same Name and you need to make the Id unique within the page. 

INPUT (Attributes):
	AutoFocus		Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	Class			Optional.	Class attribute for the form element itself. May get other stuff appended, for example, 
								"hielt", Attributes.HiGrp, "rte", etc. 
	Col				MANDATORY.	Database column name.				Used to look up column in a TCJ file, if provided. 
	Direction		Optional.	"vertical" or "horizontal".			Used with "multitags". If not given, checkboxes default 
								to vertical and radio buttons default to horizontal. 
	Height			Optional.	CSS height, with units. For example, "20px", "1.5em", etc. Uses style attribute. 
	HiGrp			Optional.	"Highlighting group" (see /library/javascripts/jquery/HiGrpUtils.js). 
	Id				Optional.	Standard <input id="xxx">.			If given, passed through to the form element as-is. 
	Indent			Optional.	Indention prepended to each line generated. Allows this custom tag to call itself and 
								have the inner form element indented relative to the outer form element. 
	IndentChars		Optional.	Characters to be used for indentation. The default is "  " (two spaces). 
	Label			MANDATORY.	The English name of the field.		This name will also appear in data validation alerts. 
								(Note: For now, it's English, but there's nothing to prevent it from being Spanish.) 
	List			Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	MandOpt			Optional.	"mand", "opt", "reqd" (new) or "view". If not given, defaults to "opt" (optional). 
								The difference between "mand" and "reqd" is that, if a "reqd" field isn't given, the form 
								will not submit. An empty "mand" field will submit, but the application won't validate. 
								Use "reqd" for columns that are defined as "NOT NULL" on the database. 
	MapIndex		Optional.	Index into Request.MapData array containing information about the current database column. 
	Max				Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	MaxLength		Optional.	Standard <input maxlength="xxx">.	If given, passed through to the form element as-is. 
	Min				Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	NoValidate		Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	Name			Optional.	Standard <input name="xxx">.		If given, passed through to the form element as-is. 
	Placeholder		Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	Query			Optional.	[Usually ServerCached]Query name.	If given, may be used to generate <option> tags, e.g. 
	RowClass		Optional.	class="tbl_row xxx", may be used for show and hide logic or alternating background colors. 
	Size			Optional.	Standard <input size="xxx">.		If given, passed through to the form element as-is. 
	Step			Optional.	HTML 5. Passed through as-is.		(Implemented by JavaScript if browser isn't HTML 5.) 
	Style			Optional.	Standard <input style="xxx">.		May have height and width appended to it. 
	TabIndex		Optional.	Standard <input tabindex="xxx">.	If given, passed through to the form element as-is. 
	Title			Optional.	Prepends to generated tooltip. Generally not needed. 
	Toolbar			Optional.	Rich text editor's toolbar name.	("Basic", "Default", "SBABasic" or "SBADefault") 
	Type			MANDATORY.	Physical data type ("Checkbox", "Radio", "Text", etc) or logical data type. Currently, 
								the only logical data types supported are "Date", "State", "TaxId" and "Zip". 
								If Attributes.Type is on the list of supported logical data types, then 
								<input type="xxx" data-type="yyy" ... etc> will be whatever we need them to be to support 
								validation of that logical data type. Otherwise (if it isn't on the list of supported 
								logical data types), it's assumed to be a physical data type and passed through as-is to 
								<input type="whatever">. 
	Value			Optional.	(Although you might think it should be mandatory.) 
	Visible			Optional.	Used with hidden fields. Gives the id of a span to be used to visually represent the 
								contents of the hidden field. 
	Width			Optional.	CSS with, with units. For example, "80px", "15em", etc. Uses style attribute. 
INPUT (Request scope variables):
	SfeActNoUpdate	Optional.	Action page ("act_") to use if no update is required. If not the nullstring, this causes 
								the generation of "Old_" hidden elements to detect what screen elements have changed. If 
								none have changed, the generated onsubmit script will change the action attribute of the 
								form to the URL in SfeActNoUpdate and return true. Used for "I/O avoidance". May also be 
								used in "act_" pages to cfinclude or cflocation to the page if no updates detected there, 
								in case JavaScript is off. 
	SfeDebug		Optional.	Puts up buttons to dump form elements and their associated jQuery objects. 
	SfeIndent		Optional.	If Attributes.Indent is the nullstring, SfeIndent is the default. If SfeIndent is not 
								defined, then the default is the nullstring. Used to set a global default. 
	SfeIndentChars	Optional.	If Attributes.IndentChars is the nullstring, SfeIndentChars is the default. If SfeIndentChars 
								is not defined, then the default is the "  " (two spaces). Used to set a global default. 
	SfeParent		Optional.	NEW: If this is defined and has length greated than 0, it means that cf_sbaformelt is being 
								called as a "subtag" of another custom tag (like cf_sbatreeitem is a subtag of cf_sbatree). 
								When that's true, cf_sbaformelt DOES NOT OUTPUT THE FORM ELEMENT WITH CFOUTPUT. Instead, 
								it just passes back the form element to the parent tag. 
	SfeRowClass		Optional.	Default "". Similar to Attributes.RowClass. Used to set a global default. 
	SfeSeqNmb		Automatic.	Default 0, bumped up by 1 on each call. 
	SfeTaxIdPrefixEIN			Overridable configuration parameter. If a Tax ID is 10 digits, and the first digit is this 
								character, it will be formatted as an Employee Identification Number (EIN). Not yet in use. 
	SfeTaxIdPrefixSSN			Overridable configuration parameter. If a Tax ID is 10 digits, and the first digit is this 
								character, it will be formatted as a Social Security Number (SSN). Not yet in use. 
	Slaf variables	Mandatory.	This file assumes that the caller has included get_sbashared_variables (or sbalookandfeel). 
	TcjData			Optional.	"Table Columns in JSON" derived data (in ColdFusion struct format). Top level is Oracle 
								schema name or Sybase database name. Within those structures, next level is table name. 
								Final level is column name. Generated by cfincluding TCJ files of all tables for which 
								TCJ validation is required. 
	TcjSchemaName	Optional.	String. Oracle schema name or Sybase database name, used to look up Attributes.Col. 
								If nullstring and TcjData contains more than one schema name, no lookup occurs. 
	TcjTableName	Optional.	String. Table name, used to look up Attributes.Col. 
								If nullstring and TcjData[TcjSchemaName] contains more than one table, no lookup occurs. 
OUTPUT:				HTML, Request.SfeMightNotGetSent (accumulated, useful when done). 
REVISION HISTORY:	04/08/2014, SRS:	Took another snapshot with check-in/check-out to rework/rethink how this custom tag 
										does its job. The previous attempt was using "map files" and SfeSameLineMode. 
										Not removing that code just yet, but instead adding supertag/subtag capability via 
										Request.SfeParent mechanism. The intent is to reverse the direction of the map 
										(generate the map from the calls, not the other way around). See SfeParent and 
										all code that references SfeParent. 
					10/27/2010, SRS:	Took snapshot with check-in/check-out to rework/rethink how this custom tag does 
										its job. Need to have a base-level custom tag (this one) that generates ONLY the 
										label and form element, so that other, more physical-view-oriented custom tags 
										can position those outputs in different ways (horizontally, vertically, etc) 
										within the page. Also began adding TCJ data validation logic. Added detection of 
										Request.SfeActNoUpdate (see explanation, above). 
					02/25/2010, SRS:	Original implementation. 
--->

<!--- If user codes </cf_sbaformelt> (or </cfmodule>), ignore it: --->

<cfif thisTag.ExecutionMode IS "end">
	<cfexit>
</cfif>

<!--- Configuration Parameters: --->

<cfset Variables.CrashMessage						= Request.SlafEOL
													& "INTERNAL ERROR: Please report the following error to the "
													& "SBA's Office of Capital Access. "
													& Request.SlafEOL
													& "The following information may help: ">
<cfset Variables.RequiredFieldMsg					= "Required field. Form will not submit without it. ">

<!--- Attributes: --->

<cfparam name="Attributes.Col"						default=""><!--- MANDATORY --->
<cfparam name="Attributes.Label"					default=""><!--- MANDATORY --->
<cfparam name="Attributes.Type"						default=""><!--- MANDATORY --->

<cfparam name="Attributes.Autofocus"				default="No">
<cfparam name="Attributes.Class"					default="">
<cfparam name="Attributes.Direction"				default="">
<cfparam name="Attributes.HiGrp"					default="">
<cfparam name="Attributes.Height"					default="">
<cfparam name="Attributes.Help"						default="">
<cfparam name="Attributes.Id"						default="">
<cfparam name="Attributes.Indent"					default="">
<cfparam name="Attributes.IndentChars"				default="">
<cfparam name="Attributes.List"						default="">
<cfparam name="Attributes.MandOpt"					default="">					<!--- Defaulted to "opt", below. --->
<cfparam name="Attributes.Max"						default="">
<cfparam name="Attributes.MaxLength"				default="">					<!--- Default is ** NO MAX LENGTH **. --->
<cfparam name="Attributes.Min"						default="">
<cfparam name="Attributes.Name"						default="">
<cfparam name="Attributes.NotSelectedText"			default="Not Yet Selected">	<!--- select-one and select-multiple --->
<cfparam name="Attributes.NoValidate"				default="No">
<cfparam name="Attributes.Options"					default="">					<!--- select-one and select-multiple --->
<cfparam name="Attributes.Placeholder"				default="">
<cfparam name="Attributes.Query"					default="">					<!--- select-one and select-multiple --->
<cfparam name="Attributes.RowClass"					default="">
<cfparam name="Attributes.ShowCode"					default="No">				<!--- select-one and select-multiple --->
<cfparam name="Attributes.SkipList"					default="">					<!--- select-one and select-multiple --->
<cfparam name="Attributes.Size"						default="">
<cfparam name="Attributes.Step"						default="">
<cfparam name="Attributes.Style"					default="">
<cfparam name="Attributes.TabIndex"					default="">
<cfparam name="Attributes.Title"					default="">					<!--- Use Attributes.Help for inline help. --->
<cfparam name="Attributes.Toolbar"					default="SBADefault">		<!--- rte --->
<cfparam name="Attributes.Title"					default="">
<cfparam name="Attributes.Value"					default="">
<cfparam name="Attributes.Visible"					default="">
<cfparam name="Attributes.Width"					default="">
<!---
The "map" is intended to be used both in the dsp page (where this custom tag is called) and in the act page (where it isn't). 
That way, we build act page validation error messages in dsp page order, and they will represent the exact same validation 
criteria as on the dsp page. Therefore, we cannot allow Attributes to override the map. If we did, it would be possible for 
the dsp and act pages to get out of sync. That's why, if present, the map overrides Attributes. If you want to override map 
elements, you have to do so by altering the map, usually by editing the "map_" file manually. If you want to change something 
without editing the "map_" file, you could also change the contents of Request.MapData after it's been built. 
--->
<cfset Variables.IsThisAColumn						= "No">
<cfset Variables.MapStructForColumn					= StructNew()>
<cfif	IsDefined("Attributes.MapIndex")
	and	IsDefined("Request.MapData")>
	<cfif IsStruct(Request.MapData[Attributes.MapIndex][2])><!--- (Crash if Attributes.MapIndex is invalid.) --->
		<cfset Variables.MapStructForColumn			= Request.MapData[Attributes.MapIndex][2]>
		<cfset StructAppend(Attributes, Variables.MapStructForColumn, "Yes")><!--- "Yes  = overwrite existing keys.--->
	</cfif>
	<cfif Request.MapData[Attributes.MapIndex][1]	is "column">
		<cfset Variables.IsThisAColumn				= "Yes">
	</cfif>
</cfif>

<!--- The following are external because they're used between calls (or across multiple calls) to cf_sbaformelt: --->

<cfparam name="Request.SfeActNoUpdate"				default="">
<cfparam name="Request.SfeDateSeqNmb"				default=0><!--- Always used numerically, so don't quote this zero. --->
<cfparam name="Request.SfeDebug"					default="No">
<cfparam name="Request.SfeMightNotGetSent"			default=""><!--- Output element, useful after ProcessMapDataFormElts. --->
<cfparam name="Request.SfeParent"					default="">
<cfparam name="Request.SfeRowClass"					default="">
<cfparam name="Request.SfeSeqNmb"					default=0><!--- Always used numerically, so don't quote this zero. --->
<cfset		   Request.SfeSeqNmb					= Request.SfeSeqNmb + 1>
<cfparam name="Request.SfeTaxIdPrefixEIN"			default="0">
<cfparam name="Request.SfeTaxIdPrefixSSN"			default="1">
<cfparam name="Request.TcjSchemaName"				default=""><!--- Always exists, but not always used. --->
<cfparam name="Request.TcjTableName"				default=""><!--- Always exists, but not always used. --->

<!--- Initialize Layout Variables: --->

<cfinclude template="/library/cfincludes/sbalookandfeel/bld_SBAFormEltLayoutVars.cfm">

<!--- Overrides of Attributes: --->

<cfif Len(Attributes.Name) is 0>
	<cfset Attributes.Name							= Attributes.Col>
</cfif>
<cfif Len(Attributes.Name) gt 0>
	<cfif ListFindNoCase(Request.SfeMightNotGetSent, Attributes.Name) is 0>
		<cfset Request.SfeMightNotGetSent			= ListAppend(Request.SfeMightNotGetSent, Attributes.Name)>
	</cfif>
</cfif>
<cfif Len(Attributes.Id) IS 0>
	<cfset Attributes.Id							= Attributes.Name>
</cfif>
<cfset Variables.Type								= LCase(Attributes.Type)><!--- Save on calls to LCase. --->

<cfif Len(Attributes.Col) is 0>
	<cfoutput>#Variables.CrashMessage#
&lt;cf_sbaformelt&gt; was called without the "col" attribute, 
and col could not be inferred from the database.</cfoutput>
	<cfabort>
<cfelseif Len(Attributes.Label) is 0>
	<cfoutput>#Variables.CrashMessage#
&lt;cf_sbaformelt&gt; was called without the "label" attribute, 
and label could not be inferred from the database.</cfoutput>
	<cfabort>
<cfelseif Len(Variables.Type) is 0>
	<cfoutput>#Variables.CrashMessage#
&lt;cf_sbaformelt&gt; was called without the "type" attribute, 
and type could not be inferred from the database.</cfoutput>
	<cfabort>
</cfif>

<!--- Now that Variables.Type is determined, infer not-given Attributes, many of which depend on Variables.Type: --->

<cfset Variables.TypeAllowsUserToChangeValue		= (Len(Request.SfeActNoUpdate) gt 0)><!--- Used in I/O avoidance. --->

<!--- Class: --->

<cfif Variables.Type is "rte">
	<cfset Attributes.Class							= ListAppend(Attributes.Class, "rte", " ")>
</cfif>
<cfif Len(Attributes.HiGrp) gt 0>
	<cfset Attributes.Class							= ListAppend(Attributes.Class, "hielt #Attributes.HiGrp#", " ")>
</cfif>

<cfif Len(Attributes.Direction) is 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="checkbox">						<cfset Attributes.Direction	= "vertical">	</cfcase>
	<cfcase value="radio">							<cfset Attributes.Direction	= "horizontal">	</cfcase>
	</cfswitch>
</cfif>

<!--- Height/Width: --->

<cfset Variables.Options							= ""><!--- In case "select-one" or "select-multiple". --->
<cfswitch expression="#Variables.Type#">
<cfcase value="textarea,rte">
	<cfif Len(Attributes.Height)					IS 0>
		<cfset Attributes.Height					= "300px"><!--- A visually pleasing height. --->
	</cfif>
	<cfif Len(Attributes.Width)						IS 0>
		<cfif CompareNoCase(Attributes.Toolbar, "SBABasic") IS 0>
			<cfset Attributes.Width					= "340px"><!--- Just enough room for the basic toolbar. --->
		<cfelse>
			<cfset Attributes.Width					= "420px"><!--- Just enough room for the default toolbar. --->
		</cfif>
	</cfif>
</cfcase>
<cfcase value="select-one,select-multiple">
	<cfif Len(Attributes.Options)					GT 0>
		<cfset Variables.Options					= Attributes.Options>
	<cfelseif Len(Attributes.Query)					GT 0>
		<cfif		FindNoCase("Variables.",		Attributes.Query) GT 0>
			<cfset Variables.DspOptsQueryName		= ReplaceNoCase(Attributes.Query, "Variables.", "Caller.", "One")>
		<cfelseif	FindNoCase("Request.",			Attributes.Query) GT 0>
			<cfset Variables.DspOptsQueryName		= Attributes.Query>
		<cfelse>
			<cfset Variables.DspOptsQueryName		= "Caller." & Attributes.Query>
		</cfif>
		<cfif IsQuery(Evaluate(Variables.DspOptsQueryName))>
			<!--- No override for "code" and "description", because they're used throughout ServerCachedQueries. --->
			<cfset Variables.DspOptsNotSelText		= Attributes.NotSelectedText>
			<cfset Variables.DspOptsShowCode		= Attributes.ShowCode>
			<cfset Variables.DspOptsSelList			= Attributes.Value>
			<cfset Variables.DspOptsSkipList		= Attributes.SkipList>
			<cfset Variables.DspOptsTabs			= Variables.IndentPlus3>
			<cfsavecontent variable="Variables.Options"
			><cfinclude template="/library/cfincludes/dsp_options.cfm"
			></cfsavecontent>
			<cfset Variables.Options				= Replace(Variables.Options, "</option>", "</option>#Request.SlafEOL#", "ALL")>
		<cfelse>
			<cfset Variables.Options				= Request.SlafEOL
													& '<option value="">"#Attributes.Query#" is not a query.</option>'
													& Request.SlafEOL>
		</cfif>
	<cfelse>
		<!--- Not required to populate options in cf_sbaformelt call. Allowed to populate by AJAX, for example. --->
	</cfif>
</cfcase>
</cfswitch>

<!--- MandOpt: --->

<cfset Attributes.MandOpt							= LCase(Attributes.MandOpt)>
<cfswitch expression="#Attributes.MandOpt#">
<cfcase value="mand,opt,reqd,view"></cfcase><!--- Idea: Also allow "hide" and "none". --->
<cfdefaultcase>
	<cfset Attributes.MandOpt						= "opt">
</cfdefaultcase>
</cfswitch>

<!--- Max/Min: --->

<cfif Len(Attributes.Max) is 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="datenonfuture">					<cfset Attributes.Max		= "today"></cfcase>
	<cfcase value="int">							<cfset Attributes.Max		= "2147483647"></cfcase>
	<cfcase value="intunsigned">					<cfset Attributes.Max		= "4294967295"></cfcase>
	<cfcase value="smallint">						<cfset Attributes.Max		= "32767"></cfcase>
	<cfcase value="smallintunsigned">				<cfset Attributes.Max		= "65535"></cfcase>
	<cfcase value="tinyint">						<cfset Attributes.Max		= "255"></cfcase>
	<cfcase value="tinyintsigned">					<cfset Attributes.Max		= "127"></cfcase>
	</cfswitch>
</cfif>
<cfif Len(Attributes.Min) is 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="int">							<cfset Attributes.Min		= "-2147483648"></cfcase>
	<cfcase value="intunsigned">					<cfset Attributes.Min		= "0"></cfcase>
	<cfcase value="smallint">						<cfset Attributes.Min		= "-32768"></cfcase>
	<cfcase value="smallintunsigned">				<cfset Attributes.Min		= "0"></cfcase>
	<cfcase value="tinyint">						<cfset Attributes.Min		= "0"></cfcase>
	<cfcase value="tinyintsigned">					<cfset Attributes.Min		= "-128"></cfcase>
	</cfswitch>
</cfif>

<!--- MaxLength and Size (which is inferred from MaxLength only if MaxLength was actually given): --->

<cfswitch expression="#Variables.Type#">
<cfcase value="checkbox,hidden,lookup,radio">
	<cfset Variables.AppendMaxLength				= "No">
	<cfset Variables.AppendSize						= "No">
</cfcase>
<cfcase value="select-one,select-multiple">
	<cfset Variables.AppendMaxLength				= "No">
	<cfset Variables.AppendSize						= "Yes"><!--- To dropdowns, "size" is how many rows are visible. --->
</cfcase>
<cfdefaultcase>
	<cfset Variables.AppendMaxLength				= "Yes">
	<cfset Variables.AppendSize						= "Yes">
</cfdefaultcase>
</cfswitch>

<cfif Len(Attributes.MaxLength) is 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="date,datenonfuture">				<cfset Attributes.MaxLength	= 10></cfcase>
	<cfcase value="int,intunsigned">				<cfset Attributes.MaxLength	= 14></cfcase>
	<cfcase value="money,moneysigned">				<cfset Attributes.MaxLength	= 20></cfcase>
	<cfcase value="percent">						<cfset Attributes.MaxLength	= 8></cfcase>
	<cfcase value="phone">							<cfset Attributes.MaxLength	= 16></cfcase>
	<cfcase value="smallint,smallintunsigned">		<cfset Attributes.MaxLength	= 7></cfcase>
	<cfcase value="state">							<cfset Attributes.MaxLength	= 2></cfcase>
	<cfcase value="taxid">							<cfset Attributes.MaxLength	= 11></cfcase>
	<cfcase value="tinyint">						<cfset Attributes.MaxLength	= 3></cfcase>
	<cfcase value="tinyintsigned">					<cfset Attributes.MaxLength	= 4></cfcase>
	<cfcase value="zip">							<cfset Attributes.MaxLength	= 5></cfcase>
	</cfswitch>
<cfelseif Attributes.MaxLength is 10><!--- Special case. Already defined due to TCJ file, but not big enough. --->
	<cfswitch expression="#Variables.Type#"><!--- Add room for parens, hyphens and/or spaces: --->
	<cfcase value="phone">							<cfset Attributes.MaxLength	= 16></cfcase>
	</cfswitch>
</cfif>

<cfif Len(Attributes.Size) is 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="select-one,select-multiple"></cfcase><!--- Don't set dropdown sizes based on maxlength. --->
	<cfdefaultcase>
		<cfif Attributes.MaxLength gt 40>
			<cfset Attributes.Size					= 40>
		<cfelse>
			<cfset Attributes.Size					= Attributes.MaxLength>
		</cfif>
	</cfdefaultcase>
	</cfswitch>
</cfif>

<cfif Len(Attributes.Size) IS 0><!--- That is, if it's STILL 0, because MaxLength was inferred. --->
	<cfswitch expression="#Variables.Type#">
	<cfcase value="date,datenonfuture">				<cfset Attributes.Size		= 10></cfcase>
	<cfcase value="int,intunsigned">				<cfset Attributes.Size		= 14></cfcase>
	<cfcase value="money,moneysigned">				<cfset Attributes.Size		= 20></cfcase>
	<cfcase value="select-one">						<cfset Attributes.Size		= 1></cfcase>
	<cfcase value="select-multiple">
		<!--- Base the size of the dropdown on QueryName's RecordCount, if available, else don't even specify size: --->
		<cfif Len(Attributes.QueryName) GT 0>
			<cfif IsDefined("#Attributes.QueryName#.RecordCount")>
				<cfset Variables.RecCnt				= Evaluate("#Attributes.QueryName#.RecordCount")>
				<cfif Variables.RecCnt GE 9>
					<cfset Variables.Size			= 10>
				<cfelse>
					<cfset Variables.Size			= IncrementValue(Variables.RecCnt)>
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	<cfcase value="smallint,smallintunsigned">		<cfset Attributes.Size		= 7></cfcase>
	<cfcase value="percent">						<cfset Attributes.Size		= 8></cfcase>
	<cfcase value="phone">							<cfset Attributes.Size		= 16></cfcase>
	<cfcase value="state">							<cfset Attributes.Size		= 2></cfcase>
	<cfcase value="taxid">							<cfset Attributes.Size		= 11></cfcase>
	<cfcase value="tinyint">						<cfset Attributes.Size		= 3></cfcase>
	<cfcase value="tinyintsigned">					<cfset Attributes.Size		= 4></cfcase>
	<cfcase value="zip">							<cfset Attributes.Size		= 5></cfcase>
	</cfswitch>
</cfif>

<!--- Style, which may use height/width inferred above: --->

<cfif Len(Attributes.Height) gt 0>
	<cfset Attributes.Style							= ListAppend(Attributes.Style, "height:#Attributes.Height#;", " ")>
</cfif>
<cfif Len(Attributes.Width) gt 0>
	<cfset Attributes.Style							= ListAppend(Attributes.Style, "width:#Attributes.Width#;", " ")>
</cfif>

<!--- Title (generated content appends to existing Attributes.Title, if any): --->

<cfset Variables.GenerateHelpIcon					= "No">
<cfset Variables.GenerateHelpText					= "No">
<cfswitch expression="#Variables.Type#">
<cfcase value="date,datenonfuture,email,int,intunsigned,money,moneysigned,number,percent,phone,smallint,smallintunsigned,state,taxid,tinyint,tinyintsigned,zip">
	<cfset Variables.GenerateHelpText				= "Yes">
</cfcase>
<cfdefaultcase>
	<cfif Attributes.MandOpt is "reqd">
		<cfset Variables.GenerateHelpText			= "Yes">
	</cfif>
</cfdefaultcase>
</cfswitch>
<cfif Variables.GenerateHelpText>
	<cfif Len(Attributes.Title) GT 0>
		<cfif Right(Attributes.Title,1)				is not " "><!--- If there's more stuff, add a spacer: --->
			<cfset Attributes.Title					&= " ">
		</cfif>
	</cfif>
	<cfif Attributes.MandOpt IS "reqd">
		<cfset Attributes.Title						&= Variables.RequiredFieldMsg>
	</cfif>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="date,datenonfuture">
		<cfset Attributes.Title						&= "Must be in m/d/yy, m/d/19yy, m/d/20yy, yy-m-d, 19yy-m-d, "
													& "20yy-m-d, 19yymmdd or 20yymmdd format. "
													& "2-digit years will be inferred to be in the range 1953-2052. ">
		<cfif Compare(Variables.Type, "datenonfuture") IS 0>
			<cfset Attributes.Title					&= "This date cannot be in the future. ">
		</cfif>
		<cfset Attributes.Title						&= "If found valid, date will be converted to mm/dd/yyyy format. ">
	</cfcase>
	<cfcase value="email">
		<cfset Attributes.Title						&= "Must be letters, digits, underscores, periods and/or dashes, "
													& "followed by exactly one '@', "
													& "followed by letters, digits, periods and/or dashes. ">
	</cfcase>
	<cfcase value="int">
		<cfset Attributes.Title						&= "Must be a number in the range of -2,147,483,648 to 2,147,483,647. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="intunsigned">
		<cfset Attributes.Title						&= "Must be a number in the range of 0 to 4,294,967,295. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="number">
		<cfset Attributes.Title						&= "Must be a positive number without a fractional part. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="smallint">
		<cfset Attributes.Title						&= "Must be a number in the range of -32,768 to 32,767. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="smallintunsigned">
		<cfset Attributes.Title						&= "Must be a number in the range of 0 to 65,535. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="tinyint">
		<cfset Attributes.Title						&= "Must be a number in the range of 0 to 255. ">
	</cfcase>
	<cfcase value="tinyintsigned">
		<cfset Attributes.Title						&= "Must be a number in the range of -128 to 127. ">
	</cfcase>
	<cfcase value="money">
		<cfset Attributes.Title						&= "Must be in the following format and order: "
													& "optional $ sign, any combination of digits and commas "
													& "and finally optional decimal point and 2 digits. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="moneysigned">
		<cfset Attributes.Title						&= "Must be in the following format and order: optional +/- sign, "
													& "optional $ sign, any combination of digits and commas "
													& "and finally optional decimal point and 2 digits. "
													& "If found valid, will be normalized to 'floating commas' format. ">
	</cfcase>
	<cfcase value="percent">
		<cfset Attributes.Title						&= "If not 100%, must be at most 2 digits before the decimal point, "
													& "optionally followed by decimal point and 1-3 digits, "
													& "optionally followed by percent sign. ">
	</cfcase>
	<cfcase value="phone">
		<cfset Attributes.Title						&= "Must be in 999-999-9999 or (999) 999 - 9999 format. "
													& "Actually, any set of 3 digits, 3 digits and 4 digits, "
													& "with optional non-digit separators, is acceptable. "
													& "If found valid, will be converted to 999-999-9999. ">
	</cfcase>
	<cfcase value="state">
		<cfset Attributes.Title						&= "Must be one of the 59 US Postal Service 2-letter state codes: "
													& "AL, AK, AS, AZ, AR, CA, CO, CT, DE, DC, FL, GA, GU, HI, ID, "
													& "IL, IN, IA, KS, KY, LA, ME, MH, MD, MA, MI, FM, MN, MS, MO, "
													& "MT, MP, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PW, PA, "
													& "PR, RI, SC, SD, TN, TX, UT, VT, VI, VA, WA, WV, WI or WY. ">
	</cfcase>
	<cfcase value="taxid">
		<cfset Attributes.Title						&= "Must be in 999-99-9999 (SSN) or 99-9999999 (EIN) format. ">
	</cfcase>
	<cfcase value="zip4">
		<cfset Attributes.Title						&= "If given, must be a 4-digit number.">
	</cfcase>
	<cfcase value="zip5">
		<cfset Attributes.Title						&= "Must be a 5-digit number.">
	</cfcase>
	</cfswitch>
</cfif>

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

<!--- Begin generating the form element tag: --->

<cfset Variables.Tag								= "">

<cfswitch expression="#Variables.Type#">
<cfcase value="date,email,money,phone,state,taxid,text,zip,zip5,zip4">
	<cfset Tag										&= '<input type="Text" data-type="#Variables.Type#" '>
</cfcase>
<cfcase value="checkbox">
	<cfset Tag										&= "<label>#Request.SlafEOL & Variables.IndentChars#"
													& '<input type="checkbox" data-type="chk" '>
</cfcase>
<cfcase value="datenonfuture">
	<cfset Tag										&= '<input type="text" data-type="date" '>
</cfcase>
<cfcase value="int,smallint,tinyintsigned">
	<cfset Tag										&= '<input type="text" data-type="number" '>
</cfcase>
<cfcase value="intunsigned,number,smallintunsigned,tinyint">
	<cfset Tag										&= '<input type="text" data-type="numbun" '>
</cfcase>
<cfcase value="lookup">
	<cfset Tag										&= '<input type="button" '><!--- No validation. --->
</cfcase>
<cfcase value="moneysigned">
	<cfset Tag										&= '<input type="text" data-type="monys" '>
</cfcase>
<cfcase value="percent">
	<cfset Tag										&= '<input type="text" data-type="pct" '>
</cfcase>
<cfcase value="radio">
	<cfset Tag										&= "<label>#Request.SlafEOL & Variables.IndentChars#"
													& '<input type="radio" data-type="rad" '>
</cfcase>
<cfcase value="rte">
	<cfset Tag										&= '<textarea data-type="rte" rtetoolbar="#Attributes.Toolbar#" '>
</cfcase>
<cfcase value="select-one">
	<cfset Tag										&= '<select data-type="select" '>
</cfcase>
<cfcase value="select-multiple">
	<cfset Tag										&= '<select data-type="select" multiple '>
</cfcase>
<cfcase value="textarea">
	<cfset Tag										&= '<textarea data-type="textarea" '>
</cfcase>
<cfdefaultcase>
	<cfset Tag										&= '<input type="#Variables.Type#" '>
</cfdefaultcase>
</cfswitch>

<cfif Attributes.MandOpt IS "reqd">
	<cfset Tag										&= "required "><!--- Boolean (HTML 5 requires no value). --->
</cfif>
<cfif Variables.AppendMaxLength						and (Len(Attributes.MaxLength)	gt 0)>
	<cfset Variables.Tag							&= 'maxlength="#Attributes.MaxLength#" '>
</cfif>
<cfif Variables.AppendSize							and (Len(Attributes.Size)		gt 0)>
	<cfset Variables.Tag							&= 'size="#Attributes.Size#" '>
</cfif>
<cfif Attributes.Autofocus>
	<cfset Variables.Tag							&= "autofocus "><!--- Boolean (HTML 5 requires no value). --->
</cfif>
<cfif Attributes.NoValidate>
	<cfset Variables.Tag							&= "novalidate "><!--- Boolean (HTML 5 requires no value). --->
</cfif>
<cfif Len(Attributes.List)							gt 0>
	<cfset Variables.Tag							&= 'list="#Attributes.List#" '>
</cfif>
<cfif Len(Attributes.Placeholder)					gt 0>
	<cfset Variables.Tag							&= 'placeholder="#Attributes.Placeholder#" '>
</cfif>

<cfswitch expression="#Variables.Type#">
<cfcase value="checkbox,radio"><!--- New lines and indention controlled in the "MultiTag loop", below. ---></cfcase>
<cfcase value="lookup">
	<cfset Variables.MapStructForColumnJSON			= Replace(SerializeJSON(Variables.MapStructForColumn),"""", "'", "ALL")>
	<cfset Variables.Tag
		&= 'name="JustAButton" value="Lookup" onclick="window.sba.lookup(this, '&Variables.MapStructForColumnJSON&');">'>
	<cfset Variables.TypeAllowsUserToChangeValue	= "No"><!--- No "Old_" hidden field for lookups. --->
</cfcase>
<cfdefaultcase>
	<cfset Variables.Tag							&= Request.SlafEOL
													& 'name="#Attributes.Name#" id="#Attributes.Id#" '>
</cfdefaultcase>
</cfswitch>

<cfif Len(Attributes.Class) gt 0>
	<cfset Variables.Tag							&= 'class="#Attributes.Class#" '>
</cfif>
<cfif Len(Attributes.Style) gt 0>
	<cfset Variables.Tag							&= 'style="#Attributes.Style#" '>
</cfif>
<cfif (Attributes.MandOpt IS "view")				and (Variables.Type is not "hidden")>
	<cfset Variables.Tag							&= "readonly ">
</cfif>
<cfif Len(Attributes.Min)							gt 0>
	<cfset Variables.Tag							&= 'min="#Attributes.Min#" '>
</cfif>
<cfif Len(Attributes.Step)							gt 0>
	<cfset Variables.Tag							&= 'step="#Attributes.Step#" '>
</cfif>
<cfif Len(Attributes.Max)							gt 0>
	<cfset Variables.Tag							&= 'max="#Attributes.Max#" '>
</cfif>

<cfif Len(Attributes.Title) gt 0>
	<cfswitch expression="#Variables.Type#">
	<cfcase value="checkbox,radio"><!--- New lines and indention controlled in the "MultiTag loop", below. --->
		<cfset Variables.Tag						&= Request.SlafEOL & Variables.IndentChars
													& 'title="#HTMLEditFormat(Attributes.Title)#" '>
	</cfcase>
	<cfdefaultcase>
		<cfset Variables.Tag						&= Request.SlafEOL
													& 'title="#HTMLEditFormat(Attributes.Title)#" '>
	</cfdefaultcase>
	</cfswitch>
</cfif>

<!--- We're almost done. Generate value last as we close the tag, each in its own manner: --->

<cfswitch expression="#Variables.Type#">
<cfcase value="checkbox,radio">
	<!---
	Checkboxes and radio buttons are "multitags". There are 2 mechanisms to define their values and labels: LISTS or QUERY. 
	(1)	LISTS: To use the LISTS mechanism, define the map with 2 comma delimited lists (labels and values), which must be 
		the same length. (Actually, labels can be longer, and the excess is ignored.) Both list names are plural, so that 
		we can distinguish Values (list of all possible values) from Value (list of values to get the "checked" attribute). 
		We loop on generating the same tag over and over, with the only differences being: 
			Value	The list loop's index. 
			Id		Inferred from Name with 1, 2, 3, etc, appended to the end of it. 
			Checked	Inferred from comparing current value to the list in Attributes.Value (singular). 
			Label	Which follows the tag. (Checkboxes and radio buttons use the <label> tag and label </label> syntax.) 
	(2)	QUERY: To use the QUERY mechanism, define the map with a query name, just as select-one and select-multiple 
		elements do. In that case, we also loop on generating the same tag over and over, with: 
			Value	From ((query name)).code 
			Id		Inferred from Name with 1, 2, 3, etc, appended to the end of it. 
			Checked	Inferred from comparing current value to the list in Attributes.Value (singular). 
			Label	From ((query name)) description. Otherwise, everything's the same as lists (after, nested label syntax). 
	If Attributes.query is not the nullstring, the QUERY mechanism is used. Otherwise, LISTS is used. That means that 
	Attributes.query and Attributes.values cannot both be the nullstring if Attributes.type is checkbox or radio. 
	One more thing: For now anyway, within SBA Look-and-Feel, checkboxes are vertical, radio buttons are horizontal. 
	Please design your forms accordingly. (That is, if vertical layout of checkboxes wastes too much space, consider using 
	a select-multiple box instead, with an Attribues.size that wastes just thr right amount of vertical space.) 
	--->
	<cfset i										= 0>
	<cfset Variables.LabelBeg						= "">
	<cfset Variables.LabelEnd						= "">
	<cfif Attributes.MandOpt is "reqd">
		<!--- The reqddata background-color is kinda dark. Make it white for the label (and repeat the tooltip). --->
		<cfset Variables.LabelBeg					= Request.SlafEOL & Variables.IndentChars
													& '<span class="bgwhite" '
													& 'title="#HTMLEditFormat(Variables.RequiredFieldMsg)#">'>
		<cfset Variables.LabelEnd					= "</span>">
	</cfif>
	<cfset Variables.MultiTag						= "">
	<!--- The following is a convenient way to share code between LISTS mode and QUERY mode (and keep them in sync). --->
	<cfif NOT IsDefined("SfeGenMultiTag")>			<!--- Already defined if custom tag called more than once (likely). --->
		<cffunction name="SfeGenMultiTag"			returntype="any">
			<cfif Len(Attributes.TabIndex) gt 0>
				<cfset Variables.ti					= Attributes.TabIndex + i><!--- Before bumping i, because 0-based. --->
			</cfif>
			<cfset Variables.Id						= "#Attributes.Id#_#i#">
			<cfif ListFindNoCase(Attributes.Value, Variables.Value) GT 0>
				<cfset Variables.Checked			= "checked ">
			<cfelse>
				<cfset Variables.Checked			= "        ">
			</cfif>
			<cfset Variables.MultiTag				&= Variables.Tag
													& Request.SlafEOL & Variables.IndentChars
													& 'name="#Attributes.Name#" id="#Variables.Id#" '
													& ((Len(Attributes.TabIndex) gt 0)
														? 'tabindex="#Variables.ti#" '
														: "")
													& Variables.Checked
													& 'value="#HTMLEditFormat(Variables.Value)#"> '
													& Variables.LabelBeg
													& "#Variables.Label#"
													& Variables.LabelEnd
													& Request.SlafEOL
													& "</label>">
			<cfif i LT Variables.LastRow>
				<cfif Attributes.Direction is "vertical">
					<cfset Variables.MultiTag		&= "<br/>">
				</cfif>
				<cfset Variables.MultiTag			&= Request.SlafEOL>
			</cfif>
		</cffunction>
	</cfif>
	<cfif Len(Attributes.query) is 0>				<!--- LISTS mechanism: --->
		<cfset Variables.LastRow					= ListLen(Attributes.Values)>
		<cfloop index="Variables.Value" list="#Attributes.Values#">
			<cfset i								= i + 1>
			<cfset Variables.Label					= ListGetAt(Attributes.Labels, i)>
			<cfset SfeGenMultiTag()>
		</cfloop>
	<cfelse>										<!--- QUERY mechanism: --->
		<cfif		FindNoCase("Variables.",		Attributes.Query) GT 0>
			<cfset Variables.QueryName				= ReplaceNoCase(Attributes.Query, "Variables.", "Caller.", "One")>
		<cfelseif	FindNoCase("Request.",			Attributes.Query) GT 0>
			<cfset Variables.QueryName				= Attributes.Query>
		<cfelse>
			<cfset Variables.QueryName				= "Caller." & Attributes.Query>
		</cfif>
		<cfset Variables.LastRow					= Evaluate("#Variables.QueryName#.RecordCount")>
		<cfloop query="#Variables.QueryName#">
			<cfset i								= i + 1>
			<cfset Variables.Value					= Evaluate("#Variables.QueryName#.code")>
			<cfset Variables.Label					= Evaluate("#Variables.QueryName#.description")>
			<cfset SfeGenMultiTag()>
		</cfloop>
	</cfif>
	<cfset Variables.Tag							= Variables.MultiTag>
</cfcase>
<cfcase value="lookup"><!--- Already closed tag earlier. ---></cfcase>
<cfcase value="rte,textarea">
	<cfset Variables.Tag							&= Request.SlafEOL
													& ((Len(Attributes.TabIndex) gt 0)
														? 'tabindex="#Attributes.TabIndex#">'
														: ">")
													& HTMLEditFormat(Attributes.Value)
													& "</textarea>">
</cfcase>
<cfcase value="select-one,select-multiple">
	<cfset Variables.Tag							&= Request.SlafEOL
													& ((Len(Attributes.TabIndex) gt 0)
														? 'tabindex="#Attributes.TabIndex#">'
														: ">")
													& Request.SlafEOL
													& Variables.Options
													& Variables.IndentChars
													& "</select>">
</cfcase>
<cfdefaultcase>
	<cfset Variables.Tag							&= Request.SlafEOL
													& ((Len(Attributes.TabIndex) gt 0)
														? 'tabindex="#Attributes.TabIndex#" '
														: "")
													& 'value="#HTMLEditFormat(Attributes.Value)#">'>
</cfdefaultcase>
</cfswitch>

<!--- Stuff to be appended AFTER the tag. --->

<cfswitch expression="#Variables.Type#">
<cfcase value="date,datenonfuture">
	<cfset Request.SfeDateSeqNmb					= Request.SfeDateSeqNmb + 1>
	<cfif "No" and (Request.SfeDateSeqNmb is 1)><!--- This "No" essentially comments this code out for now. Here's why: --->
		<!---
		Unfortunately, although this cfset defines CalendarPopup correctly, it doesn't happen soon enough to get into 
		window.sba.calendarpopup. Therefore, we'll just have to forgo dynamic JS inclusion for now. 
		--->
		<cfset Variables.Tag						&= Request.SlafEOL
													& Trim(Request.SlafTopOfHeadCalendarPopup)>
	</cfif>
	<cfset Variables.Tag							&= Request.SlafEOL
													& '<a name="popup_#Attributes.Id#" id="popup_#Attributes.Id#" '
													& 'href="javascript:window.sba.calendarpopup.select(document.getElementById(''#Attributes.Id#''),''popup_#Attributes.Id#'',''MM/dd/yyyy'');">'
													& Request.SlafEOL & Variables.IndentChars
													& '<img src="/library/images/CalendarPopup.gif" border="0" alt="Pick a date using a popup calendar. (Opens a new window.)" title="Pick a date using a popup calendar. (Opens a new window.)">'
													& Request.SlafEOL
													& "</a>">
</cfcase>
<cfcase value="hidden">
	<!---
	Some hidden have to be displayed to the user (in a viewdata box). If so, we're liable to get "stcdview", indicating 
	the name of a span to display the hidden field. That's what this is all about: 
	--->
	<cfif Len(Attributes.visible) gt 0>
		<cfset Variables.Tag						&= '<span id="#Attributes.visible#">'
													& ((Len(Attributes.Value) gt 0)
														? HTMLEditFormat(Attributes.Value)
														: "&nbsp;")
													& "</span>">
	</cfif>
	<cfset Variables.TypeAllowsUserToChangeValue	= "No"><!--- Hidden fields don't get a second "Old_" hidden field! --->
</cfcase>
</cfswitch>

<cfif Variables.TypeAllowsUserToChangeValue>
	<!---
	Actually, if MandOpt is "view", we currently need a Hidden variable to preserve its contents under FormDataRecovery. 
	But in the new environment, where the map object is generated from the calls to cf_sbaformelt, it makes more sense to 
	preserve the original value on the server via Session scope. There shouldn't be *** ANY *** "Old_" values in the form. 
	--->
	<cfset Variables.Tag							&= Request.SlafEOL
													& '<input type="Hidden" name="Old_#Attributes.Name#" '
													& 'value="#HTMLEditFormat(Attributes.Value)#">'>
</cfif>

<!--- <cfset Variables.Tag								&= Request.SlafEOL>Whew!! --->

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

<cfif Len(Request.SfeParent) gt 0>
	<cfset Attributes["GeneratedData"]				=	{
														"Tag"	= Variables.Tag
														}>
	<cfassociate basetag="#Request.SfeParent#">
	<cfexit>
</cfif>

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

<cfset Variables.RowClass							= ListAppend(Attributes.RowClass, Request.SfeRowClass, " ")>
<cfoutput>#Request.SlafEOL#<cfif "No" and (Request.SfeSeqNmb is 1)><!--- See note about Request.SfeDateSeqNmb, above. --->
#Variables.IndentPlus0#<script src="#Variables.LibJsURL#/sbaformelt.js?CachedAsOf=2011-04-22T17:01"></script></cfif>
#Variables.IndentPlus0#<div class="tbl_row<cfif Len(Variables.RowClass) GT 0> #Variables.RowClass#</cfif>">
#Variables.IndentPlus1#<div class="tbl_cell formlabel">
#Variables.IndentPlus2#<label for="#Attributes.Id#" class="#Attributes.MandOpt#label#Variables.HiGrpLab#">#Attributes.Label#</label>
#Variables.IndentPlus1#</div>
#Variables.IndentPlus1#<div class="tbl_cell formdata">
#Variables.IndentPlus2#<div class="#Attributes.MandOpt#data#Variables.HiGrpBox#">
#Variables.IndentPlus3##Replace(Variables.Tag, Request.SlafEOL, Request.SlafEOL & Variables.IndentPlus3, "ALL")##Request.SlafEOL#
#Variables.IndentPlus2#</div><!-- #Attributes.MandOpt#data#HiGrpBox# -->
#Variables.IndentPlus1#</div><!-- formdata -->
#Variables.IndentPlus1#<div class="tbl_cell formerr"></div><cfif Request.SfeDebug>#Request.SlafEOL#
#Variables.IndentPlus1#<div class="tbl_cell formetc">
#Variables.IndentPlus2#<!-- #Variables.SfeDebugTrace# -->
#Variables.IndentPlus2#<input type="Button" value="E" onclick="
#Variables.IndentPlus2#DumpObject(document.getElementById('#Attributes.Id#'), '#Attributes.Id#', 15);
#Variables.IndentPlus2#">
#Variables.IndentPlus1#</div>
#Variables.IndentPlus1#<div class="tbl_cell formetc">
#Variables.IndentPlus2#<input type="Button" value="$(E)" onclick="
#Variables.IndentPlus2#DumpObject($(document.getElementById('#Attributes.Id#'))[0], '$(\'#Attributes.Id#\')', 15);
#Variables.IndentPlus2#">
#Variables.IndentPlus1#</div></cfif>#Request.SlafEOL#
#Variables.IndentPlus0#</div><!-- tbl_row -->
</cfoutput>

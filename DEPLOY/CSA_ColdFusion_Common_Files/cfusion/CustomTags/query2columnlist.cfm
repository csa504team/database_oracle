<!--- 
Author:			Daniel Lee
Date:			8/31/2004
Purpose:		Displays a query result set in a series of columns.
Version:		1.0.0

Syntax:			<cf_query2columnlist 	legend_name = "This is my legend"
										legend_class = "legend"
										legend_id = "myField_txt"
										border_class = "myField_tbl"
										border_id = "legend_tbl"
										border = "0"
										width = "90%"
										maxcolumns = "2"
										query = "myQuery"
										input_type = "checkbox"
										input_name = "IMCntctSourcCd"
										input_id = "IMCntctSourcDescTxt"
										input_value = "IMCntctSourcCd"
										checked_value_list = "myCheckedValueList"
										otherTextVal = "valueToTestFor"
										textName = "formFieldToTestAgainst"
										textValue = "textBoxValue"
										textSize = "1"
										textMaxlength = "5">

Required Parameters:
	query				- The name of a previously executed query
	input_name 			- name of input field
	input_id 			- id of input field
	input_value			- value of input field
		  
Optional Parameters:
	legend_name 		- description of a legend
	legend_id			- legend id for changing the class
	legend_class		- css of a legend
	border_id 			- border id for changing the class
	border_class 		- css of a table
	border				- table border (default 0)
	width				- table width (defaul 100%)
	maxcolumns			- max columns of a table (default 2)
	input_type			- type of input field (default checkbox)
	checked_value_list 	- a list of default values
	otherTextVal		- trigger to indicate a testbox is needed for clarifying data
						  (can be more than one value, separated by commas, to indicate
						   more than one textbox instance)
	textName			- name of the form element to test for equality
	textValue			- value to be displayed in the textbox
						  (if more than one value is entered for "otherTextVal",
						   the number of values entered for "textValue", must match
						   so that each textbox has it's unique value.)
	textSize			- desired size of the textbox to be created
	textMaxlength		- maxLength setting for the textbox to be created

Modification Log:
Change #	Date		Author			Modification
-----------	-----------	--------------- ---------------------------


--->


<cfparam name="attributes.legend_name" 			type="string" 	default="">
<cfparam name="attributes.legend_id" 			type="string" 	default="">
<cfparam name="attributes.legend_class" 		type="string" 	default="">
<cfparam name="attributes.border_id" 			type="string" 	default="">
<cfparam name="attributes.border_class" 		type="string" 	default="">
<cfparam name="attributes.border"				type="numeric"	default="0">
<cfparam name="attributes.width"				type="string"	default="100%">
<cfparam name="attributes.maxcolumns"			type="numeric"	default="2">
<cfparam name="attributes.query" 				type="string">
<cfparam name="attributes.input_type"			type="string" 	default="radio">
<cfparam name="attributes.input_name"			type="string">
<cfparam name="attributes.input_id"				type="string">
<cfparam name="attributes.input_value"			type="string">
<cfparam name="attributes.checked_value_list" 	type="string" 	default="">

<!--- Begin variables to create a textbox for items requiring clarifying data entry --->

<cfparam name="attributes.otherTextVal"			type="string" 	default="">
<cfparam name="attributes.textName"				type="string" 	default="">
<cfparam name="attributes.textValue"			type="string" 	default="">
<cfparam name="attributes.textSize"				type="numeric"	default="1">
<cfparam name="attributes.textMaxlength"		type="numeric"	default="5">

<cfset queryname 	= "caller." & attributes.query>

<cfif attributes.checked_value_list NEQ "">
	<cfset checked_value_list = Evaluate("caller." & attributes.checked_value_list)>
<cfelse>
	<cfset checked_value_list = "">
</cfif>

<!--- calculate the max rows on each column --->
<cfset queryrecords	= Evaluate(queryname & ".recordcount")>

<cfset columncount = ceiling(queryrecords / attributes.maxcolumns)>

<cfif attributes.border_class EQ "">
	<cfset table_summary  = "This is a data table.">
<cfelse>
	<cfset table_summary  = "Mandatory">
</cfif>

<!--- output the table --->
<cfoutput>
	<cfif attributes.legend_name NEQ "">
		<fieldset style="padding:8px;">
			<legend id="#attributes.legend_id#" class="#attributes.legend_class#">#attributes.legend_name#</LEGEND>
	</cfif>
	
	<table id="#attributes.border_id#" class="#attributes.border_class#" border="#attributes.border#" 
			width="#attributes.width#" SUMMARY="#variables.table_summary#">
</cfoutput>
	
		<tr>
			<cfset counter = 0>
			<cfoutput query="#queryname#">
				<cfif variables.counter EQ 0>
					<td width="#Evaluate(100/attributes.maxcolumns)#%" valign="top">
				</cfif>
			
				<INPUT TYPE="#attributes.input_type#" NAME="#attributes.input_name#" 
						ID="#attributes.input_name#-#Evaluate(attributes.input_id)#" 
						VALUE="#Evaluate(attributes.input_value)#"
					<CFIF ListFind(variables.checked_value_list, 
									Evaluate(attributes.input_value))>CHECKED</CFIF>

				>
				<LABEL FOR="#attributes.input_name#-#Evaluate(attributes.input_id)#">
					#Evaluate(attributes.input_id)#
				</LABEL>
				
				
<!--- Add a textbox for additional information when needed by Ann Casey--->
				
				<cfif isDefined("attributes.otherTextVal") AND attributes.otherTextVal NEQ "">
					<cfset generate = 0>
					<cfset compare	= Evaluate(queryname & "." & #attributes.input_value#)>
	 				<cfif Find(",",attributes.otherTextVal)>
					<cfset findPosition = 0>
						<cfloop index="x" list="#attributes.otherTextVal#">
							<cfset findPosition = findPosition + 1>
							<cfif Evaluate(compare) EQ x>
								<cfset generate = 1>
								<cfset foundPosition = findPosition>
							</cfif>
						</cfloop>
					<cfelseif Evaluate(compare) EQ Evaluate(attributes.otherTextVal)>
							<cfset generate = 1>
					</cfif>
					<cfif generate EQ 1>
					<cfif isDefined("findPosition")>
						<cfset findValue = 0>
						<cfloop index="y" list="#attributes.textValue#">
							<cfset findValue = findValue + 1>
							<cfif findValue EQ foundPosition>
								<cfif y EQ "empty_string">
									<cfset gotTextValue = "">
								<cfelse>
									<cfset gotTextValue = y>
								</cfif>
							</cfif>	
						</cfloop>
						
<!--- 						get textbox name --->
						<cfset findName = 0>
						<cfloop index="z" list="#attributes.textName#">
							<cfset findName = findName + 1>
							<cfif findName EQ foundPosition>
								<cfif z EQ "empty_string">
									<cfset gotNameValue = "">
								<cfelse>
									<cfset gotNameValue = z>
								</cfif>
							</cfif>	
						</cfloop>
					<cfelse>
						<cfset gotTextValue = #attributes.textValue#>
						<cfset gotNameValue = #attributes.textName#>
					</cfif>

							: <cfinput type="Text"
								name=#gotNameValue#
								value=#gotTextValue#
								size="#attributes.textSize#"
								maxlength="#attributes.textMaxlength#">
							<BR>
					<cfelse>
					 	<BR>
					</cfif>
				<cfelse>
				 	<BR>
				</cfif>
				
<!--- End of add a textbox --->
			
				<cfset variables.counter = variables.counter + 1>
				
				<cfif variables.counter EQ variables.columncount>
					<cfset variables.counter = 0>
					</td>
				</cfif>
			</cfoutput>
		</tr>
	</table>
	
<cfif attributes.legend_name NEQ "">
	</fieldset>
</cfif>
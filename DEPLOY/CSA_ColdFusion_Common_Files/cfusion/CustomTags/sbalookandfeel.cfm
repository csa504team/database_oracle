<!---
AUTHOR:				Steve Seaquist. 
DATE:				11/16/2006. 
DESCRIPTION:		Custom tag to present SBA standard look-and-feel Web page elements. 

NOTES:

	Although the standard button set's names are predefined, there is nothing to prevent an application from defining more, 
	with arbitrary names, provided that the graphics exist in /images/button_name_hi.gif and /images/button_name_lo.gif, and 
	a JavaScript with a DoButton_Name function exists in /#RootURL#/javascripts/DoButton_Name.js. The Action page must also 
	handle all allowed button_name passoffs, based on Form.SubmitButton, which will contain button_name, in case the user 
	has JavaScript turned off.

INPUT (Attributes):
	ActionURL		Button-handler URL, relative to the calling page (example: Action="act_mainnav.cfm"). 
					See Notes, above. Passed through to cf_mainnav if MainNavURL attribute not given. 
	AppDataInline	HTML to go into the AppData region if AppDataURL wasn't given. In addition, anything between the start and 
					end tags (<cf_sbalookandfeel> and </cf_sbalookandfeel>) will also be inline contents of the AppData region. 
					Specifically, AppDataInline will be displayed first, followed by whatever's between the start and end tags. 
	AppDataURL		AppData frame's URL, relative to the calling page (example: AppData="dsp_entry.cfm"). 
					If not given, no AppData frame will be generated, and the AppData region's contents will be AppDataInline 
					and/or whatever's between <cf_sbalookandfeel> and </cf_sbalookandfeel>. 
	AppHiddenURL	AppHidden frame's URL. A zero-pixel hidden frame called AppHidden is always generated. You can use this 
					hidden frame to do server callbacks, such as Franchise Code, NAICS Code or Zip Code lookups. If not given, 
					the hidden frame will still be generated, using "/library/html/blank.html". 
	AppInfoInline	HTML to go into the AppInfo area if AppInfoURL wasn't given. Also prevents expansion of MainNav into the 
					AppInfo area (as AppInfoURL does). 
	AppInfoURL		AppInfo frame's URL, relative to the calling page (example: AppInfoURL="dsp_info.cfm"). 
					If not given, no AppInfo frame will be generated. 
	AppName			Name of the app, which appears just below the SBA logo. 
	AppNavInline	HTML to go into the AppNav area if AppNavURL wasn't given. 
	AppNavURL		AppNav  frame's URL, relative to the calling page (example: AppNavURL="dsp_navtree.cfm"). 
					If not given, no AppNav  frame will be generated. 
	AutoAlignTbls	Legacy input. Ignored and forced to "No". (Only applicable to MSIE 6, 7 and 8, which we no longer support.) 
	Configs			If given, it will be cfincluded after configuration parameters are defined allowing you to do overrides. 
	Debug			"Yes" or "No" (default). Obvious. 
	HTML5			Legacy input. Ignored and forced to "Yes". 
	Icons			If given, overrides the default set of icon buttons displayed by cf_buttonbar. Similar to Show, below, 
					but restricted to specific icon names because they're implemented as graphics files. 
	jQueryURL		If given, it will be used as the URL for jQuery, overriding the default URL. DEPRECATED. 
	jQuery1Ver		If given, it will be inserted after "jquery" in the "jquery.js" of the 1.9+ tract for MSIE 6/7/8. 
					If not given, it will default to "1", yielding "jquery1.js", the symbolic link default for MSIE 6/7/8. 
					Example values: "-1.9.2.min" or ".latest". 
	jQuery2Ver		If given, it will be inserted after "jquery" in the "jquery.js" of the 2.0+ tract for all other browsers. 
					If not given, it will default to "2", yielding "jquery2.js", the symbolic link default of other browsers. 
					(NOTE: jquery.migrate, if needed, is the responsibility of the calling page. SBA Look-and-Feel doesn't 
					need it and will not include it automatically.) 
	jQueryMobileVer	If given, it will be inserted after "jquery.mobile" in the "jquery.mobile.js" when MobileAware="Yes". 
					Example value: ".latest". ("-version.min" won't work because of need to drill down a directory.) 
	JSInline		If given, it will be cfincluded in the <head>. Generally, this is used to define JavaScripts, but it 
					can also be used with <link> or <style> to override default CSS, or even to insert HTML comments. 
	LibURL			If not given, "/library" is assumed. 
	MainNavHiddens	If given, HTML to be included immediately after <form name="FormMainNav". Usually contains hidden fields 
					to be passed to ActionURL. 
	MainNavJSURL	If given, Server-relative URL of JavaScripts to support each button in MainNav area/frame. 
					For example, if MainNavJSURL="/elend/javascripts", DoExit.js, DoHelp.js, etc, are in that directory. 
	MainNavMenuURL	If given, URL of cfinclude that defines Request.SlafShowButtonsInMainNavMenu, a structure-of-structures 
					that maps Show to the MainNavMenu. The cfinclude file is allowed not to exist, so that switching over to 
					menus can be externally configurable. First example is /security/dsp_mainnav_menu.cfm. 
	MainNavURL		MainNav frame's URL, relative to the calling page (example: MainNavURL="dsp_mainnav.cfm"). 
					If not given, the main navigation buttons will be defined in the MainNav area of the caller's page. 
	MobileAware		If given, list of mobile frameworks that the page supports. In particular, "jqm" means jQuery Mobile. 
					Forces HTML5 strict mode. 
	ReadyLight		"Yes" or "No". If "Yes" (default), generates loading/ready indicator and the onloads to set it to "Ready". 
	Show			Which buttons to show in the MainNav area (for example: Show="Admin,Help,Exit"). 
	TextOnly		Ignored. Now defaulting to the Request.SlafTextOnly mechanism. 
	WindowTitle		<title>WindowTitle</title>. If not given, "SBA - #AppName#" will be used. 

OUTPUT:				Web page with the SBA's standard look-and-feel Web page elements. 
REVISION HISTORY:	03/15/2016, SRS:	Name changes (CurrIP to SlafIPAddrIs). RELEASE REQUIRES bld_CurrIPs!!! 
					10/19/2015, SRS:	Reworked lots of stuff to be "themeable" and simpler because we no longer have 
										to support MSIE 8: Got rid of files for quirks mode, which we haven't generated 
										in over 4 years, to relieve that maintenance burden. As part of that, to eliminate 
										dual maintenance, got rid of _mobile and _nomobile versions. This file allows mobile 
										(able to include /library/cfincludes/sbalookandfeel/sbalookandfeel_jqm), so it's 
										the more capable of the two. But OCA pages never actually do that, because none of 
										them specifies MobilAware="jqm" (yet). Added Attributes.Icons as an input. Removed 
										AutoAlignTbls and HTML5 as inputs, now that everything runs in strict mode. 
										Added new Attributes.MainNavMenuURL input (normally a passthrough to cf_mainnav). 
					04/26/2013, SRS:	Added DivSliderControl (for user complaints that not all of AppNav was visible). 
										Removed "AllowSlider" logic because now everyone has the feature. 
					04/24/2013, SRS:	Changes to support the merge of the temporary file get_sbalookandfeel_topofjqm 
										into the permanent file get_sbalookandfeel_topofhead. This included adding hooks 
										to allow both jquery and jquery.mobile to override version for testing. 
					03/28/2013, SRS:	Adapted to the move of jQuery Mobile support into the /library directory. 
					02/14/2013, SRS:	Added ability to sync up Session Timeout countdown timer across multiple windows. 
										(Code is in sbalookandfeel.?????.js, but needed to reset CachedAsOf string.) 
										Also added Federal Analytics (Google Analytics for the Federal Government.) 
					09/04/2012, SRS:	Set default for SlafHTML5 to "Yes" to turn on Strict Mode, even in production. 
					08/03/2011 -
					06/28/2012, SRS:	In preparation for MSIE 10, which will TOTALLY mess us up with regard to QuirksMode, 
										started getting really serious about HTML5 (strict mode) and fixes for strict mode 
										related problems. Redid how show/hide debug info works in the question mark menu. 
										Per request, created "OISS Debug: Show/Hide Envelope" SlafMenu item. Stratified out 
										gradients (supported in IE 10) from display tables (supported in IE 8), so using 
										"lte IE 9" on IE-only mainnav css files. Added MobileAware and includes of "_dtv" 
										or "_jqm" include files. (Hence, "lte IE 9" is in "_dtv" only.) 
					07/20/2011, SRS:	Minor change to support "external system" buttons in main navigation. 
					02/25/2011, SRS:	CachedAsOf update on sbalookandfeel.js. Added support for AutoAlignTbls. 
					11/09/2010, SRS:	Now that CSS buttons are proven to work, made code more efficient. 
					10/01/2010, SRS:	Added HTML5 support (setting the DOCTYPE to turn on browser's HTML5 features). 
										Now that New Look-and-Feel is pretty stable, moved previously-prototyped-here 
										static JS into sbalookandfeel.js so that it can be csched by the browser. To 
										eliminate caching problems with sbalookandfeel.js, added AppendCachedAsOf support 
										and used it to force a first-time-seen reload of sbalookandfeel.js. Added WAI-ARIA 
										role="timer" to AppTimeout. 
					07/20/2010, SRS:	Continuing "New Look-and-Feel", moved AppName to between SBALogo and MainNav. 
										Added WAI-ARIA "landmark roles", where appropriate ("navigation" and "main"). 
					07/16-20/2010, SRS:	"New Look-and-Feel". Took a checkpoint at 07/20/2010. 
					07/15/2010, SRS:	Checkpoint of all changes prior to "New Look-and-Feel" (Request.SlafTopOfHead, 
										bluesteel and magnum backgrounds, giving SlafMenu dynamic height and width 
										(expanding and contracting according to its contents), adding "Edit Preferences" 
										(also known as "Accessibility Options") to SlafMenu, etc). 
					03/10/2010, SRS:	Moved the cfsavecontent of Variables.LastModified outside of the cfif, cfelse and 
										/cfif, so that it's more obvious that it's always generated. In the process, added 
										a title to explain the Timeout Countdown a bit better. Added jQueryURL and Lang 
										attributes. Split out standard CSS and JS initialization to make it easier to do 
										everything properly in a frame (by using Request.SlafTopOfHead). 
					02/17/2010, SRS:	Added invisible "Skip over navigation" hotlink for Section 508. Added access keys for 0 
										(list of access keys), 1 (home = GLS Choose Function if logged into GLS), 2 (skip navigation). 
										On sbalookandfeel_help.html page, linked to by SlafMenu, documented access keys, 
										"required" form elements and added President Obama to sortable list example. 
					01/27/2010, SRS:	Added a session timeout countdown timer ("AppTimeout") to the lower-right corner of BotMost. 
										(Most of the code to do this is in /library/javascripts/sbalookandfeel/sbalookandfeel.js.) 
					01/06/2010, SRS:	Added alternate stylesheet sba.leaner_and_meaner.css, but only from my IP address 
										and only in development for now. 
					05/29/2009, SRS:	For usability with JS off, need to define (noscript)(link to noscript.css)(/noscript). 
					05/28/2009, SRS:	Use jQuery's document ready event to trigger SBA Look-and-Feel's DoThisOnLoad logic earlier. 
										Continuing to use window load event to trigger developer routines, however, since they may 
										be dependent on that timing. Added automatic support for ReadyLight. The only thing you have 
										to do manually now is call top.SlafSetReadyLightToLoading() before loading a new URL into a 
										frame. (For example: "top.SlafSetReadyLightToLoading(); top.AppData.location.href = url;".) 
					03/20/2009, SRS:	Use Request.SlafIPAddrIsOISS to determine whether 4th floor or Herndon. Also added "favicon" 
										link. Also began "laser pointer" logic to allow highlighting the cursor in presentations. 
					11/26/2008, SRS:	Changes to clearly identify the development, test and production environments. Also added 
										jQuery control of SlafMenu, because it does a better job of mouse tracking. Changed the 
										meta tag to identify the character set as UTF-8, which is what the CF documentation says 
										the default character set of a CF page is. (The page on the cfcontent tag says "By default, 
										ColdFusion returns character data using the Unicode UTF-8 format, regardless of the value 
										of an HTML meta tag in the page.". By making it accurate, it may improve browsers' encoding 
										of the form data they send back to us.) 
					11/23/2007, SRS:	Went to a new method of scheduling onLoad event handlers, which allows them to cascade. 
										Initially used in SortableTables.js, this will allow any of our JavaScript routines to 
										schedule its own onLoad event handler. Did the same for onResize. 
					03/01/2007, SRS:	Changes to make printing easier. Also added SlafMenu, which includes global "Print". 
					12/08/2006, SRS:	Made ColdFusion errors appear on the screen by moving all HTML generation to the end tag. 
					11/16/2006, SRS:	Original implementation. Cannibalized previous sbalookandfeel.cfm to create this one. 
--->

<cfswitch expression="#thisTag.ExecutionMode#">
<cfcase value="start">
	<cfif NOT thisTag.HasEndTag>
		<cfabort showerror="ERROR. &lt;cf_sbalookandfeel&gt; called without matching &lt;/cf_sbalookandfeel&gt;.">
	</cfif>

<!--- Configuration Parameters: --->

<cfset Variables.DefaultAppHiddenURL				= "/library/html/blank.html">
<cfset Variables.NoIFrames							= "This application requires a browser that supports IFRAME. "
													& "Please upgrade your browser.">
<cfset Variables.SelfName							= "sbalookandfeel">
<cfset Variables.ShowSkipLinks						= "Yes">

<!--- Covert Configuration Parameters --->

<cfset Variables.DumpDynamicContent					= IsDefined("URL.DumpDynamicContent")>

<!--- Attribute Defaults: --->

<cfparam name="Attributes.LibURL"					default="/library"><!--- Has to be done out of alphabetical order. --->

<cfif NOT IsDefined("Request.SlafDevTestProd")>
	<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbashared_variables.cfm">
</cfif>
<!---
<cfif NOT IsDefined("AppendCachedAsOf")><!--- Tricks browsers to assure that we pick up edited JS/CSS right away. --->
	<cfinclude template="#Attributes.LibURL#/udf/bld_AppendCachedAsOf.cfm">
</cfif>
--->
<!---
<cfif NOT IsDefined("Request.SlafIPAddrIsSteveSeaquist")>
	<cfinclude template="#Attributes.LibURL#/cfincludes/bld_CurrIPs.cfm">
</cfif>
--->

<cfparam name="Attributes.ActionURL"				default="">
<cfparam name="Attributes.AppDataURL"				default="">
<cfif (NOT IsDefined("Attributes.AppHiddenURL"))	OR (Len(Attributes.AppHiddenURL) IS 0)>
	<cfset Attributes.AppHiddenURL					= "#Variables.DefaultAppHiddenURL#">
</cfif>
<cfparam name="Attributes.AppInfoInline"			default="">
<cfparam name="Attributes.AppInfoURL"				default="">
<cfparam name="Attributes.AppName"					default="Unknown Application">
<cfparam name="Attributes.AppNavInline"				default="">
<cfparam name="Attributes.AppNavURL"				default="">
<cfset		   Attributes.AutoAlignTbls				= "No"><!--- Not used anymore. (Always "No".) --->
<cfparam name="Attributes.Configs"					default="">
<cfparam name="Attributes.Debug"					default="No">
<cfset		   Attributes.HTML5						= "Yes"><!--- Not used anymore. (Always "Yes".) --->
<cfparam name="Attributes.Icons"					default="">
<cfparam name="Attributes.jQueryURL"				default="">
<cfparam name="Attributes.jQuery1Ver"				default="">
<cfparam name="Attributes.jQuery2Ver"				default="none"><!--- Temporary jQuery 2.0 suppression code, for now. --->
<cfparam name="Attributes.jQueryMobileVer"			default="">
<cfparam name="Attributes.JSInline"					default="">
<cfparam name="Attributes.Lang"						default="en-US"><!--- United States English --->
<cfparam name="Attributes.MainNavHiddens"			default="">
<cfparam name="Attributes.MainNavJSURL"				default="/library/javascripts/sbalookandfeel">
<cfparam name="Attributes.MainNavMenuURL"			default="">
<cfparam name="Attributes.MainNavURL"				default="">
<cfparam name="Attributes.MobileAware"				default="">
<cfparam name="Attributes.ReadyLight"				default="Yes">
<cfparam name="Attributes.Show"						default="">
<cfset Attributes.TextOnly							= ""><!--- Ignore what's on the cf_sbalookandfeel call. --->
<cfif NOT IsDefined("Attributes.WindowTitle")		OR (Len(Attributes.WindowTitle) IS 0)>
	<cfset Attributes.WindowTitle					= "SBA - #Attributes.AppName#">
</cfif>
<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_ConfigStdButtons.cfm">

<!--- MOBILE --->

<cfset Variables.DesktopView						= "Yes"><!--- Not currently used. --->
<cfset Variables.jQueryMobileAware					= "No">
<cfset Variables.jQueryMobileView					= "No">
<!--- Allow for multiple strings beginning with "jqm", such as "jqmmultipage" or "jqmsimple". All imply jQueryMobileAware. --->
<cfloop index="MobileAwareType" list="#Attributes.MobileAware#">
	<cfif (Len(MobileAwareType) ge 3) and (Left(MobileAwareType,3) is "jqm")>
		<cfset Variables.jQueryMobileAware			= "Yes">
		<cfbreak>
	</cfif>
</cfloop>
<cfif Variables.jQueryMobileAware><!--- All branches of this if set jQueryMobileView and SlafRenderingMode. --->
	<!--- Even if a jQueryMobileAware page is in DesktopView, still force HTML5 mode: --->
	<cfset Request.SlafHTML5						= "Yes"><!--- Not used anymore. (Always "Yes".) --->
	<cfif IsDefined("Request.SlafRenderingMode")>
		<cfswitch expression="#Request.SlafRenderingMode#"><!--- Allow for other rendering modes in the future. --->
		<cfcase value="jqm">
			<cfset Variables.jQueryMobileView		= "Yes">
		</cfcase>
		</cfswitch>
	<cfelseif Request.SlafBrowser.Mobile and Variables.jQueryMobileAware>
		<!--- Default mobile browsers to mobile if the page is MobileAware: --->
		<cfset Variables.jQueryMobileView			= "Yes">
		<cfset Request.SlafRenderingMode			= "jqm">
	<cfelse>
		<cfset Request.SlafRenderingMode			= "dtv">
	</cfif>
<cfelse>
	<cfset Request.SlafRenderingMode				= "dtv">
</cfif>

<cfif Variables.jQueryMobileView><!--- No frames allowed in jQuery Mobile *** YET *** (planned): --->
	<cfset Attributes.AppDataURL					= "">
	<cfset Attributes.AppInfoURL					= "">
	<cfset Attributes.AppNavURL						= "">
	<cfset Attributes.MainNavURL					= "">
	<cfset Variables.DesktopView					= "No"><!--- Not currently used. --->
</cfif>

<!--- /MOBILE --->

<!--- Allow runtime overrides, especially from Central Office (4th floor) and Herndon: --->

<!--- Use these 3 variables to control experiments or other features with limited scope. --->
<cfset Variables.DeveloperOfSlaf					= FALSE>
<cfset Variables.DeveloperOfSlafOrManager			= FALSE >
<cfset Variables.SlafIPAddrIsOISS					= Request.SlafIPAddrIsOISS><!--- Allows quick expansion of criteria. --->

<!--- Begin the long, detailed task of deciding what to put on the screen: --->

<cfset Variables.BrowserIsMSIE						=	(FindNoCase("MSIE",				CGI.HTTP_User_Agent) GT 0)
													OR	(FindNoCase("Internet Explorer",CGI.HTTP_User_Agent) GT 0)>
<cfset Variables.BrowserIsOpera						=	(FindNoCase("Opera",			CGI.HTTP_User_Agent) GT 0)>
<cfif Variables.BrowserIsOpera>
	<cfset Variables.BrowserIsMSIE					= "No"><!--- Opera mentions MSIE --->
</cfif>

<cfset Request.SlafHTML5							= "Yes"><!--- Not used anymore. (Always "Yes".) --->

<cfset Variables.IFrameAttribs						= ' frameborder="0" height="100%" width="100%"'>

<cfif (Len(Attributes.AppNavInline) GT 0) OR (Len(Attributes.AppNavURL) GT 0)>
	<cfset Variables.AppNavIsVisible				= "Yes">
	<cfset Variables.MaybeNoAppNavClass				= "">
<cfelse>
	<cfset Variables.AppNavIsVisible				= "No">
	<cfset Variables.MaybeNoAppNavClass				= "class=""NoAppNavContents"" ">
</cfif>

<!--- Overrides of Configuration Parameters: --->

<cfif Len(Attributes.TextOnly) IS 0>
	<cfif IsDefined("Request.SlafTextOnly")>
		<cfset Attributes.TextOnly					= Request.SlafTextOnly>
	<cfelse>
		<cfset Attributes.TextOnly					= "No">
	</cfif>
</cfif>
<cfif Len(Attributes.Configs) GT 0>
	<cfinclude template="#Attributes.Configs#">
</cfif>
<cfif Variables.jQueryMobileAware>
	<cfif Variables.jQueryMobileView>
		<cfset Variables.ListShowBtnsFull			= "Desktop View,#Attributes.Show#">
	<cfelse>
		<cfset Variables.ListShowBtnsFull			= "Mobile View,#Attributes.Show#">
	</cfif>
<cfelse>
	<cfset Variables.ListShowBtnsFull				= Attributes.Show>
</cfif>
<cfset Variables.ListShowBtnsPacked					= Replace(Replace(Variables.ListShowBtnsFull," ","","ALL"),"-","","ALL")>
<cfset Variables.CLSAuthorized						= "No">
<cftry>
	<cflock scope="Session" type="ReadOnly" timeout="30">
		<cfif IsDefined("Session.CLS.CLSAuthorized")>
			<cfset Variables.CLSAuthorized			= Session.CLS.CLSAuthorized>
		</cfif>
	</cflock>
	<cfcatch type="any"><!--- Ignore if user isn't in a session (Scheduled Task directories, for example). ---></cfcatch>
</cftry>
<cfif (Len(Attributes.Icons) is 0) and (NOT Variables.CLSAuthorized)>
	<cfset Attributes.Icons							= "settings,help,printer,email"><!--- But not "home" or "logout". --->
</cfif>

<!--- Validations (note: done after overrides): --->

<cfif (Len(Attributes.AppInfoURL) GT 0) AND (Len(Attributes.AppInfoInline) GT 0)>
	<cfoutput><!doctype html><html lang="en-US"><head><title>#Attributes.WindowTitle#</title>
<body>&lt;cf_#Variables.SelfName# AppInfo&gt; and &lt;cf_#Variables.SelfName# AppInfoInline&gt; both given.</body></html></cfoutput>
	<cfabort>
</cfif>
<cfif (Len(Attributes.AppNavURL)  GT 0) AND (Len(Attributes.AppNavInline)  GT 0)>
	<cfoutput><!doctype html><html lang="en-US"><head><title>#Attributes.WindowTitle#</title>
<body>&lt;cf_#Variables.SelfName# AppNav&gt; and &lt;cf_#Variables.SelfName# AppNavInline&gt; both given.</body></html></cfoutput>
	<cfabort>
</cfif>

<!--- SlafToggleTextOnlyForm's action attribute is the same page as we're on: --->

<cfset Variables.SlafToggleTextOnlyFormAct			= CGI.Script_Name>
<cfif Len(CGI.Query_String) GT 0>
	<cfif Left(CGI.Query_String,1) is "?">
		<cfset Variables.SlafToggleTextOnlyFormAct	&= CGI.Query_String>
	<cfelse>
		<cfset Variables.SlafToggleTextOnlyFormAct	&= "?#CGI.Query_String#">
	</cfif>
</cfif>

<!---
For strict mode (also known as standards compliance mode) in MSIE, there should NEVER be ANY non-whitespace above
the doctype. If MSIE sees so much as a single non-whitespace character above the doctype, it reverts to quirks mode.
This greatly messes up our strict mode CSS, which was carefully crafted to not require any JavaScript support at all
for region sizing (onload, onresize, etc). This makes all of our pages more responsive and even works if the user
has turned JavaScript off. Doing cfcontent reset="true" throws away everthing in ColdFusion's output buffer, so the
the doctype at the start of Request.SlafHead will be the very first thing that MSIE sees. The only downside to doing
this is if an inexperienced developer has put JavaScript or CSS onto the page prior to calling cf_sbalookandfeel
(a common mistake). Note to those who do this: TOUGH LUCK, cf_sbalookandfeel controls the page layout, not you.
If you want JS to appear in the head, add it to JSInline. There are a ton of pages that do this right, good examples
to learn from. It's not a great hardship to learn how to use JSInline correctly. That said, FORCE strict mode for all:
--->
<cfcontent reset="true">
<!---
P.S. (learned this the hard way): Doing cfcontent reset="true" when thisTag.ExecutionMode is "end" results in the
ENTIRE PAGE being thrown away!! It's as if the cfcontent is delayed until the custom tag exits, then executed in
the context of the page calling the custom tag. So if you do it when thisTag.ExecutionMode is "start", no problem.
You're just throwing away the CF output buffer BEFORE the page is generated. Also works with self-closing call
(cf_sbalookandfeel .../), which behaves the same as (cf_sbalookandfeel ...)(/cf_sbalookandfeel).
--->

</cfcase><!--- End of processing when thisTag.ExecutionMode is "start". --->

<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->
<!--- **************************************************************************************************** --->

<cfcase value="end">
	<!---
	The time required to generate HTML is negligible, especially as compared to database I/O. So generate the
	LastModified string here, before displaying any of the divs. That way, if an error occurs in cf_lastmodified,
	its error display will be fully visible and developers will get good feedback. Request.SlafAppDataIsInline
	is used to tell cf_lastmodified to split the display with 2 spans:
	--->
	<cfset Variables.LastModified					= "">
	<cfset Request.SlafAppDataIsInline				= (Len(Attributes.AppDataURL) IS 0)>
	<cfsavecontent variable="Variables.LastModified">
		<cfif Request.SlafAppDataIsInline>
			<cfoutput>
			<div id="DivLastModified"><span id="SpanLastModifiedL"></cfoutput>
			<cfif CGI.Server_Name IS "localhost"><!--- Prevents a crash in offline testing: --->
				<cfoutput>Metrics Service Is ...</span><span id="SpanLastModifiedR">... Not Available</cfoutput>
			<cfelse>
				<cf_lastmodified>
			</cfif>
		<cfelse>
			<cfoutput>
			<div id="DivLastModified"><span id="SpanLastModifiedR"></cfoutput>
		</cfif>
		<cfset Variables.AppTimeoutTooltip			= "Estimated time remaining until your session on "
													& CGI.Server_Name
													& " times out. (Other SBA servers are not affected.)">
		<cfoutput><br/>
			<span id="AppTimeout" title="#Variables.AppTimeoutTooltip#"
			role="timer"></span></span></div></cfoutput>
	</cfsavecontent>
	<!---
	The following variables have been namespaced, so that get_sbalookandfeel_topofhead doesn't *** HAVE *** to be called
	ONLY by this custom tag:
	--->
	<cfset Request.SlafTopOfHeadjQ1Ver				= Attributes.jQuery1Ver>
	<cfset Request.SlafTopOfHeadjQ2Ver				= Attributes.jQuery2Ver>
	<cfset Request.SlafTopOfHeadjQM					= Variables.jQueryMobileAware>
	<cfset Request.SlafTopOfHeadjQMVer				= Attributes.jQueryMobileVer>
	<cfset Request.SlafTopOfHeadLang				= Attributes.Lang>
	<cfset Request.SlafTopOfHeadLibURL				= Attributes.LibURL>
	<cfset Request.SlafTopOfHeadTextOnly			= Attributes.TextOnly>
	<cfinclude template="#Attributes.LibURL#/cfincludes/get_sbalookandfeel_topofhead.cfm">
	<cfif Variables.jQueryMobileAware>
		<cfif Variables.jQueryMobileView>
			<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/sbalookandfeel_jqm.cfm">
		<cfelse><!--- cfelse = default = desktop view ("dtv") --->
			<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/sbalookandfeel_dtv.cfm">
		</cfif>
	<cfelse><!--- The following is what's done if the the page is NOT jQueryMobileAware: --->
		<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/sbalookandfeel_dtv.cfm">
	</cfif>
</cfcase><!--- End of processing when thisTag.ExecutionMode is "end". --->
</cfswitch>

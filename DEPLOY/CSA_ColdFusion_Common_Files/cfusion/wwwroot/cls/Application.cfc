<cfcomponent output="false" extends="/library/Application">

	<!---
	AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc, for the US Small Business Administration. 
	DATE:				07/31/2014. 
	DESCRIPTION:		Application and Session control for all CLS systems. CLS systems wull extend this component. 
	NOTES:				Until further notice, please, no one but Steve Seaquist should work on this file. If it's not right, 
						all of our systems crash. It impacts too many other developers to be casually edited. Thanks a bunch. 
	INPUT:				None. 
	OUTPUT:				Application and Session control. 
	REVISION HISTORY:	12/02/2015, SRS:	Changed many IP addresses. Since that's the sort of thing that's liable to be ongoing and 
											common, let's agree that simple IP address changes in the 6, 85, 86, 87 and 95 subnets 
											no longer require a full check-in/check-out in the future, okay? Let this be the last one. 
											Added ca5tiber to the internal environment, so that it will request bld_SecurityUDFs.cls. 
											Changed ca1anacostia to ca1danube for the new environment. 
						06/03/2015 -
						08/05/2015, SRS:	Massive changes due to everyone moving to the Concourse level. Reversed the sense of 
											"CLSOnlyEnvironment" to "GLSEnvironment". The default application name is now "CLS". 
											This is important when accessing rivernames directly (which no one should ever do). 
						05/01/2015, SRS:	Updated my IP address. Added more CLSOnlyServerNames. 
						11/09/2014, NS:		Updated my IP address. 
						09/03/2014, SRS:	Added more debugging addresses due to Windows 7 machines causing change of IP. 
						07/31/2014, SRS:	Original implementation. 
	--->

	<!--- ******************* APPLICATION CONTROL ******************* --->

	<cfset this.ListGLSServerNames							= "dweb.sba.gov,"
															& "dwebsp.sba.gov,"
															& "enile.sba.gov,"
															& "enilesp.sba.gov,"
															& "eweb.sba.gov,"
															& "iweb.sba.gov,"
															& "eweb1.sba.gov,"
															& "eweb1sp.sba.gov,"
															& "web.sba.gov"><!--- Finalized. --->
	<cfset this.GLSEnvironment								= (ListFindNoCase(this.ListGLSServerNames,		CGI.Server_Name) gt 0)>
	<cfset this.ListInternalServerNames						= "cadweb.sba.gov,"
															& "catweb.sba.gov,"
															& "ca1danube.sba.gov,"
															& "ca2rouge.sba.gov,"
															& "ca2yukon.sba.gov,"
															& "ca5tiber.sba.gov">
	<cfset this.InternalEnvironment							= (ListFindNoCase(this.ListInternalServerNames,	CGI.Server_Name) gt 0)>
	<cfif this.GLSEnvironment>
		<cfset this.name									= "GLS">
	<cfelse>
		<cfset this.name									= "CLS">
		<cfif this.InternalEnvironment><!--- Just us web devs. Not visible to lenders, cfo or disaster. --->
			<cfset this.enableRobustException				= true><!--- Unfortunately, this doesn't work. --->
		</cfif>
	</cfif>
	<cfset this.applicationTimeout							= CreateTimeSpan(1, 0, 0, 0)><!--- 1 day --->
	<cfset this.debuggingIPAddresses						= "165.110.6.48,"	<!--- Asad --->
															& "165.110.6.15,"	<!--- Bhargavi --->
															& "165.110.6.39,"	<!--- Chitra --->
															& "165.110.6.33,"	<!--- Chuda --->
															& "165.110.6.70,"	<!--- DanDu --->
															& "165.110.6.32,"	<!--- Durga --->
															& "165.110.6.46,"	<!--- Haseefa --->
															& "165.110.6.8,"	<!--- Ian --->
															& "165.110.6.17,"	<!--- Ike M. --->
															& "165.110.87.114,"	<!--- Jame --->
															& "165.110.87.117,"	<!--- Jay --->
															& "165.110.6.11,"	<!--- Matt --->
															& "165.110.6.21,"	<!--- Mikhail --->
															& "165.110.6.6,"	<!--- Nelli --->
															& "165.110.87.155,"	<!--- Nirish --->
															& "165.110.6.44,"	<!--- Prathyusha --->
															& "165.110.6.66,"	<!--- Priya --->
															& "165.110.6.24,"	<!--- Rahul --->
															& "127.0.0.1,"	<!--- Robert --->
															& "165.110.85.112,"	<!--- Ron --->
															& "165.110.6.45,"	<!--- Rushan --->
															& "165.110.6.31,"	<!--- Selvin --->
															& "165.110.6.90,"	<!--- Sheen --->
															& "165.110.85.153,"	<!--- Sheri --->
															& "165.110.6.36,"	<!--- Sherry Liu --->
															& "165.110.6.27,"	<!--- Steve S. --->
															& "165.110.6.40,"	<!--- Steve S. old --->
															& "165.110.6.100,"	<!--- Steve S. new --->
															& "165.110.87.97,"	<!--- Timalyn --->
															& "165.110.6.43"	<!--- Vidya --->
															><!--- Because "per app" setting, independent of "SBA" app (/library). --->
	<cfset this.scriptProtect								= true>
	<!--- This was in case CF Admin's classpath didn't contain cfx.jar and /library/classes/, but it does:
	<cfset this.javaSettings								=
		{
		loadPaths											=
			[
			"/opt/coldfusion11/cfusion/wwwroot/WEB-INF/lib/cfx.jar",
			"/opt/iplanet/servers/docs/library/classes/"
			],
		loadColdFusionClassPath								= false,
		reloadOnChange										= true
		}> --->
	<cfset this.sessionManagement							= "Yes">
	<cfset this.sessionTimeout								= CreateTimeSpan(0, 1, 0, 0)><!--- 1 hour --->
	<cfset this.setClientCookies							= false>

	<!--- ******************* SERVER-GLOBAL INSTANCE VARIABLES (DO NOT OVERRIDE) ******************* --->

	<!--- None. Inherits this.LibURL from /library/Application, however. --->

	<!--- ******************* INSTANCE VARIABLES FOR THIS APPLICATION.CFC ******************* --->

	<cfset this.SBALogSystemName							= "GLS"><!--- Our log4j appender name is still GLS. --->
	<cfset this.AppURL										= "/cls">
	<cfset this.SysURL										= "/cls">
	<cfset this.OnRequestStartInclude						= "/cls/OnRequestStart.cfm"><!--- Or "none" --->
	<cfset this.WindowTitleBase								= "SBA - CapAccess Login System">

	<!--- ******************* SERVER-GLOBAL CUSTOM METHODS ******************* --->

	<!--- None. Inherits DefineStandardSubdirNames from /library/Application, however. --->

	<!--- ******************* STANDARD METHODS ******************* --->

	<!--- No onApplicationStart yet. --->
	<!--- No onCFCRequest yet. --->
	<!--- No onError yet. --->
	<!--- No onMissingTemplate yet. --->
	<!--- No onSessionStart yet. --->

	<cffunction name="onRequestStart"						returnType="boolean">
		<cfargument name="targetPage"						type="String" required=true/>
		<cfset var Local									= StructNew()>
		<!---
		Application.cfc files that extend this file and define their own onRequestStart method must call Super.onRequestStart
		before doing their own stuff. Web Service subdirectory Application.cfc files must StructDelete onRequestStart and
		onRequest. If you don't do that, calls to the web services will hang without explanation (known restriction).
		--->
		<cfset this.DefineStandardSubdirNames("App", this.AppURL)><!--- Varies by system based on their own this.AppURL. --->
		<cfset this.DefineStandardSubdirNames("Lib", this.LibURL)><!--- Always, do not override. --->
		<cfset this.DefineStandardSubdirNames("Sys", this.SysURL)><!--- Varies by system based on their own this.SysURL. --->
		<cfset Variables.LogURL								= Variables.LibIncURL>
		<cfset Request.SBALogSystemName						= this.SBALogSystemName>
		<cfinclude template="#Variables.LogURL#/inc_starttickcount.cfm">
		<cfif this.GLSEnvironment>
			<cflock scope="Session" type="Exclusive" timeout="30">
				<cfif		(NOT	IsDefined("Session.CLS"))
					and				IsDefined("Session.GLS")
					and				IsStruct  (Session.GLS)>
					<cfset Session.CLS						= Session.GLS><!--- Assignment! (See end-of-cflock comment.) --->
					<!---
					The only place CLS/GLS is mentioned in the struct is at the top level. 10 of 28 keys have to be copied
					to new name. Looping is safer in the long run than naming them. Also, this loop will be done only once:
					--->
					<cfset Local.StructKeys					= StructKeyList(Session.CLS)>
					<cfloop index="Key" list="#Local.StructKeys#">
						<cfif	(Len	(Key)				gt 3)
							and	(Left	(Key, 3)			is "GLS")>
							<cfset Local.NewKey				= "CLS" & RemoveChars(Key, 1, 3)>
							<cfset Session.CLS[Local.NewKey]= Session.CLS[Key]>
							<cfif StructKeyExists(Session, Key)>
								<cfset Session[Local.NewKey]= Session[Key]>
							</cfif>
						</cfif>
					</cfloop>
				<cfelseif	(NOT	IsDefined("Session.GLS"))
					and				IsDefined("Session.CLS")
					and				IsStruct  (Session.CLS)><!--- Only one or the other of these will be done. --->
					<cfset Session.GLS						= Session.CLS><!--- Assignment! (See end-of-cflock comment.) --->
					<cfset Local.StructKeys					= StructKeyList(Session.GLS)>
					<cfloop index="Key" list="#Local.StructKeys#">
						<cfif	(Len	(Key)				gt 3)
							and	(Left	(Key, 3)			is "CLS")>
							<cfset Local.NewKey				= "GLS" & RemoveChars(Key, 1, 3)>
							<cfset Session.GLS[Local.NewKey]= Session.GLS[Key]>
							<cfif StructKeyExists(Session, Key)>
								<cfset Session[Local.NewKey]= Session[Key]>
							</cfif>
						</cfif>
					</cfloop>
				</cfif>
				<!---
				In a mixed CLS/GLS environment, at this point, both Session.CLS and Session.GLS will exist and be identical.
				Moreover, if either is changed, both will change, since they're pointers to the same structure. That's why
				we had to use assignment (not Duplicate or StructCopy), above.
				--->
			</cflock>
		</cfif>
		<cfinclude template="#Variables.LibIncURL#/get_sbalookandfeel_variables.cfm">
		<cfif this.OnRequestStartInclude is not "none">
		<!---
			<cfinclude template="#this.OnRequestStartInclude#">
			--->
		</cfif>
		<cfreturn true><!--- Success. --->
	</cffunction>

	<cffunction name="onRequest"							returnType="void">
		<cfargument name="targetPage"						type="String" required=true/>
		<cfinclude template="#Arguments.targetPage#">
		<!--- Do not use cfreturn. --->
	</cffunction>

	<cffunction name="onRequestEnd"							returnType="void">
		<cfargument type="String"							name="targetPage" required=true/>
		<cfinclude template="#Variables.LibIncURL#/OnRequestEnd.cfm">
		<!--- Do not use cfreturn. --->
	</cffunction>

	<!--- No onSessionEnd yet. --->
	<!--- No onApplicationEnd yet. --->

</cfcomponent>

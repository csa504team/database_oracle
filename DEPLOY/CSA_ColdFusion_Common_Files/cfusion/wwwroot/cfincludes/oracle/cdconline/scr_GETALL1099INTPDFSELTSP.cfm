<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE GetAll1099IntPDFSelTSP 
 -- =============================================
 -- Author: <Author :SBA504>
 -- Create date: <Create :10-07-2016>
 -- Description: <Description: select 1099 INT TaxYr>
 -- Modified: <Modified :,>
 -- =============================================
 
 (
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM Tax1099IntPDFTbl 
 WHERE LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 ORDER BY NamAddrID ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


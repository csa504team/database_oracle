<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE LatePymt45SelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CDC_Cert IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 sp.BorrNm ,
 --sp.StmtNm,
 delq.LoanNmb ,
 delq.DueAmt ,
 delq.PymtDue ,
 delq.LASTUPDTDT
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.BorrNm ;
 ELSE
 IF LTRIM(RTRIM(p_SOD)) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 sp.BorrNm ,
 --sp.StmtNm,
 delq.LoanNmb ,
 delq.DueAmt ,
 delq.PymtDue ,
 delq.LASTUPDTDT
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 
 --WHERE UserLevelRoleID = @SOD
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY sp.BorrNm ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 --sp.BorrNm,
 sp.StmtNm ,
 delq.LoanNmb ,
 delq.DueAmt ,
 delq.PymtDue ,
 delq.LASTUPDTDT
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


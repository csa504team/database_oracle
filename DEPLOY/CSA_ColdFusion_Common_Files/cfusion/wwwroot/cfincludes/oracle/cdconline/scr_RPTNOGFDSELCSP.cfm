<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTNoGFDSelCSP 
 (
 p_cdc_num IN CHAR,
 p_sort_order IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR
 )
 AS
 ---------get third thirsday date ------------
 v_Day NUMBER(10,0);
 v_date_in DATE;
 v_third_thursday_day NUMBER(10,0);
 v_first_thursday DATE;
 v_first_day_of_month DATE;
 v_third_thursday DATE;
 v_third_thursday_next_month DATE;
 v_first_day_next_month DATE;
 
 BEGIN
 
 v_date_in := SYSDATE ;
 v_first_day_of_month := TRUNC(v_date_in, 'MONTH');
 v_first_day_next_month := ADD_MONTHS(v_first_day_of_month, 1);
 /*TODO:SQLDEV*/ --SET DATEFIRST 1 /*END:SQLDEV*/
 v_day := TO_NUMBER(TO_CHAR(TRUNC(v_first_day_of_month), 'D'));
 IF v_day > 4 THEN
 
 BEGIN
 v_day := 7 - v_day + 4 ;
 v_third_thursday_day := 28 ;
 
 END;
 ELSE
 
 BEGIN
 v_day := 4 - v_day ;
 v_third_thursday_day := 35 ;
 
 END;
 END IF;
 v_third_thursday := TRUNC(v_first_day_of_month + v_day + INTERVAL '14' DAY);
 /* 2. ------get records-------*/
 /*sort by Sba Loan Number*/
 IF LTRIM(RTRIM(p_sort_order)) = 'LoanNmb' THEN
 
 BEGIN
 ---Colson
 IF p_cdc_num = 0 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.LoanNmb,
 r.PrepayDt ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur1 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.LoanNmb,
 r.PrepayDt ;
 END IF;
 
 END;
 /*sort by Prepayment Date*/
 ELSE
 IF LTRIM(RTRIM(p_sort_order)) = 'PrepayDt' THEN
 
 BEGIN
 ---Colson
 IF p_cdc_num = 0 THEN
 
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.PrepayDt,
 r.LoanNmb ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur2 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.PrepayDt,
 r.LoanNmb ;
 END IF;
 
 END;
 /*sort by CDC Number*/
 ELSE
 
 BEGIN
 ---Colson
 IF p_cdc_num = 0 THEN
 
 BEGIN
 OPEN p_SelCur3 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY p.CDCRegnCd,
 p.CDCNmb,
 r.LoanNmb,
 r.PrepayDt ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur3 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND r.CDCPortflStatCdCd IN ( 'OP','CN' )
 
 AND p.IssDt IS NOT NULL
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(p.IssDt) < 0
 AND ( r.GFDRecv <> 'Y'
 OR r.GFDRecv IS NULL )
 AND TRUNC(r.PrepayDt) - TRUNC(v_third_thursday) >= 0
 AND TRUNC(v_first_day_next_month) - TRUNC(r.PrepayDt) > 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY p.CDCRegnCd,
 p.CDCNmb,
 r.LoanNmb,
 r.PrepayDt ;
 END IF;
 
 END;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


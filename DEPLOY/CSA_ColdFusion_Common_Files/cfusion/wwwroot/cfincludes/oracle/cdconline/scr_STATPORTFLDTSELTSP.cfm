<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatPortflDtSelTSP
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_portfolio_date DATE;
 v_portfolio_as_of_date DATE;
 v_ddate DATE;
 
 BEGIN
 
 --max issue date from the portfolio table
 SELECT ( SELECT MAX(IssDt)
 FROM PortflTbl )
 
 INTO v_ddate
 FROM DUAL ;
 --cal next month
 v_ddate := ADD_MONTHS(v_ddate, 1);
 --select @ddate as IssDt
 --calculate end of prior month
 v_portfolio_as_of_date := TRUNC(v_ddate) - (EXTRACT(DAY FROM v_ddate));
 --SELECT @portfolio_as_of_date as portfolio_as_of_date
 --calc portfolio date
 v_portfolio_date := v_portfolio_as_of_date + INTERVAL '1' DAY;
 --SELECT @portfolio_date as portfolio_date
 OPEN p_SelCur1 FOR
 SELECT v_portfolio_date portfolio_date ,
 v_portfolio_as_of_date balance_as_of_date
 FROM DUAL ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclAllowUpdCSP 
 (
 p_permission IN VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 /*
 accl_allow_update 3
 accl_allow_update 5
 accl_allow_update 9
 
 select getdate()
 */
 --select allow_update = 0
 IF p_permission = '9' THEN
 
 -- cs manager level
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT '1' allow_update 
 FROM DUAL ;
 
 END;
 ELSE
 DECLARE
 v_cur_date DATE;
 v_biz_day_num NUMBER(10,0);
 v_11th_date DATE;
 
 BEGIN
 v_cur_date := SYSDATE ;
 /*
 ZM: Updated to remove any reference to utils functions
 MSSQL: Select @cur_date = convert(smalldatetime, convert(varchar, month(@cur_date)) + '/01/' + convert(varchar, year(@cur_date)) )
 ORACLE: v_cur_date := UTILS.CONVERT_TO_SMALLDATETIME(UTILS.CONVERT_TO_VARCHAR2(utils.month_(v_cur_date),30) || '/01/' || UTILS.CONVERT_TO_VARCHAR2(utils.year_(v_cur_date),30));
 Note: 
 */
 v_cur_date := TRUNC(v_cur_date) - (TO_NUMBER(TO_CHAR(v_cur_date,'DD')) - 1);
 -- use the last day of the previous month in the case of the 1st of the month falls on holiday
 /*
 ZM: Updated to remove any reference to utils functions
 MSSQL: Select @cur_date = dateadd(dd, -1, @cur_date)
 ORACLE: v_cur_date := utils.dateadd('DD', -1, v_cur_date) ;
 */
 v_cur_date := TRUNC(ADD_MONTHS(v_cur_date, -1));
 v_biz_day_num := 11 ;
 v_11th_date := NULL ;
 UTLBusDtAddSelTSP (p_in_date => v_cur_date, p_biz_day => v_biz_day_num, p_out_date => v_11th_date) ;
 /*
 ZM: Updated to remove any reference to utils functions; this is trying to see the difference in the number of days, where positive implies it is in the future
 MSSQL: if datediff(d, getdate(), @11th_date) >= 0
 ORACLE: IF utils.datediff('D', SYSDATE, v_11th_date) >= 0 THEN
 */
 IF TRUNC(v_11th_date)-TRUNC(SYSDATE) >= 0 THEN
 OPEN p_SelCur1 FOR
 SELECT 1 allow_update 
 FROM DUAL ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT 0 allow_update -- temporary for 11th businees date
 -- select allow_update = 1
 FROM DUAL ;
 END IF;
 
 END;
 END IF;
 
 /*
 ZM: Updated to remove any reference to utils functions; handing errors
 MSSQL: (none)
 ORACLE: EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 */
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


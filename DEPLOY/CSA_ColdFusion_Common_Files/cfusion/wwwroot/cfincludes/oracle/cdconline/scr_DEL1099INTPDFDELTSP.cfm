<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE Del1099IntPDFDelTSP 
 -- =============================================
 -- Author: <Author :SBA504>
 -- Create date: <Create :10-07-2016>
 -- Description: <Description: select 1099 INT TaxYr>
 -- Modified: <Modified :,>
 -- =============================================
 
 (
 p_nmad_id IN CHAR
 )
 AS
 
 BEGIN
 
 DELETE Tax1099IntPDFTbl
 
 WHERE LTRIM(RTRIM(NamAddrID)) = LTRIM(RTRIM(p_nmad_id));
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


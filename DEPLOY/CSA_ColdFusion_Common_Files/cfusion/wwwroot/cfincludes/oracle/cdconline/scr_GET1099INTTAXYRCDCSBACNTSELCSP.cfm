<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE Get1099IntTaxYrCDCSBACntSelCSP 
 -- =============================================
 -- Author: <Author :SBA504>
 -- Create date: <Create :10-07-2016>
 -- Description: <Description: select 1099 INT TaxYr>
 -- Modified: <Modified :,>
 -- =============================================
 
 (
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT NamAddrID 
 FROM Tax1099IntPDFTbl 
 WHERE LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 AND NamAddrID = ( SELECT NamAddrID 
 FROM NMADCrosrefTbl 
 WHERE CDCNmb IS NULL
 AND CDCRegnCd IS NULL ) ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


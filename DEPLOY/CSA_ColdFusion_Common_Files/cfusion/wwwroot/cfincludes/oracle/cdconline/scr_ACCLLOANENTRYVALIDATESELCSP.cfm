<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclLoanEntryValidateSelCSP
 (
 p_sba_loan_no IN CHAR,
 p_serv_center_id IN VARCHAR2 DEFAULT '' ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 
 -- accl_loan_entry_validate'2814616002'
 -- accl_loan_entry_validate'1183494009'
 -- accl_loan_entry_validate'34234'
 -- accl_loan_entry_validate'2222416008'
 --
 -- declare @LoanNmb char(10), @ServCntrID char(4)
 -- set @LoanNmb='2222416008'
 -- set @ServCntrID = 'sc02'
 --
 -- get cdc records based on the servicing center, if cust. svc. get all cdc's
 INSERT INTO TTVCDC2Tbl
 ( CDCRegnCd, CDCNmb, CreatUserID, LastUpdtUserID)
 ( SELECT DISTINCT CDCRegnCd ,
 CDCNmb ,
 USER ,
 USER
 FROM ServCntrTbl
 WHERE ( ( p_serv_center_id IS NULL )
 OR ( ServCntrID = p_serv_center_id ) ) );
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE NOT EXISTS ( SELECT 'Y'
 FROM LoanTbl l
 JOIN TTVCDC2Tbl c ON l.CDCRegnCd = c.CDCRegnCd
 AND l.CDCNmb = c.CDCNmb
 WHERE l.LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT 'does not exist.' err_msg
 FROM DUAL ;
 
 END;
 ELSE
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE NOT EXISTS ( SELECT 'Y'
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LoanStatCd IN ( 'ACTIVE','CATCH-UP','DEFERRED' )
 );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT 'inactive loan' err_msg
 FROM DUAL ;
 
 END;
 ELSE
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'Y'
 FROM AcclLoanEntryTbl
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT 'duplicate loan' err_msg
 FROM DUAL ;
 
 END;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT ' ' err_msg
 FROM DUAL ;
 END IF;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


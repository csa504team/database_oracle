<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE GetLateFeeDisbDtSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DueFromBorrClsDt StmtDt ,
 EXTRACT(MONTH FROM DueFromBorrClsDt) disbursed_month ,
 EXTRACT(YEAR FROM DueFromBorrClsDt) disbursed_year 
 FROM LateFeeDisbTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 ORDER BY DueFromBorrClsDt DESC ;/*
 SELECT DISTINCT DFBClsDt as StmtDt
 FROM CDCLateFeeDisbTbl
 WHERE CDCRegn = @region_num
 AND CDCNmb = @CDCNmb
 ORDER BY DFBClsDt DESC
 */
 /*
 SELECT DISTINCT 
 disbursed_month = DATEPART(mm,DFBClsDt), 
 disbursed_year = DATEPART(yy,DFBClsDt),
 DFBClsDt as StmtDt
 FROM [webdb].[dbo].[CDCLateFeeDisbTbl]
 WHERE CDCRegn = @region_num
 AND CDCNmb = @CDCNmb
 ORDER BY 
 DATEPART(yy,DFBClsDt) DESC,
 DATEPART(mm,DFBClsDt) DESC
 */
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


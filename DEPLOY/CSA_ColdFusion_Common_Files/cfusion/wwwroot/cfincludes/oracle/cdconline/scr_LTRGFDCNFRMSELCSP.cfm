<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LtrGFDCnfrmSelCSP 
 (
 p_sba_loan_no IN CHAR,
 p_prepay_date IN CHAR,
 p_req_code IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_status CHAR(2);
 v_contact_name VARCHAR2(30);
 
 BEGIN
 
 SELECT CDCPortflStatCdCd 
 
 INTO v_status
 FROM RqstTbl 
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND RqstCd = p_req_code;
 IF v_status = 'OP' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT LTRIM(RTRIM(NVL(a.CDCNm, ' '))) name ,
 LTRIM(RTRIM(a.CDCMailAdrStr1Nm)) street ,
 LTRIM(RTRIM(a.CDCMailAdrStr2Nm)) street1 ,
 LTRIM(RTRIM(a.CDCMailAdrCtyNm)) city ,
 a.CDCMailAdrStCd state ,
 a.CDCMailAdrZipCd zip ,
 CASE NVL(LTRIM(RTRIM(r.RqstFrom)), ' ')
 WHEN ' ' THEN a.CntctNm
 ELSE LTRIM(RTRIM(r.RqstFrom))
 END CntctNm ,
 NVL(CASE LTRIM(l.SmllBusCons)
 WHEN ' ' THEN l.BorrNm
 ELSE l.SmllBusCons
 END, l.BorrNm) SmllBusCons ,
 l.LoanNmb ,
 r.PrepayDt ,
 r.FaxNmb 
 FROM RqstTbl r
 LEFT JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 LEFT JOIN AdrTbl a ON l.CDCNmb = a.CDCNmb
 AND l.CDCRegnCd = a.CDCRegnCd
 WHERE r.LoanNmb = p_sba_loan_no
 
 ---and convert(varchar(10),r.PrepayDt,101)= convert(varchar(10),convert(smalldatetime,@PrepayDt),101)
 AND TRUNC(r.PrepayDt) = TRUNC(TO_DATE(p_prepay_date)) 
 AND r.RqstCd = p_req_code
 AND r.GFDRecv = 'Y' ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT LTRIM(RTRIM(NVL(a.CDCNm, ' '))) name ,
 LTRIM(RTRIM(a.CDCMailAdrStr1Nm)) street ,
 LTRIM(RTRIM(a.CDCMailAdrStr2Nm)) street1 ,
 LTRIM(RTRIM(a.CDCMailAdrCtyNm)) city ,
 a.CDCMailAdrStCd state ,
 a.CDCMailAdrZipCd zip ,
 CASE NVL(LTRIM(RTRIM(r.RqstFrom)), ' ')
 WHEN ' ' THEN a.CntctNm
 ELSE LTRIM(RTRIM(r.RqstFrom))
 END CntctNm ,
 NVL(CASE LTRIM(l.SmllBusCons)
 WHEN ' ' THEN l.BorrNm
 ELSE l.SmllBusCons
 END, l.BorrNm) SmllBusCons ,
 l.LoanNmb ,
 r.PrepayDt ,
 r.FaxNmb 
 FROM RqstTbl r
 LEFT JOIN LoanSaveTbl l ON r.LoanNmb = l.LoanNmb
 LEFT JOIN AdrTbl a ON l.CDCNmb = a.CDCNmb
 AND l.CDCRegnCd = a.CDCRegnCd
 WHERE r.LoanNmb = p_sba_loan_no
 
 ---and convert(varchar(10),r.PrepayDt,101)= convert(varchar(10),convert(smalldatetime,@PrepayDt),101)
 AND TRUNC(r.PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND r.RqstCd = p_req_code
 AND r.GFDRecv = 'Y' ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


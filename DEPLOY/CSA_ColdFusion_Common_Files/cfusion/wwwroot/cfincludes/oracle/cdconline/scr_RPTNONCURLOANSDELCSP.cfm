<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTNoncurLoansDelCSP 
 --cdc_rpt_noncur_loans 0,0,'LoanNmb'
 
 (
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_sort_order IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTemp12Tbl;
 /*Colson users*/
 IF RTRIM(LTRIM(p_cdc_num)) = '0' THEN
 
 BEGIN
 /*get records from CDCLoanTbl table*/
 INSERT INTO TTTemp12Tbl (LoanNmb,BorrNm,CDCNmb,CDCNm,PrepymtDt,CreatUserID,LastUpdtUserID)
 ( SELECT r.LoanNmb ,
 l.BorrNm ,
 RTRIM(LTRIM(l.CDCRegnCd)) || '-' || RTRIM(LTRIM(l.CDCNmb)) CDCNmb ,
 NULL CDCNm ,
 r.PrepayDt ,
 USER ,
 USER 
 FROM RqstTbl r
 JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 WHERE CDCPortflStatCdCd = 'OP'
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 AND l.LoanStatCd = 'ACTIVE'
 AND l.CurBalNeededAmt > 0 );
 /*in case loan was removed from CDCLoanTbl table get records from CDCLoanSaveTbl table*/
 INSERT INTO TTTemp12Tbl (LoanNmb,BorrNm,CDCNmb,CDCNm,PrepymtDt,CreatUserID,LastUpdtUserID)
 ( SELECT r.LoanNmb ,
 l.BorrNm ,
 RTRIM(LTRIM(l.CDCRegnCd)) || '-' || RTRIM(LTRIM(l.CDCNmb)) CDCNmb ,
 NULL CDCNm ,
 r.PrepayDt ,
 USER ,
 USER
 FROM RqstTbl r
 JOIN LoanSaveTbl l ON r.LoanNmb = l.LoanNmb
 WHERE CDCPortflStatCdCd = 'OP'
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 AND l.LoanStatCd = 'ACTIVE'
 AND l.CurBalNeededAmt > 0
 AND r.LoanNmb NOT IN ( SELECT r.LoanNmb 
 FROM RqstTbl r
 JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 WHERE l.CurBalNeededAmt >= 0 )
 );
 /*get CDCNm*/
 MERGE INTO TTTemp12Tbl t
 USING (SELECT t.ROWID row_id, RTRIM(LTRIM(p.CDCNm)) AS CDCNm
 FROM PortflTbl p
 JOIN TTTemp12Tbl t ON p.LoanNmb = t.LoanNmb ) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 MERGE INTO TTTemp12Tbl t
 USING (SELECT t.ROWID row_id, RTRIM(LTRIM(p.CDCNm)) AS CDCNm
 FROM PortflTbl p
 JOIN TTTemp12Tbl t ON p.LoanNmb = t.LoanNmb 
 WHERE t.CDCNm IS NULL) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 
 END;
 /*Clients*/
 ELSE
 
 BEGIN
 /*get records from CDCLoanTbl table*/
 INSERT INTO TTTemp12Tbl (LoanNmb,BorrNm,CDCNmb,CDCNm,PrepymtDt,CreatUserID,LastUpdtUserID)
 ( SELECT r.LoanNmb ,
 l.BorrNm ,
 RTRIM(LTRIM(l.CDCRegnCd)) || '-' || RTRIM(LTRIM(l.CDCNmb)) CDCNmb ,
 NULL CDCNm ,
 r.PrepayDt ,
 USER ,
 USER
 FROM RqstTbl r
 JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 WHERE CDCPortflStatCdCd = 'OP'
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 AND l.LoanStatCd = 'ACTIVE'
 AND l.CurBalNeededAmt > 0
 AND LTRIM(RTRIM(l.CDCRegnCd)) = p_cdc_region
 AND LTRIM(RTRIM(l.CDCNmb)) = p_cdc_num );
 /*if loan was removed from CDCLoanTbl table get records from CDCLoanSaveTbl table*/
 INSERT INTO TTTemp12Tbl (LoanNmb,BorrNm,CDCNmb,CDCNm,PrepymtDt,CreatUserID,LastUpdtUserID)
 ( SELECT r.LoanNmb ,
 l.BorrNm ,
 RTRIM(LTRIM(l.CDCRegnCd)) || '-' || RTRIM(LTRIM(l.CDCNmb)) CDCNmb ,
 NULL CDCNm ,
 r.PrepayDt ,
 USER ,
 USER
 FROM RqstTbl r
 JOIN LoanSaveTbl l ON r.LoanNmb = l.LoanNmb
 WHERE CDCPortflStatCdCd = 'OP'
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 AND l.LoanStatCd = 'ACTIVE'
 AND l.CurBalNeededAmt > 0
 AND LTRIM(RTRIM(l.CDCNmb)) = p_cdc_num
 AND r.LoanNmb NOT IN ( SELECT r.LoanNmb 
 FROM RqstTbl r
 JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 WHERE l.CurBalNeededAmt >= 0
 AND LTRIM(RTRIM(l.CDCRegnCd)) = p_cdc_region
 AND LTRIM(RTRIM(l.CDCNmb)) = p_cdc_num )
 );
 /*get CDCNm*/
 MERGE INTO TTTemp12Tbl t
 USING (SELECT t.ROWID row_id, RTRIM(LTRIM(p.CDCNm)) AS CDCNm
 FROM PortflTbl p
 JOIN TTTemp12Tbl t ON p.LoanNmb = t.LoanNmb ) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 MERGE INTO TTTemp12Tbl t
 USING (SELECT t.ROWID row_id, RTRIM(LTRIM(p.CDCNm)) AS CDCNm
 FROM PortflTbl p
 JOIN TTTemp12Tbl t ON p.LoanNmb = t.LoanNmb 
 WHERE t.CDCNm IS NULL) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 
 END;
 END IF;
 DELETE TTTemp12Tbl
 WHERE ROWID IN 
 ( SELECT TTTemp12Tbl.ROWID
 FROM TTTemp12Tbl t
 JOIN LoanTbl l ON t.LoanNmb = l.LoanNmb,
 TTTemp12Tbl
 WHERE LoanStatCd = 'PREPAID' );
 /* Sort */
 IF p_sort_order = 'LoanNmb' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTTemp12Tbl 
 ORDER BY LoanNmb ASC ;
 
 END;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTTemp12Tbl 
 ORDER BY PrepymtDt DESC ;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


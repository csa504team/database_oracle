<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetSpecific1099MISCPDFSelTSP 
 (
 p_nmad_id IN CHAR,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM Tax1099MiscPDFTbl 
 WHERE LTRIM(RTRIM(NamAddrID)) = LTRIM(RTRIM(p_nmad_id))
 AND LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 ORDER BY NamAddrID ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AnnStmtGetLoanSelTSP 
 (
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT (LoanNmb) 
 
 --from CDCAnnStmtLoanInfoTbl
 FROM AnnStmtRecipntTbl 
 WHERE CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 AND TaxYrNmb = p_tax_year
 ORDER BY LoanNmb ASC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:35. --->
PROCEDURE AcclTrnscrptGen2TBLCSP
 (p_retval out number, 
 p_errval out number, 
 p_errmsg out varchar2, 
 p_identifier number:=0, 
 p_userid varchar2, 
 p_rpt_start_dt date, 
 p_rpt_end_dt date) AS
 --
 pgm varchar2(30):='ACCLTRNSCRPTGENDEL4JLCSP';
 ver varchar2(40):='V1.0 15 Dec 2017'; 
 dtfmt varchar2(40):='YYYY-MM-DD HH24:MI:SS';
 log_LOANNMB char(10):=null;
 log_TRANSID number:=null;
 log_dbmdl varchar2(10):=null;
 log_entrydt date:=null;
 log_userid varchar2(32):=null;
 log_usernm varchar2(199):=null;
 log_timestampfld date:=null; 
 logged_msg_id number;
 -- accl_transcript_generate_r 2 -- 'cdccsRV1'
 v_sys_lasttime DATE;
 v_ws_sba_loan_no CHAR(10);
 v_issue_date_char CHAR(10);
 v_issue_date DATE;
 v_payment_type CHAR(2);
 v_posting_date DATE;
 v_sba_fee NUMBER(19,4);
 v_csa_fee NUMBER(19,4);
 v_cdc_fee NUMBER(19,4);
 v_interest_amount NUMBER(19,4);
 v_principal_amount NUMBER(19,4);
 v_unalloc_amount NUMBER(19,4);
 v_balance NUMBER(19,4);
 v_late_fee NUMBER(19,4);
 v_dfb_amount NUMBER(19,4);
 v_accr_sba_fee NUMBER(19,4);
 v_accr_csa_fee NUMBER(19,4);
 v_accr_cdc_fee NUMBER(19,4);
 v_accr_late_fee NUMBER(19,4);
 v_accr_int NUMBER(19,4);
 v_accr_prin NUMBER(19,4);
 v_m_sba_fee -- monthly fee starts with m_
 NUMBER(19,4);
 v_m_csa_fee NUMBER(19,4);
 v_m_cdc_fee NUMBER(19,4);
 v_m_late_fee NUMBER(19,4);
 v_m_int NUMBER(19,4);
 v_m_prin NUMBER(19,4);
 v_pymt_no NUMBER(10,0);
 v_ws_pymt_no NUMBER(10,0);
 v_month_diff NUMBER(10,0);
 v_balance_used NUMBER(19,4);
 v_cdc_pct NUMBER(8,5);
 v_sba_pct NUMBER(8,5);
 v_csa_pct NUMBER(8,5);
 v_late_pct NUMBER(8,5);
 v_note_rate NUMBER(8,5);
 v_monthly_escrow NUMBER(19,4);
 v_ws_balance NUMBER(19,4);
 v_ws_posting_date DATE;
 v_semi_annual_date DATE;
 v_last_pymt_date DATE;
 v_max_pymt_no NUMBER(10,0);
 v_int_due_days NUMBER(5,0);
 v_int_paid_days NUMBER(5,0);
 v_loan_status CHAR(8);
 v_late_fee_paid_date -- last date of late fee posting
 DATE;
 v_pymt_amt NUMBER(19,4);
 v_m1_int_days NUMBER(3,0);
 -- accl_transcript_generate_r
 -- CSTitl, CSPhn
 v_cs_title VARCHAR2(30);
 v_cs_phone VARCHAR2(25);
 -- resultset + wire info
 v_trustee_name VARCHAR2(80);
 v_aba_number VARCHAR2(20);
 v_account_number VARCHAR2(20);
 v_account_name VARCHAR2(50);
 v_wire_re VARCHAR2(100);
 v_recip_name VARCHAR2(80);
 v_recip_name_1 VARCHAR2(80);
 v_recip_salutation VARCHAR2(10);
 v_recip_contact VARCHAR2(50);
 v_recip_street_1 VARCHAR2(100);
 v_recip_street_2 VARCHAR2(100);
 v_recip_city VARCHAR2(40);
 v_recip_state CHAR(10);
 v_recip_zip CHAR(10);
 v_sender_name VARCHAR2(80);
 v_sender_street_1 VARCHAR2(100);
 v_sender_street_2 VARCHAR2(100);
 v_sender_city VARCHAR2(40);
 v_sender_state CHAR(10);
 v_sender_zip CHAR(10);
 v_trustee_finacial_street_1 -- newly added for ALM 1049
 VARCHAR2(100);
 v_trustee_finacial_street_2 VARCHAR2(100);
 v_trustee_finacial_city VARCHAR2(40);
 v_trustee_finacial_state CHAR(10);
 v_trustee_finacial_zip CHAR(10);
 CURSOR cur_trans
 IS SELECT DISTINCT LoanNmb ,
 IssDt ,
 CDCPct ,
 SBAPct ,
 CSAPct ,
 LatePct ,
 NoteRtPct ,
 MoEscrowAmt ,
 LastPymtDt ,
 IntDueDays ,
 IntPaidDays ,
 LoanStatCd ,
 SemiAnnDt
 FROM TTTransTbl ;
 
 BEGIN
 p_errval:=0;
 p_retval:=0;
 p_errmsg:=pgm||' '||ver||': Normal completion';
 --
 
 
 
 /*
 CREATE OR REPLACE procedure STGCSA.runtime_logger(
 logentryid out number,
 msg_cat varchar2,
 msg_id number,
 msg_txt varchar2,
 userid varchar2,
 log_sev number:=1,
 usernm varchar2:=null,
 loannmb char:=null,
 transid number:=null,
 dbmdl varchar:=null,
 entrydt date:=null,
 timestampfield date:=null,
 logtxt1 varchar2:=null,
 logtxt2 varchar2:=null, 
 logtxt3 varchar2:=null, 
 logtxt4 varchar2:=null, 
 logtxt5 varchar2:=null, 
 lognmb1 number:=null,
 lognmb2 number:=null, 
 lognmb3 number:=null, 
 lognmb4 number:=null, 
 lognmb5 number:=null, 
 logdt1 date:=null,
 logdt2 date:=null, 
 logdt3 date:=null, 
 logdt4 date:=null, 
 logdt5 date:=null) */
 stgcsa.runtime_logger(logged_msg_id,'STDLOG',100,
 pgm||' '||ver||' entered, P_ID='||P_IDENTIFIER|| 
 ' Start_dt='||to_char(p_rpt_start_dt,dtfmt)||' End_dt='||to_char(p_rpt_end_dt,dtfmt),
 p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
 if p_identifier<> 0 then
 p_errval:=-20001;
 p_errmsg:=PGM||' '||ver||' called with invalid p_identifier: '||P_IDENTIFIER
 ||' Only supported value is 0';
 end if;
 if p_errval=0 then 
 begin 
 ----prod
 v_sys_lasttime := SYSDATE ;-- '5/18/09'
 DELETE FROM TTTransTbl;
 DELETE FROM TTPymtHistTmpTbl;
 DELETE FROM TTPymtHistTbl;
 DELETE FROM TTPymtMoTbl;
 DELETE FROM TTPymtNoTbl;
 DELETE FROM TTAccrTbl;
 DELETE FROM TTMoTbl;
 DELETE FROM TT5YrTbl;
 DELETE FROM TTEscrowTbl;
 DELETE FROM TTAdjTbl;
 delete from TTtranscriptrpt1;
 delete from TTtranscriptrpt2;
 ----test
 ---select@SYSLastTime= '10/26/2012'
 -- CDCTTAdjTbl to calculate the accrued Int if there is an adjustment after the last pymt date
 -- get loan for the process month
 INSERT INTO TTTransTbl
 ( RefID, RqstDt, LoanNmb, SemiAnnDt, AccrIntASOFDt, DbentrPrinBalAmt, DbentrIntDueAmt, CreatUserID, LastUpdtUserID )
 ( SELECT e.CDCAcclLoanEntryTblPK ,
 e.RqstDt ,
 e.LoanNmb ,
 e.SemiAnnDt ,
 CASE EXTRACT(MONTH FROM e.SemiAnnDt) WHEN 3 THEN
 TRUNC(e.SemiAnnDt) - INTERVAL '1' DAY
 ELSE
 TO_DATE(EXTRACT(MONTH FROM ADD_MONTHS(e.SemiAnnDt, -1)) || '/30/' || EXTRACT(YEAR FROM ADD_MONTHS(e.SemiAnnDt, -1)), 'MM/DD/YYYY')
 END AccrIntASOFDt ,
 e.DbentrPrinBalAmt ,
 e.DbentrIntDueAmt ,
 USER ,
 USER
 FROM AcclLoanEntryTbl e,
 LoanTbl l
 WHERE e.LoanNmb = l.LoanNmb
 AND l.LoanStatCd NOT IN ( 'PREPAID' )
 
 AND e.RqstDt BETWEEN p_rpt_start_dt AND p_rpt_end_dt );
 ---and datediff(m, e.RqstDt, @SYSLastTime) = 0 --prod
 ------- datediff(m, e.RqstDt, @SYSLastTime) <= 1-- 11/17/09 display this month and last month's acceleration
 --and datediff(m, e.RqstDt, @SYSLastTime) = 1
 -- (l.LoanStatnot in ('ACCELER.', 'PREPAID') or
 -- datediff(m, e.RqstDt, @SYSLastTime) = 1--show prior month
 --)
 /*
 ande.LoanNmb in ('2518106008', '6496144002', '2708366006', '7772614003',
 '2617406010', '9496384008', '3155806000', '2862576008', '2994156002', '8808714006'-- UAT test loans
 , '6207804004', '2658536010', '6048223002', '2627294004')-- not part of UAT
 */
 -- accl_transcript_generate_r
 -- ande.LoanNmb = '2304304000'--'2500284004'
 -- get loan information
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, l.SmllBusCons, l.SmllBusCons AS SmllBusCons2, l.CDCRegnCd, l.CDCNmb, l.IssDt, l.MatDt, l.NotePrinAmt, l.NoteRtPct, NVL(l.AmortIntAmt, 0) + NVL(l.AmortPrinAmt, 0) AS pos_10, l.IntNeededAmt, l.PrinNeededAmt,
 l.CDCFeeNeededAmt, l.SBAFeeNeededAmt, l.CSAFeeNeededAmt, l.LateFeeNeededAmt, l.CDCPct, l.SBAPct, l.LoanStatCd
 FROM TTTransTbl t ,LoanTbl l
 WHERE t.LoanNmb = l.LoanNmb) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET SmllBusCons = src.SmllBusCons,
 StmtNm = src.SmllBusCons2,
 CDCRegnCd = src.CDCRegnCd,
 CDCNmb = src.CDCNmb,
 IssDt = src.IssDt,
 MatDt = src.MatDt,
 NotePrinAmt = src.NotePrinAmt,
 NoteRtPct = src.NoteRtPct,
 MoEscrowAmt = src.pos_10,
 IntNeededAmt = src.IntNeededAmt,
 PrinNeededAmt = src.PrinNeededAmt,
 CDCFeeNeededAmt = src.CDCFeeNeededAmt,
 SBAFeeNeededAmt = src.SBAFeeNeededAmt,
 CSAFeeNeededAmt = src.CSAFeeNeededAmt,
 LateFeeNeededAmt = src.LateFeeNeededAmt,
 CDCPct = src.CDCPct,
 SBAPct = src.SBAPct,
 LoanStatCd = src.LoanStatCd;
 /*
 -- for deferred loans, use the amounts needed from the portfolio
 updateCDCTTTransTbl
 setIntNeeded= p.IntDueAmt,
 PrinNeeded= p.PrinDueAmt,
 CDCFeeNeeded= p.CDCFee,
 SBAFeeNeeded= p.SBAFee,
 CSAFeeNeeded= p.CSAFee,
 LateFeeNeeded= p.LateFeeDueAmt
 fromCDCTTTransTblt,
 CDCPortflTblp
 wheret.LoanNmb= p.LoanNmb
 andt.LoanStat= 'DEFERRED'
 */
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, l.BorrNm, l.BorrNm AS BorrNm2
 FROM TTTransTbl t ,LoanTbl l
 WHERE t.LoanNmb = l.LoanNmb
 AND ( LTRIM(RTRIM(t.SmllBusCons)) = ''
 OR LTRIM(RTRIM(t.SmllBusCons)) IS NULL )) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET SmllBusCons = src.BorrNm,
 StmtNm = src.BorrNm2;
 -- CSA pct
 MERGE INTO TTTransTbl
 USING (SELECT TTTransTbl.ROWID row_id, CDCRtPct
 FROM TTTransTbl ,RtTbl
 WHERE CDCRtCd = 6) src
 ON ( TTTransTbl.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CSAPct = CDCRtPct;
 -- late pct
 MERGE INTO TTTransTbl
 USING (SELECT TTTransTbl.ROWID row_id, CDCRtPct
 FROM TTTransTbl ,RtTbl
 WHERE CDCRtCd = 7) src
 ON ( TTTransTbl.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET LatePct = CDCRtPct;
 -- debenture rate
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
 FROM TTTransTbl t ,( SELECT CDC5YrBreakTbl.LoanNmb ,
 MAX(CDC5YrBreakTbl.PymtDt) PymtDt
 FROM CDC5YrBreakTbl
 WHERE CDC5YrBreakTbl.PymtDt IS NOT NULL
 GROUP BY CDC5YrBreakTbl.LoanNmb ) M ,CDC5YrBreakTbl b
 WHERE t.LoanNmb = M.LoanNmb
 AND M.LoanNmb = b.LoanNmb
 AND M.PymtDt = b.PymtDt) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
 IntDueDays = src.IntDueDays,
 IntPaidDays = src.IntPaidDays;
 -- debenture rate for loans without any payments
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
 FROM TTTransTbl t ,CDC5YrBreakTbl b
 WHERE t.LoanNmb = b.LoanNmb
 AND t.DbentrRtPct IS NULL) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
 IntDueDays = src.IntDueDays,
 IntPaidDays = src.IntPaidDays;
 -- insert from pymt (with date posted prior to current run date) for the loans in CDCTTTransTbl
 INSERT INTO TTPymtHistTmpTbl
 ( SELECT p.*
 FROM TTTransTbl t,
 PymtHistryTbl p
 WHERE t.LoanNmb = p.LoanNmb
 AND ( p.PymtTyp IN ( 'A','C','W','X' )
 
 OR p.PymtTyp LIKE '%R%' )
 AND TRUNC(v_sys_lasttime) - TRUNC(p.PostingDt) >= 0 );
 -- add fees if there is due from borrower receivable
 MERGE INTO TTPymtHistTmpTbl h
 USING (SELECT h.ROWID row_id, h.SBAFeeAmt + b.PostingAmt AS SBAFee
 FROM TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
 b1.DtCls ,
 SUM(b1.PostingAmt) PostingAmt
 FROM DueFromBorrTbl b1,
 TTPymtHistTmpTbl h1
 WHERE b1.LoanNmb = h1.LoanNmb
 AND h1.PostingDt = b1.DtCls
 AND h1.DueFromBorrAmt > 0
 AND b1.RecrdTyp = 7 -- sba code
 
 AND b1.StatCd = 9
 GROUP BY b1.LoanNmb,b1.DtCls ) b
 WHERE h.LoanNmb = b.LoanNmb
 AND h.PostingDt = b.DtCls
 AND h.DueFromBorrAmt > 0) src
 ON ( h.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET SBAFeeAmt = src.SBAFee;
 MERGE INTO TTPymtHistTmpTbl h
 USING (SELECT h.ROWID row_id, h.CSAFeeAmt + b.PostingAmt AS CSAFee
 FROM TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
 b1.DtCls ,
 SUM(b1.PostingAmt) PostingAmt
 FROM DueFromBorrTbl b1,
 TTPymtHistTmpTbl h1
 WHERE b1.LoanNmb = h1.LoanNmb
 AND h1.PostingDt = b1.DtCls
 AND h1.DueFromBorrAmt > 0
 AND b1.RecrdTyp = 1 -- csa code
 
 AND b1.StatCd = 9
 GROUP BY b1.LoanNmb,b1.DtCls ) b
 WHERE h.LoanNmb = b.LoanNmb
 AND h.PostingDt = b.DtCls
 AND h.DueFromBorrAmt > 0) src
 ON ( h.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CSAFeeAmt = src.CSAFee;
 MERGE INTO TTPymtHistTmpTbl h
 USING (SELECT h.ROWID row_id, h.CDCFeeAmt + b.PostingAmt AS CDCFeeAmt
 FROM TTPymtHistTmpTbl h ,( SELECT b1.LoanNmb ,
 b1.DtCls ,
 SUM(b1.PostingAmt) PostingAmt
 FROM DueFromBorrTbl b1,
 TTPymtHistTmpTbl h1
 WHERE b1.LoanNmb = h1.LoanNmb
 AND h1.PostingDt = b1.DtCls
 AND h1.DueFromBorrAmt > 0
 AND b1.RecrdTyp = 2 -- cdc code
 
 AND b1.StatCd = 9
 GROUP BY b1.LoanNmb,b1.DtCls ) b
 WHERE h.LoanNmb = b.LoanNmb
 AND h.PostingDt = b.DtCls
 AND h.DueFromBorrAmt > 0) src
 ON ( h.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCFeeAmt = src.CDCFeeAmt;
 -- last pymt date
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, h.LastPymtDt
 FROM TTTransTbl t ,( SELECT TTPymtHistTmpTbl.LoanNmb ,
 MAX(TTPymtHistTmpTbl.PostingDt) LastPymtDt
 FROM TTPymtHistTmpTbl
 WHERE TTPymtHistTmpTbl.PymtTyp IN ( 'A','C','W' )
 
 GROUP BY TTPymtHistTmpTbl.LoanNmb ) h
 WHERE t.LoanNmb = h.LoanNmb) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET LastPymtDt = src.LastPymtDt;
 -- last Bal
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, h.BalAmt
 FROM TTTransTbl t ,TTPymtHistTmpTbl h ,( SELECT TTPymtHistTmpTbl.LoanNmb ,
 MAX(TTPymtHistTmpTbl.PostingDt) PostingDt
 FROM TTPymtHistTmpTbl
 WHERE TTPymtHistTmpTbl.PymtTyp IN ( 'A','C','W','X' )
 
 GROUP BY TTPymtHistTmpTbl.LoanNmb ) D
 WHERE t.LoanNmb = D.LoanNmb
 AND t.LoanNmb = h.LoanNmb
 AND h.PostingDt = D.PostingDt) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET LastBalAmt = src.BalAmt;
 -- for loans without any payments
 UPDATE TTTransTbl
 SET LastBalAmt = NotePrinAmt
 WHERE LastBalAmt IS NULL;
 OPEN cur_trans;
 FETCH cur_trans INTO v_ws_sba_loan_no,v_issue_date_char,v_cdc_pct,v_sba_pct,v_csa_pct,v_late_pct,v_note_rate,v_monthly_escrow,v_last_pymt_date,v_int_due_days,v_int_paid_days,v_loan_status,v_semi_annual_date;
 
 WHILE cur_trans%FOUND
 LOOP
 DECLARE
 CURSOR cur_pymt
 IS SELECT PymtTyp ,
 PostingDt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 IntAmt ,
 PrinAmt ,
 UnallocAmt ,
 LateFeeAmt ,
 DueFromBorrAmt
 FROM TTPymtHistTmpTbl
 WHERE LoanNmb = v_ws_sba_loan_no
 AND ( PymtTyp IN ( 'A','C','W','X' )
 
 OR ( PymtTyp LIKE '%R%'
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(PostingDt), LAST_DAY(v_last_pymt_date))) > 0 ) )
 ORDER BY PostingDt;
 
 BEGIN
 -- insert loan funded record
 IF v_issue_date_char IS NOT NULL THEN
 
 BEGIN
 v_issue_date := TRUNC(TO_DATE(v_issue_date_char)) ;
 -- Mainframe added 1 day to Int basis day to calculate
 -- the first month Int and turn into production
 -- approx. on 9/30/1999.
 -- update the cutoff date to 8/10/1999 due to production for loan 2500284004 - updated on 11/17/09
 IF v_issue_date - TO_DATE('08/10/1999', 'mm/dd/yyyy') >= 1 THEN
 v_m1_int_days := 30 - EXTRACT(DAY FROM v_issue_date) + 1 ;
 ELSE
 v_m1_int_days := 30 - EXTRACT(DAY FROM v_issue_date) ;
 END IF;
 
 END;
 
 -- accl_transcript_generate_r
 ELSE
 
 BEGIN
 v_issue_date := NULL ;
 
 END;
 END IF;
 IF v_last_pymt_date IS NULL THEN
 
 BEGIN
 v_last_pymt_date := v_issue_date ;
 
 END;
 END IF;
 INSERT INTO TTPymtHistTbl
 ( LoanNmb, PostingDt, PymtNmb, CreatUserID, LastUpdtUserID )
 ( SELECT DISTINCT v_ws_sba_loan_no LoanNmb ,
 v_issue_date PostingDt ,
 0 pymt_no ,
 USER ,
 USER
 FROM TTTransTbl );
 SELECT 0 ,
 NotePrinAmt
 
 INTO v_ws_pymt_no,
 v_ws_balance
 FROM TTTransTbl
 WHERE LoanNmb = v_ws_sba_loan_no;
 -- initial monthly Int and Prin
 v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * v_m1_int_days, 2) ;
 SELECT MoEscrowAmt - v_m_int
 
 INTO v_m_prin
 
 --,@PymtAmt= Pymt160
 FROM TTTransTbl
 WHERE LoanNmb = v_ws_sba_loan_no;
 v_accr_sba_fee := 0 ;
 v_accr_csa_fee := 0 ;
 v_accr_cdc_fee := 0 ;
 v_accr_int := 0 ;
 v_accr_prin := 0 ;
 /*
 -- initial monthly Int, Prin and fees
 select@balance_used= NotePrin,
 fromCDCTTTransTbl
 whereLoanNmb= @ws_sba_loan_no
 */
 SELECT SUM(PostingAmt) ,
 MAX(PostingDt)
 
 INTO v_accr_late_fee,
 v_late_fee_paid_date
 FROM DueFromBorrTbl
 WHERE LoanNmb = v_ws_sba_loan_no
 AND RecrdTyp = 3
 AND StatCd = 1;
 v_accr_late_fee := NVL(v_accr_late_fee, 0) ;
 v_late_fee_paid_date := NVL(v_late_fee_paid_date, v_issue_date) ;
 OPEN cur_pymt;
 FETCH cur_pymt INTO v_payment_type,v_posting_date,v_sba_fee,v_csa_fee,v_cdc_fee,v_interest_amount,v_principal_amount,v_unalloc_amount,v_late_fee,v_dfb_amount;
 -- pyment cursor
 WHILE cur_pymt%FOUND
 LOOP
 
 BEGIN
 v_pymt_no := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_issue_date)));
 
 -- find missing pymt months between 2 received pymt months
 WHILE v_pymt_no - v_ws_pymt_no > 1
 LOOP
 
 BEGIN
 v_ws_pymt_no := v_ws_pymt_no + 1 ;
 /*
 if @ws_pymt_no= 1
 begin
 
 -- initial monthly Int and Prin
 select@m_int= NotePrin * @NoteRt/100 / 360 * @m1_int_days,
 fromCDCTTTransTbl
 whereLoanNmb= @ws_sba_loan_no
 
 select@accr_sba_fee= 0,
 
 end
 */
 -- missing pymt falls on 5 year break
 IF MOD(v_ws_pymt_no, 60) = 1 THEN
 
 BEGIN
 v_balance_used := v_ws_balance ;
 v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
 v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
 v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
 v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
 UPDATE TTTransTbl
 SET Pymt160Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 1;
 UPDATE TTTransTbl
 SET Pymt61120Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 61;
 UPDATE TTTransTbl
 SET Pymt121180Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 121;
 UPDATE TTTransTbl
 SET Pymt181240Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 181;
 
 END;
 END IF;
 /*
 select@PymtAmt= case
 when@pymt_no between 1 and 60 then Pymt160
 when@pymt_no between 61 and 120then Pymt61120
 when@pymt_no between 121 and 180then Pymt121180
 when@pymt_no between 181 and 240then Pymt181240
 end
 fromCDCTTTransTbl
 whereLoanNmb= @ws_sba_loan_no
 */
 -- monthly prin uses prior month accrued Int as part of the calculation
 IF v_ws_pymt_no = 1 THEN
 
 BEGIN
 SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
 MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2)
 
 INTO v_m_int,
 v_m_prin
 FROM TTTransTbl
 WHERE LoanNmb = v_ws_sba_loan_no;
 
 END;
 ELSE
 
 BEGIN
 v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;
 v_m_prin := v_monthly_escrow - ROUND((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30, 2) ;
 
 END;
 END IF;
 -- calc. accr. int and prin first since they use prior month accr. bal
 v_accr_int := v_accr_int + v_m_int ;
 v_accr_prin := v_accr_prin + v_m_prin ;
 v_balance := v_ws_balance ;
 -- calculate accr. fees
 v_accr_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) + v_accr_sba_fee ;
 v_accr_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) + v_accr_csa_fee ;
 v_accr_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) + v_accr_cdc_fee ;
 v_ws_posting_date := ADD_MONTHS(v_issue_date, v_ws_pymt_no);
 v_ws_posting_date := TRUNC(v_ws_posting_date, 'MONTH') ;
 -- calculate accr. late fee for non-deferred loan with
 -- posting date later than last pymt date and
 -- posting month <> s/a month
 IF v_loan_status <> 'DEFERRED'
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_ws_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_ws_posting_date))) > 0 THEN
 
 BEGIN
 v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
 IF v_m_late_fee < 100 THEN
 v_m_late_fee := 100 ;
 END IF;
 v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;
 
 END;
 END IF;
 -- insert payment hist table
 INSERT INTO TTPymtHistTbl
 ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_ws_pymt_no pymt_no ,
 'DUE' STATUS ,
 'D' PymtTyp ,
 v_ws_posting_date PostingDt ,
 NULL SBAFee ,
 NULL CSAFee ,
 NULL CDCFeeAmt ,
 NULL IntAmt ,
 NULL PrinAmt ,
 NULL UnallocAmt ,
 v_balance Bal ,
 NULL LateFee ,
 NULL PrepayAmt ,
 NULL DFBAmt ,
 USER ,
 USER
 FROM DUAL );
 -- insert to CDCTTMoTbl (the accrued table)
 -- ifdatediff(m, @LastPymtDtx, @ws_posting_date) > 0
 IF v_accr_int <> 0 THEN
 
 BEGIN
 INSERT INTO TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_ws_posting_date PostingDt ,
 v_accr_int AccrInt ,
 v_accr_cdc_fee CDCFeeAmt ,
 v_accr_sba_fee SBAFee ,
 v_accr_csa_fee CSAFee ,
 v_accr_late_fee LateFee ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 END IF;
 
 END;
 END LOOP;
 /*
 selectpymt_no= @ws_pymt_no,
 PostingDt= @ws_posting_date,
 late_fee_paid_date = @late_fee_paid_date,
 prior_bal= @ws_balance,
 accr_int = @accr_int,
 accr_prin= @accr_prin,
 accr_sba_fee= @accr_sba_fee,
 accr_csa_fee= @accr_csa_fee,
 accr_cdc_fee= @accr_cdc_fee,
 accr_late_fee= @accr_late_fee,
 balance_used= @balance_used,
 m_int= @m_int,
 m_prin= @m_prin,
 m_sba_fee= @m_sba_fee,
 m_csa_fee= @m_csa_fee,
 m_cdc_fee= @m_cdc_fee,
 m_late_fee= @m_late_fee
 -- accl_transcript_generate_r
 */
 -- while @pymt_no - @ws_pymt_no > 1
 -- pymt received for the month
 IF v_payment_type LIKE '%R%' THEN
 
 BEGIN
 v_sba_fee := 0 ;
 v_csa_fee := 0 ;
 v_cdc_fee := 0 ;
 v_interest_amount := 0 ;
 v_principal_amount := 0 ;
 v_unalloc_amount := 0 ;
 
 END;
 END IF;
 IF ( MOD(v_pymt_no, 60) = 1
 AND v_pymt_no <> v_ws_pymt_no ) THEN
 
 -- every 5 year break
 BEGIN
 v_balance_used := v_ws_balance ;
 v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
 v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
 v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
 v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
 UPDATE TTTransTbl
 SET Pymt160Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_pymt_no = 1;
 UPDATE TTTransTbl
 SET Pymt61120Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_pymt_no = 61;
 UPDATE TTTransTbl
 SET Pymt121180Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_pymt_no = 121;
 UPDATE TTTransTbl
 SET Pymt181240Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_pymt_no = 181;
 
 END;
 END IF;
 IF v_pymt_no = 1
 AND v_pymt_no <> v_ws_pymt_no THEN
 
 BEGIN
 -- very first pymt initialization
 -- initial monthly Int and Prin
 SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
 MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2)
 
 INTO v_m_int,
 v_m_prin
 FROM TTTransTbl
 WHERE LoanNmb = v_ws_sba_loan_no;
 v_m_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) ;
 v_m_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) ;
 v_m_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) ;
 v_accr_sba_fee := 0 ;
 v_accr_csa_fee := 0 ;
 v_accr_cdc_fee := 0 ;
 v_accr_int := 0 ;
 v_accr_prin := 0 ;
 v_balance := v_ws_balance ;
 
 END;
 ELSE
 
 BEGIN
 -- monthly prin uses prior month accrued Int as part of the calculation
 IF v_pymt_no = v_ws_pymt_no THEN
 
 BEGIN
 v_m_prin := 0 ;
 v_m_int := 0 ;
 v_m_sba_fee := 0 ;
 v_m_cdc_fee := 0 ;
 v_m_csa_fee := 0 ;
 
 END;
 ELSE
 
 BEGIN
 v_m_prin := v_monthly_escrow - ROUND(((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30), 2) ;
 v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;
 v_m_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) ;
 v_m_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) ;
 v_m_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) ;
 
 END;
 END IF;
 
 END;
 END IF;
 -- calc. accr. int and prin first since they use prior month accr. bal
 v_accr_int := v_accr_int + v_m_int - v_interest_amount ;
 v_accr_prin := v_accr_prin + v_m_prin - v_principal_amount ;
 -- calculate accr. fees
 v_accr_sba_fee := v_m_sba_fee + v_accr_sba_fee - v_sba_fee ;
 v_accr_csa_fee := v_m_csa_fee + v_accr_csa_fee - v_csa_fee ;
 v_accr_cdc_fee := v_m_cdc_fee + v_accr_cdc_fee - v_cdc_fee ;
 v_balance := v_ws_balance - v_principal_amount ;
 -- insert payment hist table
 IF v_payment_type LIKE '%R%' THEN
 
 BEGIN
 -- calculate accr. late fee for non-deferred loan with
 -- posting date later than last pymt date and
 -- posting month <> s/a month
 IF v_loan_status <> 'DEFERRED'
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_posting_date))) > 0 THEN
 
 BEGIN
 v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
 IF v_m_late_fee < 100 THEN
 v_m_late_fee := 100 ;
 END IF;
 v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;
 
 END;
 END IF;
 INSERT INTO TTPymtHistTbl
 ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_pymt_no pymt_no ,
 'REJECTED' STATUS ,
 v_payment_type PymtTyp ,
 v_posting_date PostingDt ,
 NULL SBAFee ,
 NULL CSAFee ,
 NULL CDCFeeAmt ,
 NULL IntAmt ,
 NULL PrinAmt ,
 NULL UnallocAmt ,
 v_balance Bal ,
 v_late_fee LateFee ,
 NULL PrepayAmt ,
 v_dfb_amount DueFromBorrAmt ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTPymtHistTbl
 ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 CASE v_payment_type
 WHEN 'X' THEN NULL
 ELSE v_pymt_no
 END pymt_no ,
 CASE v_payment_type
 WHEN 'X' THEN 'ADJ'
 ELSE 'REC' || CHR(39) || 'D'
 END STATUS ,
 v_payment_type PymtTyp ,
 v_posting_date PostingDt ,
 v_sba_fee SBAFee ,
 v_csa_fee CSAFee ,
 v_cdc_fee CDCFeeAmt ,
 v_interest_amount IntAmt ,
 v_principal_amount PrinAmt ,
 v_unalloc_amount UnallocAmt ,
 v_balance Bal ,
 v_late_fee LateFeeAmt ,
 NULL PrepayAmt ,
 v_dfb_amount DueFromBorrAmt ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 END IF;
 -- insert to CDCTTMoTbl (the accrued table)
 IF v_accr_int <> 0 THEN
 
 BEGIN
 INSERT INTO TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_posting_date PostingDt ,
 v_accr_int AccrInt ,
 v_accr_cdc_fee CDCFeeAmt ,
 v_accr_sba_fee SBAFee ,
 v_accr_csa_fee CSAFee ,
 v_accr_late_fee LateFeeAmt ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 END IF;
 /*
 selectws_pymt_no= @ws_pymt_no,
 late_fee_paid_date = @late_fee_paid_date,
 pymt_no= @pymt_no,
 PostingDt= @PostingDt,
 prior_bal= @ws_balance,
 accr_int = @accr_int,
 accr_prin= @accr_prin,
 accr_sba_fee= @accr_sba_fee,
 accr_csa_fee= @accr_csa_fee,
 accr_cdc_fee= @accr_cdc_fee,
 accr_late_fee= @accr_late_fee,
 balance_used= @balance_used,
 m_int= @m_int,
 m_prin= @m_prin,
 m_sba_fee= @m_sba_fee,
 m_csa_fee= @m_csa_fee,
 m_cdc_fee= @m_cdc_fee,
 m_late_fee= @m_late_fee,
 IntAmt= @IntAmt,
 PrinAmt= @PrinAmt
 */
 -- accl_transcript_generate_r
 IF TRUNC(MONTHS_BETWEEN(LAST_DAY(v_posting_date), LAST_DAY(v_last_pymt_date))) > 1 THEN
 
 BEGIN
 -- insert to CDCTTMoTbl (the accrued table)
 INSERT INTO TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_posting_date PostingDt ,
 v_accr_int AccrInt ,
 v_accr_cdc_fee CDCFeeAmt ,
 v_accr_sba_fee SBAFee ,
 v_accr_csa_fee CSAFee ,
 v_accr_late_fee LateFeeAmt ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 END IF;
 v_ws_balance := v_balance ;
 --if@PymtTyp<> 'X'
 v_ws_pymt_no := v_pymt_no ;
 FETCH cur_pymt INTO v_payment_type,v_posting_date,v_sba_fee,v_csa_fee,v_cdc_fee,v_interest_amount,v_principal_amount,v_unalloc_amount,v_late_fee,v_dfb_amount;
 
 END;
 END LOOP;
 CLOSE cur_pymt;
 -- missing pymt after last pymt date, until next s/a date
 v_max_pymt_no := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_issue_date))) ;
 WHILE v_max_pymt_no > v_ws_pymt_no
 LOOP
 
 BEGIN
 v_ws_pymt_no := v_ws_pymt_no + 1 ;
 v_ws_posting_date := ADD_MONTHS(v_issue_date, v_ws_pymt_no);
 v_ws_posting_date := TRUNC(v_ws_posting_date, 'MONTH') ;
 IF v_ws_pymt_no = 1 THEN
 
 BEGIN
 -- initial monthly Int and Prin
 SELECT ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
 MoEscrowAmt - ROUND(NotePrinAmt * v_note_rate / 100 / 360 * v_m1_int_days, 2) ,
 Pymt160Amt
 
 INTO v_m_int,
 v_m_prin,
 v_pymt_amt
 FROM TTTransTbl
 WHERE LoanNmb = v_ws_sba_loan_no;
 v_accr_sba_fee := 0 ;
 v_accr_csa_fee := 0 ;
 v_accr_cdc_fee := 0 ;
 v_accr_int := 0 ;
 v_accr_prin := 0 ;
 
 END;
 ELSE
 
 BEGIN
 -- monthly prin uses prior month accrued Int as part of the calculation
 v_m_prin := v_monthly_escrow - ROUND(((v_ws_balance - v_accr_prin) * v_note_rate / 100 / 360 * 30), 2) ;
 v_m_int := ROUND(v_ws_balance * v_note_rate / 100 / 360 * 30, 2) ;
 
 END;
 END IF;
 -- missing pymt falls on 5 year break
 IF MOD(v_ws_pymt_no, 60) = 1 THEN
 
 BEGIN
 v_balance_used := v_ws_balance ;
 v_m_sba_fee := ROUND(v_ws_balance * v_sba_pct / 12, 2) ;
 v_m_cdc_fee := ROUND(v_ws_balance * v_cdc_pct / 12, 2) ;
 v_m_csa_fee := ROUND(v_ws_balance * v_csa_pct / 12, 2) ;
 v_pymt_amt := v_monthly_escrow + v_m_sba_fee + v_m_cdc_fee + v_m_csa_fee ;
 UPDATE TTTransTbl
 SET Pymt160Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 1;
 UPDATE TTTransTbl
 SET Pymt61120Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 61;
 UPDATE TTTransTbl
 SET Pymt121180Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 121;
 UPDATE TTTransTbl
 SET Pymt181240Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no = 181;
 
 END;
 END IF;
 /*
 select@PymtAmt= case
 when@ws_pymt_no between 1 and 60 then Pymt160Amt
 when@ws_pymt_no between 61 and 120then Pymt61120Amt
 when@ws_pymt_no between 121 and 180then Pymt121180
 when@ws_pymt_no between 181 and 240then Pymt181240Amt
 end
 fromCDCTTTransTbl
 whereLoanNmb= @ws_sba_loan_no
 */
 -- calc. accr. int and prin first since they use prior month accr. bal
 --if@ws_pymt_no - @pymt_no = 1
 --select@accr_int= @accr_int + @m_int + round(@m_int / 30 * (@IntDueDays - @IntPaidDays), 2)
 --else
 v_accr_int := v_accr_int + v_m_int ;
 v_accr_prin := v_accr_prin + v_m_prin ;
 v_balance := v_ws_balance ;
 -- calculate accr. fees
 v_accr_sba_fee := ROUND(v_balance_used * v_sba_pct / 12, 2) + v_accr_sba_fee ;
 v_accr_csa_fee := ROUND(v_balance_used * v_csa_pct / 12, 2) + v_accr_csa_fee ;
 v_accr_cdc_fee := ROUND(v_balance_used * v_cdc_pct / 12, 2) + v_accr_cdc_fee ;
 -- calculate accr. late fee for non-deferred loan with
 -- posting date later than last pymt date and
 -- posting month <> s/a month
 IF v_loan_status <> 'DEFERRED'
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_ws_posting_date), LAST_DAY(v_late_fee_paid_date))) > 0
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(v_semi_annual_date), LAST_DAY(v_ws_posting_date))) > 0 THEN
 
 BEGIN
 v_m_late_fee := ROUND(v_pymt_amt * v_late_pct, 2) ;
 IF v_m_late_fee < 100 THEN
 v_m_late_fee := 100 ;
 END IF;
 v_accr_late_fee := v_accr_late_fee + v_m_late_fee ;
 
 END;
 END IF;
 /*
 
 selectPostingDt= @ws_posting_date,
 late_fee_paid_date = @late_fee_paid_date,
 prior_bal= @ws_balance,
 accr_int = @accr_int,
 accr_prin= @accr_prin,
 accr_sba_fee= @accr_sba_fee,
 accr_csa_fee= @accr_csa_fee,
 accr_cdc_fee= @accr_cdc_fee,
 accr_late_fee= @accr_late_fee,
 balance_used= @balance_used,
 m_int= @m_int,
 m_prin= @m_prin,
 m_sba_fee= @m_sba_fee,
 m_csa_fee= @m_csa_fee,
 m_cdc_fee= @m_cdc_fee,
 m_late_fee= @m_late_fee
 -- accl_transcript_generate_r
 */
 -- insert payment hist table
 INSERT INTO TTPymtHistTbl
 ( LoanNmb, PymtNmb, LoanStatCd, PymtTyp, PostingDt, SBAFeeAmt, CSAFeeAmt, CDCFeeAmt, IntAmt, PrinAmt, UnallocAmt, BalAmt, LateFeeAmt, PrepayAmt, DueFromBorrAmt, CreatUserID, LastUpdtUserID )
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_ws_pymt_no pymt_no ,
 'DUE' STATUS ,
 'D' PymtTyp ,
 v_ws_posting_date PostingDt ,
 NULL SBAFee ,
 NULL CSAFee ,
 NULL CDCFeeAmt ,
 NULL IntAmt ,
 NULL PrinAmt ,
 NULL UnallocAmt ,
 v_balance Bal ,
 NULL LateFeeAmt ,
 NULL PrepayAmt ,
 NULL DueFromBorrAmt ,
 USER ,
 USER
 FROM DUAL );
 -- insert to CDCTTMoTbl (the accrued table)
 IF v_accr_int <> 0 THEN
 
 BEGIN
 INSERT INTO TTMoTbl (LoanNmb,PostingDt,AccrIntAmt,CDCFeeAmt,SBAFeeAmt,CSAFeeAmt,LateFeeAmt, CreatUserID, LastUpdtUserID)
 ( SELECT v_ws_sba_loan_no LoanNmb ,
 v_ws_posting_date PostingDt ,
 v_accr_int AccrInt ,
 v_accr_cdc_fee CDCFeeAmt ,
 v_accr_sba_fee SBAFee ,
 v_accr_csa_fee CSAFee ,
 v_accr_late_fee LateFeeAmt ,
 USER ,
 USER
 FROM DUAL );
 
 END;
 END IF;
 
 END;
 END LOOP;
 -- while @max_pymt_no > @ws_pymt_no
 UPDATE TTTransTbl
 SET PrinAmt = v_accr_prin,
 IntAmt = v_accr_int,
 SBAFeeAmt = v_accr_sba_fee,
 CDCFeeAmt = v_accr_cdc_fee,
 CSAFeeAmt = v_accr_csa_fee,
 LateFeeAmt = v_accr_late_fee
 WHERE LoanNmb = v_ws_sba_loan_no;
 UPDATE TTTransTbl
 SET LastBalAmt = v_balance
 WHERE LoanNmb = v_ws_sba_loan_no
 AND LastBalAmt <> v_balance;
 -- update the rest of the 5 year break pymt after the s/a date
 UPDATE TTTransTbl
 SET Pymt160Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no < 1
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 1;
 UPDATE TTTransTbl
 SET Pymt61120Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no < 61
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 61;
 UPDATE TTTransTbl
 SET Pymt121180Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no < 121
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 121;
 UPDATE TTTransTbl
 SET Pymt181240Amt = v_pymt_amt
 WHERE LoanNmb = v_ws_sba_loan_no
 AND v_ws_pymt_no < 181
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(MatDt), LAST_DAY(v_issue_date))) >= 181;
 FETCH cur_trans INTO v_ws_sba_loan_no,v_issue_date_char,v_cdc_pct,v_sba_pct,v_csa_pct,v_late_pct,v_note_rate,v_monthly_escrow,v_last_pymt_date,v_int_due_days,v_int_paid_days,v_loan_status,v_semi_annual_date;
 
 END;
 END LOOP;
 CLOSE cur_trans;
 --=================================================== new stop
 -- get info from the portfolio
 --setCDCNm= isnull(ltrim(rtrim(p.CDCNm)), '') + isnull(' ' + ltrim(rtrim(p.CDCNm1)), '')
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, NVL(LTRIM(RTRIM(p.CDCNm)), ' ') AS CDCNm
 FROM TTTransTbl t ,PortflTbl p
 WHERE t.LoanNmb = p.LoanNmb) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 -- debenture rate
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
 FROM TTTransTbl t ,( SELECT CDC5YrBreakTbl.LoanNmb ,
 MAX(CDC5YrBreakTbl.PymtDt) PymtDt
 FROM CDC5YrBreakTbl
 WHERE CDC5YrBreakTbl.PymtDt IS NOT NULL
 GROUP BY CDC5YrBreakTbl.LoanNmb ) M ,CDC5YrBreakTbl b
 WHERE t.LoanNmb = M.LoanNmb
 AND M.LoanNmb = b.LoanNmb
 AND M.PymtDt = b.PymtDt) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
 IntDueDays = src.IntDueDays,
 IntPaidDays = src.IntPaidDays;
 -- debenture rate for loans without any payments
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, b.DbentrRtPct, b.IntDueDays, b.IntPaidDays
 FROM TTTransTbl t ,CDC5YrBreakTbl b
 WHERE t.LoanNmb = b.LoanNmb
 AND t.DbentrRtPct IS NULL) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrRtPct = src.DbentrRtPct,
 IntDueDays = src.IntDueDays,
 IntPaidDays = src.IntPaidDays;
 -- escrow Bal
 INSERT INTO TTEscrowTbl (LoanNmb,SemiAnnDt,EscrowBalAmt,EscrowDt,DbentrPoBalAmt,DbentrPoDt,CalcEscrowBalAmt,CalcDt, CreatUserID, LastUpdtUserID)
 ( SELECT LoanNmb ,
 SemiAnnDt ,
 NULL EscrowBal ,
 NULL EscrowDt ,
 NULL DbentrPoBal ,
 NULL DbentrPoDt ,
 NULL CalcEscrowBal ,
 NULL CalcDt ,
 USER ,
 USER
 FROM TTTransTbl );
 -- ZM: this had duplicate values
 MERGE INTO TTEscrowTbl t1
 USING (SELECT DISTINCT t1.ROWID row_id, y1.EscrowBalAmt, y1.EscrowDt
 FROM TTEscrowTbl t1 ,CDC5YrBreakTbl y1
 WHERE t1.LoanNmb = y1.LoanNmb
 AND y1.PymtDt IS NOT NULL) src
 ON ( t1.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET EscrowBalAmt = src.EscrowBalAmt,
 EscrowDt = src.EscrowDt;
 /*
 updateCDCTTEscrowTbl
 setDbentrPoBal= d.AvailAmt - d.PaidAmt,
 DbentrPoDt= d.PymtDt
 fromCDCTTEscrowTble,
 CDCDbentrPayoutTbld,
 (selectLoanNmb,
 PymtDt= max(PymtDt)
 fromCDCDbentrPayoutTbl
 wherestatus= 1
 group by LoanNmb) m
 wheree.LoanNmb= d.LoanNmb
 andd.LoanNmb= m.LoanNmb
 andd.PymtDt= m.PymtDt
 andd.status= 1
 */
 -- ZM: this has duplicate row ids and amount values; very troublesome
 MERGE INTO TTEscrowTbl e
 USING (SELECT e.ROWID row_id, MIN(D.AvailAmt - D.PaidAmt) AS pos_2, D.PymtDt
 FROM TTEscrowTbl e ,DbentrPayoutTbl D ,( SELECT d2.LoanNmb ,
 d2.PymtDt ,
 MAX(d1.StatCd) status
 FROM DbentrPayoutTbl d1,
 ( SELECT DbentrPayoutTbl.LoanNmb ,
 MAX(DbentrPayoutTbl.PymtDt) PymtDt
 FROM DbentrPayoutTbl
 GROUP BY DbentrPayoutTbl.LoanNmb ) d2
 WHERE d1.LoanNmb = d2.LoanNmb
 AND TRUNC(d1.PymtDt) = TRUNC(d2.PymtDt)
 AND d1.StatCd IN ( 1,2 -- payout and guarantee
 )
 
 GROUP BY d2.LoanNmb,d2.PymtDt ) M
 WHERE e.LoanNmb = D.LoanNmb
 AND D.LoanNmb = M.LoanNmb
 AND TRUNC(D.PymtDt) = TRUNC(M.PymtDt)
 AND D.StatCd = M.STATUS GROUP BY e.ROWID, D.PymtDt) src
 ON ( e.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrPoBalAmt = pos_2,
 DbentrPoDt = src.PymtDt;
 UPDATE TTEscrowTbl
 SET DbentrPoBalAmt = 0
 WHERE DbentrPoBalAmt IS NULL;
 -- take the later date and Bal between EscrowDt and DbentrPoDt
 UPDATE TTEscrowTbl
 SET CalcEscrowBalAmt = DbentrPoBalAmt,
 CalcDt = DbentrPoDt
 WHERE DbentrPoDt > EscrowDt
 OR EscrowDt IS NULL;
 UPDATE TTEscrowTbl
 SET CalcEscrowBalAmt = EscrowBalAmt,
 CalcDt = EscrowDt
 WHERE DbentrPoDt <= EscrowDt
 OR DbentrPoDt IS NULL;
 UPDATE TTEscrowTbl
 SET CalcEscrowBalAmt = 0
 WHERE CalcEscrowBalAmt IS NULL;
 -- calculate escrow Bal
 MERGE INTO TTEscrowTbl e
 USING (SELECT e.ROWID row_id, CalcEscrowBalAmt + NVL(p.PymtAmt, 0) AS CalcEscrowBal
 FROM TTEscrowTbl e ,( SELECT p1.LoanNmb ,
 SUM(p1.IntAmt) + SUM(p1.PrinAmt) + SUM(p1.UnallocAmt) PymtAmt
 FROM TTEscrowTbl e1,
 TTPymtHistTbl p1
 WHERE e1.LoanNmb = p1.LoanNmb
 AND p1.PostingDt > e1.CalcDt
 GROUP BY p1.LoanNmb ) p
 WHERE e.LoanNmb = p.LoanNmb) src
 ON ( e.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CalcEscrowBalAmt = src.CalcEscrowBal;
 -- select * from CDCTTEscrowTbl -- accl_transcript_generate_r
 /*
 -- when process date is close to semi-annual, and the debenture payout has not
 -- been posted yet on the process, but a few days later, the escrow will be zero
 updateCDCTTEscrowTbl
 setCalcEscrowBal= 0
 where@SYSLastTime between CalcDt and SemiAnnDt
 anddatediff(m, @SYSLastTime, SemiAnnDt) <= 1
 */
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, e.CalcEscrowBalAmt
 FROM TTTransTbl t ,TTEscrowTbl e
 WHERE t.LoanNmb = e.LoanNmb) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET EscrowBalAmt = src.CalcEscrowBalAmt;
 -- if escrow Bal is less than $1 due to rounding, change to zero 10/30/09
 UPDATE TTTransTbl
 SET EscrowBalAmt = 0
 WHERE EscrowBalAmt BETWEEN 0 AND 1;
 -- amortization scheduled Bal
 UPDATE TTTransTbl
 SET AmortBalAmt = LastBalAmt - PrinAmt;
 -- cdc fees owed
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, D.CDCFeeOwed
 FROM TTTransTbl t ,( SELECT DueFromBorrTbl.LoanNmb ,
 SUM(DueFromBorrTbl.PostingAmt) CDCFeeOwed
 FROM DueFromBorrTbl
 WHERE DueFromBorrTbl.RecrdTyp = 2
 AND DueFromBorrTbl.StatCd = 1
 GROUP BY DueFromBorrTbl.LoanNmb ) D
 WHERE t.LoanNmb = D.LoanNmb) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCFeeOwedAmt = src.CDCFeeOwed;
 UPDATE TTTransTbl
 SET CDCFeeOwedAmt = 0
 WHERE CDCFeeOwedAmt IS NULL;
 -- CSA Fees Due
 UPDATE TTTransTbl
 SET CSAFeeDueAmt = CSAFeeAmt
 WHERE EscrowBalAmt >= CSAFeeAmt;
 -- if EscrowBal only covers partial CSAFee, cover the full month fee starting from the beginning of the missing payment month
 MERGE INTO TTTransTbl t
 USING (SELECT t.ROWID row_id, M.CSAFee
 FROM TTTransTbl t ,( SELECT m1.LoanNmb ,
 MAX(m1.CSAFeeAmt) CSAFee
 FROM TTTransTbl t1,
 TTMoTbl m1
 WHERE m1.LoanNmb = t1.LoanNmb
 AND m1.CSAFeeAmt <= t1.EscrowBalAmt
 GROUP BY m1.LoanNmb ) M
 WHERE t.LoanNmb = M.LoanNmb
 AND t.EscrowBalAmt < t.CSAFeeAmt) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CSAFeeDueAmt = src.CSAFee;
 -- for partial cover of CSAFee, if escrow bal cannot cover even 1 pymt, show full csa fee 10/30/09
 -- request on 3/1/2010, CSAFeeDue s/b 0 instead of CSAFee
 UPDATE TTTransTbl
 SET CSAFeeDueAmt = 0 -- CSAFee
 
 WHERE EscrowBalAmt < CSAFeeAmt
 AND CSAFeeDueAmt IS NULL;
 UPDATE TTTransTbl
 SET CSAFeeDueAmt = 0
 WHERE EscrowBalAmt <= 0;
 -- due Amt
 UPDATE TTTransTbl
 SET DueAmt = DbentrPrinBalAmt + DbentrIntDueAmt - (EscrowBalAmt - CSAFeeDueAmt);
 -- DueDt, 2 business days prior to semi-annual date
 UPDATE TTTransTbl
 SET DueDt = fn_BizDateAdd(SemiAnnDt, -2);
 SELECT pa.CntctTitl ,
 pa.CntctPhnNmb
 
 INTO v_cs_title,
 v_cs_phone
 FROM PartcpntTbl p,
 PartcpntAdrTbl pa
 WHERE p.PrgrmCd = pa.PrgrmCd
 AND p.PrgrmCd = 10
 AND p.PartcpntCd = 20 -- sender
 
 AND pa.PartcpntAdrCd = 101 AND ROWNUM <= 1;-- primary contact
 UPDATE TTTransTbl
 SET CSTitl = v_cs_title,
 CSPhnNmb = v_cs_phone,
 RlseDt = v_sys_lasttime
 -- use RqstDt 11/18/09
 -- AccelASOFDt= convert(smalldatetime, convert(varchar, month(dateadd(m, 1, @SYSLastTime))) + '/01/' + convert(varchar, year(dateadd(m, 1, @SYSLastTime))) )
 ,
 AccelASOFDt = TRUNC(ADD_MONTHS(RqstDt, 1), 'MONTH');
 -- get trustee info
 SELECT PartcpntNm ,
 ABANmb ,
 AcctNmb ,
 AcctNm ,
 WireRE ,
 a.PartcpntMailAdrStr1Nm ,-- newly added for ALM 1049
 
 a.PartcpntMailAdrStr2Nm ,
 a.PartcpntMailAdrCtyNm ,
 a.PartcpntSt ,
 a.PartcpntMailAdrZipCd -- newly added for ALM 1049
 
 
 INTO v_trustee_name,
 v_aba_number,
 v_account_number,
 v_account_name,
 v_wire_re,
 v_trustee_finacial_street_1,
 v_trustee_finacial_street_2,
 v_trustee_finacial_city,
 v_trustee_finacial_state,
 v_trustee_finacial_zip
 FROM PartcpntTbl p,
 WireInstrctnTbl W,
 PartcpntAdrTbl a
 WHERE p.PrgrmCd = W.PrgrmCd
 AND p.PartcpntID = W.PartcpntID
 AND p.PrgrmCd = 10
 AND p.PartcpntCd = 30
 AND p.PartcpntID = a.PartcpntID;
 -- get recipient info
 SELECT p.PartcpntNm ,
 p.PartcpntNm ,
 a.CntctSalutation ,
 a.CntctNm ,
 a.PartcpntMailAdrStr1Nm ,
 a.PartcpntMailAdrStr2Nm ,
 a.PartcpntMailAdrCtyNm ,
 a.PartcpntSt ,
 a.PartcpntMailAdrZipCd
 
 INTO v_recip_name,
 v_recip_name_1,
 v_recip_salutation,
 v_recip_contact,
 v_recip_street_1,
 v_recip_street_2,
 v_recip_city,
 v_recip_state,
 v_recip_zip
 FROM PartcpntTbl p,
 PartcpntAdrTbl a
 WHERE p.PrgrmCd = a.PrgrmCd
 AND p.PartcpntID = a.PartcpntID
 AND a.PartcpntAdrCd = 101
 AND p.PartcpntCd = 10;
 -- get sender info
 SELECT p.PartcpntNm ,
 a.PartcpntMailAdrStr1Nm ,
 a.PartcpntMailAdrStr2Nm ,
 a.PartcpntMailAdrCtyNm ,
 a.PartcpntSt ,
 a.PartcpntMailAdrZipCd
 
 INTO v_sender_name,
 v_sender_street_1,
 v_sender_street_2,
 v_sender_city,
 v_sender_state,
 v_sender_zip
 FROM PartcpntTbl p,
 PartcpntAdrTbl a
 WHERE p.PrgrmCd = a.PrgrmCd
 AND p.PartcpntID = a.PartcpntID
 AND a.PartcpntAdrCd = 101
 AND p.PartcpntCd = 20;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:=pgm||' '||ver||': Oracle error occured while preparing data.'
 ||' SQLCODE='||sqlcode||' '||sqlerrm(sqlcode); 
 end; 
 end if;
 if p_errval=0 then 
 BEGIN
 insert into TTtranscriptrpt1
 SELECT DISTINCT t.RefID ,
 t.RqstDt ,
 t.SmllBusCons ,
 t.StmtNm ,
 t.CDCRegnCd ,
 t.CDCNmb ,
 t.CDCNm ,
 t.LoanNmb ,
 t.IssDt ,
 t.MatDt ,
 t.SemiAnnDt ,
 t.NotePrinAmt ,
 t.DbentrRtPct ,
 t.NoteRtPct ,
 t.LastPymtDt ,
 t.LastBalAmt ,
 t.MoEscrowAmt ,
 t.Pymt160Amt ,
 t.Pymt61120Amt ,
 t.Pymt121180Amt ,
 t.Pymt181240Amt ,
 t.PrinAmt AS tPrinAmt,
 t.IntAmt AS tIntAmt,
 t.SBAFeeAmt AS tSBAFeeAmt,
 t.CDCFeeAmt AS tCDCFeeAmt,
 t.CSAFeeAmt AS tCSAFeeAmt,
 t.LateFeeAmt AS tLateFeeAmt,
 t.AmortBalAmt ,
 t.CDCFeeOwedAmt ,
 t.DbentrPrinBalAmt ,
 t.DbentrIntDueAmt ,
 t.EscrowBalAmt ,
 t.EscrowDt ,
 t.CSAFeeDueAmt ,
 t.DueAmt ,
 t.DueDt ,
 t.RlseDt ,
 t.AccelASOFDt ,
 t.CSREP ,
 t.CSTitl ,
 t.CSPhnNmb ,
 h.PymtNmb ,
 h.LoanStatCd ,
 h.PymtTyp ,
 h.PostingDt ,
 h.SBAFeeAmt ,
 h.CSAFeeAmt ,
 h.CDCFeeAmt ,
 h.IntAmt ,
 h.PrinAmt ,
 h.UnallocAmt ,
 h.BalAmt ,
 h.LateFeeAmt ,
 h.PrepayAmt ,
 h.DueFromBorrAmt ,
 CASE h.PymtTyp
 WHEN 'X' THEN NULL
 ELSE h.IntAmt + h.PrinAmt + h.SBAFeeAmt + h.CSAFeeAmt + h.CDCFeeAmt + h.LateFeeAmt + h.UnallocAmt
 END PymtAmt ,
 M.AccrIntAmt ,
 v_trustee_name trustee_name ,
 v_aba_number ABANmb ,
 v_account_number AcctNmb ,
 v_account_name AcctNm ,
 v_wire_re WireRE ,
 v_recip_name recip_name ,
 v_recip_name_1 recip_name_1 ,
 v_recip_salutation recip_salutation ,
 v_recip_contact recip_contact ,
 v_recip_street_1 recip_street_1 ,
 v_recip_street_2 recip_street_2 ,
 v_recip_state recip_state ,
 v_recip_zip recip_zip ,
 v_sender_name sender_name ,
 v_sender_street_1 sender_street_1 ,
 v_sender_street_2 sender_street_2 ,
 v_sender_state sender_state ,
 v_sender_zip sender_zip ,
 v_sender_city sender_city ,
 v_recip_city recip_city ,
 t.AccrIntASOFDt ,
 v_trustee_finacial_street_1
 trustee_street_1 ,-- Newly added for ALM 1048
 
 v_trustee_finacial_street_2 trustee_street_2 ,
 v_trustee_finacial_state trustee_state ,
 v_trustee_finacial_zip trustee_zip ,
 v_trustee_finacial_city trustee_city
 FROM TTTransTbl t
 LEFT JOIN TTPymtHistTbl h ON t.LoanNmb = h.LoanNmb
 LEFT JOIN TTMoTbl M ON t.LoanNmb = M.LoanNmb
 AND h.PostingDt = M.PostingDt
 ORDER BY t.SemiAnnDt,
 t.LoanNmb,
 h.PostingDt,
 h.PymtNmb ;
 
 -- debenture recap report
 Insert into TTtranscriptrpt2
 SELECT SmllBusCons ,
 StmtNm ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 SemiAnnDt ,
 DueAmt ,
 AccelASOFDt ,
 CSREP ,
 CSTitl ,
 CSPhnNmb ,
 v_trustee_name trustee_name ,
 v_aba_number ABANmb ,
 v_account_number AcctNmb ,
 v_account_name AcctNm ,
 v_wire_re WireRE ,
 v_recip_name recip_name ,
 v_recip_name_1 recip_name_1 ,
 v_recip_salutation recip_salutation ,
 v_recip_contact recip_contact ,
 v_recip_street_1 recip_street_1 ,
 v_recip_street_2 recip_street_2 ,
 v_recip_state recip_state ,
 v_recip_zip recip_zip ,
 v_sender_name sender_name ,
 v_sender_street_1 sender_street_1 ,
 v_sender_street_2 sender_street_2 ,
 v_sender_state sender_state ,
 v_sender_zip sender_zip ,
 v_sender_city sender_city ,
 v_recip_city recip_city ,
 RlseDt ,
 DueDt ,
 v_trustee_finacial_street_1
 trustee_street_1 ,-- Newly added for ALM 1048
 
 v_trustee_finacial_street_2 trustee_street_2 ,
 v_trustee_finacial_state trustee_state ,
 v_trustee_finacial_zip trustee_zip ,
 v_trustee_finacial_city trustee_city
 FROM TTTransTbl
 ORDER BY SemiAnnDt,
 LoanNmb ;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:=pgm||' '||ver||': Oracle error occured while saving results into temp tables.'
 ||' SQLCODE='||sqlcode||' '||sqlerrm(sqlcode); 
 end;
 end if;
 if p_errval=0 then
 stgcsa.runtime_logger(logged_msg_id,'STDLOG',101,p_errmsg,
 p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld); 
 else
 stgcsa.runtime_logger(logged_msg_id,'STDERR',103,p_errmsg,
 p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld); 
 end if; 
 END AcclTrnscrptGen2TBLCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


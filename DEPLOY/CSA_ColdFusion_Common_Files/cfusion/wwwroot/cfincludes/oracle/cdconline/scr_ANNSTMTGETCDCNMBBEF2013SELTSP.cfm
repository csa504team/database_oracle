<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AnnStmtGetCDCNmbBef2013SelTSP
 (
 p_cdc_region IN CHAR,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT CDCNmb 
 FROM Tax1098PrintTbl 
 WHERE LTRIM(RTRIM(CDCRegnCd)) = LTRIM(RTRIM(p_cdc_region))
 AND LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 AND CDCRegnCd <> ' '
 AND CDCNmb <> ' '
 AND CDCRegnCd <> '00'
 ORDER BY CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE OutstandingGntyGetSumrySelCSP 
 (
 p_reported_date IN DATE DEFAULT NULL ,
 p_request_type IN CHAR DEFAULT 'S' ,
 p_output_type IN CHAR DEFAULT 'S' ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 ---select * from OutGntyTbl
 ---cdc_outstanding_guarantee_get_summary_r '11/15/2007','D','F'
 IF p_request_type = 'S' THEN
 
 BEGIN
 DELETE FROM TTGntyDataSumryTbl;
 DELETE FROM TTGntyLoanAmtTbl;
 INSERT INTO TTGntyLoanAmtTbl (LoanNmb,GntyAmt,CreatUserID,LastUpdtUserID)
 ( SELECT LoanNmb LoanNmb ,
 SUM(GntyAmt) GntyAmt ,
 USER ,
 USER
 
 --select *
 FROM OutGntyTbl 
 WHERE TRUNC(RptDt) = TRUNC(p_reported_date)
 GROUP BY LoanNmb );
 INSERT INTO TTGntyDataSumryTbl (PrgrmNmb,CDCRegnCd,CDCNmb,LoanNmb,GntyAmt,CreatUserID,LastUpdtUserID)
 ( SELECT DISTINCT PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 LoanNmb LoanNmb ,
 NULL GntyAmt ,
 USER ,
 USER
 FROM OutGntyTbl 
 WHERE TRUNC(RptDt) = TRUNC(p_reported_date) );
 --select * from TTGntyDataSumryTbl
 --select * from TTGntyLoanAmtTbl
 MERGE INTO TTGntyDataSumryTbl gd
 USING (SELECT gd.ROWID row_id, ga.GntyAmt
 FROM TTGntyDataSumryTbl gd
 JOIN TTGntyLoanAmtTbl ga ON gd.LoanNmb = ga.LoanNmb ) src
 ON ( gd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET GntyAmt = src.GntyAmt;
 
 END;
 ELSE
 
 BEGIN
 DELETE FROM TTGntyDataTbl;
 INSERT INTO TTGntyDataTbl (PrgrmNmb,CDCRegnCd,CDCNmb,LoanNmb,StatCd,StatCdDesc,GntyTyp,GntyTypDesc,GntyDt,GntyAmt,CreatUserID,LastUpdtUserID)
 ( SELECT PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 LoanNmb LoanNmb ,
 StatCd StatCd ,
 NULL status_code_dec ,
 GntyTyp GntyTyp ,
 NULL GntyTypDesc ,
 GntyDt GntyDt ,
 GntyAmt GntyAmt ,
 USER ,
 USER
 FROM OutGntyTbl 
 WHERE TRUNC(RptDt) = TRUNC(p_reported_date) );
 MERGE INTO TTGntyDataTbl gd
 USING (SELECT gd.ROWID row_id, gt.GntyTypDesc
 FROM TTGntyDataTbl gd
 JOIN GntyTypDescTbl gt ON gd.GntyTyp = gt.GntyTyp ) src
 ON ( gd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET GntyTypDesc = src.GntyTypDesc;
 MERGE INTO TTGntyDataTbl gd
 USING (SELECT gd.ROWID row_id, cs.StatCdDesc
 FROM TTGntyDataTbl gd
 JOIN StatCdDescTbl cs ON gd.StatCd = cs.StatCd ) src
 ON ( gd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET StatCdDesc = src.StatCdDesc;
 
 END;
 END IF;
 IF p_output_type = 'S' THEN
 
 BEGIN
 IF p_request_type = 'S' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT PrgrmNmb PrgrmNmb ,
 LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)) CDCNmb ,
 LoanNmb LoanNmb ,
 GntyAmt GntyAmt 
 FROM TTGntyDataSumryTbl 
 ORDER BY PrgrmNmb,
 CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT PrgrmNmb PrgrmNmb ,
 LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)) CDCNmb ,
 LoanNmb LoanNmb ,
 StatCdDesc StatCdDesc ,
 GntyTypDesc GntyTypDesc ,
 GntyDt GntyDt ,
 GntyAmt GntyAmt 
 FROM TTGntyDataTbl 
 ORDER BY PrgrmNmb,
 CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 END IF;
 
 END;
 END IF;
 IF p_output_type = 'F' THEN
 
 BEGIN
 DELETE FROM TTFile3Tbl;
 -- max length of the records, try to keep it short
 -- since osql packs spaces after each line
 IF p_request_type = 'S' THEN
 
 BEGIN
 INSERT INTO TTFile3Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 ( SELECT 'PROGRAM NUMBER' || ',' || 'CDC NUMBER' || ',' || 'LOAN NUMBER' || ',' || 'GUARANTEE Amt' , USER, USER
 FROM DUAL );
 INSERT INTO TTFile3Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 SELECT NVL(PrgrmNmb, ' ') || ',' || NVL(LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)), ' ') || ',' || NVL(LoanNmb, ' ') || ',' || NVL(TO_CHAR(GntyAmt), ' '), USER, USER 
 FROM TTGntyDataSumryTbl 
 ORDER BY PrgrmNmb,
 CDCRegnCd,
 CDCNmb,
 LoanNmb;
 OPEN p_SelCur2 FOR
 SELECT TTLine 
 FROM TTFile3Tbl 
 ORDER BY RefID ;
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTFile3Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 ( SELECT 'PROGRAM NUMBER' || ',' || 'CDC NUMBER' || ',' || 'LOAN NUMBER' || ',' || 'LOAN STATUS' || ',' || 'GUARANTEE DATE' || ',' || 'GUARANTEE TYPE' || ',' || 'GUARANTEE Amt' , USER, USER
 FROM DUAL );
 INSERT INTO TTFile3Tbl
 ( TTLine )
 SELECT NVL(PrgrmNmb, ' ') || ',' || NVL(LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)), ' ') || ',' || NVL(LoanNmb, ' ') || ',' || NVL(StatCdDesc, ' ') || ',' || NVL(TO_DATE(GntyDt, 'MM/DD/YYYY'), ' ') || ',' || NVL(GntyTypDesc, ' ') || ',' || NVL(TO_CHAR(GntyAmt), ' ') 
 FROM TTGntyDataTbl 
 ORDER BY PrgrmNmb,
 CDCRegnCd,
 CDCNmb,
 LoanNmb;
 OPEN p_SelCur2 FOR
 SELECT TTLine 
 FROM TTFile3Tbl 
 ORDER BY RefID ;
 
 END;
 END IF;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


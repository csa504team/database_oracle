<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE LateDisbDataSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_statement_date IN DATE DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT LoanNmb ,
 DueFromBorrClsDt ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 DueFromBorrPostingDt ,
 BorrNm ,
 DueFromBorrAmt ,
 RjctCd 
 FROM LateFeeDisbTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 
 --AND DFBClsDt = @StmtDt
 AND EXTRACT(MONTH FROM DueFromBorrClsDt) = EXTRACT(MONTH FROM p_statement_date)
 AND EXTRACT(YEAR FROM DueFromBorrClsDt) = EXTRACT(YEAR FROM p_statement_date
 --order by LoanNmb asc
 )
 ORDER BY LoanNmb,
 DueFromBorrPostingDt ;
 --- calculate the statment total due Amt
 OPEN p_SelCur2 FOR
 SELECT SUM(DueFromBorrAmt) total_amount_due 
 FROM LateFeeDisbTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 
 --AND DFBClsDt = @StmtDt
 AND EXTRACT(MONTH FROM DueFromBorrClsDt) = EXTRACT(MONTH FROM p_statement_date)
 AND EXTRACT(YEAR FROM DueFromBorrClsDt) = EXTRACT(YEAR FROM p_statement_date)
 AND RjctCd <> 'R' ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AdrGetSelTSP 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 EmailAdr 
 FROM AdrTbl 
 ORDER BY CDCRegnCd,
 CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


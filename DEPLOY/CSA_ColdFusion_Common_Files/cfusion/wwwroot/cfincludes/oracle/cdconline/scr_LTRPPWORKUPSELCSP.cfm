<!--- Saved 09/21/2018 18:20:44. --->
PROCEDURE LtrPPWorkUpSelCSP
 /*******************************************************************
 * PROCEDURE: CDC_LTR_PPWORKUP
 * PURPOSE: Generates Prepayment Workup cover letters.
 * NOTES:
 * CREATED:
 * MODIFIED
 * DATEAUTHORDESCRIPTION
 * 08/13/2013Bharani TalluriIF The loan amortization when recalculated for the payoff determines that the Bal at Next Semi-Annual Date is negative
 THEN Bal at Next Semi-Annual Date should be set to zero
 08/14/2013 Bharani Talluri Check for New posting Dates for Prepayment Estimates and Prepayment Actuals
 
 
 * Changes related to PRB/Project
 1. SBA Phase 2
 *
 * Details:
 Prepayment Actual and Prepayment Estimate As Of Letter details
 
 10/29/2015 Bharani Talluri Add new column Unallocated Funds
 02/09/2016 Bharani Talluri HPALM defect 835
 *-------------------------------------------------------------------
 *******************************************************************/
 (
 p_SBALoanNmb IN VARCHAR2,
 p_PrepayDt IN DATE,
 p_RqstCd IN NUMBER,
 p_LevelID IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR
 )
 AS
 v_prepay_date DATE := p_PrepayDt;
 v_biz_date DATE;
 v_start_date DATE;
 v_ltr_available CHAR(1);
 v_prpt_date DATE;
 v_rqst_date DATE;
 v_mm_to_sadate NUMBER(10,0);
 v_req_ref NUMBER(10,0);
 
 BEGIN
 
 v_prpt_date := v_prepay_date ;
 IF p_LevelID IN ( 7,9 )
 THEN
 
 /*** Colson staff: letter available after 3rd business of prepay_month ***/
 BEGIN
 v_start_date := TRUNC(v_prpt_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 3, v_biz_date) ;
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 --- if today is 3rd business day or later
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 v_start_date := TRUNC(v_prepay_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 6, v_biz_date) ;-- get the 6th business day of the pp month
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 -- if today is later than the 6th business day of the pp month
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 
 END;
 /*** CDC: Letter availableon the 6th business day of the prepayment month ***/
 END IF;
 /*** Estimated type request - letter available after 3rd business day ***/
 BEGIN
 SELECT MAX(RqstDt)
 INTO v_rqst_date
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(v_prepay_date)
 AND RqstCd = p_RqstCd AND ROWNUM < 2;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_rqst_date := NULL;
 END;
 IF p_RqstCd = 9 THEN
 
 BEGIN
 v_start_date := TRUNC(v_rqst_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 3, v_biz_date) ;
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 --- if today is 3rd business day or later
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 
 END;
 END IF;
 IF v_ltr_available = 'Y' THEN
 DECLARE
 v_rate NUMBER(8,5);
 v_amort_sba_fee NUMBER(19,4);
 v_amort_cdc_fee NUMBER(19,4);
 v_amort_csa_fee NUMBER(19,4);
 v_cdc_pct NUMBER(8,5);
 v_sba_pct NUMBER(8,5);
 v_piamt NUMBER(19,4);
 v_date DATE;
 v_bal NUMBER(19,4);
 v_curbal NUMBER(19,4);
 v_num_of_month NUMBER(10,0);
 v_next_sa_date DATE;
 v_inum NUMBER(10,0);
 v_posting_date DATE;
 v_last_posting_date DATE;
 v_xadj_posting_date DATE;
 v_issue_date DATE;
 v_req_ref NUMBER(10,0);
 v_unallocatedAmount NUMBER(19,4);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 BEGIN
 SELECT MAX(RqstREF)
 INTO v_req_ref
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(v_prepay_date)
 AND RqstCd = p_RqstCd AND ROWNUM < 2;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_req_ref := NULL;
 END;
 /* 1. Check if GFD is received,if not letter can not be displayed. */
 /*if exists (select * from CDCRqstTbl where RqstREF = @req_ref and GFDRecv = 'Y' and status <> 'CN')
 or exists (select * from CDCRqstTbl r,CDCLoanTbl l where r.RqstREF = @req_ref and l.LoanNmb = r.LoanNmb and l.LoanNmb = @LoanNmb and convert(smalldatetime,l.IssDt) < '01/01/1992' and status <> 'CN')
 or exists (select * from CDCRqstTbl r,CDCLoanSaveTbl ls where r.RqstREF = @req_ref and r.RqstREF = ls.RqstREF and convert(smalldatetime,ls.IssDt) < '01/01/1992' and status <> 'CN')
 ---request with type 1,6,7,9 always available---
 or exists (select * from CDCRqstTbl where RqstREF = @req_ref and RqstCd in(1,6,7,9) and status <> 'CN')*/
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT *
 FROM RqstTbl
 WHERE RqstREF = v_req_ref
 AND CDCPortflStatCdCd <> 'CN' );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 v_x_adj_prin NUMBER(19,4);
 /* Rule:
 The next semi-annual date is always a multiple of 6 from the Issue date
 The next semi-annual date is always greater than the Prepayment date
 */
 v_MnDiff -- Number of months between the Issue Date and Prepayment date
 NUMBER(10,0);
 v_MntoSADt NUMBER(10,0);-- The number of months needed to make @MnDiff a multiple of 6 (SemiAnnual)
 /* ream and save records to temp table */
 /*If there is a payment received on the month of a prepayment date start projection fron the next month. */
 v_gfd_received NUMBER(19,4);
 /*save prepayment Amt after ream*/
 /*** variables to hold values for Estimate Type request ***/
 v_next_prin_bal NUMBER(19,4);
 v_net_amt NUMBER(19,4);
 v_tot_pmts NUMBER(19,4);
 v_less_cdc NUMBER(19,4);
 v_tot_fee NUMBER(19,4);
 v_prin NUMBER(19,4);
 v_int NUMBER(19,4);
 v_sba NUMBER(19,4);
 v_cdc NUMBER(19,4);
 v_csa NUMBER(19,4);
 v_sched_bal NUMBER(19,4);
 v_tot_pp_amt NUMBER(19,4);
 v_premium_amount NUMBER(19,4);
 v_late_fee NUMBER(19,4);
 /******** new code (7/19/04) to set the release letter availability *********/
 v_prepay_amount NUMBER(19,4);
 
 BEGIN
 /* 2. Save record to the history file(CDCLoanSaveTbl) if it's not there. */
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE NOT EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 BEGIN
 SELECT MAX(PostingDt)
 INTO v_posting_date
 FROM PymtHistryTbl
 WHERE LoanNmb = p_SBALoanNmb
 AND ( LTRIM(RTRIM(PymtTyp)) NOT IN ( 'X','AR','CR',' ' )
 
 AND PymtTyp IS NOT NULL );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_posting_date := NULL;
 END;
 HistSaveInsCSP(v_req_ref,
 p_SBALoanNmb,
 v_posting_date,
 v_prepay_date) ;
 
 END;
 END IF;
 --ELSE
 --BEGIN /* if exists look for the most recent Posting date*/
 --EXEC cdc_get_newPostingDatePrepayment @LoanNmb,@req_ref,@PostingDt OUTPUT,@last_posting_date OUTPUT
 --END
 BEGIN
 SELECT SUM(UnallocAmt)
 INTO v_unallocatedAmount
 FROM PymtHistryTbl
 WHERE LoanNmb = p_SBALoanNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_unallocatedAmount := NULL;
 END;
 /*for non-current loans get x-adj payment */
 XADJ504SelCSP(p_SBALoanNmb,
 v_req_ref,
 v_posting_date,
 v_last_posting_date) ;
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_x_adj_prin
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PymtTyp = 'X';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_x_adj_prin := NULL;
 END;
 v_x_adj_prin := NVL(v_x_adj_prin, 0) ;
 /* 3. Run Ream procedure. */
 BEGIN
 SELECT NoteRtPct
 INTO v_rate
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_rate := NULL;
 END;
 BEGIN
 SELECT (NVL(AmortIntAmt, 0) + NVL(AmortPrinAmt, 0))
 INTO v_piamt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_piamt := NULL;
 END;
 BEGIN
 SELECT CurBalNeededAmt
 INTO v_curbal
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_curbal := NULL;
 END;
 IF v_curbal > 0 THEN
 
 BEGIN
 BEGIN
 SELECT (NVL(p.BalAmt, 0) - NVL(l.PrinNeededAmt, 0) - NVL(v_x_adj_prin, 0))
 INTO v_bal
 FROM PymtSaveTbl p,
 LoanSaveTbl l
 WHERE p.RqstREF = v_req_ref
 AND p.RqstREF = l.RqstREF
 AND p.PostingDt = v_posting_date;---@PostingDt
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 
 END;
 ELSE
 
 BEGIN
 BEGIN
 SELECT BalAmt
 INTO v_bal
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PostingDt = v_posting_date;---@PostingDt
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 
 END;
 END IF;
 BEGIN
 SELECT AmortSBAAmt
 INTO v_amort_sba_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_sba_fee := NULL;
 END;
 BEGIN
 SELECT AmortCDCAmt
 INTO v_amort_cdc_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_cdc_fee := NULL;
 END;
 BEGIN
 SELECT AmortCSAAmt
 INTO v_amort_csa_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_csa_fee := NULL;
 END;
 BEGIN
 SELECT IssDt
 INTO v_issue_date
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_issue_date := NULL;
 END;
 BEGIN
 SELECT CDCPct
 INTO v_cdc_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_pct := NULL;
 END;
 BEGIN
 SELECT SBAPct
 INTO v_sba_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba_pct := NULL;
 END;
 -- the sum of @MnDiff and @MnSADt will always be a multiple of 6
 v_MntoSADt := 0 ;
 v_MnDiff := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(v_issue_date)));
 IF MOD(v_MnDiff, 6) <> 0 THEN
 v_MntoSADt := v_MnDiff + 6 - (MOD(v_MnDiff, 6)) ;-- Adjust the number of months to the next sa date
 ELSE
 v_MntoSADt := v_MnDiff + 6 ;
 END IF;
 v_next_sa_date := ADD_MONTHS(v_issue_date, v_MntoSADt);
 /******* original *************************/
 /*set @inum=datediff(mm,@PrepayDt,@next_sa_date)*/
 /************************************************/
 /*As per John's instruction 2/28/2003*/
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(ADD_MONTHS(v_posting_date, 1))));
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ) = 'Y';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 BEGIN
 SELECT SUM(WireAmt)
 INTO v_gfd_received
 FROM WiresTbl
 WHERE LTRIM(RTRIM(LoanNmb)) = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(v_prepay_date)
 AND RqstCd = p_RqstCd;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_gfd_received := NULL;
 END;
 
 END;
 ELSE
 
 BEGIN
 v_gfd_received := 0.00 ;
 
 END;
 END IF;
 /*If request type is 1,6,7,9 use 0.00 Amt for GFD 2070*/
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT RqstCd
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ) IN ( 1,6,7,9 )
 ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_gfd_received := 0.00 ;
 
 END;
 END IF;
 /**********************************************************/
 /* as per John's instruction 2/28/2003 */
 IF v_curbal > 0
 AND p_RqstCd <> 9 THEN
 
 --non-current loans
 BEGIN
 v_prepay_date := ADD_MONTHS(SYSDATE, 1);
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_prepay_date)));
 --Use next month from current date
 
 END;
 ELSE
 
 BEGIN
 /*05/18/04 for current loans start projection from the next month off the Prepay Date month */
 v_prepay_date := ADD_MONTHS(v_prepay_date, 1);
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_prepay_date)));
 --Always use next month from last payment
 
 END;
 END IF;
 DELETE FROM TTWSCDCInstl2Tbl;
 /**********************************************************/
 /*temp table to hold values after ream procedure */
 IF p_RqstCd <> 9 THEN
 
 BEGIN
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 
 END;
 END IF;
 /* 4. Get the record. */
 UPDATE RqstTbl
 SET CDCPortflStatCdCd = 'CL',
 SYSUserID = 'systems',
 SYSProgID = 'sp_ppworkup_ltr',
 SYSLastTime = SYSDATE
 WHERE RqstREF = v_req_ref;
 /*estimate types 08/03/04*/
 BEGIN
 SELECT PrepayPremAmt
 INTO v_premium_amount
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_premium_amount := NULL;
 END;
 IF p_RqstCd = 9 THEN
 
 /*** Estimate type requests***/
 BEGIN
 IF v_curbal = 0 THEN
 DECLARE
 v_EstimProjectStartDate DATE;
 v_holdPrepayDate DATE;
 v_mm_to_sadate NUMBER(10,0);
 
 /*** current loans ,cur_bal_needed = 0 ***/
 BEGIN
 v_EstimProjectStartDate := ADD_MONTHS(v_posting_date, 1);
 v_holdPrepayDate := v_prepay_date ;
 v_prepay_date := v_EstimProjectStartDate ;
 v_inum := MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_EstimProjectStartDate));
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 v_prepay_date := v_holdPrepayDate ;
 BEGIN
 --TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prepay_date))) >= 0;
 SELECT COUNT(DueDt)
 INTO v_mm_to_sadate
 FROM TTWSCDCInstl2Tbl
 WHERE MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(DueDt)) >= 0;
 
 
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_mm_to_sadate := 0;
 END;
 BEGIN
 SELECT NVL(( SELECT SUM(PrinAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_prin
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prin := 0;
 END;
 BEGIN
 SELECT NVL(( SELECT SUM(IntAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT IntNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_int
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_int := 0;
 END;
 BEGIN
 SELECT NVL(( SELECT SUM(SBAFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT SBAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_sba
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba := 0;
 END;
 BEGIN
 SELECT NVL(( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT CDCFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_cdc
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc := 0;
 END;
 BEGIN
 SELECT NVL(( SELECT SUM(CSAFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT CSAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_csa
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_csa := 0;
 END;
 BEGIN
 SELECT LateFeeNeededAmt
 INTO v_late_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_late_fee := 0;
 END;
 v_tot_fee := NVL(v_csa, 0) + NVL(v_cdc, 0) + NVL(v_sba, 0) + NVL(v_late_fee, 0) ;
 BEGIN
 SELECT ( SELECT SUM(DueAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1 ) - ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1 )
 
 INTO v_net_amt
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_net_amt := 0;
 END;
 BEGIN
 SELECT SUM(DueAmt)
 INTO v_tot_pmts
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_tot_pmts := 0;
 END;
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_less_cdc
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_less_cdc := 0;
 END;
 v_tot_pp_amt := ROUND(NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(v_premium_amount, 0) - NVL(v_gfd_received, 0) + NVL(v_prin, 0) + NVL(v_int, 0) + NVL(v_csa, 0) + NVL(v_cdc, 0) + NVL(v_sba, 0) + NVL(v_late_fee, 0), 2) ;
 
 END;
 ELSE
 IF v_curbal > 0 THEN
 DECLARE
 ---1.ream pmts prior to the prepayment date - estimated payments
 v_diff NUMBER(19,4);
 v_start_proj_date DATE;
 v_request_date DATE;
 v_loan_status CHAR(8);
 -- pmts after PrepayDt
 v_est_pr NUMBER(19,4);
 v_est_int NUMBER(19,4);
 v_est_sba NUMBER(19,4);
 v_est_cdc NUMBER(19,4);
 v_mm_to_sadate NUMBER(10,0);
 v_est_csa NUMBER(19,4);
 v_lt_fee NUMBER(19,4);
 v_sba_fee_needed NUMBER(19,4);
 v_cdc_fee_needed NUMBER(19,4);
 v_csa_fee_needed NUMBER(19,4);
 v_int_needed NUMBER(19,4);
 v_prin_needed NUMBER(19,4);
 
 /*** noncurrent loans ,cur_bal_needed > 0 ***/
 BEGIN
 DELETE FROM TTWSCDCEstInstl2Tbl;
 BEGIN
 SELECT LTRIM(RTRIM(LoanStatCd))
 INTO v_loan_status
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_loan_status := NULL;
 END;
 BEGIN
 SELECT RqstDt
 INTO v_request_date
 FROM RqstTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_request_date := NULL;
 END;
 v_start_proj_date := ADD_MONTHS(v_request_date, 1);---@postig_date
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(v_request_date))) - 1;-----datediff(mm,@PostingDt,@prpt_date)-1
 BEGIN
 SELECT BalAmt
 INTO v_bal
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND TRUNC(PostingDt) = TRUNC(v_posting_date);
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 ReamEstLoan(v_inum,
 v_rate,
 v_piamt,
 v_start_proj_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date,
 v_req_ref,
 v_posting_date,
 v_request_date) ;
 ---2.ream pmts after prepay date - to sa date
 BEGIN
 SELECT LateFeeNeededAmt
 INTO v_late_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_late_fee := NULL;
 END;
 --- get Bal for projection to SA Date
 BEGIN
 SELECT ( SELECT BalAmt
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND Trunc(PostingDt) = TRUNC(v_posting_date) ) - NVL(( SELECT SUM(PrinAmt)
 FROM TTWSCDCEstInstl2Tbl ), 0 ----where datediff(mm,@RqstDt,DueDt)> 0),0)
 ) - ( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref )
 
 INTO v_bal
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 v_inum := MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_prpt_date)) - 1 ;
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 v_mm_to_sadate := v_inum + 1 ;
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_est_pr
 FROM TTWSCDCEstInstl2Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_pr := NULL;
 END;
 BEGIN
 SELECT SUM(AccrIntAmt)
 INTO v_est_int
 FROM TTWSCDCEstInstl2Tbl ;--- where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_int := NULL;
 END;
 BEGIN
 SELECT SUM(SBAFeeAmt)
 INTO v_est_sba
 FROM TTWSCDCEstInstl2Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_sba := NULL;
 END;
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_est_cdc
 FROM TTWSCDCEstInstl2Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_cdc := NULL;
 END;
 BEGIN
 SELECT SUM(CSAFeeAmt)
 INTO v_est_csa
 FROM TTWSCDCEstInstl2Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_csa := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT LateFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 INTO v_lt_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_lt_fee := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT SBAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 INTO v_sba_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba_fee_needed := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT CDCFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 INTO v_cdc_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_fee_needed := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT CSAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_csa_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_csa_fee_needed := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT IntNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 INTO v_int_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_int_needed := NULL;
 END;
 BEGIN
 SELECT NVL(( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_prin_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prin_needed := NULL;
 END;
 --RS3 past due pmts
 BEGIN
 SELECT NVL(( SELECT LateFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_late_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_late_fee := NULL;
 END;
 ----if prepay month is a current month - add amounts needed to projected amounts
 v_prin := NVL(v_est_pr, 0) + NVL(v_prin_needed, 0) ;
 v_int := NVL(v_est_int, 0) + NVL(v_int_needed, 0) ;
 v_sba := NVL(v_est_sba, 0) + NVL(v_sba_fee_needed, 0) ;
 v_cdc := NVL(v_est_cdc, 0) + NVL(v_cdc_fee_needed, 0) ;
 v_csa := NVL(v_est_csa, 0) + NVL(v_csa_fee_needed, 0) ;
 v_tot_fee := NVL(v_csa, 0) + NVL(v_cdc, 0) + NVL(v_sba, 0) + NVL(v_late_fee, 0) ;
 BEGIN
 SELECT NVL(( SELECT SUM(DueAmt) - SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl ), 0)
 INTO v_net_amt
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_net_amt := NULL;
 END;
 BEGIN
 SELECT SUM(DueAmt)
 INTO v_tot_pmts
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_tot_pmts := NULL;
 END;
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_less_cdc
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_less_cdc := NULL;
 END;
 v_tot_pp_amt := ROUND(NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(v_premium_amount, 0) - NVL(v_gfd_received, 0) + NVL(v_prin, 0) + NVL(v_int, 0) + NVL(v_csa, 0) + NVL(v_cdc, 0) + NVL(v_sba, 0) + NVL(v_late_fee, 0), 2) ;
 
 END;
 END IF;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 BEGIN
 SELECT (NVL(l.CSAFeeNeededAmt, 0) + NVL(l.CDCFeeNeededAmt, 0) + NVL(l.SBAFeeNeededAmt, 0) + NVL(l.LateFeeNeededAmt, 0))
 INTO v_tot_fee
 FROM LoanSaveTbl l
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_tot_fee := NULL;
 END;
 BEGIN
 SELECT ( SELECT SUM(DueAmt)
 FROM TTWSCDCInstl2Tbl ) - ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl )
 
 INTO v_net_amt
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_net_amt := NULL;
 END;
 BEGIN
 SELECT SUM(DueAmt)
 INTO v_tot_pmts
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_tot_pmts := NULL;
 END;
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_less_cdc
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_less_cdc := NULL;
 END;
 END;
 END IF;
 BEGIN
 SELECT MIN(EndBalAmt)
 INTO v_next_prin_bal
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_next_prin_bal := NULL;
 END;
 UPDATE LoanSaveTbl l
 SET l.WKPrepayAmt = (NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(v_premium_amount, 0) - NVL(v_gfd_received, 0) + NVL(l.PrinNeededAmt, 0) + NVL(l.IntNeededAmt, 0) + NVL(l.CSAFeeNeededAmt, 0) + NVL(l.CDCFeeNeededAmt, 0) + NVL(l.SBAFeeNeededAmt, 0) + NVL(l.LateFeeNeededAmt, 0))
 WHERE l.RqstREF = v_req_ref;
 DELETE FROM TTTempAvailTbl;
 BEGIN
 SELECT PrepayAmt
 INTO v_prepay_amount
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PymtTyp = 'X';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prepay_amount := NULL;
 END;
 /* list of loans which release letters are available */
 INSERT INTO TTTempAvailTbl (RqstRef,CreatUserID,LastUpdtUserID)
 ( SELECT rq.RqstREF , USER, USER
 FROM RqstTbl rq,
 LoanSaveTbl ls,
 LoanTbl ln
 WHERE rq.RqstREF = ls.RqstREF
 AND rq.RqstREF = v_req_ref
 AND ls.LoanNmb = ln.LoanNmb
 AND ln.NoteBalAmt = 0
 AND rq.CDCPortflStatCdCd = 'CL'
 AND ls.WKPrepayAmt IS NOT NULL
 AND v_prepay_amount IS NOT NULL
 AND ls.WKPrepayAmt <= v_prepay_amount + 6 );
 --and @PrepayAmt between ls.WKPrepayAmt and ls.WKPrepayAmt + 6
 /* insert current date only if not already there */
 INSERT INTO RlsAvailTbl
 ( RqstREF, AvailDt,CreatUserID,LastUpdtUserID )
 ( SELECT ta.RqstREF ,
 SYSDATE , USER , USER
 FROM TTTempAvailTbl ta
 LEFT JOIN RlsAvailTbl av ON ta.RqstREF = av.RqstREF
 WHERE av.RqstREF IS NULL );
 /********** new code end ****************/
 --Check if the next_prin_bal (Bal at next Semi Annual date) < 0
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( ( SELECT PrinAmt
 FROM TTWSCDCInstl2Tbl
 WHERE EndBalAmt = ( SELECT MIN(EndBalAmt)
 FROM TTWSCDCInstl2Tbl ) ) > ( SELECT foo1.EndBalAmt
 FROM ( SELECT ROW_NUMBER() OVER ( ORDER BY DueDt DESC ) rownumber ,
 PrinAmt ,
 EndBalAmt
 FROM TTWSCDCInstl2Tbl ) foo
 JOIN ( SELECT ROW_NUMBER() OVER ( ORDER BY DueDt DESC ) rownumber ,
 EndBalAmt
 FROM TTWSCDCInstl2Tbl ) foo1 ON foo.rownumber = foo1.rownumber - 1
 WHERE foo.rownumber = 1
 AND foo1.rownumber = 2 ) );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 /* only rows 1 and 2 */
 BEGIN
 v_next_prin_bal := 0 ;
 
 END;
 ELSE
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( ( SELECT MIN(EndBalAmt)
 FROM TTWSCDCInstl2Tbl ) < 0
 AND v_inum = 0 );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_next_prin_bal := 0 ;
 
 END;
 END IF;
 END IF;
 
 /* ZM: Insert an audit record so that we can indicate when the letter was first generated */
 BEGIN
 INSERT INTO cdconline.rqstaudttbl(rqstref,rqstdt,audtactn,rqstfrom,phnnmb,faxnmb,emailadr,loannmb,rqstcd,semianndt,prepaydt,gfdrecv,statcd,statdt,sysprogid,sysuserid,syslasttime,creatuserid,creatdt,lastupdtuserid,lastupdtdt)
 SELECT
 rqstref,rqstdt,'PPWORKUP',rqstfrom,phnnmb,faxnmb,emailadr,loannmb,rqstcd,semianndt,prepaydt,gfdrecv,cdcportflstatcdcd,statdt,sysprogid,sysuserid,syslasttime,USER,SYSDATE,USER,SYSDATE
 FROM 
 cdconline.rqsttbl
 WHERE
 rqstref = v_req_ref
 AND
 NOT EXISTS (SELECT * FROM cdconline.rqstaudttbl WHERE rqstref = v_req_ref);
 END;
 
 /* 4. Get the record. */
 OPEN p_SelCur1 FOR
 SELECT a.CntctNm ,
 LTRIM(RTRIM(a.CDCNm)) CDCNm ,
 a.CDCRegnCd || '-' || a.CDCNmb CDCNmb ,
 a.FaxNmb ,
 r.LoanNmb ,
 NVL(TRIM(l.SmllBusCons), l.BorrNm) SmllBusCons ,
 r.PrepayDt ,
 v_next_prin_bal next_prin_bal ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_prin, 0)
 ELSE l.PrinNeededAmt
 END PrinNeededAmt ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_int, 0)
 ELSE l.IntNeededAmt
 END IntNeededAmt ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_csa, 0)
 ELSE l.CSAFeeNeededAmt
 END CSAFeeNeededAmt ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_cdc, 0)
 ELSE l.CDCFeeNeededAmt
 END CDCFeeNeededAmt ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_sba, 0)
 ELSE l.SBAFeeNeededAmt
 END SBAFeeNeededAmt ,
 CASE p_RqstCd
 WHEN 9 THEN NVL(v_late_fee, 0)
 ELSE l.LateFeeNeededAmt
 END LateFeeNeededAmt ,
 NVL(v_tot_fee, 0) TotFeeDueAmt ,
 NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_prin, 0)
 ELSE l.PrinNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_int, 0)
 ELSE l.IntNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_csa, 0)
 ELSE l.CSAFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_cdc, 0)
 ELSE l.CDCFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_sba, 0)
 ELSE l.SBAFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN NVL(v_late_fee, 0)
 ELSE l.LateFeeNeededAmt
 END, 0) total_amt_past_due ,
 CASE p_RqstCd
 WHEN 9 THEN ABS(MONTHS_BETWEEN(LAST_DAY(p_PrepayDt), LAST_DAY(v_next_sa_date))) --v_mm_to_sadate ---@inum
 
 ELSE v_inum + 1
 END month_sa_date ,
 v_mm_to_sadate v_mm_to_sadate,
 v_inum v_inum,
 ABS(MONTHS_BETWEEN(LAST_DAY(p_PrepayDt), LAST_DAY(v_next_sa_date))) estNextMonth,
 v_tot_pmts tot_pmts ,
 v_less_cdc less_cdc ,
 v_net_amt net_amt ,
 l.PrepayPremAmt PrepayPremAmt ,
 /*GFDRecv = case r.GFDRecv
 when 'Y' then 1000
 else 0
 end,
 */
 NVL(v_gfd_received, 0) GFDRecv ,
 ROUND(NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(v_premium_amount, 0) - NVL(v_gfd_received, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_prin
 ELSE l.PrinNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_int
 ELSE l.IntNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_csa
 ELSE l.CSAFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_cdc
 ELSE l.CDCFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_sba
 ELSE l.SBAFeeNeededAmt
 END, 0) + NVL(CASE p_RqstCd
 WHEN 9 THEN v_late_fee
 ELSE l.LateFeeNeededAmt
 END, 0) - NVL(v_unallocatedAmount, 0), 2) tot_pp_amt ,
 v_unallocatedAmount unallocatedAmount ,
 '021-000-018' ACHRoutingNmb ,
 '890-0606762' ACHAcct
 FROM RqstTbl r
 LEFT JOIN LoanSaveTbl l ON r.RqstREF = l.RqstREF
 LEFT JOIN AdrTbl a ON l.CDCNmb = a.CDCNmb
 AND l.CDCRegnCd = a.CDCRegnCd
 WHERE r.RqstREF = v_req_ref ;
 
 END;
 END IF;
 
 END;
 /* delete ws_cdc_install */
 ELSE
 
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT *
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ; --and GFDRecv = 'Y'
 
 END;
 END IF;
 IF SQL%ROWCOUNT = 0 THEN
 OPEN p_SelCur3 FOR
 SELECT 'recordcount'
 FROM DUAL
 WHERE 1 = 2 ;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


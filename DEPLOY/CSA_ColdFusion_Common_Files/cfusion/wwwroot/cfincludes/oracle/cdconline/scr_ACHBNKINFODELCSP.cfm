<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE ACHBnkInfoDelCSP 
 --cdc_ach_bank_info_R '09','111'
 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTCDCACHInfoTbl;
 IF p_region_num IS NULL
 AND p_cdc_num IS NULL THEN
 
 BEGIN
 INSERT INTO TTCDCACHInfoTbl (CDCRegnCd,CDCNmb,LoanNmb,BorrNm,ACHBnk,ACHRoutingNmb,ACHAcct,TotLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 ACHBnk ACHBnk ,
 ACHRoutingNmb ACHRoutingNmb ,
 ACHAcct ACHAcct ,
 NULL total_loans ,
 USER ,
 USER
 FROM LoanTbl 
 WHERE LoanStatCd NOT IN ('PREPAID','ACCELER.','TRAN SBA','OTHER','LIQUID')
 
 AND LTRIM(RTRIM(CDCNmb)) <> ''
 AND ( ACHBnk IS NULL
 OR ACHRoutingNmb IS NULL
 OR ACHAcct IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHBnk))) IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHRoutingNmb))) IS NULL
 OR LENGTH(RTRIM(LTRIM(ACHAcct))) IS NULL ) );
 DELETE FROM TTCDCTotTbl;
 INSERT INTO TTCDCTotTbl (CDCRegnCd,CDCNmb,TotLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 COUNT(LoanNmb) total_loans ,
 USER , 
 USER
 FROM LoanTbl 
 WHERE LoanStatCd NOT IN ('PREPAID','ACCELER.','TRAN SBA','OTHER','LIQUID')
 
 AND LTRIM(RTRIM(CDCNmb)) <> ''
 AND ( ACHBnk IS NULL
 OR ACHRoutingNmb IS NULL
 OR ACHAcct IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHBnk))) IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHRoutingNmb))) IS NULL
 OR LENGTH(RTRIM(LTRIM(ACHAcct))) IS NULL )
 GROUP BY CDCRegnCd,CDCNmb );
 MERGE INTO TTCDCACHInfoTbl ca
 USING (SELECT ca.ROWID row_id, ct.TotLoan
 FROM TTCDCACHInfoTbl ca
 JOIN TTCDCTotTbl ct ON ca.CDCRegnCd = ct.CDCRegnCd
 AND ca.CDCNmb = ct.CDCNmb ) src
 ON ( ca.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET TotLoan = src.TotLoan;
 INSERT INTO TTCDCACHInfoTbl (CDCRegnCd,CDCNmb,LoanNmb,BorrNm,ACHBnk,ACHRoutingNmb,ACHAcct,TotLoan,CreatUserID,LastUpdtUserID)
 ( SELECT '99' CDCRegnCd ,
 '999' CDCNmb ,
 NULL BorrNm ,
 NULL LoanNmb ,
 NULL ACHBnk ,
 NULL ACHRoutingNmb ,
 NULL ACHAcct ,
 COUNT(LoanNmb) TotLoan ,
 USER ,
 USER
 FROM LoanTbl 
 WHERE LoanStatCd NOT IN ('PREPAID','ACCELER.','TRAN SBA','OTHER','LIQUID')
 
 AND LTRIM(RTRIM(CDCNmb)) <> ''
 AND ( ACHBnk IS NULL
 OR ACHRoutingNmb IS NULL
 OR ACHAcct IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHBnk))) IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHRoutingNmb))) IS NULL
 OR LENGTH(RTRIM(LTRIM(ACHAcct))) IS NULL ) );
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTCDCACHInfoTbl (CDCRegnCd,CDCNmb,LoanNmb,BorrNm,ACHBnk,ACHRoutingNmb,ACHAcct,TotLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 ACHBnk ACHBnk ,
 ACHRoutingNmb ACHRoutingNmb ,
 ACHAcct ACHAcct ,
 NULL TotLoan ,
 USER ,
 USER
 FROM LoanTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND LoanStatCd NOT IN ('PREPAID','ACCELER.','TRAN SBA','OTHER','LIQUID')
 
 AND ( ACHBnk IS NULL
 OR ACHRoutingNmb IS NULL
 OR ACHAcct IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHBnk))) IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHRoutingNmb))) IS NULL
 OR LENGTH(RTRIM(LTRIM(ACHAcct))) IS NULL ) );
 INSERT INTO TTCDCACHInfoTbl (CDCRegnCd,CDCNmb,LoanNmb,BorrNm,ACHBnk,ACHRoutingNmb,ACHAcct,TotLoan,CreatUserID,LastUpdtUserID)
 ( SELECT '99' CDCRegnCd ,
 '999' CDCNmb ,
 NULL BorrNm ,
 NULL LoanNmb ,
 NULL ACHBnk ,
 NULL ACHRoutingNmb ,
 NULL ACHAcct ,
 COUNT(LoanNmb) TotLoan ,
 USER ,
 USER
 FROM LoanTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND LoanStatCd NOT IN ('PREPAID','ACCELER.','TRAN SBA','OTHER','LIQUID')
 
 AND ( ACHBnk IS NULL
 OR ACHRoutingNmb IS NULL
 OR ACHAcct IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHBnk))) IS NULL
 OR LENGTH(LTRIM(RTRIM(ACHRoutingNmb))) IS NULL
 OR LENGTH(RTRIM(LTRIM(ACHAcct))) IS NULL ) );
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 BorrNm BorrNm ,
 LoanNmb LoanNmb ,
 ACHBnk ACHBnk ,
 ACHRoutingNmb ACHRoutingNmb ,
 ACHAcct ACHAcct ,
 TotLoan TotLoan 
 FROM TTCDCACHInfoTbl 
 ORDER BY CDCRegnCd,
 CDCNmb,
 BorrNm,
 LoanNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclLoanEntryUpdTSP 
 (
 p_sba_loan_no IN CHAR,
 p_deb_prin_bal IN NUMBER,
 p_deb_int_due IN NUMBER,
 p_sys_userid IN VARCHAR2,
 p_sys_progid IN VARCHAR2
 )
 AS
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'Y' 
 FROM AcclLoanEntryTbl 
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 UPDATE AcclLoanEntryTbl
 SET DbentrPrinBalAmt = p_deb_prin_bal,
 DbentrIntDueAmt = p_deb_int_due,
 SYSUserID = p_sys_userid,
 SYSProgID = p_sys_progid,
 SYSLastTime = SYSDATE
 WHERE LoanNmb = p_sba_loan_no;/*
 else 
 --exec accel_add_records_m
 exec accl_loan_entry_add_m
 @LoanNmb,
 @DbentrPrinBal,
 @DbentrIntDue,
 @SYSUserID ,
 @SYSProgID 
 */
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


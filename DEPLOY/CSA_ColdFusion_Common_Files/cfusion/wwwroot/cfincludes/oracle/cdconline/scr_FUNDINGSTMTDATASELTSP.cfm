<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE FundingStmtDataSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_funding_month IN NUMBER DEFAULT NULL ,
 p_funding_year IN NUMBER DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 FundingDt FundingDt ,
 LoanNmb LoanNmb ,
 StmtDt StmtDt ,
 PrgrmNmb PrgrmNmb ,
 BorrNm BorrNm ,
 DbentrAmt DbentrAmt ,
 CDCFeeAmt CDCFeeAmt ,
 ClsAmt ClsAmt ,
 BorrAmt BorrAmt ,
 CAIPInd CAIPInd ,
 WithheldAmt WithheldAmt ,
 ACHInd ACHInd ,
 ChkACHAmt ChkACHAmt 
 FROM FundingStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND EXTRACT(YEAR FROM FundingDt) = p_funding_year
 AND EXTRACT(MONTH FROM FundingDt) = p_funding_month
 ORDER BY LoanNmb ;
 --- calculate the statment check/ach Amt
 OPEN p_SelCur2 FOR
 SELECT SUM(ChkACHAmt) total_check_ach_amount 
 FROM FundingStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND EXTRACT(YEAR FROM FundingDt) = p_funding_year
 AND EXTRACT(MONTH FROM FundingDt) = p_funding_month ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


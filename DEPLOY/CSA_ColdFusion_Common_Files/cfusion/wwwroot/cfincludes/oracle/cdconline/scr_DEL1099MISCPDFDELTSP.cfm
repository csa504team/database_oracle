<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE Del1099MISCPDFDelTSP 
 (
 --@TaxYr char(4),
 p_nmad_id IN CHAR
 )
 AS
 
 BEGIN
 
 /*
 DELETE 
 FROM NMADCrosrefTbl
 Where ltrim(rtrim(NMADID)) = ltrim(rtrim(@NMADID))
 */
 DELETE Tax1099MiscPDFTbl
 
 WHERE LTRIM(RTRIM(NamAddrID)) = LTRIM(RTRIM(p_nmad_id));
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AmortGetLoanSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_funding_month IN NUMBER DEFAULT NULL ,
 p_funding_year IN NUMBER DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT amh.LoanNmb ,
 amh.BorrNm ,
 amh.NoteAmt ,
 amh.LoanTrm ,
 ln.RefiLoan 
 FROM AmortHdrTbl amh
 LEFT JOIN LoanTbl ln ON amh.LoanNmb = ln.LoanNmb
 WHERE amh.CDCRegnCd = p_region_num
 AND amh.CDCNmb = p_cdc_num
 AND EXTRACT(MONTH FROM amh.FundingDt) = p_funding_month
 AND EXTRACT(YEAR FROM amh.FundingDt) = p_funding_year ;--cdc_amort_get_loans_r '02','005',2,2011
 --select * from CDCAmortHdrTbl 
 --WHERE CDCRegn = '02'
 ---AND CDCNmb = '005'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


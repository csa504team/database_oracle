<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LoanDtlInfoSelTSP 
 (
 p_GPNum IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT L.PrgrmNmb ,
 L.UserLevelRoleID ,
 L.CDCRegnCd ,
 L.CDCNmb ,
 L.LoanNmb ,
 L.LoanStatCd ,
 L.NotePrinAmt ,
 L.NoteRtPct ,
 L.IssDt ,
 L.MatDt ,
 L.BorrNm ,
 L.BorrMailAdrStr1Nm ,
 L.BorrMailAdrCtyNm ,
 L.BorrMailAdrStCd ,
 L.BorrMailAdrZipCd ,
 L.SmllBusCons ,
 L.StmtNm ,
 L.ACHBnk ,
 L.ACHRoutingNmb ,
 L.ACHAcct ,
 L.TaxID ,
 L.TaxNm ,
 L.LateFeeStatCd ,
 L.PrinLeftAmt ,
 L.AmortDtx ,
 L.AmortIntAmt ,
 L.AmortPrinAmt ,
 L.AmortPymtAmt ,
 L.NoteBalAmt ,
 L.FeePaidDtx ,
 L.IntDtx ,
 L.CurLoanDtx ,
 L.CurBalNeededAmt ,
 L.PrinNeededAmt ,
 L.IntNeededAmt ,
 L.CDCFeeNeededAmt ,
 L.SBAFeeNeededAmt ,
 L.CSAFeeNeededAmt ,
 L.LateFeeNeededAmt ,
 L.DueFromBorrNeededAmt ,
 L.AmortCDCAmt ,
 L.AmortSBAAmt ,
 L.AmortCSAAmt ,
 L.CDCPct ,
 L.SBAPct ,
 L.RefiLoan ,
 W9.W9TaxID W9TaxID ,
 W9.W9Nm W9Nm ,
 W9.W9MailAdrStr1Nm W9MailAdrStr1Nm ,
 W9.W9MailAdrCtyNm W9MailAdrCtyNm ,
 W9.W9MailAdrStCd W9MailAdrStCd ,
 W9.W9MailAdrZipCd W9MailAdrZipCd 
 FROM LoanTbl L
 LEFT JOIN LoanW9Tbl W9 ON L.LoanNmb = W9.LoanNmb
 WHERE L.LoanNmb = p_GPNum ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


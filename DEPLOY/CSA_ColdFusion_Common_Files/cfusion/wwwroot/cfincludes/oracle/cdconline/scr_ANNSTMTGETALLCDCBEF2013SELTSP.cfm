<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AnnStmtGetAllCDCBef2013SelTSP
 (
 p_TaxYr IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT CDCRegnCd
 FROM Tax1098PrintTbl
 WHERE LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_TaxYr))
 AND CDCRegnCd <> ' '
 AND CDCNmb <> ' '
 AND CDCRegnCd <> '00'
 ORDER BY CDCRegnCd ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AppFundsEmailSelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CDC_Cert IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 sp.BorrNm ,
 --sp.StmtNm,
 ap.LoanNmb ,
 ap.PymtRecvDDtx ,
 ap.SBAFeeAmt ,
 ap.CSAFeeAmt ,
 ap.CDCFeeAmt ,
 ap.IntApplAmt ,
 ap.PrinApplAmt ,
 ap.DiffToEscrowAmt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.BorrNm ;
 ELSE
 IF LTRIM(RTRIM(p_SOD)) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 sp.BorrNm ,
 --sp.StmtNm,
 ap.LoanNmb ,
 ap.PymtRecvDDtx ,
 ap.SBAFeeAmt ,
 ap.CSAFeeAmt ,
 ap.CDCFeeAmt ,
 ap.IntApplAmt ,
 ap.PrinApplAmt ,
 ap.DiffToEscrowAmt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 
 --WHERE SODNmb = @SOD
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY sp.BorrNm ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT sp.UserLevelRoleID ,
 sp.DistNm ,
 sp.CDCRegnCd ,
 sp.CDCNmb ,
 sp.CDCNm ,
 --sp.BorrNm,
 sp.StmtNm ,
 ap.LoanNmb ,
 NVL(ap.PymtRecvDDtx, ' ') PymtRecvDDtx ,
 NVL(ap.SBAFeeAmt, 0.00) SBAFee ,
 NVL(ap.CSAFeeAmt, 0.00) csc_fee ,
 NVL(ap.CDCFeeAmt, 0.00) CDCFeeAmt ,
 NVL(ap.IntApplAmt, 0.00) IntAppl ,
 NVL(ap.PrinApplAmt, 0.00) PrinAppl ,
 NVL(ap.DiffToEscrowAmt, 0.00) DiffToEscrowAmt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


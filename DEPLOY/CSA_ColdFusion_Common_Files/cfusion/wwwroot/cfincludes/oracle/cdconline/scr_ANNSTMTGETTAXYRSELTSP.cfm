<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AnnStmtGetTaxYrSelTSP 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT (TaxYrNmb) 
 FROM AnnStmtRecipntTbl 
 ORDER BY TaxYrNmb DESC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


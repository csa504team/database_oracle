<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetRjctPymtDelCSP 
 (
 p_OutputTyp IN CHAR DEFAULT NULL ,
 p_RjctDt IN DATE DEFAULT NULL ,
 p_CDCRegn IN CHAR DEFAULT NULL ,
 p_CDCNmb IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTRjctData2Tbl;
 --cdc_get_rejected_payments_r 'S','06/03/2008'
 ----cdc_get_rejected_payments_r 'S', '06/03/2008' , '01','019'
 --cdc_get_rejected_payments_r 'F','07/01/2008'
 ----cdc_get_rejected_payments_r 'F', '06/04/2008' , '06','102'
 --drop table CDCTTRjctData2Tbl
 IF p_CDCRegn IS NULL
 AND p_CDCNmb IS NULL THEN
 
 BEGIN
 INSERT INTO TTRjctData2Tbl (PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,BorrNm,LoanNmb,PostingDt,PymtTyp,RjctDt,RjctCd,RjctCdDesc,RjctAmt,PymtPosted,ACHDel,ACHMultiRjct,EmailNote, CreatUserID, LastUpdtUserID)
 ( SELECT NULL PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 NULL CDCNm ,
 NULL borroer_name ,
 LoanNmb LoanNmb ,
 PostingDt PostingDt ,
 PymtTyp PymtTyp ,
 RjctDt RjctDt ,
 RjctCd RjctCd ,
 NULL RjctCdDesc ,
 RjctAmt RjctAmt ,
 PymtPosted PymtPosted ,
 CASE 
 WHEN ACHDel = 1 THEN 'Y'
 ELSE 'N'
 END ACHDel ,
 CASE 
 WHEN ACHMultiRjct = 1 THEN 'Y'
 ELSE 'N'
 END ACHMultiRjct ,
 EmailNote EmailNote ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE RjctDt = p_RjctDt );
 MERGE INTO TTRjctData2Tbl pr
 USING (SELECT pr.ROWID row_id, ca.CDCNm
 FROM TTRjctData2Tbl pr
 JOIN AdrTbl ca ON pr.CDCRegnCd = ca.CDCRegnCd
 AND pr.CDCNmb = ca.CDCNmb ) src
 ON ( pr.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 MERGE INTO TTRjctData2Tbl rd
 USING (SELECT rd.ROWID row_id, ln.PrgrmNmb, ln.BorrNm
 FROM TTRjctData2Tbl rd
 JOIN LoanTbl ln ON rd.LoanNmb = ln.LoanNmb ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET PrgrmNmb = src.PrgrmNmb,
 BorrNm = src.BorrNm;
 MERGE INTO TTRjctData2Tbl rd
 USING (SELECT rd.ROWID row_id, rc.RjctCdDesc
 FROM TTRjctData2Tbl rd
 JOIN RjctCdDescTbl rc ON rd.RjctCd = rc.RjctCd ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RjctCdDesc = src.RjctCdDesc;
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTRjctData2Tbl (PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,BorrNm,LoanNmb,PostingDt,PymtTyp,RjctDt,RjctCd,RjctCdDesc,RjctAmt,PymtPosted,ACHDel,ACHMultiRjct,EmailNote, CreatUserID, LastUpdtUserID)
 ( SELECT NULL PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 NULL CDCNm ,
 NULL borroer_name ,
 LoanNmb LoanNmb ,
 PostingDt PostingDt ,
 PymtTyp PymtTyp ,
 RjctDt RjctDt ,
 RjctCd RjctCd ,
 NULL RjctCdDesc ,
 RjctAmt RjctAmt ,
 PymtPosted PymtPosted ,
 CASE 
 WHEN ACHDel = 1 THEN 'Y'
 ELSE 'N'
 END ACHDel ,
 CASE 
 WHEN ACHMultiRjct = 1 THEN 'Y'
 ELSE 'N'
 END ACHMultiRjct ,
 EmailNote EmailNote ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE RjctDt = p_RjctDt
 AND CDCRegnCd = p_CDCRegn
 AND CDCNmb = p_CDCNmb );
 MERGE INTO TTRjctData2Tbl pr
 USING (SELECT pr.ROWID row_id, ca.CDCNm
 FROM TTRjctData2Tbl pr
 JOIN AdrTbl ca ON pr.CDCRegnCd = ca.CDCRegnCd
 AND pr.CDCNmb = ca.CDCNmb ) src
 ON ( pr.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 MERGE INTO TTRjctData2Tbl rd
 USING (SELECT rd.ROWID row_id, ln.PrgrmNmb, ln.BorrNm
 FROM TTRjctData2Tbl rd
 JOIN LoanTbl ln ON rd.LoanNmb = ln.LoanNmb ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET PrgrmNmb = src.PrgrmNmb,
 BorrNm = src.BorrNm;
 MERGE INTO TTRjctData2Tbl rd
 USING (SELECT rd.ROWID row_id, rc.RjctCdDesc
 FROM TTRjctData2Tbl rd
 JOIN RjctCdDescTbl rc ON rd.RjctCd = rc.RjctCd ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RjctCdDesc = src.RjctCdDesc;
 
 END;
 END IF;
 INSERT INTO TTRjctData2Tbl (PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,BorrNm,LoanNmb,PostingDt,PymtTyp,RjctDt,RjctCd,RjctCdDesc,RjctAmt,PymtPosted,ACHDel,ACHMultiRjct,EmailNote, CreatUserID, LastUpdtUserID)
 ( SELECT NULL PrgrmNmb ,
 '99' CDCRegnCd ,
 '9999' CDCNmb ,
 NULL CDCNm ,
 NULL borroer_name ,
 COUNT(LoanNmb) LoanNmb ,
 NULL PostingDt ,
 NULL PymtTyp ,
 NULL RjctDt ,
 NULL RjctCd ,
 NULL RjctCdDesc ,
 SUM(RjctAmt) RjctAmt ,
 NULL PymtPosted ,
 NULL ACHDel ,
 NULL ACHMultiRjct ,
 NULL EmailNote ,
 USER ,
 USER
 FROM TTRjctData2Tbl );
 IF p_OutputTyp = 'S' THEN
 
 BEGIN
 IF p_CDCRegn IS NULL
 AND p_CDCNmb IS NULL THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT PrgrmNmb PrgrmNmb ,
 LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)) CDCNmb ,
 LoanNmb LoanNmb ,
 CDCNm CDCNm ,
 BorrNm BorrNm ,
 PostingDt PostingDt ,
 RjctAmt RjctAmt ,
 RjctDt RjctDt ,
 RjctCdDesc RjctCdDesc ,
 'R' || RjctCd RjctCd ,
 ACHDel ACHDel ,
 ACHMultiRjct ACHMultiRjct 
 FROM TTRjctData2Tbl 
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT PrgrmNmb PrgrmNmb ,
 LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)) CDCNmb ,
 LoanNmb LoanNmb ,
 CDCNm CDCNm ,
 BorrNm BorrNm ,
 PostingDt PostingDt ,
 RjctAmt RjctAmt ,
 RjctDt RjctDt ,
 RjctCdDesc RjctCdDesc ,
 'R' || RjctCd RjctCd ,
 ACHDel ACHDel ,
 ACHMultiRjct ACHMultiRjct 
 FROM TTRjctData2Tbl 
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 DELETE FROM TTFile2Tbl;
 -- max length of the records, try to keep it short
 -- since osql packs spaces after each line
 IF p_CDCRegn IS NULL
 AND p_CDCNmb IS NULL THEN
 
 BEGIN
 INSERT INTO TTFile2Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 ( SELECT 'PROGRAM NUMBER' || CHR(9) || 'CDC NUMBER' || CHR(9) || 'LOAN NUMBER' || CHR(9) || 'BORROWER NAME' || CHR(9) || 'POSTING DATE' || CHR(9) || 'REJECTED DATE' || CHR(9) || 'REJECTED Amt' || CHR(9) || 'REJECTED CODE' || CHR(9) || 'REJECTED REASON' || CHR(9) || 'ACH DELETE' || CHR(9) || 'ACH MULTIPLE REJECT' || CHR(13) , USER, USER
 FROM DUAL );
 INSERT INTO TTFile2Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 SELECT NVL(PrgrmNmb, ' ') || CHR(9) || NVL(LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)), ' ') || CHR(9) || NVL(LoanNmb, ' ') || CHR(9) || NVL(BorrNm, ' ') || CHR(9) || NVL(TO_CHAR(PostingDt, 'MM/DD/YYYY'), ' ') || CHR(9) || NVL(TO_CHAR(RjctDt, 'MM/DD/YYYY'), ' ') || CHR(9) || NVL(RjctAmt, ' ') || CHR(9) || NVL(RjctCd, ' ') || CHR(9) || NVL(RjctCdDesc, ' ') || CHR(9) || NVL(ACHDel, ' ') || CHR(9) || NVL(ACHMultiRjct, ' ') || CHR(13) , USER, USER 
 FROM TTRjctData2Tbl 
 WHERE CDCNmb <> '9999'
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb;
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTFile2Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 ( SELECT 'PROGRAM NUMBER' || CHR(9) || 'CDC NUMBER' || CHR(9) || 'LOAN NUMBER' || CHR(9) || 'BORROWER NAME' || CHR(9) || 'POSTING DATE' || CHR(9) || 'REJECTED DATE' || CHR(9) || 'REJECTED Amt' || CHR(9) || 'REJECTED CODE' || CHR(9) || 'REJECTED REASON' || CHR(9) || 'ACH DELETE' || CHR(9) || 'ACH MULTIPLE REJECT' || CHR(13) , USER, USER
 FROM DUAL );
 INSERT INTO TTFile2Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 SELECT NVL(PrgrmNmb, ' ') || CHR(9) || NVL(LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)), ' ') || CHR(9) || NVL(LoanNmb, ' ') || CHR(9) || NVL(BorrNm, ' ') || CHR(9) || NVL(TO_CHAR(PostingDt, 'MM/DD/YYYY'), ' ') || CHR(9) || NVL(TO_CHAR(RjctDt, 'MM/DD/YYYY'), ' ') || CHR(9) || NVL(TO_CHAR(RjctAmt), ' ') || CHR(9) || NVL(RjctCd, ' ') || CHR(9) || NVL(RjctCdDesc, ' ') || CHR(9) || NVL(ACHDel, ' ') || CHR(9) || NVL(ACHMultiRjct, ' ') || CHR(13) , USER, USER
 FROM TTRjctData2Tbl 
 WHERE CDCNmb <> '9999'
 ORDER BY LoanNmb;
 
 END;
 END IF;
 OPEN p_SelCur2 FOR
 SELECT * 
 FROM TTFile2Tbl ;/*
 R02
 R07
 R08
 R10
 R13
 R16
 R29
 */
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatNonCurrentSelTSP
 --- CDC_Status_Noncurrent_R null, '02','650'
 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 PrgrmNmb ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 BorrNm ,
 IssDt ,
 MatDt ,
 LastPymtDtx ,
 LoanAmt ,
 MoPymtAmt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 DiffToEscrowAmt ,
 TotFeeDueAmt ,
 IntDueAmt ,
 PrinDueAmt ,
 LateFeeDueAmt ,
 TotDueAmt ,
 PrinLeftAmt ,
 CDCPortflStatCdCd ,
 LASTUPDTDT
 FROM PortflTbl
 WHERE TRIM(CDCRegnCd) = p_RegionNum
 AND TRIM(CDCNmb) = p_CdcNum
 AND ( SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) IS NOT NULL
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY BorrNm ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 PrgrmNmb ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 BorrNm ,
 IssDt ,
 MatDt ,
 LastPymtDtx ,
 LoanAmt ,
 MoPymtAmt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 DiffToEscrowAmt ,
 TotFeeDueAmt ,
 IntDueAmt ,
 PrinDueAmt ,
 LateFeeDueAmt ,
 TotDueAmt ,
 PrinLeftAmt ,
 CDCPortflStatCdCd ,
 LASTUPDTDT
 FROM PortflTbl
 --WHEREUserLevelRoleID = @SOD
 WHERE TRIM(CDCRegnCd) = p_RegionNum
 AND TRIM(CDCNmb) = p_CdcNum
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY BorrNm ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 PrgrmNmb ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 StmtNm ,
 IssDt ,
 MatDt ,
 LastPymtDtx ,
 LoanAmt ,
 MoPymtAmt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 DiffToEscrowAmt ,
 TotFeeDueAmt ,
 IntDueAmt ,
 PrinDueAmt ,
 LateFeeDueAmt ,
 TotDueAmt ,
 PrinLeftAmt ,
 CDCPortflStatCdCd ,
 LASTUPDTDT
 FROM PortflTbl
 WHERE TRIM(CDCRegnCd) = p_RegionNum
 AND TRIM(CDCNmb) = p_CdcNum
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY StmtNm ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


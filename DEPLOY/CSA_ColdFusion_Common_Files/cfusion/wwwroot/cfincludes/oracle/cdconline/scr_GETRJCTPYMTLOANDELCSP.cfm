<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetRjctPymtLoanDelCSP 
 (
 p_sba_loan_no IN CHAR DEFAULT NULL ,
 p_posting_date IN DATE DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 ----cdc_get_rejected_payments_loan_r '2776856002', '06/03/2008'
 v_posting_month NUMBER(3,0);
 v_posting_year NUMBER(5,0);
 v_prior_posting_date DATE;
 
 BEGIN
 
 v_posting_month := EXTRACT(MONTH FROM ADD_MONTHS(p_posting_date, -1)) ;
 v_posting_year := EXTRACT(YEAR FROM ADD_MONTHS(p_posting_date, -1)) ;
 v_prior_posting_date := TRUNC(ADD_MONTHS(p_posting_date, -1), 'MONTH');
 DELETE FROM TTRjctDataTbl;
 --cdc_get_rejected_payments_r '06/04/2008'
 ----cdc_get_rejected_payments_r '06/04/2008' , '06','102'
 --drop table CDCTTRjctDataTbl
 INSERT INTO TTRjctDataTbl(PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,BorrNm,LoanNmb,PostingDt,RjctDt,RjctCd,RjctCdDesc,RjctAmt, CreatUserID, LastUpdtUserID)
 ( SELECT NULL PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 NULL CDCNm ,
 NULL BorrNm ,
 LoanNmb LoanNmb ,
 PostingDt PostingDt ,
 RjctDt RjctDt ,
 RjctCd RjctCd ,
 NULL RjctCdDesc ,
 RjctAmt RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE LoanNmb = p_sba_loan_no
 AND PostingDt <= p_posting_date
 AND PostingDt > v_prior_posting_date );
 MERGE INTO TTRjctDataTbl 
 USING (SELECT TTRjctDataTbl.ROWID row_id, po.CDCNm
 FROM TTRjctDataTbl ,PymtRjctHistryTbl pr
 JOIN PortflTbl po ON pr.LoanNmb = po.LoanNmb ) src
 ON ( TTRjctDataTbl.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 MERGE INTO TTRjctDataTbl rd
 USING (SELECT rd.ROWID row_id, ln.PrgrmNmb, ln.BorrNm
 FROM TTRjctDataTbl rd
 JOIN LoanTbl ln ON rd.LoanNmb = ln.LoanNmb ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET PrgrmNmb = src.PrgrmNmb,
 BorrNm = src.BorrNm;
 MERGE INTO TTRjctDataTbl rd
 USING (SELECT rd.ROWID row_id, rc.RjctCdDesc
 FROM TTRjctDataTbl rd
 JOIN RjctCdDescTbl rc ON rd.RjctCd = rc.RjctCd ) src
 ON ( rd.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RjctCdDesc = src.RjctCdDesc;
 OPEN p_SelCur1 FOR
 SELECT PrgrmNmb PrgrmNmb ,
 LTRIM(RTRIM(CDCRegnCd)) || '-' || LTRIM(RTRIM(CDCNmb)) CDCNmb ,
 LoanNmb LoanNmb ,
 CDCNm CDCNm ,
 BorrNm BorrNm ,
 PostingDt PostingDt ,
 RjctAmt RjctAmt ,
 RjctDt RjctDt ,
 RjctCdDesc RjctCdDesc ,
 'R' || RjctCd RjctCd 
 FROM TTRjctDataTbl 
 ORDER BY PostingDt DESC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


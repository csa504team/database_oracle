<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE PayoutStmtSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT StmtDt StmtDt ,
 ACHInd ACHInd 
 FROM PayoutStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 ORDER BY StmtDt DESC ;--cdc_payout_statements_r '02','005'
 --select * from PayoutStmtTbl 
 --WHERE CDCRegn = '02'
 --AND CDCNmb = '005'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


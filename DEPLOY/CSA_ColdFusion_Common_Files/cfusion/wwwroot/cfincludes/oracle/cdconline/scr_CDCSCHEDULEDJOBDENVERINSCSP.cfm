<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE CDCScheduledJobDenverInsCSP
 (
 p_Identifier IN number := 0,
 --p_Qry IN SYS_REFCURSOR,
 p_RetVal OUT number,
 p_ErrVal OUT number,
 p_ErrMsg OUT varchar2,
 p_SysRefCursor1 OUT SYS_REFCURSOR
 )
 AS
 -- variable declaration
 BEGIN
 SAVEPOINT CDCScheduledJobDenverInsCSP;
 
 IF p_Identifier = 0 OR p_Identifier IS NULL
 THEN
 /* Insert into xxxxTbl Table */
 BEGIN
 OPEN p_SysRefCursor1 FOR
 SELECT * FROM cdconline.OutGntyTbl WHERE LoanNmb = '2922256009';
 
 p_RetVal := SQL%ROWCOUNT;
 p_ErrVal := SQLCODE;
 p_ErrMsg := SQLERRM;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 p_RetVal := 0;
 p_ErrVal := SQLCODE;
 p_ErrMsg := SQLERRM;
 
 ROLLBACK TO CDCScheduledJobDenverInsCSP;
 RAISE;
 END;
 END CDCScheduledJobDenverInsCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTSchdlClsSelCSP
 (
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_sort_order IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 v_start_date DATE;
 v_end_date DATE;
 
 BEGIN
 
 v_start_date := TRUNC(SYSDATE, 'MONTH');
 v_end_date := ADD_MONTHS(v_start_date, 1);
 v_end_date := v_end_date - INTERVAL '1' DAY;
 ---order by Sba Loan Number
 IF p_sort_order = 'LoanNmb' THEN
 
 BEGIN
 ----Colson users
 IF TRIM(p_cdc_num) = '0' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND CDCPortflStatCdCd = 'CL'
 AND TRUNC(v_start_date) - TRUNC(r.PrepayDt) <= 0
 AND TRUNC(v_end_date) - TRUNC(r.PrepayDt) >= 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.LoanNmb ASC ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur1 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND RTRIM(LTRIM(l.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND RTRIM(LTRIM(l.CDCRegnCd)) = RTRIM(LTRIM(p_cdc_region))
 AND CDCPortflStatCdCd = 'CL'
 AND TRUNC(v_start_date) - TRUNC(r.PrepayDt) <= 0
 AND TRUNC(v_end_date) - TRUNC(r.PrepayDt) >= 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.LoanNmb ASC ;
 END IF;
 
 END;
 
 ---order by Prepay Date
 ELSE
 ----Colson users
 IF p_cdc_num = '0' THEN
 
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND CDCPortflStatCdCd = 'CL'
 AND TRUNC(v_start_date) - TRUNC(r.PrepayDt) <= 0
 AND TRUNC(v_end_date) - TRUNC(r.PrepayDt) >= 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.PrepayDt DESC,
 r.LoanNmb ASC ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur2 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND RTRIM(LTRIM(l.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND RTRIM(LTRIM(l.CDCRegnCd)) = RTRIM(LTRIM(p_cdc_region))
 AND CDCPortflStatCdCd = 'CL'
 AND TRUNC(v_start_date) - TRUNC(r.PrepayDt) <= 0
 AND TRUNC(v_end_date) - TRUNC(r.PrepayDt) >= 0
 AND r.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY r.PrepayDt DESC,
 r.LoanNmb ASC ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


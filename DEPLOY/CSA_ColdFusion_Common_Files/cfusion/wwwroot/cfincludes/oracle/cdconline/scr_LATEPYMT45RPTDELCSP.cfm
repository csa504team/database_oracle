<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE LatePymt45RptDelCSP
 -- drop procedure CDC_LatePayment45_report_r
 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CDC_Cert IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 ---CDC_LatePayment45_report_r ' ', '04','493',0
 --select * from PortflTbl
 --where LoanNmb = '1002486000'
 v_current_date DATE;
 
 BEGIN
 
 DELETE FROM TTTempDelnqTbl;
 v_current_date := SYSDATE ;
 IF p_SBA = 1 THEN
 
 BEGIN
 INSERT INTO TTTempDelnqTbl
 ( UserLevelRoleID, DistNm, CDCRegnCd, CDCNmb, CDCNm, StmtNm, LoanNmb, DueAmt, PymtDue, MoDiff, IssDt, CreatUserID, LastUpdtUserID )
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.StmtNm StmtNm ,
 delq.LoanNmb LoanNmb ,
 delq.DueAmt DueAmt ,
 delq.PymtDue PymtDue ,
 MONTHS_BETWEEN(LAST_DAY(v_current_date), LAST_DAY(sp.IssDt)) MoDiff ,
 sp.IssDt IssDt ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm;
 
 END;
 ELSE
 IF LTRIM(RTRIM(p_SOD)) > 0 THEN
 
 BEGIN
 INSERT INTO TTTempDelnqTbl
 ( UserLevelRoleID, DistNm, CDCRegnCd, CDCNmb, CDCNm, StmtNm, LoanNmb, DueAmt, PymtDue, MoDiff, IssDt, CreatUserID, LastUpdtUserID )
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.StmtNm StmtNm ,
 delq.LoanNmb LoanNmb ,
 delq.DueAmt DueAmt ,
 delq.PymtDue PymtDue ,
 MONTHS_BETWEEN(LAST_DAY(v_current_date), LAST_DAY(sp.IssDt)) MoDiff ,
 sp.IssDt IssDt ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY sp.StmtNm;
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTTempDelnqTbl
 ( UserLevelRoleID, DistNm, CDCRegnCd, CDCNmb, CDCNm, StmtNm, LoanNmb, DueAmt, PymtDue, MoDiff, IssDt, CreatUserID, LastUpdtUserID )
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.StmtNm StmtNm ,
 delq.LoanNmb LoanNmb ,
 delq.DueAmt DueAmt ,
 delq.PymtDue PymtDue ,
 MONTHS_BETWEEN(LAST_DAY(v_current_date), LAST_DAY(sp.IssDt)) MoDiff ,
 sp.IssDt IssDt ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm;
 
 END;
 END IF;
 END IF;
 UPDATE TTTempDelnqTbl
 SET NxtSemiAnnDt = TRUNC(ADD_MONTHS(IssDt, (MoDiff + 6 - (MOD(MoDiff, 6)))))
 WHERE MOD(MoDiff, 6) <> 0;
 UPDATE TTTempDelnqTbl
 SET NxtSemiAnnDt = TRUNC(ADD_MONTHS(IssDt, (MoDiff + 6)))
 WHERE MOD(MoDiff, 6) = 0;
 --
 MERGE INTO TTTempDelnqTbl tmp
 USING (SELECT tmp.ROWID row_id, ln.RefiLoan
 FROM TTTempDelnqTbl tmp ,LoanTbl ln
 WHERE tmp.LoanNmb = ln.LoanNmb) src
 ON ( tmp.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 StmtNm StmtNm ,
 LoanNmb LoanNmb ,
 DueAmt DueAmt ,
 PymtDue PymtDue ,
 TO_CHAR(NxtSemiAnnDt, 'MM/YYYY') NxtSemiAnnDt ,
 RefiLoan
 FROM TTTempDelnqTbl
 ORDER BY StmtNm ;
 OPEN p_SelCur2 FOR
 SELECT SUM(DueAmt) total_amount_due
 FROM TTTempDelnqTbl ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


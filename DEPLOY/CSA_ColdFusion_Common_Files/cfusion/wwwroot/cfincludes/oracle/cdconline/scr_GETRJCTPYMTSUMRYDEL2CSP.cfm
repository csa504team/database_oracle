<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetRjctPymtSumryDel2CSP 
 (
 p_rejected_month IN NUMBER DEFAULT NULL ,
 p_rejected_year IN NUMBER DEFAULT NULL ,
 p_cdc_region IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTRjctPymtTbl;
 DELETE FROM TTSumryTbl;
 ----cdc_get_rejected_payments_summary_r 6,2008,'01','311'
 ----cdc_get_rejected_payments_summary_r 6,2008
 --PostingDt smalldatetime NULL,
 IF p_cdc_region IS NULL
 AND p_cdc_num IS NULL THEN
 
 BEGIN
 INSERT INTO TTRjctPymtTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 SELECT DISTINCT RjctDt RjctDt ,
 ---PostingDt = PostingDt ,
 NULL RjctPymt ,
 NULL RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month
 ORDER BY RjctDt;
 INSERT INTO TTSumryTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 ( SELECT RjctDt RjctDt ,
 COUNT(LoanNmb) RjctPymt ,
 SUM(RjctAmt) RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month
 GROUP BY RjctDt );
 MERGE INTO TTRjctPymtTbl rp
 USING (SELECT rp.ROWID row_id, S.RjctPymt, S.RjctAmt
 FROM TTRjctPymtTbl rp
 JOIN TTSumryTbl S ON rp.RjctDt = S.RjctDt ) src
 ON ( rp.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RjctPymt = src.RjctPymt,
 RjctAmt = src.RjctAmt;
 INSERT INTO TTRjctPymtTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 ( SELECT RjctDt RjctDt ,
 ---PostingDt = NULL ,
 COUNT(LoanNmb) RjctPymt ,
 SUM(RjctAmt) RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month );
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTRjctPymtTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 SELECT DISTINCT RjctDt RjctDt ,
 ---PostingDt = PostingDt ,
 NULL RjctPymt ,
 NULL RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month
 AND CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 ORDER BY RjctDt;
 INSERT INTO TTSumryTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 ( SELECT RjctDt RjctDt ,
 COUNT(LoanNmb) RjctPymt ,
 SUM(RjctAmt) RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month
 AND CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 GROUP BY RjctDt );
 MERGE INTO TTRjctPymtTbl rp
 USING (SELECT rp.ROWID row_id, S.RjctPymt, S.RjctAmt
 FROM TTRjctPymtTbl rp
 JOIN TTSumryTbl S ON rp.RjctDt = S.RjctDt ) src
 ON ( rp.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RjctPymt = src.RjctPymt,
 RjctAmt = src.RjctAmt;
 INSERT INTO TTRjctPymtTbl (RjctDt,RjctPymt,RjctAmt, CreatUserID, LastUpdtUserID)
 ( SELECT RjctDt RjctDt ,
 --PostingDt = NULL ,
 COUNT(LoanNmb) RjctPymt ,
 SUM(RjctAmt) RjctAmt ,
 USER ,
 USER
 FROM PymtRjctHistryTbl 
 WHERE EXTRACT(YEAR FROM RjctDt) = p_rejected_year
 AND EXTRACT(MONTH FROM RjctDt) = p_rejected_month
 AND CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num );
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTRjctPymtTbl 
 ORDER BY RjctDt DESC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


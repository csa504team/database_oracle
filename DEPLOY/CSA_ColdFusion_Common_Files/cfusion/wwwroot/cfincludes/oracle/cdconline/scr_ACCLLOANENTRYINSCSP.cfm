<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclLoanEntryInsCSP
 (
 p_sba_loan_no IN CHAR,
 p_sys_userid IN VARCHAR2,
 p_sys_progid IN VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 /*
 accl_loan_entry_add_m '1910604002', 'test', 'test'
 
 select IssDt, * from CDCLoanTbl
 whereLoanStat = 'active'
 where month(IssDt) = 4
 
 */
 v_issue_date DATE;
 v_semi_annual_date DATE;
 v_month_diff NUMBER(10,0);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 SELECT TRUNC(IssDt, 'MONTH')
 
 INTO v_issue_date
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no;
 -- calculate the next s/a date
 -- v_issue_date := UTILS.CONVERT_TO_SMALLDATETIME(UTILS.CONVERT_TO_VARCHAR2(utils.month_(v_issue_date),30) || '/01/' || UTILS.CONVERT_TO_VARCHAR2(utils.year_(v_issue_date),30)) ;
 v_month_diff := MONTHS_BETWEEN(TRUNC(LAST_DAY(SYSDATE)), TRUNC(LAST_DAY(v_issue_date)));
 -- if s/a month is the same as request month, get the next s/a date
 IF MOD(v_month_diff, 6) = 0 THEN
 v_semi_annual_date := TRUNC(ADD_MONTHS(v_issue_date, v_month_diff + 6));
 
 -- if request date is on or after 20th and the s/a month is 1 month after the request
 
 -- month, get the next s/a date
 ELSE
 IF MOD(v_month_diff, 6) = 5
 AND EXTRACT(DAY FROM SYSDATE) >= 20 THEN
 v_semi_annual_date := TRUNC(ADD_MONTHS(v_issue_date, v_month_diff + 7)) ;
 
 -- else get the very next s/a date
 ELSE
 v_semi_annual_date := TRUNC(ADD_MONTHS(v_issue_date, v_month_diff + 6 - (MOD(v_month_diff, 6))));
 END IF;
 END IF;
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE NOT EXISTS ( SELECT 'Y'
 FROM AcclLoanEntryTbl
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 INSERT INTO AcclLoanEntryTbl
 ( CDCACCLLOANENTRYTBLPK, RqstDt, LoanNmb, SemiAnnDt, DbentrPrinBalAmt, DbentrIntDueAmt, SYSProgID, SYSUserID, SYSLastTime, CreatUserID, LastUpdtUserID)
 ( SELECT CDCAcclLoanEntryTblPKSEQ.NEXTVAL,
 SYSDATE AS RqstDt ,
 TRIM(p_sba_loan_no) LoanNmb ,
 v_semi_annual_date SemiAnnDt ,
 0 DbentrPrinBalAmt ,
 0 DbentrIntDueAmt ,
 p_sys_progid SYSProgID ,
 p_sys_userid SYSUserID ,
 SYSDATE SYSLastTime , USER , USER
 FROM DUAL );
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT *
 FROM AcclLoanEntryTbl
 WHERE LoanNmb = p_sba_loan_no ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


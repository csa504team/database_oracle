<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatNonCurrentRptSelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTempPortflTbl;
 ----select * from CDCLoanTbl where RefiLoan = 'Y'
 ---CDC_Status_Noncurrent_report_R NULL,'09','024'
 ---CDC_Status_Noncurrent_report_R '0100','01','246'
 --BorrNm varchar(30) NULL,
 IF p_SBA = 1 THEN
 INSERT INTO TTTempPortflTbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,LoanStatCd,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd CDCPortflStatCdCd ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl 
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY BorrNm;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 INSERT INTO TTTempPortflTbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,LoanStatCd,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd LoanStatCd ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2) )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY BorrNm;
 ELSE
 INSERT INTO TTTempPortflTbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,LoanStatCd,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd LoanStatCd ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl 
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' )
 ORDER BY StmtNm;
 END IF;
 END IF;
 MERGE INTO TTTempPortflTbl p
 USING (SELECT p.ROWID row_id, ln.RefiLoan
 FROM TTTempPortflTbl p
 JOIN LoanTbl ln ON p.LoanNmb = ln.LoanNmb ) src
 ON ( p.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 LoanStatCd LoanStatCd ,
 RefiLoan RefiLoan 
 FROM TTTempPortflTbl ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


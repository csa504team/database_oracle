<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE GetLoanInfoSelCSP
 (
 p_sba_loan_no IN CHAR,
 p_prepay_date IN CHAR,
 p_type IN NUMBER,
 p_action IN VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 v_next_sa_date DATE;
 v_issue_date DATE;
 v_gfd_aval CHAR(1);
 /*get semi-annual date that is closest to current date --03/03/03 as of John's request--*/
 /*if isdate(@IssDt)=1
 begin
 set @next_sa_date = convert(varchar(2),datepart(mm,@IssDt)) +'/01/'+ convert(varchar(4),datepart(yyyy,getdate()))
 if datepart(mm,@IssDt) > datepart(mm,getdate()) and datepart(mm,@IssDt)-datepart(mm,getdate())<=6
 begin
 set @next_sa_date=convert(varchar(2),datepart(mm,@IssDt)) +'/01/'+ convert(varchar(4),datepart(yyyy,getdate()))
 end
 else
 if datepart(mm,@IssDt)-datepart(mm,getdate())>6
 begin
 set @next_sa_date=dateadd(mm,-6,@next_sa_date)
 end
 else
 set @next_sa_date=dateadd(mm,6,@next_sa_date)
 
 end*/
 /* Rule:
 The next semi-annual date is always a multiple of 6 from the Issue date
 The next semi-annual date is always greater than the Prepayment date
 */
 v_MnDiff -- Number of months between the Issue Date and Prepayment date
 NUMBER(10,0);
 v_MntoSADt NUMBER(10,0);-- The number of months needed to make @MnDiff a multiple of 6 (SemiAnnual)
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT DISTINCT IssDt
 
 INTO v_issue_date
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND IssDt IS NOT NULL;
 IF v_issue_date IS NULL THEN
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT DISTINCT IssDt
 
 INTO v_issue_date
 FROM LoanSaveTbl
 WHERE LoanNmb = p_sba_loan_no
 AND IssDt IS NOT NULL;
 
 END;
 END IF;
 -- the sum of @MnDiff and @MnSADt will always be a multiple of 6
 v_MntoSADt := 0 ;
 v_MnDiff := MONTHS_BETWEEN(LAST_DAY(p_prepay_date), LAST_DAY(v_issue_date));
 IF MOD(v_MnDiff, 6) <> 0 THEN
 v_MntoSADt := v_MnDiff + 6 - (MOD(v_MnDiff, 6)) ;-- Adjust the number of months to the next sa date
 ELSE
 v_MntoSADt := v_MnDiff + 6 ;
 END IF;
 v_next_sa_date := TRUNC(ADD_MONTHS(v_issue_date, v_MntoSADt));
 --- check if there are 'open' wires avalible for this loan ---
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT *
 FROM WiresTbl
 WHERE LoanNmb = p_sba_loan_no
 AND WireStatCd = 'OP' );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_gfd_aval := 'Y' ;
 
 END;
 ELSE
 
 BEGIN
 v_gfd_aval := 'N' ;
 
 END;
 END IF;
 --- for new loan select data from CDCLoanTbl and CDCPortflTbl tables ---
 IF LOWER(p_action) = 'new' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT
 ' ' RqstREF ,---max(req_ref) + 1
 a.CntctNm RqstFrom ,
 ' ' GFDRecv ,
 ' ' PhnNmb ,
 a.FaxNmb ,
 a.EmailAdr ,
 v_next_sa_date SemiAnnDt ,
 ' ' PrepayDt ,
 p.CDCPortflStatCdCd ,
 ' ' RqstCd ,
 p.CDCNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 p.LoanNmb ,
 p.PrgrmNmb ,
 p.IssDt ,
 p.LoanAmt ,
 p.BorrNm ,
 NVL(CASE LTRIM(ln.SmllBusCons)
 WHEN ' ' THEN ln.BorrNm
 ELSE ln.SmllBusCons
 END, ln.BorrNm) SmllBusCons ,
 ' ' detail ,
 v_gfd_aval gfd_aval
 FROM PortflTbl p,
 LoanTbl ln,
 AdrTbl a
 WHERE p.LoanNmb = p_sba_loan_no
 AND p.LoanNmb = ln.LoanNmb
 AND p.CDCNmb = a.CDCNmb
 AND p.CDCRegnCd = a.CDCRegnCd ;
 
 END;
 ELSE
 DECLARE
 --- for existing records first look for record on CDCLoanSaveTbl(requests saved to history )
 v_req_ref NUMBER(10,0);
 v_temp NUMBER(1, 0) := 0;
 
 
 --- for loan edit select data from CDCRqstTbl table ---
 BEGIN
 SELECT MAX(RqstREF)
 
 INTO v_req_ref
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND RqstCd = p_type AND ROWNUM < 2;
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT r.RqstREF ,
 r.RqstFrom ,
 r.GFDRecv ,
 r.PhnNmb ,
 r.FaxNmb ,
 r.EmailAdr ,
 r.SemiAnnDt ,
 r.PrepayDt ,
 r.CDCPortflStatCdCd ,
 cd.RqstCd ,
 ls.CDCNm ,
 ls.CDCRegnCd || '-' || ls.CDCNmb CDCNmb ,
 r.LoanNmb ,
 ls.PrgrmNmb ,
 ls.IssDt ,
 ls.LoanAmt ,
 ls.BorrNm ,
 NVL(CASE LTRIM(ls.SmllBusCons)
 WHEN ' ' THEN ls.BorrNm
 ELSE ls.SmllBusCons
 END, ls.BorrNm) SmllBusCons ,
 dt.AudtDtl ,
 v_gfd_aval gfd_aval
 FROM RqstTbl r,
 LoanSaveTbl ls,
 RqstCdDescTbl cd,
 RqstDtlTbl dt
 WHERE r.RqstREF = v_req_ref
 AND r.RqstREF = ls.RqstREF
 AND r.RqstCd = cd.RqstCd
 AND r.RqstREF = dt.RqstREF ;
 
 END;
 ELSE
 
 
 --- if existing request not on history table,get data from portfolio table
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT r.RqstREF ,
 r.RqstFrom ,
 r.GFDRecv ,
 r.PhnNmb ,
 r.FaxNmb ,
 r.EmailAdr ,
 r.SemiAnnDt ,
 r.PrepayDt ,
 r.CDCPortflStatCdCd ,
 cd.RqstCd ,
 p.CDCNm ,
 p.CDCRegnCd || '-' || p.CDCNmb CDCNmb ,
 p.LoanNmb ,
 p.PrgrmNmb ,
 p.IssDt ,
 p.LoanAmt ,
 p.BorrNm ,
 NVL(CASE LTRIM(ln.SmllBusCons)
 WHEN ' ' THEN ln.BorrNm
 ELSE ln.SmllBusCons
 END, ln.BorrNm) SmllBusCons ,
 dt.AudtDtl ,
 v_gfd_aval gfd_aval
 FROM RqstTbl r,
 PortflTbl p,
 LoanTbl ln,
 RqstCdDescTbl cd,
 RqstDtlTbl dt
 WHERE r.RqstREF = v_req_ref
 AND r.LoanNmb = p.LoanNmb
 AND r.LoanNmb = ln.LoanNmb
 AND r.RqstCd = cd.RqstCd
 AND r.RqstREF = dt.RqstREF ;
 
 END;
 END IF;
 
 END;
 END IF;
 DELETE FROM TTTemp1Tbl;
 ---request status
 INSERT INTO TTTemp1Tbl (StatCd,CreatUserID,LastUpdtUserID)
 VALUES ( 'Open', USER, USER );--'OP'
 INSERT INTO TTTemp1Tbl (StatCd,CreatUserID,LastUpdtUserID)
 VALUES ( 'Cancelled', USER, USER );---'CN'
 INSERT INTO TTTemp1Tbl (StatCd,CreatUserID,LastUpdtUserID)
 VALUES ( 'Closed', USER, USER );---'CL'
 OPEN p_SelCur2 FOR
 SELECT StatCd
 FROM TTTemp1Tbl ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


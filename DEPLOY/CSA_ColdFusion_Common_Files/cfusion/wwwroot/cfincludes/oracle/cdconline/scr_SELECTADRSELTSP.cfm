<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE SelectAdrSelTSP 
 (
 p_Region IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 CDCMailAdrStr1Nm CDCMailAdrStr1Nm ,
 CDCMailAdrStr2Nm CDCMailAdrStr2Nm ,
 CDCMailAdrCtyNm CDCMailAdrCtyNm ,
 CDCMailAdrStCd CDCMailAdrStCd ,
 CDCMailAdrZipCd CDCMailAdrZipCd ,
 CntctNm CntctNm ,
 CntctTitl CntctTitl 
 FROM AdrTbl 
 WHERE CDCRegnCd = p_Region
 AND CDCNmb = p_CdcNum ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


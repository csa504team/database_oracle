<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE Get504AnnStmtByLoanNmbSelCSP 
 (
 p_year IN NUMBER,
 p_cdcregion IN VARCHAR2,
 p_cdcnum IN VARCHAR2,
 p_sba_loan_number IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 /*
 execute [CDC_get_504AnnualStatement_byLoannum] 2013 ,'09','024',1001956008
 */
 OPEN p_SelCur1 FOR
 SELECT SUM(asli.IntPaidAmt) total_interestpaid ,
 SUM(asli.CDCFeePaidAmt) total_cdcfees ,
 SUM(asli.CSAFeePaidAmt) total_csa_fees ,
 SUM(asli.SBAFeePaidAmt) total_sba_fees ,
 asr.LoanNmb ,
 asr.TaxID ,
 asr.BorrNm ,
 asr.BorrMailAdrStr1Nm ,
 asr.BorrMailAdrCtyNm ,
 asr.BorrMailAdrStCd ,
 asr.BorrMailAdrZipCd ,
 RTRIM(asr.BorrMailAdrCtyNm) || ', ' || RTRIM(asr.BorrMailAdrStCd) || ' ' || CASE 
 WHEN ( LENGTH(asr.BorrMailAdrZipCd) = 9 ) THEN SUBSTR(asr.BorrMailAdrZipCd, 0, 5) || '-' || SUBSTR(asr.BorrMailAdrZipCd, -4, 4)
 ELSE asr.BorrMailAdrZipCd
 END BorowrCtySTZipCd ,
 asr.NoteBalAmt ,
 asr.CDCNm ,
 asr.CDCPhnNmb ,
 asr.BorrDesgntn 
 FROM AnnStmtLoanInfoTbl asli
 RIGHT JOIN AnnStmtRecipntTbl asr ON asli.LoanNmb = asr.LoanNmb
 AND asli.CDCNmb = asr.CDCNmb
 AND asli.CDCRegnCd = asr.CDCRegnCd
 AND asli.TaxID = asr.TaxID
 AND asli.TaxYrNmb = asr.TaxYrNmb
 AND asli.BorrDesgntn = asr.BorrDesgntn
 WHERE asr.TaxYrNmb = p_year
 AND asr.CDCRegnCd = p_cdcregion
 AND asr.CDCNmb = p_cdcnum
 AND asr.LoanNmb = p_sba_loan_number
 GROUP BY asr.LoanNmb,asr.TaxID,asr.BorrNm,asr.BorrMailAdrStr1Nm,asr.BorrMailAdrCtyNm,asr.BorrMailAdrStCd,asr.BorrMailAdrZipCd,asr.NoteBalAmt,asr.CDCNm,asr.CDCPhnNmb,asr.BorrDesgntn ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


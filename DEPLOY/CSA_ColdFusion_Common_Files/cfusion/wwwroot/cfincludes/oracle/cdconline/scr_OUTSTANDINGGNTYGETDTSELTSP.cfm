<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE OutstandingGntyGetDtSelTSP 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT RptDt 
 FROM OutGntyTbl 
 ORDER BY RptDt DESC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


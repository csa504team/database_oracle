<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatPortflREFISelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 --select * from CDCLoanTbl where RefiLoan = 'Y'
 ---CDC_Status_Portfolio_REFI_R NULL,'09','024'
 ---CDC_Status_Portfolio_REFI_R '1086','10','468'
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT ln.CDCRegnCd ,
 ln.CDCNmb ,
 ad.CDCNm ,
 ln.ACHBnk ,
 ln.BorrNm ,
 ln.LoanNmb ,
 ln.IssDt ,
 ln.MatDt ,
 ln.NotePrinAmt ,
 ln.AmortIntAmt ,
 ln.AmortPrinAmt ,
 ln.AmortPymtAmt ,
 ln.PrinLeftAmt 
 FROM LoanTbl ln
 JOIN AdrTbl ad ON ln.CDCRegnCd = ad.CDCRegnCd
 AND ln.CDCNmb = ad.CDCNmb
 WHERE ln.CDCRegnCd = p_RegionNum
 AND ln.CDCNmb = p_CdcNum
 AND ln.RefiLoan = 'Y'
 ORDER BY ln.ACHBnk ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT ln.CDCRegnCd ,
 ln.CDCNmb ,
 ad.CDCNm ,
 ln.ACHBnk ,
 ln.BorrNm ,
 ln.LoanNmb ,
 ln.IssDt ,
 ln.MatDt ,
 ln.NotePrinAmt ,
 ln.AmortIntAmt ,
 ln.AmortPrinAmt ,
 ln.AmortPymtAmt ,
 ln.PrinLeftAmt 
 FROM LoanTbl ln
 JOIN AdrTbl ad ON ln.CDCRegnCd = ad.CDCRegnCd
 AND ln.CDCNmb = ad.CDCNmb
 WHERE ln.CDCRegnCd = p_RegionNum
 AND ln.CDCNmb = p_CdcNum
 AND ln.RefiLoan = 'Y'
 AND SUBSTR(ln.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY ln.ACHBnk ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT ln.CDCRegnCd ,
 ln.CDCNmb ,
 ad.CDCNm ,
 ln.ACHBnk ,
 ln.BorrNm ,
 ln.LoanNmb ,
 ln.IssDt ,
 ln.MatDt ,
 ln.NotePrinAmt ,
 ln.AmortIntAmt ,
 ln.AmortPrinAmt ,
 ln.AmortPymtAmt ,
 ln.PrinLeftAmt 
 FROM LoanTbl ln
 JOIN AdrTbl ad ON ln.CDCRegnCd = ad.CDCRegnCd
 AND ln.CDCNmb = ad.CDCNmb
 WHERE ln.CDCRegnCd = p_RegionNum
 AND ln.CDCNmb = p_CdcNum
 AND ln.RefiLoan = 'Y'
 ORDER BY ln.ACHBnk ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


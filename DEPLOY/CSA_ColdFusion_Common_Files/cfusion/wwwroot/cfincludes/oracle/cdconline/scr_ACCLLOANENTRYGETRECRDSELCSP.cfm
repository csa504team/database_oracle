<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclLoanEntryGetRecrdSelCSP (
 p_sba_loan_no IN VARCHAR2 DEFAULT '',
 p_serv_center_id IN VARCHAR2 DEFAULT '',
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 --
 -- accl_loan_entry_get_records_r @LoanNmb=NULL,@ServCntrID='sc02'
 -- accl_loan_entry_get_records_r @LoanNmb='2222416008'
 -- accl_loan_entry_get_records_r
 
 -- drop table tt_cdc_loan
 
 -- declare @LoanNmb char(10),@ServCntrID char(4)
 -- set @LoanNmb='2222416008'
 -- set @ServCntrID = 'sc02'
 
 -- get cdc records based on the servicing center,if cust. svc. get all cdc's
 INSERT INTO TTVCDCTbl ( CDCRegnCd, CDCNmb, CreatUserID, LastUpdtUserID ) ( SELECT DISTINCT
 CDCRegnCd,
 CDCNmb ,
 USER ,
 USER
 FROM
 ServCntrTbl
 WHERE (
 (
 p_serv_center_id IS NULL
 ) OR (
 ServCntrID = TRIM(p_serv_center_id)
 )
 ) );
 
 DELETE FROM TTCDCLoanTbl;
 
 INSERT INTO TTCDCLoanTbl (
 LoanNmb,
 CDCRegnCd,
 CDCNmb,
 IssDt,
 NotePrinAmt,
 NoteRtPct,
 LoanStatCd,
 CreatUserID,
 LastUpdtUserID
 ) ( SELECT
 LoanNmb,
 CDCRegnCd,
 CDCNmb,
 IssDt,
 NotePrinAmt,
 NoteRtPct,
 LoanStatCd,
 USER,
 USER
 FROM
 LoanTbl
 WHERE (
 LoanStatCd NOT IN (
 'ACCELER.','PREPAID'
 )
 ) );
 
 IF (
 p_sba_loan_no IS NULL
 ) THEN
 DECLARE
 v_temp NUMBER(1,0) := 0;
 BEGIN
 BEGIN
 SELECT
 1
 INTO
 v_temp
 FROM
 dual
 WHERE
 EXISTS (
 SELECT
 'Y'
 FROM
 AcclLoanEntryTbl e
 LEFT JOIN
 PortflTbl p
 ON e.LoanNmb = p.LoanNmb
 JOIN TTCDCLoanTbl l ON e.LoanNmb = l.LoanNmb
 JOIN TTVCDCTbl c ON
 l.CDCRegnCd = c.CDCRegnCd
 AND
 l.CDCNmb = c.CDCNmb
 );
 
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 BEGIN
 INSERT INTO TTVTmpTbl (
 RefID,
 RqstDt,
 LoanNmb,
 SemiAnnDt,
 DbentrPrinBalAmt,
 DbentrIntDueAmt,
 CDCNm,
 BorrNm,
 IssDt,
 NotePrinAmt,
 NoteRtPct,
 LoanStatCd,
 RvwCd,
 CreatUserID,
 LastUpdtUserID
 ) ( SELECT
 e.CDCAcclLoanEntryTblPK,
 e.RqstDt,
 e.LoanNmb,
 e.SemiAnnDt,
 e.DbentrPrinBalAmt,
 e.DbentrIntDueAmt,
 ltrim(rtrim(p.CDCNm) )CDCNm,
 p.BorrNm,
 l.IssDt,
 l.NotePrinAmt,
 l.NoteRtPct,
 l.LoanStatCd,
 NULL RvwCd,
 USER,
 USER
 FROM
 AcclLoanEntryTbl e
 LEFT JOIN
 PortflTbl p
 ON e.LoanNmb = p.LoanNmb
 JOIN TTCDCLoanTbl l ON e.LoanNmb = l.LoanNmb
 JOIN TTVCDCTbl c ON
 l.CDCRegnCd = c.CDCRegnCd
 AND
 l.CDCNmb = c.CDCNmb
 );
 
 END;
 
 ELSE
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT
 0 RefID,
 ' ' RqstDt,
 ' ' LoanNmb,
 ' ' SemiAnnDt,
 ' ' DbentrPrinBalAmt,
 ' ' DbentrIntDueAmt,
 ' ' CDCNm,
 ' ' BorrNm,
 ' ' IssDt,
 ' ' NotePrinAmt,
 ' ' NoteRtPct,
 ' ' LoanStatCd,
 ' ' RvwCd
 FROM
 dual;
 
 END;
 END IF;
 
 END;
 -- if @LoanNmb = ''
 ELSE
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT
 e.CDCAcclLoanEntryTblPK,
 e.RqstDt,
 e.LoanNmb,
 e.SemiAnnDt,
 e.DbentrPrinBalAmt,
 e.DbentrIntDueAmt,
 ltrim(rtrim(p.CDCNm) ) CDCNm,
 p.BorrNm,
 l.IssDt,
 l.NotePrinAmt,
 l.NoteRtPct,
 l.LoanStatCd
 FROM
 AcclLoanEntryTbl e
 LEFT JOIN
 PortflTbl p
 ON e.LoanNmb = p.LoanNmb
 JOIN LoanTbl l ON e.LoanNmb = l.LoanNmb
 JOIN TTVCDCTbl c ON
 l.CDCRegnCd = c.CDCRegnCd
 AND
 l.CDCNmb = c.CDCNmb
 WHERE (
 e.LoanNmb = p_sba_loan_no
 );
 
 END;
 END IF;
 
 IF p_serv_center_id IS NULL THEN
 
 -- customer svc needs to see the review code
 BEGIN
 UPDATE TTVTmpTbl
 SET
 RvwCd = 'D'
 WHERE
 LoanStatCd = 'DEFERRED';
 -- MERGE INTO CDCTTVTmpTbl
 -- USING (SELECT CDCTTVTmpTbl.ROWID row_id,'D' || p.STATUS AS RvwCd
 -- FROM CDCTTVTmpTbl ,CDCTTVTmpTbl t
 -- JOIN CDCPortflTbl p ON t.LoanNmb = p.LoanNmb
 -- WHERE ( t.LoanStat = 'DEFERRED'
 -- AND p.STATUS <> 'D' )) src
 -- ON ( CDCTTVTmpTbl.ROWID = src.row_id )
 -- WHEN MATCHED THEN UPDATE SET RvwCd = src.RvwCd;
 
 END;
 END IF;
 
 OPEN p_SelCur3 FOR
 SELECT
 RefID,
 RqstDt,
 LoanNmb,
 SemiAnnDt,
 DbentrPrinBalAmt,
 DbentrIntDueAmt,
 CDCNm,
 BorrNm,
 IssDt,
 NotePrinAmt,
 NoteRtPct,
 LoanStatCd,
 RvwCd
 FROM
 TTVTmpTbl
 ORDER BY RqstDt,LoanNmb;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


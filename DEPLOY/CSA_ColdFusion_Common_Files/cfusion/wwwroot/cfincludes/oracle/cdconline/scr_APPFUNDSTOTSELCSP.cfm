<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AppFundsTotSelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CDC_Cert IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(ap.LoanNmb) sba_loan_no_tt ,
 SUM(ap.SBAFeeAmt) sba_fee_tt ,
 SUM(ap.CSAFeeAmt) csa_fee_tt ,
 SUM(ap.CDCFeeAmt) cdc_fee_tt ,
 SUM(ap.LateFeeAmt) late_fee_tt ,
 SUM(ap.IntApplAmt) interest_applied_tt ,
 SUM(ap.PrinApplAmt) principal_applied_tt ,
 SUM(ap.DiffToEscrowAmt) diff_to_escrow_tt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert ;
 
 -- ORDER BY sp.BorrNm 
 ELSE
 IF LTRIM(RTRIM(p_SOD)) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(ap.LoanNmb) sba_loan_no_tt ,
 SUM(ap.SBAFeeAmt) sba_fee_tt ,
 SUM(ap.CSAFeeAmt) csa_fee_tt ,
 SUM(ap.CDCFeeAmt) cdc_fee_tt ,
 SUM(ap.LateFeeAmt) late_fee_tt ,
 SUM(ap.IntApplAmt) interest_applied_tt ,
 SUM(ap.PrinApplAmt) principal_applied_tt ,
 SUM(ap.DiffToEscrowAmt) diff_to_escrow_tt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 
 --WHERE SODNmb = @SOD
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2) ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT COUNT(ap.LoanNmb) sba_loan_no_tt ,
 SUM(ap.SBAFeeAmt) sba_fee_tt ,
 SUM(ap.CSAFeeAmt) csa_fee_tt ,
 SUM(ap.CDCFeeAmt) cdc_fee_tt ,
 SUM(ap.LateFeeAmt) late_fee_tt ,
 SUM(ap.IntApplAmt) interest_applied_tt ,
 SUM(ap.PrinApplAmt) principal_applied_tt ,
 SUM(ap.DiffToEscrowAmt) diff_to_escrow_tt 
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE GetAllCDC1099IntPDFSelCSP 
 -- =============================================
 -- Author: <Author :SBA504>
 -- Create date: <Create :10-07-2016>
 -- Description: <Description: select 1099 INT TaxYr>
 -- Modified: <Modified :,>
 -- =============================================
 
 (
 p_cdc_region IN VARCHAR2,
 p_cdc_num IN VARCHAR2,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM Tax1099IntPDFTbl 
 WHERE NamAddrID IN ( SELECT NamAddrID 
 FROM NMADCrosrefTbl 
 WHERE LTRIM(RTRIM(CDCRegnCd)) = LTRIM(RTRIM(p_cdc_region))
 AND LTRIM(RTRIM(CDCNmb)) = LTRIM(RTRIM(p_cdc_num)) )
 
 AND LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 ORDER BY NamAddrID ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


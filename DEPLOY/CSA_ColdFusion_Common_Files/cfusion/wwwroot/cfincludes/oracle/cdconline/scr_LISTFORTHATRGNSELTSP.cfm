<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE ListForThatRgnSelTSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT CDCRegnCd ,
 CDCNmb 
 FROM PortflTbl 
 WHERE SUBSTR(CDCRegnCd, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY CDCRegnCd,
 CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


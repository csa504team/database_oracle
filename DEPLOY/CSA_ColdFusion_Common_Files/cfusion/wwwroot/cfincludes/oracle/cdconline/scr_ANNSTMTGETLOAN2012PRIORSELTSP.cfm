<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AnnStmtGetLoan2012PriorSelTSP 
 (
 p_cdc_region IN VARCHAR2,
 p_cdc_num IN VARCHAR2,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT (LoanNmb) 
 FROM Tax1098PrintTbl 
 WHERE LTRIM(RTRIM(CDCRegnCd)) = LTRIM(RTRIM(p_cdc_region))
 AND LTRIM(RTRIM(CDCNmb)) = LTRIM(RTRIM(p_cdc_num))
 AND LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 ORDER BY LoanNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


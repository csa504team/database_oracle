<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE Get1099IntSBAOnlySelCSP
 
 (
 p_TaxYr IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT *
 FROM ( SELECT NamAddrID
 FROM NMADCrosrefTbl
 WHERE NamAddrID IN ( SELECT NamAddrID
 FROM Tax1099IntPDFTbl
 WHERE LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_TaxYr)) )
 
 AND CDCNmb IS NULL
 AND CDCRegnCd IS NULL
 ORDER BY CDCRegnCd ASC,
 CDCNmb ASC )
 WHERE ROWNUM <= 1 ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatPortflTotREFISelTSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(NotePrinAmt) loan_amount_tt ,
 SUM(AmortIntAmt) interest_due_tt ,
 SUM(AmortPrinAmt) principal_due_tt ,
 SUM(AmortPymtAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM LoanTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND RefiLoan = 'Y' ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(NotePrinAmt) loan_amount_tt ,
 SUM(AmortIntAmt) interest_due_tt ,
 SUM(AmortPrinAmt) principal_due_tt ,
 SUM(AmortPymtAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM LoanTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND RefiLoan = 'Y'
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2) ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(NotePrinAmt) loan_amount_tt ,
 SUM(AmortIntAmt) interest_due_tt ,
 SUM(AmortPrinAmt) principal_due_tt ,
 SUM(AmortPymtAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM LoanTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND RefiLoan = 'Y' ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


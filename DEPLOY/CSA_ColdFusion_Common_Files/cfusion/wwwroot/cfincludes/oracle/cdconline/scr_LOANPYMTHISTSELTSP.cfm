<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LoanPymtHistSelTSP 
 --DROP PROCEDURE [dbo].[CDC_LoanPaymtHistory_R]
 
 (
 p_GPNum IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT LoanNmb ,
 PymtTyp ,
 PostingDt ,
 CASE 
 WHEN PymtTyp = 'X' THEN NULL
 ELSE SBAFeeAmt + CSAFeeAmt + CDCFeeAmt + IntAmt + PrinAmt + UnallocAmt + LateFeeAmt + DueFromBorrAmt
 END total_pymt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 IntAmt ,
 PrinAmt ,
 LateFeeAmt ,
 UnallocAmt ,
 PrepayAmt ,
 BalAmt ,
 DueFromBorrAmt 
 FROM PymtHistryTbl 
 WHERE LoanNmb = p_GPNum
 ORDER BY PostingDt DESC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


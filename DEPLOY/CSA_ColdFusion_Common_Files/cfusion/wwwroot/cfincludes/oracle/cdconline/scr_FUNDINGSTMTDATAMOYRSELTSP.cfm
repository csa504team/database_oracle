<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE FundingStmtDataMoYrSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_region_num IS NULL
 AND p_cdc_num IS NULL THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT EXTRACT(MONTH FROM FundingDt) funding_month ,
 EXTRACT(YEAR FROM FundingDt) funding_year 
 FROM FundingStmtTbl 
 ORDER BY EXTRACT(YEAR FROM FundingDt) DESC,
 EXTRACT(MONTH FROM FundingDt) DESC ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT EXTRACT(MONTH FROM FundingDt) funding_month ,
 EXTRACT(YEAR FROM FundingDt) funding_year ,
 ACHInd ACHInd 
 FROM FundingStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 ORDER BY EXTRACT(YEAR FROM FundingDt) DESC,
 EXTRACT(MONTH FROM FundingDt) DESC ;--cdc_funding_statements_month_year_r '02','005'
 --select * from CDCFundingStmtTbl 
 --WHERE CDCRegnCd = '02'
 --AND CDCNmb = '005'
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


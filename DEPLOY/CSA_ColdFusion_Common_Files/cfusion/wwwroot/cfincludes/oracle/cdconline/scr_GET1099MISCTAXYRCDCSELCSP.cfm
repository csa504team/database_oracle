<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE Get1099MISCTaxYrCDCSelCSP 
 (
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT NamAddrID ,
 CDCRegnCd ,
 CDCNmb ,
 SBAInd 
 FROM NMADCrosrefTbl 
 WHERE NamAddrID IN ( SELECT NamAddrID 
 FROM Tax1099MiscPDFTbl 
 WHERE LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year)) )
 
 AND SUBSTR(CDCNmb, -1, 1) <> 's'
 ORDER BY CDCRegnCd ASC,
 CDCNmb ASC ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


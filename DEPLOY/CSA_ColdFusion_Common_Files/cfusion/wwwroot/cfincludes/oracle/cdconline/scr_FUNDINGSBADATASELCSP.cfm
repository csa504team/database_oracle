<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE FundingSBADataSelCSP 
 -- cdc_funding_sba_data_r 5, 2006, 'F'
 
 (
 p_funding_month IN NUMBER DEFAULT NULL ,
 p_funding_year IN NUMBER DEFAULT NULL ,
 p_output_type IN CHAR DEFAULT 'S' ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_output_type = 'S' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 LoanNmb LoanNmb ,
 FundingDt FundingDt 
 FROM FundingStmtTbl 
 WHERE EXTRACT(YEAR FROM FundingDt) = p_funding_year
 AND EXTRACT(MONTH FROM FundingDt) = p_funding_month
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 ELSE
 
 BEGIN
 DELETE FROM TTFileTbl;
 INSERT INTO TTFileTbl
 ( TTLine )
 ( SELECT 'CDC REGION' || ',' || 'CDC NUMBER' || ',' || 'LOAN NUMBER' || ',' || 'FUNDING DATE' 
 FROM DUAL );
 INSERT INTO TTFileTbl
 ( CDCRegnCd, CDCNmb, LoanNmb, TTLine )
 SELECT CDCRegnCd ,
 CDCNmb ,
 LoanNmb ,
 NVL(CDCRegnCd, ' ') || ',' || NVL(CDCNmb, ' ') || ',' || NVL(LoanNmb, ' ') || ',' || NVL(TO_CHAR(FundingDt, 'MM/DD/YYYY'), ' ') 
 FROM FundingStmtTbl 
 WHERE EXTRACT(YEAR FROM FundingDt) = p_funding_year
 AND EXTRACT(MONTH FROM FundingDt) = p_funding_month
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb;
 OPEN p_SelCur1 FOR
 SELECT TTLine 
 FROM TTFileTbl 
 ORDER BY CDCRegnCd,
 CDCNmb,
 LoanNmb ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


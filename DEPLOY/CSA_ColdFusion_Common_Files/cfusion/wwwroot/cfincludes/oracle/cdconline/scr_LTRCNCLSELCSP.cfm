<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LtrCnclSelCSP 
 (
 p_sba_loan_no IN CHAR,
 p_prepay_date IN CHAR,
 p_req_code IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 --declare @buz_day smalldatetime
 --declare @pp_date smalldatetime
 --select @pp_date=convert(smalldatetime,@PrepayDt)
 --exec UTL_prior_biz_date @pp_date, -8, @buz_day output
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'y' 
 FROM LoanSaveTbl 
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT ad.CDCNm name ,
 ad.CDCMailAdrStr1Nm BorrMailAdrStr1Nm ,
 ad.CDCMailAdrCtyNm || ', ' || ad.CDCMailAdrStCd || ' ' || ad.CDCMailAdrZipCd city ,
 ad.CntctNm ,
 NVL(CASE LTRIM(ln.SmllBusCons)
 WHEN ' ' THEN ln.BorrNm
 ELSE ln.SmllBusCons
 END, ln.BorrNm) SmllBusCons ,
 rq.LoanNmb ,
 rq.PrepayDt ,
 rq.FaxNmb ,
 rq.CDCPortflStatCdCd 
 FROM RqstTbl rq
 JOIN LoanSaveTbl ln ON rq.RqstREF = ln.RqstREF
 AND rq.LoanNmb = p_sba_loan_no
 AND TRUNC(rq.PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND rq.RqstCd = p_req_code
 JOIN AdrTbl ad ON ln.CDCNmb = ad.CDCNmb
 AND ln.CDCRegnCd = ad.CDCRegnCd
 WHERE rq.CDCPortflStatCdCd = 'CN'
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(ln.IssDt) < 0
 AND rq.GFDRecv = 'N'
 OR rq.GFDRecv IS NULL ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT ad.CDCNm name ,
 ad.CDCMailAdrStr1Nm BorrMailAdrStr1Nm ,
 ad.CDCMailAdrCtyNm || ', ' || ad.CDCMailAdrStCd || ' ' || ad.CDCMailAdrZipCd city ,
 ad.CntctNm ,
 CASE ln.SmllBusCons
 WHEN ' ' THEN ln.BorrNm
 WHEN NULL THEN ln.BorrNm
 ELSE ln.SmllBusCons
 END SmllBusCons ,
 rq.LoanNmb ,
 rq.PrepayDt ,
 rq.FaxNmb ,
 rq.CDCPortflStatCdCd 
 FROM RqstTbl rq
 JOIN LoanTbl ln ON rq.LoanNmb = ln.LoanNmb
 AND rq.LoanNmb = p_sba_loan_no
 AND TRUNC(rq.PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND rq.RqstCd = p_req_code
 JOIN AdrTbl ad ON ln.CDCNmb = ad.CDCNmb
 AND ln.CDCRegnCd = ad.CDCRegnCd
 WHERE rq.CDCPortflStatCdCd = 'CN'
 AND TO_DATE('01/01/1992', 'MM/DD/YYYY') - TRUNC(ln.IssDt) < 0
 AND rq.GFDRecv = 'N'
 OR rq.GFDRecv IS NULL ;---and getdate()> @buz_day
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


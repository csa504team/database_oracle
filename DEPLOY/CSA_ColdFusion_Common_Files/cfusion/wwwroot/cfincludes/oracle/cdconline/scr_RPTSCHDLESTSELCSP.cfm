<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTSchdlEstSelCSP
 (
 p_CDCNmb IN CHAR,
 p_CDCRegn IN CHAR,
 p_SortOrd IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 ---order by Sba Loan Number
 IF TRIM(p_SortOrd) = 'LoanNmb' THEN
 
 BEGIN
 ----Colson users
 IF TRIM(p_CDCNmb) = '0' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND CDCPortflStatCdCd = 'OP'
 AND r.RqstCd = 9
 
 ORDER BY r.LoanNmb ASC ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur1 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND RTRIM(LTRIM(l.CDCNmb)) = RTRIM(LTRIM(p_CDCNmb))
 AND RTRIM(LTRIM(l.CDCRegnCd)) = RTRIM(LTRIM(p_CDCRegn))
 AND CDCPortflStatCdCd = 'OP'
 AND r.RqstCd = 9
 
 ORDER BY r.LoanNmb ASC ;
 END IF;
 
 END;
 
 ---order by Prepay Date
 ELSE
 ----Colson users
 IF TRIM(p_CDCNmb) = '0' THEN
 
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND CDCPortflStatCdCd = 'OP'
 AND r.RqstCd = 9
 
 ORDER BY r.PrepayDt DESC,
 r.LoanNmb ASC ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur2 FOR
 SELECT DISTINCT l.PrgrmNmb ,
 r.LoanNmb ,
 l.BorrNm ,
 r.PrepayDt ,
 ' ' wire_instr ,
 r.SemiAnnDt ,
 r.GFDRecv ,
 CDCPortflStatCdCd
 FROM RqstTbl r,
 LoanTbl l
 WHERE r.LoanNmb = l.LoanNmb
 AND RTRIM(LTRIM(l.CDCNmb)) = RTRIM(LTRIM(p_CDCNmb))
 AND RTRIM(LTRIM(l.CDCRegnCd)) = RTRIM(LTRIM(p_CDCRegn))
 AND CDCPortflStatCdCd = 'OP'
 AND r.RqstCd = 9
 
 ORDER BY r.PrepayDt DESC,
 r.LoanNmb ASC ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


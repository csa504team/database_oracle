<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE ChkExistAnnStmtSelTSP 
 --exec cdc_checkexistence_annualstatement 7299544010,2013
 --exec cdc_checkexistence_annualstatement 7299544010,2012
 
 (
 p_sba_loan_no IN VARCHAR2,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_val_record VARCHAR2(10);
 v_loanfound VARCHAR2(10);
 
 BEGIN
 
 v_val_record := ' ' ;
 IF p_tax_year > 2012 THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb 
 FROM AnnStmtRecipntTbl 
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_loanfound := 'yes' ;
 
 END;
 ELSE
 
 BEGIN
 v_loanfound := 'no' ;
 
 END;
 END IF;
 
 END;
 ELSE
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb 
 FROM Tax1098PrintTbl 
 WHERE LoanNmb = p_sba_loan_no );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_loanfound := 'yes' ;
 
 END;
 ELSE
 
 BEGIN
 v_loanfound := 'no' ;
 
 END;
 END IF;
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT v_loanfound checkifexists 
 FROM DUAL ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


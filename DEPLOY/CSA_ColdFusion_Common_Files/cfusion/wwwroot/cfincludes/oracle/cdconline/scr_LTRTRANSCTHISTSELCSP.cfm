<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE LtrTransctHistSelCSP
 (
 --- cdc_ltr_trnsct_hist '7179674009','05/19/05',9,7
 p_SBALoanNmb IN VARCHAR2,
 p_PrepayDt IN DATE,
 p_RqstCd IN NUMBER,
 p_LevelID IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR,
 p_SelCur4 OUT SYS_REFCURSOR,
 p_SelCur5 OUT SYS_REFCURSOR,
 p_SelCur6 OUT SYS_REFCURSOR
 )
 AS
 v_prepay_date DATE := p_PrepayDt;
 v_biz_date DATE;
 v_start_date DATE;
 v_ltr_available CHAR(1);
 v_prpt_date DATE;
 v_rqst_date DATE;
 v_req_ref NUMBER(10,0);
 
 BEGIN
 
 v_prpt_date := v_prepay_date ;
 IF p_LevelID IN ( 7,9 )
 THEN
 
 /*** Colson staff: letter available after 3rd business of prepay_month ***/
 BEGIN
 v_start_date := TRUNC(v_prpt_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 3, v_biz_date) ;
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 --- if today is 3rd business day or later
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 v_start_date := TRUNC(v_prepay_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 6, v_biz_date) ;-- get the 6th business day of the pp month
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 -- if today is later than the 6th business day of the pp month
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 -- CDC: Letter should be available at the earliest on the 6th business day of the prepayment month
 
 END;
 END IF;
 /*** Estimated type request - letter available after 3rd business day ***/
 BEGIN
 SELECT MAX(RqstDt)
 INTO v_rqst_date
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(v_prepay_date)
 AND RqstCd = p_RqstCd and ROWNUM < 2;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_rqst_date := NULL;
 END;
 IF p_RqstCd = 9 THEN
 
 BEGIN
 v_start_date := TRUNC(v_rqst_date, 'MONTH');
 v_start_date := v_start_date - INTERVAL '1' DAY;
 UTLDefineNthBusDtSelTSP(v_start_date, 3, v_biz_date);
 IF TRUNC(SYSDATE) - TRUNC(v_biz_date) >= 0 --- if today is 3rd business day or later
 THEN
 v_ltr_available := 'Y' ;
 ELSE
 v_ltr_available := 'N' ;
 END IF;
 
 END;
 END IF;
 IF v_ltr_available = 'Y' THEN
 DECLARE
 v_rate NUMBER(8,5);
 v_amort_sba_fee NUMBER(19,4);
 v_amort_cdc_fee NUMBER(19,4);
 v_amort_csa_fee NUMBER(19,4);
 v_cdc_pct NUMBER(8,5);
 v_sba_pct NUMBER(8,5);
 v_piamt NUMBER(19,4);
 v_date DATE;
 v_bal NUMBER(19,4);
 v_curbal NUMBER(19,4);
 v_num_of_month NUMBER(10,0);
 v_next_sa_date DATE;
 v_inum NUMBER(10,0);
 v_posting_date DATE;
 v_last_posting_date DATE;
 v_xadj_posting_date DATE;
 v_issue_date DATE;
 v_req_ref NUMBER(10,0);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 BEGIN
 SELECT MAX(RqstREF)
 INTO v_req_ref
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(v_prepay_date)
 AND RqstCd = p_RqstCd AND ROWNUM < 2;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_req_ref := NULL;
 END;
 /* 1. Check if GFD is received ,if not letter can not be displayed. */
 --if exists (select * from CDCRqstTbl where RqstREF = @req_ref and GFDRecv = 'Y' and status <> 'CN')
 --or exists (select * from CDCRqstTbl r,CDCLoanTbl l where r.RqstREF = @req_ref and l.LoanNmb = r.LoanNmb and l.LoanNmb = @LoanNmb and convert(smalldatetime,l.IssDt) < '01/01/1992' and status <> 'CN')
 --or exists (select * from CDCRqstTbl r,CDCLoanSaveTbl ls where r.RqstREF = @req_ref and r.RqstREF = ls.RqstREF and convert(smalldatetime,ls.IssDt) < '01/01/1992' and status <> 'CN')
 /*** request with type 1,6,7,9 always available ***/
 --or exists (select * from CDCRqstTbl where RqstREF = @req_ref and RqstCd in(1,6,7,9) and status <> 'CN')
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT *
 FROM RqstTbl
 WHERE RqstREF = v_req_ref
 AND CDCPortflStatCdCd <> 'CN' );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 
 IF v_temp = 1 THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 v_x_adj_prin NUMBER(19,4);
 /*** get the number of month from PrepayDt to the next semi_annual date ***/
 /* Rule:
 The next semi-annual date is always a multiple of 6 from the Issue date
 The next semi-annual date is always greater than the Prepayment date
 */
 v_MnDiff -- Number of months between the Issue Date and Prepayment date
 NUMBER(10,0);
 v_MntoSADt NUMBER(10,0);-- The number of months needed to make @MnDiff a multiple of 6 (SemiAnnual)
 /************************************************/
 /*As per John's instruction 2/28/2003*/
 /*Get GFD*/
 v_gfd_received NUMBER(19,4);
 /*save prepayment Amt after ream*/
 v_next_prin_bal NUMBER(19,4);
 v_net_amt NUMBER(19,4);
 /********* new code (7/19/04) to set the release letter availability ***********/
 v_prepay_amount NUMBER(19,4);
 /********* new code end ****************/
 /*if maturity date field is not a date select empty string*/
 v_maturity_date CHAR(10);
 v_loan_term CHAR(2);
 
 BEGIN
 /* 2. Save record to the history files(CDCLoanSaveTbl,CDCPymtSaveTbl) if it's not there. */
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE NOT EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 BEGIN
 SELECT MAX(PostingDt)
 
 INTO v_posting_date
 FROM PymtHistryTbl
 WHERE LoanNmb = p_SBALoanNmb
 AND ( LTRIM(RTRIM(PymtTyp)) NOT IN ( 'X','AR','CR',' ' )
 
 AND PymtTyp IS NOT NULL );
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_posting_date := NULL;
 END;
 
 HistSaveInsCSP(v_req_ref,
 p_SBALoanNmb,
 v_posting_date,
 v_prepay_date) ;
 
 END;
 END IF;
 ---exec cdc_hist_debenture_m @req_ref ,@LoanNmb ,@PrepayDt
 /*for non-current loans get x-adj payment */
 XADJ504SelCSP(p_SBALoanNmb,
 v_req_ref,
 v_posting_date,
 v_last_posting_date) ;
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_x_adj_prin
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PymtTyp = 'X';
 v_x_adj_prin := NVL(v_x_adj_prin, 0) ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_x_adj_prin := NULL;
 END;
 
 /* 3. Run Ream procedure. */
 BEGIN
 SELECT NoteRtPct
 INTO v_rate
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_rate := NULL;
 END;
 
 BEGIN
 SELECT (AmortIntAmt + AmortPrinAmt)
 INTO v_piamt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_piamt := NULL;
 END;
 
 BEGIN
 SELECT CurBalNeededAmt
 INTO v_curbal
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_curbal := NULL;
 END;
 
 IF v_curbal > 0 THEN
 
 ----and @req_code <> 9
 BEGIN
 BEGIN
 SELECT NVL(p.BalAmt, 0) - NVL(l.PrinNeededAmt, 0) - NVL(v_x_adj_prin, 0)
 INTO v_bal
 FROM PymtSaveTbl p,
 LoanSaveTbl l
 WHERE p.RqstREF = v_req_ref
 AND p.RqstREF = l.RqstREF
 AND p.PostingDt = v_posting_date;---@last_posting_date
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 END;
 ELSE
 
 BEGIN
 BEGIN
 SELECT BalAmt
 INTO v_bal
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PostingDt = v_posting_date;---@last_posting_date
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 END;
 END IF;
 
 BEGIN
 SELECT AmortSBAAmt
 INTO v_amort_sba_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_sba_fee := NULL;
 END;
 
 BEGIN
 SELECT AmortCDCAmt
 INTO v_amort_cdc_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_cdc_fee := NULL;
 END;
 
 BEGIN
 SELECT AmortCSAAmt
 INTO v_amort_csa_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_amort_csa_fee := NULL;
 END;
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 BEGIN
 SELECT IssDt
 INTO v_issue_date
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_issue_date := NULL;
 END;
 
 BEGIN
 SELECT CDCPct
 INTO v_cdc_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_pct := NULL;
 END;
 
 BEGIN
 SELECT SBAPct
 INTO v_sba_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba_pct := NULL;
 END;
 
 /* the sum of @MnDiff and @MnSADt will always be a multiple of 6 */
 v_MntoSADt := 0 ;
 v_MnDiff := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(v_issue_date)));
 IF MOD(v_MnDiff, 6) <> 0 THEN
 v_MntoSADt := v_MnDiff + 6 - (MOD(v_MnDiff, 6)) ;-- Adjust the number of months to the next sa date
 ELSE
 v_MntoSADt := v_MnDiff + 6 ;
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 END IF;
 v_next_sa_date := TRUNC(ADD_MONTHS(v_issue_date, v_MntoSADt));
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ) = 'Y';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 BEGIN
 SELECT SUM(WireAmt)
 INTO v_gfd_received
 FROM WiresTbl
 WHERE LTRIM(RTRIM(LoanNmb)) = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(v_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_RqstCd;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_gfd_received := NULL;
 END;
 END;
 ELSE
 
 BEGIN
 v_gfd_received := 0.00 ;
 
 END;
 END IF;
 /*If request type is 1,6,7,9 use 0.00 Amt for GFD */
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT RqstCd
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ) IN ( 1,6,7,9 );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_temp := NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_gfd_received := 0.00 ;
 
 END;
 END IF;
 /*If there is a payment received on the month of a prepayment date start projection fron the next month.*/
 /*if datediff(mm,@PrepayDt,@PostingDt)=0
 begin
 set @PrepayDt = convert(varchar(10),dateadd(mm,1,@PrepayDt),101)
 set @inum = @inum - 1
 end */
 /**********************************************************/
 /* as per John's instruction 2/28/2003 - Always use next month from last payment */
 IF v_curbal > 0
 AND p_RqstCd <> 9 THEN
 
 --non-current loans
 BEGIN
 v_prepay_date := TRUNC(ADD_MONTHS(SYSDATE, 1));
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(ADD_MONTHS(v_prepay_date, 1)))) + 1;
 
 END;
 ELSE
 
 BEGIN
 /* 05/18/04 for current loans start projection from the next month off the Prepay Date month */
 v_prepay_date := TRUNC(ADD_MONTHS(v_prepay_date, 1));
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_prepay_date)));
 
 END;
 END IF;
 DELETE FROM TTWSCDCInstl2Tbl;
 /**********************************************************/
 /*temp table to hold values after ream procedure */
 IF p_RqstCd <> 9 THEN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 BEGIN
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 
 END;
 END IF;
 BEGIN
 SELECT MIN(EndBalAmt)
 INTO v_next_prin_bal
 FROM TTWSCDCInstl2Tbl ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_next_prin_bal := NULL;
 END;
 
 BEGIN
 SELECT ( SELECT SUM(DueAmt)
 FROM TTWSCDCInstl2Tbl ) - ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl )
 
 INTO v_net_amt
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_net_amt := NULL;
 END;
 
 UPDATE LoanSaveTbl l
 SET l.WKPrepayAmt = (NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(l.PrepayPremAmt, 0) - NVL(v_gfd_received, 0) + NVL(l.PrinNeededAmt, 0) + NVL(l.IntNeededAmt, 0) + NVL(l.CSAFeeNeededAmt, 0) + NVL(l.CDCFeeNeededAmt, 0) + NVL(l.SBAFeeNeededAmt, 0) + NVL(l.LateFeeNeededAmt, 0))
 WHERE l.RqstREF = v_req_ref;
 UPDATE RqstTbl
 SET CDCPortflStatCdCd = 'CL',
 SYSUserID = 'systems',
 SYSProgID = 'sp_trnscr_hist_ltr',
 SYSLastTime = SYSDATE
 WHERE RqstREF = v_req_ref;
 DELETE FROM TTTempAvail2Tbl;
 
 BEGIN
 SELECT PrepayAmt
 INTO v_prepay_amount
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PymtTyp = 'X';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prepay_amount := NULL;
 END;
 /* list of loans which release letters are available */
 INSERT INTO TTTempAvail2Tbl (RqstRef,CreatUserID,LastUpdtUserID)
 ( SELECT rq.RqstREF , USER, USER
 FROM RqstTbl rq,
 LoanSaveTbl ls,
 LoanTbl ln
 WHERE rq.RqstREF = ls.RqstREF
 AND rq.RqstREF = v_req_ref
 AND ls.LoanNmb = ln.LoanNmb
 AND ln.NoteBalAmt = 0
 AND rq.CDCPortflStatCdCd = 'CL'
 AND ls.WKPrepayAmt IS NOT NULL
 AND v_prepay_amount IS NOT NULL
 AND ls.WKPrepayAmt <= v_prepay_amount + 6 );
 --and @PrepayAmt between ls.WKPrepayAmt and ls.WKPrepayAmt + 6
 /* insert current date only if not already there */
 INSERT INTO RlsAvailTbl (RqstREF,AvailDT,CreatUserID,LastUpdtUserID)
 ( SELECT ta.RqstREF ,
 SYSDATE ,
 USER ,
 USER
 FROM TTTempAvail2Tbl ta
 LEFT JOIN RlsAvailTbl av ON ta.RqstREF = av.RqstREF
 WHERE av.RqstREF IS NULL );
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 BEGIN
 SELECT MATDT
 INTO v_maturity_date
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_maturity_date := NULL;
 END;
 IF v_maturity_date IS NOT NULL THEN
 
 BEGIN
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT TRUNC(MONTHS_BETWEEN(LAST_DAY(MATDT), LAST_DAY(IssDt))) / 12
 INTO v_loan_term
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 END;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_loan_term := NULL;
 END;
 
 ELSE
 
 BEGIN
 v_maturity_date := '' ;
 v_loan_term := '' ;
 
 END;
 END IF;
 /* 4. Get the record. */
 OPEN p_SelCur1 FOR
 SELECT v_req_ref req_ref ,
 l.BorrNm ,
 NVL(CASE LTRIM(l.SmllBusCons)
 WHEN ' ' THEN l.BorrNm
 ELSE l.SmllBusCons
 END, BorrNm) SmllBusCons ,
 l.NoteBalAmt debenture ,
 NVL(LTRIM(RTRIM(l.CDCNm)), ' ') CDCNm ,
 l.CDCRegnCd || '-' || l.CDCNmb CDCNmb ,
 l.NoteRtPct ,
 l.LoanNmb ,
 v_loan_term loan_term ,
 l.IssDt ,
 v_posting_date date_last_pmt ,
 v_next_sa_date sa_date ,
 v_maturity_date MATDT ,
 r.PrepayDt ,
 l.SemiAnnPymtAmt SemiAnnPymtAmt ,
 ' ' pmt_num ,
 p.PrinAmt ,
 p.IntAmt ,
 p.SBAFeeAmt ,
 p.CDCFeeAmt ,
 p.CSAFeeAmt ,
 p.LateFeeAmt ,
 p.DueFromBorrAmt ,
 NVL(p.PrinAmt, 0) + NVL(p.IntAmt, 0) + NVL(p.SBAFeeAmt, 0) + NVL(p.CDCFeeAmt, 0) + NVL(p.CSAFeeAmt, 0) + NVL(p.DueFromBorrAmt, 0) + NVL(p.LateFeeAmt, 0) + NVL(p.UnallocAmt, 0) pmt_rcvd ,
 l.PrinNeededAmt ,
 l.IntNeededAmt ,
 l.CSAFeeNeededAmt ,
 l.CDCFeeNeededAmt ,
 l.SBAFeeNeededAmt ,
 l.LateFeeNeededAmt ,
 p.UnallocAmt exc_amt ,
 NVL(l.PrinNeededAmt, 0) + NVL(l.IntNeededAmt, 0) + NVL(l.CSAFeeNeededAmt, 0) + NVL(l.CDCFeeNeededAmt, 0) + NVL(l.SBAFeeNeededAmt, 0) + NVL(l.LateFeeNeededAmt, 0) tot_amt ,
 v_bal bal ,
 ---tot_to_sa_date = @inum,
 ( SELECT MIN(EndBalAmt)
 FROM TTWSCDCInstl2Tbl ) tot_to_sa_date ,
 ( SELECT SUM(PrinAmt)
 FROM TTWSCDCInstl2Tbl ) total_prin ,
 ( SELECT SUM(IntAmt)
 FROM TTWSCDCInstl2Tbl ) total_int ,
 ( SELECT SUM(SBAFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_sba_fee ,
 ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_cdc_fee ,
 ( SELECT SUM(CSAFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_sca_fee ,
 l.PrepayRtPct * 100 PrepayRtPct ,
 l.PrepayPremAmt PrepayPremAmt ,
 DbentrBalAmt BalPrinSA
 FROM RqstTbl r
 LEFT JOIN LoanSaveTbl l ON r.RqstREF = l.RqstREF
 LEFT JOIN PymtSaveTbl p ON r.RqstREF = p.RqstREF
 AND p.PostingDt = v_posting_date
 WHERE r.RqstREF = v_req_ref ;
 /*** Estimate type requests ***/
 IF p_RqstCd = 9 AND v_curbal = 0 THEN
 DECLARE
 v_EstimProjectStartDate DATE;
 v_holdPrepayDate VARCHAR2(10);
 /*** get payments and totals for month up to estimated payoff date ***/
 v_prin NUMBER(19,4);
 v_int NUMBER(19,4);
 v_sba NUMBER(19,4);
 v_cdc NUMBER(19,4);
 v_sca NUMBER(19,4);
 v_late_fee NUMBER(19,4);
 v_est_prin NUMBER(19,4);
 v_prin_amount NUMBER(19,4);
 v_int_amount NUMBER(19,4);
 v_sba_fee NUMBER(19,4);
 v_cdc_fee NUMBER(19,4);
 v_csa_fee NUMBER(19,4);
 v_l_fee NUMBER(19,4);
 /*** display months of estimaited payments up to prepay date ***/
 v_pmt_month DATE;
 
 BEGIN
 v_EstimProjectStartDate := TRUNC(ADD_MONTHS(v_posting_date, 1));
 v_holdPrepayDate := v_prepay_date ;
 v_prepay_date := v_EstimProjectStartDate ;
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_EstimProjectStartDate)));----- 1
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 v_prepay_date := v_holdPrepayDate ;
 
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_prin
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;---@EstimProjectStartDate
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prin := NULL;
 END;
 
 BEGIN
 SELECT SUM(IntAmt)
 INTO v_int
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_int := NULL;
 END;
 
 BEGIN
 SELECT SUM(SBAFeeAmt)
 INTO v_sba
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba := NULL;
 END;
 
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_cdc
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc := NULL;
 END;
 
 BEGIN
 SELECT SUM(CSAFeeAmt)
 INTO v_sca
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sca := NULL;
 END;
 
 BEGIN
 SELECT LateFeeNeededAmt
 INTO v_late_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_late_fee := NULL;
 END;
 
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_est_prin
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) <= 0;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_prin := NULL;
 END;
 
 OPEN p_SelCur2 FOR
 SELECT TTWSCDCInstl2Tbl.* ,
 NVL(v_prin, 0) total_prin ,
 NVL(v_int, 0) total_int ,
 NVL(v_sba, 0) total_sba_fee ,
 NVL(v_cdc, 0) total_cdc_fee ,
 NVL(v_sca, 0) total_sca_fee
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(DueDt), LAST_DAY(v_prpt_date))) >= 1 ;
 DELETE FROM TTTempPDPymtTbl;
 /*** get payments and totals for month after estimated payoff date ***/
 INSERT INTO TTTempPDPymtTbl (LoanNmb,LateFeeAmt,SchedBalAmt,CreatUserID,LastUpdtUserID)
 ( SELECT p_SBALoanNmb ,
 NVL(v_late_fee, 0) ,
 NVL(v_bal, 0) - NVL(v_est_prin, 0),
 USER ,
 USER
 FROM DUAL );
 
 BEGIN
 SELECT NVL(( SELECT SUM(PrinAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_prin_amount
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prin_amount := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT SUM(IntAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT IntNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_int_amount
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_int_amount := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT SUM(SBAFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT SBAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_sba_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba_fee := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT CDCFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_cdc_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_fee := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT SUM(CSAFeeAmt)
 FROM TTWSCDCInstl2Tbl
 WHERE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(DueDt))) >= 0 ), 0) + NVL(( SELECT CSAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_csa_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_csa_fee := NULL;
 END;
 
 BEGIN
 SELECT LateFeeNeededAmt
 INTO v_l_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_l_fee := NULL;
 END;
 
 OPEN p_SelCur3 FOR
 SELECT LoanNmb ,
 v_prin_amount PrinAmt ,
 v_int_amount int_amount ,
 v_sba_fee SBAFeeAmt ,
 v_cdc_fee CDCFeeAmt ,
 v_csa_fee CSAFeeAmt ,
 v_l_fee
 LateFeeAmt ,----t.LateFee ,---@LateFee,
 
 v_prin_amount + v_int_amount + v_sba_fee + v_cdc_fee + v_csa_fee + v_l_fee
 total_amt ,---t.LateFee,
 
 t.SchedBalAmt ---@bal - isnull(sum(PrinAmt),0)
 sched_bal
 FROM TTTempPDPymtTbl t ;
 DELETE FROM TTESTIMMoTbl;
 /*populate estimated payments*/
 v_pmt_month := TRUNC(ADD_MONTHS(v_posting_date, 1));
 WHILE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(v_pmt_month))) > 0
 LOOP
 
 BEGIN
 INSERT INTO TTESTIMMoTbl (DueDt,CreatUserID,LastUpdtUserID)
 ( SELECT v_pmt_month , USER, USER
 FROM DUAL );
 v_pmt_month := TRUNC(ADD_MONTHS(v_pmt_month, 1));
 
 END;
 END LOOP;
 OPEN p_SelCur4 FOR
 SELECT *
 FROM TTESTIMMoTbl ;
 
 END;
 END IF;
 /*Estimate transcript for non current loans*/
 IF p_RqstCd = 9 AND v_curbal > 0 THEN
 DECLARE
 ---1.ream pmts prior to the prepayment date - estimated payments
 v_diff NUMBER(19,4);
 v_start_proj_date DATE;
 v_request_date DATE;
 v_loan_status CHAR(8);
 --RS2 ream
 v_est_pr NUMBER(19,4);
 v_est_int NUMBER(19,4);
 v_est_sba NUMBER(19,4);
 v_est_cdc NUMBER(19,4);
 v_est_csa NUMBER(19,4);
 v_lt_fee NUMBER(19,4);
 v_late_fee NUMBER(19,4);
 v_sba_fee_needed NUMBER(19,4);
 v_cdc_fee_needed NUMBER(19,4);
 v_csa_fee_needed NUMBER(19,4);
 v_int_needed NUMBER(19,4);
 v_prin_needed NUMBER(19,4);
 ---RS4 pastt due pmts dates
 v_due_date DATE;
 
 BEGIN
 DELETE FROM TTWSCDCEstInstl3Tbl;
 
 BEGIN
 SELECT LTRIM(RTRIM(LoanStatCd))
 INTO v_loan_status
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_loan_status := NULL;
 END;
 
 BEGIN
 SELECT RqstDt
 INTO v_request_date
 FROM RqstTbl
 WHERE RqstREF = v_req_ref;
 v_start_proj_date := TRUNC(ADD_MONTHS(v_request_date, 1));
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prpt_date), LAST_DAY(v_request_date))) - 1 ;--- datediff(mm,@PostingDt,@prpt_date)-1
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_request_date := NULL;
 END;
 
 BEGIN
 SELECT BalAmt
 INTO v_bal
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PostingDt = v_posting_date;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 
 ReamEstLoan(v_inum,
 v_rate,
 v_piamt,
 v_start_proj_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date,
 v_req_ref,
 v_posting_date,
 v_request_date) ;
 ---2.ream pmts after prepay date - to sa date
 BEGIN
 SELECT LateFeeNeededAmt
 INTO v_late_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_late_fee := NULL;
 END;
 
 --- get Bal for projection to SA Date
 BEGIN
 SELECT ( SELECT BalAmt
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PostingDt = v_posting_date ) - NVL(( SELECT SUM(PrinAmt)
 FROM TTWSCDCEstInstl3Tbl ), 0 ---where datediff(mm,@RqstDt,DueDt)> 0),0)
 ) - ( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref )
 
 INTO v_bal
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_bal := NULL;
 END;
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), LAST_DAY(v_prpt_date))) - 1 ;
 ReamLoan(p_SBALoanNmb,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 BEGIN
 SELECT SUM(PrinAmt)
 INTO v_est_pr
 FROM TTWSCDCEstInstl3Tbl ;--- where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_pr := NULL;
 END;
 
 BEGIN
 SELECT SUM(AccrIntAmt)
 INTO v_est_int
 FROM TTWSCDCEstInstl3Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_int := NULL;
 END;
 
 BEGIN
 SELECT SUM(SBAFeeAmt)
 INTO v_est_sba
 FROM TTWSCDCEstInstl3Tbl ;--- where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_sba := NULL;
 END;
 
 BEGIN
 SELECT SUM(CDCFeeAmt)
 INTO v_est_cdc
 FROM TTWSCDCEstInstl3Tbl ;--- where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_cdc := NULL;
 END;
 
 BEGIN
 SELECT SUM(CSAFeeAmt)
 INTO v_est_csa
 FROM TTWSCDCEstInstl3Tbl ;---where datediff(mm,@RqstDt,DueDt)>0)
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_est_csa := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT LateFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_lt_fee
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_lt_fee := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT SBAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_sba_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_sba_fee_needed := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT CDCFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_cdc_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_fee_needed := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT CSAFeeNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_csa_fee_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_csa_fee_needed := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT IntNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_int_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_int_needed := NULL;
 END;
 
 BEGIN
 SELECT NVL(( SELECT PrinNeededAmt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref ), 0)
 
 INTO v_prin_needed
 FROM DUAL ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prin_needed := NULL;
 END;
 
 --RS3 pmts to SA date
 OPEN p_SelCur2 FOR
 SELECT TTWSCDCInstl2Tbl.* ,
 ( SELECT SUM(PrinAmt)
 FROM TTWSCDCInstl2Tbl ) total_prin ,
 ( SELECT SUM(IntAmt)
 FROM TTWSCDCInstl2Tbl ) total_int ,
 ( SELECT SUM(SBAFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_sba_fee ,
 ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_cdc_fee ,
 ( SELECT SUM(CSAFeeAmt)
 FROM TTWSCDCInstl2Tbl ) total_sca_fee
 FROM TTWSCDCInstl2Tbl ;
 OPEN p_SelCur3 FOR
 SELECT NVL(v_est_pr, 0) + NVL(v_prin_needed, 0) PrinAmt ,
 NVL(v_est_int, 0) + NVL(v_int_needed, 0) int_amount ,
 NVL(v_est_sba, 0) + NVL(v_sba_fee_needed, 0) SBAFeeAmt ,
 NVL(v_est_cdc, 0) + NVL(v_cdc_fee_needed, 0) CDCFeeAmt ,
 NVL(v_est_csa, 0) + NVL(v_csa_fee_needed, 0) CSAFeeAmt ,
 v_lt_fee LateFeeAmt ,
 NVL(v_est_pr, 0) + NVL(v_prin_needed, 0) + NVL(v_est_int, 0) + NVL(v_int_needed, 0) + NVL(v_est_sba, 0) + NVL(v_sba_fee_needed, 0) + NVL(v_est_cdc, 0) + NVL(v_cdc_fee_needed, 0) + NVL(v_est_csa, 0) + NVL(v_csa_fee_needed, 0) + NVL(v_lt_fee, 0) total_amt ,
 NVL(v_lt_fee, 0) ,
 NVL(v_bal, 0) sched_bal
 FROM DUAL ;
 v_due_date := TRUNC(ADD_MONTHS(v_posting_date, 1)) ;
 DELETE FROM TTDueDtSTbl;
 WHILE TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(v_due_date))) <> 0
 LOOP
 
 BEGIN
 INSERT INTO TTDueDtSTbl (DueDt,CreatUserID,LastUpdtUserID)
 ( SELECT v_due_date , USER, USER
 FROM DUAL );
 v_due_date := ADD_MONTHS(v_due_date, 1);
 
 END;
 END LOOP;
 OPEN p_SelCur4 FOR
 SELECT DueDt
 FROM TTDueDtSTbl ;--- TTWSCDCEstInstl3Tbl
 
 END;
 ELSE
 
 /*** for all other types display projected payments in one table ***/
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT *
 FROM TTWSCDCInstl2Tbl ;
 
 END;
 END IF;
 
 END;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur5 FOR
 SELECT *
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ; ---and GFDRecv = 'Y'
 
 END;
 END IF;
 IF SQL%ROWCOUNT = 0 THEN
 OPEN p_SelCur6 FOR
 SELECT 'recordcount'
 FROM DUAL
 WHERE 1 = 2 ;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


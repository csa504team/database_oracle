<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE PayoutFloatStmtDataSelTSP
 -- cdc_payout_float_statements_data_r '01','009 ','10/19/2005'
 
 (
 p_RegnNmb IN CHAR DEFAULT NULL ,
 p_CDCNmb IN CHAR DEFAULT NULL ,
 p_StmtDt IN DATE DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 StmtDt StmtDt ,
 CASE
 WHEN EXTRACT(MONTH FROM StmtDt) < 7 THEN 'October 1, ' || TO_CHAR(EXTRACT(YEAR FROM StmtDt) - 1)
 WHEN EXTRACT(MONTH FROM StmtDt) > 7 THEN 'April 1, ' || TO_CHAR(EXTRACT(YEAR FROM StmtDt)) END date_from ,
 CASE
 WHEN EXTRACT(MONTH FROM StmtDt) < 7 THEN 'March 31, ' || TO_CHAR(EXTRACT(YEAR FROM StmtDt))
 WHEN EXTRACT(MONTH FROM StmtDt) > 7 THEN 'September 30, ' || TO_CHAR(EXTRACT(YEAR FROM StmtDt)) END date_to ,
 PrgrmNmb PrgrmNmb ,
 ACHInd ACHInd ,
 StmtNm StmtNm ,
 CDCAccrAmt CDCAccrAmt ,
 PrepayAccrAmt PrepayAccrAmt ,
 DueToCDCAmt DueToCDCAmt ,
 ChkACHAmt ChkACHAmt ,
 CASE
 WHEN ChkACHAmt < 0 THEN 'The negative amount wil be carried forward for the next interest accrual calculation.'
 WHEN ChkACHAmt = 0
 AND ACHInd = 'N' THEN 'No check issued. '
 WHEN ChkACHAmt = 0
 AND ACHInd = 'Y' THEN 'No ACH credit issued. '
 WHEN ChkACHAmt > 0
 AND ACHInd = 'N' THEN 'A check for the above amount has been issued. '
 WHEN ChkACHAmt > 0
 AND ACHInd = 'Y' THEN 'An ACH credit for the above amount has been issued. ' END message_text
 FROM PayoutFltStmtTbl
 WHERE CDCRegnCd = p_RegnNmb
 AND CDCNmb = p_CDCNmb
 AND StmtDt = p_StmtDt ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTSchdlPODollrSelCSP
 (
 p_sort_order IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 ---order by Sba Loan Number
 IF LTRIM(RTRIM(p_sort_order)) = 'LoanNmb' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT rq.LoanNmb ,
 ls.CDCRegnCd ,
 ls.CDCNmb ,
 NVL(LTRIM(RTRIM(ls.SmllBusCons)), ls.BorrNm) BorrNm ,
 rq.PrepayDt ,
 ls.WKPrepayAmt
 FROM RqstTbl rq,
 LoanSaveTbl ls
 WHERE rq.RqstREF = ls.RqstREF
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.PrepayDt))) = 0 -- the month of the prepay date must be in the current month
 
 AND rq.CDCPortflStatCdCd = 'CL' -- suggests that the user already generated the transcript and/or prepayment work-up
 
 
 -- and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
 AND rq.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY rq.LoanNmb ASC ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT rq.LoanNmb ,
 ls.CDCRegnCd ,
 ls.CDCNmb ,
 NVL(LTRIM(RTRIM(ls.SmllBusCons)), ls.BorrNm) BorrNm ,
 rq.PrepayDt ,
 ls.WKPrepayAmt
 FROM RqstTbl rq,
 LoanSaveTbl ls
 WHERE rq.RqstREF = ls.RqstREF
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.PrepayDt))) = 0 -- the month of the prepay date must be in the current month
 
 AND rq.CDCPortflStatCdCd = 'CL' -- suggests that the user already generated the transcript and/or prepayment work-up
 
 
 ---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
 AND rq.RqstCd NOT IN ( 1,6,7,9 )
 
 ORDER BY ls.WKPrepayAmt ASC ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


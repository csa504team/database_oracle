<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LateStmtDataSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_statement_date IN DATE DEFAULT NULL ,
 p_sort_order IN VARCHAR2 DEFAULT 'StmtNm' ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 /* -- 2013-10-11 SBA Code Review Remediation
 -- Edited by Tom Newton
 -- Removed commented out statements and formatted indentation
 -- */
 -- The @sort_order variable is not passed to the stored proc, so dynamic SQL is unnecessary
 -- Hardcoding ORDER BY to be default input value of 'StmtNm'
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 StmtDt StmtDt ,
 StmtNm StmtNm ,
 LoanNmb LoanNmb ,
 DueAmt DueAmt ,
 LateFeeAmt LateFeeAmt 
 FROM LateStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND StmtDt = p_statement_date
 ORDER BY StmtNm ;
 --- calculate the statment total due Amt
 OPEN p_SelCur2 FOR
 SELECT SUM(DueAmt) total_amount_due 
 FROM LateStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND StmtDt = p_statement_date ;
 --- calculate the statment total fee due
 OPEN p_SelCur3 FOR
 SELECT SUM(LateFeeAmt) TotFeeDueAmt 
 FROM LateStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND StmtDt = p_statement_date ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


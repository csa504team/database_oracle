<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTCnclSelCSP 
 (
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_sort_order IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR,
 p_SelCur3 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 /* ------get records-------*/
 /*sort by Sba Loan Number*/
 IF LTRIM(RTRIM(p_sort_order)) = 'LoanNmb' THEN
 
 BEGIN
 ---Colson
 IF p_cdc_num = '0' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY r.LoanNmb,
 r.PrepayDt ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur1 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND RTRIM(LTRIM(p.CDCRegnCd)) = RTRIM(LTRIM(p_cdc_region))
 AND r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY r.LoanNmb,
 r.PrepayDt ;
 END IF;
 
 END;
 /*sort by Prepayment Date*/
 ELSE
 IF LTRIM(RTRIM(p_sort_order)) = 'PrepayDt' THEN
 
 BEGIN
 ---Colson
 IF p_cdc_num = '0' THEN
 
 BEGIN
 OPEN p_SelCur2 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY r.PrepayDt,
 r.LoanNmb ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur2 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND RTRIM(LTRIM(p.CDCRegnCd)) = RTRIM(LTRIM(p_cdc_region))
 AND r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY r.PrepayDt,
 r.LoanNmb ;
 END IF;
 
 END;
 /*sort by CDC Number*/
 ELSE
 
 BEGIN
 ---Colson
 IF p_cdc_num = '0' THEN
 
 BEGIN
 OPEN p_SelCur3 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY p.CDCRegnCd,
 p.CDCNmb,
 r.LoanNmb,
 r.PrepayDt ;
 
 END;
 
 ---Clients
 ELSE
 OPEN p_SelCur3 FOR
 SELECT r.LoanNmb ,
 r.CDCPortflStatCdCd ,
 p.BorrNm ,
 LTRIM(RTRIM(p.CDCRegnCd)) || '-' || LTRIM(RTRIM(p.CDCNmb)) CDCNmb ,
 NVL(LTRIM(RTRIM(p.CDCNm)), ' ') CDCNm ,
 r.PrepayDt 
 FROM RqstTbl r
 LEFT JOIN PortflTbl p ON r.LoanNmb = p.LoanNmb
 WHERE RTRIM(LTRIM(p.CDCNmb)) = RTRIM(LTRIM(p_cdc_num))
 AND RTRIM(LTRIM(p.CDCRegnCd)) = RTRIM(LTRIM(p_cdc_region))
 AND r.CDCPortflStatCdCd = 'CN'
 AND p.IssDt IS NOT NULL
 ORDER BY p.CDCRegnCd,
 p.CDCNmb,
 r.LoanNmb,
 r.PrepayDt ;
 END IF;
 
 END;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


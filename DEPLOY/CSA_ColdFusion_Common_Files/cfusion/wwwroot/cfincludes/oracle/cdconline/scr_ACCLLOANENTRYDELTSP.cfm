<!--- Saved 09/21/2018 18:20:34. --->
PROCEDURE AcclLoanEntryDelTSP 
 (
 p_sba_loan_no IN CHAR
 )
 AS
 
 BEGIN
 
 DELETE AcclLoanEntryTbl
 
 WHERE LoanNmb = p_sba_loan_no;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


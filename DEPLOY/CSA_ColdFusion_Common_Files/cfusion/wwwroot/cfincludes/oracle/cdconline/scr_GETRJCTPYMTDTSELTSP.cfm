<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetRjctPymtDtSelTSP
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_reject_date DATE;
 
 BEGIN
 
 ----cdc_get_rejected_payments_date_r
 ----cdc_get_rejected_payments_date_r '01','311'
 ----cdc_get_rejected_payments_date_r '01','311'
 v_reject_date := TRUNC(ADD_MONTHS(SYSDATE, -1), 'MONTH');
 IF (TRIM(p_region_num) IS NULL OR TRIM(p_region_num) = '')
 AND (TRIM(p_cdc_num) IS NULL OR TRIM(p_cdc_num) = '') THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT EXTRACT(MONTH FROM RjctDt) rejected_month ,
 EXTRACT(YEAR FROM RjctDt) rejected_year
 FROM PymtRjctHistryTbl
 WHERE TRUNC(RjctDt) >= TRUNC(v_reject_date)
 ORDER BY EXTRACT(YEAR FROM RjctDt) DESC,
 EXTRACT(MONTH FROM RjctDt) DESC ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT EXTRACT(MONTH FROM RjctDt) rejected_month ,
 EXTRACT(YEAR FROM RjctDt) rejected_year
 FROM PymtRjctHistryTbl
 WHERE TRUNC(RjctDt) >= TRUNC(v_reject_date)
 AND CDCRegnCd = TRIM(p_region_num)
 AND TRIM(CDCNmb) = TRIM(p_cdc_num)
 ORDER BY EXTRACT(YEAR FROM RjctDt) DESC,
 EXTRACT(MONTH FROM RjctDt) DESC ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


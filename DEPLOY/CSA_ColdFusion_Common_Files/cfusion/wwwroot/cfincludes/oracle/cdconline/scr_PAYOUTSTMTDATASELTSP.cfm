<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE PayoutStmtDataSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_statement_date IN DATE DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 StmtDt StmtDt ,
 LoanNmb LoanNmb ,
 PrgrmNmb PrgrmNmb ,
 BorrNm BorrNm ,
 DbentrAmt DbentrAmt ,
 CDCFeeAmt CDCFeeAmt ,
 MultiCDCFeeInd MultiCDCFeeInd ,
 WithheldAmt WithheldAmt ,
 ACHInd ACHInd ,
 ChkACHAmt ChkACHAmt 
 FROM PayoutStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND StmtDt = p_statement_date
 ORDER BY BorrNm ;
 --- calculate the statment check/ach Amt
 OPEN p_SelCur2 FOR
 SELECT SUM(ChkACHAmt) total_check_ach_amount 
 FROM PayoutStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND StmtDt = p_statement_date ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


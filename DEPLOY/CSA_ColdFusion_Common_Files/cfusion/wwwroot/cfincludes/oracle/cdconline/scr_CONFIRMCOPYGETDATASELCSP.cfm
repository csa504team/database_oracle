<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE ConfirmCopyGetDataSelCSP
 (
 p_sba_loan_no IN CLOB DEFAULT NULL ,
 p_1 OUT SYS_REFCURSOR
 )
 AS
 /* -- 2013-10-14 SBA Code Review Remediation
 -- Edited by Tom Newton
 -- Replaced dynamic SQL statement with code that parses the input list into a table variable
 -- Also removed commented out statements and formatted indentation
 
 -- SQL statements for testing
 DROP TABLE TTTempCnfrmTbl
 DROP TABLE TTCDCLoan2Tbl
 DROP TABLE TTMoneyTempTbl
 DECLARE @LoanNmb varchar(6000)
 SET @LoanNmb = '3438176001,4281595008,4099845004,3292866003,3006746002,4844685005,3239236003,2760986001,9795434010,3655455002'
 -- */
 -- Begin reusable list-parsing code
 v_listString CLOB;
 v_delim CHAR(1);
 
 BEGIN
 
 -- Declare a single-column table variable to store the split string
 v_delim := ',' ;
 v_listString := p_sba_loan_no ;
 -- Create a recursive CTE (common table expression) to split string and insert into table variable
 
 INSERT INTO TTVTBLSplitStrng2Tbl
 ( StrngVal, CreatUserID, LastUpdtUserID )
 (SELECT TRIM(REGEXP_SUBSTR(v_listString,'[^,]+', 1, LEVEL)) AS Item, User, USER FROM DUAL
 CONNECT BY LEVEL <= REGEXP_COUNT(v_listString, ',') +1)
 ;
 DELETE FROM TTTempCnfrmTbl;
 DELETE FROM TTCDCLoan2Tbl;
 -- SELECT * FROM @tblSplitString
 -- End reusable list-parsing code
 -- Join from split string table to CDCCnfrmCopyTbl table to make sure LoanNmb values are valid
 INSERT INTO TTCDCLoan2Tbl
 ( LoanNmb, CreatUserID, LastUpdtUserID )
 ( SELECT LoanNmb, USER, USER
 FROM CnfrmCopyTbl cc
 JOIN TTVTBLSplitStrng2Tbl tmpSS ON cc.LoanNmb = tmpSS.StrngVal );
 DELETE FROM TTMoneyTempTbl;
 --select the Amt/amounts to create the Amt text
 INSERT INTO TTMoneyTempTbl
 ( TempAmt, AmtTxt, CreatUserID, LastUpdtUserID )
 ( SELECT cc.DbentrAmt amt ,
 NULL AmtTxt , USER , USER 
 FROM CnfrmCopyTbl cc
 JOIN TTCDCLoan2Tbl cl ON cc.LoanNmb = cl.LoanNmb );
 --UTLMoneyToTxtDelCSP();
 --- confirm info 
 INSERT INTO TTTempCnfrmTbl (CDCRegnCd,CDCNmb,CDCNm,FundingDt,LoanNmb,FirstPymtDt,SemiAnnDt,DbentrAmt,DbentrAmtTxt,DbentrIntRtPct,AppvDt,DbentrMatDt,NoteAmt,NoteIntRtPct,NoteIssDt,NoteMatDt,BorrNm,SBCncrnNm,
 PrinIntAmt,MoPymtAmt,SemiAnnAmt,UndrwtrFeeAmt,UndrwtrIntRtPct,UndrwtrIntRtTxt,CSAAmt,DbentrProcdAmt,ClsAmt,GntyFeeAmt,FundingFeeAmt,PrcsFeeAmt,CDCClsAmt,BorrAmt,CSADisbAmt,
 RecipntBnkNm,RecipntMailAdrStr1Nm,RecipntMailAdrCtyNm,RecipntMailAdrStCd,RecipntMailAdrZipCd,RecipntMailAdrZip4Cd,RecipntAcct,RecipntAcctNmb,RecipntRoutingNmb,RecipntTransCd,RecipntAttentionNm,CrspndBnkNm,
 CrspndMailAdrStr1Nm,CrspndMailAdrCtyNm,CrspndMailAdrStCd,CrspndMailAdrZipCd,CrspndMailAdrZip4Cd,CrspndAcct,CrspndAcctNmb,CrspndRoutingNmb,CrspndTransCd,CrspndAttentionNm, CreatUserID, LastUpdtUserID)
 ( SELECT cc.CDCRegnCd CDCRegnCd ,
 cc.CDCNmb CDCNmb ,
 NULL CDCNm ,
 cc.FundingDt FundingDt ,
 cc.LoanNmb LoanNmb , 
 ADD_MONTHS(cc.FundingDt, 1) - (TO_NUMBER(EXTRACT(DAY FROM ADD_MONTHS(cc.FundingDt, 1))) - 1) FirstPymtDt ,
 ADD_MONTHS(cc.FundingDt, 6) - (TO_NUMBER(EXTRACT(DAY FROM ADD_MONTHS(cc.FundingDt, 6))) - 1) SemiAnnDt ,
 cc.DbentrAmt DbentrAmt ,
 NULL DbentrAmtTxt ,
 cc.DbentrIntRtPct DbentrIntRtPct ,
 cc.AppvDt AppvDt ,
 cc.DbentrMatDt DbentrMatDt ,
 cc.NoteAmt NoteAmt ,
 cc.NoteIntRtPct NoteIntRtPct ,
 cc.NoteIssDt NoteIssDt ,
 cc.NoteMatDt NoteMatDt ,
 cc.BorrNm BorrNm ,
 cc.SBCncrnNm SBCncrnNm ,
 cc.PrinIntAmt PrinIntAmt ,
 cc.MoPymtAmt MoPymtAmt ,
 cc.SemiAnnAmt SemiAnnAmt ,
 cc.UndrwtrFeeAmt UndrwtrFeeAmt ,
 cc.UndrwtrIntRtPct UndrwtrIntRtPct ,
 NULL UndrwtrIntRtTxt ,
 cc.CSAAmt CSAAmt ,
 cc.DbentrProcdAmt DbentrProcdAmt ,
 cc.ClsAmt ClsAmt ,
 cc.GntyFeeAmt GntyFeeAmt ,
 cc.FundingFeeAmt FundingFeeAmt ,
 cc.PrcsFeeAmt PrcsFeeAmt ,
 cc.CDCClsAmt CDCClsAmt ,
 cc.BorrAmt BorrAmt ,
 cc.CSADisbAmt CSADisbAmt ,
 cc.RecipntBnkNm RecipntBnkNm ,
 cc.RecipntMailAdrStr1Nm RecipntMailAdrStr1Nm ,
 cc.RecipntMailAdrCtyNm RecipntMailAdrCtyNm ,
 cc.RecipntMailAdrStCd RecipntMailAdrStCd ,
 cc.RecipntMailAdrZipCd RecipntMailAdrZipCd ,
 cc.RecipntMailAdrZip4Cd RecipntMailAdrZip4Cd ,
 cc.RecipntAcct RecipntAcct ,
 cc.RecipntAcctNmb RecipntAcctNmb ,
 cc.RecipntRoutingNmb RecipntRoutingNmb ,
 cc.RecipntTransCd RecipntTransCd ,
 cc.RecipntAttentionNm RecipntAttentionNm ,
 NULL CrspndBnkNm ,
 NULL CrspndMailAdrStr1Nm ,
 NULL CrspndMailAdrCtyNm ,
 NULL CrspndMailAdrStCd ,
 NULL CrspndMailAdrZipCd ,
 NULL CrspndMailAdrZip4Cd ,
 NULL CrspndAcct ,
 NULL CrspndAcctNmb ,
 NULL CrspndRoutingNmb ,
 NULL CrspndTransCd ,
 NULL CrspndAttentionNm ,
 USER ,
 USER
 FROM CnfrmCopyTbl cc
 JOIN TTCDCLoan2Tbl cl ON cc.LoanNmb = cl.LoanNmb );
 -- get cdc name 
 MERGE INTO TTTempCnfrmTbl tc
 USING (SELECT tc.ROWID row_id, ca.CDCNm
 FROM TTTempCnfrmTbl tc
 JOIN AdrTbl ca ON tc.CDCRegnCd = ca.CDCRegnCd
 AND tc.CDCNmb = ca.CDCNmb ) src
 ON ( tc.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCNm = src.CDCNm;
 ---get cdc correspondent bank info
 MERGE INTO TTTempCnfrmTbl tc
 USING (SELECT tc.ROWID row_id, cb.CrspndBnkNm, cb.CrspndMailAdrStr1Nm, cb.CrspndMailAdrCtyNm, cb.CrspndMailAdrStCd, cb.CrspndMailAdrZipCd, cb.CrspndMailAdrZip4Cd, cb.CrspndAcct, cb.CrspndAcctNmb, cb.CrspndRoutingNmb, cb.CrspndTransCd, cb.CrspndAttentionNm
 FROM TTTempCnfrmTbl tc
 JOIN CrspndBnkTbl cb ON tc.LoanNmb = cb.LoanNmb ) src
 ON ( tc.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CrspndBnkNm = src.CrspndBnkNm,
 CrspndMailAdrStr1Nm = src.CrspndMailAdrStr1Nm,
 CrspndMailAdrCtyNm = src.CrspndMailAdrCtyNm,
 CrspndMailAdrStCd = src.CrspndMailAdrStCd,
 CrspndMailAdrZipCd = src.CrspndMailAdrZipCd,
 CrspndMailAdrZip4Cd = src.CrspndMailAdrZip4Cd,
 CrspndAcct = src.CrspndAcct,
 CrspndAcctNmb = src.CrspndAcctNmb,
 CrspndRoutingNmb = src.CrspndRoutingNmb,
 CrspndTransCd = src.CrspndTransCd,
 CrspndAttentionNm = src.CrspndAttentionNm;
 -- update Amt text
 MERGE INTO TTTempCnfrmTbl tc
 USING (SELECT tc.ROWID row_id, mt.AmtTxt
 FROM TTTempCnfrmTbl tc
 JOIN TTMoneyTempTbl mt ON tc.DbentrAmt = mt.TempAmt ) src
 ON ( tc.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DbentrAmtTxt = src.AmtTxt;
 MERGE INTO TTTempCnfrmTbl tc
 USING (SELECT tc.ROWID row_id, r.CDCRtDesc
 FROM TTTempCnfrmTbl tc
 JOIN RtTbl r ON tc.UndrwtrIntRtPct = r.CDCRtPct ) src
 ON ( tc.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET UndrwtrIntRtTxt
 -- UndrwtrIntRt = UndrwtrIntRt / 100
 = src.CDCRtDesc;
 OPEN p_1 FOR
 SELECT CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 FundingDt ,
 LoanNmb ,
 FirstPymtDt ,
 SemiAnnDt ,
 DbentrAmt ,
 SPELL_NUMBER(DbentrAmt) DbentrAmtTxt,
 DbentrIntRtPct ,
 AppvDt ,
 DbentrMatDt ,
 NoteAmt ,
 NoteIntRtPct ,
 NoteIssDt ,
 NoteMatDt ,
 BorrNm ,
 SBCncrnNm ,
 PrinIntAmt ,
 MoPymtAmt ,
 SemiAnnAmt ,
 UndrwtrFeeAmt ,
 UndrwtrIntRtPct ,
 UndrwtrIntRtTxt ,
 CSAAmt ,
 DbentrProcdAmt ,
 ClsAmt ,
 GntyFeeAmt ,
 FundingFeeAmt ,
 PrcsFeeAmt ,
 CDCClsAmt ,
 BorrAmt ,
 CSADisbAmt ,
 RecipntBnkNm ,
 RecipntMailAdrStr1Nm ,
 RecipntMailAdrCtyNm ,
 RecipntMailAdrStCd ,
 RecipntMailAdrZipCd ,
 RecipntMailAdrZip4Cd ,
 RecipntAcct ,
 RecipntAcctNmb ,
 RecipntRoutingNmb ,
 RecipntTransCd ,
 RecipntAttentionNm ,
 CrspndBnkNm ,
 CrspndMailAdrStr1Nm ,
 CrspndMailAdrCtyNm ,
 CrspndMailAdrStCd ,
 CrspndMailAdrZipCd ,
 CrspndMailAdrZip4Cd ,
 CrspndAcct ,
 CrspndAcctNmb ,
 CrspndRoutingNmb ,
 CrspndTransCd ,
 CrspndAttentionNm 
 FROM TTTempCnfrmTbl 
 ORDER BY LoanNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


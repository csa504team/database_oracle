<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatPortflDelCSP
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_SBA NUMBER DEFAULT 0 ;
 BEGIN
 
 DELETE FROM TTTempPortfl2Tbl;
 ----select * from CDCLoanTbl where RefiLoan = 'Y'
 ---CDC_Status_Portfolio_R NULL,'09','024'
 ---CDC_Status_Portfolio_R '0100','01','246'
 IF v_SBA = 1 THEN
 INSERT INTO TTTempPortfl2Tbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,BorrNm,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,StatCd,RefiLoan,CreatUserID,LastUpdtUserID,LASTUPDTDT)
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd CDCPortflStatCdCd ,
 NULL RefiLoan ,
 USER ,
 USER,
 LASTUPDTDT
 FROM PortflTbl
 
 --WHERE UserLevelRoleID = @SOD
 WHERE CDCRegnCd = TRIM(p_RegionNum)
 AND CDCNmb = TRIM(p_CdcNum)
 AND ( TRIM(CDCPortflStatCdCd) IS NULL OR TRIM(CDCPortflStatCdCd) = ''
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY StmtNm;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 INSERT INTO TTTempPortfl2Tbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,BorrNm,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,StatCd,RefiLoan,CreatUserID,LastUpdtUserID,LASTUPDTDT)
 SELECT UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd StatCd ,
 NULL RefiLoan ,
 USER ,
 USER ,
 LASTUPDTDT
 FROM PortflTbl
 
 --WHERE UserLevelRoleID = @SOD
 WHERE CDCRegnCd = TRIM(p_RegionNum)
 AND CDCNmb = TRIM(p_CdcNum)
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 AND ( CDCPortflStatCdCd IS NULL
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY StmtNm;
 ELSE
 INSERT INTO TTTempPortfl2Tbl (UserLevelRoleID,DistNm,PrgrmNmb,CDCRegnCd,CDCNmb,CDCNm,LoanNmb,BorrNm,StmtNm,IssDt,MatDt,LastPymtDtx,LoanAmt,MoPymtAmt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,DiffToEscrowAmt,TotFeeDueAmt,IntDueAmt,PrinDueAmt,LateFeeDueAmt,TotDueAmt,PrinLeftAmt,StatCd,RefiLoan,CreatUserID,LastUpdtUserID,LASTUPDTDT)
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 StmtNm StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 CDCPortflStatCdCd StatCd ,
 NULL RefiLoan ,
 USER ,
 USER ,
 LASTUPDTDT
 FROM PortflTbl
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND ( CDCPortflStatCdCd IS NULL
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY StmtNm;
 END IF;
 END IF;
 MERGE INTO TTTempPortfl2Tbl p
 USING (SELECT p.ROWID row_id, ln.RefiLoan
 FROM TTTempPortfl2Tbl p
 JOIN LoanTbl ln ON p.LoanNmb = ln.LoanNmb ) src
 ON ( p.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 PrgrmNmb PrgrmNmb ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 LoanNmb LoanNmb ,
 BorrNm BorrNm ,
 UPPER(StmtNm) StmtNm ,
 IssDt IssDt ,
 MatDt MatDt ,
 LastPymtDtx LastPymtDtx ,
 LoanAmt LoanAmt ,
 MoPymtAmt MoPymtAmt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 TotFeeDueAmt TotFeeDueAmt ,
 IntDueAmt IntDueAmt ,
 PrinDueAmt PrinDueAmt ,
 LateFeeDueAmt LateFeeDueAmt ,
 TotDueAmt TotDueAmt ,
 PrinLeftAmt PrinLeftAmt ,
 StatCd StatCd ,
 RefiLoan RefiLoan ,
 LASTUPDTDT LASTUPDTDT
 FROM TTTempPortfl2Tbl 
 ORDER BY UPPER(StmtNm);
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


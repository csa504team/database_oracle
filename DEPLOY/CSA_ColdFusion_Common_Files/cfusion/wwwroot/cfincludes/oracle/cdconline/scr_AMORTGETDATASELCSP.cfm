<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AmortGetDataSelCSP
 (
 p_sba_loan_no IN CLOB DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 /* -- 2013-10-11 SBA Code Review Remediation
 -- Edited by Tom Newton
 -- Replaced dynamic SQL statement with code that parses the input list into a table variable
 -- Also removed commented out statements and formatted indentation
 
 -- SQL statements for testing
 DROP TABLE TTTempAmortTbl
 DROP TABLE TTCDCLoansTbl
 DROP TABLE CDCTTCDCLoanTotTbl
 DECLARE @LoanNmb varchar(6000)
 --SET @LoanNmb = '8646454007,8763764005,3470565004,2912086002,2609486001,3559415001,4121465005,3756405008,4001645006,3136226008'
 SET @LoanNmb = '8646454007,8763764005,3470565004,2912086002'
 -- */
 -- Begin reusable list-parsing code
 v_listString CLOB;
 v_delim CHAR(1);
 
 BEGIN
 
 -- Declare a single-column table variable to store the split string
 v_delim := ',' ;
 v_listString := p_sba_loan_no ;
 -- Create a recursive CTE (common table expression) to split string and insert into table variable
 
 INSERT INTO TTVTBLSplitStrngTbl
 ( StrngVal,CreatUserID,LastUpdtUserID )
 ( SELECT TRIM(REGEXP_SUBSTR(v_listString,'[^,]+', 1, LEVEL)) AS Item, USER, USER FROM DUAL
 CONNECT BY LEVEL <= REGEXP_COUNT(v_listString, ',') +1)
 ;
 DELETE FROM TTTempAmortTbl;
 DELETE FROM TTCDCLoansTbl;
 -- SELECT * FROM @tblSplitString
 -- End reusable list-parsing code
 -- Join from split string table to CDCAmortHdrTbl table to make sure LoanNmb values are valid
 INSERT INTO TTCDCLoansTbl
 ( LoanNmb,CreatUserID,LastUpdtUserID )
 ( SELECT cad.LoanNmb , USER , USER
 FROM AmortHdrTbl cad
 JOIN TTVTBLSplitStrngTbl tmpSS ON cad.LoanNmb = tmpSS.StrngVal );
 --- header info 
 INSERT INTO TTTempAmortTbl (RecrdTyp,CDCRegnCd,CDCNmb,LoanNmb,NoteAmt,IssDt,BorrNm,SBCncrnNm,SBAOfcNmb,IntRtPct,EscrowAmt,LoanTrm,Pymt0105Amt,Pymt0610Amt,Pymt1115Amt,Pymt1620Amt,Pymt2125Amt,TotIntAmt,TotPrinAmt,PymtMo,PymtYrNmb,PymtNmb,IntAmt,PrinAmt,BalAmt,SBAFeeAmt,CDCFeeAmt,CSAFeeAmt,PymtAmt,CreatUserID,LastUpdtUserID)
 ( SELECT 'A' RecrdTyp ,
 cad.CDCRegnCd CDCRegnCd ,
 cad.CDCNmb CDCNmb ,
 cad.LoanNmb LoanNmb ,
 cad.NoteAmt NoteAmt ,
 cad.IssDt IssDt ,
 cad.BorrNm BorrNm ,
 cad.SBCncrnNm SBCncrnNm ,
 cad.SBAOfcNmb SBAOfcNmb ,
 cad.IntRtPct IntRtPct ,
 cad.EscrowAmt EscrowAmt ,
 cad.LoanTrm LoanTrm ,
 cad.Pymt0105Amt Pymt0105Amt ,
 cad.Pymt0610Amt Pymt0610Amt ,
 cad.Pymt1115Amt Pymt1115Amt ,
 cad.Pymt1620Amt Pymt1620Amt ,
 cad.Pymt2125Amt Pymt2125Amt ,
 NULL total_interest ,
 NULL total_principal ,
 NULL PymtMo ,
 NULL PymtYr ,
 NULL PymtNmb ,
 NULL Int ,
 NULL Prin ,
 NULL Bal ,
 NULL SBAFee ,
 NULL CDCFeeAmt ,
 NULL CSAFee ,
 NULL payment_amount ,
 USER ,
 USER
 FROM AmortHdrTbl cad
 JOIN TTCDCLoansTbl cl ON cad.LoanNmb = cl.LoanNmb );
 --detail info
 INSERT INTO TTTempAmortTbl (RecrdTyp,CDCRegnCd,CDCNmb,LoanNmb,NoteAmt,IssDt,BorrNm,SBCncrnNm,SBAOfcNmb,IntRtPct,EscrowAmt,LoanTrm,Pymt0105Amt,Pymt0610Amt,Pymt1115Amt,Pymt1620Amt,Pymt2125Amt,TotIntAmt,TotPrinAmt,PymtMo,PymtYrNmb,PymtNmb,IntAmt,PrinAmt,BalAmt,SBAFeeAmt,CDCFeeAmt,CSAFeeAmt,PymtAmt,CreatUserID,LastUpdtUserID)
 ( SELECT 'B' RecrdTyp ,
 NULL CDCRegnCd ,
 NULL CDCNmb ,
 cad.LoanNmb LoanNmb ,
 NULL NoteAmt ,
 NULL IssDt ,
 NULL BorrNm ,
 NULL SBCncrnNm ,
 NULL SBAOfcNmb ,
 NULL IntRt ,
 NULL EscrowAmt ,
 NULL terms ,
 NULL Pymt0105 ,
 NULL Pymt0610 ,
 NULL Pymt1115 ,
 NULL Pymt1620 ,
 NULL Pymt2125 ,
 NULL total_interest ,
 NULL total_principal ,
 cad.PymtMo PymtMo ,
 cad.PymtYrNmb PymtYrNmb ,
 cad.PymtNmb PymtNmb ,
 cad.IntAmt IntAmt ,
 cad.PrinAmt PrinAmt ,
 cad.BalAmt BalAmt ,
 cad.SBAFeeAmt SBAFeeAmt ,
 cad.CDCFeeAmt CDCFeeAmt ,
 cad.CSAFeeAmt CSAFeeAmt ,
 cad.PymtAmt PymtAmt ,
 USER ,
 USER
 FROM AmortDtlTbl cad
 JOIN TTCDCLoansTbl cl ON cad.LoanNmb = cl.LoanNmb );
 DELETE FROM TTCDCLoanTotTbl;
 INSERT INTO TTCDCLoanTotTbl (LoanNmb,TotIntAmt,TotPrinAmt,CreatUserID,LastUpdtUserID)
 ( SELECT ta.LoanNmb LoanNmb ,
 SUM(ta.IntAmt) total_interest ,
 SUM(ta.PrinAmt) total_principal ,
 USER ,
 USER
 FROM TTTempAmortTbl ta
 WHERE ta.RecrdTyp = 'B'
 GROUP BY ta.LoanNmb );
 MERGE INTO TTTempAmortTbl ta
 USING (SELECT ta.ROWID row_id, clt.TotIntAmt, clt.TotPrinAmt
 FROM TTTempAmortTbl ta
 JOIN TTCDCLoanTotTbl clt ON ta.LoanNmb = clt.LoanNmb 
 WHERE ta.RecrdTyp = 'A') src
 ON ( ta.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET TotIntAmt = src.TotIntAmt,
 TotPrinAmt = src.TotPrinAmt;
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTTempAmortTbl 
 ORDER BY LoanNmb,
 RecrdTyp,
 PymtYrNmb,
 PymtNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


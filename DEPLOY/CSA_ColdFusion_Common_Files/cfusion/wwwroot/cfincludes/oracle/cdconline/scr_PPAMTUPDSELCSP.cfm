<!--- Saved 09/21/2018 18:20:45. --->
PROCEDURE PPAmtUpdSelCSP
 ---2
 
 (
 p_sba_loan_no IN VARCHAR2,
 p_prepay_date IN DATE,
 p_req_code IN NUMBER
 )
 AS
 v_prepay_date DATE := p_prepay_date;
 v_rate NUMBER(8,5);
 v_x_adj_prin NUMBER(19,4);
 v_amort_sba_fee NUMBER(19,4);
 v_amort_cdc_fee NUMBER(19,4);
 v_amort_csa_fee NUMBER(19,4);
 v_cdc_pct NUMBER(8,5);
 v_sba_pct NUMBER(8,5);
 v_piamt NUMBER(19,4);
 v_bal NUMBER(19,4);
 v_curbal NUMBER(19,4);
 v_next_sa_date DATE;
 v_inum NUMBER(10,0);
 v_posting_date DATE;
 v_issue_date DATE;
 v_req_ref NUMBER(10,0);
 v_gfd_received NUMBER(19,4);
 /* Rule:
 The next semi-annual date is always a multiple of 6 from the Issue date
 The next semi-annual date is always greater than the Prepayment date
 */
 v_MnDiff NUMBER(10,0); -- Number of months between the Issue Date and Prepayment date
 v_MntoSADt NUMBER(10,0);-- The number of months needed to make @MnDiff a multiple of 6 (SemiAnnual)
 v_temp NUMBER(1, 0) := 0;
 -- Get the record.
 -- save prepayment Amt after ream
 v_next_prin_bal NUMBER(19,4);
 v_net_amt NUMBER(19,4);
 
 BEGIN
 
 SELECT MAX(RqstREF)
 
 INTO v_req_ref
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_sba_loan_no))
 AND TRUNC(v_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code AND ROWNUM < 2;
 SELECT MAX(PostingDt)
 
 INTO v_posting_date
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND RTRIM(LTRIM(PymtTyp)) <> 'X';
 -- Run Ream procedure.
 SELECT SUM(PrinAmt)
 
 INTO v_x_adj_prin
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PymtTyp = 'X';
 v_x_adj_prin := NVL(v_x_adj_prin, 0) ;
 SELECT NoteRtPct
 
 INTO v_rate
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT NVL(AmortIntAmt, 0) + NVL(AmortPrinAmt, 0)
 
 INTO v_piamt
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT CurBalNeededAmt
 
 INTO v_curbal
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 IF v_curbal > 0 THEN
 
 BEGIN
 SELECT (NVL(p.BalAmt, 0) - NVL(l.PrinNeededAmt, 0) - NVL(v_x_adj_prin, 0))
 
 INTO v_bal
 FROM PymtSaveTbl p,
 LoanSaveTbl l
 WHERE p.RqstREF = v_req_ref
 AND p.RqstREF = l.RqstREF
 AND p.PostingDt = v_posting_date;
 
 END;
 ELSE
 
 BEGIN
 SELECT BalAmt
 
 INTO v_bal
 FROM PymtSaveTbl
 WHERE RqstREF = v_req_ref
 AND PostingDt = v_posting_date;
 
 END;
 END IF;
 SELECT AmortSBAAmt
 
 INTO v_amort_sba_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT AmortCDCAmt
 
 INTO v_amort_cdc_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT AmortCSAAmt
 
 INTO v_amort_csa_fee
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT IssDt
 
 INTO v_issue_date
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT CDCPct
 
 INTO v_cdc_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 SELECT SBAPct
 
 INTO v_sba_pct
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 -- the sum of @MnDiff and @MnSADt will always be a multiple of 6
 v_MntoSADt := 0 ;
 v_MnDiff := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_prepay_date), LAST_DAY(v_issue_date)));
 IF MOD(v_MnDiff, 6) <> 0 THEN
 v_MntoSADt := v_MnDiff + 6 - (MOD(v_MnDiff, 6)) ;-- Adjust the number of months to the next sa date
 ELSE
 v_MntoSADt := v_MnDiff + 6 ;
 END IF;
 v_next_sa_date := TRUNC(ADD_MONTHS(v_issue_date, v_MntoSADt));
 --- ream and save records to temp table ---
 ---If there is a payment received on the month of a prepayment date start projection fron the next month.
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = v_req_ref ) = 'Y';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 SELECT SUM(WireAmt)
 
 INTO v_gfd_received
 FROM WiresTbl
 WHERE LTRIM(RTRIM(LoanNmb)) = LTRIM(RTRIM(p_sba_loan_no))
 AND TRUNC(v_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code;
 
 END;
 ELSE
 
 BEGIN
 v_gfd_received := 0.00 ;
 
 END;
 END IF;
 --**********************************************************
 -- as per John's instruction 2/28/2003
 IF v_curbal > 0 THEN
 
 --non-current loans
 BEGIN
 v_prepay_date := TRUNC(ADD_MONTHS(SYSDATE, 1));
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), TRUNC(LAST_DAY(ADD_MONTHS(v_prepay_date, 1))))) + 1;
 --Use next month from current date
 
 END;
 ELSE
 
 BEGIN
 v_prepay_date := TRUNC(ADD_MONTHS(v_posting_date, 1));
 v_inum := TRUNC(MONTHS_BETWEEN(LAST_DAY(v_next_sa_date), TRUNC(LAST_DAY(ADD_MONTHS(v_posting_date, 1)))));
 --Always use next month from last payment
 
 END;
 END IF;
 DELETE FROM TTWSCDCInstl4Tbl;
 --**********************************************************
 --temp table to hold values after ream procedure
 ReamLoan(p_sba_loan_no,
 v_inum,
 v_rate,
 v_piamt,
 v_prepay_date,
 v_bal,
 v_amort_sba_fee,
 v_amort_cdc_fee,
 v_amort_csa_fee,
 v_cdc_pct,
 v_sba_pct,
 v_issue_date) ;
 SELECT MIN(EndBalAmt)
 
 INTO v_next_prin_bal
 FROM TTWSCDCInstl4Tbl ;
 SELECT ( SELECT SUM(DueAmt)
 FROM TTWSCDCInstl4Tbl ) - ( SELECT SUM(CDCFeeAmt)
 FROM TTWSCDCInstl4Tbl )
 
 INTO v_net_amt
 FROM DUAL ;
 UPDATE LoanSaveTbl ls ---, DbentrTbl db
 
 SET WKPrepayAmt = (NVL(v_next_prin_bal, 0) + NVL(v_net_amt, 0) + NVL(ls.PrepayPremAmt, 0 ---isnull(db.PremAmt,0)
 ) - NVL(v_gfd_received, 0) + NVL(ls.PrinNeededAmt, 0) + NVL(ls.IntNeededAmt, 0) + NVL(ls.CSAFeeNeededAmt, 0) + NVL(ls.CDCFeeNeededAmt, 0) + NVL(ls.SBAFeeNeededAmt, 0) + NVL(ls.LateFeeNeededAmt, 0))
 WHERE ls.RqstREF = v_req_ref;---and ls.LoanNmb = db.LoanNmb
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


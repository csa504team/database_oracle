<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatPortflEmailSelTSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 PrgrmNmb ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 BorrNm ,
 --BorowrStr,
 --BorowrCty,
 --BorowrSt,
 --BorowrZipCd,
 --StmtNm,
 IssDt ,
 MatDt ,
 LastPymtDtx ,
 LoanAmt ,
 MoPymtAmt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 DiffToEscrowAmt ,
 TotFeeDueAmt ,
 IntDueAmt ,
 PrinDueAmt ,
 LateFeeDueAmt ,
 TotDueAmt ,
 PrinLeftAmt ,
 CDCPortflStatCdCd 
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND ( CDCPortflStatCdCd IS NULL
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY BorrNm ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 PrgrmNmb ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 LoanNmb ,
 BorrNm ,
 --BorowrStr,
 --BorowrCty,
 --BorowrSt,
 --BorowrZipCd,
 --StmtNm,
 IssDt ,
 MatDt ,
 LastPymtDtx ,
 LoanAmt ,
 MoPymtAmt ,
 SBAFeeAmt ,
 CSAFeeAmt ,
 CDCFeeAmt ,
 DiffToEscrowAmt ,
 TotFeeDueAmt ,
 IntDueAmt ,
 PrinDueAmt ,
 LateFeeDueAmt ,
 TotDueAmt ,
 PrinLeftAmt ,
 CDCPortflStatCdCd 
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 AND ( CDCPortflStatCdCd IS NULL
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY BorrNm ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 DistNm ,
 CDCRegnCd ,
 CDCNmb ,
 CDCNm ,
 PrgrmNmb ,
 LoanNmb ,
 --BorrNm,
 --BorowrStr,
 --BorowrCty,
 --BorowrSt,
 --BorowrZipCd,
 StmtNm ,
 IssDt ,
 MatDt ,
 NVL(LastPymtDtx, ' ') LastPymtDtx ,
 NVL(LoanAmt, 0.00) LoanAmt ,
 NVL(MoPymtAmt, 0.00) MoPymtAmt ,
 NVL(SBAFeeAmt, 0.00) SBAFeeAmt ,
 NVL(CSAFeeAmt, 0.00) CSAFeeAmt ,
 NVL(CDCFeeAmt, 0.00) CDCFeeAmt ,
 NVL(DiffToEscrowAmt, 0.00) DiffToEscrowAmt ,
 NVL(TotFeeDueAmt, 0.00) TotFeeDueAmt ,
 NVL(IntDueAmt, 0.00) IntDueAmt ,
 NVL(PrinDueAmt, 0.00) PrinDueAmt ,
 NVL(LateFeeDueAmt, 0.00) LateFeeDueAmt ,
 NVL(TotDueAmt, 0.00) TotDueAmt ,
 NVL(PrinLeftAmt, 0.00) PrinLeftAmt ,
 NVL(CDCPortflStatCdCd, ' ') CDCPortflStatCdCd 
 FROM PortflTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND ( CDCPortflStatCdCd IS NULL
 OR CDCPortflStatCdCd <> 'XX' )
 ORDER BY StmtNm ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


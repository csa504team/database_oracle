<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AdrUpdTSP 
 (
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_e_mail IN VARCHAR2,
 p_sys_progid IN CHAR,
 p_sys_userid IN VARCHAR2
 )
 AS
 
 BEGIN
 
 UPDATE AdrTbl
 SET EmailAdr = p_e_mail,
 SYSProgID = p_sys_progid,
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RqstAudtSelTSP
 (
 p_Identifier IN number := 0,
 p_LOANNMB IN char := NULL,
 p_RetVal OUT number,
 p_ErrVal OUT number,
 p_ErrMsg OUT varchar2,
 p_SelCur OUT SYS_REFCURSOR
 )AS
 -- variable declaration
 BEGIN
 SAVEPOINT RqstAudtSelTSP;
 
 IF p_Identifier = 0 OR p_Identifier IS NULL
 THEN
 /* select from RQSTAUDTTBL Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT
 RQSTREF,RQSTDT,AUDTACTN,RQSTFROM,PHNNMB,FAXNMB,EMAILADR,LOANNMB,RQSTCD,SEMIANNDT,PREPAYDT,GFDRECV,STATCD,STATDT,SYSPROGID,SYSUSERID,SYSLASTTIME,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT
 FROM RQSTAUDTTBL
 WHERE LOANNMB = p_LOANNMB;
 
 p_RetVal := SQL%ROWCOUNT;
 p_ErrVal := SQLCODE;
 p_ErrMsg := SQLERRM;
 END;
 ELSIF p_Identifier = 2
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT
 1
 FROM RQSTAUDTTBL
 WHERE LOANNMB = p_LOANNMB;
 
 p_RetVal := SQL%ROWCOUNT;
 p_ErrVal := SQLCODE;
 p_ErrMsg := SQLERRM;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 p_RetVal := 0;
 p_ErrVal := SQLCODE;
 p_ErrMsg := SQLERRM;
 
 ROLLBACK TO RqstAudtSelTSP;
 RAISE;
 END;
 END RqstAudtSelTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GPNmbValidateSelCSP
 
 /*******************************************************************
 * PROCEDURE: CDC_GP_NUM_VALIDATE
 * PURPOSE: Validates Loan number and checks for existing prepay actual / estimate request
 * NOTES:
 * CREATED:
 * MODIFIED
 * DATEAUTHORDESCRIPTION
 * 07/30/2013Bharani TalluriValidate IF Prepayment estimate request exists and alert the user.
 * 08/14/2013Bharani TalluriValidate Prepay date should be before the Maturity date
 * 02/17/2015 Bharani Talluri Added Dot to the error strings
 
 * Changes related to PRB/Project
 1. SBA Phase 2
 *
 * Details:
 returns @valid
 *-------------------------------------------------------------------
 *******************************************************************/
 (
 p_sba_loan_no IN VARCHAR2,
 p_cdc_num IN CHAR,
 p_action IN VARCHAR2,
 p_type IN NUMBER,
 p_prepay_date IN DATE,
 p_valid OUT VARCHAR2
 )
 AS
 v_maturityDate DATE;
 v_temp NUMBER(1, 0) := 0;
 v_temp_counter NUMBER(1, 0) := 0;
 v_temp_counter_2 NUMBER(1, 0) := 0;
 v_temp_counter_3 NUMBER(1, 0) := 0;
 
 BEGIN
 
 /*
 declare @valid varchar(150)
 
 exec cdc_gp_num_validate '9258084006', '040','new',4, '7/21/2008' ,@valid output
 
 SELECT @valid
 */
 /*Check IF loan number belongs to cdc number */
 /*If used by Colson users @CDCNmb=0 */
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LTRIM(RTRIM(CDCNmb)) = LTRIM(RTRIM(p_cdc_num))
 AND PrgrmNmb = '504' )
 OR EXISTS ( SELECT *
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LTRIM(RTRIM(CDCNmb)) = LTRIM(RTRIM(p_cdc_num))
 AND PrgrmNmb = '504' )
 OR EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LTRIM(RTRIM(p_cdc_num)) = '0'
 AND PrgrmNmb = '504' )
 OR EXISTS ( SELECT *
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LTRIM(RTRIM(p_cdc_num)) = '0'
 AND PrgrmNmb = '504' );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT TRUNC(MatDt)
 
 INTO v_maturityDate
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no;
 
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_maturityDate := NULL;
 END;
 
 IF LOWER(p_action) = 'new' THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT NoteBalAmt
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no ) = 0;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 p_valid := 'Request can not be processed .Loan was paid off.' ;
 
 END;
 ELSE
 IF ( (p_prepay_date) < (v_maturityDate) ) THEN
 DECLARE
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 IF p_type = 4 THEN
 
 --Check IF Prepayment Actual already exists
 BEGIN
 SELECT 1 INTO v_temp_counter_3
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND (LoanNmb NOT IN ( SELECT LoanNmb
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND CDCPortflStatCdCd IN ('OP')
 
 AND RqstCd = p_type ) OR
 
 LoanNmb NOT IN ( SELECT LoanNmb
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND CDCPortflStatCdCd IN ('CL')
 
 AND RqstCd = p_type AND TRUNC(PrepayDt) = TRUNC(p_prepay_date))) );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 IF v_temp_counter_3 > 0 THEN
 
 BEGIN
 p_valid := 'valid' ;
 
 END;
 ELSE
 
 BEGIN
 p_valid := 'A duplicate record exists for this loan number.' ;
 
 END;
 END IF;
 
 END;
 ELSE
 
 --Check IF Prepayment Estimate already exists
 BEGIN
 
 BEGIN
 SELECT 1 INTO v_temp_counter
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND LoanNmb NOT IN ( SELECT LoanNmb
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND CDCPortflStatCdCd IN ('OP','CL')
 
 AND RqstCd = 9
 AND TRUNC(PrepayDt) = TRUNC(p_prepay_date) ) );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp_counter > 0 THEN
 
 BEGIN
 p_valid := 'valid' ;
 
 END;
 ELSE
 
 BEGIN
 p_valid := 'A duplicate record exists for this loan number.' ;
 
 END;
 END IF;
 
 END;
 END IF;
 /*IF this is a brand new loan funding this month request can not be created*/
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND ( IssDt IS NULL
 OR MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(IssDt)) = 0 ) );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 p_valid := 'The Loan number has no data available.' ;
 
 END;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 p_valid := 'Prepayment date should be before the Maturing Date. ' ;
 
 END;
 END IF;
 END IF;
 
 END;
 /*can not run transcript on cancelled loans */
 ELSE
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( LTRIM(RTRIM(p_action)) = 'Voluntary Prepayment Work-up'
 OR LTRIM(RTRIM(p_action)) = 'Transcript History' )
 AND EXISTS ( SELECT LoanNmb
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(p_prepay_date)
 AND RqstCd = p_type
 AND LTRIM(RTRIM(CDCPortflStatCdCd)) = 'CN' );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 p_valid := 'The ' || LTRIM(RTRIM(p_action)) || ' letter is not available for cancelled requests.' ;
 
 END;
 ELSE
 
 /*To edit or delete loan must exist on RqstCntlTbl table */
 BEGIN
 
 BEGIN
 SELECT 1 INTO v_temp_counter_2
 FROM DUAL
 WHERE EXISTS ( SELECT LoanNmb
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(p_prepay_date)
 AND RqstCd = p_type );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp_counter_2 > 0 THEN
 
 BEGIN
 p_valid := 'valid' ;
 
 END;
 ELSE
 
 BEGIN
 p_valid := 'Loan number is not found or you have entered an incorrect Prepayment Date and Request Type.' ;
 
 END;
 END IF;
 
 END;
 END IF;
 END IF;
 
 END;
 ELSE
 p_valid := 'Loan number is not found or you have entered incorrect Prepayment Date and Request Type.' ;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


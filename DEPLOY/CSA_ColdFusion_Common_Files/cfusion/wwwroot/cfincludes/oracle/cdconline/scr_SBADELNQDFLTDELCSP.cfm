<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE SBADelnqDfltDelCSP 
 -- drop procedure cdc_SBA_delinquent_default_r
 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTemp4Tbl;
 DELETE FROM TTTempSumryTbl;
 DELETE FROM TTTempDelqDfltTbl;
 INSERT INTO TTTemp4Tbl(CDCRegnCd,CDCNmb,LoanNmb,DueAmt,RecrdTyp,CreatUserID,LastUpdtUserID)
 ( SELECT sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 delq.LoanNmb LoanNmb ,
 delq.DueAmt DueAmt ,
 1 RecrdTyp , USER , USER 
 FROM PortflTbl sp
 JOIN DelnqTbl delq ON sp.LoanNmb = delq.LoanNmb );
 INSERT INTO TTTemp4Tbl(CDCRegnCd,CDCNmb,LoanNmb,DueAmt,RecrdTyp,CreatUserID,LastUpdtUserID)
 ( SELECT sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 def.LoanNmb LoanNmb ,
 def.DueAmt DueAmt ,
 2 RecrdTyp , USER , USER
 FROM PortflTbl sp
 JOIN DfltTbl def ON sp.LoanNmb = def.LoanNmb );
 INSERT INTO TTTempSumryTbl(CDCRegnCd,CDCNmb,RecrdTyp,DueAmt,TempLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 RecrdTyp RecrdTyp ,
 SUM(DueAmt) DueAmt ,
 COUNT(LoanNmb) TempLoan , USER , USER 
 FROM TTTemp4Tbl 
 GROUP BY CDCRegnCd,CDCNmb,RecrdTyp );
 INSERT INTO TTTempDelqDfltTbl(CDCRegnCd,CDCNmb,DELQDueAmt,DELQLoan,DfltDueAmt,DfltLoan,CreatUserID,LastUpdtUserID)
 ( SELECT DISTINCT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 0 DELQAmtDue ,
 0 DELQLoan ,
 0 DfltAmtDue ,
 0 DfltLoan , USER , USER 
 FROM TTTempSumryTbl );
 MERGE INTO TTTempDelqDfltTbl t
 USING (SELECT t.ROWID row_id, DueAmt, TempLoan
 FROM TTTempDelqDfltTbl t
 JOIN TTTempSumryTbl S ON t.CDCRegnCd = S.CDCRegnCd
 AND t.CDCNmb = S.CDCNmb 
 WHERE RecrdTyp = 1) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DELQDueAmt = DueAmt,
 DELQLoan = TempLoan;
 MERGE INTO TTTempDelqDfltTbl t
 USING (SELECT t.ROWID row_id, DueAmt, TempLoan
 FROM TTTempDelqDfltTbl t
 JOIN TTTempSumryTbl S ON t.CDCRegnCd = S.CDCRegnCd
 AND t.CDCNmb = S.CDCNmb 
 WHERE RecrdTyp = 2) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET DfltDueAmt = DueAmt,
 DfltLoan = TempLoan;
 OPEN p_SelCur1 FOR
 SELECT t.CDCRegnCd || '-' || t.CDCNmb CDCRegnCd ,
 a.CDCNm CDCNm ,
 t.DELQDueAmt DELQDueAmt ,
 t.DELQLoan DELQLoan ,
 t.DfltDueAmt DfltDueAmt ,
 t.DfltLoan DfltLoan 
 FROM TTTempDelqDfltTbl t
 JOIN AdrTbl a ON t.CDCRegnCd = a.CDCRegnCd
 AND t.CDCNmb = a.CDCNmb
 ORDER BY t.CDCRegnCd,
 t.CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:44. --->
PROCEDURE LtrRlseSelCSP
 ---9
 
 (
 p_SBALoanNmb IN CHAR,
 p_PrepayDt IN DATE,
 p_RqstCd IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_request_ref NUMBER(10,0);
 v_region CHAR(2);
 v_prepay_amount NUMBER(19,4);
 v_posting_date DATE;
 v_last_posting_date DATE;
 v_UnallocatedAmount NUMBER(19,4);
 v_cdc_num CHAR(4);
 
 BEGIN
 
 BEGIN
 SELECT RqstREF
 INTO v_request_ref
 FROM RqstTbl
 WHERE LoanNmb = LTRIM(RTRIM(p_SBALoanNmb))
 AND TRUNC(PrepayDt) = TRUNC(p_PrepayDt)
 AND RqstCd = p_RqstCd;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_request_ref := NULL;
 END;
 
 BEGIN
 SELECT CDCRegnCd
 INTO v_region
 FROM LoanSaveTbl
 WHERE RqstREF = v_request_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_region := NULL;
 END;
 
 BEGIN
 SELECT CDCNmb
 INTO v_cdc_num
 FROM LoanSaveTbl
 WHERE RqstREF = v_request_ref;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_cdc_num := NULL;
 END;
 
 XADJ504SelCSP(p_SBALoanNmb,
 v_request_ref,
 v_posting_date,
 v_last_posting_date) ;
 /*update loan history*/
 /*if exists(select * from CDCLoanTbl where LoanNmb=@LoanNmb)
 begin
 update CDCLoanSaveTbl
 setLoanStat=l.LoanStat,
 NoteBal=l.NoteBal
 from CDCLoanTbl l
 where l.LoanNmb = @LoanNmb
 end --03/06/03 --*/
 BEGIN
 SELECT PrepayAmt
 INTO v_prepay_amount
 FROM PymtSaveTbl
 WHERE RqstREF = v_request_ref
 AND PymtTyp = 'X';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_prepay_amount := NULL;
 END;
 
 BEGIN
 SELECT UnallocAmt
 INTO v_UnallocatedAmount
 FROM PymtSaveTbl
 WHERE RqstREF = v_request_ref
 AND PymtTyp = 'X';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_UnallocatedAmount := NULL;
 END;
 
 DELETE FROM TTTemp13Tbl;
 /*if @PrepayAmt = 0
 begin
 set @PrepayAmt = (select PrepayAmt from CDCPymtSaveTbl where RqstREF = @RqstREF and PymtTyp <> 'X')
 end*/
 INSERT INTO TTTemp13Tbl
 ( LoanNmb, SmllBusCons, TempAmt, PrepayAmt, SemiAnnDt, CDCNm, CDCFaxNmb,CreatUserID,LastUpdtUserID )
 ( SELECT l.LoanNmb ,
 NVL(CASE TRIM(l.SmllBusCons)
 WHEN '' THEN l.BorrNm
 ELSE l.SmllBusCons
 END, l.BorrNm) SmllBusCons ,
 ROUND(l.WKPrepayAmt, 2) TempAmt ,
 ROUND(v_prepay_amount, 2) PrepayAmt ,
 r.SemiAnnDt ,
 NVL(LTRIM(RTRIM(l.CDCNm)), ' ') CDCNm ,
 SUBSTR(a.FaxNmb, 1, 3) || '-' || SUBSTR(a.FaxNmb, 4, 3) || '-' || SUBSTR(a.FaxNmb, 7, 4) CDCFaxNmb ,
 USER ,
 USER
 FROM RqstTbl r,
 LoanSaveTbl l,
 LoanTbl ln,
 AdrTbl a
 WHERE r.RqstREF = l.RqstREF
 AND ln.LoanNmb = l.LoanNmb
 AND l.CDCNmb = a.CDCNmb
 AND l.CDCRegnCd = a.CDCRegnCd
 AND r.RqstREF = v_request_ref
 
 --andln.CurBalNeeded=0
 
 --andln.LoanStat='PREPAID'
 AND ln.NoteBalAmt = 0
 AND r.CDCPortflStatCdCd = 'CL'
 AND ( l.WKPrepayAmt IS NOT NULL
 AND v_prepay_amount IS NOT NULL ) );
 IF v_UnallocatedAmount > 0 THEN
 
 BEGIN
 UPDATE TTTemp13Tbl
 SET TempAmt = TempAmt - v_UnallocatedAmount;
 
 END;
 ELSE
 
 BEGIN
 UPDATE TTTemp13Tbl
 SET TempAmt = TempAmt + v_UnallocatedAmount;
 
 END;
 END IF;
 MERGE INTO TTTemp13Tbl
 USING (SELECT TTTemp13Tbl.ROWID row_id, a.ServCntrNm, a.ServCntrMailAdrStr1Nm, a.ServCntrMailAdrStr2Nm, a.ServCntrMailAdrCtyNm, a.ServCntrMailAdrStCd, a.ServCntrMailAdrZipCd, a.ServCntrCntctNm, a.ServCntrFaxNmb
 FROM TTTemp13Tbl ,ServCntrTbl c ,ServCntrAdrTbl a
 WHERE c.ServCntrID = a.ServCntrID
 AND TRIM(c.CDCRegnCd) = TRIM(v_region)
 AND TRIM(c.CDCNmb) = TRIM(v_cdc_num)) src
 ON ( TTTemp13Tbl.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET TempSCNm = src.ServCntrNm,
 SCMailAdrStr1Nm = src.ServCntrMailAdrStr1Nm,
 SCMailAdrStr2Nm = src.ServCntrMailAdrStr2Nm,
 SCMailAdrCtyNm = src.ServCntrMailAdrCtyNm,
 SCMailAdrStCd = src.ServCntrMailAdrStCd,
 SCMailAdrZipCd = src.ServCntrMailAdrZipCd,
 SCCntctNm = src.ServCntrCntctNm,
 SCFaxNmb = src.ServCntrFaxNmb;
 OPEN p_SelCur1 FOR
 SELECT *
 FROM TTTemp13Tbl ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


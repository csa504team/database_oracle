<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE REQSaveSelCSP
 (
 p_request_ref IN NUMBER,
 p_req_code IN NUMBER,
 p_sba_loan_no IN VARCHAR2,
 p_entry_by IN VARCHAR2,
 p_entry_date IN DATE,
 p_from IN VARCHAR2,
 p_phone IN CHAR,
 p_fax IN CHAR,
 p_e_mail IN VARCHAR2,
 p_semi_annual_date IN DATE,
 p_prepay_date IN DATE,
 p_gfd_received IN CHAR,
 p_status IN CHAR,
 p_detail IN VARCHAR2,
 p_action IN VARCHAR2,
 p_sys_userid IN VARCHAR2,
 p_err OUT VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_gfd_received CHAR(1) := p_gfd_received;
 v_sys_error NUMBER := 0;
 v_maturityDate DATE;
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT TRUNC(MatDt)
 
 INTO v_maturityDate
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no;
 IF ( TRUNC(p_prepay_date) < TRUNC(v_maturityDate) ) THEN
 DECLARE
 ----Get next Semi-Annual date
 v_next_sa_date DATE;
 v_issue_date DATE;
 v_var_gfd_rcvd CHAR(1);
 v_var_status CHAR(2);
 v_upd_ppamt_flag CHAR(1);
 /*
 set @next_sa_date = convert(varchar(2),datepart(mm,@IssDt)) +'/01/'+ convert(varchar(4),datepart(yyyy,getdate()))
 if datepart(mm,@IssDt) > datepart(mm,getdate()) and datepart(mm,@IssDt)-datepart(mm,getdate())<=6
 begin
 set @next_sa_date=convert(varchar(2),datepart(mm,@IssDt)) +'/01/'+ convert(varchar(4),datepart(yyyy,getdate()))
 end
 else
 if datepart(mm,@IssDt)-datepart(mm,getdate())>6
 begin
 set @next_sa_date=dateadd(mm,-6,@next_sa_date)
 end
 else
 set @next_sa_date=dateadd(mm,6,@next_sa_date)
 */
 ---Make sure there is no record for this loan with the same Prepayment Date and Request Type. ---
 v_type CHAR(1);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 --SQL Server BEGIN TRANSACTION;
 SAVEPOINT start_tran;
 IF v_gfd_received IS NULL THEN
 
 BEGIN
 v_gfd_received := 'N' ;
 
 END;
 END IF;
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT IssDt
 
 INTO v_issue_date
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no;
 ---Check if issue date is a valid date
 /*get semi-annual date that is closest to current date --03/03/03 as of John's request--*/
 IF v_issue_date IS NOT NULL THEN
 DECLARE
 /* Rule:
 The next semi-annual date is always a multiple of 6 from the Issue date
 The next semi-annual date is always greater than the Prepayment date
 */
 v_MnDiff -- Number of months between the Issue Date and Prepayment date
 NUMBER(10,0);
 v_MntoSADt NUMBER(10,0);-- The number of months needed to make @MnDiff a multiple of 6 (SemiAnnual)
 
 BEGIN
 -- the sum of @MnDiff and @MnSADt will always be a multiple of 6
 v_MntoSADt := 0 ;
 -- ZM: correction made to this calculation to calculate the number of months between prepay date and issue date; previously, the last_day function was not being used
 v_MnDiff := TRUNC(MONTHS_BETWEEN(LAST_DAY(p_prepay_date), LAST_DAY(v_issue_date)));
 IF MOD(v_MnDiff, 6) <> 0 THEN
 v_MntoSADt := v_MnDiff + 6 - (MOD(v_MnDiff, 6)) ;-- Adjust the number of months to the next sa date
 ELSE
 v_MntoSADt := v_MnDiff + 6 ;
 END IF;
 v_next_sa_date := TRUNC(ADD_MONTHS(v_issue_date, v_MntoSADt));
 --semi annual date must be the first of the month.
 v_next_sa_date := TRUNC(v_next_sa_date, 'MONTH') ;
 
 END;
 END IF;
 v_type := 'W' ;
 --check for duplicate autural
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'Y'
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND RqstCd = 4
 AND CDCPortflStatCdCd = 'OP'
 AND p_status = 'OP'
 AND p_req_code = 4
 AND RqstREF <> p_request_ref );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 p_err := 'duplicate' ;
 ROLLBACK TO start_tran;
 RETURN;
 
 END;
 END IF;
 IF p_req_code = 4 THEN
 
 ----'Prepayment Actual'
 BEGIN
 v_type := 'A' ;
 
 END;
 END IF;
 --There should not be a record with the same prepay date and request type
 IF p_request_ref IS NULL THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT RqstREF
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(p_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT RqstREF
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(p_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code
 AND RqstREF <> p_request_ref ;
 
 END;
 END IF;
 IF SQL%ROWCOUNT > 0 THEN
 
 ---duplicates
 BEGIN
 p_err := 'duplicate' ;
 ROLLBACK TO start_tran;
 RETURN;
 
 END;
 ELSE
 
 ---valid loan
 BEGIN
 --ADD NEW LOAN --
 IF LOWER(p_action) = 'new' THEN
 
 BEGIN
 ---------------insert into RqstCntlTbl table----------
 INSERT INTO RqstCntlTbl
 ( RqstREF, RqstTyp, LoanNmb, EntryDt, EntryBy, SYSLastTime, SYSProgID, SYSUserID,CreatUserID,LastUpdtUserID )
 VALUES ( RqstCntlTblRqstREFSeq.NEXTVAL, v_type, p_sba_loan_no, p_entry_date, p_entry_by, SYSDATE, 'cdc_request_new', p_sys_userid, USER, USER );
 -----------insert into CDCRqstTbl table----------------
 INSERT INTO RqstTbl
 ( RqstREF, RqstDt, RqstFrom, PhnNmb, FaxNmb, EmailAdr, LoanNmb, RqstCd, SemiAnnDt, PrepayDt, GFDRecv, CDCPortflStatCdCd, StatDt, SYSProgID, SYSUserID, SYSLastTime,CreatUserID,LastUpdtUserID )
 ( SELECT MAX(cnt.RqstREF) ,
 p_entry_date ,
 p_from ,
 p_phone ,
 p_fax ,
 p_e_mail ,
 p_sba_loan_no ,
 p_req_code ,
 ----@SemiAnnDt,
 v_next_sa_date ,
 p_prepay_date ,
 v_gfd_received ,
 p_status ,
 p_entry_date ,
 'cdc_request_new' ,
 p_sys_userid ,
 SYSDATE,
 USER,
 USER
 FROM RqstCntlTbl cnt );
 ---------------insert into RqstDtlTbl table----------------
 INSERT INTO RqstDtlTbl
 ( RqstREF, AudtDtl, SYSProgID, SYSUserID, SYSLastTime,CreatUserID,LastUpdtUserID )
 ( SELECT MAX(cnt.RqstREF) ,
 p_detail ,
 'cdc_request_new' ,
 p_sys_userid ,
 SYSDATE ,
 USER ,
 USER
 FROM RqstCntlTbl cnt );
 -----if Gfd received was changed to 'Y' pick open wires for this loan
 IF v_gfd_received = 'Y' THEN
 
 BEGIN
 Wires504UpdCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code,
 v_gfd_received,
 'cdc_req_save',
 p_sys_userid) ;
 
 END;
 END IF;
 p_err := 'valid' ;
 
 END;
 
 --EDIT LOAN --
 ELSE
 DECLARE
 v_prep_prepay_date DATE;
 v_prep_req_code NUMBER(10,0);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 SELECT PrepayDt
 
 INTO v_prep_prepay_date
 FROM RqstTbl
 WHERE RqstREF = p_request_ref;
 SELECT RqstCd
 
 INTO v_prep_req_code
 FROM RqstTbl
 WHERE RqstREF = p_request_ref;
 IF (TRUNC(p_prepay_date) - TRUNC(v_prep_prepay_date)<> 0
 OR v_prep_req_code <> p_req_code ) THEN
 
 BEGIN
 UPDATE WiresTbl
 SET PrepayDt = p_prepay_date,
 RqstCd = p_req_code,
 SYSProgID = 'cdc_req_save',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE LoanNmb = LTRIM(RTRIM(p_sba_loan_no))
 AND TRUNC(PrepayDt) - TRUNC(v_prep_prepay_date) = 0
 AND RqstCd = v_prep_req_code;
 
 END;
 END IF;
 -- variables to hold the GFDRecv field value
 SELECT LTRIM(RTRIM(GFDRecv))
 
 INTO v_var_gfd_rcvd
 FROM RqstTbl
 WHERE RqstREF = p_request_ref;
 -----if Gfd received was changed from 'N' to 'Y' - pick open wires for this loan
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) IN ( 'N',' ' )
 
 OR ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) IS NULL )
 AND v_gfd_received = 'Y';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 Wires504UpdCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code,
 v_gfd_received,
 'cdc_req_save',
 p_sys_userid) ;
 
 END;
 END IF;
 -----if Gfd received was changed from 'Y' to 'N' - free up wires for this loan
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT GFDRecv
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) = 'Y'
 AND v_gfd_received = 'N';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 Wires504UpdCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code,
 v_gfd_received,
 'cdc_req_save',
 p_sys_userid) ;
 
 END;
 END IF;
 --update---------------------------------------------------------
 -----if request status was changed from Open to Closed or Cancelled - save record to hist tables----
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT CDCPortflStatCdCd
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) = 'OP'
 AND p_status <> 'OP'
 AND NOT EXISTS ( SELECT *
 FROM LoanSaveTbl
 WHERE RqstREF = p_request_ref );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 DECLARE
 v_posting_date DATE;
 
 BEGIN
 SELECT MAX(TRUNC(PostingDt))
 
 INTO v_posting_date
 FROM PymtHistryTbl
 WHERE LoanNmb = p_sba_loan_no
 AND PymtTyp <> 'X'
 AND PymtTyp <> 'AR'
 AND PymtTyp <> 'CR';
 HistSaveInsCSP(p_request_ref,
 p_sba_loan_no,
 v_posting_date,
 p_prepay_date) ;
 
 END;
 END IF;
 --******* new code 9/19/2003 ******* scott
 v_upd_ppamt_flag := 'N' ;
 -- if gfd is required
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'Y'
 FROM LoanSaveTbl
 WHERE RqstREF = p_request_ref
 AND TRUNC(TO_DATE('1/1/1992', 'MM/DD/YYYY')) - TRUNC(IssDt) < 0) ;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 -- check if GFDRecv WAS NOT Y and user changed it to Y or
 -- if status changed from NOT CL to CL
 IF ( v_var_gfd_rcvd <> 'Y'
 AND v_gfd_received = 'Y' ) THEN
 
 BEGIN
 -- Call procedure for ream process and to save the Prepayment Amt!!!
 PPAmtUpdSelCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code) ;
 v_upd_ppamt_flag := 'Y' ;
 
 END;
 END IF;
 
 END;
 ELSE
 
 -- gfd not required
 BEGIN
 IF ( v_var_gfd_rcvd <> v_gfd_received ) THEN
 
 BEGIN
 -- Call procedure for ream process and to save the Prepayment Amt!!!
 PPAmtUpdSelCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code) ;
 v_upd_ppamt_flag := 'Y' ;
 
 END;
 END IF;
 
 END;
 END IF;
 IF v_upd_ppamt_flag = 'N' THEN
 
 -- if prepayment Amt not already calculated from above
 BEGIN
 -- variables to hold the status field value
 SELECT LTRIM(RTRIM(CDCPortflStatCdCd))
 
 INTO v_var_status
 FROM RqstTbl
 WHERE RqstREF = p_request_ref;
 -- check if GFDRecv WAS NOT Y and user changed it to Y or
 -- if status changed from NOT CL to CL
 IF ( v_var_status <> 'CL'
 AND p_status = 'CL' ) THEN
 
 BEGIN
 -- Call procedure for ream process and to save the Prepayment Amt!!!
 PPAmtUpdSelCSP(p_sba_loan_no,
 p_prepay_date,
 p_req_code) ;
 
 END;
 END IF;
 
 END;
 END IF;
 -- ******* new code end ******* scott
 -----if request status was changed from Open to Cancelled - update WireStat to open ----
 ----- set GFD received flag to 'N' ----
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( ( SELECT CDCPortflStatCdCd
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) IN ( 'CL','OP' )
 )
 AND p_status = 'CN';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 UPDATE WiresTbl
 SET WireStatCd = 'OP',
 SYSProgID = 'cdc_req_save',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE LoanNmb = LTRIM(RTRIM(p_sba_loan_no))
 AND TRUNC(p_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code;
 
 END;
 END IF;
 /***update CDCRqstTbl
 set GFDRecv ='N',
 SYSProgID='cdc_req_save',
 SYSUserID=@SYSUserID,
 SYSLastTime=getdate()
 where LoanNmb =ltrim(rtrim(@LoanNmb))
 and datediff(dd,PrepayDt,@PrepayDt)= 0
 and RqstCd = @req_code***/
 -----if request status was changed from Cancelled or Closed to Open - delete record from hist tables----
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE ( SELECT CDCPortflStatCdCd
 FROM RqstTbl
 WHERE RqstREF = p_request_ref ) <> 'OP'
 AND p_status = 'OP';
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 DELETE LoanSaveTbl
 
 WHERE RqstREF = p_request_ref;
 DELETE PymtSaveTbl
 
 WHERE RqstREF = p_request_ref;
 UPDATE WiresTbl
 SET WireStatCd = 'CL',
 SYSProgID = 'cdc_req_save',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE LoanNmb = LTRIM(RTRIM(p_sba_loan_no))
 AND TRUNC(p_prepay_date) - TRUNC(PrepayDt) = 0
 AND RqstCd = p_req_code;
 UPDATE RqstTbl
 SET GFDRecv = 'Y',
 SYSProgID = 'cdc_req_save',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE LoanNmb IN ( SELECT WiresTbl.LoanNmb
 FROM WiresTbl
 WHERE WiresTbl.LoanNmb = p_sba_loan_no
 AND TRUNC(WiresTbl.PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND WiresTbl.RqstCd = p_req_code )
 ;
 
 END;
 END IF;
 --set @GFDRecv='Y'
 /*set @GFDRecv=(select GFDRecv from CDCRqstTbl where LoanNmb =ltrim(rtrim(@LoanNmb))
 and datediff(dd,PrepayDt,@PrepayDt)= 0
 and RqstCd = @req_code)
 if @GFDRecv is null
 begin
 set @GFDRecv = 'N'
 end*/
 -----update request----------------------------------------------
 BEGIN
 UPDATE RqstCntlTbl
 SET RqstTyp = v_type,
 EntryDt = TRUNC(p_entry_date),
 EntryBy = p_entry_by,
 SYSLastTime = SYSDATE,
 SYSProgID = 'cdc_request_mod',
 SYSUserID = p_sys_userid
 WHERE RqstREF = p_request_ref;
 EXCEPTION WHEN OTHERS THEN
 v_sys_error := SQLCODE;
 END;
 IF v_sys_error > 0 THEN
 
 BEGIN
 p_err := 'sql_error' ;
 ROLLBACK TO start_tran;
 
 END;
 END IF;
 v_sys_error := 0;
 BEGIN
 UPDATE RqstTbl
 SET RqstDt = TRUNC(p_entry_date),
 RqstFrom = p_from,
 PhnNmb = p_phone,
 FaxNmb = p_fax,
 EmailAdr = p_e_mail,
 RqstCd = p_req_code
 ---SemiAnnDt=convert(datetime,@SemiAnnDt),
 ,
 SemiAnnDt = v_next_sa_date,
 PrepayDt = TRUNC(p_prepay_date),
 GFDRecv = CASE p_status
 WHEN 'CN' THEN 'N'
 ELSE v_gfd_received
 END,
 CDCPortflStatCdCd = p_status,
 StatDt = TRUNC(p_entry_date),
 SYSProgID = 'cdc_request_mod',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE RqstREF = p_request_ref;
 EXCEPTION WHEN OTHERS THEN
 v_sys_error := SQLCODE;
 END;
 IF v_sys_error > 0 THEN
 
 BEGIN
 p_err := 'sql_error' ;
 ROLLBACK TO start_tran;
 
 END;
 END IF;
 v_sys_error := 0;
 BEGIN
 UPDATE RqstDtlTbl
 SET AudtDtl = p_detail,
 SYSProgID = 'cdc_request_mod',
 SYSUserID = p_sys_userid,
 SYSLastTime = SYSDATE
 WHERE RqstREF = p_request_ref;
 EXCEPTION WHEN OTHERS THEN
 v_sys_error := SQLCODE;
 END;
 IF v_sys_error > 0 THEN
 
 BEGIN
 p_err := 'sql_error' ;
 
 END;
 ELSE
 
 BEGIN
 p_err := 'valid' ;
 
 END;
 END IF;
 v_sys_error := 0;
 
 END;
 END IF;
 
 END;
 END IF;
 COMMIT;
 
 END;
 ELSE
 
 BEGIN
 p_err := 'Prepayment date should be before the Maturing Date' ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


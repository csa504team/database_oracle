<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE SBSREFILoanDelCSP 
 -- cdc_SBA_refi_loans_r
 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTemp6Tbl;
 DELETE FROM TTTempSumry2Tbl;
 INSERT INTO TTTemp6Tbl (CDCRegnCd,CDCNmb,LoanNmb,LoanAmt,RefiLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd ,
 CDCNmb ,
 LoanNmb ,
 NotePrinAmt LoanAmt ,
 RefiLoan , USER , USER
 FROM LoanTbl 
 WHERE RefiLoan = 'Y' );
 INSERT INTO TTTempSumry2Tbl (CDCRegnCd,CDCNmb,LoanAmt,TempLoan,CreatUserID,LastUpdtUserID)
 ( SELECT CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 SUM(LoanAmt) LoanAmt ,
 COUNT(LoanNmb) TempLoan , USER , USER 
 FROM TTTemp6Tbl 
 GROUP BY CDCRegnCd,CDCNmb );
 OPEN p_SelCur1 FOR
 SELECT t.CDCRegnCd || '-' || t.CDCNmb CDCRegnCd ,
 a.CDCNm CDCNm ,
 t.LoanAmt LoanAmt ,
 t.TempLoan TempLoan 
 FROM TTTempSumry2Tbl t
 JOIN AdrTbl a ON t.CDCRegnCd = a.CDCRegnCd
 AND t.CDCNmb = a.CDCNmb
 ORDER BY t.CDCRegnCd,
 t.CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


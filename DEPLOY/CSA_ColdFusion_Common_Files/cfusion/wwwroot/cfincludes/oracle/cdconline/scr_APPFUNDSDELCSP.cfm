<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AppFundsDelCSP 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CDC_Cert IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTempAppFundsTbl;
 -- CDC_App_Funds_R null, '10','468 ',0
 IF p_SBA = 1 THEN
 INSERT INTO TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.BorrNm BorrNm ,
 sp.StmtNm StmtNm ,
 ap.LoanNmb LoanNmb ,
 ap.PymtRecvDDtx PymtRecvDDtx ,
 ap.SBAFeeAmt SBAFeeAmt ,
 ap.CSAFeeAmt CSAFeeAmt ,
 ap.CDCFeeAmt CDCFeeAmt ,
 ap.LateFeeAmt LateFeeAmt ,
 ap.IntApplAmt IntApplAmt ,
 ap.PrinApplAmt PrinApplAmt ,
 ap.DiffToEscrowAmt DiffToEscrowAmt ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm;
 
 --select * from CDCPortflTbl
 ELSE
 IF LTRIM(RTRIM(p_SOD)) > 0 THEN
 INSERT INTO TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.BorrNm BorrNm ,
 sp.StmtNm StmtNm ,
 ap.LoanNmb LoanNmb ,
 ap.PymtRecvDDtx PymtRecvDDtx ,
 ap.SBAFeeAmt SBAFeeAmt ,
 ap.CSAFeeAmt CSAFeeAmt ,
 ap.CDCFeeAmt CDCFeeAmt ,
 ap.LateFeeAmt LateFeeAmt ,
 ap.IntApplAmt IntApplAmt ,
 ap.PrinApplAmt PrinApplAmt ,
 ap.DiffToEscrowAmt DiffToEscrowAmt ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 
 --WHERE UserLevelRoleID = @SOD
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 ORDER BY sp.StmtNm;
 ELSE
 INSERT INTO TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
 SELECT sp.UserLevelRoleID UserLevelRoleID ,
 sp.DistNm DistNm ,
 sp.CDCRegnCd CDCRegnCd ,
 sp.CDCNmb CDCNmb ,
 sp.CDCNm CDCNm ,
 sp.BorrNm BorrNm ,
 sp.StmtNm StmtNm ,
 ap.LoanNmb LoanNmb ,
 ap.PymtRecvDDtx PymtRecvDDtx ,
 ap.SBAFeeAmt SBAFeeAmt ,
 ap.CSAFeeAmt CSAFeeAmt ,
 ap.CDCFeeAmt CDCFeeAmt ,
 ap.LateFeeAmt LateFeeAmt ,
 ap.IntApplAmt IntApplAmt ,
 ap.PrinApplAmt PrinApplAmt ,
 ap.DiffToEscrowAmt DiffToEscrowAmt ,
 NULL RefiLoan ,
 USER ,
 USER
 FROM PortflTbl sp
 JOIN FundsTbl ap ON sp.LoanNmb = ap.LoanNmb
 WHERE sp.CDCRegnCd = p_RegionNum
 AND sp.CDCNmb = p_CDC_Cert
 ORDER BY sp.StmtNm;
 END IF;
 END IF;
 MERGE INTO TTTempAppFundsTbl p
 USING (SELECT p.ROWID row_id, ln.RefiLoan
 FROM TTTempAppFundsTbl p
 JOIN LoanTbl ln ON p.LoanNmb = ln.LoanNmb ) src
 ON ( p.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID UserLevelRoleID ,
 DistNm DistNm ,
 CDCRegnCd CDCRegnCd ,
 CDCNmb CDCNmb ,
 CDCNm CDCNm ,
 BorrNm BorrNm ,
 StmtNm StmtNm ,
 LoanNmb LoanNmb ,
 PymtRecvDt PymtRecvDt ,
 SBAFeeAmt SBAFeeAmt ,
 CSAFeeAmt CSAFeeAmt ,
 CDCFeeAmt CDCFeeAmt ,
 LateFeeAmt LateFeeAmt ,
 IntApplAmt IntApplAmt ,
 PrinApplAmt PrinApplAmt ,
 DiffToEscrowAmt DiffToEscrowAmt ,
 RefiLoan RefiLoan 
 FROM TTTempAppFundsTbl ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


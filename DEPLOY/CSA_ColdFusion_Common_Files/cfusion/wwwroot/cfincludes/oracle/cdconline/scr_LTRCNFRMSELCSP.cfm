<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LtrCnfrmSelCSP
 (
 p_sba_loan_no IN CHAR,
 p_prepay_date IN CHAR,
 p_req_code IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 v_status CHAR(2);
 v_issue_date DATE;
 v_gfd VARCHAR2(3);
 v_req_ref NUMBER(10,0);
 
 BEGIN
 
 SELECT MAX(RqstREF)
 
 INTO v_req_ref
 FROM RqstTbl
 WHERE LoanNmb = p_sba_loan_no
 AND TRUNC(PrepayDt) = TRUNC(TO_DATE(p_prepay_date))
 AND RqstCd = p_req_code AND ROWNUM < 2;
 v_gfd := 'No' ;
 SELECT CDCPortflStatCdCd
 
 INTO v_status
 FROM RqstTbl
 WHERE RqstREF = v_req_ref;
 IF v_status = 'OP' THEN
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT IssDt
 
 INTO v_issue_date
 FROM LoanTbl
 WHERE LoanNmb = p_sba_loan_no
 AND IssDt IS NOT NULL;
 IF TRUNC(v_issue_date) > TO_DATE('01/01/1992', 'MM/DD/YYYY') THEN
 
 BEGIN
 v_gfd := 'Yes' ;
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT rq.RqstREF ,
 rq.LoanNmb ,
 rq.FaxNmb ,
 rq.PrepayDt ,
 ln.IssDt ,
 NVL(TRIM(ln.SmllBusCons), ln.BorrNm) SmllBusCons ,
 v_gfd gfd
 FROM RqstTbl rq
 JOIN LoanTbl ln ON rq.RqstREF = v_req_ref
 AND rq.LoanNmb = ln.LoanNmb ;
 
 END;
 ELSE
 
 BEGIN
 /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
 SELECT IssDt
 
 INTO v_issue_date
 FROM LoanSaveTbl
 WHERE RqstREF = v_req_ref;
 IF TRUNC(v_issue_date) > TO_DATE('01/01/1992', 'MM/DD/YYYY') THEN
 
 BEGIN
 v_gfd := 'Yes' ;
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT rq.RqstREF ,
 rq.LoanNmb ,
 rq.FaxNmb ,
 rq.PrepayDt ,
 ls.IssDt ,
 NVL(TRIM(ls.SmllBusCons), ls.BorrNm) SmllBusCons ,
 v_gfd gfd
 FROM RqstTbl rq
 JOIN LoanSaveTbl ls ON rq.RqstREF = v_req_ref
 AND rq.RqstREF = ls.RqstREF ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


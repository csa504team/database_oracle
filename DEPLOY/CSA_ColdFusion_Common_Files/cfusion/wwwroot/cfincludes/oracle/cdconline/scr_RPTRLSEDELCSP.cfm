<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTRlseDelCSP
 (
 p_srv_cntr_code IN CHAR,
 p_months IN NUMBER,
 p_sort_order IN VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTmpRLSTbl;
 DELETE FROM TTTmpPostingDtTbl;
 DELETE FROM TTTmpPrepymtAmtTbl;
 IF p_months = 0 THEN
 
 -- All months
 BEGIN
 INSERT INTO TTTmpRLSTbl
 ( RqstREF, LoanNmb, PrepymtDt, RqstCd, BorrNm, CDCNm, WKPrepymtAmt ,CreatUserID,LastUpdtUserID)
 ( SELECT rq.RqstREF RqstREF ,
 ls.LoanNmb LoanNmb ,
 rq.PrepayDt PrepayDt ,
 rq.RqstCd
 RqstCd ,-- always 4
 
 NVL(ls.SmllBusCons, ls.BorrNm) BorrNm ,
 RTRIM(NVL(ls.CDCNm, ' ')) CDCNm ,
 ls.WKPrepayAmt ,
 USER ,
 USER
 FROM RqstTbl rq,
 LoanSaveTbl ls,
 LoanTbl ln
 WHERE rq.RqstREF = ls.RqstREF
 AND ls.LoanNmb = ln.LoanNmb
 AND ls.CDCRegnCd IN ( SELECT CDCRegnCd
 FROM ServCntrTbl
 WHERE ServCntrID = TRIM(p_srv_cntr_code) )
 
 AND TRIM(ls.CDCNmb) IN ( SELECT TRIM(CDCNmb)
 FROM ServCntrTbl
 WHERE ServCntrID = TRIM(p_srv_cntr_code) )
 
 AND ln.NoteBalAmt = 0
 AND rq.CDCPortflStatCdCd = 'CL'
 AND rq.RqstCd NOT IN ( 1,6,7,9 )
 
 AND ls.WKPrepayAmt IS NOT NULL );
 
 END;
 ELSE
 
 BEGIN
 INSERT INTO TTTmpRLSTbl
 ( RqstREF, LoanNmb, PrepymtDt, RqstCd, BorrNm, CDCNm, WKPrepymtAmt,CreatUserID,LastUpdtUserID )
 ( SELECT rq.RqstREF RqstREF ,
 ls.LoanNmb LoanNmb ,
 rq.PrepayDt PrepayDt ,
 rq.RqstCd
 RqstCd ,-- always 4
 
 NVL(ls.SmllBusCons, ls.BorrNm) BorrNm ,
 RTRIM(NVL(ls.CDCNm, ' ')) CDCNm ,
 ls.WKPrepayAmt ,
 USER ,
 USER
 FROM RqstTbl rq,
 LoanSaveTbl ls,
 LoanTbl ln
 WHERE rq.RqstREF = ls.RqstREF
 AND ls.LoanNmb = ln.LoanNmb
 AND ls.CDCRegnCd IN ( SELECT CDCRegnCd
 FROM ServCntrTbl
 WHERE ServCntrID = TRIM(p_srv_cntr_code) )
 
 AND TRIM(ls.CDCNmb) IN ( SELECT TRIM(CDCNmb)
 FROM ServCntrTbl
 WHERE ServCntrID = TRIM(p_srv_cntr_code) )
 
 AND ln.NoteBalAmt = 0
 AND rq.CDCPortflStatCdCd = 'CL'
 AND rq.RqstCd NOT IN ( 1,6,7,9 )
 
 AND ls.WKPrepayAmt IS NOT NULL
 AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.PrepayDt))) < p_months );
 
 END;
 END IF;
 --** Remove Prepayment Amt column as per task 2248: Scott 5/10/2004
 --update the prepayment Amt
 --updateTTTmpRLSTbl
 --setPrepayAmt = ps.PrepayAmt
 --fromCDCPymtSaveTbl ps, TTTmpRLSTbl tr
 --whereps.RqstREF = tr.RqstREF
 --andps.PymtTyp = 'X'
 INSERT INTO TTTmpPostingDtTbl
 ( RqstREF, PostingDt,CreatUserID,LastUpdtUserID )
 ( SELECT ps.RqstREF ,
 MAX(ps.PostingDt) , USER, USER
 FROM TTTmpRLSTbl tr,
 PymtSaveTbl ps
 WHERE tr.RqstREF = ps.RqstREF
 AND RTRIM(LTRIM(ps.PymtTyp)) <> 'X'
 GROUP BY ps.RqstREF );
 MERGE INTO TTTmpRLSTbl tr
 USING (SELECT tr.ROWID row_id, tp.PostingDt
 FROM TTTmpRLSTbl tr ,TTTmpPostingDtTbl tp
 WHERE tr.RqstREF = tp.RqstREF) src
 ON ( tr.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET PymtPostDt = src.PostingDt;
 INSERT INTO TTTmpPrepymtAmtTbl
 ( LoanNmb, PrepayAmt ,CreatUserID,LastUpdtUserID)
 ( SELECT ph.LoanNmb ,
 SUM(ph.PrepayAmt) - SUM(ph.UnallocAmt) PrepayAmt , USER, USER
 FROM TTTmpRLSTbl tr,
 PymtHistryTbl ph
 WHERE tr.LoanNmb = ph.LoanNmb
 AND TRUNC(ph.PostingDt) > TRUNC(tr.PymtPostDt)
 AND ph.PymtTyp = 'X'
 GROUP BY ph.LoanNmb );
 MERGE INTO TTTmpRLSTbl tr
 USING (SELECT tr.ROWID row_id, tp.PrepayAmt
 FROM TTTmpRLSTbl tr ,TTTmpPrepymtAmtTbl tp
 WHERE tr.LoanNmb = tp.LoanNmb) src
 ON ( tr.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET PrepayAmt = src.PrepayAmt;
 --update the servicing center name
 MERGE INTO TTTmpRLSTbl
 USING (SELECT TTTmpRLSTbl.ROWID row_id, ServCntrAdrTbl.ServCntrNm
 FROM TTTmpRLSTbl ,ServCntrAdrTbl
 WHERE LOWER(TRIM(ServCntrAdrTbl.ServCntrID)) = LOWER(TRIM(p_srv_cntr_code))) src
 ON ( TTTmpRLSTbl.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET ServCntrNm = src.ServCntrNm;
 --update the date-time when the release letter became available
 -- insert current date only if not already there
 INSERT INTO RlsAvailTbl
 ( RqstREF, AvailDt ,CreatUserID,LastUpdtUserID)
 ( SELECT tr.RqstREF ,
 SYSDATE , USER, USER
 FROM TTTmpRLSTbl tr
 LEFT JOIN RlsAvailTbl av ON tr.RqstREF = av.RqstREF
 WHERE av.AvailDt IS NULL );
 MERGE INTO TTTmpRLSTbl tr
 USING (SELECT tr.ROWID row_id, ra.AvailDt
 FROM TTTmpRLSTbl tr ,RlsAvailTbl ra
 WHERE tr.RqstREF = ra.RqstREF) src
 ON ( tr.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET AvailDt = src.AvailDt;
 --select records with appropriate sorting
 IF p_sort_order = 'PrepayDt' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 LTRIM(RTRIM(CDCNm)) CDCNm ,
 ServCntrNm ,
 AvailDt
 FROM TTTmpRLSTbl
 WHERE WKPrepymtAmt <= PrepayAmt + 6
 ORDER BY PrepymtDt DESC,
 LoanNmb ASC ;
 
 END;
 ELSE
 IF p_sort_order = 'CDCNm' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 LTRIM(RTRIM(CDCNm)) CDCNm ,
 ServCntrNm ,
 AvailDt
 FROM TTTmpRLSTbl
 WHERE WKPrepymtAmt <= PrepayAmt + 6
 ORDER BY CDCNm,
 LoanNmb ;
 
 END;
 ELSE
 IF p_sort_order = 'LoanNmb' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT DISTINCT LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 LTRIM(RTRIM(CDCNm)) CDCNm ,
 ServCntrNm ,
 AvailDt
 FROM TTTmpRLSTbl
 WHERE WKPrepymtAmt <= PrepayAmt + 6
 ORDER BY LoanNmb ASC,
 PrepymtDt ASC ;
 
 END;
 ELSE
 
 -- default: sort by date available
 BEGIN
 DELETE FROM TTTmpRLSSortTbl;
 --******** insert into another temp table to preserve sort order for nulls *****
 INSERT INTO TTTmpRLSSortTbl(RqstRef,LoanNmb,PrepymtDt,RqstCd,BorrNm,CDCNm,ServCntrNm,AvailDt,CreatUserID,LastUpdtUserID)
 SELECT RqstREF ,
 LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 CDCNm ,
 ServCntrNm ,
 AvailDt ,
 USER ,
 USER
 FROM TTTmpRLSTbl
 WHERE AvailDt IS NULL
 AND WKPrepymtAmt <= PrepayAmt + 6
 ORDER BY LoanNmb ASC,
 PrepymtDt ASC;
 INSERT INTO TTTmpRLSSortTbl(RqstRef,LoanNmb,PrepymtDt,RqstCd,BorrNm,CDCNm,ServCntrNm,AvailDt,CreatUserID,LastUpdtUserID)
 SELECT RqstREF ,
 LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 CDCNm ,
 ServCntrNm ,
 AvailDt ,
 USER ,
 USER
 FROM TTTmpRLSTbl
 WHERE AvailDt IS NOT NULL
 AND WKPrepymtAmt <= PrepayAmt + 6
 ORDER BY AvailDt DESC,
 LoanNmb ASC,
 PrepymtDt ASC;
 OPEN p_SelCur1 FOR
 SELECT DISTINCT RlseRef ,
 LoanNmb ,
 PrepymtDt ,
 RqstCd ,
 BorrNm ,
 LTRIM(RTRIM(CDCNm)) CDCNm ,
 ServCntrNm ,
 AvailDt
 FROM TTTmpRLSSortTbl
 ORDER BY RlseRef ASC,
 LoanNmb ASC,
 PrepymtDt ASC ;
 --EXECUTE IMMEDIATE ' TRUNCATE TABLE TTTmpRLSSortTbl ';
 
 END;
 END IF;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


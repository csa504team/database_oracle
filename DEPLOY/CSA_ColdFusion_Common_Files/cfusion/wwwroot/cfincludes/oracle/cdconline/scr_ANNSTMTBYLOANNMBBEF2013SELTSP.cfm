<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AnnStmtByLoanNmbBef2013SelTSP
 (
 v_year IN NUMBER,
 v_cdcregion IN VARCHAR2,
 v_cdcnum IN VARCHAR2,
 v_sba_loan_number IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT ctp.LoanNmb LoanNmb ,
 ctp.BorrNm ,
 ctp.BorrMailAdrStr1Nm BorrMailAdrStr1Nm ,
 ctp.BorrMailAdrCtyStZipCd BorrMailAdrCtyStZipCd ,
 ctp.TaxID ,
 ctp.IntPaidAmt total_interestpaid ,
 ctp.CDCFeeAmt total_cdcfees ,
 ctp.CSAFeeAmt total_csa_fees ,
 ctp.SBAFeeAmt total_sba_fees ,
 ctp.NoteBalAmt ,
 ctp.TaxYrNmb ,
 ctp.CDCNmb ,
 ' ' CDCNm ,
 ' ' CDCPhnNmb 
 FROM Tax1098PrintTbl ctp
 WHERE 1 = 1
 AND ctp.TaxYrNmb = v_year
 AND ctp.CDCRegnCd = v_cdcregion
 AND ctp.CDCNmb = v_cdcnum
 AND ctp.LoanNmb = v_sba_loan_number ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


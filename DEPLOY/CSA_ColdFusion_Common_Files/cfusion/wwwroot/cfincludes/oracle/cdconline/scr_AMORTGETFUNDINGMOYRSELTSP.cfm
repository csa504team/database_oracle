<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AmortGetFundingMoYrSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT EXTRACT(MONTH FROM FundingDt) funding_month,
 EXTRACT(YEAR FROM FundingDt) funding_year 
 FROM AmortHdrTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 ORDER BY EXTRACT(YEAR FROM FundingDt) DESC,
 EXTRACT(MONTH FROM FundingDt) DESC ;--cdc_amort_get_funding_month_year_r '02','005'
 --select * from CDCAmortHdrTbl 
 --WHERE CDCRegn = '02'
 --AND CDCNmb = '005'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


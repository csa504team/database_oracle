<!--- Saved 09/21/2018 18:20:47. --->
PROCEDURE StatNonCurrentTotSelTSP 
 --CDC_Status_Noncurrent_Total_R null, '02','650'
 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(LoanAmt) loan_amount_tt ,
 SUM(MoPymtAmt) monthly_pym_tt ,
 SUM(SBAFeeAmt) sba_fee_tt ,
 SUM(CSAFeeAmt) csa_fee_tt ,
 SUM(CDCFeeAmt) cdc_fee_tt ,
 SUM(DiffToEscrowAmt) diff_to_escrow_tt ,
 SUM(TotFeeDueAmt) total_fee_due_tt ,
 SUM(IntDueAmt) interest_due_tt ,
 SUM(PrinDueAmt) principal_due_tt ,
 SUM(LateFeeDueAmt) late_fee_due_tt ,
 SUM(TotDueAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM PortflTbl 
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' ) ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(LoanAmt) loan_amount_tt ,
 SUM(MoPymtAmt) monthly_pym_tt ,
 SUM(SBAFeeAmt) sba_fee_tt ,
 SUM(CSAFeeAmt) csa_fee_tt ,
 SUM(CDCFeeAmt) cdc_fee_tt ,
 SUM(DiffToEscrowAmt) diff_to_escrow_tt ,
 SUM(TotFeeDueAmt) total_fee_due_tt ,
 SUM(IntDueAmt) interest_due_tt ,
 SUM(PrinDueAmt) principal_due_tt ,
 SUM(LateFeeDueAmt) late_fee_due_tt ,
 SUM(TotDueAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2) )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' ) ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT COUNT(LoanNmb) sba_loan_no_tt ,
 SUM(LoanAmt) loan_amount_tt ,
 SUM(MoPymtAmt) monthly_pym_tt ,
 SUM(SBAFeeAmt) sba_fee_tt ,
 SUM(CSAFeeAmt) csa_fee_tt ,
 SUM(CDCFeeAmt) cdc_fee_tt ,
 SUM(DiffToEscrowAmt) diff_to_escrow_tt ,
 SUM(TotFeeDueAmt) total_fee_due_tt ,
 SUM(IntDueAmt) interest_due_tt ,
 SUM(PrinDueAmt) principal_due_tt ,
 SUM(LateFeeDueAmt) late_fee_due_tt ,
 SUM(TotDueAmt) total_due_tt ,
 SUM(PrinLeftAmt) principal_left_tt 
 FROM PortflTbl 
 WHERE ( CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum )
 AND ( REGEXP_LIKE(SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1), '^[[:digit:]]+$')
 OR LTRIM(CDCPortflStatCdCd) = '*'
 OR SUBSTR(LTRIM(CDCPortflStatCdCd), 1, 1) = '*' ) ;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


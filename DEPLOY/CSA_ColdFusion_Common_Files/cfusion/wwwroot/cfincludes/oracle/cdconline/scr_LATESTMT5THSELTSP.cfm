<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LateStmt5thSelTSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT MAX(StmtDt) StmtDt 
 FROM LateStmtTbl 
 WHERE CDCRegnCd = p_region_num
 AND CDCNmb = p_cdc_num
 AND EXTRACT(DAY FROM StmtDt) < 10 ;--cdc_late_statements_date_5th_r '02','308'
 --select * from LateStmtTbl 
 --WHERE CDCRegnCd = '02'
 --AND CDCNmb = '308'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


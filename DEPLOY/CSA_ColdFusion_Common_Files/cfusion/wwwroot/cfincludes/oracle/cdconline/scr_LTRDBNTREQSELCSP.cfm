<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LtrDbntREQSelCSP 
 (
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT a.CntctNm ,
 l.BorrNm ,
 r.LoanNmb ,
 r.SemiAnnDt 
 FROM RqstTbl r
 LEFT JOIN LoanTbl l ON r.LoanNmb = l.LoanNmb
 LEFT JOIN AdrTbl a ON l.CDCNmb = a.CDCNmb
 AND l.CDCRegnCd = a.CDCRegnCd
 WHERE r.CDCPortflStatCdCd = 'OP' ;---and r.gfd_rcvd='yes'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


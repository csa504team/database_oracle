<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE ChkDupPrepayAmtRqstSelTSP
 (
 p_SBALoanNmb IN VARCHAR2,
 p_RqstTyp IN NUMBER,
 p_PrepayDt IN DATE,
 p_IsDuplicate OUT VARCHAR2
 )
 AS
 v_temp NUMBER(1,0) := 0;
 BEGIN
 
 begin
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS (SELECT RQSTREF FROM RQSTTBL
 WHERE LOANNMB = p_SBALoanNmb
 AND CDCPORTFLSTATCDCD = 'OP'
 AND p_RqstTyp = 4
 AND RQSTCD = p_RqstTyp);
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 begin
 IF v_temp > 0 THEN
 begin
 p_IsDuplicate :='yes' ;
 end;
 ELSE
 begin
 p_IsDuplicate :='no' ;
 end;
 END IF;
 end;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


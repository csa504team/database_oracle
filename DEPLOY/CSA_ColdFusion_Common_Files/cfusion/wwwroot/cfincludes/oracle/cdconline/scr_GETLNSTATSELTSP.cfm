<!--- Saved 09/21/2018 18:20:39. --->
PROCEDURE GetLnStatSelTSP 
 (
 p_sba_loan_no IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT LoanStatCd 
 FROM LoanTbl 
 WHERE LoanNmb = p_sba_loan_no ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE ConfirmCopyGetLoansSelCSP 
 (
 p_region_num IN CHAR DEFAULT NULL ,
 p_cdc_num IN CHAR DEFAULT NULL ,
 p_funding_month IN NUMBER DEFAULT NULL ,
 p_funding_year IN NUMBER DEFAULT NULL ,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT con.LoanNmb ,
 con.BorrNm ,
 con.NoteAmt ,
 ln.RefiLoan 
 FROM CnfrmCopyTbl con
 LEFT JOIN LoanTbl ln ON con.LoanNmb = ln.LoanNmb
 WHERE con.CDCRegnCd = p_region_num
 AND con.CDCNmb = p_cdc_num
 AND EXTRACT(MONTH FROM con.FundingDt) = p_funding_month
 AND EXTRACT(YEAR FROM con.FundingDt) = p_funding_year ;--cdc_confirm_copy_get_loans_r '02','005',2,2011
 --select * from CDCCnfrmCopyTbl 
 --WHERE CDCRegn = '02'
 ---AND CDCNmb = '005'
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


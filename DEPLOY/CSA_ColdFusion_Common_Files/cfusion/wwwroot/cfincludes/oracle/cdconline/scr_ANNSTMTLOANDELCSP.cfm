<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AnnStmtLoanDelCSP 
 (
 p_sba_loan_number IN VARCHAR2,
 p_cdc_region IN VARCHAR2,
 p_cdc_num IN VARCHAR2,
 p_year IN NUMBER
 )
 AS
 v_TotalCount NUMBER(10,0);
 
 BEGIN
 
 --SQL Server BEGIN TRANSACTION;
 BEGIN
 -- OLD WAY
 --@year smallint,
 --@LoanNmb varchar(10) 
 DELETE AnnStmtLoanInfoTbl
 
 WHERE LoanNmb = p_sba_loan_number
 AND TaxYrNmb = p_year;
 DELETE AnnStmtRecipntTbl
 
 WHERE LoanNmb = p_sba_loan_number
 AND TaxYrNmb = p_year;
 UPDATE AnnStmtRecipntTbl
 SET LastUpdt = '1900-01-01'
 WHERE CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 AND TaxYrNmb = p_year;
 END;
 COMMIT;
 SELECT COUNT(1) 
 
 INTO v_TotalCount
 FROM AnnStmtRecipntTbl 
 WHERE CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 AND TaxYrNmb = p_year;
 IF v_TotalCount = 0 THEN
 DELETE AnnStmtRecipntTbl
 
 WHERE CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 AND TaxYrNmb = p_year;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


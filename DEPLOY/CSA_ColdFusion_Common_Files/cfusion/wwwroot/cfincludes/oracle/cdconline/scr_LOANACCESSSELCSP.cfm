<!--- Saved 09/21/2018 18:20:41. --->
PROCEDURE LoanAccessSelCSP 
 ----CDC_LoanAccess_R null,'04','493', '1015956009',0
 
 (
 p_SOD IN CHAR DEFAULT NULL ,
 p_RegionNum IN CHAR DEFAULT NULL ,
 p_CdcNum IN CHAR DEFAULT NULL ,
 p_GPNum IN CHAR DEFAULT NULL ,
 p_SBA IN NUMBER DEFAULT 0 ,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT 'Y' 
 FROM PortflTbl 
 WHERE LoanNmb = p_GPNum );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 IF p_SBA = 1 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE LoanNmb = p_GPNum ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM PortflTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 AND LoanNmb = p_GPNum ;
 ELSE
 OPEN p_SelCur1 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM PortflTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND LoanNmb = p_GPNum ;
 END IF;
 END IF;
 
 END;
 ELSE
 
 BEGIN
 --patch to skip PortflTbl
 --user cdc loan file
 IF p_SBA = 1 THEN
 OPEN p_SelCur2 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM LoanTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE LoanNmb = p_GPNum ;
 ELSE
 IF LENGTH(LTRIM(RTRIM(p_SOD))) > 0 THEN
 OPEN p_SelCur2 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM LoanTbl 
 
 --WHERE UserLevelRoleID = @SOD
 WHERE SUBSTR(UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
 AND LoanNmb = p_GPNum ;
 ELSE
 OPEN p_SelCur2 FOR
 SELECT UserLevelRoleID ,
 CDCRegnCd ,
 CDCNmb ,
 LoanNmb 
 FROM LoanTbl 
 WHERE CDCRegnCd = p_RegionNum
 AND CDCNmb = p_CdcNum
 AND LoanNmb = p_GPNum ;
 END IF;
 END IF;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


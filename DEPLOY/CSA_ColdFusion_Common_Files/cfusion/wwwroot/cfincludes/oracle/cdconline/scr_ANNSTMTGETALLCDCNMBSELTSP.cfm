<!--- Saved 09/21/2018 18:20:37. --->
PROCEDURE AnnStmtGetAllCDCNmbSelTSP 
 (
 p_cdc_region IN VARCHAR2,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 OPEN p_SelCur1 FOR
 SELECT DISTINCT CDCNmb 
 FROM AnnStmtRecipntTbl 
 WHERE LTRIM(RTRIM(CDCRegnCd)) = LTRIM(RTRIM(p_cdc_region))
 AND LTRIM(RTRIM(TaxYrNmb)) = LTRIM(RTRIM(p_tax_year))
 ORDER BY CDCNmb ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


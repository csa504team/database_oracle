<!--- Saved 09/21/2018 18:20:40. --->
PROCEDURE GetREQCdSelTSP 
 (
 p_level_id IN NUMBER,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 --- Colson can see all types
 IF p_level_id IN ( 7,9 )
 THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT RqstCd ,
 RqstCdDesc 
 FROM RqstCdDescTbl ;
 
 END;
 ELSE
 
 
 --- Clients can only see types 1,4,6,7,9
 /* types (1,6,7,9) is taken out for now, will be available for outside clients after 
 Colson users run a parallel test for this type of request */
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT RqstCd ,
 RqstCdDesc 
 FROM RqstCdDescTbl 
 WHERE RqstCd IN ( 1,4,6,7,9 )
 ;
 
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:38. --->
PROCEDURE AnnStmtValidateBef2013SelTSP 
 (
 p_sba_loan_no IN VARCHAR2,
 p_cdc_region IN CHAR,
 p_cdc_num IN CHAR,
 p_tax_year IN CHAR,
 p_SelCur1 OUT SYS_REFCURSOR
 )
 AS
 /*Check if loan number belongs to cdc number */
 v_val_record VARCHAR2(10);
 v_valid VARCHAR2(10);
 v_temp NUMBER(1, 0) := 0;
 
 BEGIN
 
 v_val_record := ' ' ;
 BEGIN
 SELECT 1 INTO v_temp
 FROM DUAL
 WHERE EXISTS ( SELECT * 
 FROM Tax1098PrintTbl 
 WHERE LoanNmb = p_sba_loan_no
 AND CDCRegnCd = p_cdc_region
 AND CDCNmb = p_cdc_num
 AND TaxYrNmb = p_tax_year );
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 THEN
 
 BEGIN
 v_valid := 'valid' ;
 
 END;
 ELSE
 
 BEGIN
 v_valid := 'invalid' ;
 
 END;
 END IF;
 OPEN p_SelCur1 FOR
 SELECT v_valid validateIt 
 FROM DUAL ;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 09/21/2018 18:20:46. --->
PROCEDURE RPTSemiAnDtDelCSP 
 --- exec cdc_rpt_sa_date '06/01/2006','07/01/2006','S','LoanNmb'
 
 (
 p_date_from IN DATE,
 p_date_to IN DATE,
 p_output_type IN CHAR DEFAULT 'S' ,---, -- S = Screen, F = File
 p_sort_order IN VARCHAR2,
 p_SelCur1 OUT SYS_REFCURSOR,
 p_SelCur2 OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 DELETE FROM TTTemp7Tbl;
 INSERT INTO TTTemp7Tbl(RqstRef,RqstCd,RqstTyp,PrepymtDt,SemiAnDt,SemiAnnDt,CDCRegnCd,CDCNmb,CDCNmb2,LoanNmb,SemiAnnPymtAmt,BalAmt,StatCd,TempGFD)
 SELECT r.RqstREF ,
 r.RqstCd ,
 ' ' ,
 NVL(r.PrepayDt, '') ,
 r.SemiAnnDt ,
 NVL(r.SemiAnnDt, '') ,
 NVL(S.CDCRegnCd, '') ,
 NVL(S.CDCNmb, '') ,
 S.CDCRegnCd || '-' || S.CDCNmb CDCNmb ,
 NVL(r.LoanNmb, '') ,
 NVL(S.SemiAnnPymtAmt, 0) ,
 NVL(S.PrepayPremAmt, 0) + NVL(S.DbentrBalAmt, 0) BalAmt ,
 NVL(CDCPortflStatCdCd, '') ,
 ' ' 
 FROM RqstTbl r
 LEFT JOIN LoanSaveTbl S ON r.RqstREF = S.RqstREF
 WHERE r.SemiAnnDt IS NOT NULL
 AND TRUNC(MONTHS_BETWEEN(TRUNC(r.SemiAnnDt), TRUNC(p_date_from))) >= 0
 AND TRUNC(MONTHS_BETWEEN(TRUNC(r.SemiAnnDt), TRUNC(p_date_to))) <= 0
 AND ( CDCPortflStatCdCd = 'OP'
 OR CDCPortflStatCdCd = 'CL' )
 ORDER BY r.SemiAnnDt,
 S.LoanNmb,
 S.CDCRegnCd,
 S.CDCNmb;
 --- wires received 
 MERGE INTO TTTemp7Tbl t
 USING (SELECT t.ROWID row_id, 'Y'
 FROM TTTemp7Tbl t
 JOIN WiresTbl W ON t.LoanNmb = W.LoanNmb
 AND TRUNC(t.PrepymtDt) = TRUNC(TO_DATE(W.PrepayDt))
 AND t.RqstCd = W.RqstCd ) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET TempGFD = 'Y';
 --- delete records with no wires
 DELETE TTTemp7Tbl
 
 WHERE TempGFD <> 'Y';
 ---get cdc number for scheduled but not closed prepayments
 MERGE INTO TTTemp7Tbl t
 USING (SELECT t.ROWID row_id, l.CDCRegnCd, l.CDCNmb, l.CDCRegnCd || '-' || l.CDCNmb AS pos_4
 FROM LoanTbl l
 JOIN TTTemp7Tbl t ON t.LoanNmb = l.LoanNmb
 AND t.CDCNmb = ' ' ) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET CDCRegnCd = src.CDCRegnCd,
 CDCNmb = src.CDCNmb,
 CDCNmb2 = pos_4;
 MERGE INTO TTTemp7Tbl t
 USING (SELECT t.ROWID row_id, CASE t.RqstCd
 WHEN 4 THEN 'Actual'
 WHEN 9 THEN 'Estimated'
 ELSE D.RjctCdDesc
 END AS RqstTyp
 FROM TTTemp7Tbl t
 JOIN RjctCdDescTbl D ON t.RqstCd = D.RjctCd ) src
 ON ( t.ROWID = src.row_id )
 WHEN MATCHED THEN UPDATE SET RqstTyp = src.RqstTyp;
 IF p_output_type = 'S' THEN
 
 BEGIN
 IF p_sort_order = 'LoanNmb' THEN
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTTemp7Tbl 
 ORDER BY SemiAnnDt,
 LoanNmb,
 CDCNmb ;
 
 END;
 ELSE
 
 BEGIN
 OPEN p_SelCur1 FOR
 SELECT * 
 FROM TTTemp7Tbl 
 ORDER BY SemiAnnDt,
 StatCd,
 LoanNmb,
 CDCNmb ;
 
 END;
 END IF;
 
 END;
 ELSE
 IF p_output_type = 'F' THEN
 
 BEGIN
 DELETE FROM TTFile4Tbl;
 -- max length of the records, try to keep it short
 -- since osql packs spaces after each line
 INSERT INTO TTFile4Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 ( SELECT 'CONTROL NUMBER' || CHR(9) || 'S/A DATE' || CHR(9) || 'CDC NUMBER' || CHR(9) || 'LOAN NUMBER' || CHR(9) || 'S/A PAYMENT' || CHR(9) || 'DEB.Bal + PREMIUM' || CHR(9) || 'STATUS' || CHR(9) || 'PREPAYMENT TYPE' || CHR(13) , USER, USER 
 FROM DUAL );
 INSERT INTO TTFile4Tbl
 ( TTLine,CreatUserID,LastUpdtUserID )
 SELECT RqstREF || CHR(9) || SemiAnDt || CHR(9) || CDCNmb || CHR(9) || LoanNmb || CHR(9) || SemiAnnPymtAmt || CHR(9) || BalAmt || CHR(9) || StatCd || CHR(9) || RqstTyp || CHR(13) , USER, USER 
 FROM TTTemp7Tbl 
 ORDER BY SemiAnDt,
 LoanNmb;
 OPEN p_SelCur2 FOR
 SELECT LTRIM(RTRIM(TTLine)) TTLine 
 FROM TTFile4Tbl 
 ORDER BY RefID ;
 
 END;
 END IF;
 END IF;
 
 EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


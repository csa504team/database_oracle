<!--- Saved 10/09/2018 11:29:04. --->
PROCEDURE CORETRANSATTRERRINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_TRANSERRID VARCHAR2:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_ATTRID VARCHAR2:=null
 ,p_TRANSACTV VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:10
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:45:10
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CORETRANSATTRERRTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.CORETRANSATTRERRTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize TRANSERRID from sequence TRANSERRIDSEQ
 Function NEXTVAL_TRANSERRIDSEQ return number is
 N number;
 begin
 select TRANSERRIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint CORETRANSATTRERRINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init CORETRANSATTRERRINSTSP',p_userid,4,
 logtxt1=>'CORETRANSATTRERRINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'CORETRANSATTRERRINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='TRANSID';
 cur_colvalue:=P_TRANSID;
 l_TRANSID:=P_TRANSID; cur_colname:='TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_TRANSERRID';
 cur_colvalue:=P_TRANSERRID;
 if P_TRANSERRID is null then
 
 new_rec.TRANSERRID:=NEXTVAL_TRANSERRIDSEQ();
 
 else 
 new_rec.TRANSERRID:=P_TRANSERRID;
 end if;
 cur_colname:='P_TRANSID';
 cur_colvalue:=P_TRANSID;
 if P_TRANSID is null then
 
 new_rec.TRANSID:=0;
 
 else 
 new_rec.TRANSID:=P_TRANSID;
 end if;
 cur_colname:='P_ATTRID';
 cur_colvalue:=P_ATTRID;
 if P_ATTRID is null then
 
 new_rec.ATTRID:=0;
 
 else 
 new_rec.ATTRID:=P_ATTRID;
 end if;
 cur_colname:='P_TRANSACTV';
 cur_colvalue:=P_TRANSACTV;
 if P_TRANSACTV is null then
 
 new_rec.TRANSACTV:=0;
 
 else 
 new_rec.TRANSACTV:=P_TRANSACTV;
 end if;
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 if P_TIMESTAMPFLD is null then
 
 new_rec.TIMESTAMPFLD:=jan_1_1900;
 
 else 
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(TRANSERRID)=('||new_rec.TRANSERRID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.CORETRANSATTRERRTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End CORETRANSATTRERRINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'CORETRANSATTRERRINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in CORETRANSATTRERRINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:45:10';
 ROLLBACK TO CORETRANSATTRERRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'CORETRANSATTRERRINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CORETRANSATTRERRINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:45:10';
 ROLLBACK TO CORETRANSATTRERRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'CORETRANSATTRERRINSTSP',force_log_entry=>TRUE);
 --
 END CORETRANSATTRERRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPMFGNTYUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPMFGNTYID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_OPENGNTYDT DATE:=null
 ,p_OPENGNTYTYP VARCHAR2:=null
 ,p_GNTYAMT VARCHAR2:=null
 ,p_STATCD VARCHAR2:=null
 ,p_GNTYCLS VARCHAR2:=null
 ,p_PRPCMNT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_PREPAYDT DATE:=null
 ,p_OPENGNTYDTCONVERTED DATE:=null
 ,p_OPENGNTYDTPREPAYDTDIFF VARCHAR2:=null
 ,p_GNTYIND VARCHAR2:=null
 ,p_SEMIANDTCONVERTED DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:38
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:46:38
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure PRPMFGNTYUPDTSP performs UPDATE on STGCSA.PRPMFGNTYTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPMFGNTYID
 for P_IDENTIFIER=1 qualification is on LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.PRPMFGNTYTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.PRPMFGNTYTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.PRPMFGNTYTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_PRPMFGNTYID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRPMFGNTYID';
 cur_colvalue:=P_PRPMFGNTYID;
 DBrec.PRPMFGNTYID:=P_PRPMFGNTYID; 
 end if;
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_OPENGNTYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYDT';
 cur_colvalue:=P_OPENGNTYDT;
 DBrec.OPENGNTYDT:=P_OPENGNTYDT; 
 end if;
 if P_OPENGNTYTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYTYP';
 cur_colvalue:=P_OPENGNTYTYP;
 DBrec.OPENGNTYTYP:=P_OPENGNTYTYP; 
 end if;
 if P_GNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYAMT';
 cur_colvalue:=P_GNTYAMT;
 DBrec.GNTYAMT:=P_GNTYAMT; 
 end if;
 if P_STATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_STATCD';
 cur_colvalue:=P_STATCD;
 DBrec.STATCD:=P_STATCD; 
 end if;
 if P_GNTYCLS is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYCLS';
 cur_colvalue:=P_GNTYCLS;
 DBrec.GNTYCLS:=P_GNTYCLS; 
 end if;
 if P_PRPCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRPCMNT';
 cur_colvalue:=P_PRPCMNT;
 DBrec.PRPCMNT:=P_PRPCMNT; 
 end if;
 if P_SEMIANDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 DBrec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_PREPAYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 DBrec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_OPENGNTYDTCONVERTED is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYDTCONVERTED';
 cur_colvalue:=P_OPENGNTYDTCONVERTED;
 DBrec.OPENGNTYDTCONVERTED:=P_OPENGNTYDTCONVERTED; 
 end if;
 if P_OPENGNTYDTPREPAYDTDIFF is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYDTPREPAYDTDIFF';
 cur_colvalue:=P_OPENGNTYDTPREPAYDTDIFF;
 DBrec.OPENGNTYDTPREPAYDTDIFF:=P_OPENGNTYDTPREPAYDTDIFF; 
 end if;
 if P_GNTYIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYIND';
 cur_colvalue:=P_GNTYIND;
 DBrec.GNTYIND:=P_GNTYIND; 
 end if;
 if P_SEMIANDTCONVERTED is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDTCONVERTED';
 cur_colvalue:=P_SEMIANDTCONVERTED;
 DBrec.SEMIANDTCONVERTED:=P_SEMIANDTCONVERTED; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPMFGNTYTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,PRPMFGNTYID=DBrec.PRPMFGNTYID
 ,LOANNMB=DBrec.LOANNMB
 ,OPENGNTYDT=DBrec.OPENGNTYDT
 ,OPENGNTYTYP=DBrec.OPENGNTYTYP
 ,GNTYAMT=DBrec.GNTYAMT
 ,STATCD=DBrec.STATCD
 ,GNTYCLS=DBrec.GNTYCLS
 ,PRPCMNT=DBrec.PRPCMNT
 ,SEMIANDT=DBrec.SEMIANDT
 ,PREPAYDT=DBrec.PREPAYDT
 ,OPENGNTYDTCONVERTED=DBrec.OPENGNTYDTCONVERTED
 ,OPENGNTYDTPREPAYDTDIFF=DBrec.OPENGNTYDTPREPAYDTDIFF
 ,GNTYIND=DBrec.GNTYIND
 ,SEMIANDTCONVERTED=DBrec.SEMIANDTCONVERTED
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.PRPMFGNTYTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFGNTYUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPMFGNTYUPDTSP',p_userid,4,
 logtxt1=>'PRPMFGNTYUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'PRPMFGNTYUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB; l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(PRPMFGNTYID)=('||P_PRPMFGNTYID||')';
 begin
 select rowid into rec_rowid from STGCSA.PRPMFGNTYTBL where 1=1
 and PRPMFGNTYID=P_PRPMFGNTYID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFGNTYTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 for r1 in (select rowid from STGCSA.PRPMFGNTYTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.PRPMFGNTYTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFGNTYTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFGNTYUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'PRPMFGNTYUPDTSP');
 else
 ROLLBACK TO PRPMFGNTYUPDTSP;
 p_errmsg:=p_errmsg||' in PRPMFGNTYUPDTSP build 2018-10-08 15:46:38';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFGNTYUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRPMFGNTYUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFGNTYUPDTSP build 2018-10-08 15:46:38';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFGNTYUPDTSP',force_log_entry=>true);
 --
 END PRPMFGNTYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


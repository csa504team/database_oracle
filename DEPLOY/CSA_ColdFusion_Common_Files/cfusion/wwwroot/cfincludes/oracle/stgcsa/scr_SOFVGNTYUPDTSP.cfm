<!--- Saved 10/09/2018 11:29:12. --->
PROCEDURE SOFVGNTYUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_GNTYDT DATE:=null
 ,p_GNTYTYP CHAR:=null
 ,p_GNTYGNTYAMT VARCHAR2:=null
 ,p_GNTYGNTYORGLDT DATE:=null
 ,p_GNTYSTATCD CHAR:=null
 ,p_GNTYCLSDT DATE:=null
 ,p_GNTYCREATDT DATE:=null
 ,p_GNTYLASTMAINTNDT DATE:=null
 ,p_GNTYCMNT VARCHAR2:=null
 ,p_GNTYCMNT2 VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:35
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:47:35
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure SOFVGNTYUPDTSP performs UPDATE on STGCSA.SOFVGNTYTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, GNTYDT, GNTYTYP
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.SOFVGNTYTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.SOFVGNTYTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.SOFVGNTYTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_GNTYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYDT';
 cur_colvalue:=P_GNTYDT;
 DBrec.GNTYDT:=P_GNTYDT; 
 end if;
 if P_GNTYTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYTYP';
 cur_colvalue:=P_GNTYTYP;
 DBrec.GNTYTYP:=P_GNTYTYP; 
 end if;
 if P_GNTYGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYGNTYAMT';
 cur_colvalue:=P_GNTYGNTYAMT;
 DBrec.GNTYGNTYAMT:=P_GNTYGNTYAMT; 
 end if;
 if P_GNTYGNTYORGLDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYGNTYORGLDT';
 cur_colvalue:=P_GNTYGNTYORGLDT;
 DBrec.GNTYGNTYORGLDT:=P_GNTYGNTYORGLDT; 
 end if;
 if P_GNTYSTATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYSTATCD';
 cur_colvalue:=P_GNTYSTATCD;
 DBrec.GNTYSTATCD:=P_GNTYSTATCD; 
 end if;
 if P_GNTYCLSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYCLSDT';
 cur_colvalue:=P_GNTYCLSDT;
 DBrec.GNTYCLSDT:=P_GNTYCLSDT; 
 end if;
 if P_GNTYCREATDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYCREATDT';
 cur_colvalue:=P_GNTYCREATDT;
 DBrec.GNTYCREATDT:=P_GNTYCREATDT; 
 end if;
 if P_GNTYLASTMAINTNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYLASTMAINTNDT';
 cur_colvalue:=P_GNTYLASTMAINTNDT;
 DBrec.GNTYLASTMAINTNDT:=P_GNTYLASTMAINTNDT; 
 end if;
 if P_GNTYCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYCMNT';
 cur_colvalue:=P_GNTYCMNT;
 DBrec.GNTYCMNT:=P_GNTYCMNT; 
 end if;
 if P_GNTYCMNT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYCMNT2';
 cur_colvalue:=P_GNTYCMNT2;
 DBrec.GNTYCMNT2:=P_GNTYCMNT2; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVGNTYTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,LOANNMB=DBrec.LOANNMB
 ,GNTYDT=DBrec.GNTYDT
 ,GNTYTYP=DBrec.GNTYTYP
 ,GNTYGNTYAMT=DBrec.GNTYGNTYAMT
 ,GNTYGNTYORGLDT=DBrec.GNTYGNTYORGLDT
 ,GNTYSTATCD=DBrec.GNTYSTATCD
 ,GNTYCLSDT=DBrec.GNTYCLSDT
 ,GNTYCREATDT=DBrec.GNTYCREATDT
 ,GNTYLASTMAINTNDT=DBrec.GNTYLASTMAINTNDT
 ,GNTYCMNT=DBrec.GNTYCMNT
 ,GNTYCMNT2=DBrec.GNTYCMNT2
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.SOFVGNTYTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVGNTYUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVGNTYUPDTSP',p_userid,4,
 logtxt1=>'SOFVGNTYUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'SOFVGNTYUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVGNTYTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')'||' and '||'(GNTYDT)=('||P_GNTYDT||')'||' and '||'(GNTYTYP)=('||P_GNTYTYP||')';
 begin
 select rowid into rec_rowid from STGCSA.SOFVGNTYTBL where 1=1
 and LOANNMB=P_LOANNMB and GNTYDT=P_GNTYDT and GNTYTYP=P_GNTYTYP;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.SOFVGNTYTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVGNTYUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'SOFVGNTYUPDTSP');
 else
 ROLLBACK TO SOFVGNTYUPDTSP;
 p_errmsg:=p_errmsg||' in SOFVGNTYUPDTSP build 2018-10-08 15:47:35';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'SOFVGNTYUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO SOFVGNTYUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVGNTYUPDTSP build 2018-10-08 15:47:35';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'SOFVGNTYUPDTSP',force_log_entry=>true);
 --
 END SOFVGNTYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


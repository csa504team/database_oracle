<!--- Saved 10/09/2018 11:29:02. --->
PROCEDURE CHRTOPNGNTYSALLUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CHRTOPNGNTYSALLID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_PREPAYDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_TOTOPENGNTYAMT VARCHAR2:=null
 ,p_ERROPENGNTYAMT VARCHAR2:=null
 ,p_REMAINDEROPENGNTYAMT VARCHAR2:=null
 ,p_OPENGNTYDIFFAMT VARCHAR2:=null
 ,p_PROOFAMT VARCHAR2:=null
 ,p_OPNGNTYSCMNT VARCHAR2:=null
 ,p_OPENGNTYCLSDT DATE:=null
 ,p_OPENGNTYPAIDDT DATE:=null
 ,p_TOBECLSFLAG VARCHAR2:=null
 ,p_TOBEPAIDFLAG VARCHAR2:=null
 ,p_GNTYAMT VARCHAR2:=null
 ,p_OPENGNTYVALIDATION VARCHAR2:=null
 ,p_UNALLOCAMT VARCHAR2:=null
 ,p_UPDTUNALLOCPORTOFGUARANTEAMT VARCHAR2:=null
 ,p_ACCRINTONDELNQLOANAMT VARCHAR2:=null
 ,p_UPDTACCRINTDELNQLOANAMT VARCHAR2:=null
 ,p_ACCRINTPAIDDT DATE:=null
 ,p_REPORTDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:32
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:44:33
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure CHRTOPNGNTYSALLUPDTSP performs UPDATE on STGCSA.CHRTOPNGNTYSALLTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CHRTOPNGNTYSALLID
 for P_IDENTIFIER=1 qualification is on LOANNMB
 for P_IDENTIFIER=2 qualification is on LOANNMB, GNTYAMT, REPORTDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.CHRTOPNGNTYSALLTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.CHRTOPNGNTYSALLTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.CHRTOPNGNTYSALLTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_CHRTOPNGNTYSALLID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CHRTOPNGNTYSALLID';
 cur_colvalue:=P_CHRTOPNGNTYSALLID;
 DBrec.CHRTOPNGNTYSALLID:=P_CHRTOPNGNTYSALLID; 
 end if;
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_PREPAYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 DBrec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_SEMIANDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 DBrec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_TOTOPENGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TOTOPENGNTYAMT';
 cur_colvalue:=P_TOTOPENGNTYAMT;
 DBrec.TOTOPENGNTYAMT:=P_TOTOPENGNTYAMT; 
 end if;
 if P_ERROPENGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ERROPENGNTYAMT';
 cur_colvalue:=P_ERROPENGNTYAMT;
 DBrec.ERROPENGNTYAMT:=P_ERROPENGNTYAMT; 
 end if;
 if P_REMAINDEROPENGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_REMAINDEROPENGNTYAMT';
 cur_colvalue:=P_REMAINDEROPENGNTYAMT;
 DBrec.REMAINDEROPENGNTYAMT:=P_REMAINDEROPENGNTYAMT; 
 end if;
 if P_OPENGNTYDIFFAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYDIFFAMT';
 cur_colvalue:=P_OPENGNTYDIFFAMT;
 DBrec.OPENGNTYDIFFAMT:=P_OPENGNTYDIFFAMT; 
 end if;
 if P_PROOFAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PROOFAMT';
 cur_colvalue:=P_PROOFAMT;
 DBrec.PROOFAMT:=P_PROOFAMT; 
 end if;
 if P_OPNGNTYSCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPNGNTYSCMNT';
 cur_colvalue:=P_OPNGNTYSCMNT;
 DBrec.OPNGNTYSCMNT:=P_OPNGNTYSCMNT; 
 end if;
 if P_OPENGNTYCLSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYCLSDT';
 cur_colvalue:=P_OPENGNTYCLSDT;
 DBrec.OPENGNTYCLSDT:=P_OPENGNTYCLSDT; 
 end if;
 if P_OPENGNTYPAIDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYPAIDDT';
 cur_colvalue:=P_OPENGNTYPAIDDT;
 DBrec.OPENGNTYPAIDDT:=P_OPENGNTYPAIDDT; 
 end if;
 if P_TOBECLSFLAG is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TOBECLSFLAG';
 cur_colvalue:=P_TOBECLSFLAG;
 DBrec.TOBECLSFLAG:=P_TOBECLSFLAG; 
 end if;
 if P_TOBEPAIDFLAG is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TOBEPAIDFLAG';
 cur_colvalue:=P_TOBEPAIDFLAG;
 DBrec.TOBEPAIDFLAG:=P_TOBEPAIDFLAG; 
 end if;
 if P_GNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GNTYAMT';
 cur_colvalue:=P_GNTYAMT;
 DBrec.GNTYAMT:=P_GNTYAMT; 
 end if;
 if P_OPENGNTYVALIDATION is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYVALIDATION';
 cur_colvalue:=P_OPENGNTYVALIDATION;
 DBrec.OPENGNTYVALIDATION:=P_OPENGNTYVALIDATION; 
 end if;
 if P_UNALLOCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UNALLOCAMT';
 cur_colvalue:=P_UNALLOCAMT;
 DBrec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_UPDTUNALLOCPORTOFGUARANTEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTUNALLOCPORTOFGUARANTEAMT';
 cur_colvalue:=P_UPDTUNALLOCPORTOFGUARANTEAMT;
 DBrec.UPDTUNALLOCPORTOFGUARANTEAMT:=P_UPDTUNALLOCPORTOFGUARANTEAMT; 
 end if;
 if P_ACCRINTONDELNQLOANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ACCRINTONDELNQLOANAMT';
 cur_colvalue:=P_ACCRINTONDELNQLOANAMT;
 DBrec.ACCRINTONDELNQLOANAMT:=P_ACCRINTONDELNQLOANAMT; 
 end if;
 if P_UPDTACCRINTDELNQLOANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTACCRINTDELNQLOANAMT';
 cur_colvalue:=P_UPDTACCRINTDELNQLOANAMT;
 DBrec.UPDTACCRINTDELNQLOANAMT:=P_UPDTACCRINTDELNQLOANAMT; 
 end if;
 if P_ACCRINTPAIDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ACCRINTPAIDDT';
 cur_colvalue:=P_ACCRINTPAIDDT;
 DBrec.ACCRINTPAIDDT:=P_ACCRINTPAIDDT; 
 end if;
 if P_REPORTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_REPORTDT';
 cur_colvalue:=P_REPORTDT;
 DBrec.REPORTDT:=P_REPORTDT; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.CHRTOPNGNTYSALLTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,CHRTOPNGNTYSALLID=DBrec.CHRTOPNGNTYSALLID
 ,LOANNMB=DBrec.LOANNMB
 ,PREPAYDT=DBrec.PREPAYDT
 ,SEMIANDT=DBrec.SEMIANDT
 ,TOTOPENGNTYAMT=DBrec.TOTOPENGNTYAMT
 ,ERROPENGNTYAMT=DBrec.ERROPENGNTYAMT
 ,REMAINDEROPENGNTYAMT=DBrec.REMAINDEROPENGNTYAMT
 ,OPENGNTYDIFFAMT=DBrec.OPENGNTYDIFFAMT
 ,PROOFAMT=DBrec.PROOFAMT
 ,OPNGNTYSCMNT=DBrec.OPNGNTYSCMNT
 ,OPENGNTYCLSDT=DBrec.OPENGNTYCLSDT
 ,OPENGNTYPAIDDT=DBrec.OPENGNTYPAIDDT
 ,TOBECLSFLAG=DBrec.TOBECLSFLAG
 ,TOBEPAIDFLAG=DBrec.TOBEPAIDFLAG
 ,GNTYAMT=DBrec.GNTYAMT
 ,OPENGNTYVALIDATION=DBrec.OPENGNTYVALIDATION
 ,UNALLOCAMT=DBrec.UNALLOCAMT
 ,UPDTUNALLOCPORTOFGUARANTEAMT=DBrec.UPDTUNALLOCPORTOFGUARANTEAMT
 ,ACCRINTONDELNQLOANAMT=DBrec.ACCRINTONDELNQLOANAMT
 ,UPDTACCRINTDELNQLOANAMT=DBrec.UPDTACCRINTDELNQLOANAMT
 ,ACCRINTPAIDDT=DBrec.ACCRINTPAIDDT
 ,REPORTDT=DBrec.REPORTDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.CHRTOPNGNTYSALLTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint CHRTOPNGNTYSALLUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init CHRTOPNGNTYSALLUPDTSP',p_userid,4,
 logtxt1=>'CHRTOPNGNTYSALLUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'CHRTOPNGNTYSALLUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(CHRTOPNGNTYSALLID)=('||P_CHRTOPNGNTYSALLID||')';
 begin
 select rowid into rec_rowid from STGCSA.CHRTOPNGNTYSALLTBL where 1=1
 and CHRTOPNGNTYSALLID=P_CHRTOPNGNTYSALLID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.CHRTOPNGNTYSALLTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 for r1 in (select rowid from STGCSA.CHRTOPNGNTYSALLTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.CHRTOPNGNTYSALLTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.CHRTOPNGNTYSALLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')'||' and '||'(REPORTDT)=('||P_REPORTDT||')'||' and '||'(GNTYAMT)=('||P_GNTYAMT||')';
 begin
 for r2 in (select rowid from STGCSA.CHRTOPNGNTYSALLTBL a where 1=1 
 and GNTYAMT=P_GNTYAMT
 and LOANNMB=P_LOANNMB
 and REPORTDT=P_REPORTDT
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.CHRTOPNGNTYSALLTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.CHRTOPNGNTYSALLTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End CHRTOPNGNTYSALLUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'CHRTOPNGNTYSALLUPDTSP');
 else
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errmsg:=p_errmsg||' in CHRTOPNGNTYSALLUPDTSP build 2018-10-08 15:44:32';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'CHRTOPNGNTYSALLUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO CHRTOPNGNTYSALLUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in CHRTOPNGNTYSALLUPDTSP build 2018-10-08 15:44:32';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'CHRTOPNGNTYSALLUPDTSP',force_log_entry=>true);
 --
 END CHRTOPNGNTYSALLUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


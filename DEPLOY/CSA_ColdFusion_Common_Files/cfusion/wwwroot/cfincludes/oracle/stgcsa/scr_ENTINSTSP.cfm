<!--- Saved 10/09/2018 11:29:04. --->
PROCEDURE ENTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ENTID VARCHAR2:=null
 ,p_ENTNM VARCHAR2:=null
 ,p_ENTTYP VARCHAR2:=null
 ,p_ENTMAILADRSTR1NM VARCHAR2:=null
 ,p_ENTMAILADRSTR2NM VARCHAR2:=null
 ,p_ENTMAILADRCTYNM VARCHAR2:=null
 ,p_ENTMAILADRSTCD CHAR:=null
 ,p_ENTMAILADRZIPCD CHAR:=null
 ,p_ENTMAILADRZIP4CD CHAR:=null
 ,p_ENTPHNNMB CHAR:=null
 ,p_CREATBY VARCHAR2:=null
 ,p_UPDTBY VARCHAR2:=null
 ,p_REGNCD CHAR:=null
 ,p_ENTNMB CHAR:=null
 ,p_DISTCD CHAR:=null
 ,p_STATCD VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:26
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:45:26
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ENTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.ENTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize ENTID from sequence ENTIDSEQ
 Function NEXTVAL_ENTIDSEQ return number is
 N number;
 begin
 select ENTIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint ENTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init ENTINSTSP',p_userid,4,
 logtxt1=>'ENTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'ENTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_ENTID';
 cur_colvalue:=P_ENTID;
 if P_ENTID is null then
 
 new_rec.ENTID:=NEXTVAL_ENTIDSEQ();
 
 else 
 new_rec.ENTID:=P_ENTID;
 end if;
 cur_colname:='P_ENTNM';
 cur_colvalue:=P_ENTNM;
 if P_ENTNM is null then
 
 new_rec.ENTNM:=' ';
 
 else 
 new_rec.ENTNM:=P_ENTNM;
 end if;
 cur_colname:='P_ENTTYP';
 cur_colvalue:=P_ENTTYP;
 if P_ENTTYP is null then
 
 new_rec.ENTTYP:=' ';
 
 else 
 new_rec.ENTTYP:=P_ENTTYP;
 end if;
 cur_colname:='P_ENTMAILADRSTR1NM';
 cur_colvalue:=P_ENTMAILADRSTR1NM;
 if P_ENTMAILADRSTR1NM is null then
 
 new_rec.ENTMAILADRSTR1NM:=' ';
 
 else 
 new_rec.ENTMAILADRSTR1NM:=P_ENTMAILADRSTR1NM;
 end if;
 cur_colname:='P_ENTMAILADRSTR2NM';
 cur_colvalue:=P_ENTMAILADRSTR2NM;
 if P_ENTMAILADRSTR2NM is null then
 
 new_rec.ENTMAILADRSTR2NM:=' ';
 
 else 
 new_rec.ENTMAILADRSTR2NM:=P_ENTMAILADRSTR2NM;
 end if;
 cur_colname:='P_ENTMAILADRCTYNM';
 cur_colvalue:=P_ENTMAILADRCTYNM;
 if P_ENTMAILADRCTYNM is null then
 
 new_rec.ENTMAILADRCTYNM:=' ';
 
 else 
 new_rec.ENTMAILADRCTYNM:=P_ENTMAILADRCTYNM;
 end if;
 cur_colname:='P_ENTMAILADRSTCD';
 cur_colvalue:=P_ENTMAILADRSTCD;
 if P_ENTMAILADRSTCD is null then
 
 new_rec.ENTMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.ENTMAILADRSTCD:=P_ENTMAILADRSTCD;
 end if;
 cur_colname:='P_ENTMAILADRZIPCD';
 cur_colvalue:=P_ENTMAILADRZIPCD;
 if P_ENTMAILADRZIPCD is null then
 
 new_rec.ENTMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.ENTMAILADRZIPCD:=P_ENTMAILADRZIPCD;
 end if;
 cur_colname:='P_ENTMAILADRZIP4CD';
 cur_colvalue:=P_ENTMAILADRZIP4CD;
 if P_ENTMAILADRZIP4CD is null then
 
 new_rec.ENTMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.ENTMAILADRZIP4CD:=P_ENTMAILADRZIP4CD;
 end if;
 cur_colname:='P_ENTPHNNMB';
 cur_colvalue:=P_ENTPHNNMB;
 if P_ENTPHNNMB is null then
 
 new_rec.ENTPHNNMB:=rpad(' ',10);
 
 else 
 new_rec.ENTPHNNMB:=P_ENTPHNNMB;
 end if;
 cur_colname:='P_CREATBY';
 cur_colvalue:=P_CREATBY;
 if P_CREATBY is null then
 
 new_rec.CREATBY:=0;
 
 else 
 new_rec.CREATBY:=P_CREATBY;
 end if;
 cur_colname:='P_UPDTBY';
 cur_colvalue:=P_UPDTBY;
 if P_UPDTBY is null then
 
 new_rec.UPDTBY:=0;
 
 else 
 new_rec.UPDTBY:=P_UPDTBY;
 end if;
 cur_colname:='P_REGNCD';
 cur_colvalue:=P_REGNCD;
 if P_REGNCD is null then
 
 new_rec.REGNCD:=rpad(' ',2);
 
 else 
 new_rec.REGNCD:=P_REGNCD;
 end if;
 cur_colname:='P_ENTNMB';
 cur_colvalue:=P_ENTNMB;
 if P_ENTNMB is null then
 
 new_rec.ENTNMB:=rpad(' ',4);
 
 else 
 new_rec.ENTNMB:=P_ENTNMB;
 end if;
 cur_colname:='P_DISTCD';
 cur_colvalue:=P_DISTCD;
 if P_DISTCD is null then
 
 new_rec.DISTCD:=rpad(' ',5);
 
 else 
 new_rec.DISTCD:=P_DISTCD;
 end if;
 cur_colname:='P_STATCD';
 cur_colvalue:=P_STATCD;
 if P_STATCD is null then
 
 new_rec.STATCD:=0;
 
 else 
 new_rec.STATCD:=P_STATCD;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(ENTID)=('||new_rec.ENTID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.ENTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End ENTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'ENTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in ENTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:45:26';
 ROLLBACK TO ENTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'ENTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in ENTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:45:26';
 ROLLBACK TO ENTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'ENTINSTSP',force_log_entry=>TRUE);
 --
 END ENTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


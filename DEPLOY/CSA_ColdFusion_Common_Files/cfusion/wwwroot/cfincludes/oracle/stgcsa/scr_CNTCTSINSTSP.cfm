<!--- Saved 10/09/2018 11:29:03. --->
PROCEDURE CNTCTSINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCNMB CHAR:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_LASTNMCNTCT VARCHAR2:=null
 ,p_CNTCTMAINPHNNMB CHAR:=null
 ,p_CNTCTSERVICINGPHNNMB CHAR:=null
 ,p_CNTCTFUNDINGPHNNMB CHAR:=null
 ,p_BOUNCEDCHK VARCHAR2:=null
 ,p_MAILDOCPOC VARCHAR2:=null
 ,p_PROBLEMLOANMTHDOFCNTCT VARCHAR2:=null
 ,p_BUSEMAILADR VARCHAR2:=null
 ,p_CNTCTBUSPHNNMB VARCHAR2:=null
 ,p_CNTCTBUSFAXNMB VARCHAR2:=null
 ,p_CNTCTCDC VARCHAR2:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTSID VARCHAR2:=null
 ,p_MAINCNTCT VARCHAR2:=null
 ,p_SERVICINGCNTCTNM VARCHAR2:=null
 ,p_FUNDINGCNTCT VARCHAR2:=null
 ,p_CDCID VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:44
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:44
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CNTCTSTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.CNTCTSTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize CNTCTSID from sequence CNTCTSIDSEQ
 Function NEXTVAL_CNTCTSIDSEQ return number is
 N number;
 begin
 select CNTCTSIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint CNTCTSINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init CNTCTSINSTSP',p_userid,4,
 logtxt1=>'CNTCTSINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'CNTCTSINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CDCNMB';
 cur_colvalue:=P_CDCNMB;
 if P_CDCNMB is null then
 
 new_rec.CDCNMB:=rpad(' ',8);
 
 else 
 new_rec.CDCNMB:=P_CDCNMB;
 end if;
 cur_colname:='P_CNTCTFIRSTNM';
 cur_colvalue:=P_CNTCTFIRSTNM;
 if P_CNTCTFIRSTNM is null then
 
 new_rec.CNTCTFIRSTNM:=' ';
 
 else 
 new_rec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM;
 end if;
 cur_colname:='P_LASTNMCNTCT';
 cur_colvalue:=P_LASTNMCNTCT;
 if P_LASTNMCNTCT is null then
 
 new_rec.LASTNMCNTCT:=' ';
 
 else 
 new_rec.LASTNMCNTCT:=P_LASTNMCNTCT;
 end if;
 cur_colname:='P_CNTCTMAINPHNNMB';
 cur_colvalue:=P_CNTCTMAINPHNNMB;
 if P_CNTCTMAINPHNNMB is null then
 
 new_rec.CNTCTMAINPHNNMB:=rpad(' ',1);
 
 else 
 new_rec.CNTCTMAINPHNNMB:=P_CNTCTMAINPHNNMB;
 end if;
 cur_colname:='P_CNTCTSERVICINGPHNNMB';
 cur_colvalue:=P_CNTCTSERVICINGPHNNMB;
 if P_CNTCTSERVICINGPHNNMB is null then
 
 new_rec.CNTCTSERVICINGPHNNMB:=rpad(' ',1);
 
 else 
 new_rec.CNTCTSERVICINGPHNNMB:=P_CNTCTSERVICINGPHNNMB;
 end if;
 cur_colname:='P_CNTCTFUNDINGPHNNMB';
 cur_colvalue:=P_CNTCTFUNDINGPHNNMB;
 if P_CNTCTFUNDINGPHNNMB is null then
 
 new_rec.CNTCTFUNDINGPHNNMB:=rpad(' ',1);
 
 else 
 new_rec.CNTCTFUNDINGPHNNMB:=P_CNTCTFUNDINGPHNNMB;
 end if;
 cur_colname:='P_BOUNCEDCHK';
 cur_colvalue:=P_BOUNCEDCHK;
 if P_BOUNCEDCHK is null then
 
 new_rec.BOUNCEDCHK:=' ';
 
 else 
 new_rec.BOUNCEDCHK:=P_BOUNCEDCHK;
 end if;
 cur_colname:='P_MAILDOCPOC';
 cur_colvalue:=P_MAILDOCPOC;
 if P_MAILDOCPOC is null then
 
 new_rec.MAILDOCPOC:=' ';
 
 else 
 new_rec.MAILDOCPOC:=P_MAILDOCPOC;
 end if;
 cur_colname:='P_PROBLEMLOANMTHDOFCNTCT';
 cur_colvalue:=P_PROBLEMLOANMTHDOFCNTCT;
 if P_PROBLEMLOANMTHDOFCNTCT is null then
 
 new_rec.PROBLEMLOANMTHDOFCNTCT:=' ';
 
 else 
 new_rec.PROBLEMLOANMTHDOFCNTCT:=P_PROBLEMLOANMTHDOFCNTCT;
 end if;
 cur_colname:='P_BUSEMAILADR';
 cur_colvalue:=P_BUSEMAILADR;
 if P_BUSEMAILADR is null then
 
 new_rec.BUSEMAILADR:=' ';
 
 else 
 new_rec.BUSEMAILADR:=P_BUSEMAILADR;
 end if;
 cur_colname:='P_CNTCTBUSPHNNMB';
 cur_colvalue:=P_CNTCTBUSPHNNMB;
 if P_CNTCTBUSPHNNMB is null then
 
 new_rec.CNTCTBUSPHNNMB:=' ';
 
 else 
 new_rec.CNTCTBUSPHNNMB:=P_CNTCTBUSPHNNMB;
 end if;
 cur_colname:='P_CNTCTBUSFAXNMB';
 cur_colvalue:=P_CNTCTBUSFAXNMB;
 if P_CNTCTBUSFAXNMB is null then
 
 new_rec.CNTCTBUSFAXNMB:=' ';
 
 else 
 new_rec.CNTCTBUSFAXNMB:=P_CNTCTBUSFAXNMB;
 end if;
 cur_colname:='P_CNTCTCDC';
 cur_colvalue:=P_CNTCTCDC;
 if P_CNTCTCDC is null then
 
 new_rec.CNTCTCDC:=' ';
 
 else 
 new_rec.CNTCTCDC:=P_CNTCTCDC;
 end if;
 cur_colname:='P_CNTCTMAILADRSTR1NM';
 cur_colvalue:=P_CNTCTMAILADRSTR1NM;
 if P_CNTCTMAILADRSTR1NM is null then
 
 new_rec.CNTCTMAILADRSTR1NM:=' ';
 
 else 
 new_rec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM;
 end if;
 cur_colname:='P_CNTCTMAILADRSTR2NM';
 cur_colvalue:=P_CNTCTMAILADRSTR2NM;
 if P_CNTCTMAILADRSTR2NM is null then
 
 new_rec.CNTCTMAILADRSTR2NM:=' ';
 
 else 
 new_rec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM;
 end if;
 cur_colname:='P_CNTCTMAILADRCTYNM';
 cur_colvalue:=P_CNTCTMAILADRCTYNM;
 if P_CNTCTMAILADRCTYNM is null then
 
 new_rec.CNTCTMAILADRCTYNM:=' ';
 
 else 
 new_rec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM;
 end if;
 cur_colname:='P_CNTCTMAILADRSTCD';
 cur_colvalue:=P_CNTCTMAILADRSTCD;
 if P_CNTCTMAILADRSTCD is null then
 
 new_rec.CNTCTMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD;
 end if;
 cur_colname:='P_CNTCTMAILADRZIPCD';
 cur_colvalue:=P_CNTCTMAILADRZIPCD;
 if P_CNTCTMAILADRZIPCD is null then
 
 new_rec.CNTCTMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD;
 end if;
 cur_colname:='P_CNTCTMAILADRZIP4CD';
 cur_colvalue:=P_CNTCTMAILADRZIP4CD;
 if P_CNTCTMAILADRZIP4CD is null then
 
 new_rec.CNTCTMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD;
 end if;
 cur_colname:='P_CNTCTSID';
 cur_colvalue:=P_CNTCTSID;
 if P_CNTCTSID is null then
 
 new_rec.CNTCTSID:=NEXTVAL_CNTCTSIDSEQ();
 
 else 
 new_rec.CNTCTSID:=P_CNTCTSID;
 end if;
 cur_colname:='P_MAINCNTCT';
 cur_colvalue:=P_MAINCNTCT;
 if P_MAINCNTCT is null then
 
 new_rec.MAINCNTCT:=' ';
 
 else 
 new_rec.MAINCNTCT:=P_MAINCNTCT;
 end if;
 cur_colname:='P_SERVICINGCNTCTNM';
 cur_colvalue:=P_SERVICINGCNTCTNM;
 if P_SERVICINGCNTCTNM is null then
 
 new_rec.SERVICINGCNTCTNM:=' ';
 
 else 
 new_rec.SERVICINGCNTCTNM:=P_SERVICINGCNTCTNM;
 end if;
 cur_colname:='P_FUNDINGCNTCT';
 cur_colvalue:=P_FUNDINGCNTCT;
 if P_FUNDINGCNTCT is null then
 
 new_rec.FUNDINGCNTCT:=' ';
 
 else 
 new_rec.FUNDINGCNTCT:=P_FUNDINGCNTCT;
 end if;
 cur_colname:='P_CDCID';
 cur_colvalue:=P_CDCID;
 if P_CDCID is null then
 
 new_rec.CDCID:=0;
 
 else 
 new_rec.CDCID:=P_CDCID;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CNTCTSID)=('||new_rec.CNTCTSID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.CNTCTSTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End CNTCTSINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'CNTCTSINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in CNTCTSINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:44';
 ROLLBACK TO CNTCTSINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'CNTCTSINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CNTCTSINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:44';
 ROLLBACK TO CNTCTSINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'CNTCTSINSTSP',force_log_entry=>TRUE);
 --
 END CNTCTSINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


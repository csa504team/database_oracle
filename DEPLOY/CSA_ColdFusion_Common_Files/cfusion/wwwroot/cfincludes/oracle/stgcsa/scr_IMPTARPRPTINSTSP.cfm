<!--- Saved 10/09/2018 11:29:05. --->
PROCEDURE IMPTARPRPTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_IMPTARPRPTID VARCHAR2:=null
 ,p_STARTDT DATE:=null
 ,p_ENDDT DATE:=null
 ,p_BNKID VARCHAR2:=null
 ,p_BNKNM VARCHAR2:=null
 ,p_BNKSTCD CHAR:=null
 ,p_ACCTNMB VARCHAR2:=null
 ,p_ACCTTYP VARCHAR2:=null
 ,p_ACCTNM VARCHAR2:=null
 ,p_CURCY VARCHAR2:=null
 ,p_SCTNRPTNM VARCHAR2:=null
 ,p_TRANSTYP VARCHAR2:=null
 ,p_SRLNMBREFNMB VARCHAR2:=null
 ,p_CHKAMT VARCHAR2:=null
 ,p_ISSDT DATE:=null
 ,p_POSTEDDT DATE:=null
 ,p_STOPDT DATE:=null
 ,p_RLSEDT DATE:=null
 ,p_ASOFDT DATE:=null
 ,p_RVRSDDT DATE:=null
 ,p_DRCRDTCD VARCHAR2:=null
 ,p_OPTINFOTRANDESC VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:12
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:46:12
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.IMPTARPRPTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.IMPTARPRPTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize IMPTARPRPTID from sequence IMPTARPRPTIDSEQ
 Function NEXTVAL_IMPTARPRPTIDSEQ return number is
 N number;
 begin
 select IMPTARPRPTIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint IMPTARPRPTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init IMPTARPRPTINSTSP',p_userid,4,
 logtxt1=>'IMPTARPRPTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'IMPTARPRPTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_IMPTARPRPTID';
 cur_colvalue:=P_IMPTARPRPTID;
 if P_IMPTARPRPTID is null then
 
 new_rec.IMPTARPRPTID:=NEXTVAL_IMPTARPRPTIDSEQ();
 
 else 
 new_rec.IMPTARPRPTID:=P_IMPTARPRPTID;
 end if;
 cur_colname:='P_STARTDT';
 cur_colvalue:=P_STARTDT;
 if P_STARTDT is null then
 
 new_rec.STARTDT:=jan_1_1900;
 
 else 
 new_rec.STARTDT:=P_STARTDT;
 end if;
 cur_colname:='P_ENDDT';
 cur_colvalue:=P_ENDDT;
 if P_ENDDT is null then
 
 new_rec.ENDDT:=jan_1_1900;
 
 else 
 new_rec.ENDDT:=P_ENDDT;
 end if;
 cur_colname:='P_BNKID';
 cur_colvalue:=P_BNKID;
 if P_BNKID is null then
 
 new_rec.BNKID:=' ';
 
 else 
 new_rec.BNKID:=P_BNKID;
 end if;
 cur_colname:='P_BNKNM';
 cur_colvalue:=P_BNKNM;
 if P_BNKNM is null then
 
 new_rec.BNKNM:=' ';
 
 else 
 new_rec.BNKNM:=P_BNKNM;
 end if;
 cur_colname:='P_BNKSTCD';
 cur_colvalue:=P_BNKSTCD;
 if P_BNKSTCD is null then
 
 new_rec.BNKSTCD:=rpad(' ',2);
 
 else 
 new_rec.BNKSTCD:=P_BNKSTCD;
 end if;
 cur_colname:='P_ACCTNMB';
 cur_colvalue:=P_ACCTNMB;
 if P_ACCTNMB is null then
 
 new_rec.ACCTNMB:=' ';
 
 else 
 new_rec.ACCTNMB:=P_ACCTNMB;
 end if;
 cur_colname:='P_ACCTTYP';
 cur_colvalue:=P_ACCTTYP;
 if P_ACCTTYP is null then
 
 new_rec.ACCTTYP:=' ';
 
 else 
 new_rec.ACCTTYP:=P_ACCTTYP;
 end if;
 cur_colname:='P_ACCTNM';
 cur_colvalue:=P_ACCTNM;
 if P_ACCTNM is null then
 
 new_rec.ACCTNM:=' ';
 
 else 
 new_rec.ACCTNM:=P_ACCTNM;
 end if;
 cur_colname:='P_CURCY';
 cur_colvalue:=P_CURCY;
 if P_CURCY is null then
 
 new_rec.CURCY:=' ';
 
 else 
 new_rec.CURCY:=P_CURCY;
 end if;
 cur_colname:='P_SCTNRPTNM';
 cur_colvalue:=P_SCTNRPTNM;
 if P_SCTNRPTNM is null then
 
 new_rec.SCTNRPTNM:=' ';
 
 else 
 new_rec.SCTNRPTNM:=P_SCTNRPTNM;
 end if;
 cur_colname:='P_TRANSTYP';
 cur_colvalue:=P_TRANSTYP;
 if P_TRANSTYP is null then
 
 new_rec.TRANSTYP:=' ';
 
 else 
 new_rec.TRANSTYP:=P_TRANSTYP;
 end if;
 cur_colname:='P_SRLNMBREFNMB';
 cur_colvalue:=P_SRLNMBREFNMB;
 if P_SRLNMBREFNMB is null then
 
 new_rec.SRLNMBREFNMB:=' ';
 
 else 
 new_rec.SRLNMBREFNMB:=P_SRLNMBREFNMB;
 end if;
 cur_colname:='P_CHKAMT';
 cur_colvalue:=P_CHKAMT;
 if P_CHKAMT is null then
 
 new_rec.CHKAMT:=0;
 
 else 
 new_rec.CHKAMT:=P_CHKAMT;
 end if;
 cur_colname:='P_ISSDT';
 cur_colvalue:=P_ISSDT;
 if P_ISSDT is null then
 
 new_rec.ISSDT:=jan_1_1900;
 
 else 
 new_rec.ISSDT:=P_ISSDT;
 end if;
 cur_colname:='P_POSTEDDT';
 cur_colvalue:=P_POSTEDDT;
 if P_POSTEDDT is null then
 
 new_rec.POSTEDDT:=jan_1_1900;
 
 else 
 new_rec.POSTEDDT:=P_POSTEDDT;
 end if;
 cur_colname:='P_STOPDT';
 cur_colvalue:=P_STOPDT;
 if P_STOPDT is null then
 
 new_rec.STOPDT:=jan_1_1900;
 
 else 
 new_rec.STOPDT:=P_STOPDT;
 end if;
 cur_colname:='P_RLSEDT';
 cur_colvalue:=P_RLSEDT;
 if P_RLSEDT is null then
 
 new_rec.RLSEDT:=jan_1_1900;
 
 else 
 new_rec.RLSEDT:=P_RLSEDT;
 end if;
 cur_colname:='P_ASOFDT';
 cur_colvalue:=P_ASOFDT;
 if P_ASOFDT is null then
 
 new_rec.ASOFDT:=jan_1_1900;
 
 else 
 new_rec.ASOFDT:=P_ASOFDT;
 end if;
 cur_colname:='P_RVRSDDT';
 cur_colvalue:=P_RVRSDDT;
 if P_RVRSDDT is null then
 
 new_rec.RVRSDDT:=jan_1_1900;
 
 else 
 new_rec.RVRSDDT:=P_RVRSDDT;
 end if;
 cur_colname:='P_DRCRDTCD';
 cur_colvalue:=P_DRCRDTCD;
 if P_DRCRDTCD is null then
 
 new_rec.DRCRDTCD:=' ';
 
 else 
 new_rec.DRCRDTCD:=P_DRCRDTCD;
 end if;
 cur_colname:='P_OPTINFOTRANDESC';
 cur_colvalue:=P_OPTINFOTRANDESC;
 if P_OPTINFOTRANDESC is null then
 
 new_rec.OPTINFOTRANDESC:=' ';
 
 else 
 new_rec.OPTINFOTRANDESC:=P_OPTINFOTRANDESC;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(IMPTARPRPTID)=('||new_rec.IMPTARPRPTID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.IMPTARPRPTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End IMPTARPRPTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'IMPTARPRPTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in IMPTARPRPTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:46:12';
 ROLLBACK TO IMPTARPRPTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'IMPTARPRPTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in IMPTARPRPTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:46:12';
 ROLLBACK TO IMPTARPRPTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'IMPTARPRPTINSTSP',force_log_entry=>TRUE);
 --
 END IMPTARPRPTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:04. --->
PROCEDURE GENAPPVUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_APPVID VARCHAR2:=null
 ,p_APPVTYPID VARCHAR2:=null
 ,p_ATTCHLOC VARCHAR2:=null
 ,p_APPVDT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_ATTCHNM VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:28
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:45:28
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure GENAPPVUPDTSP performs UPDATE on STGCSA.GENAPPVTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: APPVID
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.GENAPPVTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.GENAPPVTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.GENAPPVTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_APPVID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_APPVID';
 cur_colvalue:=P_APPVID;
 DBrec.APPVID:=P_APPVID; 
 end if;
 if P_APPVTYPID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_APPVTYPID';
 cur_colvalue:=P_APPVTYPID;
 DBrec.APPVTYPID:=P_APPVTYPID; 
 end if;
 if P_ATTCHLOC is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ATTCHLOC';
 cur_colvalue:=P_ATTCHLOC;
 DBrec.ATTCHLOC:=P_ATTCHLOC; 
 end if;
 if P_APPVDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_APPVDT';
 cur_colvalue:=P_APPVDT;
 DBrec.APPVDT:=P_APPVDT; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 if P_ATTCHNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ATTCHNM';
 cur_colvalue:=P_ATTCHNM;
 DBrec.ATTCHNM:=P_ATTCHNM; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENAPPVTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,APPVID=DBrec.APPVID
 ,APPVTYPID=DBrec.APPVTYPID
 ,ATTCHLOC=DBrec.ATTCHLOC
 ,APPVDT=DBrec.APPVDT
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 ,ATTCHNM=DBrec.ATTCHNM
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.GENAPPVTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENAPPVUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init GENAPPVUPDTSP',p_userid,4,
 logtxt1=>'GENAPPVUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'GENAPPVUPDTSP');
 --
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(APPVID)=('||P_APPVID||')';
 begin
 select rowid into rec_rowid from STGCSA.GENAPPVTBL where 1=1
 and APPVID=P_APPVID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.GENAPPVTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENAPPVUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'GENAPPVUPDTSP');
 else
 ROLLBACK TO GENAPPVUPDTSP;
 p_errmsg:=p_errmsg||' in GENAPPVUPDTSP build 2018-10-08 15:45:28';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'GENAPPVUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO GENAPPVUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENAPPVUPDTSP build 2018-10-08 15:45:28';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'GENAPPVUPDTSP',force_log_entry=>true);
 --
 END GENAPPVUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


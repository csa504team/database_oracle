<!--- Saved 10/09/2018 11:29:14. --->
PROCEDURE SOFVOTRNINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_ONLNTRANSTRANSDT TIMESTAMP:=null
 ,p_ONLNTRANSTRMNLID CHAR:=null
 ,p_ONLNTRANSOPRTRINITIAL CHAR:=null
 ,p_ONLNTRANSFILENM CHAR:=null
 ,p_ONLNTRANSSCN CHAR:=null
 ,p_ONLNTRANSFIL10 CHAR:=null
 ,p_ONLNTRANSDETPYMTDT DATE:=null
 ,p_ONLNTRANSDTLREST CHAR:=null
 ,p_ONLNTRANSDTLREST5 CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:56
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:47:56
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVOTRNTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVOTRNTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- vars for checking non-defaultable columns
 onespace varchar2(10):=' '; 
 ndfltmsg varchar2(300);
 v_errfnd boolean:=false;
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVOTRNINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVOTRNINSTSP',p_userid,4,
 logtxt1=>'SOFVOTRNINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVOTRNINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVOTRNTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 raise_application_error(-20001,cur_colname||' may not be null');
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_ONLNTRANSTRANSDT';
 cur_colvalue:=P_ONLNTRANSTRANSDT;
 if P_ONLNTRANSTRANSDT is null then
 
 new_rec.ONLNTRANSTRANSDT:=jan_1_1900;
 
 else 
 new_rec.ONLNTRANSTRANSDT:=P_ONLNTRANSTRANSDT;
 end if;
 cur_colname:='P_ONLNTRANSTRMNLID';
 cur_colvalue:=P_ONLNTRANSTRMNLID;
 if P_ONLNTRANSTRMNLID is null then
 
 new_rec.ONLNTRANSTRMNLID:=rpad(' ',4);
 
 else 
 new_rec.ONLNTRANSTRMNLID:=P_ONLNTRANSTRMNLID;
 end if;
 cur_colname:='P_ONLNTRANSOPRTRINITIAL';
 cur_colvalue:=P_ONLNTRANSOPRTRINITIAL;
 if P_ONLNTRANSOPRTRINITIAL is null then
 
 new_rec.ONLNTRANSOPRTRINITIAL:=rpad(' ',4);
 
 else 
 new_rec.ONLNTRANSOPRTRINITIAL:=P_ONLNTRANSOPRTRINITIAL;
 end if;
 cur_colname:='P_ONLNTRANSFILENM';
 cur_colvalue:=P_ONLNTRANSFILENM;
 if P_ONLNTRANSFILENM is null then
 
 new_rec.ONLNTRANSFILENM:=rpad(' ',8);
 
 else 
 new_rec.ONLNTRANSFILENM:=P_ONLNTRANSFILENM;
 end if;
 cur_colname:='P_ONLNTRANSSCN';
 cur_colvalue:=P_ONLNTRANSSCN;
 if P_ONLNTRANSSCN is null then
 
 new_rec.ONLNTRANSSCN:=rpad(' ',8);
 
 else 
 new_rec.ONLNTRANSSCN:=P_ONLNTRANSSCN;
 end if;
 cur_colname:='P_ONLNTRANSFIL10';
 cur_colvalue:=P_ONLNTRANSFIL10;
 if P_ONLNTRANSFIL10 is null then
 
 new_rec.ONLNTRANSFIL10:=rpad(' ',10);
 
 else 
 new_rec.ONLNTRANSFIL10:=P_ONLNTRANSFIL10;
 end if;
 cur_colname:='P_ONLNTRANSDETPYMTDT';
 cur_colvalue:=P_ONLNTRANSDETPYMTDT;
 if P_ONLNTRANSDETPYMTDT is null then
 
 new_rec.ONLNTRANSDETPYMTDT:=jan_1_1900;
 
 else 
 new_rec.ONLNTRANSDETPYMTDT:=P_ONLNTRANSDETPYMTDT;
 end if;
 cur_colname:='P_ONLNTRANSDTLREST';
 cur_colvalue:=P_ONLNTRANSDTLREST;
 if P_ONLNTRANSDTLREST is null then
 
 new_rec.ONLNTRANSDTLREST:=rpad(' ',9);
 
 else 
 new_rec.ONLNTRANSDTLREST:=P_ONLNTRANSDTLREST;
 end if;
 cur_colname:='P_ONLNTRANSDTLREST5';
 cur_colvalue:=P_ONLNTRANSDTLREST5;
 if P_ONLNTRANSDTLREST5 is null then
 
 new_rec.ONLNTRANSDTLREST5:=rpad(' ',5);
 
 else 
 new_rec.ONLNTRANSDTLREST5:=P_ONLNTRANSDTLREST5;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')'||' and '||'(ONLNTRANSTRANSDT)=('||new_rec.ONLNTRANSTRANSDT||')'||' and '||'(ONLNTRANSTRMNLID)=('||new_rec.ONLNTRANSTRMNLID||')'||' and '||'(ONLNTRANSOPRTRINITIAL)=('||new_rec.ONLNTRANSOPRTRINITIAL||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVOTRNTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVOTRNINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVOTRNINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVOTRNINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:47:56';
 ROLLBACK TO SOFVOTRNINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVOTRNINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVOTRNINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:47:56';
 ROLLBACK TO SOFVOTRNINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVOTRNINSTSP',force_log_entry=>TRUE);
 --
 END SOFVOTRNINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


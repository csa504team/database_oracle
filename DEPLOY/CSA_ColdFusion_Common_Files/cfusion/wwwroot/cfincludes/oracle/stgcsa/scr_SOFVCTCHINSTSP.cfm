<!--- Saved 10/09/2018 11:29:12. --->
PROCEDURE SOFVCTCHINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CATCHUPPLANSBANMB CHAR:=null
 ,p_CATCHUPPLANSTRTDT1 DATE:=null
 ,p_CATCHUPPLANENDDT1 DATE:=null
 ,p_CATCHUPPLANRQ1AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT2 DATE:=null
 ,p_CATCHUPPLANENDDT2 DATE:=null
 ,p_CATCHUPPLANRQ2AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT3 DATE:=null
 ,p_CATCHUPPLANENDDT3 DATE:=null
 ,p_CATCHUPPLANRQ3AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT4 DATE:=null
 ,p_CATCHUPPLANENDDT4 DATE:=null
 ,p_CATCHUPPLANRQ4AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT5 DATE:=null
 ,p_CATCHUPPLANENDDT5 DATE:=null
 ,p_CATCHUPPLANRQ5AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT6 DATE:=null
 ,p_CATCHUPPLANENDDT6 DATE:=null
 ,p_CATCHUPPLANRQ6AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT7 DATE:=null
 ,p_CATCHUPPLANENDDT7 DATE:=null
 ,p_CATCHUPPLANRQ7AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT8 DATE:=null
 ,p_CATCHUPPLANENDDT8 DATE:=null
 ,p_CATCHUPPLANRQ8AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT9 DATE:=null
 ,p_CATCHUPPLANENDDT9 DATE:=null
 ,p_CATCHUPPLANRQ9AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT10 DATE:=null
 ,p_CATCHUPPLANENDDT10 DATE:=null
 ,p_CATCHUPPLANRQ10AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT11 DATE:=null
 ,p_CATCHUPPLANENDDT11 DATE:=null
 ,p_CATCHUPPLANRQ11AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT12 DATE:=null
 ,p_CATCHUPPLANENDDT12 DATE:=null
 ,p_CATCHUPPLANRQ12AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT13 DATE:=null
 ,p_CATCHUPPLANENDDT13 DATE:=null
 ,p_CATCHUPPLANRQ13AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT14 DATE:=null
 ,p_CATCHUPPLANENDDT14 DATE:=null
 ,p_CATCHUPPLANRQ14AMT VARCHAR2:=null
 ,p_CATCHUPPLANSTRTDT15 DATE:=null
 ,p_CATCHUPPLANENDDT15 DATE:=null
 ,p_CATCHUPPLANRQ15AMT VARCHAR2:=null
 ,p_CATCHUPPLANCREDT DATE:=null
 ,p_CATCHUPPLANMODDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:22
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:47:22
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVCTCHTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVCTCHTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVCTCHINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVCTCHINSTSP',p_userid,4,
 logtxt1=>'SOFVCTCHINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVCTCHINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCTCHTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CATCHUPPLANSBANMB';
 cur_colvalue:=P_CATCHUPPLANSBANMB;
 if P_CATCHUPPLANSBANMB is null then
 
 new_rec.CATCHUPPLANSBANMB:=rpad(' ',10);
 
 else 
 new_rec.CATCHUPPLANSBANMB:=P_CATCHUPPLANSBANMB;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT1';
 cur_colvalue:=P_CATCHUPPLANSTRTDT1;
 if P_CATCHUPPLANSTRTDT1 is null then
 
 new_rec.CATCHUPPLANSTRTDT1:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT1:=P_CATCHUPPLANSTRTDT1;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT1';
 cur_colvalue:=P_CATCHUPPLANENDDT1;
 if P_CATCHUPPLANENDDT1 is null then
 
 new_rec.CATCHUPPLANENDDT1:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT1:=P_CATCHUPPLANENDDT1;
 end if;
 cur_colname:='P_CATCHUPPLANRQ1AMT';
 cur_colvalue:=P_CATCHUPPLANRQ1AMT;
 if P_CATCHUPPLANRQ1AMT is null then
 
 new_rec.CATCHUPPLANRQ1AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ1AMT:=P_CATCHUPPLANRQ1AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT2';
 cur_colvalue:=P_CATCHUPPLANSTRTDT2;
 if P_CATCHUPPLANSTRTDT2 is null then
 
 new_rec.CATCHUPPLANSTRTDT2:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT2:=P_CATCHUPPLANSTRTDT2;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT2';
 cur_colvalue:=P_CATCHUPPLANENDDT2;
 if P_CATCHUPPLANENDDT2 is null then
 
 new_rec.CATCHUPPLANENDDT2:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT2:=P_CATCHUPPLANENDDT2;
 end if;
 cur_colname:='P_CATCHUPPLANRQ2AMT';
 cur_colvalue:=P_CATCHUPPLANRQ2AMT;
 if P_CATCHUPPLANRQ2AMT is null then
 
 new_rec.CATCHUPPLANRQ2AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ2AMT:=P_CATCHUPPLANRQ2AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT3';
 cur_colvalue:=P_CATCHUPPLANSTRTDT3;
 if P_CATCHUPPLANSTRTDT3 is null then
 
 new_rec.CATCHUPPLANSTRTDT3:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT3:=P_CATCHUPPLANSTRTDT3;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT3';
 cur_colvalue:=P_CATCHUPPLANENDDT3;
 if P_CATCHUPPLANENDDT3 is null then
 
 new_rec.CATCHUPPLANENDDT3:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT3:=P_CATCHUPPLANENDDT3;
 end if;
 cur_colname:='P_CATCHUPPLANRQ3AMT';
 cur_colvalue:=P_CATCHUPPLANRQ3AMT;
 if P_CATCHUPPLANRQ3AMT is null then
 
 new_rec.CATCHUPPLANRQ3AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ3AMT:=P_CATCHUPPLANRQ3AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT4';
 cur_colvalue:=P_CATCHUPPLANSTRTDT4;
 if P_CATCHUPPLANSTRTDT4 is null then
 
 new_rec.CATCHUPPLANSTRTDT4:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT4:=P_CATCHUPPLANSTRTDT4;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT4';
 cur_colvalue:=P_CATCHUPPLANENDDT4;
 if P_CATCHUPPLANENDDT4 is null then
 
 new_rec.CATCHUPPLANENDDT4:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT4:=P_CATCHUPPLANENDDT4;
 end if;
 cur_colname:='P_CATCHUPPLANRQ4AMT';
 cur_colvalue:=P_CATCHUPPLANRQ4AMT;
 if P_CATCHUPPLANRQ4AMT is null then
 
 new_rec.CATCHUPPLANRQ4AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ4AMT:=P_CATCHUPPLANRQ4AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT5';
 cur_colvalue:=P_CATCHUPPLANSTRTDT5;
 if P_CATCHUPPLANSTRTDT5 is null then
 
 new_rec.CATCHUPPLANSTRTDT5:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT5:=P_CATCHUPPLANSTRTDT5;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT5';
 cur_colvalue:=P_CATCHUPPLANENDDT5;
 if P_CATCHUPPLANENDDT5 is null then
 
 new_rec.CATCHUPPLANENDDT5:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT5:=P_CATCHUPPLANENDDT5;
 end if;
 cur_colname:='P_CATCHUPPLANRQ5AMT';
 cur_colvalue:=P_CATCHUPPLANRQ5AMT;
 if P_CATCHUPPLANRQ5AMT is null then
 
 new_rec.CATCHUPPLANRQ5AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ5AMT:=P_CATCHUPPLANRQ5AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT6';
 cur_colvalue:=P_CATCHUPPLANSTRTDT6;
 if P_CATCHUPPLANSTRTDT6 is null then
 
 new_rec.CATCHUPPLANSTRTDT6:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT6:=P_CATCHUPPLANSTRTDT6;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT6';
 cur_colvalue:=P_CATCHUPPLANENDDT6;
 if P_CATCHUPPLANENDDT6 is null then
 
 new_rec.CATCHUPPLANENDDT6:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT6:=P_CATCHUPPLANENDDT6;
 end if;
 cur_colname:='P_CATCHUPPLANRQ6AMT';
 cur_colvalue:=P_CATCHUPPLANRQ6AMT;
 if P_CATCHUPPLANRQ6AMT is null then
 
 new_rec.CATCHUPPLANRQ6AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ6AMT:=P_CATCHUPPLANRQ6AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT7';
 cur_colvalue:=P_CATCHUPPLANSTRTDT7;
 if P_CATCHUPPLANSTRTDT7 is null then
 
 new_rec.CATCHUPPLANSTRTDT7:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT7:=P_CATCHUPPLANSTRTDT7;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT7';
 cur_colvalue:=P_CATCHUPPLANENDDT7;
 if P_CATCHUPPLANENDDT7 is null then
 
 new_rec.CATCHUPPLANENDDT7:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT7:=P_CATCHUPPLANENDDT7;
 end if;
 cur_colname:='P_CATCHUPPLANRQ7AMT';
 cur_colvalue:=P_CATCHUPPLANRQ7AMT;
 if P_CATCHUPPLANRQ7AMT is null then
 
 new_rec.CATCHUPPLANRQ7AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ7AMT:=P_CATCHUPPLANRQ7AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT8';
 cur_colvalue:=P_CATCHUPPLANSTRTDT8;
 if P_CATCHUPPLANSTRTDT8 is null then
 
 new_rec.CATCHUPPLANSTRTDT8:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT8:=P_CATCHUPPLANSTRTDT8;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT8';
 cur_colvalue:=P_CATCHUPPLANENDDT8;
 if P_CATCHUPPLANENDDT8 is null then
 
 new_rec.CATCHUPPLANENDDT8:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT8:=P_CATCHUPPLANENDDT8;
 end if;
 cur_colname:='P_CATCHUPPLANRQ8AMT';
 cur_colvalue:=P_CATCHUPPLANRQ8AMT;
 if P_CATCHUPPLANRQ8AMT is null then
 
 new_rec.CATCHUPPLANRQ8AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ8AMT:=P_CATCHUPPLANRQ8AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT9';
 cur_colvalue:=P_CATCHUPPLANSTRTDT9;
 if P_CATCHUPPLANSTRTDT9 is null then
 
 new_rec.CATCHUPPLANSTRTDT9:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT9:=P_CATCHUPPLANSTRTDT9;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT9';
 cur_colvalue:=P_CATCHUPPLANENDDT9;
 if P_CATCHUPPLANENDDT9 is null then
 
 new_rec.CATCHUPPLANENDDT9:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT9:=P_CATCHUPPLANENDDT9;
 end if;
 cur_colname:='P_CATCHUPPLANRQ9AMT';
 cur_colvalue:=P_CATCHUPPLANRQ9AMT;
 if P_CATCHUPPLANRQ9AMT is null then
 
 new_rec.CATCHUPPLANRQ9AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ9AMT:=P_CATCHUPPLANRQ9AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT10';
 cur_colvalue:=P_CATCHUPPLANSTRTDT10;
 if P_CATCHUPPLANSTRTDT10 is null then
 
 new_rec.CATCHUPPLANSTRTDT10:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT10:=P_CATCHUPPLANSTRTDT10;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT10';
 cur_colvalue:=P_CATCHUPPLANENDDT10;
 if P_CATCHUPPLANENDDT10 is null then
 
 new_rec.CATCHUPPLANENDDT10:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT10:=P_CATCHUPPLANENDDT10;
 end if;
 cur_colname:='P_CATCHUPPLANRQ10AMT';
 cur_colvalue:=P_CATCHUPPLANRQ10AMT;
 if P_CATCHUPPLANRQ10AMT is null then
 
 new_rec.CATCHUPPLANRQ10AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ10AMT:=P_CATCHUPPLANRQ10AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT11';
 cur_colvalue:=P_CATCHUPPLANSTRTDT11;
 if P_CATCHUPPLANSTRTDT11 is null then
 
 new_rec.CATCHUPPLANSTRTDT11:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT11:=P_CATCHUPPLANSTRTDT11;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT11';
 cur_colvalue:=P_CATCHUPPLANENDDT11;
 if P_CATCHUPPLANENDDT11 is null then
 
 new_rec.CATCHUPPLANENDDT11:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT11:=P_CATCHUPPLANENDDT11;
 end if;
 cur_colname:='P_CATCHUPPLANRQ11AMT';
 cur_colvalue:=P_CATCHUPPLANRQ11AMT;
 if P_CATCHUPPLANRQ11AMT is null then
 
 new_rec.CATCHUPPLANRQ11AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ11AMT:=P_CATCHUPPLANRQ11AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT12';
 cur_colvalue:=P_CATCHUPPLANSTRTDT12;
 if P_CATCHUPPLANSTRTDT12 is null then
 
 new_rec.CATCHUPPLANSTRTDT12:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT12:=P_CATCHUPPLANSTRTDT12;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT12';
 cur_colvalue:=P_CATCHUPPLANENDDT12;
 if P_CATCHUPPLANENDDT12 is null then
 
 new_rec.CATCHUPPLANENDDT12:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT12:=P_CATCHUPPLANENDDT12;
 end if;
 cur_colname:='P_CATCHUPPLANRQ12AMT';
 cur_colvalue:=P_CATCHUPPLANRQ12AMT;
 if P_CATCHUPPLANRQ12AMT is null then
 
 new_rec.CATCHUPPLANRQ12AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ12AMT:=P_CATCHUPPLANRQ12AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT13';
 cur_colvalue:=P_CATCHUPPLANSTRTDT13;
 if P_CATCHUPPLANSTRTDT13 is null then
 
 new_rec.CATCHUPPLANSTRTDT13:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT13:=P_CATCHUPPLANSTRTDT13;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT13';
 cur_colvalue:=P_CATCHUPPLANENDDT13;
 if P_CATCHUPPLANENDDT13 is null then
 
 new_rec.CATCHUPPLANENDDT13:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT13:=P_CATCHUPPLANENDDT13;
 end if;
 cur_colname:='P_CATCHUPPLANRQ13AMT';
 cur_colvalue:=P_CATCHUPPLANRQ13AMT;
 if P_CATCHUPPLANRQ13AMT is null then
 
 new_rec.CATCHUPPLANRQ13AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ13AMT:=P_CATCHUPPLANRQ13AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT14';
 cur_colvalue:=P_CATCHUPPLANSTRTDT14;
 if P_CATCHUPPLANSTRTDT14 is null then
 
 new_rec.CATCHUPPLANSTRTDT14:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT14:=P_CATCHUPPLANSTRTDT14;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT14';
 cur_colvalue:=P_CATCHUPPLANENDDT14;
 if P_CATCHUPPLANENDDT14 is null then
 
 new_rec.CATCHUPPLANENDDT14:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT14:=P_CATCHUPPLANENDDT14;
 end if;
 cur_colname:='P_CATCHUPPLANRQ14AMT';
 cur_colvalue:=P_CATCHUPPLANRQ14AMT;
 if P_CATCHUPPLANRQ14AMT is null then
 
 new_rec.CATCHUPPLANRQ14AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ14AMT:=P_CATCHUPPLANRQ14AMT;
 end if;
 cur_colname:='P_CATCHUPPLANSTRTDT15';
 cur_colvalue:=P_CATCHUPPLANSTRTDT15;
 if P_CATCHUPPLANSTRTDT15 is null then
 
 new_rec.CATCHUPPLANSTRTDT15:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANSTRTDT15:=P_CATCHUPPLANSTRTDT15;
 end if;
 cur_colname:='P_CATCHUPPLANENDDT15';
 cur_colvalue:=P_CATCHUPPLANENDDT15;
 if P_CATCHUPPLANENDDT15 is null then
 
 new_rec.CATCHUPPLANENDDT15:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANENDDT15:=P_CATCHUPPLANENDDT15;
 end if;
 cur_colname:='P_CATCHUPPLANRQ15AMT';
 cur_colvalue:=P_CATCHUPPLANRQ15AMT;
 if P_CATCHUPPLANRQ15AMT is null then
 
 new_rec.CATCHUPPLANRQ15AMT:=0;
 
 else 
 new_rec.CATCHUPPLANRQ15AMT:=P_CATCHUPPLANRQ15AMT;
 end if;
 cur_colname:='P_CATCHUPPLANCREDT';
 cur_colvalue:=P_CATCHUPPLANCREDT;
 if P_CATCHUPPLANCREDT is null then
 
 new_rec.CATCHUPPLANCREDT:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANCREDT:=P_CATCHUPPLANCREDT;
 end if;
 cur_colname:='P_CATCHUPPLANMODDT';
 cur_colvalue:=P_CATCHUPPLANMODDT;
 if P_CATCHUPPLANMODDT is null then
 
 new_rec.CATCHUPPLANMODDT:=jan_1_1900;
 
 else 
 new_rec.CATCHUPPLANMODDT:=P_CATCHUPPLANMODDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CATCHUPPLANSBANMB)=('||new_rec.CATCHUPPLANSBANMB||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVCTCHTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVCTCHINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVCTCHINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVCTCHINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:47:22';
 ROLLBACK TO SOFVCTCHINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVCTCHINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVCTCHINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:47:22';
 ROLLBACK TO SOFVCTCHINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVCTCHINSTSP',force_log_entry=>TRUE);
 --
 END SOFVCTCHINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:03. --->
PROCEDURE COREEMPATTRINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMPATTRID VARCHAR2:=null
 ,p_EMPID VARCHAR2:=null
 ,p_ATTRVAL VARCHAR2:=null
 ,p_ATTRTYPID VARCHAR2:=null
 ,p_EFFDT DATE:=null
 ,p_TRMDT DATE:=null
 ,p_ACTVATTR VARCHAR2:=null
 ,p_EMPATTRSHOW VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:58
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:58
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.COREEMPATTRTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.COREEMPATTRTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize EMPATTRID from sequence EMPATTRIDSEQ
 Function NEXTVAL_EMPATTRIDSEQ return number is
 N number;
 begin
 select EMPATTRIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint COREEMPATTRINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init COREEMPATTRINSTSP',p_userid,4,
 logtxt1=>'COREEMPATTRINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'COREEMPATTRINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_EMPATTRID';
 cur_colvalue:=P_EMPATTRID;
 if P_EMPATTRID is null then
 
 new_rec.EMPATTRID:=NEXTVAL_EMPATTRIDSEQ();
 
 else 
 new_rec.EMPATTRID:=P_EMPATTRID;
 end if;
 cur_colname:='P_EMPID';
 cur_colvalue:=P_EMPID;
 if P_EMPID is null then
 
 new_rec.EMPID:=0;
 
 else 
 new_rec.EMPID:=P_EMPID;
 end if;
 cur_colname:='P_ATTRVAL';
 cur_colvalue:=P_ATTRVAL;
 if P_ATTRVAL is null then
 
 new_rec.ATTRVAL:=' ';
 
 else 
 new_rec.ATTRVAL:=P_ATTRVAL;
 end if;
 cur_colname:='P_ATTRTYPID';
 cur_colvalue:=P_ATTRTYPID;
 if P_ATTRTYPID is null then
 
 new_rec.ATTRTYPID:=0;
 
 else 
 new_rec.ATTRTYPID:=P_ATTRTYPID;
 end if;
 cur_colname:='P_EFFDT';
 cur_colvalue:=P_EFFDT;
 if P_EFFDT is null then
 
 new_rec.EFFDT:=jan_1_1900;
 
 else 
 new_rec.EFFDT:=P_EFFDT;
 end if;
 cur_colname:='P_TRMDT';
 cur_colvalue:=P_TRMDT;
 if P_TRMDT is null then
 
 new_rec.TRMDT:=jan_1_1900;
 
 else 
 new_rec.TRMDT:=P_TRMDT;
 end if;
 cur_colname:='P_ACTVATTR';
 cur_colvalue:=P_ACTVATTR;
 if P_ACTVATTR is null then
 
 new_rec.ACTVATTR:=0;
 
 else 
 new_rec.ACTVATTR:=P_ACTVATTR;
 end if;
 cur_colname:='P_EMPATTRSHOW';
 cur_colvalue:=P_EMPATTRSHOW;
 if P_EMPATTRSHOW is null then
 
 new_rec.EMPATTRSHOW:=0;
 
 else 
 new_rec.EMPATTRSHOW:=P_EMPATTRSHOW;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(EMPATTRID)=('||new_rec.EMPATTRID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.COREEMPATTRTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End COREEMPATTRINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'COREEMPATTRINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in COREEMPATTRINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:58';
 ROLLBACK TO COREEMPATTRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'COREEMPATTRINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in COREEMPATTRINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:58';
 ROLLBACK TO COREEMPATTRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'COREEMPATTRINSTSP',force_log_entry=>TRUE);
 --
 END COREEMPATTRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


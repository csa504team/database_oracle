<!--- Saved 10/09/2018 11:29:15. --->
PROCEDURE WFCTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_WFCTID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_WFCTAMT VARCHAR2:=null
 ,p_CDCNMB CHAR:=null
 ,p_REPDT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:48:23
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:48:23
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.WFCTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.WFCTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize WFCTID from sequence WFCTIDSEQ
 Function NEXTVAL_WFCTIDSEQ return number is
 N number;
 begin
 select WFCTIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint WFCTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init WFCTINSTSP',p_userid,4,
 logtxt1=>'WFCTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'WFCTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_WFCTID';
 cur_colvalue:=P_WFCTID;
 if P_WFCTID is null then
 
 new_rec.WFCTID:=NEXTVAL_WFCTIDSEQ();
 
 else 
 new_rec.WFCTID:=P_WFCTID;
 end if;
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 
 new_rec.LOANNMB:=rpad(' ',10);
 
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_WFCTAMT';
 cur_colvalue:=P_WFCTAMT;
 if P_WFCTAMT is null then
 
 new_rec.WFCTAMT:=0;
 
 else 
 new_rec.WFCTAMT:=P_WFCTAMT;
 end if;
 cur_colname:='P_CDCNMB';
 cur_colvalue:=P_CDCNMB;
 if P_CDCNMB is null then
 
 new_rec.CDCNMB:=rpad(' ',8);
 
 else 
 new_rec.CDCNMB:=P_CDCNMB;
 end if;
 cur_colname:='P_REPDT';
 cur_colvalue:=P_REPDT;
 if P_REPDT is null then
 
 new_rec.REPDT:=0;
 
 else 
 new_rec.REPDT:=P_REPDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(WFCTID)=('||new_rec.WFCTID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.WFCTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End WFCTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'WFCTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in WFCTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:48:23';
 ROLLBACK TO WFCTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'WFCTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in WFCTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:48:23';
 ROLLBACK TO WFCTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'WFCTINSTSP',force_log_entry=>TRUE);
 --
 END WFCTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPMFDUEFROMBORRUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPMFDUEFROMBORRID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_SEMIANAMT VARCHAR2:=null
 ,p_ESCPRIORPYMTAMT VARCHAR2:=null
 ,p_CDCDUEFROMBORRAMT VARCHAR2:=null
 ,p_DUEFROMBORRAMT VARCHAR2:=null
 ,p_CSADUEFROMBORRAMT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_APPRDT DATE:=null
 ,p_CURRFEEBALAMT VARCHAR2:=null
 ,p_LATEFEEAMT VARCHAR2:=null
 ,p_MATRDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:35
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:46:35
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure PRPMFDUEFROMBORRUPDTSP performs UPDATE on STGCSA.PRPMFDUEFROMBORRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPMFDUEFROMBORRID
 for P_IDENTIFIER=1 qualification is on LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.PRPMFDUEFROMBORRTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.PRPMFDUEFROMBORRTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_PRPMFDUEFROMBORRID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRPMFDUEFROMBORRID';
 cur_colvalue:=P_PRPMFDUEFROMBORRID;
 DBrec.PRPMFDUEFROMBORRID:=P_PRPMFDUEFROMBORRID; 
 end if;
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_SEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANAMT';
 cur_colvalue:=P_SEMIANAMT;
 DBrec.SEMIANAMT:=P_SEMIANAMT; 
 end if;
 if P_ESCPRIORPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ESCPRIORPYMTAMT';
 cur_colvalue:=P_ESCPRIORPYMTAMT;
 DBrec.ESCPRIORPYMTAMT:=P_ESCPRIORPYMTAMT; 
 end if;
 if P_CDCDUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCDUEFROMBORRAMT';
 cur_colvalue:=P_CDCDUEFROMBORRAMT;
 DBrec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT; 
 end if;
 if P_DUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRAMT';
 cur_colvalue:=P_DUEFROMBORRAMT;
 DBrec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT; 
 end if;
 if P_CSADUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSADUEFROMBORRAMT';
 cur_colvalue:=P_CSADUEFROMBORRAMT;
 DBrec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT; 
 end if;
 if P_SEMIANDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 DBrec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_APPRDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_APPRDT';
 cur_colvalue:=P_APPRDT;
 DBrec.APPRDT:=P_APPRDT; 
 end if;
 if P_CURRFEEBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CURRFEEBALAMT';
 cur_colvalue:=P_CURRFEEBALAMT;
 DBrec.CURRFEEBALAMT:=P_CURRFEEBALAMT; 
 end if;
 if P_LATEFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LATEFEEAMT';
 cur_colvalue:=P_LATEFEEAMT;
 DBrec.LATEFEEAMT:=P_LATEFEEAMT; 
 end if;
 if P_MATRDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MATRDT';
 cur_colvalue:=P_MATRDT;
 DBrec.MATRDT:=P_MATRDT; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPMFDUEFROMBORRTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,PRPMFDUEFROMBORRID=DBrec.PRPMFDUEFROMBORRID
 ,LOANNMB=DBrec.LOANNMB
 ,SEMIANAMT=DBrec.SEMIANAMT
 ,ESCPRIORPYMTAMT=DBrec.ESCPRIORPYMTAMT
 ,CDCDUEFROMBORRAMT=DBrec.CDCDUEFROMBORRAMT
 ,DUEFROMBORRAMT=DBrec.DUEFROMBORRAMT
 ,CSADUEFROMBORRAMT=DBrec.CSADUEFROMBORRAMT
 ,SEMIANDT=DBrec.SEMIANDT
 ,APPRDT=DBrec.APPRDT
 ,CURRFEEBALAMT=DBrec.CURRFEEBALAMT
 ,LATEFEEAMT=DBrec.LATEFEEAMT
 ,MATRDT=DBrec.MATRDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.PRPMFDUEFROMBORRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFDUEFROMBORRUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPMFDUEFROMBORRUPDTSP',p_userid,4,
 logtxt1=>'PRPMFDUEFROMBORRUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'PRPMFDUEFROMBORRUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(PRPMFDUEFROMBORRID)=('||P_PRPMFDUEFROMBORRID||')';
 begin
 select rowid into rec_rowid from STGCSA.PRPMFDUEFROMBORRTBL where 1=1
 and PRPMFDUEFROMBORRID=P_PRPMFDUEFROMBORRID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFDUEFROMBORRTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 for r1 in (select rowid from STGCSA.PRPMFDUEFROMBORRTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.PRPMFDUEFROMBORRTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFDUEFROMBORRTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFDUEFROMBORRUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'PRPMFDUEFROMBORRUPDTSP');
 else
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 p_errmsg:=p_errmsg||' in PRPMFDUEFROMBORRUPDTSP build 2018-10-08 15:46:35';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRPMFDUEFROMBORRUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFDUEFROMBORRUPDTSP build 2018-10-08 15:46:35';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRUPDTSP',force_log_entry=>true);
 --
 END PRPMFDUEFROMBORRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


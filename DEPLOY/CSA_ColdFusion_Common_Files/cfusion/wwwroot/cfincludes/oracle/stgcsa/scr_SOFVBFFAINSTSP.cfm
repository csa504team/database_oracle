<!--- Saved 10/09/2018 11:29:09. --->
PROCEDURE SOFVBFFAINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_ASSUMPTAXID CHAR:=null
 ,p_ASSUMPNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR1NM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTR2NM VARCHAR2:=null
 ,p_ASSUMPMAILADRCTYNM VARCHAR2:=null
 ,p_ASSUMPMAILADRSTCD CHAR:=null
 ,p_ASSUMPMAILADRZIPCD CHAR:=null
 ,p_ASSUMPMAILADRZIP4CD CHAR:=null
 ,p_ASSUMPOFC CHAR:=null
 ,p_ASSUMPPRGRM CHAR:=null
 ,p_ASSUMPSTATCDOFLOAN CHAR:=null
 ,p_ASSUMPVERIFICATIONIND CHAR:=null
 ,p_ASSUMPVERIFICATIONDT DATE:=null
 ,p_ASSUMPNMMAILADRCHNGIND CHAR:=null
 ,p_ASSUMPNMADRCHNGDT DATE:=null
 ,p_ASSUMPTAXFORMFLD CHAR:=null
 ,p_ASSUMPTAXMAINTNDT DATE:=null
 ,p_ASSUMPKEYLOANNMB CHAR:=null
 ,p_ASSUMPLASTEFFDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:06
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:47:06
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVBFFATBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVBFFATBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- vars for checking non-defaultable columns
 onespace varchar2(10):=' '; 
 ndfltmsg varchar2(300);
 v_errfnd boolean:=false;
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVBFFAINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVBFFAINSTSP',p_userid,4,
 logtxt1=>'SOFVBFFAINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVBFFAINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBFFATBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 raise_application_error(-20001,cur_colname||' may not be null');
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_ASSUMPTAXID';
 cur_colvalue:=P_ASSUMPTAXID;
 if P_ASSUMPTAXID is null then
 
 new_rec.ASSUMPTAXID:=rpad(' ',10);
 
 else 
 new_rec.ASSUMPTAXID:=P_ASSUMPTAXID;
 end if;
 cur_colname:='P_ASSUMPNM';
 cur_colvalue:=P_ASSUMPNM;
 if P_ASSUMPNM is null then
 
 new_rec.ASSUMPNM:=' ';
 
 else 
 new_rec.ASSUMPNM:=P_ASSUMPNM;
 end if;
 cur_colname:='P_ASSUMPMAILADRSTR1NM';
 cur_colvalue:=P_ASSUMPMAILADRSTR1NM;
 if P_ASSUMPMAILADRSTR1NM is null then
 
 new_rec.ASSUMPMAILADRSTR1NM:=' ';
 
 else 
 new_rec.ASSUMPMAILADRSTR1NM:=P_ASSUMPMAILADRSTR1NM;
 end if;
 cur_colname:='P_ASSUMPMAILADRSTR2NM';
 cur_colvalue:=P_ASSUMPMAILADRSTR2NM;
 if P_ASSUMPMAILADRSTR2NM is null then
 
 new_rec.ASSUMPMAILADRSTR2NM:=' ';
 
 else 
 new_rec.ASSUMPMAILADRSTR2NM:=P_ASSUMPMAILADRSTR2NM;
 end if;
 cur_colname:='P_ASSUMPMAILADRCTYNM';
 cur_colvalue:=P_ASSUMPMAILADRCTYNM;
 if P_ASSUMPMAILADRCTYNM is null then
 
 new_rec.ASSUMPMAILADRCTYNM:=' ';
 
 else 
 new_rec.ASSUMPMAILADRCTYNM:=P_ASSUMPMAILADRCTYNM;
 end if;
 cur_colname:='P_ASSUMPMAILADRSTCD';
 cur_colvalue:=P_ASSUMPMAILADRSTCD;
 if P_ASSUMPMAILADRSTCD is null then
 
 new_rec.ASSUMPMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.ASSUMPMAILADRSTCD:=P_ASSUMPMAILADRSTCD;
 end if;
 cur_colname:='P_ASSUMPMAILADRZIPCD';
 cur_colvalue:=P_ASSUMPMAILADRZIPCD;
 if P_ASSUMPMAILADRZIPCD is null then
 
 new_rec.ASSUMPMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.ASSUMPMAILADRZIPCD:=P_ASSUMPMAILADRZIPCD;
 end if;
 cur_colname:='P_ASSUMPMAILADRZIP4CD';
 cur_colvalue:=P_ASSUMPMAILADRZIP4CD;
 if P_ASSUMPMAILADRZIP4CD is null then
 
 new_rec.ASSUMPMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.ASSUMPMAILADRZIP4CD:=P_ASSUMPMAILADRZIP4CD;
 end if;
 cur_colname:='P_ASSUMPOFC';
 cur_colvalue:=P_ASSUMPOFC;
 if P_ASSUMPOFC is null then
 
 new_rec.ASSUMPOFC:=rpad(' ',5);
 
 else 
 new_rec.ASSUMPOFC:=P_ASSUMPOFC;
 end if;
 cur_colname:='P_ASSUMPPRGRM';
 cur_colvalue:=P_ASSUMPPRGRM;
 if P_ASSUMPPRGRM is null then
 
 new_rec.ASSUMPPRGRM:=rpad(' ',3);
 
 else 
 new_rec.ASSUMPPRGRM:=P_ASSUMPPRGRM;
 end if;
 cur_colname:='P_ASSUMPSTATCDOFLOAN';
 cur_colvalue:=P_ASSUMPSTATCDOFLOAN;
 if P_ASSUMPSTATCDOFLOAN is null then
 
 new_rec.ASSUMPSTATCDOFLOAN:=rpad(' ',2);
 
 else 
 new_rec.ASSUMPSTATCDOFLOAN:=P_ASSUMPSTATCDOFLOAN;
 end if;
 cur_colname:='P_ASSUMPVERIFICATIONIND';
 cur_colvalue:=P_ASSUMPVERIFICATIONIND;
 if P_ASSUMPVERIFICATIONIND is null then
 
 new_rec.ASSUMPVERIFICATIONIND:=rpad(' ',1);
 
 else 
 new_rec.ASSUMPVERIFICATIONIND:=P_ASSUMPVERIFICATIONIND;
 end if;
 cur_colname:='P_ASSUMPVERIFICATIONDT';
 cur_colvalue:=P_ASSUMPVERIFICATIONDT;
 if P_ASSUMPVERIFICATIONDT is null then
 
 new_rec.ASSUMPVERIFICATIONDT:=jan_1_1900;
 
 else 
 new_rec.ASSUMPVERIFICATIONDT:=P_ASSUMPVERIFICATIONDT;
 end if;
 cur_colname:='P_ASSUMPNMMAILADRCHNGIND';
 cur_colvalue:=P_ASSUMPNMMAILADRCHNGIND;
 if P_ASSUMPNMMAILADRCHNGIND is null then
 
 new_rec.ASSUMPNMMAILADRCHNGIND:=rpad(' ',1);
 
 else 
 new_rec.ASSUMPNMMAILADRCHNGIND:=P_ASSUMPNMMAILADRCHNGIND;
 end if;
 cur_colname:='P_ASSUMPNMADRCHNGDT';
 cur_colvalue:=P_ASSUMPNMADRCHNGDT;
 if P_ASSUMPNMADRCHNGDT is null then
 
 new_rec.ASSUMPNMADRCHNGDT:=jan_1_1900;
 
 else 
 new_rec.ASSUMPNMADRCHNGDT:=P_ASSUMPNMADRCHNGDT;
 end if;
 cur_colname:='P_ASSUMPTAXFORMFLD';
 cur_colvalue:=P_ASSUMPTAXFORMFLD;
 if P_ASSUMPTAXFORMFLD is null then
 
 new_rec.ASSUMPTAXFORMFLD:=rpad(' ',1);
 
 else 
 new_rec.ASSUMPTAXFORMFLD:=P_ASSUMPTAXFORMFLD;
 end if;
 cur_colname:='P_ASSUMPTAXMAINTNDT';
 cur_colvalue:=P_ASSUMPTAXMAINTNDT;
 if P_ASSUMPTAXMAINTNDT is null then
 
 new_rec.ASSUMPTAXMAINTNDT:=jan_1_1900;
 
 else 
 new_rec.ASSUMPTAXMAINTNDT:=P_ASSUMPTAXMAINTNDT;
 end if;
 cur_colname:='P_ASSUMPKEYLOANNMB';
 cur_colvalue:=P_ASSUMPKEYLOANNMB;
 if P_ASSUMPKEYLOANNMB is null then
 
 new_rec.ASSUMPKEYLOANNMB:=rpad(' ',10);
 
 else 
 new_rec.ASSUMPKEYLOANNMB:=P_ASSUMPKEYLOANNMB;
 end if;
 cur_colname:='P_ASSUMPLASTEFFDT';
 cur_colvalue:=P_ASSUMPLASTEFFDT;
 if P_ASSUMPLASTEFFDT is null then
 
 new_rec.ASSUMPLASTEFFDT:=jan_1_1900;
 
 else 
 new_rec.ASSUMPLASTEFFDT:=P_ASSUMPLASTEFFDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(ASSUMPKEYLOANNMB)=('||new_rec.ASSUMPKEYLOANNMB||')'||' and '||'(ASSUMPLASTEFFDT)=('||new_rec.ASSUMPLASTEFFDT||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVBFFATBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVBFFAINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVBFFAINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVBFFAINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:47:06';
 ROLLBACK TO SOFVBFFAINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVBFFAINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVBFFAINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:47:06';
 ROLLBACK TO SOFVBFFAINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVBFFAINSTSP',force_log_entry=>TRUE);
 --
 END SOFVBFFAINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


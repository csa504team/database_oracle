<!--- Saved 10/09/2018 11:29:14. --->
PROCEDURE SOFVRTSCINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRGMRTBASIS CHAR:=null
 ,p_PRGMRTFEEDESC CHAR:=null
 ,p_PRGMRTENDDT DATE:=null
 ,p_PRGMRTSTARTDT DATE:=null
 ,p_PRGMRTPCT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:48:04
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:48:04
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVRTSCTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVRTSCTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVRTSCINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVRTSCINSTSP',p_userid,4,
 logtxt1=>'SOFVRTSCINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVRTSCINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVRTSCTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_PRGMRTBASIS';
 cur_colvalue:=P_PRGMRTBASIS;
 if P_PRGMRTBASIS is null then
 
 new_rec.PRGMRTBASIS:=rpad(' ',2);
 
 else 
 new_rec.PRGMRTBASIS:=P_PRGMRTBASIS;
 end if;
 cur_colname:='P_PRGMRTFEEDESC';
 cur_colvalue:=P_PRGMRTFEEDESC;
 if P_PRGMRTFEEDESC is null then
 
 new_rec.PRGMRTFEEDESC:=rpad(' ',10);
 
 else 
 new_rec.PRGMRTFEEDESC:=P_PRGMRTFEEDESC;
 end if;
 cur_colname:='P_PRGMRTENDDT';
 cur_colvalue:=P_PRGMRTENDDT;
 if P_PRGMRTENDDT is null then
 
 new_rec.PRGMRTENDDT:=jan_1_1900;
 
 else 
 new_rec.PRGMRTENDDT:=P_PRGMRTENDDT;
 end if;
 cur_colname:='P_PRGMRTSTARTDT';
 cur_colvalue:=P_PRGMRTSTARTDT;
 if P_PRGMRTSTARTDT is null then
 
 new_rec.PRGMRTSTARTDT:=jan_1_1900;
 
 else 
 new_rec.PRGMRTSTARTDT:=P_PRGMRTSTARTDT;
 end if;
 cur_colname:='P_PRGMRTPCT';
 cur_colvalue:=P_PRGMRTPCT;
 if P_PRGMRTPCT is null then
 
 new_rec.PRGMRTPCT:=0;
 
 else 
 new_rec.PRGMRTPCT:=P_PRGMRTPCT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(PRGMRTFEEDESC)=('||new_rec.PRGMRTFEEDESC||')'||' and '||'(PRGMRTENDDT)=('||new_rec.PRGMRTENDDT||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVRTSCTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVRTSCINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVRTSCINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVRTSCINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:48:04';
 ROLLBACK TO SOFVRTSCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVRTSCINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVRTSCINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:48:04';
 ROLLBACK TO SOFVRTSCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVRTSCINSTSP',force_log_entry=>TRUE);
 --
 END SOFVRTSCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPMFWIREPOSTINGUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_BORRFEEAMT VARCHAR2:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_CSAFEEAMT VARCHAR2:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CDCFEEAPPLAMT VARCHAR2:=null
 ,p_CDCFEEDISBAMT VARCHAR2:=null
 ,p_CDCDISBAMT VARCHAR2:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_ADJTOAPPLINTAMT VARCHAR2:=null
 ,p_ADJTOAPPLPRINAMT VARCHAR2:=null
 ,p_LATEFEEADJAMT VARCHAR2:=null
 ,p_UNALLOCAMT VARCHAR2:=null
 ,p_CURBALAMT VARCHAR2:=null
 ,p_PREPAYAMT VARCHAR2:=null
 ,p_WIRECMNT VARCHAR2:=null
 ,p_ADJTYP VARCHAR2:=null
 ,p_OVRWRITECD VARCHAR2:=null
 ,p_POSTINGTYP VARCHAR2:=null
 ,p_POSTINGSCHDLDT DATE:=null
 ,p_POSTINGACTUALDT DATE:=null
 ,p_WIREPOSTINGID VARCHAR2:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_MFSTATCD VARCHAR2:=null
 ,p_ACHDEL VARCHAR2:=null
 ,p_OVRPAYAMT VARCHAR2:=null
 ,p_CDCREPDAMT VARCHAR2:=null
 ,p_CSAREPDAMT VARCHAR2:=null
 ,p_REPDAMT VARCHAR2:=null
 ,p_PREPAYDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_ZEROBALAMT VARCHAR2:=null
 ,p_SEMIANCDCNMB CHAR:=null
 ,p_SEMIANNOTEBALAMT VARCHAR2:=null
 ,p_SEMIANWIREAMT VARCHAR2:=null
 ,p_SEMIANPRINACCRAMT VARCHAR2:=null
 ,p_SEMIANINTACCRAMT VARCHAR2:=null
 ,p_SEMIANOPENGNTYAMT VARCHAR2:=null
 ,p_SEMIANOPENGNTYERRAMT VARCHAR2:=null
 ,p_SEMIANOPENGNTYREMAINAMT VARCHAR2:=null
 ,p_SEMIANPIPYMTAFT6MOAMT VARCHAR2:=null
 ,p_SEMIANPISHORTOVRAMT VARCHAR2:=null
 ,p_SEMIANESCROWPRIORPYMTAMT VARCHAR2:=null
 ,p_SEMIANDBENTRBALAMT VARCHAR2:=null
 ,p_SEMIANPREPAYPREMAMT VARCHAR2:=null
 ,p_SEMIANAMT VARCHAR2:=null
 ,p_SEMIANSBADUEFROMBORRAMT VARCHAR2:=null
 ,p_SEMIANSBABEHINDAMT VARCHAR2:=null
 ,p_SEMIANSBAACCRAMT VARCHAR2:=null
 ,p_SEMIANSBAPRJCTDAMT VARCHAR2:=null
 ,p_SEMIANCSADUEFROMBORRAMT VARCHAR2:=null
 ,p_SEMIANCSABEHINDAMT VARCHAR2:=null
 ,p_SEMIANCSAACCRAMT VARCHAR2:=null
 ,p_SEMIANCSAPRJCTDAMT VARCHAR2:=null
 ,p_SEMIANCDCDUEFROMBORRAMT VARCHAR2:=null
 ,p_SEMIANCDCBEHINDAMT VARCHAR2:=null
 ,p_SEMIANCDCACCRAMT VARCHAR2:=null
 ,p_SEMIANACCRINTONDELAMT VARCHAR2:=null
 ,p_SEMIANLATEFEEAMT VARCHAR2:=null
 ,p_SEMIANPROOFAMT VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_CREATUSERID VARCHAR2:=null
 ,p_CREATDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:42
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:46:42
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure PRPMFWIREPOSTINGUPDTSP performs UPDATE on STGCSA.PRPMFWIREPOSTINGTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: WIREPOSTINGID
 for P_IDENTIFIER=1 qualification is on TRANSID
 for P_IDENTIFIER=2 qualification is on CREATUSERID, CREATDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.PRPMFWIREPOSTINGTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.PRPMFWIREPOSTINGTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.PRPMFWIREPOSTINGTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_BORRFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BORRFEEAMT';
 cur_colvalue:=P_BORRFEEAMT;
 DBrec.BORRFEEAMT:=P_BORRFEEAMT; 
 end if;
 if P_BORRPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BORRPAIDTHRUDT';
 cur_colvalue:=P_BORRPAIDTHRUDT;
 DBrec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT; 
 end if;
 if P_CSAFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAFEEAMT';
 cur_colvalue:=P_CSAFEEAMT;
 DBrec.CSAFEEAMT:=P_CSAFEEAMT; 
 end if;
 if P_CSAPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAPAIDTHRUDT';
 cur_colvalue:=P_CSAPAIDTHRUDT;
 DBrec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT; 
 end if;
 if P_CDCFEEAPPLAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCFEEAPPLAMT';
 cur_colvalue:=P_CDCFEEAPPLAMT;
 DBrec.CDCFEEAPPLAMT:=P_CDCFEEAPPLAMT; 
 end if;
 if P_CDCFEEDISBAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCFEEDISBAMT';
 cur_colvalue:=P_CDCFEEDISBAMT;
 DBrec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT; 
 end if;
 if P_CDCDISBAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCDISBAMT';
 cur_colvalue:=P_CDCDISBAMT;
 DBrec.CDCDISBAMT:=P_CDCDISBAMT; 
 end if;
 if P_CDCPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCPAIDTHRUDT';
 cur_colvalue:=P_CDCPAIDTHRUDT;
 DBrec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT; 
 end if;
 if P_INTPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INTPAIDTHRUDT';
 cur_colvalue:=P_INTPAIDTHRUDT;
 DBrec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT; 
 end if;
 if P_ADJTOAPPLINTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ADJTOAPPLINTAMT';
 cur_colvalue:=P_ADJTOAPPLINTAMT;
 DBrec.ADJTOAPPLINTAMT:=P_ADJTOAPPLINTAMT; 
 end if;
 if P_ADJTOAPPLPRINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ADJTOAPPLPRINAMT';
 cur_colvalue:=P_ADJTOAPPLPRINAMT;
 DBrec.ADJTOAPPLPRINAMT:=P_ADJTOAPPLPRINAMT; 
 end if;
 if P_LATEFEEADJAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LATEFEEADJAMT';
 cur_colvalue:=P_LATEFEEADJAMT;
 DBrec.LATEFEEADJAMT:=P_LATEFEEADJAMT; 
 end if;
 if P_UNALLOCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UNALLOCAMT';
 cur_colvalue:=P_UNALLOCAMT;
 DBrec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_CURBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CURBALAMT';
 cur_colvalue:=P_CURBALAMT;
 DBrec.CURBALAMT:=P_CURBALAMT; 
 end if;
 if P_PREPAYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYAMT';
 cur_colvalue:=P_PREPAYAMT;
 DBrec.PREPAYAMT:=P_PREPAYAMT; 
 end if;
 if P_WIRECMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_WIRECMNT';
 cur_colvalue:=P_WIRECMNT;
 DBrec.WIRECMNT:=P_WIRECMNT; 
 end if;
 if P_ADJTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ADJTYP';
 cur_colvalue:=P_ADJTYP;
 DBrec.ADJTYP:=P_ADJTYP; 
 end if;
 if P_OVRWRITECD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OVRWRITECD';
 cur_colvalue:=P_OVRWRITECD;
 DBrec.OVRWRITECD:=P_OVRWRITECD; 
 end if;
 if P_POSTINGTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGTYP';
 cur_colvalue:=P_POSTINGTYP;
 DBrec.POSTINGTYP:=P_POSTINGTYP; 
 end if;
 if P_POSTINGSCHDLDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGSCHDLDT';
 cur_colvalue:=P_POSTINGSCHDLDT;
 DBrec.POSTINGSCHDLDT:=P_POSTINGSCHDLDT; 
 end if;
 if P_POSTINGACTUALDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGACTUALDT';
 cur_colvalue:=P_POSTINGACTUALDT;
 DBrec.POSTINGACTUALDT:=P_POSTINGACTUALDT; 
 end if;
 if P_WIREPOSTINGID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_WIREPOSTINGID';
 cur_colvalue:=P_WIREPOSTINGID;
 DBrec.WIREPOSTINGID:=P_WIREPOSTINGID; 
 end if;
 if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TRANSID';
 cur_colvalue:=P_TRANSID;
 DBrec.TRANSID:=P_TRANSID; 
 end if;
 if P_MFSTATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MFSTATCD';
 cur_colvalue:=P_MFSTATCD;
 DBrec.MFSTATCD:=P_MFSTATCD; 
 end if;
 if P_ACHDEL is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ACHDEL';
 cur_colvalue:=P_ACHDEL;
 DBrec.ACHDEL:=P_ACHDEL; 
 end if;
 if P_OVRPAYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OVRPAYAMT';
 cur_colvalue:=P_OVRPAYAMT;
 DBrec.OVRPAYAMT:=P_OVRPAYAMT; 
 end if;
 if P_CDCREPDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCREPDAMT';
 cur_colvalue:=P_CDCREPDAMT;
 DBrec.CDCREPDAMT:=P_CDCREPDAMT; 
 end if;
 if P_CSAREPDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAREPDAMT';
 cur_colvalue:=P_CSAREPDAMT;
 DBrec.CSAREPDAMT:=P_CSAREPDAMT; 
 end if;
 if P_REPDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_REPDAMT';
 cur_colvalue:=P_REPDAMT;
 DBrec.REPDAMT:=P_REPDAMT; 
 end if;
 if P_PREPAYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 DBrec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_SEMIANDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 DBrec.SEMIANDT:=P_SEMIANDT; 
 end if;
 if P_ZEROBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ZEROBALAMT';
 cur_colvalue:=P_ZEROBALAMT;
 DBrec.ZEROBALAMT:=P_ZEROBALAMT; 
 end if;
 if P_SEMIANCDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCDCNMB';
 cur_colvalue:=P_SEMIANCDCNMB;
 DBrec.SEMIANCDCNMB:=P_SEMIANCDCNMB; 
 end if;
 if P_SEMIANNOTEBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANNOTEBALAMT';
 cur_colvalue:=P_SEMIANNOTEBALAMT;
 DBrec.SEMIANNOTEBALAMT:=P_SEMIANNOTEBALAMT; 
 end if;
 if P_SEMIANWIREAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANWIREAMT';
 cur_colvalue:=P_SEMIANWIREAMT;
 DBrec.SEMIANWIREAMT:=P_SEMIANWIREAMT; 
 end if;
 if P_SEMIANPRINACCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANPRINACCRAMT';
 cur_colvalue:=P_SEMIANPRINACCRAMT;
 DBrec.SEMIANPRINACCRAMT:=P_SEMIANPRINACCRAMT; 
 end if;
 if P_SEMIANINTACCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANINTACCRAMT';
 cur_colvalue:=P_SEMIANINTACCRAMT;
 DBrec.SEMIANINTACCRAMT:=P_SEMIANINTACCRAMT; 
 end if;
 if P_SEMIANOPENGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANOPENGNTYAMT';
 cur_colvalue:=P_SEMIANOPENGNTYAMT;
 DBrec.SEMIANOPENGNTYAMT:=P_SEMIANOPENGNTYAMT; 
 end if;
 if P_SEMIANOPENGNTYERRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANOPENGNTYERRAMT';
 cur_colvalue:=P_SEMIANOPENGNTYERRAMT;
 DBrec.SEMIANOPENGNTYERRAMT:=P_SEMIANOPENGNTYERRAMT; 
 end if;
 if P_SEMIANOPENGNTYREMAINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANOPENGNTYREMAINAMT';
 cur_colvalue:=P_SEMIANOPENGNTYREMAINAMT;
 DBrec.SEMIANOPENGNTYREMAINAMT:=P_SEMIANOPENGNTYREMAINAMT; 
 end if;
 if P_SEMIANPIPYMTAFT6MOAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANPIPYMTAFT6MOAMT';
 cur_colvalue:=P_SEMIANPIPYMTAFT6MOAMT;
 DBrec.SEMIANPIPYMTAFT6MOAMT:=P_SEMIANPIPYMTAFT6MOAMT; 
 end if;
 if P_SEMIANPISHORTOVRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANPISHORTOVRAMT';
 cur_colvalue:=P_SEMIANPISHORTOVRAMT;
 DBrec.SEMIANPISHORTOVRAMT:=P_SEMIANPISHORTOVRAMT; 
 end if;
 if P_SEMIANESCROWPRIORPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANESCROWPRIORPYMTAMT';
 cur_colvalue:=P_SEMIANESCROWPRIORPYMTAMT;
 DBrec.SEMIANESCROWPRIORPYMTAMT:=P_SEMIANESCROWPRIORPYMTAMT; 
 end if;
 if P_SEMIANDBENTRBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANDBENTRBALAMT';
 cur_colvalue:=P_SEMIANDBENTRBALAMT;
 DBrec.SEMIANDBENTRBALAMT:=P_SEMIANDBENTRBALAMT; 
 end if;
 if P_SEMIANPREPAYPREMAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANPREPAYPREMAMT';
 cur_colvalue:=P_SEMIANPREPAYPREMAMT;
 DBrec.SEMIANPREPAYPREMAMT:=P_SEMIANPREPAYPREMAMT; 
 end if;
 if P_SEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANAMT';
 cur_colvalue:=P_SEMIANAMT;
 DBrec.SEMIANAMT:=P_SEMIANAMT; 
 end if;
 if P_SEMIANSBADUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANSBADUEFROMBORRAMT';
 cur_colvalue:=P_SEMIANSBADUEFROMBORRAMT;
 DBrec.SEMIANSBADUEFROMBORRAMT:=P_SEMIANSBADUEFROMBORRAMT; 
 end if;
 if P_SEMIANSBABEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANSBABEHINDAMT';
 cur_colvalue:=P_SEMIANSBABEHINDAMT;
 DBrec.SEMIANSBABEHINDAMT:=P_SEMIANSBABEHINDAMT; 
 end if;
 if P_SEMIANSBAACCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANSBAACCRAMT';
 cur_colvalue:=P_SEMIANSBAACCRAMT;
 DBrec.SEMIANSBAACCRAMT:=P_SEMIANSBAACCRAMT; 
 end if;
 if P_SEMIANSBAPRJCTDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANSBAPRJCTDAMT';
 cur_colvalue:=P_SEMIANSBAPRJCTDAMT;
 DBrec.SEMIANSBAPRJCTDAMT:=P_SEMIANSBAPRJCTDAMT; 
 end if;
 if P_SEMIANCSADUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCSADUEFROMBORRAMT';
 cur_colvalue:=P_SEMIANCSADUEFROMBORRAMT;
 DBrec.SEMIANCSADUEFROMBORRAMT:=P_SEMIANCSADUEFROMBORRAMT; 
 end if;
 if P_SEMIANCSABEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCSABEHINDAMT';
 cur_colvalue:=P_SEMIANCSABEHINDAMT;
 DBrec.SEMIANCSABEHINDAMT:=P_SEMIANCSABEHINDAMT; 
 end if;
 if P_SEMIANCSAACCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCSAACCRAMT';
 cur_colvalue:=P_SEMIANCSAACCRAMT;
 DBrec.SEMIANCSAACCRAMT:=P_SEMIANCSAACCRAMT; 
 end if;
 if P_SEMIANCSAPRJCTDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCSAPRJCTDAMT';
 cur_colvalue:=P_SEMIANCSAPRJCTDAMT;
 DBrec.SEMIANCSAPRJCTDAMT:=P_SEMIANCSAPRJCTDAMT; 
 end if;
 if P_SEMIANCDCDUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCDCDUEFROMBORRAMT';
 cur_colvalue:=P_SEMIANCDCDUEFROMBORRAMT;
 DBrec.SEMIANCDCDUEFROMBORRAMT:=P_SEMIANCDCDUEFROMBORRAMT; 
 end if;
 if P_SEMIANCDCBEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCDCBEHINDAMT';
 cur_colvalue:=P_SEMIANCDCBEHINDAMT;
 DBrec.SEMIANCDCBEHINDAMT:=P_SEMIANCDCBEHINDAMT; 
 end if;
 if P_SEMIANCDCACCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANCDCACCRAMT';
 cur_colvalue:=P_SEMIANCDCACCRAMT;
 DBrec.SEMIANCDCACCRAMT:=P_SEMIANCDCACCRAMT; 
 end if;
 if P_SEMIANACCRINTONDELAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANACCRINTONDELAMT';
 cur_colvalue:=P_SEMIANACCRINTONDELAMT;
 DBrec.SEMIANACCRINTONDELAMT:=P_SEMIANACCRINTONDELAMT; 
 end if;
 if P_SEMIANLATEFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANLATEFEEAMT';
 cur_colvalue:=P_SEMIANLATEFEEAMT;
 DBrec.SEMIANLATEFEEAMT:=P_SEMIANLATEFEEAMT; 
 end if;
 if P_SEMIANPROOFAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANPROOFAMT';
 cur_colvalue:=P_SEMIANPROOFAMT;
 DBrec.SEMIANPROOFAMT:=P_SEMIANPROOFAMT; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 if DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CREATDT';
 cur_colvalue:=P_CREATDT;
 DBrec.CREATDT:=P_CREATDT;
 cur_colname:='P_CREATUSERID';
 cur_colvalue:=P_CREATUSERID;
 DBrec.CREATUSERID:=P_CREATUSERID;
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPMFWIREPOSTINGTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,LOANNMB=DBrec.LOANNMB
 ,BORRFEEAMT=DBrec.BORRFEEAMT
 ,BORRPAIDTHRUDT=DBrec.BORRPAIDTHRUDT
 ,CSAFEEAMT=DBrec.CSAFEEAMT
 ,CSAPAIDTHRUDT=DBrec.CSAPAIDTHRUDT
 ,CDCFEEAPPLAMT=DBrec.CDCFEEAPPLAMT
 ,CDCFEEDISBAMT=DBrec.CDCFEEDISBAMT
 ,CDCDISBAMT=DBrec.CDCDISBAMT
 ,CDCPAIDTHRUDT=DBrec.CDCPAIDTHRUDT
 ,INTPAIDTHRUDT=DBrec.INTPAIDTHRUDT
 ,ADJTOAPPLINTAMT=DBrec.ADJTOAPPLINTAMT
 ,ADJTOAPPLPRINAMT=DBrec.ADJTOAPPLPRINAMT
 ,LATEFEEADJAMT=DBrec.LATEFEEADJAMT
 ,UNALLOCAMT=DBrec.UNALLOCAMT
 ,CURBALAMT=DBrec.CURBALAMT
 ,PREPAYAMT=DBrec.PREPAYAMT
 ,WIRECMNT=DBrec.WIRECMNT
 ,ADJTYP=DBrec.ADJTYP
 ,OVRWRITECD=DBrec.OVRWRITECD
 ,POSTINGTYP=DBrec.POSTINGTYP
 ,POSTINGSCHDLDT=DBrec.POSTINGSCHDLDT
 ,POSTINGACTUALDT=DBrec.POSTINGACTUALDT
 ,WIREPOSTINGID=DBrec.WIREPOSTINGID
 ,TRANSID=DBrec.TRANSID
 ,MFSTATCD=DBrec.MFSTATCD
 ,ACHDEL=DBrec.ACHDEL
 ,OVRPAYAMT=DBrec.OVRPAYAMT
 ,CDCREPDAMT=DBrec.CDCREPDAMT
 ,CSAREPDAMT=DBrec.CSAREPDAMT
 ,REPDAMT=DBrec.REPDAMT
 ,PREPAYDT=DBrec.PREPAYDT
 ,SEMIANDT=DBrec.SEMIANDT
 ,ZEROBALAMT=DBrec.ZEROBALAMT
 ,SEMIANCDCNMB=DBrec.SEMIANCDCNMB
 ,SEMIANNOTEBALAMT=DBrec.SEMIANNOTEBALAMT
 ,SEMIANWIREAMT=DBrec.SEMIANWIREAMT
 ,SEMIANPRINACCRAMT=DBrec.SEMIANPRINACCRAMT
 ,SEMIANINTACCRAMT=DBrec.SEMIANINTACCRAMT
 ,SEMIANOPENGNTYAMT=DBrec.SEMIANOPENGNTYAMT
 ,SEMIANOPENGNTYERRAMT=DBrec.SEMIANOPENGNTYERRAMT
 ,SEMIANOPENGNTYREMAINAMT=DBrec.SEMIANOPENGNTYREMAINAMT
 ,SEMIANPIPYMTAFT6MOAMT=DBrec.SEMIANPIPYMTAFT6MOAMT
 ,SEMIANPISHORTOVRAMT=DBrec.SEMIANPISHORTOVRAMT
 ,SEMIANESCROWPRIORPYMTAMT=DBrec.SEMIANESCROWPRIORPYMTAMT
 ,SEMIANDBENTRBALAMT=DBrec.SEMIANDBENTRBALAMT
 ,SEMIANPREPAYPREMAMT=DBrec.SEMIANPREPAYPREMAMT
 ,SEMIANAMT=DBrec.SEMIANAMT
 ,SEMIANSBADUEFROMBORRAMT=DBrec.SEMIANSBADUEFROMBORRAMT
 ,SEMIANSBABEHINDAMT=DBrec.SEMIANSBABEHINDAMT
 ,SEMIANSBAACCRAMT=DBrec.SEMIANSBAACCRAMT
 ,SEMIANSBAPRJCTDAMT=DBrec.SEMIANSBAPRJCTDAMT
 ,SEMIANCSADUEFROMBORRAMT=DBrec.SEMIANCSADUEFROMBORRAMT
 ,SEMIANCSABEHINDAMT=DBrec.SEMIANCSABEHINDAMT
 ,SEMIANCSAACCRAMT=DBrec.SEMIANCSAACCRAMT
 ,SEMIANCSAPRJCTDAMT=DBrec.SEMIANCSAPRJCTDAMT
 ,SEMIANCDCDUEFROMBORRAMT=DBrec.SEMIANCDCDUEFROMBORRAMT
 ,SEMIANCDCBEHINDAMT=DBrec.SEMIANCDCBEHINDAMT
 ,SEMIANCDCACCRAMT=DBrec.SEMIANCDCACCRAMT
 ,SEMIANACCRINTONDELAMT=DBrec.SEMIANACCRINTONDELAMT
 ,SEMIANLATEFEEAMT=DBrec.SEMIANLATEFEEAMT
 ,SEMIANPROOFAMT=DBrec.SEMIANPROOFAMT
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 ,CREATUSERID=DBrec.CREATUSERID
 ,CREATDT=DBrec.CREATDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.PRPMFWIREPOSTINGTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFWIREPOSTINGUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPMFWIREPOSTINGUPDTSP',p_userid,4,
 logtxt1=>'PRPMFWIREPOSTINGUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'PRPMFWIREPOSTINGUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB; l_TRANSID:=P_TRANSID; l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(WIREPOSTINGID)=('||P_WIREPOSTINGID||')';
 begin
 select rowid into rec_rowid from STGCSA.PRPMFWIREPOSTINGTBL where 1=1
 and WIREPOSTINGID=P_WIREPOSTINGID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFWIREPOSTINGTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(TRANSID)=('||P_TRANSID||')';
 begin
 for r1 in (select rowid from STGCSA.PRPMFWIREPOSTINGTBL a where 1=1 
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.PRPMFWIREPOSTINGTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFWIREPOSTINGTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(CREATDT)=('||P_CREATDT||')'||' and '||'(CREATUSERID)=('||P_CREATUSERID||')';
 begin
 for r2 in (select rowid from STGCSA.PRPMFWIREPOSTINGTBL a where 1=1 
 and CREATDT=P_CREATDT
 and CREATUSERID=P_CREATUSERID
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.PRPMFWIREPOSTINGTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFWIREPOSTINGTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFWIREPOSTINGUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'PRPMFWIREPOSTINGUPDTSP');
 else
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 p_errmsg:=p_errmsg||' in PRPMFWIREPOSTINGUPDTSP build 2018-10-08 15:46:42';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFWIREPOSTINGUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRPMFWIREPOSTINGUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFWIREPOSTINGUPDTSP build 2018-10-08 15:46:42';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFWIREPOSTINGUPDTSP',force_log_entry=>true);
 --
 END PRPMFWIREPOSTINGUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


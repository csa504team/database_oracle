<!--- Saved 10/09/2018 11:29:02. --->
PROCEDURE CHRTOPNGNTYSALLINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CHRTOPNGNTYSALLID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_PREPAYDT DATE:=null
 ,p_SEMIANDT DATE:=null
 ,p_TOTOPENGNTYAMT VARCHAR2:=null
 ,p_ERROPENGNTYAMT VARCHAR2:=null
 ,p_REMAINDEROPENGNTYAMT VARCHAR2:=null
 ,p_OPENGNTYDIFFAMT VARCHAR2:=null
 ,p_PROOFAMT VARCHAR2:=null
 ,p_OPNGNTYSCMNT VARCHAR2:=null
 ,p_OPENGNTYCLSDT DATE:=null
 ,p_OPENGNTYPAIDDT DATE:=null
 ,p_TOBECLSFLAG VARCHAR2:=null
 ,p_TOBEPAIDFLAG VARCHAR2:=null
 ,p_GNTYAMT VARCHAR2:=null
 ,p_OPENGNTYVALIDATION VARCHAR2:=null
 ,p_UNALLOCAMT VARCHAR2:=null
 ,p_UPDTUNALLOCPORTOFGUARANTEAMT VARCHAR2:=null
 ,p_ACCRINTONDELNQLOANAMT VARCHAR2:=null
 ,p_UPDTACCRINTDELNQLOANAMT VARCHAR2:=null
 ,p_ACCRINTPAIDDT DATE:=null
 ,p_REPORTDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:34
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:34
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CHRTOPNGNTYSALLTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.CHRTOPNGNTYSALLTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize CHRTOPNGNTYSALLID from sequence CHRTOPNGNTYSALLIDSEQ
 Function NEXTVAL_CHRTOPNGNTYSALLIDSEQ return number is
 N number;
 begin
 select CHRTOPNGNTYSALLIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint CHRTOPNGNTYSALLINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init CHRTOPNGNTYSALLINSTSP',p_userid,4,
 logtxt1=>'CHRTOPNGNTYSALLINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'CHRTOPNGNTYSALLINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CHRTOPNGNTYSALLID';
 cur_colvalue:=P_CHRTOPNGNTYSALLID;
 if P_CHRTOPNGNTYSALLID is null then
 
 new_rec.CHRTOPNGNTYSALLID:=NEXTVAL_CHRTOPNGNTYSALLIDSEQ();
 
 else 
 new_rec.CHRTOPNGNTYSALLID:=P_CHRTOPNGNTYSALLID;
 end if;
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 
 new_rec.LOANNMB:=rpad(' ',10);
 
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 if P_PREPAYDT is null then
 
 new_rec.PREPAYDT:=jan_1_1900;
 
 else 
 new_rec.PREPAYDT:=P_PREPAYDT;
 end if;
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 if P_SEMIANDT is null then
 
 new_rec.SEMIANDT:=jan_1_1900;
 
 else 
 new_rec.SEMIANDT:=P_SEMIANDT;
 end if;
 cur_colname:='P_TOTOPENGNTYAMT';
 cur_colvalue:=P_TOTOPENGNTYAMT;
 if P_TOTOPENGNTYAMT is null then
 
 new_rec.TOTOPENGNTYAMT:=0;
 
 else 
 new_rec.TOTOPENGNTYAMT:=P_TOTOPENGNTYAMT;
 end if;
 cur_colname:='P_ERROPENGNTYAMT';
 cur_colvalue:=P_ERROPENGNTYAMT;
 if P_ERROPENGNTYAMT is null then
 
 new_rec.ERROPENGNTYAMT:=0;
 
 else 
 new_rec.ERROPENGNTYAMT:=P_ERROPENGNTYAMT;
 end if;
 cur_colname:='P_REMAINDEROPENGNTYAMT';
 cur_colvalue:=P_REMAINDEROPENGNTYAMT;
 if P_REMAINDEROPENGNTYAMT is null then
 
 new_rec.REMAINDEROPENGNTYAMT:=0;
 
 else 
 new_rec.REMAINDEROPENGNTYAMT:=P_REMAINDEROPENGNTYAMT;
 end if;
 cur_colname:='P_OPENGNTYDIFFAMT';
 cur_colvalue:=P_OPENGNTYDIFFAMT;
 if P_OPENGNTYDIFFAMT is null then
 
 new_rec.OPENGNTYDIFFAMT:=0;
 
 else 
 new_rec.OPENGNTYDIFFAMT:=P_OPENGNTYDIFFAMT;
 end if;
 cur_colname:='P_PROOFAMT';
 cur_colvalue:=P_PROOFAMT;
 if P_PROOFAMT is null then
 
 new_rec.PROOFAMT:=0;
 
 else 
 new_rec.PROOFAMT:=P_PROOFAMT;
 end if;
 cur_colname:='P_OPNGNTYSCMNT';
 cur_colvalue:=P_OPNGNTYSCMNT;
 if P_OPNGNTYSCMNT is null then
 
 new_rec.OPNGNTYSCMNT:=' ';
 
 else 
 new_rec.OPNGNTYSCMNT:=P_OPNGNTYSCMNT;
 end if;
 cur_colname:='P_OPENGNTYCLSDT';
 cur_colvalue:=P_OPENGNTYCLSDT;
 if P_OPENGNTYCLSDT is null then
 
 new_rec.OPENGNTYCLSDT:=jan_1_1900;
 
 else 
 new_rec.OPENGNTYCLSDT:=P_OPENGNTYCLSDT;
 end if;
 cur_colname:='P_OPENGNTYPAIDDT';
 cur_colvalue:=P_OPENGNTYPAIDDT;
 if P_OPENGNTYPAIDDT is null then
 
 new_rec.OPENGNTYPAIDDT:=jan_1_1900;
 
 else 
 new_rec.OPENGNTYPAIDDT:=P_OPENGNTYPAIDDT;
 end if;
 cur_colname:='P_TOBECLSFLAG';
 cur_colvalue:=P_TOBECLSFLAG;
 if P_TOBECLSFLAG is null then
 
 new_rec.TOBECLSFLAG:=0;
 
 else 
 new_rec.TOBECLSFLAG:=P_TOBECLSFLAG;
 end if;
 cur_colname:='P_TOBEPAIDFLAG';
 cur_colvalue:=P_TOBEPAIDFLAG;
 if P_TOBEPAIDFLAG is null then
 
 new_rec.TOBEPAIDFLAG:=0;
 
 else 
 new_rec.TOBEPAIDFLAG:=P_TOBEPAIDFLAG;
 end if;
 cur_colname:='P_GNTYAMT';
 cur_colvalue:=P_GNTYAMT;
 if P_GNTYAMT is null then
 
 new_rec.GNTYAMT:=0;
 
 else 
 new_rec.GNTYAMT:=P_GNTYAMT;
 end if;
 cur_colname:='P_OPENGNTYVALIDATION';
 cur_colvalue:=P_OPENGNTYVALIDATION;
 if P_OPENGNTYVALIDATION is null then
 
 new_rec.OPENGNTYVALIDATION:=0;
 
 else 
 new_rec.OPENGNTYVALIDATION:=P_OPENGNTYVALIDATION;
 end if;
 cur_colname:='P_UNALLOCAMT';
 cur_colvalue:=P_UNALLOCAMT;
 if P_UNALLOCAMT is null then
 
 new_rec.UNALLOCAMT:=0;
 
 else 
 new_rec.UNALLOCAMT:=P_UNALLOCAMT;
 end if;
 cur_colname:='P_UPDTUNALLOCPORTOFGUARANTEAMT';
 cur_colvalue:=P_UPDTUNALLOCPORTOFGUARANTEAMT;
 if P_UPDTUNALLOCPORTOFGUARANTEAMT is null then
 
 new_rec.UPDTUNALLOCPORTOFGUARANTEAMT:=0;
 
 else 
 new_rec.UPDTUNALLOCPORTOFGUARANTEAMT:=P_UPDTUNALLOCPORTOFGUARANTEAMT;
 end if;
 cur_colname:='P_ACCRINTONDELNQLOANAMT';
 cur_colvalue:=P_ACCRINTONDELNQLOANAMT;
 if P_ACCRINTONDELNQLOANAMT is null then
 
 new_rec.ACCRINTONDELNQLOANAMT:=0;
 
 else 
 new_rec.ACCRINTONDELNQLOANAMT:=P_ACCRINTONDELNQLOANAMT;
 end if;
 cur_colname:='P_UPDTACCRINTDELNQLOANAMT';
 cur_colvalue:=P_UPDTACCRINTDELNQLOANAMT;
 if P_UPDTACCRINTDELNQLOANAMT is null then
 
 new_rec.UPDTACCRINTDELNQLOANAMT:=0;
 
 else 
 new_rec.UPDTACCRINTDELNQLOANAMT:=P_UPDTACCRINTDELNQLOANAMT;
 end if;
 cur_colname:='P_ACCRINTPAIDDT';
 cur_colvalue:=P_ACCRINTPAIDDT;
 if P_ACCRINTPAIDDT is null then
 
 new_rec.ACCRINTPAIDDT:=jan_1_1900;
 
 else 
 new_rec.ACCRINTPAIDDT:=P_ACCRINTPAIDDT;
 end if;
 cur_colname:='P_REPORTDT';
 cur_colvalue:=P_REPORTDT;
 if P_REPORTDT is null then
 
 new_rec.REPORTDT:=jan_1_1900;
 
 else 
 new_rec.REPORTDT:=P_REPORTDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CHRTOPNGNTYSALLID)=('||new_rec.CHRTOPNGNTYSALLID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.CHRTOPNGNTYSALLTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End CHRTOPNGNTYSALLINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'CHRTOPNGNTYSALLINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in CHRTOPNGNTYSALLINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:34';
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'CHRTOPNGNTYSALLINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CHRTOPNGNTYSALLINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:34';
 ROLLBACK TO CHRTOPNGNTYSALLINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'CHRTOPNGNTYSALLINSTSP',force_log_entry=>TRUE);
 --
 END CHRTOPNGNTYSALLINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


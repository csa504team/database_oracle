<!--- Saved 10/09/2018 11:29:10. --->
PROCEDURE SOFVCDCMSTRINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCCERTNMB CHAR:=null
 ,p_CDCPOLKNMB CHAR:=null
 ,p_CDCNM VARCHAR2:=null
 ,p_CDCMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCMAILADRCTYNM VARCHAR2:=null
 ,p_CDCMAILADRSTCD CHAR:=null
 ,p_CDCMAILADRZIPCD CHAR:=null
 ,p_CDCMAILADRZIP4CD CHAR:=null
 ,p_CDCCNTCT VARCHAR2:=null
 ,p_CDCAREACD CHAR:=null
 ,p_CDCEXCH CHAR:=null
 ,p_CDCEXTN CHAR:=null
 ,p_CDCSTATCD CHAR:=null
 ,p_CDCTAXID CHAR:=null
 ,p_CDCSETUPDT DATE:=null
 ,p_CDCLMAINTNDT DATE:=null
 ,p_CDCTOTDOLLRDBENTRAMT VARCHAR2:=null
 ,p_CDCTOTNMBPART VARCHAR2:=null
 ,p_CDCTOTPAIDDBENTRAMT VARCHAR2:=null
 ,p_CDCHLDPYMT CHAR:=null
 ,p_CDCDIST CHAR:=null
 ,p_CDCMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHBNK VARCHAR2:=null
 ,p_CDCACHMAILADRSTR1NM VARCHAR2:=null
 ,p_CDCACHMAILADRSTR2NM VARCHAR2:=null
 ,p_CDCACHMAILADRCTYNM VARCHAR2:=null
 ,p_CDCACHMAILADRSTCD CHAR:=null
 ,p_CDCACHMAILADRZIPCD CHAR:=null
 ,p_CDCACHMAILADRZIP4CD CHAR:=null
 ,p_CDCACHBR CHAR:=null
 ,p_CDCACHACCTTYP CHAR:=null
 ,p_CDCACHACCTNMB VARCHAR2:=null
 ,p_CDCACHROUTSYM CHAR:=null
 ,p_CDCACHTRANSCD CHAR:=null
 ,p_CDCACHBNKIDNMB VARCHAR2:=null
 ,p_CDCACHDEPSTR VARCHAR2:=null
 ,p_CDCACHSIGNDT DATE:=null
 ,p_CDCPRENOTETESTDT DATE:=null
 ,p_CDCACHPRENTIND CHAR:=null
 ,p_CDCCLSBALAMT VARCHAR2:=null
 ,p_CDCCLSBALDT DATE:=null
 ,p_CDCNETPREPAYAMT VARCHAR2:=null
 ,p_CDCFAXAREACD CHAR:=null
 ,p_CDCFAXEXCH CHAR:=null
 ,p_CDCFAXEXTN CHAR:=null
 ,p_CDCPREPAYCLSBALAMT VARCHAR2:=null
 ,p_CDCNAMADDRID CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:18
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:47:18
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVCDCMSTRTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVCDCMSTRTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVCDCMSTRINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVCDCMSTRINSTSP',p_userid,4,
 logtxt1=>'SOFVCDCMSTRINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVCDCMSTRINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVCDCMSTRTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CDCREGNCD';
 cur_colvalue:=P_CDCREGNCD;
 if P_CDCREGNCD is null then
 
 new_rec.CDCREGNCD:=rpad(' ',2);
 
 else 
 new_rec.CDCREGNCD:=P_CDCREGNCD;
 end if;
 cur_colname:='P_CDCCERTNMB';
 cur_colvalue:=P_CDCCERTNMB;
 if P_CDCCERTNMB is null then
 
 new_rec.CDCCERTNMB:=rpad(' ',4);
 
 else 
 new_rec.CDCCERTNMB:=P_CDCCERTNMB;
 end if;
 cur_colname:='P_CDCPOLKNMB';
 cur_colvalue:=P_CDCPOLKNMB;
 if P_CDCPOLKNMB is null then
 
 new_rec.CDCPOLKNMB:=rpad(' ',7);
 
 else 
 new_rec.CDCPOLKNMB:=P_CDCPOLKNMB;
 end if;
 cur_colname:='P_CDCNM';
 cur_colvalue:=P_CDCNM;
 if P_CDCNM is null then
 
 new_rec.CDCNM:=' ';
 
 else 
 new_rec.CDCNM:=P_CDCNM;
 end if;
 cur_colname:='P_CDCMAILADRSTR1NM';
 cur_colvalue:=P_CDCMAILADRSTR1NM;
 if P_CDCMAILADRSTR1NM is null then
 
 new_rec.CDCMAILADRSTR1NM:=' ';
 
 else 
 new_rec.CDCMAILADRSTR1NM:=P_CDCMAILADRSTR1NM;
 end if;
 cur_colname:='P_CDCMAILADRCTYNM';
 cur_colvalue:=P_CDCMAILADRCTYNM;
 if P_CDCMAILADRCTYNM is null then
 
 new_rec.CDCMAILADRCTYNM:=' ';
 
 else 
 new_rec.CDCMAILADRCTYNM:=P_CDCMAILADRCTYNM;
 end if;
 cur_colname:='P_CDCMAILADRSTCD';
 cur_colvalue:=P_CDCMAILADRSTCD;
 if P_CDCMAILADRSTCD is null then
 
 new_rec.CDCMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.CDCMAILADRSTCD:=P_CDCMAILADRSTCD;
 end if;
 cur_colname:='P_CDCMAILADRZIPCD';
 cur_colvalue:=P_CDCMAILADRZIPCD;
 if P_CDCMAILADRZIPCD is null then
 
 new_rec.CDCMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.CDCMAILADRZIPCD:=P_CDCMAILADRZIPCD;
 end if;
 cur_colname:='P_CDCMAILADRZIP4CD';
 cur_colvalue:=P_CDCMAILADRZIP4CD;
 if P_CDCMAILADRZIP4CD is null then
 
 new_rec.CDCMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.CDCMAILADRZIP4CD:=P_CDCMAILADRZIP4CD;
 end if;
 cur_colname:='P_CDCCNTCT';
 cur_colvalue:=P_CDCCNTCT;
 if P_CDCCNTCT is null then
 
 new_rec.CDCCNTCT:=' ';
 
 else 
 new_rec.CDCCNTCT:=P_CDCCNTCT;
 end if;
 cur_colname:='P_CDCAREACD';
 cur_colvalue:=P_CDCAREACD;
 if P_CDCAREACD is null then
 
 new_rec.CDCAREACD:=rpad(' ',3);
 
 else 
 new_rec.CDCAREACD:=P_CDCAREACD;
 end if;
 cur_colname:='P_CDCEXCH';
 cur_colvalue:=P_CDCEXCH;
 if P_CDCEXCH is null then
 
 new_rec.CDCEXCH:=rpad(' ',3);
 
 else 
 new_rec.CDCEXCH:=P_CDCEXCH;
 end if;
 cur_colname:='P_CDCEXTN';
 cur_colvalue:=P_CDCEXTN;
 if P_CDCEXTN is null then
 
 new_rec.CDCEXTN:=rpad(' ',4);
 
 else 
 new_rec.CDCEXTN:=P_CDCEXTN;
 end if;
 cur_colname:='P_CDCSTATCD';
 cur_colvalue:=P_CDCSTATCD;
 if P_CDCSTATCD is null then
 
 new_rec.CDCSTATCD:=rpad(' ',2);
 
 else 
 new_rec.CDCSTATCD:=P_CDCSTATCD;
 end if;
 cur_colname:='P_CDCTAXID';
 cur_colvalue:=P_CDCTAXID;
 if P_CDCTAXID is null then
 
 new_rec.CDCTAXID:=rpad(' ',10);
 
 else 
 new_rec.CDCTAXID:=P_CDCTAXID;
 end if;
 cur_colname:='P_CDCSETUPDT';
 cur_colvalue:=P_CDCSETUPDT;
 if P_CDCSETUPDT is null then
 
 new_rec.CDCSETUPDT:=jan_1_1900;
 
 else 
 new_rec.CDCSETUPDT:=P_CDCSETUPDT;
 end if;
 cur_colname:='P_CDCLMAINTNDT';
 cur_colvalue:=P_CDCLMAINTNDT;
 if P_CDCLMAINTNDT is null then
 
 new_rec.CDCLMAINTNDT:=jan_1_1900;
 
 else 
 new_rec.CDCLMAINTNDT:=P_CDCLMAINTNDT;
 end if;
 cur_colname:='P_CDCTOTDOLLRDBENTRAMT';
 cur_colvalue:=P_CDCTOTDOLLRDBENTRAMT;
 if P_CDCTOTDOLLRDBENTRAMT is null then
 
 new_rec.CDCTOTDOLLRDBENTRAMT:=0;
 
 else 
 new_rec.CDCTOTDOLLRDBENTRAMT:=P_CDCTOTDOLLRDBENTRAMT;
 end if;
 cur_colname:='P_CDCTOTNMBPART';
 cur_colvalue:=P_CDCTOTNMBPART;
 if P_CDCTOTNMBPART is null then
 
 new_rec.CDCTOTNMBPART:=0;
 
 else 
 new_rec.CDCTOTNMBPART:=P_CDCTOTNMBPART;
 end if;
 cur_colname:='P_CDCTOTPAIDDBENTRAMT';
 cur_colvalue:=P_CDCTOTPAIDDBENTRAMT;
 if P_CDCTOTPAIDDBENTRAMT is null then
 
 new_rec.CDCTOTPAIDDBENTRAMT:=0;
 
 else 
 new_rec.CDCTOTPAIDDBENTRAMT:=P_CDCTOTPAIDDBENTRAMT;
 end if;
 cur_colname:='P_CDCHLDPYMT';
 cur_colvalue:=P_CDCHLDPYMT;
 if P_CDCHLDPYMT is null then
 
 new_rec.CDCHLDPYMT:=rpad(' ',1);
 
 else 
 new_rec.CDCHLDPYMT:=P_CDCHLDPYMT;
 end if;
 cur_colname:='P_CDCDIST';
 cur_colvalue:=P_CDCDIST;
 if P_CDCDIST is null then
 
 new_rec.CDCDIST:=rpad(' ',5);
 
 else 
 new_rec.CDCDIST:=P_CDCDIST;
 end if;
 cur_colname:='P_CDCMAILADRSTR2NM';
 cur_colvalue:=P_CDCMAILADRSTR2NM;
 if P_CDCMAILADRSTR2NM is null then
 
 new_rec.CDCMAILADRSTR2NM:=' ';
 
 else 
 new_rec.CDCMAILADRSTR2NM:=P_CDCMAILADRSTR2NM;
 end if;
 cur_colname:='P_CDCACHBNK';
 cur_colvalue:=P_CDCACHBNK;
 if P_CDCACHBNK is null then
 
 new_rec.CDCACHBNK:=' ';
 
 else 
 new_rec.CDCACHBNK:=P_CDCACHBNK;
 end if;
 cur_colname:='P_CDCACHMAILADRSTR1NM';
 cur_colvalue:=P_CDCACHMAILADRSTR1NM;
 if P_CDCACHMAILADRSTR1NM is null then
 
 new_rec.CDCACHMAILADRSTR1NM:=' ';
 
 else 
 new_rec.CDCACHMAILADRSTR1NM:=P_CDCACHMAILADRSTR1NM;
 end if;
 cur_colname:='P_CDCACHMAILADRSTR2NM';
 cur_colvalue:=P_CDCACHMAILADRSTR2NM;
 if P_CDCACHMAILADRSTR2NM is null then
 
 new_rec.CDCACHMAILADRSTR2NM:=' ';
 
 else 
 new_rec.CDCACHMAILADRSTR2NM:=P_CDCACHMAILADRSTR2NM;
 end if;
 cur_colname:='P_CDCACHMAILADRCTYNM';
 cur_colvalue:=P_CDCACHMAILADRCTYNM;
 if P_CDCACHMAILADRCTYNM is null then
 
 new_rec.CDCACHMAILADRCTYNM:=' ';
 
 else 
 new_rec.CDCACHMAILADRCTYNM:=P_CDCACHMAILADRCTYNM;
 end if;
 cur_colname:='P_CDCACHMAILADRSTCD';
 cur_colvalue:=P_CDCACHMAILADRSTCD;
 if P_CDCACHMAILADRSTCD is null then
 
 new_rec.CDCACHMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.CDCACHMAILADRSTCD:=P_CDCACHMAILADRSTCD;
 end if;
 cur_colname:='P_CDCACHMAILADRZIPCD';
 cur_colvalue:=P_CDCACHMAILADRZIPCD;
 if P_CDCACHMAILADRZIPCD is null then
 
 new_rec.CDCACHMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.CDCACHMAILADRZIPCD:=P_CDCACHMAILADRZIPCD;
 end if;
 cur_colname:='P_CDCACHMAILADRZIP4CD';
 cur_colvalue:=P_CDCACHMAILADRZIP4CD;
 if P_CDCACHMAILADRZIP4CD is null then
 
 new_rec.CDCACHMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.CDCACHMAILADRZIP4CD:=P_CDCACHMAILADRZIP4CD;
 end if;
 cur_colname:='P_CDCACHBR';
 cur_colvalue:=P_CDCACHBR;
 if P_CDCACHBR is null then
 
 new_rec.CDCACHBR:=rpad(' ',15);
 
 else 
 new_rec.CDCACHBR:=P_CDCACHBR;
 end if;
 cur_colname:='P_CDCACHACCTTYP';
 cur_colvalue:=P_CDCACHACCTTYP;
 if P_CDCACHACCTTYP is null then
 
 new_rec.CDCACHACCTTYP:=rpad(' ',1);
 
 else 
 new_rec.CDCACHACCTTYP:=P_CDCACHACCTTYP;
 end if;
 cur_colname:='P_CDCACHACCTNMB';
 cur_colvalue:=P_CDCACHACCTNMB;
 if P_CDCACHACCTNMB is null then
 
 new_rec.CDCACHACCTNMB:=' ';
 
 else 
 new_rec.CDCACHACCTNMB:=P_CDCACHACCTNMB;
 end if;
 cur_colname:='P_CDCACHROUTSYM';
 cur_colvalue:=P_CDCACHROUTSYM;
 if P_CDCACHROUTSYM is null then
 
 new_rec.CDCACHROUTSYM:=rpad(' ',15);
 
 else 
 new_rec.CDCACHROUTSYM:=P_CDCACHROUTSYM;
 end if;
 cur_colname:='P_CDCACHTRANSCD';
 cur_colvalue:=P_CDCACHTRANSCD;
 if P_CDCACHTRANSCD is null then
 
 new_rec.CDCACHTRANSCD:=rpad(' ',15);
 
 else 
 new_rec.CDCACHTRANSCD:=P_CDCACHTRANSCD;
 end if;
 cur_colname:='P_CDCACHBNKIDNMB';
 cur_colvalue:=P_CDCACHBNKIDNMB;
 if P_CDCACHBNKIDNMB is null then
 
 new_rec.CDCACHBNKIDNMB:=' ';
 
 else 
 new_rec.CDCACHBNKIDNMB:=P_CDCACHBNKIDNMB;
 end if;
 cur_colname:='P_CDCACHDEPSTR';
 cur_colvalue:=P_CDCACHDEPSTR;
 if P_CDCACHDEPSTR is null then
 
 new_rec.CDCACHDEPSTR:=' ';
 
 else 
 new_rec.CDCACHDEPSTR:=P_CDCACHDEPSTR;
 end if;
 cur_colname:='P_CDCACHSIGNDT';
 cur_colvalue:=P_CDCACHSIGNDT;
 if P_CDCACHSIGNDT is null then
 
 new_rec.CDCACHSIGNDT:=jan_1_1900;
 
 else 
 new_rec.CDCACHSIGNDT:=P_CDCACHSIGNDT;
 end if;
 cur_colname:='P_CDCPRENOTETESTDT';
 cur_colvalue:=P_CDCPRENOTETESTDT;
 if P_CDCPRENOTETESTDT is null then
 
 new_rec.CDCPRENOTETESTDT:=jan_1_1900;
 
 else 
 new_rec.CDCPRENOTETESTDT:=P_CDCPRENOTETESTDT;
 end if;
 cur_colname:='P_CDCACHPRENTIND';
 cur_colvalue:=P_CDCACHPRENTIND;
 if P_CDCACHPRENTIND is null then
 
 new_rec.CDCACHPRENTIND:=rpad(' ',1);
 
 else 
 new_rec.CDCACHPRENTIND:=P_CDCACHPRENTIND;
 end if;
 cur_colname:='P_CDCCLSBALAMT';
 cur_colvalue:=P_CDCCLSBALAMT;
 if P_CDCCLSBALAMT is null then
 
 new_rec.CDCCLSBALAMT:=0;
 
 else 
 new_rec.CDCCLSBALAMT:=P_CDCCLSBALAMT;
 end if;
 cur_colname:='P_CDCCLSBALDT';
 cur_colvalue:=P_CDCCLSBALDT;
 if P_CDCCLSBALDT is null then
 
 new_rec.CDCCLSBALDT:=jan_1_1900;
 
 else 
 new_rec.CDCCLSBALDT:=P_CDCCLSBALDT;
 end if;
 cur_colname:='P_CDCNETPREPAYAMT';
 cur_colvalue:=P_CDCNETPREPAYAMT;
 if P_CDCNETPREPAYAMT is null then
 
 new_rec.CDCNETPREPAYAMT:=0;
 
 else 
 new_rec.CDCNETPREPAYAMT:=P_CDCNETPREPAYAMT;
 end if;
 cur_colname:='P_CDCFAXAREACD';
 cur_colvalue:=P_CDCFAXAREACD;
 if P_CDCFAXAREACD is null then
 
 new_rec.CDCFAXAREACD:=rpad(' ',3);
 
 else 
 new_rec.CDCFAXAREACD:=P_CDCFAXAREACD;
 end if;
 cur_colname:='P_CDCFAXEXCH';
 cur_colvalue:=P_CDCFAXEXCH;
 if P_CDCFAXEXCH is null then
 
 new_rec.CDCFAXEXCH:=rpad(' ',3);
 
 else 
 new_rec.CDCFAXEXCH:=P_CDCFAXEXCH;
 end if;
 cur_colname:='P_CDCFAXEXTN';
 cur_colvalue:=P_CDCFAXEXTN;
 if P_CDCFAXEXTN is null then
 
 new_rec.CDCFAXEXTN:=rpad(' ',4);
 
 else 
 new_rec.CDCFAXEXTN:=P_CDCFAXEXTN;
 end if;
 cur_colname:='P_CDCPREPAYCLSBALAMT';
 cur_colvalue:=P_CDCPREPAYCLSBALAMT;
 if P_CDCPREPAYCLSBALAMT is null then
 
 new_rec.CDCPREPAYCLSBALAMT:=0;
 
 else 
 new_rec.CDCPREPAYCLSBALAMT:=P_CDCPREPAYCLSBALAMT;
 end if;
 cur_colname:='P_CDCNAMADDRID';
 cur_colvalue:=P_CDCNAMADDRID;
 if P_CDCNAMADDRID is null then
 
 new_rec.CDCNAMADDRID:=rpad(' ',6);
 
 else 
 new_rec.CDCNAMADDRID:=P_CDCNAMADDRID;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CDCREGNCD)=('||new_rec.CDCREGNCD||')'||' and '||'(CDCCERTNMB)=('||new_rec.CDCCERTNMB||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVCDCMSTRTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVCDCMSTRINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVCDCMSTRINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVCDCMSTRINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:47:18';
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVCDCMSTRINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVCDCMSTRINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:47:18';
 ROLLBACK TO SOFVCDCMSTRINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVCDCMSTRINSTSP',force_log_entry=>TRUE);
 --
 END SOFVCDCMSTRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


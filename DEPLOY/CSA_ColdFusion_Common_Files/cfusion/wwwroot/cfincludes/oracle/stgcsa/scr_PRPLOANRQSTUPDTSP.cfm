<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPLOANRQSTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_RQSTREF VARCHAR2:=null
 ,p_RQSTCD VARCHAR2:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCNMB CHAR:=null
 ,p_LOANSTAT VARCHAR2:=null
 ,p_BORRNM VARCHAR2:=null
 ,p_PREPAYAMT VARCHAR2:=null
 ,p_PREPAYDT DATE:=null
 ,p_PREPAYMO VARCHAR2:=null
 ,p_PREPAYYRNMB CHAR:=null
 ,p_PREPAYCALCDT DATE:=null
 ,p_SEMIANNDT DATE:=null
 ,p_SEMIANMO VARCHAR2:=null
 ,p_SEMIANNAMT VARCHAR2:=null
 ,p_SEMIANNPYMTAMT VARCHAR2:=null
 ,p_PRINCURAMT VARCHAR2:=null
 ,p_BALAMT VARCHAR2:=null
 ,p_BALCALCAMT VARCHAR2:=null
 ,p_NOTEBALCALCAMT VARCHAR2:=null
 ,p_PIAMT VARCHAR2:=null
 ,p_NOTERTPCT VARCHAR2:=null
 ,p_ISSDT DATE:=null
 ,p_DBENTRBALAMT VARCHAR2:=null
 ,p_PREPAYPREMAMT VARCHAR2:=null
 ,p_POSTINGDT DATE:=null
 ,p_POSTINGMO VARCHAR2:=null
 ,p_POSTINGYRNMB CHAR:=null
 ,p_BUSDAY6DT DATE:=null
 ,p_THIRDTHURSDAY DATE:=null
 ,p_ACTUALRQST VARCHAR2:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_APPVDT DATE:=null
 ,p_PRINNEEDEDAMT VARCHAR2:=null
 ,p_INTNEEDEDAMT VARCHAR2:=null
 ,p_MODELNQ VARCHAR2:=null
 ,p_CURRFEEBALAMT VARCHAR2:=null
 ,p_CSAFEENEEDEDAMT VARCHAR2:=null
 ,p_CDCFEENEEDEDAMT VARCHAR2:=null
 ,p_FEENEEDEDAMT VARCHAR2:=null
 ,p_LATEFEENEEDEDAMT VARCHAR2:=null
 ,p_CSADUEFROMBORRAMT VARCHAR2:=null
 ,p_CDCDUEFROMBORRAMT VARCHAR2:=null
 ,p_DUEFROMBORRAMT VARCHAR2:=null
 ,p_CSABEHINDAMT VARCHAR2:=null
 ,p_CDCBEHINDAMT VARCHAR2:=null
 ,p_BEHINDAMT VARCHAR2:=null
 ,p_BORRFEEAMT VARCHAR2:=null
 ,p_CDCFEEDISBAMT VARCHAR2:=null
 ,p_AMORTCDCAMT VARCHAR2:=null
 ,p_AMORTSBAAMT VARCHAR2:=null
 ,p_AMORTCSAAMT VARCHAR2:=null
 ,p_CDCPCT VARCHAR2:=null
 ,p_PREPAYPCT VARCHAR2:=null
 ,p_FEECALCAMT VARCHAR2:=null
 ,p_CSAFEECALCAMT VARCHAR2:=null
 ,p_CSAFEECALCONLYAMT VARCHAR2:=null
 ,p_CDCFEECALCONLYAMT VARCHAR2:=null
 ,p_FEECALCONLYAMT VARCHAR2:=null
 ,p_FIVEYRADJ VARCHAR2:=null
 ,p_FIVEYRADJCSAAMT VARCHAR2:=null
 ,p_FIVEYRADJCDCAMT VARCHAR2:=null
 ,p_FIVEYRADJSBAAMT VARCHAR2:=null
 ,p_BALCALC1AMT VARCHAR2:=null
 ,p_BALCALC2AMT VARCHAR2:=null
 ,p_BALCALC3AMT VARCHAR2:=null
 ,p_BALCALC4AMT VARCHAR2:=null
 ,p_BALCALC5AMT VARCHAR2:=null
 ,p_BALCALC6AMT VARCHAR2:=null
 ,p_PRINAMT VARCHAR2:=null
 ,p_PRIN2AMT VARCHAR2:=null
 ,p_PRIN3AMT VARCHAR2:=null
 ,p_PRIN4AMT VARCHAR2:=null
 ,p_PRIN5AMT VARCHAR2:=null
 ,p_PRIN6AMT VARCHAR2:=null
 ,p_PRINPROJAMT VARCHAR2:=null
 ,p_PIPROJAMT VARCHAR2:=null
 ,p_INTAMT VARCHAR2:=null
 ,p_INT2AMT VARCHAR2:=null
 ,p_INT3AMT VARCHAR2:=null
 ,p_INT4AMT VARCHAR2:=null
 ,p_INT5AMT VARCHAR2:=null
 ,p_INT6AMT VARCHAR2:=null
 ,p_ESCROWAMT VARCHAR2:=null
 ,p_PI6MOAMT VARCHAR2:=null
 ,p_GFDAMT VARCHAR2:=null
 ,p_OPENGNTYAMT VARCHAR2:=null
 ,p_OPENGNTYERRAMT VARCHAR2:=null
 ,p_OPENGNTYREGAMT VARCHAR2:=null
 ,p_OPENGNTYUNALLOCAMT VARCHAR2:=null
 ,p_UNALLOCAMT VARCHAR2:=null
 ,p_USERLCK VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_CDCSBAFEEDISBAMT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:26
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:46:26
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure PRPLOANRQSTUPDTSP performs UPDATE on STGCSA.PRPLOANRQSTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.PRPLOANRQSTTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.PRPLOANRQSTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.PRPLOANRQSTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_RQSTREF is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_RQSTREF';
 cur_colvalue:=P_RQSTREF;
 DBrec.RQSTREF:=P_RQSTREF; 
 end if;
 if P_RQSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_RQSTCD';
 cur_colvalue:=P_RQSTCD;
 DBrec.RQSTCD:=P_RQSTCD; 
 end if;
 if P_CDCREGNCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCREGNCD';
 cur_colvalue:=P_CDCREGNCD;
 DBrec.CDCREGNCD:=P_CDCREGNCD; 
 end if;
 if P_CDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCNMB';
 cur_colvalue:=P_CDCNMB;
 DBrec.CDCNMB:=P_CDCNMB; 
 end if;
 if P_LOANSTAT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANSTAT';
 cur_colvalue:=P_LOANSTAT;
 DBrec.LOANSTAT:=P_LOANSTAT; 
 end if;
 if P_BORRNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BORRNM';
 cur_colvalue:=P_BORRNM;
 DBrec.BORRNM:=P_BORRNM; 
 end if;
 if P_PREPAYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYAMT';
 cur_colvalue:=P_PREPAYAMT;
 DBrec.PREPAYAMT:=P_PREPAYAMT; 
 end if;
 if P_PREPAYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 DBrec.PREPAYDT:=P_PREPAYDT; 
 end if;
 if P_PREPAYMO is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYMO';
 cur_colvalue:=P_PREPAYMO;
 DBrec.PREPAYMO:=P_PREPAYMO; 
 end if;
 if P_PREPAYYRNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYYRNMB';
 cur_colvalue:=P_PREPAYYRNMB;
 DBrec.PREPAYYRNMB:=P_PREPAYYRNMB; 
 end if;
 if P_PREPAYCALCDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYCALCDT';
 cur_colvalue:=P_PREPAYCALCDT;
 DBrec.PREPAYCALCDT:=P_PREPAYCALCDT; 
 end if;
 if P_SEMIANNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANNDT';
 cur_colvalue:=P_SEMIANNDT;
 DBrec.SEMIANNDT:=P_SEMIANNDT; 
 end if;
 if P_SEMIANMO is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANMO';
 cur_colvalue:=P_SEMIANMO;
 DBrec.SEMIANMO:=P_SEMIANMO; 
 end if;
 if P_SEMIANNAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANNAMT';
 cur_colvalue:=P_SEMIANNAMT;
 DBrec.SEMIANNAMT:=P_SEMIANNAMT; 
 end if;
 if P_SEMIANNPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SEMIANNPYMTAMT';
 cur_colvalue:=P_SEMIANNPYMTAMT;
 DBrec.SEMIANNPYMTAMT:=P_SEMIANNPYMTAMT; 
 end if;
 if P_PRINCURAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRINCURAMT';
 cur_colvalue:=P_PRINCURAMT;
 DBrec.PRINCURAMT:=P_PRINCURAMT; 
 end if;
 if P_BALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALAMT';
 cur_colvalue:=P_BALAMT;
 DBrec.BALAMT:=P_BALAMT; 
 end if;
 if P_BALCALCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALCAMT';
 cur_colvalue:=P_BALCALCAMT;
 DBrec.BALCALCAMT:=P_BALCALCAMT; 
 end if;
 if P_NOTEBALCALCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_NOTEBALCALCAMT';
 cur_colvalue:=P_NOTEBALCALCAMT;
 DBrec.NOTEBALCALCAMT:=P_NOTEBALCALCAMT; 
 end if;
 if P_PIAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PIAMT';
 cur_colvalue:=P_PIAMT;
 DBrec.PIAMT:=P_PIAMT; 
 end if;
 if P_NOTERTPCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_NOTERTPCT';
 cur_colvalue:=P_NOTERTPCT;
 DBrec.NOTERTPCT:=P_NOTERTPCT; 
 end if;
 if P_ISSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ISSDT';
 cur_colvalue:=P_ISSDT;
 DBrec.ISSDT:=P_ISSDT; 
 end if;
 if P_DBENTRBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DBENTRBALAMT';
 cur_colvalue:=P_DBENTRBALAMT;
 DBrec.DBENTRBALAMT:=P_DBENTRBALAMT; 
 end if;
 if P_PREPAYPREMAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYPREMAMT';
 cur_colvalue:=P_PREPAYPREMAMT;
 DBrec.PREPAYPREMAMT:=P_PREPAYPREMAMT; 
 end if;
 if P_POSTINGDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGDT';
 cur_colvalue:=P_POSTINGDT;
 DBrec.POSTINGDT:=P_POSTINGDT; 
 end if;
 if P_POSTINGMO is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGMO';
 cur_colvalue:=P_POSTINGMO;
 DBrec.POSTINGMO:=P_POSTINGMO; 
 end if;
 if P_POSTINGYRNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_POSTINGYRNMB';
 cur_colvalue:=P_POSTINGYRNMB;
 DBrec.POSTINGYRNMB:=P_POSTINGYRNMB; 
 end if;
 if P_BUSDAY6DT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BUSDAY6DT';
 cur_colvalue:=P_BUSDAY6DT;
 DBrec.BUSDAY6DT:=P_BUSDAY6DT; 
 end if;
 if P_THIRDTHURSDAY is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_THIRDTHURSDAY';
 cur_colvalue:=P_THIRDTHURSDAY;
 DBrec.THIRDTHURSDAY:=P_THIRDTHURSDAY; 
 end if;
 if P_ACTUALRQST is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ACTUALRQST';
 cur_colvalue:=P_ACTUALRQST;
 DBrec.ACTUALRQST:=P_ACTUALRQST; 
 end if;
 if P_CSAPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAPAIDTHRUDT';
 cur_colvalue:=P_CSAPAIDTHRUDT;
 DBrec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT; 
 end if;
 if P_CDCPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCPAIDTHRUDT';
 cur_colvalue:=P_CDCPAIDTHRUDT;
 DBrec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT; 
 end if;
 if P_BORRPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BORRPAIDTHRUDT';
 cur_colvalue:=P_BORRPAIDTHRUDT;
 DBrec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT; 
 end if;
 if P_INTPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INTPAIDTHRUDT';
 cur_colvalue:=P_INTPAIDTHRUDT;
 DBrec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT; 
 end if;
 if P_APPVDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_APPVDT';
 cur_colvalue:=P_APPVDT;
 DBrec.APPVDT:=P_APPVDT; 
 end if;
 if P_PRINNEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRINNEEDEDAMT';
 cur_colvalue:=P_PRINNEEDEDAMT;
 DBrec.PRINNEEDEDAMT:=P_PRINNEEDEDAMT; 
 end if;
 if P_INTNEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INTNEEDEDAMT';
 cur_colvalue:=P_INTNEEDEDAMT;
 DBrec.INTNEEDEDAMT:=P_INTNEEDEDAMT; 
 end if;
 if P_MODELNQ is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MODELNQ';
 cur_colvalue:=P_MODELNQ;
 DBrec.MODELNQ:=P_MODELNQ; 
 end if;
 if P_CURRFEEBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CURRFEEBALAMT';
 cur_colvalue:=P_CURRFEEBALAMT;
 DBrec.CURRFEEBALAMT:=P_CURRFEEBALAMT; 
 end if;
 if P_CSAFEENEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAFEENEEDEDAMT';
 cur_colvalue:=P_CSAFEENEEDEDAMT;
 DBrec.CSAFEENEEDEDAMT:=P_CSAFEENEEDEDAMT; 
 end if;
 if P_CDCFEENEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCFEENEEDEDAMT';
 cur_colvalue:=P_CDCFEENEEDEDAMT;
 DBrec.CDCFEENEEDEDAMT:=P_CDCFEENEEDEDAMT; 
 end if;
 if P_FEENEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FEENEEDEDAMT';
 cur_colvalue:=P_FEENEEDEDAMT;
 DBrec.FEENEEDEDAMT:=P_FEENEEDEDAMT; 
 end if;
 if P_LATEFEENEEDEDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LATEFEENEEDEDAMT';
 cur_colvalue:=P_LATEFEENEEDEDAMT;
 DBrec.LATEFEENEEDEDAMT:=P_LATEFEENEEDEDAMT; 
 end if;
 if P_CSADUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSADUEFROMBORRAMT';
 cur_colvalue:=P_CSADUEFROMBORRAMT;
 DBrec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT; 
 end if;
 if P_CDCDUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCDUEFROMBORRAMT';
 cur_colvalue:=P_CDCDUEFROMBORRAMT;
 DBrec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT; 
 end if;
 if P_DUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRAMT';
 cur_colvalue:=P_DUEFROMBORRAMT;
 DBrec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT; 
 end if;
 if P_CSABEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSABEHINDAMT';
 cur_colvalue:=P_CSABEHINDAMT;
 DBrec.CSABEHINDAMT:=P_CSABEHINDAMT; 
 end if;
 if P_CDCBEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCBEHINDAMT';
 cur_colvalue:=P_CDCBEHINDAMT;
 DBrec.CDCBEHINDAMT:=P_CDCBEHINDAMT; 
 end if;
 if P_BEHINDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BEHINDAMT';
 cur_colvalue:=P_BEHINDAMT;
 DBrec.BEHINDAMT:=P_BEHINDAMT; 
 end if;
 if P_BORRFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BORRFEEAMT';
 cur_colvalue:=P_BORRFEEAMT;
 DBrec.BORRFEEAMT:=P_BORRFEEAMT; 
 end if;
 if P_CDCFEEDISBAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCFEEDISBAMT';
 cur_colvalue:=P_CDCFEEDISBAMT;
 DBrec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT; 
 end if;
 if P_AMORTCDCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_AMORTCDCAMT';
 cur_colvalue:=P_AMORTCDCAMT;
 DBrec.AMORTCDCAMT:=P_AMORTCDCAMT; 
 end if;
 if P_AMORTSBAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_AMORTSBAAMT';
 cur_colvalue:=P_AMORTSBAAMT;
 DBrec.AMORTSBAAMT:=P_AMORTSBAAMT; 
 end if;
 if P_AMORTCSAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_AMORTCSAAMT';
 cur_colvalue:=P_AMORTCSAAMT;
 DBrec.AMORTCSAAMT:=P_AMORTCSAAMT; 
 end if;
 if P_CDCPCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCPCT';
 cur_colvalue:=P_CDCPCT;
 DBrec.CDCPCT:=P_CDCPCT; 
 end if;
 if P_PREPAYPCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PREPAYPCT';
 cur_colvalue:=P_PREPAYPCT;
 DBrec.PREPAYPCT:=P_PREPAYPCT; 
 end if;
 if P_FEECALCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FEECALCAMT';
 cur_colvalue:=P_FEECALCAMT;
 DBrec.FEECALCAMT:=P_FEECALCAMT; 
 end if;
 if P_CSAFEECALCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAFEECALCAMT';
 cur_colvalue:=P_CSAFEECALCAMT;
 DBrec.CSAFEECALCAMT:=P_CSAFEECALCAMT; 
 end if;
 if P_CSAFEECALCONLYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CSAFEECALCONLYAMT';
 cur_colvalue:=P_CSAFEECALCONLYAMT;
 DBrec.CSAFEECALCONLYAMT:=P_CSAFEECALCONLYAMT; 
 end if;
 if P_CDCFEECALCONLYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCFEECALCONLYAMT';
 cur_colvalue:=P_CDCFEECALCONLYAMT;
 DBrec.CDCFEECALCONLYAMT:=P_CDCFEECALCONLYAMT; 
 end if;
 if P_FEECALCONLYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FEECALCONLYAMT';
 cur_colvalue:=P_FEECALCONLYAMT;
 DBrec.FEECALCONLYAMT:=P_FEECALCONLYAMT; 
 end if;
 if P_FIVEYRADJ is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FIVEYRADJ';
 cur_colvalue:=P_FIVEYRADJ;
 DBrec.FIVEYRADJ:=P_FIVEYRADJ; 
 end if;
 if P_FIVEYRADJCSAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FIVEYRADJCSAAMT';
 cur_colvalue:=P_FIVEYRADJCSAAMT;
 DBrec.FIVEYRADJCSAAMT:=P_FIVEYRADJCSAAMT; 
 end if;
 if P_FIVEYRADJCDCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FIVEYRADJCDCAMT';
 cur_colvalue:=P_FIVEYRADJCDCAMT;
 DBrec.FIVEYRADJCDCAMT:=P_FIVEYRADJCDCAMT; 
 end if;
 if P_FIVEYRADJSBAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_FIVEYRADJSBAAMT';
 cur_colvalue:=P_FIVEYRADJSBAAMT;
 DBrec.FIVEYRADJSBAAMT:=P_FIVEYRADJSBAAMT; 
 end if;
 if P_BALCALC1AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC1AMT';
 cur_colvalue:=P_BALCALC1AMT;
 DBrec.BALCALC1AMT:=P_BALCALC1AMT; 
 end if;
 if P_BALCALC2AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC2AMT';
 cur_colvalue:=P_BALCALC2AMT;
 DBrec.BALCALC2AMT:=P_BALCALC2AMT; 
 end if;
 if P_BALCALC3AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC3AMT';
 cur_colvalue:=P_BALCALC3AMT;
 DBrec.BALCALC3AMT:=P_BALCALC3AMT; 
 end if;
 if P_BALCALC4AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC4AMT';
 cur_colvalue:=P_BALCALC4AMT;
 DBrec.BALCALC4AMT:=P_BALCALC4AMT; 
 end if;
 if P_BALCALC5AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC5AMT';
 cur_colvalue:=P_BALCALC5AMT;
 DBrec.BALCALC5AMT:=P_BALCALC5AMT; 
 end if;
 if P_BALCALC6AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_BALCALC6AMT';
 cur_colvalue:=P_BALCALC6AMT;
 DBrec.BALCALC6AMT:=P_BALCALC6AMT; 
 end if;
 if P_PRINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRINAMT';
 cur_colvalue:=P_PRINAMT;
 DBrec.PRINAMT:=P_PRINAMT; 
 end if;
 if P_PRIN2AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRIN2AMT';
 cur_colvalue:=P_PRIN2AMT;
 DBrec.PRIN2AMT:=P_PRIN2AMT; 
 end if;
 if P_PRIN3AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRIN3AMT';
 cur_colvalue:=P_PRIN3AMT;
 DBrec.PRIN3AMT:=P_PRIN3AMT; 
 end if;
 if P_PRIN4AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRIN4AMT';
 cur_colvalue:=P_PRIN4AMT;
 DBrec.PRIN4AMT:=P_PRIN4AMT; 
 end if;
 if P_PRIN5AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRIN5AMT';
 cur_colvalue:=P_PRIN5AMT;
 DBrec.PRIN5AMT:=P_PRIN5AMT; 
 end if;
 if P_PRIN6AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRIN6AMT';
 cur_colvalue:=P_PRIN6AMT;
 DBrec.PRIN6AMT:=P_PRIN6AMT; 
 end if;
 if P_PRINPROJAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRINPROJAMT';
 cur_colvalue:=P_PRINPROJAMT;
 DBrec.PRINPROJAMT:=P_PRINPROJAMT; 
 end if;
 if P_PIPROJAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PIPROJAMT';
 cur_colvalue:=P_PIPROJAMT;
 DBrec.PIPROJAMT:=P_PIPROJAMT; 
 end if;
 if P_INTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INTAMT';
 cur_colvalue:=P_INTAMT;
 DBrec.INTAMT:=P_INTAMT; 
 end if;
 if P_INT2AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INT2AMT';
 cur_colvalue:=P_INT2AMT;
 DBrec.INT2AMT:=P_INT2AMT; 
 end if;
 if P_INT3AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INT3AMT';
 cur_colvalue:=P_INT3AMT;
 DBrec.INT3AMT:=P_INT3AMT; 
 end if;
 if P_INT4AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INT4AMT';
 cur_colvalue:=P_INT4AMT;
 DBrec.INT4AMT:=P_INT4AMT; 
 end if;
 if P_INT5AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INT5AMT';
 cur_colvalue:=P_INT5AMT;
 DBrec.INT5AMT:=P_INT5AMT; 
 end if;
 if P_INT6AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INT6AMT';
 cur_colvalue:=P_INT6AMT;
 DBrec.INT6AMT:=P_INT6AMT; 
 end if;
 if P_ESCROWAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ESCROWAMT';
 cur_colvalue:=P_ESCROWAMT;
 DBrec.ESCROWAMT:=P_ESCROWAMT; 
 end if;
 if P_PI6MOAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PI6MOAMT';
 cur_colvalue:=P_PI6MOAMT;
 DBrec.PI6MOAMT:=P_PI6MOAMT; 
 end if;
 if P_GFDAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_GFDAMT';
 cur_colvalue:=P_GFDAMT;
 DBrec.GFDAMT:=P_GFDAMT; 
 end if;
 if P_OPENGNTYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYAMT';
 cur_colvalue:=P_OPENGNTYAMT;
 DBrec.OPENGNTYAMT:=P_OPENGNTYAMT; 
 end if;
 if P_OPENGNTYERRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYERRAMT';
 cur_colvalue:=P_OPENGNTYERRAMT;
 DBrec.OPENGNTYERRAMT:=P_OPENGNTYERRAMT; 
 end if;
 if P_OPENGNTYREGAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYREGAMT';
 cur_colvalue:=P_OPENGNTYREGAMT;
 DBrec.OPENGNTYREGAMT:=P_OPENGNTYREGAMT; 
 end if;
 if P_OPENGNTYUNALLOCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_OPENGNTYUNALLOCAMT';
 cur_colvalue:=P_OPENGNTYUNALLOCAMT;
 DBrec.OPENGNTYUNALLOCAMT:=P_OPENGNTYUNALLOCAMT; 
 end if;
 if P_UNALLOCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UNALLOCAMT';
 cur_colvalue:=P_UNALLOCAMT;
 DBrec.UNALLOCAMT:=P_UNALLOCAMT; 
 end if;
 if P_USERLCK is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_USERLCK';
 cur_colvalue:=P_USERLCK;
 DBrec.USERLCK:=P_USERLCK; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 if P_CDCSBAFEEDISBAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCSBAFEEDISBAMT';
 cur_colvalue:=P_CDCSBAFEEDISBAMT;
 DBrec.CDCSBAFEEDISBAMT:=P_CDCSBAFEEDISBAMT; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.PRPLOANRQSTTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,LOANNMB=DBrec.LOANNMB
 ,RQSTREF=DBrec.RQSTREF
 ,RQSTCD=DBrec.RQSTCD
 ,CDCREGNCD=DBrec.CDCREGNCD
 ,CDCNMB=DBrec.CDCNMB
 ,LOANSTAT=DBrec.LOANSTAT
 ,BORRNM=DBrec.BORRNM
 ,PREPAYAMT=DBrec.PREPAYAMT
 ,PREPAYDT=DBrec.PREPAYDT
 ,PREPAYMO=DBrec.PREPAYMO
 ,PREPAYYRNMB=DBrec.PREPAYYRNMB
 ,PREPAYCALCDT=DBrec.PREPAYCALCDT
 ,SEMIANNDT=DBrec.SEMIANNDT
 ,SEMIANMO=DBrec.SEMIANMO
 ,SEMIANNAMT=DBrec.SEMIANNAMT
 ,SEMIANNPYMTAMT=DBrec.SEMIANNPYMTAMT
 ,PRINCURAMT=DBrec.PRINCURAMT
 ,BALAMT=DBrec.BALAMT
 ,BALCALCAMT=DBrec.BALCALCAMT
 ,NOTEBALCALCAMT=DBrec.NOTEBALCALCAMT
 ,PIAMT=DBrec.PIAMT
 ,NOTERTPCT=DBrec.NOTERTPCT
 ,ISSDT=DBrec.ISSDT
 ,DBENTRBALAMT=DBrec.DBENTRBALAMT
 ,PREPAYPREMAMT=DBrec.PREPAYPREMAMT
 ,POSTINGDT=DBrec.POSTINGDT
 ,POSTINGMO=DBrec.POSTINGMO
 ,POSTINGYRNMB=DBrec.POSTINGYRNMB
 ,BUSDAY6DT=DBrec.BUSDAY6DT
 ,THIRDTHURSDAY=DBrec.THIRDTHURSDAY
 ,ACTUALRQST=DBrec.ACTUALRQST
 ,CSAPAIDTHRUDT=DBrec.CSAPAIDTHRUDT
 ,CDCPAIDTHRUDT=DBrec.CDCPAIDTHRUDT
 ,BORRPAIDTHRUDT=DBrec.BORRPAIDTHRUDT
 ,INTPAIDTHRUDT=DBrec.INTPAIDTHRUDT
 ,APPVDT=DBrec.APPVDT
 ,PRINNEEDEDAMT=DBrec.PRINNEEDEDAMT
 ,INTNEEDEDAMT=DBrec.INTNEEDEDAMT
 ,MODELNQ=DBrec.MODELNQ
 ,CURRFEEBALAMT=DBrec.CURRFEEBALAMT
 ,CSAFEENEEDEDAMT=DBrec.CSAFEENEEDEDAMT
 ,CDCFEENEEDEDAMT=DBrec.CDCFEENEEDEDAMT
 ,FEENEEDEDAMT=DBrec.FEENEEDEDAMT
 ,LATEFEENEEDEDAMT=DBrec.LATEFEENEEDEDAMT
 ,CSADUEFROMBORRAMT=DBrec.CSADUEFROMBORRAMT
 ,CDCDUEFROMBORRAMT=DBrec.CDCDUEFROMBORRAMT
 ,DUEFROMBORRAMT=DBrec.DUEFROMBORRAMT
 ,CSABEHINDAMT=DBrec.CSABEHINDAMT
 ,CDCBEHINDAMT=DBrec.CDCBEHINDAMT
 ,BEHINDAMT=DBrec.BEHINDAMT
 ,BORRFEEAMT=DBrec.BORRFEEAMT
 ,CDCFEEDISBAMT=DBrec.CDCFEEDISBAMT
 ,AMORTCDCAMT=DBrec.AMORTCDCAMT
 ,AMORTSBAAMT=DBrec.AMORTSBAAMT
 ,AMORTCSAAMT=DBrec.AMORTCSAAMT
 ,CDCPCT=DBrec.CDCPCT
 ,PREPAYPCT=DBrec.PREPAYPCT
 ,FEECALCAMT=DBrec.FEECALCAMT
 ,CSAFEECALCAMT=DBrec.CSAFEECALCAMT
 ,CSAFEECALCONLYAMT=DBrec.CSAFEECALCONLYAMT
 ,CDCFEECALCONLYAMT=DBrec.CDCFEECALCONLYAMT
 ,FEECALCONLYAMT=DBrec.FEECALCONLYAMT
 ,FIVEYRADJ=DBrec.FIVEYRADJ
 ,FIVEYRADJCSAAMT=DBrec.FIVEYRADJCSAAMT
 ,FIVEYRADJCDCAMT=DBrec.FIVEYRADJCDCAMT
 ,FIVEYRADJSBAAMT=DBrec.FIVEYRADJSBAAMT
 ,BALCALC1AMT=DBrec.BALCALC1AMT
 ,BALCALC2AMT=DBrec.BALCALC2AMT
 ,BALCALC3AMT=DBrec.BALCALC3AMT
 ,BALCALC4AMT=DBrec.BALCALC4AMT
 ,BALCALC5AMT=DBrec.BALCALC5AMT
 ,BALCALC6AMT=DBrec.BALCALC6AMT
 ,PRINAMT=DBrec.PRINAMT
 ,PRIN2AMT=DBrec.PRIN2AMT
 ,PRIN3AMT=DBrec.PRIN3AMT
 ,PRIN4AMT=DBrec.PRIN4AMT
 ,PRIN5AMT=DBrec.PRIN5AMT
 ,PRIN6AMT=DBrec.PRIN6AMT
 ,PRINPROJAMT=DBrec.PRINPROJAMT
 ,PIPROJAMT=DBrec.PIPROJAMT
 ,INTAMT=DBrec.INTAMT
 ,INT2AMT=DBrec.INT2AMT
 ,INT3AMT=DBrec.INT3AMT
 ,INT4AMT=DBrec.INT4AMT
 ,INT5AMT=DBrec.INT5AMT
 ,INT6AMT=DBrec.INT6AMT
 ,ESCROWAMT=DBrec.ESCROWAMT
 ,PI6MOAMT=DBrec.PI6MOAMT
 ,GFDAMT=DBrec.GFDAMT
 ,OPENGNTYAMT=DBrec.OPENGNTYAMT
 ,OPENGNTYERRAMT=DBrec.OPENGNTYERRAMT
 ,OPENGNTYREGAMT=DBrec.OPENGNTYREGAMT
 ,OPENGNTYUNALLOCAMT=DBrec.OPENGNTYUNALLOCAMT
 ,UNALLOCAMT=DBrec.UNALLOCAMT
 ,USERLCK=DBrec.USERLCK
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 ,CDCSBAFEEDISBAMT=DBrec.CDCSBAFEEDISBAMT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.PRPLOANRQSTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPLOANRQSTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPLOANRQSTUPDTSP',p_userid,4,
 logtxt1=>'PRPLOANRQSTUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'PRPLOANRQSTUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB; l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 select rowid into rec_rowid from STGCSA.PRPLOANRQSTTBL where 1=1
 and LOANNMB=P_LOANNMB;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPLOANRQSTTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPLOANRQSTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'PRPLOANRQSTUPDTSP');
 else
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 p_errmsg:=p_errmsg||' in PRPLOANRQSTUPDTSP build 2018-10-08 15:46:26';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPLOANRQSTUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRPLOANRQSTUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPLOANRQSTUPDTSP build 2018-10-08 15:46:26';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPLOANRQSTUPDTSP',force_log_entry=>true);
 --
 END PRPLOANRQSTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


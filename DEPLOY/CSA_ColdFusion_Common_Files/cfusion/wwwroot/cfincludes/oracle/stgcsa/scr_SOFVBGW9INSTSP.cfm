<!--- Saved 10/09/2018 11:29:09. --->
PROCEDURE SOFVBGW9INSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_W9TAXID CHAR:=null
 ,p_W9NM VARCHAR2:=null
 ,p_W9MAILADRSTR1NM VARCHAR2:=null
 ,p_W9MAILADRSTR2NM VARCHAR2:=null
 ,p_W9MAILADRCTYNM VARCHAR2:=null
 ,p_W9MAILADRSTCD CHAR:=null
 ,p_W9MAILADRZIPCD CHAR:=null
 ,p_W9MAILADRZIP4CD CHAR:=null
 ,p_W9VERIFICATIONIND CHAR:=null
 ,p_W9VERIFICATIONDT DATE:=null
 ,p_W9NMMAILADRCHNGIND CHAR:=null
 ,p_W9NMADRCHNGDT DATE:=null
 ,p_W9TAXFORMFLD CHAR:=null
 ,p_W9TAXMAINTNDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:14
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:47:14
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.SOFVBGW9TBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.SOFVBGW9TBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 crossover_not_active char(1);
 -- vars for checking non-defaultable columns
 onespace varchar2(10):=' '; 
 ndfltmsg varchar2(300);
 v_errfnd boolean:=false;
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint SOFVBGW9INSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVBGW9INSTSP',p_userid,4,
 logtxt1=>'SOFVBGW9INSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'SOFVBGW9INSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVBGW9TBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 raise_application_error(-20001,cur_colname||' may not be null');
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_W9TAXID';
 cur_colvalue:=P_W9TAXID;
 if P_W9TAXID is null then
 
 new_rec.W9TAXID:=rpad(' ',10);
 
 else 
 new_rec.W9TAXID:=P_W9TAXID;
 end if;
 cur_colname:='P_W9NM';
 cur_colvalue:=P_W9NM;
 if P_W9NM is null then
 
 new_rec.W9NM:=' ';
 
 else 
 new_rec.W9NM:=P_W9NM;
 end if;
 cur_colname:='P_W9MAILADRSTR1NM';
 cur_colvalue:=P_W9MAILADRSTR1NM;
 if P_W9MAILADRSTR1NM is null then
 
 new_rec.W9MAILADRSTR1NM:=' ';
 
 else 
 new_rec.W9MAILADRSTR1NM:=P_W9MAILADRSTR1NM;
 end if;
 cur_colname:='P_W9MAILADRSTR2NM';
 cur_colvalue:=P_W9MAILADRSTR2NM;
 if P_W9MAILADRSTR2NM is null then
 
 new_rec.W9MAILADRSTR2NM:=' ';
 
 else 
 new_rec.W9MAILADRSTR2NM:=P_W9MAILADRSTR2NM;
 end if;
 cur_colname:='P_W9MAILADRCTYNM';
 cur_colvalue:=P_W9MAILADRCTYNM;
 if P_W9MAILADRCTYNM is null then
 
 new_rec.W9MAILADRCTYNM:=' ';
 
 else 
 new_rec.W9MAILADRCTYNM:=P_W9MAILADRCTYNM;
 end if;
 cur_colname:='P_W9MAILADRSTCD';
 cur_colvalue:=P_W9MAILADRSTCD;
 if P_W9MAILADRSTCD is null then
 
 new_rec.W9MAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.W9MAILADRSTCD:=P_W9MAILADRSTCD;
 end if;
 cur_colname:='P_W9MAILADRZIPCD';
 cur_colvalue:=P_W9MAILADRZIPCD;
 if P_W9MAILADRZIPCD is null then
 
 new_rec.W9MAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.W9MAILADRZIPCD:=P_W9MAILADRZIPCD;
 end if;
 cur_colname:='P_W9MAILADRZIP4CD';
 cur_colvalue:=P_W9MAILADRZIP4CD;
 if P_W9MAILADRZIP4CD is null then
 
 new_rec.W9MAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.W9MAILADRZIP4CD:=P_W9MAILADRZIP4CD;
 end if;
 cur_colname:='P_W9VERIFICATIONIND';
 cur_colvalue:=P_W9VERIFICATIONIND;
 if P_W9VERIFICATIONIND is null then
 
 new_rec.W9VERIFICATIONIND:=rpad(' ',1);
 
 else 
 new_rec.W9VERIFICATIONIND:=P_W9VERIFICATIONIND;
 end if;
 cur_colname:='P_W9VERIFICATIONDT';
 cur_colvalue:=P_W9VERIFICATIONDT;
 if P_W9VERIFICATIONDT is null then
 
 new_rec.W9VERIFICATIONDT:=jan_1_1900;
 
 else 
 new_rec.W9VERIFICATIONDT:=P_W9VERIFICATIONDT;
 end if;
 cur_colname:='P_W9NMMAILADRCHNGIND';
 cur_colvalue:=P_W9NMMAILADRCHNGIND;
 if P_W9NMMAILADRCHNGIND is null then
 
 new_rec.W9NMMAILADRCHNGIND:=rpad(' ',1);
 
 else 
 new_rec.W9NMMAILADRCHNGIND:=P_W9NMMAILADRCHNGIND;
 end if;
 cur_colname:='P_W9NMADRCHNGDT';
 cur_colvalue:=P_W9NMADRCHNGDT;
 if P_W9NMADRCHNGDT is null then
 
 new_rec.W9NMADRCHNGDT:=jan_1_1900;
 
 else 
 new_rec.W9NMADRCHNGDT:=P_W9NMADRCHNGDT;
 end if;
 cur_colname:='P_W9TAXFORMFLD';
 cur_colvalue:=P_W9TAXFORMFLD;
 if P_W9TAXFORMFLD is null then
 
 new_rec.W9TAXFORMFLD:=rpad(' ',1);
 
 else 
 new_rec.W9TAXFORMFLD:=P_W9TAXFORMFLD;
 end if;
 cur_colname:='P_W9TAXMAINTNDT';
 cur_colvalue:=P_W9TAXMAINTNDT;
 if P_W9TAXMAINTNDT is null then
 
 new_rec.W9TAXMAINTNDT:=jan_1_1900;
 
 else 
 new_rec.W9TAXMAINTNDT:=P_W9TAXMAINTNDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
 if p_errval=0 then
 begin
 insert into STGCSA.SOFVBGW9TBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End SOFVBGW9INSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'SOFVBGW9INSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in SOFVBGW9INSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:47:14';
 ROLLBACK TO SOFVBGW9INSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVBGW9INSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in SOFVBGW9INSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:47:14';
 ROLLBACK TO SOFVBGW9INSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'SOFVBGW9INSTSP',force_log_entry=>TRUE);
 --
 END SOFVBGW9INSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


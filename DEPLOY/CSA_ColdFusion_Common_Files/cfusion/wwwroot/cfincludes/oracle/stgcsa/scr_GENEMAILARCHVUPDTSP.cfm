<!--- Saved 10/09/2018 11:29:05. --->
PROCEDURE GENEMAILARCHVUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMAILID VARCHAR2:=null
 ,p_DBMDL VARCHAR2:=null
 ,p_RELTDTBLNM VARCHAR2:=null
 ,p_RELTDTBLREF VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_EMAILCATID VARCHAR2:=null
 ,p_EMAILTO VARCHAR2:=null
 ,p_EMAILFROMNM VARCHAR2:=null
 ,p_EMAILFROMADR VARCHAR2:=null
 ,p_EMAILCC VARCHAR2:=null
 ,p_EMAILSUBJ VARCHAR2:=null
 ,p_EMAILBODY VARCHAR2:=null
 ,p_EMAILSENTDT DATE:=null
 ,p_MSGFILENM VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:42
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:45:42
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure GENEMAILARCHVUPDTSP performs UPDATE on STGCSA.GENEMAILARCHVTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: EMAILID
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.GENEMAILARCHVTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.GENEMAILARCHVTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.GENEMAILARCHVTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_EMAILID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILID';
 cur_colvalue:=P_EMAILID;
 DBrec.EMAILID:=P_EMAILID; 
 end if;
 if P_DBMDL is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DBMDL';
 cur_colvalue:=P_DBMDL;
 DBrec.DBMDL:=P_DBMDL; 
 end if;
 if P_RELTDTBLNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_RELTDTBLNM';
 cur_colvalue:=P_RELTDTBLNM;
 DBrec.RELTDTBLNM:=P_RELTDTBLNM; 
 end if;
 if P_RELTDTBLREF is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_RELTDTBLREF';
 cur_colvalue:=P_RELTDTBLREF;
 DBrec.RELTDTBLREF:=P_RELTDTBLREF; 
 end if;
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_EMAILCATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILCATID';
 cur_colvalue:=P_EMAILCATID;
 DBrec.EMAILCATID:=P_EMAILCATID; 
 end if;
 if P_EMAILTO is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILTO';
 cur_colvalue:=P_EMAILTO;
 DBrec.EMAILTO:=P_EMAILTO; 
 end if;
 if P_EMAILFROMNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILFROMNM';
 cur_colvalue:=P_EMAILFROMNM;
 DBrec.EMAILFROMNM:=P_EMAILFROMNM; 
 end if;
 if P_EMAILFROMADR is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILFROMADR';
 cur_colvalue:=P_EMAILFROMADR;
 DBrec.EMAILFROMADR:=P_EMAILFROMADR; 
 end if;
 if P_EMAILCC is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILCC';
 cur_colvalue:=P_EMAILCC;
 DBrec.EMAILCC:=P_EMAILCC; 
 end if;
 if P_EMAILSUBJ is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILSUBJ';
 cur_colvalue:=P_EMAILSUBJ;
 DBrec.EMAILSUBJ:=P_EMAILSUBJ; 
 end if;
 if P_EMAILBODY is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILBODY';
 cur_colvalue:=P_EMAILBODY;
 DBrec.EMAILBODY:=P_EMAILBODY; 
 end if;
 if P_EMAILSENTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_EMAILSENTDT';
 cur_colvalue:=P_EMAILSENTDT;
 DBrec.EMAILSENTDT:=P_EMAILSENTDT; 
 end if;
 if P_MSGFILENM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MSGFILENM';
 cur_colvalue:=P_MSGFILENM;
 DBrec.MSGFILENM:=P_MSGFILENM; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.GENEMAILARCHVTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,EMAILID=DBrec.EMAILID
 ,DBMDL=DBrec.DBMDL
 ,RELTDTBLNM=DBrec.RELTDTBLNM
 ,RELTDTBLREF=DBrec.RELTDTBLREF
 ,LOANNMB=DBrec.LOANNMB
 ,EMAILCATID=DBrec.EMAILCATID
 ,EMAILTO=DBrec.EMAILTO
 ,EMAILFROMNM=DBrec.EMAILFROMNM
 ,EMAILFROMADR=DBrec.EMAILFROMADR
 ,EMAILCC=DBrec.EMAILCC
 ,EMAILSUBJ=DBrec.EMAILSUBJ
 ,EMAILBODY=DBrec.EMAILBODY
 ,EMAILSENTDT=DBrec.EMAILSENTDT
 ,MSGFILENM=DBrec.MSGFILENM
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.GENEMAILARCHVTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint GENEMAILARCHVUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init GENEMAILARCHVUPDTSP',p_userid,4,
 logtxt1=>'GENEMAILARCHVUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'GENEMAILARCHVUPDTSP');
 --
 l_DBMDL:=P_DBMDL; l_LOANNMB:=P_LOANNMB; l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(EMAILID)=('||P_EMAILID||')';
 begin
 select rowid into rec_rowid from STGCSA.GENEMAILARCHVTBL where 1=1
 and EMAILID=P_EMAILID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.GENEMAILARCHVTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End GENEMAILARCHVUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'GENEMAILARCHVUPDTSP');
 else
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 p_errmsg:=p_errmsg||' in GENEMAILARCHVUPDTSP build 2018-10-08 15:45:42';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'GENEMAILARCHVUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO GENEMAILARCHVUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in GENEMAILARCHVUPDTSP build 2018-10-08 15:45:42';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'GENEMAILARCHVUPDTSP',force_log_entry=>true);
 --
 END GENEMAILARCHVUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:02. --->
PROCEDURE ACCPRCSCYCSTATUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CYCPRCSID VARCHAR2:=null
 ,p_CYCID VARCHAR2:=null
 ,p_PRCSID VARCHAR2:=null
 ,p_SCHDLDT DATE:=null
 ,p_SCHDLUSER VARCHAR2:=null
 ,p_SETUPDT DATE:=null
 ,p_LASTREFRESHDT DATE:=null
 ,p_INITIALRVWCMPLT VARCHAR2:=null
 ,p_INITIALRVWUSER VARCHAR2:=null
 ,p_INITIALRVWDT DATE:=null
 ,p_PEERRVWCMPLT VARCHAR2:=null
 ,p_PEERRVWUSER VARCHAR2:=null
 ,p_PEERRVWDT DATE:=null
 ,p_UPDTCMPLT VARCHAR2:=null
 ,p_UPDTCMPLTUSER VARCHAR2:=null
 ,p_UPDTCMPLTDT DATE:=null
 ,p_MGRRVWCMPLT VARCHAR2:=null
 ,p_MGRRVWUSER VARCHAR2:=null
 ,p_MGRRVWDT DATE:=null
 ,p_UPDTRUNNING VARCHAR2:=null
 ,p_UPDTRUNNINGUSER VARCHAR2:=null
 ,p_PRCSCYCCMNT VARCHAR2:=null
 ,p_PROPOSEDDT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:25
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:44:25
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure ACCPRCSCYCSTATUPDTSP performs UPDATE on STGCSA.ACCPRCSCYCSTATTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: CYCPRCSID
 for P_IDENTIFIER=1 qualification is on CYCID, PRCSID
 for P_IDENTIFIER=2 qualification is on CYCID, PROPOSEDDT is not null
 for P_IDENTIFIER=3 qualification is on CYCID, PRCSID in (30,31,32)
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.ACCPRCSCYCSTATTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.ACCPRCSCYCSTATTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.ACCPRCSCYCSTATTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_CYCPRCSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CYCPRCSID';
 cur_colvalue:=P_CYCPRCSID;
 DBrec.CYCPRCSID:=P_CYCPRCSID; 
 end if;
 if P_CYCID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CYCID';
 cur_colvalue:=P_CYCID;
 DBrec.CYCID:=P_CYCID; 
 end if;
 if P_PRCSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRCSID';
 cur_colvalue:=P_PRCSID;
 DBrec.PRCSID:=P_PRCSID; 
 end if;
 if P_SCHDLDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SCHDLDT';
 cur_colvalue:=P_SCHDLDT;
 DBrec.SCHDLDT:=P_SCHDLDT; 
 end if;
 if P_SCHDLUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SCHDLUSER';
 cur_colvalue:=P_SCHDLUSER;
 DBrec.SCHDLUSER:=P_SCHDLUSER; 
 end if;
 if P_SETUPDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_SETUPDT';
 cur_colvalue:=P_SETUPDT;
 DBrec.SETUPDT:=P_SETUPDT; 
 end if;
 if P_LASTREFRESHDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LASTREFRESHDT';
 cur_colvalue:=P_LASTREFRESHDT;
 DBrec.LASTREFRESHDT:=P_LASTREFRESHDT; 
 end if;
 if P_INITIALRVWCMPLT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INITIALRVWCMPLT';
 cur_colvalue:=P_INITIALRVWCMPLT;
 DBrec.INITIALRVWCMPLT:=P_INITIALRVWCMPLT; 
 end if;
 if P_INITIALRVWUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INITIALRVWUSER';
 cur_colvalue:=P_INITIALRVWUSER;
 DBrec.INITIALRVWUSER:=P_INITIALRVWUSER; 
 end if;
 if P_INITIALRVWDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_INITIALRVWDT';
 cur_colvalue:=P_INITIALRVWDT;
 DBrec.INITIALRVWDT:=P_INITIALRVWDT; 
 end if;
 if P_PEERRVWCMPLT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PEERRVWCMPLT';
 cur_colvalue:=P_PEERRVWCMPLT;
 DBrec.PEERRVWCMPLT:=P_PEERRVWCMPLT; 
 end if;
 if P_PEERRVWUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PEERRVWUSER';
 cur_colvalue:=P_PEERRVWUSER;
 DBrec.PEERRVWUSER:=P_PEERRVWUSER; 
 end if;
 if P_PEERRVWDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PEERRVWDT';
 cur_colvalue:=P_PEERRVWDT;
 DBrec.PEERRVWDT:=P_PEERRVWDT; 
 end if;
 if P_UPDTCMPLT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTCMPLT';
 cur_colvalue:=P_UPDTCMPLT;
 DBrec.UPDTCMPLT:=P_UPDTCMPLT; 
 end if;
 if P_UPDTCMPLTUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTCMPLTUSER';
 cur_colvalue:=P_UPDTCMPLTUSER;
 DBrec.UPDTCMPLTUSER:=P_UPDTCMPLTUSER; 
 end if;
 if P_UPDTCMPLTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTCMPLTDT';
 cur_colvalue:=P_UPDTCMPLTDT;
 DBrec.UPDTCMPLTDT:=P_UPDTCMPLTDT; 
 end if;
 if P_MGRRVWCMPLT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MGRRVWCMPLT';
 cur_colvalue:=P_MGRRVWCMPLT;
 DBrec.MGRRVWCMPLT:=P_MGRRVWCMPLT; 
 end if;
 if P_MGRRVWUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MGRRVWUSER';
 cur_colvalue:=P_MGRRVWUSER;
 DBrec.MGRRVWUSER:=P_MGRRVWUSER; 
 end if;
 if P_MGRRVWDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_MGRRVWDT';
 cur_colvalue:=P_MGRRVWDT;
 DBrec.MGRRVWDT:=P_MGRRVWDT; 
 end if;
 if P_UPDTRUNNING is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTRUNNING';
 cur_colvalue:=P_UPDTRUNNING;
 DBrec.UPDTRUNNING:=P_UPDTRUNNING; 
 end if;
 if P_UPDTRUNNINGUSER is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTRUNNINGUSER';
 cur_colvalue:=P_UPDTRUNNINGUSER;
 DBrec.UPDTRUNNINGUSER:=P_UPDTRUNNINGUSER; 
 end if;
 if P_PRCSCYCCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PRCSCYCCMNT';
 cur_colvalue:=P_PRCSCYCCMNT;
 DBrec.PRCSCYCCMNT:=P_PRCSCYCCMNT; 
 end if;
 if P_PROPOSEDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_PROPOSEDDT';
 cur_colvalue:=P_PROPOSEDDT;
 DBrec.PROPOSEDDT:=P_PROPOSEDDT; 
 end if;
 if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.ACCPRCSCYCSTATTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,CYCPRCSID=DBrec.CYCPRCSID
 ,CYCID=DBrec.CYCID
 ,PRCSID=DBrec.PRCSID
 ,SCHDLDT=DBrec.SCHDLDT
 ,SCHDLUSER=DBrec.SCHDLUSER
 ,SETUPDT=DBrec.SETUPDT
 ,LASTREFRESHDT=DBrec.LASTREFRESHDT
 ,INITIALRVWCMPLT=DBrec.INITIALRVWCMPLT
 ,INITIALRVWUSER=DBrec.INITIALRVWUSER
 ,INITIALRVWDT=DBrec.INITIALRVWDT
 ,PEERRVWCMPLT=DBrec.PEERRVWCMPLT
 ,PEERRVWUSER=DBrec.PEERRVWUSER
 ,PEERRVWDT=DBrec.PEERRVWDT
 ,UPDTCMPLT=DBrec.UPDTCMPLT
 ,UPDTCMPLTUSER=DBrec.UPDTCMPLTUSER
 ,UPDTCMPLTDT=DBrec.UPDTCMPLTDT
 ,MGRRVWCMPLT=DBrec.MGRRVWCMPLT
 ,MGRRVWUSER=DBrec.MGRRVWUSER
 ,MGRRVWDT=DBrec.MGRRVWDT
 ,UPDTRUNNING=DBrec.UPDTRUNNING
 ,UPDTRUNNINGUSER=DBrec.UPDTRUNNINGUSER
 ,PRCSCYCCMNT=DBrec.PRCSCYCCMNT
 ,PROPOSEDDT=DBrec.PROPOSEDDT
 ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.ACCPRCSCYCSTATTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCPRCSCYCSTATUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init ACCPRCSCYCSTATUPDTSP',p_userid,4,
 logtxt1=>'ACCPRCSCYCSTATUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'ACCPRCSCYCSTATUPDTSP');
 --
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(CYCPRCSID)=('||P_CYCPRCSID||')';
 begin
 select rowid into rec_rowid from STGCSA.ACCPRCSCYCSTATTBL where 1=1
 and CYCPRCSID=P_CYCPRCSID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCPRCSCYCSTATTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(PRCSID)=('||P_PRCSID||')'||' and '||'(CYCID)=('||P_CYCID||')';
 begin
 for r1 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 and CYCID=P_CYCID
 and PRCSID=P_PRCSID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCPRCSCYCSTATTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(PROPOSEDDT)is not null'||' and '||'(CYCID)=('||P_CYCID||')';
 begin
 for r2 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 and CYCID=P_CYCID
 
 and PROPOSEDDT is not null
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCPRCSCYCSTATTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to update one or more rows based on keyset KEYS3
 if p_errval=0 then
 keystouse:='(PRCSID)in (30,31,32)'||' and '||'(CYCID)=('||P_CYCID||')';
 begin
 for r3 in (select rowid from STGCSA.ACCPRCSCYCSTATTBL a where 1=1 
 and CYCID=P_CYCID
 
 and PRCSID in (30,31,32)
 ) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCPRCSCYCSTATTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCPRCSCYCSTATTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCPRCSCYCSTATUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'ACCPRCSCYCSTATUPDTSP');
 else
 ROLLBACK TO ACCPRCSCYCSTATUPDTSP;
 p_errmsg:=p_errmsg||' in ACCPRCSCYCSTATUPDTSP build 2018-10-08 15:44:25';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ACCPRCSCYCSTATUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO ACCPRCSCYCSTATUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCPRCSCYCSTATUPDTSP build 2018-10-08 15:44:25';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ACCPRCSCYCSTATUPDTSP',force_log_entry=>true);
 --
 END ACCPRCSCYCSTATUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:15. --->
PROCEDURE TEMPMACROQUERYUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_RECORDID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCCERTNMB CHAR:=null
 ,p_STATUS CHAR:=null
 ,p_CREATUSERID VARCHAR2:=null
 ,p_KEYTYPE CHAR:=null
 ,p_KEYDATE DATE:=null
 ,p_DISBIND CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:48:19
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:48:19
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure TEMPMACROQUERYUPDTSP performs UPDATE on STGCSA.TEMPMACROQUERYTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: RECORDID
 for P_IDENTIFIER=1 qualification is on CREATUSERID is not null or 1=1
 for P_IDENTIFIER=2 qualification is on CREATUSERID
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.TEMPMACROQUERYTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.TEMPMACROQUERYTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.TEMPMACROQUERYTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_RECORDID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_RECORDID';
 cur_colvalue:=P_RECORDID;
 DBrec.RECORDID:=P_RECORDID; 
 end if;
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_CDCREGNCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCREGNCD';
 cur_colvalue:=P_CDCREGNCD;
 DBrec.CDCREGNCD:=P_CDCREGNCD; 
 end if;
 if P_CDCCERTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CDCCERTNMB';
 cur_colvalue:=P_CDCCERTNMB;
 DBrec.CDCCERTNMB:=P_CDCCERTNMB; 
 end if;
 if P_STATUS is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_STATUS';
 cur_colvalue:=P_STATUS;
 DBrec.STATUS:=P_STATUS; 
 end if;
 if P_KEYTYPE is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_KEYTYPE';
 cur_colvalue:=P_KEYTYPE;
 DBrec.KEYTYPE:=P_KEYTYPE; 
 end if;
 if P_KEYDATE is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_KEYDATE';
 cur_colvalue:=P_KEYDATE;
 DBrec.KEYDATE:=P_KEYDATE; 
 end if;
 if P_DISBIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DISBIND';
 cur_colvalue:=P_DISBIND;
 DBrec.DISBIND:=P_DISBIND; 
 end if;
 if DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CREATUSERID';
 cur_colvalue:=P_CREATUSERID;
 DBrec.CREATUSERID:=P_CREATUSERID;
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.TEMPMACROQUERYTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,RECORDID=DBrec.RECORDID
 ,LOANNMB=DBrec.LOANNMB
 ,CDCREGNCD=DBrec.CDCREGNCD
 ,CDCCERTNMB=DBrec.CDCCERTNMB
 ,STATUS=DBrec.STATUS
 ,CREATUSERID=DBrec.CREATUSERID
 ,KEYTYPE=DBrec.KEYTYPE
 ,KEYDATE=DBrec.KEYDATE
 ,DISBIND=DBrec.DISBIND
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.TEMPMACROQUERYTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint TEMPMACROQUERYUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init TEMPMACROQUERYUPDTSP',p_userid,4,
 logtxt1=>'TEMPMACROQUERYUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'TEMPMACROQUERYUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB;
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(RECORDID)=('||P_RECORDID||')';
 begin
 select rowid into rec_rowid from STGCSA.TEMPMACROQUERYTBL where 1=1
 and RECORDID=P_RECORDID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.TEMPMACROQUERYTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(CREATUSERID) is not null or 1=1';
 begin
 for r1 in (select rowid from STGCSA.TEMPMACROQUERYTBL a where 1=1 
 
 and CREATUSERID is not null or 1=1
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.TEMPMACROQUERYTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.TEMPMACROQUERYTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(CREATUSERID)=('||P_CREATUSERID||')';
 begin
 for r2 in (select rowid from STGCSA.TEMPMACROQUERYTBL a where 1=1 
 and CREATUSERID=P_CREATUSERID
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.TEMPMACROQUERYTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.TEMPMACROQUERYTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End TEMPMACROQUERYUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'TEMPMACROQUERYUPDTSP');
 else
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errmsg:=p_errmsg||' in TEMPMACROQUERYUPDTSP build 2018-10-08 15:48:19';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'TEMPMACROQUERYUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO TEMPMACROQUERYUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in TEMPMACROQUERYUPDTSP build 2018-10-08 15:48:19';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'TEMPMACROQUERYUPDTSP',force_log_entry=>true);
 --
 END TEMPMACROQUERYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


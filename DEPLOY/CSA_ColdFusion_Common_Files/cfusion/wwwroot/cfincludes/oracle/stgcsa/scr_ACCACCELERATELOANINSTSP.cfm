<!--- Saved 10/09/2018 11:29:01. --->
PROCEDURE ACCACCELERATELOANINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCELERATNID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_PRIORACCELERATN VARCHAR2:=null
 ,p_STATID VARCHAR2:=null
 ,p_ACCELERATECMNT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_MFINITIALSCRAPEDT DATE:=null
 ,p_MFLASTSCRAPEDT DATE:=null
 ,p_MFCURSTATID VARCHAR2:=null
 ,p_MFCURCMNT VARCHAR2:=null
 ,p_MFINITIALSTATID VARCHAR2:=null
 ,p_MFINITIALCMNT VARCHAR2:=null
 ,p_MFLASTUPDTDT DATE:=null
 ,p_MFISSDT DATE:=null
 ,p_MFLOANMATDT DATE:=null
 ,p_MFORGLPRINAMT VARCHAR2:=null
 ,p_MFINTRTPCT VARCHAR2:=null
 ,p_MFDBENTRAMT VARCHAR2:=null
 ,p_MFTOBECURAMT VARCHAR2:=null
 ,p_MFLASTPYMTDT DATE:=null
 ,p_MFESCROWPRIORPYMTAMT VARCHAR2:=null
 ,p_MFCDCPAIDTHRUDT CHAR:=null
 ,p_MFCDCMODELNQ VARCHAR2:=null
 ,p_MFCDCMOFEEAMT VARCHAR2:=null
 ,p_MFCDCFEEAMT VARCHAR2:=null
 ,p_MFCDCDUEFROMBORRAMT VARCHAR2:=null
 ,p_MFCSAPAIDTHRUDT CHAR:=null
 ,p_MFCSAMODELNQ VARCHAR2:=null
 ,p_MFCSAMOFEEAMT VARCHAR2:=null
 ,p_MFCSAFEEAMT VARCHAR2:=null
 ,p_MFCSADUEFROMBORRAMT VARCHAR2:=null
 ,p_PPAENTRYDT DATE:=null
 ,p_PPAGROUP VARCHAR2:=null
 ,p_PPACDCNMB CHAR:=null
 ,p_PPAORGLPRINAMT VARCHAR2:=null
 ,p_PPADBENTRBALATSEMIANAMT VARCHAR2:=null
 ,p_PPAINTDUEATSEMIANAMT VARCHAR2:=null
 ,p_PPADUEATSEMIANAMT VARCHAR2:=null
 ,p_SBAENTRYDT DATE:=null
 ,p_SBALASTTRNSCRPTIMPTDT DATE:=null
 ,p_SBADBENTRBALATSEMIANAMT VARCHAR2:=null
 ,p_SBAINTDUEATSEMIANAMT VARCHAR2:=null
 ,p_SBACSAFEEAMT VARCHAR2:=null
 ,p_SBACDCFEEAMT VARCHAR2:=null
 ,p_SBAESCROWPRIORPYMTAMT VARCHAR2:=null
 ,p_SBAACCELAMT VARCHAR2:=null
 ,p_SBADUEDT DATE:=null
 ,p_SBANOTERTPCT VARCHAR2:=null
 ,p_SBALASTNOTEBALAMT VARCHAR2:=null
 ,p_SBALASTPYMTDT DATE:=null
 ,p_SBAACCRINTDUEAMT VARCHAR2:=null
 ,p_SBAEXCESSAMT VARCHAR2:=null
 ,p_MFPREVSTATID VARCHAR2:=null
 ,p_MFPREVCMNT VARCHAR2:=null
 ,p_CDCNMB CHAR:=null
 ,p_SERVCNTRID VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:20
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:20
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCACCELERATELOANTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.ACCACCELERATELOANTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize ACCELERATNID from sequence ACCELERATNIDSEQ
 Function NEXTVAL_ACCELERATNIDSEQ return number is
 N number;
 begin
 select ACCELERATNIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint ACCACCELERATELOANINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init ACCACCELERATELOANINSTSP',p_userid,4,
 logtxt1=>'ACCACCELERATELOANINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'ACCACCELERATELOANINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; cur_colname:='TRANSID';
 cur_colvalue:=P_TRANSID;
 l_TRANSID:=P_TRANSID; cur_colname:='TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_ACCELERATNID';
 cur_colvalue:=P_ACCELERATNID;
 if P_ACCELERATNID is null then
 
 new_rec.ACCELERATNID:=NEXTVAL_ACCELERATNIDSEQ();
 
 else 
 new_rec.ACCELERATNID:=P_ACCELERATNID;
 end if;
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 
 new_rec.LOANNMB:=rpad(' ',10);
 
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_TRANSID';
 cur_colvalue:=P_TRANSID;
 if P_TRANSID is null then
 
 new_rec.TRANSID:=0;
 
 else 
 new_rec.TRANSID:=P_TRANSID;
 end if;
 cur_colname:='P_PRIORACCELERATN';
 cur_colvalue:=P_PRIORACCELERATN;
 if P_PRIORACCELERATN is null then
 
 new_rec.PRIORACCELERATN:=0;
 
 else 
 new_rec.PRIORACCELERATN:=P_PRIORACCELERATN;
 end if;
 cur_colname:='P_STATID';
 cur_colvalue:=P_STATID;
 if P_STATID is null then
 
 new_rec.STATID:=0;
 
 else 
 new_rec.STATID:=P_STATID;
 end if;
 cur_colname:='P_ACCELERATECMNT';
 cur_colvalue:=P_ACCELERATECMNT;
 if P_ACCELERATECMNT is null then
 
 new_rec.ACCELERATECMNT:=' ';
 
 else 
 new_rec.ACCELERATECMNT:=P_ACCELERATECMNT;
 end if;
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 if P_SEMIANDT is null then
 
 new_rec.SEMIANDT:=jan_1_1900;
 
 else 
 new_rec.SEMIANDT:=P_SEMIANDT;
 end if;
 cur_colname:='P_MFINITIALSCRAPEDT';
 cur_colvalue:=P_MFINITIALSCRAPEDT;
 if P_MFINITIALSCRAPEDT is null then
 
 new_rec.MFINITIALSCRAPEDT:=jan_1_1900;
 
 else 
 new_rec.MFINITIALSCRAPEDT:=P_MFINITIALSCRAPEDT;
 end if;
 cur_colname:='P_MFLASTSCRAPEDT';
 cur_colvalue:=P_MFLASTSCRAPEDT;
 if P_MFLASTSCRAPEDT is null then
 
 new_rec.MFLASTSCRAPEDT:=jan_1_1900;
 
 else 
 new_rec.MFLASTSCRAPEDT:=P_MFLASTSCRAPEDT;
 end if;
 cur_colname:='P_MFCURSTATID';
 cur_colvalue:=P_MFCURSTATID;
 if P_MFCURSTATID is null then
 
 new_rec.MFCURSTATID:=0;
 
 else 
 new_rec.MFCURSTATID:=P_MFCURSTATID;
 end if;
 cur_colname:='P_MFCURCMNT';
 cur_colvalue:=P_MFCURCMNT;
 if P_MFCURCMNT is null then
 
 new_rec.MFCURCMNT:=' ';
 
 else 
 new_rec.MFCURCMNT:=P_MFCURCMNT;
 end if;
 cur_colname:='P_MFINITIALSTATID';
 cur_colvalue:=P_MFINITIALSTATID;
 if P_MFINITIALSTATID is null then
 
 new_rec.MFINITIALSTATID:=0;
 
 else 
 new_rec.MFINITIALSTATID:=P_MFINITIALSTATID;
 end if;
 cur_colname:='P_MFINITIALCMNT';
 cur_colvalue:=P_MFINITIALCMNT;
 if P_MFINITIALCMNT is null then
 
 new_rec.MFINITIALCMNT:=' ';
 
 else 
 new_rec.MFINITIALCMNT:=P_MFINITIALCMNT;
 end if;
 cur_colname:='P_MFLASTUPDTDT';
 cur_colvalue:=P_MFLASTUPDTDT;
 if P_MFLASTUPDTDT is null then
 
 new_rec.MFLASTUPDTDT:=jan_1_1900;
 
 else 
 new_rec.MFLASTUPDTDT:=P_MFLASTUPDTDT;
 end if;
 cur_colname:='P_MFISSDT';
 cur_colvalue:=P_MFISSDT;
 if P_MFISSDT is null then
 
 new_rec.MFISSDT:=jan_1_1900;
 
 else 
 new_rec.MFISSDT:=P_MFISSDT;
 end if;
 cur_colname:='P_MFLOANMATDT';
 cur_colvalue:=P_MFLOANMATDT;
 if P_MFLOANMATDT is null then
 
 new_rec.MFLOANMATDT:=jan_1_1900;
 
 else 
 new_rec.MFLOANMATDT:=P_MFLOANMATDT;
 end if;
 cur_colname:='P_MFORGLPRINAMT';
 cur_colvalue:=P_MFORGLPRINAMT;
 if P_MFORGLPRINAMT is null then
 
 new_rec.MFORGLPRINAMT:=0;
 
 else 
 new_rec.MFORGLPRINAMT:=P_MFORGLPRINAMT;
 end if;
 cur_colname:='P_MFINTRTPCT';
 cur_colvalue:=P_MFINTRTPCT;
 if P_MFINTRTPCT is null then
 
 new_rec.MFINTRTPCT:=0;
 
 else 
 new_rec.MFINTRTPCT:=P_MFINTRTPCT;
 end if;
 cur_colname:='P_MFDBENTRAMT';
 cur_colvalue:=P_MFDBENTRAMT;
 if P_MFDBENTRAMT is null then
 
 new_rec.MFDBENTRAMT:=0;
 
 else 
 new_rec.MFDBENTRAMT:=P_MFDBENTRAMT;
 end if;
 cur_colname:='P_MFTOBECURAMT';
 cur_colvalue:=P_MFTOBECURAMT;
 if P_MFTOBECURAMT is null then
 
 new_rec.MFTOBECURAMT:=0;
 
 else 
 new_rec.MFTOBECURAMT:=P_MFTOBECURAMT;
 end if;
 cur_colname:='P_MFLASTPYMTDT';
 cur_colvalue:=P_MFLASTPYMTDT;
 if P_MFLASTPYMTDT is null then
 
 new_rec.MFLASTPYMTDT:=jan_1_1900;
 
 else 
 new_rec.MFLASTPYMTDT:=P_MFLASTPYMTDT;
 end if;
 cur_colname:='P_MFESCROWPRIORPYMTAMT';
 cur_colvalue:=P_MFESCROWPRIORPYMTAMT;
 if P_MFESCROWPRIORPYMTAMT is null then
 
 new_rec.MFESCROWPRIORPYMTAMT:=0;
 
 else 
 new_rec.MFESCROWPRIORPYMTAMT:=P_MFESCROWPRIORPYMTAMT;
 end if;
 cur_colname:='P_MFCDCPAIDTHRUDT';
 cur_colvalue:=P_MFCDCPAIDTHRUDT;
 if P_MFCDCPAIDTHRUDT is null then
 
 new_rec.MFCDCPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.MFCDCPAIDTHRUDT:=P_MFCDCPAIDTHRUDT;
 end if;
 cur_colname:='P_MFCDCMODELNQ';
 cur_colvalue:=P_MFCDCMODELNQ;
 if P_MFCDCMODELNQ is null then
 
 new_rec.MFCDCMODELNQ:=0;
 
 else 
 new_rec.MFCDCMODELNQ:=P_MFCDCMODELNQ;
 end if;
 cur_colname:='P_MFCDCMOFEEAMT';
 cur_colvalue:=P_MFCDCMOFEEAMT;
 if P_MFCDCMOFEEAMT is null then
 
 new_rec.MFCDCMOFEEAMT:=0;
 
 else 
 new_rec.MFCDCMOFEEAMT:=P_MFCDCMOFEEAMT;
 end if;
 cur_colname:='P_MFCDCFEEAMT';
 cur_colvalue:=P_MFCDCFEEAMT;
 if P_MFCDCFEEAMT is null then
 
 new_rec.MFCDCFEEAMT:=0;
 
 else 
 new_rec.MFCDCFEEAMT:=P_MFCDCFEEAMT;
 end if;
 cur_colname:='P_MFCDCDUEFROMBORRAMT';
 cur_colvalue:=P_MFCDCDUEFROMBORRAMT;
 if P_MFCDCDUEFROMBORRAMT is null then
 
 new_rec.MFCDCDUEFROMBORRAMT:=0;
 
 else 
 new_rec.MFCDCDUEFROMBORRAMT:=P_MFCDCDUEFROMBORRAMT;
 end if;
 cur_colname:='P_MFCSAPAIDTHRUDT';
 cur_colvalue:=P_MFCSAPAIDTHRUDT;
 if P_MFCSAPAIDTHRUDT is null then
 
 new_rec.MFCSAPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.MFCSAPAIDTHRUDT:=P_MFCSAPAIDTHRUDT;
 end if;
 cur_colname:='P_MFCSAMODELNQ';
 cur_colvalue:=P_MFCSAMODELNQ;
 if P_MFCSAMODELNQ is null then
 
 new_rec.MFCSAMODELNQ:=0;
 
 else 
 new_rec.MFCSAMODELNQ:=P_MFCSAMODELNQ;
 end if;
 cur_colname:='P_MFCSAMOFEEAMT';
 cur_colvalue:=P_MFCSAMOFEEAMT;
 if P_MFCSAMOFEEAMT is null then
 
 new_rec.MFCSAMOFEEAMT:=0;
 
 else 
 new_rec.MFCSAMOFEEAMT:=P_MFCSAMOFEEAMT;
 end if;
 cur_colname:='P_MFCSAFEEAMT';
 cur_colvalue:=P_MFCSAFEEAMT;
 if P_MFCSAFEEAMT is null then
 
 new_rec.MFCSAFEEAMT:=0;
 
 else 
 new_rec.MFCSAFEEAMT:=P_MFCSAFEEAMT;
 end if;
 cur_colname:='P_MFCSADUEFROMBORRAMT';
 cur_colvalue:=P_MFCSADUEFROMBORRAMT;
 if P_MFCSADUEFROMBORRAMT is null then
 
 new_rec.MFCSADUEFROMBORRAMT:=0;
 
 else 
 new_rec.MFCSADUEFROMBORRAMT:=P_MFCSADUEFROMBORRAMT;
 end if;
 cur_colname:='P_PPAENTRYDT';
 cur_colvalue:=P_PPAENTRYDT;
 if P_PPAENTRYDT is null then
 
 new_rec.PPAENTRYDT:=jan_1_1900;
 
 else 
 new_rec.PPAENTRYDT:=P_PPAENTRYDT;
 end if;
 cur_colname:='P_PPAGROUP';
 cur_colvalue:=P_PPAGROUP;
 if P_PPAGROUP is null then
 
 new_rec.PPAGROUP:=' ';
 
 else 
 new_rec.PPAGROUP:=P_PPAGROUP;
 end if;
 cur_colname:='P_PPACDCNMB';
 cur_colvalue:=P_PPACDCNMB;
 if P_PPACDCNMB is null then
 
 new_rec.PPACDCNMB:=rpad(' ',8);
 
 else 
 new_rec.PPACDCNMB:=P_PPACDCNMB;
 end if;
 cur_colname:='P_PPAORGLPRINAMT';
 cur_colvalue:=P_PPAORGLPRINAMT;
 if P_PPAORGLPRINAMT is null then
 
 new_rec.PPAORGLPRINAMT:=0;
 
 else 
 new_rec.PPAORGLPRINAMT:=P_PPAORGLPRINAMT;
 end if;
 cur_colname:='P_PPADBENTRBALATSEMIANAMT';
 cur_colvalue:=P_PPADBENTRBALATSEMIANAMT;
 if P_PPADBENTRBALATSEMIANAMT is null then
 
 new_rec.PPADBENTRBALATSEMIANAMT:=0;
 
 else 
 new_rec.PPADBENTRBALATSEMIANAMT:=P_PPADBENTRBALATSEMIANAMT;
 end if;
 cur_colname:='P_PPAINTDUEATSEMIANAMT';
 cur_colvalue:=P_PPAINTDUEATSEMIANAMT;
 if P_PPAINTDUEATSEMIANAMT is null then
 
 new_rec.PPAINTDUEATSEMIANAMT:=0;
 
 else 
 new_rec.PPAINTDUEATSEMIANAMT:=P_PPAINTDUEATSEMIANAMT;
 end if;
 cur_colname:='P_PPADUEATSEMIANAMT';
 cur_colvalue:=P_PPADUEATSEMIANAMT;
 if P_PPADUEATSEMIANAMT is null then
 
 new_rec.PPADUEATSEMIANAMT:=0;
 
 else 
 new_rec.PPADUEATSEMIANAMT:=P_PPADUEATSEMIANAMT;
 end if;
 cur_colname:='P_SBAENTRYDT';
 cur_colvalue:=P_SBAENTRYDT;
 if P_SBAENTRYDT is null then
 
 new_rec.SBAENTRYDT:=jan_1_1900;
 
 else 
 new_rec.SBAENTRYDT:=P_SBAENTRYDT;
 end if;
 cur_colname:='P_SBALASTTRNSCRPTIMPTDT';
 cur_colvalue:=P_SBALASTTRNSCRPTIMPTDT;
 if P_SBALASTTRNSCRPTIMPTDT is null then
 
 new_rec.SBALASTTRNSCRPTIMPTDT:=jan_1_1900;
 
 else 
 new_rec.SBALASTTRNSCRPTIMPTDT:=P_SBALASTTRNSCRPTIMPTDT;
 end if;
 cur_colname:='P_SBADBENTRBALATSEMIANAMT';
 cur_colvalue:=P_SBADBENTRBALATSEMIANAMT;
 if P_SBADBENTRBALATSEMIANAMT is null then
 
 new_rec.SBADBENTRBALATSEMIANAMT:=0;
 
 else 
 new_rec.SBADBENTRBALATSEMIANAMT:=P_SBADBENTRBALATSEMIANAMT;
 end if;
 cur_colname:='P_SBAINTDUEATSEMIANAMT';
 cur_colvalue:=P_SBAINTDUEATSEMIANAMT;
 if P_SBAINTDUEATSEMIANAMT is null then
 
 new_rec.SBAINTDUEATSEMIANAMT:=0;
 
 else 
 new_rec.SBAINTDUEATSEMIANAMT:=P_SBAINTDUEATSEMIANAMT;
 end if;
 cur_colname:='P_SBACSAFEEAMT';
 cur_colvalue:=P_SBACSAFEEAMT;
 if P_SBACSAFEEAMT is null then
 
 new_rec.SBACSAFEEAMT:=0;
 
 else 
 new_rec.SBACSAFEEAMT:=P_SBACSAFEEAMT;
 end if;
 cur_colname:='P_SBACDCFEEAMT';
 cur_colvalue:=P_SBACDCFEEAMT;
 if P_SBACDCFEEAMT is null then
 
 new_rec.SBACDCFEEAMT:=0;
 
 else 
 new_rec.SBACDCFEEAMT:=P_SBACDCFEEAMT;
 end if;
 cur_colname:='P_SBAESCROWPRIORPYMTAMT';
 cur_colvalue:=P_SBAESCROWPRIORPYMTAMT;
 if P_SBAESCROWPRIORPYMTAMT is null then
 
 new_rec.SBAESCROWPRIORPYMTAMT:=0;
 
 else 
 new_rec.SBAESCROWPRIORPYMTAMT:=P_SBAESCROWPRIORPYMTAMT;
 end if;
 cur_colname:='P_SBAACCELAMT';
 cur_colvalue:=P_SBAACCELAMT;
 if P_SBAACCELAMT is null then
 
 new_rec.SBAACCELAMT:=0;
 
 else 
 new_rec.SBAACCELAMT:=P_SBAACCELAMT;
 end if;
 cur_colname:='P_SBADUEDT';
 cur_colvalue:=P_SBADUEDT;
 if P_SBADUEDT is null then
 
 new_rec.SBADUEDT:=jan_1_1900;
 
 else 
 new_rec.SBADUEDT:=P_SBADUEDT;
 end if;
 cur_colname:='P_SBANOTERTPCT';
 cur_colvalue:=P_SBANOTERTPCT;
 if P_SBANOTERTPCT is null then
 
 new_rec.SBANOTERTPCT:=0;
 
 else 
 new_rec.SBANOTERTPCT:=P_SBANOTERTPCT;
 end if;
 cur_colname:='P_SBALASTNOTEBALAMT';
 cur_colvalue:=P_SBALASTNOTEBALAMT;
 if P_SBALASTNOTEBALAMT is null then
 
 new_rec.SBALASTNOTEBALAMT:=0;
 
 else 
 new_rec.SBALASTNOTEBALAMT:=P_SBALASTNOTEBALAMT;
 end if;
 cur_colname:='P_SBALASTPYMTDT';
 cur_colvalue:=P_SBALASTPYMTDT;
 if P_SBALASTPYMTDT is null then
 
 new_rec.SBALASTPYMTDT:=jan_1_1900;
 
 else 
 new_rec.SBALASTPYMTDT:=P_SBALASTPYMTDT;
 end if;
 cur_colname:='P_SBAACCRINTDUEAMT';
 cur_colvalue:=P_SBAACCRINTDUEAMT;
 if P_SBAACCRINTDUEAMT is null then
 
 new_rec.SBAACCRINTDUEAMT:=0;
 
 else 
 new_rec.SBAACCRINTDUEAMT:=P_SBAACCRINTDUEAMT;
 end if;
 cur_colname:='P_SBAEXCESSAMT';
 cur_colvalue:=P_SBAEXCESSAMT;
 if P_SBAEXCESSAMT is null then
 
 new_rec.SBAEXCESSAMT:=0;
 
 else 
 new_rec.SBAEXCESSAMT:=P_SBAEXCESSAMT;
 end if;
 cur_colname:='P_MFPREVSTATID';
 cur_colvalue:=P_MFPREVSTATID;
 if P_MFPREVSTATID is null then
 
 new_rec.MFPREVSTATID:=0;
 
 else 
 new_rec.MFPREVSTATID:=P_MFPREVSTATID;
 end if;
 cur_colname:='P_MFPREVCMNT';
 cur_colvalue:=P_MFPREVCMNT;
 if P_MFPREVCMNT is null then
 
 new_rec.MFPREVCMNT:=' ';
 
 else 
 new_rec.MFPREVCMNT:=P_MFPREVCMNT;
 end if;
 cur_colname:='P_CDCNMB';
 cur_colvalue:=P_CDCNMB;
 if P_CDCNMB is null then
 
 new_rec.CDCNMB:=rpad(' ',8);
 
 else 
 new_rec.CDCNMB:=P_CDCNMB;
 end if;
 cur_colname:='P_SERVCNTRID';
 cur_colvalue:=P_SERVCNTRID;
 if P_SERVCNTRID is null then
 
 new_rec.SERVCNTRID:=' ';
 
 else 
 new_rec.SERVCNTRID:=P_SERVCNTRID;
 end if;
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 if P_TIMESTAMPFLD is null then
 
 new_rec.TIMESTAMPFLD:=jan_1_1900;
 
 else 
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(ACCELERATNID)=('||new_rec.ACCELERATNID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.ACCACCELERATELOANTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End ACCACCELERATELOANINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'ACCACCELERATELOANINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in ACCACCELERATELOANINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:20';
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'ACCACCELERATELOANINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in ACCACCELERATELOANINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:20';
 ROLLBACK TO ACCACCELERATELOANINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'ACCACCELERATELOANINSTSP',force_log_entry=>TRUE);
 --
 END ACCACCELERATELOANINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


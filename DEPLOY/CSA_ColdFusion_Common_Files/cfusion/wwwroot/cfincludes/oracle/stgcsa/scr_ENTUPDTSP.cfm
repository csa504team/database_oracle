<!--- Saved 10/09/2018 11:29:04. --->
PROCEDURE ENTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ENTID VARCHAR2:=null
 ,p_ENTNM VARCHAR2:=null
 ,p_ENTTYP VARCHAR2:=null
 ,p_ENTMAILADRSTR1NM VARCHAR2:=null
 ,p_ENTMAILADRSTR2NM VARCHAR2:=null
 ,p_ENTMAILADRCTYNM VARCHAR2:=null
 ,p_ENTMAILADRSTCD CHAR:=null
 ,p_ENTMAILADRZIPCD CHAR:=null
 ,p_ENTMAILADRZIP4CD CHAR:=null
 ,p_ENTPHNNMB CHAR:=null
 ,p_CREATBY VARCHAR2:=null
 ,p_UPDTBY VARCHAR2:=null
 ,p_REGNCD CHAR:=null
 ,p_ENTNMB CHAR:=null
 ,p_DISTCD CHAR:=null
 ,p_STATCD VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:25
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:45:25
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure ENTUPDTSP performs UPDATE on STGCSA.ENTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ENTID
 for P_IDENTIFIER=1 qualification is on ENTNM, ENTTYP
 for P_IDENTIFIER=2 qualification is on ENTPHNNMB
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.ENTTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.ENTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.ENTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_ENTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTID';
 cur_colvalue:=P_ENTID;
 DBrec.ENTID:=P_ENTID; 
 end if;
 if P_ENTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTNM';
 cur_colvalue:=P_ENTNM;
 DBrec.ENTNM:=P_ENTNM; 
 end if;
 if P_ENTTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTTYP';
 cur_colvalue:=P_ENTTYP;
 DBrec.ENTTYP:=P_ENTTYP; 
 end if;
 if P_ENTMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRSTR1NM';
 cur_colvalue:=P_ENTMAILADRSTR1NM;
 DBrec.ENTMAILADRSTR1NM:=P_ENTMAILADRSTR1NM; 
 end if;
 if P_ENTMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRSTR2NM';
 cur_colvalue:=P_ENTMAILADRSTR2NM;
 DBrec.ENTMAILADRSTR2NM:=P_ENTMAILADRSTR2NM; 
 end if;
 if P_ENTMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRCTYNM';
 cur_colvalue:=P_ENTMAILADRCTYNM;
 DBrec.ENTMAILADRCTYNM:=P_ENTMAILADRCTYNM; 
 end if;
 if P_ENTMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRSTCD';
 cur_colvalue:=P_ENTMAILADRSTCD;
 DBrec.ENTMAILADRSTCD:=P_ENTMAILADRSTCD; 
 end if;
 if P_ENTMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRZIPCD';
 cur_colvalue:=P_ENTMAILADRZIPCD;
 DBrec.ENTMAILADRZIPCD:=P_ENTMAILADRZIPCD; 
 end if;
 if P_ENTMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTMAILADRZIP4CD';
 cur_colvalue:=P_ENTMAILADRZIP4CD;
 DBrec.ENTMAILADRZIP4CD:=P_ENTMAILADRZIP4CD; 
 end if;
 if P_ENTPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTPHNNMB';
 cur_colvalue:=P_ENTPHNNMB;
 DBrec.ENTPHNNMB:=P_ENTPHNNMB; 
 end if;
 if P_CREATBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_CREATBY';
 cur_colvalue:=P_CREATBY;
 DBrec.CREATBY:=P_CREATBY; 
 end if;
 if P_UPDTBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_UPDTBY';
 cur_colvalue:=P_UPDTBY;
 DBrec.UPDTBY:=P_UPDTBY; 
 end if;
 if P_REGNCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_REGNCD';
 cur_colvalue:=P_REGNCD;
 DBrec.REGNCD:=P_REGNCD; 
 end if;
 if P_ENTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_ENTNMB';
 cur_colvalue:=P_ENTNMB;
 DBrec.ENTNMB:=P_ENTNMB; 
 end if;
 if P_DISTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DISTCD';
 cur_colvalue:=P_DISTCD;
 DBrec.DISTCD:=P_DISTCD; 
 end if;
 if P_STATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_STATCD';
 cur_colvalue:=P_STATCD;
 DBrec.STATCD:=P_STATCD; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.ENTTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,ENTID=DBrec.ENTID
 ,ENTNM=DBrec.ENTNM
 ,ENTTYP=DBrec.ENTTYP
 ,ENTMAILADRSTR1NM=DBrec.ENTMAILADRSTR1NM
 ,ENTMAILADRSTR2NM=DBrec.ENTMAILADRSTR2NM
 ,ENTMAILADRCTYNM=DBrec.ENTMAILADRCTYNM
 ,ENTMAILADRSTCD=DBrec.ENTMAILADRSTCD
 ,ENTMAILADRZIPCD=DBrec.ENTMAILADRZIPCD
 ,ENTMAILADRZIP4CD=DBrec.ENTMAILADRZIP4CD
 ,ENTPHNNMB=DBrec.ENTPHNNMB
 ,CREATBY=DBrec.CREATBY
 ,UPDTBY=DBrec.UPDTBY
 ,REGNCD=DBrec.REGNCD
 ,ENTNMB=DBrec.ENTNMB
 ,DISTCD=DBrec.DISTCD
 ,STATCD=DBrec.STATCD
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.ENTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ENTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init ENTUPDTSP',p_userid,4,
 logtxt1=>'ENTUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'ENTUPDTSP');
 --
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(ENTID)=('||P_ENTID||')';
 begin
 select rowid into rec_rowid from STGCSA.ENTTBL where 1=1
 and ENTID=P_ENTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ENTTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(ENTTYP)=('||P_ENTTYP||')'||' and '||'(ENTNM)=('||P_ENTNM||')';
 begin
 for r1 in (select rowid from STGCSA.ENTTBL a where 1=1 
 and ENTNM=P_ENTNM
 and ENTTYP=P_ENTTYP
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ENTTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ENTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(ENTPHNNMB)=('||P_ENTPHNNMB||')';
 begin
 for r2 in (select rowid from STGCSA.ENTTBL a where 1=1 
 and ENTPHNNMB=P_ENTPHNNMB
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ENTTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ENTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ENTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'ENTUPDTSP');
 else
 ROLLBACK TO ENTUPDTSP;
 p_errmsg:=p_errmsg||' in ENTUPDTSP build 2018-10-08 15:45:25';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ENTUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO ENTUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in ENTUPDTSP build 2018-10-08 15:45:25';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ENTUPDTSP',force_log_entry=>true);
 --
 END ENTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:03. --->
PROCEDURE CNTCTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CNTCTID VARCHAR2:=null
 ,p_CNTCTFIRSTNM VARCHAR2:=null
 ,p_CNTCTLASTNM VARCHAR2:=null
 ,p_CNTCTEMAILADR VARCHAR2:=null
 ,p_CNTCTPHNNMB VARCHAR2:=null
 ,p_CNTCTPHNXTN CHAR:=null
 ,p_CNTCTFAXNMB VARCHAR2:=null
 ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
 ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
 ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
 ,p_CNTCTMAILADRSTCD CHAR:=null
 ,p_CNTCTMAILADRZIPCD CHAR:=null
 ,p_CNTCTMAILADRZIP4CD CHAR:=null
 ,p_CNTCTTITL VARCHAR2:=null
 ,p_CDCREF VARCHAR2:=null
 ,p_CREATBY VARCHAR2:=null
 ,p_UPDTBY VARCHAR2:=null
 ,p_ENTID VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:48
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:48
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.CNTCTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.CNTCTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize CNTCTID from sequence CNTCTIDSEQ
 Function NEXTVAL_CNTCTIDSEQ return number is
 N number;
 begin
 select CNTCTIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint CNTCTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init CNTCTINSTSP',p_userid,4,
 logtxt1=>'CNTCTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'CNTCTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CNTCTID';
 cur_colvalue:=P_CNTCTID;
 if P_CNTCTID is null then
 
 new_rec.CNTCTID:=NEXTVAL_CNTCTIDSEQ();
 
 else 
 new_rec.CNTCTID:=P_CNTCTID;
 end if;
 cur_colname:='P_CNTCTFIRSTNM';
 cur_colvalue:=P_CNTCTFIRSTNM;
 if P_CNTCTFIRSTNM is null then
 
 new_rec.CNTCTFIRSTNM:=' ';
 
 else 
 new_rec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM;
 end if;
 cur_colname:='P_CNTCTLASTNM';
 cur_colvalue:=P_CNTCTLASTNM;
 if P_CNTCTLASTNM is null then
 
 new_rec.CNTCTLASTNM:=' ';
 
 else 
 new_rec.CNTCTLASTNM:=P_CNTCTLASTNM;
 end if;
 cur_colname:='P_CNTCTEMAILADR';
 cur_colvalue:=P_CNTCTEMAILADR;
 if P_CNTCTEMAILADR is null then
 
 new_rec.CNTCTEMAILADR:=' ';
 
 else 
 new_rec.CNTCTEMAILADR:=P_CNTCTEMAILADR;
 end if;
 cur_colname:='P_CNTCTPHNNMB';
 cur_colvalue:=P_CNTCTPHNNMB;
 if P_CNTCTPHNNMB is null then
 
 new_rec.CNTCTPHNNMB:=' ';
 
 else 
 new_rec.CNTCTPHNNMB:=P_CNTCTPHNNMB;
 end if;
 cur_colname:='P_CNTCTPHNXTN';
 cur_colvalue:=P_CNTCTPHNXTN;
 if P_CNTCTPHNXTN is null then
 
 new_rec.CNTCTPHNXTN:=rpad(' ',10);
 
 else 
 new_rec.CNTCTPHNXTN:=P_CNTCTPHNXTN;
 end if;
 cur_colname:='P_CNTCTFAXNMB';
 cur_colvalue:=P_CNTCTFAXNMB;
 if P_CNTCTFAXNMB is null then
 
 new_rec.CNTCTFAXNMB:=' ';
 
 else 
 new_rec.CNTCTFAXNMB:=P_CNTCTFAXNMB;
 end if;
 cur_colname:='P_CNTCTMAILADRSTR1NM';
 cur_colvalue:=P_CNTCTMAILADRSTR1NM;
 if P_CNTCTMAILADRSTR1NM is null then
 
 new_rec.CNTCTMAILADRSTR1NM:=' ';
 
 else 
 new_rec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM;
 end if;
 cur_colname:='P_CNTCTMAILADRSTR2NM';
 cur_colvalue:=P_CNTCTMAILADRSTR2NM;
 if P_CNTCTMAILADRSTR2NM is null then
 
 new_rec.CNTCTMAILADRSTR2NM:=' ';
 
 else 
 new_rec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM;
 end if;
 cur_colname:='P_CNTCTMAILADRCTYNM';
 cur_colvalue:=P_CNTCTMAILADRCTYNM;
 if P_CNTCTMAILADRCTYNM is null then
 
 new_rec.CNTCTMAILADRCTYNM:=' ';
 
 else 
 new_rec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM;
 end if;
 cur_colname:='P_CNTCTMAILADRSTCD';
 cur_colvalue:=P_CNTCTMAILADRSTCD;
 if P_CNTCTMAILADRSTCD is null then
 
 new_rec.CNTCTMAILADRSTCD:=rpad(' ',2);
 
 else 
 new_rec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD;
 end if;
 cur_colname:='P_CNTCTMAILADRZIPCD';
 cur_colvalue:=P_CNTCTMAILADRZIPCD;
 if P_CNTCTMAILADRZIPCD is null then
 
 new_rec.CNTCTMAILADRZIPCD:=rpad(' ',5);
 
 else 
 new_rec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD;
 end if;
 cur_colname:='P_CNTCTMAILADRZIP4CD';
 cur_colvalue:=P_CNTCTMAILADRZIP4CD;
 if P_CNTCTMAILADRZIP4CD is null then
 
 new_rec.CNTCTMAILADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD;
 end if;
 cur_colname:='P_CNTCTTITL';
 cur_colvalue:=P_CNTCTTITL;
 if P_CNTCTTITL is null then
 
 new_rec.CNTCTTITL:=' ';
 
 else 
 new_rec.CNTCTTITL:=P_CNTCTTITL;
 end if;
 cur_colname:='P_CDCREF';
 cur_colvalue:=P_CDCREF;
 if P_CDCREF is null then
 
 new_rec.CDCREF:=0;
 
 else 
 new_rec.CDCREF:=P_CDCREF;
 end if;
 cur_colname:='P_CREATBY';
 cur_colvalue:=P_CREATBY;
 if P_CREATBY is null then
 
 new_rec.CREATBY:=0;
 
 else 
 new_rec.CREATBY:=P_CREATBY;
 end if;
 cur_colname:='P_UPDTBY';
 cur_colvalue:=P_UPDTBY;
 if P_UPDTBY is null then
 
 new_rec.UPDTBY:=0;
 
 else 
 new_rec.UPDTBY:=P_UPDTBY;
 end if;
 cur_colname:='P_ENTID';
 cur_colvalue:=P_ENTID;
 if P_ENTID is null then
 
 new_rec.ENTID:=0;
 
 else 
 new_rec.ENTID:=P_ENTID;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CNTCTID)=('||new_rec.CNTCTID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.CNTCTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End CNTCTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'CNTCTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in CNTCTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:48';
 ROLLBACK TO CNTCTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'CNTCTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in CNTCTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:48';
 ROLLBACK TO CNTCTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'CNTCTINSTSP',force_log_entry=>TRUE);
 --
 END CNTCTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


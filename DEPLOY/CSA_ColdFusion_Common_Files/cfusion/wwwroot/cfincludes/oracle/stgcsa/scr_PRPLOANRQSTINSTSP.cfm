<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPLOANRQSTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_RQSTREF VARCHAR2:=null
 ,p_RQSTCD VARCHAR2:=null
 ,p_CDCREGNCD CHAR:=null
 ,p_CDCNMB CHAR:=null
 ,p_LOANSTAT VARCHAR2:=null
 ,p_BORRNM VARCHAR2:=null
 ,p_PREPAYAMT VARCHAR2:=null
 ,p_PREPAYDT DATE:=null
 ,p_PREPAYMO VARCHAR2:=null
 ,p_PREPAYYRNMB CHAR:=null
 ,p_PREPAYCALCDT DATE:=null
 ,p_SEMIANNDT DATE:=null
 ,p_SEMIANMO VARCHAR2:=null
 ,p_SEMIANNAMT VARCHAR2:=null
 ,p_SEMIANNPYMTAMT VARCHAR2:=null
 ,p_PRINCURAMT VARCHAR2:=null
 ,p_BALAMT VARCHAR2:=null
 ,p_BALCALCAMT VARCHAR2:=null
 ,p_NOTEBALCALCAMT VARCHAR2:=null
 ,p_PIAMT VARCHAR2:=null
 ,p_NOTERTPCT VARCHAR2:=null
 ,p_ISSDT DATE:=null
 ,p_DBENTRBALAMT VARCHAR2:=null
 ,p_PREPAYPREMAMT VARCHAR2:=null
 ,p_POSTINGDT DATE:=null
 ,p_POSTINGMO VARCHAR2:=null
 ,p_POSTINGYRNMB CHAR:=null
 ,p_BUSDAY6DT DATE:=null
 ,p_THIRDTHURSDAY DATE:=null
 ,p_ACTUALRQST VARCHAR2:=null
 ,p_CSAPAIDTHRUDT CHAR:=null
 ,p_CDCPAIDTHRUDT CHAR:=null
 ,p_BORRPAIDTHRUDT CHAR:=null
 ,p_INTPAIDTHRUDT CHAR:=null
 ,p_APPVDT DATE:=null
 ,p_PRINNEEDEDAMT VARCHAR2:=null
 ,p_INTNEEDEDAMT VARCHAR2:=null
 ,p_MODELNQ VARCHAR2:=null
 ,p_CURRFEEBALAMT VARCHAR2:=null
 ,p_CSAFEENEEDEDAMT VARCHAR2:=null
 ,p_CDCFEENEEDEDAMT VARCHAR2:=null
 ,p_FEENEEDEDAMT VARCHAR2:=null
 ,p_LATEFEENEEDEDAMT VARCHAR2:=null
 ,p_CSADUEFROMBORRAMT VARCHAR2:=null
 ,p_CDCDUEFROMBORRAMT VARCHAR2:=null
 ,p_DUEFROMBORRAMT VARCHAR2:=null
 ,p_CSABEHINDAMT VARCHAR2:=null
 ,p_CDCBEHINDAMT VARCHAR2:=null
 ,p_BEHINDAMT VARCHAR2:=null
 ,p_BORRFEEAMT VARCHAR2:=null
 ,p_CDCFEEDISBAMT VARCHAR2:=null
 ,p_AMORTCDCAMT VARCHAR2:=null
 ,p_AMORTSBAAMT VARCHAR2:=null
 ,p_AMORTCSAAMT VARCHAR2:=null
 ,p_CDCPCT VARCHAR2:=null
 ,p_PREPAYPCT VARCHAR2:=null
 ,p_FEECALCAMT VARCHAR2:=null
 ,p_CSAFEECALCAMT VARCHAR2:=null
 ,p_CSAFEECALCONLYAMT VARCHAR2:=null
 ,p_CDCFEECALCONLYAMT VARCHAR2:=null
 ,p_FEECALCONLYAMT VARCHAR2:=null
 ,p_FIVEYRADJ VARCHAR2:=null
 ,p_FIVEYRADJCSAAMT VARCHAR2:=null
 ,p_FIVEYRADJCDCAMT VARCHAR2:=null
 ,p_FIVEYRADJSBAAMT VARCHAR2:=null
 ,p_BALCALC1AMT VARCHAR2:=null
 ,p_BALCALC2AMT VARCHAR2:=null
 ,p_BALCALC3AMT VARCHAR2:=null
 ,p_BALCALC4AMT VARCHAR2:=null
 ,p_BALCALC5AMT VARCHAR2:=null
 ,p_BALCALC6AMT VARCHAR2:=null
 ,p_PRINAMT VARCHAR2:=null
 ,p_PRIN2AMT VARCHAR2:=null
 ,p_PRIN3AMT VARCHAR2:=null
 ,p_PRIN4AMT VARCHAR2:=null
 ,p_PRIN5AMT VARCHAR2:=null
 ,p_PRIN6AMT VARCHAR2:=null
 ,p_PRINPROJAMT VARCHAR2:=null
 ,p_PIPROJAMT VARCHAR2:=null
 ,p_INTAMT VARCHAR2:=null
 ,p_INT2AMT VARCHAR2:=null
 ,p_INT3AMT VARCHAR2:=null
 ,p_INT4AMT VARCHAR2:=null
 ,p_INT5AMT VARCHAR2:=null
 ,p_INT6AMT VARCHAR2:=null
 ,p_ESCROWAMT VARCHAR2:=null
 ,p_PI6MOAMT VARCHAR2:=null
 ,p_GFDAMT VARCHAR2:=null
 ,p_OPENGNTYAMT VARCHAR2:=null
 ,p_OPENGNTYERRAMT VARCHAR2:=null
 ,p_OPENGNTYREGAMT VARCHAR2:=null
 ,p_OPENGNTYUNALLOCAMT VARCHAR2:=null
 ,p_UNALLOCAMT VARCHAR2:=null
 ,p_USERLCK VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ,p_CDCSBAFEEDISBAMT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:28
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:46:28
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.PRPLOANRQSTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.PRPLOANRQSTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint PRPLOANRQSTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init PRPLOANRQSTINSTSP',p_userid,4,
 logtxt1=>'PRPLOANRQSTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'PRPLOANRQSTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; cur_colname:='TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 
 new_rec.LOANNMB:=rpad(' ',10);
 
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_RQSTREF';
 cur_colvalue:=P_RQSTREF;
 if P_RQSTREF is null then
 
 new_rec.RQSTREF:=0;
 
 else 
 new_rec.RQSTREF:=P_RQSTREF;
 end if;
 cur_colname:='P_RQSTCD';
 cur_colvalue:=P_RQSTCD;
 if P_RQSTCD is null then
 
 new_rec.RQSTCD:=0;
 
 else 
 new_rec.RQSTCD:=P_RQSTCD;
 end if;
 cur_colname:='P_CDCREGNCD';
 cur_colvalue:=P_CDCREGNCD;
 if P_CDCREGNCD is null then
 
 new_rec.CDCREGNCD:=rpad(' ',2);
 
 else 
 new_rec.CDCREGNCD:=P_CDCREGNCD;
 end if;
 cur_colname:='P_CDCNMB';
 cur_colvalue:=P_CDCNMB;
 if P_CDCNMB is null then
 
 new_rec.CDCNMB:=rpad(' ',4);
 
 else 
 new_rec.CDCNMB:=P_CDCNMB;
 end if;
 cur_colname:='P_LOANSTAT';
 cur_colvalue:=P_LOANSTAT;
 if P_LOANSTAT is null then
 
 new_rec.LOANSTAT:=' ';
 
 else 
 new_rec.LOANSTAT:=P_LOANSTAT;
 end if;
 cur_colname:='P_BORRNM';
 cur_colvalue:=P_BORRNM;
 if P_BORRNM is null then
 
 new_rec.BORRNM:=' ';
 
 else 
 new_rec.BORRNM:=P_BORRNM;
 end if;
 cur_colname:='P_PREPAYAMT';
 cur_colvalue:=P_PREPAYAMT;
 if P_PREPAYAMT is null then
 
 new_rec.PREPAYAMT:=0;
 
 else 
 new_rec.PREPAYAMT:=P_PREPAYAMT;
 end if;
 cur_colname:='P_PREPAYDT';
 cur_colvalue:=P_PREPAYDT;
 if P_PREPAYDT is null then
 
 new_rec.PREPAYDT:=jan_1_1900;
 
 else 
 new_rec.PREPAYDT:=P_PREPAYDT;
 end if;
 cur_colname:='P_PREPAYMO';
 cur_colvalue:=P_PREPAYMO;
 if P_PREPAYMO is null then
 
 new_rec.PREPAYMO:=0;
 
 else 
 new_rec.PREPAYMO:=P_PREPAYMO;
 end if;
 cur_colname:='P_PREPAYYRNMB';
 cur_colvalue:=P_PREPAYYRNMB;
 if P_PREPAYYRNMB is null then
 
 new_rec.PREPAYYRNMB:=rpad(' ',4);
 
 else 
 new_rec.PREPAYYRNMB:=P_PREPAYYRNMB;
 end if;
 cur_colname:='P_PREPAYCALCDT';
 cur_colvalue:=P_PREPAYCALCDT;
 if P_PREPAYCALCDT is null then
 
 new_rec.PREPAYCALCDT:=jan_1_1900;
 
 else 
 new_rec.PREPAYCALCDT:=P_PREPAYCALCDT;
 end if;
 cur_colname:='P_SEMIANNDT';
 cur_colvalue:=P_SEMIANNDT;
 if P_SEMIANNDT is null then
 
 new_rec.SEMIANNDT:=jan_1_1900;
 
 else 
 new_rec.SEMIANNDT:=P_SEMIANNDT;
 end if;
 cur_colname:='P_SEMIANMO';
 cur_colvalue:=P_SEMIANMO;
 if P_SEMIANMO is null then
 
 new_rec.SEMIANMO:=0;
 
 else 
 new_rec.SEMIANMO:=P_SEMIANMO;
 end if;
 cur_colname:='P_SEMIANNAMT';
 cur_colvalue:=P_SEMIANNAMT;
 if P_SEMIANNAMT is null then
 
 new_rec.SEMIANNAMT:=0;
 
 else 
 new_rec.SEMIANNAMT:=P_SEMIANNAMT;
 end if;
 cur_colname:='P_SEMIANNPYMTAMT';
 cur_colvalue:=P_SEMIANNPYMTAMT;
 if P_SEMIANNPYMTAMT is null then
 
 new_rec.SEMIANNPYMTAMT:=0;
 
 else 
 new_rec.SEMIANNPYMTAMT:=P_SEMIANNPYMTAMT;
 end if;
 cur_colname:='P_PRINCURAMT';
 cur_colvalue:=P_PRINCURAMT;
 if P_PRINCURAMT is null then
 
 new_rec.PRINCURAMT:=0;
 
 else 
 new_rec.PRINCURAMT:=P_PRINCURAMT;
 end if;
 cur_colname:='P_BALAMT';
 cur_colvalue:=P_BALAMT;
 if P_BALAMT is null then
 
 new_rec.BALAMT:=0;
 
 else 
 new_rec.BALAMT:=P_BALAMT;
 end if;
 cur_colname:='P_BALCALCAMT';
 cur_colvalue:=P_BALCALCAMT;
 if P_BALCALCAMT is null then
 
 new_rec.BALCALCAMT:=0;
 
 else 
 new_rec.BALCALCAMT:=P_BALCALCAMT;
 end if;
 cur_colname:='P_NOTEBALCALCAMT';
 cur_colvalue:=P_NOTEBALCALCAMT;
 if P_NOTEBALCALCAMT is null then
 
 new_rec.NOTEBALCALCAMT:=0;
 
 else 
 new_rec.NOTEBALCALCAMT:=P_NOTEBALCALCAMT;
 end if;
 cur_colname:='P_PIAMT';
 cur_colvalue:=P_PIAMT;
 if P_PIAMT is null then
 
 new_rec.PIAMT:=0;
 
 else 
 new_rec.PIAMT:=P_PIAMT;
 end if;
 cur_colname:='P_NOTERTPCT';
 cur_colvalue:=P_NOTERTPCT;
 if P_NOTERTPCT is null then
 
 new_rec.NOTERTPCT:=0;
 
 else 
 new_rec.NOTERTPCT:=P_NOTERTPCT;
 end if;
 cur_colname:='P_ISSDT';
 cur_colvalue:=P_ISSDT;
 if P_ISSDT is null then
 
 new_rec.ISSDT:=jan_1_1900;
 
 else 
 new_rec.ISSDT:=P_ISSDT;
 end if;
 cur_colname:='P_DBENTRBALAMT';
 cur_colvalue:=P_DBENTRBALAMT;
 if P_DBENTRBALAMT is null then
 
 new_rec.DBENTRBALAMT:=0;
 
 else 
 new_rec.DBENTRBALAMT:=P_DBENTRBALAMT;
 end if;
 cur_colname:='P_PREPAYPREMAMT';
 cur_colvalue:=P_PREPAYPREMAMT;
 if P_PREPAYPREMAMT is null then
 
 new_rec.PREPAYPREMAMT:=0;
 
 else 
 new_rec.PREPAYPREMAMT:=P_PREPAYPREMAMT;
 end if;
 cur_colname:='P_POSTINGDT';
 cur_colvalue:=P_POSTINGDT;
 if P_POSTINGDT is null then
 
 new_rec.POSTINGDT:=jan_1_1900;
 
 else 
 new_rec.POSTINGDT:=P_POSTINGDT;
 end if;
 cur_colname:='P_POSTINGMO';
 cur_colvalue:=P_POSTINGMO;
 if P_POSTINGMO is null then
 
 new_rec.POSTINGMO:=0;
 
 else 
 new_rec.POSTINGMO:=P_POSTINGMO;
 end if;
 cur_colname:='P_POSTINGYRNMB';
 cur_colvalue:=P_POSTINGYRNMB;
 if P_POSTINGYRNMB is null then
 
 new_rec.POSTINGYRNMB:=rpad(' ',4);
 
 else 
 new_rec.POSTINGYRNMB:=P_POSTINGYRNMB;
 end if;
 cur_colname:='P_BUSDAY6DT';
 cur_colvalue:=P_BUSDAY6DT;
 if P_BUSDAY6DT is null then
 
 new_rec.BUSDAY6DT:=jan_1_1900;
 
 else 
 new_rec.BUSDAY6DT:=P_BUSDAY6DT;
 end if;
 cur_colname:='P_THIRDTHURSDAY';
 cur_colvalue:=P_THIRDTHURSDAY;
 if P_THIRDTHURSDAY is null then
 
 new_rec.THIRDTHURSDAY:=jan_1_1900;
 
 else 
 new_rec.THIRDTHURSDAY:=P_THIRDTHURSDAY;
 end if;
 cur_colname:='P_ACTUALRQST';
 cur_colvalue:=P_ACTUALRQST;
 if P_ACTUALRQST is null then
 
 new_rec.ACTUALRQST:=' ';
 
 else 
 new_rec.ACTUALRQST:=P_ACTUALRQST;
 end if;
 cur_colname:='P_CSAPAIDTHRUDT';
 cur_colvalue:=P_CSAPAIDTHRUDT;
 if P_CSAPAIDTHRUDT is null then
 
 new_rec.CSAPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT;
 end if;
 cur_colname:='P_CDCPAIDTHRUDT';
 cur_colvalue:=P_CDCPAIDTHRUDT;
 if P_CDCPAIDTHRUDT is null then
 
 new_rec.CDCPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT;
 end if;
 cur_colname:='P_BORRPAIDTHRUDT';
 cur_colvalue:=P_BORRPAIDTHRUDT;
 if P_BORRPAIDTHRUDT is null then
 
 new_rec.BORRPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT;
 end if;
 cur_colname:='P_INTPAIDTHRUDT';
 cur_colvalue:=P_INTPAIDTHRUDT;
 if P_INTPAIDTHRUDT is null then
 
 new_rec.INTPAIDTHRUDT:=rpad(' ',10);
 
 else 
 new_rec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT;
 end if;
 cur_colname:='P_APPVDT';
 cur_colvalue:=P_APPVDT;
 if P_APPVDT is null then
 
 new_rec.APPVDT:=jan_1_1900;
 
 else 
 new_rec.APPVDT:=P_APPVDT;
 end if;
 cur_colname:='P_PRINNEEDEDAMT';
 cur_colvalue:=P_PRINNEEDEDAMT;
 if P_PRINNEEDEDAMT is null then
 
 new_rec.PRINNEEDEDAMT:=0;
 
 else 
 new_rec.PRINNEEDEDAMT:=P_PRINNEEDEDAMT;
 end if;
 cur_colname:='P_INTNEEDEDAMT';
 cur_colvalue:=P_INTNEEDEDAMT;
 if P_INTNEEDEDAMT is null then
 
 new_rec.INTNEEDEDAMT:=0;
 
 else 
 new_rec.INTNEEDEDAMT:=P_INTNEEDEDAMT;
 end if;
 cur_colname:='P_MODELNQ';
 cur_colvalue:=P_MODELNQ;
 if P_MODELNQ is null then
 
 new_rec.MODELNQ:=0;
 
 else 
 new_rec.MODELNQ:=P_MODELNQ;
 end if;
 cur_colname:='P_CURRFEEBALAMT';
 cur_colvalue:=P_CURRFEEBALAMT;
 if P_CURRFEEBALAMT is null then
 
 new_rec.CURRFEEBALAMT:=0;
 
 else 
 new_rec.CURRFEEBALAMT:=P_CURRFEEBALAMT;
 end if;
 cur_colname:='P_CSAFEENEEDEDAMT';
 cur_colvalue:=P_CSAFEENEEDEDAMT;
 if P_CSAFEENEEDEDAMT is null then
 
 new_rec.CSAFEENEEDEDAMT:=0;
 
 else 
 new_rec.CSAFEENEEDEDAMT:=P_CSAFEENEEDEDAMT;
 end if;
 cur_colname:='P_CDCFEENEEDEDAMT';
 cur_colvalue:=P_CDCFEENEEDEDAMT;
 if P_CDCFEENEEDEDAMT is null then
 
 new_rec.CDCFEENEEDEDAMT:=0;
 
 else 
 new_rec.CDCFEENEEDEDAMT:=P_CDCFEENEEDEDAMT;
 end if;
 cur_colname:='P_FEENEEDEDAMT';
 cur_colvalue:=P_FEENEEDEDAMT;
 if P_FEENEEDEDAMT is null then
 
 new_rec.FEENEEDEDAMT:=0;
 
 else 
 new_rec.FEENEEDEDAMT:=P_FEENEEDEDAMT;
 end if;
 cur_colname:='P_LATEFEENEEDEDAMT';
 cur_colvalue:=P_LATEFEENEEDEDAMT;
 if P_LATEFEENEEDEDAMT is null then
 
 new_rec.LATEFEENEEDEDAMT:=0;
 
 else 
 new_rec.LATEFEENEEDEDAMT:=P_LATEFEENEEDEDAMT;
 end if;
 cur_colname:='P_CSADUEFROMBORRAMT';
 cur_colvalue:=P_CSADUEFROMBORRAMT;
 if P_CSADUEFROMBORRAMT is null then
 
 new_rec.CSADUEFROMBORRAMT:=0;
 
 else 
 new_rec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT;
 end if;
 cur_colname:='P_CDCDUEFROMBORRAMT';
 cur_colvalue:=P_CDCDUEFROMBORRAMT;
 if P_CDCDUEFROMBORRAMT is null then
 
 new_rec.CDCDUEFROMBORRAMT:=0;
 
 else 
 new_rec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT;
 end if;
 cur_colname:='P_DUEFROMBORRAMT';
 cur_colvalue:=P_DUEFROMBORRAMT;
 if P_DUEFROMBORRAMT is null then
 
 new_rec.DUEFROMBORRAMT:=0;
 
 else 
 new_rec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT;
 end if;
 cur_colname:='P_CSABEHINDAMT';
 cur_colvalue:=P_CSABEHINDAMT;
 if P_CSABEHINDAMT is null then
 
 new_rec.CSABEHINDAMT:=0;
 
 else 
 new_rec.CSABEHINDAMT:=P_CSABEHINDAMT;
 end if;
 cur_colname:='P_CDCBEHINDAMT';
 cur_colvalue:=P_CDCBEHINDAMT;
 if P_CDCBEHINDAMT is null then
 
 new_rec.CDCBEHINDAMT:=0;
 
 else 
 new_rec.CDCBEHINDAMT:=P_CDCBEHINDAMT;
 end if;
 cur_colname:='P_BEHINDAMT';
 cur_colvalue:=P_BEHINDAMT;
 if P_BEHINDAMT is null then
 
 new_rec.BEHINDAMT:=0;
 
 else 
 new_rec.BEHINDAMT:=P_BEHINDAMT;
 end if;
 cur_colname:='P_BORRFEEAMT';
 cur_colvalue:=P_BORRFEEAMT;
 if P_BORRFEEAMT is null then
 
 new_rec.BORRFEEAMT:=0;
 
 else 
 new_rec.BORRFEEAMT:=P_BORRFEEAMT;
 end if;
 cur_colname:='P_CDCFEEDISBAMT';
 cur_colvalue:=P_CDCFEEDISBAMT;
 if P_CDCFEEDISBAMT is null then
 
 new_rec.CDCFEEDISBAMT:=0;
 
 else 
 new_rec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT;
 end if;
 cur_colname:='P_AMORTCDCAMT';
 cur_colvalue:=P_AMORTCDCAMT;
 if P_AMORTCDCAMT is null then
 
 new_rec.AMORTCDCAMT:=0;
 
 else 
 new_rec.AMORTCDCAMT:=P_AMORTCDCAMT;
 end if;
 cur_colname:='P_AMORTSBAAMT';
 cur_colvalue:=P_AMORTSBAAMT;
 if P_AMORTSBAAMT is null then
 
 new_rec.AMORTSBAAMT:=0;
 
 else 
 new_rec.AMORTSBAAMT:=P_AMORTSBAAMT;
 end if;
 cur_colname:='P_AMORTCSAAMT';
 cur_colvalue:=P_AMORTCSAAMT;
 if P_AMORTCSAAMT is null then
 
 new_rec.AMORTCSAAMT:=0;
 
 else 
 new_rec.AMORTCSAAMT:=P_AMORTCSAAMT;
 end if;
 cur_colname:='P_CDCPCT';
 cur_colvalue:=P_CDCPCT;
 if P_CDCPCT is null then
 
 new_rec.CDCPCT:=0;
 
 else 
 new_rec.CDCPCT:=P_CDCPCT;
 end if;
 cur_colname:='P_PREPAYPCT';
 cur_colvalue:=P_PREPAYPCT;
 if P_PREPAYPCT is null then
 
 new_rec.PREPAYPCT:=0;
 
 else 
 new_rec.PREPAYPCT:=P_PREPAYPCT;
 end if;
 cur_colname:='P_FEECALCAMT';
 cur_colvalue:=P_FEECALCAMT;
 if P_FEECALCAMT is null then
 
 new_rec.FEECALCAMT:=0;
 
 else 
 new_rec.FEECALCAMT:=P_FEECALCAMT;
 end if;
 cur_colname:='P_CSAFEECALCAMT';
 cur_colvalue:=P_CSAFEECALCAMT;
 if P_CSAFEECALCAMT is null then
 
 new_rec.CSAFEECALCAMT:=0;
 
 else 
 new_rec.CSAFEECALCAMT:=P_CSAFEECALCAMT;
 end if;
 cur_colname:='P_CSAFEECALCONLYAMT';
 cur_colvalue:=P_CSAFEECALCONLYAMT;
 if P_CSAFEECALCONLYAMT is null then
 
 new_rec.CSAFEECALCONLYAMT:=0;
 
 else 
 new_rec.CSAFEECALCONLYAMT:=P_CSAFEECALCONLYAMT;
 end if;
 cur_colname:='P_CDCFEECALCONLYAMT';
 cur_colvalue:=P_CDCFEECALCONLYAMT;
 if P_CDCFEECALCONLYAMT is null then
 
 new_rec.CDCFEECALCONLYAMT:=0;
 
 else 
 new_rec.CDCFEECALCONLYAMT:=P_CDCFEECALCONLYAMT;
 end if;
 cur_colname:='P_FEECALCONLYAMT';
 cur_colvalue:=P_FEECALCONLYAMT;
 if P_FEECALCONLYAMT is null then
 
 new_rec.FEECALCONLYAMT:=0;
 
 else 
 new_rec.FEECALCONLYAMT:=P_FEECALCONLYAMT;
 end if;
 cur_colname:='P_FIVEYRADJ';
 cur_colvalue:=P_FIVEYRADJ;
 if P_FIVEYRADJ is null then
 
 new_rec.FIVEYRADJ:=0;
 
 else 
 new_rec.FIVEYRADJ:=P_FIVEYRADJ;
 end if;
 cur_colname:='P_FIVEYRADJCSAAMT';
 cur_colvalue:=P_FIVEYRADJCSAAMT;
 if P_FIVEYRADJCSAAMT is null then
 
 new_rec.FIVEYRADJCSAAMT:=0;
 
 else 
 new_rec.FIVEYRADJCSAAMT:=P_FIVEYRADJCSAAMT;
 end if;
 cur_colname:='P_FIVEYRADJCDCAMT';
 cur_colvalue:=P_FIVEYRADJCDCAMT;
 if P_FIVEYRADJCDCAMT is null then
 
 new_rec.FIVEYRADJCDCAMT:=0;
 
 else 
 new_rec.FIVEYRADJCDCAMT:=P_FIVEYRADJCDCAMT;
 end if;
 cur_colname:='P_FIVEYRADJSBAAMT';
 cur_colvalue:=P_FIVEYRADJSBAAMT;
 if P_FIVEYRADJSBAAMT is null then
 
 new_rec.FIVEYRADJSBAAMT:=0;
 
 else 
 new_rec.FIVEYRADJSBAAMT:=P_FIVEYRADJSBAAMT;
 end if;
 cur_colname:='P_BALCALC1AMT';
 cur_colvalue:=P_BALCALC1AMT;
 if P_BALCALC1AMT is null then
 
 new_rec.BALCALC1AMT:=0;
 
 else 
 new_rec.BALCALC1AMT:=P_BALCALC1AMT;
 end if;
 cur_colname:='P_BALCALC2AMT';
 cur_colvalue:=P_BALCALC2AMT;
 if P_BALCALC2AMT is null then
 
 new_rec.BALCALC2AMT:=0;
 
 else 
 new_rec.BALCALC2AMT:=P_BALCALC2AMT;
 end if;
 cur_colname:='P_BALCALC3AMT';
 cur_colvalue:=P_BALCALC3AMT;
 if P_BALCALC3AMT is null then
 
 new_rec.BALCALC3AMT:=0;
 
 else 
 new_rec.BALCALC3AMT:=P_BALCALC3AMT;
 end if;
 cur_colname:='P_BALCALC4AMT';
 cur_colvalue:=P_BALCALC4AMT;
 if P_BALCALC4AMT is null then
 
 new_rec.BALCALC4AMT:=0;
 
 else 
 new_rec.BALCALC4AMT:=P_BALCALC4AMT;
 end if;
 cur_colname:='P_BALCALC5AMT';
 cur_colvalue:=P_BALCALC5AMT;
 if P_BALCALC5AMT is null then
 
 new_rec.BALCALC5AMT:=0;
 
 else 
 new_rec.BALCALC5AMT:=P_BALCALC5AMT;
 end if;
 cur_colname:='P_BALCALC6AMT';
 cur_colvalue:=P_BALCALC6AMT;
 if P_BALCALC6AMT is null then
 
 new_rec.BALCALC6AMT:=0;
 
 else 
 new_rec.BALCALC6AMT:=P_BALCALC6AMT;
 end if;
 cur_colname:='P_PRINAMT';
 cur_colvalue:=P_PRINAMT;
 if P_PRINAMT is null then
 
 new_rec.PRINAMT:=0;
 
 else 
 new_rec.PRINAMT:=P_PRINAMT;
 end if;
 cur_colname:='P_PRIN2AMT';
 cur_colvalue:=P_PRIN2AMT;
 if P_PRIN2AMT is null then
 
 new_rec.PRIN2AMT:=0;
 
 else 
 new_rec.PRIN2AMT:=P_PRIN2AMT;
 end if;
 cur_colname:='P_PRIN3AMT';
 cur_colvalue:=P_PRIN3AMT;
 if P_PRIN3AMT is null then
 
 new_rec.PRIN3AMT:=0;
 
 else 
 new_rec.PRIN3AMT:=P_PRIN3AMT;
 end if;
 cur_colname:='P_PRIN4AMT';
 cur_colvalue:=P_PRIN4AMT;
 if P_PRIN4AMT is null then
 
 new_rec.PRIN4AMT:=0;
 
 else 
 new_rec.PRIN4AMT:=P_PRIN4AMT;
 end if;
 cur_colname:='P_PRIN5AMT';
 cur_colvalue:=P_PRIN5AMT;
 if P_PRIN5AMT is null then
 
 new_rec.PRIN5AMT:=0;
 
 else 
 new_rec.PRIN5AMT:=P_PRIN5AMT;
 end if;
 cur_colname:='P_PRIN6AMT';
 cur_colvalue:=P_PRIN6AMT;
 if P_PRIN6AMT is null then
 
 new_rec.PRIN6AMT:=0;
 
 else 
 new_rec.PRIN6AMT:=P_PRIN6AMT;
 end if;
 cur_colname:='P_PRINPROJAMT';
 cur_colvalue:=P_PRINPROJAMT;
 if P_PRINPROJAMT is null then
 
 new_rec.PRINPROJAMT:=0;
 
 else 
 new_rec.PRINPROJAMT:=P_PRINPROJAMT;
 end if;
 cur_colname:='P_PIPROJAMT';
 cur_colvalue:=P_PIPROJAMT;
 if P_PIPROJAMT is null then
 
 new_rec.PIPROJAMT:=0;
 
 else 
 new_rec.PIPROJAMT:=P_PIPROJAMT;
 end if;
 cur_colname:='P_INTAMT';
 cur_colvalue:=P_INTAMT;
 if P_INTAMT is null then
 
 new_rec.INTAMT:=0;
 
 else 
 new_rec.INTAMT:=P_INTAMT;
 end if;
 cur_colname:='P_INT2AMT';
 cur_colvalue:=P_INT2AMT;
 if P_INT2AMT is null then
 
 new_rec.INT2AMT:=0;
 
 else 
 new_rec.INT2AMT:=P_INT2AMT;
 end if;
 cur_colname:='P_INT3AMT';
 cur_colvalue:=P_INT3AMT;
 if P_INT3AMT is null then
 
 new_rec.INT3AMT:=0;
 
 else 
 new_rec.INT3AMT:=P_INT3AMT;
 end if;
 cur_colname:='P_INT4AMT';
 cur_colvalue:=P_INT4AMT;
 if P_INT4AMT is null then
 
 new_rec.INT4AMT:=0;
 
 else 
 new_rec.INT4AMT:=P_INT4AMT;
 end if;
 cur_colname:='P_INT5AMT';
 cur_colvalue:=P_INT5AMT;
 if P_INT5AMT is null then
 
 new_rec.INT5AMT:=0;
 
 else 
 new_rec.INT5AMT:=P_INT5AMT;
 end if;
 cur_colname:='P_INT6AMT';
 cur_colvalue:=P_INT6AMT;
 if P_INT6AMT is null then
 
 new_rec.INT6AMT:=0;
 
 else 
 new_rec.INT6AMT:=P_INT6AMT;
 end if;
 cur_colname:='P_ESCROWAMT';
 cur_colvalue:=P_ESCROWAMT;
 if P_ESCROWAMT is null then
 
 new_rec.ESCROWAMT:=0;
 
 else 
 new_rec.ESCROWAMT:=P_ESCROWAMT;
 end if;
 cur_colname:='P_PI6MOAMT';
 cur_colvalue:=P_PI6MOAMT;
 if P_PI6MOAMT is null then
 
 new_rec.PI6MOAMT:=0;
 
 else 
 new_rec.PI6MOAMT:=P_PI6MOAMT;
 end if;
 cur_colname:='P_GFDAMT';
 cur_colvalue:=P_GFDAMT;
 if P_GFDAMT is null then
 
 new_rec.GFDAMT:=0;
 
 else 
 new_rec.GFDAMT:=P_GFDAMT;
 end if;
 cur_colname:='P_OPENGNTYAMT';
 cur_colvalue:=P_OPENGNTYAMT;
 if P_OPENGNTYAMT is null then
 
 new_rec.OPENGNTYAMT:=0;
 
 else 
 new_rec.OPENGNTYAMT:=P_OPENGNTYAMT;
 end if;
 cur_colname:='P_OPENGNTYERRAMT';
 cur_colvalue:=P_OPENGNTYERRAMT;
 if P_OPENGNTYERRAMT is null then
 
 new_rec.OPENGNTYERRAMT:=0;
 
 else 
 new_rec.OPENGNTYERRAMT:=P_OPENGNTYERRAMT;
 end if;
 cur_colname:='P_OPENGNTYREGAMT';
 cur_colvalue:=P_OPENGNTYREGAMT;
 if P_OPENGNTYREGAMT is null then
 
 new_rec.OPENGNTYREGAMT:=0;
 
 else 
 new_rec.OPENGNTYREGAMT:=P_OPENGNTYREGAMT;
 end if;
 cur_colname:='P_OPENGNTYUNALLOCAMT';
 cur_colvalue:=P_OPENGNTYUNALLOCAMT;
 if P_OPENGNTYUNALLOCAMT is null then
 
 new_rec.OPENGNTYUNALLOCAMT:=0;
 
 else 
 new_rec.OPENGNTYUNALLOCAMT:=P_OPENGNTYUNALLOCAMT;
 end if;
 cur_colname:='P_UNALLOCAMT';
 cur_colvalue:=P_UNALLOCAMT;
 if P_UNALLOCAMT is null then
 
 new_rec.UNALLOCAMT:=0;
 
 else 
 new_rec.UNALLOCAMT:=P_UNALLOCAMT;
 end if;
 cur_colname:='P_USERLCK';
 cur_colvalue:=P_USERLCK;
 if P_USERLCK is null then
 
 new_rec.USERLCK:=' ';
 
 else 
 new_rec.USERLCK:=P_USERLCK;
 end if;
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 if P_TIMESTAMPFLD is null then
 
 new_rec.TIMESTAMPFLD:=jan_1_1900;
 
 else 
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
 end if;
 cur_colname:='P_CDCSBAFEEDISBAMT';
 cur_colvalue:=P_CDCSBAFEEDISBAMT;
 if P_CDCSBAFEEDISBAMT is null then
 
 new_rec.CDCSBAFEEDISBAMT:=0;
 
 else 
 new_rec.CDCSBAFEEDISBAMT:=P_CDCSBAFEEDISBAMT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
 if p_errval=0 then
 begin
 insert into STGCSA.PRPLOANRQSTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End PRPLOANRQSTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'PRPLOANRQSTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in PRPLOANRQSTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:46:27';
 ROLLBACK TO PRPLOANRQSTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'PRPLOANRQSTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in PRPLOANRQSTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:46:27';
 ROLLBACK TO PRPLOANRQSTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'PRPLOANRQSTINSTSP',force_log_entry=>TRUE);
 --
 END PRPLOANRQSTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


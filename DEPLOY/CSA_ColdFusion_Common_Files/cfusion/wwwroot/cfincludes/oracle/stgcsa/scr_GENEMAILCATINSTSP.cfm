<!--- Saved 10/09/2018 11:29:05. --->
PROCEDURE GENEMAILCATINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_EMAILCATID VARCHAR2:=null
 ,p_DBMDL VARCHAR2:=null
 ,p_SORTORD VARCHAR2:=null
 ,p_CATNM VARCHAR2:=null
 ,p_CATDESC VARCHAR2:=null
 ,p_CATACTV VARCHAR2:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:45:50
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:45:50
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.GENEMAILCATTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.GENEMAILCATTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize EMAILCATID from sequence EMAILCATIDSEQ
 Function NEXTVAL_EMAILCATIDSEQ return number is
 N number;
 begin
 select EMAILCATIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint GENEMAILCATINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init GENEMAILCATINSTSP',p_userid,4,
 logtxt1=>'GENEMAILCATINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'GENEMAILCATINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='DBMDL';
 cur_colvalue:=P_DBMDL;
 l_DBMDL:=P_DBMDL; cur_colname:='TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_EMAILCATID';
 cur_colvalue:=P_EMAILCATID;
 if P_EMAILCATID is null then
 
 new_rec.EMAILCATID:=NEXTVAL_EMAILCATIDSEQ();
 
 else 
 new_rec.EMAILCATID:=P_EMAILCATID;
 end if;
 cur_colname:='P_DBMDL';
 cur_colvalue:=P_DBMDL;
 if P_DBMDL is null then
 
 new_rec.DBMDL:=' ';
 
 else 
 new_rec.DBMDL:=P_DBMDL;
 end if;
 cur_colname:='P_SORTORD';
 cur_colvalue:=P_SORTORD;
 if P_SORTORD is null then
 
 new_rec.SORTORD:=0;
 
 else 
 new_rec.SORTORD:=P_SORTORD;
 end if;
 cur_colname:='P_CATNM';
 cur_colvalue:=P_CATNM;
 if P_CATNM is null then
 
 new_rec.CATNM:=' ';
 
 else 
 new_rec.CATNM:=P_CATNM;
 end if;
 cur_colname:='P_CATDESC';
 cur_colvalue:=P_CATDESC;
 if P_CATDESC is null then
 
 new_rec.CATDESC:=' ';
 
 else 
 new_rec.CATDESC:=P_CATDESC;
 end if;
 cur_colname:='P_CATACTV';
 cur_colvalue:=P_CATACTV;
 if P_CATACTV is null then
 
 new_rec.CATACTV:=0;
 
 else 
 new_rec.CATACTV:=P_CATACTV;
 end if;
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 if P_TIMESTAMPFLD is null then
 
 new_rec.TIMESTAMPFLD:=jan_1_1900;
 
 else 
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(EMAILCATID)=('||new_rec.EMAILCATID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.GENEMAILCATTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End GENEMAILCATINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'GENEMAILCATINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in GENEMAILCATINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:45:50';
 ROLLBACK TO GENEMAILCATINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'GENEMAILCATINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in GENEMAILCATINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:45:50';
 ROLLBACK TO GENEMAILCATINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'GENEMAILCATINSTSP',force_log_entry=>TRUE);
 --
 END GENEMAILCATINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:03. --->
PROCEDURE COMPRPTUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_COMPRPTID VARCHAR2:=null
 ,p_COMPRPTCDCNMB CHAR:=null
 ,p_COMPRPTSOEZAMT VARCHAR2:=null
 ,p_COMPRPTWFCTAMT VARCHAR2:=null
 ,p_COMPRPTR VARCHAR2:=null
 ,p_REPDT VARCHAR2:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:50
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:44:50
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure COMPRPTUPDTSP performs UPDATE on STGCSA.COMPRPTTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: COMPRPTID
 for P_IDENTIFIER=1 qualification is on REPDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.COMPRPTTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.COMPRPTTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.COMPRPTTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_COMPRPTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_COMPRPTID';
 cur_colvalue:=P_COMPRPTID;
 DBrec.COMPRPTID:=P_COMPRPTID; 
 end if;
 if P_COMPRPTCDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_COMPRPTCDCNMB';
 cur_colvalue:=P_COMPRPTCDCNMB;
 DBrec.COMPRPTCDCNMB:=P_COMPRPTCDCNMB; 
 end if;
 if P_COMPRPTSOEZAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_COMPRPTSOEZAMT';
 cur_colvalue:=P_COMPRPTSOEZAMT;
 DBrec.COMPRPTSOEZAMT:=P_COMPRPTSOEZAMT; 
 end if;
 if P_COMPRPTWFCTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_COMPRPTWFCTAMT';
 cur_colvalue:=P_COMPRPTWFCTAMT;
 DBrec.COMPRPTWFCTAMT:=P_COMPRPTWFCTAMT; 
 end if;
 if P_COMPRPTR is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_COMPRPTR';
 cur_colvalue:=P_COMPRPTR;
 DBrec.COMPRPTR:=P_COMPRPTR; 
 end if;
 if P_REPDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_REPDT';
 cur_colvalue:=P_REPDT;
 DBrec.REPDT:=P_REPDT; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.COMPRPTTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,COMPRPTID=DBrec.COMPRPTID
 ,COMPRPTCDCNMB=DBrec.COMPRPTCDCNMB
 ,COMPRPTSOEZAMT=DBrec.COMPRPTSOEZAMT
 ,COMPRPTWFCTAMT=DBrec.COMPRPTWFCTAMT
 ,COMPRPTR=DBrec.COMPRPTR
 ,REPDT=DBrec.REPDT
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.COMPRPTTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint COMPRPTUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init COMPRPTUPDTSP',p_userid,4,
 logtxt1=>'COMPRPTUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'COMPRPTUPDTSP');
 --
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(COMPRPTID)=('||P_COMPRPTID||')';
 begin
 select rowid into rec_rowid from STGCSA.COMPRPTTBL where 1=1
 and COMPRPTID=P_COMPRPTID;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.COMPRPTTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(REPDT)=('||P_REPDT||')';
 begin
 for r1 in (select rowid from STGCSA.COMPRPTTBL a where 1=1 
 and REPDT=P_REPDT
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.COMPRPTTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.COMPRPTTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End COMPRPTUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'COMPRPTUPDTSP');
 else
 ROLLBACK TO COMPRPTUPDTSP;
 p_errmsg:=p_errmsg||' in COMPRPTUPDTSP build 2018-10-08 15:44:50';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'COMPRPTUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO COMPRPTUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in COMPRPTUPDTSP build 2018-10-08 15:44:50';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'COMPRPTUPDTSP',force_log_entry=>true);
 --
 END COMPRPTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


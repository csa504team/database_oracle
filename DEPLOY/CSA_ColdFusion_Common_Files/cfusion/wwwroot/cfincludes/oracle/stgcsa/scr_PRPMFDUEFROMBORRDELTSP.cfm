<!--- Saved 10/09/2018 11:29:08. --->
PROCEDURE PRPMFDUEFROMBORRDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_PRPMFDUEFROMBORRID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:37
 Created by: GENR
 Crerated from template stdtbldel_template v3.21 8 Sep 2018 on 2018-10-08 15:46:37
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure PRPMFDUEFROMBORRDELTSP performs DELETE on STGCSA.PRPMFDUEFROMBORRTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: PRPMFDUEFROMBORRID
 for P_IDENTIFIER=1 qualification is on LOANNMB
 */
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 maxsev number:=0;
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars (0 or more actually used)
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 Begin
 delete STGCSA.PRPMFDUEFROMBORRTBL
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while deleting from STGCSA.PRPMFDUEFROMBORRTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint PRPMFDUEFROMBORRDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPMFDUEFROMBORRDELTSP',p_userid,4,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRDELTSP');
 --
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(PRPMFDUEFROMBORRID)=('||P_PRPMFDUEFROMBORRID||')';
 begin
 select rowid into rec_rowid from STGCSA.PRPMFDUEFROMBORRTBL where 1=1
 and PRPMFDUEFROMBORRID=P_PRPMFDUEFROMBORRID;
 exception when no_data_found then
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFDUEFROMBORRTBL row to delete with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 for r1 in (select rowid from STGCSA.PRPMFDUEFROMBORRTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.PRPMFDUEFROMBORRTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.PRPMFDUEFROMBORRTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:=' In PRPMFDUEFROMBORRDELTSP build 2018-10-08 15:46:37 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End PRPMFDUEFROMBORRDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRDELTSP');
 else
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 p_errmsg:=p_errmsg||' in PRPMFDUEFROMBORRDELTSP build 2018-10-08 15:46:37';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRDELTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRPMFDUEFROMBORRDELTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in PRPMFDUEFROMBORRDELTSP build 2018-10-08 15:46:37';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'PRPMFDUEFROMBORRDELTSP',force_log_entry=>true);
 --
 END PRPMFDUEFROMBORRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


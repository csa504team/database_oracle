<!--- Saved 10/09/2018 11:29:02. --->
PROCEDURE ACCPRCSCYCINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CYCID VARCHAR2:=null
 ,p_CYCYR VARCHAR2:=null
 ,p_CYCMO VARCHAR2:=null
 ,p_CYCMOTXT VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ,p_BUSDAY1DT DATE:=null
 ,p_TIMESTAMPFLD DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:30
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:44:30
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.ACCPRCSCYCTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.ACCPRCSCYCTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize CYCID from sequence CYCIDSEQ
 Function NEXTVAL_CYCIDSEQ return number is
 N number;
 begin
 select CYCIDSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint ACCPRCSCYCINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init ACCPRCSCYCINSTSP',p_userid,4,
 logtxt1=>'ACCPRCSCYCINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'ACCPRCSCYCINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 l_TIMESTAMPFLD:=P_TIMESTAMPFLD; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CYCID';
 cur_colvalue:=P_CYCID;
 if P_CYCID is null then
 
 new_rec.CYCID:=NEXTVAL_CYCIDSEQ();
 
 else 
 new_rec.CYCID:=P_CYCID;
 end if;
 cur_colname:='P_CYCYR';
 cur_colvalue:=P_CYCYR;
 if P_CYCYR is null then
 
 new_rec.CYCYR:=0;
 
 else 
 new_rec.CYCYR:=P_CYCYR;
 end if;
 cur_colname:='P_CYCMO';
 cur_colvalue:=P_CYCMO;
 if P_CYCMO is null then
 
 new_rec.CYCMO:=0;
 
 else 
 new_rec.CYCMO:=P_CYCMO;
 end if;
 cur_colname:='P_CYCMOTXT';
 cur_colvalue:=P_CYCMOTXT;
 if P_CYCMOTXT is null then
 
 new_rec.CYCMOTXT:=' ';
 
 else 
 new_rec.CYCMOTXT:=P_CYCMOTXT;
 end if;
 cur_colname:='P_SEMIANDT';
 cur_colvalue:=P_SEMIANDT;
 if P_SEMIANDT is null then
 
 new_rec.SEMIANDT:=jan_1_1900;
 
 else 
 new_rec.SEMIANDT:=P_SEMIANDT;
 end if;
 cur_colname:='P_BUSDAY1DT';
 cur_colvalue:=P_BUSDAY1DT;
 if P_BUSDAY1DT is null then
 
 new_rec.BUSDAY1DT:=jan_1_1900;
 
 else 
 new_rec.BUSDAY1DT:=P_BUSDAY1DT;
 end if;
 cur_colname:='P_TIMESTAMPFLD';
 cur_colvalue:=P_TIMESTAMPFLD;
 if P_TIMESTAMPFLD is null then
 
 new_rec.TIMESTAMPFLD:=jan_1_1900;
 
 else 
 new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CYCID)=('||new_rec.CYCID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.ACCPRCSCYCTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End ACCPRCSCYCINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'ACCPRCSCYCINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in ACCPRCSCYCINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:44:30';
 ROLLBACK TO ACCPRCSCYCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'ACCPRCSCYCINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in ACCPRCSCYCINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:44:30';
 ROLLBACK TO ACCPRCSCYCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'ACCPRCSCYCINSTSP',force_log_entry=>TRUE);
 --
 END ACCPRCSCYCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


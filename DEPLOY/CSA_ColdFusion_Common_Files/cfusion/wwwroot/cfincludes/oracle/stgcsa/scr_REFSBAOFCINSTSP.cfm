<!--- Saved 10/09/2018 11:29:09. --->
PROCEDURE REFSBAOFCINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_SBAOFCCD CHAR:=null
 ,p_SBAOFCNM VARCHAR2:=null
 ,p_SBAOFCADRSTR1NM VARCHAR2:=null
 ,p_SBAOFCADRSTR2NM VARCHAR2:=null
 ,p_SBAOFCADRCTYNM VARCHAR2:=null
 ,p_SBAOFCADRSTCD CHAR:=null
 ,p_SBAOFCADRZIPCD CHAR:=null
 ,p_SBAOFCADRZIP4CD CHAR:=null
 ,p_SBAOFCPHNNMB CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:46:59
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:46:59
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.REFSBAOFCTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.REFSBAOFCTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- vars for checking non-defaultable columns
 onespace varchar2(10):=' '; 
 ndfltmsg varchar2(300);
 v_errfnd boolean:=false;
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Main body begins here
 begin
 -- setup
 savepoint REFSBAOFCINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init REFSBAOFCINSTSP',p_userid,4,
 logtxt1=>'REFSBAOFCINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'REFSBAOFCINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_SBAOFCCD';
 cur_colvalue:=P_SBAOFCCD;
 if P_SBAOFCCD is null then
 raise_application_error(-20001,cur_colname||' may not be null');
 else 
 new_rec.SBAOFCCD:=P_SBAOFCCD;
 end if;
 cur_colname:='P_SBAOFCNM';
 cur_colvalue:=P_SBAOFCNM;
 if P_SBAOFCNM is null then
 
 new_rec.SBAOFCNM:=' ';
 
 else 
 new_rec.SBAOFCNM:=P_SBAOFCNM;
 end if;
 cur_colname:='P_SBAOFCADRSTR1NM';
 cur_colvalue:=P_SBAOFCADRSTR1NM;
 if P_SBAOFCADRSTR1NM is null then
 
 new_rec.SBAOFCADRSTR1NM:=' ';
 
 else 
 new_rec.SBAOFCADRSTR1NM:=P_SBAOFCADRSTR1NM;
 end if;
 cur_colname:='P_SBAOFCADRSTR2NM';
 cur_colvalue:=P_SBAOFCADRSTR2NM;
 if P_SBAOFCADRSTR2NM is null then
 
 new_rec.SBAOFCADRSTR2NM:=' ';
 
 else 
 new_rec.SBAOFCADRSTR2NM:=P_SBAOFCADRSTR2NM;
 end if;
 cur_colname:='P_SBAOFCADRCTYNM';
 cur_colvalue:=P_SBAOFCADRCTYNM;
 if P_SBAOFCADRCTYNM is null then
 
 new_rec.SBAOFCADRCTYNM:=' ';
 
 else 
 new_rec.SBAOFCADRCTYNM:=P_SBAOFCADRCTYNM;
 end if;
 cur_colname:='P_SBAOFCADRSTCD';
 cur_colvalue:=P_SBAOFCADRSTCD;
 if P_SBAOFCADRSTCD is null then
 
 new_rec.SBAOFCADRSTCD:=rpad(' ',4);
 
 else 
 new_rec.SBAOFCADRSTCD:=P_SBAOFCADRSTCD;
 end if;
 cur_colname:='P_SBAOFCADRZIPCD';
 cur_colvalue:=P_SBAOFCADRZIPCD;
 if P_SBAOFCADRZIPCD is null then
 
 new_rec.SBAOFCADRZIPCD:=rpad(' ',4);
 
 else 
 new_rec.SBAOFCADRZIPCD:=P_SBAOFCADRZIPCD;
 end if;
 cur_colname:='P_SBAOFCADRZIP4CD';
 cur_colvalue:=P_SBAOFCADRZIP4CD;
 if P_SBAOFCADRZIP4CD is null then
 
 new_rec.SBAOFCADRZIP4CD:=rpad(' ',4);
 
 else 
 new_rec.SBAOFCADRZIP4CD:=P_SBAOFCADRZIP4CD;
 end if;
 cur_colname:='P_SBAOFCPHNNMB';
 cur_colvalue:=P_SBAOFCPHNNMB;
 if P_SBAOFCPHNNMB is null then
 
 new_rec.SBAOFCPHNNMB:=rpad(' ',10);
 
 else 
 new_rec.SBAOFCPHNNMB:=P_SBAOFCPHNNMB;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(SBAOFCCD)=('||new_rec.SBAOFCCD||')';
 if p_errval=0 then
 begin
 insert into STGCSA.REFSBAOFCTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End REFSBAOFCINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'REFSBAOFCINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in REFSBAOFCINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:46:59';
 ROLLBACK TO REFSBAOFCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'REFSBAOFCINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in REFSBAOFCINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:46:59';
 ROLLBACK TO REFSBAOFCINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'REFSBAOFCINSTSP',force_log_entry=>TRUE);
 --
 END REFSBAOFCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:12. --->
PROCEDURE SOFVDUEBUPDTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_LOANNMB CHAR:=null
 ,p_DUEFROMBORRRECRDTYP VARCHAR2:=null
 ,p_DUEFROMBORRPOSTINGDT DATE:=null
 ,p_DUEFROMBORRDOLLRAMT VARCHAR2:=null
 ,p_DUEFROMBORRRJCTDT DATE:=null
 ,p_DUEFROMBORRRJCTCD CHAR:=null
 ,p_DUEFROMBORRRECRDSTATCD VARCHAR2:=null
 ,p_DUEFROMBORRCLSDT DATE:=null
 ,p_DUEFROMBORRCMNT1 VARCHAR2:=null
 ,p_DUEFROMBORRCMNT2 VARCHAR2:=null
 ,p_DUEFROMBORRRPTDAILY CHAR:=null
 ,p_DUEFROMBORRDISBIND CHAR:=null
 ,p_DUEFROMBORRDELACH CHAR:=null
 ) as
 /*
 Created on: 2018-10-08 15:47:28
 Created by: GENR
 Crerated from template stdtblupd_template v3.21 8 Sep 2018 on 2018-10-08 15:47:28
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure SOFVDUEBUPDTSP performs UPDATE on STGCSA.SOFVDUEBTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, DUEFROMBORRRECRDTYP, DUEFROMBORRPOSTINGDT
 for P_IDENTIFIER=1 qualification is on DUEFROMBORRDOLLRAMT, DUEFROMBORRRECRDTYP, DUEFROMBORRRECRDSTATCD<>, LOANNMB, DUEFROMBORRPOSTINGDT
 Updates all columns except the qualification keys and columns for which a
 null value is passed.
 SPECIAL secret update kludge!
 if p_identifier is set to -1 then a flag is set to use all input parm values
 and p_identifier is changed to 0 (update by PK).
 THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
 This option allows setting colulmns to null!
 */
 dbrec STGCSA.SOFVDUEBTBL%rowtype;
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 maxsev number:=0;
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 crossover_not_active char(1);
 procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 begin
 select * into dbrec from STGCSA.SOFVDUEBTBL where rowid=rowid_in;
 exception when others then
 p_errval:=sqlcode;
 maxsev:=3;
 p_retval:=0;
 p_errmsg:='Fetch of STGCSA.SOFVDUEBTBL row by rowid '||rowid_in
 ||' orinally fetched using '||orig_keyset_values
 ||' failed with '||sqlerrm(sqlcode);
 end;
 end if;
 if p_errval=0 then
 -- set this as lastupdt
 DBrec.lastupdtuserid:=p_userid;
 DBrec.lastupdtdt:=sysdate;
 -- copy into DBrec all table column input parameters that are not null,
 -- saving column name/value if needed for error reporting
 -- (if p_identifier=-1 then move input even if null)
 begin
 if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 DBrec.LOANNMB:=P_LOANNMB; 
 end if;
 if P_DUEFROMBORRRECRDTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRRECRDTYP';
 cur_colvalue:=P_DUEFROMBORRRECRDTYP;
 DBrec.DUEFROMBORRRECRDTYP:=P_DUEFROMBORRRECRDTYP; 
 end if;
 if P_DUEFROMBORRPOSTINGDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRPOSTINGDT';
 cur_colvalue:=P_DUEFROMBORRPOSTINGDT;
 DBrec.DUEFROMBORRPOSTINGDT:=P_DUEFROMBORRPOSTINGDT; 
 end if;
 if P_DUEFROMBORRDOLLRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRDOLLRAMT';
 cur_colvalue:=P_DUEFROMBORRDOLLRAMT;
 DBrec.DUEFROMBORRDOLLRAMT:=P_DUEFROMBORRDOLLRAMT; 
 end if;
 if P_DUEFROMBORRRJCTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRRJCTDT';
 cur_colvalue:=P_DUEFROMBORRRJCTDT;
 DBrec.DUEFROMBORRRJCTDT:=P_DUEFROMBORRRJCTDT; 
 end if;
 if P_DUEFROMBORRRJCTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRRJCTCD';
 cur_colvalue:=P_DUEFROMBORRRJCTCD;
 DBrec.DUEFROMBORRRJCTCD:=P_DUEFROMBORRRJCTCD; 
 end if;
 if P_DUEFROMBORRRECRDSTATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRRECRDSTATCD';
 cur_colvalue:=P_DUEFROMBORRRECRDSTATCD;
 DBrec.DUEFROMBORRRECRDSTATCD:=P_DUEFROMBORRRECRDSTATCD; 
 end if;
 if P_DUEFROMBORRCLSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRCLSDT';
 cur_colvalue:=P_DUEFROMBORRCLSDT;
 DBrec.DUEFROMBORRCLSDT:=P_DUEFROMBORRCLSDT; 
 end if;
 if P_DUEFROMBORRCMNT1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRCMNT1';
 cur_colvalue:=P_DUEFROMBORRCMNT1;
 DBrec.DUEFROMBORRCMNT1:=P_DUEFROMBORRCMNT1; 
 end if;
 if P_DUEFROMBORRCMNT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRCMNT2';
 cur_colvalue:=P_DUEFROMBORRCMNT2;
 DBrec.DUEFROMBORRCMNT2:=P_DUEFROMBORRCMNT2; 
 end if;
 if P_DUEFROMBORRRPTDAILY is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRRPTDAILY';
 cur_colvalue:=P_DUEFROMBORRRPTDAILY;
 DBrec.DUEFROMBORRRPTDAILY:=P_DUEFROMBORRRPTDAILY; 
 end if;
 if P_DUEFROMBORRDISBIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRDISBIND';
 cur_colvalue:=P_DUEFROMBORRDISBIND;
 DBrec.DUEFROMBORRDISBIND:=P_DUEFROMBORRDISBIND; 
 end if;
 if P_DUEFROMBORRDELACH is not null or DO_NORMAL_FIELD_CHECKING='N' then
 cur_colname:='P_DUEFROMBORRDELACH';
 cur_colvalue:=P_DUEFROMBORRDELACH;
 DBrec.DUEFROMBORRDELACH:=P_DUEFROMBORRDELACH; 
 end if;
 exception when others then
 p_errval:=sqlcode;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 end if;
 -- if all still ok do actual update
 if p_errval=0 then
 Begin
 update STGCSA.SOFVDUEBTBL
 set lastupdtuserid=DBrec.lastupdtuserid,
 lastupdtdt=DBrec.lastupdtdt
 ,LOANNMB=DBrec.LOANNMB
 ,DUEFROMBORRRECRDTYP=DBrec.DUEFROMBORRRECRDTYP
 ,DUEFROMBORRPOSTINGDT=DBrec.DUEFROMBORRPOSTINGDT
 ,DUEFROMBORRDOLLRAMT=DBrec.DUEFROMBORRDOLLRAMT
 ,DUEFROMBORRRJCTDT=DBrec.DUEFROMBORRRJCTDT
 ,DUEFROMBORRRJCTCD=DBrec.DUEFROMBORRRJCTCD
 ,DUEFROMBORRRECRDSTATCD=DBrec.DUEFROMBORRRECRDSTATCD
 ,DUEFROMBORRCLSDT=DBrec.DUEFROMBORRCLSDT
 ,DUEFROMBORRCMNT1=DBrec.DUEFROMBORRCMNT1
 ,DUEFROMBORRCMNT2=DBrec.DUEFROMBORRCMNT2
 ,DUEFROMBORRRPTDAILY=DBrec.DUEFROMBORRRPTDAILY
 ,DUEFROMBORRDISBIND=DBrec.DUEFROMBORRDISBIND
 ,DUEFROMBORRDELACH=DBrec.DUEFROMBORRDELACH
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while doing update on STGCSA.SOFVDUEBTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint SOFVDUEBUPDTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVDUEBUPDTSP',p_userid,4,
 logtxt1=>'SOFVDUEBUPDTSP',logtxt2=>'stdtblupd_template v3.21 8 Sep 2018',
 PROGRAM_NAME=>'SOFVDUEBUPDTSP');
 --
 l_LOANNMB:=P_LOANNMB;
 --
 -- Check to see if crossover is ongoing, in which case updates are prohibited
 SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
 IF crossover_not_active != 'Y' THEN
 p_errval := -20099;
 p_errmsg := 'Warning: Cannot update table SOFVDUEBTBL at this time because crossover is running, please try later.';
 END IF;
 -- end of crossover_lockout_check
 --
 -- special p_identifier kludge
 -- if p_identifier is set to -1 then it treated as 0 (update by PK)
 -- but a flag is set use passed value EVEN IF NULL. Since for option -1
 -- every column is updated to what was passed, intended values MUST be passed
 -- for all columns. This option allows setting colulmns to null!
 overridden_p_identifier:=p_identifier;
 if p_identifier=-1 then
 do_normal_field_checking:='N';
 overridden_p_identifier:=0;
 end if;
 case overridden_p_identifier
 when 0 then
 -- case to update one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')'||' and '||'(DUEFROMBORRRECRDTYP)=('||P_DUEFROMBORRRECRDTYP||')'||' and '||'(DUEFROMBORRPOSTINGDT)=('||P_DUEFROMBORRPOSTINGDT||')';
 begin
 select rowid into rec_rowid from STGCSA.SOFVDUEBTBL where 1=1
 and LOANNMB=P_LOANNMB and DUEFROMBORRRECRDTYP=P_DUEFROMBORRRECRDTYP and DUEFROMBORRPOSTINGDT=P_DUEFROMBORRPOSTINGDT;
 exception when no_data_found then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.SOFVDUEBTBL row to update with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(DUEFROMBORRDOLLRAMT)=('||P_DUEFROMBORRDOLLRAMT||')'||' and '||'(DUEFROMBORRRECRDTYP)=('||P_DUEFROMBORRRECRDTYP||')'||' and '||'(DUEFROMBORRPOSTINGDT)=('||P_DUEFROMBORRPOSTINGDT||')'||' and '||'(LOANNMB)=('||P_LOANNMB||')'||' and '||'(DUEFROMBORRRECRDSTATCD)$OP:<>';
 begin
 for r1 in (select rowid from STGCSA.SOFVDUEBTBL a where 1=1 
 and DUEFROMBORRDOLLRAMT=P_DUEFROMBORRDOLLRAMT
 and DUEFROMBORRPOSTINGDT=P_DUEFROMBORRPOSTINGDT
 
 and DUEFROMBORRRECRDSTATCD<>P_DUEFROMBORRRECRDSTATCD
 and DUEFROMBORRRECRDTYP=P_DUEFROMBORRRECRDTYP
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_update_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.SOFVDUEBTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.SOFVDUEBTBL row(s) to update with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 -- post to log
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End SOFVDUEBUPDTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
 ,PROGRAM_NAME=>'SOFVDUEBUPDTSP');
 else
 ROLLBACK TO SOFVDUEBUPDTSP;
 p_errmsg:=p_errmsg||' in SOFVDUEBUPDTSP build 2018-10-08 15:47:28';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'SOFVDUEBUPDTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO SOFVDUEBUPDTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in SOFVDUEBUPDTSP build 2018-10-08 15:47:28';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'SOFVDUEBUPDTSP',force_log_entry=>true);
 --
 END SOFVDUEBUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


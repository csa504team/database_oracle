<!--- Saved 10/09/2018 11:29:01. --->
PROCEDURE ACCACCELERATELOANDELTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_ACCELERATNID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_TRANSID VARCHAR2:=null
 ,p_STATID VARCHAR2:=null
 ,p_SEMIANDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:44:20
 Created by: GENR
 Crerated from template stdtbldel_template v3.21 8 Sep 2018 on 2018-10-08 15:44:21
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure ACCACCELERATELOANDELTSP performs DELETE on STGCSA.ACCACCELERATELOANTBL
 for P_IDENTIFIER=0 qualification is on Primary Key: ACCELERATNID
 for P_IDENTIFIER=1 qualification is on SEMIANDT, STATID
 for P_IDENTIFIER=2 qualification is on SEMIANDT, STATID =49
 for P_IDENTIFIER=3 qualification is on SEMIANDT, STATID =50
 for P_IDENTIFIER=4 qualification is on TRANSID
 for P_IDENTIFIER=5 qualification is on LOANNMB
 */
 logged_msg_id number;
 recnum number;
 rec_rowid rowid;
 keystouse varchar2(256);
 pctsign char(1):='%';
 cur_colname varchar2(100):=null;
 cur_colvalue varchar2(4000);
 maxsev number:=0;
 do_normal_field_checking char(1):='Y';
 overridden_p_identifier number;
 -- standard activity log capture vars (0 or more actually used)
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 procedure fetch_and_delete_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
 begin
 if p_errval=0 then
 Begin
 delete STGCSA.ACCACCELERATELOANTBL
 where rowid=rowid_in;
 recnum:=recnum+1;
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:= 'Error while deleting from STGCSA.ACCACCELERATELOANTBL, on rec with rowid '
 ||rowid_in||' previously fetched using key: '||orig_keyset_values
 ||' got Oracle error '||sqlerrm(sqlcode);
 end;
 end if;
 END;
 begin
 -- setup
 savepoint ACCACCELERATELOANDELTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 recnum:=0;
 runtime.logger(logged_msg_id,'STDLOG',100,'Init ACCACCELERATELOANDELTSP',p_userid,4,
 PROGRAM_NAME=>'ACCACCELERATELOANDELTSP');
 --
 case p_identifier
 when 0 then
 -- case to delete one row based on primary key
 -- p_identifier=0 (choice 0) is default action for a stored procedure
 -- get existing version of row
 if p_errval=0 then
 keystouse:='(ACCELERATNID)=('||P_ACCELERATNID||')';
 begin
 select rowid into rec_rowid from STGCSA.ACCACCELERATELOANTBL where 1=1
 and ACCELERATNID=P_ACCELERATNID;
 exception when no_data_found then
 p_errval:=-100;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row to delete with key(s) '
 ||keystouse;
 end;
 end if;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if;
 when 1 then 
 -- case to update one or more rows based on keyset KEYS1
 if p_errval=0 then
 keystouse:='(STATID)=('||P_STATID||')'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
 begin
 for r1 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 and STATID=P_STATID
 ) 
 loop
 rec_rowid:=r1.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 2 then 
 -- case to update one or more rows based on keyset KEYS2
 if p_errval=0 then
 keystouse:='(STATID)=49'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
 begin
 for r2 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =49
 ) 
 loop
 rec_rowid:=r2.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 3 then 
 -- case to update one or more rows based on keyset KEYS3
 if p_errval=0 then
 keystouse:='(STATID)=50'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
 begin
 for r3 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and SEMIANDT=P_SEMIANDT
 
 and STATID =50
 ) 
 loop
 rec_rowid:=r3.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 4 then 
 -- case to update one or more rows based on keyset KEYS4
 if p_errval=0 then
 keystouse:='(TRANSID)=('||P_TRANSID||')';
 begin
 for r4 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and TRANSID=P_TRANSID
 ) 
 loop
 rec_rowid:=r4.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 when 5 then 
 -- case to update one or more rows based on keyset KEYS5
 if p_errval=0 then
 keystouse:='(LOANNMB)=('||P_LOANNMB||')';
 begin
 for r5 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1 
 and LOANNMB=P_LOANNMB
 ) 
 loop
 rec_rowid:=r5.rowid;
 if p_errval=0 then
 fetch_and_delete_by_rowid(rec_rowid, keystouse);
 end if; 
 end loop; 
 exception when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
 ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
 end;
 if recnum=0 then 
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Error No STGCSA.ACCACCELERATELOANTBL row(s) to delete with key(s) '
 ||keystouse;
 end if; 
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:=' In ACCACCELERATELOANDELTSP build 2018-10-08 15:44:20 '
 ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
 end case;
 P_RETVAL:=RECNUM;
 if p_errval=0 then
 runtime.logger(logged_msg_id,'STDLOG',101,
 'End ACCACCELERATELOANDELTSP With 0 return, P_RETVAL='
 ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ACCACCELERATELOANDELTSP');
 else
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errmsg:=p_errmsg||' in ACCACCELERATELOANDELTSP build 2018-10-08 15:44:20';
 runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
 maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ACCACCELERATELOANDELTSP',force_log_entry=>true);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO ACCACCELERATELOANDELTSP;
 p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
 ||' in ACCACCELERATELOANDELTSP build 2018-10-08 15:44:20';
 runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
 3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
 PROGRAM_NAME=>'ACCACCELERATELOANDELTSP',force_log_entry=>true);
 --
 END ACCACCELERATELOANDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 10/09/2018 11:29:15. --->
PROCEDURE TEMPCDAMORTINSTSP(
 p_retval out number,
 p_errval out number,
 p_errmsg out varchar2,
 p_identifier number:=0,
 p_userid varchar2
 ,p_CDAMORTID VARCHAR2:=null
 ,p_LOANNMB CHAR:=null
 ,p_PD DATE:=null
 ,p_PD_BEGIN_PRIN VARCHAR2:=null
 ,p_PYMT_AMT VARCHAR2:=null
 ,p_PRIN_AMT VARCHAR2:=null
 ,p_INT_AMT VARCHAR2:=null
 ,p_CDC_AMT VARCHAR2:=null
 ,p_SBA_AMT VARCHAR2:=null
 ,p_CSA_AMT VARCHAR2:=null
 ,p_LATE_AMT VARCHAR2:=null
 ,p_PRIN_BALANCE VARCHAR2:=null
 ,p_PRIN_NEEDED VARCHAR2:=null
 ,p_INT_NEEDED VARCHAR2:=null
 ,p_CDC_FEE_NEEDED VARCHAR2:=null
 ,p_SBA_FEE_NEEDED VARCHAR2:=null
 ,p_CSA_FEE_NEEDED VARCHAR2:=null
 ,p_LATE_FEE_NEEDED VARCHAR2:=null
 ,p_UNALLOCATED_AMT VARCHAR2:=null
 ,p_PRIN_PYMT VARCHAR2:=null
 ,p_INT_PYMT VARCHAR2:=null
 ,p_TOTAL_PRIN VARCHAR2:=null
 ,p_STATUS VARCHAR2:=null
 ,p_TYPENM VARCHAR2:=null
 ,p_FIRSTPAYMENTDT DATE:=null
 ,p_LASTPAYMENTDT DATE:=null
 ) as
 /*
 Created on: 2018-10-08 15:48:07
 Created by: GENR
 Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-10-08 15:48:07
 Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
 Procedure performs INSERT on STGCSA.TEMPCDAMORTTBLfor P_IDENTIFIER=0
 */
 rundate date;
 gn global_name.global_name%type;
 new_rec STGCSA.TEMPCDAMORTTBL%rowtype;
 logged_msgid_retval number;
 audretval number;
 crossover_active char(1);
 l_LOANNMB char(10):=null;
 l_TRANSID number:=null;
 l_dbmdl varchar2(10):=null;
 l_entrydt date:=null;
 l_userid varchar2(32):=null;
 l_usernm varchar2(199):=null;
 l_timestampfld date:=null;
 cur_col_name varchar2(100):=null;
 maxsev number:=0;
 holdstring varchar2(4000);
 keystouse varchar2(1000);
 -- std date for column defaulting
 JAN_1_1900 constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
 cur_colvalue varchar2(4095);
 cur_colname varchar2(30);
 -- Function used to initialize CDAMORTID from sequence TEMPCDAMORTSEQ
 Function NEXTVAL_TEMPCDAMORTSEQ return number is
 N number;
 begin
 select TEMPCDAMORTSEQ.nextval into n from dual;
 return n;
 end;
 -- Main body begins here
 begin
 -- setup
 savepoint TEMPCDAMORTINSTSP;
 p_retval:=0;
 p_errval:=0;
 p_errmsg:=null;
 runtime.logger(logged_msgid_retval,'STDLOG',100,'Init TEMPCDAMORTINSTSP',p_userid,4,
 logtxt1=>'TEMPCDAMORTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
 PROGRAM_NAME=>'TEMPCDAMORTINSTSP');
 select global_name, sysdate into gn, rundate from global_name;
 begin 
 cur_colname:='LOANNMB';
 cur_colvalue:=P_LOANNMB;
 l_LOANNMB:=P_LOANNMB; 
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
 end;
 new_rec.creatuserid:=p_userid;
 new_rec.creatdt:=sysdate;
 new_rec.lastupdtuserid:=p_userid;
 new_rec.lastupdtdt:=sysdate;
 if p_identifier=0 then
 -- p_identifier=0 this is default for a stored procedure, only action for insert
 runtime.errfound:=false;
 runtime.errstr:=' ';
 --
 -- Default columns as applicable
 if p_errval=0 then 
 begin
 cur_colname:='P_CDAMORTID';
 cur_colvalue:=P_CDAMORTID;
 if P_CDAMORTID is null then
 
 new_rec.CDAMORTID:=NEXTVAL_TEMPCDAMORTSEQ();
 
 else 
 new_rec.CDAMORTID:=P_CDAMORTID;
 end if;
 cur_colname:='P_LOANNMB';
 cur_colvalue:=P_LOANNMB;
 if P_LOANNMB is null then
 
 new_rec.LOANNMB:=rpad(' ',10);
 
 else 
 new_rec.LOANNMB:=P_LOANNMB;
 end if;
 cur_colname:='P_PD';
 cur_colvalue:=P_PD;
 if P_PD is null then
 
 new_rec.PD:=jan_1_1900;
 
 else 
 new_rec.PD:=P_PD;
 end if;
 cur_colname:='P_PD_BEGIN_PRIN';
 cur_colvalue:=P_PD_BEGIN_PRIN;
 if P_PD_BEGIN_PRIN is null then
 
 new_rec.PD_BEGIN_PRIN:=0;
 
 else 
 new_rec.PD_BEGIN_PRIN:=P_PD_BEGIN_PRIN;
 end if;
 cur_colname:='P_PYMT_AMT';
 cur_colvalue:=P_PYMT_AMT;
 if P_PYMT_AMT is null then
 
 new_rec.PYMT_AMT:=0;
 
 else 
 new_rec.PYMT_AMT:=P_PYMT_AMT;
 end if;
 cur_colname:='P_PRIN_AMT';
 cur_colvalue:=P_PRIN_AMT;
 if P_PRIN_AMT is null then
 
 new_rec.PRIN_AMT:=0;
 
 else 
 new_rec.PRIN_AMT:=P_PRIN_AMT;
 end if;
 cur_colname:='P_INT_AMT';
 cur_colvalue:=P_INT_AMT;
 if P_INT_AMT is null then
 
 new_rec.INT_AMT:=0;
 
 else 
 new_rec.INT_AMT:=P_INT_AMT;
 end if;
 cur_colname:='P_CDC_AMT';
 cur_colvalue:=P_CDC_AMT;
 if P_CDC_AMT is null then
 
 new_rec.CDC_AMT:=0;
 
 else 
 new_rec.CDC_AMT:=P_CDC_AMT;
 end if;
 cur_colname:='P_SBA_AMT';
 cur_colvalue:=P_SBA_AMT;
 if P_SBA_AMT is null then
 
 new_rec.SBA_AMT:=0;
 
 else 
 new_rec.SBA_AMT:=P_SBA_AMT;
 end if;
 cur_colname:='P_CSA_AMT';
 cur_colvalue:=P_CSA_AMT;
 if P_CSA_AMT is null then
 
 new_rec.CSA_AMT:=0;
 
 else 
 new_rec.CSA_AMT:=P_CSA_AMT;
 end if;
 cur_colname:='P_LATE_AMT';
 cur_colvalue:=P_LATE_AMT;
 if P_LATE_AMT is null then
 
 new_rec.LATE_AMT:=0;
 
 else 
 new_rec.LATE_AMT:=P_LATE_AMT;
 end if;
 cur_colname:='P_PRIN_BALANCE';
 cur_colvalue:=P_PRIN_BALANCE;
 if P_PRIN_BALANCE is null then
 
 new_rec.PRIN_BALANCE:=0;
 
 else 
 new_rec.PRIN_BALANCE:=P_PRIN_BALANCE;
 end if;
 cur_colname:='P_PRIN_NEEDED';
 cur_colvalue:=P_PRIN_NEEDED;
 if P_PRIN_NEEDED is null then
 
 new_rec.PRIN_NEEDED:=0;
 
 else 
 new_rec.PRIN_NEEDED:=P_PRIN_NEEDED;
 end if;
 cur_colname:='P_INT_NEEDED';
 cur_colvalue:=P_INT_NEEDED;
 if P_INT_NEEDED is null then
 
 new_rec.INT_NEEDED:=0;
 
 else 
 new_rec.INT_NEEDED:=P_INT_NEEDED;
 end if;
 cur_colname:='P_CDC_FEE_NEEDED';
 cur_colvalue:=P_CDC_FEE_NEEDED;
 if P_CDC_FEE_NEEDED is null then
 
 new_rec.CDC_FEE_NEEDED:=0;
 
 else 
 new_rec.CDC_FEE_NEEDED:=P_CDC_FEE_NEEDED;
 end if;
 cur_colname:='P_SBA_FEE_NEEDED';
 cur_colvalue:=P_SBA_FEE_NEEDED;
 if P_SBA_FEE_NEEDED is null then
 
 new_rec.SBA_FEE_NEEDED:=0;
 
 else 
 new_rec.SBA_FEE_NEEDED:=P_SBA_FEE_NEEDED;
 end if;
 cur_colname:='P_CSA_FEE_NEEDED';
 cur_colvalue:=P_CSA_FEE_NEEDED;
 if P_CSA_FEE_NEEDED is null then
 
 new_rec.CSA_FEE_NEEDED:=0;
 
 else 
 new_rec.CSA_FEE_NEEDED:=P_CSA_FEE_NEEDED;
 end if;
 cur_colname:='P_LATE_FEE_NEEDED';
 cur_colvalue:=P_LATE_FEE_NEEDED;
 if P_LATE_FEE_NEEDED is null then
 
 new_rec.LATE_FEE_NEEDED:=0;
 
 else 
 new_rec.LATE_FEE_NEEDED:=P_LATE_FEE_NEEDED;
 end if;
 cur_colname:='P_UNALLOCATED_AMT';
 cur_colvalue:=P_UNALLOCATED_AMT;
 if P_UNALLOCATED_AMT is null then
 
 new_rec.UNALLOCATED_AMT:=0;
 
 else 
 new_rec.UNALLOCATED_AMT:=P_UNALLOCATED_AMT;
 end if;
 cur_colname:='P_PRIN_PYMT';
 cur_colvalue:=P_PRIN_PYMT;
 if P_PRIN_PYMT is null then
 
 new_rec.PRIN_PYMT:=0;
 
 else 
 new_rec.PRIN_PYMT:=P_PRIN_PYMT;
 end if;
 cur_colname:='P_INT_PYMT';
 cur_colvalue:=P_INT_PYMT;
 if P_INT_PYMT is null then
 
 new_rec.INT_PYMT:=0;
 
 else 
 new_rec.INT_PYMT:=P_INT_PYMT;
 end if;
 cur_colname:='P_TOTAL_PRIN';
 cur_colvalue:=P_TOTAL_PRIN;
 if P_TOTAL_PRIN is null then
 
 new_rec.TOTAL_PRIN:=0;
 
 else 
 new_rec.TOTAL_PRIN:=P_TOTAL_PRIN;
 end if;
 cur_colname:='P_STATUS';
 cur_colvalue:=P_STATUS;
 if P_STATUS is null then
 
 new_rec.STATUS:=' ';
 
 else 
 new_rec.STATUS:=P_STATUS;
 end if;
 cur_colname:='P_TYPENM';
 cur_colvalue:=P_TYPENM;
 if P_TYPENM is null then
 
 new_rec.TYPENM:=' ';
 
 else 
 new_rec.TYPENM:=P_TYPENM;
 end if;
 cur_colname:='P_FIRSTPAYMENTDT';
 cur_colvalue:=P_FIRSTPAYMENTDT;
 if P_FIRSTPAYMENTDT is null then
 
 new_rec.FIRSTPAYMENTDT:=jan_1_1900;
 
 else 
 new_rec.FIRSTPAYMENTDT:=P_FIRSTPAYMENTDT;
 end if;
 cur_colname:='P_LASTPAYMENTDT';
 cur_colvalue:=P_LASTPAYMENTDT;
 if P_LASTPAYMENTDT is null then
 
 new_rec.LASTPAYMENTDT:=jan_1_1900;
 
 else 
 new_rec.LASTPAYMENTDT:=P_LASTPAYMENTDT;
 end if;
 exception when others then
 p_errval:=sqlcode;
 p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
 ||cur_colname||' Value >'||cur_colvalue||'<'; 
 end; 
 end if;
 keystouse:='(CDAMORTID)=('||new_rec.CDAMORTID||')';
 if p_errval=0 then
 begin
 insert into STGCSA.TEMPCDAMORTTBL values new_rec;
 p_RETVAL := SQL%ROWCOUNT;
 exception
 when dup_val_on_index then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<2 then maxsev:=2; end if;
 p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
 when others then
 p_errval:=sqlcode;
 p_retval:=0;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
 end;
 end if;
 else /* P_IDENTIFIER was unexpected value */
 p_errval:=-20002;
 if maxsev<3 then maxsev:=3; end if;
 p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
 ||' inserting record with key '||keystouse;
 end if;
 if p_errval=0 then
 runtime.logger(logged_msgid_retval,'STDLOG',101,
 'End TEMPCDAMORTINSTSP With 0 return, P_RETVAL='
 ||p_retval||' Record Key '||keystouse,p_userid,
 1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
 l_timestampfld,PROGRAM_NAME=>'TEMPCDAMORTINSTSP');
 else
 p_errmsg:='Error: '||P_ERRMSG||' in TEMPCDAMORTINSTSP Record Key '||keystouse
 ||' Build: 2018-10-08 15:48:07';
 ROLLBACK TO TEMPCDAMORTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',103,
 p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
 l_entrydt, l_timestampfld,PROGRAM_NAME=>'TEMPCDAMORTINSTSP',force_log_entry=>TRUE);
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 p_errval:=SQLCODE;
 p_errmsg:=' In outer exception handler in TEMPCDAMORTINSTSP caught exception '
 ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-10-08 15:48:07';
 ROLLBACK TO TEMPCDAMORTINSTSP;
 runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
 PROGRAM_NAME=>'TEMPCDAMORTINSTSP',force_log_entry=>TRUE);
 --
 END TEMPCDAMORTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


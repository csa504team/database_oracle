<cfcomponent output="false" extends="/cls/Application">
<!--- cls is the main capital access application structure so application scope is at the cls level
allow sub system to share login information and common items --->
	<!---
	AUTHOR:				Ian Clark Based on Steve Seaquist's work  for the US Small Business Administration.
	DATE:				02/01/2017.
	

		
	DESCRIPTION:		Application and Session control for  directories. Subdirectories may extend this component.
	NOTES:				Standard heading section
	INPUT:				None.
	OUTPUT:				Application and Session control.
	REVISION HISTORY:	02/01/2017, IAC:	Original implementation.

	--->
	<!--- set information about this directory --->

	<cfset this.datasource = "SBA">
	<cfset this.AppURL										= "/experiments/ian">
	<cfset this.SBALogSystemName							= "System Name">
	<cfset this.SysURL										= "/experiments/ian">
	<cfset this.OnRequestStartInclude						= "/experiments/ian/OnRequestStart.cfm"><!--- Or "none" --->
	<cfset this.WindowTitleBase								= "SBA - Example Application">
	</cfcomponent>
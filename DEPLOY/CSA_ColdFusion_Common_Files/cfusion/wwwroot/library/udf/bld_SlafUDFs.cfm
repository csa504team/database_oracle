<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/20/2011. 
DESCRIPTION:		Defines UDFs for SBA Look-and-Feel (cf_sbalookandfeel and/or cf_mainnav) internal use. 
NOTES:				By having this as a separate file, both cf_sbalookandfeel and cf_mainnav can call it. 
INPUT:				Varies by function called. 
OUTPUT:				Varies by function called. 
REVISION HISTORY:	12/16/2015, SRS:	Added ShowButtonInMainNavMenu, which uses a structure instead of an array, 
										but is used the exact same way. Whereas ShowButtonHasOverrides allowed more 
										liberal behavior for the "external system" silver buttons, we are instead 
										specifying where something belongs in the new MainNav Menus. IT'S POSSIBLE 
										FOR BOTH TO EXIST. If so, the silver buttons could get their own menu, 
										for example. Try not to get confused. The only thing ShowButtonInMainNavMenu 
										is used for is to define menu layout. 
					07/20/2011, SRS:	Original implementation. 
--->

<cfscript>
function ShowButtonHasOverrides								(pShow)
	{
	var	i													= 0;
	var	sStruct												= "";	// For temporary use only. 
	if	(NOT IsDefined("Request.SlafShowButtonOverrides"))
		return "No";
	if	(NOT IsArray(Request.SlafShowButtonOverrides))
		return "No";
	for	(i = 1; i le ArrayLen(Request.SlafShowButtonOverrides); i = i + 1)
		{
		sStruct												= Request.SlafShowButtonOverrides[i];
		if	(NOT IsStruct(sStruct))
			continue;
		if	(NOT IsDefined("sStruct.show"))
			continue;
		if	(CompareNoCase(pShow, sStruct.show) is 0)
			{
			// In addition to returning "Yes", return the structure for the caller to use. 
			// Note that Variables.Overrides exists only if the function returns "Yes".
			Variables.Overrides								= sStruct;
			return "Yes";
			}
		}
	return "No";
	}

Variables.MainNavMenuNames									= [];// Initialize empty array. Build them here. 

function ShowButtonIsInMainNavMenu							(pShow, pBuildingMenus)
	{
	var	i													= 0;
	var	sFound												= "No";
	var	sStructConfig										= "";
	var	sStructMenu											= "";
	var	sStructItem											= "";
	if	(NOT IsDefined("Request.SlafShowButtonsInMainNavMenu"))
		return "No";
	if	(NOT StructKeyExists(Request.SlafShowButtonsInMainNavMenu, pShow))
		return "No";
	sStructConfig											= Request.SlafShowButtonsInMainNavMenu[pShow];
	if	(NOT IsStruct(sStructConfig))
		return "No";
	if	(NOT StructKeyExists(sStructConfig, "type"))		// The only mandatory struct key. 
		return "No";
	Variables.MainNavItemStruct								= sStructConfig;	// In case the caller needs it. 
	if	(sStructConfig.type is "none")
		return "Yes";										// That is, we're handling it (by suppressing it). 
	if	(!pBuildingMenus)
		return "Yes";
	// The following is done only in the first pass through Attributes.Show, to build Variables.MainNavMenuNames. 
	// We may not ever have any need to call ShowButtonIsInMainNavMenu after the Attributes.Show pass, but in any case, 
	// the following should be done only in the first pass. 
	switch (sStructConfig.type)
		{
		case "item":
		case "text":
			// In menu items and text, "menu" has to have been defined in sStructConfig. Make it obvious if not: 
			if	(NOT StructKeyExists(sStructConfig, "menu"))
				sStructConfig["menu"]						= "Unknown";
			break;											// Out of switch. 
		case "link":
		case "page":
			// In menu bar links and callbacks, "menu" is forced to be pShow (ignored if other): 
			sStructConfig["menu"]							= pShow;
			break;											// Out of switch. 
		default:
			// Unknown type. Make error obvious. 
			sStructConfig["type"]							= "text";
			sStructConfig["menu"]							= "Error";
			pShow											&= " has an unknown type.";
		};
	sFound													= "No";
	for	(i = 1; i <= ArrayLen(Variables.MainNavMenuNames); i = i + 1)
		{
		sStructMenu											= Variables.MainNavMenuNames[i];
		if	(sStructMenu.menu is sStructConfig["menu"])
			{
			sFound											= "Yes";
			break;											// Out of loop. sStructMenu points to found menu in array. 
			}
		}
	if	(!sFound)
		{
		sStructMenu											= {"type" = "????", "menu" = sStructConfig.menu, "items" = []};
		ArrayAppend(Variables.MainNavMenuNames, sStructMenu);// sStructMenu points to new menu in array. 
		}
	sStructItem												= {"type" = sStructConfig.type};
	switch (sStructConfig.type)
		{
		case "item":
			sStructItem["item"]								= pShow;
			sStructMenu.type								= "menu";			// "item" implies we're in a menu. 
			if			(StructKeyExists(sStructConfig, "DoScript"))			sStructItem["DoScript"]	= sStructConfig.DoScript;
			// Copy struct keys only if they exist. The code that builds MainNav menus does StructKeyExists. 
			// It messes up of we set a default-if-not-given. 
			if	(ShowButtonHasOverrides(pShow))
				{// sStructConfig has priority over Variables.Overrides: 
				if		(StructKeyExists(sStructConfig,			"external"))	sStructItem["external"]	= sStructConfig.external;
				else if	(StructKeyExists(Variables.Overrides,	"external"))	sStructItem["external"]	= Variables.Overrides.external;
				if		(StructKeyExists(sStructConfig,			"href"))		sStructItem["href"]		= sStructConfig.href;
				else if	(StructKeyExists(Variables.Overrides,	"href"))		sStructItem["href"]		= Variables.Overrides.href;
				if		(StructKeyExists(sStructConfig,			"target"))		sStructItem["target"]	= sStructConfig.target;
				else if	(StructKeyExists(Variables.Overrides,	"target"))		sStructItem["target"]	= Variables.Overrides.target;
				if		(StructKeyExists(sStructConfig,			"title"))		sStructItem["title"]	= sStructConfig.title;
				else if	(StructKeyExists(Variables.Overrides,	"title"))		sStructItem["title"]	= Variables.Overrides.title;
				}
			else
				{
				if		(StructKeyExists(sStructConfig,			"external"))	sStructItem["external"]	= sStructConfig.external;
				if		(StructKeyExists(sStructConfig,			"href"))		sStructItem["href"]		= sStructConfig.href;
				if		(StructKeyExists(sStructConfig,			"target"))		sStructItem["target"]	= sStructConfig.target;
				if		(StructKeyExists(sStructConfig,			"title"))		sStructItem["title"]	= sStructConfig.title;
				}
			ArrayAppend(sStructMenu.items, sStructItem);
			break;											// Out of switch. 
		case "link":
			sStructMenu.type								= "link";			// "link" implies menu name itself is a link. No menu exists. 
			if			(StructKeyExists(sStructConfig, "DoScript"))			sStructMenu["DoScript"]	= sStructConfig.DoScript;
			break;											// Out of switch. 
		case "page":
			if	(StructKeyExists(sStructConfig, "url"))
				{
				sStructMenu.type							= "page";			// "page" implies calling PageSegmentsGrabber. 
				sStructMenu["url"]							= sStructConfig.url;
				}
			else
				{
				sStructMenu.type							= "menu";			// intended to be a page, but converting to menu. 
				sStructItem									= {"item" = "ERROR. No callback URL provided."};
				ArrayAppend(sStructMenu.items, sStructItem);
				}
			break;											// Out of switch. 
		case "text":
			sStructItem["item"]								= pShow;
			sStructMenu.type								= "menu";			// "text" implies we're in a menu. 
			ArrayAppend(sStructMenu.items, sStructItem);
			break;											// Out of switch. 
		default:
		};
	return "Yes";
	}
</cfscript>

<cfif CGI.Script_Name is "/library/udf/bld_SlafUDFs.cfm"><!--- Self-test mode. --->
	<cfif NOT IsDefined("Request.SlafDevTestProd")>
		<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
	</cfif>
	<cfif NOT IsDefined("Request.SlafTopOfHead")>
		<cfinclude template="/library/cfincludes/get_sbalookandfeel_topofhead.cfm">
	</cfif>
	<cfif NOT IsDefined("Request.SlafShowButtonOverrides")>
		<cfset Request.SlafShowButtonOverrides				=
			[
			{"show" = "User Profile", "target" = "_blank",	"href"	= "/security/user/dsp_user_old.cfm"}
			]><!--- Prototype of a new structure of structures to define menus from Show names. Top level key is Show name. --->
	</cfif>
	<cfif NOT IsDefined("Request.SlafShowButtonsInMainNavMenu")>
		<cfset Request.SlafShowButtonsInMainNavMenu			=
			{
			"Exit"			= {"type" = "none"},			<!--- Because it's now handled by the button bar. --->
			"Help"			= {"type" = "none"},			<!--- Because it's now handled by the button bar. --->
			"Print"			= {"type" = "none"},			<!--- Because it's now handled by the button bar. --->
			"Reports"		= {"type" = "page",	"url"		= "/security/reports/dsp_AppNav_steve.cfm"},
			"Say Hello 1"	= {"type" = "link",	"DoScript"	= "alert('Hello from the DoScript in the MainNavMenu configuration!');"},
			"Say Hello 2"	= {"type" = "link"},
			"User Access"	= {"type" = "item",	"menu"		= "User"},
			"DeliberateErr"	= {"type" = "err!",	"menu"		= "User"},
			"Plain Text"	= {"type" = "text",	"menu"		= "User"},
			"User Profile"	= {"type" = "item",	"menu"		= "User", "target" = "_top", "href" = "/security/user/dsp_user.cfm"},
			"UnknownMenu"	= {"type" = "item"}
			}><!--- Prototype of a new structure of structures to define menus from Show names. Top level key is Show name. --->
	</cfif>
	<cfoutput>#Request.SlafHead#
<title>SBA - Library - Self-Test of SBA Look-and-Feel UDFs</title>#Request.SlafTopOfHead#
<script>
function DoSayHello2										()
	{
	alert('Hello from the DoSayHello2 script!');
	}
</script>
</head>
<body class="pad10">
<p>
	Before running ShowButtonIsInMainNavMenu(): 
</p>
<ol></cfoutput>
	<cfloop index="Button" list="Exit,User Access,User Profile,Reports,Plain Text,Say Hello 1,Say Hello 2,DeliberateErr,UnknownMenu">
		<cfoutput>
	<li>#Button#: #ShowButtonIsInMainNavMenu(Button, "Yes")#</li></cfoutput>
	</cfloop>
	<cfoutput>
</ol>
<p>
	After running ShowButtonIsInMainNavMenu(), the following is a dump of Variables.MainNavMenuNames: 
</p>
<cfdump var="#Variables.MainNavMenuNames#" label="MainNavMenuNames" expand="No">
</body>
</html>
</cfoutput>
	<cfsetting showdebugoutput="No">
</cfif>

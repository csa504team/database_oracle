<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				12/08/2008
DESCRIPTION:		Common functions associated with SBA Java Objects. 
NOTES:				None. 
INPUT:				Varies by function. 
OUTPUT:				Varies by function. 
REVISION HISTORY:	05/04/2014, SRS:	Since this file can be executed by /library/ws/act_directorywatcher.cfc, it's subject to 
										a new problem with directory watchers, namely, that ColdFusion mapping are no longer 
										being respected. template="/xxx" doesn't work. template="/opt/iplanet/servers/docs/xxx" 
										doesn't work. The only thing that works is template="../xxx". What a nuisance! But, sad 
										to say, we don't know the relative directory path when NewSBAJavaObject is called. 
										So we have to comment out our cfincludes of log_SleSPCCatch and log_SPCSuccess! 
					09/15/2009, NNI:	Added gov.sba.etran to list where prepObj method is called for object intantiation.
					12/09/2008, SRS:	Original implementation.
--->

<cffunction name="NewSBAJavaObject"	output="false"	returntype="any"	hint="Instantiates an SBA Java Object only. Not for use with standard Java Objects, which you can and should instantiate with your own direct call to CreateObject. Returns null in case of error, which implies that the receiving variable will not be defined if TxnErr is 'Yes'.">
	<cfargument name="ObjectName"	type="string"	required="yes"		hint="Mandatory. Just the name of an SBA Java Object itself. (Package name will be inferred.) Pass constructor parameters as optional subsequent arguments.">
	<!--- In CF 7-8, local variables ("var") must be defined immediately after cfarguments. (CF 9 allows anywhere.) --->
	<cfset var MaxArgumentsCurrentlySupported			= 7>
	<cfset var NbrArgumentsMinus1						= ArrayLen(Arguments) - 1>
	<cfset var NewObj									= "">
	<cfset var ObjectNameInferred						= Arguments.ObjectName>
	<cfset var ObjectNameArray							= ArrayNew(1)>
	<cfset var ObjectNameArrayLen						= 0>
	<cfset var PackageName								= "">
	<!---
	NewSBAJavaObject could be used within a transaction block, because updates to security use Java objects. Therefore, 
	we obey all the rules of SPC files, even to the extent of calling SPC-specific logging functions. Note that this 
	implies that we check TxnErr every time we might set it, so that almost all cfreturn logic occurs at the very end. 
	The only exception is if TxnErr was already "Yes" at the time this function was called. In that case, we simply 
	cfreturn null, as if this function was never called. 
	--->
	<cfparam name="Variables.TxnErr"					default="No">
	<cfif Variables.TxnErr>
		<cfreturn JavaCast("null", "")>
	</cfif>
	<cfparam name="Variables.ErrMsg"					default="">
	<cfif ArrayLen(Arguments) GT MaxArgumentsCurrentlySupported>
		<cfset Variables.TxnErr							= "Yes">
		<cfset Variables.ErrMsg							= "#Variables.ErrMsg# <li>NewSBAJavaObject was asked to create a "
														& "Java object with too many constructor parameters "
														& "(#NbrArgumentsMinus1#).</li> ">
	</cfif>
	<cfif NOT Variables.TxnErr>
		<cfif		(Len	(Arguments.ObjectName)		GE 7)
			AND		(Left	(Arguments.ObjectName,		7) IS "SbaPims")>
			<cfset PackageName							= "gov.sba.pims">
		<cfelseif	(Len	(Arguments.ObjectName)		GE 6)
			AND		(Left	(Arguments.ObjectName,		6) IS "SbaSax")>
			<cfset PackageName							= "gov.sba.sax">
		<cfelseif	(Len	(Arguments.ObjectName)		GE 7)
			AND		(Left	(Arguments.ObjectName,		7) IS "SbaUtil")>
			<cfset PackageName							= "gov.sba.util">
		<cfelse>
			<cfset ObjectNameArray						= ListToArray(Arguments.ObjectName, ".")>
			<cfset ObjectNameArrayLen					= ArrayLen(ObjectNameArray)>
			<cfif	ObjectNameArrayLen					GT 1>
				<cfset ObjectNameInferred				= ObjectNameArray[ObjectNameArrayLen]>
				<cfset ArrayDeleteAt(ObjectNameArray, ObjectNameArrayLen)>
				<cfswitch expression="#ObjectNameArray[1]#">
				<cfcase value="java,javax"><!--- Leave JRE package names untouched ---></cfcase>
				<cfdefaultcase>
					<!--- Otherwise, prepend "gov." and/or "sba.", as necessary: --->
					<cfif ObjectNameArray[1] IS NOT "gov">
						<cfset ArrayInsertAt(ObjectNameArray, 1, "gov")>
					</cfif>
					<cfif ObjectNameArray[2] IS NOT "sba">
						<cfset ArrayInsertAt(ObjectNameArray, 2, "sba")>
					</cfif>
				</cfdefaultcase>
				</cfswitch>
				<cfset PackageName						= ArrayToList(ObjectNameArray, ".")>
			<cfelse>
				<cfswitch expression="#ObjectNameInferred#">
				<!---
				Authorization and CentralBean are not supported due to the naming conflict with gov.sba.security. So 
				you'll have to pass "pims.Authorization", for example, if you want that one, not the one in security. 
				--->
				<cfcase value="Address,Agreement,Contact,Location,Note,Partner">
					<cfset PackageName					= "gov.sba.pims">
				</cfcase>
				<!--- Cfcase doesn't like to concatenate value strings with "&", so this list is necessarily long: --->
				<cfcase value="AppSystem,AppURL,Assurance,Authorization,CentralBean,CodeTable,Credential,CSProvider,DBPlatform,DecisionType,LDAPUtil,ProcessForm,Report,Request,SBACredential,User,UserBusiness,UserRole,UserSession,UserType">
					<cfset PackageName					= "gov.sba.security">
				</cfcase>
				<!--- Add more cfcase statements here (for objects whose package names can't be inferred by prefix). --->
				<cfdefaultcase>
					<cfset Variables.TxnErr				= "Yes">
					<cfset Variables.ErrMsg				= "#Variables.ErrMsg# <li>NewSBAJavaObject was asked to create an "
														& "unknown Java object ('#Arguments.ObjectName#').</li> ">
				</cfdefaultcase>
				</cfswitch>
			</cfif>
		</cfif>
		<cfif NOT Variables.TxnErr>
			<cftry>
				<cfset NewObj							= CreateObject("java", "#PackageName#.#ObjectNameInferred#")>
				<cfswitch expression="#ArrayLen(Arguments)#">
				<cfcase value="1"><cfset NewObj.init()></cfcase>
				<cfcase value="2"><cfset NewObj.init(Arguments[2])></cfcase>
				<cfcase value="3"><cfset NewObj.init(Arguments[2],Arguments[3])></cfcase>
				<cfcase value="4"><cfset NewObj.init(Arguments[2],Arguments[3],Arguments[4])></cfcase>
				<cfcase value="5"><cfset NewObj.init(Arguments[2],Arguments[3],Arguments[4],Arguments[5])></cfcase>
				<cfcase value="6"><cfset NewObj.init(Arguments[2],Arguments[3],Arguments[4],Arguments[5],Arguments[6])></cfcase>
				<cfcase value="7"><cfset NewObj.init(Arguments[2],Arguments[3],Arguments[4],Arguments[5],Arguments[6],Arguments[7])></cfcase>
				<!---
				There isn't any need for a defaultcase here. This was handled above by MaxArgumentsCurrentlySupported. We 
				don't want to test the number of arguments after the CreateObject call, so it's better to do the test at the 
				very start of the cffunction. 
				--->
				</cfswitch>
				<cfswitch expression="#PackageName#">
				<cfcase value="gov.sba.pims,gov.sba.security,gov.sba.etran">
					<cfset NewObj.prepObj()>
				</cfcase>
				</cfswitch>
				<cfcatch type="Any">
					<cfset Variables.TxnErr				= "Yes">
					<cfset Variables.ErrMsg				= "#Variables.ErrMsg# <li>NewSBAJavaObject encountered an error "
														& "trying to create an object of type '#Arguments.ObjectName#'">
					<cfif ArrayLen(Arguments) GT 1>
						<cfset Variables.ErrMsg			= "#Variables.ErrMsg# with #NbrArgumentsMinus1# parameters.">
					<cfelse>
						<cfset Variables.ErrMsg			= "#Variables.ErrMsg#.">
					</cfif>
					<cfset Variables.ErrMsg				= "#Variables.ErrMsg# The following information may help: "
														& "#CFCatch.Message# #CFCatch.Detail#</li>">
					<!--- cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm" --->
				</cfcatch>
			</cftry>
		</cfif>
	</cfif>
	<cfif NOT Variables.TxnErr>
		<!--- cfinclude template="/library/cfincludes/log_SPCSuccess.cfm" --->
		<cfreturn NewObj>
	</cfif>
	<!---
	Because this is a UDF, not an include like SPC files, the cfreturn just done is possible. This means we don't have 
	to check TxnErr anymore. All of the following is what we do in case of an error return. 
	--->
	<cfset Variables.SleEntityName						= "">
	<cfif IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
	<cfreturn JavaCast("null", "")>
</cffunction>

<cfif CGI.Script_Name IS "/library/udf/bld_JavaUDFs.cfm"><!--- Called directly from a browser = self-test mode: --->
	<cfset Variables.DumpObject							= NewSBAJavaObject("SbaUtilDumpObject")>
	<cfset Variables.ConfigCacheVector					= NewSBAJavaObject(
															"SbaSaxConfigCacheVector",
															JavaCast("int", "1"),
															JavaCast("int", "2")
															)>
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>Test bld_JavaUDFs</title>
<link href="/library/css/sba.css" rel="stylesheet" type="text/css">
</head>
<body class="normal pad20">
<h3 align="center">Test bld_JavaUDFs</h3>

<cfif Variables.TxnErr>	#Variables.ErrMsg#
<cfelse><!-- Use DumpObject to dump itself! -->
#Variables.DumpObject.dumpAsHTML(Variables.DumpObject, "Variables.DumpObject", true)#<br>
<p>*****************************************************</p>
#Variables.DumpObject.dumpAsHTML(Variables.ConfigCacheVector, "Variables.ConfigCacheVector", true)#<br></cfif>

</body>
</html>
</cfoutput>
</cfif>

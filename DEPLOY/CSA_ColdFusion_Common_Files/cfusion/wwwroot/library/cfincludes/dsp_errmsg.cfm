<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Displays the Server.Msg, Variables.Commentary and Variables.ErrMsg in a standard format. 
NOTES:

	Include this file as the first display in the AppData region of the screen. It's okay to include a padding div before 
	including this file, or not, as you wish. If you don't first code a padding div, the topmost message (if any) will be 
	displayed adjacent to the 1px solid black line at the top of AppData. That's okay too. No problem. Whichever you like. 

	Even if you can't implement Form Data Recovery for some reason, it's nevertheless an SBA ColdFusion Standard to include 
	this file at the top of **EVERY** AppData. The reason is, the sysadmins may choose to set a server-wide message, and we 
	need that message to appear at the top of every page, in case it's an emergency to get that message out to every user. 

	To change font characteristics or div boxes, use the style tag to override the .dem_xxx classes. For reference, the 
	original class definitions are in sba.{quirks,strict}.css. But don't edit them there except to make a permanent change. 
	Use the style tag instead. To override display order, set Variables.DEM_DisplayOrder before including this file. 
	Overrides are to allow prototyping changes and possibly to deal with special situations, such as externally-imposed 
	policies. They should NOT be used to violate consistent look-and-feel in OISS. (Normally, stick with the defaults!) 

	The namespace prefix "DEM_" is short for "dsp_errmsg", to prevent conflicts with your own variable names. 

INPUT:				Variables.DEM_DisplayOrder, CSS class properties. 
OUTPUT:				Displays the Server.Msg, Variables.Commentary and Variables.ErrMsg in a standard format. 
REVISION HISTORY:	01/06/2017, SMJ:	Removed the "Warning(s) text from warning messages per action item from the Sprint 2-52 review."
					09/20/2013, SRS:	Fixed bullets outside the block problem. Removed temporary "CacheBuster" code. 
					04/16/2012, SRS:	Part of 02/02/2012 checkout. Added temporary "CacheBuster" code. (Remove the next 
										time sba.quirks.css is released to production.) 
					02/02/2012, SRS:	Added new "Warning(s)" message type and self-test mode. 
					03/29/2011, SRS:	Added this header. Made it all configurable via CSS and DEM_DisplayOrder. 
					Unknown, SRS:		Original implementation.
--->

<!--- Configuration Parameters: --->

<cfparam name="Variables.DEM_DisplayOrder"		default="S,C,W,E"><!--- Default display order. --->
<cfset Variables.DEM_SelfTestMode				= (CGI.Script_Name is "/library/cfincludes/dsp_errmsg.cfm")>

<!--- Initializations: --->

<cfset Variables.DEM_ServerMsg					= "">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.Msg")>
		<cfset Variables.DEM_ServerMsg			= Server.Msg>
	<cfelseif Variables.DEM_SelfTestMode>
		<cfset Variables.DEM_ServerMsg			= "Simulated server message.">
	</cfif>
</cflock>

<cfif Variables.DEM_SelfTestMode>
	<cfset Variables.Commentary					= "Simulated commentary.">
	<cfset Variables.WarnMsg					= "Simulated warning.">
	<cfset Variables.ErrMsg						= "Simulated error message.">
	<!--- Make sure we pick up the CSS classes in /library/css!! --->
	<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
	<cfinclude template="/library/cfincludes/get_sbalookandfeel_topofhead.cfm">
	<cfoutput>#Request.SlafHead#
<title>SBA - Library - dsp_errmsg Self Test Mode</title>#Request.SlafTopOfHead#
<body class="pad20">
</cfoutput>
</cfif>

<!--- Display the variables: --->

<cfloop index="Variables.DEM_Idx" list="#Variables.DEM_DisplayOrder#">
	<cfswitch expression="#Variables.DEM_Idx#">
	<cfcase value="S"><!--- Server-wide message, used by sysadmins to warn users of impending down time. --->
		<cfif Len(Variables.DEM_ServerMsg)>
			<cfoutput>
<div align="center" class="dem_positioning">
	<div class="inlineblock dem_servermsg">
		#Variables.DEM_ServerMsg#
	</div>
</div>
</cfoutput>
		</cfif>
	</cfcase>
	<cfcase value="C"><!--- Commentary. --->
		<cfif IsDefined("Variables.Commentary") AND (Len(Variables.Commentary) GT 0)>
			<cfoutput>
<div align="center" class="dem_positioning">
	<div class="inlineblock dem_commentary">
		Commentary: #Variables.Commentary#
	</div>
</div>
</cfoutput>
		</cfif>
	</cfcase>
	<cfcase value="W"><!--- WarnMsg. --->
		<cfif IsDefined("Variables.WarnMsg") AND (Len(Variables.WarnMsg) GT 0)>
			<cfoutput>
<div align="center" class="dem_positioning">
	<div class="inlineblock dem_warnmsg">
		#Variables.WarnMsg#
	</div>
</div>
</cfoutput>
		</cfif>
	</cfcase>
	<cfcase value="E"><!--- Error message (usually from an act page on return to associated dsp page). --->
		<cfif IsDefined("Variables.ErrMsg") AND (Len(Variables.ErrMsg) GT 0)>
			<cfoutput>
<div align="center" class="dem_positioning">
	<div class="inlineblock dem_errmsg">
		#Variables.ErrMsg#
	</div>
</div>
</cfoutput>
		</cfif>
	</cfcase>
	</cfswitch>
</cfloop>

<cfif Variables.DEM_SelfTestMode>
	<cfoutput>
</body>
</head>
</cfoutput>
</cfif>

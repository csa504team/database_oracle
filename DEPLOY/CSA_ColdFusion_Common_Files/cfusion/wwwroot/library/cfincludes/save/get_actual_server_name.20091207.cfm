<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Defines Request.SlafServerName and Request.SlafLocalHost. 
NOTES:

	If you cfinclude get_sbashared_variables.cfm, it will also define these 2 variables. Therefore, this is a minimal, 
	low-level routine you can call when you don't want to do everything in get_sbashared_variables. Think of this routine 
	as a stripped-down alternative to get_sbashared_variables. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. 
REVISION HISTORY:	12/07/2009, SRS:	Added Request.SlafServerStartupTime (allowing a rough estimate of CF uptime). 
					10/09/2008, SRS:	Added Request.SlafServerInstanceName for multiserver installations. On a single 
										instance multiserver installation, the new code always returns "cfusion". On a 
										standalone server installation, it always returns "coldfusion". So the new code 
										doesn't hurt anything in those situations. 
					04/27/2006, SRS:	Made self-testing. (Call as a Web page to see the variables it defines.) 
					01/31/2006, SRS:	Original implementation. 
--->

<!--- Initializations --->

<cfset		   Variables.GetActualServerSelfTestMode		= (CGI.Script_Name IS "/library/cfincludes/get_sbashared_variables.cfm")>
<cfparam name="Variables.GetActualServerForceRebuild"		default="No"><!--- Allows rebuild even if not self test. --->
<cfif Variables.GetActualServerSelfTestMode>
	<cfset Variables.GetActualServerForceRebuild			= "Yes">
</cfif>

<cfset Request.SlafServerAddr								= "">
<cfset Request.SlafServerName								= "">
<cfset Request.SlafServerInstanceName						= "">
<cfset Request.SlafServerStartupTime						= "">
<cfif NOT Variables.GetActualServerForceRebuild>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SlafServerStartupTime")>
			<cfset Request.SlafServerStartupTime			= Server.SlafServerStartupTime>
		</cfif>
		<cfif	IsDefined("Server.SlafServerAddr")
			AND	IsDefined("Server.SlafServerName")
			AND	IsDefined("Server.SlafServerInstanceName")>
			<cfset Request.SlafServerAddr					= Server.SlafServerAddr>
			<cfset Request.SlafServerName					= Server.SlafServerName>
			<cfset Request.SlafServerInstanceName			= Server.SlafServerInstanceName>
		</cfif>
	</cflock>
</cfif>
<cfif Len(Request.SlafServerStartupTime) IS 0>
	<cfset Request.SlafServerStartupTime					= Now()>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerStartupTime					= Request.SlafServerStartupTime>
	</cflock>
</cfif>
<cfif Len(Request.SlafServerInstanceName) IS 0>
	<cfset Variables.LocalHost								= CreateObject("Java", "java.net.InetAddress").getLocalHost()>
	<cfset Request.SlafServerAddr							= Trim(Variables.LocalHost.getHostAddress())>
	<cfset Request.SlafServerName							= Trim(Variables.LocalHost.getHostName())>
	<cfset Variables.JRunKernel								= CreateObject("Java", "jrunx.kernel.JRun")>
	<cfset Request.SlafServerInstanceName					= Trim(Variables.JRunKernel.getServerName())>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerAddr						= Request.SlafServerAddr>
		<cfset Server.SlafServerName						= Request.SlafServerName>
		<cfset Server.SlafServerInstanceName				= Request.SlafServerInstanceName>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">
<cfset Request.SlafServerAndInstanceName					= "#Request.SlafServerName# (#Request.SlafServerInstanceName#)">

<cfif CGI.Script_Name IS "/library/cfincludes/get_actual_server_name.cfm">
	<cfoutput>
Request.SlafServerAddr = &quot;#Request.SlafServerAddr#&quot;. <br/>
Request.SlafServerName = &quot;#Request.SlafServerName#&quot;. <br/>
Request.SlafServerInstanceName = &quot;#Request.SlafServerInstanceName#&quot;. <br/>
Request.SlafServerStartupTime = &quot;#Request.SlafServerStartupTime#&quot;. <br/>
Request.SlafLocalHost = &quot;#Request.SlafLocalHost#&quot;. 
</cfoutput>
</cfif>

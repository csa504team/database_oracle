<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif NOT IsNumeric(Variables.FVal)>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not numeric.</li>">
	<cfelseif Variables.FVal LT -2415919104>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot be less than -2,415,919,104.</li>">
	<cfelseif Variables.FVal GT 2415919103>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 2,415,919,103.</li>">
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				08/19/2005
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel and cf_mainnav. 
NOTES:				<cfinclude template="get_sbalookandfeel_variables.cfm"> right after <cfapplication>. 
					See also: 
						/library/cfincludes/dsp_sbalookandfeel_variables.cfm (include to define form variables)
						/library/javascripts/SetSbalookandfeelVariables.js   (JavaScript to set form variables)
INPUT:				Variables in the Session and/or Form scopes. 
OUTPUT:
	Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other variables 
	defined by the application. Possible changes to Session scope variables with the same names. 

	Variables scope variables: Always defines Variables.FormDataRecovered and Variables.PageName, which contains the current 
	page name, including ".cfm". If form data was automatically recovered, Variables.FormDataRecovered will be "Yes" and the 
	Variables scope will also contain data from the last Form scope saved by put_sbalookandfeel_saveformdata. But if 
	Variables.FormDataRecovered is "No", no form data was copied to the Variables scope. Since form data recovery is normally 
	used by the application, not by custom tags, "Slaf" is NOT prepended to form data recovery fields. 

REVISION HISTORY:	01/31/2006, SRS:	Moved Server scope logic to get_sbashared_variables.cfm. 
					12/16/2005, SRS:	Added SlafLogoWidth (for resizing left side elements). 
					11/16/2005, SRS:	Added determination of SlafLocalHost, SlafServerOsName and various associated 
										platform-related variables. 
					09/26/2005, SRS:	Added "automatic form data recovery". 
					08/19/2005, SRS:	Original implementation. 
--->

<!--- Predefine Request scope variables that are not technically look-and-feel related, but which look-and-feel uses: --->

<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">

<!--- Predefine Request scope output variables, so that they'll be guaranteed to exist after calling this routine: --->

<cfset Request.SlafClientHeight								= ""><!--- "Slaf" = "sbalookandfeel". --->
<cfset Request.SlafClientWidth								= "">
<cfset Request.SlafLogoWidth								= "196">
<cfset Request.SlafResolution								= "1024x768"><!--- Always make sure it's a lowercase "x". --->
<cfset Request.SlafSessionControl							= "No"><!--- Will be set to "ColdFusion" or "Java" if on. --->
<cfset Request.SlafTextOnly									= "No"><!--- Maintained independently from resolution vars. --->

<!--- Prepare for form data recovery. Use Variables scope because these are typically not passed to custom tags: --->

<cfset Variables.FormDataRecovered							= "No">
<cfset Variables.PageName									= REReplace (CGI.Script_Name, ".*/", "", "ALL")>
<cfset Variables.QueryString								= "">
<!--- Remove question mark and URL parameters, if present: --->
<cfset Variables.SlafQMOffset								= Find("?", Variables.PageName)>
<cfif Variables.SlafQMOffset GT 0>
	<cfset Variables.PageName								= Left	(Variables.PageName, Variables.SlafQMOffset - 1)>
	<cfset Variables.QueryStringLen							= Len	(Variables.PageName) -	Variables.SlafQMOffset>
	<cfset Variables.QueryString							= Right	(Variables.PageName,	Variables.SlafQMOffset)>
<cfelse>
	<cfset Variables.QueryString							= CGI.Query_String>
	<cfset Variables.QueryStringLen							= Len	(Variables.QueryString)>
</cfif>

<!--- Now go get their real values from Form, URL or Session scope (whichever exists), and maintain Session scope values. --->

<cfset Variables.SessionVariablesExist						= "No">
<cftry>
	<cflock scope="SESSION" type="READONLY" timeout="30">
		<cfif IsDefined("Session.SessionId")>
			<cfif Find("_", Session.SessionId) GT 0>
				<!--- CF session control has Session.SessionId format: appname_CFID_CFToken, so "_" would be found. --->
				<cfset Request.SlafSessionControl			= "ColdFusion">
				<cfset Variables.CFID						= Session.CFID>
				<cfset Variables.CFToken					= Session.CFToken>
			<cfelse>
				<!--- Java session control has (100% numeric) jsessionid as Session.SessionId, so "_" would NOT be found. --->
				<cfset Request.SlafSessionControl			= "Java">
				<cfset Variables.JSessionId					= Session.JSessionId>
			</cfif>
			<cfset Variables.SessionVariablesExist			= "Yes">
			<cfset Variables.SlafTextChanged				= "No">
			<cfset Variables.SlafVarsChanged				= "No">
			<cfif IsDefined("Session.SlafClientHeight")><!--- If one exists, they're all 3 assumed to exist. --->
				<cfif IsDefined("Form.SlafClientHeight")>
					<cfif	(Session.SlafClientHeight		IS NOT Form.SlafClientHeight)
						OR	(Session.SlafClientWidth		IS NOT Form.SlafClientWidth)
						OR	(Session.SlafLogoWidth			IS NOT Form.SlafLogoWidth)
						OR	(Session.SlafResolution			IS NOT Form.SlafResolution)>
						<cfset Request.SlafClientHeight		= Form.SlafClientHeight>
						<cfset Request.SlafClientWidth		= Form.SlafClientWidth>
						<cfset Request.SlafLogoWidth		= Form.SlafLogoWidth>
						<cfset Request.SlafResolution		= Form.SlafResolution>
						<cfset Variables.SlafVarsChanged	= "Yes">
					<cfelse>
						<cfset Request.SlafClientHeight		= Session.SlafClientHeight>
						<cfset Request.SlafClientWidth		= Session.SlafClientWidth>
						<cfset Request.SlafLogoWidth		= Session.SlafLogoWidth>
						<cfset Request.SlafResolution		= Session.SlafResolution>
					</cfif>
				<cfelse>
					<cfset Request.SlafClientHeight			= Session.SlafClientHeight>
					<cfset Request.SlafClientWidth			= Session.SlafClientWidth>
					<cfset Request.SlafLogoWidth			= Session.SlafLogoWidth>
					<cfset Request.SlafResolution			= Session.SlafResolution>
				</cfif>
			<cfelseif IsDefined("Form.SlafClientHeight")><!--- That is, this is first time we're seeing Form variables. --->
				<cfset Request.SlafClientHeight				= Form.SlafClientHeight>
				<cfset Request.SlafClientWidth				= Form.SlafClientWidth>
				<cfset Request.SlafLogoWidth				= Form.SlafLogoWidth>
				<cfset Request.SlafResolution				= Form.SlafResolution>
				<cfset Variables.SlafVarsChanged			= "Yes">
			</cfif>
			<!--- Maintain SlafTextOnly separately, because it can change independently of the others. --->
			<cfif IsDefined("Session.SlafTextOnly")>
				<cfif IsDefined("Form.SlafTextOnly")>
					<cfif	(Session.SlafTextOnly			IS NOT Form.SlafTextOnly)>
						<cfset Request.SlafTextOnly			= Form.SlafTextOnly>
						<cfset Variables.SlafTextChanged	= "Yes">
					<cfelse>
						<cfset Request.SlafTextOnly			= Session.SlafTextOnly>
					</cfif>
				<cfelse>
					<cfset Request.SlafTextOnly				= Session.SlafTextOnly>
				</cfif>
			<cfelseif IsDefined("Form.SlafTextOnly")><!--- That is, this is first time we're seeing Form.SlafTextOnly. --->
				<cfset Request.SlafTextOnly					= Form.SlafTextOnly>
				<cfset Variables.SlafTextChanged			= "Yes">
			</cfif>
			<!--- Automatic form data recovery (as opposed to manually including get_sbalookandfeel_saveformdata[_nolock]): --->
			<cfif	(NOT IsDefined("Form.PageNames"))
				AND	IsDefined("Session.SlafSaveFormData.PageNames")
				AND	(ListFind(Session.SlafSaveFormData.PageNames, Variables.PageName) GT 0)>
				<cfinclude template="get_sbalookandfeel_saveformdata_nolock.cfm">
				<cfset Variables.FormDataRecovered			= "Yes">
			</cfif>
		</cfif>
	</cflock>
	<cfcatch type="Any">
		<!--- Do nothing. If we can't lock the Session scope, that just means that there ain't no Session scope. --->
	</cfcatch>
</cftry>
<cfif Variables.SessionVariablesExist>
	<cfif Variables.SlafVarsChanged OR Variables.SlafTextChanged OR IsDefined("Form.PageNames")>
		<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
			<cfif Variables.SlafVarsChanged>
				<cfset Session.SlafClientHeight				= Request.SlafClientHeight>
				<cfset Session.SlafClientWidth				= Request.SlafClientWidth>
				<cfset Session.SlafLogoWidth				= Request.SlafLogoWidth>
				<cfset Session.SlafResolution				= Request.SlafResolution>
			</cfif>
			<cfif Variables.SlafTextChanged>
				<cfset Session.SlafTextOnly					= Request.SlafTextOnly>
			</cfif>
			<cfif IsDefined("Form.PageNames")>
				<cfinclude template="put_sbalookandfeel_saveformdata_nolock.cfm">
				<!---
				If Form.PageNames is defined, we avoided doing the get in the first cflock, because we want the CURRENT 
				contents of the Form scope to be recovered, not the previous contents as of the last put. But now that we've 
				put the current contents of the Form scope out there, now we DO want to do form data recovery (if this page 
				is one of the magic PageNames): 
				--->
				<cfif	(ListFind(Form.PageNames, Variables.PageName) GT 0)>
					<cfinclude template="get_sbalookandfeel_saveformdata_nolock.cfm">
				</cfif>
			</cfif>
		</cflock>
	</cfif>
<cfelse>
	<!--- Even though (here) the application isn't using SessionManagement, set Request.Slaf vars if we can: --->
	<cfif IsDefined("Form.SlafClientHeight")>
		<cfset Request.SlafClientHeight						= Form.SlafClientHeight>
		<cfset Request.SlafClientWidth						= Form.SlafClientWidth>
		<cfset Request.SlafLogoWidth						= Form.SlafLogoWidth>
		<cfset Request.SlafResolution						= Form.SlafResolution>
		<cfset Request.SlafTextOnly							= Form.SlafTextOnly>
	</cfif>
</cfif>

<!--- The following is for testing and debugging. If this file is called directly (not cfincluded), dump what we did: --->

<cfif CGI.Script_Name IS "/library/cfincludes/get_sbalookandfeel_variables.cfm">
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>get_sbalookandfeel_variables.cfm</title></head>
<body bgcolor="##ffffff">

<p>Variables scope: <cfdump var="#Variables#"></p>
<p>Request scope: <cfdump var="#Request#"></p>

</body>
</html>
</cfoutput>
</cfif>

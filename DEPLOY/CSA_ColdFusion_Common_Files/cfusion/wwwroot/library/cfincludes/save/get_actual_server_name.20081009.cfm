<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Defines Request.SlafServerName and Request.SlafLocalHost. 
NOTES:

	If you cfinclude get_sbashared_variables.cfm, it will also define these 2 variables. Therefore, this is a minimal, 
	low-level routine you can call when you don't want to do everything in get_sbashared_variables. Think of this routine 
	as a stripped-down alternative to get_sbashared_variables. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. 
REVISION HISTORY:	10/09/2008, SRS:	Added Request.SlafServerInstanceName for multiserver installations. On a single 
										instance multiserver installation, the new code always returns "cfusion". On a 
										standalone server installation, it always returns "coldfusion". So the new code 
										doesn't hurt anything in those situations. 
					04/27/2006, SRS:	Made self-testing. (Call as a Web page to see the variables it defines.) 
					01/31/2006, SRS:	Original implementation. 
--->

<cfset Request.SlafServerInstanceName						= "">
<cfset Request.SlafServerName								= "">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.SlafServerInstanceName")>
		<cfset Request.SlafServerInstanceName				= Server.SlafServerInstanceName>
	</cfif>
	<cfif IsDefined("Server.SlafServerName")>
		<cfset Request.SlafServerName						= Server.SlafServerName>
	</cfif>
</cflock>
<cfif Len(Request.SlafServerInstanceName) IS 0>
	<cfset Variables.JRunKernel								= CreateObject("Java", "jrunx.kernel.JRun")>
	<cfset Request.SlafServerInstanceName					= Trim(Variables.JRunKernel.getServerName())>
	<cfif Len(Request.SlafServerName) IS 0>
		<cfset Variables.InetAddress						= CreateObject("Java", "java.net.InetAddress")>
		<cfset Request.SlafServerName						= Trim(Variables.InetAddress.getLocalHost().getHostName())>
	</cfif>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerInstanceName				= Request.SlafServerInstanceName>
		<cfset Server.SlafServerName						= Request.SlafServerName>
	</cflock>
<cfelseif Len(Request.SlafServerName) IS 0>
	<cfset Variables.InetAddress							= CreateObject("Java", "java.net.InetAddress")>
	<cfset Request.SlafServerName							= Trim(Variables.InetAddress.getLocalHost().getHostName())>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerName						= Request.SlafServerName>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">
<cfset Request.SlafServerAndInstanceName					= "#Request.SlafServerName# (#Request.SlafServerInstanceName#)">

<cfif CGI.Script_Name IS "/library/cfincludes/get_actual_server_name.cfm">
	<cfoutput>
Request.SlafServerName = &quot;#Request.SlafServerName#&quot;. <br>
Request.SlafLocalHost = &quot;#Request.SlafLocalHost#&quot;. 
</cfoutput>
</cfif>

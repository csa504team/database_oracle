<!---
AUTHOR:				Steve Seaquist
DATE:				05/08/2002
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR10 and converts to SBA TaxId format. 
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	09/03/2002, JBB:	Added code for validating the SSN digits as allocated by SSA
					05/08/2002, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFIF		(Len(Variables.FVal)		IS 10)
				AND	(IsNumeric(Left	(Variables.FVal, 2)))
				AND	(Mid			(Variables.FVal, 3, 1)	IS "-")
				AND	(IsNumeric(Right(Variables.FVal, 7)))>
			<CFIF (NOT IsDefined("Variables.TaxIdInd")) OR (Variables.TaxIdInd IS "E")>
				<CFSET Variables.FVal			= Variables.PrefixEIN
												& Left	(Variables.FVal, 2)
												& Right	(Variables.FVal, 7)>
			<CFELSE>
				<CFSET Variables.FieldErrMsg	= Variables.FName & " must be in 99-9999999 format.">
				<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
			</CFIF>
		<CFELSEIF	(Len(Variables.FVal)		IS 11)
				AND	(IsNumeric(Left	(Variables.FVal, 3)))
				AND	(Mid(Variables.FVal, 4, 1)	IS "-")
				AND	(IsNumeric(Mid	(Variables.FVal, 5, 2)))
				AND	(Mid(Variables.FVal, 7, 1)	IS "-")
				AND	(IsNumeric(Right(Variables.FVal, 4)))>
			<CFIF (NOT IsDefined("Variables.TaxIdInd")) OR (Variables.TaxIdInd IS "S")>
				<CFSET Variables.FVal			= Variables.PrefixSSN
												& Left	(Variables.FVal, 3)
												& Mid	(Variables.FVal, 5, 2)
												& Right	(Variables.FVal, 4)>
			<CFELSE>
				<CFSET Variables.FieldErrMsg	= Variables.FName & " must be in 999-99-9999 format.">
				<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
			</CFIF>
		<CFELSE>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " must be in 99-9999999 or 999-99-9999 format.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		</CFIF>
		<CFIF (NOT IsDefined("Variables.TaxIdInd")) OR (Variables.TaxIdInd IS "S")>
			<CFSET SArea	= int(mid(Variables.FVal,2,3))>
			<CFIF NOT (((SArea GE 1) AND (SArea LE 728)) OR ((SArea GE 750) AND (SArea LE 772)))>
				<CFSET Variables.FieldErrMsg	= "Although the Tax ID is in SSN format (999-99-9999),"
												& "it does not begin with a valid 'area' (first 3 digits),"
												& "as allocated by the Social Security Administration.">
				<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
			<CFELSEIF mid(Variables.FVal,5,2) EQ "00">
				<CFSET Variables.FieldErrMsg	= "Although the Tax ID is in SSN format (999-99-9999),"
												& "it does not contain a valid 'group' (middle 2 digits),"
												& "as allocated by the Social Security Administration.">
				<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
			<CFELSEIF right(Variables.FVal,4) EQ "0000">
				<CFSET Variables.FieldErrMsg	= "Although the Tax ID is in SSN format (999-99-9999),"
												& "it does not end with a valid last 4 digits,"
												& "as allocated by the Social Security Administration.">
				<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
			</CFIF>		
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

	

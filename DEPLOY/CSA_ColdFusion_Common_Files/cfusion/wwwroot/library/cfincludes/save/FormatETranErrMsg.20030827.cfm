<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Provides consistent handling of e-tran error messages. 
NOTES:				None
INPUT:				Variables.EtranErrMsg
OUTPUT:				Variables.ErrMsg and Variables.SaveMe
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	12/15/2000, SRS:	Moved just about every e-tran file to the new etran subdirectory. 
										Added this comment header. 
--->
<CFIF (IsDefined("Variables.WSAuthorized") AND (Variables.WSAuthorized))>
	<CFIF Find(Variables.MsgDelimiter,Variables.EtranErrMsg) EQ 0 >
		<CFSET Variables.EtranErrMsg = Variables.EtranErrMsg & Variables.MsgDelimiter>
	</CFIF>
	<CFIF Find("Errors occurred while attempting to validate",Variables.EtranErrMsg) EQ 0 >
		<CFSET Variables.ErrMsg								= Variables.ErrMsg & Variables.AppMsg 
															& Variables.EtranErrMsg>
	<CFELSE>
		<CFSET Variables.ErrMsg							= Variables.ErrMsg & Variables.AppMsg>
	</CFIF>
	<CFSET Variables.ErrMsgDelimiter =	Chr(13) & Chr(10)>
	<CFSET Variables.ErrMsg							= REReplace(Variables.ErrMsg, Variables.ErrMsgDelimiter, "", "ALL")>
	<CFSET Variables.ErrMsg							= REReplace(Variables.ErrMsg, "<li>", "", "ALL")>
	<CFSET Variables.ErrMsg							= REReplace(Variables.ErrMsg, "</li>", "", "ALL")>
<CFELSE>
	<CFSET Variables.ErrMsg								= Variables.ErrMsg & Variables.AppMsg & CHR(13) & CHR(10) 
														& Variables.EtranLineIndicator
														& Variables.EtranErrMsg>
</CFIF>
<CFSET Variables.AppMsg								= "">
<CFSET Variables.SaveMe								= "No">


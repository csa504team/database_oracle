<!---
AUTHOR:             Sheen Mathew Justin, TMS Inc., for the US Small Business Administration.
DATE:               12/21/2015
DESCRIPTION:        Display splash graphic for application based on input parameters
NOTES:				      None.
INPUT:              6 Parameters below.
OUTPUT:             Splash graphic.
REVISION HISTORY:   08/18/2015, SMJ:	Original implementation.
--->

<!--- Input Parameters --->
<cfparam name = "SystemName"           default = "SBA" />
<cfparam name = "SubSystemName"        default = "Security" />

<cfparam name = "TopLeftBGColor"       default = "##f1f1f1" />
<cfparam name = "BottomRightBGColor"   default = "##205493" />

<cfparam name = "TopLeftTextColor"     default = "##205493" />
<cfparam name = "BottomRightTextColor" default = "##f1f1f1" />

<cfoutput>

	<!--- Set Styling --->
	<style>

		##container {
			border: 2px solid ##333;
			border-radius: 2px;
			height: 120px;
			width: 201px;
			box-shadow: 1px 2px 5px ##333;
			font-size: 16px;
		}

		##triangle-topleft {
		  width: 0;
		  height: 0;
		  border-top: 120px solid #TopLeftBGColor#;
		  border-right: 201px solid transparent;
		}

		##triangle-bottomright {
			height: 120px;
			width: 201px;
			overflow: hidden;
			background-color: #BottomRightBGColor#;
			position: relative;
		}

		##SystemName{
			position: absolute;
			top: 1em;
			left: 1em;
			color: #TopLeftTextColor#;
		}

		##SubSystemName{
			position: absolute;
			bottom: 0;
			right: 0;
			color: #BottomRightTextColor#;
			max-width: 100px;
		}

	</style>

	<!--- HTML --->
	<div id = "container">

		<div id = "triangle-bottomright">

			<div id = "triangle-topleft"></div>

			<div id = "SystemName">
				#SystemName#
			</div>

			<div id = "SubSystemName">
				#SubSystemName#
			</div>

		</div>

	</div>

</cfoutput>

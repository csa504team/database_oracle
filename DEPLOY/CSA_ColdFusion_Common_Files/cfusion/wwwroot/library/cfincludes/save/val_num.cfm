<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfset Variables.FVal			= Replace(Variables.FVal, ",", "", "ALL")>
	<cfif NOT IsNumeric(Variables.FVal)>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not numeric.</li>">
	<cfelse>
		<cfset "Variables.#FNam#"	= Variables.FVal>
	</cfif>
</cfif>

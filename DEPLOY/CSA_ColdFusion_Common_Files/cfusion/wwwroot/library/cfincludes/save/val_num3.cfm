<!---
AUTHOR:				Steve Seaquist
DATE:				12/15/2000
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_NUMERIC(3,0)
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.CVal (cleaned up FVal), Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	01/15/2002, SRS:	Change of methodology: Do everything in the val files, including setting the cleaned-up 
										variable into Variables.#FNam#, building the URLString and setting BlankScreen. 
					12/15/2000, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFSET Variables.CVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFSET Variables.CVal						= Variables.FVal>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFSET Variables.CVal					= Replace(Variables.FVal,	",", "", "ALL")>
		<CFIF NOT IsNumeric(Variables.CVal)>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " must be numeric (decimal point allowed).">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSEIF Variables.CVal LT 0>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot be less than 0.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSEIF Variables.CVal GT 999>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot exceed 999.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSE>
			<CFSET Variables.CVal				= Round(Variables.CVal)>
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.CVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc. 
DATE:				07/06/2011
DESCRIPTION:		Builds Request.SlafShowButtonOverrides based on user's system access rights. 
NOTES:

	Internally, SBA Look-and-Feel's MainNav code uses the "compressed name" (stripped of spaces and hyphens) for lookup, 
	then uses the name in the Show list for display. So in SlafShowButtonOverrides, the "show" key must also be the 
	compressed name, because it too will be used for lookup (of which button to override). In other words, please do NOT 
	add a space to "PostServicing" to make it look nicer in the code. That would cause the button not to be found! 

	We have to do Duplicate (or equivalently, StructCopy) of the Temp variable to guarantee uniqueness in the array. 

	The external system passoff is being done through act_choosefunction for 2 reasons: 
	(1) It writes an audit trail of the user's change of system. 
	(2) It picks up the new location of the system if it has moved, EVEN IF IT HAS CHANGED SERVERS(!!!). 

	Consequently, there's no need to hard-code URLs of the external systems. We use the current URLs from Distributed GLS. 
	If the target URL isn't what you want to go to within the external system, that's a routing issue to be addressed in the 
	external system. GLS doesn't currently have any mechanism to override the URLs specified in the Security System. 

	The standard behavior for going to an external system is to do so in a new window or tab. Therefore, setting external to 
	"Yes" results in implicitly setting target to "_blank". If you have a good reason (and approval) NOT to open a new 
	window, you can specify your own target, which would override "_blank". (In particular, to get the external system to 
	open the same window (the current window), do <cfset Variables.Temp.target = "_top">.) 

INPUT:				Variables.Show, Variables.ShowButtonList, Variables.RequestedSubsystemDir, Variables.RoleParm variables. 
OUTPUT:				Request.SlafShowButtonOverrides. 
REVISION HISTORY:	08/15/2011, SRv:	Changed RequestedSubSystemDir name for Post Servicing to Post.
					08/01/2011, SRS:	Added ability of changesystem hotlinks to log system changes. This necessitated 
										going back to act_choosefunction, but also got rid of the need for DoButtonName.js. 
					07/06/2011, SRv:	Original implementation. 
--->

<!--- Variables.Temp is to include Steve's override for Buttons code. --->
<cfset Request.SlafShowButtonOverrides				= ArrayNew(1)>
<cfif isDefined("Variables.Show")>
	<cfloop list="#Variables.ShowButtonList#" index="i">
		<cfif trim(ucase(i)) NEQ ucase(Variables.RequestedSubsystemDir)>
			<cfswitch expression="#i#">
				<cfcase value="applications">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmELendOrig#">
					<cfset Variables.Temp.show		= "Origination">
					<cfset Variables.Temp.title		= "Opens a new window to Electronic Lending - Origination.">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "Origination")>
				</cfcase>
				<cfcase value="servicing">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmELendServicing#">
					<cfset Variables.Temp.show		= "Servicing">
					<cfset Variables.Temp.title		= "Opens a new window to Electronic Lending - Servicing.">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "Servicing")>
				</cfcase>
				<cfcase value="LANA">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmLana#">
					<cfset Variables.Temp.show		= "LANA">
					<cfset Variables.Temp.title		= "Opens a new window to Loan Associated Names and Addresses (LANA).">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "#i#")>
				</cfcase>
				<cfcase value="Chron">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmCLCS#">
					<cfset Variables.Temp.show		= "Chron">
					<cfset Variables.Temp.title		= "Opens a new window to Centralized Loan Chron System (CLCS).">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "#i#")>
				</cfcase>
				<cfcase value="GPTS">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmGPTS#">
					<cfset Variables.Temp.show		= "GPTS">
					<cfset Variables.Temp.title		= "Opens a new window to Guarantee Purchase Tracking System (GPTS).">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "#i#")>
				</cfcase>
				<cfcase value="Elips">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmElips#">
					<cfset Variables.Temp.show		= "ELIPS">
					<cfset Variables.Temp.title		= "Opens a new window to ELIPS.">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "#i#")>
				</cfcase>
				<cfcase value="Post">
					<cfset Variables.Temp			= StructNew()>
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/cls/act_choosefunction.cfm?#Variables.RoleParmPostServicing#">
					<cfset Variables.Temp.show		= "PostServicing">
					<cfset Variables.Temp.title		= "Opens a new window to Electronic Lending - Post Servicing (ETRAN).">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show			= ListAppend(Variables.Show, "Post Servicing")>
				</cfcase>
			</cfswitch>
		</cfif>
	</cfloop>
</cfif>


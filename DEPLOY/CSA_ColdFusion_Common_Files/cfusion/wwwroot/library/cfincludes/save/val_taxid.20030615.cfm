<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Variables.BusEINSSNInd IS 1>
		<cfif Len(Variables.FVal) IS NOT 11>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# (SSN) must be exactly 11 characters.</li>">
		<cfelseif	(NOT IsNumeric	(Mid(Variables.FVal, 1, 3)))
			OR						(Mid(Variables.FVal, 4, 1) IS NOT "-")
			OR		(NOT IsNumeric	(Mid(Variables.FVal, 5, 2)))
			OR						(Mid(Variables.FVal, 7, 1) IS NOT "-")
			OR		(NOT IsNumeric	(Mid(Variables.FVal, 8, 4)))>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# (SSN) is not in 999-99-9999 format.</li>">
		<cfelse>
			<cfset Variables.FVal		= Replace(Variables.FVal, "-", "", "ALL")>
			<cfset "Variables.#FNam#"	= Variables.FVal>
		</cfif>
	<cfelse>
		<cfif Len(Variables.FVal) IS NOT 10>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# (EIN) must be exactly 10 characters.</li>">
		<cfelseif	(NOT IsNumeric	(Mid(Variables.FVal, 1, 2)))
			OR						(Mid(Variables.FVal, 3, 1) IS NOT "-")
			OR		(NOT IsNumeric	(Mid(Variables.FVal, 4, 7)))>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# (EIN) is not in 99-99999999 format.</li>">
		<cfelse>
			<cfset Variables.FVal		= Replace(Variables.FVal, "-", "", "ALL")>
			<cfset "Variables.#FNam#"	= Variables.FVal>
		</cfif>
	</cfif>
</cfif>

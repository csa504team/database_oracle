<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc. 
DATE:				07/06/2010
DESCRIPTION:		Builds various logic variables used to make MainNav buttons into external system passoffs. 
NOTES:				Must be called before bld_buttonlist, which uses these variables. 
INPUT:				Variables.ArrayUserRoles (context sensitive, trimmed down version of ArrayAllUserRoles, from GLS). 
OUTPUT:				Various logic variables used to make MainNav buttons into external system passoffs. 
REVISION HISTORY:	08/01/2011, SRS:	Added ability of changesystem hotlinks to log system changes. This required saving 
										the highest powered role that triggered the change of system. 
					07/06/2010, SRv:	Original implementation. Include routine to build Access List. 
--->

<!--- Configuration Parameters: --->

<cfset Variables.ChgSysCrypticPassoff					= "Yes"><!--- Using AURIdx is cryptic. Using RoleName is not. --->
<cfset Variables.ExternalSystemAbbrsList				= "CLCS,ElendOrig,ELendServicing,Elips,GPTS,Lana,PostServicing">

<!--- Initializations of defaults: --->

<cfloop index="Idx" list="#Variables.ExternalSystemAbbrsList#">
	<cfset Variables["CanSee"	& Idx]					= "No">
	<cfset Variables["RoleIdx"	& Idx]					= "">
	<cfset Variables["RoleNm"	& Idx]					= "">
	<cfset Variables["RoleParm"	& Idx]					= "">
</cfloop>
<cfset Variables.UserCanSeeSys							= "">
<cfset Variables.ShowButtonList							= "">

<cfif NOT IsDefined("SetHighestPoweredRoleForSys")>		<!--- (Highest power we care about that user has, that is.) --->
	<cffunction name="SetHighestPoweredRoleForSys"		returntype="any"><!--- Actually returntype="void", if that existed. --->
		<cfargument name="SysAbbr"						required="Yes" type="string">
		<cfset Variables["RoleIdx"	& Arguments.SysAbbr]= Idx>
		<cfset Variables["RoleNm"	& Arguments.SysAbbr]= Variables.ChgSysRoleNm>
	</cffunction>
</cfif>

<!--- Accumulate actual data: --->

<cfloop index="Idx" from="1" to="#ArrayLen(Variables.ArrayUserRoles)#">
	<!--- 13 is the column number of the login credential's assurance level (how trusted is this current login): --->
	<cfif Variables.ArrayUserRoles[Idx][13] LTE Variables.OrigLoginLevel>
		<!--- 1 is the column number of the role name itself: --->
		<cfset Variables.ChgSysRoleNm					= Trim(Variables.ArrayUserRoles[Idx][1])>
		<cfswitch expression="#Variables.ChgSysRoleNm#">
			<!--- Alphabetize cases by role name for ease of lookup, even if it doesn't keep systems together: --->
			<cfcase value="CLCSAdmin">
				<cfset Variables.CanSeeCLCS				= "Yes">
				<cfset SetHighestPoweredRoleForSys		("CLCS")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="CLCSRead">
				<cfset Variables.CanSeeCLCS				= "Yes">
				<cfif Len(Variables.RoleNmCLCS) is 0><!--- Lowest power in sys, least override capability. --->
					<cfset SetHighestPoweredRoleForSys	("CLCS")>
				</cfif>
			</cfcase>
			<cfcase value="CLCSUpdate">
				<cfset Variables.CanSeeCLCS				= "Yes">
				<cfif Variables.RoleNmCLCS NEQ "CLCSAdmin"><!--- Middle power in sys, doesn't override higher. --->
					<cfset SetHighestPoweredRoleForSys	("CLCS")>
				</cfif>
			</cfcase>
			<cfcase value="ELIPSRead">
				<cfset Variables.CanSeeElips			= "Yes">
				<cfset SetHighestPoweredRoleForSys		("Elips")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="GPTSMgmtReports">
				<cfset Variables.CanSeeGPTS				= "Yes">
				<cfif Variables.RoleNmGPTS NEQ "GPTSUpdt"><!--- Middle power in sys, doesn't override higher. --->
					<cfset SetHighestPoweredRoleForSys	("GPTS")>
				</cfif>
			</cfcase>
			<cfcase value="GPTSRead">
				<cfset Variables.CanSeeGPTS				= "Yes">
				<cfif Len(Variables.RoleNmGPTS) is 0><!--- Lowest power in sys, least override capability. --->
					<cfset SetHighestPoweredRoleForSys	("GPTS")>
				</cfif>
			</cfcase>
			<cfcase value="GPTSUpdt">
				<cfset Variables.CanSeeGPTS				= "Yes">
				<cfset SetHighestPoweredRoleForSys		("GPTS")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="LANAREAD">
				<cfset Variables.CanSeeLana				= "Yes">
				<cfif Len(Variables.RoleNmLANA) is 0><!--- Lowest power in sys, least override capability. --->
					<cfset SetHighestPoweredRoleForSys	("Lana")>
				</cfif>
			</cfcase>
			<cfcase value="LANAUPDATE">
				<cfset Variables.CanSeeLana				= "Yes">
				<cfset SetHighestPoweredRoleForSys		("Lana")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="LoanGov">
				<cfset Variables.CanSeeELendServicing	= "Yes">
				<cfset SetHighestPoweredRoleForSys		("ELendServicing")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="LoanPostServRead">
				<cfset Variables.CanSeePostServicing	= "Yes">
				<cfset SetHighestPoweredRoleForSys		("PostServicing")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="LoanPrtUpdt">
				<cfset Variables.CanSeeELendServicing	= "Yes">
				<cfif Len(Variables.RoleNmELendServicing) is 0><!--- Lowest power in sys, least override capability. --->
					<cfset SetHighestPoweredRoleForSys	("ELendServicing")>
				</cfif>
			</cfcase>
			<cfcase value="OrigAdmin">
				<cfset Variables.CanSeeElendOrig		= "Yes">
				<cfset SetHighestPoweredRoleForSys		("ELendOrig")><!--- Highest power in sys, unconditional. --->
			</cfcase>
			<cfcase value="OrigGov">
				<cfset Variables.CanSeeElendOrig		= "Yes">
				<cfif Variables.RoleNmELendOrig NEQ "OrigAdmin"><!--- Middle power in sys, doesn't override higher. --->
					<cfset SetHighestPoweredRoleForSys	("ELendOrig")>
				</cfif>
			</cfcase>
			<cfcase value="OrigPrtUpdt">
				<cfset Variables.CanSeeElendOrig		= "Yes">
				<cfif Len(Variables.RoleNmELendOrig) is 0><!--- Lowest power in sys, least override capability. --->
					<cfset SetHighestPoweredRoleForSys	("ELendOrig")>
				</cfif>
			</cfcase>
		</cfswitch>
	</cfif>
</cfloop>
<cfif Variables.CanSeeElendOrig>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Origination")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"applications")>
</cfif>
<cfif Variables.CanSeeElendServicing>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Servicing")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"servicing")>
</cfif>
<cfif Variables.CanSeeLana>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"LANA")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"LANA")>
</cfif>
<cfif Variables.CanSeeCLCS>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Chron")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"Chron")>
</cfif>
<cfif Variables.CanSeeGPTS>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"GPTS")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"GPTS")>
</cfif>
<cfif Variables.CanSeeElips>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"ELIPS")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"ELIPS")>
</cfif>
<cfif Variables.CanSeePostServicing>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Post Servicing")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"PostServicing")>
</cfif>

<cfloop index="Idx" list="#Variables.ExternalSystemAbbrsList#">
	<cfif Variables.ChgSysCrypticPassoff><!--- Configuration Parameter, above. --->
		<cfset Variables["RoleParm"&Idx]				= "AURIdx="		& URLEncodedFormat(Variables["RoleIdx"&Idx])>
	<cfelse>
		<cfset Variables["RoleParm"&Idx]				= "RoleName="	& URLEncodedFormat(Variables["RoleNm"&Idx])>
	</cfif>
</cfloop>


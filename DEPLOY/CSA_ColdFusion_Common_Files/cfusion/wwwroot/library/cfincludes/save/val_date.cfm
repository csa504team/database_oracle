<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif IsDate(Variables.FVal)>
		<cfset Variables.FVal		= DateFormat(Variables.FVal, "MM/DD/YYYY")><!--- Standardize format. --->
		<cfset "Variables.#FNam#"	= Variables.FVal>
	<cfelse>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not in a recognizable date format.</li>">
	</cfif>
</cfif>

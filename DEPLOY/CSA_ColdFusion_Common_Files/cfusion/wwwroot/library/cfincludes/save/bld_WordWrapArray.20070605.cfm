<!---
AUTHOR:				Steve Seaquist
DATE:				06/05/2007
DESCRIPTION:		Breaks a string on whitespace boundaries, like word wrap in a text editor. 
NOTES:

	This routine was originally written to allow inserting and updating Centralized Loan Chron System (CLCS) 
	chron comments, without caring whether the original text came from the mainframe or the Web. So CLCS serves 
	as a good example of when you might want to use this routine. 

	On the mainframe, chron comment text is stored in the ACTIVITY-COMMENT field of the COLL-ACTIVITY-REC. The 
	COLL-ACTIVITY-REC is also known as the "chron record", the same chron from which we get the name of CLCS. 
	The ACTIVITY-COMMENT field's COBOL format is PIC X(30). In relational database terms, PIC X(30) can be viewed 
	as equivalent to char(30), because trailing spaces are included in the field. When the monthly mainframe 
	extract occurs, new chron comments get stored in separate rows of LoanChronCmntTbl, TRIMMED OF TRAILING 
	SPACES. The following is an example from the development database: 

	LoanChronSeqNmb		LoanChronCmntSeqNmb		LoanChronCmntTxt (double-quotes added to show actual size)
	===============		===================		==========================================================
	218					1						"CALLED SEC OF STATE FOR CORP"
	218					2						"STATUS. TLKED TO TWANA. CORP"
	218					3						"IN GOOD STANDING. 9004 N. MAY,"
	218					4						"OKC, OK 73120. SEC OF STATE"
	218					5						"TEL # 521-3911."
	218					6						"This is a test."

	Because there are no trailing spaces on the incoming data, the rows have to be concatenated with an added space: 

		"CALLED SEC OF STATE FOR CORP STATUS. TLKED TO TWANA. CORP IN GOOD STANDING. 9004 N. MAY, OKC, OK 73120. 
		SEC OF STATE TEL # 521-3911. This is a test."

	Otherwise, they would get jammed together. This is what it would look like WITHOUT an added space. (Note: 
	This is an example of what NOT to do with LoanChronCmntTbl data!) 

		"CALLED SEC OF STATE FOR CORPSTATUS. TLKED TO TWANA. CORPIN GOOD STANDING. 9004 N. MAY,OKC, OK 73120. 
		SEC OF STATETEL # 521-3911.This is a test."

	So that's how the problem of mainframe data without trailing spaces was solved. Now the question became, when 
	the user stores a new chron via the ***Web***, how do we write the rows such that this added space will not \
	mess up in the other direction? That is, how do we avoid accidentally adding a space in the middle of a word? 

	That's where this routine comes in. It breaks up Variables.WWALongText into an array, such that the break 
	always occurs at a whitespace boundary, and without including the whitespace itself. If there isn't any 
	whitespace boundary, the long substring not containing any whitespace will have to be truncated, but that 
	can't be helped. 

	You can go to this page in a browser ("self-test mode") to see an example of what it does. 

INPUT:				Variables.WWALongText (which will be destroyed in the conversion). 
					Variables.WWAPreserveLinefeeds (used to replicate manual line breaks entered by the user). 
					Variables.WWATargetMaxLength (size of the target database column), if not varchar(255). 
OUTPUT:				Variables.WordWrapArray and Variables.WordWrapArrayLen. 
REVISION HISTORY:	06/05/2007, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfparam name="Variables.WWAPreserveLinefeeds"			default="No">
<cfparam name="Variables.WWATargetMaxLength"			default="255">

<!--- Initializations: --->

<cfif CGI.Script_Name IS "/library/cfincludes/bld_WordWrapArray.cfm"><!--- Self-test mode: --->
	<cfset Variables.WWAPreserveLinefeeds				= "Yes">
	<cfset Variables.WWATargetMaxLength					= 15>
	<cfset Variables.WWALongText						= "Now is the time for all good men to come to the aid of "
														& "their party. ThisIsAVeryLongSubstringThatUnfortunately "
														& "wouldn't fit into the (self-test mode) varchar("
														& Variables.WWATargetMaxLength
														& ") column. "
														& "This line contains a few #Chr(10)# manual line #Chr(10)# "
														& "breaks, like those requested #Chr(10)# by #Chr(10)# a "
														& "user hitting the #Chr(10)# Return key in a textarea box. "
														& "Also, here's a long substring of whitespace characters "
														& "that should be completely ignored: "
														& RepeatString("#Chr(9)# #Chr(13)# ", 10)
														& "Let's hope that they were. ">
	<cfset Variables.TempString							= Replace(Variables.WWALongText, Chr(9), "&lt;HT&gt;", "ALL")>
	<cfset Variables.TempString							= Replace(Variables.TempString, Chr(10), "&lt;LF&gt;", "ALL")>
	<cfset Variables.TempString							= Replace(Variables.TempString, Chr(13), "&lt;CR&gt;", "ALL")>
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>bld_WordWrapArray, Self-Test Mode</title>
<link href="/library/css/sba.css" rel="stylesheet" type="text/css">
</head>
<body class="normal pad15">

<h3 align="center">bld_WordWrapArray, Self-Test Mode</h3>

Variables.WWAPreserveLinefeeds is "#Variables.WWAPreserveLinefeeds#".<br>
<br>
Variables.WWATargetMaxLength is #Variables.WWATargetMaxLength#, 
indicating that the target database column's datatype is varchar(#Variables.WWATargetMaxLength#).<br>
<br>
Variables.WWALongText (long text variable to be broken up into chunks for the target database column): <br>
<blockquote>
&quot;#Variables.TempString#&quot;
</blockquote>
</cfoutput>
</cfif>

<cfset Variables.WordWrapArray							= ArrayNew(1)><!--- WordWrapArrayLen set after conversion. --->
<cfset Variables.WWATargetMaxLengthPlus1				= Variables.WWATargetMaxLength + 1>
<cfset Variables.WWAIdx									= 0><!--- Short for WordWrapArrayIdx. --->

<!--- The big loop will unnecessarily break up the last row if we don't tack on a trailing space: --->

<cfset Variables.WWALongText							= Trim(Variables.WWALongText) & " ">

<!--- Break up Variables.WWALongText, however long it is: --->

<cfloop condition="Len(Variables.WWALongText) GT 0">
	<cfset Variables.WWALastNonSpace					= 0><!--- That is, last non-space before last space. --->
	<cfset Variables.WWALastSpace						= 0>
	<cfloop index="MSACharNmb" from="#Variables.WWATargetMaxLengthPlus1#" step="-1" to="1">
		<cfswitch expression="#Asc(Mid(Variables.WWALongText,MSACharNmb,1))#">
		<cfcase value="9,13,32"><!--- HT, CR and SP = whitespace that could be entered in a textarea. --->
			<cfif Variables.WWALastSpace IS 0>
				<cfset Variables.WWALastSpace			= MSACharNmb>
			</cfif>
		</cfcase>
		<cfcase value="10">
			<cfif Variables.WWAPreserveLinefeeds>
				<!--- If we're preserving linefeeds, do what defaultcase does: --->
				<cfif Variables.WWALastSpace GT 0>
					<cfset Variables.WWALastNonSpace	= MSACharNmb>
					<cfbreak>
				</cfif>
			<cfelse>
				<!--- If we're not preserving linefeeds, do what case value="9,13,32" does: --->
				<cfif Variables.WWALastSpace IS 0>
					<cfset Variables.WWALastSpace		= MSACharNmb>
				</cfif>
			</cfif>
		</cfcase>
		<cfdefaultcase>
			<cfif Variables.WWALastSpace GT 0>
				<cfset Variables.WWALastNonSpace		= MSACharNmb>
				<cfbreak>
			</cfif>
		</cfdefaultcase>
		</cfswitch>
	</cfloop>
	<cfif Variables.WWALastSpace GT 0>
		<cfif Variables.WWALastNonSpace GT 0>
			<cfset Variables.WWAIdx						= Variables.WWAIdx + 1>
			<cfset Variables.WWATempString				= Left(Variables.WWALongText, Variables.WWALastNonSpace)>
			<cfif Variables.WWAPreserveLinefeeds>
				<cfset Variables.WordWrapArray[WWAIdx]	= Variables.WWATempString>
			<cfelse>
				<!--- This is the only way to get rid of linefeeds in the middle of the string: --->
				<cfset Variables.WordWrapArray[WWAIdx]	= Replace(Variables.WWATempString, Chr(10), " ", "ALL")>
			</cfif>
		</cfif>
		<cfloop index="MSACharNmb" from="#Variables.WWALastSpace#" to="#Len(Variables.WWALongText)#">
			<cfswitch expression="#Asc(Mid(Variables.WWALongText,MSACharNmb,1))#">
			<cfcase value="9,13,32"><!--- HT, CR and SP = whitespace that could be entered in a textarea. --->
				<cfset Variables.WWALastSpace			= MSACharNmb>
			</cfcase>
			<cfcase value="10">
				<cfif Variables.WWAPreserveLinefeeds>
					<!--- If we're preserving linefeeds, do what defaultcase does: --->
					<cfbreak>
				<cfelse>
					<!--- If we're not preserving linefeeds, do what case value="9,13,32" does: --->
					<cfset Variables.WWALastSpace		= MSACharNmb>
				</cfif>
			</cfcase>
			<cfdefaultcase>
				<cfbreak>
			</cfdefaultcase>
			</cfswitch>
		</cfloop>
	<cfelse>
		<cfset Variables.WWALastSpace					= Variables.WWATargetMaxLength>
		<cfset Variables.WWAIdx							= Variables.WWAIdx + 1>
		<cfset Variables.WWATempString					= Left(Variables.WWALongText, Variables.WWALastSpace)>
		<cfif Variables.WWAPreserveLinefeeds>
			<cfset Variables.WordWrapArray[WWAIdx]		= Variables.WWATempString>
		<cfelse>
			<!--- This is the only way to get rid of linefeeds in the middle of the string: --->
			<cfset Variables.WordWrapArray[WWAIdx]		= Replace(Variables.WWATempString, Chr(10), " ", "ALL")>
		</cfif>
	</cfif>
	<cfset Variables.WWALongText						= RemoveChars(Variables.WWALongText,1,Variables.WWALastSpace)>
</cfloop>
<cfset Variables.WordWrapArrayLen						= Variables.WWAIdx>

<cfif CGI.Script_Name IS "/library/cfincludes/bld_WordWrapArray.cfm"><!--- Self-test mode: --->
	<cfoutput>
Variables.WordWrapArray contains #Variables.WordWrapArrayLen# row(s), as follows:<br>
<ol></cfoutput>
	<cfloop index="i" from="1" to="#Variables.WordWrapArrayLen#">
		<cfset Variables.TempString						= Variables.WordWrapArray[i]>
		<cfset Variables.TempLen						= Len(Variables.TempString)>
		<!---
		To recreate line breaks in plain HTML when using the WWAPreserveLinefeeds feature, 
		use Replace(Variables.TempString, Chr(10), "<br>", "ALL") instead of the following: 
		--->
		<cfset Variables.TempString						= Replace(Variables.TempString, Chr (9), "&lt;HT&gt;", "ALL")>
		<cfset Variables.TempString						= Replace(Variables.TempString, Chr(10), "&lt;LF&gt;", "ALL")>
		<cfset Variables.TempString						= Replace(Variables.TempString, Chr(13), "&lt;CR&gt;", "ALL")>
		<cfoutput>
	<li>&quot;#Variables.TempString#&quot; - #Variables.TempLen# character(s)</li></cfoutput>
	</cfloop>
	<cfoutput>
</ol>

</body>
</html>
</cfoutput>
</cfif>

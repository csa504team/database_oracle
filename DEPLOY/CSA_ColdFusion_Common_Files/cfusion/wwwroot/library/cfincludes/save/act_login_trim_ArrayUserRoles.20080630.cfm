<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Based on current values in Variables.CLS.IMUserTbl, builds Variables.ArrayUserRoles. 
NOTES: 

	This code assumes that Variables.CLS is defined and contains the read-only copy of ALL of the user's roles, which 
	was formerly kept in ArrayAllUserRoles. It doesn't really matter whether this Variables.CLS structure was obtained 
	by calling the Login Web Service, or by Duplicate(Session.CLS), or whatever. In particular, we do **NOT** assume 
	that act_login_establish_variables_vars was run. If it was, there's already a copy of Variables.ArrayUserRoles that 
	we will throw away. If there isn't, we will build it. Hence the start with ArrayNew(2). 

	Although Session.CLS.ArrayUserRoles is now sacrosanct, read-only. Other data in Session.CLS, such as partner location 
	or office code in Session.CLS.IMUserTbl, are not similarly off-limits. 

INPUT:				Variables.CLS.IMUserTbl, Variables.CLS.ArrayUserRoles. 
OUTPUT:				Variables.ArrayUserRoles 
REVISION HISTORY:	06/30/2008, SRS:	Original implementation, created by cannibalizing code in /cls/Application.cfm. 
										Got rid of ArrayAllUserRoles in favor of keeping Session.CLS.ArrayUserRoles as 
										the read-only copy that never gets altered. This cuts down biggest overhead in 
										the Session scope (multiple copies of ArrayUserRoles) from 4 copies down to 2. 
--->

<cfif Variables.CLS.CLSAuthorized AND IsDefined("Variables.ArrayAllUserRoles")>
	<cfset Variables.TempLen						= ArrayLen(Variables.ArrayAllUserRoles)>
	<cfset Variables.ArrayUserRoles					= Variables.ArrayAllUserRoles>
	<cfset Variables.ListToDelete					= "">
	<cfloop index="RoleIdx" from="1" to="#ArrayLen(Variables.ArrayUserRoles)#">
		<cfset Variables.FndRqdDataTypCd			= "No">
		<cfif Variables.ArrayUserRoles[RoleIdx][18] IS "O">
			<!--- This role requires an office code, check if the user role is assigned to the Session.SBAOfcCd --->
			<cfloop index="OfcIdx" from="1" to="#ArrayLen(Variables.ArrayUserRoleOfc)#">
				<cfif Variables.ArrayUserRoles[RoleIdx][1] EQ Variables.ArrayUserRoleOfc[OfcIdx][2]>
					<cfif Variables.SBAOfcCd EQ Variables.ArrayUserRoleOfc[OfcIdx][3]>
						<cfset Variables.FndRqdDataTypCd				= "Yes">
					</cfif>
				</cfif>
			</cfloop>
		<cfelseif Variables.ArrayUserRoles[RoleIdx][18] IS "L">
			<cfloop index="LocIdx" from="1" to="#ArrayLen(Variables.ArrayUserRoleLoc)#">
				<cfif Variables.ArrayUserRoles[RoleIdx][1] EQ Variables.ArrayUserRoleLoc[LocIdx][2]>
					<cfif Variables.LocId EQ Variables.ArrayUserRoleLoc[LocIdx][3]>
						<cfset Variables.FndRqdDataTypCd				= "Yes">
					</cfif>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset Variables.FndRqdDataTypCd							= "Yes">
		</cfif>
		<cfif Not Variables.FndRqdDataTypCd>
			<cfset Variables.ListToDelete	= ListAppend(Variables.ListToDelete,Variables.ArrayUserRoles[RoleIdx][1])>
		</cfif>
	</cfloop>
	<cfloop index="AllRoleIdx" from="1" to="#Variables.TempLen#">
		<cfloop index="RoleIdx" from="1" to="#ArrayLen(Variables.ArrayUserRoles)#">
			<cfif ListFind(Variables.ListToDelete,Variables.ArrayUserRoles[RoleIdx][1])>
				<cfset ArrayDeleteAt(Variables.ArrayUserRoles,RoleIdx)>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfloop>
	<cflock scope="Session" type="Exclusive" timeout="30">
		<cfset Session.CLS.ArrayUserRoles			= Variables.ArrayUserRoles>
		<cfset Session.CLS.ArrayAllUserRoles		= Variables.ArrayAllUserRoles>
	</cflock>
</cfif>

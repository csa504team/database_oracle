<!---
AUTHOR:				Steve Seaquist
DATE:				03/02/2007
DESCRIPTION:		Defines Request.CurrIPxxx variables (logical names to control temporarily IP-restricted code). 
NOTES:				In general, code should be restricted by GLS Role. These logical names are for those occassions 
					where defining a GLS Role would be overkill, such as in Developer Utilities. Also, all of these 
					IP addresses are subject to change, hence the prefix "Curr". But then, that's exactly why we need 
					logical names. By coding to these names, rather than the actual IP address constants, we can 
					maintain the values in only one place. These names are normally tested against CGI.Remote_Addr. 
INPUT:				None. 
OUTPUT:				Request.CurrIPxxx variables. 
REVISION HISTORY:	04/30,2008, SRS:	Added Matt Forman. 
					03/21,2008, SRS:	Changes to support some folks' leaving and others' returning to 4th floor. 
					05/17-21,2007, SRS:	Changes to support 5 folks' moves to Concourse level of Central Office. 
					03/02/2007, SRS:	Original implementation.
--->

<cfset Request.CurrIPChuda							= "165.110.40.101">
<cfset Request.CurrIPDileep							= "no longer here">	<!--- On 4th floor, used to be 165.110.40.125, then 165.110.95.14. --->
<cfset Request.CurrIPIanClark						= "165.110.40.117">	<!--- On Concourse, used to be 165.110.95.12. --->
<cfset Request.CurrIPJameGeorge						= "no longer here">	<!--- On 4th floor, used to be 165.110.40.77, then 165.110.95.131. --->
<cfset Request.CurrIPMattForman						= "165.110.40.158">
<cfset Request.CurrIPRonWhalen						= "165.110.40.60">
<cfset Request.CurrIPSheriMcConville				= "165.110.40.106">	<!--- On Concourse, used to be 165.110.95.136. --->
<cfset Request.CurrIPSteveSeaquist					= "165.110.40.166">	<!--- On Concourse, used to be 165.110.95.206. --->

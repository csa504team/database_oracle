<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Reformats Variables.Temp from database format to human format
NOTES:				None
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	12/15/2000, SRS:	Added this comment header. 
--->
<CFIF Len(Variables.Temp) GT 0><!--- Leading minus if negative, blank when zero: --->
	<CFIF IsNumeric(Variables.Temp)>
		<CFIF		Variables.Temp LT 0>	<CFSET	Variables.Temp	= "-" &	DollarFormat( -	Variables.Temp)>
		<CFELSEIF	Variables.Temp GT 0>	<CFSET	Variables.Temp	=		DollarFormat(	Variables.Temp)>
		<CFELSE>							<CFSET	Variables.Temp	= "">
		</CFIF>
	<CFELSE>								<CFSET	Variables.Temp	= "(not numeric)">
	<!--- That won't look good on a display, but it will help us to diagnose problems. --->
	</CFIF>
</CFIF>

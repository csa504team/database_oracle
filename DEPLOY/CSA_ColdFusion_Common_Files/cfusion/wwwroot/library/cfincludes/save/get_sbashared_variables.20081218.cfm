<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel, cf_setrequesttimeout, etc. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm right after <cfapplication>, it will include this file for you. 
	Therefore, you would normally NOT include this file directly. It has been separated out of get_sbalookandfeel_variables 
	for use in Web Services, apps without <cfapplication> (and hence don't have Session variables) and other situations where 
	you would not be including get_sbalookandfeel_variables. However, because the output variables will most often be used in 
	the SBA look-and-feel context, they retain the "Slaf" prefix. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. Set RequestTimeout from URL.RequestTimeout for upward compatibility. 
					(Can be overridden later by caller.) 
REVISION HISTORY:	12/04/2008, SRS:	Adapted to change of Jaguar package name from "gov.sba.CLS" to "gov.sba.security". 
					11/05-21/2008, SRS:	Minor cleanup. Tested for presence of Jaguar by actually trying to call it inside a 
										cftry/cfcatch block. Adapted to new database tables and columns. 
								IAC:	Modified query based on database changes table to allow developers to access GLS. 
					10/24/2008, SRS:	Adapted to new (fake) server-related queries in bld_ServerCachedQueries. 
					09/16/2008, SRS:	Request.SlafURL variables (all of which are associated with and inferrd from the URL). 
										Also added support for "colorado" (new name of wocs41), without removing support for 
										wocs41. This allows the renaming to occur anytime. (We can remove wocs41 afterwards.) 
					07/03/2008, SRS:	Defined Request.SlafLoginServerURL for use in Distributed GLS. 
					11/20/2007, SRS:	Configured newdanube and newyukon as potential server names. 
					11/20/2007, SRS:	Added new production server volga (runs under iweb). 
					10/23/2007, SRS:	Added 206.66.57.* ("the DMZ") as one of valid SBA IP address criteria. Allowed eweb 
										and enile callers to call the Login Web Service(s) without hitting the https problem. 
					10/22/2007, DKC:	Added 199.171.55.* ("the Class C") as one of valid SBA IP address criteria. 
					10/20/2007, SRS:	Added SlafLoginWSDL to allow logging in via Web Services. 
					09/28/2007, SRS:	Added SlafDefaultNAICSYrNmb for use with 2007 NAICS conversion. Also added new logic 
										to prevent existing sessions from crashing because they never defined a Server variable. 
										Also added SlafMustLoginURL and SlafVersionRevision. 
					03/21/2007, SRS:	Added SlafLoginURL, for use with "Distributed GLS". 
					07/25/2006, SRS:	Added support for SlafIPAddrIsNAT (10/8, 172.16/12, 192.168/16). Required by PRO-Net, 
										which allows all SBA users with NAT IP addresses to search for non-active firms. 
										Corrected the logic for localhost to be the entire Class A range beginning with 127. 
					07/17/2006, SRS:	Moved Variables.PageName code from get_sbalookandfeel_variables, so that we can set 
										a new variable, namely, Request.SlafCFC. 
					07/03/2006, SRS:	Just in case we ever get IPv6 or no IP address in CGI.Remote_Addr, added a check to 
										make sure that ArrayLen(Variables.SlafArrayRemote_Addr) is 4. This means that all 
										IPv6 and absent Remote_Addr addresses will be treated as NOT being SBA. We will 
										definitely have to modify this code after the conversion to IPv6 infrastructure. 
					06/26/2006, SRS:	Added Request.SlafIPAddrIsSBA code. (Essentially, just copied from PRO-Net and 
										stripped of PRO-Net-specific code.) 
					01/31/2006, SRS:	Original implementation. 
--->

<!--- Global, SBA-Wide Configuration Parameters: --->

<cfset Request.SlafDefaultJaguarPort						= 9000><!--- Default host set below. --->
<cfset Request.SlafDefaultNAICSYrNmb						= "2007">
<cfset Request.SlafDefaultNAICSYrNmbCurr					= Request.SlafDefaultNAICSYrNmb>
<cfset Request.SlafDefaultNAICSYrNmbPrev					= "2002">

<!--- Initializations --->

<cfset		   Variables.SbaSharedVariablesSelfTestMode		= (CGI.Script_Name IS "/library/cfincludes/get_sbashared_variables.cfm")>
<cfparam name="Variables.SbaSharedVariablesForceRebuild"	default="No"><!--- Allows rebuild even if not self test. --->
<cfif Variables.SbaSharedVariablesSelfTestMode>
	<cfset Variables.SbaSharedVariablesForceRebuild			= "Yes">
</cfif>

<cfset StructDelete(Request, "Gssv")><!--- Make sure Request.Gssv is defined ONLY if we copied it from Server struct. --->
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif	(NOT Variables.SbaSharedVariablesForceRebuild)
		AND	IsDefined("Server.SlafGssvStruct")
		AND	IsStruct  (Server.SlafGssvStruct)><!--- "Gssv" = "get_sbashared_variables" --->
		<cfset Request.Gssv									= Duplicate(Server.SlafGssvStruct)>
	</cfif>
	<cfif IsDefined("Server.SlafServerInstanceName")>
		<cfset Request.SlafServerInstanceName				= Server.SlafServerInstanceName>
	</cfif>
	<cfif IsDefined("Server.SlafServerName")>
		<cfset Request.SlafServerName						= Server.SlafServerName>
	</cfif>
	<cfset Request.SlafServerOSName							= Trim(Server.OS.Name)>
	<cfset Request.SlafVersionList							= Server.ColdFusion.ProductVersion><!--- e.g.: "7,0,2,142559" --->
</cflock>
<cfset Request.SlafVersionMajor								= ListGetAt(Request.SlafVersionList, 1)><!--- "7" --->
<cfset Request.SlafVersionMinor								= ListGetAt(Request.SlafVersionList, 2)><!--- "0" --->
<cfset Request.SlafVersionRevision							= ListGetAt(Request.SlafVersionList, 3)><!--- "2" --->
<cfset Request.SlafVersion									= Request.SlafVersionMajor
															& "."
															& Request.SlafVersionMinor>
<cfif Request.SlafVersionRevision IS NOT "0"><!--- "8.0.0" should be displayed as only "8.0" --->
	<cfset Request.SlafVersion								= Request.SlafVersion
															& "."
															& Request.SlafVersionRevision>
</cfif>
<cfif NOT IsDefined("Request.Gssv")>
	<cfset Request.Gssv										= StructNew()>
	<cfset Variables.GssvUsesSybase							= "No"><!--- Save Sybase code for quick fallback: --->
	<cfif (NOT IsDefined("Request.SlafServerName")) OR (NOT IsDefined("Request.SlafServerInstanceName"))>
		<cfinclude template="get_actual_server_name.cfm"><!--- Probably safe to assume same directory. --->
	</cfif>
	<cfif NOT IsDefined("NewSBAJavaObject")>
		<cfinclude template="/library/udf/bld_JavaUDFs.cfm">
	</cfif>
	<cfif IsDefined("Variables.ErrMsg")>
		<cfset Variables.SeeIfThisServerHasJaguarErrMsg		= Variables.ErrMsg>
	</cfif>
	<cfif IsDefined("Variables.TxnErr")>
		<cfset Variables.SeeIfThisServerHasJaguarTxnErr		= Variables.TxnErr>
	</cfif>
	<cfset Variables.ErrMsg									= "">
	<cfset Variables.TxnErr									= "No">
	<cfset Request.Gssv.SlafServerHasJaguar					= "Yes">
	<!--- "Authorization" is enough, but be EXTRA sure we can get to "security.Authorization": --->
	<cfset Variables.SeeIfThisServerHasJaguar				= NewSBAJavaObject("security.Authorization")>
	<cfif Variables.TxnErr OR (Len(Variables.ErrMsg) GT 0)>
		<cfset Request.Gssv.SlafServerHasJaguar				= "No">
		<cfset Request.Gssv.SlafServerHasJaguarErrMsg		= Variables.ErrMsg>
		<cfset Request.Gssv.SlafServerHasJaguarTxnErr		= Variables.TxnErr>
	<cfelse>
		<cfif IsDefined("Variables.SeeIfThisServerHasJaguarErrMsg")>
			<cfset Variables.ErrMsg							= Variables.SeeIfThisServerHasJaguarErrMsg>
		</cfif>
		<cfif IsDefined("Variables.SeeIfThisServerHasJaguarTxnErr")>
			<cfset Variables.TxnErr							= Variables.SeeIfThisServerHasJaguarTxnErr>
		</cfif>
		<cfset Variables.SeeIfThisServerHasJaguarErrMsg		= JavaCast("null", "")><!--- Discard testing object. --->
		<cfset Variables.SeeIfThisServerHasJaguarTxnErr		= JavaCast("null", "")><!--- Discard testing object. --->
	</cfif>
	<cfset Variables.SeeIfThisServerHasJaguar				= JavaCast("null", "")><!--- Discard testing object. --->
	<cfif Request.Gssv.SlafServerHasJaguar>
		<cfset Request.SlafServerName4SrvrTbls				= Request.SlafServerName>
		<cfset Request.SlafServerInstanceName4SrvrTbls		= Request.SlafServerInstanceName>
	<cfelse><!--- Offline testing from a PC: --->
		<cfset Request.SlafServerName4SrvrTbls				= "tiber">		<!--- Treat same as development. --->
		<cfset Request.SlafServerInstanceName4SrvrTbls		= "instance1">	<!--- Treat same as development. --->
	</cfif>
	<cftry><!--- There doesn't exist a SrvrSelTSP identifier for this: --->
		<cfquery name="GssvGetServers" datasource="oracle_webanalytic_publicread" dbtype="Oracle80">
		select		distinct
					s.InstanceId,
					case s.CLSInd
					when 'Y'	then						'Yes'
					else									'No'
					end										as GLSInd,
					case s.LognInd
					when 'Y'	then						'Yes'
					else									'No'
					end										as LognInd,
					s.EnvCd,
					s.SrvrGrpTxt,
					e.EnvTypCd,
					e.LognURLTxt<cfif Variables.GssvUsesSybase>
		from						SrvrTbl					s
					left outer join	EnvTbl					e on (s.EnvCd = e.EnvCd)<cfelse><!--- else = Oracle, the default: --->
		from						SBAREF.SrvrTbl			s
					left outer join	SBAREF.EnvTbl			e on (s.EnvCd = e.EnvCd)</cfif>
		where		s.SrvrNm								= '#Request.SlafServerName4SrvrTbls#'
		and			s.InstanceNm							= '#Request.SlafServerInstanceName4SrvrTbls#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,s.SrvrStrtDt,getdate())>= 0)
		and			(	(			 s.SrvrEndDt			is null)
					or	(datediff(dd,s.SrvrEndDt, getdate())<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	s.SrvrStrtDt)			>= 0)
		and			(	(			s.SrvrEndDt				is null)
					or	((sysdate -	s.SrvrEndDt)			<= 0)
					)</cfif>
		</cfquery>
		<cfcatch type="Any">
			<cfoutput>INTERNAL ERROR. Unable to establish the server environment. 
The following information may help: #CFCatch.Message# #CFCatch.Detail#</cfoutput>
			<cfabort>
		</cfcatch>
	</cftry>
	<cfif GssvGetServers.RecordCount IS 0>
		<cfoutput>INTERNAL ERROR. Unable to establish the server environment. (Nothing found.) </cfoutput>
		<cfabort>
	</cfif>
	<!--- Values taken directly from the database (or Java): --->
	<cfset Request.Gssv.SlafDevTestProd						= Trim(	GssvGetServers.EnvTypCd)>
	<cfset Request.Gssv.SlafLoginURL						=		GssvGetServers.LognURLTxt>
	<cfset Request.Gssv.SlafServerEnvironment				= Trim(	GssvGetServers.EnvCd)>
	<cfset Request.Gssv.SlafServerGroup						=		GssvGetServers.SrvrGrpTxt>
	<cfset Request.Gssv.SlafServerHasTrueGLS				=		GssvGetServers.CLSInd>
	<cfset Request.Gssv.SlafServerInstanceId				=		GssvGetServers.InstanceId>
	<cfset Request.Gssv.SlafServerInstanceLogn				=		GssvGetServers.LognInd>
	<cfset Request.Gssv.SlafServerInstanceName				= Request.SlafServerInstanceName>
	<cfset Request.Gssv.SlafServerInstanceName4SrvrTbls		= Request.SlafServerInstanceName4SrvrTbls>
	<cfset Request.Gssv.SlafServerName						= Request.SlafServerName>
	<cfset Request.Gssv.SlafServerName4SrvrTbls				= Request.SlafServerName4SrvrTbls>
	<!--- Second pass through the database: --->
	<cftry><!--- There don't exist SrvrSelTSP or SrvrCacheQrySelTSP identifiers for this: --->
		<cfquery name="GssvGetHosts" datasource="oracle_webanalytic_publicread" dbtype="Oracle80">
		select		distinct
					SrvrNm<cfif Variables.GssvUsesSybase>
		from		SrvrTbl<cfelse>
		from		SBAREF.SrvrTbl</cfif>
		where		SrvrGrpTxt								= '#Request.Gssv.SlafServerGroup#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,SrvrStrtDt,getdate())	>= 0)
		and			(	(			 SrvrEndDt				is null)
					or	(datediff(dd,SrvrEndDt, getdate())	<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	SrvrStrtDt)				>= 0)
		and			(	(			SrvrEndDt				is null)
					or	((sysdate -	SrvrEndDt)				<= 0)
					)</cfif>
		order by	SrvrNm
		</cfquery>
		<cfquery name="GssvGetScqs" datasource="oracle_webanalytic_publicread" dbtype="Oracle80">
		select		distinct
					case CacheQryCd
					when 'sbaref'	then					1
					else									2
					end										as SbarefFirst,
					CacheQryCd<cfif Variables.GssvUsesSybase>
		from		SrvrCacheQryTbl<cfelse>
		from		SBAREF.SrvrCacheQryTbl</cfif>
		where		SrvrNm									= '#Request.SlafServerName4SrvrTbls#'
		and			InstanceNm								= '#Request.SlafServerInstanceName4SrvrTbls#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,SrvrCacheQryStrtDt,getdate())	>= 0)
		and			(	(			 SrvrCacheQryEndDt				is null)
					or	(datediff(dd,SrvrCacheQryEndDt, getdate())	<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	SrvrCacheQryStrtDt)				>= 0)
		and			(	(			SrvrCacheQryEndDt				is null)
					or	((sysdate -	SrvrCacheQryEndDt)				<= 0)
					)</cfif>
		order by	SbarefFirst,
					CacheQryCd
		</cfquery>
		<cfcatch type="Any">
			<cfoutput>INTERNAL ERROR. Unable to establish the server environment. 
The following information may help: #CFCatch.Message# #CFCatch.Detail#</cfoutput>
			<cfabort>
		</cfcatch>
	</cftry>
	<cfset Request.Gssv.SlafLoginHostList					= Replace(ValueList(GssvGetHosts.SrvrNm),	" ","","ALL")>
	<cfset Request.Gssv.SlafServerCachedQueryGroups			= Replace(ValueList(GssvGetScqs.CacheQryCd)," ","","ALL")>
	<!--- Inferred values: --->
	<cfswitch expression="#Request.Gssv.SlafDevTestProd#">
	<cfcase value="Dev">									<cfset Request.Gssv.SlafDevTestProdInd = 0></cfcase>
	<cfcase value="Test">									<cfset Request.Gssv.SlafDevTestProdInd = 1></cfcase>
	<cfcase value="Prod">									<cfset Request.Gssv.SlafDevTestProdInd = 2></cfcase>
	<cfdefaultcase>											<cfset Request.Gssv.SlafDevTestProdInd = 0></cfdefaultcase>
	</cfswitch>
	<cfset Request.Gssv.SlafLoginProtocol					= ListGetAt(Request.Gssv.SlafLoginURL,		1,":")>
	<cfset Request.Gssv.SlafLoginServer						= ListGetAt(Request.Gssv.SlafLoginURL,		2,":")>
	<cfset Request.Gssv.SlafLoginServer						= ListGetAt(Request.Gssv.SlafLoginServer,	1,"/")>
	<cfset Request.Gssv.SlafLoginServerURL					= Request.Gssv.SlafLoginProtocol
															& "://"
															& Request.Gssv.SlafLoginServer>
	<cfset Request.Gssv.SlafLoginWSDL						= "#Request.Gssv.SlafLoginServerURL#/cls/ws/wbs_login.wsdl">
	<cfset Request.Gssv.SlafMustLoginURL					= "#Request.Gssv.SlafLoginServerURL#/cls/dsp_mustlogin.cfm">
	<cfif	Request.Gssv.SlafServerHasTrueGLS
		AND	Request.Gssv.SlafServerHasJaguar
		AND	Request.Gssv.SlafServerInstanceLogn>
		<cfset Request.Gssv.SlafLoginInclude				= "/cls/ws/wbs_login.cfm">
		<cfset Request.Gssv.SlafLoginLocally				= "Yes">
	<cfelse>
		<cfset Request.Gssv.SlafLoginInclude				= "">
		<cfset Request.Gssv.SlafLoginLocally				= "No">
	</cfif>
	<!---
	The following prevents "connection refused" errors if the current instance is on the server group as its (https) 
	login server. This consideration affects only eweb, enile and enilesp, and only when the Login Web Service is 
	called as a Web Service. This cannot be part of the preceding cfif, because enilesp has SlafLoginLocally set to 
	"No" (must do the callback as a Web Service), but, according to Chuda, the callback from enilesp to enile will fail 
	if it uses https. The ".wsdl" file forces Web Service calls to use https, so we have to avoid it. 
	--->
	<cfif Compare("#Request.Gssv.SlafServerGroup#.sba.gov",	Request.Gssv.SlafLoginServer) IS 0>
		<cfset Request.Gssv.SlafLoginWSDL					= Replace(Replace(Request.Gssv.SlafLoginWSDL,
																"https",	"http",		"ONE"),
																".wsdl",	".cfc?WSDL","ONE")>
	</cfif>
	<cfset Request.Gssv.SlafLocalHost						= "#Request.Gssv.SlafServerName#.sba.gov">
	<cfset Request.Gssv.SlafDefaultJaguarHost				= Request.Gssv.SlafLocalHost>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SlafGssvStruct.LastUpdateOfThisStructure")>
			<cfset Request.Gssv.PrevUpdateOfThisStructure	= Server.SlafGssvStruct.LastUpdateOfThisStructure>
		<cfelse>
			<cfset Request.Gssv.PrevUpdateOfThisStructure	= "N/A">
		</cfif>
	</cflock>
	<cfset Request.Gssv.LastUpdateOfThisStructure			= DateFormat(Now(), "YYYY-MM-DD")
															& " "
															& TimeFormat(Now(), "HH:mm:ss")>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafGssvStruct						= Duplicate(Request.Gssv)>
	</cflock>
</cfif>
<cfloop index="GssvKeyName" list="#StructKeyList(Request.Gssv)#">
	<cfswitch expression="#GssvKeyName#">
	<cfcase value="LastUpdateOfThisStructure,PrevUpdateOfThisStructure"><!--- Don't copy. ---></cfcase>
	<cfdefaultcase>
		<cfset Request[GssvKeyName]							= Request.Gssv[GssvKeyName]>
	</cfdefaultcase>
	</cfswitch>
</cfloop>
<cfif NOT Variables.SbaSharedVariablesSelfTestMode>
	<cfset StructClear (Request.Gssv)>
	<cfset StructDelete(Request,	"Gssv")>
	<cfset StructDelete(Variables,	"GssvGetHosts")>
	<cfset StructDelete(Variables,	"GssvGetServers")>
	<cfset StructDelete(Variables,	"GssvGetScqs")>
	<cfset StructDelete(Variables,	"SbaSharedVariablesForceRebuild")>
</cfif>

<!---
The following provide a canonical case (specifically, lowercase) for http/https and server name, in case the user typed 
them in mixed case. Note that path and filename must be in the case given by the user, because Unix is case sensitive. 
Note that we cannot infer the protocol from CGI.HTTPS, CGI.Server_Port or CGI.Server_Protocol, because the underlying 
servers don't know that the user is referencing https. So we infer it from Request.SlafURLServerShortName instead. 
--->

<cfset Request.SlafURLServerName							= LCase(CGI.Server_Name)>
<cfset Request.SlafURLServerShortName						= Replace(Request.SlafURLServerName, ".sba.gov", "", "ALL")>
<cfswitch expression="#Request.SlafURLServerShortName#">
<cfcase value="enile,eweb,inile,iweb,pro-net">				<cfset Request.SlafURLProtocol	= "https"></cfcase>
<cfdefaultcase>												<cfset Request.SlafURLProtocol	= "http"></cfdefaultcase>
</cfswitch>
<cfset Request.SlafURLProtocolServerAndScriptName			= "#Request.SlafURLProtocol#://#Request.SlafURLServerName#"
															& CGI.Script_Name><!--- (already contains initial slash) --->

<!---
Note: In the following, "EOL" is short for the platform-specific "end of line" characters that terminate text lines. 
--->

<cfif		FindNoCase("Windows",	Request.SlafServerOSName) GT 0>
	<cfset Request.SlafEOL									= Chr(13)&Chr(10)><!--- CRLF (Windows) --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafWindows								= "Yes">
	<cfset Request.SlafUnix									= "No">
<cfelseif	FindNoCase("Mac",		Request.SlafServerOSName) GT 0><!--- CFMX 7.01(+) supports Mac OS X(+). --->
	<!--- NOTE: The SBA doesn't have any Mac servers yet, but coding for compatibility keeps that option open. --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Same as Unix, because Mac OS X is Unix). --->
	<cfset Request.SlafMacintosh							= "Yes">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
<cfelse><!--- Because it's the "else" condition, Unix is the default: --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Unix). --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
</cfif>

<!---
If you call Now() repeatedly near midnight, it's possible for the displayed date not to match up with the displayed time. 
Therefore we grab it once and use it for all calculated date/time values. That way, they'll all match up. 
--->

<cfset Request.SlafDateTimeNow								= Now()>
<!--- Calculate current Federal Fiscal Year: --->
<cfset Request.SlafCurrFY									= Year(Request.SlafDateTimeNow)>
<cfif Month(Request.SlafDateTimeNow) GT 9>
	<cfset Request.SlafCurrFY								= Request.SlafCurrFY + 1>
</cfif>
<!--- Request.SlafDateTimeNow, formatted for display in a consistent manner: --->
<cfset Request.SlafDateTimeNowDisplay						= DateFormat(Request.SlafDateTimeNow, "YYYY-MM-DD")
															& " "
															& TimeFormat(Request.SlafDateTimeNow, "hh:mm:ss tt")>

<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<cfif	(Request.SlafDevTestProd IS "Prod")
	AND	(DateDiff("n", Request.SlafDateTimeNow, ParseDateTime("10/01/2007 6:00 AM")) GT 0)>
	<cfset Request.SlafDefaultNAICSYrNmb					= "2002">
	<cfset Request.SlafDefaultNAICSYrNmbCurr				= Request.SlafDefaultNAICSYrNmb>
	<cfset Request.SlafDefaultNAICSYrNmbPrev				= "1997">
</cfif>
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->

<cfif IsDefined("URL.RequestTimeout")>
	<cf_SetRequestTimeout seconds="#URL.RequestTimeout#">
</cfif>

<!--- SBA-Only IP Restriction, for applications that need it: --->

<cfset Request.SlafIPAddrIsNAT								= "No">
<cfset Request.SlafIPAddrIsSBA								= "No">
<cfset Variables.SlafArrayRemote_Addr						= ListToArray(CGI.Remote_Addr, ".")>
<cfset Variables.SlafArrayRemote_AddrLen					= ArrayLen(Variables.SlafArrayRemote_Addr)>
<cfif Variables.SlafArrayRemote_AddrLen IS 4>
	<cfswitch expression="#Variables.SlafArrayRemote_Addr[1]#"><!--- The cfcases are ordered numerically for easy lookup: --->
	<cfcase value="10"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfset Request.SlafIPAddrIsNAT						= "Yes">
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="63"><!--- 63.241.202.* are our proxy servers, including the VPN server. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 241)
			AND	(Variables.SlafArrayRemote_Addr[3] IS 202)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="127"><!--- Need to be at SBA to access datasources, so localhost must be SBA user. --->
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="165"><!--- According to Chuda, we own the entire 165.110 Class B network. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS "110")>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="172"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] GE  16)
			AND	(Variables.SlafArrayRemote_Addr[2] LE  31)>	<!--- 172.16/12 --->
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="192"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 168)>
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="199"><!--- According to Chuda, we own the entire 199.171.55 "Class C" subnet. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 171)
			AND	(Variables.SlafArrayRemote_Addr[3] IS  55)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="206"><!--- According to Chuda, we own the entire 206.66.57 DMZ subnet. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS  66)
			AND	(Variables.SlafArrayRemote_Addr[3] IS  57)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	</cfswitch>
</cfif>

<!--- Detect whether request was to a ColdFusion Component (CFC file): --->

<cfset Variables.PageName									= REReplace (CGI.Script_Name, ".*/", "", "ALL")>
<cfset Variables.QueryString								= "">
<!--- Remove question mark and URL parameters, if present: --->
<cfset Variables.SlafQMOffset								= Find("?", Variables.PageName)>
<cfif Variables.SlafQMOffset GT 0>
	<cfset Variables.PageName								= Left	(Variables.PageName, Variables.SlafQMOffset - 1)>
	<cfset Variables.QueryStringLen							= Len	(Variables.PageName) -	Variables.SlafQMOffset>
	<cfset Variables.QueryString							= Right	(Variables.PageName,	Variables.SlafQMOffset)>
<cfelse>
	<cfset Variables.QueryString							= CGI.Query_String>
	<cfset Variables.QueryStringLen							= Len	(Variables.QueryString)>
</cfif>
<cfset Request.SlafCFC										= "No">
<cfif	(Len(Variables.PageName) GT 4)
	AND	(CompareNoCase(Right(Variables.PageName, 4), ".cfc") IS 0)>
	<cfset Request.SlafCFC									= "Yes">
</cfif>

<!--- Self-Test Mode: --->

<cfif Variables.SbaSharedVariablesSelfTestMode>
	<cfdump var="#Request#" label="Request">
	<cfdump var="#Variables#" label="Variables">
</cfif>

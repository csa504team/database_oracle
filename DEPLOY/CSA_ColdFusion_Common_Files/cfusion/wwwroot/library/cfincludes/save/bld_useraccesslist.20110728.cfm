<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc. 
DATE:				07/06/2010
DESCRIPTION:		
NOTES:				None
INPUT:				None
OUTPUT:				
REVISION HISTORY:	07/06/2010, SRv:	Include routine to build Access List.
--->
<cfset Variables.CanSeeElendOrig		= "No">
<cfset Variables.CanSeeElendServicing	= "No">					
<cfset Variables.CanSeeLana				= "No">
<cfset Variables.CanSeeCLCS				= "No">	
<CFSET Variables.CanSeeGPTS				= "No">
<Cfset Variables.CanSeeElips			= "No">
<cfset Variables.CanSeePostServicing	= "No">
<cfset Variables.UserCanSeeSys			= "">
<Cfset Variables.ShowButtonList			= "">

<CFLOOP INDEX="Idx" FROM="1" TO="#ArrayLen(Variables.ArrayUserRoles)#">
<cfif Variables.ArrayUserRoles[Idx][13] LTE Variables.OrigLoginLevel>	
	<cfswitch expression="#Trim(Variables.ArrayUserRoles[Idx][1])#"><!--- 1 is index of Role. --->						
		<cfcase value="OrigAdmin,OrigGov,OrigPrtUpdt">
			<cfset Variables.CanSeeElendOrig		= "Yes">			
		</cfcase>
		<cfcase value="LoanGov,LoanPrtUpdt">
			<cfset Variables.CanSeeElendServicing	= "Yes">			
		</cfcase>				
		<cfcase value="LANAUPDATE,LANAREAD">
			<cfset Variables.CanSeeLana				= "Yes">			
		</cfcase>				
		<cfcase value="CLCSUpdate,CLCSAdmin,CLCSRead">	
			<cfset Variables.CanSeeCLCS				= "Yes">			
		</cfcase>								
		<cfcase value="GPTSUpdt,GPTSRead,GPTSMgmtReports">
			<CFSET Variables.CanSeeGPTS				= "Yes">	
		</cfcase>	
		<cfcase value="ELIPSRead">
			<cfset Variables.CanSeeElips			= "Yes">
		</cfcase>
		<cfcase value="LoanPostServRead">
			<cfset Variables.CanSeePostServicing	= "Yes">
		</cfcase>
	</cfswitch>								
</cfif>	
</cfloop>		
<cfif Variables.CanSeeElendOrig>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"Origination")>
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"applications")>
</cfif>
<cfif Variables.CanSeeElendServicing>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"Servicing")>								
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"servicing")>
</cfif>
<cfif Variables.CanSeeLana>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"LANA")>	
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"LANA")>
</cfif>
<cfif Variables.CanSeeCLCS>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"Chron")>		
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"Chron")>				
</cfif>
<cfif Variables.CanSeeGPTS>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"GPTS")>	
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"GPTS")>
</cfif>
<cfif Variables.CanSeeElips>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"ELIPS")>	
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"ELIPS")>
</cfif>
<cfif Variables.CanSeePostServicing>
	<Cfset Variables.UserCanSeeSys			= ListAppend(Variables.UserCanSeeSys,"Post Servicing")>	
	<Cfset Variables.ShowButtonList			= ListAppend(Variables.ShowButtonList,"PostServicing")>
</cfif>
			
	
		
		

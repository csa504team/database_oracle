<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Defines Request.SlafServerName and Request.SlafLocalHost. 
NOTES:

	If you cfinclude get_sbashared_variables.cfm, it will also define these 2 variables. Therefore, this is a minimal, 
	low-level routine you can call when you don't want to do everything in get_sbashared_variables. Think of this routine 
	as a stripped-down alternative to get_sbashared_variables. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. 
REVISION HISTORY:	01/31/2006, SRS:	Original implementation. 
--->

<cfset Request.SlafServerName								= "">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.SlafServerName")>
		<cfset Request.SlafServerName						= Server.SlafServerName>
	</cfif>
</cflock>
<cfif Len(Request.SlafServerName) IS 0>
	<cfobject action="CREATE" type="JAVA" name="Variables.InetAddress" class="java.net.InetAddress">
	<cfset Request.SlafServerName							= Trim(Variables.InetAddress.getLocalHost().getHostName())>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerName						= Request.SlafServerName>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">

<!---
AUTHOR:				Steve Seaquist
DATE:				12/15/2000
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_INTEGER
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.CVal (cleaned up FVal), Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	01/15/2002, SRS:	Change of methodology: Do everything in the val files, including setting the cleaned-up 
										variable into Variables.#FNam#, building the URLString and setting BlankScreen. 
					12/15/2000, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFSET Variables.CVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFSET Variables.CVal						= Variables.FVal>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFIF NOT IsNumeric(Variables.FVal)>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " must be numeric.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSEIF Variables.FVal LT -2415919104>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot be less than -2,415,919,104.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSEIF Variables.FVal GT 2415919103>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot exceed 2,415,919,103.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSE>
			<CFSET Variables.CVal				= Round(Variables.FVal)>
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.CVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

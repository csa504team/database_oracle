<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs Custom log entry. 
NOTES:				Called by manual include. 
INPUT:				Request.SBALogSystemName, maybe. If that's given, Request.SBALogText is mandatory. 
OUTPUT:				If currently configured to log it, Custom log entry ("SleCustom"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif IsDefined("Request.SBALogText")>
		<cfif Request.SleIsDebugEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
			<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
			<cfset Request.SleLogger.debug	("SleCustom"			& Request.SleHT
											& Request.SleScriptName	& Request.SleHT 
											& Request.SleUser		& RepeatString(Request.SleHT, 4)
											& Request.SBALogText)>
		</cfif>
	<cfelseif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.debug	("SleCustom"			& Request.SleHT
										& Request.SleScriptName	& Request.SleHT 
										& Request.SleUser		& RepeatString(Request.SleHT, 4)
										& "(log_SleCustom called without first setting Request.SBALogText)")>
	</cfif>
</cfif>

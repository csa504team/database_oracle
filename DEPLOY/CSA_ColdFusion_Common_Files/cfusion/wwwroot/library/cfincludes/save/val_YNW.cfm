<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif (Variables.FVal IS NOT "Y") AND (Variables.FVal IS NOT "N") AND (Variables.FVal IS NOT "W")>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be ""Y"", ""N"" or ""W"".</li>">
	</cfif>
</cfif>

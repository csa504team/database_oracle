<!---
AUTHOR:				Jaimin Bhatt
DATE:				06/24/2002
DESCRIPTION:		Validates that Administration Action has been selected
NOTES:				
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	06/24/2002, JBB:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFIF NOT IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FieldErrMsg				= Variables.FName & " must be selected.">
	<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
<CFELSE>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

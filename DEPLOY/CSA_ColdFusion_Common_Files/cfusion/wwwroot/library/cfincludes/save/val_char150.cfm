<!---
AUTHOR:				Steve Seaquist
DATE:				04/24/2002
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR150
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	04/25/2002, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFIF Len(Variables.FVal) GT 150>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot be more than 150 characters long.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

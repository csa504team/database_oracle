<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds Request.SlafHead and Request.SlafTopOfHead for use by cf_sbalookandfeel, reports, pages in 
					frames, etc. Request.SlafTopOfHead is the important one. It provides a standard, correct inclusion 
					sequence of SBA CSS files and JavaScript variables useful with CSS manipulation. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm, it will include this file for you. Therefore, you would normally 
	NOT include this file directly. It exists outside of get_sbalookandfeel_variables so that the cf_sbalookandfeel can 
	override some of the generated code, based on Attributes passed to it in the custom tag call. 

	If you want to define your own DOCTYPE, html tag, head tag and title tag first, cfoutput #Request.SlafTopOfHead# just 
	after the title tag. You would then continue on to define other system-specific CSS, page-specific JavaScript and the 
	/head. 

	If you don't care to define DOCTYPE, html and head yourself, simply cfoutput #Request.Head# instead. You would then 
	continue on to define the title, #Request.SlafTopOfHead#, system-specific CSS, page-specific JavaScript and the /head. 

	And if you don't mind defining the title tag lower down, simply cfoutput #Request.SlafHeadAndTopOfHead#, then title, 
	system-specific CSS, page-specific javascript and the /head. 

	When a custom tag calls a cfinclude, the Attributes scope is not passed to the cfinclude. So cf_sbalookandfeel passes 
	Attributes namespaced as Request.SlafTopOfHead variables to prevent name conflicts with the caller's variables. 

	You do NOT have to be using SBA Look-and-Feel to use Request.SlafHead or SlafTopOfHead. In particular, developers of 
	<cfdocument format="pdf" ... > reports and other formatted reports can use it, so that the report will pick up the 
	SBA's standard fonts and styles. 

INPUT:				Request.SlafApplicationName, Request.SlafTopOfHead variables (all optional, normally used only by 
					cf_sbalookandfeel). ApplicationName may or may not have been defined by get_sbalookandfeel_variables. 
					Assumes all get_sbashared_variables are defined. 
OUTPUT:				Request.SlafHead, Request.SlafTopOfHead and Request.SlafHeadAndTopOfHead. 
REVISION HISTORY:	08/10/2010, SRS:	Began prototyping HTML 5. 
					07/16/2010, SRS:	"New Look-and-Feel". Made Request.SlafTextOnly affect SlafTopOfHeadTextOnly. 
					06/10/2010, SRS:	Changed Variables.AppicationName to Request.SlafApplicationName, so that this 
										routine won't mess up if cfincluded from within a custom tag. 
					03/22/2010, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfif NOT IsDefined("Request.SlafHTML5")><!--- Will someday be set in cf_sbalookandfeel itself. --->
	<cfset Request.SlafHTML5						= (CGI.Remote_Addr is "165.110.40.166")><!--- DeveloperOfSlaf's PC --->
	<cfset Request.SlafHTML5						= "No"><!--- Turns off HTML 5, even on DeveloperOfSlaf's PC. --->
	<!---
	Synopsis of HTML problems so far: 
		* Firefox 3.6.8 messes up cf_sbatree display. 
		* MSIE 7 messes up GLS Choose Function: MainNav graphics and center justifies list of roles (across 2 columns)
	Synopsis of HTML improvements so far: 
		* MSIE 7 surprisingly starts supporting rounded buttons with green colored rollovers! CSS3 gets turned on! 
	--->
</cfif>

<!--- Runtime Options: --->

<cfparam name="Request.SlafTopOfHeadLang"			default="en-US">
<cfparam name="Request.SlafTopOfHeadLibURL"			default="/library">
<cfif IsDefined("Request.SlafTextOnly")>
	<cfset Request.SlafTopOfHeadTextOnly			= Request.SlafTextOnly>
<cfelse>
	<cfparam name="Request.SlafTopOfHeadTextOnly"	default="No">
</cfif>

<cfset Request.SlafTopOfHeadSelfTestMode			= "No">
<cfif CGI.Script_Name is "/library/cfincludes/get_sbalookandfeel_topofhead.cfm">
	<cfset Request.SlafTopOfHeadSelfTestMode		= "Yes">
	<cfinclude template="get_sbashared_variables.cfm"><!--- Safe to omit path. We're both in /library/cfincludes. --->
</cfif>
<cfparam name="Request.SlafButtons3D"				default="Yes"><!--- Until MSIE 9 is commonplace. --->

<cfsavecontent variable="Request.SlafHead">
	<cfif Request.SlafHTML5>
		<!--- Currently, the HTML 5 header messes up cf_sbatree, but not much else: --->
		<cfoutput><!doctype html>
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr">
<head>
<meta charset="utf-8"></cfoutput>
	<cfelse>
		<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></cfoutput>
	</cfif>
</cfsavecontent>

<cfsavecontent variable="Request.SlafTopOfHead">
	<cfoutput><cfif IsDefined("Request.SlafApplicationName") AND (Request.SlafApplicationName IS "CLS")>
<link		href="/cls/dsp_choosefunction.cfm"	accesskey="1"	rel="Home"			title="Home (Return to GLS Choose Function)"></cfif>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.css"						rel="stylesheet"	type="text/css" media="all"/>
<noscript>
	<link	href="#Request.SlafTopOfHeadLibURL#/css/sba.noscript.css"				rel="stylesheet"	type="text/css" media="all"/>
</noscript><cfif Request.SlafTopOfHeadTextOnly>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.textonly.css"				rel="stylesheet"	type="text/css" media="all"/></cfif>
<!--[if lt IE 8]><cfif Request.SlafButtons3D>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.msie6and7.css3pie.css"		rel="stylesheet"	type="text/css" media="all"/><cfelse>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.msie6and7.css"				rel="stylesheet"	type="text/css" media="all"/></cfif>
<![endif]-->
<script>
var	gSlafDevTestProd					= "#Request.SlafDevTestProd#";
var	gSlafDevTestProdInd					= "#Request.SlafDevTestProdInd#";
var	gSlafInlineBlock					= "inline-block";
var	gSlafInlineTable					= "inline-table";
var	gSlafMSIE6or7						= false;
</script>
<!--[if lt IE 8]>
<script>
gSlafInlineBlock						= "inline";
gSlafInlineTable						= "inline";
gSlafMSIE6or7							= true;
</script>
<![endif]-->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Request.SlafHeadAndTopOfHead">
	<cfoutput>#Request.SlafHead##Request.SlafTopOfHead#</cfoutput>
</cfsavecontent>

<cfif Request.SlafTopOfHeadSelfTestMode>
	<cfoutput>
<pre>
<span style="color:##900; font-weight:bold;">Request.SlafHead:</span>

#Replace(Request.SlafHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafTopOfHead:</span>

#Replace(Request.SlafTopOfHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafHeadAndTopOfHead:</span>

#Replace(Request.SlafHeadAndTopOfHead, "<", "&lt;", "ALL")#
</pre>
</cfoutput>
</cfif>

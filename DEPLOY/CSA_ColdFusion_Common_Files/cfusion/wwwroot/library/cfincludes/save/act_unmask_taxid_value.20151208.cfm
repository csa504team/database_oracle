<!---
AUTHOR:				Sherry Liu
DATE:				12/08/2015
DESCRIPTION:		Unmask form value when there is no changes made.
NOTES:				Only works when 
					1, The original form has both "xxxTaxID" and "xxxTaxIDNotChanged" in the form fields, 
					2, The "xxxTaxID".value is masked as ***-**-nnnn, or **-***nnnn
REVISION HISTORY:
--->

<cfloop collection="#form#"  item="key">
	<cfif findNoCase('TAXID',key) AND NOT findNoCase('NotChanged',key)>  
  
    	<cfset ToThisValue = evaluate(key &'NotChanged')>
        <!--- ToThisValue: #ToThisValue#<br /> --->     
        
		<cfif (left(form[key],3) EQ "***" AND mid(form[key],5,2) EQ "**") OR (left(form[key],2) EQ "**" AND mid(form[key],4,3) EQ "***")>    		
			<cfscript>
				structUpdate(form,key,ToThisValue);
			</cfscript> 
        </cfif>        
    </cfif>    
</cfloop>
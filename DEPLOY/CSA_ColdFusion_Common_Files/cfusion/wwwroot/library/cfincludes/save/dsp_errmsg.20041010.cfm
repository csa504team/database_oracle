<cfset Variables.ServerMsg			= "">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.Msg")>
		<cfset Variables.ServerMsg	= Server.Msg>
	</cfif>
</cflock>
<cfif Len(Variables.ServerMsg)>
	<cfoutput>
<center><table border="1"><tr><td><b><font color="##990000">
	#Variables.ServerMsg#
</font></b></td></tr></table></center><p></cfoutput>
</cfif>
<cfif IsDefined("Variables.ErrMsg") AND (Len(Variables.ErrMsg) GT 0)>
	<cfoutput>
<center><table border="1"><tr><td><b><font color="##990000">
	#Variables.ErrMsg#
</font></b></td></tr></table></center><p></cfoutput>
</cfif>
<cfif IsDefined("Variables.Commentary") AND (Len(Variables.Commentary) GT 0)>
	<cfoutput>
<center><table border="1"><tr><td><b><font color="##990000">
	Commentary: #Variables.Commentary#
</font></b></td></tr></table></center><p></cfoutput>
</cfif>

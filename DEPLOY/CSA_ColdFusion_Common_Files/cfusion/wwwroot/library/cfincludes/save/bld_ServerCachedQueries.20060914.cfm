<!---
AUTHOR:				Steve Seaquist
DATE:				04/28/2006
DESCRIPTION:		Makes sure that all "server cached queries" ("Scq") variables are defined in the Server scope. 
NOTES:

	Cfinclude this file before attempting to retrieve Scq variables from the Server scope. 

	Don't be misled by this file's name. It's a "build" routine, not a "get" routine. All it does is build server cached 
	queries in the SERVER scope, and only if necessary. THIS FILE DOES NOT ACTUALLY COPY SCQ VARIABLES INTO THE VARIABLES 
	OR REQUEST SCOPE. It just makes sure they're defined so that YOU can copy them into the Variables or Request scope, 
	because only you know which ones you actually need. 

	The purpose of the "Scq" prefix (as with the Slaf prefix in SBA look-and-feel) is to avoid naming conflicts with your 
	own variables. When you copy them into the Variables or Request scope, uou can keep their names the same (to show their 
	origin) or rename them (to make your code more readable). In the following example, Server.Scq.ActvStTbl is renamed to 
	[Variables.]getStates for readability: 

		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cflock scope="SERVER" type="READONLY" timeout="30">
			<cfset getStates					= Server.Scq.ActvStTbl>
		</cflock>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use the datasource "public_sbaref" (which uses the login "sbaselect"), you cannot include this file inside 
	a cftransaction that uses a different datasource or login. But it doesn't really have to be close to the code that locks 
	the Server scope. Just include this file at the beginning of your CFM file, among its initializations. 

	Note that code and description columns are available in 2 ways: (1) as their actual database column names, and (2) as 
	"code" and "description". If you don't want to use "code" and "description" (because they aren't unique across queries), 
	then don't use them. No harm done. They exist for use by shared routines (eg: to populate a variety of drop-down menus). 

INPUT:				None. 
OUTPUT:				Server scope variables prefixed with "Scq" (short for "server cached queries"), so as not to conflict 
					with other variables defined by the application. If the current user happens to be the one who takes 
					the I/O hit to define the queries, the "Scq" variables will also be defined in the Variables scope. 
					Generally, this will only happen when this routine is executed on its own as a Scheduled Task. 
REVISION HISTORY:	09/14/2006, DKC:	Added support for new table IMDomnTypTbl.
					08/11/2006, SRS:	Added support for pronet database cached queries (formerly cached in the Application 
										scope of PRO-Net when PRO-Net was not part of GLS). Also added "new logging". 
					06/14/2006, SRS:	Added 32 new cached query types. Allowed controlling what queries get defined 
										by controlling ScqAvailableResultSetNames. Per Ron Whalen, don't include Scq.ActvCMSACdTbl 
										in ScqAvailableResultSetNames, because it appear not to be in use. 
					05/12/2006, SRS:	Added IMCntryDialCd to ScqActvCountries, for use with dsp_IMCntryCdToIMCntryDialCd. 
					04/28/2006, SRS:	Original implementation. Used cfqueries initially, knowing that we will someday 
										rewrite it to use a stored procedure call (SPC) file. That's why there isn't any 
										cftry/cfcatch code. That'll be automatically added when we call the SPC file. 
--->

<!--- Configuration Variables: --->

<cfset Variables.ScqAvailableResultSetNames							=  "Scq.ActvBusAgeCdTbl"
																	& ",Scq.ActvBusTypTbl"
																	& ",Scq.ActvCalndrPrdTbl"
																	& ",Scq.ActvCitznshipCdTbl"
																	& ",Scq.ActvCohortCdTbl"
																	& ",Scq.ActvEconDevObjctCdTbl"
																	& ",Scq.ActvIMCntryCdTbl"
																	& ",Scq.ActvIMCrdtScorSourcTblBus"
																	& ",Scq.ActvIMCrdtScorSourcTblPer"
																	& ",Scq.ActvIMDomnTypTbl"
																	& ",Scq.ActvIMEPCOperCdTbl"
																	& ",Scq.ActvEthnicCdTbl"
																	& ",Scq.ActvGndrTbl"
																	& ",Scq.ActvGndrMFCdTbl"
																	& ",Scq.ActvInjctnTypCdTbl"
																	& ",Scq.ActvLoanCollatTypCdTbl"
																	& ",Scq.ActvLoanCollatValSourcTbl"
																	& ",Scq.ActvLoanCrdtUnavRsnCdTbl"
																	& ",Scq.ActvLoanFinanclStmtSourcTbl"
																	& ",Scq.ActvLoanMFDisastrTypTbl"
																	& ",Scq.ActvLoanMFPrcsMthdTypTbl"
																	& ",Scq.ActvLoanPartLendrTypTbl"
																	& ",Scq.ActvLoanPckgSourcTypTbl"
																	& ",Scq.ActvLoanPrevFinanStatTbl"
																	& ",Scq.ActvLoanProcdTypTbl"
																	& ",Scq.ActvLoanStatCdTbl"
																	& ",Scq.ActvMfSubPrgrmCdTbl"
																	& ",Scq.ActvPrcsMthdTbl"
																	& ",Scq.ActvPrgrmTbl"
																	& ",Scq.ActvPrgrmValidTbl"
																	& ",Scq.ActvRaceCdTbl"
																	& ",Scq.ActvSpcPurpsLoanTbl"
																	& ",Scq.ActvStatCdTbl"
																	& ",Scq.ActvStTbl"
																	& ",Scq.ActvVetTbl"
																	& ",Scq.pronet"
																	& ",Scq.pronet.ActvCountryCdTblForeign"
																	& ",Scq.pronet.AllCountryCdTbl"
																	& ",Scq.pronet.ActvFrmlDtrmntnCtgryTbl"
																	& ",Scq.pronet.ActvExportMainActvtyTypTbl"
																	& ",Scq.pronet.ActvExportMrktTypTbl"
																	& ",Scq.pronet.ActvIMAreaTypCdTbl"
																	& ",Scq.pronet.ActvIMQAStdCdTbl"
																	& ",Scq.pronet.AllIMQAStdCdTbl"
																	& ",Scq.pronet.ActvMntrAreaTypTbl">

<!--- First, check the master variable that defines whether or not all the others are defined: --->

<cfset Variables.ScqIsDefined										= "No">
<cfif CGI.Script_Name IS NOT "/library/cfincludes/bld_ServerCachedQueries.cfm">
	<!---
	When this file is cfincluded (the usual case), the following check will be done. When executed as a Scheduled Task, 
	however, it will not, and the queries will be refreshed from the database. This Scheduled Task is done around 5:00 AM. 
	--->
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq")>
			<cfset Variables.ScqIsDefined							= "Yes">
		</cfif>
	</cflock>
</cfif>

<!--- Build the queries if they don't exist, or if we were told to (by calling this page directly): --->

<cfif NOT Variables.ScqIsDefined>
	<!---
	Do the queries outside of the exclusive cflock of the Server scope, so as to minimize exclusive lock time. Also, 
	because some servers have no need of some cached queries, decfine the queries in a loop. This allows creating or 
	not creating a query simply by altering the contents of ScqAvailableResultSetNames on a server-by-server basis. 
	--->
	<cfset Variables.Scq											= StructNew()>
	<cfloop index="ScqQueryName" list="#Variables.ScqAvailableResultSetNames#">
		<cfswitch expression="#ScqQueryName#">
		<cfcase				  value="Scq.ActvBusAgeCdTbl">
			<cfquery name="Variables.Scq.ActvBusAgeCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		BusAgeCd,
						BusAgeDesc,
						BusMFAgeCd,
						BusAgeCd									AS code,
						BusAgeDesc									AS description
			from		BusAgeCdTbl
			where		(	datediff(dd,BusAgeStrtDt,				getdate()) >= 0)
			and			(				BusAgeEndDt					is null
						or	datediff(dd,BusAgeEndDt,				getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvBusTypTbl">
			<cfquery name="Variables.Scq.ActvBusTypTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		BusTypCd,
						BusTypTxt,
						BusTypMFCd,
						BusTypMFTxt,
						BusTypCd									AS code,
						BusTypTxt									AS description
			from		BusTypTbl
			where		(	datediff(dd,BusTypStrtDt,				getdate()) >= 0)
			and			(				BusTypEndDt					is null
						or	datediff(dd,BusTypEndDt,				getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCalndrPrdTbl">
			<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		CalndrPrdCd,
						CalndrPrdDesc,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CalndrPrdCd = 'M'						then 1
						when CalndrPrdCd = 'Q'						then 2
						when CalndrPrdCd = 'S'						then 3
						when CalndrPrdCd = 'A'						then 4
						else										5
						end											AS displayorder,
						CalndrPrdCd									AS code,
						CalndrPrdDesc								AS description
			from		CalndrPrdTbl
			where		(	datediff(dd,CalndrPrdStrtDt,			getdate()) >= 0)
			and			(				CalndrPrdEndDt				is null
						or	datediff(dd,CalndrPrdEndDt,				getdate()) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCitznshipCdTbl">
			<cfquery name="Variables.Scq.ActvCitznshipCdTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		CitznshipCd,
						CitznshipTxt,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CitznshipCd = 'US'						then 1
						when CitznshipCd = 'RA'						then 2
						when CitznshipCd = 'NR'						then 3
						when CitznshipCd = 'IA'						then 4
						else										5
						end											AS displayorder,
						CitznshipCd									AS code,
						CitznshipTxt								AS description
			from		CitznshipCdTbl
			where		(	datediff(dd,CitznshipStrtDt,			getdate()) >= 0)
			and			(				CitznshipEndDt				is null
						or	datediff(dd,CitznshipEndDt,				getdate()) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCMSACdTbl">
			<cfquery name="Variables.Scq.ActvCMSACdTbl"					datasource="public_sbaref" dbtype="Sybase11">
			select		CMSACd,
						CMSANm,
						CMSACd										AS code,
						CMSANm										AS description
			from		CMSACdTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCohortCdTbl">
			<cfquery name="Variables.Scq.ActvCohortCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		CohortCd,
						CohortDesc,
						CohortCd									AS code,
						CohortDesc									AS description
			from		CohortCdTbl
			where		(	datediff(dd,CohortStrtDt,				getdate()) >= 0)
			and			(				CohortEndDt					is null
						or	datediff(dd,CohortEndDt,				getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEconDevObjctCdTbl">
			<cfquery name="Variables.Scq.ActvEconDevObjctCdTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		EconDevObjctCd,
						EconDevObjctTxt,
						EconDevObjctCd								AS code,
						EconDevObjctTxt								AS description
			from		EconDevObjctCdTbl
			where		(	datediff(dd,EconDevObjctStrtDt,			getdate()) >= 0)
			and			(				EconDevObjctEndDt			is null
						or	datediff(dd,EconDevObjctEndDt,			getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCntryCdTbl">
			<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		IMCntryCd,
						IMCntryNm,
						IMCntryDialCd,
						IMCntryCd									AS code,
						IMCntryNm									AS description
			from		IMCntryCdTbl
			where		(	datediff(dd,IMCntryStrtDt,				getdate()) >= 0)
			and			(				IMCntryEndDt				is null
						or	datediff(dd,IMCntryEndDt,				getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblBus">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblBus"	datasource="public_sbaref" dbtype="Sybase11">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		IMCrdtScorSourcTbl
			where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'B')
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblPer">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblPer"	datasource="public_sbaref" dbtype="Sybase11">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
						when IMCrdtScorSourcCd = 12					then 1
						when IMCrdtScorSourcCd = 13					then 2
						when IMCrdtScorSourcCd = 11					then 3
						when IMCrdtScorSourcCd = 14					then 4
						when IMCrdtScorSourcCd = 15					then 5
						else										6
						end											AS displayorder,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		IMCrdtScorSourcTbl
			where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'P')
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMDomnTypTbl">
			<cfquery name="Variables.Scq.ActvIMDomnTypTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select 		IMDomnTypCd									AS code,
						IMDomnTypDescTxt							AS description
			from 		IMDomnTypTbl
			where 		(	datediff(dd,IMDomnTypStrtDt,			getdate()) >= 0)
			and			(				IMDomnTypEndDt				is null
						or	datediff(dd,IMDomnTypEndDt,				getdate()) <= 0)
			order by	IMDomnTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMEPCOperCdTbl">
			<cfquery name="Variables.Scq.ActvIMEPCOperCdTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		IMEPCOperCd,
						IMEPCOperDescTxt,
						IMEPCOperCd									AS code,
						IMEPCOperDescTxt							AS description
			from		IMEPCOperCdTbl
			where		(	datediff(dd,IMEPCOperCdStrtDt,			getdate()) >= 0)
			and			(				IMEPCOperCdEndDt			is null
						or	datediff(dd,IMEPCOperCdEndDt,			getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEthnicCdTbl">
			<cfquery name="Variables.Scq.ActvEthnicCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		EthnicCd,
						EthnicDesc,
						EthnicCd									AS code,
						EthnicDesc									AS description
			from		EthnicCdTbl
			where		(	datediff(dd,EthnicStrtDt,				getdate()) >= 0)
			and			(				EthnicEndDt					is null
						or	datediff(dd,EthnicEndDt,				getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrTbl">
			<cfquery name="Variables.Scq.ActvGndrTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		GndrCd,
						GndrDesc,
						GndrCd										AS code,
						GndrDesc									AS description
			from		GndrCdTbl
			where		(	datediff(dd,GndrStrtDt,					getdate()) >= 0)
			and			(				GndrEndDt					is null
						or	datediff(dd,GndrEndDt,					getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrMFCdTbl">
			<cfquery name="Variables.Scq.ActvGndrMFCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		GndrMFCd,
						GndrMFDescTxt,
						GndrCd,
						GndrMFCd									AS code,
						GndrMFDescTxt								AS description
			from		GndrMFCdTbl
			where		(	datediff(dd,GndrMFStrtDt,				getdate()) >= 0)
			and			(				GndrMFEndDt					is null
						or	datediff(dd,GndrMFEndDt,				getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvInjctnTypCdTbl">
			<cfquery name="Variables.Scq.ActvInjctnTypCdTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		InjctnTypCd,
						InjctnTypTxt,
						case<!--- Reorder cash types first, then non-cash, then standby debt, then other: --->
						when InjctnTypCd = 'C'						then 1
						when InjctnTypCd = 'G'						then 2
						when InjctnTypCd = 'D'						then 3
						when InjctnTypCd = 'A'						then 4
						when InjctnTypCd = 'S'						then 5
						when InjctnTypCd = 'O'						then 6
						else										7
						end											AS displayorder,
						InjctnTypCd									AS code,
						InjctnTypTxt								AS description
			from		InjctnTypCdTbl
			where		(	datediff(dd,InjctnTypStrtDt,			getdate()) >= 0)
			and			(				InjctnTypEndDt				is null
						or	datediff(dd,InjctnTypEndDt,				getdate()) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatTypCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatTypCdTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanCollatTypCd,
						LoanCollatTypDescTxt,
						LoanCollatTypCd								AS code,
						LoanCollatTypDescTxt						AS description
			from		LoanCollatTypCdTbl
			where		(	datediff(dd,LoanCollatTypStrtDt,		getdate()) >= 0)
			and			(				LoanCollatTypEndDt			is null
						or	datediff(dd,LoanCollatTypEndDt,			getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatValSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatValSourcTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanCollatValSourcCd,
						LoanCollatValSourcDescTxt,
						LoanCollatValSourcCd						AS code,
						LoanCollatValSourcDescTxt					AS description
			from		LoanCollatValSourcTbl
			where		(	datediff(dd,LoanCollatValSourcStrtDt,	getdate()) >= 0)
			and			(				LoanCollatValSourcEndDt		is null
						or	datediff(dd,LoanCollatValSourcEndDt,	getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCrdtUnavRsnCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCrdtUnavRsnCdTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanCrdtUnavRsnCd,
						LoanCrdtUnavRsnDescTxt,
						LoanCrdtUnavRsnCd							AS code,
						LoanCrdtUnavRsnDescTxt						AS description
			from		LoanCrdtUnavRsnCdTbl
			where		(	datediff(dd,LoanCrdtUnavRsnStrtDt,		getdate()) >= 0)
			and			(				LoanCrdtUnavRsnEndDt		is null
						or	datediff(dd,LoanCrdtUnavRsnEndDt,		getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanFinanclStmtSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanFinanclStmtSourcTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanFinanclStmtSourcCd,
						LoanFinanclStmtSourcDescTxt,
						LoanFinanclStmtSourcCd						AS code,
						LoanFinanclStmtSourcDescTxt					AS description
			from		LoanFinanclStmtSourcTbl
			where		(	datediff(dd,LoanFinanclStmtSourcStrtDt,	getdate()) >= 0)
			and			(				LoanFinanclStmtSourcEndDt	is null
						or	datediff(dd,LoanFinanclStmtSourcEndDt,	getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFDisastrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFDisastrTypTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanMFDisastrCd,
						LoanMFDisastrDescTxt,
						LoanMFDisastrCd								AS code,
						LoanMFDisastrDescTxt						AS description
			from		LoanMFDisastrTypTbl
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFPrcsMthdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFPrcsMthdTypTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanMFPrcsMthdCd,
						LoanMFPrcsMthdDescTxt,
						LoanMFPrcsMthdCd							AS code,
						LoanMFPrcsMthdDescTxt						AS description
			from		LoanMFPrcsMthdTypTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPartLendrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPartLendrTypTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanPartLendrTypCd,
						LoanPartLendrTypDescTxt,
						case<!--- Force Participating to the top and Others to the end: --->
						when LoanPartLendrTypCd = 'P'				then 1
						when LoanPartLendrTypCd = 'I'				then 2
						when LoanPartLendrTypCd = 'G'				then 3
						when LoanPartLendrTypCd = 'O'				then 4
						else										5
						end											AS displayorder,
						LoanPartLendrTypCd							AS code,
						LoanPartLendrTypDescTxt						AS description
			from		LoanPartLendrTypTbl
			where		(	datediff(dd,LoanPartLendrTypStrtDt,		getdate()) >= 0)
			and			(				LoanPartLendrTypEndDt		is null
						or	datediff(dd,LoanPartLendrTypEndDt,		getdate()) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPckgSourcTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPckgSourcTypTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanPckgSourcTypCd,
						LoanPckgSourcTypDescTxt,
						LoanPckgSourcTypCd							AS code,
						LoanPckgSourcTypDescTxt						AS description
			from		LoanPckgSourcTypTbl
			where		(	datediff(dd,LoanPckgSourcTypStrtDt,		getdate()) >= 0)
			and			(				LoanPckgSourcTypEndDt		is null
						or	datediff(dd,LoanPckgSourcTypEndDt,		getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPrevFinanStatTbl">
			<cfquery name="Variables.Scq.ActvLoanPrevFinanStatTbl"	datasource="public_sbaref" dbtype="Sybase11">
			select		LoanPrevFinanStatCd,
						LoanPrevFinanStatDescTxt,
						LoanPrevFinanStatCd							AS code,
						LoanPrevFinanStatDescTxt					AS description
			from		LoanPrevFinanStatTbl
			where		(	datediff(dd,LoanPrevFinanStatStrtDt,	getdate()) >= 0)
			and			(				LoanPrevFinanStatEndDt		is null
						or	datediff(dd,LoanPrevFinanStatEndDt,		getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanProcdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanProcdTypTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		LoanProcdTypCd,
						LoanProcdTypDescTxt,
						ProcdTypCd,<!--- Ignoring LoanProcdTypCmnt for now, because it's always null. --->
						ProcdTypCd + LoanProcdTypCd					AS code,
						LoanProcdTypDescTxt							AS description
			from		LoanProcdTypTbl
			where		(	datediff(dd,LoanProcdTypCdStrtDt,		getdate()) >= 0)
			and			(				LoanProcdTypCdEndDt			is null
						or	datediff(dd,LoanProcdTypCdEndDt,		getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanStatCdTbl">
			<cfquery name="Variables.Scq.ActvLoanStatCdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		LoanStatCd,
						LoanStatDescTxt,
						LoanStatCd									AS code,
						LoanStatDescTxt								AS description
			from		LoanStatCdTbl
			where		(	datediff(dd,LoanStatStrtDt,				getdate()) >= 0)
			and			(				LoanStatEndDt				is null
						or	datediff(dd,LoanStatEndDt,				getdate()) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvMfSubPrgrmCdTbl">
			<cfquery name="Variables.Scq.ActvMfSubPrgrmCdTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		MfSubPrgrmCd,
						MfSubPrgrmDesc,
						MfSubPrgrmCd								AS code,
						MfSubPrgrmDesc								AS description
			from		MfSubPrgrmCdTbl
			where		(	datediff(dd,MfSubPrgrmStrtDt,			getdate()) >= 0)
			and			(				MfSubPrgrmEndDt				is null
						or	datediff(dd,MfSubPrgrmEndDt,			getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrcsMthdTbl">
			<cfquery name="Variables.Scq.ActvPrcsMthdTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description
			from		PrcsMthdTbl
			where		(	datediff(dd,PrcsMthdStrtDt,				getdate()) >= 0)
			and			(				PrcsMthdEndDt				is null
						or	datediff(dd,PrcsMthdEndDt,				getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmTbl">
			<cfquery name="Variables.Scq.ActvPrgrmTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description
			from		PrgrmTbl
			where		(	datediff(dd,PrgrmStrtDt,				getdate()) >= 0)
			and			(				PrgrmEndDt					is null
						or	datediff(dd,PrgrmEndDt,					getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmValidTbl">
			<cfquery name="Variables.Scq.ActvPrgrmValidTbl"			datasource="public_sbaref" dbtype="Sybase11">
			select		PrgrmCd,
						PrcsMthdCd,
						SpcPurpsLoanCd,
						ProcdTypCd,
						PIMSPrgrmId,
						SubPrgrmMFCd,
						CohortCd,
						LoanMFSpcPurpsCd,
						LoanMFPrcsMthdCd,
						LoanMFSTARInd,
						LoanMFDisastrCd,
						PrgrmAuthOthAgrmtInd,
						PrgrmAuthAreaInd<!--- Because this table is never displayed, it doesn't have code and description columns. --->
			from		PrgrmValidTbl
			where		(	datediff(dd,PrgrmValidStrtDt,			getdate()) >= 0)
			and			(				PrgrmValidEndDt				is null
						or	datediff(dd,PrgrmValidEndDt,			getdate()) <= 0)
			order by	PrgrmValidSeqNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvRaceCdTbl">
			<cfquery name="Variables.Scq.ActvRaceCdTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		RaceCd,
						RaceTxt,
						RaceMFCd,
						RaceMFTxt,
						RaceCd										AS code,
						RaceTxt										AS description
			from		RaceCdTbl
			where		(	datediff(dd,RaceStrtDt,					getdate()) >= 0)
			and			(				RaceEndDt					is null
						or	datediff(dd,RaceEndDt,					getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!---
		IMRtTbl (current interest rates) vary over time, so the contents of that table aren't very static. Also, they're often 
		accessed in the past. (For example, if electronic lending has a received-date, LoanAppRecvDt, it grabs the interest rate 
		for that date, rather than today's date.) Therefore a snapshot of today's interest rates is not very useful. Even when 
		today's rate **IS** needed, a snapshot from 5:00 AM (when these cached queries are refreshed) is liable to be wrong, 
		because rates are updated during the day. Furthermore, IMRtTypTbl is generally only accessed in joins to IMRtTbl. 
		Therefore, we aren't caching that table either. 				Steve Seaquist
		--->
		<cfcase				  value="Scq.ActvSpcPurpsLoanTbl">
			<cfquery name="Variables.Scq.ActvSpcPurpsLoanTbl"		datasource="public_sbaref" dbtype="Sybase11">
			select		SpcPurpsLoanCd,
						SpcPurpsLoanDesc,
						SpcPrgrmCd,
						SpcPurpsLoanCd								AS code,
						SpcPurpsLoanDesc							AS description
			from		SpcPurpsLoanTbl
			where		(	datediff(dd,SpcPurpsLoanStrtDt,			getdate()) >= 0)
			and			(				SpcPurpsLoanEndDt			is null
						or	datediff(dd,SpcPurpsLoanEndDt,			getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStatCdTbl">
			<cfquery name="Variables.Scq.ActvStatCdTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		StatCd,
						StatTxt,
						StatCdDispInd,
						StatCdDispOrdNmb							AS displayorder,
						StatCd										AS code,
						StatTxt										AS description
			from		StatCdTbl
			where		(	datediff(dd,StatCdStrtDt,				getdate()) >= 0)
			and			(				StatCdEndDt					is null
						or	datediff(dd,StatCdEndDt,				getdate()) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStTbl">
			<cfquery name="Variables.Scq.ActvStTbl"					datasource="public_sbaref" dbtype="Sybase11">
			select		StCd,
						StNm,
						StCd										AS code,
						StNm										AS description
			from		StTbl
			where		(	datediff(dd,StStrtDt,					getdate()) >= 0)
			and			(				StEndDt						is null
						or	datediff(dd,StEndDt,					getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvVetTbl">
			<cfquery name="Variables.Scq.ActvVetTbl"				datasource="public_sbaref" dbtype="Sybase11">
			select		VetCd,
						VetTxt,
						VetMFCd,
						VetMFDescTxt,
						VetCd										AS code,
						VetTxt										AS description
			from		VetTbl
			where		(	datediff(dd,VetStrtDt,					getdate()) >= 0)
			and			(				VetEndDt					is null
						or	datediff(dd,VetEndDt,					getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet">
			<cfset Variables.Scq.pronet								= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvCountryCdTblForeign">
			<cfquery name="Variables.Scq.pronet.ActvCountryCdTblForeign"	datasource="pronet" dbtype="Sybase11">
			select		CountryCd									code,
						CountryNm									description
			from		CountryCdTbl
			where		DomForInd									> 0 /* indicating foreign */
			and			PubBanInd									= 0
			and			PvtBanInd									= 0
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllCountryCdTbl">
			<cfquery name="Variables.Scq.pronet.AllCountryCdTbl"	datasource="pronet" dbtype="Sybase11">
			select		CountryCd									code,
						CountryNm									description,
						DomForInd,
						PubBanInd,
						PvtBanInd
			from		CountryCdTbl
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvFrmlDtrmntnCtgryTbl">
			<cfquery name="Variables.Scq.pronet.ActvFrmlDtrmntnCtgryTbl"	datasource="pronet" dbtype="Sybase11">
			select		CtgryCd										code,
						CtgryNm										description
			from		FrmlDtrmntnCtgryTbl
			where		(	datediff(dd,StartDt,					getdate()) >= 0)
			and			(				EndDt						is null
						or	datediff(dd,EndDt,						getdate()) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMainActvtyTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMainActvtyTypTbl"	datasource="pronet" dbtype="Sybase11">
			select		ExportMainActvtyCd							code,
						ExportMainActvtyDescTxt						description
			from		ExportMainActvtyTypTbl
			order by	ExportMainActvtyCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMrktTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMrktTypTbl"	datasource="pronet" dbtype="Sybase11">
			select		ExportMrktTypCd								code,
						ExportMrktTypDescTxt						description
			from		ExportMrktTypTbl
			order by	ExportMrktTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMAreaTypCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.cfprname								= "GetIMAreaTypCdTbl">
			<cfset Variables.db										= "pronet">
			<cfset Variables.dbtype									= "Sybase11">
			<cfinclude template="/cfincludes/pronet/spc_IMAreaTypCdSelTSP.cfm">
			<!--- Code and description are not returned by the stored procedure, so we need to do a query-of-queries: --->
			<cfquery name="Variables.Scq.pronet.ActvIMAreaTypCdTbl"	dbtype="query">
			select		IMAreaTypCd									code,
						IMAreaTypDescTxt							description
			from		GetIMAreaTypCdTbl
			order by	IMAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMQAStdCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.cfprname								= "GetIMQAStdCdTbl">
			<cfset Variables.db										= "pronet">
			<cfset Variables.dbtype									= "Sybase11">
			<cfinclude template="/cfincludes/pronet/spc_IMQAStdCdSelTSP.cfm">
			<cfquery name="Variables.Scq.pronet.ActvIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		GetIMQAStdCdTbl<!--- Eliminate deleted codes: --->
			where		IMQAStdCd									in ('B','A','I','F','E')
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllIMQAStdCdTbl">	<!--- Requires ActvIMQAStdCdTbl (for GetIMQAStdCdTbl) --->
			<cfquery name="Variables.Scq.pronet.AllIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		GetIMQAStdCdTbl
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvMntrAreaTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvMntrAreaTypTbl"	datasource="pronet" dbtype="Sybase11">
			select		MntrAreaTypCd								code,
						MntrAreaTypDescTxt							description
			from		MntrAreaTypTbl
			order by	MntrAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		</cfswitch>
	</cfloop>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.Scq											= Duplicate(Variables.Scq)>
		<cfset Server.ScqActvCountries								= Variables.Scq.ActvIMCntryCdTbl><!--- Temp!!! --->
		<cfset Server.ScqActvStates									= Variables.Scq.ActvStTbl><!--- Temp!!! --->
	</cflock>
	<cfif CGI.Script_Name IS "/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cfdump label="Server Cached Queries"						var="#Variables.Scq#">
	</cfif>
</cfif>

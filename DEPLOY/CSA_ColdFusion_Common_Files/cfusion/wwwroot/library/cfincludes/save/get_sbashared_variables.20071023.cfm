<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel, cf_setrequesttimeout, etc. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm right after <cfapplication>, it will include this file for you. 
	Therefore, you would normally NOT include this file directly. It has been separated out of get_sbalookandfeel_variables 
	for use in Web Services, apps without <cfapplication> (and hence don't have Session variables) and other situations where 
	you would not be including get_sbalookandfeel_variables. However, because the output variables will most often be used in 
	the SBA look-and-feel context, they retain the "Slaf" prefix. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. Set RequestTimeout from URL.RequestTimeout for upward compatibility. 
					(Can be overridden later by caller.) 
REVISION HISTORY:	10/23/2007, SRS:	Added 206.66.57.* ("the DMZ") as one of valid SBA IP address criteria. Allowed eweb 
										and enile callers to call the Login Web Service(s) without hitting the https problem. 
					10/22/2007, DKC:	Added 199.171.55.* ("the Class C") as one of valid SBA IP address criteria. 
					10/20/2007, SRS:	Added SlafLoginWSDL to allow logging in via Web Services. 
					09/28/2007, SRS:	Added SlafDefaultNAICSYrNmb for use with 2007 NAICS conversion. Also added new logic 
										to prevent existing sessions from crashing because they never defined a Server variable. 
										Also added SlafMustLoginURL and SlafVersionRevision. 
					03/21/2007, SRS:	Added SlafLoginURL, for use with "Distributed GLS". 
					07/25/2006, SRS:	Added support for SlafIPAddrIsNAT (10/8, 172.16/12, 192.168/16). Required by PRO-Net, 
										which allows all SBA users with NAT IP addresses to search for non-active firms. 
										Corrected the logic for localhost to be the entire Class A range beginning with 127. 
					07/17/2006, SRS:	Moved Variables.PageName code from get_sbalookandfeel_variables, so that we can set 
										a new variable, namely, Request.SlafCFC. 
					07/03/2006, SRS:	Just in case we ever get IPv6 or no IP address in CGI.Remote_Addr, added a check to 
										make sure that ArrayLen(Variables.SlafArrayRemote_Addr) is 4. This means that all 
										IPv6 and absent Remote_Addr addresses will be treated as NOT being SBA. We will 
										definitely have to modify this code after the conversion to IPv6 infrastructure. 
					06/26/2006, SRS:	Added Request.SlafIPAddrIsSBA code. (Essentially, just copied from PRO-Net and 
										stripped of PRO-Net-specific code.) 
					01/31/2006, SRS:	Original implementation. 
--->

<!--- Global, SBA-Wide Configuration Parameters: --->

<cfset Request.SlafDefaultJaguarPort						= 9000><!--- Default host set below. --->
<cfset Request.SlafDefaultNAICSYrNmb						= "2007">
<cfset Request.SlafDefaultNAICSYrNmbCurr					= Request.SlafDefaultNAICSYrNmb>
<cfset Request.SlafDefaultNAICSYrNmbPrev					= "2002">

<!--- Initializations --->

<cfset Request.SlafLoginLocally								= ""><!--- Newest Server variable. --->
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif CGI.Script_Name IS NOT "/library/cfincludes/get_sbashared_variables.cfm">
		<cfif IsDefined("Server.SlafLoginLocally")>			<!--- Newest Server variable. --->
			<cfset Request.SlafDevTestProd					= Server.SlafDevTestProd>
			<cfset Request.SlafDevTestProdInd				= Server.SlafDevTestProdInd>
			<cfset Request.SlafLoginHostList				= Server.SlafLoginHostList>
			<cfset Request.SlafLoginInclude					= Server.SlafLoginInclude>
			<cfset Request.SlafLoginLocally					= Server.SlafLoginLocally>
			<cfset Request.SlafLoginServer					= Server.SlafLoginServer>
			<cfset Request.SlafLoginURL						= Server.SlafLoginURL>
			<cfset Request.SlafLoginWSDL					= Server.SlafLoginWSDL>
			<cfset Request.SlafMustLoginURL					= Server.SlafMustLoginURL>
			<cfset Request.SlafServerName					= Server.SlafServerName>
		</cfif>
	</cfif>
	<cfset Request.SlafServerOSName							= Trim(Server.OS.Name)>
	<cfset Request.SlafVersionList							= Server.ColdFusion.ProductVersion><!--- e.g.: "7,0,2,142559" --->
	<cfset Request.SlafVersionMajor							= ListGetAt(Request.SlafVersionList, 1)><!--- "7" --->
	<cfset Request.SlafVersionMinor							= ListGetAt(Request.SlafVersionList, 2)><!--- "0" --->
	<cfset Request.SlafVersionRevision						= ListGetAt(Request.SlafVersionList, 3)><!--- "2" --->
	<cfset Request.SlafVersion								= Request.SlafVersionMajor
															& "."
															& Request.SlafVersionMinor>
	<cfif Request.SlafVersionRevision IS NOT "0"><!--- "8.0.0" should be displayed as only "8.0" --->
		<cfset Request.SlafVersion							= Request.SlafVersion
															& "."
															& Request.SlafVersionRevision>
	</cfif>
</cflock>
<cfif Len(Request.SlafLoginLocally) IS 0>					<!--- Newest Server variable. --->
	<!--- Request.SlafLoginLocally is the one that for-sure should have a non-zero length, so that's the one we test. --->
	<cfset Request.SlafDevTestProd							= "">
	<cfset Request.SlafDevTestProdInd						= "">
	<cfset Request.SlafLoginServer							= "">
	<cfset Request.SlafLoginURL								= "">
	<cfobject action="CREATE" type="JAVA" name="Variables.InetAddress" class="java.net.InetAddress">
	<cfset Request.SlafServerName							= Trim(Variables.InetAddress.getLocalHost().getHostName())>
	<cfswitch expression="#Request.SlafServerName#">
	<cfcase value="amazon,missouri,potomac,riogrande,wocs09,wocs41">
		<cfset Request.SlafDevTestProd						= "Prod">
		<cfset Request.SlafDevTestProdInd					= "2">
		<cfset Request.SlafLoginHostList					= "riogrande,wocs41">
		<cfset Request.SlafLoginServer						= "eweb">
		<cfset Request.SlafLoginURL							= "https://eweb.sba.gov/cls/dsp_login.cfm">
		<cfset Request.SlafLoginWSDL						= "https://eweb.sba.gov/cls/ws/wbs_login.wsdl">
		<cfset Request.SlafMustLoginURL						= "https://eweb.sba.gov/cls/dsp_mustlogin.cfm">
	</cfcase>
	<cfcase value="rouge,yukon">
		<cfset Request.SlafDevTestProd						= "Test">
		<cfset Request.SlafDevTestProdInd					= "1">
		<cfset Request.SlafLoginHostList					= "rouge,yukon">
		<cfset Request.SlafLoginServer						= "enile">
		<cfset Request.SlafLoginURL							= "https://enile.sba.gov/cls/dsp_login.cfm">
		<cfset Request.SlafLoginWSDL						= "https://enile.sba.gov/cls/ws/wbs_login.wsdl">
		<cfset Request.SlafMustLoginURL						= "https://enile.sba.gov/cls/dsp_mustlogin.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfset Request.SlafDevTestProd						= "Dev">
		<cfset Request.SlafDevTestProdInd					= "0">
		<cfset Request.SlafLoginHostList					= "danube">
		<cfset Request.SlafLoginServer						= "danube">
		<cfset Request.SlafLoginURL							= "http://danube.sba.gov/cls/dsp_login.cfm">
		<cfset Request.SlafLoginWSDL						= "http://danube.sba.gov/cls/ws/wbs_login.wsdl">
		<cfset Request.SlafMustLoginURL						= "http://danube.sba.gov/cls/dsp_mustlogin.cfm">
	</cfdefaultcase>
	</cfswitch>
	<cfswitch expression="#Request.SlafServerName#">
	<cfcase value="danube,rouge,yukon,riogrande,wocs41">
		<cfset Request.SlafLoginInclude						= "/cls/ws/wbs_login.cfm">
		<cfset Request.SlafLoginLocally						= "Yes">
		<!---
		Although the caller CAN login locally, the following prevents "connection refused" errors if the 
		caller goes ahead and uses the Web Service anyway on eweb or enile: 
		--->
		<cfset Request.SlafLoginWSDL						= Replace(Replace(Request.SlafLoginWSDL, 
															  "https://",		"http://",				"ONE"),
															  "wbs_login.wsdl",	"wbs_login.cfc?WSDL",	"ONE")>
	</cfcase>
	<cfdefaultcase>
		<cfset Request.SlafLoginInclude						= "">
		<cfset Request.SlafLoginLocally						= "No">
	</cfdefaultcase>
	</cfswitch>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafDevTestProd						= Request.SlafDevTestProd>
		<cfset Server.SlafDevTestProdInd					= Request.SlafDevTestProdInd>
		<cfset Server.SlafLoginHostList						= Request.SlafLoginHostList>
		<cfset Server.SlafLoginInclude						= Request.SlafLoginInclude>
		<cfset Server.SlafLoginLocally						= Request.SlafLoginLocally>
		<cfset Server.SlafLoginServer						= Request.SlafLoginServer>
		<cfset Server.SlafLoginURL							= Request.SlafLoginURL>
		<cfset Server.SlafLoginWSDL							= Request.SlafLoginWSDL>
		<cfset Server.SlafMustLoginURL						= Request.SlafMustLoginURL>
		<cfset Server.SlafServerName						= Request.SlafServerName>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">
<cfset Request.SlafDefaultJaguarHost						= Request.SlafLocalHost>
<!--- Note: In the following, "EOL" is short for the platform-specific "end of line" characters that terminate text lines. --->
<cfif		FindNoCase("Windows",	Request.SlafServerOSName) GT 0>
	<cfset Request.SlafEOL									= Chr(13)&Chr(10)><!--- CRLF (Windows) --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafWindows								= "Yes">
	<cfset Request.SlafUnix									= "No">
<cfelseif	FindNoCase("Mac",		Request.SlafServerOSName) GT 0><!--- CFMX 7.01(+) supports Mac OS X(+). --->
	<!--- NOTE: The SBA doesn't have any Mac servers yet, but coding for compatibility keeps that option open. --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Same as Unix, because Mac OS X is Unix). --->
	<cfset Request.SlafMacintosh							= "Yes">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
<cfelse><!--- Because it's the "else" condition, Unix is the default: --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Unix). --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
</cfif>

<!---
If you call Now() repeatedly near midnight, it's possible for the displayed date not to match up with the displayed time. 
Therefore we grab it once and use it for all calculated date/time values. That way, they'll all match up. 
--->

<cfset Request.SlafDateTimeNow								= Now()>
<!--- Calculate current Federal Fiscal Year: --->
<cfset Request.SlafCurrFY									= Year(Request.SlafDateTimeNow)>
<cfif Month(Request.SlafDateTimeNow) GT 9>
	<cfset Request.SlafCurrFY								= Request.SlafCurrFY + 1>
</cfif>
<!--- Request.SlafDateTimeNow, formatted for display in a consistent manner: --->
<cfset Request.SlafDateTimeNowDisplay						= DateFormat(Request.SlafDateTimeNow, "YYYY-MM-DD")
															& " "
															& TimeFormat(Request.SlafDateTimeNow, "hh:mm:ss tt")>

<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<cfif	(Request.SlafDevTestProd IS "Prod")
	AND	(DateDiff("n", Request.SlafDateTimeNow, ParseDateTime("10/01/2007 6:00 AM")) GT 0)>
	<cfset Request.SlafDefaultNAICSYrNmb					= "2002">
	<cfset Request.SlafDefaultNAICSYrNmbCurr				= Request.SlafDefaultNAICSYrNmb>
	<cfset Request.SlafDefaultNAICSYrNmbPrev				= "1997">
</cfif>
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->

<cfif IsDefined("URL.RequestTimeout")>
	<cf_SetRequestTimeout seconds="#URL.RequestTimeout#">
</cfif>

<!--- SBA-Only IP Restriction, for applications that need it: --->

<cfset Request.SlafIPAddrIsNAT								= "No">
<cfset Request.SlafIPAddrIsSBA								= "No">
<cfset Variables.SlafArrayRemote_Addr						= ListToArray(CGI.Remote_Addr, ".")>
<cfset Variables.SlafArrayRemote_AddrLen					= ArrayLen(Variables.SlafArrayRemote_Addr)>
<cfif Variables.SlafArrayRemote_AddrLen IS 4>
	<cfswitch expression="#Variables.SlafArrayRemote_Addr[1]#"><!--- The cfcases are ordered numerically for easy lookup: --->
	<cfcase value="10"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfset Request.SlafIPAddrIsNAT						= "Yes">
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="63"><!--- 63.241.202.* are our proxy servers, including the VPN server. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 241)
			AND	(Variables.SlafArrayRemote_Addr[3] IS 202)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="127"><!--- Need to be at SBA to access datasources, so localhost must be SBA user. --->
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="165"><!--- According to Chuda, we own the entire 165.110 Class B network. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS "110")>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="172"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] GE  16)
			AND	(Variables.SlafArrayRemote_Addr[2] LE  31)>	<!--- 172.16/12 --->
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="192"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 168)>
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="199"><!--- According to Chuda, we own the entire 199.171.55 "Class C" subnet. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 171)
			AND	(Variables.SlafArrayRemote_Addr[3] IS  55)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="206"><!--- According to Chuda, we own the entire 206.66.57 DMZ subnet. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS  66)
			AND	(Variables.SlafArrayRemote_Addr[3] IS  57)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	</cfswitch>
</cfif>

<!--- Detect whether request was to a ColdFusion Component (CFC file): --->

<cfset Variables.PageName									= REReplace (CGI.Script_Name, ".*/", "", "ALL")>
<cfset Variables.QueryString								= "">
<!--- Remove question mark and URL parameters, if present: --->
<cfset Variables.SlafQMOffset								= Find("?", Variables.PageName)>
<cfif Variables.SlafQMOffset GT 0>
	<cfset Variables.PageName								= Left	(Variables.PageName, Variables.SlafQMOffset - 1)>
	<cfset Variables.QueryStringLen							= Len	(Variables.PageName) -	Variables.SlafQMOffset>
	<cfset Variables.QueryString							= Right	(Variables.PageName,	Variables.SlafQMOffset)>
<cfelse>
	<cfset Variables.QueryString							= CGI.Query_String>
	<cfset Variables.QueryStringLen							= Len	(Variables.QueryString)>
</cfif>
<cfset Request.SlafCFC										= "No">
<cfif	(Len(Variables.PageName) GT 4)
	AND	(CompareNoCase(Right(Variables.PageName, 4), ".cfc") IS 0)>
	<cfset Request.SlafCFC									= "Yes">
</cfif>

<!--- Self-Test Mode: --->

<cfif CGI.Script_Name IS "/library/cfincludes/get_sbashared_variables.cfm">
	<cfdump var="#Server#" label="Server">
	<cfdump var="#Request#" label="Request">
	<cfdump var="#Variables#" label="Variables">
</cfif>

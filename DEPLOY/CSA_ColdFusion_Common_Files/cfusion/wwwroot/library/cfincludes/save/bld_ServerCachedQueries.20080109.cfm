<!---
AUTHOR:				Steve Seaquist
DATE:				04/28/2006
DESCRIPTION:		Makes sure that all "server cached queries" ("Scq") variables are defined in the Server scope. 
NOTES:

	Cfinclude this file before attempting to retrieve Scq variables from the Server scope. 

	Don't be misled by this file's name. It's a "build" routine, not a "get" routine. All it does is build server cached 
	queries in the SERVER scope, and only if necessary. THIS FILE DOES NOT ACTUALLY COPY SCQ VARIABLES INTO THE VARIABLES 
	OR REQUEST SCOPE. It just makes sure they're defined so that YOU can copy them into the Variables or Request scope, 
	because only you know which ones you actually need. 

	The purpose of the "Scq" prefix (as with the Slaf prefix in SBA look-and-feel) is to avoid naming conflicts with your 
	own variables. When you copy them into the Variables or Request scope, uou can keep their names the same (to show their 
	origin) or rename them (to make your code more readable). In the following example, Server.Scq.ActvStTbl is renamed to 
	[Variables.]getStates for readability: 

		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cflock scope="SERVER" type="READONLY" timeout="30">
			<cfset getStates					= Server.Scq.ActvStTbl>
		</cflock>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use the datasource "public_sbaref" (which uses the login "sbaselect"), you cannot include this file inside 
	a cftransaction that uses a different datasource or login. But it doesn't really have to be close to the code that locks 
	the Server scope. Just include this file at the beginning of your CFM file, among its initializations. 

	Note that code and description columns are available in 2 ways: (1) as their actual database column names, and (2) as 
	"code" and "description". If you don't want to use "code" and "description" (because they aren't unique across queries), 
	then don't use them. No harm done. They exist for use by shared routines (eg: to populate a variety of drop-down menus). 

INPUT:				None. 
OUTPUT:				Server scope variables prefixed with "Scq" (short for "server cached queries"), so as not to conflict 
					with other variables defined by the application. If the current user happens to be the one who takes 
					the I/O hit to define the queries, the "Scq" variables will also be defined in the Variables scope. 
					Generally, this will only happen when this routine is executed on its own as a Scheduled Task. 
REVISION HISTORY:	01/07/2008, NNI:	Made changes to code to point to Oracle db servers. public_sbaref, pronet and partner datasources 
										have been changed to oracle_webanalytic_publicread. 
					08/20/2007, DKC:	Added support for partner database cached queries (replaces static html file used in PIMS). 
					06/04/2007, DKC:	Added VetGrpCd to the Scq.ActvVetTbl.
					04/25/2007, SRS:	Per Dileep and Sheri, saved and restored db and dbtype (if pronet is being built). 
					04/04/2007, SRS:	Per Sheri McConville, added ActvSBAOfcTbl result set. Also allowed for AllSBAOfcTbl 
										result set, but because it had 430 rows, removed it from ScqAvailableResultSetNames. 
										(That effectively comments it out / doesn't cache the query.) Added StrtDt and EndDt 
										to all previously-defined "All" result sets, if they have those columns. (It was an 
										oversight not to have included them in the first place.) 
					03/28/2007, SRS:	Per Ron Whalen, added AllPrcsMthdTbl and AllPrgrmTbl result sets for loan servicing. 
					12/07/2006, SRS:	Restricted Scq.pronet loads to explicitly named servers. (Previously, it was loaded 
										by default on all servers except devyesapp and yesapp.) 
					10/03/2006, SRS:	Special exception for Scq.pronet.ActvFrmlDtrmntnCtgryTbl extended to enile.
					09/27/2006, DKC:	Allowed it to be directly executed out of stagelibrary and build the same display.
					09/14/2006, DKC:	Added support for new table IMDomnTypTbl.
					08/11/2006, SRS:	Added support for pronet database cached queries (formerly cached in the Application 
										scope of PRO-Net when PRO-Net was not part of GLS). Also added "new logging". 
					06/14/2006, SRS:	Added 32 new cached query types. Allowed controlling what queries get defined 
										by controlling ScqAvailableResultSetNames. Per Ron Whalen, don't include Scq.ActvCMSACdTbl 
										in ScqAvailableResultSetNames, because it appear not to be in use. 
					05/12/2006, SRS:	Added IMCntryDialCd to ScqActvCountries, for use with dsp_IMCntryCdToIMCntryDialCd. 
					04/28/2006, SRS:	Original implementation. Used cfqueries initially, knowing that we will someday 
										rewrite it to use a stored procedure call (SPC) file. That's why there isn't any 
										cftry/cfcatch code. That'll be automatically added when we call the SPC file. 
--->

<!--- Configuration Variables: --->

<cfset Variables.ScqAvailableResultSetNames							=  "Scq.ActvBusAgeCdTbl"
																	& ",Scq.ActvBusTypTbl"
																	& ",Scq.ActvCalndrPrdTbl"
																	& ",Scq.ActvCitznshipCdTbl"
																	& ",Scq.ActvCohortCdTbl"
																	& ",Scq.ActvEconDevObjctCdTbl"
																	& ",Scq.ActvIMCntryCdTbl"
																	& ",Scq.ActvIMCrdtScorSourcTblBus"
																	& ",Scq.ActvIMCrdtScorSourcTblPer"
																	& ",Scq.ActvIMDomnTypTbl"
																	& ",Scq.ActvIMEPCOperCdTbl"
																	& ",Scq.ActvEthnicCdTbl"
																	& ",Scq.ActvGndrTbl"
																	& ",Scq.ActvGndrMFCdTbl"
																	& ",Scq.ActvInjctnTypCdTbl"
																	& ",Scq.ActvLoanCollatTypCdTbl"
																	& ",Scq.ActvLoanCollatValSourcTbl"
																	& ",Scq.ActvLoanCrdtUnavRsnCdTbl"
																	& ",Scq.ActvLoanFinanclStmtSourcTbl"
																	& ",Scq.ActvLoanMFDisastrTypTbl"
																	& ",Scq.ActvLoanMFPrcsMthdTypTbl"
																	& ",Scq.ActvLoanPartLendrTypTbl"
																	& ",Scq.ActvLoanPckgSourcTypTbl"
																	& ",Scq.ActvLoanPrevFinanStatTbl"
																	& ",Scq.ActvLoanProcdTypTbl"
																	& ",Scq.ActvLoanStatCdTbl"
																	& ",Scq.ActvMfSubPrgrmCdTbl"
																	& ",Scq.ActvPrcsMthdTbl"
																	& ",Scq.ActvPrgrmTbl"
																	& ",Scq.ActvPrgrmValidTbl"
																	& ",Scq.ActvRaceCdTbl"
																	& ",Scq.ActvSBAOfcTbl"
																	& ",Scq.ActvSpcPurpsLoanTbl"
																	& ",Scq.ActvStatCdTbl"
																	& ",Scq.ActvStTbl"
																	& ",Scq.ActvVetTbl"
																	& ",Scq.AllPrcsMthdTbl"
																	& ",Scq.AllPrgrmTbl">
<!---
In the following, use cfcase with multiple values, because it's more efficient than cfif OR OR OR OR, etc: 
Also, don't just name the 3 logical servers. Name all servers that end users COULD enter on the URL too: 
--->
<cfset Variables.ShortServerName									= ReplaceNoCase(CGI.Server_Name,".sba.gov","","ALL")>
<cfswitch expression="#ShortServerName#"><!--- Shortens comparisons. --->
<cfcase value="amazon,danube,dsbs,enile,eweb,eweb1,missouri,pronet,pro-net,riogrande,rouge,wocs41,yukon">
	<cfset Variables.ScqAvailableResultSetNames						= Variables.ScqAvailableResultSetNames
																	& ",Scq.pronet"
																	& ",Scq.pronet.ActvCountryCdTblForeign"
																	& ",Scq.pronet.ActvExportMainActvtyTypTbl"
																	& ",Scq.pronet.ActvExportMrktTypTbl"
																	& ",Scq.pronet.ActvFrmlDtrmntnCtgryTbl"
																	& ",Scq.pronet.ActvIMAreaTypCdTbl"
																	& ",Scq.pronet.ActvIMQAStdCdTbl"
																	& ",Scq.pronet.ActvMntrAreaTypTbl"
																	& ",Scq.pronet.AllCountryCdTbl"
																	& ",Scq.pronet.AllIMQAStdCdTbl">
</cfcase>
</cfswitch>
<cfswitch expression="#ShortServerName#"><!--- Shortens comparisons. --->
<cfcase value="danube,enile,eweb,riogrande,rouge,wocs41,yukon">
	<cfset Variables.ScqAvailableResultSetNames						= Variables.ScqAvailableResultSetNames
																	& ",Scq.partner.AllPrgrmTbl"
																	& ",Scq.partner.AllValidLocTypTbl"
																	& ",Scq.partner.AllSearchSubCatTyps">
</cfcase>
</cfswitch>

<!--- First, check the master variable that defines whether or not all the others are defined: --->

<cfset Variables.ScqIsDefined										= "No">
<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/library/cfincludes/bld_ServerCachedQueries.cfm,/stagelibrary/cfincludes/bld_ServerCachedQueries.cfm"></cfcase>
<cfdefaultcase>
	<!---
	When this file is cfincluded (the usual case), the following check will be done. When executed as a Scheduled Task, 
	however, it will not, and the queries will be refreshed from the database. This Scheduled Task is done around 5:00 AM. 
	--->
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq")>
			<cfset Variables.ScqIsDefined							= "Yes">
		</cfif>
	</cflock>
</cfdefaultcase>
</cfswitch>

<!--- Set the datasource based on actual server name: --->

<cfif NOT IsDefined("Request.SlafServerName")>
    <cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
</cfif>
<cfswitch expression="#Request.SlafServerName#">
<cfcase value="amazon,missouri">
    <cfset Variables.qrydb = "oracle_webanalytic_publicread">
</cfcase>
<cfdefaultcase>
    <cfset Variables.qrydb = "oracle_analytic_publicread">
</cfdefaultcase>
</cfswitch>

<!--- Build the queries if they don't exist, or if we were told to (by calling this page directly): --->

<cfif NOT Variables.ScqIsDefined>
	<!---
	Do the queries outside of the exclusive cflock of the Server scope, so as to minimize exclusive lock time. Also, 
	because some servers have no need of some cached queries, decfine the queries in a loop. This allows creating or 
	not creating a query simply by altering the contents of ScqAvailableResultSetNames on a server-by-server basis. 
	Also, since the caller may be using TxnErr and ErrMsg, save them (and restore them if we don't crash). 
	--->
	<cfset Variables.Scq											= StructNew()>
	<cfif IsDefined("Variables.ErrMsg")>
		<cfset Variables.ScqErrMsg									= Variables.ErrMsg>
	</cfif>
	<cfif IsDefined("Variables.TxnErr")>
		<cfset Variables.ScqTxnErr									= Variables.TxnErr>
	</cfif>
	<cfset Variables.ErrMsg											= "Problems occurred while attempting to cache database queries: ">
	<cfset Variables.TxnErr											= "No">
	<!---
	Although ScqAvailableResultSetNames is defined alphabetically (all Actvs, followed by all Alls), it's easier 
	to maintain the result sets if the same table's result sets are adjacent. So the ordering below defines 
	ActvPrgrmTbl, immediately followed by AllPrgrmTbl, for example. (Knowing this makes it easier to find stuff.) 
	--->
	<cfloop index="ScqQueryName" list="#Variables.ScqAvailableResultSetNames#">
		<cfswitch expression="#ScqQueryName#">
		<cfcase				  value="Scq.ActvBusAgeCdTbl">
			<cfquery name="Variables.Scq.ActvBusAgeCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select			BusAgeCd,
						BusAgeDesc,
						BusMFAgeCd,
						BusAgeCd									AS code,
						BusAgeDesc									AS description
			from		sbaref.BusAgeCdTbl
			where		(	(SYSDATE-BusAgeStrtDt) >= 0)
			and			(				BusAgeEndDt					is null
						or	(SYSDATE-BusAgeEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvBusTypTbl">
			<cfquery name="Variables.Scq.ActvBusTypTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		BusTypCd,
						BusTypTxt,
						BusTypMFCd,
						BusTypMFTxt,
						BusTypCd									AS code,
						BusTypTxt									AS description
			from		sbaref.BusTypTbl
			where		(	(SYSDATE-BusTypStrtDt) >= 0)
			and			(				BusTypEndDt					is null
						or	(SYSDATE-BusTypEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCalndrPrdTbl">
			<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CalndrPrdCd,
						CalndrPrdDesc,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CalndrPrdCd = 'M'						then 1
						when CalndrPrdCd = 'Q'						then 2
						when CalndrPrdCd = 'S'						then 3
						when CalndrPrdCd = 'A'						then 4
						else										5
						end											AS displayorder,
						CalndrPrdCd									AS code,
						CalndrPrdDesc								AS description
			from		sbaref.CalndrPrdTbl
			where		(	(SYSDATE-CalndrPrdStrtDt) >= 0)
			and			(				CalndrPrdEndDt				is null
						or	(SYSDATE-CalndrPrdEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCitznshipCdTbl">
			<cfquery name="Variables.Scq.ActvCitznshipCdTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CitznshipCd,
						CitznshipTxt,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CitznshipCd = 'US'						then 1
						when CitznshipCd = 'RA'						then 2
						when CitznshipCd = 'NR'						then 3
						when CitznshipCd = 'IA'						then 4
						else										5
						end											AS displayorder,
						CitznshipCd									AS code,
						CitznshipTxt								AS description
			from		sbaref.CitznshipCdTbl
			where		(	(SYSDATE-CitznshipStrtDt) >= 0)
			and			(				CitznshipEndDt				is null
						or	(SYSDATE-CitznshipEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCMSACdTbl">
			<cfquery name="Variables.Scq.ActvCMSACdTbl"					datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CMSACd,
						CMSANm,
						CMSACd										AS code,
						CMSANm										AS description
			from		sbaref.CMSACdTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCohortCdTbl">
			<cfquery name="Variables.Scq.ActvCohortCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CohortCd,
						CohortDesc,
						CohortCd									AS code,
						CohortDesc									AS description
			from		sbaref.CohortCdTbl
			where		(	(SYSDATE-CohortStrtDt) >= 0)
			and			(				CohortEndDt					is null
						or	(SYSDATE-CohortEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEconDevObjctCdTbl">
			<cfquery name="Variables.Scq.ActvEconDevObjctCdTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		EconDevObjctCd,
						EconDevObjctTxt,
						EconDevObjctCd								AS code,
						EconDevObjctTxt								AS description
			from		sbaref.EconDevObjctCdTbl
			where		(	(SYSDATE-EconDevObjctStrtDt) >= 0)
			and			(				EconDevObjctEndDt			is null
						or	(SYSDATE-EconDevObjctEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCntryCdTbl">
			<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		IMCntryCd,
						IMCntryNm,
						IMCntryDialCd,
						IMCntryCd									AS code,
						IMCntryNm									AS description
			from		sbaref.IMCntryCdTbl
			where		(	(SYSDATE-IMCntryStrtDt) >= 0)
			and			(				IMCntryEndDt				is null
						or	(SYSDATE-IMCntryEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblBus">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblBus"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		sbaref.IMCrdtScorSourcTbl
			where		(	(SYSDATE-IMCrdtScorSourcStrtDt) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(SYSDATE-IMCrdtScorSourcEndDt) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'B')
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblPer">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblPer"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
						when IMCrdtScorSourcCd = 12					then 1
						when IMCrdtScorSourcCd = 13					then 2
						when IMCrdtScorSourcCd = 11					then 3
						when IMCrdtScorSourcCd = 14					then 4
						when IMCrdtScorSourcCd = 15					then 5
						else										6
						end											AS displayorder,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		sbaref.IMCrdtScorSourcTbl
			where		(	(SYSDATE-IMCrdtScorSourcStrtDt) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(SYSDATE-IMCrdtScorSourcEndDt) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'P')
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMDomnTypTbl">
			<cfquery name="Variables.Scq.ActvIMDomnTypTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select 		IMDomnTypCd									AS code,
						IMDomnTypDescTxt							AS description
			from 		sbaref.IMDomnTypTbl
			where 		(	(SYSDATE-IMDomnTypStrtDt) >= 0)
			and			(				IMDomnTypEndDt				is null
						or	(SYSDATE-IMDomnTypEndDt) <= 0)
			order by	IMDomnTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMEPCOperCdTbl">
			<cfquery name="Variables.Scq.ActvIMEPCOperCdTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		IMEPCOperCd,
						IMEPCOperDescTxt,
						IMEPCOperCd									AS code,
						IMEPCOperDescTxt							AS description
			from		sbaref.IMEPCOperCdTbl
			where		(	(SYSDATE-IMEPCOperCdStrtDt) >= 0)
			and			(				IMEPCOperCdEndDt			is null
						or	(SYSDATE-IMEPCOperCdEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEthnicCdTbl">
			<cfquery name="Variables.Scq.ActvEthnicCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		EthnicCd,
						EthnicDesc,
						EthnicCd									AS code,
						EthnicDesc									AS description
			from		sbaref.EthnicCdTbl
			where		(	(SYSDATE-EthnicStrtDt) >= 0)
			and			(				EthnicEndDt					is null
						or	(SYSDATE-EthnicEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrTbl">
			<cfquery name="Variables.Scq.ActvGndrTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		GndrCd,
						GndrDesc,
						GndrCd										AS code,
						GndrDesc									AS description
			from		sbaref.GndrCdTbl
			where		(	(SYSDATE-GndrStrtDt) >= 0)
			and			(				GndrEndDt					is null
						or	(SYSDATE-GndrEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrMFCdTbl">
			<cfquery name="Variables.Scq.ActvGndrMFCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		GndrMFCd,
						GndrMFDescTxt,
						GndrCd,
						GndrMFCd									AS code,
						GndrMFDescTxt								AS description
			from		sbaref.GndrMFCdTbl
			where		(	(SYSDATE-GndrMFStrtDt) >= 0)
			and			(				GndrMFEndDt					is null
						or	(SYSDATE-GndrMFEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvInjctnTypCdTbl">
			<cfquery name="Variables.Scq.ActvInjctnTypCdTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		InjctnTypCd,
						InjctnTypTxt,
						case<!--- Reorder cash types first, then non-cash, then standby debt, then other: --->
						when InjctnTypCd = 'C'						then 1
						when InjctnTypCd = 'G'						then 2
						when InjctnTypCd = 'D'						then 3
						when InjctnTypCd = 'A'						then 4
						when InjctnTypCd = 'S'						then 5
						when InjctnTypCd = 'O'						then 6
						else										7
						end											AS displayorder,
						InjctnTypCd									AS code,
						InjctnTypTxt								AS description
			from		sbaref.InjctnTypCdTbl
			where		(	(SYSDATE-InjctnTypStrtDt) >= 0)
			and			(				InjctnTypEndDt				is null
						or	(SYSDATE-InjctnTypEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatTypCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatTypCdTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanCollatTypCd,
						LoanCollatTypDescTxt,
						LoanCollatTypCd								AS code,
						LoanCollatTypDescTxt						AS description
			from		sbaref.LoanCollatTypCdTbl
			where		(	(SYSDATE-LoanCollatTypStrtDt) >= 0)
			and			(				LoanCollatTypEndDt			is null
						or	(SYSDATE-LoanCollatTypEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatValSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatValSourcTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanCollatValSourcCd,
						LoanCollatValSourcDescTxt,
						LoanCollatValSourcCd						AS code,
						LoanCollatValSourcDescTxt					AS description
			from		sbaref.LoanCollatValSourcTbl
			where		(	(SYSDATE-LoanCollatValSourcStrtDt) >= 0)
			and			(				LoanCollatValSourcEndDt		is null
						or	(SYSDATE-LoanCollatValSourcEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCrdtUnavRsnCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCrdtUnavRsnCdTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanCrdtUnavRsnCd,
						LoanCrdtUnavRsnDescTxt,
						LoanCrdtUnavRsnCd							AS code,
						LoanCrdtUnavRsnDescTxt						AS description
			from		sbaref.LoanCrdtUnavRsnCdTbl
			where		(	(SYSDATE-LoanCrdtUnavRsnStrtDt) >= 0)
			and			(				LoanCrdtUnavRsnEndDt		is null
						or	(SYSDATE-LoanCrdtUnavRsnEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanFinanclStmtSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanFinanclStmtSourcTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanFinanclStmtSourcCd,
						LoanFinanclStmtSourcDescTxt,
						LoanFinanclStmtSourcCd						AS code,
						LoanFinanclStmtSourcDescTxt					AS description
			from		sbaref.LoanFinanclStmtSourcTbl
			where		(	(SYSDATE-LoanFinanclStmtSourcStrtDt) >= 0)
			and			(				LoanFinanclStmtSourcEndDt	is null
						or	(SYSDATE-LoanFinanclStmtSourcEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFDisastrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFDisastrTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanMFDisastrCd,
						LoanMFDisastrDescTxt,
						LoanMFDisastrCd								AS code,
						LoanMFDisastrDescTxt						AS description
			from		sbaref.LoanMFDisastrTypTbl
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFPrcsMthdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFPrcsMthdTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanMFPrcsMthdCd,
						LoanMFPrcsMthdDescTxt,
						LoanMFPrcsMthdCd							AS code,
						LoanMFPrcsMthdDescTxt						AS description
			from		sbaref.LoanMFPrcsMthdTypTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPartLendrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPartLendrTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanPartLendrTypCd,
						LoanPartLendrTypDescTxt,
						case<!--- Force Participating to the top and Others to the end: --->
						when LoanPartLendrTypCd = 'P'				then 1
						when LoanPartLendrTypCd = 'I'				then 2
						when LoanPartLendrTypCd = 'G'				then 3
						when LoanPartLendrTypCd = 'O'				then 4
						else										5
						end											AS displayorder,
						LoanPartLendrTypCd							AS code,
						LoanPartLendrTypDescTxt						AS description
			from		sbaref.LoanPartLendrTypTbl
			where		(	(SYSDATE-LoanPartLendrTypStrtDt) >= 0)
			and			(				LoanPartLendrTypEndDt		is null
						or	(SYSDATE-LoanPartLendrTypEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPckgSourcTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPckgSourcTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanPckgSourcTypCd,
						LoanPckgSourcTypDescTxt,
						LoanPckgSourcTypCd							AS code,
						LoanPckgSourcTypDescTxt						AS description
			from		sbaref.LoanPckgSourcTypTbl
			where		(	(SYSDATE-LoanPckgSourcTypStrtDt) >= 0)
			and			(				LoanPckgSourcTypEndDt		is null
						or	(SYSDATE-LoanPckgSourcTypEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPrevFinanStatTbl">
			<cfquery name="Variables.Scq.ActvLoanPrevFinanStatTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanPrevFinanStatCd,
						LoanPrevFinanStatDescTxt,
						LoanPrevFinanStatCd							AS code,
						LoanPrevFinanStatDescTxt					AS description
			from		sbaref.LoanPrevFinanStatTbl
			where		(	(SYSDATE-LoanPrevFinanStatStrtDt) >= 0)
			and			(				LoanPrevFinanStatEndDt		is null
						or	(SYSDATE-LoanPrevFinanStatEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanProcdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanProcdTypTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanProcdTypCd,
						LoanProcdTypDescTxt,
						ProcdTypCd,<!--- Ignoring LoanProcdTypCmnt for now, because it's always null. --->
						ProcdTypCd || LoanProcdTypCd					AS code,
						LoanProcdTypDescTxt							AS description
			from		sbaref.LoanProcdTypTbl
			where		(	(SYSDATE-LoanProcdTypCdStrtDt) >= 0)
			and			(				LoanProcdTypCdEndDt			is null
						or	(SYSDATE-LoanProcdTypCdEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanStatCdTbl">
			<cfquery name="Variables.Scq.ActvLoanStatCdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		LoanStatCd,
						LoanStatDescTxt,
						LoanStatCd									AS code,
						LoanStatDescTxt								AS description
			from		sbaref.LoanStatCdTbl
			where		(	(SYSDATE-LoanStatStrtDt) >= 0)
			and			(				LoanStatEndDt				is null
						or	(SYSDATE-LoanStatEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvMfSubPrgrmCdTbl">
			<cfquery name="Variables.Scq.ActvMfSubPrgrmCdTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		MfSubPrgrmCd,
						MfSubPrgrmDesc,
						MfSubPrgrmCd								AS code,
						MfSubPrgrmDesc								AS description
			from		sbaref.MfSubPrgrmCdTbl
			where		(	(SYSDATE-MfSubPrgrmStrtDt) >= 0)
			and			(				MfSubPrgrmEndDt				is null
						or	(SYSDATE-MfSubPrgrmEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrcsMthdTbl">
			<cfquery name="Variables.Scq.ActvPrcsMthdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description
			from		sbaref.PrcsMthdTbl
			where		(	(SYSDATE-PrcsMthdStrtDt) >= 0)
			and			(				PrcsMthdEndDt				is null
						or	(SYSDATE-PrcsMthdEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllPrcsMthdTbl">
			<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdStrtDt,
						PrcsMthdEndDt,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description
			from		sbaref.PrcsMthdTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmTbl">
			<cfquery name="Variables.Scq.ActvPrgrmTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description
			from		sbaref.PrgrmTbl
			where		(	(SYSDATE-PrgrmStrtDt) >= 0)
			and			(				PrgrmEndDt					is null
						or	(SYSDATE-PrgrmEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllPrgrmTbl">
			<cfquery name="Variables.Scq.AllPrgrmTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmStrtDt,
						PrgrmEndDt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description
			from		sbaref.PrgrmTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmValidTbl">
			<cfquery name="Variables.Scq.ActvPrgrmValidTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrgrmCd,
						PrcsMthdCd,
						SpcPurpsLoanCd,
						ProcdTypCd,
						PIMSPrgrmId,
						SubPrgrmMFCd,
						CohortCd,
						LoanMFSpcPurpsCd,
						LoanMFPrcsMthdCd,
						LoanMFSTARInd,
						LoanMFDisastrCd,
						PrgrmAuthOthAgrmtInd,
						PrgrmAuthAreaInd<!--- Because this table is never displayed, it doesn't have code and description columns. --->
			from		sbaref.PrgrmValidTbl
			where		(	(SYSDATE-PrgrmValidStrtDt) >= 0)
			and			(				PrgrmValidEndDt				is null
						or	(SYSDATE-PrgrmValidEndDt) <= 0)
			order by	PrgrmValidSeqNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvRaceCdTbl">
			<cfquery name="Variables.Scq.ActvRaceCdTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		RaceCd,
						RaceTxt,
						RaceMFCd,
						RaceMFTxt,
						RaceCd										AS code,
						RaceTxt										AS description
			from		sbaref.RaceCdTbl
			where		(	(SYSDATE-RaceStrtDt) >= 0)
			and			(				RaceEndDt					is null
						or	(SYSDATE-RaceEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvSBAOfcTbl">
			<cfquery name="Variables.Scq.ActvSBAOfcTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description
			from		sbaref.SBAOfcTbl
			where		(	(SYSDATE-SBAOfcStrtDt) >= 0)
			and			(				SBAOfcEndDt					is null
						or	(SYSDATE-SBAOfcEndDt) <= 0)
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllSBAOfcTbl">
			<cfquery name="Variables.Scq.AllSBAOfcTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcStrtDt,
						SBAOfcEndDt,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description
			from		sbaref.SBAOfcTbl
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!---
		IMRtTbl (current interest rates) vary over time, so the contents of that table aren't very static. Also, they're often 
		accessed in the past. (For example, if electronic lending has a received-date, LoanAppRecvDt, it grabs the interest rate 
		for that date, rather than today's date.) Therefore a snapshot of today's interest rates is not very useful. Even when 
		today's rate **IS** needed, a snapshot from 5:00 AM (when these cached queries are refreshed) is liable to be wrong, 
		because rates are updated during the day. Furthermore, IMRtTypTbl is generally only accessed in joins to IMRtTbl. 
		Therefore, we aren't caching that table either. 				Steve Seaquist
		--->
		<cfcase				  value="Scq.ActvSpcPurpsLoanTbl">
			<cfquery name="Variables.Scq.ActvSpcPurpsLoanTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		SpcPurpsLoanCd,
						SpcPurpsLoanDesc,
						SpcPrgrmCd,
						SpcPurpsLoanCd								AS code,
						SpcPurpsLoanDesc							AS description
			from		sbaref.SpcPurpsLoanTbl
			where		(	(SYSDATE-SpcPurpsLoanStrtDt) >= 0)
			and			(				SpcPurpsLoanEndDt			is null
						or	(SYSDATE-SpcPurpsLoanEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStatCdTbl">
			<cfquery name="Variables.Scq.ActvStatCdTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		StatCd,
						StatTxt,
						StatCdDispInd,
						StatCdDispOrdNmb							AS displayorder,
						StatCd										AS code,
						StatTxt										AS description
			from		sbaref.StatCdTbl
			where		(	(SYSDATE-StatCdStrtDt) >= 0)
			and			(				StatCdEndDt					is null
						or	(SYSDATE-StatCdEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStTbl">
			<cfquery name="Variables.Scq.ActvStTbl"					datasource="#Variables.qrydb#" dbtype="Oracle">
			select		StCd,
						StNm,
						StCd										AS code,
						StNm										AS description
			from		sbaref.StTbl
			where		(	(SYSDATE-StStrtDt) >= 0)
			and			(				StEndDt						is null
						or	(SYSDATE-StEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvVetTbl">
			<cfquery name="Variables.Scq.ActvVetTbl"				datasource="#Variables.qrydb#" dbtype="Oracle">
			select		VetCd,
						VetTxt,
						VetMFCd,
						VetMFDescTxt,
						VetGrpCd,
						VetCd										AS code,
						VetTxt										AS description
			from		sbaref.VetTbl
			where		(	(SYSDATE-VetStrtDt) >= 0)
			and			(				VetEndDt					is null
						or	(SYSDATE-VetEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet">
			<cfset Variables.Scq.pronet								= StructNew()>
			<cfif IsDefined("Variables.db")>
				<cfset Variables.ScqDB								= Variables.db>
			</cfif>
			<cfif IsDefined("Variables.dbtype")>
				<cfset Variables.ScqDBType							= Variables.dbtype>
			</cfif>
			<cfset Variables.db										= Variables.qrydb>			
			<cfset Variables.dbtype									= "Oracle">
			<cfif Variables.db eq "oracle_webanalytic_publicread">
				<cfset Variables.PNSchemaNm								= "viewpronet">
			<cfelse>
				<cfset Variables.PNSchemaNm								= "pronet">
			</cfif>
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvCountryCdTblForeign">
			<cfquery name="Variables.Scq.pronet.ActvCountryCdTblForeign"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CountryCd									code,
						CountryNm									description
			from		#Variables.PNSchemaNm#.CountryCdTbl
			where		DomForInd									> 0 /* indicating foreign */
			and			PubBanInd									= 0
			and			PvtBanInd									= 0
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllCountryCdTbl">
			<cfquery name="Variables.Scq.pronet.AllCountryCdTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CountryCd									code,
						CountryNm									description,
						DomForInd,
						PubBanInd,
						PvtBanInd
			from		#Variables.PNSchemaNm#.CountryCdTbl
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvFrmlDtrmntnCtgryTbl">
			<cfquery name="Variables.Scq.pronet.ActvFrmlDtrmntnCtgryTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		CtgryCd										code,
						CtgryNm										description
			from		#Variables.PNSchemaNm#.FrmlDtrmntnCtgryTbl
			where		(	(SYSDATE-StartDt) >= 0)
			and			(				EndDt						is null
						or	(SYSDATE-EndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMainActvtyTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMainActvtyTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		ExportMainActvtyCd							code,
						ExportMainActvtyDescTxt						description
			from		#Variables.PNSchemaNm#.ExportMainActvtyTypTbl
			order by	ExportMainActvtyCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMrktTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMrktTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		ExportMrktTypCd								code,
						ExportMrktTypDescTxt						description
			from		#Variables.PNSchemaNm#.ExportMrktTypTbl
			order by	ExportMrktTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMAreaTypCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.cfprname								= "GetIMAreaTypCdTbl">
			<cfoutput><cfinclude template="/cfincludes/oracle/#Variables.PNSchemaNm#/spc_#Ucase('IMAreaTypCdSelTSP')#.cfm"></cfoutput>
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<!--- Code and description are not returned by the stored procedure, so we need to do a query-of-queries: --->
			<cfquery name="Variables.Scq.pronet.ActvIMAreaTypCdTbl"	dbtype="query">
			select		IMAreaTypCd									code,
						IMAreaTypDescTxt							description
			from		GetIMAreaTypCdTbl
			order by	IMAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMQAStdCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.cfprname								= "GetIMQAStdCdTbl">
			<cfoutput><cfinclude template="/cfincludes/oracle/#Variables.PNSchemaNm#/spc_#UCase('IMQAStdCdSelTSP')#.cfm"></cfoutput>
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<cfquery name="Variables.Scq.pronet.ActvIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		GetIMQAStdCdTbl<!--- Eliminate deleted codes: --->
			where		IMQAStdCd									in ('B','A','I','F','E')
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllIMQAStdCdTbl">	<!--- Requires ActvIMQAStdCdTbl (for GetIMQAStdCdTbl) --->
			<cfquery name="Variables.Scq.pronet.AllIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		GetIMQAStdCdTbl
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvMntrAreaTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvMntrAreaTypTbl"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		MntrAreaTypCd								code,
						MntrAreaTypDescTxt							description
			from		#Variables.PNSchemaNm#.MntrAreaTypTbl
			order by	MntrAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
	<!--- 	& ",Scq.partner.AllPrgrmTbl"
																	& ",Scq.partner.AllValidLocTypTbl"
																	& ",Scq.partner.AllSearchSubCatTyps --->
		<cfcase				value="Scq.partner.AllPrgrmTbl">
			<cfquery name="Variables.Scq.partner.AllPrgrmTbl"			datasource="#Variables.qrydb#" dbtype="Oracle">
			select		PrgrmId,
						PrgrmDesc,
						PrgrmDocReqdInd,
						PrgrmEffDt,
						PrgrmFormId,
						ValidPrgrmTyp,
						PrgrmAreaOfOperReqdInd
			from		partner.PrgrmTbl
			where		PrgrmDispOrdNmb != 0
			order by	PrgrmDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				value="Scq.partner.AllValidLocTypTbl">
			<cfquery name="Variables.Scq.partner.AllValidLocTypTbl"		datasource="#Variables.qrydb#" dbtype="Oracle">
			select		ValidLocTyp,
						ValidLocTypDesc
			from		partner.ValidLocTypTbl
			where		ValidLocTypDispOrdNmb != 0
			order by	ValidLocTypDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				value="Scq.partner.AllSearchSubCatTyps">
			<cfquery name="Variables.Scq.partner.AllSearchSubCatTyps"	datasource="#Variables.qrydb#" dbtype="Oracle">
			select		v.ValidSBAPrtPrimCatTyp,
						c.ValidPrtPrimCatTypDesc,
						v.ValidPrtSubCatTyp,
						v.ValidPrtSubCatDesc,
						v.RolePrivilegePrgrmId
			from		partner.ValidPrtSubCatTbl v, partner.ValidPrtPrimCatTbl c 
			where		v.ValidSBAPrtPrimCatTyp = c.ValidPrtPrimCatTyp 
			order by	c.ValidPrtPrimCatDispOrdNmb, c.ValidPrtPrimCatTypDesc, v.ValidPrtSubCatDesc
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		</cfswitch>
	</cfloop>
	<cfif IsDefined("Variables.ScqDB")>
		<cfset Variables.db											= Variables.ScqDB>
	</cfif>
	<cfif IsDefined("Variables.ScqDBType")>
		<cfset Variables.dbtype										= Variables.ScqDBType>
	</cfif>
	<cfif IsDefined("Variables.ScqErrMsg")>
		<cfset Variables.ErrMsg										= Variables.ScqErrMsg>
	</cfif>
	<cfif IsDefined("Variables.ScqTxnErr")>
		<cfset Variables.TxnErr										= Variables.ScqTxnErr>
	</cfif>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.Scq											= Duplicate(Variables.Scq)>
		<cfset Server.ScqActvCountries								= Variables.Scq.ActvIMCntryCdTbl><!--- Temp!!! --->
		<cfset Server.ScqActvStates									= Variables.Scq.ActvStTbl><!--- Temp!!! --->
	</cflock>
	<cfswitch expression="#CGI.Script_Name#">
	<cfcase value="/library/cfincludes/bld_ServerCachedQueries.cfm,/stagelibrary/cfincludes/bld_ServerCachedQueries.cfm">
		<cfdump label="Server Cached Queries"						var="#Variables.Scq#">
	</cfcase>
	</cfswitch>
</cfif>

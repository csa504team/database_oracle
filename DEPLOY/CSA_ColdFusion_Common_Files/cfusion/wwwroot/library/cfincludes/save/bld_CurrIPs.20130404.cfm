<!---
AUTHOR:				Steve Seaquist
DATE:				03/02/2007
DESCRIPTION:		Defines Request.CurrIPxxx variables (logical names to control temporarily IP-restricted code). 
NOTES:				In general, code should be restricted by GLS Role. These logical names are for those occassions 
					where defining a GLS Role would be overkill, such as in Developer Utilities. Also, all of these 
					IP addresses are subject to change, hence the prefix "Curr". But then, that's exactly why we need 
					logical names. By coding to these names, rather than the actual IP address constants, we can 
					maintain the values in only one place. These names are normally tested against CGI.Remote_Addr. 
INPUT:				None. 
OUTPUT:				Request.CurrIPxxx variables. 
REVISION HISTORY:	03/13/2013, SRS:	Removed Frank, Nirish, Ramana, Sirisha and Sridhar. Changed Robert, Ron and Steve's 
										to their 8th floor IP addresses. 
					08/14/2012, SRS:	Removed Raja Bhandarkar. 
					??/??/2012, ???:	Changed Ian's primary IP from ".40.117" to ".45.3". 
					04/04/2012, SRS:	Added Sirisha, Nelli, Frank and Ramana. 
					12/16/2011, NNI:	Updated values for Nirish and Sheri. 
					03/29/2011, SRS:	Added Sridhar Ravinuthala. 
					11/15/2010 -
					12/20/2010, SRS:	Added Raja Bhandarkar (twice). 
					07/20/2010, SRS:	Edited Jame George's IP to what it is now. 
					10/14/2008, SRS:	Added Asad Chaklader and Robert Doyle. 
					09/05/2008, SRS:	Restored Jame George. 
					07/11/2007, SRS:	Added Nirish Namilae. 
					04/30,2008, SRS:	Added Matt Forman. 
					03/21,2008, SRS:	Changes to support some folks' leaving and others' returning to 4th floor. 
					05/17-21,2007, SRS:	Changes to support 5 folks' moves to Concourse level of Central Office. 
					03/02/2007, SRS:	Original implementation.
--->

<!--- Alphabetical first-then-last for easier lookup by humans: --->
<cfset Request.CurrIPAsadChaklader					= "165.110.85.128">
<cfset Request.CurrIPChuda							= "165.110.40.101">
<cfset Request.CurrIPDileep							= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPFrankMijares					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPGunitaMakkar					= "165.110.85.52">
<cfset Request.CurrIPIanClark						= "165.110.45.3">
<cfset Request.CurrIPJameGeorge						= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPMattForman						= "165.110.40.185">
<cfset Request.CurrIPNelliSalatova					= "165.110.85.55">
<cfset Request.CurrIPNirishNamilae					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRahulGundala					= "165.110.85.179">
<cfset Request.CurrIPRajaBhandarkar					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRajaBhandarkarToo				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRamanaBandreddi				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRobertDoyle					= "165.110.85.33">
<cfset Request.CurrIPRonWhalen						= "165.110.85.112">
<cfset Request.CurrIPSheriMcConville				= "165.110.85.16">
<cfset Request.CurrIPSirishaRavula					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSridharRavinuthala				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSruthiPenmetsa					= "165.110.85.34">
<cfset Request.CurrIPSteveSeaquist					= "165.110.85.129"><!--- Was 165.110.40.166 on 4th floor. --->
<cfset Request.CurrIPTimalynFranklin				= "165.110.85.158">

<!---
AUTHOR:				Jaimin Bhatt
DATE:				10/04/2002
DESCRIPTION:		Reformats Variables.Temp from database format to human format
NOTES:				None
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	10/04/2002, JBB:	Original implementation
										Created for GPTS use to add "-" to raw phone number digits 
										coming from database
--->
<CFIF Len(Variables.Temp) IS 10>
		<CFSET Variables.Temp							= Left(Variables.Temp,3) 
														& "-" 
														& Mid(Variables.Temp,4,3) 
														& "-" 
														& Right(Variables.Temp,4)>
<CFELSE>
	<CFSET Variables.Temp								= Variables.Temp>
</CFIF>

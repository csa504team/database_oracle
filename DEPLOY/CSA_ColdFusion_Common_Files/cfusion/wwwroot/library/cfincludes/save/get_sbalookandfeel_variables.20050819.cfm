<!---
AUTHOR:				Steve Seaquist
DATE:				08/19/2005
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel and cf_mainnav. 
NOTES:				<cfinclude template="get_sbalookandfeel_variables.cfm"> right after <cfapplication>. 
					See also: 
						/library/cfincludes/dsp_sbalookandfeel_variables.cfm (include to define form variables)
						/library/javascripts/SetSbalookandfeelVariables.js   (JavaScript to set form variables)
INPUT:				Variables in the Session and/or Form scopes. 
OUTPUT:				Request scope variables. Possible changes to Session scope variables. By the way, they're prefixed with 
					"Slaf" (= "sbalookandfeel"), so as not to conflict with other variables defined by the application. 
REVISION HISTORY:	08/19/2005, SRS:	Original implementation. 
--->

<!--- Predefine output variables, so that they'll be guaranteed to exist after calling this routine: --->

<cfset Request.SlafClientHeight								= ""><!--- "Slaf" = "sbalookandfeel". --->
<cfset Request.SlafClientWidth								= "">
<cfset Request.SlafResolution								= "1024x768"><!--- Always make sure it's a lowercase "x". --->
<cfset Request.SlafTextOnly									= "No"><!--- Maintained independently from the previous 3. --->

<!--- Now go get their real values from Form, URL or Session scope (whichever exists), and maintain Session scope values. --->

<cfset Variables.SessionVariablesExist						= "No">
<cftry>
	<cflock scope="SESSION" type="READONLY" timeout="30">
		<cfif IsDefined("Session.SessionId")>
			<cfset Variables.SessionVariablesExist			= "Yes">
			<cfset Variables.SlafTextChanged				= "No">
			<cfset Variables.SlafVarsChanged				= "No">
			<cfif IsDefined("Session.SlafClientHeight")><!--- If one exists, they're all 3 assumed to exist. --->
				<cfif IsDefined("Form.SlafClientHeight")>
					<cfif	(Session.SlafClientHeight		IS NOT Form.SlafClientHeight)
						OR	(Session.SlafClientWidth		IS NOT Form.SlafClientWidth)
						OR	(Session.SlafResolution			IS NOT Form.SlafResolution)>
						<cfset Request.SlafClientHeight		= Form.SlafClientHeight>
						<cfset Request.SlafClientWidth		= Form.SlafClientWidth>
						<cfset Request.SlafResolution		= Form.SlafResolution>
						<cfset Variables.SlafVarsChanged	= "Yes">
					<cfelse>
						<cfset Request.SlafClientHeight		= Session.SlafClientHeight>
						<cfset Request.SlafClientWidth		= Session.SlafClientWidth>
						<cfset Request.SlafResolution		= Session.SlafResolution>
					</cfif>
				<cfelse>
					<cfset Request.SlafClientHeight			= Session.SlafClientHeight>
					<cfset Request.SlafClientWidth			= Session.SlafClientWidth>
					<cfset Request.SlafResolution			= Session.SlafResolution>
				</cfif>
			<cfelseif IsDefined("Form.SlafClientHeight")><!--- That is, this is first time we're seeing Form variables. --->
				<cfset Request.SlafClientHeight				= Form.SlafClientHeight>
				<cfset Request.SlafClientWidth				= Form.SlafClientWidth>
				<cfset Request.SlafResolution				= Form.SlafResolution>
				<cfset Variables.SlafVarsChanged			= "Yes">
			</cfif>
			<!--- Maintain SlafTextOnly separately, because it can change independently of the other 3. --->
			<cfif IsDefined("Session.SlafTextOnly")>
				<cfif IsDefined("Form.SlafTextOnly")>
					<cfif	(Session.SlafTextOnly			IS NOT Form.SlafTextOnly)>
						<cfset Request.SlafTextOnly			= Form.SlafTextOnly>
						<cfset Variables.SlafTextChanged	= "Yes">
					<cfelse>
						<cfset Request.SlafTextOnly			= Session.SlafTextOnly>
					</cfif>
				<cfelse>
					<cfset Request.SlafTextOnly				= Session.SlafTextOnly>
				</cfif>
			<cfelseif IsDefined("Form.SlafTextOnly")><!--- That is, this is first time we're seeing Form.SlafTextOnly. --->
				<cfset Request.SlafTextOnly					= Form.SlafTextOnly>
				<cfset Variables.SlafTextChanged			= "Yes">
			</cfif>
		</cfif>
	</cflock>
	<cfcatch type="Any">
		<!--- Do nothing. If we can't lock the Session scope, that just means that there ain't no Session scope. --->
	</cfcatch>
</cftry>
<cfif Variables.SessionVariablesExist>
	<cfif Variables.SlafVarsChanged OR Variables.SlafTextChanged>
		<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
			<cfif Variables.SlafVarsChanged>
				<cfset Session.SlafClientHeight				= Request.SlafClientHeight>
				<cfset Session.SlafClientWidth				= Request.SlafClientWidth>
				<cfset Session.SlafResolution				= Request.SlafResolution>
			</cfif>
			<cfif Variables.SlafTextChanged>
				<cfset Session.SlafTextOnly					= Request.SlafTextOnly>
			</cfif>
		</cflock>
	</cfif>
<cfelse>
	<!--- Even though (here) the application isn't using SessionManagement, set Request.Slaf vars if we can: --->
	<cfif IsDefined("Form.SlafClientHeight")>
		<cfset Request.SlafClientHeight						= Form.SlafClientHeight>
		<cfset Request.SlafClientWidth						= Form.SlafClientWidth>
		<cfset Request.SlafResolution						= Form.SlafResolution>
		<cfset Request.SlafTextOnly							= Form.SlafTextOnly>
	</cfif>
</cfif>

<!--- The following is for testing and debugging. If this file is called directly (not cfincluded), dump what we did: --->

<cfif CGI.Script_Name IS "/library/cfincludes/get_sbalookandfeel_variables.cfm">
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>get_sbalookandfeel_variables.cfm</title></head>
<body bgcolor="##ffffff">

<p>Variables scope: <cfdump var="#Variables#"></p>
<p>Request scope: <cfdump var="#Request#"></p>

</body>
</html>
</cfoutput>
</cfif>

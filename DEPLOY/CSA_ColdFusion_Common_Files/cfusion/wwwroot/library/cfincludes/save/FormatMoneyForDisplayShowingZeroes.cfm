<!---
AUTHOR:				Steve Seaquist
DATE:				02/20/2001
DESCRIPTION:		Reformats Variables.Temp from database format to human format, but don't do zero suppression. 
NOTES:				None
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	02/20/2001, SRS:	Original implementation. 
--->
<CFIF Len(Variables.Temp) GT 0><!--- Leading minus if negative, blank when zero: --->
	<CFIF IsNumeric(Variables.Temp)>
		<CFIF		Variables.Temp LT 0>	<CFSET	Variables.Temp	= "-" &	DollarFormat( -	Variables.Temp)>
		<CFELSEIF	Variables.Temp GE 0>	<CFSET	Variables.Temp	=		DollarFormat(	Variables.Temp)>
		<!---<CFELSE>							<CFSET	Variables.Temp	= "$0.00">--->
		</CFIF>
	<CFELSE>								<CFSET	Variables.Temp	= "(not numeric)">
	<!--- That won't look good on a display, but it will help us to diagnose problems. --->
	</CFIF>
<CFELSE>
	<CFSET	Variables.Temp	= "">
</CFIF>

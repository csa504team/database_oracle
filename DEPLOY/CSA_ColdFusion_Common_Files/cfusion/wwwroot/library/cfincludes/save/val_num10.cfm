<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfset Variables.FVal			= Replace(Variables.FVal, ",", "", "ALL")>
	<cfif NOT IsNumeric(Variables.FVal)>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not numeric.</li>">
	<cfelseif Variables.FVal LT 0>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot be a negative number.</li>">
	<cfelseif Len(Variables.FVal) GT 10>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 10 digits.</li>">
	<cfelse>
		<cfset "Variables.#FNam#"	= NumberFormat(Variables.FVal, "0000000000")>
	</cfif>
</cfif>

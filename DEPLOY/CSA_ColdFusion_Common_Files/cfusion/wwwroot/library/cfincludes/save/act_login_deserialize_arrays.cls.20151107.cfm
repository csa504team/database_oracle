<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				10/20/2007. 
DESCRIPTION:		Deserializes Arrays and IMUserTbl after calling the wbs_login.cfc Web Service. 
NOTES:				None. 
INPUT:				Variables.CLS with serialized Arrays and IMUserTbl. 
OUTPUT:				Variables.CLS with deserialized Arrays and IMUserTbl. 
REVISION HISTORY:	11/07/2015, SRS:	Allowed the return of CLS.Systems, a tree structure equivalent of the Arrays. 
										Also, a new array CLS.FilteredRoles, and another tree structure CLS.Roles 
										inferred from CLS.Systems to minimize unnecessary traffic over the network. 
										Added HQLocId into IMUserTbl, per NS change to wbs_login.cfm on 11/05. 
					07/31/2014, SRS:	GLS-to-CLS conversion.
					03/07/2012, SRS:	Added a new IMUserTbl column for SBIC TaxId.
					02/04/2011, IAC:	Added the new four IMUserTbl columns for department and agency.
					07/02/2008, SRS:	Changes to support saving memory.
					03/07/2008, SRS:	Moved to /library/cfincludes, so that it will for-sure exist on machines that
										have to log in remotely via the Login Web Service (where it NEEDS to exist).
					10/22/2007, SRS:	Got rid of weird, improperly-deserialized Arrays that could cause other GLS
										code to error (because they don't contain enough columns).
					10/20/2007, SRS:	Original implementation.
--->

<cfif NOT IsDefined("ListToArray2DAllowingNulls")>
	<cfinclude template="/library/udf/bld_ListToArrayUDFs.cfm">
</cfif>
<cfif IsDefined("Variables.CLS.ArrayAllUserRoles")>
	<cfset Variables.CLS.ArrayAllUserRoles					= ListToArray2DAllowingNulls
															(Variables.CLS.ArrayAllUserRoles,	Chr(10), Chr(9), "null")>
	<!---
	Empty Arrays don't deserialize as empty, for some reason. Instead they deserialize with only ArrayName[1][1]
	defined and equal to the nullstring. Until the Array2DToList and ListToArray2DAllowingNulls logic is better,
	the following logic fixes that situation. We can do this automatically because we know that that situation
	would otherwise never occur in these arrays:
	--->
	<cfif	(ArrayLen(Variables.CLS.ArrayAllUserRoles)		IS 1)
		AND	(ArrayLen(Variables.CLS.ArrayAllUserRoles[1])	IS 1)
		AND	(	  Len(Variables.CLS.ArrayAllUserRoles[1][1])IS 0)>
		<cfset ArrayClear(Variables.CLS.ArrayAllUserRoles)>
	</cfif>
</cfif>
<cfif IsDefined("Variables.CLS.ArrayUserRoleBus")>
	<cfset Variables.CLS.ArrayUserRoleBus					= ListToArray2DAllowingNulls
															(Variables.CLS.ArrayUserRoleBus,	Chr(10), Chr(9), "null")>
	<cfif	(ArrayLen(Variables.CLS.ArrayUserRoleBus)		IS 1)
		AND	(ArrayLen(Variables.CLS.ArrayUserRoleBus[1])	IS 1)
		AND	(	  Len(Variables.CLS.ArrayUserRoleBus[1][1])	IS 0)>
		<cfset ArrayClear(Variables.CLS.ArrayUserRoleBus)>
	</cfif>
</cfif>
<cfif IsDefined("Variables.CLS.ArrayUserRoleLoc")>
	<cfset Variables.CLS.ArrayUserRoleLoc					= ListToArray2DAllowingNulls
															(Variables.CLS.ArrayUserRoleLoc,	Chr(10), Chr(9), "null")>
	<cfif	(ArrayLen(Variables.CLS.ArrayUserRoleLoc)		IS 1)
		AND	(ArrayLen(Variables.CLS.ArrayUserRoleLoc[1])	IS 1)
		AND	(	  Len(Variables.CLS.ArrayUserRoleLoc[1][1])	IS 0)>
		<cfset ArrayClear(Variables.CLS.ArrayUserRoleLoc)>
	</cfif>
</cfif>
<cfif IsDefined("Variables.CLS.ArrayUserRoleOfc")>
	<cfset Variables.CLS.ArrayUserRoleOfc					= ListToArray2DAllowingNulls
															(Variables.CLS.ArrayUserRoleOfc,	Chr(10), Chr(9), "null")>
	<cfif	(ArrayLen(Variables.CLS.ArrayUserRoleOfc)		IS 1)
		AND	(ArrayLen(Variables.CLS.ArrayUserRoleOfc[1])	IS 1)
		AND	(	  Len(Variables.CLS.ArrayUserRoleOfc[1][1])	IS 0)>
		<cfset ArrayClear(Variables.CLS.ArrayUserRoleOfc)>
	</cfif>
</cfif>
<cfif IsDefined("Variables.CLS.IMUserTbl")>
	<cfset Variables.CLS.ArrayIMUserTbl						= ListToArray2DAllowingNulls
															(Variables.CLS.IMUserTbl,			Chr(10), Chr(9), "null")>
	<cfset Variables.CLS.IMUserTbl							= StructNew()>
	<cfset Variables.CLS.IMUserTbl.IMUserId					= Variables.CLS.ArrayIMUserTbl[1][ 1]>
	<cfset Variables.CLS.IMUserTbl.IMUserNm					= Variables.CLS.ArrayIMUserTbl[1][ 2]>
	<cfset Variables.CLS.IMUserTbl.IMUserTypCd				= Variables.CLS.ArrayIMUserTbl[1][ 3]>
	<cfset Variables.CLS.IMUserTbl.IMUserFirstNm			= Variables.CLS.ArrayIMUserTbl[1][ 4]>
	<cfset Variables.CLS.IMUserTbl.IMUserLastNm				= Variables.CLS.ArrayIMUserTbl[1][ 5]>
	<cfset Variables.CLS.IMUserTbl.IMUserMidNm				= Variables.CLS.ArrayIMUserTbl[1][ 6]>
	<cfset Variables.CLS.IMUserTbl.IMUserSfxNm				= Variables.CLS.ArrayIMUserTbl[1][ 7]>
	<cfset Variables.CLS.IMUserTbl.IMUserPrtNm				= Variables.CLS.ArrayIMUserTbl[1][ 8]>
	<cfset Variables.CLS.IMUserTbl.IMUserDOBDt				= Variables.CLS.ArrayIMUserTbl[1][ 9]>
	<cfset Variables.CLS.IMUserTbl.IMUserLast4DgtSSNNmb		= Variables.CLS.ArrayIMUserTbl[1][10]>
	<cfset Variables.CLS.IMUserTbl.IMUserStr1Txt			= Variables.CLS.ArrayIMUserTbl[1][11]>
	<cfset Variables.CLS.IMUserTbl.IMUserStr2Txt			= Variables.CLS.ArrayIMUserTbl[1][12]>
	<cfset Variables.CLS.IMUserTbl.IMUserCtyNm				= Variables.CLS.ArrayIMUserTbl[1][13]>
	<cfset Variables.CLS.IMUserTbl.IMUserStCd				= Variables.CLS.ArrayIMUserTbl[1][14]>
	<cfset Variables.CLS.IMUserTbl.IMUserStNm				= Variables.CLS.ArrayIMUserTbl[1][15]>
	<cfset Variables.CLS.IMUserTbl.IMUserZip5Cd				= Variables.CLS.ArrayIMUserTbl[1][16]>
	<cfset Variables.CLS.IMUserTbl.IMUserZip4Cd				= Variables.CLS.ArrayIMUserTbl[1][17]>
	<cfset Variables.CLS.IMUserTbl.IMUserPostCd				= Variables.CLS.ArrayIMUserTbl[1][18]>
	<cfset Variables.CLS.IMUserTbl.IMCntryCd				= Variables.CLS.ArrayIMUserTbl[1][19]>
	<cfset Variables.CLS.IMUserTbl.IMUserPhnCntryCd			= Variables.CLS.ArrayIMUserTbl[1][20]>
	<cfset Variables.CLS.IMUserTbl.IMUserPhnAreaCd			= Variables.CLS.ArrayIMUserTbl[1][21]>
	<cfset Variables.CLS.IMUserTbl.IMUserPhnLclNmb			= Variables.CLS.ArrayIMUserTbl[1][22]>
	<cfset Variables.CLS.IMUserTbl.IMUserPhnExtnNmb			= Variables.CLS.ArrayIMUserTbl[1][23]>
	<cfset Variables.CLS.IMUserTbl.IMUserEmailAdrTxt		= Variables.CLS.ArrayIMUserTbl[1][24]>
	<cfset Variables.CLS.IMUserTbl.HQLocId					= Variables.CLS.ArrayIMUserTbl[1][25]>
	<cfset Variables.CLS.IMUserTbl.LocId					= Variables.CLS.ArrayIMUserTbl[1][26]>
	<cfset Variables.CLS.IMUserTbl.PrtId					= Variables.CLS.ArrayIMUserTbl[1][27]>
	<cfset Variables.CLS.IMUserTbl.PrtLocNm					= Variables.CLS.ArrayIMUserTbl[1][28]>
	<cfset Variables.CLS.IMUserTbl.SBAOfcCd					= Variables.CLS.ArrayIMUserTbl[1][29]>
	<cfset Variables.CLS.IMUserTbl.SBAOfc1Nm				= Variables.CLS.ArrayIMUserTbl[1][30]>
	<cfset Variables.CLS.IMUserTbl.IMAsurncLvlCd			= Variables.CLS.ArrayIMUserTbl[1][31]>
	<cfset Variables.CLS.IMUserTbl.IMUserActvInactInd		= Variables.CLS.ArrayIMUserTbl[1][32]>
	<cfset Variables.CLS.IMUserTbl.DeptId					= Variables.CLS.ArrayIMUserTbl[1][33]>
	<cfset Variables.CLS.IMUserTbl.DeptNm					= Variables.CLS.ArrayIMUserTbl[1][34]>
	<cfset Variables.CLS.IMUserTbl.AgncyId					= Variables.CLS.ArrayIMUserTbl[1][35]>
	<cfset Variables.CLS.IMUserTbl.AgncyNm					= Variables.CLS.ArrayIMUserTbl[1][36]>
	<!--- The following cfif is temporary, until all sessions using now have a 36th element. Probably 3/8/2012 only: --->
	<cfif ArrayLen(Variables.CLS.ArrayIMUserTbl[1]) gt 36>
		<cfset Variables.CLS.IMUserTbl.TaxIdOfLocIdIfSBIC	= Variables.CLS.ArrayIMUserTbl[1][37]>
	<cfelse>
		<cfset Variables.CLS.IMUserTbl.TaxIdOfLocIdIfSBIC	= "">
	</cfif>
	<cfset ArrayClear	(Variables.CLS.ArrayIMUserTbl)>
	<cfset StructDelete	(Variables.CLS, "ArrayIMUserTbl")>
</cfif>
<cfif IsDefined("Variables.CLS.FilteredRoles")>
	<cfset Variables.CLS["FilteredRoles"]					= DeserializeJSON(Variables.CLS.FilteredRoles)>
</cfif>
<cfif IsDefined("Variables.CLS.Systems")>
	<cfset Variables.CLS["Systems"]							= DeserializeJSON(Variables.CLS.Systems)>
	<cfset Variables.CLS["Roles"]							= StructNew()>
	<!--- ALDA = act_login_deserialize_arrays namespace prefix. --->
	<cfloop index="Variables.ALDA_SysNm" list="#StructKeyList(Variables.CLS.Systems)#">
		<cfset Variables.ALDA_SysStruct						= Variables.CLS.Systems[Variables.ALDA_SysNm]>
		<cfset StructAppend(Variables.CLS.Roles, Variables.ALDA_SysStruct.Roles, "Yes")>
	</cfloop>
	<cfset StructDelete(Variables, "ALDA_SysNm")>
	<cfset StructDelete(Variables, "ALDA_SysStruct")>
</cfif>

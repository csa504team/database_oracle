<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/14/2008
DESCRIPTION:		Shared routine to copy Variables.CLS into Session.CLS and assorted other Session variable names. 
NOTES:				New apps should only be using the GLS structure and its substructures, not those assorted other 
					Session variables. At some point, we would like to have only Session.CLS in the Session scope, 
					but we can't do that as long as people keep assuming that its substructures are also defined. 
INPUT:				Session.CLS. If Session.CLS has been deleted, but Variables.CLS exists, use Variables.CLS to 
					make darn sure the variables OUTSIDE of Session.CLS also got deleted. 
OUTPUT:				Clears Session.CLS and (for now) other Session variables based on Session.CLS. 
REVISION HISTORY:	06/09/2008, SRS:	Added a call to setMaxInactiveInterval(1). This times out the current session 
										after 1 second, but Session scope memory won't actually be liberated until the 
										next page request. Therefore, continue clearing Session scope variables anyway, 
										so that ColdFusion Server will get the memory back right away. 
					03/14/2008, SRS:	Original Implementation. 
--->

<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfset Session.setMaxInactiveInterval(1)>
	<cfif IsDefined("Session.CLS")>
		<cfloop index="Level1Key"			list="#StructKeyList(Session.CLS)#">
			<cfif IsStruct(Session.CLS[Level1Key])>
				<cfloop index="Level2Key"	list="#StructKeyList(Session.CLS[Level1Key])#">
					<cfset StructDelete		(Session, Level2Key)>		<!--- Compatibility var outside GLS structure. --->
				</cfloop>
				<cfset StructClear			(Session.CLS[Level1Key])>	<!--- Var you SHOULD be using. --->
			</cfif>
			<cfset StructDelete				(Session, Level1Key)>		<!--- Compatibility var outside GLS structure. --->
		</cfloop>
		<cfset StructClear					(Session.CLS)>				<!--- Var you SHOULD be using. --->
	<cfelseif IsDefined("Variables.CLS")>
		<cfloop index="Level1Key"			list="#StructKeyList(Variables.CLS)#">
			<cfif IsStruct(Variables.CLS[Level1Key])>
				<cfloop index="Level2Key"	list="#StructKeyList(Variables.CLS[Level1Key])#">
					<cfset StructDelete		(Session, Level2Key)>		<!--- Compatibility var outside GLS structure. --->
				</cfloop>
			</cfif>
			<cfset StructDelete				(Session, Level1Key)>		<!--- Compatibility var outside GLS structure. --->
		</cfloop>
	</cfif>
</cflock>

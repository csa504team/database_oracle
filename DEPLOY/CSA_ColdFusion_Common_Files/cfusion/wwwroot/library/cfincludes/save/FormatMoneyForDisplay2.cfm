<!---
AUTHOR:				Marco Ortiz
DATE:				Unknown
DESCRIPTION:		Reformats Variables.Temp from database format to human format
NOTES:				Call by many places in dsp_print_prin_per.cfm
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	12/15/2000, SRS:	Added this comment header. 
--->
<CFIF Len(Variables.TempFin[PrinIdx][FinIdx]) GT 0><!--- Leading minus if negative, blank when zero: --->
	<CFIF IsNumeric(Variables.TempFin[PrinIdx][FinIdx])>
		<CFIF		Variables.TempFin[PrinIdx][FinIdx] LT 0>	<CFSET	Variables.TempFin[PrinIdx][FinIdx]	= "-" &	DollarFormat( -	Variables.TempFin[PrinIdx][FinIdx])>
		<CFELSEIF	Variables.TempFin[PrinIdx][FinIdx] GT 0>	<CFSET	Variables.TempFin[PrinIdx][FinIdx]	=		DollarFormat(	Variables.TempFin[PrinIdx][FinIdx])>
		<CFELSE>							<CFSET	Variables.TempFin[PrinIdx][FinIdx]	= "">
		</CFIF>
	<CFELSE>								<CFSET	Variables.TempFin[PrinIdx][FinIdx]	= "(not numeric)">
	<!--- That won't look good on a display, but it will help us to diagnose problems. --->
	</CFIF>
</CFIF>		

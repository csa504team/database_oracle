<!---
AUTHOR:				Steve Seaquist
DATE:				01/15/2002
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_NUMERIC(9,3), stuffs in 999999.999 if larger. 
NOTES:				For use with act_appreview.cfm's ratio fields. (999999.999 is overflow indicator.) 
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.CVal (cleaned up FVal), Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	01/23/2002, SRS:	Corrected rounding problem. 
					01/15/2002, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFSET Variables.CVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFSET Variables.CVal						= Variables.FVal>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFSET Variables.CVal					= Replace(Variables.FVal,	",", "", "ALL")>
		<CFIF NOT IsNumeric(Variables.CVal)>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " must be numeric (commas and decimal point allowed).">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		<CFELSEIF Variables.CVal LT -999999.999>
			<CFSET Variables.CVal				= -999999.999>
		<CFELSEIF Variables.CVal GT 999999.999>
			<CFSET Variables.CVal				= 999999.999>
		<CFELSE>
			<CFSET Variables.CVal				= Round(Variables.CVal * 1000) / 1000>
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.CVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

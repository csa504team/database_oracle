<!---
AUTHOR:				Steve Seaquist
DATE:				10/20/2005
DESCRIPTION:		Shared termination routines. Will also be used in SBA logging, if enabled. 
NOTES:				None. 
INPUT:				Variables.Debug (optional), Variables.DebugMsg (optional). 
OUTPUT:				Unless told not to (that is, unless IsDefined("Variables.Debug") AND (Variables.Debug IS "Yes")), 
					turn off debug output when we reach end-of-page normally. In addition, if Variables.DebugMsg 
					is defined and not the nullstring, display it, regardless of whether we're turning debug output off. 
REVISION HISTORY:	06/20/2006, SRS:	As requested by Chuda, who wanted to be able to force a debug display externally, 
										added the ability to pull in URL.Debug. Since the IP address of the browser has 
										to be already defined in ColdFusion Administrator as a Debugging IP address, this 
										new feature does not present any security vulnerabilities. 
					10/20/2005, SRS:	Original implementation. 
--->

<cfif (NOT IsDefined("Variables.Debug")) AND IsDefined("URL.Debug")>
	<cfset Variables.Debug				= URL.Debug>
</cfif>
<cfif (NOT IsDefined("Variables.Debug")) OR (Variables.Debug IS NOT "Yes")>
	<cfsetting showdebugoutput="No">
</cfif>
<cfif IsDefined("Variables.DebugMsg") AND (Len(Variables.DebugMsg) GT 0)>
	<cfoutput>
<br><br><br><br><br><b>Variables.DebugMsg:</b><br>#Variables.DebugMsg#<br><br><br><br><br></cfoutput>
</cfif>
<cfinclude template="log_SleEnd.cfm">

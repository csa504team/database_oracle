<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Defines Request.SlafServerName and Request.SlafLocalHost. 
NOTES:

	If you cfinclude get_sbashared_variables.cfm, it will also define these 2 variables. Therefore, this is a minimal, 
	low-level routine you can call when you don't want to do everything in get_sbashared_variables. Think of this routine 
	as a stripped-down alternative to get_sbashared_variables. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

	A test version of this file resides in /library/experiments/coldfusion/get_actual_server_name.coldfusion10.cfm. If you 
	want to test out changes, it would be better to do it there first, as this file is included or implicitly included in 
	every SBA Look-and-Feel request. (Everything crashes if this file crashes.) 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. 
REVISION HISTORY:	05/22/2015, SRS:	Added URL.AllInstances code to touch flg_actual_server_name. 
					05/15/2015, SRS:	Added SlafServerRiverName and SlafServerUniqueCode to help determine SAVVIS CF Servers. 
					02/24/2014, SRS:	In preparation for ColdFusion 10, reworked how we derive SlafServerInstanceName. 
					12/07/2009, SRS:	Added Request.SlafServerStartupTime (allowing a rough estimate of CF uptime). 
					10/09/2008, SRS:	Added Request.SlafServerInstanceName for multiserver installations. On a single 
										instance multiserver installation, the new code always returns "cfusion". On a 
										standalone server installation, it always returns "coldfusion". So the new code 
										doesn't hurt anything in those situations. 
					04/27/2006, SRS:	Made self-testing. (Call as a Web page to see the variables it defines.) 
					01/31/2006, SRS:	Original implementation. 
--->

<!--- Initializations --->

<cfset		   Variables.GetActualServerSelfTestMode		= (CGI.Script_Name IS "/library/cfincludes/get_actual_server_name.cfm")>
<cfparam name="Variables.GetActualServerForceRebuild"		default="No"><!--- Allows rebuild even if not self test. --->
<cfif Variables.GetActualServerSelfTestMode>
	<cfset Variables.GetActualServerForceRebuild			= "Yes">
</cfif>
<cfif Variables.GetActualServerSelfTestMode>
	<cfif IsDefined("URL.AllInstances")>
		<cfif		FileExists("/usr/ccs/bin/touch")><!--- Solaris 11 and above --->
			<cfexecute name="/usr/ccs/bin/touch" arguments=" #ExpandPath('flg_actual_server_name.cfm')#" timeout="30" />
			<cfoutput><p>/usr/ccs/bin/touch flg_actual_server_name.cfm<br/>Done.</p></cfoutput>
		<cfelseif	FileExists("/bin/touch")><!--- Solaris 10 and below --->
			<cfexecute name="/bin/touch" arguments=" #ExpandPath('flg_actual_server_name.cfm')#" timeout="30" />
			<cfoutput><p>/bin/touch flg_actual_server_name.cfm<br/>Done.</p></cfoutput>
		<cfelse>
			<cfoutput><p>Neither /usr/ccs/bin/touch nor /bin/touch exists. Unable to perform request. </p></cfoutput>
		</cfif>
		<cfabort>
	</cfif>
</cfif>

<cfset Request.SlafServerAddr								= "">
<cfset Request.SlafServerName								= "">
<cfset Request.SlafServerInstanceName						= "">
<cfset Request.SlafServerRiverName							= ""><!--- Different from SlafServerName (ca1/ca2/ca3 stripped) --->
<cfset Request.SlafServerUniqueCode							= "">
<cfset Request.SlafServerStartupTime						= "">
<cfif NOT Variables.GetActualServerForceRebuild>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SlafServerStartupTime")>
			<cfset Request.SlafServerStartupTime			= Server.SlafServerStartupTime>
		</cfif>
		<cfif	IsDefined("Server.SlafServerAddr")
			and	IsDefined("Server.SlafServerName")
			and	IsDefined("Server.SlafServerInstanceName")>
			<cfset Request.SlafServerAddr					= Server.SlafServerAddr>
			<cfset Request.SlafServerName					= Server.SlafServerName>
			<cfset Request.SlafServerInstanceName			= Server.SlafServerInstanceName>
			<cfset Request.SlafServerRiverName				= Server.SlafServerRiverName>
			<cfset Request.SlafServerUniqueCode				= Server.SlafServerUniqueCode>
		</cfif>
	</cflock>
</cfif>
<cfif Len(Request.SlafServerStartupTime) IS 0>
	<cfset Request.SlafServerStartupTime					= Now()>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerStartupTime					= Request.SlafServerStartupTime>
	</cflock>
</cfif>
<cfif Len(Request.SlafServerInstanceName) IS 0>
	<cfset Variables.LocalHost								= CreateObject("Java", "java.net.InetAddress").getLocalHost()>
	<cfset Request.SlafServerAddr							= Trim(Variables.LocalHost.getHostAddress())>
	<cfset Request.SlafServerName							= Trim(Variables.LocalHost.getHostName())>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfset Request.SlafVersionMajor						= ListGetAt(Server.ColdFusion.ProductVersion, 1)>
	</cflock>
	<cfif IsNumeric(Request.SlafVersionMajor)>
		<cfif Request.SlafVersionMajor ge 10>
			<!--- From http://stackoverflow.com/questions/10105250/get-instance-name-in-coldfusion-10 --->
			<cfset Variables.AdminApiRuntime				= CreateObject("component", "CFIDE.adminapi.runtime")>
			<cfset Request.SlafServerInstanceName			= Trim(Variables.AdminApiRuntime.getInstanceName())>
		<cfelse>
			<cfset Variables.JRunKernel						= CreateObject("Java", "jrunx.kernel.JRun")>
			<cfset Request.SlafServerInstanceName			= Trim(Variables.JRunKernel.getServerName())>
		</cfif>
		<!--- Derive SlafServerUniqueCode based on SlafServerName and SlafServerInstanceName (already in Request scope): --->
		<cfif Len(Request.SlafServerName) gt 3>
			<cfif ListFindNoCase("ca1,ca2,ca3", Left(Request.SlafServerName, 3)) gt 0>
				<cfset Request.SlafServerRiverName			= LCase(Right	(Request.SlafServerName, Len(Request.SlafServerName)-3))>
			<cfelse>
				<cfset Request.SlafServerRiverName			= LCase			(Request.SlafServerName)><!--- CF9 servers --->
			</cfif>
			<cfswitch expression="#Request.SlafServerRiverName#"><!--- Case-insensitive compare, easy expansion. --->
			<cfcase value="rhine">	<cfset Request.SlafServerUniqueCode = "d"><!--- Deutchland, double as in catweb2. ---></cfcase>
			<cfdefaultcase>			<cfset Request.SlafServerUniqueCode = Left(Request.SlafServerRiverName, 1)></cfdefaultcase>
			</cfswitch>
			<cfset Request.SlafServerUniqueCode				&= ReplaceNoCase(Request.SlafServerInstanceName, "instance","","ONE")>
		<cfelse>
			<cfset Request.SlafServerUniqueCode				= "??"><!--- Should never happen. --->
		</cfif>
	</cfif>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerAddr						= Request.SlafServerAddr>
		<cfset Server.SlafServerName						= Request.SlafServerName>
		<cfset Server.SlafServerInstanceName				= Request.SlafServerInstanceName>
		<cfset Server.SlafServerRiverName					= Request.SlafServerRiverName>
		<cfset Server.SlafServerUniqueCode					= Request.SlafServerUniqueCode>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">
<cfset Request.SlafServerAndInstanceName					= "#Request.SlafServerName# (#Request.SlafServerInstanceName#)">

<cfif Variables.GetActualServerSelfTestMode>
	<cfoutput>
Request.SlafServerAddr = &quot;#Request.SlafServerAddr#&quot;. <br/>
Request.SlafServerName = &quot;#Request.SlafServerName#&quot;. <br/>
Request.SlafServerInstanceName = &quot;#Request.SlafServerInstanceName#&quot;. <br/>
Request.SlafServerRiverName = &quot;#Request.SlafServerRiverName#&quot;. <br/>
Request.SlafServerUniqueCode = &quot;#Request.SlafServerUniqueCode#&quot;. <br/>
Request.SlafServerStartupTime = &quot;#Request.SlafServerStartupTime#&quot;. <br/>
Request.SlafLocalHost = &quot;#Request.SlafLocalHost#&quot;. 
</cfoutput>
</cfif>

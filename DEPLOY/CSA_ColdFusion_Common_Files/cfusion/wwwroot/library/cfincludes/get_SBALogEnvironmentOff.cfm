<!---
AUTHOR:				Steve Seaquist
DATE:				07/18/2007
DESCRIPTION:		Builds fake log environment so that we are for-sure not going through log4j when logging's off. 
INPUT:				None. 
OUTPUT:				Request.SBALogger ("main logger"). 
REVISION HISTORY:	05/30/2014, SRS:	To guard against defining the same UDF twice, offloaded the actual definition of 
										SBALogDummyFunction to bld_SBALogDummyFunction. 
					07/18/2007, SRS:	Original implementation. 
--->

<cfif NOT IsDefined("SBALogDummyFunction")>
	<cfinclude template="bld_SBALogDummyFunction.cfm">
</cfif>

<!---
If logging is off, SBALogger is a Struct with dummy logging functions that we duplicate to create SleLogger. 
The dummy functions LOOK like methods of a Category/Logger object, because they're referenced the same way, 
to wit: Variables.SleLogger.debug(string). 
--->
<cfset Request.SBALogIsOff								= "Yes"><!--- Just in case it's not set to "Yes". --->
<cfset Request.SBALogger								= StructNew()>
<cfset Request.SBALogger.debug							= SBALogDummyFunction>
<cfset Request.SBALogger.info							= SBALogDummyFunction>
<cfset Request.SBALogger.warn							= SBALogDummyFunction>
<cfset Request.SBALogger.error							= SBALogDummyFunction>
<cfset Request.SBALogger.fatal							= SBALogDummyFunction>

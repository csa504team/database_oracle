<cfif CGI.Script_Name is "/library/cfincludes/bld_CurrIPs.cfm"><cfsetting enablecfoutputonly="Yes" showdebugoutput="Yes"></cfif>
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				03/02/2007. 
DESCRIPTION:		Defines Request.CurrIPxxx variables (logical names to control temporarily IP-restricted code). 
NOTES:				In general, code should be restricted by GLS Role. These logical names are for those occassions 
					where defining a GLS Role would be overkill, such as in Developer Utilities. Also, all of these 
					IP addresses are subject to change, hence the prefix "Curr". But then, that's exactly why we need 
					logical names. By coding to these names, rather than the actual IP address constants, we can 
					maintain the values in only one place. These names are normally tested against CGI.Remote_Addr. 
INPUT:				None. 
OUTPUT:				Request.CurrIPxxx variables. 
REVISION HISTORY:	07/25/2016 -		Massive changes due to netword reconfig to subnets ending in 6 and 7. Also, 
					12/02/2016, SRS:	Ron and Sheri's IPs changed. I guess they don't have reserved IPs as we devs do. 
										Added SelfTestMode if you call it directly from an SBA IP address. 
					06/03/2015 -		Massive changes due to almost all developers moving to the Concourse level. 
					11/12/2015, SRS:	No one gets a "name2" (yet) because of lack of spare data ports. 
					11/20/2014, SRS:	Changed my IP. 
					11/05/2014, NS: 	Added new IP for myself. 
					03/13/2014 -		Changed/added many addresses, since fixed IP addresses are linked to MAC address, 
					09/03/2014, SRS:	and the new Windows 7 machines have new network cards, hence new MAC addresses. 
					01/17/2013, SRS:	Changed Nelli's address. Added Melissa Kidd and Nancy Maynard. 
					05/14/2013, NNI:	Added Nirish with a new IPaddress. 
					03/13/2013, SRS:	Removed Frank, Nirish, Ramana, Sirisha and Sridhar. Changed Robert, Ron and Steve's 
										to their 8th floor IP addresses. 
					08/14/2012, SRS:	Removed Raja Bhandarkar. 
					??/??/2012, ???:	Changed Ian's primary IP from ".40.117" to ".45.3". 
					04/04/2012, SRS:	Added Sirisha, Nelli, Frank and Ramana. 
					12/16/2011, NNI:	Updated values for Nirish and Sheri. 
					03/29/2011, SRS:	Added Sridhar Ravinuthala. 
					11/15/2010 -
					12/20/2010, SRS:	Added Raja Bhandarkar (twice). 
					07/20/2010, SRS:	Edited Jame George's IP to what it is now. 
					10/14/2008, SRS:	Added Asad Chaklader and Robert Doyle. 
					09/05/2008, SRS:	Restored Jame George. 
					07/11/2007, SRS:	Added Nirish Namilae. 
					04/30,2008, SRS:	Added Matt Forman. 
					03/21,2008, SRS:	Changes to support some folks' leaving and others' returning to 4th floor. 
					05/17-21,2007, SRS:	Changes to support 5 folks' moves to Concourse level of Central Office. 
					03/02/2007, SRS:	Original implementation. 
--->

<!--- Alphabetical first-then-last for easier lookup by us developers: --->
<cfset Request.CurrIPAsadChaklader					= "165.110.6.48"><!--- win 7 --->
<cfset Request.CurrIPAsadChaklader2					= Request.CurrIPAsadChaklader><!--- win xp, disabled --->
<cfset Request.CurrIPAdamGeorge						= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPBhargavi						= "165.110.6.15">
<cfset Request.CurrIPChitra							= "165.110.6.39">
<cfset Request.CurrIPChuda							= "165.110.6.33">
<cfset Request.CurrIPDileep							= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPDanDu							= "165.110.6.70">
<cfset Request.CurrIPDurga							= "165.110.6.32">
<cfset Request.CurrIPFrankMijares					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPGunitaMakkar					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPHaseefaAboobackersideeque		= "165.110.6.46">
<cfset Request.CurrIPIanClark						= "165.110.6.8">
<cfset Request.CurrIPIkeMaduforo					= "165.110.6.17">
<cfset Request.CurrIPJameGeorge						= "165.110.87.114"><!--- New 8th floor. --->
<cfset Request.CurrIPJayJackson						= "165.110.87.117"><!--- New 8th floor. --->
<cfset Request.CurrIPMattForman						= "165.110.6.11">
<cfset Request.CurrIPMelissaKidd					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPMikhailLevin					= "165.110.6.21">
<cfset Request.CurrIPNancyMaynard					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPNancyMaynard2					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPNancyMaynard3					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPNelliSalatova					= "165.110.6.6"><!--- win 7 --->
<cfset Request.CurrIPNelliSalatova2					= Request.CurrIPNelliSalatova><!--- win xp, disabled --->
<cfset Request.CurrIPNirishNamilae					= "165.110.87.155"><!--- New 8th floor, win 7 --->
<cfset Request.CurrIPNirishNamilae2					= Request.CurrIPNirishNamilae><!--- win xp, disabled --->
<cfset Request.CurrIPNirishNamilae3					= Request.CurrIPNirishNamilae><!--- old, disabled --->
<cfset Request.CurrIPPrathyushaRepaka				= "165.110.6.44">
<cfset Request.CurrIPPriyaJayagopal					= "165.110.6.66">
<cfset Request.CurrIPRahulGundala					= "165.110.6.24">
<cfset Request.CurrIPRajaBhandarkar					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRajaBhandarkarToo				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRamanaBandreddi				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRobertDoyle					= "165.110.6.7"><!--- win 7 --->
<cfset Request.CurrIPRobertDoyle2					= Request.CurrIPRobertDoyle><!--- win xp, disabled --->
<cfset Request.CurrIPRonWhalen						= "165.110.85.112"><!--- Old 8th floor. --->
<cfset Request.CurrIPRushanAhmed					= "165.110.6.45">
<cfset Request.CurrIPSelvinInparaj					= "165.110.6.31">
<cfset Request.CurrIPSheenJustin					= "165.110.6.7">
<cfset Request.CurrIPSheriMcConville				= "doesn't want IP restricted features anymore"><!--- New 8th floor. --->
<cfset Request.CurrIPSherryLiu						= "165.110.6.36">
<cfset Request.CurrIPSirishaRavula					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSridharRavinuthala				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSruthiPenmetsa					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSteveSeaquist					= "165.110.6.27" ><!--- old win 7 --->
<cfset Request.CurrIPSteveSeaquist2					= "165.110.6.100"><!--- new win 7 --->
<cfset Request.CurrIPSushil							= "165.110.6.40">
<cfset Request.CurrIPTimalynFranklin				= "165.110.87.97"><!--- New 8th floor. --->
<cfset Request.CurrIPVidya							= "165.110.6.43">

<!---
Introducing a new approach/technique: The following allows using the same variable name to test 2 or more IP addresses. 
The previous technique (variables containing IP addresses, above, requiring comparison expressions) is now deprecated. 
Also note, the beginning of the new variable names ("Request.SlafIPAddrIs") is the same as in get_sbashared_variables 
names for OCA, OCIO and SBA. Not only is there less to type, we now have only one naming convention to remember! Yay!! 

If you have a second IP address, the following block of code is where it belongs. 
--->

<cfset Request.SlafIPAddrIsAsadChaklader			= (ListFind("#Request.CurrIPAsadChaklader#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsBhargavi					= (ListFind("#Request.CurrIPBhargavi#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsChitra					= (ListFind("#Request.CurrIPChitra#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsChuda					= (ListFind("#Request.CurrIPChuda#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsDanDu					= (ListFind("#Request.CurrIPDanDu#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsDurga					= (ListFind("#Request.CurrIPDurga#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsHaseefa					= (ListFind("#Request.CurrIPHaseefaAboobackersideeque#",CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsIanClark					= (ListFind("#Request.CurrIPIanClark#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsIkeMaduforo				= (ListFind("#Request.CurrIPIkeMaduforo#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsJameGeorge				= (ListFind("#Request.CurrIPJameGeorge#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsJayJackson				= (ListFind("#Request.CurrIPJayJackson#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsMattForman				= (ListFind("#Request.CurrIPMattForman#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsMikhailLevin				= (ListFind("#Request.CurrIPMikhailLevin#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsNelliSalatova			= (ListFind("#Request.CurrIPNelliSalatova#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsNirishNamilae			= (ListFind("#Request.CurrIPNirishNamilae#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsPrathyushaRepaka			= (ListFind("#Request.CurrIPPrathyushaRepaka#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsPriyaJayagopal			= (ListFind("#Request.CurrIPPriyaJayagopal#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsRahulGundala				= (ListFind("#Request.CurrIPRahulGundala#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsRobertDoyle				= (ListFind("#Request.CurrIPRobertDoyle#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsRonWhalen				= (ListFind("#Request.CurrIPRonWhalen#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsRushanAhmed				= (ListFind("#Request.CurrIPRushanAhmed#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSelvinInparaj			= (ListFind("#Request.CurrIPSelvinInparaj#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSheenJustin				= (ListFind("#Request.CurrIPSheenJustin#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSheriMcConville			= (ListFind("#Request.CurrIPSheriMcConville#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSherryLiu				= (ListFind("#Request.CurrIPSherryLiu#",				CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSteveSeaquist			= (ListFind("#Request.CurrIPSteveSeaquist#,#Request.CurrIPSteveSeaquist2#",
																											CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsSushil					= (ListFind("#Request.CurrIPSushil#",					CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsTimalynFranklin			= (ListFind("#Request.CurrIPTimalynFranklin#",			CGI.Remote_Addr) gt 0)>
<cfset Request.SlafIPAddrIsVidya					= (ListFind("#Request.CurrIPVidya#",					CGI.Remote_Addr) gt 0)>

<cfif CGI.Script_Name is "/library/cfincludes/bld_CurrIPs.cfm"><!--- SelfTestMode --->
	<cfif NOT IsDefined("Request.SlafIPAddrIsSBA")>
		<cfinclude template="get_sbashared_variables.cfm"><!--- Directory-relative reference. --->
	</cfif>
	<cfif Request.SlafIPAddrIsSBA><!--- Don't let folks outside the firewall see these values. --->
		<cfoutput>
<pre>
</cfoutput>
		<cfloop index="Variables.CurrIPIdx" list="#ListSort(StructKeyList(Request),'textnocase')#">
			<cfif	((Len(Variables.CurrIPIdx) gt  6)	and (CompareNoCase(Left(Variables.CurrIPIdx,  6), "CurrIP")			is 0))
				or	((Len(Variables.CurrIPIdx) gt 12)	and (CompareNoCase(Left(Variables.CurrIPIdx, 12), "SlafIPAddrIs")	is 0))>
				<cfif IsBoolean(Request[Variables.CurrIPIdx]) and Request[Variables.CurrIPIdx]>
					<cfoutput>
<b style="color:red;">Request.#Variables.CurrIPIdx# is "#Request[Variables.CurrIPIdx]#".</b></cfoutput>
				<cfelse>
					<cfoutput>
Request.#Variables.CurrIPIdx# is "#Request[Variables.CurrIPIdx]#".</cfoutput>
				</cfif>
			</cfif>
		</cfloop>
		<cfoutput>
</pre>
</cfoutput>
	<cfelse>
		<cfoutput>
Sorry.
</cfoutput>
	</cfif>
	<cfsetting showdebugoutput="No">
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Builds variables in the Variables scope that were last saved from the Form scope ("form data recovery"). 
NOTES:				Assumes a Session lock is active. This can be a ReadOnly lock. 
INPUT:				Variables in the Session scope. 
OUTPUT:				Variables in the Variables scope. 
REVISION HISTORY:	06/16/2010, SRS:	Instead of cfloop, use StructAppend for speed improvements. 
					09/26/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Session.SlafSaveFormData")>
	<!--- Do the MightNotGetSent loop before the SlafSaveForm loop, so that actual form data overrides. --->
	<cfif IsDefined("Session.SlafSaveFormData.MightNotGetSent")>
		<cfloop index="SlafKey" list="#Session.SlafSaveFormData.MightNotGetSent#">
			<cfset Variables[SlafKey]						= "">
		</cfloop>
	</cfif>
	<cfset StructAppend(Variables, Session.SlafSaveFormData, "Yes")>
</cfif>

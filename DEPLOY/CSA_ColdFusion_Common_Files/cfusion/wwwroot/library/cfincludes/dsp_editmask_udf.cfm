<cfsetting enablecfoutputonly="Yes">

<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>Test val_editmask_udf.cfm</title>
<script src="/javascripts/EditMask.js"></script>
</head>
<body bgcolor="##ffffff">

</cfoutput>

<cfif IsDefined("Form.FieldNames")>
	<cfinclude template="val_editmask_udf.cfm">
	<cfset Variables.ErrMsg			= "">
	<cfset Variables.EditMaskReturn	= EditMask(
									Form.EnglishName, 
									Form.FieldValue, 
									Form.MaskValue, 
									Form.Required, 
									Form.FieldType, 
									Form.MinLen, 
									Form.MaxLen)>

	<cfoutput>
EditMask(
"#Form.EnglishName#", 
"#Form.FieldValue#", 
"#Form.MaskValue#", 
"#Form.Required#", 
#Form.FieldType#, 
#Form.MinLen#, 
#Form.MaxLen#) returned #Variables.EditMaskReturn#.<cfif Len(Variables.ErrMsg) GT 0><p>

ErrMsg: <ul>#Variables.ErrMsg#"</ul></cfif><p>
</cfoutput>
<cfelse>
	<cfset Form.EnglishName	= "Telephone Number">
	<cfset Form.FieldValue	= "301-868-04s3">
	<cfset Form.MaskValue	= "999-999-9999">
	<cfset Form.Required	= "No">
	<cfset Form.FieldType	= "1">
	<cfset Form.MinLen		= "12">
	<cfset Form.MaxLen		= "12">
</cfif>

<cfoutput>

<center>
<form action="dsp_editmask_udf.cfm" method="post">
<table border="3">
<tr>
<td align="right">English Name:</td>
<td><input type="Text" name="EnglishName"		value="#Form.EnglishName#"></td>
</tr>
<tr>
<td align="right">Field Value:</td>
<td><input type="Text" name="FieldValue"		value="#Form.FieldValue#"></td>
</tr>
<tr>
<td align="right">Edit Mask:</td>
<td><input type="Text" name="MaskValue"			value="#Form.MaskValue#"></td>
</tr>
<tr>
<td align="right">Required?:</td>
<td><input type="Radio" name="Required"			value="Yes"<cfif Form.Required IS "Yes"> checked</cfif>> Yes
	<input type="Radio" name="Required"			value="No"<cfif Form.Required IS "No"> checked</cfif>> No
</td>
</tr>
<tr>
<td align="right">Field Type</td>
<td><input type="Radio" name="FieldType"		value="1"<cfif Form.FieldType IS "1"> checked</cfif>> Text or Textarea<br>
	<input type="Radio" name="FieldType"		value="2"<cfif Form.FieldType IS "2"> checked</cfif>> Radio Button<br>
	<input type="Radio" name="FieldType"		value="3"<cfif Form.FieldType IS "3"> checked</cfif>> Drop-Down Menu<br>
	<input type="Radio" name="FieldType"		value="4"<cfif Form.FieldType IS "4"> checked</cfif>> Hidden<br>
	<input type="Radio" name="FieldType"		value="5"<cfif Form.FieldType IS "5"> checked</cfif>> Checkbox
</td>
</tr>
<tr>
<td align="right">Min Len If Given:</td>
<td><input type="Text" name="MinLen"			value="#Form.MinLen#" size="3" maxlength="3" onChange="
	if	(!EditMask('Min Len', this.value, '9', 1, 1, 3))
		this.focus();
	">
</td>
</tr>
<tr>
<td align="right">Max Len If Given:</td>
<td><input type="Text" name="MaxLen"			value="#Form.MaxLen#" size="3" maxlength="3" onChange="
	if	(!EditMask('Max Len', this.value, '9', 1, 1, 3))
		this.focus();
	">
</td>
</tr>
<tr><td colspan="2" align="center">
	<input type="Submit" name="SubmitButton"	value="Run EditMask">
</td></tr>
</table>
</form>
</center>
</body>
</html>
</cfoutput>

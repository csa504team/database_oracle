<!---
AUTHOR:				Steve Seaquist
DATE:				06/27/2006
DESCRIPTION:		Retrieves 401 challenge data as user name and password strings. 
NOTES:
	If, after including this file, HttpHeaderUsername or HttpHeaderPassword is the nullstring, you can immediately include 
	dsp_401AuthDialog.cfm. That isn't done automatically because the 401 challenge could be requested by the Web server. 
	Example usage (using parentheses instead of angle brackets): 

					(cfinclude template="/library/cfincludes/get_401AuthData.cfm")
					(cfinclude template="/library/cfincludes/dsp_401AuthDialog.cfm")
					(!--- At this point, both HttpHeaderUsername and HttpHeaderPassword will contain data, for sure. ---)

INPUT:				HTTP request headers. 
OUTPUT:				Variables.HttpHeaderUsername, Variables.HttpHeaderPassword. 
REVISION HISTORY:	06/27/2006, SRS:	Original implementation.
--->

<cfset Variables.HttpRequestData					= GetHttpRequestData()>
<cfset Variables.HttpHeaderPassword					= "">
<cfset Variables.HttpHeaderUsername					= "">
<cfif IsDefined("Variables.HttpRequestData.headers.authorization")>
	<cfset Variables.HttpHeaderAuth					= Variables.HttpRequestData.headers.authorization>
	<cfset Variables.HttpHeaderAuthLen				= Len(Variables.HttpHeaderAuth)>
	<cfif	(Variables.HttpHeaderAuthLen			GT 6)
		AND	(Left(Variables.HttpHeaderAuth, 6)		IS "Basic ")>
		<cfset Variables.HttpHeaderByteArray		= ToBinary(Right(Variables.HttpHeaderAuth,Variables.HttpHeaderAuthLen-6))>
		<cfset Variables.HttpHeaderByteArrayLen		= ArrayLen(Variables.HttpHeaderByteArray)>
		<cfset Variables.HttpHeaderUserPass			= "">
		<cfloop index="i" from="1" to="#Variables.HttpHeaderByteArrayLen#">
			<cfset Variables.HttpHeaderUserPass		= Variables.HttpHeaderUserPass & Chr(Variables.HttpHeaderByteArray[i])>
		</cfloop>
		<cfset Variables.HttpHeaderUserPassArray	= ListToArray(Variables.HttpHeaderUserPass, ":")>
		<cfif ArrayLen(Variables.HttpHeaderUserPassArray) IS 2>
			<cfset Variables.HttpHeaderUsername		= Variables.HttpHeaderUserPassArray[1]>
			<cfset Variables.HttpHeaderPassword		= Variables.HttpHeaderUserPassArray[2]>
		</cfif>
	</cfif>
</cfif>

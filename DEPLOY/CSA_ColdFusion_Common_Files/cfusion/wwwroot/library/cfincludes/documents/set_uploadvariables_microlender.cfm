<!---
AUTHOR:				Vidya
DATE:				06/23/2016
DESCRIPTION:		Microlender system setup for file upload
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/06/2016, VRVK:(OPSMDEV-938). Modified as per new requirements ( no finalize status and HTML5 for file  															 														upload) Microlender centralized upload changes
--->
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "loandocs" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/loandocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DocSelCSP" />
<cfparam name = "Variables.goToSys"				default = "" />
<cfparam name = "Variables.SystemCode"			default = "" />

<cfif IsDefined("url.goToSys")>
	<cfset Variables.SubSystemCd = trim(url.goToSys)>
</cfif>
<cfif IsDefined("url.SystemCode")>
	<cfset Variables.SystemCode = trim(url.SystemCode)>
</cfif>

<cfset Variables.db			 		= "oracle_transaction_object">
<cfset Variables.SubSystemName 		= "Microlender">
<cfset Variables.CreatUserId 		= session.imuserid>
<cfset Variables.PageTitle			= "Microlender Upload Documents">
<cfset Variables.BusPrcsTypCd		= 5>
<cfset Variables.ShowDeleteCheck	= "Yes">
<cfset Variables.ShowFinalize		= "No">



<cfset Variables.AppSchemaName 		= "Microlender" >
<!---<cfset Variables.AppSPCUrl 			= "/cfincludes/oracle/microlender">--->
<cfset Variables.SubSystemName		= "Microlender">
<cfset Variables.PageTitle			= "Microlender Upload Documents">
<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">


	<cfset Variables.GoBackUrl	= "/mlmis/documents/dsp_centralizedupload.cfm">
<cfset Variables.checkboxname	= "DelDocId">

<!--- Get Document Types List --->
<cfset Variables.LogAct					= "get document types">
<cfset Variables.cfprname				= "Doclist">
<cfset Variables.Identifier				= "11">
<cfinclude template="/cfincludes/oracle/sbaref/spc_DOCTYPSELTSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>
<cfset Variables.BusPrcsTypCd			= 5>
<CFSET Variables.LogAct					= "find documents">
<CFSET Variables.cfprname				= "GetDocs">
<cfset Variables.Identifier				= "14"><!--- 13 --->
<cfset Variables.LoanAppNmb				= session.apphistryid>
<cfinclude template="/cfincludes/oracle/loandocs/spc_DOCSELTSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>
<cfif listfindnocase("5", Variables.BUSPRCSTYPCD)>
	<cfquery name="GetDocs" dbtype="query">
		select * from GetDocs where BUSPRCSTYPCD = <cfif Variables.BUSPRCSTYPCD eq 5>5</cfif>
	</cfquery>
</cfif>
<!--- <cfdump var="#GetDocs#"><cfabort> --->
<cfif isDefined("GetDocs.RecordCount")>
	<cfset Variables.RecordCnt			= GetDocs.RecordCount>
</cfif>

<cfif IsDefined("url.goToSys")>
	<cfset Variables.goToSys    = trim(url.goToSys)>
</cfif>
<cfif IsDefined("url.systemCode")>
	<cfset Variables.systemCode = trim(url.systemCode)>
</cfif>
<CFIF NOT IsDefined("Variables.OnRefresh")>
<cfset Variables.DocumentResult	= Variables.GetDocs>
<cfset Variables.ShowComments		= "No">
<cfinclude template = "/library/cfincludes/documents/dsp_fileupload.cfm" />
</CFIF>
<!---

AUTHOR:				Sherry Liu
DATE:				08/17/2016
DESCRIPTION:
NOTES:
INPUT:
OUTPUT:
REVISION HISTORY:	03/01/2017, SL:		Add 504 loans to display a overall progress bar chart. 
					02/16/2017, NS: 	Modified hyperlink text from "Missing Documents" to "Click here for Missing Documents" (as per NN request)
					08/17/2016, SL:		(JIRA 1047) Original Implementation
--->

<cfparam name="Variables.SubSystemCd" default="">

<cfoutput>
	<style>
    table.noborder, td.noborder {
        border: 0;
        padding: 0;
        margin:0;
    }
    ##div_totalProgBar {	     
      overflow:hidden;
      width: 50%;  
      font-weight:bold; 
	  margin: 0 auto;	  
	}
    ##div_progressBar { 
      overflow:hidden;
	  display:none;
      width: 50%;
      float:right;      
      font-weight:bold;
	  margin: 200px 50px 50px;
    }
    ##myProgress {
      position: relative;
      width: 100%;
      height: 30px;
      background-color:##999999;
    }
    
    ##myBar {
      position: absolute;
      width: 1%;
      height: 100%;
      background-color: ##66CC66;
    }
    .progress-label {
        position: absolute;
        left: 30%;
        top: 4px;
        font-weight: bold;
      }
      ##div_missingDoc {font-weight: bold; font-size:14px;}
    </style>
    
    <script>
		function Validate () {
			var	sWindowObject	= window.open("/elend/applications/dataentry/dsp_validate.cfm?loanauth=1", "newwin", "toolbar=no,scrollbars=yes,menubar=no,resizable=yes");
			if	(sWindowObject)
				sWindowObject.focus();	// Some browsers open new windows behind the current one. Bring to front.
        }
	</script>
</cfoutput>

<cfset Variables.dbtemp = Variables.db>
<cfset Variables.LogAct		 	= "call Document upload for graphic csp">
<CFSET Variables.cfprname		= "GetDocUploaded">
<cfset Variables.db 			= "oracle_housekeeping">
<cfinclude template 			= "/cfincludes/oracle/loanapp/spc_LOANDOCUPLDGRPHCSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#">
	<cfabort>
</cfif>
<cfset Variables.db = Variables.dbtemp>
<!--- <cfdump var="#GetDocUploaded#"> --->

<cfif Variables.PrgmCd IS "A">
    <cfset aDoclist = "Application, Lender Credit Memorandum, Draft Authorization,Personal Information (Owner/Operator/Guarantor), Other Processing Documents, Business Financial Statements, Affiliate Financial Statements,  Supporting Docs (Eligibility), Supporting Docs (Collateral), Supporting Docs (Other)">
    <cfset aCount = "3,3,1,3,4,9,4,3,3,1">
    
    <cfset qDoc = queryNew("Name,totalCount,uploadedPercent")>
    <cfset newRow = QueryAddRow(qDoc, 10)>
    
    <!--- convert single row query result into a list --->
    <cfset aFileLoaded = GetQueryRow (GetDocUploaded,1)>
    <cfset aCount = "">
    <cfset aFileLoadedPercent= "">
    <cfloop list="#aFileLoaded#" item="value" index="i">
    	<cfif i LTE 10>
    		<cfset aCount = listAppend(aCount,value)>            
        <cfelse>
        	<cfset aFileLoadedPercent = listAppend(aFileLoadedPercent,value)>
        </cfif>
    </cfloop>   
    <!--- Pug all values into a single query to easy use --->
    <cfloop from="1" to="10" index="i" >
        <cfset temp = QuerySetCell(qDoc, "Name", "Tab " & i & " - " & ListGetAt(aDoclist,i), i)>
        <cfset temp = QuerySetCell(qDoc, "totalCount", ListGetAt(aCount,i), i)>
        <cfset temp = QuerySetCell(qDoc, "uploadedPercent", ListGetAt(aFileLoadedPercent,i), i)>
    </cfloop>
    
    <!--- <cfdump var="#qDoc#" label="qDoc"> --->     
	<cfscript>
        function GetQueryRow(query, rowNumber) {
            var i = 0;
            var rowData = "";
            var colHeaderNames = ArrayToList(GetDocUploaded.getColumnList()); //This line is for keeping the column order same as it is in oracle table.
            var cols    = ListToArray(colHeaderNames);
            for (i = 1; i lte listlen(colHeaderNames); i = i + 1) {
                if (isNumeric(query[cols[i]][rowNumber])) {
                    newValue = numberformat(query[cols[i]][rowNumber],"__");
                    rowData = listAppend(rowData,newValue);
                }
            }
            return rowData;
        }
    </cfscript>
    
    <cfoutput>
    <script>
    function showProgress(thisValue) {
    
      thisTab = thisValue.substring(0, 6);
      thisTab = thisTab.trim();
      thisPercent = 0;
      for (i=1;i<=10;i++){
          var thisTabName = "Tab " + i;
          if (thisTab == thisTabName) {
            var aaaa = "#aFileLoadedPercent#";
            var array = aaaa.split(',');
            var thisI = i-1;
            thisPercent = array[thisI];
          }
      }
    
      document.getElementById("div_progressBar").style.display = "inline";
      document.getElementById("stepName").innerHTML = thisValue;
      document.getElementById("stepPercent").innerHTML = thisPercent + '%';
    
      var elem = document.getElementById("myBar");
      var width = 0;
      var id = setInterval(frame, 15);
      function frame() {
        if (width >= thisPercent) {
          elem.style.width = width + '%';
          clearInterval(id);
        } else {
          width++;
          elem.style.width = width + '%';
        }
      }
    }
    </script>
    
    <div style="float:left; width:100%;">
        <div style="float:left;">
            <cfchart show3d="no" format="html" showlegend="no" showborder="no" chartwidth="600" chartheight="500" URL="javascript:showProgress('$SERIESLABEL$');" fontsize="9" pieslicestyle="solid" title="" foregroundcolor="##333333" tipbgcolor="##CCCCCC">
                <cfchartseries type="pie" paintstyle="shade" serieslabel="<b>Documents needed for each tab</b>">
                    <cfloop query="qDoc">
                        <cfset thisText = ListGetAt(Name, 1, "-") & "-">
                        <cfset thisTextPart2 = ListGetAt(Name, 2, "-")>
                        <cfset thisText = thisText & "\n" & left(thisTextPart2,24)>
                        <cfif len(thisTextPart2) GT 24>
                            <cfset thisText = thisText & "\n" & mid(thisTextPart2,25,25)>
                        </cfif>
                        <cfchartdata item="#thisText#" value="#totalCount#">
                    </cfloop>
                </cfchartseries>
            </cfchart>
            <p id="div_missingDoc" style="display:inline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Validate();">Click here for Missing Documents</a></p>
        </div>
    
        <div  id="div_progressBar">
            <div class="progress-title">File(s) uploaded for <span id="stepName"></span></div><br>
            <div id="myProgress">
              <div id="myBar"></div><div class="progress-label">Document Upload Completed: <span id="stepPercent"></span></div>
            </div><br>
        </div>
    </div>
    </cfoutput>
    
<cfelseif Variables.PrgmCd IS "E">	    
	<cfoutput>	
    <p><br>
    <div id="div_totalProgBar"> 
    	<p align="center"><strong>Total document(s) uploaded: </strong>#GetDocUploaded.V_504UPLDDOCS# &nbsp; &nbsp; <strong>Total documents required: </strong>#GetDocUploaded.V_504TOTALDOCS#    	<br /><br /></p>
        <div id="myProgress">
          <div id="myBar"></div><div class="progress-label">Document Upload Completed: <span id="stepPercent"></span></div>
        </div><br>
        <p id="div_missingDoc" align="center"><a href="javascript:Validate();">Click here for Missing Documents</a></p>
    </div>
    
    <script>
	  var thisPercent = #round(GetDocUploaded.V_504DOCUPLDPCT)#;
	
      document.getElementById("stepPercent").innerHTML = thisPercent + '%';
    
      var elem = document.getElementById("myBar");
      var width = 0;
      var id = setInterval(frame, 15);
      function frame() {
        if (width >= thisPercent) {
          elem.style.width = width + '%';
          clearInterval(id);
        } else {
          width++;
          elem.style.width = width + '%';
        }
      }	
    </script>
    </cfoutput>
</cfif>
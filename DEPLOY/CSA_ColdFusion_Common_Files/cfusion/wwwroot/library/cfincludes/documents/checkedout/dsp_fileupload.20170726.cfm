<!---
	AUTHOR:				VRVK
	DESCRIPTION:		Sharable file to display uploaded documents and file upload form.
	NOTES:				Display page for all documents pending upload/delete
	INPUT:
	OUTPUT:				Display of outstanding file uploads.
	REVISION HISTORY:	02/14/2017, SMJ: (OPSMDEV-1272) Added Date document exported to Center in display
						07/27/2016, SL:  (OPSMDEV-1047) Added chart report as a cfinclude file.
						07/06/2016, VRVK:(OPSMDEV-934,OPSMDEV-938). Modified as per new requirements ( no finalize status and HTML5 for file  															 														upload) Microlender and PIMS centralized upload changes
						07/07/2016, NS:  (OPSMDEV-936,937). Fixed "View Document" link for GPTS
						07/06/2016, NS:  (OPSMDEV-936,937). Modified as per new requirements ( no finalize status and HTML5 for file upload)
										for ETRAN and GPTS
	06/17/2016, VRVK:	Original Implementation ( for SBG)
	--->
<cfparam name = "Variables.PageTitle" 	default = "#Variables.PageTitle#" />
<cfparam name = "Variables.RecCount" 	default = "0" />
<cfparam name = "Variables.RecordCnt" 	default = "0" />
<cfparam name = "Variables.systemCode" 	default = "" />
<cfparam name = "Variables.GoToSys" 	default = "" />
<cfparam name = "Variables.GoBackUrl" 	default = "" />
<cfparam name = "Variables.SystemCode" 	default = "" />
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.db"					default = "oracle_spc_caob1" />
<cfparam name = "Variables.BusPrcsTypCd"		default = "#Variables.BusPrcsTypCd#" />
<!--- limit the result set to the approprate business process --->
<cfparam name = "Variables.PrcsMthdCd"			default = "#Variables.PrcsMthdCd#" />
<cfparam name = "Variables.ShowDeleteCheck"		default = "#Variables.ShowDeleteCheck#" />
<cfparam name = "Variables.ShowComments"		default = "#Variables.ShowComments#" />
<cfparam name = "Variables.ShowFinalize"		default = "#Variables.ShowFinalize#" />
<cfparam name = "Variables.OracleSchemaName"	default = "#Variables.OracleSchemaName#" />
<cfparam name = "Variables.SPCUrl"      		default = "#Variables.SPCUrl#" />
<cfparam name = "Variables.AppSchemaName"		default = "#Variables.AppSchemaName#" />
<cfparam name = "Variables.AppSPCUrl"      		default = "#Variables.AppSPCUrl#" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "#Variables.UploadedDocsSPC#" />
<cfset Variables.RowColorB = "E7E7E7">
<cfif NOT IsDefined("spcn")>
	<cfinclude template = "/library/udf/bld_dbutils_for_dbtype.cfm" />
</cfif>

<!--- Get Documents List --->
<cfset Variables.Identifier		= "11">
<cfset Variables.LogAct		 	= "call DOCTYPSELTSP">
<cfset Variables.cfprname	 	= "Doclist">
<cfinclude template 			= "/cfincludes/oracle/sbaref/spc_DOCTYPSELTSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#">
	<cfabort>
</cfif>

<cfoutput>
	#Request.SlafHead#
	<title>
		#Variables.PageTitle#
	</title>
	#Request.SlafTopOfHead# #Request.SlafTopOfHeadJQuery#
	<cfinclude template="dsp_fileuploadscript.cfm">
	<style>
		table, th, td { border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; }
	</style>

	<body>
		<div class = "pad20">
			<h1 class = "title1" align = "center">
				#Variables.PageTitle#
			</h1>
			<form method="post" name="FormRight" onSubmit="return DoSomethingDifferentOnSubmit(this);" action="#Variables.URLToDeleteFile#">
				<input type="Hidden" name="dbtype"				value = "Oracle">
				<input type="Hidden" name="LASTUPDTUSERID"		value = "#Variables.Origuserid#">
				<input type="Hidden" name="PageNames"			value="dsp_outstanding.cfm">
				<input type="Hidden" name="username"			value = "">
				<input type="Hidden" name="password"			value="">
				<input type="hidden" name="count"				value="1"   id="count">
				<input type="hidden" name="DocDelChanged"		value="No" 	id="DocDelChanged">
				<input type="hidden" name="RecordCnt"			value="#Variables.RecordCnt#" 	id="RecordCnt">
				<div id = "displayrecords">
					<cfif IsDefined("DocumentResult")>
						<cfif DocumentResult.RecordCount GT 0>
							<table id="existingFiles"  align="center" border="1"  >
								<thead>
									<tr>
										<th  style="width: 10px" align="right">
											<p>
												Row
											</p>
										</th>
										<cfif Variables.ShowDeleteCheck>
											<th style="width: 40px" align="center">
												<p>
													Delete?
												</p>
											</th>
										</cfif>
										<th  style="width: 200px" align="center">
											<p>
												File Name
											</p>
										</th>
										<th style="width: 200px">
											<p>
												Document type
											</p>
										</th>
										<cfif Variables.ShowFinalize>
											<th style="width: 80px" align="center">
												<p>
													Finalize?
												</p>
											</th>
										</cfif>
										<cfif Variables.ShowComments>
											<th style="width: 80px" align="center">
												<p>
													Comment
												</p>
											</th>
										</cfif>
										<th style="width: 80px" align="center">
											<p>
												Uploader
											</p>
										</th>
										<th style="width: 80px" align="center">
											<p>
												Date Uploaded
											</p>
										</th>
										<cfif Variables.ShowFinalize>
											<th style="width: 80px" >
												<p>
													Upload Status
												</p>
											</th>
										</cfif>
										<cfif ListFindNoCase("Orig,Serv,Post,LANA", Variables.SubSystemName)>
											<th style="width:80px">
												Exported to Center?
											</th>
										</cfif>
									</tr>
								</thead>
								<cfset Row =0>
								<cfif IsDefined("DocumentResult") and DocumentResult.recordcount>
									<cfloop query="DocumentResult">
										<cfset row= row+1>
										<tr>
											<td align="right"  style="width: 10px" >
												#row#
											</td>
											<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",Variables.SubSystemName) gt 0>
												<cfset Variables.checkboxvalue	= DocId>
												<input type="hidden" name="DOCID#row#" value="#DocId#">
											<cfelse>
												<input type="hidden" name="DOCID1" value="1111">
												<!--- dummy data for now --->
											</cfif>
											<cfif Variables.ShowDeleteCheck>
												<td align="center" class="optlabel"  style="width: 40px">
													<label class="optlabel nowrap">
														<cfif Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "Claims" OR Variables.SubSystemName EQ "ClaimsPartner">
															<cfset Variables.checkboxvalue	= DocUpldSeqNmb>
														</cfif>
														<cfif IsDefined("Variables.checkboxvalue")>
															<cfif Variables.checkboxvalue NEQ "">
																<input type="checkbox" name="#Variables.checkboxname##row#" value="#Variables.checkboxvalue#" onClick="chk_FinalDel('#row#','Del','Final');">
																Del
															</cfif>
														</cfif>
														<cfif IsDefined("Variables.hiddenvariablename")>
															<cfif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "Claims" OR Variables.SubSystemName EQ "ClaimsPartner">
																<cfset Variables.hiddenvalue	= DocumentResult.BndAppSubNmb>
															</cfif>
															<input type="hidden" name="#Variables.hiddenvariablename##row#" value="#Variables.hiddenvalue#">
														</cfif>
													</label>
												</td>
											</cfif>
											<td>
												<cfif Variables.SubSystemName EQ "SBGBUS">
													<a href="dsp_opendoc.cfm?bus=#DocUpldSeqNmb#">
														#DocNm#
													</a>
												<cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims" OR Variables.SubSystemName EQ "ClaimsPartner">
													<a href= "dsp_opendoc.cfm?bnd=#DocUpldSeqNmb#&Sub=#DocumentResult.BndAppSubNmb#">
														#DocNm#
													</a>
												<cfelseif Variables.SubSystemName EQ "PIMS" or Variables.GoToSys eq 0>
													<!--- Pims or GPTS --->
													<a href= "dsp_dwnlddoc.cfm?DOCID=#DOCID#">
														#DocNm#
													</a>
												<cfelseif Variables.SubSystemName EQ "Microlender">
													<font size="-1">
														<a href= "/mlmis/documents/dsp_dwnldletter.cfm?DOCID=#DOCID#" target="_blank">
															#DocNm#
														</a>
													</font>
												<cfelse>
													<font size="-1">
														<a href= "dsp_dwnldletter.cfm?DOCID=#DOCID#" target="_blank">
															#DocNm#
														</a>
													</font>
												</cfif>
											</td>
											<td>
												<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",Variables.SubSystemName) gt 0>
													#TYPEDESCRIPTION#
												<cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims" OR Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ "ClaimsPartner">
													#DocBusDescTxt#
												</cfif>
											</td>
											<cfif Variables.ShowFinalize>
												<td align="center">
													<label class="optlabel nowrap"  style="width: 80px" >
														<input type="checkbox" name="Final#row#">
														Fin
													</label>
												</td>
											</cfif>
											<cfif Variables.ShowComments>
												<td align="center" style="width: 80px">
													#DocCmntTxt#
												</td>
											</cfif>
											<td align="center" style="width: 80px">
												<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",Variables.SubSystemName) gt 0>
													#LASTUPDUSERID#
												<cfelse>
													#DocUpldrNm#&nbsp;
													<cfif Ucase(trim(DocumentResult.DocUpldrUsrTyp)) EQ "C">
														[Contractor]
													<cfelseif Ucase(trim(DocumentResult.DocUpldrUsrTyp)) EQ "A">
														[Agent]
													<cfelse>
														[SBA]
													</cfif>
												</cfif>
											</td>
											<td align="center" style="width: 80px">
												#dateFormat(CreatDt,"mm/dd/yyyy")#
												<br>
												#TimeFormat(CreatDt, "hh:mm:ss tt")#&nbsp;
											</td>
											<cfif Variables.ShowFinalize>
												<td align="center" style="width: 80px">
													&nbsp;&nbsp;&nbsp;
												</td>
											</cfif>
											<cfif ListFindNoCase("Orig,Serv,Post,LANA", Variables.SubSystemName)>
												<td align="center" style="width: 80px">
													<cfif IsDefined("ExprtEndDt") and ( ExprtEndDt neq "" )>
														<br />
														#dateFormat(ExprtEndDt,"mm/dd/yyyy")#
														<br />
														#TimeFormat(ExprtEndDt, "hh:mm:ss tt")#&nbsp;
													<cfelse>
														No
													</cfif>
												</td>
											</cfif>
										</tr>
									</cfloop>
								</cfif>
								<cfset Variables.RecCount = #row#>
								<input type="hidden" name="RecCount" 		value="#RecCount#">
							</table>
							<cfif Variables.ShowDeleteCheck>
								<div align="center">
									<input type="Reset"  value="Reset">
									&nbsp;
									<input type="submit" name="DelDoc" value="Delete">
							</cfif>
							</div>
						<cfelse>
							<table align="center" width="90%">
								<tr bgcolor="#Variables.RowColorB#">
									<td colspan="5" align="center" class="mandlabel">
										No Documents uploaded yet
									</td>
								</tr>
							</table>
						</cfif>
					</cfif>
			</form>
			</div>
			<cfif listfindnocase("0,2,3,4",Variables.goToSys) gt 0>
				<br>
				<br>
				<div align="center" width="90%" border="0" align="center" class="title1" summary="Title">
					<a href="#Variables.GoBackUrl#" style="font-size:smaller; font-weight:lighter;" title="Back to Documents List">
						Back to Documents List
					</a>
				</div>
			</cfif>
			<br>
			<br>
			<cfinclude template="dsp_fileuploadform.cfm">
		</div>
	</body>
	<cfif Variables.ShowDeleteCheck and IsDefined("Variables.checkboxname")>
		<script language="JavaScript">
function DelChk(){
	var reccnt = 0;
	<cfif DocumentResult.recordcount>
		reccnt=#Variables.RecCount#;
	</cfif>
	var k = 0;
	for (var i=1; i<=reccnt; i++) {
			if (eval("document.FormRight.#Variables.checkboxname#"+i+".checked")) {
				k = 1;
			}
		}
	if (k == 0) {
	   	alert ("No document has been selected for deletion.");
		return false;
   	}else{
		var answer = confirm ("Selected documents will be deleted.\n Click on OK to continue, or CANCEL")
		if (answer){
			ColdFusion.Window.show('LoadWindow');
			return true
		}
		return false
	}

}

function DoSomethingDifferentOnSubmit	(pThis)
		{
		<cfif Variables.ShowDeleteCheck>
			DelChk();
		</cfif>
		}
</script>
	</cfif>
	</html>
</cfoutput>

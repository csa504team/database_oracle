<!---
AUTHOR:				NS
DATE:				06/23/2016
DESCRIPTION:		GPTS system setup for file upload
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/12/2016, NS:(OPSMDEV-936). Changed Variables.GoBackUrl,Variables.URLToDeleteFile
					06/23/2016, NS:(OPSMDEV- 937) GPTS centralized upload variables setup
--->
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "loandocs" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/loandocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DocSelCSP" />
<cfparam name = "Variables.goToSys"				default = "" />
<cfparam name = "Variables.SystemCode"			default = "" />

<cfif IsDefined("url.goToSys")>
	<cfset Variables.SubSystemCd 	= trim(url.goToSys)>
	<cfset Variables.goToSys    	= trim(url.goToSys)>
</cfif>
<cfif IsDefined("url.SystemCode")>
	<cfset Variables.SystemCode = trim(url.SystemCode)>
</cfif>


<cfset Variables.db			 		= "oracle_transaction_object">
<cfset Variables.SubSystemName 		= "GPTS">
<cfset Variables.CreatUserId 		= session.imuserid>
<cfset Variables.PageTitle			= "GPTS Documents for SBA Loan Number "		& Session.LoanNmb>
<cfset Variables.BusPrcsTypCd		= 4>
<cfset Variables.ShowDeleteCheck	= "Yes">
<cfset Variables.ShowComments		= "No">
<cfset Variables.ShowFinalize		= "No">



<cfset Variables.AppSchemaName 		= "GPTS" >
<cfset Variables.AppSPCUrl 			= "/cfincludes/oracle/gpts">
<cfset Variables.SubSystemName		= "GPTS">
<cfset Variables.PageTitle			= "GPTS Upload - Documents for SBA Loan Number " & Session.LoanNmb>


<cfif Variables.goToSys eq 2 and Variables.SystemCode eq 4>
	<cfset Variables.GoBackUrl			= "/elend/documents/dsp_viewdoclist.cfm?goToSys=#Variables.SubSystemCd#&SystemCode=4">
	<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#&SystemCode=4">
<cfelse>
	<cfset Variables.GoBackUrl			= "/gpts/dataentry/documents/dsp_doclist.cfm?goToSys=#Variables.SubSystemCd#">
	<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">
</cfif>

<cfset Variables.checkboxname	= "DelDocId">

<!--- Get Document Types List --->
<cfset Variables.LogAct					= "get document types">
<cfset Variables.cfprname				= "Doclist">
<cfset Variables.Identifier				= "11">
<cfinclude template="/cfincludes/oracle/sbaref/spc_DOCTYPSELTSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>

<CFSET Variables.LogAct					= "find documents">
<CFSET Variables.cfprname				= "GetDocsAll">
<cfset Variables.Identifier				= "14">
<cfset Variables.BusPrcsTypCd			= 4>
<cfinclude template="/cfincludes/oracle/loandocs/spc_DOCSELTSP.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>
<cfif listfindnocase("2,4", Variables.BUSPRCSTYPCD)>
	<cfquery name="GetDocs" dbtype="query">
		select * from GetDocsAll where BUSPRCSTYPCD = #Variables.BUSPRCSTYPCD#
	</cfquery>
</cfif>
<!--- <cfdump var="#GetDocs#"><cfabort> --->
<cfif isDefined("GetDocs.RecordCount")>
	<cfset Variables.RecordCnt			= GetDocs.RecordCount>
</cfif>

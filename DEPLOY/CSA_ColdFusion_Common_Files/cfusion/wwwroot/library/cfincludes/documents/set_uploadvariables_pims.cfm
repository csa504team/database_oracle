<!---
AUTHOR:				Vidya
DATE:				05/27/2016
DESCRIPTION:		System setup
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/07/2016, VRVK:(OPSMDEV-934). Modified as per new requirements ( no finalize status and HTML5 for file  															 														upload) PIMS centralized upload changes
--->

<cfset Variables.db					= "oracle_transaction_object">
<cfif IsDefined("Session.SubSystemName")>
<cfset Variables.SubSystemName	= Session.SubSystemName>
</cfif>
<cfif IsDefined("URL.SubSystemName")>
<cfset Variables.SubSystemName	= URL.SubSystemName>
</cfif>
<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/set_uploadvariables_pims.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">
<cfif IsDefined("Variables.SubSystemName")>
	<CFSWITCH EXPRESSION="#Variables.SubSystemName#">
		<CFCASE VALUE="PIMS">
			<cfparam name = "Variables.BusPrcsTypCd"		default = 0 /><!--- limit the result set to the approprate business process --->
			<cfparam name = "Variables.PrcsMthdCd"			default = "" />
		</CFCASE>
	</CFSWITCH>
</cfif>

<!---<cfset Variables.BndRecCount = 0>--->
	<cfset Variables.RecCount = 0>

		<cfset Variables.Identifier				= "14">
		<CFSET Variables.LogAct				= "call DOCSELTSP">
		<cfset Variables.LocId				= session.lid>
        <cfset Variables.BusPrcsTypCd			= 0>
		<CFSET Variables.cfprname			= "PimsDocSearch">
		<cfinclude template="/cfincludes/oracle/pimsdocs/spc_DOCSELTSP.PUBLIC.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		</cfif>
		<CFSET Variables.CanPimsDelete = "YES">
        <cfset Variables.ShowDeleteCheck	= Variables.CanPimsDelete>
			<CFSET Variables.CreatUserId		= Variables.origuserid>
<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">
	<cfset Variables.GoBackUrl			= "/pims/documents/dsp_centralizedupload.cfm">
	<cfset Variables.checkboxname		= "DelDocId">
<cfset Variables.tmpdb				= Variables.db />
<cfset Variables.db					= "oracle_spc_caob1" />
<cfset Variables.Identifier			= "11" />
<cfset Variables.ShowComments		= "No" /><!---
<cfset Variables.ShowDeleteCheck	= "Yes" />--->
<cfset Variables.ShowFinalize	= "No" />
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "sbaref" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSchemaName"		default = "pimsdocs" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/pimsdocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DOCSELTSP" />
<cfif IsDefined("Variables.SubSystemName") AND Variables.SubSystemName EQ "PIMS">
<cfset Variables.PageTitle			= "PIMS Upload Documents">
</cfif>
<cfif isDefined("PimsDocSearch.RecordCount")>
	<cfset Variables.RecordCnt			= PimsDocSearch.RecordCount>
</cfif>
<CFIF NOT IsDefined("Variables.OnRefresh")>
<cfset Variables.DocumentResult	= Variables.PimsDocSearch>
<cfinclude template = "/library/cfincludes/documents/dsp_fileupload.cfm" />
</CFIF>
<cfset Variables.db = Variables.tmpdb />

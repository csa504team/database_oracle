
<!---
AUTHOR:				Ian Clark
DATE:				03/15/2016
DESCRIPTION:		Form for Jquery File Upload
NOTES:				This was based on a sample of code using JQuery File Upload Library. This is the form for defining
                    what gets displayed for the files uploaded and what gets passed to the server. This requires fileUploadscript.cfm be included in the header section of the calling page.
INPUT:
OUTPUT:
REVISION HISTORY:	07/06/2016, NS  :(OPSMDEV-936,OPSMDEV-937). Modified as per new requirements ( no finalize status and HTML5 for file upload)
									ELEND and GPTS
					06/17/2016, VRVK:(OPSMDEV-903)SBG centralized upload changes
					03/15/2016 IAC Original Implementation
--->
<cfoutput>
<div class="container" >
		<form name="FileUploadData" id="FileUploadData">
			<input type="hidden" name="counter" id="counter" value="0" />
			<input type="hidden" name="ORIGUSERID" id="ORIGUSERID" value="#Variables.ORIGUSERNAME#" />
			<input type="hidden" name="SubSystemName" id="SubSystemName" value="#Variables.SubSystemName#" />
			<input type="hidden" name="CREATUSERID" id="CREATUSERID" value="#Variables.CREATUSERID#" />
			<input type="hidden" name="dbtype" id="dbtype" value="#Variables.dbtype#" />
			<input type="hidden" name="trequests" id="trequests" value="1" />
			<input type="hidden" name="fileTypCd" id="fileTypCd" value ="">
			<input type="hidden" name="FileSize" id="FileSize" value="0" />
			<input type="hidden" name="FileName" id="FileName" value="" />
			<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",trim(Variables.SubSystemName)) gt 0>
				<input type="hidden" name="BusPrcsTypCd" id="BusPrcsTypCd" value="#Variables.BusPrcsTypCd#" />
					<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(Variables.SubSystemName)) gt 0>
						<input type="hidden" name="loanappnmb"   id="loanappnmb" 	value="#Variables.CurrAppId#" />
					<cfelseif listfindnocase("PIMS",trim(Variables.SubSystemName)) gt 0>
						<input type="hidden" name="locid"   id="locid" 	value="#Variables.locid#" />
					<cfelseif listfindnocase("Microlender",trim(Variables.SubSystemName)) gt 0>
						<input type="hidden" name="loanappnmb"   id="loanappnmb" 	value="#Variables.loanappnmb#" />
					</cfif>
			</cfif>
			<cfif IsDefined("Variables.DescTxt")>
				<input type="hidden" name="DescTxt" id="DescTxt" value="#Variables.DescTxt#" />
			<cfelse>
				<input type="hidden" name="DescTxt" id="DescTxt" value="" />
			</cfif>
				<input type="hidden" name="Row" id="Row" value="0" />
			<!--- Hidden Variables set for SBG- Bond and Business, PSB and Claims--->
			<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",trim(Variables.SubSystemName)) eq 0>
				<cfset Variables.Dir = "/sbgdocs/" & "#Variables.OrigUserId#">
			<cfelseif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",trim(Variables.SubSystemName)) gt 0>
				<cfset Variables.Dir = "/applicdocs/" & "#Variables.OrigUserId#">
			</cfif>
			<cfdirectory action="list" directory="#ExpandPath( Variables.Dir )#"  name="FileListZip"/>
			<cfset Variables.DocNm  	 = FileListZip.NAME>
				<input type="hidden" name="DOCNM" id="DOCNM" value="#Variables.DOCNM#" />
			<cfif IsDefined("Variables.SubSystemName") AND (Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ 	"SBGBND" OR Variables.SubSystemName EQ "PSB")>
				<cfif IsDefined("Variables.BUSID") AND Variables.BUSID NEQ "">
					<input type="hidden" name="BUSID" id="BUSID" value="#Variables.BUSID#" />
				<cfelseif IsDefined("Session.BUSID")>
					<input type="hidden" name="BUSID" id="BUSID" value="#Session.BUSID#" />
				<cfelse>
					<input type="hidden" name="BUSID" id="BUSID" value="" />
				</cfif>
					<input type="hidden" name="FirstNm" id="FirstNm" value="#Variables.FirstNm#" />
					<input type="hidden" name="LastNm" id="LastNm" value="#Variables.LastNm#" />
				<cfif IsDefined("Variables.BNDAPPCOMNSEQNMB")>
					<input type="hidden" name="BNDAPPCOMNSEQNMB" id="BNDAPPCOMNSEQNMB" value="#Variables.BNDAPPCOMNSEQNMB#" />
				<cfelse>
					<input type="hidden" name="BNDAPPCOMNSEQNMB" id="BNDAPPCOMNSEQNMB" value="" />
				</cfif>
				<cfif IsDefined("Session.BNDAPPCOMNSEQNMB")>
					<input type="hidden" name="BNDAPPCOMNSEQNMB" id="BNDAPPCOMNSEQNMB" value="#Session.BNDAPPCOMNSEQNMB#" />
				<cfelse>
					<input type="hidden" name="BNDAPPCOMNSEQNMB" id="BNDAPPCOMNSEQNMB" value="" />
				</cfif>
				<cfif IsDefined("Variables.BNDAPPSUBNMB")>
					<input type="hidden" name="BNDAPPSUBNMB" id="BNDAPPSUBNMB" value="#Variables.BNDAPPSUBNMB#" />
				<cfelse>
					<input type="hidden" name="BNDAPPSUBNMB" id="BNDAPPSUBNMB" value="" />
				</cfif>
				<cfif IsDefined("Session.BNDAPPSUBNMB")>
					<input type="hidden" name="BNDAPPSUBNMB" id="BNDAPPSUBNMB" value="#Session.BNDAPPSUBNMB#" />
				<cfelse>
					<input type="hidden" name="BNDAPPSUBNMB" id="BNDAPPSUBNMB" value="" />
				</cfif>
					<input type="hidden" name="PrcsMthdCd" id="PrcsMthdCd" value="#Variables.PrcsMthdCd#" />
					<input type="hidden" name="DOCUPLDRUSRTYP" id="DOCUPLDRUSRTYP" value="#Variables.DOCUPLDRUSRTYP#" />
			</cfif>
	 		<cfif IsDefined("Variables.SubSystemName") AND Variables.SubSystemName EQ "Claims">
				<input type="hidden" name="FirstNm" id="FirstNm" value="#Variables.IMUSERFIRSTNM#" />
				<input type="hidden" name="LastNm" id="LastNm" value="#Variables.IMUSERLASTNM#" />
				<input type="hidden" name="SBGNmb" id="SBGNmb" value="#Variables.SBGNmb#" />
			</cfif>
	</form>
      <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="fileupload-buttonbar">
        <div class="fileupload-buttons">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button">
                   <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <input type='text' name ='index'/>
                <input id="fileupload"  type="file" name="file" multiple>
            </span>

            <div style="display:none"> <!--- create to dropdown so we can clone it and then hide it unless we want to
                                             allow for a global document type selection.   style="border: solid;border-width: thin;"--->
			<select name="DocTypeSel" id="DocTypeSel" class="DocsSel" required onChange="setUpload(this)">
				<option value="0"  selected>Select Document Type</option>
					<cfloop query="Doclist">
					<cfif Variables.SubSystemName EQ "SBGBUS" >
						<cfif trim(Doclist.DocTypCd) NEQ 590>
							<option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option>
						</cfif>
					<cfelseif Variables.SubSystemName EQ "SBGBND">
						<cfif trim(Doclist.DocTypCd) NEQ 553>
							<option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option>
						</cfif>
					<cfelseif Variables.SubSystemName EQ "PSB">
						<cfif trim(Doclist.DocTypCd) NEQ 591>
							<option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option>
						</cfif>
					<cfelseif Variables.SubSystemName EQ "Claims">
							<option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option>
					<cfelse>
							<option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option>
					</cfif>
					</cfloop>
			</select>
</div>
        <!--- At some point we will want to have an upload all button and a cancel button.
                  At the momoent these will sunbit the whole form. Which is not want we want so comment them out for now.

             <button type="submit" class="start" name="start" id="start" disabled>Start upload</button>
             <button type="reset" class="cancel">Cancel upload</button>
            --->
        </div>
    </div>
    <br>
                   <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <!---   <div class="Table">

    </div> --->
    <div id="files" class="Table">
        <div class="Heading">
            <div class="Cell1">
               <p>File Name</p>
            </div>
            <div class="Cell2">
               <p>File Size</p>
            </div>
			<cfif listfindnocase("Orig,Serv,Post,GPTS,PIMS,Microlender",trim(Variables.SubSystemName)) eq 0>
            <div class="Cell3">
               <p>Comments</p>
            </div>
			</cfif>
            <div class="Cell4">
				<p>Document type</p>
            </div>
            <div class="Cell5">
				<p></p>
            </div>
        </div>
    </div>
   <br>
</div>
</cfoutput>
<!---
AUTHOR:				Nelli Salatova
DESCRIPTION:		Delete documents page
NOTES:
INPUT:				Form variables.
OUTPUT:
REVISION HISTORY:	06/28/2016, NS:	(OPSMDEV-936). Original Implementation
--->

<!--- <cfdump var="#form#"><cfabort> --->
	<!--- <cfinclude template="act_beg.cfm"> --->
    <cfdump var="#Variables.SubSystemName#"><cfabort>
	<cfparam name="Variables.SubSystemName" default="">
	<cfif isDefined("url.SubSystemName") and len(trim(url.SubSystemName))>
		<cfset Variables.SubSystemName = url.SubSystemName>
	</cfif>
	<CFSET Variables.db						= "oracle_transaction_object">
	<CFSET Variables.dbtype					= "Oracle80">
	<cfset Variables.NewSPCURL				= "/cfincludes/oracle/loandocs">
    <cfset Variables.NewSPCURL				= "/cfincludes/oracle/pimsdocs">
	<cfset Variables.LibUdfUrl				= "/library/udf">

	<CFINCLUDE TEMPLATE="#Variables.LibUdfURL#/val_num.cfm">


	<cfparam name="Variables.PrevErr" 			default="N">
	<cfparam name="Variables.UrlAppnd" 			default="">
	<cfparam name="Variables.ErrMsg" 			default="">
	<cfparam name="Variables.Username" 			default="">
	<cfparam name="Variables.Password" 		    default="">

	<cfset Variables.SaveMe  					= "Yes">
	<cfset val_num	("RecordCnt","int","Recordcount")>

	<cfif len(Variables.RecordCnt)>
		<cfloop index="Idx" from="1" to="#Variables.RecordCnt#">
			<cfset val_num("DOCID#Idx#", "int", "Document ID",	"Yes")>
		</cfloop>
	</cfif>

	<cfif Variables.RecordCnt gt 0>
		<cfloop index="Idx" from="1" to="#Variables.RecordCnt#">
			<cfif IsDefined("Form.DELDOCID#Idx#")>
				<CFSET Variables.cfprname	 = "DOCUPDTSP">
				<cfset Variables.DOCID	 = trim(evaluate("Variables.DOCID" & Idx))>
				<cfif listfindnocase("Orig,Serv,Post,GPTS",Variables.SubSystemName) gt 0>
					<cfinclude template = "#Variables.NewSPCURL#/spc_DOCDELTSP.cfm">
				</cfif>
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe  = "No">
					<cfdump var="#Variables.ErrMsg#">
					<cfabort>
				</cfif>
			</cfif>
		</cfloop>
	</cfif>

<cfswitch EXPRESSION="#Variables.SubSystemName#">
	<cfcase value="Orig">
		<cfset Variables.UrlAppnd = "/elend/documents/dsp_outstanding1.cfm?GoToSys=1">
	</cfcase>
	<cfcase value="Serv">
		<cfset Variables.UrlAppnd= "/elend/documents/dsp_outstanding1.cfm?GoToSys=2">
	</cfcase>
	<cfcase value="Post">
		<cfset Variables.UrlAppnd = "/elend/documents/dsp_outstanding1.cfm?GoToSys=3">
	</cfcase>
	<cfcase value="GPTS">
		<cfset Variables.UrlAppnd = "/gpts/dataentry/documents/dsp_docupload.cfm?GoToSys=4">
	</cfcase>
</cfswitch>

<cfif NOT Variables.SaveMe>
	<cfset Variables.UrlAppnd = Variables.UrlAppnd & "&PrevErr=Y&Errmsg=#urlencodedformat(Variables.ErrMsg)#">
</cfif>

<cfif NOT Variables.SaveMe>
	<cfinclude template="act_saveform.cfm">
	<cflocation url="#Variables.UrlAppnd#">
<cfelse>
	<cflocation url="#Variables.UrlAppnd#">
</cfif>
<!---
AUTHOR:				NS
DATE:				06/23/2016
DESCRIPTION:		GPTS system setup for file upload
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
		REVISION HISTORY:	06/23/2016, NS:(OPSMDEV- 937)ELEND-GPTS centralized upload changes
--->
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "loandocs" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/loandocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DocSelCSP" />
<cfparam name = "Variables.goToSys"				default = "" />
<cfparam name = "Variables.SystemCode"			default = "" />

<cfif IsDefined("url.goToSys")>
	<cfset Variables.SubSystemCd = trim(url.goToSys)>
</cfif>
<cfif IsDefined("url.SystemCode")>
	<cfset Variables.SystemCode = trim(url.SystemCode)>
</cfif>


<cfset Variables.db			 		= "oracle_transaction_object">
<cfset Variables.SubSystemName 		= "GPTS">
<cfset Variables.CreatUserId 		= session.imuserid>
<cfset Variables.PageTitle			= "GPTS Documents for SBA Loan Number "		& Session.LoanNmb>
<cfset Variables.BusPrcsTypCd		= 4>
<cfset Variables.ShowDeleteCheck	= "Yes">
<cfset Variables.ShowComments		= "No">
<cfset Variables.ShowFinalize		= "No">



<cfset Variables.AppSchemaName 		= "GPTS" >
<cfset Variables.AppSPCUrl 			= "/cfincludes/oracle/gpts">
<cfset Variables.SubSystemName		= "GPTS">
<cfset Variables.PageTitle			= "GPTS Upload - Documents for SBA Loan Number " & Session.LoanNmb>
<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">

<cfif Variables.goToSys eq 2 and Variables.SystemCode eq 4>
	<cfset Variables.GoBackUrl	= "/elend/documents/dsp_viewdoclist.cfm">
<cfelse>
	<cfset Variables.GoBackUrl	= "/gpts/dataentry/documents/dsp_doclist.cfm">
</cfif>

<cfset Variables.checkboxname	= "DelDocId">

<!--- Get Document Types List --->
<cfset Variables.LogAct					= "get document types">
<cfset Variables.cfprname				= "Doclist">
<cfset Variables.Identifier				= "11">
<cfinclude template="/cfincludes/oracle/sbaref/spc_DOCTYPSELTSP.PUBLIC.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>

<CFSET Variables.LogAct					= "find documents">
<CFSET Variables.cfprname				= "GetDocs">
<cfset Variables.Identifier				= "14"><!--- 13 --->
<cfset Variables.BusPrcsTypCd			= 4>
<cfinclude template="/cfincludes/oracle/loandocs/spc_DOCSELTSP.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#"><cfabort>
</cfif>
<cfif listfindnocase("2,3,4", Variables.BUSPRCSTYPCD)>
	<cfquery name="GetDocs" dbtype="query">
		select * from GetDocs where BUSPRCSTYPCD = <cfif Variables.BUSPRCSTYPCD eq 2>2<cfelseif Variables.BUSPRCSTYPCD eq 3>3<cfelse>4</cfif>
	</cfquery>
</cfif>
<!--- <cfdump var="#GetDocs#"><cfabort> --->
<cfif isDefined("GetDocs.RecordCount")>
	<cfset Variables.RecordCnt			= GetDocs.RecordCount>
</cfif>

<cfif IsDefined("url.goToSys")>
	<cfset Variables.goToSys    = trim(url.goToSys)>
</cfif>
<cfif IsDefined("url.systemCode")>
	<cfset Variables.systemCode = trim(url.systemCode)>
</cfif>

		<!---REVISION HISTORY:	06/17/2016, VRVK:(OPSMDEV-903)SBG centralized upload changes--->


<cfparam name = "Variables.PageTitle" default = "#Variables.PageTitle#" />
<cfparam name = "Variables.RecCount" default = "0" />

<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.db"					default = "oracle_spc_caob1" />
<cfparam name = "Variables.BusPrcsTypCd"		default = "#Variables.BusPrcsTypCd#" /><!--- limit the result set to the approprate business process --->
<cfparam name = "Variables.PrcsMthdCd"			default = "#Variables.PrcsMthdCd#" />

<cfparam name = "Variables.ShowDeleteCheck"		default = "#Variables.ShowDeleteCheck#" />
<cfparam name = "Variables.ShowComments"		default = "#Variables.ShowComments#" />
<cfparam name = "Variables.ShowFinalize"		default = "#Variables.ShowFinalize#" />

<cfparam name = "Variables.OracleSchemaName"	default = "#Variables.OracleSchemaName#" />
<cfparam name = "Variables.SPCUrl"      		default = "#Variables.SPCUrl#" />
<cfparam name = "Variables.AppSchemaName"		default = "#Variables.AppSchemaName#" />
<cfparam name = "Variables.AppSPCUrl"      		default = "#Variables.AppSPCUrl#" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "#Variables.UploadedDocsSPC#" />
<cfset Variables.RowColorB = "E7E7E7">
<cfif NOT IsDefined("spcn")>
	<cfinclude template = "/library/udf/bld_dbutils_for_dbtype.cfm" />
</cfif>

<!--- Get Documents List --->
<cfset Variables.Identifier		= "11"><!--- new --->
<cfset Variables.LogAct		 	= "call DOCTYPSELTSP">
<cfset Variables.cfprname	 	= "Doclist">
<cfinclude template 			= "#spcn('DOCTYPSELTSP.PUBLIC', 'sbaref')#" />
<cfif Variables.TxnErr>
	#Variables.ErrMsg#<cfabort>
</cfif>

<cfoutput>
#Request.SlafHead#
<title>#Variables.PageTitle#</title>
#Request.SlafTopOfHead#
#Request.SlafTopOfHeadJQuery#

<cfinclude template="dsp_fileuploadscript.cfm">

<style>
	table, th, td {
    	border: 1px solid black;
    	border-collapse: collapse;
	    padding: 5px;
	    text-align: center;
	}
</style>
<!---<cfdump var="#DocumentResult#"> <cfabort> --->
<body>
	<div class = "pad20">
		<h1 class = "title1" align = "center">
			#Variables.PageTitle#
		</h1>
<form method="post" name="FormRight" onSubmit="return DoSomethingDifferentOnSubmit(this);" action="#Variables.URLToDeleteFile#">											 			<input type="Hidden" name="dbtype"				value = "Oracle">
			<input type="Hidden" name="LASTUPDTUSERID"		value = "#Variables.Origuserid#">
			<input type="Hidden" name="username"			value = "#Variables.username#">
			<input type="Hidden" name="password"			value = "#Variables.password#">
			<input type="Hidden" name="PageNames"			value="dsp_outstanding.cfm">
			<input type="hidden" name="count"				value="1"   id="count">
			<input type="hidden" name="DocTypCdChanged"		value="No" 	id="DocTypCdChanged">
			<input type="hidden" name="DocFinalChanged"		value="No" 	id="DocFinalChanged">
			<input type="hidden" name="DocDelChanged"		value="No" 	id="DocDelChanged">
			<input type="hidden" name="DocTypCdChanged1"	value="No" id="DocTypCdChanged1">
			<input type="hidden" name="DocFinalChanged1"	value="No" id="DocFinalChanged1">
			<input type="hidden" name="DocDelChanged1"		value="No" id="DocDelChanged1">
    <div id = "displayrecords">
    <cfif IsDefined("DocumentResult")> <cfif DocumentResult.RecordCount GT 0>
	<table id="existingFiles"  align="center" border="1"  >
					<thead>
						<tr>
							<th  style="width: 10px" align="right">
								<p>Row</p>
    						</th>
							<cfif Variables.ShowDeleteCheck>
								<th style="width: 40px" align="center">
					    	        <p>Delete?</p>
						        </th>
							</cfif>
							<th  style="width: 200px" align="center">
								<p>File Name</p>
							</th>
							<th style="width: 200px">
								<p>Document type</p>
							</th>
							<cfif Variables.ShowFinalize>
								<th style="width: 80px" align="center">
									<p>Finalize?</p>
								</th>
							</cfif>
							<cfif Variables.ShowComments>
							<th style="width: 80px" align="center">
					    	        <p>Comment</p>
						        </th>
						    </cfif>
                                <th style="width: 80px" align="center">
					    	        <p>Uploader</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Date Uploaded</p>
						        </th>
						<cfif Variables.ShowFinalize>
		 					<th style="width: 80px" >
           					<p>Upload Status</p>
        					</th>
						</cfif>
    				</tr>
				</thead>
    <cfset Row =0>
    <cfif IsDefined("DocumentResult") and DocumentResult.recordcount>
		<cfloop query="DocumentResult">
        <cfset row= row+1>
		<tr>
			<td align="right"  style="width: 10px" >#row#</td>
			<cfif isdefined("Variables.SubSystemCd") and listfind("1,2,3",Variables.SubSystemCd) gt 0>
				<input type="hidden" name="DOCID_#row#" value="#docid#">
			<cfelse>
				<input type="hidden" name="DOCID1" value="1111"><!--- dummy data for now --->
			</cfif>

			<cfif Variables.ShowDeleteCheck>
            <td align="center" class="optlabel"  style="width: 40px" >
					<label class="optlabel nowrap">
                    	<cfif Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ "SBGBND">
                        <cfset Variables.checkboxvalue	= DocUpldSeqNmb>
                        </cfif>
                        <input type="checkbox" name="#Variables.checkboxname##row#" value="#Variables.checkboxvalue#" onClick="chk_FinalDel('1','Del','Final');">
						Del
                        <cfif IsDefined("Variables.hiddenvariablename")>
                        <cfif Variables.SubSystemName EQ "SBGBND">
                        <cfset Variables.hiddenvalue	= DocumentResult.BndAppSubNmb>
                        </cfif>
                        <input type="hidden" name="#Variables.hiddenvariablename##row#" value="#Variables.hiddenvalue#"> </cfif>
					</label>
				</td>
			</cfif>
			<td>
            <cfif Variables.SubSystemName EQ "SBGBUS">
            <a href="dsp_opendoc.cfm?bus=#DocUpldSeqNmb#">#DocNm#</a>
            <cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims">
            <a href= "dsp_opendoc.cfm?bnd=#DocUpldSeqNmb#&Sub=#DocumentResult.BndAppSubNmb#">#DocNm#</a>
            <cfelseif Variables.SubSystemName EQ "PIMS">
            <a href= "dsp_dwnlddoc.cfm?DOCID=#DOCID#">#DocNm#</a>
			<cfelse>
				<font size="-1"><a href= "dsp_dwnldletter.cfm?DOCID=#DOCID#" target="_blank">#DocNm#</a></font>
            </cfif>
        	</td>
		 <td>
		<cfif isDefined("Variables.SubSystemCd") and listfind("1,2,3",Variables.SubSystemCd) gt 0>
        	#TYPEDESCRIPTION#
		<cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims" OR Variables.SubSystemName EQ "SBGBUS">
		#DocBusDescTxt#
		<cfelse>
			#TYPEDESCRIPTION#
		</cfif>
        </td>

		<cfif Variables.ShowFinalize>
			<td align="center">
				<label class="optlabel nowrap"  style="width: 80px" >
					<input type="checkbox" name="Final1" onClick="chk_FinalDel('1','Final','Del');" value="0">
					Fin
				</label>
    	    </td>
		</cfif>
		<cfif Variables.ShowComments>
        	<td align="center" style="width: 80px">
				#DocCmntTxt#
    	    </td>
		</cfif>
            <td align="center" style="width: 80px">
				<cfif isdefined("Variables.SubSystemCd") and listfind("1,2,3",Variables.SubSystemCd) gt 0>
				#LASTUPDUSERID#
				<cfelse>
				#DocUpldrNm#&nbsp;
												<cfif Ucase(trim(DocumentResult.DocUpldrUsrTyp)) EQ "C">[Contractor]
												<cfelseif Ucase(trim(DocumentResult.DocUpldrUsrTyp)) EQ "A">[Agent]
												<cfelse>[SBA]
												</cfif>
				</cfif>
    	    </td>
          <td align="center" style="width: 80px">
				#dateFormat(CreatDt,"mm/dd/yyyy")#<br>
												#TimeFormat(CreatDt, "hh:mm:ss tt")#&nbsp;
    	    </td>
       <cfif Variables.ShowFinalize><td align="center" style="width: 80px">&nbsp;&nbsp;&nbsp;</td></cfif>
		</tr>
        </cfloop></cfif>
        <cfset Variables.RecCount = #row#>
        <input type="hidden" name="RecCount" 		value="#RecCount#">
	</table>
    <cfif Variables.ShowDeleteCheck><div align="center">
	<!---<input type="button" value="Clear" onClick="ClearForm(this.form, 0);">
	&nbsp;--->
	<input type="Reset"  value="Reset">
	&nbsp;
	<input type="submit" name="DelDoc" value="Save" onClick="return DelChk();"></cfif>
</div>

<cfelse><table align="center" width="90%">

		<tr bgcolor="#Variables.RowColorB#">
			<td colspan="5" align="center" class="mandlabel">No Documents uploaded yet</td>
		</tr>
	</table>
</cfif></cfif>
	</form>
</div>
	<br>
	<br>
<!--- <cfset Variables.fileTypCd=10> --->
<cfinclude template="dsp_fileuploadform.cfm">
</div>
</body>
<cfif Variables.ShowDeleteCheck>
<script language="JavaScript">
function DelChk(){
	var reccnt = 0;
	<cfif DocumentResult.recordcount>
		reccnt=#Variables.RecCount#;
	</cfif>
	var k = 0;
	for (var i=1; i<=reccnt; i++) {
			if (eval("document.FormRight.#Variables.checkboxname#"+i+".checked")) {
				k = 1;
			}
		}
	if (k == 0) {
	   	alert ("No document has been selected for deletion.");
		return false;
   	}else{
		var answer = confirm ("Selected documents will be deleted.\n Click on OK to continue, or CANCEL")
		if (answer){
			ColdFusion.Window.show('LoadWindow');
			return true
		}
		return false
	}

}

function DoSomethingDifferentOnSubmit	(pThis)
		{
		<!---pThis.CollatLienChanged.value				= "No";
		if	(gCollatLienSynopsis					!= FormSynopsis(pThis, "+LoanCollatLien"))
			pThis.CollatLienChanged.value			= "Yes";
		return DoThisOnSubmit(pThis);--->
		}
</script></cfif>
</html>
</cfoutput>

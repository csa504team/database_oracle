
<!---
AUTHOR:				Ian Clark
DATE:				03/15/2016
DESCRIPTION:		Form for Jquery File Upload
NOTES:				This was based on a sample of code using JQuery File Upload Library. This is the form for defining
					what gets displayed for the files uploaded and what gets passed to the server. This requires fileUploadscript.cfm
					be included in the header section of the calling page.
INPUT:
OUTPUT:
REVISION HISTORY:   03/15/2016 IAC Original Implementation
--->
<div class="container" >
<form name="FileUploadData" id="FileUploadData">
<input type="hidden" name="counter" id="counter" value="0" />
<cfoutput>
<input type="hidden" name="ORIGUSERID" id="ORIGUSERID" value="#Variables.ORIGUSERNAME#" />
</cfoutput>
<input type="hidden" name="trequests" id="trequests" value="1" />
<input type="hidden" name="fileTypCd" id="fileTypCd" value ="">
<input type="hidden" name="FileSize" id="FileSize" value="0" />
<input type="hidden" name="FileName" id="FileName" value="" />
<!--- <input type="hidden" name="DescTxt" id="DescTxt" value="" /> --->
<input type="hidden" name="Row" id="Row" value="0" />

	</form>
      <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="fileupload-buttonbar">
        <div class="fileupload-buttons">
            <!-- The fileinput-button span is used to style the file input field as button -->
             <span class="btn btn-success fileinput-button">
       		 <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
				   <input type='text' name ='index'/>
				   <input id="fileupload"  type="file" name="file" multiple>


            </span>
			<div style="display:none"> <!--- create to dropdown so we can clone it and then hide it unless we want to
											 allow for a global document type selection.   style="border: solid;border-width: thin;"--->
			  <select name="DocTypeSel" id="DocTypeSel" class="DocsSel" required onChange="setUpload(this)">
				 <option value="0"  selected>Select Document Type</option>
				<cfloop query="Doclist">
					<cfoutput><option value="#DOCTYPCD#">#DOCTYPDESCTXT#</option></cfoutput>
				</cfloop>
				</select>
			</div>
		<!--- At some point we will want to have an upload all button and a cancel button.
				  At the momoent these will sunbit the whole form. Which is not want we want so comment them out for now.

			 <button type="submit" class="start" name="start" id="start" disabled>Start upload</button>
             <button type="reset" class="cancel">Cancel upload</button>
			--->
		</div>
	</div>
	<br>
                   <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
	<!---   <div class="Table">

	</div> --->
    <div id="files" class="Table">
	 <div class="Heading">
	    <div class="Cell">
            <p>File Name</p>
        </div>
        <div class="Cell1">
            <p>File Size</p>
        </div>
       <div class="Cell3">
            <p>Document type</p>
        </div>
		 <div class="Cell4">

        </div>

    </div>
    </div>


    <br>

</div>

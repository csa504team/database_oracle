<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				09/01/2011
DESCRIPTION:		Displays the name part of the user profile for use in a form.
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html.
INPUT:				Variables scope variables for each of the IMUserTbl columns supported.
					ReadOnlyProfile and other flags.
OUTPUT:				Form elements in "tbl tags".
REVISION HISTORY:	09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables.
					12/22/2008, NNI:	Changed javascript message text from Surname and Given Name to Last Name and
										First Names, respectively.
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables.
					05/16/2006, SRS:	Original implementation.
--->

<!--- Initializations: --->

<cfinclude template="bld_readonly_name.cfm">

<cfif CGI.Script_Name is not "/security/user/dsp_user.cfm">
	<cfset Variables.ShowFullName						= ""><!--- Only used by /security/user/dsp_user.cfm. --->
</cfif>

<!--- Display the data: --->

<cfif Variables.ReadOnlyProfile>
	<cfoutput>
  <div class="tbl">
    <div class="tbl_row" id="dspFullName" style="display:#Variables.ShowFullName#">
      <div class="tbl_cell formlabel viewlabel">Name:</div>
      <div class="tbl_cell formdata nowrap">
        <div class="viewdata">&nbsp;#Variables.IMUserFirstNm#&nbsp;</div>&nbsp;
        <div class="viewdata">&nbsp;#Variables.IMUserMidNm#&nbsp;</div>&nbsp;
        <div class="viewdata">&nbsp;#Variables.IMUserLastNm#&nbsp;</div>&nbsp;
        <div class="viewdata">&nbsp;#Variables.IMUserSfxNm#&nbsp;</div>
        <input type="Hidden" name="IMUserFirstNm" value="#Variables.IMUserFirstNm#">
        <input type="Hidden" name="IMUserMidNm"   value="#Variables.IMUserMidNm#">
        <input type="Hidden" name="IMUserLastNm"  value="#Variables.IMUserLastNm#">
        <input type="Hidden" name="IMUserSfxNm"   value="#Variables.IMUserSfxNm#">
      </div><!-- /formdata -->
    </div><!-- tbl_row -->
  </div><!-- /tbl --></cfoutput>
<cfelse>
	<cfoutput>
  <div class="tbl" id="dspFullName" style="display:#Variables.ShowFullName#">
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel">Name:</div>
      <div class="tbl_cell formdata nowrap"><!-- Side-by-Side -->
        <div class="inlineblock"><!-- Vertical -->
          <div class="manddata">
          <input type="Text" data-type="text" maxlength="128" size="30"
          name="IMUserFirstNm" id="IMUserFirstNm" value="#Variables.IMUserFirstNm#" onchange="
          if  (EditMask('First Name', this.value, 'X', 1, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          ">
          </div><br/>
          <label for="IMUserFirstNm" class="mandlabel">(First)</label>
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div class="optdata"><!-- 2nd col data -->
          <input type="Text" data-type="text" maxlength="128" size="10"
          name="IMUserMidNm" id="IMUserMidNm" value="#Variables.IMUserMidNm#" onchange="
          if  (EditMask('Middle Name', this.value, 'X', 0, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          ">
          </div><br/>
          <label for="IMUserMidNm" class="optlabel">(Middle)</label>
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div class="manddata">
          <input type="Text" data-type="text" maxlength="128" size="30"
          name="IMUserLastNm" id="IMUserLastNm" value="#Variables.IMUserLastNm#" onchange="
          if  (EditMask('Last Name', this.value, 'X', 1, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          ">
          </div><br/>
          <label for="IMUserLastNm" class="mandlabel">(Last)</label>
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div class="optdata">
          <input type="Text" data-type="text" maxlength="30" size="10"
          name="IMUserSfxNm" id="IMUserSfxNm" value="#Variables.IMUserSfxNm#" onchange="
          if  (EditMask('Name Suffix', this.value, 'X', 0, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          ">
          </div><br/>
          <label for="IMUserSfxNm" class="optlabel">(Suffix - Jr, Sr, I, II, etc)</label>
        </div><!-- /Vertical -->
      </div><!-- /formdata = /Side-by-Side -->
    </div><!-- tbl_row -->
  </div><!-- /tbl --></cfoutput>
</cfif>


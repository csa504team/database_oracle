<!--- <cfset Variables.SPINCLUDESURL = "/cfincludes/oracle/security/">

<cfset Variables.Identifier = 3>
<cfset Variables.IMUserId = 228633>

<cfinclude template="/cls/cfincludes/get_userroles.cfm">
<cfdump var="#getRoleLoc#" label="getRoleLoc">

<cfif getRoleLoc.recordcount>
    <select name="loc_id">
        <cfset temp = "">
        <cfset tempList = "">
        <cfoutput query="getRoleLoc">    	
            <cfif NOT listFind(tempList,getRoleLoc.LOCID)>
            <cfset tempList = listAppend(tempList,LOCID)>
            <option value="#getRoleLoc.LOCID#">#temp# - #getRoleLoc.PRTLOCNM#</option>
            </cfif>
        </cfoutput>
    </select>
</cfif> --->

<!--- ====================================================================== --->




<!--- Initializations: --->


<cfset Variables.ArrayRoleLocIds					= ArrayNew(2)>
<cfset Variables.cfpra								= ArrayNew(2)><!--- "cfpra" is SPC file abbreviation for "cfprocresult array" --->
<cfset Idx = 0>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoles">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getPrivileges">	<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleOfc">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleLoc">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleBus">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getBusinesses">	<cfset Variables.cfpra[Idx][2] = Idx>
<!--- Identifier, IMUserId and IMPOUserId are set by the caller. --->

<cfset Identifier = 3>
<cfset IMUserId = 228633>
<cfset Variables.db									= "oracle_scheduled">
<cfset Variables.LogAct								= "get profile">
<cfinclude template="/cfincludes/oracle/security/spc_USERROLESELCSP.PUBLIC.cfm">

<cfdump var="#getRoleLoc#" label="getRoleLoc">

<cfif Variables.TxnErr>
	<cfset Variables.SaveMe							= "No">
<cfelse>
	<!--- LocIds --->
	<cfloop query="getRoleOfc">
		<cfset Idx									= getRoleOfc.CurrentRow>
		<cfset Variables.ArrayRoleOfcCds[Idx][1]	= getRoleOfc.IMAppRoleId>
		<cfset Variables.ArrayRoleOfcCds[Idx][2]	= getRoleOfc.SBAOfcCd>
	</cfloop>
	<!--- OfcCds --->
	<cfloop query="getRoleLoc">
		<cfset Idx									= getRoleLoc.CurrentRow>
		<cfset Variables.ArrayRoleLocIds[Idx][1]	= getRoleLoc.IMAppRoleId>
		<cfset Variables.ArrayRoleLocIds[Idx][2]	= getRoleLoc.LocId>
	</cfloop>
	<!--- Role Businesses --->
	<cfloop query="getRoleBus">
		<cfset Idx									= getRoleBus.CurrentRow>
		<cfset Variables.ArrayRoleBusInfo[Idx][1]	= getRoleBus.IMAppRoleId>
		<cfset Variables.ArrayRoleBusInfo[Idx][2]	= getRoleBus.IMUserBusSeqNmb>
		<cfset Variables.ArrayRoleBusInfo[Idx][3]	= getRoleBus.IMUserBusEINSSNInd>
		<cfset Variables.ArrayRoleBusInfo[Idx][4]	= getRoleBus.IMUserBusTaxId>
		<cfset Variables.ArrayRoleBusInfo[Idx][5]	= getRoleBus.IMUserBusLocDUNSNmb>
	</cfloop>
	<!--- getRoles, getPrivileges and getBusinesses are used by the caller as-is (that is, as query objects). --->
</cfif>

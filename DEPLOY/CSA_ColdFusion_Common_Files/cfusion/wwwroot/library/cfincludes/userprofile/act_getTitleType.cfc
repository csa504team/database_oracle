<cfcomponent>
<cffunction name="getTitle" access="remote" returnType="query" returnFormat="JSON">
	<cfargument name="ImUserTypCd" type="string">
	<cfset Variables.LogAct									= "retrieve Federal employee job titles">
	<cfset Variables.ImUserTypCd							= arguments.ImUserTypCd>
	<cfset Variables.cfprname								= "titleList">
	<cfset Variables.Identifier								= "12"><!--- Same as Identifier 4, only alphabetized. --->
	<cfset Variables.db										= "oracle_scheduled">
	<cfinclude template="/cfincludes/oracle/security/spc_IMJOBTITLTYPSELTSP.PUBLIC.cfm">
   	<cfreturn titleList>
</cffunction>

<!--- This will be the preferable function when we implement the final version used in conjuction with jQuery .load (RSD) --->
<cffunction name="getTitleHTML" access="remote" returnFormat="plain">
	<cfargument name="ImUserTypCd" type="string">
	<cfset Variables.LogAct									= "retrieve Federal employee job titles">
	<cfset Variables.ImUserTypCd							= arguments.ImUserTypCd>
	<cfset Variables.cfprname								= "titleList">
	<cfset Variables.Identifier								= "12"><!--- Same as Identifier 4, only alphabetized. --->
	<cfset Variables.db										= "oracle_scheduled">
	<cfinclude template="/cfincludes/oracle/security/spc_IMJOBTITLTYPSELTSP.PUBLIC.cfm">
   	<cfset Variables.HTMLString = "">
	<cfloop query="titleList">
		<cfset Variables.HTMLString = Variables.HTMLString & "<li><input type='checkbox' name='JobTitlCd' value='" & titlelist.JobTitlCd & "' id='JobTitlCd" & titlelist.JobTitlCd & "'><label for='JobTitlCd" & titlelist.JobTitlCd & "'>" & titlelist.JobTitlDescTxt & "</label></li>">
	</cfloop>
	<cfreturn Variables.HTMLString>
</cffunction>
</cfcomponent>
<!---
AUTHOR:				Steve Seaquist
DATE:				06/06/2006
DESCRIPTION:		Builds Variables.ReadOnly((columnname)) variables from Variables.ReadOnlyProfile and defaults. 
NOTES:				Called in both the dsp_formdata and dsp_onsubmit files. 
INPUT:				Variables.ReadOnlyProfile. 
OUTPUT:				Variables.ReadOnly((columnname)) variables. 
REVISION HISTORY:	10/17/2012, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to changes in dsp_formdata_contact. 
					06/06/2006, SRS:	Original implementation.
--->

<cfparam name="Variables.ReadOnlyProfile"				default="No">
<cfif Variables.ReadOnlyProfile>
	<cfset Variables.ReadOnlyIMCntryCd					= "Yes">
	<cfset Variables.ReadOnlyIMUserZipCd				= "Yes">
	<cfset Variables.ReadOnlyStCd						= "Yes">
	<cfset Variables.ReadOnlyIMUserPostCd				= "Yes">
	<cfset Variables.ReadOnlyIMUserStNm					= "Yes">
	<cfset Variables.ReadOnlyIMUserCtyNm				= "Yes">
	<cfset Variables.ReadOnlyIMUserStr1Txt				= "Yes">
	<cfset Variables.ReadOnlyIMUserStr2Txt				= "Yes">
	<cfset Variables.ReadOnlyIMUserPhnCntryCd			= "Yes">
	<cfset Variables.ReadOnlyIMUserPhnAreaCd			= "Yes">
	<cfset Variables.ReadOnlyIMUserPhnLclNmb			= "Yes">
	<cfset Variables.ReadOnlyIMUserPhnExtnNmb			= "Yes">
	<cfset Variables.ReadOnlyIMUserEmailAdrTxt			= "Yes">
<cfelse>
	<cfparam name="Variables.ReadOnlyIMCntryCd"			default="No">
	<cfparam name="Variables.ReadOnlyIMUserZipCd"		default="No">
	<cfparam name="Variables.ReadOnlyStCd"				default="Yes">
	<cfparam name="Variables.ReadOnlyIMUserPostCd"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserStNm"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserCtyNm"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserStr1Txt"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserStr2Txt"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserPhnCntryCd"	default="No">
	<cfparam name="Variables.ReadOnlyIMUserPhnAreaCd"	default="No">
	<cfparam name="Variables.ReadOnlyIMUserPhnLclNmb"	default="No">
	<cfparam name="Variables.ReadOnlyIMUserPhnExtnNmb"	default="No">
	<cfparam name="Variables.ReadOnlyIMUserEmailAdrTxt"	default="No">
</cfif>

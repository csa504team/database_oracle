<cfif CGI.Script_Name IS "/cls/cfincludes/dsp_formdata_ldap.cfm"><cfsetting enablecfoutputonly="Yes"></cfif>
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration.
DATE:				09/06/2011.
DESCRIPTION:		Displays form fields relating to LDAP for SBA Employee users. Also acts as its own AJAX callback file.
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html.
					CFIncluded by dsp_formfields_userroles, so we assume the same existence of variables as that file.
					Currently in development, but will eventually be called by Section 508 compliant versions of:
						/cls/dsp_access.cfm
						/cls/dsp_addcustomer.cfm
						/security/user/dsp_profile.cfm
						/security/user/dsp_user.cfm
INPUT:				See /cls/cfincludes/dsp_formfields_userroles.cfm.
OUTPUT:				LDAP-related form fields.
REVISION HISTORY:	06/16/2015, NS:    Added ids to form fields
					06/15/2015, NS:     Added support for supervisor validation
					06/04/2015, NS:		Updated SP to IMUSERSUPRVSRSELCSP(0) in AJAX call.
					06/01/2015, NS:     Added support for ImUserTypCD passes to AJAX call.
					05/29/2015, NS:     Added support for entering Supervisor(Non CLS) info. Added search for Lender LAO
					05/20/2015, NS:     Added hiddedn fields for User own Access; added search for given until DB object fixed
					05/19/2015, NS: 	LDAP removal
					08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility.
					08/26/2013, IAC:	When this is used with dsp_addcustomer.cfm it does not include the security/dsp_user.cfm So this method: ShowHideAppropriateDataForTypCd
										Will not be defined. Not an issue as all contractors are not SBA Employees.
					07/10/2013, IAC:	The logic around the drop down for Employees was checking for recordcount GT 1
										So if the result set was only one row an error was being displayed. It did not stop the user
										from processing the page. But becuase it says error most users will stop right there.
										Also per the Auditors SBA Contracters need to have the roles approved by the COTR. As we
										do not have orginizational data the best we can do is allow them to select from LDAP.
					03/13/2013,	IAC:	The drop down box for supervisors had an ID of EltDropdownLookupSuprv the Axex code
										was using gEltDropdownLookupSuprv. Under quirks mode this still worked under strict
										mode it did not. Also  DivViewSuprvNames2.style.display was coded as gDivViewSuprvNames2.style.display
										So it was failing
					01/03/2012, SRS:	Original library implementation. Cannibalized /cls/cfincludes version, copied to
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables.
										Changed hardcoded references to ITSecurity@sba.gov to Variables.ITSecEMailAddr.
					02/08/2011, ajc:	Added support for GLS account request. (SRS: Fixed LDAPUserId lookup.)
					04/19/2010, SRS:	Some SBA Employees have been going through IT Security to change their supervisor
										info often. To prevent problems that causes, disallowed editing of supervisor info.
										(Displayed read-only.)
					02/03/2010, SRS:	Provide more feedback to end users about what to do.
					02/02/2010, SRS:	On AJAX lookups, require URL.LDAPUserId to be in LDAP.
					01/28/2010.2, SRS:	Eliminated first name lookups, added last name shortening to last name lookups,
										and made "Windows Login" fields explicitly "Your/User's" or "Super's".
					01/28/2010.1, SRS:	Avoided getting messed up by IT Security user's IMUserTypCd and displaying fields
										for non-SBA users in Security System > End Users > Update User Access - IT.
					01/26/2010, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<cfif isDefined("Variables.Identifier") and len(Variables.Identifier) gt 0>
	<cfset Variables.IdentifierHold = Variables.Identifier>
</cfif>
<cfset Variables.AllowGivenNameInLookups					= "Yes">
<cfset Variables.AllowITSecurityToDoArbitraryLookups		= "No">
<cfset Variables.SuspSupervisor 			= false>

<!--- <cfset Variables.LDAPServerName								= "newyukon.sba.gov"> --->
<cfparam name="Variables.LibIncURL"							default="/library/cfincludes">
<!--- <cfset Variables.SelfBase									= "#Variables.LibIncURL#/userprofile/dsp_formdata_ldap">--->
 <cfset Variables.SelfBase									= "#Variables.LibIncURL#/userprofile/dsp_supervisor_lookup">
<cfset Variables.SelfName									= "#Variables.SelfBase#.cfm"><!--- For AJAX callbacks to self. --->
<cfset Variables.SelfNameStaticCSS							= "#Variables.LibIncURL#/userprofile/dsp_formdata_ldap.css?CachedAsOf=2011-09-09T18:53">
<cfset Variables.SelfNameStaticScripts						= "#Variables.LibIncURL#/userprofile/dsp_formdata_ldap.js?CachedAsOf=2011-09-09T18:52">
<cfset Variables.ShowUserTheirOwnWindowsLogin				= "No">
<cfset Variables.UserIsRequestingOwnAccess					= (Left(CGI.Script_Name,5) IS "/cls/")>
<cfif listfindnocase("/cls/dsp_profile.cfm",CGI.SCRIPT_NAME) gt 0>
	<cfset Variables.UserIsRequestingOwnAccess = "No">
</cfif>
<cfset Variables.ExcludeContractors						= "Yes">


<cfset Variables.OracleSchemaName          = "security">
<cfset Variables.db                        = "oracle_scheduled">
<cfset Variables.SPCURL                    = "/cfincludes/oracle/security">
<cfset Variables.dbtype                    = "Oracle80">
<cfset Variables.Variables.LibURL          = "/library">
<cfset Variables.Variables.LibUdfURL       = "#Variables.LibURL#/udf">
<cfset Variables.username = "">
<cfset Variables.password = "">
<!--- for update role page --->
<!--- <cfif IsNumeric(Variables.IMUserId)>
	<cfinclude template="#Variables.CLSIncludesURL#/bld_UserInfoArrays.cfm"><!--- Could set TxnErr to Yes--->
	<cfif Variables.TxnErr>
		<cfoutput>Serious error occured. #Variables.ErrMsg#</cfoutput><cfabort>
	<cfelse>
		<cfif len(Variables.IMUserId) EQ 0><cfset Variables.IMUserId = 0></cfif>
	</cfif>
</cfif> --->
<cfif NOT IsDefined("spcn")>
	<cfinclude template="#Variables.LibUdfURL#/bld_dbutils_for_dbtype.cfm">
</cfif>

<!--- <cfset  Variables.ImUserTypCd 							= trim(Variables.ImUserTypCd)> --->

<cfset Variables.cfprname								= "ValidSuper">
<cfset Variables.LogAct									= "Validate Supervisor">
<cfinclude template="/cfincludes/oracle/security/spc_VALIDATEIMUSERSUPRVCSP.cfm">
<cfif Variables.TxnErr>
	<cfset Variables.SaveMe								= "No">
	<cfoutput><p class="dem_errmsg">Serious database error occured. Following information might help #Variables.ErrMsg#</p></cfoutput>
	<cfabort>
</cfif>
<!--- <cfdump var="#Variable.ImUserNm#"><cfdump var="#Variables.IMUserEmailAdrTxt#"> --->
<cfif Variables.SUPRVIND eq "I">
	<cfset Variables.SuspSupervisor = true>
</cfif>
<!-- **************************************************************************************** -->
<!--- AJAX Callback(s): --->
<cfif  CGI.Script_Name IS Variables.SelfName><!--- AJAX callbacks are to dsp_formdata_ldap, not to the caller. --->
	<cfif	IsDefined("URL.Given")
		OR	IsDefined("URL.Family")
		OR	IsDefined("URL.Email")
		OR	IsDefined("URL.IMUserNm")
		OR 	IsDefined("URL.IMUserId")
		OR	IsDefined("URL.IMUserSuprvId")
		OR	IsDefined("URL.LocId")
		OR	IsDefined("URL.IMUserTypCd")>
		<cfif IsDefined("URL.RequestTimeout")>
			<cf_setrequesttimeout seconds="#URL.RequestTimeout#">
		</cfif>
		<cfif IsDefined("URL.Given") OR IsDefined("URL.Family") or IsDefined("URL.Email")>
			<cfparam name="URL.Given"						default="">
			<cfparam name="URL.Family"						default="">
			<cfparam name="URL.Email"						default="">
			<cfparam name="URL.LocId"						default="">
			<cfparam name="URL.IMUserTypCd"					default="">
			<cfset Variables.Filter							= "(&(objectclass=person)">
			<cfset Variables.GetAll							= ((IsDefined("URL.GetAll")) AND (URL.GetAll is "Yes"))>
			<cfset Variables.Done							= "No"><!--- Keep indention low. --->

			<cfif (NOT Variables.Done)
				AND	(Len(URL.Email) gt 0)>
				<cfif NOT IsDefined("spcn")>
					<cfinclude template="#Variables.LibUdfURL#/bld_dbutils_for_dbtype.cfm">
				</cfif>
		            <cfset Variables.Identifier              = "0">
		            <cfset Variables.cfprname                = "getUidAndCn">
		            <cfset Variables.LogAct                  = "Get Users by email">
		            <cfset Variables.IMUserEmailAdrTxt 		 = trim(URL.Email)>
					<cfset Variables.IMUserTypCd 		 	 = trim(URL.IMUserTypCd)>
		            <cfset Variables.LocId 		 			 = trim(URL.LocId)>
					<cfinclude template="#spcn('IMUSERSUPRVSRSELCSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                 <cfset Variables.SaveMe                        = "No">
		            </cfif><cfdump var="#Variables.TxnErr#"><cfdump var="#getUidAndCn#">

				<cfif getUidAndCn.RecordCount GTE 1>
					<cfset Variables.Done					= "Yes">
				</cfif><!--- <cfdump var="email"><cfdump var="#getUidAndCn#"> --->
			<cfelseif	(NOT Variables.Done)
				AND	(Len(URL.Given) GT 0)
				AND	(Len(URL.Family) GT 0)>
		            <cfset Variables.Identifier              = "0">
		            <cfset Variables.cfprname                = "getUidAndCn">
		            <cfset Variables.LocId 		 			 = trim(URL.LocId)>
		            <cfset Variables.IMUserTypCd 		 	 = trim(URL.IMUserTypCd)>
		            <cfset Variables.LogAct                  = "Get Users by last name">
		            <cfset Variables.IMUserFirstNm 			 = trim(URL.Given)>
		            <cfset Variables.IMUserLastNm 			 = trim(URL.Family)>
					<cfinclude template="#spcn('IMUSERSUPRVSRSELCSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                   <cfset Variables.SaveMe                        = "No">
		            </cfif>
				<cfif getUidAndCn.RecordCount GTE 1>
					<cfset Variables.Done					= "Yes">
				</cfif><!--- <cfdump var="last/first"><cfdump var="#Variables.LocId#"><cfdump var="#getUidAndCn#"> --->
			</cfif>
			<cfif	(NOT Variables.Done)
				AND	(Len(URL.Given) GT 0)>
				<cfset Variables.Filter						&= "(givenName=#URL.Given#*)">
			</cfif>
			<cfif	(NOT Variables.Done)
				AND	 (Len(URL.Family) gt 0 )>
				<cfset Variables.FamilyLen					= Len(trim(URL.Family))>
				<cfset Variables.FamilyEnd					= Variables.FamilyLen - 2>
				<cfif Variables.FamilyEnd LT 1>
					<cfset Variables.FamilyEnd				= 1>
				</cfif>
				<cfloop index="NameLenIdx" from="#Variables.FamilyLen#" step="-1" to="#Variables.FamilyEnd#">
					<!--- <cfset Variables.FamilyFragment			= Left(URL.Family, NameLenIdx)> --->
					<cfset Variables.FamilyFragment			= trim(URL.Family)>
		            <cfset Variables.Identifier              = "0">
		            <cfset Variables.LocId 		 			 = trim(URL.LocId)>
		            <cfset Variables.IMUserTypCd 		 	 = trim(URL.IMUserTypCd)>
		            <cfset Variables.cfprname                = "getUidAndCn">
		            <cfset Variables.LogAct                  = "Get Users by last name">
		            <cfset Variables.IMUserLastNm 			 = Variables.FamilyFragment>
					<cfinclude template="#spcn('IMUSERSUPRVSRSELCSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                 <cfset Variables.SaveMe                        = "No">
		            </cfif><!--- <cfdump var="#getUidAndCn#"><cfdump var="last"> --->
				<cfif getUidAndCn.RecordCount GTE 1>
					<cfbreak>
				</cfif>
					<cfif getUidAndCn.RecordCount GTE 1>
						<cfset Variables.Done				= "Yes">
						<cfbreak>
					</cfif>
				</cfloop>
			<!--- </cfif> --->
			<cfelseif	(NOT Variables.Done)
					AND	 (Len(URL.Given) gt 0 )>
					<cfset Variables.Identifier              = "0">
		            <cfset Variables.cfprname                = "getUidAndCn">
		            <cfset Variables.LogAct                  = "Get Users by First name">
					<cfinclude template="#spcn('IMUSERSUPRVSRSELCSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                 <cfset Variables.SaveMe             = "No">
		            </cfif>
		            <cfif Variables.TxnErr>
		                   <cfset Variables.SaveMe                        = "No">
		            </cfif><!--- <cfdump var="#getUidAndCn#"><cfdump var="First"> --->
					<cfif getUidAndCn.RecordCount GTE 1>
						<cfset Variables.Done				= "Yes">
					</cfif>
			</cfif>
			<!--- <cfabort> --->
			<cfcontent type="text/html">
			<cfif getUidAndCn.RecordCount GT 0>
				<cfoutput>
						<option value="">Choose An SBA Employee</option></cfoutput>
				<cfloop query="getUidAndCn">
					<cfset Variables.OptionValue			= getUidAndCn.IMUserNm>
					<cfif Variables.GetAll>
						<cfset Variables.OptionValue		&=	"|#getUidAndCn.IMUserFirstNm#"
															&	"|#getUidAndCn.IMUserMidNm#"
															&	"|#getUidAndCn.IMUserLastNm#"
															&	"|#getUidAndCn.IMUserEmailAdrTxt#">
					</cfif>
					<cfoutput>
						<cfif Variables.ExcludeContractors>
							 <cfif find("(C)",getUidAndCn.IMUserTypCd) EQ 0 AND find("(I)",getUidAndCn.IMUserTypCd) EQ 0>
							 	<option value="#Variables.OptionValue#">#getUidAndCn.IMUserLastNm#,#getUidAndCn.IMUserFirstNm#( #getUidAndCn.IMUSERTYPDESCTXT#)</option>
							</cfif>
						<cfelse>
								<option value="#Variables.OptionValue#">#getUidAndCn.IMUserTypCdn#</option>
						</cfif>
						</cfoutput>
				</cfloop>
			<cfelse>
				<cfoutput>
						<option value="">Nothing found. (Please try again.)</option></cfoutput>
			</cfif>
		<cfelseif IsDefined("URL.IMUserID") or IsDefined("URL.IMUserNm")>
			<cfset Variables.IMUserSuprvId					= "">
			<cfset Variables.IMUserSuprvUserId				= "">
			<cfset Variables.IMUserSuprvFirstNm				= "">
			<cfset Variables.IMUserSuprvMidNm				= "">
			<cfset Variables.IMUserSuprvLastNm				= "">
			<cfset Variables.IMUserSuprvEmailAdrTxt			= "">
			<cfset Variables.ManagerDescription				= "">

			<cfif isDefined("URL.IMUserId")>
				<cfset  Variables.IMUserId               = trim(URL.IMUserId)>
				<!--- get IMUserNm by IMUserId --->
				<cfset Variables.Identifier              = "0">
	            <cfset Variables.cfprname                = "getIMUserNm">
	            <cfset Variables.LogAct                  = "Get Manager Data">
				<cfinclude template="#spcn('IMUSERSELTSP', 'security')#">
	           	<cfif Variables.TxnErr>
	                 <cfset Variables.SaveMe                        = "No">
	            </cfif>
				<cfif getIMUserNm.recordcount gt 0>
					<cfset Variables.IMUserNm = trim(getIMUserNm.IMUserNm)>
				</cfif>
	            <cfset Variables.Identifier              = "21">
	            <cfset Variables.cfprname                = "getManagerData">
	            <cfset Variables.LogAct                  = "Get Manager Data">
				<cfinclude template="#spcn('IMUSERSELTSP', 'security')#">
	           	<cfif Variables.TxnErr>
	                 <cfset Variables.SaveMe                        = "No">
	            </cfif>
			<cfelseif isDefined("URL.IMUserNm")>
				<!--- get IMUserId by  IMUserNm --->
		            <cfset Variables.IMUserNm 			 	 = trim(URL.IMUserNm)>
		            <cfquery name="getIMUserId" datasource="#db#" dbtype="#dbtype#" username="#Variables.username#" password="#Variables.password#">
						SELECT IMUserId FROM security.IMUserTbl WHERE  upper(IMUserNm) = '#ucase(Variables.IMUserNm)#'
					</cfquery>
				<cfif getIMUserId.recordcount>
					<cfset  Variables.IMUserId               = trim(getIMUserId.IMUserId)>
		            <cfset Variables.Identifier              = "21">
		            <cfset Variables.cfprname                = "getManagerData">
		            <cfset Variables.LogAct                  = "Get Manager Data">
					<cfinclude template="#spcn('IMUSERSELTSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                 <cfset Variables.SaveMe                        = "No">
		            </cfif>
				</cfif>
			</cfif>
			<!--- get Manager's data by IMUserNm' --->
			<cfif isDefined("getManagerData") and getManagerData.RecordCount GT 0>
				<!--- Since the lookup was by uid, there should be at most 1 row returned (safe to get [1]): --->

					<cfset Variables.IMUserSuprvId			= getManagerData.IMUSERSUPRVID>
					<cfset Variables.IMUserSuprvFirstNm		= getManagerData.IMUSERSUPRVFIRSTNM>
					<cfset Variables.IMUserSuprvMidNm		= getManagerData.IMUSERSUPRVMIDNM><!---  getManagerData.IMUSERSUPRVMIDNM --->
					<cfset Variables.IMUserSuprvLastNm		= getManagerData.IMUSERSUPRVLASTNM>
					<cfset Variables.IMUserSuprvEmailAdrTxt	= getManagerData.IMUSERSUPRVEMAILADRTXT>

				<cfcontent type="application/json"><!--- **NOT** text/html! --->
				<!---
				According to http://json.org, single-quotes are passed as-is (no backslashes). So therefore, in order to escape
				double-quotes without escaping single-quotes, use Replace() instead of JSStringFormat():
				--->
				<cfoutput>{"ErrMsg":""</cfoutput>
				<cfoutput>,"IMUserNm":"#					Replace(Variables.IMUserNm,										"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvId":"#				Replace(Variables.IMUserSuprvId,								"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvFirstNm":"#			Replace(Variables.IMUserSuprvFirstNm,							"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvMidNm":"#			Replace(Variables.IMUserSuprvMidNm,								"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvLastNm":"#			Replace(Variables.IMUserSuprvLastNm,							"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvEmailAdrTxt":"#		Replace(Variables.IMUserSuprvEmailAdrTxt,						"""","\""","ALL")#"</cfoutput>
				<!--- But ManagerDescription is different in that it usually contains \, which must be escaped first: --->
				<cfoutput>,"ManagerDescription":"#			Replace(Replace(Variables.ManagerDescription,	"\","\\","ALL"),"""","\""","ALL")#"}</cfoutput>
			<cfelse>
				<cfcontent type="application/json"><!--- **NOT** text/html! --->
				<!--- There shouldn't be a double-quote in what the user typed, but in case there is, don't crash: --->
				<cfoutput>{"ErrMsg":"CLS Login '#Replace(Variables.IMUserNm,"""","\""","ALL")#' not found. Contact HQ HelpDesk."</cfoutput>
				<cfoutput>,"IMUserNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvId":""</cfoutput>
				<cfoutput>,"IMUserSuprvFirstNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvMidNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvLastNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvEmailAdrTxt":""</cfoutput>
				<cfoutput>,"ManagerDescription":""}</cfoutput>
			</cfif>
			<!--- if  IMUserSuprvUserId defined, then select from IMUserTbl user's data' --->
		</cfif>
		<cfsetting showdebugoutput="No">
		<cfabort>
	</cfif>
	<!--- No URL variables, hence, bad call: --->
	<cfcontent type="application/json"><!--- **NOT** text/html! --->
	<cfoutput>{"ErrMsg":"AJAX callback didn't pass parameters on the URL."}</cfoutput>
	<cfsetting showdebugoutput="No">
	<cfabort>
</cfif>

<!-- **************************************************************************************** -->

<!--- Initializations (for generating form data): --->
<cfset Variables.LDAPUserId									= "">
<cfset Variables.IMUserSuprvId								= "">
<cfset Variables.IMUserNm									= "">
<cfset Variables.IMUserSuprvUserId							= "">
<cfset Variables.IMUserSuprvFirstNm							= "">
<cfset Variables.IMUserSuprvMidNm							= "">
<cfset Variables.IMUserSuprvLastNm							= "">
<cfset Variables.IMUserSuprvEmailAdrTxt						= "">

<!--- IMUserId is of the user being displayed, but IMUserTypCd may be of IT Security user. Therefore, don't trust it: --->
<cfquery name="GetUserTypCd"								datasource="#Variables.db#">
select	IMUserTypCd											as IMUserTypCdOfUser
from	security.IMUserTbl
where	IMUserId											= #Variables.IMUserId#
</cfquery>
<cfset Variables.IMUserTypCdOfUser							= "">
<cfif GetUserTypCd.RecordCount GT 0>
	<cfset Variables.IMUserTypCdOfUser						= GetUserTypCd.IMUserTypCdOfUser[1]>
</cfif>
<cfset Variables.DispFormFields								= "No"><!--- Only meaningful if GenFormFields is "Yes", obviously. --->
<cfset Variables.GenFormFields								= "No">
<cfswitch expression="#Variables.IMUserTypCdOfUser#"><!--- Allow easy expansion to include SBA Contractors as well: --->
<cfcase value="S,C,U"><!--- To include SBA Contractors, make the value attribute "S,C". --->
	<cfset Variables.DispFormFields							= "Yes">
	<cfset Variables.GenFormFields							= "Yes">
</cfcase>
<cfdefaultcase>
	<cfif NOT Variables.UserIsRequestingOwnAccess><!--- Security System > End Users > Update User Profile: --->
		<cfset Variables.GenFormFields						= "Yes">
	<cfelseif CGI.Script_Name IS Variables.SelfName><!--- AJAX callback, in case we ever want to add URL.GetFormFields: --->
		<cfset Variables.GenFormFields						= "Yes">
	</cfif>
</cfdefaultcase>
</cfswitch>
<cfif IsDefined("Variables.AddBusNewGlsAct") AND Variables.AddBusNewGlsAct>
	<cfset Variables.DispFormFields							= "Yes">
	<cfset Variables.GenFormFields							= "Yes">
</cfif>

<!-- **************************************************************************************** -->

<!--- Form Data --->

<cfif Variables.GenFormFields>

	<cfquery name="getManagerData"							datasource="#Variables.db#">
		select 	a.IMUserSuprvId,
				a.IMUserSuprvFirstNm,
				a.IMUserSuprvUserId,
				a.IMUserSuprvMidNm,
				a.IMUserSuprvLastNm,
				a.IMUserSuprvEmailAdrTxt,
				t.IMUSERTYPDESCTXT,
				t.IMUSERTYPCd
		from security.IMUserTbl a, security.IMUSERTYPTBL t
		where a.IMUserTypCd = t.IMUserTypCd
		and a.IMUserId = #Variables.IMUserId#
	</cfquery>
	<cfif getManagerData.RecordCount GT 0>
		<cfset Variables.IMUserNm							= "">
		<cfset Variables.IMUserSuprvUserId					= Trim(getManagerData.IMUserSuprvUserId		[1])>
		<cfset Variables.IMUserSuprvId						= Trim(getManagerData.IMUserSuprvId			[1])>
		<cfset Variables.IMUserSuprvFirstNm					= Trim(getManagerData.IMUserSuprvFirstNm	[1])>
		<cfset Variables.IMUserSuprvMidNm					= Trim(getManagerData.IMUserSuprvMidNm		[1])>
		<cfset Variables.IMUserSuprvLastNm					= Trim(getManagerData.IMUserSuprvLastNm		[1])>
		<cfset Variables.IMUserSuprvEmailAdrTxt				= Trim(getManagerData.IMUserSuprvEmailAdrTxt[1])>
		<cfset Variables.IMUSERTYPCd						= Trim(getManagerData.IMUSERTYPCd			[1])>
	</cfif>
	<cfoutput>
<link href="#Variables.SelfNameStaticCSS#" rel="stylesheet" type="text/css" media="all" />
<script src="#Variables.SelfNameStaticScripts#"></script>
<div id="DivSbaEmpElts" style="display:<cfif NOT Variables.DispFormFields>none</cfif>;"></cfoutput>

	<cfif Variables.UserIsRequestingOwnAccess><!--- View access --->
		<!--- End User's view: --->
		<cfif <!--- (Len(Variables.ImUserId)						IS 0)
			OR  (Len(Variables.IMUserNm)					IS 0)
			OR	(Len(Variables.IMUserSuprvId)				IS 0)
			OR	(Len(Variables.IMUserSuprvUserId)			IS 0)
			OR	--->(Len(Variables.IMUserSuprvFirstNm)			IS 0)
			OR	(Len(Variables.IMUserSuprvLastNm)			IS 0)
			OR	(Len(Variables.IMUserSuprvEmailAdrTxt)		IS 0)
			<!--- OR	(Len(Variables.IMUserSuprvUserId)			IS 0) --->>
			<cfif NOT IsDefined("Variables.AddBusNewGlsAct")>
				<cfoutput>
  <div class="optlabel">
    <!--- <span class="mandlabel">New Security Requirements</span>
    require that your CLS login username be registered in the Security System.
    You are required to provide the information below this time only.
    (Any correction must be addressed to IT Security.)
    <ol>
      <li>Enter your CLS Login in the blue box that has "Your CLS Login" underneath.</li>
      <li>Tab out of the field or press the <cfif Variables.IMUserTypCdOfUser EQ "S">"Lookup Super by Hierarchy"<cfelse>"Lookup Contracting Officer Representative"</cfif> button.</li>
      <li>If all of the <cfif Variables.IMUserTypCdOfUser EQ "S">supervisor<cfelse>Contracting Officer Representative</cfif> information is entered into the <cfif Variables.IMUserTypCdOfUser EQ "S">"Super"<cfelse>"COR"</cfif> fields,
          you're done (skip ahead to step 6).
      </li>
      <li>If a message appears to the right of the <cfif Variables.IMUserTypCdOfUser EQ "S">"Lookup Super by Hierarchy"<cfelse>"Lookup Contracting Officer Representative"</cfif> button that says
          "CLS Login (something) not found.", do the following:
          <ol style="list-style-type: lower-alpha;">
            <li>If there's a typo in your CLS Login, correct it and return to step 1, above.</li>
            <li>If you entered your CLS Login correctly, contact IT Security and ask them to
                "propagate my CLS Login from Active Directory to LDAP".
                They should know what that means.
            </li>
          </ol>
      </li>
      <li>If a message appears to the right of the <cfif Variables.IMUserTypCdOfUser EQ "S">"Lookup Super by Hierarchy"<cfelse>"Lookup Contracting Officer Representative"</cfif> button that says
          "<cfif Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> (something) not found. Switching to name lookup.", do the following:
          <ol style="list-style-type: lower-alpha;">
            <li>Enter your <cfif Variables.IMUserTypCdOfUser EQ "S">supervisor's<cfelse>Contracting Officer Representative's</cfif> full last name into the Last Name box that just appeared.
                Press the "Lookup <cfif Variables.IMUserTypCdOfUser EQ "S">Super<cfelse>COR</cfif> by Name" button.
            </li>
            <li>If the dropdown menu says "Nothing found. (Please try again.)", shorten your <cfif Variables.IMUserTypCdOfUser EQ "S">supervisor's<cfelse>Contracting Officer Representative's</cfif>
                last name by taking 1 letter off of the end of the name. (For example, if you just searched
                for "Smith", shorten it to "Smit".) Press the "Lookup Super by Name" button again.
            </li>
            <li>If the dropdown menu still says "Nothing found. (Please try again.)", shorten your supervisor's
                last name by taking another letter off of the end of the name and so on. (For example, if
                you just searched for "Smit", shorten it to "Smi".) Press the "Lookup <cfif Variables.IMUserTypCdOfUser EQ "S">Super<cfelse>COR</cfif> by Name" button
                again.
            </li>
            <li>Continue this process until the dropdown menu says "Choose An SBA Employee".</li>
            <li>Choose your <cfif Variables.IMUserTypCdOfUser EQ "S">supervisor's<cfelse>Contracting Officer Representative's</cfif> name from the dropdown menu,
                which will fill in your supervisor information.
            </li>
          </ol>
      </li>
      <li>If you want to request some roles, you may do so before pressing the "Submit" button at bottom-of-page. </li>
      <li>Alternatively, if you want to make sure that the information you just entered gets saved,
          you may press "Submit" at bottom of page right away. This will take you back to the Choose Function
          page again. When you press the Access button again, you will see your <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> information displayed
          read-only (grayed out), which tells you that you're free to request roles, because your SBA
          <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> information is on file.
      </li>
    </ol>--->
	<span class="mandlabel">Supervisor Info is missing or incomplete. Please contact CLS security.</span>
<!--- /optlabel (that info isn't available) --->
</cfoutput>
				</cfif><!--- /AddBusNewGlsAct --->
			<!--- Don't allow lookup here. User should know their own CLS login username by heart. --->
		<cfelse>
			<cfif NOT IsDefined("Variables.SRV")>
				<cfswitch expression="#Request.SlafDevTestProd#">
				<cfcase value="Dev">	<cfset Variables.SRV= "DEVELOPMENT"	></cfcase>
				<cfcase value="Test">	<cfset Variables.SRV= "TEST"		></cfcase>
				<cfdefaultcase>			<cfset Variables.SRV= "PRODUCTION"	></cfdefaultcase>
				</cfswitch>
			</cfif>
			<cfset Variables.ITSecurityHotlinkSubject		= "#Variables.SRV# - For CLS Login '#Variables.OrigUserId#', "
															& "SBA ">
			<cfif Variables.IMUserTypCdOfUser EQ "S">
				<cfset Variables.ITSecurityHotlinkSubject		=Variables.ITSecurityHotlinkSubject & "Supervisor">
			<cfelseif  Variables.IMUserTypCdOfUser EQ "C">
				<cfset Variables.ITSecurityHotlinkSubject		=Variables.ITSecurityHotlinkSubject	& "Contracting Officer Representative">
			<cfelseif Variables.IMUserTypCdOfUser EQ "U">
				<cfset Variables.ITSecurityHotlinkSubject		=Variables.ITSecurityHotlinkSubject	& "Lender Authorizing Official">
			</cfif>

			<cfset Variables.ITSecurityHotlinkSubject		=Variables.ITSecurityHotlinkSubject	& " information is incorrect.">
			<cfset Variables.ITSecurityHotlinkBody			= "((Enter information about your correct ">
			<CFIF Variables.IMUserTypCdOfUser EQ "S">
				<cfset Variables.ITSecurityHotlinkBody			= Variables.ITSecurityHotlinkBody & "Supervisor">
			<cfelseif Variables.IMUserTypCdOfUser EQ "C">
				<cfset Variables.ITSecurityHotlinkBody			= Variables.ITSecurityHotlinkBody & "Contracting Officer Representative">
			<cfelseif Variables.IMUserTypCdOfUser EQ "U">
				<cfset Variables.ITSecurityHotlinkBody			= Variables.ITSecurityHotlinkBody & "Lender Authorizing Official">
			</cfif>
			<cfset Variables.ITSecurityHotlinkBody			= Variables.ITSecurityHotlinkBody &" here.))">

			<cfset Variables.ITSecurityHotlinkHRef			= "mailto:#Variables.ITSecEMailAddr#?Subject="
															& URLEncodedFormat(Variables.ITSecurityHotlinkSubject)
															& "&Body="
															& URLEncodedFormat(Variables.ITSecurityHotlinkBody)>
			<!--- <cfif Variables.ShowUserTheirOwnWindowsLogin>
				<cfoutput>
  <div class="viewlabel">
    The next box should contain your CLS login username.
    If incorrect, please contact IT Security. <br>
    <div class="viewdata">
	<!--- <input type="Text" name="LDAPUserId" size="15" disabled value="#Variables.LDAPUserId#"> --->
 	<input type="Text" name="IMUserId" size="15" disabled value="#Variables.IMUserId#">
    </div>
  </div><!--- /viewlabel ---></cfoutput>
			</cfif> ---><!--- /ShowUserTheirOwnWindowsLogin --->
			<cfoutput>
		        <input type="Hidden" name="IMUserSuprvId"       	id="IMUserSuprvId"    		value="#Variables.IMUserSuprvId#">
				<input type="Hidden" name="IMUserSuprvUserId"   	id="IMUserSuprvUserId"    	value="#Variables.IMUserSuprvUserId#">
		        <input type="Hidden" name="IMUserSuprvFirstNm"  	id="IMUserSuprvFirstNm"    	value="#Variables.IMUserSuprvFirstNm#">
		        <input type="Hidden" name="IMUserSuprvMidNm"    	id="IMUserSuprvMidNm"     	value="#Variables.IMUserSuprvMidNm#">
		        <input type="Hidden" name="IMUserSuprvLastNm"   	id="IMUserSuprvLastNm"    	value="#Variables.IMUserSuprvLastNm#">
		        <input type="Hidden" name="IMUserSuprvEmailAdrTxt" 	id="IMUserSuprvEmailAdrTxt" value="#Variables.IMUserSuprvEmailAdrTxt#">
				<input type="hidden" name="SuspSupervisor" 	    	id="SuspSupervisor"			value="#Variables.SuspSupervisor#">
  <div class="viewlabel">
    <!--- The next box should contain information about your <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif>.
    If incorrect, please contact IT Security by using
    <a href="#Variables.ITSecurityHotlinkHRef#">this hotlink</a>
    or by calling (202) 481-2946. <br> --->
    <div class="viewdata" id="DivViewSuprvNames1 nowrap"><!-- Side-by-Side -->
      <!-- Make id unique with "Span...", because MSIE's getElementById also finds form element names: -->
      <div class="viewdata">
        <span class="hdr"><cfif Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelseif Variables.IMUserTypCdOfUser EQ "U">LAO<cfelse>Supervisor</cfif>'s CLS Login</span><br/>
     	<span class="dtl">#Variables.IMUserNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">First</span><br/>
        <span class="dtl">#Variables.IMUserSuprvFirstNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">Middle</span><br/>
        <span class="dtl">#Variables.IMUserSuprvMidNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">Last</span><br/>
        <span class="dtl">#Variables.IMUserSuprvLastNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">E-Mail</span><br/>
        <span class="dtl">#Variables.IMUserSuprvEmailAdrTxt#</span>
      </div>
	<!--- /DivViewSuprvNames1 = /Side-by-Side --->
  </div>
	<cfif Variables.Imuserid gt 0>
      	 <cfif isDefined("Variables.SuspSupervisor") and (Variables.SuspSupervisor eq true)>
	      	<table class="dem_errmsg">
		      	<tr><td>Supervisor info is invalid.</td></tr></table>
	      </cfif>
      </cfif>
	<!--- /viewlabel ---></cfoutput>
		</cfif><!--- We already have all 5 Supervisor fields. --->
	<cfelse><!--- Else of UserIsRequestingOwnAccess --->
		<!--- IT Security's view: --->
		<cfoutput>
  <div class="optlabel" id="div1"><!---<cfif <!--- Len(Variables.LDAPUserId) --->Len(Variables.IMUserNm) IS 0 or Len(Variables.ImUserId) IS 0>

<span class="mandlabel" >New Requirement:</span>
    <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> information for <cfif Variables.UserIsRequestingOwnAccess>you<cfelse>the user</cfif> has not yet been entered into the Security System. Enter <cfif Variables.UserIsRequestingOwnAccess>your<cfelse>their</cfif>  <cfelse>
    <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> information for <cfif Variables.UserIsRequestingOwnAccess>you<cfelse>the user</cfif> has already been entered into the Security System. To change, enter <cfif Variables.UserIsRequestingOwnAccess>your<cfelse>the users</cfif></cfif>
     CLS login user name or you can use the drop-down menu. <br/>
	 Based on <cfif Variables.UserIsRequestingOwnAccess>your<cfelse>the users</cfif> CLS login we will query LDAP if the <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelse>Contracting Officer Representative</cfif> is found it will be displayed. If not you can do a look up based on their last name.
 --->
    &nbsp;
  </div><!--- /optlabel --->
  <!--- <div class="tbl"> ---></cfoutput>
		<cfif Variables.AllowITSecurityToDoArbitraryLookups>
			<cfif Variables.AllowGivenNameInLookups>
				<cfoutput>
    <!--- <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        These 2 lookup fields accept partial "Starts With" name, which may work better than full name:
      </div><!--- /formdata --->
    </div> ---><!--- /tbl_row --->
    <!--- <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="LDAPLookupUserGiven" class="optlabel">First Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupUserGiven" id="LDAPLookupUserGiven" value="#Variables.IMUserFirstNm#" size="20">
        </div>
      </div><!--- /formdata --->
    </div> ---><!--- /tbl_row ---></cfoutput>
			<cfelse>
				<cfoutput>
    <!--- <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        This lookup accepts partial "Starts With" name, which may work better than full name:
      </div><!--- /formdata --->
    </div> ---><!--- /tbl_row ---></cfoutput>
			</cfif><!--- /AllowGivenNameInLookups --->
			<cfoutput>
    <!--- <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel"><label for="LDAPLookupUserFamily" class="optlabel">Last Name:</label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Text" name="LDAPLookupUserFamily" value="#Variables.IMUserLastNm#" size="20">
        </div>
        &nbsp;
        <input type="Button" value="Lookup User by Name" onclick="
        this.form.EltDropdownLookupUser.options[0].text = 'Doing lookup. Please wait.';
        $(gEltDropdownLookupUser)
            .load('#Variables.SelfName#?'<cfif Variables.AllowGivenNameInLookups>
                + 'Given='  + escape(this.form.LDAPLookupUserGiven.value)+'&'</cfif>
                + 'Family=' + escape(this.form.LDAPLookupUserFamily.value),
                null,       // No data object because all parameters are on the URL = request will be a 'get'.
                function()  {MaybeSelectFirstOption(gEltDropdownLookupUser);} // AJAX call completion routine
                );
        "></cfoutput>
			<cfsavecontent variable="Variables.AlreadyLookedUpOptions">
				<cfoutput>
        <option value="">Lookup not done yet. Results will appear here.</option></cfoutput>
			</cfsavecontent>
		<cfelse><!--- Else of AllowITSecurityToDoArbitraryLookups --->
			<cfset Variables.IMUserLastNmLen				= Len(Variables.IMUserLastNm)>
			<cfset Variables.IMUserLastNmEnd				= Variables.IMUserLastNmLen - 2>
			<cfif Variables.IMUserLastNmEnd LT 1>
				<cfset Variables.IMUserLastNmEnd			= 1>
			</cfif>
			<!--- condition: do not search for customers --->
		<cfif Variables.IMUserTypCd NEQ 'U'>
			<cfloop index="NameLenIdx" from="#Variables.IMUserLastNmLen#" step="-1" to="#Variables.IMUserLastNmEnd#">
				<cfset Variables.IMUserLastNmFragment		= Left(Variables.IMUserLastNm, NameLenIdx)>
				<!--- <cfldap
					action									= "QUERY"
					name									= "getUidAndCn"
					server									= "#Variables.LDAPServerName#"
					username								= "uid=readuser,ou=people,dc=sba,dc=gov"
					password								= "readuser10"
					filter									= "(&(objectclass=person)(sn=#Variables.IMUserLastNmFragment#*))"
					attributes								= "uid,cn"
					sort									= "cn"
					start									= "ou=people,dc=sba,dc=gov"> --->
					<!--- <cfset Variables.cfpra							= "">
					<cfset Variables.Variables.LibURL         = "/library">
				    <cfset Variables.Variables.LibUdfURL      = "#Variables.LibURL#/udf">
				    <cfset Variables.dbtype                   = "Oracle">
					<cfset Variables.OracleSchemaName         = "security">
					<cfset Variables.db                       = "oracle_transaction">
					<cfset Variables.SPCURL                   = "/cfincludes/oracle/security"> --->
				<cfif NOT IsDefined("spcn")>
					<cfinclude template="#Variables.LibUdfURL#/bld_dbutils_for_dbtype.cfm">
				</cfif>


					<!--- <cfset Variables.username = "secsbaupdt">
					<cfset Variables.password = "Security192"> --->
		            <cfset Variables.Identifier              = "20">
		            <cfset Variables.cfprname                = "getUidAndCn">
		            <cfset Variables.LogAct                  = "Get Users by last name">
		            <cfset Variables.IMUserLastNm 			 = Variables.IMUserLastNmFragment>
					<cfinclude template="#spcn('IMUSERSELCSP', 'security')#">
		           	<cfif Variables.TxnErr>
		                   <cfset Variables.SaveMe                        = "No">
		            </cfif>
				<cfif getUidAndCn.RecordCount GT 1>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif><!--- end of do not search for customers --->
			<cfsavecontent variable="Variables.AlreadyLookedUpOptions">
				<cfif	IsDefined("getUidAndCn")
					AND	(getUidAndCn.RecordCount GTE 1)>
					<cfoutput>
        <option value="">Choose An SBA Employee</option></cfoutput>
						<cfloop query="getUidAndCn">
						<cfoutput>
						 	<option value="#getUidAndCn.IMUserNm#">#getUidAndCn.IMUSERLASTNM#, #getUidAndCn.IMUserFirstNm#<cfif len(getUidAndCn.IMUserMidNm) gt 0>, #getUidAndCn.IMUserMidNm#</cfif><cfif len(getUidAndCn.IMUserSfxNm) gt 0> #getUidAndCn.IMUserSfxNm# .</cfif>
						 	(#getUidAndCn.IMUSERTYPDESCTXT#)
		                   </option><!--- 		 <cfif find("Contractor",getUidAndCn.cn) EQ 0> --->
        				</cfoutput>
					</cfloop>
				<cfelse>
					<cfoutput>
        <option value="">Nothing found. <!--- (Please investigate LDAP.) ---></option></cfoutput>
				</cfif><!--- /getUidAndCn --->
			</cfsavecontent><!--- <cfdump var="#Variables.IMUserLastNm#"><cfdump var="#getUidAndCn#"> --->
			<cfoutput>
		   <!--- <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        The following drop-down menu is based on a lookup of '#Variables.IMUserLastNm#' in LDAP:
      </div><!--- /formdata --->
    </div> ---> ---><!--- /tbl_row ---></cfoutput>
		</cfif><!--- AllowITSecurityToDoArbitraryLookups --->
		<cfoutput>
    <!--- <div class="tbl_row" id="DivLookupUser">
      <div class="tbl_cell formlabel optlabel"><label for="EltDropdownLookupUser" class="optlabel"><!--- Choose User: ---></label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        </div><!--- /optdata --->
      </div><!--- /formdata --->
    </div> ---><!--- /tbl_row --->
  <!--- </div> ---><!---end <div class="tbl"> /tbl ---></cfoutput>
	</cfif><!--- /UserIsRequestingOwnAccess --->

	<!---
	Most of the preceding was just to put up info for the user, or a dropdown for ITSecurity that eases populating
	LDAPUserId. This is the point where it starts getting interesting:
	--->
	<cfif	( (Len(Variables.IMUserNm)						IS 0)
		OR	(Len(Variables.LDAPUserId)						IS 0)
		OR	(Len(Variables.IMUserId)						IS 0)
		OR	(Len(Variables.IMUserSuprvId)					IS 0)
		OR	(Len(Variables.IMUserSuprvUserId)				IS 0)
		OR	(Len(Variables.IMUserSuprvFirstNm)				IS 0)
		OR	(Len(Variables.IMUserSuprvLastNm)				IS 0)
		OR	(Len(Variables.IMUserSuprvEmailAdrTxt)			IS 0))
		<!--- and listfindnocase("/cls/dsp_access.cfm",CGI.SCRIPT_NAME) gt 0 --->
		<!--- and 	(NOT Variables.UserIsRequestingOwnAccess) --->
		and (listfindnocase("/cls/dsp_addcustomer.cfm",CGI.SCRIPT_NAME) gt 0
		or listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0
		or listfindnocase("/security/user/dsp_profile.cfm",CGI.SCRIPT_NAME) gt 0
		or listfindnocase("/cls/dsp_profile.cfm",CGI.SCRIPT_NAME) gt 0
		)>
		<cfif Variables.UserIsRequestingOwnAccess>
			<cfset NameUser									= "Your">
			<cfset NameUserJS								= "Your">
		<cfelse>
			<cfset NameUser									= "User's">
			<cfset NameUserJS								= "User\'s">
		</cfif>
		<cfoutput>
  <div class="tbl" style="width:100%;">
        <!--- <input type="Hidden" name="SaveLDAPUserId" value="Yes">
		<input type="Hidden" name="LDAPUserId" value="#Variables.LDAPUserId#"> --->
	</cfoutput>
	<cfset Variables.ShowDivViewSuprvNames2				=	(Len(Variables.IMUserNm			) gt 0)
														or  (Len(Variables.IMUserSuprvUserId		) gt 0)
														or	(Len(Variables.IMUserSuprvFirstNm		) gt 0)
														or	(Len(Variables.IMUserSuprvMidNm			) gt 0)
														or	(Len(Variables.IMUserSuprvLastNm		) gt 0)
														or	(Len(Variables.IMUserSuprvEmailAdrTxt	) gt 0)>

	<cfoutput>
	<div class="tbl_row" id="DivLookupSuprvEmail" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvEmail" class="optlabel"><CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelseif Variables.IMUserTypCdOfUser EQ "U">Lender Authorizing Offical<cfelse>Supervisor</cfif>'s Email:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupSuprvEmail"  id="LDAPLookupSuprvEmail"  value="" size="20">
		</div><!--- /optdata --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
	<div class="tbl_row" id="DivLblOr" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="DivLblOr" class="optlabel">OR</label></div>
    </div>
		<cfif Variables.AllowGivenNameInLookups>
    <div class="tbl_row" id="DivLookupSuprvGiven" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvGiven" class="optlabel"><CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelseif Variables.IMUserTypCdOfUser EQ "U">LAO<cfelse>Supervisor</cfif>'s First Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupSuprvGiven"  id="LDAPLookupSuprvGiven"  value="" size="20">
        </div><!--- /optdata --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfif>
    <div class="tbl_row" id="DivLookupSuprvFamily" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvFamily" class="optlabel" id="SuprName"><CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelseif Variables.IMUserTypCdOfUser EQ "U">LAO<cfelse>Supervisor</cfif>'s Last Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupSuprvFamily" id="LDAPLookupSuprvFamily" value="" size="20">
	  </div><!--- /optdata --->
	   &nbsp;
       <input type="Button" id="btnSuprName" title ="super" value="Lookup <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelseif Variables.IMUserTypCdOfUser EQ "U">LAO<cfelse>Supervisor</cfif> by Email or First Name/Last Name" onclick="
        var sForm 		= this.form;
		var fnm= sForm.LDAPLookupSuprvGiven.value;
		var lnm= sForm.LDAPLookupSuprvFamily.value;
		var eml= sForm.LDAPLookupSuprvEmail.value;

		var fnmtrim = fnm.trim();
		var lnmtrim = lnm.trim();
		var emltrim = eml.trim();

		<cfif Variables.IMUserId gt 0>
		if ( ! ( (fnm==null || fnmtrim=='') && (lnm==null || lnmtrim=='') && (eml==null || emltrim=='') ) ) {
			if	(!confirm('Changing Supervisor will require SBA authorization. Your account will be suspended. \n Press OK to continue. \n Press Reset button on the bottom of the form to reset original Supervisor data'))
			{
			}
		}
		</cfif>
			var usrSlctdObj = document.getElementById('IMUserTypCd');
			var loctnObj 	= document.getElementById('LocId').value;
			//alert(loctnObj);

			<cfif listfindnocase("/security/user/dsp_profile.cfm",CGI.SCRIPT_NAME) gt 0>
				var usrSlctdObjElt = usrSlctdObj.value;
			<cfelse>
				var usrSlctdObjElt = '';
			</cfif>
			//alert(usrSlctdObjElt);
				if ( usrSlctdObj.selectedIndex > 0 ) {
				 	usrSlctdObjElt = usrSlctdObj.options[usrSlctdObj.selectedIndex].value;
				  	if (usrSlctdObjElt == 'U') {
						if (loctnObj == '') {
							alert('Location ID is mandatory');
							return false;
						}
				 	}
				}
		//email is priority
		//all 3 fields are empty
		   if  ( (fnm==null || fnmtrim=='') && (lnm==null || lnmtrim=='') && (eml==null || emltrim=='') )  {
		    alert('Please enter Email or First Name/Last Name');
		    sForm.LDAPLookupSuprvEmail.focus();
		    return false;
		  }
		//email is empty
		 if (eml==null || emltrim =='') {
		   if  ( (fnm!=null || fnmtrim!='') && (lnm==null || lnmtrim=='')  )  {
		    alert('Please enter Last Name');
		    sForm.LDAPLookupSuprvFamily.focus();
		    return false;
		   }
		   if  (  (lnm!=null || lnmtrim!='') && (fnm==null || fnmtrim=='') )  {
		    alert('Please enter First Name');
		    sForm.LDAPLookupSuprvGiven.focus();
		    return false;
		  }
		 }

		gEltDropdownLookupSuprv.options[0].text       = 'Doing lookup. Please wait.';
        $(gEltDropdownLookupSuprv)
            .load('#Variables.SelfName#?'<cfif Variables.AllowGivenNameInLookups>
                + 'Given=' + escape(this.form.LDAPLookupSuprvGiven.value)+'&'</cfif>
                + 'Family='+ escape(this.form.LDAPLookupSuprvFamily.value)+'&'
				+ 'Email='+ escape(this.form.LDAPLookupSuprvEmail.value)+'&'
				+  'LocId='+ loctnObj + '&'
				+  'IMUserTypCd=' + usrSlctdObjElt + '&'
                + 'GetAll=Yes',
                null,      // No data object because all parameters are on the URL = request will be a 'get'.
                function() {MaybeSelectFirstOption(gEltDropdownLookupSuprv);} // AJAX call completion routine
                );
        ">
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
	<cfset Variables.SuprStylePrfx="">
	<cfset Variables.SuprLablPrfx="">
	<cfif listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) neq 0 or listfindnocase("/cls/dsp_profile.cfm",CGI.SCRIPT_NAME) gt 0>
		<cfset Variables.SuprStylePrfx ="opt">
		<cfset Variables.SuprLablPrfx="OR">
	<cfelse>
		<cfset Variables.SuprStylePrfx ="mand">
	</cfif>
	<div class="tbl_row" id="DivLookupSuprv" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="gEltDropdownLookupSuprv" class="#Variables.SuprStylePrfx#label" id="chooseSupr">Choose <CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">Contracting Officer Representative<cfelseif Variables.IMUserTypCdOfUser EQ "L">Lender Authorizing Official<cfelse>Supervisor</cfif>:</label></div>
      <div class="tbl_cell formdata">
        <div class="#Variables.SuprStylePrfx#data">
        <select name="gEltDropdownLookupSuprv" id="gEltDropdownLookupSuprv" onchange="
         var sForm                                 = this.form;
		if  (this.selectedIndex > 0)
            {
            var sValue                                = this.options[this.selectedIndex].value.split('|');
            //CopyToSpanAndHidden(sForm, 'IMUserNm',          	sValue[0]);
			//CopyToSpanAndHidden(sForm, 'IMUserSuprvUserId',    sValue[1]);
			CopyToSpanAndHidden(sForm, 'IMUserSuprvId',      	 sValue[0]);
			<cfif listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0>
				if (sValue[0] != ''){ document.getElementById('SpanIMUserSuprvId').setAttribute('readOnly','true'); }
			</cfif>
            CopyToSpanAndHidden(sForm, 'IMUserSuprvFirstNm',     sValue[1]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvMidNm',       sValue[2]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvLastNm',      sValue[3]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvEmailAdrTxt', sValue[4]);
            DivViewSuprvNames2.style.display                         = '';
            }
		else {
			CopyToSpanAndHidden(sForm, 'IMUserSuprvId',   		'');
			<cfif listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0>
				//document.getElementById('SpanIMUserSuprvId').setAttribute('readOnly','false');
				document.getElementById('SpanIMUserSuprvId').readOnly=false;
			</cfif>
            CopyToSpanAndHidden(sForm, 'IMUserSuprvFirstNm',    '');
            CopyToSpanAndHidden(sForm, 'IMUserSuprvMidNm',      '');
            CopyToSpanAndHidden(sForm, 'IMUserSuprvLastNm',     '');
            CopyToSpanAndHidden(sForm, 'IMUserSuprvEmailAdrTxt','');
			//DivViewSuprvNames2.style.display                     = 'none';
			<cfif listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) neq 0>
				DivViewSuprvNames2.style.display                     = '';
			<cfelse>
				DivViewSuprvNames2.style.display                     = 'none';
			</cfif>
		}
        ">
        <option value="">Lookup not done yet. Results will appear here.</option>
        </select>
        </div><!--- /manddata --->
		<!--- <cfif len(Variables.SuprLablPrfx) gt 0>
			<div class="tbl_row" id="DivLookupSuprvEmail" style="width:100%;display:;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
		      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvEmail" class="optlabel">#Variables.SuprLablPrfx#</label></div>
		    </div>
		</cfif> --->
		<cfif not ( listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0)>
       <!---  <input type="Hidden" name="IMUserNm"                value="#Variables.IMUserNm#"> --->
		<input type="Hidden" name="IMUserSuprvId"       	id="IMUserSuprvId"    		value="#Variables.IMUserSuprvId#">
		<input type="Hidden" name="IMUserSuprvUserId"   	id="IMUserSuprvUserId"    	value="#Variables.IMUserSuprvUserId#">
        <input type="Hidden" name="IMUserSuprvFirstNm"  	id="IMUserSuprvFirstNm"    	value="#Variables.IMUserSuprvFirstNm#">
        <input type="Hidden" name="IMUserSuprvMidNm"    	id="IMUserSuprvMidNm"     	value="#Variables.IMUserSuprvMidNm#">
        <input type="Hidden" name="IMUserSuprvLastNm"   	id="IMUserSuprvLastNm"    	value="#Variables.IMUserSuprvLastNm#">
        <input type="Hidden" name="IMUserSuprvEmailAdrTxt" 	id="IMUserSuprvEmailAdrTxt" value="#Variables.IMUserSuprvEmailAdrTxt#">
		<input type="hidden" name="SuspSupervisor" 	    	id="SuspSupervisor"			value="#Variables.SuspSupervisor#">
		</cfif>
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
		<cfset Variables.ShowDivViewSuprvNames2				=	(Len(Variables.IMUserNm			) gt 0)
														or  (Len(Variables.IMUserSuprvUserId		) gt 0)
														or	(Len(Variables.IMUserSuprvFirstNm		) gt 0)
														or	(Len(Variables.IMUserSuprvMidNm			) gt 0)
														or	(Len(Variables.IMUserSuprvLastNm		) gt 0)
														or	(Len(Variables.IMUserSuprvEmailAdrTxt	) gt 0)
														or listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0>
														<div class="tbl_row" id="DivViewSuprvNames2" style="width:100%;<cfif NOT Variables.ShowDivViewSuprvNames2> display:none;</cfif>"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
	      <div class="tbl_cell formlabel viewlabel" id="divSuprName"><CFIF Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">Contracting Officer Representative<cfelse>Supervisor</cfif>:</div>
	      <cfif listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) eq 0>
	      <div class="tbl_cell formdata">
	        <div class="viewdata nowrap"><!-- Side-by-Side -->
	          <!-- Make id unique with "Span...", because MSIE's getElementById also finds form element names: -->
	        <!--- <div class="viewdata">
	            <span class="hdr"  id="spanSuprName"            ><cfif Variables.IMUserTypCdOfUser EQ "S">Supervisor<cfelseif Variables.IMUserTypCdOfUser EQ "C">COR<cfelse>Super</cfif>'s CLS Login</span><br/>
	            <span class="dtl" id="SpanIMUserNm"          	>#Variables.IMUserNm#</span>
	          </div> --->

			<div class="viewdata">
	            <span class="hdr" 								>Supervisor Id</span><br/>
				<span class="dtl" id="SpanIMUserSuprvId"    >	#Variables.IMUserSuprvId#</span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr"                                >First</span><br/>
	            <span class="dtl" id="SpanIMUserSuprvFirstNm"    >#Variables.IMUserSuprvFirstNm#</span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr"                                >Middle</span><br/>
	            <span class="dtl" id="SpanIMUserSuprvMidNm"      >&nbsp;#Variables.IMUserSuprvMidNm#</span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr"                                >Last</span><br/>
	            <span class="dtl" id="SpanIMUserSuprvLastNm"     >#Variables.IMUserSuprvLastNm#</span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr"                                >E-Mail</span><br/>
	            <span class="dtl" id="SpanIMUserSuprvEmailAdrTxt">#Variables.IMUserSuprvEmailAdrTxt#</span>
	          </div>
	        </div>
	       <cfif Variables.Imuserid gt 0>
	      		<cfif <!--- isDefined("Variables.SuspSupervisor") and ---> (Variables.SuspSupervisor eq true)>
			      	<div class="dem_errmsg">
			            Please select another supervisor. Supervisor info is invalid.
			         </div>
		      	</cfif>
	      	</cfif>
	        <!--- /DivViewSuprvNames2 = /Side-by-Side --->
	      </div><!--- /formdata --->
	      <cfelse>
		      <div class="tbl_cell formdata">
		      <!--- <cfif len(Variables.IMUserSuprvId) eq 0> --->
		      <div class="viewdata">
	            <span class="hdr">Supervisor Id</span><br/>
				<span class="dtl"><input type="Text" name="IMUserSuprvId" id="SpanIMUserSuprvId" value="#Variables.IMUserSuprvId#" size="15"></span>
			  </div>
			  <!--- <cfelse>
			  <div class="viewdata">
	            <span class="hdr">Supervisor Id</span><br/>
				<span class="dtl" id="SpanIMUserSuprvId"><input type="text" name="IMUserSuprvId" value="#Variables.IMUserSuprvId#"></span>
	          </div>
			  </cfif> --->
			  <div class="viewdata">
	           <span class="hdr">First</span><br/>
	           <input type="text" name="IMUserSuprvFirstNm" id="SpanIMUserSuprvFirstNm" value="#Variables.IMUserSuprvFirstNm#" size="20" maxlength="50">
	          </div>
	          <div class="viewdata">
	            <span class="hdr">Middle</span><br/>
	            <span class="dtl"><input type="text" name="IMUserSuprvMidNm" id="SpanIMUserSuprvMidNm" value="#Variables.IMUserSuprvMidNm#" size="6" maxlength="50"></span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr">Last</span><br/>
	            <span class="dtl"><input type="text" name="IMUserSuprvLastNm" id="SpanIMUserSuprvLastNm" value="#Variables.IMUserSuprvLastNm#" size="20" maxlength="50"></span>
	          </div>
	          <div class="viewdata">
	            <span class="hdr">E-Mail</span><br/>
	            <span class="dtl"><input type="text" name="IMUserSuprvEmailAdrTxt" id="SpanIMUserSuprvEmailAdrTxt" value="#Variables.IMUserSuprvEmailAdrTxt#" size="20" maxlength="255"></span>
	          </div>
	          <cfif Variables.Imuserid gt 0>
				<input type="hidden" name="SuspSupervisor" id ="SuspSupervisor" value="#Variables.SuspSupervisor#">
		      	 <cfif <!--- isDefined("Variables.SuspSupervisor") and ---> (Variables.SuspSupervisor eq true)>
			      	<div class="dem_errmsg" id="ShowSuspSupervisor">
			            Please select or enter another supervisor. Supervisor info is invalid.
			         </div>
			      </cfif>
		      </cfif>
	        </div><!--- /DivViewSuprvNames2 = /Side-by-Side --->
	      </div><!--- /formdata --->
			<input type="hidden" name="IMUserSuprvUserId" id="IMUserSuprvUserId" value="#Variables.IMUserSuprvUserId#">

	      </cfif>
	    </div><!--- /tbl_row --->
	  </div><!--- /tbl --->
	  <!--- </cfif> --->
	</cfoutput>
	</cfif>
	<cfoutput>
</div><!--- /id="DivSbaEmpElts" ---></cfoutput>
</cfif><!--- /Variables.GenFormFields --->
<!---
/security/user/dsp_user.cfm doesn't use enablecfoutputonly="Yes". But the first line of this file does that only in
case of AJAX callback. All AJAX callbacks terminate in cfaborts, so if control got this far, we never called cfsetting.
Therefore, there's no need to revert enablecfoutputonly back to "No".
--->

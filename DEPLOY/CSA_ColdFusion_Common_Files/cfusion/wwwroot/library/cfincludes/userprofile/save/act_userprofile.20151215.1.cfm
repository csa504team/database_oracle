<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Currently just a validation strategy tester. Evolving into the real thing.
NOTES:				None.
INPUT:				Form variables, Variables.CancellationPage, Session.Origs.
OUTPUT:				Save profile to the security schema.
REVISION HISTORY:	12/03/2015, HA:     Fixed the Borrower Loan Number not getting displayed issue by adding IMUserLoanNmb.
					12/01/2015, PCL:    Fixed the error message duplication issue.
					11/10/2015, NS:     Added support for IMUserActvInactInd change from active to inactive; added support for fax form controls.
					11/04/2015, NS:     Update: set IMUserActvInactIndChanged to No for user profile page.
					10/30/2015, RSD:    Modified to validate loan number from etran OR lender loan number microloan for user type �Loan Borrower�.
					10/24/2015, NS:     Update: Added check for IMUserSuprvIdChanged
					10/01/2015, NS:     Update: set the Suspense code to 3 and trigger emails when account is set from inactive to active
					09/28/2015, NS: 	HQLocId update fix
					09/25/2015, NS: 	Fixed Variables.TxnErr and GetErrs display
					09/23/2015, NS:     Disabled suspension on HQLocId change
					09/22/2015, NS:     Added fix for lastupdtuserid(title update in  security); modified code to apply
										spc_NEW_VALIDATESUPRVCSP.cfm for new and existing users
					09/18/2015, NS:		Modified code to apply spc_NEW_VALIDATEIMUSERACCTCSP.cfm for both new and existion user
					09/18/2015, SJ:		Sheen's comments
					09/10/2015, NS:		Added prevalidation Sp for duplicates
					09/08/2015, NS:		Removed password validation routine for new security user(applies only to new customer);
										added "N" user type to set Variables.JobTitlCdStr
					09/04/2015, SRS:	Made sure user-being-edited's IMAsurncLvlCd is not edited by IMUserUpdTSP Identifier 0.
										SBAUser SecAdmin code will have top be added to set IMAsurncLvlCd (if it changed)
										using IMUserUpdTSP Identifier 12 instead. Previously could have nulled out IMAsurncLvlCd!
					08/25/2015, SRS:	Cleanups for release to production.
					08/01/2015, NS:		Added validations,SPs calls and emails.
					07/30/2015, SRS:	Original implementation as part of the new act_userprofile that consolidates all user
										profile act pages. We now have no prior knowledge as to which which columns a caller may
										chose to make uneditable, so we have to rely on the Origs structure built in the initial
										load of dsp_userprofile from the database.
--->

<cffunction name="SomethingChanged"							returntype="boolean"><!--- Defined here so usable in act_userprofile.val_calls. --->
	<cfargument name="ColumnList"							type="String" required="Yes">
	<cfset var Local										= {}>
	<cfloop index="Local.ColName" list="#Arguments.ColumnList#">
		<cfif Variables[Local.ColName]						is not Variables.Origs[Local.ColName]>
			<cfreturn true>
		</cfif>
	</cfloop>
	<cfreturn false>
</cffunction>

<!--- Configuration Parameters: --->

<cfparam name="Variables.CancellationPage"	type="String"><!--- No default attribute, therefore, a mandatory input. --->
<cfset Variables.DisplayPage								= Replace(Variables.PageName, "act_", "dsp_", "One")>
<cfparam name="Variables.SuccessPage"		type="String"	default="#Variables.DisplayPage#">
<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfset Variables.DsbsMiniProfileURL							= "https://eweb1.sba.gov/pro-net/ws/wbs_sbainternalservices.cfc?method=GetMiniProfileByDUNSOrTaxId">
<cfparam name="Variables.UpdateTrace"						default="No"><!--- "Yes" or "No" --->

<!--- Check whether user should even be at this page: --->

<cfif		NOT IsDefined("Form.FieldNames")>
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
<cfelseif	IsDefined("Form.SubmitButton")
	and		(Form.SubmitButton is "Cancel")><!--- Keep the cancellation SubmitButton value in sync with dsp_userprofile. --->
	<cflocation addtoken="No" url="#Variables.CancellationPage#">
</cfif>

<cfinclude template="#Variables.LibUdfURL#/bld_DspPageLoadTimeOrigsUDFs.cfm">
<cfinclude template="#Variables.LibUdfURL#/bld_PasswordUDFs.cfm">
<cfset GetDspPageLoadTimeOrigs()><!--- Retrieve Session.Origs. We mainly want "MayEdit" right now. --->

<cfif NOT Variables.OrigsSuccess>
	<!---
	If OrigsSuccess isn't "Yes", it probably means that Session.DspPageLoadTimeOrigs.DspPage_Handshake wasn't defined,
	or, if it was, the Handshake didn't reference the right display page. Either way, it's rather equivalent to detecting
	NOT IsDefined("Form.FieldNames"). So treat it the exact same way as above:
	--->
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
</cfif>
<cfif NOT Variables.Origs.MayEdit><!--- We WANT to crash if MayEdit is undefined (to debug), so just assume it is defined. --->
	<!---
	This condition will probably never happen, because the dsp page should set ReadOnlyProfile, which should result in
	its not generating the form nor its submit buttons. But theoretically, a hacker could manufacture a form submission.
	So there's no harm done in planning for it and telling the hacker "shame on you":
	--->
	<cfset Variables.ErrMsg									= "Error: You are not allowed to save this profile to the database. ">
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<!--- Initializations: --->

<cfparam name="Variables.Commentary"						default=""><!--- Calling page may have had something to say. Don't trash it. --->
<cfset Variables.ErrMsgBase									= "Error(s) occurred that prevented saving the page's data: ">
<cfset Variables.ErrMsg										= Variables.ErrMsgBase>
<cfset Variables.SaveMe										= "Yes">
<cfset Variables.TxnErr										= "No">
<cfset Variables.PageIsAddCustomer							= "No">
<cfset Variables.PageIsProfile								= "No">
<cfset Variables.PageIsUser									= "No">
<cfset Variables.SendEmailUserTypeFlag 						= false>
<cfset Variables.SendEmailAddrEmailFlag 					= false>
<cfset Variables.SendEmailSupervisorFlag 					= false>
<cfset Variables.SendHQLocEmailFlag							= false>
<cfset Variables.JobTitleChanged 							= false>
<cfset Variables.ErrSeqNmb									= "0">
<cfset Variables.PageTitle									= "">
<cfset Variables.DevTeamEml									= ",nelli.salatova@sba.gov,matthew.forman@sba.gov">
<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/cls/act_addcustomer.cfm,/cls/act_addcustomer2.cfm,/cls/act_addcustomer_old.cfm">
	<cfset Variables.PageIsAddCustomer						= "Yes">
	<cfset Variables.SuccessPage							= Request.SlafLoginURL>
</cfcase>
<cfcase value="/cls/act_profile.cfm,/cls/act_profile2.cfm,/cls/act_profile_old.cfm">
	<cfset Variables.PageIsProfile							= "Yes">
	<cfset Variables.SuccessPage							= "dsp_choosefunction.cfm">
</cfcase>
<cfcase value="/security/user/act_user.cfm,/security/user/act_user2.cfm,/security/user/act_user_old.cfm">
	<cfset Variables.PageIsUser								= "Yes">
</cfcase>
</cfswitch>
<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.ActvIMAsurncLvlTbl						= Server.Scq.security.ActvIMAsurncLvlTbl>
	<cfset Variables.ActvIMJobTitlTypTbl					= Server.Scq.security.ActvIMJobTitlTypTbl>
	<cfset Variables.ActvIMUserSuspRsnTbl					= Server.Scq.security.ActvIMUserSuspRsnTbl>
	<cfset Variables.ActvIMUserTypTbl						= Server.Scq.security.ActvIMUserTypTbl>
	<cfset Variables.ScqActvCountries						= Server.Scq.ActvIMCntryCdTbl>
	<cfset Variables.ScqActvStates							= Server.Scq.ActvStTbl>
</cflock>

<!--- Simple Validity Edits: --->

<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "About to call val_calls.<br/>">
</cfif>

<cfinclude template="act_userprofile.val_calls.cfm"><!--- Does it's own cflocation if SaveMe is "No". --->
<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "The page passed simple validity edits.<br/>">
</cfif>

<cfif Variables.IMUserId gt 0 and Variables.PageIsUser>
	<cfif Variables.StatActvToInctvChng>
		<cfif Variables.SaveMe>
			<cfset Variables.Identifier					= "0">
			<cfset Variables.cfprname					= "IMUSERACTVINACTUPDTSP">
			<cfset Variables.IMUSERLASTUPDTUSERID		=  Variables.Origs.ImUserNm>
			<cfset Variables.LogAct						= "Update to inactive status and reason">
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERACTVINACTUPDTSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe					= "No">
                <cfdump var="#Variables.ErrMsg#"><cfabort>
			<cfelse>
				<cfset Variables.Commentary	&= "User account has been successfully updated.<br>"><!--- relocate --->
				<cflocation addtoken="No" url="#Variables.SuccessPage#?Commentary=#URLEncodedFormat(Variables.Commentary)#">
			</cfif>
			<cfabort>
		</cfif>
	</cfif>
</cfif>


<cfset Variables.CallPasswordSetRoutine						= "No">
<cfset Variables.CallValidateIMUserSuprvCSP					= "No">
<cfset Variables.SendEmailVerify							= "No">
<cfset Variables.TempPswd									= "">
<cfset Variables.IMUserSuspRsnCdOverr						= Variables.IMUserSuspRsnCd>
<cfset Variables.ColumnListIMUserTbl						= "IMUserNm,IMUserTypCd,"
															& "IMUserPrtNm,IMUserFirstNm,IMUserLastNm,IMUserMidNm,IMUserSfxNm,"
															& "IMUserDOBDt,IMUserLast4DgtSSNNmb,"
															& "IMUserStr1Txt,IMUserStr2Txt,IMUserCtyNm,StCd,Zip5Cd,Zip4Cd,IMCntryCd,IMUserStNm,IMUserPostCd,"
															& "IMUserPhnCntryCd,IMUserPhnAreaCd,IMUserPhnLclNmb,IMUserPhnExtnNmb,"
															& "IMUserFaxCntryCd,IMUserFaxAreaCd,IMUserFaxLclNmb,"
															& "IMUserEmailAdrTxt,IMUserEmailAdrTxt2,"
															& "HQLocId,LocId,PrtId,PrtLocNm,SBAOfcCd,SBAOfc1Nm,IMAsurncLvlCd,"
															& "IMAsurncLvlCd,IMUserActvInactInd,IMUserSuspRsn,IMUserSuspRsnCd,IMUserSuspRsnDt,"
															& "IMUserSuprvId,IMUserSuprvFirstNm,IMUserSuprvMidNm,IMUserSuprvLastNm,IMUserSuprvEmailAdrTxt,IMUserSuprvUserId">
<cfset Variables.ColumnListBusinesses						= "IMUserBusSeqNmb,IMUserBusEINSSNInd,IMUserBusTaxId,IMUserBusLocDUNSNmb,BusEINCertInd">

<cfif Variables.IMUserId gt 0>
	<cfif Len(Variables.IMUserSuspRsn) GT 0 <!--- and form.SUSPENDUSERACT eq "Yes" --->>
		<cfset Variables.IMUserSuspRsn						="#Variables.IMUserSuspRsn# BYSUSP:#Variables.Origs.IMUserFirstNm# #Variables.Origs.IMUserLastNm#">
	<cfelse>
		<cfset Variables.IMUserSuspRsn						= "">
	</cfif>
</cfif>
<!--- Commented to avoid the duplication of erro message--->
<!--- <cfif Variables.IMUserEmailAdrTxt NEQ Variables.IMUserEmailAdrTxt2>
	<cfset Variables.FieldErrMsg							= "Email Addresses do not match.">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
</cfif> --->
<!--- Check for length of SBA user name only if it is a new  user--->
<cfif NOT Variables.IMUserId>
	<cfif Len(Variables.IMSBACrdntlLognNm) LT 8>
		<cfset Variables.FieldErrMsg						= "SBA Username must be at least 8 characters long.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	</cfif>
</cfif>

<!--- Cross Edits Go Here: --->

<cfif Variables.PageIsAddCustomer>
	<!--- If new user, requires valid password: --->
	<cfif		Len(Variables.NewPass1) is 0>
		<cfset Variables.FieldErrMsg						= "Enter a password.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelseif	Len(Variables.NewPass2) is 0>
		<cfset Variables.FieldErrMsg						= "Re-enter password.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelse>
		<!--- Both NewPass1 and NewPass2 were given. --->
		<cfif Compare(Variables.NewPass1,Variables.NewPass2) is not 0><!--- Case-sensitive comparison. --->
			<!--- Passwords do not match: --->
			<cfset Variables.FieldErrMsg					= "The 2 passwords did not match. Please try again">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		<cfelseif NOT PasswordValidate("cls", Variables.NewPass1)>
			<!--- NewPass1 doesn't meet minimum complexity criteria: --->
			<cfset Variables.FieldErrMsg					= Variables.PWUDF.ErrMsg>
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		<cfelse>
			<cfset Variables.CallPasswordSetRoutine			="Yes">
		</cfif>
	</cfif>
</cfif>

<!--- Validate suspense reasons against Variables.ActvIMUserSuspRsnTbl here!! --->
<!--- Validate suspense reasons against Variables.ActvIMUserSuspRsnTbl here!! --->
<!--- Validate suspense reasons against Variables.ActvIMUserSuspRsnTbl here!! --->
<!--- Validate suspense reasons against Variables.ActvIMUserSuspRsnTbl here!! --->
<!--- Validate suspense reasons against Variables.ActvIMUserSuspRsnTbl here!! ---><!--- Note: last SCQ not validated. --->

<!--- dsp_userprofile.contact fields: --->

<cfif ListFind(ValueList(Variables.ScqActvCountries.code), Variables.IMCntryCd) is 0>
	<cfset Variables.FieldErrMsg							= "Invalid country code.">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm"><!--- Sets SaveMe to "No". --->
<cfelseif Variables.IMCntryCd is "US">
	<cfif ListFind(ValueList(Variables.ScqActvStates.code), Variables.StCd) is 0>
		<cfset Variables.FieldErrMsg						= "Invalid state code.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm"><!--- Sets SaveMe to "No". --->
	</cfif>
</cfif>

<!--- Separate out User Type check and wrap cross edits that require User Type in SaveMe: --->

<cfif Len(Variables.IMUserTypCd) is 0>
	<cfset Variables.FieldErrMsg							= "User Type is mandatory.">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm"><!--- Sets SaveMe to "No". --->
<cfelseif ListFind(ValueList(Variables.ActvIMUserTypTbl.code), Variables.IMUserTypCd) is 0>
	<cfset Variables.FieldErrMsg							= "Invalid User Type.">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
</cfif>

<!--- Can't do any of the job category checks if we don't have User Type, so nest in cfif SaveMe: --->

<cfif Variables.SaveMe><!--- Keep Indention low. Cross edits that require user type. --->
	<cfif Len(Variables.JobTitlCd) gt 0>
		<cfloop index="Job" list="#Variables.JobTitlCd#">
			<cfset Variables.JobAllowed						= "No">
			<cfset Variables.JobDescription					= "Job Classification code #Job#">
			<cfloop query="Variables.ActvIMJobTitlTypTbl">
				<cfif Variables.ActvIMJobTitlTypTbl.code is Job>
					<cfset Variables.JobDescription			= Variables.ActvIMJobTitlTypTbl.description>
					<cfif Variables.ActvIMJobTitlTypTbl.IMUserTypCd is Variables.IMUserTypCd>
						<cfset Variables.JobAllowed			= "Yes">
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>
			<cfif NOT Variables.JobAllowed>
				<cfset Variables.FieldErrMsg				= "#Variables.JobDescription# is not a valid Job Classification for User Type.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfloop>
	</cfif>
	<cfif Variables.IMUserTypCd eq "N" and Variables.IMUserId eq 0><!--- only evaluate if the user is new as they will be view only for existing user --->
		<cfif isDefined("Variables.JobTitlCd")>
			<!--- have 2 possible codes 7,8 if it is not defined then none was selected. Should be caught by dsp page--->
			<cfif ListFind(Variables.JobTitlCd, 7) gt 0>
				<cfset Variables.Taxid						= "1" & Variables.TaxId1 &  Variables.TaxId2 &  Variables.TaxId3>

				<cfset Variables.TaxidRetypd				= "1" & Variables.TaxId4 &  Variables.TaxId5 &  Variables.TaxId6>
				<cfif Variables.TaxidRetypd is not Variables.Taxid>
					<cfset Variables.FieldErrMsg			= "Re-entered SSN does not match SSN.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<cfif len(Variables.Taxid) lt 10>
					<cfset Variables.FieldErrMsg			= "SSN is mandatory.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfelse>
					<cfset Variables.IMUserSSNNmb = right(Variables.Taxid,9)>
				</cfif>
				<cfif Len(Variables.LoanNmb) gt 0>
                   <cfset Variables.IMUserLoanNmb	= Variables.LoanNmb>


					<cfset Variables.cfprname					= "ValidateLoanNumberforBorr">
					<cfset Variables.LogAct						= "validate borrower has a valid SSN and loan in the system">
					<cfinclude template="/cfincludes/oracle/loan/spc_CHECKLOANEXISTCSP.cfm">
					<cfif Variables.TxnErr>
						<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
						<cfabort>
					</cfif>
					<cfif isDefined("Variables.ExistInd") and Variables.ExistInd is "N">
						<cfset Variables.FieldErrMsg			= "This is not a valid SSN, Loan Number combination in our system.">
						<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
					</cfif>
				<cfelse>
					<cfset Variables.FieldErrMsg			= "SBA Loan Number is mandatory.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
			</cfif><!--- /ListFind(Variables.JobTitlCd, 7) --->

			<cfif ListFind(Variables.JobTitlCd, 8) gt 0>
				<!--- SBG Customer. Change later to Variables.!!--->
				<cfset Variables.Taxid						= "0" & Variables.EIN1 &  Variables.EIN2>
				<cfset Variables.ReTaxid					= "0" & Variables.EIN3 &  Variables.EIN4>
				<cfif Variables.ReTaxid is not Variables.Taxid>
					<cfset Variables.FieldErrMsg			= "Re-entered EIN does not match EIN.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<!--- Check DUNS - handled already--->
				<cfif Request.SlafDevTestProd is "Prod">
					<cfif Variables.SaveMe>
						<!--- Need to check for an active PRO-Net profile for EIN and DUNS number. --->
						<!--- Cannot rely on web serivce as DSBS might time out. Use cfhttp instead. --->
						<cfhttp url="#Variables.DsbsMiniProfileURL#" method="POST" timeout="90">
							<cfhttpparam type="FormField" name="TaxId" value="#Variables.Taxid#">
						</cfhttp>
						<cfset Variables.ReturnResponse		= htmlcodeformat(cfhttp.filecontent)>
						<cfif FindNoCase("wddxPacket", Variables.ReturnResponse) gt 0>
							<!--- good call--->
							<cfset Variables.contentXML		= XMLParse(cfhttp.filecontent)>
							<cfwddx action="wddx2cfml" input="#Variables.contentXML#" output="contentXML_data"/>
							<cfif isDefined("contentXML_data.DUNS")>
								<cfset Variables.DsbsDUNS	= Variables.contentXML_data.DUNS>
								<cfif Variables.DUNS EQ Variables.DsbsDUNS and Variables.contentXML_data.STATUS EQ "A">
									<!--- Match with active profile --->
									<cfset variables.ValidEINDUNS	= "Yes">
								<cfelse>
									<cfset Variables.FieldErrMsg	= "EIN & DUNS not found in DSBS.">
									<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
								</cfif>
							<cfelse>
								<cfset Variables.FieldErrMsg		= "EIN not found in DSBS.">
								<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
							</cfif>
						<cfelse>
							<cfset Variables.FieldErrMsg			= "Unable to verify EIN is in DSBS">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif>
						<!--- end wddxPacket --->
					</cfif><!--- /SaveMe --->
				</cfif><!--- /Prod --->
			</cfif><!--- /ListFind(Variables.JobTitlCd, 8) --->
		<cfelse>
			<!--- Job title not defined (means neither was selected so display error. --->
			<cfset Variables.FieldErrMsg					= "Borrowers must select Bond Borrower or Loan Borrower.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfif>
</cfif><!--- /Cross edits that require user type. --->

<!--- email check --->
<cfif Variables.IMUserEmailAdrTxt NEQ Variables.IMUserEmailAdrTxt2>
	<cfset Variables.FieldErrMsg							= "E-mail addresses did not match.">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
</cfif>
<!--- set IMUserId --->
<cfset Variables.IMUserId									= Variables.Origs.IMUserId>
<cfif Variables.IMUserId eq 0>
	<cfset Variables.IMUserNm								= Variables.IMSBACrdntlLognNm>
	<cfset Variables.NewOldUserInd							="N">
<cfelse>
	<cfset Variables.IMUserNm								= Variables.Origs.IMUserNm>
	<cfset Variables.NewOldUserInd							="E">
</cfif>
<cfif	(Len(Variables.IMUserEmailAdrTxt)					gt 0)
	and	(ListFindNoCase("S,C",Variables.IMUserTypCd)		gt 0)>
	<cfinclude template="#Variables.CLSIncludesURL#/val_GovEmail.cfm">
</cfif>

<!--- Can't do supervisor checks if we don't have User Type, so nest in cfif SaveMe: --->

<cfif Variables.SaveMe><!--- Keep Indention low. More cross edits that require user type. --->
	<!--- Validate Supervisor --->
	<cfif ListFind("C,S,U", Variables.IMUserTypCd) gt 0>
		<cfif	(Len(Variables.IMUserSuprvFirstNm)			gt 0)
			and	(Len(Variables.IMUserSuprvLastNm)			gt 0)
			and	(Len(Variables.IMUserSuprvEmailAdrTxt)		gt 0)>
			<!--- But not IMUserSuprvId, IMUserSuprvMidNm or IMUserSuprvUserId (optional). --->
			<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
		<cfelseif Variables.IMUserTypCd is "U">
			<!--- LAOs  may not have a supervisor: --->
			<cfif NOT (IsDefined("Variables.JobTitlCd") and ListFind(Variables.JobTitlCd, 3) gt 0)>
				<cfset Variables.FieldErrMsg				= "Please lookup and choose a supervisor.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			<cfelse>
				<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
			</cfif>
		<cfelse>
			<cfset Variables.FieldErrMsg					= "Please lookup and choose a supervisor.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfif><!--- /User type [usually] requires supervisor. --->
</cfif><!--- /More cross edits that require user type. --->

<cfset Variables.ArrayNewBusLen								= ArrayLen(Variables.ArrayNewBus)>
<cfloop index="Variables.NewBusIdx" from="1" to="#Variables.ArrayNewBusLen#">
	<cfset StructAppend(Variables, Variables.ArrayNewBus[Variables.NewBusIdx], "Yes")><!--- Copies row's struct data into Variables scope. --->
	<cfif Request.SlafDevTestProd is "Prod">
		<cfif Variables.IMUserBusEINSSNInd is "S">
			<cfset Variables.IMUserBusTaxIdToPronet				= "1" & Variables.IMUserBusTaxId><!--- Does DsbsMiniProfile use ELend-format TaxIds??? VERIFY!!! --->
		<cfelse><!--- else condition = the default = EIN: --->
			<cfset Variables.IMUserBusTaxIdToPronet				= "0" & Variables.IMUserBusTaxId><!--- Does DsbsMiniProfile use ELend-format TaxIds??? VERIFY!!! --->
		</cfif>
		<!---
		Need to check for an active Pronet profile for EIN and DUNS number.
		We cannot rely on web service as DSBS might time out. So use cfhttp interface instead:
		--->
		<cfhttp url="#Variables.DsbsMiniProfileURL#" method="POST" timeout="90">
			<cfhttpparam type="Formfield" value="#Variables.IMUserBusTaxIdToPronet#" name="TaxId">
		</cfhttp>
		<cfset Variables.ReturnResponse							= htmlcodeformat(cfhttp.filecontent)>
		<cfif FindNoCase("wddxPacket", Variables.ReturnResponse) gt 0>
			<!--- good call--->
			<cfset Variables.contentXML							= XMLParse(cfhttp.filecontent)>
			<cfwddx action="wddx2cfml" input="#Variables.contentXML#" output="contentXML_data"/>
			<!--- The following code is problematic, because we told the user that DUNS was optional. But leave it in for now: --->
			<cfif isDefined("contentXML_data.DUNS")>
				<cfset Variables.DsbsDUNS						= Variables.contentXML_data.DUNS>
				<cfif Variables.IMUserBusLocDUNSNmb EQ Variables.DsbsDUNS and Variables.contentXML_data.STATUS EQ "A">
					<!--- Match with active profile --->
					<cfset variables.ValidEINDUNS				= "Yes">
				<cfelse>
					<cfset Variables.FieldErrMsg				= "EIN & DUNS not found in DSBS.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
			<cfelse>
				<cfset Variables.FieldErrMsg					= "Tax ID not found in DSBS.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		<cfelse>
			<cfset Variables.FieldErrMsg						= "Unable to verify Tax ID is in DSBS">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfif>
</cfloop>

<!--- Assurance level only changeable by SecAdmins and must match foreign key constraint: --->
<cfif StructKeyExists(Variables.ResolvedDefaults, "UserIMAsurncLvlCd")
	and	(Variables.ResolvedDefaults.UserIMAsurncLvlCd.MandOpt is not "skip")>
	<!--- The fact that UserIMAsurncLvlCd is in ResolvedDefaults implies it was on the dsp page, but confirm that just in case: --->
	<cfif NOT Variables.PageIsUser>
		<!--- It would be nice to have extra feedback if error ever did occur, so include CGI.Script_Name in the error message: --->
		<cfset Variables.FieldErrMsg						= "Assurance level is not editable in #CGI.Script_Name#.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		<cfset Variables.IMAsurncLvlCd						= Variables.Origs.IMAsurncLvlCd><!--- Leave Variables.UserIMAsurncLvlCd untouched. --->
	<cfelseif ListFind(ValueList(Variables.ActvIMAsurncLvlTbl.IMAsurncLvlCd), Variables.UserIMAsurncLvlCd) is 0>
		<cfset Variables.FieldErrMsg						= "Assurance level is not a valid code.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		<cfset Variables.IMAsurncLvlCd						= Variables.Origs.IMAsurncLvlCd><!--- Leave Variables.UserIMAsurncLvlCd untouched. --->
	</cfif>
</cfif>

<!--- Any time CAPTCHA is on the screen (probably only in PageIsAddCustomer), validate it: --->

<cfif StructKeyExists(Form, "Verify")><!--- Form names are not database column names, but Verify isn't saved to the database. --->
	<cfif Len(Form.Verify) gt 0>
		<cfif Compare(Form.Verify, Variables.Origs.Verify) is not 0><!--- Case-sensitive comparison. --->
			<cfset Variables.FieldErrMsg					= "Verify question (CAPTCHA image) failed. Please try again. ">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	<cfelse>
		<cfset Variables.FieldErrMsg						= "Please answer Verify question (CAPTCHA image).">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	</cfif>
</cfif>
<!--- Prevalidate ImUserNm and Email used to avoid duplicates --->
<cfset Variables.Identifier					= "0">
<cfset Variables.cfprname					= "GetErrs">
<cfset Variables.LogAct						= "Prevalidate account info">
<cfinclude template="/cfincludes/oracle/security/spc_NEW_PREVALIDATEIMUSERACCTCSP.cfm">
<cfif Variables.TxnErr>
	<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
	<cfabort>
<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
	<cfset Variables.SaveMe						= "No">
	<cfset Variables.FieldErrMsg					= "#GetErrs.RecordCount# validation error(s) occurred:">
	<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfloop query="GetErrs">
		<cfset Variables.FieldErrMsg				= "#NumberFormat(GetErrs.CurrentRow, '00000')#. #GetErrs.ErrTxt# (#GetErrs.ErrCd#)">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	</cfloop>
<cfelseif Variables.UpdateTrace>
	<cfset Variables.Commentary					&= "Prevalidation of duplicates is successful.<br/>">
</cfif>


<cfif NOT Variables.SaveMe>
	<!--- ((in cross edits)) --->
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cfif (Variables.PageIsUser eq "Yes") and (Variables.NewOldUserInd eq "N")>
		<cfset Variables.IMUserFirstNm						= "">
		<cfset Variables.IMUserLastNm						= "">
		<cfset Variables.IMUserTypCd						= "">
		<cfset Variables.IMUserLastNm						= "">
	</cfif>
	<cfif (Variables.NewOldUserInd eq "N")>
		<cfset Variables.PageTitle 							="Add">
	</cfif>

    <cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y&IMUserId=#Variables.IMUserId#&PageTitle=#Variables.PageTitle#&IMUserTypCd=#Variables.IMUserTypCd#&IMUserFirstNm=#URLEncodedFormat(Variables.IMUserFirstNm)#&IMUserLastNm=#URLEncodedFormat(Variables.IMUserLastNm)#">
	<!--- <cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y"> --->
</cfif>

<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "The page passed cross edits.<br/>">
</cfif>

<!--- Simple mechanism for detecting changed data, based on current data structures: --->

<cfif Variables.IMUserId gt 0>
	<cfif SomethingChanged(Variables.ColumnListIMUserTbl)>
		<cfloop index="Variables.ColName" list="#Variables.ColumnListIMUserTbl#">
			<cfset Variables[Variables.ColName&"Changed"]	= (Variables[Variables.ColName] is not Variables.Origs[Variables.ColName])>
		</cfloop>
	</cfif>
</cfif>

<!--- BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. --->
<!--- BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. --->
<!--- BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. --->
<!--- BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. --->
<!--- BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. BEGIN UPDATES. --->

<cfif Variables.SaveMe>
	<cftransaction action="Begin">
		<cfset Variables.encryptionKey						= "BeMy01BstCstmrKyToAppl==">
		<!--- Comparing job titles --->
		<!--- <cfif not compareNoCase( listSort(Variables.JobTitlCd, 'numeric'), listSort(Variables.Origs.JobTitlCd, 'numeric') ) EQ 0>
			<cfset Variables.JobTitleChanged 				= true>
			</cfif> --->
		<cfif ListFindNoCase("S,U,N", Variables.IMUserTypCd)	gt 0>
			<cfset Variables.JobTitlCdStr	 				= Variables.JobTitlCd>
		<cfelse>
			<cfset Variables.JobTitlCdStr	 				= "">
		</cfif>

		<!--- New User/Customer --->
		<cfif Variables.IMUserId eq 0>
			<!--- only new customers(not security users) must be verified --->
			<cfif Variables.PageIsAddCustomer>
				<cfset Variables.SendEmailVerify 			= "Yes">
			</cfif>
			<cfset Variables.IMUserNm 						= Variables.IMSBACrdntlLognNm>
			<cfset Variables.IMSBACrdntlCreatUserId 		= Variables.IMSBACrdntlLognNm>
			<cfset Variables.IMAsurncLvlCd					= 1>
			<!--- generate random password for insert --->
			<cfset PasswordAvoidConfusingCharacters()>
			<cfset Variables.ClearTextPassword				= PasswordGenerate("clsweak")>
			<cfset Variables.IMSBACrdntlPasswrd				= Hash(Variables.ClearTextPassword, "SHA1")>
			<!--- <cfset Variables.username 				= "">
			<cfset Variables.password 				= ""> --->
			<cfif  Variables.PageIsAddCustomer>
				<cfset Variables.IMUserActvInactInd			= "I">
				<cfset Variables.IMUserSuspRsnCd			= "1">
				<cfset Variables.IMUserSuspRsnDt			= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
			<cfelse>
				<cfset Variables.IMUserActvInactInd			= "A">
			</cfif>
			<cfset Variables.cfprname						= "InsCust">
			<cfset Variables.NewOldUserInd					= "N">
			<cfset Variables.IMUserLastUpdtUserId			= Variables.IMSBACrdntlLognNm>
			<cfset Variables.LogAct							= "created the account">
			<cfinclude template="/cfincludes/oracle/security/spc_JB_IMUSERINSCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "User 'insert' successful.<br/>">
			</cfif>
			<cfset Variables.OrigUserName 					= Variables.IMSBACrdntlLognNm>
			<cfset Variables.OrigPassword 					= Variables.ClearTextPassword>

		<cfif Variables.IMUserId eq 0>
			<!--- have 2 possible codes 7,8 if it is not defined then none was selected. Should be caught by dsp page--->
			<cfif isDefined("Variables.JobTitlCd") and ListFind(Variables.JobTitlCd, 7) gt 0>
                <cfset Variables.Identifier						= "0">
				<cfset Variables.IMUserLastUpdtUserId	 		= Variables.IMUserNm>
				<cfset Variables.cfprname						= "Insert">
				<cfset Variables.LogAct							= "do a one time insert of the SSN and Loan Number">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERSSNINSTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe						= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary					&= "User update successful.<br/>">
				</cfif>

			<cfelseif isDefined("Variables.JobTitlCd") and ListFind(Variables.JobTitlCd, 8) gt 0>

			</cfif><!--- with job title code 7 or 8, nothing for 8 yet --->
		</cfif><!--- usertype borrower --->
			<!--- <cfdump var="p=#Variables.Password#"><cfdump var="u=#Variables.UserName#">
				<cfinclude template="/cls/act_setpass.cfm"> --->
		<cfelse>
			<!--- Existing Security User/Profile --->
			<cfif Variables.PageIsProfile>
				<cfset Variables.IMUserActvInactInd			= Variables.Origs.IMUserActvInactInd>
				<cfset Variables.IMUserSuspRsnCd			= Variables.Origs.IMUserSuspRsnCd>
				<cfset Variables.IMUserActvInactIndChanged	= "No">
			</cfif>
			<!---
			Use IMUserUpdTSP Identifier 12 (not Identifier 0) to update IMAsurncLvlCd! And only if user is SBAUser and PageIsUser!
			Code will have to be added to do that. That's why we're not overwriting Variables.UserIMAsurncLvlCd. It doesn't participate in SPCs.
			--->
			<cfset Variables.IMAsurncLvlCd					= Variables.Origs.IMAsurncLvlCd><!--- Leave Variables.UserIMAsurncLvlCd untouched. --->
			<cfif 	(isDefined("Variables.IMUserSuprvIdChanged") and Variables.IMUserSuprvIdChanged)
					or ( isDefined("Variables.IMUserSuprvEmailAdrTxtChanged") and Variables.IMUserSuprvEmailAdrTxtChanged )>
				<cfset Variables.SendEmailSupervisorFlag	= true>
				<cfset Variables.IMUserSuspRsnCd			= 2>
			</cfif>
			<!--- <cfif isDefined("Variables.HQLocIdChanged") and Variables.HQLocIdChanged>
				<cfset Variables.SendHQLocEmailFlag			= true>
				<cfset Variables.SendEmailVerify			= "Yes">
				<cfset Variables.IMUserSuspRsnCd			= 3>
			</cfif> --->
			<cfif isDefined("Variables.IMUserEmailAdrTxtChanged") and Variables.IMUserEmailAdrTxtChanged>
				<cfset Variables.SendEmailAddrEmailFlag		= true>
				<cfset Variables.SendEmailVerify			= "Yes">
				<cfset Variables.IMUserSuspRsnCd			= 3>
			</cfif>

			<cfif isDefined("Variables.IMUserTypCdChanged") and Variables.IMUserTypCdChanged>
				<cfset Variables.SendEmailUserTypeFlag		= true>
				<cfset Variables.SendEmailVerify			= "Yes">
				<cfset Variables.IMUserSuspRsnCd			= 3>
			</cfif>

			<!--- update User record --->
			<cfset Variables.NewOldUserInd 					= "E">
			<cfset Variables.Identifier						= "0">
			<cfset Variables.IMUserLastUpdtUserId	 		= Variables.IMUserNm>
			<cfset Variables.cfprname						= "UpdExistProfile">
			<cfset Variables.LogAct							= "update the profile itself">
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERUPDTSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "User update successful.<br/>">
			</cfif>
			<cfif Variables.SendEmailSupervisorFlag
				or Variables.SendEmailUserTypeFlag
				or Variables.SendEmailAddrEmailFlag
				or ( isDefined("Variables.HQLocIdChanged") and Variables.HQLocIdChanged )
				or ( isDefined("Variables.IMUserSuspRsnCdChanged") and Variables.IMUserSuspRsnCdChanged )
				or ( isDefined("Variables.IMUserActvInactIndChanged") and (Variables.IMUserActvInactIndChanged eq "Yes") and (Variables.IMUserActvInactInd eq "A") )
			>
				<!--- get values to pass to the SP  --->
				<cfset Variables.Identifier					= "0">
				<cfset Variables.cfprname					= "getUserData">
				<cfset Variables.LogAct						= "get account information">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERSELTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfdump var="Serious database error occured. Following information might help. #Variables.ErrMsg#">
					<cfabort>
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary				&= "User 'select' successful.<br/>">
				</cfif>
				<!--- <cfdump var="#getUserData#"> --->
				<!--- get IMUserSuspRsnCd to pass to SP --->
				<cfif getUserData.recordcount>
					<cfset Variables.IMUserSuspRsnCd		= trim(getUserData.IMUserSuspRsnCd)>
				</cfif>
				<!--- <cfdump var="#Variables.IMUserSuspRsnCd#"><cfabort> --->
				<!--- If account status changed from inactive to active --->
				<cfif ( isDefined("Variables.IMUserActvInactIndChanged")
						and (Variables.IMUserActvInactIndChanged eq "Yes")
						and (Variables.IMUserActvInactInd eq "A") )>
					<cfset Variables.SendEmailAddrEmailFlag		= true>
					<cfset Variables.SendEmailVerify			= "Yes">
					<cfset Variables.IMUserSuspRsnCd			= 3>
				<cfelse>
					<!--- If suspense reason changed --->
					<cfif isDefined("Variables.IMUserSuspRsnCdChanged") and Variables.IMUserSuspRsnCdChanged>
						<cfif Variables.IMUserSuspRsnCdOverr neq Variables.Origs.IMUserSuspRsnCd>
							<cfset Variables.IMUserSuspRsnCd	= Variables.IMUserSuspRsnCdOverr>
							<cfif ListFindNoCase("1,3",Variables.IMUserSuspRsnCdOverr) gt 0 >
								<cfset Variables.SendEmailUserTypeFlag 		= true>
								<cfif (Variables.PageIsUser eq "Yes") and (Variables.NewOldUserInd eq "E")>
									<cfset Variables.SendEmailVerify = "Yes">
								</cfif>
								<!--- conditional  to send email --->
							<cfelseif ListFindNoCase("2",Variables.IMUserSuspRsnCdOverr)>
								<cfset Variables.SendEmailSupervisorFlag 	= true>
								<!--- conditional to send email --->
							</cfif>
						</cfif>
					</cfif>
				</cfif>
				<!--- call SP to update suspend reason, active/inactive and hqlocid --->
				<cfset Variables.Identifier					= "17">
				<cfset Variables.NewOldUserInd 				= 'E'>
				<cfset Variables.IMUserLastUpdtUserId	 	= Variables.IMUserNm>
				<cfset Variables.cfprname					= "UpdExistProfile">
				<cfset Variables.LogAct						= "Update Existing Profile">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary				&= "User update '17' successful.<br/>">
				</cfif>
			</cfif>
		</cfif><!--- /IMUserId --->

		<!--- update job title --->
		<cfset Variables.Identifier							= 0>
		<cfif Variables.PageIsUser>
			<cflock scope="session" timeout="25">
				<cfset Variables.lastupdtuserid 			= session.OrigUserId>
			</cflock>
		<cfelse>
			<cfset Variables.lastupdtuserid					= Variables.IMUserNm>
		</cfif>
		<cfset Variables.LogAct								= "save user title">
		<cfinclude template="/cfincludes/oracle/security/spc_IMUSERTYPTITLINSUPDCSP.cfm">
		<cfif Variables.TxnErr>
			<cfset Variables.SaveMe							= "No">
		<cfelseif Variables.UpdateTrace>
			<cfset Variables.Commentary						&= "Job category changes successful.<br/>">
		</cfif>
		<cfif Variables.CallPasswordSetRoutine>
			<!--- update with new password --->
			<cfset Variables.Identifier						= "11">
			<cfset Variables.RetVal							= "0">
			<!--- Initialize output parameter. --->
			<cfset Variables.IMSBACrdntlFirstLognInd		= "N">
			<cfset Variables.IMSBACrdntlPasswrd				= Hash(trim(Variables.NewPass1), "SHA1")>
			<cfset Variables.IMSBACrdntlPasswrdExprDt		= DateAdd("d", 90, Now())>
			<cfset Variables.IMSBACRDNTLLASTUPDTUSERID		=  Variables.IMUserNm>
			<!--- Variables.IMSBACrdntlLognNm --->
			<cfset Variables.cfprname						= "Ignored">
			<cfset Variables.LogAct							= "set the user's password">
			<cfinclude template="/cfincludes/oracle/security/spc_IMSBACRDNTLUPDTSP.cfm">
			<cfif Variables.TxnErr>
				<!--- Leave Variables.ErrMsg containing whatever got passed back from the SPC file. --->
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.RetVal is 0>
				<!--- Failure --->
				<cfset Variables.FieldErrMsg					= "User's profile not found.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "Credential update successful.<br/>">
			</cfif>
		</cfif>
<!--- validate that EMail domain --->
		<cfset Variables.LogAct							= "validate domains"><!--- Checks more, but user doesn't need to know that. --->
		<cfset Variables.cfprname						= "GetErrs">
        <cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATEIMUSERACCTCSP.cfm">
		<cfif Variables.TxnErr>
			<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
			<cfabort>
		<cfelseif IsDefined("GetErrs.RecordCount") and (GetErrs.RecordCount gt 0)>
			<cfset Variables.SaveMe						= "No">
		<cfelseif Variables.UpdateTrace>
			<cfset Variables.Commentary					&= "User validation call was successful.<br/>">
		</cfif>
		<!--- Add new businesses: --->

		<cfset Variables.Identifier							= 0>
		<cfset Variables.IMUserBusStrtDt					= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
		<cfset Variables.IMUserBusLocStrtDt					= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
		<cfset Variables.IMUserBusCreatUserId				= Variables.IMUserNm>
		<cfloop index="Variables.NewBusIdx" from="1" to="#Variables.ArrayNewBusLen#">
			<cfif isDefined("Variables.TxnErr") and Variables.TxnErr>
				<cfset Variables.SaveMe						= "No">
				<cfbreak>
			</cfif>
			<cfset StructAppend(Variables, Variables.ArrayNewBus[Variables.NewBusIdx], "Yes")><!--- Copies row's struct data into Variables scope. --->
			<cfset Variables.cfprname						= "IMUSERBUSINSCSP">
			<cfset Variables.LogAct							= "Insert Business">
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSINSCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe						= "No">
				<cfbreak>
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "New business ###Variables.NewBusIdx# save successful.<br/>">
			</cfif>
			<!--- Validate values to be inserted (called after insert again now)  --->
			<cfset Variables.cfprname						= "GetErrs">
			<cfset Variables.LogAct							= "validate new business ###Variables.NewBusIdx#">
			<cfinclude template="/cfincludes/oracle/security/spc_NEW_BUSINSVALIDATECSP.cfm">
			<cfif Variables.TxnErr>
				<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
				<cfabort>
			<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "New business validation ###Variables.NewBusIdx# call was successful.<br/>">
			</cfif>
		</cfloop>

		<!--- delete (update end date) businesses --->

		<cfset Variables.IMUserBusLocCreatUserId			= Variables.IMUserNm>
		<cfset Variables.IMUserBusCreatUserId				= Variables.IMUserNm>
		<cfset Variables.IMUserBusEndDt						= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
		<cfset Variables.IMUserBusLocEndDt					= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>

		<cfloop index="delarridx" from="1" to="#ArrayLen(Variables.ArrayBusDel)#">
			<cfset StructAppend(Variables, Variables.ArrayBusDel[delarridx], "Yes")><!--- Copies row's structure into Variables scope. --->
			<cfif Len(Variables.IMUserBusLocDUNSNmb)		gt 0><!--- validate business deleted (note: called before delete)--->
				<cfset Variables.cfprname					= "GetErrs">
				<cfset Variables.LogAct						= "Validate business deleted">
				<cfinclude template="/cfincludes/oracle/security/spc_NEW_BUSDELVALIDATECSP.cfm">
				<cfif Variables.TxnErr>
					<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
					<cfabort>
				<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
					<cfset Variables.SaveMe						= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary					&= "Business deletion validation ###delarridx# call was successful.<br/>">
				</cfif>

				<cfset Variables.Identifier					= "11">
				<cfset Variables.cfprname					= "IMUSERBUSLOCUPDTSP">
				<cfset Variables.LogAct						= "Delete(Update end date) Business Location Duns">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSLOCUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary				&= "Removing DUNS ###delarridx# successful.<br/>">
				</cfif><!---  ((end-date IMUserBusLocTbl for Variables.IMUserId, Variables.IMUserBusSeqNmb, Variables.IMUserBusLocDUNSNmb)) --->
			<cfelse>
				<cfset Variables.Identifier					= "11">
				<cfset Variables.cfprname					= "IMUSERBUSUPDTSP">
				<cfset Variables.LogAct						= "Delete(Update end date) Business">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary				&= "Removing business ###delarridx# successful.<br/>">
				</cfif><!--- ((end-date IMUserBusTbl for Variables.IMUserId, Variables.IMUserBusSeqNmb)) --->
			</cfif>
		</cfloop>

		<!--- Server side BR validations - Variables.NewOldUserInd defined above --->
		<!--- Validate supervisor --->
		<cfif ListFindNoCase("S,C,U", Variables.IMUserTypCd) gt 0>
			<cfif Variables.CallValidateIMUserSuprvCSP>
				<cfset Variables.cfprname				= "GetErrs">
				<cfset Variables.LogAct					= "validate supervisor">
				<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATESUPRVCSP.cfm">
				<cfif Variables.TxnErr>
					<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
					<cfabort>
				<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
					<cfset Variables.SaveMe						= "No">
				<cfelseif Variables.UpdateTrace>
					<cfset Variables.Commentary			&= "Supervisor validation call was successful.<br/>">
				</cfif>
			</cfif>
		</cfif>

		<!--- Validate Headquarters Location --->
		<cfif ListFindNoCase("U", Variables.IMUserTypCd) gt 0>
			<cfset Variables.JOBTITLCDSTR					= Variables.JOBTITLCD>
			<cfset Variables.cfprname						= "GetErrs">
			<cfset Variables.LogAct							= "validate Headquarters Location">
			<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATELOCCSP.cfm">
			<cfif Variables.TxnErr>
				<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
				<cfabort>
			<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
				<cfset Variables.SaveMe						= "No">
			<cfelseif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "Validating Headquarters Location successful.<br/>">
			</cfif>
		</cfif>
		<!--- Validate phone and address --->
		<cfset Variables.LogAct								= "validate Phone/Address">
		<cfset Variables.cfprname							= "GetErrs">
		<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATEPHNADDRCSP.cfm">

		<cfif Variables.TxnErr>
			<cfoutput><div class="dem_errmsg">#Variables.ErrMsg#</div></cfoutput>
			<cfabort>
		<cfelseif IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
			<cfset Variables.SaveMe						= "No">
		<cfelseif Variables.UpdateTrace>
			<cfset Variables.Commentary						&= "Phone/address validation call was successful.<br/>">
		</cfif>
		<cfif isDefined("GetErrs.RecordCount") and GetErrs.RecordCount gt 0>
			<cfset Variables.FieldErrMsg					= "#GetErrs.RecordCount# validation error(s) occurred:">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			<cfloop query="GetErrs">
				<!---
				The following is in the same format as validation errors in ELend's XML processing. Having the numbering of errors
				5 digits long makes it easier to distinguisly presave validation errors from stored procedure validation errors:
				--->
				<cfset Variables.FieldErrMsg				= "#NumberFormat(GetErrs.CurrentRow, '00000')#. #GetErrs.ErrTxt# (#GetErrs.ErrCd#)">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfloop>
		</cfif>
		<!--- ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. --->
		<!--- ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. --->
		<!--- ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. --->
		<!--- ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. --->
		<!--- ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. ROLLBACK OR COMMIT. --->

		<cfif NOT Variables.SaveMe>
			<cftransaction action="Rollback" />
			<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
			<cfif <!--- (Variables.PageIsUser eq "Yes") and ---> (Variables.NewOldUserInd eq "N")>
				<cfset Variables.IMUserFirstNm				= "">
				<cfset Variables.IMUserLastNm				= "">
				<cfset Variables.IMUserId					= 0>
			</cfif>
			<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y&IMUserId=#Variables.IMUserId#&IMUserFirstNm=#URLEncodedFormat(Variables.IMUserFirstNm)#&IMUserLastNm=#URLEncodedFormat(Variables.IMUserLastNm)#">
		<cfelse>

			<cftransaction action="Commit" />
			<cfif Variables.UpdateTrace>
				<cfset Variables.Commentary					&= "Changes committed.<br/>">
			</cfif>
			<!--- send emails --->
			<cfif Request.SlafDevTestProd neq "Prod">
				<cfset Variables.IMUserEmailAdrTxt			= Variables.IMUserEmailAdrTxt & Variables.DevTeamEml>
			</cfif>


			<!--- send login info for the user created in Security section --->
			<cfif (Variables.PageIsUser eq "Yes") and (Variables.NewOldUserInd eq "N")>
				<cfif Len(Variables.ClearTextPassword)		GT 0>
					<!--- send login info for the new user created in Security section --->
					<cfset Variables.username				= "">
					<cfset Variables.password				= "">
					<cfset Variables.cfprname				= "getEmail">
					<cfset Variables.LogAct					= "get email content">
					<cfset Variables.emailcdseqnmb			= "2">
					<cfset Variables.prmtr2					= Variables.IMUserNm>
					<cfset Variables.prmtr3					= Variables.ClearTextPassword>
					<cfset Variables.USERURLDSPLY			= request.slafloginserverurl & "/cls">
					<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
					<cfif Variables.TxnErr>
						<cfset Variables.SaveMe				= "No">
						<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
						<cfabort>
					</cfif>
					<cfif Request.SlafDevTestProd neq "Prod">
						<cfset Variables.EMAILRCPNT			= Variables.EMAILRCPNT & Variables.DevTeamEml>
					</cfif>

					<cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
						type="HTML" subject="#Variables.EMAILSUBJ#"
					>#Variables.emailtxt#</cfmail>
				</cfif>
			</cfif>
			<cfif Variables.SendEmailVerify>
				<cfset Variables.UPURLString				= Encrypt('IMUserId=' & trim(Variables.IMUserId), Variables.encryptionKey,"AES/CBC/PKCS5Padding", "hex")>
				<cfset Variables.cfprname					= "getEmail">
				<cfset Variables.LogAct						= "get email content">
				<cfif Variables.PageIsAddCustomer>
					<cfset Variables.emailcdseqnmb			= "11">
				<cfelse>
					<cfset Variables.emailcdseqnmb			= "8">
				</cfif>
				<cfset Variables.prmtr3						= "#request.slafloginserverurl#/cls/dsp_clsuserconfrminfo.cfm?#UrlEncodedFormat(Variables.UPURLString)#&1=1">
				<cfset Variables.USERURLDSPLY				= "#request.slafloginserverurl#/cls">
				<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
					<cfabort>
				</cfif>
				<cfif Request.SlafDevTestProd neq "Prod">
					<cfset Variables.EMAILRCPNT				= Variables.EMAILRCPNT & Variables.DevTeamEml>
				</cfif>

				<cfmail	to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
						type="HTML" subject="#Variables.EMAILSUBJ#"
				>#Variables.emailtxt#</cfmail>
			</cfif>
			<cfif Request.SlafDevTestProd neq "Prod">
				<cfset Variables.IMUserEmailAdrTxt			= Variables.IMUserEmailAdrTxt & Variables.DevTeamEml>
				<cfif ListFindNoCase( "S,U,C", trim(Variables.IMUserTypCd) )  gt 0>
					<cfset Variables.IMUserSuprvEmailAdrTxt	= Variables.IMUserSuprvEmailAdrTxt & Variables.DevTeamEml>
				</cfif>
			</cfif>
			<!---  if only supervisor changed --->
			<cfif	(Variables.SendEmailSupervisorFlag		is true)
				and	(Variables.SendEmailUserTypeFlag		is false)
				and	(Variables.SendEmailAddrEmailFlag		is false)
				and	(Variables.SendHQLocEmailFlag			is false)
				>
				<cfset Variables.cfprname					= "getEmail">
				<cfset Variables.LogAct						= "get email content">
				<cfset Variables.emailcdseqnmb				= "6">
				<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
					<cfabort>
				</cfif>
				<cfif Variables.DevTestProdInd NEQ 2>
					<cfset Variables.EMAILRCPNT				= Variables.EMAILRCPNT & Variables.DevTeamEml>
				</cfif>
				<cfmail	to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
						type="HTML" subject="#Variables.EMAILSUBJ#"
				>#Variables.emailtxt#</cfmail>

				<cfset Variables.cfprname					= "getEmail">
				<cfset Variables.LogAct						= "get email content">
				<cfset Variables.emailcdseqnmb				= "5">
				<cfset Variables.USERURLDSPLY				= request.slafloginserverurl & "/cls">
				<cfset Variables.EMAILRCPNT					= Variables.IMUserSuprvEmailAdrTxt>
				<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
					<cfabort>
				</cfif>
				<cfif Variables.DevTestProdInd NEQ 2>
					<cfset Variables.EMAILRCPNT				= Variables.EMAILRCPNT & Variables.DevTeamEml>
				</cfif>
				<cfmail	to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
						type="HTML" subject="#Variables.EMAILSUBJ#"
				>#Variables.emailtxt#</cfmail>
			</cfif>

			<!--- set session variable if account has been suspended --->
			<cfif (Variables.SendEmailUserTypeFlag or Variables.SendEmailAddrEmailFlag
				or Variables.SendEmailSupervisorFlag or Variables.SendHQLocEmailFlag) and Variables.SaveMe>
				<cfif Variables.PageIsUser>
					<cfif Variables.IMUserNm eq session.IMUserNm>
						<cflock scope="Session" type="Exclusive" timeout="30">
							<cfset Session.AccountIsSuspended = true>
						</cflock>
					</cfif>
				<cfelse>
					<cflock scope="Session" type="Exclusive" timeout="30">
						<cfset Session.AccountIsSuspended	= true>
					</cflock>
				</cfif>
			</cfif>
		</cfif><!--- /SaveMe that controls Rollback/Commit. --->
	</cftransaction>
</cfif><!--- /SaveMe that controls entry into cftransaction. --->

<!--- END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. --->
<!--- END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. --->
<!--- END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. --->
<!--- END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. --->
<!--- END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. END UPDATES. --->

<cfif Variables.SaveMe>
	<cfif Variables.PageIsAddCustomer>
		<cfset Variables.Commentary							&= "Your account has been successfully created and email has been sent. "
															& "Please check your email.">
	<cfelseif Variables.PageIsProfile>
		<cfset Variables.Commentary							&= "Your profile information has been successfully updated. ">
		<cfif Variables.SendEmailVerify>
			<cfset Variables.Commentary						&= "An email has been sent. Please check your email.">
		</cfif>
	<cfelseif Variables.PageIsUser>
		<cfif Variables.NewOldUserInd eq "N">
			<cfset Variables.Commentary						&= "User account has been successfully created.<br>"
															& "An email with the login information has been sent to the user.">
		<cfelse>
			<cfset Variables.Commentary						&= "User account has been successfully updated.<br>">
			<cfif Variables.SendEmailVerify>
				<cfset Variables.Commentary					&= "An email has been sent to the user.">
			</cfif>
		</cfif>
	</cfif>
</cfif>
<cfset Variables.ErrMsg										= "">
<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
<cflock scope="Session" type="Exclusive" timeout="30">
	<!---
	We no longer want Form Data Recovery to occur on the page we are cflocationing to. The following cfset prevents
	Form Data Recovery from occurring, as if we came from some other page:
	--->
	<cfset Session.SlafSaveFormData.PageNames				= "">
</cflock>
<cfif	Variables.PageIsUser
	and (Variables.NewOldUserInd							eq "N")>
	<!--- This implies change of IMUserId from 0 to generated value. Change success page to act_search to set new IMUserId: --->
	<cfset Variables.URLString								= "/security/user/act_search.cfm"
															& "?NewUserProfile=Yes"
															& "&IMUserId=#URLEncodedFormat(Variables.IMUserId)#"
															& "&Commentary=#URLEncodedFormat(Variables.Commentary)#">
	<cflocation addtoken="No" url="#Variables.URLString#">
	<!--- Wherever caller told us to go. --->
<cfelse>
	<cflocation addtoken="No" url="#Variables.SuccessPage#?Commentary=#URLEncodedFormat(Variables.Commentary)#">
	<!--- Wherever caller told us to go. --->
</cfif>

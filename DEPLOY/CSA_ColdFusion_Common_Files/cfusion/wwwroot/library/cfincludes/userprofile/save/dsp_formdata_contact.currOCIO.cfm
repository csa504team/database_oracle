<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				09/01/2011
DESCRIPTION:		Displays the contact part of the user profile for use in a form. 
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link 

index.html. 
INPUT:				Variables scope variables for each of the IMUserTbl columns supported. Only exception: 

IMUserZipCd, 
					which is IMUserTbl's Zip5Cd and/or Zip4Cd combined into 99999 or 99999-9999 format. 
					ReadOnlyProfile and other flags. 
OUTPUT:				Form elements in "tbl tags". 
REVISION HISTORY:	09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, 

copied to 
										/library/cfincludes/userprofile, adapted to 

use "tbl" classes instead of tables. 
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables. 
					05/16/2006, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<!--- <cfset Variables.AreaCdLabelContentDom					= "(Area Code)"> --->
<cfset Variables.AreaCdLabelContentDom					= "(Area Code)">
<cfset Variables.AreaCdLabelContentFor					= "(City Code)">

<!--- Initializations: --->

<cfparam name="Variables.LibIncURL"						default="/library/cfincludes">
<cfparam name="Variables.LibJSURL"						default="/library/javascripts">
<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfset Variables.ScqActvCountries					= Server.Scq.ActvIMCntryCdTbl>
	<cfset Variables.ScqActvStates						= Server.Scq.ActvStTbl>
</cflock>
<cfinclude template="bld_readonly_contact.cfm">

<!--- Derive phone country code from address country code or default (US): --->

<cfif (Len(Variables.IMCntryCd) GT 0) AND (Len(Variables.IMUserPhnCntryCd) IS 0)>
	<!--- If we have an address country code but no phone country code, default phone to equivalent of address: --->
	<cfloop query="Variables.ScqActvCountries">
		<cfif Variables.ScqActvCountries.IMCntryCd IS Variables.IMCntryCd>
			<cfset Variables.IMUserPhnCntryCd			= Variables.ScqActvCountries.IMCntryDialCd>
			<cfbreak>
		</cfif>
	</cfloop>
</cfif>
<cfif Len(Variables.IMUserPhnCntryCd) IS 0>
	<!--- If we STILL don't have phone country code, assume US: --->
	<cfset Variables.IMUserPhnCntryCd					= "1">
</cfif>

<!--- Determine whether to display foreign or domestic: --->

<cfif (Variables.IMCntryCd IS "US") OR (Len(Variables.IMCntryCd) IS 0)>


	<cfset Variables.CtyNmDataMandOpt					= "manddata">
	<cfset Variables.CtyNmLabelMandOpt					= "mandlabel">
	<cfset Variables.DomAddrDisplay						= "">
	<cfset Variables.ForAddrDisplay						= "none">
	<cfif Len(Variables.StCd) eq 2>
		<cfset Variables.StCdDataDisplay				= "">
		<cfset Variables.StCdLabelDisplay				= "">
	<cfelse>
		<cfset Variables.StCdDataDisplay				= "none">
		<cfset Variables.StCdLabelDisplay				= "none">
	</cfif>
<cfelse>
	<cfset Variables.CtyNmDataMandOpt					= "optdata">
	<cfset Variables.CtyNmLabelMandOpt					= "optlabel">
	<cfset Variables.DomAddrDisplay						= "none">
	<cfset Variables.ForAddrDisplay						= "">
	<cfset Variables.StCdDataDisplay					= "none">
	<cfset Variables.StCdLabelDisplay					= "none">
</cfif>
<cfif Variables.IMUserPhnCntryCd IS "1">
	<cfset Variables.AreaCdDataMandOpt					= "manddata">
	<cfset Variables.AreaCdLabelMandOpt					= "mandlabel">
	<cfset Variables.AreaCdLabelContent					= Variables.AreaCdLabelContentDom>
<cfelse>
	<cfset Variables.AreaCdDataMandOpt					= "optdata">
	<cfset Variables.AreaCdLabelMandOpt					= "optlabel">
	<cfset Variables.AreaCdLabelContent					= Variables.AreaCdLabelContentDom>
</cfif>

<cfif NOT IsDefined("Variables.IMUserEmailAdrTxt2")>
	<cfset Variables.IMUserEmailAdrTxt2					= Variables.IMUserEmailAdrTxt>
</cfif>

<cfif Variables.ReadOnlyProfile>
	<!--- Treat this as a special case. Reverse order to make it more like the way people normally read addresses: --->
	<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Street 1:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStr1Txt#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Street 2:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStr2Txt#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">City:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserCtyNm#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
<div id="DivDomAddr" style="display:#Variables.DomAddrDisplay#"><!-- ********* Domestic address only, begins. ********* -->
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">State:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.StCd#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Zip+4:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.Zip5Cd#<cfif Len(Variables.Zip4Cd) gt 0> - #Variables.Zip4Cd#</cfif>
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</div><!-- ********* Domestic address only, ends. ********* -->
<div id="DivForAddr" style="display:#Variables.ForAddrDisplay#"><!-- ********* Foreign address only, begins. ********* -->
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">State/Province:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStNm#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Postal Code:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserPostCd#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</div><!-- ********* Foreign address only, ends. ********* -->
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Country Code:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMCntryCd#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel">Phone Number:</div>
      <div class="tbl_cell formdata">
        <div class="inlineblock nowrap"><!-- Side-by-Side -->
          <div class="inlineblock nowrap"><!-- Vertical -->
            <div class="viewdata">#Variables.IMUserPhnCntryCd#</div><br/>
            <span class="viewlabel">(Country)</span>
          </div><!-- /Vertical -->
          &nbsp;
          <div class="inlineblock nowrap"><!-- Vertical -->
            <div class="viewdata">#Variables.IMUserPhnAreaCd#</div><br/>
            <span class="viewlabel"><b>#Variables.AreaCdLabelContent#</b><br/>&nbsp;</span>
          </div><!-- /Vertical -->
          &nbsp;
          <div class="inlineblock nowrap"><!-- Vertical -->
            <div class="viewdata">#Variables.IMUserPhnLclNmb#</div>
			<div class="viewdata">#Variables.IMUserPhnLclNmb#</div><br/>
            <span class="viewlabel">(Phone Number)<br/>&nbsp;</span>
          </div><!-- /Vertical -->
          &nbsp;
          <div class="inlineblock nowrap"><!-- Vertical -->
            <div class="viewdata">#trim(Variables.IMUserPhnExtnNmb)#</div><br/>
            <span class="viewlabel">(Extension)<br/>&nbsp;</span>
          </div><!-- /Vertical -->
        </div><!-- /Side-by-Side -->
      </div><!-- /formdata -->
    </div><!-- tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">E-Mail Address:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserEmailAdrTxt#
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</cfoutput>

<cfelse><!--- Else condition of Variables.ReadOnlyProfile, hence, the following are when there's an updatable form: --->

	<!---
	Normally JavaScripts are defined in the <head>, where they're guaranteed never to be displayed. Since this include is 
	intended to be included in the <body>, take double precautions to prevent inferior browsers from displaying the 

JavaScript. 
	Usually the HTML comment hack is sufficient, but just to be extra sure, enclose the whole thing in a hidden div: 
	--->
	<cfoutput>
  <div style="display:none;"><!-- Because this is in the body, make extra sure that JS inside script tags is not displayed. 

-->
  <script></cfoutput>
	<cfif (NOT Variables.ReadOnlyIMCntryCd) AND (NOT Variables.ReadOnlyIMUserPhnCntryCd)>
		<cfset Variables.CntryToDialQueryName			= "Variables.ScqActvCountries">
		<cfinclude template="#Variables.LibJSURL#/dsp_IMCntryCdToIMCntryDialCd.cfm">
	</cfif>
	<cfoutput>
  var gDivAreaCdData                                    = null;
  var gDivAreaCdLabel                                   = null;
  var gDivCtyNmData                                     = null;
  var gDivCtyNmLabel                                    = null;
  var gDivDomAddr                                       = null;
  var gDivDomStrComm                                    = null;
  var gDivForAddr                                       = null;
  var gDivStCdData                                      = null;
  var gDivStCdLabel                                     = null;
  var gDivZipDropdownRow                                = null;
  var gEltIMCntryCd                                     = null;
  var gEltIMUserPhnCntryCd                              = null;
  var gEltStCd                                          = null;
  var gEltZipDropdown                                   = null;
  var gFlagAddrIsDomFor                                 = '<cfif Variables.IMCntryCd is "US">Dom<cfelse>For</cfif>';
  var gSpanUSPhoneFormat                                = null;
  $(document).ready(function()
      {// The following are used by various scripts. Get them only once and cache to improve performance. 
      gDivAreaCdData                                    = document.getElementById('DivAreaCdData');
      gDivAreaCdLabel                                   = document.getElementById('DivAreaCdLabel');
      gDivCtyNmData                                     = document.getElementById('DivCtyNmData');
      gDivCtyNmLabel                                    = document.getElementById('DivCtyNmLabel');
      gDivDomAddr                                       = document.getElementById('DivDomAddr');
      gDivDomStrComm                                    = document.getElementById('DivDomStrComm');
      gDivForAddr                                       = document.getElementById('DivForAddr');
      gDivStCdData                                      = document.getElementById('DivStCdData');
      gDivStCdLabel                                     = document.getElementById('DivStCdLabel');
      gDivZipDropdownRow                                = document.getElementById('DivZipDropdownRow');
      gEltIMCntryCd                                     = document.getElementById('IMCntryCd');
      gEltIMUserPhnCntryCd                              = document.getElementById('IMUserPhnCntryCd');
      gEltStCd                                          = document.getElementById('StCd');
      gEltZipDropdown                                   = document.getElementById('ZipDropdown');
      gSpanUSPhoneFormat                                = document.getElementById('USPhoneFormat');
      // For accessibility, we must avoid style="display:none;" on updatable form elements, in case JavaScript is off. 
      // So instead, we hide using JavaScript. <!--- Don't show unless "none". ---><cfif Variables.DomAddrDisplay is "none">
      gDivDomAddr       .style.display                  = 'none';</cfif>
      gDivDomStrComm    .style.display                  = 'none'; // Only shown after Lookup Zip.
      gDivZipDropdownRow.style.display                  = 'none'; // Only shown after Lookup Zip.<cfif 

Variables.ForAddrDisplay is "none">
      gDivForAddr       .style.display                  = 'none';</cfif><cfif Variables.StCdDataDisplay is "none">
      gDivStCdData      .style.display                  = 'none';</cfif><cfif Variables.StCdLabelDisplay is "none">
      gDivStCdLabel     .style.display                  = 'none';</cfif>
      });
  function ShowHideDomFor()
      {
      gEltIMCntryCd       .onchange();
      gEltIMUserPhnCntryCd.onchange();
      }
  function ShowHideDropdown()
      {
      if  (gEltZipDropdown.options.length > 2)          // That is, if LookupZipToDropdown returned multiple values. 
          {
          gEltStCd.value                                = '';
          gDivZipDropdownRow.style.display              = '';
          }
      else// Otherwise, there LookupZipToDropdown already set CtnNm, StCd, etc, so don't bother the user with the dropdown. 
          gDivZipDropdownRow.style.display              = 'none';
      ShowHideStCd();
      }
  // ShowHideStCd is also called by the country dropdown onchange script. That's why it's a separate function. 
  function ShowHideStCd()
      {
      var sVal                                          = gEltStCd.value;
      if  (sVal.length == 2)
          {
          gDivStCdData.innerHTML                        = sVal;
          gDivStCdData.style.display                    = gDivStCdLabel.style.display = '';
          }
      else
          {
          gDivStCdData.innerHTML                        = '';
          gDivStCdData.style.display                    = gDivStCdLabel.style.display = 'none';
          }
      }
  </script>
  </div><!-- /style="display:none;" --></cfoutput>

	<cfif Variables.ReadOnlyIMUserEmailAdrTxt>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">E-Mail Address:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserEmailAdrTxt#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserEmailAdrTxt" value="#Variables.IMUserEmailAdrTxt#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserEmailAdrTxt">E-Mail Address:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="IMUserEmailAdrTxt" id="IMUserEmailAdrTxt" value="#Variables.IMUserEmailAdrTxt#"
        size="60" maxlength="255" onChange="
        if  (EditMask('E-Mail Address', this.value, 'E', 1, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserEmailAdrTxt2">Re-enter E-Mail:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="IMUserEmailAdrTxt2" id="IMUserEmailAdrTxt2" value="#Variables.IMUserEmailAdrTxt2#"
        size="60" maxlength="255" onChange="
        if  (EditMask('Re-enter E-Mail', this.value, 'E', 1, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif>
	<cfif Variables.ReadOnlyIMCntryCd>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Country Code:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMCntryCd#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMCntryCd" value="#Variables.IMCntryCd#">
        </div><!-- /viewdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse>
		<cfoutput>
  
  
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label class="mandlabel" for="IMCntryCd">Country:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
		 
	 <input type="Hidden" name="countryTxt" value="">
   
        <select name="IMCntryCd" id="IMCntryCd"  
		onChange="if  (this.selectedIndex > 0)
            {
			var	sOpt                                    = this.options[this.selectedIndex];
            var	sVal                                    = sOpt.value;
            this.form.elements['countryTxt'].value = sVal;
			if  ((sVal == 'US') || (sVal == 'CA') || (sVal == 'GQ')|| (sVal == 'VQ') || (sVal == 'CJ') ||
			     (sVal == 'AV') || (sVal == 'AC') || (sVal == 'BF')|| (sVal == 'BB') || (sVal == 'BD') ||
				 (sVal == 'DO') || (sVal == 'DR') || (sVal == 'GJ')|| (sVal == 'JM') || (sVal == 'MH') ||
				 (sVal == 'ST') || (sVal == 'VC') || (sVal == 'TD')|| (sVal == 'TK') || (sVal == 'CQ') ||
				 (sVal == 'RM') || (sVal == 'RQ'))                				
				{
                $(gDivCtyNmData).removeClass('optdata').addClass('manddata');// Don't trash inlineblock. 
                gDivCtyNmLabel.className                = 'mandlabel';
                gDivDomAddr.style.display               = '';
                gDivForAddr.style.display               = 'none';
                gFlagAddrIsDomFor                       = 'Dom';	
                document.getElementById('showhide').setAttribute('style', 'color:' + 'black'  + ';');
 			    document.getElementById('showhideformat1').setAttribute('style', 'color:' + 'black'  + ';');
 				var NAME = document.getElementById('DivAreaCdData');
                NAME.className='manddata';
			    var PhoneNumberText = '(Prefix)  &nbsp;&nbsp;&nbsp; (Number)';
                document.getElementById('showhideformat').innerHTML = PhoneNumberText; 
				areacodelengthfield = document.getElementById('IMUserPhnAreaCd');
				areacodelengthfield.maxLength = 3;
 				var prefixclassname = document.getElementById('prefixdiv');
                prefixclassname.className='manddata';
				if  ((sVal != 'US') && (sVal != 'GQ') && (sVal != 'VQ') && (sVal != 'RM') && 
				(sVal != 'RQ') && (sVal != 'CQ'))
				{
				gDivCtyNmLabel.className                = 'optlabel';
                gDivDomAddr.style.display               = 'none';
                gDivForAddr.style.display               = '';
                gEltStCd.value                          = '';     // Tell ShowHideStCd to hide. 
                gFlagAddrIsDomFor                       = 'For';
				}
				}
            else
                {
		$(gDivCtyNmData).removeClass('manddata').addClass('optdata');// Don't trash inlineblock. 
                gDivCtyNmLabel.className                = 'optlabel';
                gDivDomAddr.style.display               = 'none';
                gDivForAddr.style.display               = '';
                gEltStCd.value                          = '';     // Tell ShowHideStCd to hide. 
                gFlagAddrIsDomFor                       = 'For';
                document.getElementById('showhide').setAttribute('style', 'color:' + 'white'  + ';');
                document.getElementById('showhideformat1').setAttribute('style', 'color:' + 'white'  + ';');
 				var NAME = document.getElementById('DivAreaCdData');
                NAME.className='optdata';
			    var PhoneNumberText = '&nbsp; (Phone Number)';
                document.getElementById('showhideformat').innerHTML = PhoneNumberText; 
     			areacodelengthfield = document.getElementById('IMUserPhnAreaCd');
				areacodelengthfield.maxLength = 5;
 				var prefixclassname = document.getElementById('prefixdiv');
                prefixclassname.className='optdata';
				}            
			ShowHideStCd();
			gDivDomStrComm.style.display                = 'none'; // Always hide comment, then show after lookup. 
			</cfoutput>		    
     	
		<cfif NOT Variables.ReadOnlyIMUserPhnCntryCd>
			<!--- Currently all IMUserPhn fields update together, so no need to test 

readonly 

for Area, Lcl, Extn. --->
			<cfoutput>
			if ((this.form.IMUserPhnAreaCd.value == '1')   // Don't overlay phone country code if area/city code, 
            &&	(this.form.IMUserPhnLclNmb.value == '')   // phone number 
            &&	(this.form.IMUserPhnExtnNmb.value == '')) // or extension was already entered by user. 
                {								
				this.form.IMUserPhnCntryCd.onchange();
				this.form.IMUserPhnCntryCd.value        = IMCntryCdToIMCntryDialCd(sVal);
				this.form.clearfields.onchange();
                }
				else
				{
				this.form.IMUserPhnCntryCd.value        = IMCntryCdToIMCntryDialCd(sVal);
				this.form.clearfields.onchange();
				ctry = this.form.countryTxt.value;
				  if ((sVal == 'US') || (sVal == 'CA') || (sVal == 'GQ')|| (sVal == 'VQ') || (sVal == 'CJ')  ||
			     (sVal == 'AV') || (sVal == 'AC') || (sVal == 'BF')|| (sVal == 'BB') || (sVal == 'BD') ||
				 (sVal == 'DO') || (sVal == 'DR') || (sVal == 'GJ')|| (sVal == 'JM') || (sVal == 'MH') ||
				 (sVal == 'ST') || (sVal == 'VC') || (sVal == 'TD')|| (sVal == 'TK') || (sVal == 'CQ') ||
				 (sVal == 'RM') ||(sVal == 'RQ')) 
				  {
				   PrefixNumbObject = this.form.IMUserPhnPrefixNmb.value;
    	           LclNumbObject = this.form.IMUserPhnLclLineNumberNmb.value;
                   this.form.elements['IMUserPhnLclNmb'].value = PrefixNumbObject + LclNumbObject;
				  }
				}
				</cfoutput>
		</cfif>
		<cfoutput>	
        return true;
            }
        alert('Mandatory field. You must choose a country.'); 
        return false;
		">
		
		</cfoutput>
		<cfset DspOptsQueryName							= "Variables.ScqActvCountries">
		<cfset DspOptsSelList							= Variables.IMCntryCd>
		<cfset DspOptsTabs								= "#Request.SlafEOL#        

">
		<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
		<cfoutput>
        </select>
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif><!--- /ReadOnlyIMUserLast4DgtSSNNmb --->

	<cfif Variables.ReadOnlyIMUserZipCd>
		<cfoutput>
  
  <div id="DivDomAddr" class="tbl"><!-- ********* Domestic address only, begins. ********* -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Zip+4:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata nowrap">
        #Variables.Zip5Cd#<cfif Len(Variables.Zip4Cd) gt 0> - #Variables.Zip4Cd#</cfif>
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="Zip5Cd" value="#Variables.Zip5Cd#">
        <input type="Hidden" name="Zip4Cd" value="#Variables.Zip4Cd#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --><!-- ********* Domestic address only, ends. ********* --></cfoutput>

	<cfelse>
		<cfoutput>		
  <div id="DivDomAddr" class="tbl"><!-- ********* Domestic address only, begins. ********* -->
  	<div class="tbl_row">	
	  	<div class="tbl_cell formlabel"></div>
      	<div class="tbl_cell formdata nowrap">	
        	<div class="optdata">
        		(Enter Zip Code, and then click the Lookup Zip button to populate City and State)	
        	</div><!-- /optdata -->   
      	</div><!-- /formdata -->
   	</div><!-- /tbl_row -->	
    <div class="tbl_row">
      <div class="tbl_cell formlabel">
        <label class="mandlabel" for="Zip5Cd">Zip</label><label class="optlabel" for="Zip4Cd">+4:</label>
      </div>
      <div class="tbl_cell formdata nowrap">
        <div class="manddata">
        <input type="Text" name="Zip5Cd" id="Zip5Cd" value="#Variables.Zip5Cd#" 
        size="5" maxlength="5" onChange="
        if  (EditMask('Zip Code', this.value, '99999', 1, 5, 5))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
        <div class="optdata">
        <input type="Text" name="Zip4Cd" id="Zip4Cd" value="#Variables.Zip4Cd#" 
        size="4" maxlength="4" onBlur="
        if  (EditMask('Zip Code', this.value, '9999', 0, 4, 4))
            {
            LookupZipToDropdown
                (
                this.form.Zip5Cd.value + '-' + this.form.Zip4Cd.value,
                gEltZipDropdown,
                gEltStCd,
                null,
                this.form.IMUserCtyNm,
                this.form.IMUserStr1Txt,
                null,
                null,
                null,
                ShowHideDropdown // Also calls ShowHideStCd. 
                );
            gDivDomStrComm.style.display                = '';
            return true;
            }
        this.focus();
        return false;
        ">
        </div><!-- /optdata -->
        <input type="Button" value="Lookup Zip" onClick="
        gSilentEditMask = 1;
        //  gLookupZipToDropdownTrace				= true;
        if	(EditMask('', this.form.Zip5Cd.value, '9', 1, 5, 5))
            {
            gSilentEditMask                         = 1;
            if  (EditMask('', this.form.Zip4Cd.value, '9', 1, 4, 4))
                LookupZipToDropdown
                    (
                    this.form.Zip5Cd.value + '-' + this.form.Zip4Cd.value,
                    gEltZipDropdown,
                    gEltStCd,
                    null,
                    this.form.IMUserCtyNm,
                    this.form.IMUserStr1Txt,
                    null,

                    null,
                    null,
                    ShowHideDropdown // Also calls ShowHideStCd. 
                    );
            else
                LookupZipToDropdown
                    (
                    this.form.Zip5Cd.value,
                    gEltZipDropdown,
                    gEltStCd,
                    null,
                    this.form.IMUserCtyNm,
                    this.form.IMUserStr1Txt,
                    null,
                    null,
                    null,
                    ShowHideDropdown // Also calls ShowHideStCd. 
                    );
            gDivDomStrComm.style.display                = '';
            }
        else
            alert ('You cannot look up the county and city codes until you enter a 5-digit Zip code.');
        ">
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row" id="DivZipDropdownRow">
      <div class="tbl_cell formlabel"><label class="mandlabel" for="ZipDropdown">Choose Result:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <select name="ZipDropdown" id="ZipDropdown" onChange="
        if  (this.selectedIndex >= 0)
            {
            var sOpt                                    = this.options[this.selectedIndex];
            // Relay the result of an assignment to the second argument of the function, like a = b = c: 
            SetFormEltValue(this.form.StCd,             (gDivStCdData.innerHTML = sOpt.StCd));
            SetFormEltValue(this.form.IMUserCtyNm,      sOpt.CtyNm);
            if  ((sOpt.StrNm.length + sOpt.StrSfx.length) == 0)
                SetFormEltValue(this.form.IMUserStr1Txt,'');
            else
                SetFormEltValue(this.form.IMUserStr1Txt,sOpt.StrNm + ' ' + sOpt.StrSfx);
            }
        ">
        <option value="">Enter a Zip, press Lookup Zip button and choose result here.</option>
        </select>
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /DivZipDropdownRow -->
  </div><!-- /tbl --><!-- ********* Domestic address only, ends. ********* --></cfoutput>
	</cfif>

<cfoutput>
  <div id="DivForAddr" class="tbl">
 </cfoutput>

	<cfif Variables.ReadOnlyIMUserPostCd>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Postal Code:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserPostCd#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserPostCd" value="#Variables.IMUserPostCd#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	<cfelse>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserPostCd">Postal Code::</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="IMUserPostCd" id="IMUserPostCd" value="#Variables.IMUserPostCd#"
        size="20" maxlength="20" onChange="
        if  (EditMask('Postal Code', this.value, 'X', 1, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>

	<cfif Variables.ReadOnlyIMUserStNm>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">State/Province:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStNm#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserStNm" value="#Variables.IMUserStNm#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	<cfelse>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserStNm">State/Province:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="IMUserStNm" id="IMUserStNm" value="#Variables.IMUserStNm#"
        size="60" maxlength="60" onChange="
        if  (EditMask('State/Province', this.value, 'X', 0, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>

	<cfoutput>
  </div><!-- /tbl --><!-- ********* Foreign address only, ends. ********* --></cfoutput>

	<cfif Variables.ReadOnlyIMUserStr1Txt>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Street 1:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStr1Txt#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserStr1Txt" value="#Variables.IMUserStr1Txt#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel"><label for="IMUserStr1Txt">Street 1:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="IMUserStr1Txt" id="IMUserStr1Txt" value="#Variables.IMUserStr1Txt#"
        size="60" maxlength="80" onChange="
        if  (EditMask('Street 1', this.value, 'X', 1, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row" id="DivDomStrComm">
     <div class="tbl_cell formlabel optlabel"></div>
<!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif>

	<cfif Variables.ReadOnlyIMUserStr2Txt>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Street 2:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserStr2Txt#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserStr2Txt" value="#Variables.IMUserStr2Txt#">
        </div>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel"><label for="IMUserStr2Txt">Street 2:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="IMUserStr2Txt" id="IMUserStr2Txt" value="#Variables.IMUserStr2Txt#"
        size="60" maxlength="80" onChange="
        if  (EditMask('Street 2', this.value, 'X', 0, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>

	</cfif>

	<cfif Variables.ReadOnlyIMUserCtyNm>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">City:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserCtyNm#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserCtyNm" value="#Variables.IMUserCtyNm#">
        </div><cfif Len(Variables.StCd) gt 0><!--- Should only happen with a domestic address, so probably safe to do: --->
        &nbsp;
        <div class="viewdata">#Variables.StCd#</div></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->

  </div><!-- /tbl --></cfoutput>
	<cfelse>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel">
        <label for="IMUserCtyNm" id="DivCtyNmLabel" class="#Variables.CtyNmLabelMandOpt#">City</label><span id="DivStCdLabel" 

class="viewlabel">/State</span>:
      </div>
      <div class="tbl_cell formdata nowrap">
        <div id="DivCtyNmData" class="inlineblock #Variables.CtyNmDataMandOpt#">
        <input type="Text" name="IMUserCtyNm" id="IMUserCtyNm" value="#Variables.IMUserCtyNm#"
        size="60" maxlength="60" onChange="
        var sThisIsADomAddr                             = (gDivDomAddr.style.display == '');
        var sMand                                       = (sThisIsADomAddr ? 1 : 0);
        if  (EditMask('City', this.value, 'X', sMand, 1, this.maxLength))
            {
            if ((sThisIsADomAddr)
            &&  (this.form.ZipDropdown.notFoundIndex))  // Existence test, indicating that at least one lookup was done. 
                this.form.ZipDropdown.selectedIndex     = this.form.ZipDropdown.notFoundIndex;
            return true;
            }
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.CtyNmDataMandOpt# -->
        &nbsp;
        <div class="viewdata" id="DivStCdData">#Variables.StCd#</div>
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="StCd" id="StCd" value="#Variables.StCd#">
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif>

	<cfoutput>
  
  
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel <cfif Variables.ReadOnlyProfile>view<cfelse>mand</cfif>label">Phone Number:</div>
      <div class="tbl_cell formdata nowrap"><!-- Side-by-Side -->
        <div class="inlineblock"><!-- Vertical --><cfif Variables.ReadOnlyIMUserPhnCntryCd>
          <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
          <input type="Hidden" name="IMUserPhnCntryCd" value="#Variables.IMUserPhnCntryCd#">
          <div class="viewdata">
          #Variables.IMUserPhnCntryCd#
          </div><cfelse>
		  <div class="manddata">
          
		  <input type="Text" name="IMUserPhnCntryCd" id="IMUserPhnCntryCd" value="#Variables.IMUserPhnCntryCd#"
          size="6" maxlength="6"  readonly="readonly" onChange="
               if  ((this.value == '1') || (this.value == ''))
                  {
                  $(gDivAreaCdData).removeClass('optdata').addClass('manddata');// Don't trash tbl_cell, etc. 
                  gDivAreaCdLabel.className       = 'mandlabel';
                  gDivAreaCdLabel.innerHTML       = '#Variables.AreaCdLabelContentDom#';
                  gSpanUSPhoneFormat.innerHTML    = '(999-9999)';
                  }
              else
                  {
                  $(gDivAreaCdData).removeClass('manddata').addClass('optdata');// Don't trash tbl_cell, etc. 
                  gDivAreaCdLabel.className       = 'optlabel';
                  gDivAreaCdLabel.innerHTML       = '#Variables.AreaCdLabelContentFor#';
                  gSpanUSPhoneFormat.innerHTML    = '&nbsp;';
                  }
              return true;
              }
          this.focus();
          return false;
          ">
          </div></cfif><br/>
          <label for="IMUserPhnCntryCd" class="mandlabel">(Country)</label><br/>
          <span id="UScountrycode">&nbsp;</span>
        </div><!-- /Vertical -->
        &nbsp;
        <div class="inlineblock"><!-- Vertical --><cfif Variables.ReadOnlyIMUserPhnAreaCd>
          <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
          <input type="Hidden" name="IMUserPhnAreaCd" value="#Variables.IMUserPhnAreaCd#">
          <div class="viewdata">
          #Variables.IMUserPhnAreaCd#
          </div><cfelse>
          <div id="DivAreaCdData">
          <input type="Text" name="IMUserPhnAreaCd" id="IMUserPhnAreaCd" value="#Variables.IMUserPhnAreaCd#"
          size="3" 
		  onkeyup="
				  thisnumber = this.form.IMUserPhnAreaCd;
				  validnumber(thisnumber)"
		  onChange="
          var sEditMaskResult;
          var sIMUserPhnCntryCd                   = this.form.IMUserPhnCntryCd.value;
          if  ((sIMUserPhnCntryCd == '1') || (sIMUserPhnCntryCd == ''))
              sEditMaskResult                     = EditMask('Area Code', this.value, '9', 1, 3, 3);
          else
              sEditMaskResult                     = EditMask('City Code', this.value, '9', 0, 1, this.maxLength);
          if  (sEditMaskResult)
              return true;
          this.focus();
          return false;
          ">	  
		  
          </div></cfif><br/>         		  <label for="IMUserPhnAreaCd"  id="DivAreaCdLabel"><b>Area 

Code</b></label><br/> 


         
		  &nbsp;
        </div><!-- /Vertical -->
        &nbsp;
		<cfset IMUserPhnLclPrefixNmb = Left(Variables.IMUserPhnLclNmb,3)>
		<cfset IMUserPhnLclLineNumberNmb = right(Variables.IMUserPhnLclNmb,4)>


        <div class="inlineblock"><!-- Vertical --><cfif Variables.ReadOnlyIMUserPhnLclNmb>
          <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->

          <div class="viewdata">
          #Variables.IMUserPhnLclNmb#
          </div><cfelse>

	   
<!--- Jim Added script to capture prefix and local phone number information and populate hidden 
form field
--->	 

<!--- T
Jim D. 5/20/2014
    The purpose of the code below is to enable switching of phone number fields based on
	what the country code is. If the country code is US (1) show the two phone number 
	fields prefix and local. If the country code is NOT US show a single phone number field
	along with the area code field.
	
	In addition, there was a need to wipe out the phone number entered if the phone 
	country code changes. This could easily have been done using the onchange event of the
	IMCntryCd form field, to complicate things he on cnhange event of the IMCntryCd form field is called after 
	the SUBMIT button is clicked from another action page. Script to differentiat beweeen the 
	onchange event being clicked or called from another ColdFusion page has been written.
--->
          
		   <input type="Hidden" name="clearfields" value="" onchange="		   
           ctry = this.form.countryTxt.value;
		   
		   newctryObject = this.form.IMUserPhnCntryCd.value;		
			if (oldctryObject != newctryObject)
			{
			this.form.elements['IMUserPhnLclNmb'].value = '';
		    this.form.elements['IMUserPhnPrefixNmb'].value = '';
		    this.form.elements['IMUserPhnLclLineNumberNmb'].value = '';
		    this.form.elements['IMUserPhnAreaCd'].value = '';
			this.form.elements['IMUserPhnExtnNmb'].value = '';			
		    }
			if ((ctry == 'US') || (ctry == 'CA') || (ctry == 'GQ')|| (ctry == 'VQ') || (ctry == 'CJ') ||
			     (ctry == 'AV') || (ctry == 'AC') || (ctry == 'BF')|| (ctry == 'BB') || (ctry == 'BD') ||
				 (ctry == 'DO') || (ctry == 'DR') || (ctry == 'GJ')|| (ctry == 'JM') || (ctry == 'MH') ||
				 (ctry == 'ST') || (ctry == 'VC') || (ctry == 'TD')|| (ctry == 'TK') || (ctry == 'CQ') ||
				 (ctry == 'RM') || (ctry == 'RQ')  && (oldctryObject != newctryObject))	    
			 {
			  this.form.IMUserPhnLclNmb.visible = 'false';
			  document.getElementById('IMUserPhnLclNmb').style.display = 'none';
			  this.form.IMUserPhnPrefixNmb.visible = 'true';
			  document.getElementById('IMUserPhnPrefixNmb').style.display = 'block';
			  this.form.IMUserPhnLclLineNumberNmb.visible = 'true';
			  document.getElementById('IMUserPhnLclLineNumberNmb').style.display = 'block';
			  document.getElementById('IMUserPhnAreaCd').focus();
              oldctryObject = this.form.IMUserPhnCntryCd.value;
			 }	 
		  if ((ctry != 'US') && (ctry != 'CA') && (ctry != 'GQ')&& (ctry != 'VQ') && (ctry != 'CJ') &&
			     (ctry != 'AV') && (ctry != 'AC') && (ctry != 'BF')&& (ctry != 'BB') && (ctry != 'BD') &&
				 (ctry != 'DO') && (ctry != 'DR') && (ctry != 'GJ')&& (ctry != 'JM') && (ctry != 'MH') &&
				 (ctry != 'ST') && (ctry != 'VC') && (ctry != 'TD')&& (ctry != 'TK') && (ctry != 'CQ') &&
				 (ctry != 'RM') && (ctry != 'RQ')   && (oldctryObject != newctryObject))		  
		   {		 
			 setTimeout(function() { document.getElementById('IMUserPhnLclNmb').style.display = 'block'; }, 10);
             setTimeout(function() { document.getElementById('IMUserPhnPrefixNmb').style.display = 'none'; }, 10);  
			 setTimeout(function() { document.getElementById('IMUserPhnLclLineNumberNmb').style.display = 'none'; 

}, 10);
		     document.getElementById('IMUserPhnAreaCd').focus();
    		 oldctryObject = this.form.IMUserPhnCntryCd.value;
		   }
		   		   
		   ">

          <div id="prefixdiv">
       <label> 
	      	      <input type="text" name="IMUserPhnPrefixNmb" id="IMUserPhnPrefixNmb" value="#IMUserPhnLclPrefixNmb#"    

      size="3" maxlength="3"  
				  onkeyup="
				  thisnumber = this.form.IMUserPhnPrefixNmb;
				  validnumber(thisnumber)"
		   onChange="  		     
		  ctry = this.form.countryTxt.value;
          sEditMaskResult                     = EditMask('Phone Number Prefix', this.value, '9', 1, 3, 3);
          PrefixNumbObject = this.form.IMUserPhnPrefixNmb.value;
    	  LclNumbObject = this.form.IMUserPhnLclLineNumberNmb.value;
          this.form.elements['IMUserPhnLclNmb'].value = PrefixNumbObject + LclNumbObject;
          var sEditMaskResult;
          if  (sEditMaskResult)
           	  this.focus();
		      return true;
          return false;
          " 
		  >
		  </div>
</label>
	  
<span id="showhide">-</span>
			<div class="manddata">
           <label>
<input type="Text" name="IMUserPhnLclNmb" id="IMUserPhnLclNmb" value="#Variables.IMUserPhnLclNmb#"
          size="8" maxlength="15" 
		  onkeyup="
				  thisnumber = this.form.IMUserPhnLclNmb;
				  validnumber(thisnumber)"
		  onChange="
          var sEditMaskResult;
          var sIMUserPhnCntryCd                   = this.form.IMUserPhnCntryCd.value;
          if  (sIMUserPhnCntryCd != '1') 
              sEditMaskResult                     = EditMask('Phone Number', this.value, 'X', 1, 1, this.maxLength);
          if  (sEditMaskResult)
              return true;
          this.focus();
          return false;
          ">
<!--- Jim Added script to capture prefix and local phone number information and populate hidden 
      form field
--->	  
		  <input type="text"  align="middle" name="IMUserPhnLclLineNumberNmb" id="IMUserPhnLclLineNumberNmb" 

value="#IMUserPhnLclLineNumberNmb#"
          size="4" maxlength="4"   o
		  onkeyup="
				  thisnumber = this.form.IMUserPhnLclLineNumberNmb;
				  validnumber(thisnumber)"
		  nChange="
		  PrefixNumbObject = this.form.IMUserPhnPrefixNmb.value;
		  LclNumbObject = this.form.IMUserPhnLclLineNumberNmb.value;
		  this.form.elements['IMUserPhnLclNmb'].value = PrefixNumbObject + LclNumbObject;
          var sEditMaskResult;
          var sIMUserPhnCntryCd                   = this.form.IMUserPhnCntryCd.value;
          if  ((sIMUserPhnCntryCd == '1') || (sIMUserPhnCntryCd == ''))
              sEditMaskResult                     = EditMask('Phone Number Local', this.value, '999', 1, 4, 4);
          else
              sEditMaskResult                     = EditMask('Phone Number Local', this.value, 'X', 1, 1, this.maxLength);
          if  (sEditMaskResult)
              return true;
			  this.focus();
          return false;
          ">
          </div>
		  </label></cfif><br/>
		  
<!--- JD - 5/22/2014
      
	  Initiallize variables used to hold previous and current country code information 
      if the IMCntryCd.onchanged event has been triggered by an actual change in
	  the country code these will NOT be equal to each other. On the other hand if
	  these two are equeal, the IMCntryCd.onchanged event was triggered by a subsequent
	  coldfusion file included after the /security/user/act_user.cfm file which is executed upon
	  the dsp_user.cfm submit. Also this will decide what to display as far as the phone number
	  field goes. --->
	  


  
  	       <span id="USPhoneFormat"></span>

        <label for="IMUserPhnLclNmb" id="showhideformat" class="mandlabel"></label><br/> 
		  <span id="showhideformat1" >&nbsp;&nbsp;(999)&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;(9999)</span>


       </div><!-- /Vertical -->
 <script type="text/javascript"> 
function validnumber(f) 
	{	
		!(/^[��0-9-\s]*$/i).test(f.value)?f.value = f.value.replace(/[^��0-9-\s]/ig,''):null;		
		f.value=(f.value.replace(/`/g,''));
		f.value=(f.value.replace(/_/g,''));
		f.value=(f.value.replace(/-/g,''));			
		f.value=(f.value.replace(/\s\s/g,' '));
		f.value=(f.value.replace(/[\[\]']+/g,''));		
		f.value=(f.value.replace(/\\+/g,''));	
		f.value=(f.value.replace(/�/g,''));		
		f.value =(f.value.replace(/\^/g, "\x005E")); 
	} 
 </script>
		<cfif Variables.IMCntryCd EQ 'US' OR Variables.IMCntryCd EQ 'CA' OR
		 Variables.IMCntryCd EQ 'GQ' OR Variables.IMCntryCd EQ 'VQ' OR Variables.IMCntryCd EQ 'CJ' OR
		 Variables.IMCntryCd EQ 'AV' OR Variables.IMCntryCd EQ 'AC' OR Variables.IMCntryCd EQ 'BF' OR
		 Variables.IMCntryCd EQ 'BB' OR Variables.IMCntryCd EQ 'BD' OR Variables.IMCntryCd EQ 'DO' OR
		 Variables.IMCntryCd EQ 'DR' OR Variables.IMCntryCd EQ 'GJ' OR Variables.IMCntryCd EQ 'JM' OR
		 Variables.IMCntryCd EQ 'MH' OR Variables.IMCntryCd EQ 'ST' OR Variables.IMCntryCd EQ 'VC' OR
		 Variables.IMCntryCd EQ 'TD' OR Variables.IMCntryCd EQ 'TK' OR Variables.IMCntryCd EQ 'CQ' OR
	     Variables.IMCntryCd EQ 'RM' OR Variables.IMCntryCd EQ 'RQ'>
	     
		  <script type="text/javascript">  
		     {
               <cfoutput>
			     var #toScript(Variables.IMUserPhnCntryCd, "oldctryObject")#;
                 var #toScript(Variables.IMUserPhnCntryCd, "newctryObject")#;
			   </cfoutput>
			   document.getElementById('IMUserPhnLclNmb').style.display = 'none';			   
			   document.getElementById('showhide').setAttribute('style', 'color:' + 'black'  + ';');		
			   document.getElementById('showhideformat1').setAttribute('style', 'color:' + 'black'  + ';');
			   var NAME = document.getElementById("DivAreaCdData");
               NAME.className="manddata";
			   var PhoneNumberText = "(Prefix)  &nbsp;&nbsp;&nbsp; (Number)";
               document.getElementById("showhideformat").innerHTML = PhoneNumberText; 
      		   areacodelengthfield = document.getElementById('IMUserPhnAreaCd');
			   areacodelengthfield.maxLength = 3;
   			   var prefixclassname = document.getElementById('prefixdiv');
               prefixclassname.className='manddata';
			 }
			  </script> 
<cfelse>
		  <script type="text/javascript"> 
		  {
  			  <cfoutput>
                 var #toScript(Variables.IMUserPhnCntryCd, "oldctryObject")#;
                 var #toScript(Variables.IMUserPhnCntryCd, "newctryObject")#;
              </cfoutput>
			  document.getElementById('IMUserPhnPrefixNmb').style.display = 'none';
			  document.getElementById('IMUserPhnLclLineNumberNmb').style.display = 'none';
			  document.getElementById('showhide').setAttribute('style', 'color:' + 'white'  + ';');
  			  document.getElementById('showhideformat1').setAttribute('style', 'color:' + 'white'  + ';')
			  var NAME = document.getElementById("DivAreaCdData");
              NAME.className="optdata";
			  var PhoneNumberText = "&nbsp; (Phone Number)";
              document.getElementById("showhideformat").innerHTML = PhoneNumberText; 
    		  areacodelengthfield = document.getElementById('IMUserPhnAreaCd');
			  areacodelengthfield.maxLength = 5;
 	  		  var prefixclassname = document.getElementById('prefixdiv');
              prefixclassname.className='optdata';
		  }
        </script> 
	   </cfif>	<!--- is the country code US --->
        &nbsp;
        <div class="inlineblock"><!-- Vertical -->
		    <cfif Variables.ReadOnlyIMUserPhnExtnNmb>
              <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
                <input type="Hidden" name="IMUserPhnExtnNmb" id="IMUserPhnExtnNmb" value="#Variables.IMUserPhnExtnNmb#">
                <div class="viewdata">
                  #Variables.IMUserPhnExtnNmb#
                </div>
			<cfelse>
                <div class="optdata">
                <input type="Text" name="IMUserPhnExtnNmb" id="IMUserPhnExtnNmb" value="#Variables.IMUserPhnExtnNmb#"
                 size="6" maxlength="6" 
				 onkeyup="
				  thisnumber = this.form.IMUserPhnExtnNmb;
				  validnumber(thisnumber)"
				 onChange="
                 if  (EditMask('Extension', this.value, 'X', 0, 1, this.maxLength))
                 return true;
                 this.focus();
                 return false;">
                 </div>
			</cfif>
			<br/>
          <label for="IMUserPhnExtnNmb" class="optlabel">(Extension)</label><br/>
          &nbsp;
        </div><!-- /Vertical -->
      </div><!-- /formdata = /Side-by-Side -->
    </div><!-- tbl_row -->
  </div><!-- /tbl --></cfoutput>



</cfif><!--- /Variables.ReadOnlyProfile --->






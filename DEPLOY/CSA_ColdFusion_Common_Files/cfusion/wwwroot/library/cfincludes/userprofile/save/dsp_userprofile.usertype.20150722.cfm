<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/22/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				See dsp_userprofile.template.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Original implementation. Append more stuff after this line. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>User Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserTypCd", "UserType", "User Type", "mand", "TypCd")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"></cfoutput>
		<cfif Variables.MandOpt is "view">
			<!---
			Query-of-Queries is the slowest thing that ColdFusion does. It essentially has to load an entire SQL parser, 
			parse the pseudo-SQL and emulate a DBMS. It could literally add seconds to use a Query-of-Queries for this. 
			This way, however, takes only milliseconds: 
			--->
			<cfset Variables.Description					= "(none)">
			<cfloop query="Variables.ActvIMUserTypTbl">
				<cfif Variables.ActvIMUserTypTbl.code		is Variables.VarValue>
					<cfset Variables.Description			= Variables.ActvIMUserTypTbl.description>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfoutput>
        #Variables.Description#</cfoutput>
		<cfelse>
			<cfoutput>
        <select #Variables.NameAndId# onchange="DoThisOnChangeUserType(this)"></cfoutput>
			<cfset Variables.DspOptsNotSelText				= "Not Yet Selected">
			<cfset Variables.DspOptsQueryName				= "Variables.ActvIMUserTypTbl">
			<cfset Variables.DspOptsSelList					= Variables.VarValue>
			<cfset Variables.DspOptsSkipList				= "F,L,P">
			<cfset Variables.DspOptsTabs					= "#Request.SlafEOL#        ">
			<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
			<cfoutput>
        </select></cfoutput>
		</cfif>
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /IMUserTypCd --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("JobTitlCd",	"JobTitle",	"Job Title",	"opt", "")>
	<cfset Variables.VarNameJobTitl							= Variables.VarName>	<!--- Needed later. --->
	<cfif Variables.MandOpt is "view">
		<cfset Variables.CountOfJobTitlesForType			= 0>
		<cfloop query="Variables.ActvIMJobTitlTypTbl">
			<cfif Variables.ActvIMJobTitlTypTbl.IMUserTypCd	is Variables.VarValueTypCd>
				<cfset Variables.CountOfJobTitlesForType	= Variables.CountOfJobTitlesForType + 1>
			</cfif>
		</cfloop>
		<cfif Variables.CountOfJobTitlesForType is 0>
			<!--- If MandOpt is "view", we don't even want to display the Job Title label if the user type doesn't have any. --->
			<cfset Variables.MandOpt						= "skip">
		</cfif>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data">
        <!-- Using style="display:none;" because jQuery 3.0+ .show()/.hide() will break on display:none in a class: --></cfoutput>
		<cfloop query="Variables.ActvIMJobTitlTypTbl">
			<cfif	(Variables.MandOpt						is "view")
				and	(ListFindNoCase(Variables.VarValue, Variables.ActvIMJobTitlTypTbl.code) is 0)>
				<cfcontinue>
			</cfif>
			<cfif Variables.MandOpt is "view">
				<cfoutput>
        <span class="JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
          #Variables.ActvIMJobTitlTypTbl.description#<br/>
        </span></cfoutput>
			<cfelse>
				<cfif ListFindNoCase(Variables.VarValue, Variables.ActvIMJobTitlTypTbl.code) gt 0>
					<cfset Variables.Checked				= "checked">
				<cfelse>
					<cfset Variables.Checked				= "       ">
				</cfif>
				<cfoutput>
        <label class="#Variables.MandOpt#label JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
          <input type="Checkbox" name="#Variables.VarName#" #Variables.Checked# value="#Variables.ActvIMJobTitlTypTbl.code#" onclick="
          if  (this.checked)
              $('.Job'+this.value).show();
          else
              $('.Job'+this.value).hide();
          ">
          #Variables.ActvIMJobTitlTypTbl.description#<br/>
        </label></cfoutput>
			</cfif>
		</cfloop>
		<cfoutput>
        <div class="viewdata JobTitle TypeNone" style="display:none;">
          (none<cfif Variables.MandOpt is not "view"> for this type</cfif>)
        </div></cfoutput>
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /JobTitlCd --->
	<cfoutput>
  </div><!-- /tbl-->
</fieldset><!-- /User Information --><br/><!-- Break because fieldset is class="inlineblock" -->

<div class="TypeSensitive TypeN" style="display:none;">

<fieldset class="inlineblock JobSensitive Job8" style="display:none;">
  <legend>Bond Borrower</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DUNS",	"DUNSNumber",	"DUNS Number", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
        <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data">
        <input type="Text" #Variables.NameIdAndValue# size="9" maxlength="9" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 9, 9))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("EIN1",	"EIN1",	"EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
			<cfset Defaults("TaxId7",	"TaxId7",	"First 2 digits of EIN",	"mand", "Tax7")>
			<cfset Defaults("TaxId8",	"TaxId8",	"Last 7 digits of EIN",		"mand", "Tax8")>
			<cfoutput><cfif Variables.MandOptTax7 is "view">
        #Variables.VarValueTax7#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax7#">#Variables.EngTax7#</label>
        <input type="password" #Variables.NameIdAndValueTax7# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax7)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax8 is "view">
        #Variables.VarValueTax8#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax8#">#Variables.EngTax8#</label>
        <input type="Text" #Variables.NameIdAndValueTax8# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax8)#s',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("EIN2",	"EIN2",	"Re-enter EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId9",	"TaxId9",	"Re-enter First 2 digits of EIN",	"mand", "Tax9")>
		<cfset Defaults("TaxIda",	"TaxIda",	"Re-enter Last 7 digits of EIN",	"mand", "Taxa")>
		<cfoutput><cfif Variables.MandOptTax9 is "view">
        #Variables.VarValueTax9#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax9#">#Variables.EngTax9#</label>
        <input type="password" #Variables.NameIdAndValueTax9# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax9)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTaxa is "view">
        #Variables.VarValueTaxa#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTaxa#">#Variables.EngTaxa#</label>
        <input type="Text" #Variables.NameIdAndValueTaxa# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTaxa)#',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN2 --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /JobSensitive Job8 (Bond Borrower) --><br class="inlineblock JobSensitive Job8" style="display:none;">

<fieldset class="inlineblock JobSensitive Job7" style="display:none;">
  <legend>Loan Borrower</legend>
  <div class="tbl"></cfoutput>
	<cfset Defaults("SSN1",	"SSN1",	"SSN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId1",	"TaxId1",	"First 3 digits of SSN",	"mand", "Tax1")>
		<cfset Defaults("TaxId2",	"TaxId2",	"Middle 2 digits of SSN",	"mand", "Tax2")>
		<cfset Defaults("TaxId3",	"TaxId3",	"Last 4 digits of SSN",		"mand", "Tax3")>
		<cfoutput><cfif Variables.MandOptTax1 is "view">
        #Variables.VarValueTax1#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax1#">#Variables.EngTax1#</label>
        <input type="password" #Variables.NameIdAndValueTax1# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax1)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax2 is "view">
        #Variables.VarValueTax2#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax2#">#Variables.EngTax2#</label>
        <input type="password" #Variables.NameIdAndValueTax2# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax2)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax3 is "view">
        #Variables.VarValueTax3#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax3#">#Variables.EngTax3#</label>
        <input type="Text" #Variables.NameIdAndValueTax3# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax3)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("SSN2",	"SSN2",	"Re-enter SSN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId4",	"TaxId4",	"Re-enter First 3 digits of SSN",	"mand", "Tax4")>
		<cfset Defaults("TaxId5",	"TaxId5",	"Re-enter Middle 2 digits of SSN",	"mand", "Tax5")>
		<cfset Defaults("TaxId6",	"TaxId6",	"Re-enter Last 4 digits of SSN",	"mand", "Tax6")>
		<cfoutput><cfif Variables.MandOptTax4 is "view">
        #Variables.VarValueTax4#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax4#">#Variables.EngTax4#</label>
        <input type="password" #Variables.NameIdAndValueTax4# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax4)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax5 is "view">
        #Variables.VarValueTax5#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax5#">#Variables.EngTax5#</label>
        <input type="password" #Variables.NameIdAndValueTax5# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax5)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax6 is "view">
        #Variables.VarValueTax6#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax6#">#Variables.EngTax6#</label>
        <input type="Text" #Variables.NameIdAndValueTax6# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax6)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN2 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("LoanNmb",	"LoanNumber",	"Loan Number", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue# size="11" maxlength="11" onchange="
        var sMand											= isMand(this);
        if  (EditLoanNmb('#JSStringFormat(Variables.Eng)#', this.value, sMand, true))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- JobSensitive Job7 (Loan Borrower) --><br class="inlineblock JobSensitive Job7" style="display:none;">

</div><!-- /TypeSensitive TypeN -->

<fieldset class="inlineblock TypeSensitive TypeC TypeS TypeU" style="display:none;">
  <legend>
    <span class="TypeSensitive TypeC" style="display:none;">SBA COR</span>
    <span class="TypeSensitive TypeS" style="display:none;">SBA Supervisor</span>
    <span class="TypeSensitive TypeU" style="display:none;">Lender's Authorizing Official</span>
  </legend></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Variables.db_before_supervisor_lookup			= Variables.db>
	<cfset Variables.UserIsRequestingOwnAccess				= (Variables.PageIsAddCustomer or Variables.PageIsProfile)>
	<cfinclude template="#Variables.LibIncURL#/userprofile/dsp_supervisor_lookup.cfm">
	<cfset Variables.db										= Variables.db_before_supervisor_lookup>
	<cfoutput>
</fieldset><!-- /Supervisor --><br class="inlineblock TypeSensitive TypeC TypeS TypeU" style="display:none;">
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#

<!-- User-Related: -->

<script>

$(function() // Shorthand for the document ready event. 
	{
	DoThisOnChangeUserType();
	});

gUserType													= "#Variables.VarValueTypCd#";

function DoThisOnChangeUserType								()
	{<cfif ListFindNoCase("skip,view", Variables.MandOptTypCd) is 0><!--- And therefore the dropdown for UserType exists. --->
	var	i;
	var	sForm												= document.#Variables.FormName#;
	var	sDropdownUserType									= sForm.#Variables.VarNameTypCd#;
	var	sOptions											= sDropdownUserType.options;
	var	sOptionsLen											= sOptions.length;
	var	sSelIdx												= sDropdownUserType.selectedIndex;
	gUserType												= ((sSelIdx > 0) ? sOptions[sSelIdx].value : "");</cfif>
	$(".TypeSensitive").hide().filter(".Type"+gUserType).show();
	if	($(".JobTitle").hide().filter(".Type"+gUserType).show().length == 0)
		$(".JobTitle.TypeNone").show();
	$(".JobSensitive").hide();
	$("input[name='#Variables.VarNameJobTitl#']").each(function(){if (this.checked) $(".Job"+this.value).show();});
	}

</script>
<style>

label.offscreen
	{
	position:							absolute;
	left:								-9999px; /* far offscreen so won't mess up sighted users */
	}

</style>
</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

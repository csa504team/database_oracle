<!---
AUTHOR:				Dileep Chanda
DATE:				05/12/2006
DESCRIPTION:		Gets GLS profile. Formerly named dsp_getprofile, which was a misnomer, because it doesn't display 
					anything. Furthermore, it can be used in an action page. But "dsp_getprofile" still exists as a 
					symbolic link to "get_profile" (temporarily, until the references in /security can be modified). 
NOTES:				Made cfincludable as this code is used in more than one place. 
INPUT:				Variables scope: Identifier (2 or 3), IMUserId, IMPOUserId (may be ""), ErrMsg and TxnErr. 
OUTPUT:				Variables scope: ErrMsg OR User info, User Role info
REVISION HISTORY:	01/08/2010, SRS:	Java method User.getUserRoles does nothing but call security.UserRoleSelCSP. 
										It contributes no business logic. Therefore, by current OISS policy, it must be 
										converted to a direct stored procedure call. MUCH, much faster now. Renamed to 
										get_userroles to suit its actual function (previous name: dsp_getprofile.cfm). 
					12/03/2008, NNI:	Modified Java object instantiation to use library code.
					12/27/2007, DKC:	Added UserRoleRqstCount to the getRoles resultset. It is used check to see if there 
										is an aged-out request for the same user/role and display a warning message.
					05/12/2006, DKC:	Original Implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.NoOfEmptyOfcs						= 6>
<cfset Variables.NoOfEmptyLocs						= 6>

<!--- Initializations: --->

<cfset Variables.ArrayRoleOfcCds					= ArrayNew(2)>
<cfset Variables.ArrayRoleLocIds					= ArrayNew(2)>
<cfset Variables.ArrayRoleBusInfo					= ArrayNew(2)>
<cfset Variables.cfpra								= ArrayNew(2)><!--- "cfpra" is SPC file abbreviation for "cfprocresult array" --->
<cfset Idx = 0>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoles">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getPrivileges">	<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleOfc">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleLoc">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getRoleBus">		<cfset Variables.cfpra[Idx][2] = Idx>
<cfset Idx = Idx + 1><cfset Variables.cfpra[Idx][1]	= "getBusinesses">	<cfset Variables.cfpra[Idx][2] = Idx>
<!--- Identifier, IMUserId and IMPOUserId are set by the caller. --->
<cfset Variables.db									= "oracle_scheduled">
<cfset Variables.LogAct								= "get profile">
<cfinclude template="#Variables.SPIncludesURL#/spc_USERROLESELCSP.PUBLIC.cfm">

<cfif Variables.TxnErr>
	<cfset Variables.SaveMe							= "No">
<cfelse>
	<!--- LocIds --->
	<cfloop query="getRoleOfc">
		<cfset Idx									= getRoleOfc.CurrentRow>
		<cfset Variables.ArrayRoleOfcCds[Idx][1]	= getRoleOfc.IMAppRoleId>
		<cfset Variables.ArrayRoleOfcCds[Idx][2]	= getRoleOfc.SBAOfcCd>
	</cfloop>
	<!--- OfcCds --->
	<cfloop query="getRoleLoc">
		<cfset Idx									= getRoleLoc.CurrentRow>
		<cfset Variables.ArrayRoleLocIds[Idx][1]	= getRoleLoc.IMAppRoleId>
		<cfset Variables.ArrayRoleLocIds[Idx][2]	= getRoleLoc.LocId>
	</cfloop>
	<!--- Role Businesses --->
	<cfloop query="getRoleBus">
		<cfset Idx									= getRoleBus.CurrentRow>
		<cfset Variables.ArrayRoleBusInfo[Idx][1]	= getRoleBus.IMAppRoleId>
		<cfset Variables.ArrayRoleBusInfo[Idx][2]	= getRoleBus.IMUserBusSeqNmb>
		<cfset Variables.ArrayRoleBusInfo[Idx][3]	= getRoleBus.IMUserBusEINSSNInd>
		<cfset Variables.ArrayRoleBusInfo[Idx][4]	= getRoleBus.IMUserBusTaxId>
		<cfset Variables.ArrayRoleBusInfo[Idx][5]	= getRoleBus.IMUserBusLocDUNSNmb>
	</cfloop>
	<!--- getRoles, getPrivileges and getBusinesses are used by the caller as-is (that is, as query objects). --->
</cfif>

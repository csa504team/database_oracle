<!--- No comment header. This file will dissappear once the consolidated user profile page is implemented. --->

<cfoutput>
<p>
	<a href="javascript:;" onclick="var x=document.getElementById('Impl').style;x.display=(x.display=='none'?'':'none');">
		Show/Hide Implementation Info.
	</a>
</p>
<div id="Impl" style="display:none;">
<p>
	We will generate form elements here, once we've figured out how to set all logic variables appropriately. 
</p>
<hr/>
<p>
	In /cls/dsp_addcustomer and /cls/dsp_profile, where AppData is inline, call: <br/>
</p>
<ul>
	&lt;cf_sbalookandfeel <b>AppDataInline="##Variables.AppDataInline##"</b> ... &gt;
</ul>
<p>
	(Passing AppDataInline as an attribute is a little known feature of &lt;cf_sbalookandfeel&gt;.) 
	Or you can do it the more well-known way: 
</p>
<ul>
	&lt;cf_sbalookandfeel ... &gt;<b>##Variables.AppDataInline##</b>&lt;/cf_sbalookandfeel&gt;
</ul>
<p>
	Either way, pass JSInline, WindowTitle, etc, as usual (as attributes). 
</p>
<hr/>
<p>
	But /security/user/dsp_user is in a frame, so you have to position the output variables in a complete page yourself, like so: 
</p>
<ul style="white-space: pre;">
##Request.SlafHead##
&lt;title&gt;<b>##Variables.WindowTitle##</b>&lt;/title&gt;##Request.SlafTopOfHead##
...
<b>##Variables.JSInline##</b>
&lt;head&gt;
&lt;body&gt;
<b>##Variables.AppDataInline##</b>
&lt;/body&gt;
&lt;/html&gt;
</ul>
<hr/>
<p>
	Steve Seaquist, 202-777-2706 (new SBA phone number). 
</p>
<hr/>
</div><!-- /Impl -->
</cfoutput>


<cfset Variables.ListNewUserPages							= "/cls/dsp_addcustomer2.cfm">		<!--- Allow for more than one. --->
<cfset Variables.ListOwnProfilePages						= "/cls/dsp_profile2.cfm">			<!--- Allow for more than one. --->
<cfset Variables.ListSecAdminPages							= "/security/user/dsp_user2.cfm">	<!--- Allow for more than one. --->

<!--- Determine environment, defaulting to page with least privilege: --->

<cfset Variables.CreatingNewUser							= "Yes">
<cfset Variables.EditingOwnProfile							= "No">
<cfset Variables.EditingUserProfile							= "No">
<cfset Variables.IMUserId									= "">
<cfset Variables.Level1Approver								= "No">
<cfset Variables.Level2Approver								= "No">
<cfset Variables.Level3Approver								= "No">
<cfset Variables.PageHeader									= "Create Your CLS Profile"><!--- Header for CreatingNewUser. --->
<cfset Variables.PageIsNewUser								= (ListFind(Variables.ListNewUserPages,		CGI.Script_Name) gt 0)>
<cfset Variables.PageIsOwnProfile							= (ListFind(Variables.ListOwnProfilePages,	CGI.Script_Name) gt 0)>
<cfset Variables.PageIsSecAdmin								= (ListFind(Variables.ListSecAdminPages,	CGI.Script_Name) gt 0)>
<cfif NOT IsDefined("Variables.CLS.CLSAuthorized")>
	<cfset Variables.CLS.CLSAuthorized						= "No">
	<cftry><!--- Don't crash if no session has been established yet. --->
		<cflock scope="session" type="readonly" timeout="30">
			<cfif IsDefined("Session.CLS")>
				<cfset Variables.CLS						= Duplicate(Session.CLS)>
			</cfif>
		</cflock>
		<cfcatch type="any"><!--- Do nothing. Okay not to have a session. ---></cfcatch>
	</cftry>
</cfif>
<cfif Variables.CLS.CLSAuthorized><!--- Okay to assume rest of CLS structure exists: --->
	<cfloop index="i" from="1" to="#ArrayLen(Variables.CLS.ArrayAllUserRoles)#">
		<cfswitch expression="#Variables.CLS.ArrayAllUserRoles[i][1]#">
		<cfcase value="SecurityReadOnly">
			<cfset Variables.ReadOnlyProfile				= "Yes">
		</cfcase>
		<cfcase value="SecurityLenderPOC,SecuritySBACOR,SecuritySBAEmplSupervisor">
			<cfset Variables.Level1Approver					= "Yes">
		</cfcase>
		<cfcase value="SEC ADMIN PO">
			<cfset Variables.Level2Approver					= "Yes">
		</cfcase>
		<cfcase value="SEC ADMIN">
			<cfset Variables.Level3Approver					= "Yes">
		</cfcase>
		</cfswitch>
		<!--- Don't cfbreak. User might have both SECADMIN and SECADMINPO. Have to loop through all to know for sure. --->
	</cfloop>
	<!--- If the user is allowed to update, don't give them a readonly profile. No need to check ReadOnlyProfile too. --->
	<cfif Variables.Level1Approver or Variables.Level2Approver or Variables.Level3Approver>
		<cfset Variables.ReadOnlyProfile					= "No"><!--- Takes as long to check it as set it, so just set it. --->
	</cfif>
</cfif>
<cfif Variables.PageIsSecAdmin>
	<cfif Variables.Level3Approver or Variables.Level2Approver>
		<!--- Even if we're in /security/user/dsp_user ligitimately, URL.IMUserId ***MUST*** be defined. Therefore: --->
		<cfif IsDefined("URL.IMUserId")>
			<cfif Variables.Level2Approver and (NOT Variables.Level3Approver)>
				<cfset Variables.PageHeader					= "View Profile"><!--- Still pretty non-specific, will improve. --->
				<cfset Variables.ReadOnlyProfile			= "Yes">
			<cfelse>
				<cfset Variables.CreatingNewUser			= "No">
				<cfset Variables.EditingUserProfile			= "Yes">
				<cfset Variables.IMUserId					= URL.IMUserId>
				<cfset Variables.PageHeader					= "Edit Profile">
			</cfif>
		<cfelseif IsDefined("URL.PageTitle") and (URL.PageTitle is "Add")>
			<!--- We're still CreatingNewUser, but we're doing it in /security. --->
			<cfset Variables.PageHeader						= "Add End User">
		<cfelse>
			<!--- Should never happen, but if it does, there's nothing we can do. Have to tell the admin to pick a user: --->
			<cflocation addtoken="No" url="/security/user/dsp_search.cfm?CameFrom=updtProfile">
		</cfif>
	<cfelse>
		<cfoutput>
Not allowed. 
</cfoutput>
		<cfabort>
	</cfif>
</cfif>
<cfif Variables.PageIsOwnProfile>
	<cfif Variables.CLS.CLSAuthorized>
		<cfset Variables.CreatingNewUser					= "No">
		<cfset Variables.EditingOwnProfile					= "Yes">
		<cfset Variables.IMUserId							= Variables.CLS.IMUserTbl.IMUserId>
	<cfelse>
		<cfset Variables.ErrMsg								= "Please log in.">
		<cflocation addtoken="No" url="#Request.SlafMustLoginURL#?ErrMsg=#URLEncodedFormat(Variables.ErrMsg)#">
	</cfif>
</cfif>

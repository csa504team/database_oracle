<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/31/2015.
DESCRIPTION:		Currently just a validation strategy tester. Evolving into the real thing.
NOTES:

	(1) We copy everything in Origs into the Variables scope (so that fields that were "view" or "skip" will be defined).

	(2) We define some UDFs to make the match up ResolvedDefaults to possible changes that could have happened on the browser.
	For example, maybe the user being edited moved from US to Germany. At load time, ResolvedDefaults was set up with IMCntryCd
	"US" and "Zip5Cd" was mandatory. But by the time we get here, IMCntryCd has been changed to "DE" and Zip5Cd should be
	optional and cleared of the old Zip5Cd. The UDFs make this match up of ResolvedDefaults very readable and maintainable.

	(3) We reset MandOpt values based on what the user edited. For example, the original IMCntryCd value from the database
	might have been "US", which would make Zip code mandatory, right? But the user just editied it to "UK", and now Zip code
	should be wiped out and Postal code should be mandatory. Everything in the nature of "conditionally mandatory" on the
	dsp page has to be replicated on the act page before we call the val_xxx routines.

	(4) We call the val_xxx routines applying the adjusted MandOpt values. Again we're copying from Form scope to Variables
	scope, but this time we're copying the cleaned up value from a val routine.

	(5) We handle fields that are entirely Form scope (not also Origs). For now, they all relate only to user businesses.

	(6) We clean up after ourselves. That is, we cflocation back to the dsp page if any simple validity edits fail.
	Otherwise, we return to the caller, who can then do "cross-edits", such as Password having to equal Re-enter Password.

INPUT:				Form variables, Variables.CancellationPage, Session.Origs.
OUTPUT:				Save profile to the security schema.
REVISION HISTORY:	09/22/2015, SRS:	Added HQLocNm (used solely for Form Data Recovery of on-screen message). Also made LAOs' 
										supervisor first, last and email fields optional (will pick up mandatory situations in s.p.). 
					09/10/2015, NS:		Updated EIN/SSN/DUNS validation to fix issue with SSN business not showing in Variables.ArrayNewBus. 
					08/11-21/2015, SRS:	Improved support for businesses. Made supervisor fields mandatory/optional by usertype.
					07/30/2015, SRS:	Original implementation as part of the new act_userprofile that consolidates all user
										profile act pages. We now have no prior knowledge as to which which columns a caller may
										chose to make uneditable, so we have to rely on the Origs structure built in the initial
										load of dsp_userprofile from the database.
--->

<!--- Initializations: --->

<cfinclude template="#Variables.LibUdfURL#/val_char.cfm">
<cfinclude template="#Variables.LibUdfURL#/val_date.cfm">
<cfinclude template="#Variables.LibUdfURL#/val_email.cfm">
<cfinclude template="#Variables.LibUdfURL#/val_loannmb.cfm">
<cfinclude template="#Variables.LibUdfURL#/val_num.cfm">
<cfinclude template="#Variables.AppUdfURL#/val_taxid.cfm">
<cfinclude template="#Variables.LibUdfURL#/val_zip.cfm">

<!--- (1) We copy everything in Origs into the Variables scope: --->

<cfset StructAppend(Variables, Variables.Origs, "Yes")>

<!--- (2)  We define some UDFs to help match up ResolvedDefaults to data that changed on the browser: --->

<cffunction name="GotValueBecauseEditable"					returntype="boolean">
	<cfargument name="ColName"				required="Yes"	type="String" hint="(a field that affects other fields)">
	<cfargument name="ForcedValue"			required="No"	type="String" default="">
	<cfset var Local										= {}>
	<!---
	ColName might not be in ResolvedDefaults if dsp_userprofile.fieldsetname.cfm was skipped. If it was skipped,
	then the user could not have edited it, so the ResolvedDefaults at initial load of the page are good.
	--->
	<cfif NOT StructKeyExists(Variables.ResolvedDefaults, Arguments.ColName)>
		<cfreturn false>
	</cfif>
	<cfset StructAppend(Local, Variables.ResolvedDefaults[Arguments.ColName], "Yes")>
	<!--- Even if the fieldset wasn't skipped, the field itself might've been uneditable: --->
	<cfif ListFind("view,skip", Local.MandOpt)				gt 0>
		<cfreturn false>
	</cfif>
	<!---
	If a form element isn't sent (checkboxes, radios, select-ones with selectedIndex -1), treat as the nullstring.
	That value could be a legitimate edit the user made. For example, the user
	--->
	<cfif	StructKeyExists(Form, Local.VarName)
		and	(Len(Form[Local.VarName])						gt 0)>
		<!--- User could have edited it, so get its value. --->
		<cfset Variables[Arguments.ColName]					= Form[Local.VarName]>
		<cfreturn true>
	</cfif>
	<!--- Set forced values into Form scope (so that val routines will load them into Variables): --->
	<cfif		Arguments.ForcedValue						is "clear">
		<cfset Form[Local.VarName]							= "">
		<cfset Variables[Arguments.ColName]					= Form[Local.VarName]>
	<cfelseif Len(Arguments.ForcedValue)					gt 0>
		<cfset Form[Local.VarName]							= Arguments.ForcedValue>
		<cfset Variables[Arguments.ColName]					= Form[Local.VarName]>
	<cfelse>
		<cfset Variables[Arguments.ColName]					= "">
	</cfif>
	<cfreturn true>
</cffunction>

<cffunction name="AlterFieldIfEditable"						returntype="void">
	<cfargument name="ColName"				required="Yes"	type="String" hint="(a field that gets affected by another field)">
	<cfargument name="MandOpt"				required="Yes"	type="String">
	<cfargument name="ForcedValue"			required="No"	type="String" default="">
	<cfargument name="EngName"				required="No"	type="String" default=""><!--- Only IMUserPhnAreaCd  uses this capability. --->
	<cfset var Local										= {}>
	<cfif NOT StructKeyExists(Variables.ResolvedDefaults, Arguments.ColName)><!--- In case fieldset skipped. --->
		<cfreturn>
	</cfif>
	<!---
	Make Local.Ref a reference to ColName's ResolvedDefaults. Changes to Local.Ref affect Variables.ResolvedDefaults[Arguments.ColName].
	Oddly, if we simply make Local that reference, it doesn't work, maybe because it was the direct recipient of the "var" keyword, above.
	--->
	<cfset Local.Ref										= Variables.ResolvedDefaults[Arguments.ColName]>
	<cfif ListFind("view,skip", Local.Ref.MandOpt)			gt 0><!--- In case field uneditable. --->
		<cfreturn>
	</cfif>
	<!--- If Arguments.MandOpt is "same" we're only calling AlterFieldIfEditable to set EngName or set a forced value into Form. --->
	<cfif Arguments.MandOpt is not "same">
		<cfset Local.Ref.MandOpt							= Arguments.MandOpt>
	</cfif>
	<cfif Len(Arguments.EngName)							gt 0>
		<cfset Local.Ref.EngName							= Arguments.EngName>
	</cfif>
	<!--- Set forced values into Form scope (so that val routines will load them into Variables): --->
	<cfif		Arguments.ForcedValue						is "clear">
		<cfset Form[Local.Ref.VarName]						= "">
	<cfelseif	Len(Arguments.ForcedValue)					gt 0>
		<cfset Form[Local.Ref.VarName]						= Arguments.ForcedValue>
	</cfif>
	<!---
	It's not necessary to set the edited ResolvedDefaults back to Session scope. As long as we can correctly reflect the state
	of Form data at the time act_userprofile is included, this code will simply be repeated if there's more than one iteration
	(dsp to act, to dsp with PrevErr, to act, to dsp with PrevErr, to act, etc).
	--->
</cffunction>

<!--- (3) We reset MandOpt values based on what the user edited: --->

<cfif GotValueBecauseEditable("IMCntryCd", "US")>
	<cfif Variables.IMCntryCd is "US">
		<cfset AlterFieldIfEditable("Zip5Cd",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("StCd",					"mand",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserCtyNm",			"mand",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserPostCd",			"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("IMUserStNm",			"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("IMUserPhnCntryCd",		"same",	"1",		"")><!--- Cascades, below. --->
	<cfelse>
		<cfset AlterFieldIfEditable("Zip5Cd",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("StCd",					"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("IMUserCtyNm",			"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserPostCd",			"mand",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserStNm",			"mand",	"",			"")>
	</cfif>
</cfif>

<cfif GotValueBecauseEditable("IMUserPhnCntryCd", "")><!--- No default. May be from a cascade, above. --->
	<cfif Variables.IMUserPhnCntryCd is "1">
		<cfset AlterFieldIfEditable("IMUserPhnAreaCd",		"mand",	"",			"Area Code")>
	<cfelse>
		<cfset AlterFieldIfEditable("IMUserPhnAreaCd",		"opt",	"",			"City Code")>
	</cfif>
</cfif>

<cfif GotValueBecauseEditable("IMUserActvInactInd", "")><!--- No default. --->
	<cfif Variables.IMUserActvInactInd is "I">
		<cfset AlterFieldIfEditable("IMUserSuspRsn",		"mand",	"",			"")>
	<cfelse><!--- Don't clear here. It's possible to by active but suspended: --->
		<cfset AlterFieldIfEditable("IMUserSuspRsn",		"opt",	"clear",	"")>
	</cfif>
</cfif>

<cfset Variables.AllowedJobTitles							= "">
<cfif GotValueBecauseEditable("IMUserTypCd", "")><!--- No default. --->
	<cfif Len(Variables.IMUserTypCd)						gt 0>
		<cfswitch expression="#Variables.IMUserTypCd#">
		<cfcase value="P"><!--- Application-to-Application --->
			<cfset AlterFieldIfEditable("HQLocId",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserPrtNm",		"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserFirstNm",	"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserLastNm",		"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserDOBDt",		"opt",	"",			"")>
		</cfcase>
		<cfcase value="U"><!--- Partner --->
			<cfset AlterFieldIfEditable("HQLocId",			"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserPrtNm",		"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserFirstNm",	"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserLastNm",		"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserDOBDt",		"mand",	"",			"")>
		</cfcase>
		<cfdefaultcase>
			<cfset AlterFieldIfEditable("HQLocId",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserPrtNm",		"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserFirstNm",	"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserLastNm",		"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserDOBDt",		"mand",	"",			"")>
		</cfdefaultcase>
		</cfswitch>
		<!--- It would needlessly complicate the cfswitch above to do the following. (Separate cfswitch simplifies the code.) --->
		<cfswitch expression="#Variables.IMUserTypCd#">
		<cfcase value="C,S,U"><!--- SBA Contractor, SBA Employee, Partner --->
			<cfset AlterFieldIfEditable("IMUserSuprvId",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvFirstNm",		"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvMidNm",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvLastNm",		"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvEmailAdrTxt",	"mand",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvUserId",		"opt",	"",			"")>
		</cfcase>
		<cfdefaultcase>
			<cfset AlterFieldIfEditable("IMUserSuprvId",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvFirstNm",		"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvMidNm",			"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvLastNm",		"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvEmailAdrTxt",	"opt",	"",			"")>
			<cfset AlterFieldIfEditable("IMUserSuprvUserId",		"opt",	"",			"")>
		</cfdefaultcase>
		</cfswitch>
	<cfelse><!--- None of these are required until the user chooses an IMUserTypCd: --->
		<cfset AlterFieldIfEditable("HQLocId",						"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserPrtNm",					"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserFirstNm",				"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserLastNm",					"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserDOBDt",					"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvId",				"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvFirstNm",			"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvMidNm",				"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvLastNm",			"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvEmailAdrTxt",		"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvUserId",			"opt",	"",			"")>
	</cfif>
</cfif>

<cfif GotValueBecauseEditable("JobTitlCd", "")><!--- No default. --->
	<cfif ListFind(Variables.JobTitlCd, "3") gt 0><!--- LAO --->
		<!--- Let s.p. validation pick these up if needed. Overrides values just set for Partner, above. --->
		<cfset AlterFieldIfEditable("IMUserSuprvFirstNm",			"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvLastNm",			"opt",	"",			"")>
		<cfset AlterFieldIfEditable("IMUserSuprvEmailAdrTxt",		"opt",	"",			"")>
	</cfif>
	<cfif ListFind(Variables.JobTitlCd, "8") gt 0><!--- Bond Holder --->
		<cfset AlterFieldIfEditable("DUNS",					"mand",	"",			"")>
		<cfset AlterFieldIfEditable("EIN1",					"mand",	"",			"")>
		<cfset AlterFieldIfEditable("EIN2",					"mand",	"",			"")>
		<cfset AlterFieldIfEditable("EIN3",					"mand",	"",			"")>
		<cfset AlterFieldIfEditable("EIN4",					"mand",	"",			"")>
	<cfelse>
		<cfset AlterFieldIfEditable("DUNS",					"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("EIN1",					"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("EIN2",					"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("EIN3",					"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("EIN4",					"opt",	"clear",	"")>
	</cfif>
	<cfif ListFind(Variables.JobTitlCd, "7") gt 0><!--- Loan Borrower --->
		<cfset AlterFieldIfEditable("TaxId1",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("TaxId2",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("TaxId3",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("TaxId4",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("TaxId5",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("TaxId6",				"mand",	"",			"")>
		<cfset AlterFieldIfEditable("LoanNmb",				"mand",	"",			"")>
	<cfelse>
		<cfset AlterFieldIfEditable("TaxId1",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("TaxId2",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("TaxId3",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("TaxId4",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("TaxId5",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("TaxId6",				"opt",	"clear",	"")>
		<cfset AlterFieldIfEditable("LoanNmb",				"opt",	"clear",	"")>
	</cfif>
</cfif>

<!--- (4) We call the val_xxx routines applying the adjusted MandOpt values: --->

<cfif Variables.PageIsUser and "No"><!--- Commented out for now. --->
	<!--- Sometimes SecAdmins want to do just suspension or just reset password. --->
	<cfif Variables.IMUserActvInactInd						is not Variables.Origs.IMUserActvInactInd>
		<cfset Variables.ArrayColsOnDspPage					= ["IMUserActvInactInd","IMUserSuspRsnCd","IMUserSuspRsn"]>
		<cfset Variables.ArrayColsOnDspPageLen				= ArrayLen(Variables.ArrayColsOnDspPage)>
	<cfelseif Variables.IMSBACrdntlPasswrd					is not Variables.Origs.IMSBACrdntlPasswrd>
		<cfset Variables.ArrayColsOnDspPage					= ["IMSBACrdntlPasswrd","IMSBACrdntlPasswrd2"]>
		<cfset Variables.ArrayColsOnDspPageLen				= ArrayLen(Variables.ArrayColsOnDspPage)>
	</cfif>
</cfif>

<cfset Variables.ArrayColsOnDspPageLen						= ArrayLen(Variables.ArrayColsOnDspPage)>
<cfloop index="i" from="1" to="#Variables.ArrayColsOnDspPageLen#">
	<cfset Variables.ColName								= Variables.ArrayColsOnDspPage[i]>
	<cfif NOT StructKeyExists(Variables.ResolvedDefaults, Variables.ColName)>
		<!--- Probably want to diagnose this!! Code at end of Defaults is supposed to prevent it!! --->
	</cfif>
	<cfset Variables.TempStruct								= Variables.ResolvedDefaults[Variables.ColName]>
	<cfset StructAppend(Variables, Variables.TempStruct, "Yes")>
	<!---
	Just because we generated a form element doesn't mean it will show up in the Form scope. Unchecked checkboxes and radio buttons
	for example, are a well-known case. Also select-one dropdowns if selectedIndex is -1. But then, that represents a change compared
	to what it was if there was a value, right? We want to do server-side validation in the case of changes.
	--->
	<cfset Variables.Col									= Variables.ColName>
	<cfset Variables.Eng									= Variables.EngName>
	<cfset Variables.Tmp									= Variables.VarName>
	<cfset Variables.Mand									= (ListFindNoCase("mand,reqd", Variables.MandOpt) gt 0)>
	<cftry>
		<cfswitch expression="#Variables.ColName#">

		<!--- login --->							<!--- login --->

		<cfcase value="IMSBACrdntlLognNm">			<cfset val_char	(Tmp,        15, Eng, Mand)></cfcase>
		<cfcase value="IMSBACrdntlPasswrdExprDt">	<cfset val_date	(Tmp,       "N", Eng, Mand)></cfcase>
		<cfcase value="IMSBACrdntlPasswrd">			<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase>
		<cfcase value="IMSBACrdntlPasswrd2">		<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase>
		<cfcase value="IMUserActvInactInd">			<cfset val_char	(Tmp,         1, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuspRsnCd">			<cfset val_char	(Tmp,         5, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuspRsnDt">			<cfset val_date	(Tmp,       "N", Eng, Mand)></cfcase>
		<cfcase value="IMUserSuspRsn">				<cfset val_char	(Tmp,       255, Eng, Mand)></cfcase>

		<!--- identity --->							<!--- identity --->

		<cfcase value="IMUserTypCd">				<cfset val_char	(Tmp,         1, Eng, Mand)></cfcase>
		<cfcase value="IMUserPrtNm">				<cfset val_char	(Tmp,       128, Eng, Mand)></cfcase>
		<cfcase value="IMUserFirstNm">				<cfset val_char	(Tmp,       128, Eng, Mand)></cfcase>
		<cfcase value="IMUserLastNm">				<cfset val_char	(Tmp,       128, Eng, Mand)></cfcase>
		<cfcase value="IMUserMidNm">				<cfset val_char	(Tmp,       128, Eng, Mand)></cfcase>
		<cfcase value="IMUserSfxNm">				<cfset val_char	(Tmp,        20, Eng, Mand)></cfcase>
		<cfcase value="IMUserDOBDt">				<cfset val_date	(Tmp,       "N", Eng, Mand)></cfcase>
		<cfcase value="IMUserLast4DgtSSNNmb">		<cfset val_num	(Tmp,"smallint", Eng, Mand)></cfcase>

		<!--- contact --->							<!--- contact --->

		<cfcase value="IMCntryCd">					<cfset val_char	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="Zip5Cd">						<cfset val_char	(Tmp,         5, Eng, Mand)></cfcase>
		<cfcase value="Zip4Cd">						<cfset val_char	(Tmp,         4, Eng, Mand)></cfcase>
		<cfcase value="IMUserPostCd">				<cfset val_char	(Tmp,        20, Eng, Mand)></cfcase>
		<cfcase value="IMUserStNm">					<cfset val_char	(Tmp,        60, Eng, Mand)></cfcase>
		<cfcase value="IMUserStr1Txt">				<cfset val_char	(Tmp,        80, Eng, Mand)></cfcase>
		<cfcase value="IMUserStr2Txt">				<cfset val_char	(Tmp,        80, Eng, Mand)></cfcase>
		<cfcase value="IMUserCtyNm">				<cfset val_char	(Tmp,        60, Eng, Mand)></cfcase>
		<cfcase value="StCd">						<cfset val_char	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="IMUserPhnCntryCd">			<cfset val_char	(Tmp,         6, Eng, Mand)></cfcase>
		<cfcase value="IMUserPhnAreaCd">			<cfset val_char	(Tmp,         5, Eng, Mand)></cfcase>
		<cfcase value="IMUserPhnLclNmb">			<cfset val_char	(Tmp,        15, Eng, Mand)></cfcase>
		<cfcase value="IMUserPhnExtnNmb">			<cfset val_char	(Tmp,         6, Eng, Mand)></cfcase>
		<cfcase value="IMUserEmailAdrTxt">			<cfset val_email(Tmp,            Eng, Mand)></cfcase>
		<cfcase value="IMUserEmailAdrTxt2">			<cfset val_email(Tmp,            Eng, Mand)></cfcase>

		<!--- usertype --->							<!--- usertype --->

		<cfcase value="LocId">						<cfset val_char	(Tmp,         7, Eng, Mand)></cfcase>
		<cfcase value="HQLocId">					<cfset val_char	(Tmp,         7, Eng, Mand)></cfcase>
		<cfcase value="HQLocNm">					<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase><!--- Hidden field. Not used here. --->
		<cfcase value="SBAOfcCd">					<cfset val_num	(Tmp,         4, Eng, Mand)></cfcase>
		<cfcase value="JobTitlCd">					<cfset val_char	(Tmp,        20, Eng, Mand)></cfcase>
		<cfcase value="DUNS">						<cfset val_num	(Tmp,         9, Eng, Mand)></cfcase>
		<cfcase value="LoanNmb">					<cfset val_loannmb(Tmp,          Eng, Mand)></cfcase>
		<cfcase value="TaxId1">						<cfset val_num	(Tmp,         3, Eng, Mand)></cfcase>
		<cfcase value="TaxId2">						<cfset val_num	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="TaxId3">						<cfset val_num	(Tmp,         4, Eng, Mand)></cfcase>
		<cfcase value="TaxId4">						<cfset val_num	(Tmp,         3, Eng, Mand)></cfcase>
		<cfcase value="TaxId5">						<cfset val_num	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="TaxId6">						<cfset val_num	(Tmp,         4, Eng, Mand)></cfcase>
		<cfcase value="TaxId7">						<cfset val_num	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="TaxId8">						<cfset val_num	(Tmp,         7, Eng, Mand)></cfcase>
		<cfcase value="TaxId9">						<cfset val_num	(Tmp,         2, Eng, Mand)></cfcase>
		<cfcase value="TaxIda">						<cfset val_num	(Tmp,         7, Eng, Mand)></cfcase>
		<cfcase value="SuprvEmail">					<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase><!--- Search field. Not used here. --->
		<cfcase value="SuprvFirst">					<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase><!--- Search field. Not used here. --->
		<cfcase value="SuprvLast">					<cfset val_char	(Tmp,      9999, Eng, Mand)></cfcase><!--- Search field. Not used here. --->
		<cfcase value="IMUserSuprvId">				<cfset val_char	(Tmp,        15, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuprvFirstNm">			<cfset val_char	(Tmp,        50, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuprvMidNm">			<cfset val_char	(Tmp,        50, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuprvLastNm">			<cfset val_char	(Tmp,        50, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuprvEmailAdrTxt">		<cfset val_char	(Tmp,       255, Eng, Mand)></cfcase>
		<cfcase value="IMUserSuprvUserId">			<cfset val_num	(Tmp,        10, Eng, Mand)></cfcase>

		<!--- borrower --->							<!--- borrower --->

		<cfcase value="IMUserBusEINSSNInd">			<cfset val_char	(Tmp,         1, Eng, Mand)></cfcase>
		<cfcase value="IMUserBusTaxId">				<cfset val_taxid(Tmp,            Eng, Mand,"Yes",IMUserBusEINSSNInd,"No")></cfcase>
		<cfcase value="IMUserBusLocDUNSNmb">		<cfset val_char	(Tmp,         9, Eng, Mand)></cfcase>

		<!--- creds --->							<!--- creds --->

		<cfcase value="UserIMAsurncLvlCd">			<cfset val_num	(Tmp, "tinyint", Eng, Mand)></cfcase><!--- but not other creds --->

		<!--- verify --->							<!--- verify --->

		<cfcase value="Verify">						<cfset val_char	(Tmp,         5, Eng, PageIsAddCustomer)></cfcase>
		</cfswitch>
		<cfset Variables[Variables.ColName]					= Variables[Variables.VarName]>
		<cfcatch type="any">
			<!---
			SRS note: I'd like to leave this debug code in for a while. But a cfdump looks pretty unprofessional.
			So this cfcatch is at least VERY CAREFUL about who sees what:
			--->
			<cfset Variables.ErrMsg							&= " <li>#CFCatch.Message# #CFCatch.Detail#</li>">
			<cfswitch expression="#LCase(CGI.Server_Name)#">
			<cfcase value="cadweb.sba.gov,ca1anacostia.sba.gov,catweb.sba.gov,ca2rouge.sba.gov,ca2yukon.sba.gov">
				<cfif Request.SlafIPAddrIsOCA>
					<cfoutput>
<p>
	<b>This message and dump occurs only on cadweb and catweb. </b>
</p>
<p>
	A crash occurred in simple validity edits ("val_" routines).
	This cfdump of Variables may help. (#LCase(Left(Request.SlafDevTestProd,1)) & Request.SlafServerUniqueCode#)
</p>
</cfoutput>
					<cfdump var="#Variables#" label="Variables" expand="No">
					<cfabort>
				</cfif>
			</cfcase>
			</cfswitch>
			<cfoutput>
<p>
	An error occurred. Please report the following message to the SBA:
</p>
<p>
	#CFCatch.Message# #CFCatch.Detail# (#LCase(Left(Request.SlafServerDevTestProd,1)) & Request.SlafServerUniqueCode#)
</p></cfoutput>
			<cfabort>
		</cfcatch>
	</cftry>
</cfloop>

<!--- (5) We handle fields that are entirely Form scope (not also Origs): --->

<cfset Variables.ArrayBusDel								= ArrayNew(1)>
<cfif StructKeyExists(Form, "BusDel")><!--- And generally, it won't exist, leaving BusDel set to nullstring (do nothing). --->
	<cfloop index="BusDelIdx" list="#Form.BusDel#">
		<cfset ArrayAppend(	Variables.ArrayBusDel,
							{
							"IMUserBusSeqNmb"				= Variables.Origs["IMUserBusSeqNmb_#BusDelIdx#"],
							"IMUserBusLocDUNSNmb"			= Variables.Origs["IMUserBusLocDUNSNmb_#BusDelIdx#"]
							})>
	</cfloop>
</cfif>

<cfset Variables.ArrayNewBus								= ArrayNew(1)>
<cfif StructKeyExists(Form, "TotalNewBus")><!--- And generally, it won't exist, leaving ArrayNewBus empty (do nothing). --->
	<cfif NOT IsNumeric(Form.TotalNewBus)>
		<cfset Variables.FieldErrMsg						= "Internal Error: The count of new businesses you entered got "
															& "messed up somehow (""#Form.TotalNewBus#"").">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelse>
		<cfloop index="NewBusIdx" from="1" to="#Form.TotalNewBus#">
			<cfif NOT StructKeyExists(Form, "NewBusWasNotDiscarded_#NewBusIdx#")>
				<!--- If not defined, New Business was discarded, so don't require other fields to exist: --->
				<cfcontinue>
			</cfif>
			<cfif NOT StructKeyExists(Form, "NewBusEINSSN_#NewBusIdx#")>
				<cfset Variables.FieldErrMsg				= "For New Business ###NewBusIdx#, EIN/SSN not chosen.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfcontinue><!--- Done with this New Business. Don't append it to ArrayNewBus. --->
			</cfif>
			<cfset Variables.IMUserBusEINSSNInd				= Form["NewBusEINSSN_#NewBusIdx#"]>
			<cfif ListFind("E,S", Variables.IMUserBusEINSSNInd) is 0>
				<cfset Variables.FieldErrMsg				= "For New Business ###NewBusIdx#, EIN/SSN indicator got "
															& "messed up somehow (""#Variables.IMUserBusEINSSNInd#"").">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfcontinue>
			</cfif>
			<cfif StructKeyExists(Form, "NewBusCertified_#NewBusIdx#")>	<cfset Variables.BusEINCertInd	= "Y">
			<cfelse>													<cfset Variables.BusEINCertInd	= ""></cfif>
			<cfif NOT val_taxid(
								"NewBusTaxId_#NewBusIdx#",
								"For New Business ###NewBusIdx#, Tax ID",
								"Yes",<!--- Mandatory --->
								"Yes",<!--- Longer, more detailed error message --->
								Variables.IMUserBusEINSSNInd,
								(Variables.BusEINCertInd is "Y")
								)>
				<cfcontinue>
			</cfif>
			<cfset Variables.IMUserBusTaxId					= Replace(Form["NewBusTaxId_#NewBusIdx#"], "-", "", "ALL")>
			<cfif ListFind("E", Variables.IMUserBusEINSSNInd) gt 0>
				<cfif NOT val_num	(
									 "NewBusDUNS_#NewBusIdx#",
									 9,<!--- digits, max --->
									 "For New Business ###NewBusIdx#, DUNS",
									 "Yes"<!--- "FMand" argument, so "Yes" means DUNS is mandatory. --->
									)>
					<cfcontinue>
				</cfif>
				<!--- Prepend leading zeroes if DUNS number entered is less than 9 digits: --->
				<cfset Variables.IMUserBusLocDUNSNmb			= NumberFormat(Form["NewBusDUNS_#NewBusIdx#"], "000000000")>
			<cfelse>
				<cfset Variables.IMUserBusLocDUNSNmb			= "">
			</cfif>
			<cfset ArrayAppend	(Variables.ArrayNewBus,
									{
									"IMUserBusEINSSNInd"	= Variables.IMUserBusEINSSNInd,
									"BusEINCertInd"			= Variables.BusEINCertInd,
									"IMUserBusTaxId"		= Variables.IMUserBusTaxId,
									"IMUserBusLocDUNSNmb"	= Variables.IMUserBusLocDUNSNmb
									}
								)>
		</cfloop>
	</cfif><!--- /TotalNewBus --->
</cfif>

<!--- (6) We clean up after ourselves: --->

<cfif NOT Variables.SaveMe>
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<!--- For now, we're just exploring what we can do. Not trying to validate or save yet: --->

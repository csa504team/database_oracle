<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		Currently just a validation strategy tester. Evolving into the real thing. 
NOTES:				None. 
INPUT:				Form variables, Variables.CancellationPage, Session.Origs. 
OUTPUT:				Save profile to the security schema. 
REVISION HISTORY:	07/30/2015,	SRS:	Original implementation as part of the new act_userprofile that consolidates all user 
										profile act pages. We now have no prior knowledge as to which which columns a caller may 
										chose to make uneditable, so we have to rely on the Origs structure built in the initial 
										load of dsp_userprofile from the database. 
--->

<!--- Configuration Parameters: --->

<cfparam name="Variables.CancellationPage"	type="String">	<!--- No default attribute, therefore, a mandatory input. --->
<cfset Variables.DisplayPage								= Replace(Variables.PageName, "act_", "dsp_", "One")>
<cfparam name="Variables.SuccessPage"		type="String"	default="#Variables.DisplayPage#">
<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfparam name="Variables.UpdateTrace"						default="No">

<!--- Check whether user should even be at this page: --->

<cfif		NOT IsDefined("Form.FieldNames")>
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
<cfelseif	IsDefined("Form.SubmitButton")
	and		(Form.SubmitButton is "Cancel")><!--- Keep the cancellation SubmitButton value in sync with dsp_userprofile. --->
	<cflocation addtoken="No" url="#Variables.CancellationPage#">
</cfif>

<cfinclude template="#Variables.LibUdfURL#/bld_DspPageLoadTimeOrigsUDFs.cfm">
<cfset GetDspPageLoadTimeOrigs()><!--- Retrieve Session.Origs. We mainly want "MayEdit" right now. --->
<cfif NOT Variables.OrigsSuccess>
	<!---
	If OrigsSuccess isn't "Yes", it probably means that Session.DspPageLoadTimeOrigs.DspPage_Handshake wasn't defined, 
	or, if it was, the Handshake didn't reference the right display page. Either way, it's rather equivalent to detecting 
	NOT IsDefined("Form.FieldNames"). So treat it the exact same way as above: 
	--->
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
</cfif>

<cfif NOT Variables.Origs.MayEdit><!--- We WANT to crash if MayEdit is undefined (to debug), so just assume it is defined. --->
	<!---
	This condition will probably never happen, because the dsp page should set ReadOnlyProfile, which should result in 
	its not generating the form nor its submit buttons. But theoretically, a hacker could manufacture a form submission. 
	So there's no harm done in planning for it and telling the hacker "shame on you": 
	--->
	<cfset Variables.ErrMsg									= "Error: You are not allowed to save this profile to the database. ">
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<!--- Initializations: --->

<cfparam name="Variables.Commentary"						default="">
<cfset Variables.ErrMsgBase									= "Error(s) occurred that prevented saving the page's data: ">
<cfset Variables.ErrMsg										= Variables.ErrMsgBase>
<cfset Variables.SaveMe										= "Yes">
<cfset Variables.TxnErr										= "No">
<cfset Variables.PageIsAddCustomer							= "No">
<cfset Variables.PageIsProfile								= "No">
<cfset Variables.PageIsUser									= "No">
<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/cls/dsp_addcustomer.cfm,/cls/dsp_addcustomer2.cfm">
	<cfset Variables.PageIsAddCustomer						= "Yes">
</cfcase>
<cfcase value="/cls/dsp_profile.cfm,/cls/dsp_profile2.cfm">
	<cfset Variables.PageIsProfile							= "Yes">
</cfcase>
<cfcase value="/security/user/dsp_user.cfm,/security/user/dsp_user2.cfm">
	<cfset Variables.PageIsUser								= "Yes">
</cfcase>
</cfswitch>

<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.ActvIMAsurncLvlTbl						= Server.Scq.security.ActvIMAsurncLvlTbl>
	<cfset Variables.ActvIMJobTitlTypTbl					= Server.Scq.security.ActvIMJobTitlTypTbl>
	<cfset Variables.ActvIMUserSuspRsnTbl					= Server.Scq.security.ActvIMUserSuspRsnTbl>
	<cfset Variables.ActvIMUserTypTbl						= Server.Scq.security.ActvIMUserTypTbl>
	<cfset Variables.ScqActvCountries						= Server.Scq.ActvIMCntryCdTbl>	<!--- Oddly, not used by addcustomer. --->
	<cfset Variables.ScqActvStates							= Server.Scq.ActvStTbl>			<!--- Oddly, not used by addcustomer. --->
</cflock>

<!--- Simple Validity Edits: --->

<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "About to call val_calls.<br/>">
</cfif>
<cfinclude template="act_userprofile.val_calls.cfm"><!--- Does it's own cflocation if SaveMe is "No". --->
<cfif Variables.UpdateTrace or "Yes"><!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed simple validity edits.<br/>">
</cfif>

<!--- Cross Edits Go Here: --->

<cfset Variables.CallValidateIMUserSuprvCSP					= "No">
<cfif Len(Variables.IMUserTypCd) gt 0>
	<cfif ListFind(ValueList(Variables.ActvIMUserTypTbl.code), Variables.IMUserTypCd) is 0>
		<cfset Variables.FieldErrMsg						= "Invalid User Type.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelseif Len(Variables.JobTitlCd) gt 0>
		<cfloop index="Job" list="#Variables.JobTitlCd#">
			<cfset Variables.JobAllowed						= "No">
			<cfset Variables.JobDescription					= "Job Classification code #Job#">
			<cfloop query="Variables.ActvIMJobTitlTypTbl">
				<cfif Variables.ActvIMJobTitlTypTbl.code is Job>
					<cfset Variables.JobDescription			= Variables.ActvIMJobTitlTypTbl.description>
					<cfif Variables.ActvIMJobTitlTypTbl.IMUserTypCd is Variables.IMUserTypCd>
						<cfset Variables.JobAllowed			= "Yes">
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>
			<cfif NOT Variables.JobAllowed>
				<cfset Variables.FieldErrMsg				= "#Variables.JobDescription# is not a valid Job Classification for User Type.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfloop>
	</cfif>
	<cfswitch expression="#Variables.IMUserTypCd#">
	<cfcase value="C,S,U">
		<cfif	(Len(Variables.IMUserSuprvFirstNm)			is 0)
			or	(Len(Variables.IMUserSuprvLastNm)			is 0)
			or	(Len(Variables.IMUserSuprvEmailAdrTxt)		is 0)><!--- But not IMUserSuprvId, IMUserSuprvMidNm or IMUserSuprvUserId (optional). --->
			<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
			<!--- Could call ValidateIMUserSuprvCSP here, or could delay it and use CallValidateIMUserSuprvCSP. --->
		<cfelse>
			<cfset Variables.FieldErrMsg					= "Please lookup and choose a supervisor.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfcase>
	</cfswitch>
</cfif>
<!--- Add more here. Examples: --->
	<!--- 1. If Password or Re-enter Password given, they have to pass PasswordValidate (in bld_PasswordUDFs) and match each other. --->
	<!--- 2. If not done above, and if CallValidateIMUserSuprvCSP is yes, call ValidateIMUserSuprvCSP. --->
	<!--- 3. Need to validate other fields that have reference tables against ServerCachedQueries. --->

<cfif NOT Variables.SaveMe><!--- ((in cross edits)) --->
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<cfif Variables.UpdateTrace or "Yes"><!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed cross edits.<br/>">
</cfif>

<!--- Simple mechanism for detecting changed data, based on current data structures: --->

<cffunction name="SomethingChanged"							returntype="boolean">
	<cfargument name="ColumnList"							type="String" required="Yes">
	<cfset var Local										= {}>
	<cfloop index="Local.ColName" list="#Arguments.ColumnList#">
		<cfif Variables[Local.ColName]						is not Variables.Origs[Local.ColName]>
			<cfreturn true>
		</cfif>
	</cfloop>
	<cfreturn false>
</cffunction>

<cfset Variables.ColumnListIMUserTbl						= "IMUserNm,IMUserTypCd,"
															& "IMUserPrtNm,IMUserFirstNm,IMUserLastNm,IMUserMidNm,IMUserSfxNm,"
															& "IMUserDOBDt,IMUserLast4DgtSSNNmb,"
															& "IMUserStr1Txt,IMUserStr2Txt,IMUserCtyNm,StCd,Zip5Cd,Zip4Cd,IMCntryCd,IMUserStNm,IMUserPostCd,"
															& "IMUserPhnCntryCdIMUserPhnAreaCd,IMUserPhnLclNmb,IMUserPhnExtnNmb,"
															& "IMUserEmailAdrTxt,IMUserEmailAdrTxt2,"
															& "HQLocId,LocId,PrtId,PrtLocNm,SBAOfcCd,SBAOfc1Nm,IMAsurncLvlCd,"
															& "IMAsurncLvlCd,IMUserActvInactInd,IMUserSuspRsn,IMUserSuspRsnCd,IMUserSuspRsnDt,"
															& "IMUserSuprvId,IMUserSuprvFirstNm,IMUserSuprvMidNm,IMUserSuprvLastNm,IMUserSuprvEmailAdrTxt,IMUserSuprvUserId">
<cfset Variables.ColumnListBusinesses						= "IMUserBusSeqNmb,IMUserBusEINSSNInd,IMUserBusTaxId,IMUserBusLocDUNSNmb,BusEINCertInd">
<!--- Etc. --->

<cfif "No"><!--- "No" essentially comments out this example usage of SomethingChanged. --->
<cftransaction action="Begin">

	<cfif SomethingChanged(Variables.ColumnListIMUserTbl)>
		<!--- Save these IMUserTbl columns here. --->
	</cfif>

	<!--- Example of how to use SomethingChanged with an existing one-to-many table, such as user businesses in profile or user pages: --->
	<cfloop index="i" from="1" to="#Variables.RecordCountUserBus#"><!--- Rows --->
		<cfset Variables.ColumnListBusinessesIndexed		= "">
		<cfloop index="Variables.Key" list="#Variables.ColumnListBusinesses#"><!--- Columns --->
			<cfset Variables.IndexedKey						= "#Variables.Key#_#i#">
			<cfset Variables[Variables.Key]					= Variables[Variables.IndexedKey]><!--- For saving to the database if changed. --->
			<cfset Variables.ColumnListBusinessesIndexed	= ListAppend(Variables.ColumnListBusinessesIndexed, Variables.IndexedKey)>
		</cfloop>
		<!--- Now ColumnListBusinessesIndexed has the same column names as ColumnListBusinesses, but with "_#i#" suffixed. --->
		<cfif SomethingChanged(Variables.ColumnListBusinessesIndexed)>
			<!--- Columns have been loaded in Variables.Key loop, so just call update stored procedure. --->
		</cfif>
	</cfloop>

	<!--- Etc. --->

	<cfif Variables.TxnErr>
		<cftransaction action="Rollback" />
		<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
		<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
	<cfelse>
		<cftransaction action="Commit" />
		<cfset Variables.Commentary							= "Data saved to database. ">
	</cfif>

</cftransaction>
</cfif><!--- /"No" --->

<cfset Variables.ErrMsg										= "">
<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
<cflocation addtoken="No" url="#Variables.SuccessPage#?Commentary=#URLEncodedFormat(Variables.Commentary)#"><!--- Wherever caller told us to go. --->

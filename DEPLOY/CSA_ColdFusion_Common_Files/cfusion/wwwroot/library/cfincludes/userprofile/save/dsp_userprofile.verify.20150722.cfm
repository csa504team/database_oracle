<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/22/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				See dsp_userprofile.template.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Original implementation. 
--->

<cffunction name="makeRandomString" returnType="string" output="false">
	<cfset var chars										= "23456789ABCDEFGHJKMNPQRSTXYZabcdefghjkmnpqrstxyz">
	<cfset var length										= RandRange(5,5)>
	<cfset var result										= "">
	<cfset var i											= "">
	<cfset var char											= "">
	<cfscript>
	for	(i=1; i <= length; i++) {
		char												= Mid(chars, RandRange(1, Len(chars)),1);
		result												&=char;
	}
	</cfscript>
	<cfreturn result>
</cffunction>

<cfset Variables.EditMaskChar								= "N">
<cfset Variables.RandNmb4									= makeRandomString()>

<cflock scope="Session" type="Exclusive" timeout="30">
	<cfset Session.RandNmb4									= Trim(Variables.RandNmb4)>
</cflock>

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#</cfoutput>
	<cfif Variables.PageIsAddCustomer>
		<cfparam name="Variables.MandOptVerify"				default="mand">
	<cfelse>
		<cfset Variables.MandOptVerify						= "skip">
	</cfif>
	<cfif ListFindNoCase("view,skip", Variables.MandOpt) is 0><!--- Readonly display of Verify makes no sense. --->
		<cfoutput>
<fieldset class="inlineblock">
  <legend>Verify</legend>
  <cfimage action="captcha" height="40" width="250" text="#Variables.RandNmb4#" difficulty="medium"><br/>
  <label for="cptcha" class="mandlabel">Please enter text shown in the image (case sensitive)</label>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock manddata">
        <input type="text" name="cptcha" id="cptcha" size="20" maxlength="5" value="" onchange="
        var sMand											= isMand(this);
        if  (EditMask('Answer', this.value, '#Variables.EditMaskChar#', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</fieldset><!-- /Verify --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
</cfif>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

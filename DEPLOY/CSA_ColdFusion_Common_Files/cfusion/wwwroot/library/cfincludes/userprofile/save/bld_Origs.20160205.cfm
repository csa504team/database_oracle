<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Builds Variables.Origs with values from the database (or nullstring, if Variables.EndUserData.IMUserId is 0).
NOTES:				Should only be done in dsp_userprofile when DisplayingFormDataFromDatabase. In Self-Test Mode, also
					tests bld_Defaults.
INPUT:				Variables.EndUserData (containing, at a minimum, IMUserId and MayEdit).
OUTPUT:				Variables: All columns needed by included "fieldset" files, ErrMsg, TxnErr,
					GetUser, GetUBus, GetLogn, GetCred, GetFedU, GetTitl and Origs. Origs allows use by both dsp and act.
REVISION HISTORY:	02/05/2016, NS; 	Added support for JobTitlCdPendApprLst
					02/03/2016, NS:     Added LastRecertDt
					02/03/2016, RSD: 	Modified masking for SSN which will now occur on display.
					01/21/2016, NS:		Added ImUserAcctNewInd
					01/19/2015, SRS:	Adapted to no longer getting {Agncy,Dept}{Id,Nm} columns from UserInfoSelCSP.
					12/14/2015, DG:		Added IMUserCreatDt to display Account Created Date on User profile page.
					11/10/2015, NS:		Added support for fax form controls.
					09/21/2015, RSD&SRS:Saving information relating to SSN and LoanNmb. (RSD) Added HQLocNm. (SRS)
					07/30/2015,	SRS:	Original implementation. Logically similar to legacy routine bld_UserInfoArrays.cfm,
										but got rid of the arrays in favor of unique names.
--->

<!--- Configuration Parameters: --->

<cfset Variables.BldOrigs_BogusNegIMUserIdForNewProfiles	= -1>

<!--- To execute this file in self-test mode, go directly to it in a web browser (no caller): --->

<cfset Variables.BldOrigs_SelfTestMode						= (CGI.Script_Name is "/library/cfincludes/userprofile/bld_Origs.cfm")>
<cfif  Variables.BldOrigs_SelfTestMode>

	<cfswitch expression="#LCase(CGI.Server_Name)#">
	<cfcase value="cadweb.sba.gov,catweb.sba.gov,ca1anacostia.sba.gov,ca2rouge.sba.gov,ca2yukon.sba.gov">
		<!--- Allows us developers (plus other SBA folks, whom the SBA also trusts) to see any IMUserId, ... --->
		<cfif IsDefined("URL.IMUserId")>
			<cfset Variables.EndUserData					=
				{
				"IMUserId"									= URL.IMUserId,
				"MayEdit"									= "Yes"
				}>
		<cfelse>
			<cfset Variables.EndUserData					=
				{
				"IMUserId"									= 420,
				"MayEdit"									= "Yes"
				}><!--- Steve Seaquist on DEVCAFS1 and TSTCAFS4. --->
		</cfif>
	</cfcase>
	<cfdefaultcase>
		<!--- ..., but no one else!!! --->
		<cfoutput>
This file's Self-Test Mode is not allowed on servers visible to the general public.
</cfoutput>
		<cfsetting showdebugoutput="No"><!--- Don't even showdebugoutput on public servers. --->
		<cfabort>
	</cfdefaultcase>
	</cfswitch>
	<cfset Variables.DisplayingFormDataEditedByUser			= "No">	<!--- Self-Test build of Origs.ArrayColsOnDspPage. --->
	<cfset Variables.DisplayingFormDataFromDatabase			= "Yes"><!--- Self-Test build of Origs.ArrayColsOnDspPage. --->
	<cfset Variables.ReadOnlyProfile						= "No">	<!--- Self-Test will toggle it later. --->

<cfelse>

	<!--- If we're **not** in self-test mode, don't trash 3 key variables in the caller (presumed to exist): --->
	<cfif IsDefined("Variables.db")>
		<cfset Variables.BldOrigs_hold_db					= Variables.db>
	</cfif>
	<cfif IsDefined("Variables.password")>
		<cfset Variables.BldOrigs_hold_password				= Variables.password>
	</cfif>
	<cfif IsDefined("Variables.username")>
		<cfset Variables.BldOrigs_hold_username				= Variables.username>
	</cfif>

</cfif><!--- /BldOrigs_SelfTestMode --->

<!--- Verify Minimal Inputs: --->

<cfparam name="Variables.ErrMsg"							default="">
<cfparam name="Variables.TxnErr"							default="No">

<!--- Database call: --->

<cfif NOT Variables.TxnErr><!--- Keep indention low. --->
	<cfset Variables.db										= "oracle_housekeeping">
	<cfset Variables.password								= "">
	<cfset Variables.username								= "">
	<cfset Variables.Identifier								= "2">
	<cfset Variables.LogAct									= "retrieve profile">
	<cfset Variables.cfpra									=
			[
			["GetUser",										1],
			["GetUBus",										2],
			["GetLogn",										3],
			["GetCred",										4],
			["GetFedU",										5],
			["GetTitl",										6],
			["GetUSSN",										7]
			]>

	<cfif Variables.EndUserData.IMUserId is 0>
		<!---
		For the purposes of just getting all Query.Column references defined, a negative number is better than 0. (IMCrdntlTbl
		actually has rows with IMUserId 0!) So we TEMPORARILY use a bogus negative number, then IMMEDIATELY set IMUserId to 0,
		preserving 0 as the special value that means new profile. Doing a dummy I/O like this will allow us someday to support
		new columns automatically via Query.ColumnList. The I/O overhead is trivial, so maintenance benefits far outweigh it.
		--->
		<cfset Variables.IMUserId							= Variables.BldOrigs_BogusNegIMUserIdForNewProfiles>
		<cfinclude template="/cfincludes/oracle/security/spc_USERINFOSELCSP.PUBLIC.cfm">
		<cfset Variables.IMUserId							= 0>
	<cfelse>
		<cfset Variables.IMUserId							= Variables.EndUserData.IMUserId>
		<cfinclude template="/cfincludes/oracle/security/spc_USERINFOSELCSP.PUBLIC.cfm">
	</cfif>

	<cfif Variables.DoTraceAll>
		<cfset Variables.TraceTxt							&= "bld_Origs called UserInfoSelCSP with IMUserId: "
															&  "'#Variables.EndUserData.IMUserId#'. "
															&  "Results: ">
		<cfloop index="Variables.BldOrigs_Idx" from="1" to="#ArrayLen(Variables.cfpra)#">
			<cfset Variables.BldOrigs_RSName				= Variables.cfpra[Variables.BldOrigs_Idx][1]>
			<cfif NOT IsDefined(Variables.BldOrigs_RSName & ".RecordCount")>
				<cfset Variables.TraceTxt					&= "No, ">
			<cfelse>
				<cfset Variables.TraceTxt					&= Evaluate(Variables.BldOrigs_RSName & ".RecordCount") & ", ">
			</cfif>
		</cfloop>
		<cfset Variables.TraceTxt							&= "done. ">
	</cfif>
</cfif><!--- /TxtErr --->

<!--- Build Variables.Origs: --->

<cfif NOT Variables.TxnErr><!--- Keep indention low. --->

	<cfif NOT IsDefined("LoadOrig")><!--- Probably defined in caller, but BldOrigs_SelfTestMode needs it too. --->
		<cfinclude template="/library/udf/bld_DspPageLoadTimeOrigsUDFs.cfm">
	</cfif>
	<!---
	In the case of IMUserId, MayEdit, RecordCounts, etc, use LoadOrig, because we want to save those exact values, even if
	Variables.EndUserData.IMUserId is 0. But in the more common case of record set data, we define a special-use routine,
	LoadBldO, which requires a little explanation:

	When Variables.EndUserData.IMUserId is 0, all result sets SHOULD be empty, but sometimes they aren't, as mentioned in
	the comment just before calling UserInfoSelCSP, above. Theoretically, it could happen in the case of any particular bogus
	negative number we use. We don't want to load bad data in that situation. LoadBldO prevents that.
	--->
	<cfif NOT IsDefined("LoadBldO")><!--- Defined only here. --->
		<cffunction name="LoadBldO" returntype="void">
			<cfargument name="ColumnName"	required="Yes"	type="String">
			<cfargument name="ValueFromDB"	required="Yes"	type="Any">
			<cfif Variables.EndUserData.IMUserId is 0>
				<cfset LoadOrig(Arguments.ColumnName,		"")><!--- When this default isn't what you want, use LoadOrig. --->
			<cfelse>
				<cfset LoadOrig(Arguments.ColumnName,		Arguments.ValueFromDB)>
			</cfif>
		</cffunction>
	</cfif>
	<cfset LoadOrig("IMUserId",								Variables.EndUserData.IMUserId)><!--- LoadOrig, not LoadBldO --->
	<cfset LoadOrig("MayEdit",								Variables.EndUserData.MayEdit)>	<!--- LoadOrig, not LoadBldO --->

	<!--- IMUserTbl: --->

	<cfset LoadBldO("IMUserNm",								GetUser.IMUserNm)>
	<cfset LoadBldO("IMUserTypCd",							GetUser.IMUserTypCd)>
	<cfset LoadBldO("CurrIMUserTypCd",						GetUser.IMUserTypCd)><!--- Used by ValidateSupervisor callback. --->
	<cfset LoadBldO("IMUserFirstNm",						GetUser.IMUserFirstNm)>
	<cfset LoadBldO("IMUserLastNm",							GetUser.IMUserLastNm)>
	<cfset LoadBldO("IMUserMidNm",							GetUser.IMUserMidNm)>
	<cfset LoadBldO("IMUserSfxNm",							GetUser.IMUserSfxNm)>
	<cfset LoadBldO("IMUserPrtNm",							GetUser.IMUserPrtNm)>
	<cfset LoadBldO("IMUserDOBDt",							GetUser.IMUserDOBDt)>
	<!--- Only safe time to format data is right after it comes off of the database. Only a few keys have "Disp" prefixes. --->
	<cfif IsDate(GetUser.IMUserDOBDt)>
		<cfset LoadBldO("DispIMUserDOBDt",		DateFormat(	GetUser.IMUserDOBDt, "mm/dd/yyyy"))>	<!--- Disp prefix --->
	<cfelse>
		<cfset LoadBldO("DispIMUserDOBDt",					GetUser.IMUserDOBDt)>					<!--- Disp prefix --->
	</cfif>
	<cfset LoadBldO("IMUserLast4DgtSSNNmb",					GetUser.IMUserLast4DgtSSNNmb)>
	<cfset LoadBldO("IMUserStr1Txt",						GetUser.IMUserStr1Txt)>
	<cfset LoadBldO("IMUserStr2Txt",						GetUser.IMUserStr2Txt)>
	<cfset LoadBldO("IMUserCtyNm",							GetUser.IMUserCtyNm)>
	<cfset LoadBldO("IMUserStCd",							GetUser.StCd)>
	<cfset LoadBldO("StCd",									GetUser.StCd)>
	<cfset LoadBldO("IMUserStNm",							GetUser.IMUserStNm)>
	<cfset LoadBldO("IMUserZip5Cd",							GetUser.Zip5Cd)>
	<cfset LoadBldO("Zip5Cd",								GetUser.Zip5Cd)>
	<cfset LoadBldO("IMUserZip4Cd",							GetUser.Zip4Cd)>
	<cfset LoadBldO("Zip4Cd",								GetUser.Zip4Cd)>
	<cfset LoadBldO("IMUserPostCd",							GetUser.IMUserPostCd)>
	<cfif Variables.EndUserData.IMUserId is 0>
		<cfset LoadOrig("IMCntryCd",						"US")>
	<cfelse>
		<cfset LoadBldO("IMCntryCd",						GetUser.IMCntryCd)>
	</cfif>
	<cfset LoadBldO("IMUserPhnCntryCd",						GetUser.IMUserPhnCntryCd)>
	<cfset LoadBldO("IMUserPhnAreaCd",						GetUser.IMUserPhnAreaCd)>
	<cfset LoadBldO("IMUserPhnLclNmb",						GetUser.IMUserPhnLclNmb)>
	<cfset LoadBldO("IMUserFaxCntryCd",						GetUser.IMUserFaxCntryCd)>
	<cfset LoadBldO("IMUserFaxAreaCd",						GetUser.IMUserFaxAreaCd)>
	<cfset LoadBldO("IMUserFaxLclNmb",						GetUser.IMUserFaxLclNmb)>

	<!--- Because we're not looping on ColumnList yet, we can rely on Variables.Origs.IMCntryCd and its default here. --->
	<cfif	(Variables.EndUserData.IMUserId					is not 0)
		and	(Variables.Origs.IMCntryCd						is "US")
		and	(Len		(GetUser.IMUserPhnLclNmb)			is 7)
		and	IsNumeric	(GetUser.IMUserPhnLclNmb)>
		<cfset LoadOrig("DispIMUserPhnLclNmb",				Left	(GetUser.IMUserPhnLclNmb, 3)
														&	"-"
														&	Right	(GetUser.IMUserPhnLclNmb, 4))><!--- Disp prefix --->
	<cfelse>
		<cfset LoadBldO("DispIMUserPhnLclNmb",				GetUser.IMUserPhnLclNmb)><!--- Disp prefix --->
	</cfif>
	<cfset LoadBldO("IMUserPhnExtnNmb",						GetUser.IMUserPhnExtnNmb)>
	<cfif	(Variables.EndUserData.IMUserId					is not 0)
		and	(Variables.Origs.IMCntryCd						is "US")
		and	(Len		(GetUser.IMUserFaxLclNmb)			is 7)
		and	IsNumeric	(GetUser.IMUserFaxLclNmb)>
		<cfset LoadOrig("DispIMUserFaxLclNmb",				Left	(GetUser.IMUserFaxLclNmb, 3)
														&	"-"
														&	Right	(GetUser.IMUserFaxLclNmb, 4))><!--- Disp prefix --->
	<cfelse>
		<cfset LoadBldO("DispIMUserFaxLclNmb",				GetUser.IMUserFaxLclNmb)><!--- Disp prefix --->
	</cfif>
	<cfset LoadBldO("IMUserEmailAdrTxt",					GetUser.IMUserEmailAdrTxt)>
	<cfset LoadBldO("IMUserEmailAdrTxt2",					GetUser.IMUserEmailAdrTxt)><!--- Not on database. --->
	<cfset LoadBldO("HQLocId",								GetUser.HQLocId)>
	<cfset LoadBldO("HQLocNm",								GetUser.HQLocNm)>
	<cfset LoadBldO("LocId",								GetUser.LocId)>
	<cfset LoadBldO("PrtId",								GetUser.PrtId)>
	<cfset LoadBldO("PrtLocNm",								GetUser.PrtLocNm)>
	<cfset LoadBldO("SBAOfcCd",								GetUser.SBAOfcCd)>
	<cfset LoadBldO("SBAOfc1Nm",							GetUser.SBAOfc1Nm)>
	<cfset LoadBldO("IMAsurncLvlCd",						GetUser.IMAsurncLvlCd)><!--- Could be overlaid by credential. --->
	<cfset LoadBldO("UserIMAsurncLvlCd",					GetUser.IMAsurncLvlCd)><!--- Always refers to user's level. --->
	<cfset LoadBldO("IMUserActvInactInd",					GetUser.IMUserActvInactInd)>
	<cfset LoadBldO("IMUserSuspRsn",						GetUser.IMUserSuspRsn)>
	<cfset LoadBldO("IMUserSuspRsnCd",						GetUser.IMUserSuspRsnCd)>
	<cfset LoadBldO("IMUserSuspRsnDt",						GetUser.IMUserSuspRsnDt)>
	<cfset LoadBldO("ImUserAcctNewInd",						GetUser.ImUserAcctNewInd)>
	<cfif IsDate(GetUser.IMUserSuspRsnDt)>
		<cfset LoadBldO("DispIMUserSuspRsnDt",	DateFormat(	GetUser.IMUserSuspRsnDt, "mm/dd/yyyy"))>
	<cfelse>
		<cfset LoadBldO("DispIMUserSuspRsnDt",				GetUser.IMUserSuspRsnDt)>
	</cfif>
    <cfset LoadBldO("IMUserCreatDt",						GetUser.IMUserCreatDt)>
	<cfif IsDate(GetUser.IMUserCreatDt)>
		<cfset LoadBldO("DispIMUserCreatDt",	DateFormat(	GetUser.IMUserCreatDt, "mm/dd/yyyy"))>
	<cfelse>
		<cfset LoadBldO("DispIMUserCreatDt",				GetUser.IMUserCreatDt)>
	</cfif>
	<cfif IsDate(GetUser.LastRecertDt)>
		<cfset LoadBldO("LastRecertDt",			DateFormat(	GetUser.LastRecertDt, "mm/dd/yyyy"))>
	<cfelse>
		<cfset LoadBldO("LastRecertDt",						GetUser.LastRecertDt)>
	</cfif>
	<cfset LoadBldO("IMUserSuprvId",				Trim(	GetUser.IMUserSuprvId))>
	<cfset LoadBldO("IMUserSuprvFirstNm",					GetUser.IMUserSuprvFirstNm)>
	<cfset LoadBldO("IMUserSuprvMidNm",						GetUser.IMUserSuprvMidNm)>
	<cfset LoadBldO("IMUserSuprvLastNm",					GetUser.IMUserSuprvLastNm)>
	<cfset LoadBldO("IMUserSuprvEmailAdrTxt",				GetUser.IMUserSuprvEmailAdrTxt)>
	<cfset LoadBldO("IMUserSuprvUserId",					GetUser.IMUserSuprvUserId)>
	<cfset LoadBldO("DeptId",								"")><!--- These 4 (GetUser.DeptId, etc) are defunct. --->
	<cfset LoadBldO("DeptNm",								"")>
	<cfset LoadBldO("AgncyId",								"")>
	<cfset LoadBldO("AgncyNm",								"")>
	<cfset LoadBldO("LINCInd",								GetUser.LINCInd)>
	<cfif GetUser.LINCInd is "Y">
		<cfset LoadBldO("DispLINCInd",						"Yes")>									<!--- Disp prefix --->
	<cfelse>
		<cfset LoadBldO("DispLINCInd",						"No")>									<!--- Disp prefix --->
	</cfif>

	<!--- IMUserBusTbl: --->

	<cfloop query="GetUBus">
		<cfset Variables.Idx							=	GetUBus.CurrentRow>
		<cfset LoadBldO("IMUserBusSeqNmb_#Idx#",			GetUBus.IMUserBusSeqNmb)>
		<cfset LoadBldO("IMUserBusEINSSNInd_#Idx#",			GetUBus.IMUserBusEINSSNInd)>
		<cfset LoadBldO("IMUserBusTaxId_#Idx#",				GetUBus.IMUserBusTaxId)>
		<cfset LoadBldO("DispEINSSN_#Idx#",					"???")>
		<cfset LoadBldO("DispTaxId_#Idx#",					GetUBus.IMUserBusTaxId)>
		<cfif Len(GetUBus.IMUserBusTaxId) is 9>
			<cfswitch expression="#GetUBus.IMUserBusEINSSNInd#">
			<cfcase value="E">
				<cfset LoadBldO("DispEINSSN_#Idx#",			"EIN")>
				<cfset LoadBldO("DispTaxId_#Idx#",			  Left	(GetUBus.IMUserBusTaxId, 2)
															& "-"
															& Right	(GetUBus.IMUserBusTaxId, 7))>
			</cfcase>
			<cfcase value="S">
				<cfset LoadBldO("DispEINSSN_#Idx#",			"SSN")>
				<cfset LoadBldO("DispTaxId_#Idx#",			  Left	(GetUBus.IMUserBusTaxId, 3)
															& "-"
															& Mid	(GetUBus.IMUserBusTaxId, 4, 2)
															& "-"
															& Right	(GetUBus.IMUserBusTaxId, 4))>
			</cfcase>
			</cfswitch>
		</cfif>
		<cfset LoadBldO("IMUserBusLocDUNSNmb_#Idx#",		GetUBus.IMUserBusLocDUNSNmb)>
		<cfset LoadBldO("BusEINCertInd_#Idx#",				GetUBus.BusEINCertInd)>
	</cfloop>
	<cfset LoadOrig("RecordCountUserBus",					GetUBus.RecordCount)><!--- LoadOrig, not LoadBldO --->

	<!--- IMSBACrdntlTbl: --->

	<cfset LoadBldO("IMSBACrdntlLognNm",					GetLogn.IMSBACrdntlLognNm)>
	<cfset LoadBldO("IMSBACrdntlPasswrd",					"")><!--- When given on page, it's always to enter a new one. --->
	<cfset LoadBldO("IMSBACrdntlPasswrd2",					"")><!--- Not on database. --->
	<cfset LoadBldO("IMSBACrdntlPasswrdExprDt",				GetLogn.IMSBACrdntlPasswrdExprDt)>
	<cfif IsDate(GetLogn.IMSBACrdntlPasswrdExprDt)>
		<cfset LoadBldO("DispIMSBACrdntlPasswrdExprDt",
												DateFormat(	GetLogn.IMSBACrdntlPasswrdExprDt, "mm/dd/yyyy"))>
	<cfelse>
		<cfset LoadBldO("DispIMSBACrdntlPasswrdExprDt",		GetLogn.IMSBACrdntlPasswrdExprDt)>
	</cfif>
	<cfset LoadBldO("IMSBACrdntlFirstLognInd",				GetLogn.IMSBACrdntlFirstLognInd)>
	<cfset LoadBldO("IMAsurncLvlCd",						GetLogn.IMAsurncLvlCd)>

	<!--- IMCrdntlTbl: --->

	<cfloop query="GetCred">
		<cfset Variables.Idx							=	GetCred.CurrentRow>
		<cfset LoadBldO("IMCrdntlSeqNmb_#Idx#",				GetCred.IMCrdntlSeqNmb)>
		<cfset LoadBldO("IMCrdntlServPrvdrId_#Idx#",		GetCred.IMCrdntlServPrvdrId)>
		<cfset LoadBldO("IMCrdntlServPrvdrNm_#Idx#",		GetCred.IMCrdntlServPrvdrNm)>
		<cfset LoadBldO("IMCrdntlUserId_#Idx#",				GetCred.IMCrdntlUserId)>
	</cfloop>
	<cfset LoadOrig("RecordCountCrdntl",					GetCred.RecordCount)><!--- LoadOrig, not LoadBldO --->

	<!--- JobTitlTbl: --->

	<cfset LoadBldO("JobTitlCd",							ValueList(GetTitl.JobTitlCd))>
	<cfset Variables.JobTitlCdPendAppr = "">
	<cfif getTitl.recordcount>
		<cfloop query="getTitl">
			<cfif getTitl.pdngrolerqst eq "Y">
				<cfset Variables.JobTitlCdPendAppr = listappend(Variables.JobTitlCdPendAppr,trim(getTitl.JobTitlCd)) >
			</cfif>
		</cfloop>
	</cfif>
	<cfset LoadBldO("JobTitlCdPendApprLst",					Variables.JobTitlCdPendAppr)><!--- JobTitlCdPendApprLst --->

	<!---
	The following fields do not exist on the database. They are only used to verify a request for JobTitlCd. Therefore,
	when we actually USE these fields, we always want them to be initiailized to the nullstring:
	--->
	<!--- Loan Borrower --->
	<cfset LoadBldO("DoSSN1",								"")><!--- Logical Group for next 3 fields --->
	<cfset LoadBldO(	"TaxId1",							left(GetUSSN.IMUserSSNNmb,3))><!--- view only display, masked SSN 3 --->
	<cfset LoadBldO(	"TaxId2",							mid(GetUSSN.IMUserSSNNmb,3,2))><!--- view only display, masked SSN 2 --->
	<cfset LoadBldO(	"TaxId3",							right(GetUSSN.IMUserSSNNmb,4))><!--- SSN 4 --->
	<cfset LoadBldO("DoSSN2",								"")><!--- Logical Group for next 3 fields --->
	<cfset LoadBldO(	"TaxId4",							"")><!--- Re-enter SSN 3 --->
	<cfset LoadBldO(	"TaxId5",							"")><!--- Re-enter SSN 2 --->
	<cfset LoadBldO(	"TaxId6",							"")><!--- Re-enter SSN 4 --->
	<cfset LoadBldO("LoanNmb",								GetUSSN.IMUserLoanNmb)><!--- LoanNmb --->
	<!--- Bond Holder --->
	<cfset LoadBldO("DUNS",									"")><!--- DUNS --->
	<cfset LoadBldO("DoEIN1",								"")><!--- Logical Group for next 2 fields --->
	<cfset LoadBldO(	"EIN1",								"")><!--- EIN 2 --->
	<cfset LoadBldO(	"EIN2",								"")><!--- EIN 7 --->
	<cfset LoadBldO("DoEIN2",								"")><!--- Logical Group for next 2 fields --->
	<cfset LoadBldO(	"EIN3",								"")><!--- Re-enter EIN 2 --->
	<cfset LoadBldO(	"EIN4",								"")><!--- Re-enter EIN 7 --->

	<!--- Array for communication between dsp page and act page. Won't be used if this is a ReadOnlyProfile. --->

	<cfset LoadOrig("ArrayColsOnDspPage",					ArrayNew(1))>
	<cfset LoadOrig("ResolvedDefaults",						StructNew())>

	<!--- Done loading from database/nullstring, but may still append to ArrayColsOnDspPage. See Defaults UDF. --->

	<cfif Variables.BldOrigs_SelfTestMode>
		<!--- In self-test mode, no sense in saving Variables.Origs to Session. Just a waste of memory. --->
	<cfelse>
		<!--- Don't StructAppend(Variables, Variables.Origs, "Yes") here. (LoadOrig defines all values in Variables too.) --->
		<cfset Variables.IMSBACrdntlPasswrdExprDt			= Variables.DispIMSBACrdntlPasswrdExprDt>
		<cfset Variables.IMUserDOBDt						= Variables.DispIMUserDOBDt>
		<cfset Variables.IMUserPhnLclNmb					= Variables.DispIMUserPhnLclNmb>
		<cfset Variables.IMUserFaxLclNmb					= Variables.DispIMUserFaxLclNmb>
		<cfset Variables.IMUserSuspRsnDt					= Variables.DispIMUserSuspRsnDt>
        <cfset Variables.IMUserCreatDt						= Variables.DispIMUserCreatDt>
		<cfset Variables.LINCInd							= Variables.DispLINCInd>
		<!--- We don't need to do this for GetUBus "Disp" variables because businesses refs them directly. --->
	</cfif>

</cfif><!--- /TxtErr --->

<cfset Variables.cfpra										= ""><!--- Very important!! (SPC files give cfpra priority.) --->

<!--- ************************************************************************************************************ --->

<!--- Possibly another database call: --->
<!--- <cfset Variables.ErrSeqNmb								= "0"> --->
<cfif NOT Variables.TxnErr><!--- Keep indention low. --->
	<cfif	(Variables.IMUserId								is 0)
		or	(ListFindNoCase("C,S,U", Variables.IMUserTypCd)	is 0)>
		<cfset LoadOrig("SuprvInd",							"A")><!--- Supervisor AOk, because IMUserTypCd doesn't need one. --->
	<cfelse>
		<!---  Variables.IMUserNm and Variables.IMUserTypCd set above. --->
		<cfset Variables.SuprvInd							= ""><!--- Output parameter of ValidateIMUserSuprvCSP --->
		<cfset Variables.cfprname							= "GetErrs">
		<cfset Variables.LogAct								= "validate supervisor">
		<cfinclude template="/cfincludes/oracle/security/spc_VALIDATEIMUSERSUPRVCSP.cfm">
		<cfif Variables.TxnErr>
			<cfset LoadOrig("SuprvInd",						"I")><!--- Assume it's bad. --->
		<cfelse>
			<cfset LoadOrig("SuprvInd",						Variables.SuprvInd)><!--- Let the stored procedure decide. --->
		</cfif>
	</cfif>
</cfif><!--- /TxtErr --->

<!--- ************************************************************************************************************ --->

<cfif Variables.BldOrigs_SelfTestMode>
	<!--- Display results when called in self-test mode: --->
	<cfif Variables.TxnErr>
		<cfoutput>
<p>
	#Variables.ErrMsg#
</p>
</cfoutput>
		<!---
		The entire Variables scope may seem like a bit much, but we always want as much info as possible in this situation.
		(Since TxnErr happened, we're debugging!) Also, no other scopes hold much interest in self-test mode.
		--->
		<cfdump var="#Variables#"				label="Variables"	expand="No">
	<cfelse>
		<cfif IsDefined("Variables.GetUser")>
			<cfdump var="#Variables.GetUser#"	label="GetUser"		expand="No">
		</cfif>
		<cfif IsDefined("Variables.GetUBus")>
			<cfdump var="#Variables.GetUBus#"	label="GetUBus"		expand="No">
		</cfif>
		<cfif IsDefined("Variables.GetLogn")>
			<cfdump var="#Variables.GetLogn#"	label="GetLogn"		expand="No">
		</cfif>
		<cfif IsDefined("Variables.GetCred")>
			<cfdump var="#Variables.GetCred#"	label="GetCred"		expand="No">
		</cfif>
		<cfif IsDefined("Variables.GetFedU")>
			<cfdump var="#Variables.GetFedU#"	label="GetFedU"		expand="No">
		</cfif>
		<cfif IsDefined("Variables.GetTitl")>
			<cfdump var="#Variables.GetTitl#"	label="GetTitl"		expand="No">
		</cfif>
		<cfif NOT IsDefined("Defaults")>
			<cfinclude template="bld_DefaultsUDF.cfm"><!--- For now, same directory as this file. --->
		</cfif>
		<cfoutput>
<style>
b.emphasized
	{
	background-color:	##ffc;
	color:				##900;
	text-decoration:	underline;
	}
</style>
<pre>
</cfoutput>
			<cfset Variables.IMUserFirstNm					= "Steve">
			<cfset Defaults("IMUserFirstNm", "FirstName", "First name", "mand", "")>
			<!--- Begin tests. --->
			<cfoutput>
Defaults("IMUserFirstNm", "FirstName", "First name", "mand", ""):

    Variables.Eng            = <b class="emphasized">#Variables.Eng#</b>
    Variables.Label          = <b class="emphasized">#Replace(Variables.Label, "<", "&lt;", "All")#</b>
    Variables.MandOpt        = <b class="emphasized">#Variables.MandOpt#</b>
    Variables.NameAndId      = <b class="emphasized">#Variables.NameAndId#</b>
    Variables.NameIdAndValue = <b class="emphasized">#Variables.NameIdAndValue#</b>
    Variables.VarName        = <b class="emphasized">#Variables.VarName#</b>
    Variables.VarValue       = <b class="emphasized">#Variables.VarValue#</b>

</cfoutput>
			<cfset Variables.IMUserMiddleNm					= "Middy">
			<cfset Defaults("IMUserMiddleNm", "MiddleName", "Middle name", "view", "")>
			<cfoutput>
Defaults("IMUserMiddleNm", "MiddleName", "Middle name", "view", ""):

    Variables.Eng            = <b class="emphasized">#Variables.Eng#</b>
    Variables.Label          = <b class="emphasized">#Replace(Variables.Label, "<", "&lt;", "All")#</b>
    Variables.MandOpt        = <b class="emphasized">#Variables.MandOpt#</b>
    Variables.NameAndId      = <b class="emphasized">#Variables.NameAndId#</b>
    Variables.NameIdAndValue = <b class="emphasized">#Variables.NameIdAndValue#</b>
    Variables.VarName        = <b class="emphasized">#Variables.VarName#</b>
    Variables.VarValue       = <b class="emphasized">#Variables.VarValue#</b>

</cfoutput>
			<cfset Variables.IMUserLastNm					= "Seaquist">
			<cfset Defaults("IMUserLastNm", "LastName", "Last name", "reqd", "")>
			<cfoutput>
Defaults("IMUserLastNm", "LastName", "Last name", "reqd", ""):

    Variables.Eng            = <b class="emphasized">#Variables.Eng#</b>
    Variables.Label          = <b class="emphasized">#Replace(Variables.Label, "<", "&lt;", "All")#</b>
    Variables.MandOpt        = <b class="emphasized">#Variables.MandOpt#</b>
    Variables.NameAndId      = <b class="emphasized">#Variables.NameAndId#</b>
    Variables.NameIdAndValue = <b class="emphasized">#Variables.NameIdAndValue#</b>
    Variables.VarName        = <b class="emphasized">#Variables.VarName#</b>
    Variables.VarValue       = <b class="emphasized">#Variables.VarValue#</b>

</cfoutput>
			<cfset Variables.ReadOnlyProfile				= "Yes">
			<cfset Variables.IMUserSuffixNm					= "(Dragonslayer)">
			<cfset Defaults("IMUserSuffixNm", "NameSuffix", "Name suffix", "opt", "")>
			<cfset Variables.ReadOnlyProfile				= "No">
			<cfoutput>
<b class="emphasized">(Note: ReadOnlyProfile temprarily set to "Yes".)</b>

Defaults("IMUserSuffixNm", "NameSuffix", "Name suffix", "opt", ""):

    Variables.Eng            = <b class="emphasized">#Variables.Eng#</b>
    Variables.Label          = <b class="emphasized">#Replace(Variables.Label, "<", "&lt;", "All")#</b>
    Variables.MandOpt        = <b class="emphasized">#Variables.MandOpt#</b>
    Variables.NameAndId      = <b class="emphasized">#Variables.NameAndId#</b>
    Variables.NameIdAndValue = <b class="emphasized">#Variables.NameIdAndValue#</b>
    Variables.VarName        = <b class="emphasized">#Variables.VarName#</b>
    Variables.VarValue       = <b class="emphasized">#Variables.VarValue#</b>

</cfoutput>
			<!--- End tests, so close out pre. --->
			<cfoutput>
</pre>
</cfoutput>
		<cfif IsDefined("Variables.Origs")>
			<cfdump var="#Variables.Origs#"		label="Origs"		expand="No">
		</cfif>
	</cfif><!--- /TxnErr --->
	<!--- Could only be looking at dev or test data in self-test mode, so it's okay not to turn off showdebugoutput. --->
	<cfabort>
</cfif><!--- /BldOrigs_SelfTestMode --->

<!--- Cleanup when called by cfinclude: --->

<cfif IsDefined("Variables.BldOrigs_hold_db")>
	<cfset Variables.db										= Variables.BldOrigs_hold_db>
</cfif>
<cfif IsDefined("Variables.BldOrigs_hold_password")>
	<cfset Variables.password								= Variables.BldOrigs_hold_password>
</cfif>
<cfif IsDefined("Variables.BldOrigs_hold_username")>
	<cfset Variables.username								= Variables.BldOrigs_hold_username>
</cfif>

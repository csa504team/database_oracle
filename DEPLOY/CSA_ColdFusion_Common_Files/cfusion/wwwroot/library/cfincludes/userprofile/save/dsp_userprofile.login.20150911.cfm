<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				Called by dsp_userprofile.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	Appended to the end of AppDataInline and JSInline. 
REVISION HISTORY:	09/11/2015, SRS:	Fixed ReadOnlyProfile (don't try to call active/inactive onchange if it doesn't exist). 
					07/30/2015, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfset Variables.AlternativeLoginNamesMax					= 3><!--- Number of alternative login names. --->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>Login Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0>
		<cfset Defaults("IMSBACrdntlLognNm", "UserID", "User ID", "mand", "")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlLognNm", "UserID", "User ID", "view", "")>
	</cfif>
	<cfset Variables.EngLogn								= Variables.Eng>	<!--- Needed later. --->
	<cfset Variables.MandOptLogn							= Variables.MandOpt><!--- Needed later. --->
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="inlineblock #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="text" #Variables.NameIdAndValue#
        size="15" maxlength="15" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'G', sMand, 8, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;<a href ="javascript:;" onclick="DisplaySBAUsernameRules();">SBA #Variables.Eng# Rules</a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0>
		<cfparam name="Variables.MandOptIMSBACrdntlPasswrdExprDt" default="skip">
	<cfelse>
		<cfparam name="Variables.MandOptIMSBACrdntlPasswrdExprDt" default="view">
	</cfif>
	<cfif Variables.MandOptIMSBACrdntlPasswrdExprDt is not "skip">
		<cfset Variables.MandOptIMSBACrdntlPasswrdExprDt	= "view">
	</cfif>
	<cfset Defaults("IMSBACrdntlPasswrdExprDt", "Expires", "Password Expires",	"view", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfset Variables.VarValue							= Variables.Origs.DispIMSBACrdntlPasswrdExprDt>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data">
        #Variables.VarValue#
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsAddCustomer>
		<cfset Defaults("IMSBACrdntlPasswrd", "NewPass1", "Password",			"mand",	"")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlPasswrd", "NewPass1", "Password",			"skip",	"")>
	</cfif>
	<cfif ListFindNoCase("view,skip", Variables.MandOpt) is 0><!--- Readonly display of Password makes no sense. --->
		<cfset Variables.VarValue							= "">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><!-- Always value="" -->
        <input type="Password" #Variables.NameAndId# value="" size="12" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 8, 9999))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;<a href ="javascript:;" onclick="DisplaySBAPasswordRules();">SBA #Variables.Eng# Rules</a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsAddCustomer>
		<cfset Defaults("IMSBACrdntlPasswrd2", "NewPass2", "Re-enter Password",	"mand",	"")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlPasswrd2", "NewPass2", "Re-enter Password",	"skip",	"")>
	</cfif>
	<cfif ListFindNoCase("view,skip", Variables.MandOpt) is 0><!--- Readonly display of Re-enter Password makes no sense. --->
		<cfset Variables.VarValue							= "">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><!-- Always value="" -->
        <input type="Password" #Variables.NameAndId# value="" size="12" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 8, 9999))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif ListFindNoCase("view,skip", Variables.MandOptLogn) is 0><!--- Readonly display of other avail logins makes no sense. --->
		<cfif	(IsDefined("URL.LoginNm1") and (Len(URL.LoginNm1) gt 0))
			or	(IsDefined("URL.LoginNm2") and (Len(URL.LoginNm2) gt 0))
			or	(IsDefined("URL.LoginNm3") and (Len(URL.LoginNm3) gt 0))>
			<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel">These User IDs are available:</div>
      <div class="tbl_cell formdata">
        <div class="inlineblock optdata"></cfoutput>
			<cfloop index="Idx" from="1" to="#Variables.AlternativeLoginNamesMax#">
				<cfif IsDefined("URL.LoginNm#Idx#") and (Len(URL["LoginNm#Idx#"]) gt 0)>
					<cfset Variables.LoginNmBtnNm				= URL["LoginNm#Idx#"]>
					<cfoutput>
        <label class="optlabel" title="Choose &quot;#Variables.LoginNmBtnNm#&quot; as the User ID.">
          <input type="Radio" name="LoginNmOption" value="#Variables.LoginNmBtnNm#" onclick="
          this.form.#Variables.EngLogn#.value				= this.value;
          "> #Variables.LoginNmBtnNm#
        </label><cfif Idx lt Variables.AlternativeLoginNamesMax><br/></cfif></cfoutput>
				</cfif>
			</cfloop>
			<cfoutput>
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
		</cfif>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"opt",	"ActInact")>
	<cfelse>
		<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"skip",	"ActInact")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view"><cfif Variables.VarValue is "A">
        Active<cfelse>
        Inactive</cfif><cfelse>
        <select #Variables.NameAndId# onchange="ShowSuspendAccountReason(this);">
        <option <cfif Variables.VarValue is "A">selected<cfelse>        </cfif> value="A">Active</option>
        <option <cfif Variables.VarValue is "I">selected<cfelse>        </cfif> value="I">Inactive</option>
        </select></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"mand",	"")>
	<cfelse>
		<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <!-- Using style="display:none;" because jQuery 3.0+ .show()/.hide() will break on display:none in a class: -->
    <div class="tbl_row" id="SuspendAccountReason" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="text" #Variables.NameIdAndValue#
        size="40" maxlength="255" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("IMUserSuspRsnCd", "ReasonCode", "Automatic Suspense Reason",	"opt",	"")>
	<cfelse>
		<cfset Defaults("IMUserSuspRsnCd", "ReasonCode", "Automatic Suspense Reason",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"></cfoutput>
		<cfif Variables.MandOpt is "view">
			<!---
			Query-of-Queries is the slowest thing that ColdFusion does. It essentially has to load an entire SQL parser, 
			parse the pseudo-SQL and emulate a DBMS. It could literally add seconds to use a Query-of-Queries for this. 
			This way, however, takes only milliseconds: 
			--->
			<cfset Variables.Description					= "(none)">
			<cfloop query="Variables.ActvIMUserSuspRsnTbl">
				<cfif Variables.ActvIMUserSuspRsnTbl.code	is Variables.VarValue>
					<cfset Variables.Description			= Variables.ActvIMUserSuspRsnTbl.description>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfoutput>
        #Variables.Description#</cfoutput>
		<cfelse>
			<cfoutput>
        <select #Variables.NameAndId#></cfoutput>
			<cfset Variables.DspOptsNotSelText				= "Not Suspended">
			<cfset Variables.DspOptsQueryName				= "Variables.ActvIMUserSuspRsnTbl">
			<cfset Variables.DspOptsSelList					= Variables.VarValue>
			<cfset Variables.DspOptsSkipList				= "">
			<cfset Variables.DspOptsTabs					= "#Request.SlafEOL#        ">
			<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
			<cfoutput>
        </select></cfoutput>
		</cfif><!--- /view --->
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip of IMUserSuspRsnCd --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("IMUserSuspRsnDt", "ReasonDate", "Automatic Suspense Date",	"view",	"")>
	<cfelse>
		<cfset Defaults("IMUserSuspRsnDt", "ReasonDate", "Automatic Suspense Date",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="10" maxlength="10" placeholder="mm/dd/yyyy" onchange="
        var sMand											= isMand(this);
        if  (EditDate('#JSStringFormat(Variables.Eng)#', this.value, sMand))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;
        <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
        href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
        <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
        alt="Pick a date using a popup calendar. (Opens a new window.)"
        title="Pick a date using a popup calendar. (Opens a new window.)">
        </a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip of IMUserSuspRsnDt (nested with IMUserSuspRsnCd's skip) --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /CLS Login --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#

<!-- Login-Related -->

<script>
<cfif Variables.MandOptActInact is not "skip">
$(function() // Shorthand for the document ready event. 
	{
	var	sElt												= document.getElementById("#Variables.VarNameActInact#");
	if	(sElt)// (Won't exist if readonly profile.) 
		sElt.onchange();
	});</cfif>

function DisplaySBAPasswordRules							()
	{
	alert("Your password must be complex and in compliance with SBA's password policy.\n"
		+ "A complex password must be a minimum of 8 characters long and contain \n"
		+ "at least 3 of the following 4 properties:\n"
		+ "  (1) Upper Case Letters A, B, C, ... Z\n"
		+ "  (2) Lower Case Letters a, b, c, ... z\n"
		+ "  (3) Numerals 0, 1, 2, ... 9\n"
		+ "  (4) Special Characters  { } [ ] < > : ? | \ ` ~ ! @ $ % ^ & * _ - + = .");
	return false;
	}

function DisplaySBAUsernameRules							()
	{
	alert("Your login User ID must be 8 to 15 characters long.\n"
		+ "No <, >,\',\", & or accented characters. \n");
	return false;
	}

function ShowSuspendAccountReason							(pThis)
	{
	var	sValue												= "A";
	if	(pThis.selectedIndex >= 0)
		sValue												= pThis.options[pThis.selectedIndex].value;
	if	(sValue === "I")
		$("##SuspendAccountReason").show();
	else
		$("##SuspendAccountReason").hide();
	}

</script>
</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

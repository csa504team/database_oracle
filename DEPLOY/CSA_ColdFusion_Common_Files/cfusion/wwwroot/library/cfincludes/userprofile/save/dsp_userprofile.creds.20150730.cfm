<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				Called by dsp_userprofile.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	Appended to the end of AppDataInline and JSInline. 
REVISION HISTORY:	07/30/2015, SRS:	Original implementation. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>Credential Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("UserIMAsurncLvlCd", "AssuranceLevel", "Assurance Level", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"></cfoutput>
		<cfif Variables.MandOpt is "view">
			<cfoutput>
        #Variables.VarValue#</cfoutput>
		<cfelse>
			<cfoutput>
        <select #Variables.NameAndId#></cfoutput>
			<cfset Variables.DspOptsNotSelText				= "Select Assurance Level">
			<cfset Variables.DspOptsQueryName				= "Variables.ActvIMAsurncLvlTbl">
			<cfset Variables.DspOptsSelList					= Variables.VarValue>
			<cfset Variables.DspOptsSkipList				= "">
			<cfset Variables.DspOptsTabs					= "#Request.SlafEOL#        ">
			<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
			<cfoutput>
        </select></cfoutput>
		</cfif>
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.Origs.RecordCountCrdntl					gt 0>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">Other Credentials</div>
      <div class="tbl_cell formdata nowrap">
        <div class="viewdata">
        <table border="1" cellpadding="2" summary="Other Credentials">
        <thead>
          <tr>
            <th>##</th>
            <th>CSP ID</th>
            <th>Credential Service Provider Name</th>
            <th>User Name</th>
          </tr>
        </thead>
        <tbody></cfoutput>
		<cfif NOT IsDefined("AlternatingRowClassName")>
			<cfinclude template="#Variables.LibUdfURL#/bld_SearchUDFs.cfm">
		</cfif>
		<cfloop index="Idx" from="1" to="#Variables.Origs.RecordCountCrdntl#">
			<cfoutput>
          <tr class="#AlternatingRowClassName(Idx)#">
            <td align="right">#Idx#</td>
            <td>#Variables.Origs["IMCrdntlServPrvdrId_"	& Idx]#</td>
            <td>#Variables.Origs["IMCrdntlServPrvdrNm_"	& Idx]#</td>
            <td>#Variables.Origs["IMCrdntlUserId_"		& Idx]#</td>
          </tr></cfoutput>
		</cfloop>
		<cfoutput>
        </tbody>
        </table>
        </div><!-- /viewdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /Variables.Origs.RecordCountCrdntl --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Credential Information --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

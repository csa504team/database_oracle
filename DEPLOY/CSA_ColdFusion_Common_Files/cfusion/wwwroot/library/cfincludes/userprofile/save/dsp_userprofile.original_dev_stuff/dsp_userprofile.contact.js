
// Begin dsp_userprofile.contact.js
//
// Created by Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 

gDomestic													= true;
gForeign													= false;
gRadioDomestic												= null;
gRadioForeign												= null;

function DoThisOnChangeCountry								(pThis, pPhoneElt)
	{
	var	sForm												= pThis.form;
	var	sOptions											= pThis.options;
	var	sOptionsLen											= sOptions.length;
	var	sSelIdx												= pThis.selectedIndex;
	var	sFIPSCountryCode									= ((sSelIdx < 1) ? "US" : sOptions[sSelIdx].value);
	LookupCountryPhoneCode($, sFIPSCountryCode, pPhoneElt);
	if	(!gRadioDomestic)
		gRadioDomestic										= document.getElementById("domestic");	// So we can click() them later. 
	if	(!gRadioForeign)
		gRadioForeign										= document.getElementById("foreign");	// So we can click() them later. 
	if	(sFIPSCountryCode === "US")
		{
		gDomestic											= true;	// Used in JavaScript. 
		gForeign											= false;// Used in JavaScript. 
		gRadioDomestic.click();										// Used in CSS selector. 
		$(".hilab.area").each(function(){this.innerHTML = "Area Code"});
		HiGrpMandatory(".area");
		HiGrpMandatory(".city");
		}
	else
		{
		gDomestic											= false;// Used in JavaScript. 
		gForeign											= true;	// Used in JavaScript. 
		gRadioForeign.click();										// Used in CSS selector. 
		$(".hilab.area").each(function(){this.innerHTML = "City Code"});
		HiGrpOptional(".area");
		HiGrpOptional(".city");
		}
	ShowHideStCd();
	// Add an AJAX callback here to set this.form.IMUserPhnCntryCd.value = IMCntryCdToIMCntryDialCd(pThis.value);
	// Or add it in this.onchange. 
	return true;
	}

// End dsp_userprofile.contact.js

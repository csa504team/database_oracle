
// Start of dsp_formdata_ldap.js			// (in case included inline)

// These routines are separate from dsp_formdata_ldap.cfm so that they can be included in the caller's JSInline. 
// Because they're in a separate *.js file, they can be cached by the browser, which speeds up page load times. That 
// was the main reason for separating out into a separate file. 
//
// Another advantage to having JS in a separate file: Wehen viewing this file in HomeSite or Dreamweaver, the code 
// will be color-coded (syntax-highlighted) according to JavaScript syntax, which also makes it more readable. 
//
// As of the /library/cfincludes/userprofile version, dsp_formdata_ldap.cfm loads this file itself, so that it can be 
// loaded only if needed. Therefore, the calling page should *NOT* include this file. The same goes for the .css file. 
// Part of the conversion to the library version is to eliminate dsp_formdata_ldap support scripts and CSS. 
//
//											Steve Seaquist
//											Trusted Mission Solutions, Inc.
//											09/07/2011,	Original implementation in /library/cfincludes/userprofile. 
//														Added MaybeSelectFirstOption, globals and $(document).ready(). 
//											04/19/2010,	Original implementation in /gls/cfincludes. 
//
// REVISION HISTORY:	04/19/2010, SRS:	Original implementation. 

function CopyToSpanAndHidden			(pForm, pName, pValue)
	{
	// Don't undergo the overhead of building a jQuery object just to do a simple getElementById: 
	var	sHTMLElt						= document.getElementById("Span"+pName);
	var	sFormElt						= pForm[pName];
	// This function SHOULD be called only when both elements exist. Make sure we find out when there's a coding error: 
	if		(sHTMLElt == null)			alert("INTERNAL ERROR. No HTML element found with id='Span"	+pName+"'.");
	else if	(sFormElt == null)			alert("INTERNAL ERROR. No form element found with name='"	+pName+"'.");
	else// Don't copy one unless you can copy both. (Keeps HTML element and form element in sync.) 
		{
		sHTMLElt.innerHTML				= pValue;
		sFormElt.value					= pValue;
		}
	}

function MaybeSelectFirstOption			(pEltDropdown)
	{
	if	(pEltDropdown.options.length	== 2)
		{
		pEltDropdown.selectedIndex		= 1;
		pEltDropdown.onchange();
		}
	}

var	gDivLookupSuprv						= null;
var	gDivLookupSuprvFamily				= null;
var	gDivLookupSuprvGiven				= null;
var	gDivLookupSuprvStatus				= null;// Currently a span, but it's just a container. Div in name doesn't matter. 
var	gDivLookupUser						= null;
var	gDivSbaEmpElts						= null;
var	gDivViewSuprvNames1					= null;
var	gDivViewSuprvNames2					= null;
var	gEltDropdownLookupSuprv				= null;
var	gEltDropdownLookupUser				= null;

$(document).ready(function()
	{
	gDivLookupSuprv						= document.getElementById('DivLookupSuprv');
	gDivLookupSuprvFamily				= document.getElementById('DivLookupSuprvFamily');
	gDivLookupSuprvGiven				= document.getElementById('DivLookupSuprvGiven');// May still be null if not in HTML. 
	gDivLookupSuprvStatus				= document.getElementById("DivLookupSuprvStatus");
	gDivLookupUser						= document.getElementById("DivLookupUser");
	gDivSbaEmpElts						= document.getElementById("DivSbaEmpElts");
	gDivViewSuprvNames1					= document.getElementById("DivViewSuprvNames1");
	gDivViewSuprvNames2					= document.getElementById("DivViewSuprvNames2");
	gEltDropdownLookupSuprv				= document.getElementById("EltDropdownLookupSuprv");
	gEltDropdownLookupUser				= document.getElementById("EltDropdownLookupUser");
	// Since we're splitting up tbls we need to make sure formlabel and formdata cells align. This may already have been 
	// done by the caller, so using the context (2nd argument) to make it as efficient as possible. 
	$(".tbl,.tbl_row", gDivSbaEmpElts)	.each(function(){ this.style.width = "100%"; });
	});

// End of dsp_formdata_ldap.js			// (in case included inline)


<!---
AUTHOR:				Ian Clark
DATE:				05/20/2015.
DESCRIPTION:		Used to display Linc SWF file to select / Display areas of interest for Linc Program
NOTES:
INPUT:
OUTPUT:
REVISION HISTORY:
					05/20/2015, IAC:   	Initial Implementation.
--->
 <div class="tbl_cell formlabel"> </div>
<div class="tbl_cell formdata">
	<cfif NOT isDefined("session.GetUserData")>
		<cflock scope="Session" type="ReadOnly" timeout="30">
			<cfset session.GetUserData =variables.GetUser>
		</cflock>
	</cfif>
		 <cfinclude template="/linc/areas/dsp_areas.cfm">

 </div><!-- /formdata -->
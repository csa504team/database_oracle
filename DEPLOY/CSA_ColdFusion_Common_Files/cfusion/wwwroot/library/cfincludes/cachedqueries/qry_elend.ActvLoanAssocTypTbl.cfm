<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc, for the US Small Business Administration. 
DATE:				09/23/2014. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/23/2014, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanAssocTypTbl"	datasource="#Variables.db#">
select		LoanAssocTypCd,
			LoanAssocDescTxt,
			LoanAssocTypCd								code,
			LoanAssocDescTxt							description<cfif Variables.Sybase>
from		loan..LoanAssocTypTbl<cfelse>
from		loan.LoanAssocTypTbl</cfif><!--- No where clause because all are active (at this time). --->
order by	LoanAssocTypOrdNmb
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

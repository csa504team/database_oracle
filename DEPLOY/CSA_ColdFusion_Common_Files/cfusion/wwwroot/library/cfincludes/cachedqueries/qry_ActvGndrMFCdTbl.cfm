<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvGndrMFCdTbl"			datasource="#Variables.db#">
select		GndrMFCd,
			GndrMFDescTxt,
			GndrCd,
			GndrMFCd									AS code,
			GndrMFDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..GndrMFCdTbl
where		(	datediff(dd,GndrMFStrtDt,				getdate()) >= 0)
and			(				GndrMFEndDt					is null
			or	datediff(dd,GndrMFEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.GndrMFCdTbl
where		(	(sysdate -	GndrMFStrtDt)				>= 0)
and			(				GndrMFEndDt					is null
			or	(sysdate -	GndrMFEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2016, SRS:	OPSMDEV-1085: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanCntlIntTypCdTbl"	datasource="#Variables.db#">
select		CntlIntTypCd,
			CntlIntTypTxt,
			CntlIntInd,
			CntlIntTypDispInd,
			CntlIntTypCd								AS code,
			CntlIntTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanCntlIntTypCdTbl
where		(	datediff(dd,CntlIntTypCdStrtDt,			getdate()) >= 0)
and			(				CntlIntTypCdEndDt			is null
			or	datediff(dd,CntlIntTypCdEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanCntlIntTypCdTbl
where		(	(sysdate -	CntlIntTypCdStrtDt)			>= 0)
and			(				CntlIntTypCdEndDt			is null
			or	(sysdate -	CntlIntTypCdEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

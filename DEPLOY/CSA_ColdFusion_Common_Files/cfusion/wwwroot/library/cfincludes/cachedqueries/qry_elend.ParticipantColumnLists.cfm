<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				06/23/2016. 
DESCRIPTION:		For the elend CacheQryCd only, builds lists column names for participants. 
NOTES:

	SBA_ETran XML processing will use these lists. In XML, participant elements (Assoc, Borr, Guar and Prin) 
	contain business or person columns, which may be shared and affect the same business and person rows. 
	These lists will be used to properly apply edits to shared business and person rows. By defining them in 
	Server Cached Queries, we assure that the addition of new columns are automatically supported. 

	We use simple assignment so as to copy just a structure reference. Unlike using StructCopy or Duplicate, 
	we **WANT** structure changes to propagate back to the original structure. 

	By the way, "ePCL" is a namespace prefix that's short for "elend.ParticipantColumnLists".

INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/03/2016, SRS:	Added virtual column "BusTaxId" to prin_and_busprin. 
					06/23/2016, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.ePCLQry"							datasource="#Variables.db#">
select		table_name,
			column_name
from		all_tab_cols
where		owner											in ('LOAN','LOANAPP')
and			table_name										in (<!--- LOAN schema: --->
																'BUSPRINTBL',
																'LOANASSOCTBL',
																'LOANGNTYBORRTBL',
																'LOANGNTYGUARTBL',
																'LOANGNTYPRINTBL',
																<!--- LOANAPP schema: --->
																'LOANBUSPRINTBL',
																'LOANBORRTBL',
																'LOANGUARTBL',
																'LOANPRINTBL')
and			virtual_column									= 'NO'
order by	table_name,
			column_name
</cfquery>

<cfset Variables.ePCLs										=	{
																"loan"			=
																	{
																	"assoc"		= "",
																	"borr"		= "",
																	"guar"		= "",
																	"prin"		= "",
																	"busprin"	= ""
																	},
																"loanapp"		=
																	{
																	"borr"		= "",
																	"guar"		= "",
																	"prin"		= "",
																	"busprin"	= ""
																	}
																}><!--- Structure of 2 structures of strings --->
<cfset Variables.ePCL1										= Variables.ePCLs.loan>		<!--- Assignment, so changes propagate. --->
<cfset Variables.ePCL2										= Variables.ePCLs.loanapp>	<!--- Assignment, so changes propagate. --->

<cfloop query="Variables.ePCLQry">
	<cfset Variables.ePCLTbl								=		Variables.ePCLQry.table_name>
	<cfset Variables.ePCLCol								= LCase(Variables.ePCLQry.column_name)>
	<cfswitch expression="#Variables.ePCLTbl#">
	<!--- LOAN schema: --->
	<cfcase value="BUSPRINTBL">		<cfset Variables.ePCL1.busprin	= ListAppend(Variables.ePCL1.busprin,	Variables.ePCLCol)></cfcase>
	<cfcase value="LOANASSOCTBL">	<cfset Variables.ePCL1.assoc	= ListAppend(Variables.ePCL1.assoc,		Variables.ePCLCol)></cfcase>
	<cfcase value="LOANGNTYBORRTBL"><cfset Variables.ePCL1.borr		= ListAppend(Variables.ePCL1.borr,		Variables.ePCLCol)></cfcase>
	<cfcase value="LOANGNTYGUARTBL"><cfset Variables.ePCL1.guar		= ListAppend(Variables.ePCL1.guar,		Variables.ePCLCol)></cfcase>
	<cfcase value="LOANGNTYPRINTBL"><cfset Variables.ePCL1.prin		= ListAppend(Variables.ePCL1.prin,		Variables.ePCLCol)></cfcase>
	<!--- LOANAPP schema: --->
	<cfcase value="LOANBUSPRINTBL">	<cfset Variables.ePCL2.busprin	= ListAppend(Variables.ePCL2.busprin,	Variables.ePCLCol)></cfcase>
	<cfcase value="LOANBORRTBL">	<cfset Variables.ePCL2.borr		= ListAppend(Variables.ePCL2.borr,		Variables.ePCLCol)></cfcase>
	<cfcase value="LOANGUARTBL">	<cfset Variables.ePCL2.guar		= ListAppend(Variables.ePCL2.guar,		Variables.ePCLCol)></cfcase>
	<cfcase value="LOANPRINTBL">	<cfset Variables.ePCL2.prin		= ListAppend(Variables.ePCL2.prin,		Variables.ePCLCol)></cfcase>
	</cfswitch>
</cfloop><!--- /ePCLQry --->
<cfset Variables.ePCL1["prin_and_busprin"]					= ListSort(
																ListRemoveDuplicates
																		(
																		Variables.ePCL1.prin
																	&	","
																	&	Variables.ePCL1.busprin
																	&	",bustaxid"
																		), "text")>
<cfset Variables.ePCL2["prin_and_busprin"]					= ListSort(
																	ListRemoveDuplicates
																		(
																		Variables.ePCL2.prin
																	&	","
																	&	Variables.ePCL2.busprin
																	&	",bustaxid"
																		), "text")>

<!--- Attach the whole structure, containing both, to the Scq structure: --->

<cfset Variables.Scq.elend.ParticipantColumnLists			= Variables.ePCLs>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

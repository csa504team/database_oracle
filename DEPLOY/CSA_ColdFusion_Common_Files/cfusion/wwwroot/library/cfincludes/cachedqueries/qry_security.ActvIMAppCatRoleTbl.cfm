<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.

	Although the usual size limit for a result set to be in Server Cached Queries is 100 row, and this result set 
	has 151 rows (at original implementation time), this result set is needed for every display of the new CLS 
	"home page". Our history with Choose Function suggest that the new home page will probably become our most 
	heavily hit page. So it's worth caching, in other words, even though it exceeds 100 rows. 

	Unlike most Server Cached Queries result sets, this one doesn't have "code" and "description" derived columns. 
	(Its usage is quite a bit more complex than building a dropdown with /library/cfincludes/dsp_options.cfm.) 

INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/02/2015, SRS:	We got rid of IMAppCatRoleOrdNmb and replaced it with IMAppCatTypOrdNmb. 
										Also got rid of the outer join to IMAppURLTbl. Handling that better via 
										the Roles structure (see /cls/cfincludes/bld_ArrayCategorizedHotlinks. 
					10/22/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<!---
#Variables.db# is getting "table or view does not exist" (without saying which one, of course). 
Until that's fixed, we have to use oracle_housekeeping: 
--->
<cfquery name="Variables.Scq.security.ActvIMAppCatRoleTbl"	datasource="oracle_housekeeping">
select		nvl(c.IMAppCatTypOrdNmb,	999)				IMAppCatTypOrdNmb,
			nvl(c.IMAppCatTypDescTxt,	'Uncategorized')	IMAppCatTypDescTxt,
			r.IMAppRoleNm
from		security.IMAppCatRoleTbl	cr
left join	security.IMAppCatTypTbl		c on	(	(cr.IMAppCatTypCd			= c.IMAppCatTypCd)
												and	(c.IMAppCatTypStrtDt		<= sysdate)
												and	(	(c.IMAppCatTypEndDt		is null)
													or	(c.IMAppCatTypEndDt		>= sysdate)
													)
												)
inner join	security.IMAppRoleTbl		r on	(	(cr.IMAppRoleId				= r.IMAppRoleId)
												and	(r.IMAppRoleStrtDt			<= sysdate)
												and	(	(r.IMAppRoleEndDt		is null)
													or	(r.IMAppRoleEndDt		>= sysdate)
													)
												)
where		(cr.IMAppCatRoleStrtDt		<= sysdate)
and			(	(cr.IMAppCatRoleEndDt	is null)
			or	(cr.IMAppCatRoleEndDt	>= sysdate)
			)
order by	c.IMAppCatTypOrdNmb
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

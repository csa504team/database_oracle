<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				04/06/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	04/06/2016, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCAUndrServMrktTbl"				datasource="#Variables.db#">
select		UndrServMrktCd,
			UndrServMrktDesc,
			UndrServMrktStrtDt,
			UndrServMrktEndDt,
			case UndrServMrktCd when 99 then 2 else 1 end as	NoneOfTheAboveComesLast,
			UndrServMrktCd										AS code,
			UndrServMrktDesc									AS description<cfif Variables.Sybase>
from		sbaref..CAUndrServMrktTbl<cfelse>
from		sbaref.CAUndrServMrktTbl</cfif>
order by	NoneOfTheAboveComesLast,
			description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

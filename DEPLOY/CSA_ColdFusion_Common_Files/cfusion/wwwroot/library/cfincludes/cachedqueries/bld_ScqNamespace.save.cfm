<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Saves the caller's variables that are not in the Variables.Scq namespace. Also initializes some \
					variables that ARE in the Variables.Scq namespace. Please keep in sync with the ".restore.cfm" file. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs. 
INPUT:				Maybe Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), etc. 
					Request.Slaf variables (from get_sbashared_variables). 
OUTPUT:				Variables.ScqXXXX variables. 
REVISION HISTORY:	05/27/2014, SRS:	Since this file can be executed by /library/ws/act_directorywatcher.cfc, it's subject to 
										a new problem with directory watchers, namely, that ColdFusion mappings are no longer 
										being respected. template="/xxx" doesn't work. template="/opt/iplanet/servers/docs/xxx" 
										doesn't work. The only thing that works is template="../xxx". What a nuisance! 
					12/14/2010, SRS:	Original implementation. 
--->

<cfif IsDefined("Variables.db")>
	<cfset Variables.ScqDB								= Variables.db>
</cfif>
<cfif IsDefined("Variables.dbtype")>
	<cfset Variables.ScqDBType							= Variables.dbtype>
</cfif>
<cfif IsDefined("Variables.Sybase")>
	<cfset Variables.ScqSybase							= Variables.Sybase>
</cfif>
<!--- Set the default datasource based on actual server name: --->
<cfswitch expression="#Request.SlafServerGroup#"><!--- No hole in the firewall for eweb1 servers: --->
<cfcase value="eweb1">	<cfset Variables.ScqDBDefault	= "oracle_webanalytic_publicread">	</cfcase>
<cfdefaultcase>			<cfset Variables.ScqDBDefault	= "oracle_transaction_publicread">	</cfdefaultcase>
</cfswitch>
<cfset Variables.db										= Variables.ScqDBDefault>
<cfset Variables.dbtype									= "Oracle80">
<cfset Variables.ScqCoopTesting							= "No">
<cfif FileExists(ExpandPath("../../../coop.txt"))><!--- This file is in /library/cfincludes/cachedqueries, so doc root level. --->
	<!--- Existence of coop.txt at doc root is how Chuda wanted to indicate whether or not we're on the COOP machine. --->
	<cfset Variables.ScqCoopTesting						= "Yes">
</cfif>
<cfif Variables.ScqCoopTesting>
	<cfset Variables.db									= "sbaref">
	<cfset Variables.ScqDBDefault						= "sbaref">
	<cfset Variables.dbtype								= "Sybase11">
</cfif>

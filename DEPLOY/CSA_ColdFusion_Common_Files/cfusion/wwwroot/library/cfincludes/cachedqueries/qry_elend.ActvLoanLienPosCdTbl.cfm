<!---
AUTHOR:				Steve Seaquist, Alpha Omega Integration, Inc, for the US Small Business Administration. 
DATE:				06/29/2017. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/29/2017, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanLienPosCdTbl"	datasource="#Variables.db#">
select		LoanLienPosCd,
			LoanLienPosDescTxt,
			LoanLienPosCd									code,
			LoanLienPosDescTxt								description<cfif Variables.Sybase>
from		loanapp..LoanLienPosTbl
where		(	datediff(dd,LoanLienPosCdStrtDt,			getdate()) >= 0)
and			(				LoanLienPosCdEndDt				is null
			or	datediff(dd,LoanLienPosCdEndDt,				getdate()) <= 0)<cfelse>
from		loanapp.LoanLienPosCdTbl
where		(	(sysdate -	LoanLienPosCdStrtDt)			>= 0)
and			(				LoanLienPosCdEndDt				is null
			or	(sysdate -	LoanLienPosCdEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl"	datasource="#Variables.db#">
select		IMFedAgncySeqNmb,
			IMFedAgncyNm,
			IMFedAgncySeqNmb							code,
			IMFedAgncyNm								description<cfif Variables.Sybase>
from		technet..IMFedAgncyTbl<cfelse>
from		technet.IMFedAgncyTbl</cfif>
where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
and			IMFedAgncyActvInactvInd						 = 0
order by	IMFedAgncyNm
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

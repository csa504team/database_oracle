<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/28/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/28/2011, SRv:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.LqdJustfctnCdTbl" datasource="#Variables.db#">
select		LqdJustfctnCd,
	    	LqdJustfctnDescTxt,
			LqdJustfctnCd						as code,							
	    	LqdJustfctnDescTxt 					as description<cfif Variables.Sybase>
from		sbaref..LqdJustfctnCdTbl
where		(	datediff(dd,LqdJustfctnStrtDt,	getdate()) >= 0)
and			(				LqdJustfctnEndDt	is null
			or	datediff(dd,LqdJustfctnEndDt,	getdate()) <= 0)<cfelse>
from		sbaref.LqdJustfctnCdTbl
where		(	(sysdate -	LqdJustfctnStrtDt)	>= 0)
and			(				LqdJustfctnEndDt	is null
			or	(sysdate -	LqdJustfctnEndDt)	<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

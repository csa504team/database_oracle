<!---
AUTHOR:				Robert Doyle, Solutions By Design, Inc., for the US Small Business Administration. 
DATE:				11/04/2013
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/04/2013, RSD:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCalndrPrdPymtTbl"			datasource="#Variables.db#">
select	CalndrPrdPymtCd,
		CalndrPrdPymtDesc,
		CalndrPrdPymtOrd,
		CalndrPrdPymtCd										AS code,
		CalndrPrdPymtDesc									AS description
from		sbaref.CalndrPrdPymtTbl
where		(	(sysdate -	CalndrPrdPymtStrtDt)			>= 0)
and			(				CalndrPrdPymtEndDt				is null
			or	(sysdate -	CalndrPrdPymtEndDt)				<= 0)
order by	CalndrPrdPymtOrd
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
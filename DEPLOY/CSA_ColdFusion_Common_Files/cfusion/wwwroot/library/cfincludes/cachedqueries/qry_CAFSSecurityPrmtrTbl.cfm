<!---
AUTHOR:				Vidya Rajan
DATE:				03/01/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	03/01/2016, VR:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.CAFSSecurityPrmtrTbl" datasource="#Variables.db#">
SELECT a.IMPrmtrId,
       b.IMPrmtrValTxt,
       a.IMPrmtrSeqNmb,
       a.IMPrmtrId as code,
       a.IMPrmtrDescTxt as description,
       b.IMPrmtrValEndDt
 FROM security.IMPrmtrTbl a, security.IMPrmtrValTbl b
 WHERE a.IMPrmtrId IN ('CLSCNTCTNMB')
       AND a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
       AND IMPrmtrValStrtDt <= SYSDATE
       AND (IMPrmtrValEndDt IS NULL OR SYSDATE < IMPrmtrValEndDt + 1)                                         
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

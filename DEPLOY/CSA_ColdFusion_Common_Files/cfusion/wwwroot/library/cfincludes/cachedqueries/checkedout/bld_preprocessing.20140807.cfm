<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/10/2010
DESCRIPTION:		Preprocessing for qry_((ScqQueryName)).cfm. Sets up qry_((ScqQueryName)).cfm environment. 
NOTES:				Manually called at the beginning of each every qry_((ScqQueryName)).cfm file. 
					Uses StructName[keyname] syntax to define StructName.keyname in mixed case (eases reading dumps). 
INPUT:				Variables.ScqQueryName. Probably also Variables.ScqSubstructureName. 
OUTPUT:				Variables.db, Variables.dbtype, Variables.Sybase. 
REVISION HISTORY:	08/07/2014, SRS:	As part of removing dbtype attribute for CF11 compatibility (in the qry_ files), 
										defined Variables.LogURL, in case not already defined, as "..". This allows us to 
										rename Library's top-level directory and the qry_ files will all still work. 
					12/10/2010, SRS:	Original implementation. 
--->

<cfparam name="Variables.LogURL"											default=".."><!--- See Revision History. --->
<cfif NOT IsDefined("Variables.Scq")>
	<cfset Variables["Scq"]													= StructNew()>
</cfif>
<cfif ListLen(Variables.ScqQueryName, ".") gt 1>
	<cfif NOT IsDefined("Variables.Scq.#ListGetAt(Variables.ScqQueryName,1,'.')#")>
		<cfset Variables.Scq[ListGetAt(Variables.ScqQueryName, 1, ".")]		= StructNew()>
	</cfif>
	<cfset Variables.Scq[ListGetAt(Variables.ScqQueryName, 1, ".")]
						[ListGetAt(Variables.ScqQueryName, 2, ".")]			= ""><!--- Define name for query object. --->
<cfelseif ListLen(Variables.ScqQueryName, ".") is 1>
	<cfset Variables.Scq[Variables.ScqQueryName]							= ""><!--- Define name for query object. --->
</cfif>

<!--- The following code makes sure that db, etc, is ALWAYS correct for the current substructure. --->

<cfif NOT IsDefined("Variables.ScqSubstructureName")>
	<cfif ListLen(Variables.ScqQueryName, ".") gt 1>
		<cfset Variables.ScqSubstructureName			= ListGetAt(Variables.ScqQueryName, 1, ".")>
	<cfelse>
		<cfset Variables.ScqSubstructureName			= "">
	</cfif>
</cfif>
<cfif Variables.ScqCoopTesting><!--- See bld_ScqNamespace.save.cfm. --->
	<cfswitch expression="#Variables.ScqSubstructureName#">
	<cfcase value="partner">	<cfset Variables.db		= "partner_scheduled">	</cfcase>
	<cfdefaultcase>				<cfset Variables.db		= "sbaref">				</cfdefaultcase>
	</cfswitch>
	<cfset Variables.dbtype								= "Sybase11">
	<cfset Variables.Sybase								= "Yes">
<cfelse>
	<cfswitch expression="#Variables.ScqSubstructureName#">
	<cfcase value="partner">	<cfset Variables.db		= Variables.ScqDBDefault>				</cfcase>
	<cfcase value="pronet">		<cfset Variables.db		= "oracle_webanalytic_publicread">		</cfcase>
	<cfcase value="technet">	<cfset Variables.db		= "oracle_webtransaction_publicread">	</cfcase>
	<cfdefaultcase>				<cfset Variables.db		= Variables.ScqDBDefault>				</cfdefaultcase>
	</cfswitch>
	<cfset Variables.dbtype								= "Oracle80">
	<cfset Variables.Sybase								= "No">
</cfif>

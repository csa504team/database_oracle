<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/13/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/01/2016, SRS:	OPSMDEV-SMM: Reversed meaning of code and description. 
					06/13/2016, SRS:	OPSMDEV-861, 862: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvVndrSwCdTbl"			datasource="#Variables.db#">
select		distinct
			VndrNm,
			VndrSwNm,
			VndrContact,
			VndrTypInd,
			VndrSwNm									as code,
			VndrNm										as description<cfif Variables.Sybase>
from		sbaref..VndrSwCdTbl
where		(	datediff(dd,VndrSwStrtDt,				getdate()) >= 0)
and			(				VndrSwEndDt					is null
			or	datediff(dd,VndrSwEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.VndrSwCdTbl
where		(	(sysdate -	VndrSwStrtDt)				>= 0)
and			(				VndrSwEndDt					is null
			or	(sysdate -	VndrSwEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

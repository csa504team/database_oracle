<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				01/22/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	01/28/2015, SRS:	Got rid of LogUDF, because enile and eweb don't define it. Added to the 
										elend cached query group for the same reason. 
					01/22/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanIssCdTbl"	datasource="#Variables.db#">
select		LoanIssCd,
			LoanIssDescTxt,
			SysCd,
			LoanIssCd									AS code,
			LoanIssDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanIssCdTbl
where		(	datediff(dd,LoanIssStrtDt,				getdate()) >= 0)
and			(				LoanIssEndDt				is null
			or	datediff(dd,LoanIssEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanIssCdTbl
where		(	(sysdate -	LoanIssStrtDt)				>= 0)
and			(				LoanIssEndDt				is null
			or	(sysdate -	LoanIssEndDt)				<= 0)</cfif>
order by	SysCd,
			LoanIssCd
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

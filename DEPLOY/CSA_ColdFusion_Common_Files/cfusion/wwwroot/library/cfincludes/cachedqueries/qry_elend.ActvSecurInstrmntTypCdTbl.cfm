<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				11/22/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/22/2016, SRS:	OPSMDEV-1085: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvSecurInstrmntTypCdTbl"	datasource="#Variables.db#">
select		SecurInstrmntTypCd,
			SecurInstrmntTypDescTxt,
			SecurInstrmntTypCd							code,
			SecurInstrmntTypDescTxt						description<cfif Variables.Sybase>
from		loanapp.SecurInstrmntTypCdTbl
where		(	datediff(dd,SecurInstrmntStrtDt,		getdate()) >= 0)
and			(				SecurInstrmntEndDt			is null
			or	datediff(dd,SecurInstrmntEndDt,			getdate()) <= 0)<cfelse>
from		loanapp.SecurInstrmntTypCdTbl
where		(	(sysdate -	SecurInstrmntStrtDt)		>= 0)
and			(				SecurInstrmntEndDt			is null
			or	(sysdate -	SecurInstrmntEndDt)			<= 0)</cfif>
order by	upper(description)
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

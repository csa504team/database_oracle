<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/10/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2015, SRS:	Original implementation. First query for CacheQryCd "security". 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMAppURLTbl"	datasource="#Variables.db#">
select		IMAppURLSeqNmb,
			IMAppURLAdrTxt,
			IMAppURLSrvrAdrTxt,
			IMAppURLDsplyNm,
	<!---	IMAppURLDsplyPrtyNmb, ((no data yet)) --->
			IMAppURLStrtDt,
			IMAppURLEndDt,
			IMAppURLDsplyNm								AS displayorder,
			IMAppURLSeqNmb								AS code,
			IMAppURLDsplyNm								AS description<cfif Variables.Sybase>
from		security..IMAppURLTbl
where		(	datediff(dd,IMAppURLStrtDt,				getdate()) >= 0)
and			(				IMAppURLEndDt				is null
			or	datediff(dd,IMAppURLEndDt,				getdate()) <= 0)<cfelse>
from		security.IMAppURLTbl
where		(	(sysdate -	IMAppURLStrtDt)				>= 0)
and			(				IMAppURLEndDt				is null
			or	(sysdate -	IMAppURLEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ThisSrvrURLTbl"			datasource="#Variables.db#">
select		distinct
			rtrim(EnvCd)								as EnvCd,
			SrvrLocCd,
			SrvrURLCd,
			SrvrURLDescTxt,<cfif Variables.Sybase>
			rtrim(EnvCd)+'.'+convert(varchar,SrvrLocCd)	as displayorder,<cfelse>
			rtrim(EnvCd)||'.'||SrvrLocCd				as displayorder,</cfif>
			SrvrURLCd									as code,
			SrvrURLDescTxt								as description<cfif Variables.Sybase>
from		sbaref..SrvrURLTbl
where		(	datediff(dd,SrvrURLStrtDt,getdate())	>= 0)
and			(				SrvrURLEndDt				is null
			or	datediff(dd,SrvrURLEndDt, getdate())	<= 0)<cfelse>
from		sbaref.SrvrURLTbl
where		(	(sysdate -	SrvrURLStrtDt)				>= 0)
and			(				SrvrURLEndDt				is null
			or	(sysdate -	SrvrURLEndDt)				<= 0)</cfif>
/* The cfquery name, this line and the following line are the only differences compared to bld_ActvSrvrURLTbl.cfm: */
and			EnvCd										= '#Request.SlafServerEnvironment#'
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				KATHA PARIKH, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/07/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/17/2016, KP:	changed the LOAN_GNTY_MAT_ST_CREAT_USER_ID to LOAN_GNTY_MATSTCREATUSERID
					12/07/2016, KP:	Original implementation..  
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllLoanMatStIndCdTbl"		datasource="#Variables.db#">
SELECT
					LOAN_GNTY_MAT_ST_IND,
					LOAN_GNTY_MAT_ST_DESC_TXT				AS description,
					LOAN_GNTY_MAT_ST_STRT_DT,
					LOAN_GNTY_MAT_ST_END_DT,
					LOAN_GNTY_MATSTCREATUSERID,
					LOAN_GNTY_MAT_ST_CREAT_DT
			FROM SBAREF.LOANMATSTINDCDTBL  
		order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

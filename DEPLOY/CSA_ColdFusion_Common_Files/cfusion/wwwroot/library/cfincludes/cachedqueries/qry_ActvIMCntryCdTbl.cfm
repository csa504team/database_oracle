<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="#Variables.db#">
select		IMCntryCd,
			IMCntryNm,
			IMCntryDialCd,
			IMCntryCd									AS code,
			IMCntryNm									AS description<cfif Variables.Sybase>
from		sbaref..IMCntryCdTbl
where		(	datediff(dd,IMCntryStrtDt,				getdate()) >= 0)
and			(				IMCntryEndDt				is null
			or	datediff(dd,IMCntryEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.IMCntryCdTbl
where		(	(sysdate -	IMCntryStrtDt)				>= 0)
and			(				IMCntryEndDt				is null
			or	(sysdate -	IMCntryEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>

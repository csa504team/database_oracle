<!---
AUTHOR:				Chitralekha Pustakala, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/26/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/26/2016, PCL:	(OPSMDEV-1212)Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.AllLoanStirtPgmOptCdTbl" datasource="#Variables.db#">
select		LoanStirtPgmOptCd,
		    LoanStirtPgmOptDescTxt,
     		LoanStirtPgmOptStrtDt,
		    LoanStirtPgmOptEndDt,
		    LoanDtlCreatDt,
		    LoanDtlCreatUserId,
            LoanDtLLastUpdtDt,
            LoanDtlLastUpdtUserId,
			LoanStirtPgmOptCd			as code,
		    LoanStirtPgmOptDescTxt	as description<cfif Variables.Sybase>
from		sbaref..LoanStirtPgmOptCdTbl<cfelse>
from		sbaref.LoanStirtPgmOptCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

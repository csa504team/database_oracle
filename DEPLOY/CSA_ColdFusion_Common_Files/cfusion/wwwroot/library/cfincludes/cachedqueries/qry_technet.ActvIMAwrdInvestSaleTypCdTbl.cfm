<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.technet.ActvIMAwrdInvestSaleTypCdTbl"	datasource="#Variables.db#">
select		IMAwrdInvestSaleTypCd,
			IMAwrdInvestSaleSourcCd,
			IMAwrdInvestSaleDescTxt<cfif Variables.Sybase>
from		technet..IMAwrdInvestSaleTypCdTbl
where		(				IMAwrdInvestSaleStrtDt		is not null)
and			(	datediff(dd,IMAwrdInvestSaleStrtDt,		getdate()) >= 0)
and			(				IMAwrdInvestSaleEndDt		is null
			or	datediff(dd,IMAwrdInvestSaleEndDt,		getdate()) <= 0)<cfelse>
from		technet.IMAwrdInvestSaleTypCdTbl
where		(				IMAwrdInvestSaleStrtDt		is not null)
and			(	(sysdate -	IMAwrdInvestSaleStrtDt)		>= 0)
and			(				IMAwrdInvestSaleEndDt		is null
			or	(sysdate -	IMAwrdInvestSaleEndDt)		<= 0)</cfif>
order by	DsplyOrd
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

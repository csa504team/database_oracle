<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/22/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/22/2016, SRS:	OPSMDEV-1085: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLeasTypCdTbl"				datasource="#Variables.db#">
select		LeasTypCd,
			LeasTypDescTxt,
			LeasTypCd										as code,
			LeasTypDescTxt									as description<cfif Variables.Sybase>
from		sbaref..LeasTypCdTbl
where		(	datediff(dd,LeasTypStrtDt,getdate())		>= 0)
and			(				LeasTypEndDt					is null
			or	datediff(dd,LeasTypEndDt, getdate())		<= 0)<cfelse>
from		sbaref.LeasTypCdTbl
where		(	(sysdate -	LeasTypStrtDt)					>= 0)
and			(				LeasTypEndDt					is null
			or	(sysdate -	LeasTypEndDt)					<= 0)</cfif>
order by	upper(LeasTypDescTxt)
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

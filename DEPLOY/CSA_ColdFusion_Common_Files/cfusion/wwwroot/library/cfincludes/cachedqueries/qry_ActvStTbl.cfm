<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvStTbl"					datasource="#Variables.db#">
select		StCd,
			StNm,
			StCd										AS code,
			StNm										AS description<cfif Variables.Sybase>
from		sbaref..StTbl
where		(	datediff(dd,StStrtDt,					getdate()) >= 0)
and			(				StEndDt						is null
			or	datediff(dd,StEndDt,					getdate()) <= 0)<cfelse>
from		sbaref.StTbl
where		(	(sysdate -	StStrtDt)					>= 0)
and			(				StEndDt						is null
			or	(sysdate -	StEndDt)					<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

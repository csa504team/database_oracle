<!---
AUTHOR:				Nirish Namilae 
DATE:				08/23/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					08/23/2011, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanTaskTypCdTbl"			datasource="#Variables.db#">
select		LoanTaskTypCd,
			LoanTaskTypDescTxt,
			LoanTaskTypCd							AS code,
			LoanTaskTypDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..LoanTaskTypCdTbl<cfelse><!--- else = default = Oracle: --->
from		sbaref.LoanTaskTypCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

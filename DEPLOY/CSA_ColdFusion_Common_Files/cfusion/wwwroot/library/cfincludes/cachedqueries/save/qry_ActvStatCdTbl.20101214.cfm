<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvStatCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		StatCd,
			StatTxt,
			StatCdDispInd,
			StatCdDispOrdNmb							AS displayorder,
			StatCd										AS code,
			StatTxt										AS description<cfif Variables.Sybase>
from		sbaref..StatCdTbl
where		(	datediff(dd,StatCdStrtDt,				getdate()) >= 0)
and			(				StatCdEndDt					is null
			or	datediff(dd,StatCdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.StatCdTbl
where		(	(sysdate -	StatCdStrtDt)				>= 0)
and			(				StatCdEndDt					is null
			or	(sysdate -	StatCdEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

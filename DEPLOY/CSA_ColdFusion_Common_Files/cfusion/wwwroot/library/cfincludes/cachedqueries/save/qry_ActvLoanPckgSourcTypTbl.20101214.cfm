<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanPckgSourcTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanPckgSourcTypCd,
			LoanPckgSourcTypDescTxt,
			LoanPckgSourcTypCd							AS code,
			LoanPckgSourcTypDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..LoanPckgSourcTypTbl
where		(	datediff(dd,LoanPckgSourcTypStrtDt,		getdate()) >= 0)
and			(				LoanPckgSourcTypEndDt		is null
			or	datediff(dd,LoanPckgSourcTypEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.LoanPckgSourcTypTbl
where		(	(sysdate -	LoanPckgSourcTypStrtDt)		>= 0)
and			(				LoanPckgSourcTypEndDt		is null
			or	(sysdate -	LoanPckgSourcTypEndDt)		<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

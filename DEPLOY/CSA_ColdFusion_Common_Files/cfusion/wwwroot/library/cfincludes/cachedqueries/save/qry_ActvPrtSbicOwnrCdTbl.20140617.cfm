<!---
AUTHOR:				Elena Spratling. 
DATE:				06/10/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPrtSbicOwnrCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">

select		PRTSBICOWNRCD			AS code,
			PRTSBICOWNRDESC		AS description
<cfif Variables.Sybase>
from		sbaref..PRTSBICOWNRCDTBL
where		(	datediff(dd,PRTSBICOWNRCDSTRTDT,		getdate()) >= 0)
and			(				PRTSBICOWNRCDENDDT				is null
			or	datediff(dd,PRTSBICOWNRCDENDDT,	getdate()) <= 0)<cfelse>
from		sbaref.PRTSBICOWNRCDTBL
where		(	(sysdate -	PRTSBICOWNRCDSTRTDT)			>= 0)
and			(				PRTSBICOWNRCDENDDT				is null
			or	(sysdate -	PRTSBICOWNRCDENDDT)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">



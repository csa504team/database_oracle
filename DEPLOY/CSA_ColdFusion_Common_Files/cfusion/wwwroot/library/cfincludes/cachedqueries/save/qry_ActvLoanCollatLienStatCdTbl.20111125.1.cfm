<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvLoanCollatLienStatCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvLoanCollatLienStatCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  select
      	LOANCOLLATLIENSTATCD,
	    LOANCOLLATLIENSTATDESCTXT, 
	    LOANCOLLATLIENSTATCD								AS code,
		LOANCOLLATLIENSTATDESCTXT							AS description <cfif Variables.Sybase>	  
	   from	sbaref..LOANCOLLATLIENSTATCDTbl
	   where (datediff(dd,LOANCOLLATLIENSTATSTRTDT,	getdate()) >= 0)
		   and	(LOANCOLLATLIENSTATENDDT		is null
		   or datediff(dd,LOANCOLLATLIENSTATENDDT,	getdate()) <= 0)<cfelse>
	   from	sbaref.LOANCOLLATLIENSTATCDTbl
       where ((sysdate -	LOANCOLLATLIENSTATSTRTDT)	>= 0)
       		and	(LOANCOLLATLIENSTATENDDT	is null
			or	(sysdate -	LOANCOLLATLIENSTATENDDT)	<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvCollatSalePrcTypCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvCollatSalePrcTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	CollatSalePrcTypCd,
	    CollatSalePrcTypCdDescTxt,
	    CollatSalePrcTypCd									AS code,
		CollatSalePrcTypCdDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..CollatSalePrcTypCdTbl
	   where (datediff(dd,CollatSalePrcTypCdStrtDt,			getdate()) >= 0)
		   and	(CollatSalePrcTypCdEndDt					is null
		   or datediff(dd,CollatSalePrcTypCdEndDt,			getdate()) <= 0)<cfelse>
	   from	sbaref.CollatSalePrcTypCdTbl
       where ((sysdate -	CollatSalePrcTypCdStrtDt)		>= 0)
       		and	(CollatSalePrcTypCdEndDt					is null
			or (sysdate -	CollatSalePrcTypCdEndDt)		<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

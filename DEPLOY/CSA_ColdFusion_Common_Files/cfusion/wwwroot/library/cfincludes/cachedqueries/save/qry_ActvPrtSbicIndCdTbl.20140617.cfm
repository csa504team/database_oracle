<!---
AUTHOR:				Elena Spratling. 
DATE:				06/10/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPrtSbicIndCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">

select		PRTSBICINDCD			AS code,
			PRTSBICINDDESC		    AS description 
<cfif Variables.Sybase>
from		sbaref..PRTSBICINDCDTBL
where		(	datediff(dd,PRTSBICINDCDSTRTDT,		getdate()) >= 0)
and			(				PRTSBICINDCDENDDT				is null
			or	datediff(dd,PRTSBICINDCDENDDT,	getdate()) <= 0)<cfelse>
from		sbaref.PRTSBICINDCDTBL
where		(	(sysdate -	PRTSBICINDCDSTRTDT)			>= 0)
and			(				PRTSBICINDCDENDDT				is null
			or	(sysdate -	PRTSBICINDCDENDDT)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">




<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurGainLossCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CollatPurGainLossCd,
			CollatPurGainLossDescTxt,
			CollatPurGainLossCd										AS code,
			CollatPurGainLossDesc									AS description <cfif Variables.Sybase>
from		sbaref..CollatPurGainLossCdTbl
where		(	datediff(dd,CollatPurGainLossStrtDt,				getdate()) >= 0)
and			(				CollatPurGainLossEndDt					is null
			or	datediff(dd,CollatPurGainLossEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CollatPurGainLossCdTbl
where		(	(sysdate -	CollatPurGainLossStrtDt)				>= 0)
and			(				CollatPurGainLossEndDt					is null
			or	(sysdate -	CollatPurGainLossEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

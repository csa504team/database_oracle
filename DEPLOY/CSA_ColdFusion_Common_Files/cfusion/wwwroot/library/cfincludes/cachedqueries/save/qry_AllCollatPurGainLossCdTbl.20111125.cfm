<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCollatPurGainLossCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CollatPurGainLossCd,
			CollatPurGainLossDescTxt,
			CollatPurGainLossStrtDt,
			CollatPurGainLossEndDt,
			CollatPurGainLossCd									AS code,
			CollatPurGainLossDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..CollatPurGainLossCdTbl<cfelse>
from		sbaref.CollatPurGainLossCdTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

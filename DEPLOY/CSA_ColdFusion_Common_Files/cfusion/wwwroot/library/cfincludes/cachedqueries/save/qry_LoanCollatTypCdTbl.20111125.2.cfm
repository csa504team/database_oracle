<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((LoanCollatTypCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.LoanCollatTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	SELECT
	      LoanCollatTypCd,
		  LoanCollatTypDescTxt,
		  LoanCollatLqdPct
	FROM  SBAREF.LoanCollatTypCdTbl
	WHERE LoanCollatTypStrtDt <= now()
	AND   (LoanCollatTypEndDt is null or LoanCollatTypEndDt > now())
	ORDER BY LoanCollatTypCd
</cfquery>
<cfdump var="#Variables.Scq.LoanCollatTypCdTb#"><cfabort>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

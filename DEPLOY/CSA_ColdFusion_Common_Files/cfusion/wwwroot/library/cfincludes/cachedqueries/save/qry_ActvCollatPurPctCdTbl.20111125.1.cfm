<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvCollatPurPctCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvCollatPurPctCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  select
      	COLLATPURPCTCD, 
	  	COLLATPURPCTDESCTXT,
	  	COLLATPURPCTCD							AS code,
		COLLATPURPCTDESCTXT						AS description <cfif Variables.Sybase>	  
	   from	sbaref..CollatPurPctCdTbl
	   where (datediff(dd,COLLATPURPCTSTARTDT,	getdate()) >= 0)
		   and	(COLLATPURPCTENDDT	is null
		   or datediff(dd,COLLATPURPCTENDDT,	getdate()) <= 0)<cfelse>
	   from	sbaref.CollatPurPctCdTbl
       where	((sysdate -	COLLATPURPCTSTARTDT) >= 0)
       		and	(COLLATPURPCTENDDT	is null
			or	(sysdate -	COLLATPURPCTENDDT)		<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

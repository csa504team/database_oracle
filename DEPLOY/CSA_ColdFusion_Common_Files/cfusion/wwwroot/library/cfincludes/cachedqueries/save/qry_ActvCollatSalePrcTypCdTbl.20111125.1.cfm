<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvCollatSalePrcTypCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvCollatSalePrcTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	COLLATSALEPRCTYPCD,
	    COLLATSALEPRCTYPCDDESCTXT,
	    COLLATSALEPRCTYPCD									AS code,
		COLLATSALEPRCTYPCDDESCTXT							AS description <cfif Variables.Sybase>	  
	   from	sbaref..COLLATSALETYPCDTbl
	   where (datediff(dd,COLLATSALEPRCTYPCDSTRTDT,	getdate()) >= 0)
		   and	(COLLATSALEPRCTYPC-DENDDT		is null
		   or datediff(dd,COLLATSALEPRCTYPCDENDDT,	getdate()) <= 0)<cfelse>
	   from	sbaref.COLLATSALETYPCDTbl
       where ((sysdate -	COLLATSALEPRCTYPCDSTRTDT)	>= 0)
       		and	(COLLATSALEPRCTYPCDENDDT	is null
			or (sysdate -	COLLATSALEPRCTYPCDENDDT)	<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

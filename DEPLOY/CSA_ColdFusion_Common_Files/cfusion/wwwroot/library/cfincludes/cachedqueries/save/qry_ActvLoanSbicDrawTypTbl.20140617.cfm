<!---
AUTHOR:				Elena Spratling. 
DATE:				06/10/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanSbicDrawTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LOANSBICDRAWTYP			AS code,
			LOANSBICDRAWTYPDESC	    AS description 	<cfif Variables.Sybase>
from		sbaref..LOANSBICDRAWTYPTBL
where		(	datediff(dd,LOANSBICDRAWTYPSTRTDT,		getdate()) >= 0)
and			(				LOANSBICDRAWTYPENDDT				is null
			or	datediff(dd,LOANSBICDRAWTYPENDDT,	getdate()) <= 0)<cfelse>
from		sbaref.LOANSBICDRAWTYPTBL
where		(	(sysdate -	LOANSBICDRAWTYPSTRTDT)			>= 0)
and			(				LOANSBICDRAWTYPENDDT				is null
			or	(sysdate -	LOANSBICDRAWTYPENDDT)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">





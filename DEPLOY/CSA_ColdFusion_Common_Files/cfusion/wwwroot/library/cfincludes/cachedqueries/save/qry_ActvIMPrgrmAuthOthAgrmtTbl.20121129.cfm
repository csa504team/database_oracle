<!---
AUTHOR:				Steve Seaquist, Base Technologies, Inc., for the US Small Business Administration. 
DATE:				11/29/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/29/2012, SRS:	Original implementation. DOESN'T HAVE CODE/DESCRIPTION!! (3 columns for code, 
										no column for description, so this table will just have to be an exception.) 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMPrgrmAuthOthAgrmtTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		PrcsMthdCd,
			IMPrgrmAuthAgrmtSeqNmb,<!--- Actually, a "subsequence number", combo of PrcsMthdCd and SeqNmb is unique. --->
			PIMSPrgrmId,
			UndrwritngBy<cfif Variables.Sybase>
from		sbaref..IMPrgrmAuthOthAgrmtTbl
where		(	datediff(dd,IMPrgrmAuthOthStrtDt,		getdate()) >= 0)
and			(				IMPrgrmAuthOthEndDt			is null
			or	datediff(dd,IMPrgrmAuthOthEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.IMPrgrmAuthOthAgrmtTbl
where		(	(sysdate -	IMPrgrmAuthOthStrtDt)		>= 0)
and			(				IMPrgrmAuthOthEndDt			is null
			or	(sysdate -	IMPrgrmAuthOthEndDt)		<= 0)</cfif>
order by	PrcsMthdCd,
			IMPrgrmAuthAgrmtSeqNmb
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

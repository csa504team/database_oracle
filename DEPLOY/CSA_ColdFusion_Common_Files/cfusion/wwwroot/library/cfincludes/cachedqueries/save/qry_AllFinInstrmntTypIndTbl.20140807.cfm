<!---
AUTHOR:				Elena Spratling. 
DATE:				08/07/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllFinInstrmntTypIndTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">

select FININSTRMNTTYPIND		AS code,
	   FININSTRMNTTYPDESCTXT	AS description 	<cfif Variables.Sybase>
from   sbaref..fininstrmnttypindtbl
<cfelse>
from		sbaref.fininstrmnttypindtbl
</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">





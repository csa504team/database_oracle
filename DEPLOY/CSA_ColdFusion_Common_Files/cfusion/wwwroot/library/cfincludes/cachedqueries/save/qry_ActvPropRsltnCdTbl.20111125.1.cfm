<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropRsltnCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropRsltnCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	PROPRSLTNCD,
      	PROPRSLTNDESCTXT,
		PROPRSLTNCD									AS code,
		PROPRSLTNDESCTXT							AS description <cfif Variables.Sybase>	  
	   from	sbaref..PROPRSLTNCDTbl
	   where (datediff(dd,PROPRSLTNSTRTDT,		getdate()) >= 0)
		   and	(PROPRSLTNENDDT		is null
		   or datediff(dd,PROPRSLTNENDDT,		getdate()) <= 0)<cfelse>
	   from	sbaref.PROPRSLTNCDTbl
       where		((sysdate -	PROPRSLTNSTRTDT)		>= 0)
       		and		(PROPRSLTNENDDT	is null
			or	(sysdate -	PROPRSLTNENDDT)		<= 0)</cfif>
      order by code	  
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

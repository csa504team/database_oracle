<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname   					= "getFinCount" />
<cfset Variables.LogAct     					= "get financial counts" />
<cfinclude template="/cfincludes/oracle/loan/spc_FINANCOUNTSELCSP.PUBLIC.cfm" />
<cfif Variables.TxnErr><!--- stored procedure errors --->
	<cfset Variables.SaveMe					= "No">
</cfif>
<cfset Variables.ScqVars								= StructNew()>
<cfloop index="ScqVarKey"								list="LoanFYFundNmb,SBGBIDFYNmb,SBGFNLFYNmb,PSBBIDFYNmb,PSBFNLFYNmb">
	<cfset Variables.ScqVars[ScqVarKey]				= "">
</cfloop>
<cfset Variables.ScqVars.LoanFYFundNmb			= Variables.LoanFYFundNmb>
<cfset Variables.ScqVars.DEBSPRGCNmb			= Variables.DEBSPRGCNmb>
<cfset Variables.ScqVars.DISATRLNNmb			= Variables.DISATRLNNmb>
<cfset Variables.ScqVars.MICROLNSNmb			= Variables.MICROLNSNmb>
<cfset Variables.ScqVars.IntrmdLNNmb			= Variables.IntrmdLNNmb>
<cfset Variables.ScqVars.SBGBIDFYNmb			= Variables.SBGBIDFYNmb>
<cfset Variables.ScqVars.SBGFNLFYNmb			= Variables.SBGFNLFYNmb>
<cfset Variables.ScqVars.PSBBIDFYNmb			= Variables.PSBBIDFYNmb>
<cfset Variables.ScqVars.PSBFNLFYNmb			= Variables.PSBFNLFYNmb>

<cfset Variables.Scq["CAFSHomeVars"]			= Duplicate(Variables.ScqVars)>
<cfset StructDelete(Variables, "ScqVars")>

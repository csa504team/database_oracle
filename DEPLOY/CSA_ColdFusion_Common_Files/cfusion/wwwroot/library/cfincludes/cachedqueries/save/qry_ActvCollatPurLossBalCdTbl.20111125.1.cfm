<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurLossBalCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select		COLLATPURLOSSBALCD,
		    	COLLATPURLOSSBALDESCTXT,		    	
		    	COLLATPURLOSSBALCD									AS code,
				COLLATPURLOSSBALDESCTXT								AS description <cfif Variables.Sybase>
	from		sbaref..CollatPurLossBalCdTbl
	where		(	datediff(dd,COLLATPURLOSSBALSTRTDT,				getdate()) >= 0)
	and			(				COLLATPURLOSSBALENDDT					is null
				or	datediff(dd,COLLATPURLOSSBALENDDT,				getdate()) <= 0)<cfelse>
	from		sbaref.CollatPurLossBalCdTbl
	where		(	(sysdate -	COLLATPURLOSSBALSTRTDT)				>= 0)
	and			(				CollatPurGainLossEndDt					is null
				or	(sysdate -	CollatPurGainLossEndDt)				<= 0)</cfif>
	order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

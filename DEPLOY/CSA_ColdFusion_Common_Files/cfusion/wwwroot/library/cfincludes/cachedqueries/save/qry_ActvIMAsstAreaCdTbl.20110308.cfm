<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				03/08/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	03/08/2011, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMAsstAreaCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMAsstAreaCd,
			IMAsstAreaShortDescTxt,
			IMAsstAreaLongDescTxt,
			IMAsstAreaCd								AS code,
			IMAsstAreaShortDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMAsstAreaCdTbl
where		(	datediff(dd,IMAsstAreaCdStrtDt,			getdate()) >= 0)
and			(				IMAsstAreaCdEndDt			is null
			or	datediff(dd,IMAsstAreaCdEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.IMAsstAreaCdTbl
where		(	(sysdate -	IMAsstAreaCdStrtDt)			>= 0)
and			(				IMAsstAreaCdEndDt			is null
			or	(sysdate -	IMAsstAreaCdEndDt)			<= 0)</cfif>
order by	IMAsstAreaDsplyOrdNmb
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

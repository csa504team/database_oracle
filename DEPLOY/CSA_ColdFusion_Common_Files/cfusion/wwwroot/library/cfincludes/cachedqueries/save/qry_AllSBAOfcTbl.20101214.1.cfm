<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_aapreprocessing.cfm">
<cfquery name="Variables.Scq.AllSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		SBAOfcCd,
			StCd,
			ZipCd5,
			ZipCd4,
			SBAOfcTypCd,
			SBAOfc1Nm,
			SBAOfc2Nm,
			SBAOfcStrNmb,
			SBAOfcStrNm,
			SBAOfcStr2Nm,
			SBAOfcStrSfxNm,
			SBAOfcCtyNm,
			SBAOfcVoicePhnNmb,
			SBAOfcFaxPhnNmb,
			SBAOfcParntOfcCd,
			SBAOfcStrtDt,
			SBAOfcEndDt,
			SBAOfcCd									AS code,
			SBAOfc1Nm									AS description<cfif Variables.Sybase>
from		sbaref..SBAOfcTbl<cfelse>
from		sbaref.SBAOfcTbl</cfif>
order by	description
</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation.
								NS: changed CollatPurSrvcdByDesc to CollatPurSrvcdByDescTxt column
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurSrvcdByCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CollatPurSrvcdByCd,
			CollatPurSrvcdByDescTxt,
			CollatPurSrvcdByCd									AS code,
			CollatPurSrvcdByDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..CollatPurSrvcdByCdTbl
where		(	datediff(dd,CollatPurSrvcdByStartDt,				getdate()) >= 0)
and			(				CollatPurSrvcdByEndDt					is null
			or	datediff(dd,CollatPurSrvcdByEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CollatPurSrvcdByCdTbl
where		(	(sysdate -	CollatPurSrvcdByStartDt)				>= 0)
and			(				CollatPurSrvcdByEndDt					is null
			or	(sysdate -	CollatPurSrvcdByEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

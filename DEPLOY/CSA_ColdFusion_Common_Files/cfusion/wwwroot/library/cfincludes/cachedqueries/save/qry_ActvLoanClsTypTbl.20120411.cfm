<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				04/11/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	04/11/2012, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanClsTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanClsTypCd,
			LoanClsTypTxt,
			LoanClsTypCd								AS code,
			LoanClsTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanClsTypTbl
where		(	datediff(dd,LoanClsTypStrtDt,			getdate()) >= 0)
and			(				LoanClsTypEndDt				is null
			or	datediff(dd,LoanClsTypEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanClsTypTbl
where		(	(sysdate -	LoanClsTypStrtDt)			>= 0)
and			(				LoanClsTypEndDt				is null
			or	(sysdate -	LoanClsTypEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

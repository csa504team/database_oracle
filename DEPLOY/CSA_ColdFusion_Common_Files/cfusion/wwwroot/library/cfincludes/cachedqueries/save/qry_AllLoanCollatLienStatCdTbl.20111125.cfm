<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvLoanCollatLienStatCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllLoanCollatLienStatCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  select
      	LoanCollatLienStatCd,
	    LoanCollatLienStatDescTxt,
		LoanCollatLienStatStrtDt,
		LoanCollatLienStatEndDt,
	    LoanCollatLienStatCd								AS code,
		LoanCollatLienStatDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..LoanCollatLienStatCdTbl
	   <cfelse>
	   from	sbaref.LoanCollatLienStatCdTbl
       </cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

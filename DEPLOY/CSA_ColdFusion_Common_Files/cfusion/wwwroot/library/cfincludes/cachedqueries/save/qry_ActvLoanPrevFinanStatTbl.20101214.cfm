<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanPrevFinanStatTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanPrevFinanStatCd,
			LoanPrevFinanStatDescTxt,
			LoanPrevFinanStatCd							AS code,
			LoanPrevFinanStatDescTxt					AS description<cfif Variables.Sybase>
from		sbaref..LoanPrevFinanStatTbl
where		(	datediff(dd,LoanPrevFinanStatStrtDt,	getdate()) >= 0)
and			(				LoanPrevFinanStatEndDt		is null
			or	datediff(dd,LoanPrevFinanStatEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.LoanPrevFinanStatTbl
where		(	(sysdate -	LoanPrevFinanStatStrtDt)	>= 0)
and			(				LoanPrevFinanStatEndDt		is null
			or	(sysdate -	LoanPrevFinanStatEndDt)		<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

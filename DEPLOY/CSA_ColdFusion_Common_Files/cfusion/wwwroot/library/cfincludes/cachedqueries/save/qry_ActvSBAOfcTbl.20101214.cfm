<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		SBAOfcCd,
			StCd,
			ZipCd5,
			ZipCd4,
			SBAOfcTypCd,
			SBAOfc1Nm,
			SBAOfc2Nm,
			SBAOfcStrNmb,
			SBAOfcStrNm,
			SBAOfcStr2Nm,
			SBAOfcStrSfxNm,
			SBAOfcCtyNm,
			SBAOfcVoicePhnNmb,
			SBAOfcFaxPhnNmb,
			SBAOfcParntOfcCd,
			SBAOfcCd									AS code,
			SBAOfc1Nm									AS description<cfif Variables.Sybase>
from		sbaref..SBAOfcTbl
where		(	datediff(dd,SBAOfcStrtDt,				getdate()) >= 0)
and			(				SBAOfcEndDt					is null
			or	datediff(dd,SBAOfcEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.SBAOfcTbl
where		(	(sysdate -	SBAOfcStrtDt)				>= 0)
and			(				SBAOfcEndDt					is null
			or	(sysdate -	SBAOfcEndDt)				<= 0)</cfif>
order by	description
</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

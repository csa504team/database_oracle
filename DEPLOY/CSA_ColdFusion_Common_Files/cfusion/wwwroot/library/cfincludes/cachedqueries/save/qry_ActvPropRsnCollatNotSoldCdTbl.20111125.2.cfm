<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation. 
								NS: 	Changed PropRsnCollatNotSoldDesc to PropRsnCollatNotSoldDescTxt
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPropRsnCollatNotSoldCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select		PropRsnCollatNotSoldCd,
				PropRsnCollatNotSoldDescTxt,
				PropRsnCollatNotSoldCd									AS code,
				PropRsnCollatNotSoldDescTxt								AS description<cfif Variables.Sybase>
	from		sbaref..PropRsnCollatNotSoldCdTbl
	where		(	datediff(dd,PropRsnCollatNotSoldStrtDt,				getdate()) >= 0)
	and			(				PropRsnCollatNotSoldEndDt				is null
				or	datediff(dd,PropRsnCollatNotSoldEndDt,				getdate()) <= 0)<cfelse>
	from		sbaref.PropRsnCollatNotSoldCdTbl
	where		(	(sysdate -	PropRsnCollatNotSoldStrtDt)				>= 0)
	and			(				PropRsnCollatNotSoldEndDt				is null
				or	(sysdate -	PropRsnCollatNotSoldEndDt)				<= 0)</cfif>
	order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

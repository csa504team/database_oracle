<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				03/08/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	03/08/2011, SRS:	Original implementation. To make the entries come out in the order asked-for by the 
										Program Office, this query is unusual in that it's ordered by code. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCnselngHrCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMCnselngHrCd,
			IMCnselngHrCdDescTxt,
			IMCnselngHrCd								AS code,
			IMCnselngHrCdDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMCnselngHrCdTbl
where		(	datediff(dd,IMCnselngHrCdStrtDt,		getdate()) >= 0)
and			(				IMCnselngHrCdEndDt			is null
			or	datediff(dd,IMCnselngHrCdEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.IMCnselngHrCdTbl
where		(	(sysdate -	IMCnselngHrCdStrtDt)		>= 0)
and			(				IMCnselngHrCdEndDt			is null
			or	(sysdate -	IMCnselngHrCdEndDt)			<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

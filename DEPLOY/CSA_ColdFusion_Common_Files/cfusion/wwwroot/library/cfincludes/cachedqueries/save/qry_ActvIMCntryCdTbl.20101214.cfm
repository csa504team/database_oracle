<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMCntryCd,
			IMCntryNm,
			IMCntryDialCd,
			IMCntryCd									AS code,
			IMCntryNm									AS description<cfif Variables.Sybase>
from		sbaref..IMCntryCdTbl
where		(	datediff(dd,IMCntryStrtDt,				getdate()) >= 0)
and			(				IMCntryEndDt				is null
			or	datediff(dd,IMCntryEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.IMCntryCdTbl
where		(	(sysdate -	IMCntryStrtDt)				>= 0)
and			(				IMCntryEndDt				is null
			or	(sysdate -	IMCntryEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>

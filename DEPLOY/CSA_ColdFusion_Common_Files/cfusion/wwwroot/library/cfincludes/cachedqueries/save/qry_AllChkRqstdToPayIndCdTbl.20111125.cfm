<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllChkRqstdToPayIndCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	SELECT
      	ChkRqstdToPayIndCd,
	    ChkRqstdToPayIndDescTxt,
	    ChkRqstdToPayIndStrtDt,
	    ChkRqstdToPayIndEndDt,
	    ChkRqstdToPayIndCd									AS code,
		ChkRqstdToPayIndDescTxt								AS description <cfif Variables.Sybase>	  
	   from	sbaref..ChkRqstdToPayIndCdTbl
	  <cfelse>
	   from	sbaref.ChkRqstdToPayIndCdTbl
       </cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				04/11/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	04/11/2012, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanSchdlTypTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanSchdlTypCd,
			LoanSchdlTypTxt,
			LoanSchdlTypCd								AS code,
			LoanSchdlTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanSchdlTypTbl
where		(	datediff(dd,LoanSchdlTypStrtDt,			getdate()) >= 0)
and			(				LoanSchdlTypEndDt			is null
			or	datediff(dd,LoanSchdlTypEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanSchdlTypTbl
where		(	(sysdate -	LoanSchdlTypStrtDt)			>= 0)
and			(				LoanSchdlTypEndDt			is null
			or	(sysdate -	LoanSchdlTypEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

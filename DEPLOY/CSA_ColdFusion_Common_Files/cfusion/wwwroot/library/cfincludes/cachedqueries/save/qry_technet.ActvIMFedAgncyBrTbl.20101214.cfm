<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.technet.ActvIMFedAgncyBrTbl"	datasource="#db#" dbtype="#dbtype#">
select		IMFedAgncySeqNmb,
			IMFedAgncyBrSeqNmb,
			IMFedAgncyBrNm,
			IMFedAgncyBrActvInactvInd,
			IMFedAgncyBrCreatUserId,
			IMFedAgncyBrCreatDt,
			upper(IMFedAgncyBrNm)						UpperBrNm,
			IMFedAgncyBrSeqNmb							code,
			IMFedAgncyBrNm								description<cfif Variables.Sybase>
from		technet..IMFedAgncyBrTbl<cfelse>
from		technet.IMFedAgncyBrTbl</cfif>
where		IMFedAgncyBrActvInactvInd					= 0
order by	UpperBrNm
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/28/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/28/2011, SRv:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.CourtIdCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CourtIdCd,
        	CourtIdDescTxt,
        	CourtIdStrtDt,
        	CourtIdEndDt,
        	CreatUserId,
        	CreatDt,
			CourtIdCd		as code,
        	CourtIdDescTxt	as description<cfif Variables.Sybase>
from		sbaref..CourtIdCdTbl<cfelse>
from		sbaref.CourtIdCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration.
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	12/15/2015, NNI:	Added drwadown count.
					10/22/2015, NNI:	Original implementation.
									11/06/2015, SMJ:	Created a config parameter in order to increase maintainability.
--->

<!--- Configuration Parameters --->
<cfset Variables.FinCountNames =
 "BondSbgBidFyNmb,BondSbgFnlFyNmb,BondPsbBidFyNmb,BondPsbFnlFyNmb,LoanDebsPrgcNmb,LoanDisatrLnNmb,LoanMicroLnsNmb,LoanSBICLNNmb,Loan7AFYNmb,LoanMicroBrNmb,FinanFyTotNmb,FinanFyTotAmt"
/>

<cfloop list = "#Variables.FinCountNames#" index = "ScqVarKey">
	<cfset ScqVarKey = "" />
</cfloop>

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname   					= "getFinCount" />
<cfset Variables.LogAct     					= "get financial counts" />
<cfinclude template="/cfincludes/oracle/loan/spc_FINANCOUNTSELCSP.PUBLIC.cfm" />
<cfif Variables.TxnErr><!--- stored procedure errors --->
	<cfset Variables.SaveMe					= "No">
</cfif>
<cfset Variables.ScqVars								= StructNew()>

<cfloop list="#Variables.FinCountNames#" index="ScqVarKey">
	<cfset Variables.ScqVars[ScqVarKey] = "#ScqVarKey#" />
</cfloop>

<cfset Variables.ScqVars.Loan7AFYNmb	  	= Variables.Loan7AFYNmb   />
<cfset Variables.ScqVars.LoanDEBSPRGCNmb  	= Variables.LoanDEBSPRGCNmb />
<cfset Variables.ScqVars.LoanDISATRLNNmb  	= Variables.LoanDISATRLNNmb />
<cfset Variables.ScqVars.LoanMICROLNSNmb  	= Variables.LoanMICROLNSNmb />
<cfset Variables.ScqVars.LoanSBICLNNmb    	= Variables.LoanSBICLNNmb   />
<cfset Variables.ScqVars.LoanSBICDrawNmb    = Variables.LoanSBICDrawNmb   />
<cfset Variables.ScqVars.BondSBGBIDFYNmb	= Variables.BondSBGBIDFYNmb />
<cfset Variables.ScqVars.BondSBGFNLFYNmb	= Variables.BondSBGFNLFYNmb />
<cfset Variables.ScqVars.BondPSBBIDFYNmb  	= Variables.BondPSBBIDFYNmb />
<cfset Variables.ScqVars.BondPSBFNLFYNmb  	= Variables.BondPSBFNLFYNmb />
<cfset Variables.ScqVars.LoanMicroBrNmb   	= Variables.LoanMicroBrNmb />
<cfset Variables.ScqVars.FinanFyTotNmb   	= Variables.FinanFyTotNmb />
<cfset Variables.ScqVars.FinanFyTotAmt   	= Variables.FinanFyTotAmt />

<cfset Variables.Scq["CAFSHomeVars"]			= Duplicate(Variables.ScqVars)>
<cfset StructDelete(Variables, "ScqVars")>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		PrcsMthdCd,
			PrcsMthdDesc,
			PrcsMthdCd									AS code,
			PrcsMthdDesc								AS description,
			LoanTypInd,
			PrcsMthdRvwrRqrdNmb,
			PrcsMthdExprsInd,
			PrcsMthdUndrwritngBy,
			PrgrmCd,
			PrgrmAuthAreaInd<cfif Variables.Sybase>
from		sbaref..PrcsMthdTbl
where		(	datediff(dd,PrcsMthdStrtDt,				getdate()) >= 0)
and			(				PrcsMthdEndDt				is null
			or	datediff(dd,PrcsMthdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.PrcsMthdTbl
where		(	(sysdate -	PrcsMthdStrtDt)				>= 0)
and			(				PrcsMthdEndDt				is null
			or	(sysdate -	PrcsMthdEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<cfset Variables.ScqLists								= StructNew()>
<cfloop index="ScqListKey"								list="PrcsMthd7ExpSet,PrcsMthdDirectSet,PrcsMthdGuarantySet,PrcsMthdRev1Set,PrcsMthdRev2Set,PrcsMthdRev3Set,PrcsMthdUndrwritngByLndr,PrcsMthdUndrwritngBySBA">
	<cfset Variables.ScqLists[ScqListKey]				= "">
</cfloop>
<cfloop query="Variables.Scq.ActvPrcsMthdTbl">
	<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.LoanTypInd#">
	<cfcase value="D">		<cfset Variables.ScqLists.PrcsMthdDirectSet			= ListAppend(Variables.ScqLists.PrcsMthdDirectSet,			PrcsMthdCd)></cfcase>
	<cfcase value="G">		<cfset Variables.ScqLists.PrcsMthdGuarantySet		= ListAppend(Variables.ScqLists.PrcsMthdGuarantySet,		PrcsMthdCd)></cfcase>
	</cfswitch>
	<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.PrcsMthdRvwrRqrdNmb#">
	<cfcase value="1">		<cfset Variables.ScqLists.PrcsMthdRev1Set			= ListAppend(Variables.ScqLists.PrcsMthdRev1Set,			PrcsMthdCd)></cfcase>
	<cfcase value="2">		<cfset Variables.ScqLists.PrcsMthdRev2Set			= ListAppend(Variables.ScqLists.PrcsMthdRev2Set,			PrcsMthdCd)></cfcase>
	<cfcase value="3">		<cfset Variables.ScqLists.PrcsMthdRev3Set			= ListAppend(Variables.ScqLists.PrcsMthdRev3Set,			PrcsMthdCd)></cfcase>
	</cfswitch>
	<!--- Don't really need cfswitch for only 1 case, but it's just as fast as a cfif, and it makes the code more readable: --->
	<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.PrcsMthdExprsInd#">
	<cfcase value="Y">		<cfset Variables.ScqLists.PrcsMthd7ExpSet			= ListAppend(Variables.ScqLists.PrcsMthd7ExpSet,			PrcsMthdCd)></cfcase>
	</cfswitch>
	<cfswitch expression="#UCase(Trim(Variables.Scq.ActvPrcsMthdTbl.PrcsMthdUndrwritngBy))#">
	<cfcase value="LNDR">	<cfset Variables.ScqLists.PrcsMthdUndrwritngByLndr	= ListAppend(Variables.ScqLists.PrcsMthdUndrwritngByLndr,	PrcsMthdCd)></cfcase>
	<cfcase value="SBA">	<cfset Variables.ScqLists.PrcsMthdUndrwritngBySBA	= ListAppend(Variables.ScqLists.PrcsMthdUndrwritngBySBA,	PrcsMthdCd)></cfcase>
	</cfswitch>
</cfloop>
<cfset Variables.Scq["PrcsMthdLists"]					= Duplicate(Variables.ScqLists)>
<cfset StructDelete(Variables, "ScqLists")>

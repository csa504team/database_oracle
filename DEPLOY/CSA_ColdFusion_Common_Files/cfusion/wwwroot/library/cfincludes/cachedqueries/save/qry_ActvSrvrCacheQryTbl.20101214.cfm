<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSrvrCacheQryTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		distinct
			rtrim(SrvrNm)								as SrvrNm,
			InstanceNm,
			rtrim(CacheQryCd)							as CacheQryCd,<cfif Variables.Sybase>
			rtrim(SrvrNm)+'.'+InstanceNm				as code,		-- likely to change<cfelse>
			rtrim(SrvrNm)||'.'||InstanceNm				as code,		-- likely to change</cfif>
			case CacheQryCd
			when 'sbaref'	then						1
			when 'partner'	then						2
			when 'pronet'	then						3
			when 'technet'	then						4
			else										5
			end											as displayorder,
			rtrim(CacheQryCd)							as description<cfif Variables.Sybase>
from		sbaref..SrvrCacheQryTbl
where		(	datediff(dd,SrvrCacheQryStrtDt,getdate())	>= 0)
and			(				SrvrCacheQryEndDt				is null
			or	datediff(dd,SrvrCacheQryEndDt, getdate())	<= 0)<cfelse>
from		sbaref.SrvrCacheQryTbl
where		(	(sysdate -	SrvrCacheQryStrtDt)				>= 0)
and			(				SrvrCacheQryEndDt				is null
			or	(sysdate -	SrvrCacheQryEndDt)				<= 0)</cfif>
order by	code,
			displayorder
</cfquery>

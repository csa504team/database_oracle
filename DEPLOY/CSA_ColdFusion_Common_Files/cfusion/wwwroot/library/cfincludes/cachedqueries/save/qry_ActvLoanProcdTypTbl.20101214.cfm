<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanProcdTypTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanProcdTypCd,
			LoanProcdTypDescTxt,
			ProcdTypCd,<!--- Ignoring LoanProcdTypCmnt for now, because it's always null. --->
			ProcdTypCd || LoanProcdTypCd				AS code,
			LoanProcdTypDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..LoanProcdTypTbl
where		(	datediff(dd,LoanProcdTypCdStrtDt,		getdate()) >= 0)
and			(				LoanProcdTypCdEndDt			is null
			or	datediff(dd,LoanProcdTypCdEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.LoanProcdTypTbl
where		(	(sysdate -	LoanProcdTypCdStrtDt)		>= 0)
and			(				LoanProcdTypCdEndDt			is null
			or	(sysdate -	LoanProcdTypCdEndDt)		<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

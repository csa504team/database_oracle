<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		PrcsMthdCd,
			PrcsMthdDesc,
			PrcsMthdStrtDt,
			PrcsMthdEndDt,
			PrcsMthdCd									AS code,
			PrcsMthdDesc								AS description,
			LoanTypInd,
			PrcsMthdRvwrRqrdNmb,
			PrcsMthdExprsInd,
			PrcsMthdUndrwritngBy,
			PrgrmCd,
			PrgrmAuthAreaInd<cfif Variables.Sybase>
from		sbaref..PrcsMthdTbl<cfelse>
from		sbaref.PrcsMthdTbl</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

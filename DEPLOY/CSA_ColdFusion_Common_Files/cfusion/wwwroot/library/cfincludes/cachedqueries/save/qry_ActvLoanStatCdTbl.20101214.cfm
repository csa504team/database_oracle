<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanStatCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanStatCd,
			ColsonStatCd,
			LoanStatDescTxt,
			LoanStatCd									AS code,
			LoanStatDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanStatCdTbl
where		(	datediff(dd,LoanStatStrtDt,				getdate()) >= 0)
and			(				LoanStatEndDt				is null
			or	datediff(dd,LoanStatEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanStatCdTbl
where		(	(sysdate -	LoanStatStrtDt)				>= 0)
and			(				LoanStatEndDt				is null
			or	(sysdate -	LoanStatEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

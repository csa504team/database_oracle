<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/28/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/28/2011, SRv:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.LitCaseTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LitCaseTypCd,
          	LitCaseTypDescTxt,
          	LitCaseTypStrtDt,
	        LitCaseTypEndDt,
          	CreatUserId,
	        CreatDt,
        	LitCaseTypCd			as code,
        	LitCaseTypDescTxt		as description<cfif Variables.Sybase>
from		sbaref..LitCaseTypCdTbl<cfelse>
from		sbaref.LitCaseTypCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

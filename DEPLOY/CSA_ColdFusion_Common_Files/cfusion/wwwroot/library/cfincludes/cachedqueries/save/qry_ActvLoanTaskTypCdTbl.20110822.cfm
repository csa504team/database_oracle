<!---
AUTHOR:				Nirish Namilae 
DATE:				08/23/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/23/2011, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanTaskTypCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanTaskTypCd,
			LoanTaskTypDescTxt,
			LoanTaskTypCd							AS code,
			LoanTaskTypDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..LoanTaskTypCdTbl<cfelse><!--- else = default = Oracle: --->
from		sbaref.LoanTaskTypCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

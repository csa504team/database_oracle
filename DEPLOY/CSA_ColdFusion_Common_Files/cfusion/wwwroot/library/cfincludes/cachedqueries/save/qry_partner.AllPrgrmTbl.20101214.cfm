<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.partner.AllPrgrmTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		PrgrmId,
			PrgrmDesc,
			PrgrmDocReqdInd,
			PrgrmEffDt,
			PrgrmFormId,
			ValidPrgrmTyp,
			PrgrmAreaOfOperReqdInd<cfif Variables.Sybase>
from		partner..PrgrmTbl<cfelse>
from		partner.PrgrmTbl</cfif>
where		PrgrmDispOrdNmb								!= 0
order by	PrgrmDispOrdNmb
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvBusAgeCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		BusAgeCd,
			BusAgeDesc,
			BusMFAgeCd,
			BusAgeCd									AS code,
			BusAgeDesc									AS description<cfif Variables.Sybase>
from		sbaref..BusAgeCdTbl
where		(	datediff(dd,BusAgeStrtDt,				getdate()) >= 0)
and			(				BusAgeEndDt					is null
			or	datediff(dd,BusAgeEndDt,				getdate()) <= 0)<cfelse><!--- else = default = Oracle: --->
from		sbaref.BusAgeCdTbl
where		(	(sysdate -	BusAgeStrtDt)				>= 0)
and			(				BusAgeEndDt					is null
			or	(sysdate -	BusAgeEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

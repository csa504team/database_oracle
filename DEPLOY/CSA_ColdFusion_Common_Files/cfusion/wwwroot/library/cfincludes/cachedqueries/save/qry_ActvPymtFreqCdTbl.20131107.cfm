<!---
AUTHOR:				Robert Doyle, Solutions By Design, Inc., for the US Small Business Administration. 
DATE:				11/07/2013
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/07/2013, RSD:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPymtFreqCdTb"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select	PymtFreqCd,
		PymtFreqDescTxt,
		PymtFreqCd									AS code,
		PymtFreqDescTxt								AS description
from		sbaref.PymtFreqCdTbl
where		(	(sysdate -	PymtFreqStrtDt)			>= 0)
and			(				PymtFreqEndDt				is null
			or	(sysdate -	PymtFreqEndDt)				<= 0)
order by	code       
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
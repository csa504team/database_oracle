<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvACHAcctTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		ACHAcctTypCd,
			ACHAcctTypDescTxt,
			ACHAcctTypCd								AS code,
			ACHAcctTypDescTxt							AS description
from		sbaref.ACHAcctTypTbl
where 		(   (sysdate - AchAcctTypStrtDt)			>= 0)																		
and			(   AchAcctTypEndDt 						is null
or			(sysdate - AchAcctTypEndDt)					<= 0) 
order by	code
</cfquery>

<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvChkRqstdToPayIndCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvChkRqstdToPayIndCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	CHKRQSTDTOPAYINDCD,
	    CHKRQSTDTOPAYINDDESCTXT,
	    CHKRQSTDTOPAYINDCD									AS code,
		CHKRQSTDTOPAYINDDESCTXT								AS description  <cfif Variables.Sybase>	  
	   from	sbaref..CHKRQSTDTOPAYINDCDTbl
	   where (datediff(dd,CHKRQSTDTOPAYINDSTRTDT, getdate()) >= 0)
		   and	(CHKRQSTDTOPAYINDENDDT		is null
		   or datediff(dd,CHKRQSTDTOPAYINDENDDT, getdate()) <= 0)<cfelse>
	   from	sbaref.CHKRQSTDTOPAYINDCDTbl
       where ((sysdate -	CHKRQSTDTOPAYINDSTRTDT)	>= 0)
       		and	 (CHKRQSTDTOPAYINDENDDT	is null
			or	(sysdate -	CHKRQSTDTOPAYINDENDDT)		<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
	
<!---
AUTHOR:				Elena Spratling. 
DATE:				06/10/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					06/10/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanSbicLicnsTypTbl"				datasource="#Variables.db#">

select		LOANSBICLICNSTYP			AS code,
			LOANSBICLICNSTYPDESC		AS description<cfif Variables.Sybase>
from		sbaref..LOANSBICLICNSTYPTBL
where		(	datediff(dd,LOANSBICLICNSTYPSTRTDT,		getdate()) >= 0)
and			(				LOANSBICLICNSTYPENDDT				is null
			or	datediff(dd,LOANSBICLICNSTYPENDDT,	getdate()) <= 0)<cfelse>
from		sbaref.LOANSBICLICNSTYPTBL
where		(	(sysdate -	LOANSBICLICNSTYPSTRTDT)			>= 0)
and			(				LOANSBICLICNSTYPENDDT				is null
			or	(sysdate -	LOANSBICLICNSTYPENDDT)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">


<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc., for the US Small Business Administration. 
DATE:				09/23/2014. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/23/2014, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvUCCFileLvlCdTbl"	datasource="#Variables.db#">
select		UCCFileLvlCd,
			UCCFileLvlDescTxt,
			case UCCFileLvlCd
			when 'S' then 'St'
			when 'C' then 'Cnty'
			when 'L' then 'Local'
			else null
			end											as UCCFileLvlAbbrTxt,
			UCCFileLvlOrdNmb,
			UCCFileLvlOrdNmb							as displayorder,
			UCCFileLvlCd								as code,
			UCCFileLvlDescTxt							as description<cfif Variables.Sybase>
from		sbaref..UCCFileLvlCdTbl
where		(	datediff(dd,UCCFileLvlStrtDt,			getdate()) >= 0)
and			(				UCCFileLvlEndDt				is null
			or	datediff(dd,UCCFileLvlEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.UCCFileLvlCdTbl
where		(	(sysdate -	UCCFileLvlStrtDt)			>= 0)
and			(				UCCFileLvlEndDt				is null
			or	(sysdate -	UCCFileLvlEndDt)			<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

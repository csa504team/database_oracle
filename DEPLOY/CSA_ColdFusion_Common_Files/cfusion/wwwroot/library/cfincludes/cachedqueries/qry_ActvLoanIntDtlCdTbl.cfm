<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/08/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/08/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanIntDtlCdTbl"		datasource="#Variables.db#">
select		LoanIntDtlCd,
			LoanIntDtlDescTxt,
			LoanIntDtlCd								AS code,
			LoanIntDtlDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..LoanIntDtlCdTbl
where		(	datediff(dd,LoanIntDtlStrtDt,			getdate()) >= 0)
and			(				LoanIntDtlEndDt				is null
			or	datediff(dd,LoanIntDtlEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanIntDtlCdTbl
where		(	(sysdate -	LoanIntDtlStrtDt)			>= 0)
and			(				LoanIntDtlEndDt				is null
			or	(sysdate -	LoanIntDtlEndDt)			<= 0)</cfif>
order by	LoanIntDtlDispOrdNmb
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

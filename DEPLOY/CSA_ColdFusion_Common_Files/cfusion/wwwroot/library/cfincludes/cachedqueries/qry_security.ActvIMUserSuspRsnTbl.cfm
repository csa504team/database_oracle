<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration.
DATE:				06/10/2015.
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	06/10/2015, SRS:	Original implementation. First query for CacheQryCd "security".
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMUserSuspRsnTbl"		datasource="#Variables.db#">
select		IMUserSuspRsnCd,
			IMUserSuspRsnDescTxt,
	<!---	IMUserSuspRsnDispOrdNmb, ((no data yet)) --->
			IMUserSuspRsnStrtDt,
			IMUserSuspRsnEndDt,
			IMUserSuspRsnCd										AS code,
			IMUserSuspRsnDescTxt								AS description<cfif Variables.Sybase>
from		security..IMUserSuspRsnTbl
where		(	datediff(dd,IMUserSuspRsnStrtDt,				getdate()) >= 0)
and			(				IMUserSuspRsnEndDt					is null
			or	datediff(dd,IMUserSuspRsnEndDt,					getdate()) <= 0)<cfelse>
from		security.IMUserSuspRsnTbl
where		(	(sysdate -	IMUserSuspRsnStrtDt)				>= 0)
and			(				IMUserSuspRsnEndDt					is null
			or	(sysdate -	IMUserSuspRsnEndDt)					<= 0)</cfif>
and      	IMUSERSUSPRSNDISPIND = 'Y'
order by    IMUserSuspRsnCd

</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

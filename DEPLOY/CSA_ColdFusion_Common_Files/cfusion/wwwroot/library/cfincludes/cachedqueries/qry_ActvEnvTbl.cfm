<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvEnvTbl"				datasource="#Variables.db#">
select		rtrim(EnvCd)								as EnvCd,
			EnvDescTxt,
			rtrim(EnvTypCd)								as EnvTypCd,
			LognURLTxt,
			rtrim(EnvCd)								as code,
			EnvDescTxt									as description,
			case EnvTypCd
			when 'Dev'	then							0
			when 'Test'	then							1
			else										2
			end											as DevTestProdInd<cfif Variables.Sybase>
from		sbaref..EnvTbl
where		(	datediff(dd,EnvStrtDt,					getdate()) >= 0)
and			(				EnvEndDt					is null
			or	datediff(dd,EnvEndDt,					getdate()) <= 0)<cfelse>
from		sbaref.EnvTbl
where		(	(sysdate -	EnvStrtDt)					>= 0)
and			(				EnvEndDt					is null
			or	(sysdate -	EnvEndDt)					<= 0)</cfif>
order by	DevTestProdInd,
			EnvCd
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

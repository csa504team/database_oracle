<!---
AUTHOR:				Nelli Salatova. 
DATE:				05/09/2017
DESCRIPTION:		Returns all active secure instrument type codes. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	05/09/2017, NS:	(CAFSOPER-793) Original implementation. 
--->
<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSecurInstrmntTypCdTbl"	datasource="#Variables.db#">
select		SecurInstrmntTypCd,
			SecurInstrmntTypDescTxt,
			SecurInstrmntTypCd							code,
			SecurInstrmntTypDescTxt						description<cfif Variables.Sybase>
from		sbaref.SecurInstrmntTypCdTbl
where		(	datediff(dd,SecurInstrmntStrtDt,		getdate()) >= 0)
and			(				SecurInstrmntEndDt			is null
			or	datediff(dd,SecurInstrmntEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.SecurInstrmntTypCdTbl
where		(	(sysdate -	SecurInstrmntStrtDt)		>= 0)
and			(				SecurInstrmntEndDt			is null
			or	(sysdate -	SecurInstrmntEndDt)			<= 0)</cfif>
order by	upper(description)
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

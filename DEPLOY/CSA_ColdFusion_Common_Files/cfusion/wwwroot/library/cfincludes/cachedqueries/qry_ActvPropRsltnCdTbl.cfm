<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropRsltnCdTbl)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropRsltnCdTbl" datasource="#Variables.db#">
	  select
      	PropRsltnCd,
      	PropRsltnDescTxt,
		PropRsltnCd									AS code,
		PropRsltnDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..PropRsltnCdTbl
	   where (datediff(dd,PropRsltnStrtDt,			getdate()) >= 0)
		   and	(PropRsltnEndDt		is null
		   or datediff(dd,PropRsltnEndDt,			getdate()) <= 0)<cfelse>
	   from	sbaref.PropRsltnCdTbl
       where		((sysdate -	PropRsltnStrtDt)	>= 0)
       		and		(PropRsltnEndDt	is null
			or	(sysdate -	PropRsltnEndDt)			<= 0)</cfif>
      order by code	  
</cfquery>

<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

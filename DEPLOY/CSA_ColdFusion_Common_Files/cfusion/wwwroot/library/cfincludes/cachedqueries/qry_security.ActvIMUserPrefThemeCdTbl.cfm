<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/24/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/05/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMUserPrefThemeCdTbl"	datasource="#Variables.db#">
select		IMUserPrefThemeCd,
			IMUserPrefThemeDescTxt,
			IMUserPrefThemeStrtDt,
			IMUserPrefThemeEndDt,
			IMUserPrefThemeCd									as displayorder,
			IMUserPrefThemeCd									as code,
			IMUserPrefThemeDescTxt								as description<cfif Variables.Sybase>
from		security..IMUserPrefThemeCdTbl
where		(	datediff(dd,IMUserPrefThemeStrtDt,				getdate()) >= 0)
and			(				IMUserPrefThemeEndDt				is null
			or	datediff(dd,IMUserPrefThemeEndDt,				getdate()) <= 0)<cfelse>
from		security.IMUserPrefThemeCdTbl
where		(	(sysdate -	IMUserPrefThemeStrtDt)				>= 0)
and			(				IMUserPrefThemeEndDt				is null
			or	(sysdate -	IMUserPrefThemeEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<cfset Variables.ScqGetThemes									= Variables.Scq.security.ActvIMUserPrefThemeCdTbl>
<cfset Variables.ScqNamedThemes									= "">
<cfloop query="Variables.ScqGetThemes">
	<cfset Variables.ScqNamedThemes								= ListAppend(Variables.ScqNamedThemes, "th" & Variables.ScqGetThemes.code)>
</cfloop>
<cfset Variables.Scq.security["ActvIMUserPrefListNamedThemes"]	= Variables.ScqNamedThemes>
<cfset StructDelete(Variables, "ScqGetThemes")>
<cfset StructDelete(Variables, "ScqNamedThemes")>

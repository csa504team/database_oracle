<!---
AUTHOR:				Sheen Justin
DATE:				08/15/2017. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	
--->

<cfinclude template="bld_preprocessing.cfm">

<cfset Variables.tempdb = Variables.db />
<cfset Variables.db		= "oracle_spc_caob1" />

<cfquery name="Variables.Scq.linc.ActvIndstryTypTbl" datasource="#Variables.db#">
	Select	b.BUSTYPCD,
			b.BUSTYPDESCTXT
	from	SBALINC.BusTypCdTbl b 
	WHERE 	b.ENDDT Is Null
	or		( ( sysdate - b.ENDDT ) <= 0)
</cfquery>

<cfset Variables.db		= Variables.tempdb />

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

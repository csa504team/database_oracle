<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblPer"	datasource="#Variables.db#">
select		IMCrdtScorSourcCd,
			IMCrdtScorSourcDescTxt,
			IMCrdtScorSourcLowNmb,
			IMCrdtScorSourcHighNmb,
			case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
			when IMCrdtScorSourcCd = 12					then 1
			when IMCrdtScorSourcCd = 13					then 2
			when IMCrdtScorSourcCd = 11					then 3
			when IMCrdtScorSourcCd = 14					then 4
			when IMCrdtScorSourcCd = 15					then 5
			else										6
			end											AS displayorder,
			IMCrdtScorSourcCd							AS code,
			IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMCrdtScorSourcTbl
where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
and			(				IMCrdtScorSourcEndDt		is null
			or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.IMCrdtScorSourcTbl
where		(	(sysdate -	IMCrdtScorSourcStrtDt)		>= 0)
and			(				IMCrdtScorSourcEndDt		is null
			or	(sysdate -	IMCrdtScorSourcEndDt)		<= 0)</cfif>
and			(IMCrdtScorSourcBusPerInd					= 'P')
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

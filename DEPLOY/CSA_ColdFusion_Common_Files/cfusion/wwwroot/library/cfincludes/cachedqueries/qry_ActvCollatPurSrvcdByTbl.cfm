<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NNI:	Original implementation.
								NS: changed CollatPurSrvcdByDesc to CollatPurSrvcdByDescTxt column
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurSrvcdByCdTbl"			datasource="#Variables.db#">
select		CollatPurSrvcdByCd,
			CollatPurSrvcdByDescTxt,
			CollatPurSrvcdByCd									AS code,
			CollatPurSrvcdByDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..CollatPurSrvcdByCdTbl
where		(	datediff(dd,CollatPurSrvcdByStartDt,				getdate()) >= 0)
and			(				CollatPurSrvcdByEndDt					is null
			or	datediff(dd,CollatPurSrvcdByEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CollatPurSrvcdByCdTbl
where		(	(sysdate -	CollatPurSrvcdByStartDt)				>= 0)
and			(				CollatPurSrvcdByEndDt					is null
			or	(sysdate -	CollatPurSrvcdByEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

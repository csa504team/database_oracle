<!---
AUTHOR:				Sushil Inaganti, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				07/01/2016
DESCRIPTION:		Does a single query into Variables.Scq.ActvCntctRsnCdTbl for subsequent caching. 
NOTES:
INPUT:
OUTPUT:
REVISION HISTORY:
--->

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname		= "Variables.Scq.ActvCntctRsnCdTbl">
<cfset Variables.Identifier		= "14">
<cfinclude template="/cfincludes/oracle/security/spc_CNTCTRSNCDSELTSP.PUBLIC.cfm">

<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/06/2016,	SL:		(OPSMDEV-999) change from inline query to stored procedure, because we moved Events from SBAREF to SECURITY schema.
					10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname			= "Variables.Scq.CAFSHomeEvntTbl">
<cfset Variables.Identifier			= "4">
<cfinclude template="/cfincludes/oracle/security/spc_EVNTSELTSP.PUBLIC.cfm" />
<cfif Variables.TxnErr>
	<cfset Variables.Scq.CAFSHomeEvntTbl = Variables.ErrMsg />
</cfif>
<!--- 
<cfquery name="Variables.Scq.CAFSHomeEvntTbl" datasource="#Variables.db#">
SELECT EVNTNM,
       EVNTDESCTXT,
       EVNTSTRTDT,
       EVNTENDDT,
       EVNTNM as code,
       EVNTDESCTXT as description
 FROM SBAREF.EVNTTBL
 WHERE TRUNC(NVL(EVNTENDDT,EVNTSTRTDT))>= trunc(sysdate)
 ORDER BY EVNTSTRTDT
</cfquery> --->
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

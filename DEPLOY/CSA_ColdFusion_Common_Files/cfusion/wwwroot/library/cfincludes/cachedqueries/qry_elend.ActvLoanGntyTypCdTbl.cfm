<!---
AUTHOR:				Chitralekha Pustakala, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/30/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/30/2016, PCL:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanGntyTypCdTbl" datasource="#Variables.db#">
select		LoanGntyTypCd,
		    LoanGntyTypTxt,
     		LoanGntyTypCdStrtDt,
		    LoanGntyTypCdEndDt,
		    CreatUserId,
		    CreatDt,
			LoanGntyTypCd		as code,
		    LoanGntyTypTxt		as description<cfif Variables.Sybase>
from		sbaref..LoanGntyTypCdTbl
where		(	datediff(dd,LoanGntyTypCdStrtDt,				getdate()) >= 0)
and			(				LoanGntyTypCdEndDt				is null
			or	datediff(dd,LoanGntyTypCdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanGntyTypCdTbl
where		(	(sysdate -	LoanGntyTypCdStrtDt)				>= 0)
and			(				LoanGntyTypCdEndDt				is null
			or	(sysdate -	LoanGntyTypCdEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
           
            
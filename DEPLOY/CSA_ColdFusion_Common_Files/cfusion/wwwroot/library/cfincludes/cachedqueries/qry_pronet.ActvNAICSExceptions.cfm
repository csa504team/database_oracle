<!---
AUTHOR:				Ian Clark, for the US Small Business Administration. 
DATE:				07/11/2013
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					07/11/2013, IAC:	Original implementation. 
									    Ad some point might want to limit to two most recent years 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.pronet.ActvNAICSExceptions"	datasource="#Variables.db#">
     Select Naicsyrnmb,Naicscd,Count(*) As Excptcount 
		From Sbaref.Naicsexcptntbl 
 		 Having Count(*) >1
 		GROUP BY Naicsyrnmb, Naicscd 
 		ORDER BY Naicsyrnmb,Naicscd 
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

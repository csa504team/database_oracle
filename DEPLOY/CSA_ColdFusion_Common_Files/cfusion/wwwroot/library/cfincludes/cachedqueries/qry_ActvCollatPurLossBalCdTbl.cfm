<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurLossBalCdTbl"	datasource="#Variables.db#">
	select		CollatPurLossBalCd,
		    	CollatPurLossBalDescTxt,		    	
		    	CollatPurLossBalCd									AS code,
				CollatPurLossBalDescTxt								AS description <cfif Variables.Sybase>
	from		sbaref..CollatPurLossBalCdTbl
	where		(	datediff(dd,CollatPurLossBalStrtDt,				getdate()) >= 0)
	and			(				CollatPurLossBalEndDt				is null
				or	datediff(dd,CollatPurLossBalEndDt,				getdate()) <= 0)<cfelse>
	from		sbaref.CollatPurLossBalCdTbl
	where		(	(sysdate -	CollatPurLossBalStrtDt)				>= 0)
	and			(				CollatPurLossBalEndDt				is null
				or	(sysdate -	CollatPurLossBalEndDt)				<= 0)</cfif>
	order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

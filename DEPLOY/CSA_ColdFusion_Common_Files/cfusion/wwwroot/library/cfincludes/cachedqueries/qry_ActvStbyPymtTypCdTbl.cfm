<!---
AUTHOR:				Sherry Liu 
DATE:				11/02/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
REVISION HISTORY:	11/02/2016, SL:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvStbyPymtTypCdTbl"	datasource="#Variables.db#">
select	STBYPYMTTYPCD,
		STBYPYMTTYPDESCTXT,
		STBYPYMTTYPCD			AS code,
		STBYPYMTTYPDESCTXT		AS description
from		sbaref.StbyPymtTypCdTbl
where		(	(sysdate -	STBYPYMTTYPCDSTRTDT)			>= 0)
and			(				STBYPYMTTYPCDENDDT				is null
			or	(sysdate -	STBYPYMTTYPCDENDDT)				<= 0)
order by	code       
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
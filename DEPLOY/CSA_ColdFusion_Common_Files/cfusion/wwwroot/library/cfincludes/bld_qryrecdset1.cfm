<!---

AUTHOR:				Nirish Namilae

DATE:				07/10/2008

DESCRIPTION:		This code can be used to replace getCFResultSet method.

NOTES:				This costructs recordset using the sql statment output from a java method.

INPUT:				Variables.QryResVar

OUTPUT:				Variables.QryResRecdSet.

CHECKED OUT TO:		N/A

CHECKED OUT DATE:	N/A

REVISION HISTORY:	07/11/2008, NNI:	Modified the code to use getColumnLabel method instead of getColumnName to get the

										columns alias names.

					07/11/2008, NNI:	Original Implementation.

 --->

<cfset Variables.Lib.QryResMetaData						= QryResVar.getMetaData()><!--- Input form User --->

<cfset Variables.Lib.QryResColNmList					= "">

<cfset Variables.Lib.QryResColAliasNmList				= "">

<cfset Variables.Lib.QryResColTypeList					= "">

<cfparam name="Variables.Lib.QryResColOptDataTyp"	default	="N">

<cfset Variables.Lib.QryResColumnCount					= Variables.Lib.QryResMetaData.getColumnCount()>


<cfloop from="1" to="#Variables.Lib.QryResColumnCount#" index="idx">

	<cfset Variables.Lib.QryResColNmList	= ListAppend(Variables.Lib.QryResColNmList,Variables.Lib.QryResMetaData.getColumnName(idx))>

	<cfset Variables.Lib.QryResColAliasNmList	= ListAppend(Variables.Lib.QryResColAliasNmList,Variables.Lib.QryResMetaData.getColumnLabel(idx))>

	<cfif Variables.Lib.QryResColOptDataTyp EQ "Y">

		<cfswitch expression="#Variables.Lib.QryResMetaData.getColumnType(idx)#">

			<cfcase value="93"><cfset Variables.Lib.QryResColTypeListVlu	= "Date"></cfcase>

			<cfdefaultcase><cfset Variables.Lib.QryResColTypeListVlu		= "VarChar"></cfdefaultcase>

		</cfswitch>

		<cfset Variables.Lib.QryResColTypeList	= ListAppend(Variables.Lib.QryResColTypeList,Variables.Lib.QryResColTypeListVlu)>

	</cfif>

</cfloop>


<cfif Variables.Lib.QryResColOptDataTyp EQ "Y">

	<cfset Variables.Lib.QryResRecdSet 			= QueryNew(Variables.Lib.QryResColNmList, Variables.Lib.QryResColTypeList)>

	<cfset Variables.Lib.QryResRecdSetByAliasNm	= QueryNew(Variables.Lib.QryResColAliasNmList, Variables.Lib.QryResColTypeList)>

<cfelse>

	<cfset Variables.Lib.QryResRecdSet 				= QueryNew(Variables.Lib.QryResColNmList)>

	<cfset Variables.Lib.QryResRecdSetByAliasNm 	= QueryNew(Variables.Lib.QryResColAliasNmList)>

</cfif>

<cfloop condition="QryResVar.next()">

 	<cfset newRow = QueryAddRow(Variables.Lib.QryResRecdSetByAliasNm, 1)>

	<cfset Variables.ColIdxLbl	= 0><!--- Points to actual column name of the alias name counterpart --->

	<!--- The getString() does not recognize the alias names, so the actual column has be passed.--->

	<cfloop list="#Variables.Lib.QryResColAliasNmList#" index="ColIdx">

		<cfset Variables.ColIdxLbl	= Variables.ColIdxLbl +1>

		<cfset temp = QuerySetCell(Variables.Lib.QryResRecdSetByAliasNm, "#ColIdx#", QryResVar.getString("#ListGetAt(Variables.Lib.QryResColNmList,ColIdxLbl)#"))>

	</cfloop>

</cfloop>

<cfset Variables.QryResRecdSet	= Variables.Lib.QryResRecdSetByAliasNm><!--- Output to the user  --->
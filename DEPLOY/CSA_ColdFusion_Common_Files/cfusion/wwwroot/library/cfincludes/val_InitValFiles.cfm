<!---
AUTHOR:				Steve Seaquist
DATE:				01/15/2002
DESCRIPTION:		Initialize use of val files. 
NOTES:				None
INPUT:				None
OUTPUT:				Variables-scope variables
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	01/15-22/2002, SRS:	Original implementation.
--->
<CFSET Variables.BlankScreen						= "Yes">
<CFSET Variables.ErrMsg								= "Error(s) occurred.">
<CFSET Variables.SaveMe								= "Yes">
<CFSET Variables.URLString							= "">

<!---
AUTHOR:				Sheen Mathew Justin, Trusted Mission Solutions, Inc. for the US Small Business Administration.
DATE:				01/18/2017.
DESCRIPTION:		New ColdFusion only component to replace JBOSS Partner Object Calls.
NOTES:				None.
INPUT:				Each method requires username, password, and db.
OUTPUT:				the requested query method.
REVISION HISTORY:	04/21/2017, HA:		(CAFSOPER 715) Added condition to partnerSearch .
					02/28/2017, ML:		(OPSMDEV-1380) Added new function for the partnerSearch
					12/14/2016, SMJ:	(OPSMDEV-1059) Original implementation
--->
<cfcomponent>
	
	<cfset Variables.username			= "" />
	<cfset Variables.password			= "" />
	<cfset Variables.db					= "oracle_housekeeping" />
	<cfset Variables.LibURL				= "/library" />
	<cfset Variables.LibUdfURL			= "#LibURL#/udf" />
	<cfset Variables.SchemaName			= "partner" />
	
	<cffunction name = "getLocHistoryByPartner" returnType = "query">
		
		<cfargument name = "prtId" 			default = "" />
		<cfargument name = "effectiveDate" 	default = "" />
		<cfargument name = "chngRsnCode" 	default = "" />
		<cfargument name = "srchTyp" 		default = "" />

		<cfset Variables.prtId 				= Arguments.prtId />
		<cfset Variables.effectiveDate 		= Arguments.effectiveDate />
		<cfset Variables.chngRsnCode 		= Arguments.chngRsnCode />
		<cfset Variables.srchTyp 			= Arguments.srchTyp />
		<cfif Variables.srchTyp eq "report29">
			<cfquery name="getLocHistoryByPartner" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
                SELECT LOCID
                , LOCHISTRYAFFCTPRTID PrtHistryInitPrtId
                , LOCHISTRYEFFDT as PrtHistryEffDt
                , VALIDCHNGCD
                , LOCHISTRYPARTYTYP
                , LOCHISTRYPARTYTYP as PrtHistryPartyTyp
                , LOCHISTRYINITPRTID as PrtHistryResultingPrtId
                , LOCHISTRYRESULTINGPRTID
                , LOCHISTRYCHNGDESC
                , LOCHISTRYLOCNM
                , LOCHISTRYLOCTYP as PrtHistryChrtrTyp
                , LOCHISTRYCTYNM
                , LOCHISTRYCNTCD
                , LOCHISTRYFIPSCNTYCD
                , LOCHISTRYPOSTCD
                , LOCHISTRYSTCD
                , LOCHISTRYSTR1TXT
                , LOCHISTRYSTR2TXT
                , LOCHISTRYUSDPNDYIND
                , LOCHISTRYTFPHISTRYDT
                , VALIDDATASOURCCD
                , AFFECTED_INST_ID
                , LOC_ID
                , EFFECTIVE_DATE
                , PARTY_TYPE_INDICATOR
                , CHANGE_CODE
                , INITIATOR_INST_ID
                , STATUS
                , LOCHISTRYCREATUSERID
                , LOCHISTRYCREATDT
                , LASTUPDTUSERID
                , LASTUPDTDT
                FROM partner.LocHistryTbl
                WHERE LocHistryAffctPrtId = <cfqueryparam value = "#Variables.PrtId#" CFSQLType = "CF_SQL_NUMERIC" />
                ORDER BY LocHistryEffDt DESC
			</cfquery>
		<cfelse>
			<cfif Variables.srchTyp eq "forPartnerHistory">
				<cfset Variables.Identifier		= 0 />
			<cfelse>
				<cfset Variables.Identifier		= 1 />
			</cfif>
			<cfset Variables.procName			= "LocHistryByPrtSelCSP" />
			<cfset Variables.LogAct				= "get Location History By Partner" />
			<cfset Variables.cfprname			= "getLocHistoryByPartner" />
			<cfinclude template="#spcn('Variables.procName', 'Variables.SchemaName')#" />
		</cfif>
		
		<cfreturn getLocHistoryByPartner />

	</cffunction>
	
	<cffunction name = "getPartnerByPartnerID" returnType = "query">
		
		<cfargument name = "PrtId" 		default = "" />
		
		<cfquery name="getPartnerByPartnerID" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
            SELECT    p.PrtId
                    , p.PrtLglNm
                    , p.PrtLglNm as LoanAppPrtNm
                    , p.ValidPrtPrimCatTyp
                    , p.PrtLocNm
                    , p.ValidLocTyp
                    , p.PhyAddrStr1Txt
                    , p.PhyAddrCtyNm
                    , p.PhyAddrStCd
                    , p.PhyAddrPostCd
                    , l.LocId
                    , p.PrtLocFIRSNmb
                    , p.PrtLglSrchNm
            FROM      partner.PrtSrchTbl p
                    , partner.LocSrchTbl l
            WHERE    1              = 1
            AND      p.PrtId        = <cfqueryparam value = "#Arguments.PrtId#" CFSQLType = "CF_SQL_NUMERIC" />
            AND      p.PrtId        = l.PrtId
            AND      p.PrtCurStatCd = 'O'
		</cfquery>
		
		<cfreturn getPartnerByPartnerID />
	</cffunction>
	
	<cffunction name = "getPartnerByProgram" returnType = "query">
		
		<cfargument name = "prgrmId" 	default = "" />
		<cfargument name = "operState" 	default = "" />
		<cfargument name = "ofcCd" 		default = "" />
		<cfargument name = "cntyCd" 	default = "" />
		<cfargument name = "prtStatus" 	default = "" />

		<cfset Variables.prgrmId 		= Arguments.prgrmId />
		<cfset Variables.operState 		= Arguments.operState />
		<cfset Variables.ofcCd 			= Arguments.ofcCd />
		<cfset Variables.cntyCd 		= Arguments.cntyCd />
		<cfset Variables.prtStatus 		= Arguments.prtStatus />
		
		<cfif IsNumeric(Variables.cntyCd)>
			<cfset Variables.cntyCd		= NumberFormat(Variables.cntyCd, "999") />
		<cfelse>
			<cfset Variables.cntyCd		= UCase(Variables.cntyCd) />
		</cfif>
		
		<cfquery name="getPartnerByProgram" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
            SELECT DISTINCT  p.PrtId
                            ,p.PrtLglNm
                            ,p.ValidPrtPrimCatTyp
                            ,p.PrtLocNm
                            ,p.ValidLocTyp
                            ,p.PhyAddrStr1Txt
                            ,p.PhyAddrCtyNm
                            ,p.PhyAddrStCd
                            ,p.PhyAddrPostCd
                            ,p.LocId
                            ,p.PrtLocFIRSNmb
                    FROM     partner.PrtSrchTbl p
                            ,partner.PrtAgrmtTbl pa
                            
                            <cfif Variables.operState neq "">
                            ,partner.PrtAreaOfOperTbl ao
                            </cfif>
                            
                            <cfif Variables.cntyCd neq "" or ( Variables.OfcCd neq "" and Variables.operState neq "" )>
                            ,sbaref.CntyTbl c
                            </cfif>
                            
                            <cfif Variables.ofcCd neq "" and Variables.operState eq "">
                            ,partner.PrtAreaOfOperTbl ao
                            ,sbaref.CntyTbl c
                            </cfif>
                            
                    WHERE    pa.PrtId          = p.PrtId
                    AND      pa.PrgrmId        = <cfqueryparam value = "#Variables.prgrmId#"   CFSQLType = "CF_SQL_NUMERIC" />
                    
                    <cfif Variables.operState neq "">
                    AND      pa.PrtId          = ao.PrtId
                    AND      pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
                    AND      ao.StCd           = <cfqueryparam value = "#Variables.operState#" CFSQLType = "CF_SQL_VARCHAR" />
                    </cfif>
                    
                    <!--- Only include Active Areas of Operation --->
                    <cfif Variables.operState neq "" or ( Variables.OfcCd neq "" and Variables.operState eq "" )>
                    AND      ao.PrtAreaOfOperCntyEndDt IS NULL
                    </cfif>
                    
                    <!--- County --->
                    <cfif Variables.cntyCd neq "">
                    <cfif IsNumeric(Variables.cntyCd)>
                    AND      ao.CntyCd         = <cfqueryparam value = "#Variables.cntyCd#"    CFSQLType = "CF_SQL_NUMERIC" />
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    <cfelse>
                    AND      c.CntyNm       like <cfqueryparam value = "#Variables.cntyCd#%"   CFSQLType = "CF_SQL_VARCHAR" />
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    </cfif>
                    </cfif>
                    
                    <cfif Variables.ofcCd neq "" and Variables.operState eq "">
                    AND      pa.PrtId          = ao.PrtId
                    AND      pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    AND      c.OrignOfcCd      = <cfqueryparam value = "#Variables.ofcCd#"     CFSQLType = "CF_SQL_NUMERIC" />
                    <cfelseif  Variables.ofcCd neq "" and Variables.operState neq "">
                    <cfif Variables.cntyCd eq "">
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    AND      c.OrignOfcCd      = <cfqueryparam value = "#Variables.ofcCd#"     CFSQLType = "CF_SQL_NUMERIC" />
                    <cfelse>
                    AND      c.OrignOfcCd      = <cfqueryparam value = "#Variables.ofcCd#"     CFSQLType = "CF_SQL_NUMERIC" />
                    </cfif>
                    </cfif>
                    
                    <cfif Variables.statusCd eq "A">
                    AND      pa.PrtAgrmtEffDt <= sysdate
                    AND (
                            (pa.PrtAgrmtExprDt>= sysdate OR pa.PrtAgrmtExprDt is NULL)
                            AND
                            (pa.PrtAgrmtTermDt > sysdate OR pa.PrtAgrmtTermDt is NULL)
                        )
                    <cfelseif Variables.statusCd eq "I">
                    AND (
                            pa.PrtAgrmtExprDt < sysdate
                            AND pa.PrtAgrmtTermDt is NULL
                            AND
                            pa.PrtAgrmtSeqNmb = (
                                                    SELECT Max(b.PrtAgrmtSeqNmb)
                                                    FROM   partner.PrtAgrmtTbl b
                                                    WHERE pa.PrtId = b.PrtId
                                                    AND b.PrgrmId = <cfqueryparam value = "#Variables.prgrmId#" CFSQLType = "CF_SQL_NUMERIC" />
                                                )
                        )
                    <cfelseif  Variables.statusCd eq "T">
                    AND     pa.PrtAgrmtTermDt<= sysdate
                    <cfelseif  Variables.statusCd eq "P">
                    AND     pa.PrtAgrmtEffDt  > sysdate
                    </cfif>
                    
                    <cfif Variables.prtStatus neq "">
                    AND     p.PrtCurStatCd = <cfqueryparam value = "#Variables.prtStatus#"     CFSQLType = "CF_SQL_VARCHAR" />
                    </cfif>
                    
                    ORDER BY p.PrtLglNm
		</cfquery>
		
		<cfreturn getPartnerByProgram />
	</cffunction>
	
	<cffunction name = "getPartnerHistory" returnType = "query">
		
		<cfargument name = "prtId" 		default = "" />
		<cfargument name = "chngCd" 	default = "" />
		<cfargument name = "startDate" 	default = "" />
		<cfargument name = "endDate" 	default = "" />
		<cfargument name = "srchTyp" 	default = "" />
		<cfargument name = "chngOrdNm" 	default = "" />
		
		<cfset Variables.prtId 			= Arguments.prtId />
		<cfset Variables.chngCd 		= Arguments.chngCd />
		<cfset Variables.startDate 		= Arguments.startDate />
		<cfset Variables.endDate 		= Arguments.endDate />
		<cfset Variables.srchTyp 		= Arguments.srchTyp />
		<cfset Variables.chngOrdNm 		= Arguments.chngOrdNm />
		
		<cfif Variables.srchTyp eq "byChngCd" and Variables.chngOrdNm neq 0>
			
			<cfif not IsDefined("spcn")>
				<cfinclude template = "#LibUdfURL#/bld_dbutils_for_dbtype.cfm">
			</cfif>
			
			<cfset Variables.procName		= "" />
			
			<cfif Variables.chngOrdNm eq 1>
				<cfset Variables.procName		= "PrtHistARecrdCSP" />
			<cfelseif Variables.chngOrdNm eq 2>
				<cfset Variables.procName		= "PrtHistAIRecrdCSP" />
			<cfelseif Variables.chngOrdNm eq 3>
				<cfset Variables.procName		= "PrtHistARRecrdCSP" />
			<cfelseif Variables.chngOrdNm eq 4>
				<cfset Variables.procName		= "PrtHistAIRRecrdCSP" />
			</cfif>
			
			<cfset Variables.LogAct				= "get Partner History" />
			<cfset Variables.cfprname			= "getPartnerHistory" />
			<cfinclude template="#spcn('Variables.procName', 'Variables.SchemaName')#" />
			
			<cfreturn getPartnerHistory />
		</cfif>
		
		<cfquery name="getPartnerHistory" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
			
			<cfif Variables.srchTyp eq "byDate">
				SELECT DISTINCT  p.ValidChngCd 
					   , v.ValidChngCdDesc
					   , v.ValidChngCdDispOrdNmb 
					   , count( distinct p.PrtId) as PrtNum
				FROM     partner.PrtHistryTbl p
					   , partner.ValidChngCdTbl v
				WHERE    p.PrtHistryCreatDt like (substr(sysdate,1,11)||'%')
				AND      v.ValidChngCd = p.ValidChngCd
				GROUP BY p.ValidChngCd
					   , v.ValidChngCdDesc
				ORDER BY p.ValidChngCd
			</cfif>
			
			
			<cfif Variables.srchTyp eq "byChngCd" and Variables.chngOrdNmb eq 0>
				SELECT    p.PrtHistryLglNm as AffectedTitle
						, To_char(p.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate
						, p.PrtHistryCreatDt as DateAddedToPIMS
						, p.ValidChngCd
				FROM      partner.PrtHistryTbl p 
				WHERE     p.PrtHistryCreatDt like  (substr(sysdate,1,11)||'%') 
			</cfif>
			
			<cfif Variables.srchTyp eq "byPrtId">
				SELECT DISTINCT v.ValidChngCdDesc
						, p.PrtHistryLglNm
						, p.PrtHistryPartyTyp
						, To_char(p.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate
						, p.PrtHistryInitPrtId
						, p.PrtHistryResultingPrtId
				FROM      partner.PrtHistryTbl p 
						, partner.ValidChngCdTbl v 
				WHERE     p.PrtId = <cfqueryparam value = "#Variables.prtId#" CFSQLType = "cf_sql_integer" />
				AND       p.PrtHistryCreatDt <= to_date(<cfqueryparam value = "#Variables.endDate#" CFSQLType = "cf_sql_varchar" />,'MM/DD/YYYY')
				ORDER BY  p.PrtHistryEffDt 
			</cfif>
			
			<cfif Variables.srchTyp eq "forPartnerHistory">
				SELECT    i.PrtId as InitiatorPrtId
						, i.PrtHistryLglNm as InitiatorTitle
						, v.ValidChngCdDesc
						, a.PrtId as AffectedPrtId
						, a.PrtHistryLglNm as AffectedTitle
						, r.PrtId as ResultingPrtId
						, r.PrtHistryLglNm as ResultingTitle
						, To_char(a.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate
						, a.ValidChngCd
						, v.ValidChngCdDispOrdNmb
						, a.PrtHistryCreatDt as DateAddedToPIMS
						, a.PrtHistryChrtrTyp as AffectedCharter
						, r.PrtHistryChrtrTyp as ResultingCharter
						, a.PrtHistryReglAgencyNm as AffectedRegulator
						, i.PrtHistryReglAgencyNm as InitiatorRegulator
						, r.PrtHistryReglAgencyNm as ResultingRegulator
						, a.ValidDataSourcCd
				FROM      partner.PrtHistryTbl a
				LEFT OUTER JOIN             partner.PrtHistryTbl i
				ON                a.PrtId = i.PrtId
				AND      a.PrtHistryEffDt = i.PrtHistryEffDt
				AND   i.PrtHistryPartyTyp = 'I'
				AND         a.ValidChngCd = i.ValidChngCd
				LEFT OUTER JOIN             partner.PrtHistryTbl r
				ON                a.PrtId = r.PrtId
				AND      a.PrtHistryEffDt = r.PrtHistryEffDt
				AND   r.PrtHistryPartyTyp = 'R'
				AND         a.ValidChngCd = r.ValidChngCd
				LEFT OUTER JOIN             partner.ValidChngCdTbl v
				ON          a.ValidChngCd = v.ValidChngCd
				WHERE a.PrtHistryPartyTyp = 'A'
				
				<cfif Variables.prtId neq "">
					AND a.PrtId = <cfqueryparam value = "#Variables.prtId#" CFSQLType = "cf_sql_integer" />
				</cfif>
				
				<cfif Variables.startDate neq "" and Variables.endDate neq "">
					AND a.PrtHistryEffDt >= to_date(<cfqueryparam value = "#Variables.startDate#" CFSQLType = "cf_sql_varchar" />,'MM/DD/YYYY')
					AND a.PrtHistryEffDt <= to_date(<cfqueryparam value = "#Variables.endDate#"   CFSQLType = "cf_sql_varchar" />,'MM/DD/YYYY')
				</cfif>
				
				<cfif Variables.chngCd neq "">
					AND a.ValidChngCd = <cfqueryparam value = "#Variables.chngCd#" CFSQLType = "cf_sql_varchar" />
				</cfif>
				
				ORDER BY a.PrtHistryEffDt DESC
			</cfif>
			
			<cfif Variables.srchTyp eq "editPartnerHistory">
				SELECT    PrtId
						, PrtHistryEffDt
						, ValidChngCd
						, PrtHistryPartyTyp
						, PrtHistryInitPrtId
						, PrtHistryResultingPrtId
						, PrtHistryChrtrTyp
						, PrtHistryEstbDt
						, PrtHistryLglNm
						, PrtHistryPrimTyp
						, PrtHistrySubCatTyp
						, PrtHistryReglAgencyNm
						, ValidDataSourcCd
				FROM  partner.PrtHistryTbl
				WHERE PrtId          = <cfqueryparam value = "#Variables.prtId#" CFSQLType = "cf_sql_integer" />
				AND   PrtHistryEffDt = to_date(<cfqueryparam value = "#Variables.startDate#" CFSQLType = "cf_sql_varchar" />,'MM/DD/YYYY')
				AND   ValidChngCd    = <cfqueryparam value = "#Variables.chngCd#" CFSQLType = "cf_sql_varchar" />
			</cfif>
		</cfquery>
		
		<cfreturn getPartnerHistory />
	</cffunction>
	
	<cffunction name = "partnerSearch" returnType = "query">
		<cfargument name="legalNm" 				default="">
		<cfargument name="lglNmSrchInd" 		default="">
		<cfargument name="subcategoryTyp" 		default="">
		<cfargument name="cityNm" 				default="">
		<cfargument name="cityInd" 				default="">
		<cfargument name="stateCd" 				default="">
		<cfargument name="zipCd" 				default="">
		<cfargument name="zipSrchInd" 			default="">
		<cfargument name="curStatCd"			default="">
		<cfargument name="lglSrchNm"			default="">
		<cfargument name="officeCd" 			default="">
		<cfargument name="regionCd" 			default="">
		<cfargument name="cntyCd" 				default="">
		<cfargument name="congDistCd"			default="">
		<cfargument name="programId"			default="">
		<cfargument name="prgrmStatCd"			default="">
		<cfargument name="queryTyp"				default="">
		<cfargument name="rowCount"				default="">
		<cfargument name="prtrId"				default="">
		
		<cfset variables.legalNm 				= arguments.legalNm>
		<cfset variables.lglNmSrchInd			= arguments.lglNmSrchInd>
		<cfset variables.subcategoryTyp			= arguments.subcategoryTyp>
		<cfset variables.cityNm					= arguments.cityNm>
		<cfset variables.cityInd				= arguments.cityInd>
		<cfset variables.stateCd				= arguments.stateCd>
		<cfset variables.zipCd					= arguments.zipCd>
		<cfset variables.zipSrchInd				= arguments.zipSrchInd>
		<cfset variables.officeCd				= arguments.officeCd>
		<cfset variables.regionCd				= arguments.regionCd>
		<cfset variables.cntyCd					= arguments.cntyCd>
		<cfset variables.congDistCd				= arguments.congDistCd>
		<cfset variables.curStatCd				= arguments.curStatCd>
		<cfset variables.programId				= arguments.programId>
		<cfset variables.prgrmStatCd			= arguments.prgrmStatCd>
		<cfset variables.queryTyp				= arguments.queryTyp>
		<cfset variables.lglSrchNm				= arguments.lglSrchNm>
		<cfset variables.rowCount				= arguments.rowCount>
		<cfset variables.prtrId					= arguments.prtrId>
		
		<cfquery name="partnerSearch" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#" result="qryResult">
			SELECT   ROWNUM		
		            ,p.PrtId			
		            ,p.PrtLglNm			
		            ,p.ValidPrtPrimCatTyp		
		            ,p.ValidPrtSubCatTyp		
		            ,p.PrtLocNm			
		            ,p.ValidLocTyp		
		            ,p.PhyAddrStr1Txt		
		            ,p.PhyAddrCtyNm		
		            ,p.PhyAddrStCd		
		            ,p.PhyAddrPostCd		
		            ,p.LocId			
		            ,p.PrtLocFIRSNmb		
		            ,p.LocQty			
		            ,p.PrtLglSrchNm		
		            ,p.PhyAddrCtySrchNm		
		            ,p.prtCurStatCd
		            ,(CASE p.PhyAddrCtyNm
		              WHEN NULL THEN 'ZZZZZZZZZZ'
		               ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId)
		               END ) AS StateCity
			<cfif Variables.programId NEQ "">
		    	FROM    partner.PrtSrchTbl p,
		        		partner.PrtAgrmtTbl a
		    <cfelse>
		    	FROM    partner.PrtSrchTbl p
		    </cfif>
		    	WHERE 1=1
		    <cfif variables.legalNm NEQ ""><!---	legal Name		--->
		        <cfset variables.legalNm = Ucase(variables.legalNm)>
		    	<cfif variables.lglNmSrchInd EQ "C"> 
		            AND p.PrtLglSrchNm like <cfqueryparam value="%#variables.legalNm#%" cfsqltype="cf_sql_varchar">
		        <cfelseif variables.lglNmSrchInd EQ "S">
		        	AND p.PrtLglSrchNm like <cfqueryparam value="#variables.legalNm#%" cfsqltype="cf_sql_varchar">
		        <cfelse>  
		        	AND p.PrtLglSrchNm = <cfqueryparam value="#variables.legalNm#" cfsqltype="cf_sql_varchar">  
		    	</cfif>
		    </cfif>
		        
		    <cfif variables.subcategoryTyp NEQ ""><!---	Sub Cat type	--->
		    	<cfif find(",", subcategoryTyp) GT 0>
		        	AND p.ValidPrtSubCatTyp IN <cfqueryparam value="#variables.subcategoryTyp#" cfsqltype="cf_sql_varchar">
		        <cfelse>
		        	AND p.ValidPrtSubCatTyp = <cfqueryparam value="#variables.subcategoryTyp#" cfsqltype="cf_sql_varchar">
		        </cfif>
		    </cfif>    
		    
		    <cfif variables.cityNm NEQ ""><!---	City	--->
		        <cfset variables.cityNm = Ucase(variables.cityNm)>
		        <cfif variables.cityInd EQ "C"> 
		            AND p.PhyAddrCtySrchNm like <cfqueryparam value="%#variables.cityNm#%" cfsqltype="cf_sql_varchar">
		        <cfelseif variables.cityInd EQ "S">
		        	AND p.PhyAddrCtySrchNm like <cfqueryparam value="#variables.cityNm#%" cfsqltype="cf_sql_varchar">
		        <cfelse>  
		        	AND p.PhyAddrCtySrchNm = <cfqueryparam value="#variables.cityNm#" cfsqltype="cf_sql_varchar">  
		    	</cfif>
		    </cfif>
		    
		    <cfif variables.stateCd NEQ ""><!---	State	--->
		    	<cfif find(",", variables.stateCd) GT 0>
		        	AND p.PhyAddrStCd IN <cfqueryparam value="#variables.stateCd#" cfsqltype="cf_sql_varchar">
		        <cfelse>
		        	AND p.PhyAddrStCd = <cfqueryparam value="#variables.stateCd#" cfsqltype="cf_sql_varchar">
		        </cfif>
		    </cfif>
		    
		    <cfif variables.zipCd NEQ ""><!---	Zip Code	--->
		        <cfif variables.zipSrchInd EQ "C"> 
		            AND p.PhyAddrPostCd like <cfqueryparam value="%#variables.zipCd#%" cfsqltype="cf_sql_varchar">
		        <cfelseif variables.zipSrchInd EQ "S">
		        	AND p.PhyAddrPostCd like <cfqueryparam value="#variables.zipCd#%" cfsqltype="cf_sql_varchar">
		        <cfelse>  
		        	AND p.PhyAddrPostCd = <cfqueryparam value="#variables.zipCd#" cfsqltype="cf_sql_varchar">  
		    	</cfif>
		    </cfif>
		    
		    <cfif variables.officeCd NEQ ""><!---	Office Code		--->
		    	<cfif find(",", variables.officeCd) GT 0>
		        	AND  p.OrignOfcCd IN <cfqueryparam value="#variables.officeCd#" cfsqltype="cf_sql_integer" list="Yes">
		        <cfelse>
		        	AND  p.OrignOfcCd = <cfqueryparam value="#variables.officeCd#" cfsqltype="cf_sql_integer">    
		        </cfif>
		    </cfif>
		    
		    <cfif variables.regionCd NEQ ""><!---	Region Code		--->
		    	AND  p.OrignOfcCd like <cfqueryparam value="#variables.regionCd#%" cfsqltype="cf_sql_varchar">
		    </cfif>
		    
		    <cfif variables.cntyCd NEQ ""><!---		County		--->
		    	<cfset variables.cnty = "">
		        <cfif len(variables.cntyCd) EQ 1>
		        	<cfset variables.cnty = variables.cntyCd>
		        <cfelse>
		        	<cfset variables.cnty = mid(variables.cntyCd, 2, 1)>
		        </cfif> 
		        <cfset variables.checkCounty = "0,1,2,3,4,5,6,7,8,9">
		        <cfset variables.checkCounty = ListFind(variables.checkCounty, variables.cnty)>
		        <cfif variables.checkCounty NEQ 0>
		        	<cfif len(variables.cntyCd) EQ 1>
		            	<cfset variables.cntyCd = 00 & variables.cntyCd>
		                AND p.CntyCd = <cfqueryparam value="#variables.cntyCd#" cfsqltype="cf_sql_integer">
		            </cfif>
		        <cfelse>
		        	AND p.CntyNm like <cfqueryparam value="#UCASE(variables.cntyCd)#%" cfsqltype="cf_sql_varchar"><!---	code from java ???????????		--->
		        </cfif>
		    </cfif>
		    
		    <cfif variables.congDistCd NEQ ""><!---		Congressional District		--->
		    	AND  p.CongrsnlDistNmb = <cfqueryparam value="#variables.congDistCd#" cfsqltype="cf_sql_integer">
		    </cfif>
		    
		    <cfif variables.curStatCd NEQ ""><!---		Status Code		--->
		    	AND p.PrtCurStatCd =  <cfqueryparam value="#curStatCd#" cfsqltype="cf_sql_varchar">
		    </cfif>
		    
		    <cfif variables.programId NEQ ""><!---		Program Id		--->
		    	<cfif variables.prgrmStatCd EQ "A"><!---		A		--->
		        	<cfif find("'", variables.programId) GT 0 OR find(",", variables.programId) GT 0>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">)
		                AND a.PrtAgrmtEffDt <= sysdate
		                AND ((a.PrtAgrmtExprDt >= sysdate OR a.PrtAgrmtExprDt is NULL ) AND (a.PrtAgrmtTermDt > sysdate OR a.PrtAgrmtTermDt is NULL))
		            <cfelse>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId = <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">
		                AND a.PrtAgrmtEffDt <= sysdate
		                AND ((a.PrtAgrmtExprDt >= sysdate OR a.PrtAgrmtExprDt is NULL ) AND (a.PrtAgrmtTermDt > sysdate OR a.PrtAgrmtTermDt is NULL))
		            </cfif>
		        <cfelseif variables.prgrmStatCd EQ "I"><!---		I		--->
		        	<cfif find("'", variables.programId) GT 0 OR find(",", variables.programId) GT 0>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">)
		                AND ((a.PrtAgrmtExprDt < sysdate AND a.PrtAgrmtTermDt is NULL ) 
		                	AND a.PrtAgrmtSeqNmb=(SELECT Max(b.PrtAgrmtSeqNmb) 
		                    					  FROM partner.PrtAgrmtTbl b 
		                                          WHERE a.PrtId = b.PrtId 
		                                          AND b.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar" list="yes">)
		                    AND (a.PrtAgrmtTermDt > sysdate OR a.PrtAgrmtTermDt is NULL))
		            <cfelse>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId = (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">)
		                AND (a.PrtAgrmtExprDt < sysdate  AND a.PrtAgrmtTermDt is NULL 
						AND a.PrtAgrmtSeqNmb=(SELECT Max(b.PrtAgrmtSeqNmb) 
		                					  FROM partner.PrtAgrmtTbl b 
		                                      WHERE a.PrtId = b.PrtId 
		                                      AND b.PrgrmId = <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">))
		            </cfif>
		        <cfelseif variables.prgrmStatCd EQ "T"><!---		T		--->        
		        	<cfif find("'", variables.programId) GT 0 OR find(",", variables.programId) GT 0>
		            	AND p.PrtId = a.PrtId 
		                AND a.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar" list="yes">)
		                AND a.PrtAgrmtTermDt <=sysdate 
		            <cfelse>
		            	AND p.PrtId = a.PrtId 
		                AND a.PrgrmId = (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">)
						AND a.PrtAgrmtTermDt <=sysdate 
		            </cfif>
		        <cfelseif variables.prgrmStatCd EQ "P"><!---		P		--->      
		        	<cfif find("'", variables.programId) GT 0 OR find(",", variables.programId) GT 0>
		            	AND p.PrtId = a.PrtId 
		                AND a.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar" list="Yes">)
		                AND a.PrtAgrmtEffDt > sysdate
		            <cfelse>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId = <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">
		                AND a.PrtAgrmtEffDt > sysdate
		            </cfif>        	  
		        <cfelse>
		        	<cfif find("'", variables.programId) GT 0 OR find(",", variables.programId) GT 0>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId IN (<cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar" list="yes">)
		            <cfelse>
		            	AND p.PrtId = a.PrtId
		                AND a.PrgrmId = <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">
		            </cfif>
		        </cfif>
		    </cfif>
		    <cfif variables.queryTyp EQ "next">
				
		    	AND (p.PrtLglSrchNm > <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">
					OR p.PrtLglSrchNm = <cfqueryparam value="#variables.programId#" cfsqltype="cf_sql_varchar">)
				AND (CASE WHEN p.PhyAddrCtyNm is Null
					THEN 'ZZZZZZZZZZ'
					ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId)
					END) > <cfqueryparam value="#variables.stateCity#" cfsqltype="cf_sql_varchar">
		    </cfif>
		</cfquery>
		<cfreturn partnerSearch />
	</cffunction>
	
	<cffunction name = "searchPartner" returnType = "query">
		<cfargument name="PartnerName" 				default="" />
		<cfargument name="PartnerNameScope" 		default="" />
		<cfargument name="PartnerType" 				default="" />
		<cfargument name="PartnerCityName" 			default="" />
		<cfargument name="PartnerCityNameScope" 	default="" />
		<cfargument name="PartnerStateCode" 		default="" />
		<cfargument name="PartnerZipCode" 			default="" />
		<cfargument name="PartnerZipCodeScope" 		default="" />
		<cfargument name="PrevLastPrtNm" 			default="" />
		<cfargument name="PrevLastPrtId" 			default="" />
		<cfargument name="QueryTyp" 				default="" />
		<cfargument name="MaxRows" 					default="" />
		
		<cfset Variables.searchPartner 				= this.SearchAllPartner(
																				Arguments.PartnerName
																				,Arguments.PartnerNameScope
																				,Arguments.PartnerType
																				,Arguments.PartnerCityName
																				,Arguments.PartnerCityNameScope
																				,Arguments.PartnerStateCode
																				,Arguments.PartnerZipCode
																				,Arguments.PartnerZipCodeScope
																				,Arguments.PrevLastPrtNm
																				,Arguments.PrevLastPrtId
																				,Arguments.QueryTyp
																				,Arguments.MaxRows
																				,""
																			) 
		/>

		<cfreturn Variables.searchPartner />
		
	</cffunction>
	
	<cffunction name = "searchAllPartner" returnType = "query">
        
        <cfargument name="PartnerName"				default="" />
        <cfargument name="PartnerNameScope"			default="" />
        <cfargument name="PartnerType"				default="" />
        <cfargument name="PartnerCityName"			default="" />
        <cfargument name="PartnerCityNameScope"		default="" />
        <cfargument name="PartnerStateCode"			default="" />
        <cfargument name="PartnerZipCode"			default="" />
        <cfargument name="PartnerZipCodeScope"		default="" />
        <cfargument name="PrevLastPrtNm"			default="" />
        <cfargument name="PrevLastPrtId"			default="" />
        <cfargument name="QueryTyp"					default="" />
        <cfargument name="MaxRows"					default="" />
        <cfargument name="CurrentStatusCode"		default="" />
        
        <cfset Variables.PartnerName				= Arguments.PartnerName />
        <cfset Variables.PartnerNameScope			= Arguments.PartnerNameScope />
        <cfset Variables.PartnerType				= Arguments.PartnerType />
        <cfset Variables.PartnerCityName			= Arguments.PartnerCityName />
        <cfset Variables.PartnerCityNameScope		= Arguments.PartnerCityNameScope />
        <cfset Variables.PartnerStateCode			= Arguments.PartnerStateCode />
        <cfset Variables.PartnerZipCode				= Arguments.PartnerZipCode />
        <cfset Variables.PartnerZipCodeScope		= Arguments.PartnerZipCodeScope />
        <cfset Variables.PrevLastPrtNm				= Arguments.PrevLastPrtNm />
        <cfset Variables.PrevLastPrtId				= Arguments.PrevLastPrtId />
        <cfset Variables.QueryTyp					= Arguments.QueryTyp />
        <cfset Variables.MaxRows					= Arguments.MaxRows />
        <cfset Variables.CurrentStatusCode			= Arguments.CurrentStatusCode />
        
        <cfquery name="PartnerAllCategory" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
        Select distinct VALIDPRTPRIMCATTYP
                   From Partner.PrtSrchTbl
        </cfquery>
        <cfset Variables.PrtCategoryTp = ValueList(PartnerAllCategory.VALIDPRTPRIMCATTYP)>
        <cfset Variables.PrtCategoryTp = FindNoCase(Variables.PartnerType, Variables.PrtCategoryTp)>
                    
        <cfquery name="SearchAllPartner" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#" maxrows="#Variables.MaxRows#">
            SELECT   ROWNUM
                    ,p.PrtId
                    ,p.PrtLglNm
                    ,p.ValidPrtPrimCatTyp
                    ,p.ValidPrtSubCatTyp
                    ,p.PrtLocNm
                    ,p.ValidLocTyp
                    ,p.PhyAddrStr1Txt
                    ,p.PhyAddrCtyNm
                    ,p.PhyAddrStCd
                    ,p.PhyAddrPostCd
                    ,p.LocId
                    ,p.PrtLocFIRSNmb
                    ,p.LocQty
                    ,p.PrtLglSrchNm
                    ,p.PHYADDRCTYSRCHNM
                    ,p.prtCurStatCd
            FROM     Partner.PrtSrchTbl p
            WHERE    1=1
            
            <cfif Variables.PartnerName NEQ ""><!---   Partner Name   --->
            <cfif Variables.PartnerNameScope EQ "S">
            AND      PRTLGLSRCHNM LIKE <cfqueryparam value = "#UCase(Variables.PartnerName)#%" CFSQLType = "CF_SQL_VARCHAR" />
            <cfelseif Variables.PartnerNameScope EQ "C">
            AND      PRTLGLSRCHNM LIKE <cfqueryparam value = "%#UCase(Variables.PartnerName)#%" CFSQLType = "CF_SQL_VARCHAR" />
            <cfelseif Variables.PartnerNameScope EQ "E">
            AND      PRTLGLSRCHNM    = <cfqueryparam value = "#UCase(Variables.PartnerName)#"  CFSQLType = "CF_SQL_VARCHAR" />
            </cfif>
            </cfif>
            AND      p.PrtCurStatCd = 'O'
            <cfif Variables.PartnerType NEQ "null"><!---              Partner Type      --->
            <cfif Variables.PrtCategoryTp EQ 0>
            AND      VALIDPRTSUBCATTYP  = <cfqueryparam value = "#Variables.PartnerName#"      CFSQLType = "CF_SQL_VARCHAR" />
            <cfelse>
            AND      VALIDPRTPRIMCATTYP = <cfqueryparam value = "#Variables.PartnerName#"      CFSQLType = "CF_SQL_VARCHAR" />
            </cfif>
            </cfif>
            
            <cfif Variables.PartnerCityName NEQ ""><!---            City        --->
                <cfif Variables.PartnerCityNameScope EQ "S">
                    AND PHYADDRCTYSRCHNM LIKE <cfqueryparam value = "#UCase(Variables.PartnerCityName)#%"  CFSQLType = "CF_SQL_VARCHAR" />
                <cfelseif Variables.PartnerCityNameScope EQ "C">
                    AND PHYADDRCTYSRCHNM LIKE <cfqueryparam value = "%#UCase(Variables.PartnerCityName)#%" CFSQLType = "CF_SQL_VARCHAR" />
                <cfelseif Variables.PartnerCityNameScope EQ "E">
                    AND PHYADDRCTYSRCHNM    = <cfqueryparam value = "#UCase(Variables.PartnerCityName)#"   CFSQLType = "CF_SQL_VARCHAR" />
                </cfif>
            </cfif>
            
            <cfif Variables.PartnerStateCode NEQ ""><!---State--->
            AND PHYADDRSTCD = <cfqueryparam value = "#Variables.PartnerStateCode#" CFSQLType = "CF_SQL_VARCHAR" />
            </cfif>
            
            <cfif Variables.CurrentStatusCode NEQ ""><!--- partner status code --->
            AND prtCurStatCd in (<cfqueryparam value = "#Variables.CurrentStatusCode#" CFSQLType = "CF_SQL_CHAR" list = "yes">)
            </cfif>
            
            <cfif Variables.PartnerZipCode NEQ ""><!--- ZIP --->
                <cfif Variables.PartnerZipCodeScope EQ "S">
                    AND PHYADDRPOSTCD LIKE <cfqueryparam value = "#Variables.PartnerZipCode#%" CFSQLType = "CF_SQL_VARCHAR" />
                <cfelseif Variables.PartnerZipCodeScope EQ "C">
                    AND PHYADDRPOSTCD LIKE <cfqueryparam value = "%#Variables.PartnerZipCode#%" CFSQLType = "CF_SQL_VARCHAR" />
                <cfelseif Variables.PartnerZipCodeScope EQ "E">
                    AND PHYADDRPOSTCD    = <cfqueryparam value = "#Variables.PartnerZipCode#"  CFSQLType = "CF_SQL_VARCHAR" />
                </cfif>
            </cfif>
            
            <cfif Trim(Variables.QueryTyp) eq "next">
                AND  p.PrtLglSrchNm > <cfqueryparam value = "#Variables.PrevLastPrtNm#" CFSQLType = "CF_SQL_VARCHAR" />
            </cfif>
            
        </cfquery>
        
        <cfreturn SearchAllPartner />
        
    </cffunction>

</cfcomponent>

<!---
AUTHOR:				Steve Seaquist
DATE:				06/22/2001
DESCRIPTION:		Performs the same data validation functions as the EditDate JavaScript, only server-side, and in CFML.
NOTES:				Accepts only M/D/YY dates and converts them into MM/DD/YYYY dates. 
INPUT:				Attributes
OUTPUT:				Caller variables
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	06/22/2001, SRS:	Original implementation.
--->

<!--- Constants: --->
<CFINCLUDE TEMPLATE="LocalMachineSettingsEM.cfm">

<!--- Input Initializations: --->
<CFPARAM NAME="Attributes.Sco"				DEFAULT="Form">	<!--- Field's scope (almost always "Form"). --->
<CFPARAM NAME="Attributes.Fld">								<!--- Field's name (in the preceding scope). --->
<CFPARAM NAME="Attributes.Def"				DEFAULT="">		<!--- Only optional attribute, specifies a default for Form.#Attributes.Fld#. --->
<CFPARAM NAME="Attributes.Eng">								<!--- English name of field, if contains colon, generate hotlink. --->
<CFPARAM NAME="Attributes.Req"				DEFAULT="No">	<!--- Whether or not the field is required. --->
<CFPARAM NAME="Attributes.Fut"				DEFAULT="Yes">	<!--- Whether or not date is allowed to be in the future. --->
<CFPARAM NAME="Caller.EditDateCallNbr"		DEFAULT="0">
<CFPARAM NAME="Caller.ErrMsg"				DEFAULT="">
<CFPARAM NAME="Caller.InternalErrMsg"		DEFAULT="">
<CFPARAM NAME="Caller.URLEncodedFormFields"	DEFAULT="">
<CFPARAM NAME="#Attributes.Sco#.#Attributes.Fld#"	DEFAULT="#Attributes.Def#">
<CFIF Len(Caller.InternalErrMsg) GT 0>
	<CFEXIT METHOD="EXITTAG"><!--- Internal errors are severe... only one per customer. --->
</CFIF>
<CFSET Caller.EditDateCallNbr				= Caller.EditDateCallNbr + 1>
<CFIF Len(Attributes.Fld) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Fld attribute (call #Caller.EditDateCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Eng) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Eng attribute (call #Caller.EditDateCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Req) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Req attribute (call #Caller.EditDateCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>

<!--- The following allows hotlinking to the field in error. Assumes A NAME=xxx is defined in the dsp page displaying ErrMsg. --->
<CFSET Variables.EngArray					= ListToArray(Attributes.Eng,	":")>
<CFIF ArrayLen(Variables.EngArray) IS 1>	<CFSET Variables.HotFNam		= Attributes.Eng>
<CFELSE>									<CFSET Variables.HotFNam		= "<A HREF=""###Variables.EngArray[1]#"">#Variables.EngArray[2]#</A>"></CFIF>

<CFSET Variables.FVal						= Evaluate("#Attributes.Sco#.#Attributes.Fld#")>
<CFSET Caller.URLEncodedFormFields			= Caller.URLEncodedFormFields
											& "#URLEncodedFormat(Attributes.Fld)#=#URLEncodedFormat(Variables.FVal)#&">

<CFIF (NOT Attributes.Req) AND (Len(Variables.FVal) IS 0)>
	<CFEXIT METHOD="EXITTAG"><!--- SUCCESS. No Caller.ErrMsg returned. --->
<CFELSEIF Len(Variables.FVal)LT 6>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# must be at least 6 character(s) long."			& Variables.CRLF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>

<CFIF Mid(Variables.FVal, 2, 1) IS "/">		<!--- Convert M to MM --->
	<CFSET Variables.FVal					= "0" & Variables.FVal>
</CFIF>
<CFIF Mid(Variables.FVal, 5, 1) IS "/">		<!--- Convert D to DD --->
	<CFSET Variables.FVal					= Left(Variables.FVal, 3) & "0" & Right(Variables.FVal, Len(Variables.FVal) - 3)>
</CFIF>
<!--- Haven't been able to get this to work in a CFSET, so use CFSCRIPT to overwrite Form.#Attributes.Fld#: --->
<CFSCRIPT>
StructInsert(Evaluate(Attributes.Sco), Attributes.Fld, Variables.FVal, TRUE);
</CFSCRIPT>
<CFIF (IsNumeric(Mid(Variables.FVal, 1, 2)))
  AND (			 Mid(Variables.FVal, 3, 1)	IS "/")
  AND (IsNumeric(Mid(Variables.FVal, 4, 2)))
  AND (			 Mid(Variables.FVal, 6, 1)	IS "/")
  AND ((Len(Variables.FVal) IS 8) OR (Len(Variables.FVal) IS 10))
  AND (IsNumeric(Right(Variables.FVal, Len(Variables.FVal) - 6)))>
	<CFSET Variables.MM						= Mid(Variables.FVal, 1, 2)>
	<CFSET Variables.DD						= Mid(Variables.FVal, 4, 2)>
	<CFSET Variables.YYYY					= Mid(Variables.FVal, 7, Len(Variables.FVal) - 6)>
	<CFIF  Variables.YYYY LT 100>
		<CFSET Variables.EnteredYY			= "Yes">
		<CFIF Variables.YYYY GT 52>
			<CFSET Variables.YYYY			= Variables.YY + 2000>
			<CFSET Variables.FVal			= Left(Variables.FVal, 6) & "20" & Right(Variables.FVal, Len(Variables.FVal) - 6)>
		<CFELSE>
			<CFSET Variables.YYYY			= Variables.YY + 1900>
			<CFSET Variables.FVal			= Left(Variables.FVal, 6) & "19" & Right(Variables.FVal, Len(Variables.FVal) - 6)>
		</CFIF>
	<CFELSE>
		<CFSET Variables.EnteredYY			= "No">
	</CFIF>
<CFELSE>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg
											& "<LI>After expansion of M to MM and D to DD, "
											& "#Variables.HotFNam# must be of the format MM/DD/YY or MM/DD/YYYY."
											& Variables.CRLF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>
<!--- Haven't been able to get this to work in a CFSET, so use CFSCRIPT to overwrite Form.#Attributes.Fld#: --->
<CFSCRIPT>
StructInsert(Evaluate(Attributes.Sco), Attributes.Fld, Variables.FVal, TRUE);
</CFSCRIPT>
<CFIF (Variables.YYYY LT 1900) OR (Variables.YYYY GT 2099)>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg
											& "<LI>#Variables.HotFNam# cannot be outside the year range 1900-2099."
											& Variables.CRLF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>
<CFIF (Variables.MM LT 1) OR (Variables.MM GT 12)>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg
											& "<LI>#Variables.HotFNam# month (#Variables.MM#) outside the range 1-12."
											& Variables.CRLF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>
<CFSET Variables.YY							= Variables.YYYY MOD 4><!--- Good enough for our year range. --->
<CFIF Variables.YY IS 0>
	<CFSET Variables.NDList					= "31,29,31,30,31,30,31,31,30,31,30,31">
<CFELSE>
	<CFSET Variables.NDList					= "31,28,31,30,31,30,31,31,30,31,30,31">
</CFIF>
<CFSET Variables.NDM						= ListGetAt(Variables.NDList, Variables.MM)>
<CFIF (Variables.DD LT 1) OR (Variables.DD GT Variables.NDM)>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg
											& "<LI>#Variables.HotFNam# day (#Variables.DD#) outside the range 1-#Variables.NDM#."
											& Variables.CRLF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>
<CFIF NOT Attributes.Fut>
	<CFSET Variables.DateInQuestion			= CreateDateTime(Variables.YYYY, Variables.MM, Variables.DD, 0, 0, 0)>
	<CFIF DateCompare(Variables.DateInQuestion, Now(), "d") GT 0>
		<CFSET Caller.ErrMsg				= Caller.ErrMsg
											& "<LI>#Variables.HotFNam# cannot be in the future. ">
		<CFIF Variables.EnteredYY>
			<CFSET Caller.ErrMsg			= Caller.ErrMsg
											& "The SBA infers century based on the year range 1953-2052. "
											& "If you intended the date to be in the range of 1901-1952, you must enter a 4-digit year. ">
		</CFIF>
		<CFSET Caller.ErrMsg				= Caller.ErrMsg
											& Variables.CRLF>
		<!--- No need to CFEXIT here. We're done. --->
	</CFIF>
</CFIF>

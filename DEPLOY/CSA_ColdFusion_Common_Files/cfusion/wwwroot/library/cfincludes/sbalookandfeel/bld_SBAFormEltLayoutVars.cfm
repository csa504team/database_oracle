<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, for the US Small Business Administration. 
DATE:				04/09/2014. 
DESCRIPTION:		Defines variables that are needed to do pseudo-table layout according to the wishes of the developer. 
					The developer can make special runtime overrides using Attributes and Request.Sfe variables. And those 
					overrides can vary on a form-element-by-form-element basis. Therefore, we have to recalculate these 
					variables on every cf_sbaform(something) call. 
NOTES:				Called by cf_sbaformelt and any group tags that also do specialized layout, such as cf_sbaformperson. 
					As with the button includes, this file cannot reside in the /library/customtags directory. (If it did, it 
					would automatically become a custom tag too!) That's why it resides in /library/cfincludes/sbalookandfeel. 
INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	04/09/2014, SRS:	Original implementation. 
--->

<!---
"HiGrp" is a "highlighting group" (a group of elements that are worked on wholesale by one of the HiGrpUtils functions. 
Generally not used, but we support it. 
--->

<cfif Len(Attributes.HiGrp) GT 0>					<cfset Variables.HiGrpBox	= " hibox #Attributes.HiGrp#">
													<cfset Variables.HiGrpLab	= " hilab #Attributes.HiGrp#">
<cfelse>											<cfset Variables.HiGrpBox	= "">
													<cfset Variables.HiGrpLab	= "">
</cfif>


<!---
"Indent" is a global pre-indention applied to all generated code before the IndentChars indent the tbl, tbl_row, tbl_cell, 
etc. For example, suppose AppData is inline. cf_sbalookandfeel will indent DivAppData 2 tab characters. So a good Indent in 
that case would be 3 tabs, so that AppData's contents are indented relative to the containing div. 

But if AppData is in a frame, which is VERY often the case in Electronic Lending, for example, it makes sense for Indent to 
be nullstring, causing the pseudo-tables to be left-justified in the frame. 
--->

<cfif (Len(Attributes.Indent) is 0) and IsDefined("Request.SfeIndent")>
	<cfset Variables.Indent							= Request.SfeIndent>
<cfelse>
	<cfset Variables.Indent							= Attributes.Indent>
</cfif>

<!---
"IndentChars", on the other hand, is how much to indent tbl, tbl_row, tbl_cell, etc, relative to each other. To prevent 
horizontal scrolling in View Source, the default it 2 spaces. But it could just as easily be a tab character. Your choice. 
--->

<cfif Len(Attributes.IndentChars) is 0>
	<cfif IsDefined("Request.SfeIndentChars")>
		<cfset Variables.IndentChars				= Request.SfeIndentChars>
	<cfelse>
		<cfset Variables.IndentChars				= "  ">
	</cfif>
<cfelse>
	<cfset Variables.IndentChars					= Attributes.IndentChars>
</cfif>

<!---
Last but not least, the "IndentPlus" variables are the concatenation of Indent and multiple occurrences of IndentChars. 
They're what you actually use to indent tbl, tbl_row, tbl_cell, etc, relative to each other. 
--->

<cfloop index="i" from="0" to="4">
	<cfset Variables["IndentPlus#i#"]				= Variables.Indent	& RepeatString(Variables.IndentChars, i)>
</cfloop>


<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines JavaScripts, substituting /library/javascripts/sbalookandfeel defaults if files don't exist. 
NOTES:				Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.
INPUT:				Variables.ListShowBtnsPacked, Attributes.MainNavJSURL. 
OUTPUT:				Script tags. 
REVISION HISTORY:	07/20/2011, SRS:	Adapted to Request.SlafShowButtonOverrides. (See /library/udf/bld_SlafUDFs.cfm.) 
										If button has scriptsrc override, use that instead. If button has href override, 
										and does NOT have scriptsrc overried, don't generate any script tag at all. 
					07/21/2010, SRS:	"New Look-and-Feel". Removed preload of /library/images jpegs. Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfif NOT IsDefined("ShowButtonHasOverrides")>
	<cfinclude template="/library/udf/bld_SlafUDFs.cfm">
</cfif>

<cfloop index="Button" list="#Variables.ListShowBtnsPacked#">
	<cfif ShowButtonHasOverrides(Button)>
		<cfif IsDefined("Variables.Overrides.scriptsrc")>
			<cfoutput>
<script language="JavaScript" src="#Variables.Overrides.scriptsrc#"></script></cfoutput>
			<cfcontinue>
		<cfelseif IsDefined("Variables.Overrides.href")>
			<cfcontinue>
		</cfif>
	</cfif>
	<cfif	(ListFind(Variables.ListStdBtnsPacked, Button) GT 0)
		AND	(NOT FileExists(ExpandPath("#Attributes.MainNavJSURL#/Do#Button#.js")))>
		<cfoutput><!--- The /library/javascripts/sbalookandfeel/Do#Button#.js may not exist either, but at least we tried: --->
<script language="JavaScript" src="#Attributes.LibURL#/javascripts/sbalookandfeel/Do#Button#.js"></script></cfoutput>
	<cfelse>
		<cfoutput>
<script language="JavaScript" src="#Attributes.MainNavJSURL#/Do#Button#.js"></script></cfoutput>
	</cfif>
</cfloop>

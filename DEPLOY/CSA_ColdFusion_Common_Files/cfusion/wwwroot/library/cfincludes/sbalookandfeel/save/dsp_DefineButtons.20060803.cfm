<!--- Called by Custom Tag cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->

<cfoutput><!--- GetResolution(this.SlafResolution, this.SlafClientHeight, this.SlafClientWidth); --->
#i4#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
#i4#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
#i4#<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><!--- 
#i4#<input type="Hidden" name="SlafResolution"		value="1024">
#i4#<input type="Hidden" name="SlafClientHeight"	value="">
#i4#<input type="Hidden" name="SlafClientWidth"		value=""> ---><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>
#i4#<table id="TblMainNavInner" border="0" cellpadding="0" cellspacing="0">
#i4#<tr></cfoutput>

<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListStdButtons#"><!--- Defined in dsp_DefineJavaScriptsForButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowButtons, Button)>
	<cfif Found GT 0>
		<cfset Variables.Alt						= ListGetAt	(Variables.ListStdAlts,		CurrIdx)>
		<cfif Attributes.TextOnly>
			<cfoutput>
#i4#	<td>&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</td></cfoutput>
		<cfelse>
			<cfset Variables.Image					= ListGetAt(Variables.ListStdImages,	CurrIdx)>
			<cfset Variables.FilenameLo				= "#Attributes.LibURL#/images/#SelfName#/#Image#_lo.jpg">
			<cfset Variables.FilenameHi				= "#Attributes.LibURL#/images/#SelfName#/#Image#_hi.jpg">
			<cfoutput>
#i4#	<td><!--- Height and width now defined in mainnav. Makes it possible to test/override configuration if buttons change. --->
#i4#		<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#" border="0"
#i4#			src="#FilenameLo#" alt="#Alt#" title="#Alt#" value="#Image#"
#i4#			onMouseOver="if (gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
#i4#			onmouseout="if (gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
#i4#			onfocus="this.onmouseover();"
#i4#			onblur="this.onmouseout();"
#i4#			onclick="Do#Button#(this.form);">
#i4#	</td></cfoutput>
			<!--- (Note: The title attribute is to show tooltips in Netscape. All you need is the alt attribute for MSIE.) --->
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowButtons#"><!--- Defined in dsp_DefineJavaScriptsForButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdButtons, Button)>
	<cfif Found IS 0>
		<cfset Variables.Alt						= ListGetAt(Variables.ListShowAlts,		CurrIdx)>
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages,	CurrIdx)>
		<cfset Variables.FilenameLo					= "#Attributes.LibURL#/images/applookandfeel/#Image#_lo.jpg">
		<cfset Variables.FilenameHi					= "#Attributes.LibURL#/images/applookandfeel/#Image#_hi.jpg">
		<cfif NOT Variables.FirstNonStdButtonSeen>
			<cfoutput>
#i4#</tr>
#i4#<tr></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>
#i4#	<td>&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</td></cfoutput>
		<cfelse>
			<cfoutput>
#i4#	<td>
#i4#		<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#" border="0"
#i4#			src="#FilenameLo#" alt="#Alt#" title="#Alt#" value="#Image#"
#i4#			onMouseOver="if (gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
#i4#			onmouseout="if (gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
#i4#			onfocus="this.onmouseover();"
#i4#			onblur="this.onmouseout();"
#i4#			onclick="Do#Button#(this.form);">
#i4#	</td></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>
#i4#</tr>
#i4#</table><!-- TblMainNavInner -->
#i4#</form></cfoutput>

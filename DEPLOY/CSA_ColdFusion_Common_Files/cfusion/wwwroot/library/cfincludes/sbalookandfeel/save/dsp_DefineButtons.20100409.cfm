<!--- Called by Custom Tag cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->

<!---
The following takes some explanation. In Netscape 4, a div with absolute or relative positioning is rendered using layers. 
That results in Netscape treating the contents of the div as if it were separate document. Therefore, when MainNav is inline, 
you can't just reference gThisPageIsFullyLoaded (for example), you have to reference top.gThisPageIsFullyLoaded to get to the 
topmost document containing the div. In all other browsers, the "top." part is unnecessary, but doesn't hurt anything. 
--->
<cfif Attributes.InFrame>							<cfset Variables.DivContainingDoScripts	= "">
<cfelse>											<cfset Variables.DivContainingDoScripts	= "top."></cfif>

<cfoutput><!--- GetResolution(this.SlafResolution, this.SlafClientHeight, this.SlafClientWidth); --->
#indent#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
#indent#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
#indent#<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif></cfoutput>

<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListStdButtons#"><!--- Defined in dsp_ConfigStdButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowButtons, Button)>
	<cfif Found GT 0>
		<cfset Variables.Alt						= ListGetAt	(Variables.ListStdAlts,		CurrIdx)>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfset Variables.Image					= ListGetAt(Variables.ListStdImages,	CurrIdx)>
			<cfset Variables.FilenameLo				= "#Attributes.LibURL#/images/#SelfName#/#Image#_lo.jpg">
			<cfset Variables.FilenameHi				= "#Attributes.LibURL#/images/#SelfName#/#Image#_hi.jpg">
			<cfoutput>#Request.SlafEOL#
#indent#<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#"
#indent#	alt="#Alt#" border="0" src="#FilenameLo#" style="margin-top:1px;" title="#Alt#" value="#Image#"
#indent#	onMouseOver="if (#Variables.DivContainingDoScripts#gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
#indent#	onmouseout="if (#Variables.DivContainingDoScripts#gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
#indent#	onfocus="this.onmouseover();"
#indent#	onblur="this.onmouseout();"
#indent#	onclick="#Variables.DivContainingDoScripts#Do#Button#(this.form);"></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowButtons#"><!--- Defined in dsp_mainnav.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdButtons, Button)>
	<cfif Found IS 0>
		<cfset Variables.Alt						= ListGetAt(Variables.ListShowAlts,		CurrIdx)>
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages,	CurrIdx)>
		<cfset Variables.FilenameLo					= "#Attributes.LibURL#/images/applookandfeel/#Image#_lo.jpg">
		<cfset Variables.FilenameHi					= "#Attributes.LibURL#/images/applookandfeel/#Image#_hi.jpg">
		<cfif NOT Variables.FirstNonStdButtonSeen>
			<cfoutput><br/>
#indent#<!-- Second row of <cfif Attributes.TextOnly>hotlinks<cfelse>buttons</cfif>: --></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfoutput>#Request.SlafEOL#
#indent#<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#"
#indent#	alt="#Alt#" border="0" src="#FilenameLo#" title="#Alt#" value="#Image#"
#indent#	onMouseOver="if (#Variables.DivContainingDoScripts#gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
#indent#	onmouseout="if (#Variables.DivContainingDoScripts#gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
#indent#	onfocus="this.onmouseover();"
#indent#	onblur="this.onmouseout();"
#indent#	onclick="#Variables.DivContainingDoScripts#Do#Button#(this.form);"></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>#Request.SlafEOL#
#indent#</form>
</cfoutput>

<!--- Called by Custom Tags cf_sbalookandfeel and cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->
<cfset Variables.ImageHeight						= 25>
<cfset Variables.ImageWidth							= 75>
<!--- Everything else derives from the Alt attributes. --->
<cfset Variables.ListStdAlts						= "Reports,"
													& "Search,"
													& "Admin,"
													& "New Application,"
													& "Copy,"
													& "Print,"
													& "Exit,"
													& "Help">
<cfset Variables.ListStdButtons						= Replace(Replace(Variables.ListStdAlts," ","","ALL"),"-","","ALL")>
<cfset Variables.ListStdImages						= LCase(Variables.ListStdButtons)>

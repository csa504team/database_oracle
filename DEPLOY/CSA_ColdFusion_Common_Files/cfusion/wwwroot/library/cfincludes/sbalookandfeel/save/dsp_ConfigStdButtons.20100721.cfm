<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:				Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.
INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfset Variables.ImageHeight						= 25>
<cfset Variables.ImageWidth							= 75>
<cfset Variables.ImageStyle							= "border:0px;">
<cfset Variables.StartOfButtonRowSpacerStyle		= "width:48px;">
<!--- Everything else derives from the Alt attributes. --->
<cfset Variables.ListStdAlts						= "Reports,"
													& "Search,"
													& "Admin,"
													& "New Application,"
													& "Copy,"
													& "Print,"
													& "Exit,"
													& "Help">
<cfset Variables.ListStdButtons						= Replace(Replace(Variables.ListStdAlts," ","","ALL"),"-","","ALL")>
<cfset Variables.ListStdImages						= LCase(Variables.ListStdButtons)>

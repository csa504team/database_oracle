<cfoutput>#Request.SlafHead#
<title>#Attributes.WindowTitle#</title>#Request.SlafTopOfHead#
<link  href="#Attributes.LibURL#/images/sbalookandfeel/favicon.ico"                                  rel="icon" type="image/vnd.microsoft.icon"/>
<script><!--- We want to do as much as possible in cached JS files. But the following are dynamic and cannot be cached: --->
var	gSlafDebug							= <cfif Attributes.Debug		>true<cfelse>false</cfif>;
var	gSlafSessionGroupId					= "#Hash(UCase(Request.SlafApplicationName))#";<cfif IsDefined("Request.SlafSuppressRTE")>
var	gSlafSuppressRTE					= <cfif Request.SlafSuppressRTE	>true<cfelse>false</cfif>;<cfelse>
var	gSlafSuppressRTE					= false;</cfif>
var	gSlafRenderingMode					= "jqm"; // "jQuery Mobile"<!--- Commented out for now:
#Request.SlafBrowser.TouchBasedJSToShowTooltips#--->
</script><cfif Request.SlafBrowser.Android>
<style>
/* Andriod-only. (This messes up only desktop browsers, not iOS.) Try to allow scroll right without plug-in: */
div[data-role=content],
div[data-role=collapsible-set],
div[data-role=collapsible],
div[data-role=footer]
	{
	overflow:							auto !important;/* !important is just-in-case. Probably not necessary. Test later. */
	}
/* End of Android-only. */
</style></cfif></cfoutput>
	<cfif Len(Attributes.JSInline) GT 0>
		<cfoutput>
<!-- JSInline, begins. -->
#Attributes.JSInline#
<!-- JSInline, ends. --></cfoutput>
	</cfif>
	<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_DefineJavaScriptsForButtons.cfm">
	<cfoutput>
</head>
<body class="jqm">
<div data-role="page">
<div data-role="header" data-position="fixed" class="headerbar">
	<img src="logo.png" alt="SBA Logo" class="ui-btn-left" style="margin-top:-12px;">
	<h1 class="headerbar">#Attributes.AppName#</h1>
</div><!-- /data-role="header" -->
<div data-role="content">
	<div data-role="collapsible" data-mini="true"><h3>Navigation</h3></cfoutput>
	<cfif Len(Variables.ListShowBtnsPacked)					gt 0>
		<cfoutput>
	<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
	#Attributes.MainNavHiddens#
	<h3>&quot;Main Navigation&quot;:</h3></cfoutput>
		<cfset Variables.FirstStdButtonSeen					= "No">
		<cfset Variables.CurrIdx							= 0><!--- Reset to 0 later. --->
		<cfloop index="Button" list="#Variables.ListStdBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
			<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
			<cfset Found									= ListFind	(Variables.ListShowBtnsPacked, Button)><!--- Also packed. --->
			<cfif Found GT 0><!--- One of the standard buttons. --->
				<cfif NOT Variables.FirstStdButtonSeen>
					<cfoutput>
	<div data-role="controlgroup" data-mini="true" data-type="horizontal" class="sbanavbuttons"></cfoutput>
				</cfif>
				<cfset Variables.FirstStdButtonSeen			= "Yes">
				<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListStdBtnsFull,	CurrIdx)><!--- Full name. --->
				<cfset Variables.href						= "javascript:"
															& "Do#Button#(document.FormMainNav);">
				<cfset Variables.icon						= "">
				<cfset Variables.target						= "">
				<cfset Variables.title							= Variables.CanonicalName>
				<cfif ShowButtonHasOverrides(Button)>
					<cfif IsDefined("Variables.Overrides.href")>
						<cfset Variables.href				= Variables.Overrides.href>
					</cfif>
					<cfif IsDefined("Variables.Overrides.icon")>
						<cfset Variables.icon				= ' data-icon="'
															& Variables.Overrides.icon
															& '"'>
					<cfelse>
						<cfswitch expression="#Variables.CanonicalName#">
						<cfcase value="Exit"><cfset Variables.icon = ' data-icon="home"'></cfcase>
						<cfcase value="Help"><cfset Variables.icon = ' data-icon="info"'></cfcase>
						</cfswitch>
					</cfif>
					<cfif IsDefined("Variables.Overrides.external")
						and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
						<cfset Variables.pseudobutton		= "mainnavexternalsys">
						<cfif IsDefined("Variables.Overrides.target")>
							<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
						<cfelse>
							<cfset Variables.target			= " target=""_blank""">
						</cfif>
					<cfelseif IsDefined("Variables.Overrides.target")>
						<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
					</cfif>
					<cfif IsDefined("Variables.Overrides.title")>
						<cfset Variables.title				= Variables.Overrides.title>
					</cfif>
				<cfelse>
					<cfswitch expression="#Variables.CanonicalName#">
					<cfcase value="Exit"><cfset Variables.icon = ' data-icon="home"'></cfcase>
					<cfcase value="Help"><cfset Variables.icon = ' data-icon="info"'></cfcase>
					</cfswitch>
				</cfif>
				<cfoutput>
		<a data-role="button" data-inline="true"#Variables.icon##Variables.target# href="#Variables.href#">#Variables.CanonicalName#</a></cfoutput>
			</cfif>
		</cfloop>
		<cfif Variables.FirstStdButtonSeen>
			<cfoutput>
	</div><!-- /controlgroup --></cfoutput>
		</cfif>
		<cfset Variables.FirstNonStdButtonSeen				= "No">
		<cfset Variables.CurrIdx							= 0>
		<cfloop index="Button" list="#Variables.ListShowBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
			<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
			<cfset Found									= ListFind(Variables.ListStdBtnsPacked, Button)><!--- Also packed. --->
			<cfif Found IS 0><!--- NOT one of the standard buttons, since the standard buttons row has already been defined. --->
				<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListShowBtnsFull,	CurrIdx)><!--- Full name. --->
				<cfset Variables.href						= "javascript:"
															& "Do#Button#(document.FormMainNav);">
				<cfset Variables.icon						= "">
				<cfset Variables.pseudobutton				= "mainnavsubmit"><!--- Class name(s) if not textonly mode. --->
				<cfset Variables.target						= "">
				<cfset Variables.title						= Variables.CanonicalName>
				<cfif ShowButtonHasOverrides(Button)>
					<cfif IsDefined("Variables.Overrides.href")>
						<cfset Variables.href				= Variables.Overrides.href>
					</cfif>
					<cfif IsDefined("Variables.Overrides.icon")>
						<cfset Variables.icon				= ' data-icon="'
															& Variables.Overrides.icon
															& '"'>
					</cfif>
					<cfif IsDefined("Variables.Overrides.external")
						and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
						<cfset Variables.pseudobutton		= "mainnavexternalsys">
						<cfif IsDefined("Variables.Overrides.target")>
							<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
						<cfelse>
							<cfset Variables.target			= " target=""_blank""">
						</cfif>
					<cfelseif IsDefined("Variables.Overrides.target")>
						<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
					</cfif>
					<cfif IsDefined("Variables.Overrides.title")>
						<cfset Variables.title				= Variables.Overrides.title>
					</cfif>
				</cfif>
				<cfif	Variables.FirstStdButtonSeen
					and	(NOT Variables.FirstNonStdButtonSeen)>
					<cfoutput>
	<div data-role="controlgroup" data-mini="true" data-type="horizontal" class="sbanavbuttons"></cfoutput>
					<cfset Variables.FirstNonStdButtonSeen	= "Yes">
				</cfif>
				<cfif Variables.CanonicalName is "br">
					<cfoutput><br/></cfoutput>
				<cfelse>
					<cfoutput>
		<a data-role="button" data-inline="true"#Variables.icon##Variables.target# href="#Variables.href#">#Variables.CanonicalName#</a></cfoutput>
				</cfif>
			</cfif>
		</cfloop>
		<cfif Variables.FirstNonStdButtonSeen>
			<cfoutput>
	</div><!-- /controlgroup --></cfoutput>
		</cfif>
		<cfoutput>
	</form></cfoutput>
	</cfif><!--- /Len(Variables.ListShowBtnsPacked) gt 0 --->
	<cfif Len(Attributes.AppNavInline) gt 0>
		<cfoutput>
	<h3>#Attributes.AppName# In-App Navigation:</h3>
	<div id="DivAppNav" #Variables.MaybeNoAppNavClass#>
		#Attributes.AppNavInline#
	</div><!-- /DivAppNav --></cfoutput>
	</cfif>
	<cfoutput>
	<h3>Other Government Sites:</h3>
	<div data-role="controlgroup" data-mini="true" data-type="horizontal" class="sbanavbuttons">
		<a data-role="button" data-inline="true" href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.firstgov.gov/">FirstGov</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/omb/egov/">E-Gov</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.regulations.gov/">Regulations.gov</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/">White House</a>
	</div><!-- /controlgroup -->
	<div data-role="controlgroup" data-mini="true" data-type="horizontal" class="sbanavbuttons">
		<a data-role="button" data-inline="true" href="http://www.sba.gov/privacysecurity/index.html">Privacy &amp; Security</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/information/index.html">Information Quality</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/foia/index.html">FOIA</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/eeo/">No Fear Act</a>
		<a data-role="button" data-inline="true" href="http://www.sba.gov/ada/index.html">ADA</a>
	</div><!-- /controlgroup -->
	</div><!-- /collapsible Navigation -->
</div><!-- /data-role="content" --><cfif Len(Attributes.AppInfoInline) gt 0>
#Attributes.AppInfoInline#</cfif><cfif IsDefined("Attributes.AppDataInline")>
#Attributes.AppDataInline#</cfif>
#thisTag.GeneratedContent#
</div><!-- /data-role="page" -->
<div id="DivAppHidden">
	<!--
	Hiding with style="display:none" allows debug display by typing the following URL into the address/location bar: 
		javascript:alert(gDivAppHidden.style.display="block"); // overlays entire window contents
		javascript:alert(gDivAppHidden.style.display="none");  // resumes hiding it, revealing previous window contents
	-->
	<iframe id="FrmAppHidden" name="AppHidden" src="#Attributes.AppHiddenURL#" 
		frameborder="0" marginheight="0" marginwidth="0" title="Hidden Frame, sometimes used for server callbacks"
		width="100%">#Variables.NoIFrames#</iframe>
</div><!-- DivAppHidden -->
</body>
</html>
</cfoutput>

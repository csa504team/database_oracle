<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:

	Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.

	The following takes some explanation. In Netscape 4, a div with absolute or relative positioning is rendered using 
	layers. That results in Netscape treating the contents of the div as if it were separate document. Therefore, when 
	MainNav is inline, you can't just reference gThisPageIsFullyLoaded (for example), you have to reference 
	top.gThisPageIsFullyLoaded to get to the topmost document containing the div. In all other browsers, the "top." 
	part is unnecessary, but doesn't hurt anything. 

INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	07/20/2011, SRS:	Adapted to Request.SlafShowButtonOverrides. (See /library/udf/bld_SlafUDFs.cfm.) 
										Renamed lists so that the logic would be more comprehensible/readable. 
					11/09/2010, SRS:	Now that CSS buttons are proven to work, made code more efficient by moving hover 
										JavaScript to mainnav.js. 
					09/29/2010, SRS:	Section 508: Added role="button" (WAI-ARIA explanation) to pseudo-button hotlinks 
										only. Indented buttons using padding-left, not using a spacer div, so that if the 
										buttons wrap, they wrap indented. Got rid of submit buttons, which weren't being 
										used to submit the form anyway. (Hotlinks are more consistent and maintainable.) 
					09/08/2010, SRS:	Section 508: Made MSIE links tabbable. 
					07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfif NOT IsDefined("ShowButtonHasOverrides")>
	<cfinclude template="/library/udf/bld_SlafUDFs.cfm">
</cfif>

<cfif Attributes.InFrame>							<cfset Variables.FrameContainingDoScripts	= "">
<cfelse>											<cfset Variables.FrameContainingDoScripts	= "top."></cfif>
<cfif	(Len(Variables.ListShowBtnsPacked)			GT 120)
	or	(ListLen(Variables.ListShowBtnsPacked)		GT 12)>
	<!--- Although it looks good at 9pt, text-shadow looks really bad at 7pt: --->
	<cfset Variables.ImageStyle						&= " font-size:8pt !important; text-shadow:none !important;">
</cfif>

<cfoutput>#Request.SlafEOL#
#indent#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
#indent#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
#indent#<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>#Request.SlafEOL#
#indent#<div class="inlineblock" style="#Variables.SkipOverAppName#"></cfoutput>

<cfset Variables.FirstStdButtonSeen					= "No">
<cfset Variables.CurrIdx							= 0><!--- Reset to 0 later. --->
<cfloop index="Button" list="#Variables.ListStdBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowBtnsPacked, Button)><!--- Also packed. --->
	<cfif Found GT 0><!--- One of the standard buttons. --->
		<cfset Variables.FirstStdButtonSeen			= "Yes">
		<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListStdBtnsFull,	CurrIdx)><!--- Full name. --->
		<cfset Variables.href						= "javascript:"
													& Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav);">
		<cfset Variables.pseudobutton				= "mainnavsubmit"><!--- Class name(s) if not textonly mode. --->
		<cfset Variables.target						= "">
		<cfset Variables.title						= Variables.CanonicalName>
		<cfif ShowButtonHasOverrides(Button)>
			<cfif IsDefined("Variables.Overrides.href")>
				<cfset Variables.href				= Variables.Overrides.href>
			</cfif>
			<cfif IsDefined("Variables.Overrides.external")
				and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
				<cfset Variables.pseudobutton		= "mainnavexternalsys">
				<cfif IsDefined("Variables.Overrides.target")>
					<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
				<cfelse>
					<cfset Variables.target			= " target=""_blank""">
				</cfif>
			<cfelseif IsDefined("Variables.Overrides.target")>
				<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
			</cfif>
			<cfif IsDefined("Variables.Overrides.title")>
				<cfset Variables.title				= Variables.Overrides.title>
			</cfif>
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a class="menuitem"#Variables.target# href="#Variables.href#" role="button">#Variables.CanonicalName#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><a class="#Variables.pseudobutton#"#Variables.target# href="#Variables.href#"
#indent#	role="button" style="#ImageStyle#" title="#Variables.title#">#Variables.CanonicalName#</a></div></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdBtnsPacked, Button)><!--- Also packed. --->
	<cfif Found IS 0><!--- NOT one of the standard buttons, since the standard buttons row has already been defined. --->
		<cfif CompareNoCase(Button,"br") is 0><!--- Special keyword that means to do a break for a new row of buttons. --->
			<cfoutput>#Request.SlafEOL#
#indent#<br/></cfoutput>
			<cfcontinue>
		</cfif>
		<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListShowBtnsFull,	CurrIdx)><!--- Full name. --->
		<cfset Variables.href						= "javascript:"
													& Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav);">
		<cfset Variables.pseudobutton				= "mainnavsubmit"><!--- Class name(s) if not textonly mode. --->
		<cfset Variables.target						= "">
		<cfset Variables.title						= Variables.CanonicalName>
		<cfif ShowButtonHasOverrides(Button)>
			<cfif IsDefined("Variables.Overrides.href")>
				<cfset Variables.href				= Variables.Overrides.href>
			</cfif>
			<cfif IsDefined("Variables.Overrides.external")
				and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
				<cfset Variables.pseudobutton		= "mainnavexternalsys">
				<cfif IsDefined("Variables.Overrides.target")>
					<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
				<cfelse>
					<cfset Variables.target			= " target=""_blank""">
				</cfif>
			<cfelseif IsDefined("Variables.Overrides.target")>
				<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
			</cfif>
			<cfif IsDefined("Variables.Overrides.title")>
				<cfset Variables.title				= Variables.Overrides.title>
			</cfif>
		</cfif>
		<cfif	Variables.FirstStdButtonSeen
			and	(NOT Variables.FirstNonStdButtonSeen)>
			<cfoutput><br/>
#indent#<!-- Second row of <cfif Attributes.TextOnly>hotlinks<cfelse>buttons</cfif>: --></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a class="menuitem"#Variables.target# href="#Variables.href#" role="button">#Variables.CanonicalName#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><a class="#Variables.pseudobutton#"#Variables.target# href="#Variables.href#"
#indent#	role="button" style="#ImageStyle#" title="#Variables.title#">#Variables.CanonicalName#</a></div></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>#Request.SlafEOL#
#indent#</div>
#indent#</form>
</cfoutput>

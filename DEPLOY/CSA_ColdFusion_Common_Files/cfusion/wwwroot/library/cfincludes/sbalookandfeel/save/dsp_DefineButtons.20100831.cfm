<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:

	Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.

	The following takes some explanation. In Netscape 4, a div with absolute or relative positioning is rendered using 
	layers. That results in Netscape treating the contents of the div as if it were separate document. Therefore, when 
	MainNav is inline, you can't just reference gThisPageIsFullyLoaded (for example), you have to reference 
	top.gThisPageIsFullyLoaded to get to the topmost document containing the div. In all other browsers, the "top." 
	part is unnecessary, but doesn't hurt anything. 

INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfif Attributes.InFrame>							<cfset Variables.DivContainingDoScripts	= "">
<cfelse>											<cfset Variables.DivContainingDoScripts	= "top."></cfif>
<cfif	(Len(Variables.ListShowButtons)				GT 120)
	or	(ListLen(Variables.ListShowButtons)			GT 12)>
	<!--- Although it looks good at 9pt, text-shadow looks really bad at 7pt: --->
	<cfset Variables.ImageStyle						&= " font-size:8pt !important; text-shadow:none !important;">
</cfif>

<cfoutput>
#indent#<div class="hide">
#indent#<script>
#indent#$(function()
#indent#	{
#indent#	$(".divmainnavsubmit")
#indent#		.hover(
#indent#			function()	// "handlerIn"
#indent#				{
#indent#				$(this).removeClass("divmainnavsubmit").addClass("divmainnavsubmithover");
#indent#				},
#indent#			function()	// "handlerOut"
#indent#				{
#indent#				$(this).removeClass("divmainnavsubmithover").addClass("divmainnavsubmit");
#indent#				});
#indent#	});
#indent#</script>
#indent#</div><!-- display:none --></cfoutput>

<cfoutput>#Request.SlafEOL#
#indent#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
#indent#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
#indent#<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>#Request.SlafEOL#
#indent#<div class="inlineblock" style="#Variables.StartOfButtonRowSpacerStyle#">&nbsp;</div></cfoutput>

<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListStdButtons#"><!--- Defined in dsp_ConfigStdButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowButtons, Button)>
	<cfif Found GT 0>
		<cfset Variables.Alt						= ListGetAt	(Variables.ListStdAlts,		CurrIdx)>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfset Variables.Image					= ListGetAt(Variables.ListStdImages,	CurrIdx)>
			<cfset Variables.ImageValue				= Variables.Alt>
			<cfset Variables.FilenameLo				= "#Attributes.LibURL#/images/#SelfName#/#Image#_lo.jpg">
			<cfset Variables.FilenameHi				= "#Attributes.LibURL#/images/#SelfName#/#Image#_hi.jpg">
			<cfif Request.SlafBrowser.MSIE>
				<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><div class="mainnavsubmit" title="#Alt#" style="#ImageStyle#"
#indent#	onclick="#Variables.DivContainingDoScripts#Do#Button#(document.FormMainNav);">#ImageValue#</div></div></cfoutput>
			<cfelse><!--- In good browsers, use submit buttons, so that button will work even if JavaScript is off: --->
				<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><input type="submit" name="SubmitButton" style="#Variables.ImageStyle# margin-top:1px;"
#indent#	title="#Alt#" value="#ImageValue#" onclick="#Variables.DivContainingDoScripts#Do#Button#(this.form);"></div></cfoutput>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowButtons#"><!--- Defined in dsp_mainnav.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdButtons, Button)>
	<cfif Found IS 0>
		<cfset Variables.Alt						= ListGetAt(Variables.ListShowAlts,		CurrIdx)>
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages,	CurrIdx)>
		<cfset Variables.FilenameLo					= "#Attributes.LibURL#/images/applookandfeel/#Image#_lo.jpg">
		<cfset Variables.FilenameHi					= "#Attributes.LibURL#/images/applookandfeel/#Image#_hi.jpg">
		<cfset Variables.ImageValue					= Variables.Alt>
		<cfif NOT Variables.FirstNonStdButtonSeen>
			<cfoutput><br/>
#indent#<div class="inlineblock" style="#Variables.StartOfButtonRowSpacerStyle#">&nbsp;</div>
#indent#<!-- Second row of <cfif Attributes.TextOnly>hotlinks<cfelse>buttons</cfif>: --></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfif Request.SlafBrowser.MSIE>
				<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><div class="mainnavsubmit" title="#Alt#" style="#ImageStyle#"
#indent#	onclick="#Variables.DivContainingDoScripts#Do#Button#(document.FormMainNav);">#ImageValue#</div></div></cfoutput>
			<cfelse><!--- In good browsers, use submit buttons, so that button will work even if JavaScript is off: --->
				<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><input type="submit" name="SubmitButton" style="#Variables.ImageStyle#"<!--- No top margin after first row. --->
#indent#	title="#Alt#" value="#ImageValue#" onclick="#Variables.DivContainingDoScripts#Do#Button#(this.form);"></div></cfoutput>
			</cfif>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>#Request.SlafEOL#
#indent#</form>
</cfoutput>

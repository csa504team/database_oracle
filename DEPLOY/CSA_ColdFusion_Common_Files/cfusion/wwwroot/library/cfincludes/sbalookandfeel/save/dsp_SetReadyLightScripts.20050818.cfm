<cfoutput>
<!--- Keep in sync with HTML definition of the "readylight" div or image (also "Loading"), in mainnav.cfm. --->
function SetReadyLightToLoading	()
{<cfif Attributes.TextOnly>
var	sReadyLight					= document.getElementById("readylight");
sReadyLight.innerHTML			= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td bgcolor="##ffffff" align="center" valign="middle">'
								+ '<span title="Ready. Page is fully loaded.">Loading</span>'
								+ '</td></tr>'
								+ '</table>';<cfelse>
document.readylight.alt			= "Please wait. Parts of this page are still loading.";
document.readylight.src			= "#Attributes.LibURL#/images/#SelfName#/loading.gif";</cfif><!--- TextOnly --->
}

function SetReadyLightToReady	()
{<cfif Attributes.TextOnly>
var	sReadyLight					= document.getElementById("readylight");
sReadyLight.innerHTML			= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td class="headernav" align="center" valign="middle">'
								+ '<span title="Ready. Page is fully loaded.">Ready</span>'
								+ '</td></tr>'
								+ '</table>';<cfelse>
document.readylight.alt			= "Ready. Page is fully loaded.";
document.readylight.src			= "#Attributes.LibURL#/images/#SelfName#/ready_blue.gif";</cfif><!--- TextOnly --->
}
</cfoutput>

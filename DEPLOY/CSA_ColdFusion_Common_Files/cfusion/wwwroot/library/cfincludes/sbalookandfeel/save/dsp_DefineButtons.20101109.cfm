<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:

	Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.

	The following takes some explanation. In Netscape 4, a div with absolute or relative positioning is rendered using 
	layers. That results in Netscape treating the contents of the div as if it were separate document. Therefore, when 
	MainNav is inline, you can't just reference gThisPageIsFullyLoaded (for example), you have to reference 
	top.gThisPageIsFullyLoaded to get to the topmost document containing the div. In all other browsers, the "top." 
	part is unnecessary, but doesn't hurt anything. 

INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	11/09/2010, SRS:	Now that CSS buttons are proven to work, made code more efficient by moving hover 
										JavaScript to mainnav.js. 
					09/29/2010, SRS:	Section 508: Added role="button" (WAI-ARIA explanation) to pseudo-button hotlinks 
										only. Indented buttons using padding-left, not using a spacer div, so that if the 
										buttons wrap, they wrap indented. Got rid of submit buttons, which weren't being 
										used to submit the form anyway. (Hotlinks are more consistent and maintainable.) 
					09/08/2010, SRS:	Section 508: Made MSIE links tabbable. 
					07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfif Attributes.InFrame>							<cfset Variables.FrameContainingDoScripts	= "">
<cfelse>											<cfset Variables.FrameContainingDoScripts	= "top."></cfif>
<cfif	(Len(Variables.ListShowButtons)				GT 120)
	or	(ListLen(Variables.ListShowButtons)			GT 12)>
	<!--- Although it looks good at 9pt, text-shadow looks really bad at 7pt: --->
	<cfset Variables.ImageStyle						&= " font-size:8pt !important; text-shadow:none !important;">
</cfif>

<cfoutput>#Request.SlafEOL#
#indent#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
#indent#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
#indent#<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>#Request.SlafEOL#
#indent#<div class="inlineblock" style="#Variables.SkipOverAppName#"></cfoutput>

<cfset Variables.FirstStdButtonSeen					= "No">
<cfset Variables.CurrIdx							= 0><!--- Reset to 0 later. --->
<cfloop index="Button" list="#Variables.ListStdButtons#"><!--- Defined in dsp_ConfigStdButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowButtons, Button)>
	<cfif Found GT 0>
		<cfset Variables.Alt						= ListGetAt	(Variables.ListStdAlts,		CurrIdx)>
		<cfset Variables.DoScript					= Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav)">
		<cfset Variables.FirstStdButtonSeen			= "Yes">
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a class="menuitem" href="javascript:#Variables.DoScript#;" role="button">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfset Variables.Image					= ListGetAt(Variables.ListStdImages,	CurrIdx)>
			<cfset Variables.ImageValue				= Variables.Alt>
			<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><a class="mainnavsubmit" href="javascript:#Variables.DoScript#;"
#indent#	role="button" style="#ImageStyle#" title="#Alt#">#ImageValue#</a></div></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowButtons#"><!--- Defined in dsp_mainnav.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdButtons, Button)>
	<cfif Found IS 0>
		<cfif CompareNoCase(Button,"br") is 0>
			<cfoutput>#Request.SlafEOL#
#indent#<br/></cfoutput>
			<cfcontinue>
		</cfif>
		<cfset Variables.Alt						= ListGetAt(Variables.ListShowAlts,		CurrIdx)>
		<cfset Variables.DoScript					= Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav)">
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages,	CurrIdx)>
		<cfset Variables.ImageValue					= Variables.Alt>
		<cfif	Variables.FirstStdButtonSeen
			and	(NOT Variables.FirstNonStdButtonSeen)>
			<cfoutput><br/>
#indent#<!-- Second row of <cfif Attributes.TextOnly>hotlinks<cfelse>buttons</cfif>: --></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>#Request.SlafEOL#
#indent#&nbsp;<a class="menuitem" href="javascript:#Variables.DoScript#;" role="button">#Alt#</a>&nbsp;</cfoutput>
		<cfelse>
			<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit"><a class="mainnavsubmit" href="javascript:#Variables.DoScript#;"
#indent#	role="button" style="#ImageStyle#" title="#Alt#">#ImageValue#</a></div></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>#Request.SlafEOL#
#indent#</div>
#indent#</form>
</cfoutput>

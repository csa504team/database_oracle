<cfset Variables.BlankScreen	= "Yes">	<!--- Will be set to "No" if any non-blank, non-hidden field found. --->
<cfset Variables.Commentary		= "">
<cfset Variables.ErrMsgBase		= "Errors occurred that prevented your changes from being saved to the PRO-Net database: ">
<cfset Variables.ErrMsg			= Variables.ErrMsgBase>
<cfset Variables.TxnErr			= "No">

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs End-of-"TimeThis"-passage, or accumulates time for End-of-Request "TimeThis" logging. 
NOTES:				Called by log_SleTimeEnd and log_SleTimeEndAccum. Not to be called by developers directly. 
INPUT:				Variables.SleIncludeFileName. 
OUTPUT:				If currently configured to log it and entry was from log_SleTimeEnd, End-of-"TimeThis"-passage log 
					entry ("SleTimeEnd"); otherwise, just SleTimeThisArray maintenance. 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Variables.SleIncludeFileName")>
	<!--- Always do Shared SleTimeThisArray maintenance: --->
	<cfif IsDefined("Request.SBALogTimeThisName") AND (Len(Request.SBALogTimeThisName) GT 0)>
		<cfinclude template="get_SleStripHTCRLF.cfm">
		<cfset Variables.SleEntityName								= SleStripHTCRLF(Request.SBALogTimeThisName)>
		<cfset Variables.SleTimeThisNameIdx							= 0>
		<cfif IsDefined("Request.SleTimeThisArray")>
			<cfloop index="SleIdx" from="1" to="#ArrayLen(Request.SleTimeThisArray)#">
				<cfif Request.SleTimeThisArray[SleIdx][1] IS Variables.SleEntityName>
					<cfset Variables.SleTimeThisNameIdx				= SleIdx>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
		<cfif Variables.SleTimeThisNameIdx IS 0>
			<cfif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
				<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
				<cfset Request.SleLogger.error	("SleTimeEnd"			& Request.SleHT
												& Request.SleScriptName	& Request.SleHT 
												& Request.SleUser		& RepeatString(Request.SleHT, 4)
												& "(not loggable because SelTimeEnd called without same-name SleTimeBeg)")>
			</cfif>
		<cfelse>
			<cfset Variables.SleExecutionTime							= GetTickCount()
																		- Request.SleTimeThisArray[SleTimeThisNameIdx][2]>
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][4]		= Request.SleTimeThisArray[SleTimeThisNameIdx][4] 
																		+ Variables.SleExecutionTime>
			<!--- In case End called twice with intervening Beg, this assures first segment's time won't be counted twice: --->
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][2]		= GetTickCount()>
			<cfif (Variables.SleIncludeFileName IS "SleTimeEnd") AND Request.SleIsInfoEnabled>
				<cfset SleCount											= Request.SleTimeThisArray[SleTimeThisNameIdx][3]>
				<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
				<cfset Request.SleLogger.info	("SleTimeEnd"					& Request.SleHT
												& Request.SleScriptName			& Request.SleHT 
												& Request.SleUser				& RepeatString(Request.SleHT, 2)
												& Variables.SleEntityName		& Request.SleHT
												& Variables.SleExecutionTime	& Request.SleHT
												& "(iteration #SleCount#)")>
				<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][5]	= "No"><!--- Flag "don't do End-of-Request logging" --->
			<cfelseif Variables.SleIncludeFileName IS "SleTimeEndAccum">
				<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][5]	= "Yes"><!--- Flag "do End-of-Request logging" --->
			</cfif>
		</cfif>
	<cfelseif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleTimeEnd"			& Request.SleHT
										& Request.SleScriptName	& Request.SleHT 
										& Request.SleUser		& RepeatString(Request.SleHT, 4)
										& "(not loggable because Request.SBALogTimeThisName not given)")>
	</cfif>
<cfelseif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
	<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
	<cfset Request.SleLogger.error	("SleTimeEnd"			& Request.SleHT
									& Request.SleScriptName	& Request.SleHT 
									& Request.SleUser		& RepeatString(Request.SleHT, 4)
									& "(log_SleTimeEndShared not called by log_SleTimeEnd or log_SleTimeEndAccum)")>
</cfif>

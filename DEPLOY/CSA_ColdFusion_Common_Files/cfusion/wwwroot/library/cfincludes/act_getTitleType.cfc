<!---
AUTHOR:				Robert Doyle
DATE:				05/20/2015
DESCRIPTION:		Used to return ajax json to add to dropdown list of user type code.
NOTES:				None
INPUT:				ImUserTypCd
OUTPUT:				JSON
REVISION HISTORY:	05/19/2015, RSD		Original Implementation.
--->

<cfcomponent>
<cffunction name="getTitle" access="remote" returnType="query" returnFormat="JSON">
	<cfargument name="ImUserTypCd" type="string">
	<cfset Variables.LogAct									= "retrieve Federal employee job titles">
	<cfset Variables.ImUserTypCd							= arguments.ImUserTypCd>
	<cfset Variables.cfprname								= "titleList">
	<cfset Variables.Identifier								= "12"><!--- Same as Identifier 4, only alphabetized. --->
	<cfset Variables.db										= "oracle_scheduled">
	<cfinclude template="/cfincludes/oracle/security/spc_IMJOBTITLTYPSELTSP.PUBLIC.cfm">
   	<cfreturn titleList>
</cffunction>
</cfcomponent>
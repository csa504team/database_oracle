<!---
AUTHOR:				Steve Seaquist
DATE:				03/18/2003 - 05/__/2003
DESCRIPTION:		Gets Form.#FNam#, strips it of double-quotes (w/Replace) and possible malicious code (w/Val). 
NOTES:				Assumes that val_init.cfm has been called. 
INPUT:				Variables.FNam, Form.#FNam#
OUTPUT:				Variables.#FNam#, guaranteed to exist, even if Form.#FNam# is undefined (unchecked checkbox, for example). 
					Clears Variabales.BlankScreen if it sees any non-blank field. 
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	05/__/2003, SRS:	Original implementation (release). 
					03/18/2003, SRS:	Original implementation (began). 
--->

<cfif IsDefined("Form.#FNam#")>
	<cfset Variables.FVal				= Replace(Trim(Evaluate("Form.#FNam#")), Chr(34), Chr(39), "ALL")>
	<cfset "Variables.#FNam#"			= Variables.FVal>
	<cfif Len(Variables.FVal) GT 0>
		<cfset Variables.BlankScreen	= "No">
	</cfif>
<cfelse>
	<cfset Variables.FVal				= "">
	<cfset "Variables.#FNam#"			= "">
</cfif>

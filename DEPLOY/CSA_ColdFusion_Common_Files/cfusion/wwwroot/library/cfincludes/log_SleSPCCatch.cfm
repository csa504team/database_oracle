<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs CFCatch that occurred in an SPC file. 
NOTES:				Called by generated SPC files. 
INPUT:				Request.SBALogSystemName, maybe. If so, also Variables.SleEntityName (generated). 
OUTPUT:				If currently configured to log it, CFCatch log entry ("SleCatch"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<cfinclude template="get_SleStripHTCRLF.cfm">
		<cfif IsDefined("Variables.db")>
			<cfset Variables.SleDB				= Variables.db>
		<cfelse><!--- Probable cause of the CFCatch, don'tcha think? --->
			<cfset Variables.SleDB				= "(undefined)">
		</cfif>
		<cfif NOT IsDefined("Variables.SleEntityName")>
			<cfset Variables.SleEntityName		= "(not from a generated SPC file)">
		</cfif>
		<cfif IsDefined("CFStoredProc.ExecutionTime")>
			<cfset Variables.SleExecutionTime	= CFStoredProc.ExecutionTime>
		<cfelse>
			<cfset Variables.SleExecutionTime	= "">
		</cfif>
		<cfif IsDefined("CFCatch.Message")>
			<cfif IsDefined("CFCatch.Detail")>
				<cfset Variables.SleLogText		= CFCatch.Message & " " & CFCatch.Detail>
			<cfelse>
				<cfset Variables.SleLogText		= CFCatch.Message & " (no further details)">
			</cfif>
		<cfelseif IsDefined("CFCatch.Detail")>
			<cfset Variables.SleLogText			= "(no overview message) " & CFCatch.Detail>
		<cfelse>
			<cfset Variables.SleLogText			= "(not from a generated SPC file)">
		</cfif>
		<cfset Variables.SleLogText				= SleStripHTCRLF(Variables.SleLogText)>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleCatch"						& Request.SleHT
										& Request.SleScriptName			& Request.SleHT 
										& Request.SleUser				& Request.SleHT 
										& Variables.SleDB				& Request.SleHT 
										& Variables.SleEntityName		& Request.SleHT
										& Variables.SleExecutionTime	& Request.SleHT
										& Variables.SleLogText)>
	</cfif>
</cfif>

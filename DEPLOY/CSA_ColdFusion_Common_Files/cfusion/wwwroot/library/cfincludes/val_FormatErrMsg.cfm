<!---
AUTHOR:				Steve Seaquist
DATE:				12/15/2000
DESCRIPTION:		Included by other val files. Builds ErrMsg in a consistent way. 
NOTES:				None
INPUT:				Variables.FieldErrMsg
OUTPUT:				Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	12/15/2000, SRS:	Original implementation.
--->
<CFSET Variables.ErrMsg								= Variables.ErrMsg & "<BR>" & CHR(13) & CHR(10) 
													& "<LI>" & Variables.FieldErrMsg>
<CFSET Variables.SaveMe								= "No">

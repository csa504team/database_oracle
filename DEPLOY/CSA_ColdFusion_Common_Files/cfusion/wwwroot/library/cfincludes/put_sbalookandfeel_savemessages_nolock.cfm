<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Saves just Commentary and ErrMsg to the Session scope. Use in "form data recovery".
NOTES:				Assumes a Session lock is active. This MUST be an Exclusive lock.
OUTPUT:				Variables in the Session scope.
REVISION HISTORY:	08/01/2017, AR: 	Added handling of WarnMsg for use in MPERS
									09/28/2005, SRS:	Original implementation.
--->

<cfif IsDefined("Variables.Commentary")>
	<cfset Session.SlafSaveFormData.Commentary				= Variables.Commentary>
<cfelse>
	<cfset Session.SlafSaveFormData.Commentary				= "">
</cfif>
<cfif IsDefined("Variables.ErrMsg")>
	<cfif IsDefined("Variables.ErrMsgBase") AND (Len(Variables.ErrMsg) LE Len(Variables.ErrMsgBase))>
		<cfset Session.SlafSaveFormData.ErrMsg				= "">
	<cfelse>
		<cfset Session.SlafSaveFormData.ErrMsg				= Variables.ErrMsg>
	</cfif>
<cfelse>
	<cfset Session.SlafSaveFormData.ErrMsg					= "">
</cfif>

<cfif IsDefined("Variables.WarnMsg")>
	<cfif IsDefined("Variables.WarnMsgBase") AND (Len(Variables.WarnMsg) LE Len(Variables.WarnMsgBase))>
		<cfset Session.SlafSaveFormData.WarnMsg				= "">
	<cfelse>
		<cfset Session.SlafSaveFormData.WarnMsg				= Variables.WarnMsg>
	</cfif>
<cfelse>
	<cfset Session.SlafSaveFormData.WarnMsg					= "">
</cfif>

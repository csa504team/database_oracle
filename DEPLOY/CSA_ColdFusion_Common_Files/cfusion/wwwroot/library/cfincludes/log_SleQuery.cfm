<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs successful completion of a CFQuery. 
NOTES:				Called by manually coded cfincludes. 
INPUT:				Request.SBALogSystemName, maybe. 
OUTPUT:				If currently configured to log it, CFQuery log entry ("SleQuery"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsInfoEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<cfif IsDefined("Variables.db")>
			<cfset Variables.SleDB				= Variables.db>
		<cfelse><!--- Probable cause of the CFCatch, don'tcha think? --->
			<cfset Variables.SleDB				= "(undefined)">
		</cfif>
		<cfset Variables.SleEntityName			= "">
		<cfif IsDefined("CFQuery.ExecutionTime")>
			<cfset Variables.SleExecutionTime	= CFQuery.ExecutionTime>
		<cfelse>
			<cfset Variables.SleExecutionTime	= "">
		</cfif>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.info	("SleQuery"						& Request.SleHT
										& Request.SleScriptName			& Request.SleHT 
										& Request.SleUser				& Request.SleHT 
										& Variables.SleDB				& Request.SleHT 
										& ""							& Request.SleHT
										& Variables.SleExecutionTime	& Request.SleHT)>
	</cfif>
</cfif>

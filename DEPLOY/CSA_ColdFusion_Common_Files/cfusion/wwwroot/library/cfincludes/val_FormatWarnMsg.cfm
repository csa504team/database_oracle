<!---
AUTHOR:				Sheen Mathew Justin
DATE:				07/19/2016
DESCRIPTION:		Included by other val files. Builds WarnMsg in a consistent way.
					Based on Steve Seaquist's val_FormatWarnMsg.cfm, but doesn't set SaveMe to "No".
NOTES:				None
INPUT:				Variables.WarnMsg,Variables.FieldWarnMsg
OUTPUT:				Variables.WarnMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/19/2016, SMJ:	Original implementation.
--->
<cfset Variables.WarnMsg							= Variables.WarnMsg & "<BR>" & CHR(13) & CHR(10) 
													& "<LI>" & Variables.FieldWarnMsg>

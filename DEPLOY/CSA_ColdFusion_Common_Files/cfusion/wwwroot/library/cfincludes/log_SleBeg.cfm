<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs Start-of-Request.
NOTES:				Called by inc_starttickcount.cfm.
INPUT:				Request.SBALogSystemName, maybe. Otherwise, none.
OUTPUT:				If currently configured to log it, Start-of-Request log entry ("SleBeg").
REVISION HISTORY:	10/21/2005, SRS:	Original implementation.
--->
<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsDebugEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.debug	("SleBeg"				& Request.SleHT
										& Request.SleScriptName	& Request.SleHT
										& Request.SleUser		& RepeatString(Request.SleHT, 4))>
	</cfif>
</cfif>

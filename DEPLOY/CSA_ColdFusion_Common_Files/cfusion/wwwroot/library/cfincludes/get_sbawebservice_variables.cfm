<!---
AUTHOR:				Steve Seaquist, Alpha Omega Integration, Inc, for the US Small Business Administration. 
DATE:				01/16/2009. 
DESCRIPTION:		Makes sure that the WSDL is generated properly for the SBA's BIG-IP environment. 
NOTES:

	Include this file right after the cfcomponent tag. This means that you don't have to include get_sbashared_variables, 
	because we include it here. In other words get_sbawebservice_variables for the Web Services environment is the 
	equivalent of get_sbalookandfeel_variables for the webpage environment. (And neither one will work in the other's 
	environment.) 

	The protocol has to be http in development and when a non-development box calls a Web Service on the same box. 
	In all other cases, it has to be https. This code does it right. It hasn't yet been tested under BIG-IP, where 
	there can be more than one server per BIG-IP and more than one instance per server. 

INPUT:				None. 
OUTPUT:				getMetaData(this).serviceaddress. 
REVISION HISTORY:	08/31/2017, SRS:	Preparation for release (for internal web services only). 
					01/16/2009, SRS:	Original implementation. 
--->

<!--- Initializations: --->

<cfif NOT IsDefined("Request.SlafDevTestProd")>
	<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
</cfif>
<cfif NOT IsDefined("Request.SlafServerAddr")>
	<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
</cfif>

<!--- Determine proper getMetaData(this).serviceaddress --->

<cfswitch expression="#Request.SlafDevTestProd#">
<cfcase value="Dev">
	<cfset Variables.Protocol				= "http">
</cfcase>
<cfdefaultcase>
	<cfset Variables.Protocol				= "https">
</cfdefaultcase>
</cfswitch>
<cfif Request.SlafServerAddr IS CGI.Remote_Addr>
	<cfset Variables.Protocol				= "http">
</cfif>
<cfset Variables.URL						= Variables.Protocol
											& "://"
											& CGI.Server_Name
											& CGI.Script_Name>

<!--- Some magic from Elliott Sprehn in the ColdFusion 9 prerelease program's User Forums: --->

<cfset getMetaData(this).serviceaddress		="#Variables.URL#">

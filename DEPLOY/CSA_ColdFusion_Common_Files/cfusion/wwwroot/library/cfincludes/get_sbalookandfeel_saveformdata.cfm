<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Builds variables in the Variables scope that were last saved from the Form scope ("form data recovery"). 
NOTES:				Although get_sbalookandfeel_variables.cfm also does this, it only does so if the PageNames mechanism 
					is in use and the current page name (Variables.PageName) is in PageNames. This include is for manually 
					controlling form data recovery. DO NOT INCLUDE THIS FILE FROM WITHIN get_sbalookandfeel_variables. To do 
					so would result in nested locks of the Session scope, which causes a deadlock. Instead use the "_nolock" 
					version. 
INPUT:				Variables in the Session scope. 
OUTPUT:				Variables in the Variables scope. 
REVISION HISTORY:	09/26/2005, SRS:	Original implementation. 
--->

<cflock scope="Session" type="ReadOnly" timeout="30">
	<cfinclude template="get_sbalookandfeel_saveformdata_nolock.cfm">
</cflock>

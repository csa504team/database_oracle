<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Defines SleStripHTCRLF. 
NOTES:				Apparently, if ColdFusion sees a UDF definition in cfscript, it goes ahead and defines it, even if the 
					UDF definition is subordinate to a <cfif>. Therefore, the only way to prevent a crash (from defining 
					the UDF more than once) is to put the UDF definition into a separate file (this file) and use the <cfif> 
					to decide whether or not to include it. To my way of thinking, this file a workaround for a CF bug. 
INPUT:				None. 
OUTPUT:				SleStripHTCRLF. 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfscript>
function SleStripHTCRLF	(pString)
{
return Replace(Replace(Replace(pString, Chr( 9), " ", "ALL"),
										Chr(10), " ", "ALL"), 
										Chr(13), " ", "ALL");
}
</cfscript>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs Start-of-"TimeThis"-passage.
NOTES:				Called by manual include.
INPUT:				Request.SBALogSystemName, maybe. If so, Request.SBALogTimeThisName is required.
OUTPUT:				If currently configured to log it, Start-of-"TimeThis"-passage log entry ("SleTimeBeg").
REVISION HISTORY:	10/21/2005, SRS:	Original implementation.
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<!--- Always do SleTimeThisArray maintenance, in case we're logging at the info level but not at the debug level. --->
	<cfif IsDefined("Request.SBALogTimeThisName") AND (Len(Request.SBALogTimeThisName) GT 0)>
		<cfinclude template="get_SleStripHTCRLF.cfm">
		<cfset Variables.SleEntityName								= SleStripHTCRLF(Request.SBALogTimeThisName)>
		<cfset Variables.SleTimeThisNameIdx							= 0>
		<cfif IsDefined("Request.SleTimeThisArray")>
			<cfloop index="SleIdx" from="1" to="#ArrayLen(Request.SleTimeThisArray)#">
				<cfif Request.SleTimeThisArray[SleIdx][1] IS Variables.SleEntityName>
					<cfset Variables.SleTimeThisNameIdx				= SleIdx>
					<cfbreak>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset Request.SleTimeThisArray							= ArrayNew(2)>
		</cfif>
		<cfif Variables.SleTimeThisNameIdx IS 0>
			<cfset Variables.SleTimeThisNameIdx						= ArrayLen(Request.SleTimeThisArray) + 1>
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][1]	= Variables.SleEntityName>
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][3]	= 0><!--- Iteration --->
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][4]	= 0><!--- Total Execution Time for Name --->
			<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][5]	= "No"><!--- Default "don't do End-of-Request logging" --->
		</cfif>
		<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][2]		= GetTickCount()>
		<cfset Request.SleTimeThisArray[SleTimeThisNameIdx][3]		= Request.SleTimeThisArray[SleTimeThisNameIdx][3] + 1>
		<cfif Request.SleIsDebugEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
			<cfset SleCount											= Request.SleTimeThisArray[SleTimeThisNameIdx][3]>
			<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
			<cfset Request.SleLogger.debug	("SleTimeBeg"				& Request.SleHT
											& Request.SleScriptName		& Request.SleHT
											& Request.SleUser			& RepeatString(Request.SleHT, 2)
											& Variables.SleEntityName	& RepeatString(Request.SleHT, 2)
											& "(iteration #SleCount#)")>
		</cfif>
	<cfelseif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleTimeBeg"			& Request.SleHT
										& Request.SleScriptName	& Request.SleHT
										& Request.SleUser		& RepeatString(Request.SleHT, 4)
										& "(not loggable because Request.SBALogTimeThisName not given)")>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs CFCatch that did NOT occur in an SPC file. 
NOTES:				SPC files call log_SleSPCCatch.cfm, so this is for everyone else. Since all stored procedure calls are 
					required by SBA ColdFusion Programming Standards to be via SPC files, the assumption is that the CFCatch 
					is for a <cfquery>, hence the attempt to find cfquery.ExecutionTime. If it's some other kind of CFCatch, 
					no harm done. 
INPUT:				Request.SBALogSystemName, maybe. 
OUTPUT:				If currently configured to log it, CFCatch log entry ("SleCatch"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<cfinclude template="get_SleStripHTCRLF.cfm">
		<cfif IsDefined("Variables.db")>
			<cfset Variables.SleDB				= Variables.db>
		<cfelse><!--- Probable cause of the CFCatch, don'tcha think? --->
			<cfset Variables.SleDB				= "(undefined)">
		</cfif>
		<cfset Variables.SleEntityName			= "">
		<cfif IsDefined("CFQuery.ExecutionTime")>
			<cfset Variables.SleExecutionTime	= CFQuery.ExecutionTime>
		<cfelse>
			<cfset Variables.SleExecutionTime	= "">
		</cfif>
		<cfif IsDefined("CFCatch.Message")>
			<cfif IsDefined("CFCatch.Detail")>
				<cfset Variables.SleLogText		= CFCatch.Message & " " & CFCatch.Detail>
			<cfelse>
				<cfset Variables.SleLogText		= CFCatch.Message & " (no further details)">
			</cfif>
		<cfelseif IsDefined("CFCatch.Detail")>
			<cfset Variables.SleLogText			= "(no overview message) " & CFCatch.Detail>
		<cfelse>
			<cfset Variables.SleLogText			= "(log_SleCatch called outside of a CFCatch block)">
		</cfif>
		<cfset Variables.SleLogText				= SleStripHTCRLF(Variables.SleLogText)>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleCatch"						& Request.SleHT
										& Request.SleScriptName			& Request.SleHT 
										& Request.SleUser				& Request.SleHT 
										& Variables.SleDB				& Request.SleHT 
										& Variables.SleEntityName		& Request.SleHT
										& Variables.SleExecutionTime	& Request.SleHT
										& Variables.SleLogText)>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				06/18/2008
DESCRIPTION:		Shared routine to restore the data from the Variables structure (originally set by the 
					/library/udf/val routines) and processing time. Crashes if called by a Web Service or by a Web page 
					with no Session scope. But never crashes if called by a Web page that has Session scope. (Simply does 
					nothing if Session.SlafActPageVariables doesn't exist.) 
NOTES:				Cfinclude this page at the start of an update page, if the validation page called the put routine. 
					DO NOT CFINCLUDE THIS PAGE FROM A DISPLAY PAGE THAT USES FORM DATA RECOVERY. (There's really no need 
					to cfinclude it from a display page at all, actually. So don't do that.) 
INPUT:				Session.SlafActPageVariables. 
OUTPUT:				Variables. For every key xxx of Variables, copies value to Variables.xxx, 
					because that's where the SPC files expect the cleaned up data to be. 
REVISION HISTORY:	06/18/2008, SRS:	Original Implementation. 
--->

<cfset Request.SlafActPageVariables					= StructNew()>
<cflock scope="SESSION" type="READONLY" timeout="30">
	<cfif IsDefined("Session.SlafActPageVariables")>
		<cfset Request.SlafActPageVariables			= Duplicate(Session.SlafActPageVariables)>
	</cfif>
</cflock>
<cfloop index="SapvKey" list="#StructKeyList(Request.SlafActPageVariables)#">
	<cfset Variables[SapvKey]						= Request.SlafActPageVariables[SapvKey]>
</cfloop>
<cfset StructClear(Request.SlafActPageVariables)>
<cfset StructDelete(Request,"SlafActPageVariables")>

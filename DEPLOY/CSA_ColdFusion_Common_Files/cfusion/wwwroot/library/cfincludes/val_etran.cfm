<!---
AUTHOR:				Steve Seaquist
DATE:				09/10/2002
DESCRIPTION:		Validates that Variables.Name can be stored in a CF_SQL_[VAR]CHAR1
NOTES:				Called by act_presave_CurrTableRow (and only by act_presave_CurrTableRow). 
INPUT:				Variables.TblNm, Variables.ColNm, Variables.ValDataType, Variables.ValMaxLength and Variables.ValScale. 
					"Variables.#Variables.ColNm#" contains the field's data, and will be cleaned up. 
OUTPUT:				Variables.EtranErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	09/10/2002, SRS:	Original implementation. 
--->

<cfset Variables.TblNmColNm							= Variables.TblNm & "." & Variables.ColNm>
<!--- The following MUST exist, because Variables.ColNm is coming out of StructKeyList(Variables.CurrTblRow): --->
<cfset Variables.ColData							= Trim(Evaluate("Variables.CurrTblRow.#Variables.ColNm#"))>
<cfset Variables.ColDataLen							= Len(Variables.ColData)>
<cfset Variables.TblNmColNm							= "#Variables.TblNmColNm# ('#Variables.ColData#')">

<cfif Variables.ColDataLen GT 0><!--- No need to validate "". Mandatories are caught in act_presave_CurrTblRow. --->
	<cfswitch expression="#Variables.ValDataType#">
	<cfcase value="char,varchar">
		<cfif Len(Variables.ColData) GT Variables.ValMaxLength>
			<cfset Variables.EtranErrMsg			= "#Variables.TblNmColNm# cannot be more than #Variables.ValMaxLength# "
													& "character(s) long.">
			<cfinclude template="FormatETranErrMsg.cfm">
		</cfif>
	</cfcase>
	<cfcase value="int,money,double,numeric,smallint,tinyint">
		<cfset Variables.ColData					= Replace(Variables.ColData, "$", "", "ALL")>
		<cfset Variables.ColData					= Replace(Variables.ColData, ",", "", "ALL")>
		<cfif IsNumeric(Variables.ColData)>
			<cfswitch expression="#Variables.ValDataType#">
			<cfcase value="int">
				<cfset Variables.ColData			= Round(Variables.ColData)>
				<cfif		Variables.ColData LT -2415919104>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be less than -2,415,919,104.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif	Variables.ColData GT  2415919103>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be greater than 2,415,919,103.">
					<cfinclude template="FormatETranErrMsg.cfm">
				</cfif>
			</cfcase>
			<cfcase value="money,double">
				<cfset Variables.ColData			= Round(Variables.ColData * 100) / 100>
				<cfif		Variables.ColData LT -922337203685477.58>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be less than -$922,337,203,685,477.58.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif	Variables.ColData GT 922337203685477.58>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be greater than $922,337,203,685,477.58.">
					<cfinclude template="FormatETranErrMsg.cfm">
				</cfif>
			</cfcase>
			<cfcase value="numeric">
				<cfset Variables.ColDataPeriod		= Find(".", Variables.ColData)>
				<cfif Variables.ColDataPeriod IS 0>
					<cfset Variables.ColDataInt		= Variables.ColData>
					<cfset Variables.ColDataFrac	= "">
				<cfelseif Variables.ColDataPeriod IS 1>
					<cfset Variables.ColDataInt		= "">
					<cfif Variables.ColDataLen GT 1>
						<cfset Variables.ColDataFrac= Right	(Variables.ColData, Variables.ColDataLen - 1)>
					<cfelse>
						<cfset Variables.ColDataFrac= "">
					</cfif>
				<cfelseif Variables.ColDataPeriod IS Variables.ColDataLen>
					<cfset Variables.ColDataFrac	= "">
					<cfif Variables.ColDataLen GT 1>
						<cfset Variables.ColDataInt	= Left	(Variables.ColData, Variables.ColDataLen - 1)>
					<cfelse>
						<cfset Variables.ColDataInt	= "">
					</cfif>
				<cfelse>
					<cfset Variables.ColDataInt		= Left	(Variables.ColData, Variables.ColDataPeriod - 1)>
					<cfset Variables.ColDataFrac	= Right	(Variables.ColData, Variables.ColDataLen - Variables.ColDataPeriod)>
				</cfif>
				<cfif (Len(Variables.ColDataInt) + Len(Variables.ColDataFrac)) GT Variables.ValMaxLength>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot exceed #Variables.ValMaxLength# digits.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif Len(Variables.ColDataInt) GT (Variables.ValMaxLength - Variables.ValScale)>
					<cfset Variables.EtranErrMsg	= "The integer part of #Variables.TblNmColNm# cannot exceed "
													& "#Evaluate(Variables.ValMaxLength - Variables.ValScale)# digits.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif Len(Variables.ColDataFrac) GT Variables.ValScale>
					<cfset Variables.EtranErrMsg	= "The fractional part of #Variables.TblNmColNm# cannot exceed "
													& "#Variables.ValScale# digits.">
					<cfinclude template="FormatETranErrMsg.cfm">
				</cfif>
			</cfcase>
			<cfcase value="smallint">
				<cfset Variables.ColData			= Round(Variables.ColData)>
				<cfif		Variables.ColData LT -32768>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be less than -32,768.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif	Variables.ColData GT  32767>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be greater than 32,767.">
					<cfinclude template="FormatETranErrMsg.cfm">
				</cfif>
			</cfcase>
			<cfcase value="tinyint">
				<cfset Variables.ColData			= Round(Variables.ColData)>
				<cfif		Variables.ColData LT 0>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be less than 0.">
					<cfinclude template="FormatETranErrMsg.cfm">
				<cfelseif	Variables.ColData GT 255>
					<cfset Variables.EtranErrMsg	= "#Variables.TblNmColNm# cannot be greater than 255.">
					<cfinclude template="FormatETranErrMsg.cfm">
				</cfif>
			</cfcase>
			</cfswitch>
		<cfelse>
			<cfset Variables.EtranErrMsg			= "#Variables.TblNmColNm# must be numeric.">
			<cfinclude template="FormatETranErrMsg.cfm">
		</cfif>
	</cfcase>
	<cfcase value="datetime,smalldatetime">
		<cfif Variables.ValDataType IS "datetime">
			<cfset Variables.MinDate				= "01/01/1753">
			<cfset Variables.MaxDate				= "12/31/9999">
		<cfelse>
			<cfset Variables.MinDate				= "01/01/1900">
			<cfset Variables.MaxDate				= "06/06/2079">
		</cfif>
		<cfif IsDate(Variables.ColData)>
			<!--- Reformat into a format that Sybase can for-sure handle: --->
			<cfset Variables.ColData				= "#DateFormat(Variables.ColData, 'MM/DD/YYYY')# "
													& "#TimeFormat(Variables.ColData, 'HH:MM:SS')#">
			<!--- Check for valid Sybase date range: --->
			<cfif (DateCompare(Variables.ColData,Variables.MinDate,"d") LT 0)OR (DateCompare(Variables.ColData,Variables.MaxDate,"d") GT 0)>
				<cfset Variables.EtranErrMsg		= "#Variables.TblNmColNm# exceeds the acceptable date range #Variables.MinDate# through #Variables.MaxDate#.">
				<cfinclude template="FormatETranErrMsg.cfm">
			</cfif>
		<cfelse>
			<cfset Variables.EtranErrMsg			= "#Variables.TblNmColNm# cannot be converted to a date/time value.">
			<cfinclude template="FormatETranErrMsg.cfm">
		</cfif>
	</cfcase>
	<cfdefaultcase>
		<cfset Variables.EtranErrMsg				= "INTERNAL ERROR. #Variables.TblNmColNm# had unexpected data type.">
		<cfinclude template="FormatETranErrMsg.cfm">
	</cfdefaultcase>
	</cfswitch>
</cfif>
<cfset "Variables.#Variables.ColNm#"				= Variables.ColData>
<!---
Replace the data in CurrTableRow, so that the cleaned-up value will be loaded in Pass 2. And becauase CurrTblRow is a 
StructCopy, the cleaned up value will propagate through to the underlying Variables.ETranStruct.ArrayTblxxx[CurrIdx] struct. 
--->
<cfset "Variables.CurrTblRow.#Variables.ColNm#"		= Variables.ColData>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Gets Request.SBALogger, our shared Category object cached in the Server scope. Then instantiates 
					a logger for the current Request.SBALogSystemName if it hasn't already been instantiated. 
NOTES:				Assumes IsDefined("Request.SBALogSystemName"), so that we don't have to keep testing it in each 
					nested include. Included at the start of all logging routines before writing to the log. 
INPUT:				None. 
OUTPUT:				Request.SBALogger ("main logger"), Request.SleLogger ("system logger"). 
					Maybe create Server.SBALogger ("cached main logger") if 
REVISION HISTORY:	08/21/2014, SRS:	Changes to make identifying user easier and instance name less disk-consuming. 
					05/14/2014, SRS:	Automatic edit from "CLS" to "GLS" that probably shouldn't even have been done yet. 
					07/18/2007, SRS:	Create a fake logger if logging is off. 
					02/14/2007, SRS:	Added debugging and self-test mode to figure out why logging isn't turning off. 
					10/21/2005, SRS:	Original implementation. 
--->

<cfset Variables.SBALogConfigInitialized				= "No"><!--- Could change on ***ANY*** call. --->
<cfset Variables.SBALogSelfTestMode						= "No">
<cfset Request.SBALogErrMsg								= "">
<cfset Request.SBALogIsOff								= "Yes"><!--- Per Ron Whalen, default to "off". --->
<cfif CGI.Script_Name IS "/library/cfincludes/get_SBALogEnvironment.cfm">
	<cfset Variables.SBALogSelfTestMode					= "Yes">
	<cfif IsDefined("URL.System")>
		<cfset Request.SBALogSystemName					= URL.System>
	<cfelse>
		<cfset Request.SBALogSystemName					= "GLS"><!--- ... until the non-GLS logger for CLS exists. --->
	</cfif>
	<!--- If called directly, leave Variables.SBALogConfigInitialized set to "No" so we can see what happens. --->
<cfelse>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SBALogConfigInitialized")>
			<cfset Variables.SBALogConfigInitialized	= Server.SBALogConfigInitialized>
			<cfif Variables.SBALogConfigInitialized>
				<cfset Request.SBALogConfigInitDateTime	= Server.SBALogConfigInitDateTime>
				<cfset Request.SBALogErrMsg				= Server.SBALogErrMsg>
				<cfset Request.SBALogger				= Server.SBALogger>
				<cfset Request.SBALogIsOff				= Server.SBALogIsOff>
			</cfif>
		</cfif>
	</cflock>
</cfif>
<cfif NOT Variables.SBALogConfigInitialized>
	<cfset Variables.SBALogConfigFileLocation			= ExpandPath("/logging/log4j.xml")>
	<cfif FileExists(Variables.SBALogConfigFileLocation)>
		<cftry>
			<cffile action="READ" file="#Variables.SBALogConfigFileLocation#" variable="Variables.SBALogConfigFile">
			<cfcatch type="Any">
				<cfset Request.SBALogErrMsg				= "Couldn't read #Variables.SBALogConfigFileLocation#: "
														& CFCatch.Message & " " & CFCatch.Detail>
			</cfcatch>
		</cftry>
		<cfif Len(Request.SBALogErrMsg) IS 0>
			<cftry>
				<cfset Variables.SBALogXMLDoc			= XMLParse(Variables.SBALogConfigFile)>
				<cfcatch type="Any">
					<cfset Request.SBALogErrMsg			= "Couldn't parse #Variables.SBALogConfigFileLocation# contents: "
														& CFCatch.Message & " " & CFCatch.Detail>
				</cfcatch>
			</cftry>
		</cfif>
		<cfif Len(Request.SBALogErrMsg) IS 0>
			<cfif Variables.SBALogXMLDoc['log4j:configuration'].XmlAttributes['threshold'] IS "off">
				<!--- This is not actually an error, but any non-zero length forces the creation of the fake logger. --->
				<cfset Request.SBALogErrMsg				= "SBA logging threshold is off.">
			</cfif>
		</cfif>
	<cfelse>
		<cfset Request.SBALogErrMsg						= "File not found at #Variables.SBALogConfigFileLocation#">
	</cfif>
	<cfif Len(Request.SBALogErrMsg) GT 0>
		<cfinclude template="get_SBALogEnvironmentOff.cfm">
	<cfelse>
		<cfset Variables.SleConfigurator				= CreateObject("Java", "org.apache.log4j.xml.DOMConfigurator")>
		<cfset Variables.SleConfigurator.configure(Variables.SBALogConfigFileLocation)>
		<cfset Request.SBALogger						= CreateObject("Java", "org.apache.log4j.Category")>
		<cfset Request.SBALogIsOff						= "No">
	</cfif>
	<cfset Request.SBALogConfigInitDateTime				= DateFormat(Now(), "YYYY-MM-DD")
														& " "
														& TimeFormat(Now(), "hh:mm:ss tt")>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SBALogConfigInitDateTime			= Request.SBALogConfigInitDateTime>
		<cfset Server.SBALogErrMsg						= Request.SBALogErrMsg>
		<cfset Server.SBALogIsOff						= Request.SBALogIsOff>
		<cfif Server.SBALogIsOff>
			<cfset Server.SBALogger						= Duplicate(Request.SBALogger)>
		<cfelse>
			<cfset Server.SBALogger						=			Request.SBALogger>
		</cfif>
		<cfset Server.SBALogConfigInitialized			= "Yes">
	</cflock>
	<cfset StructDelete(Request, "SleLogger")><!--- Force [re-]instantiation, below, if SBALogger changed. --->
</cfif>

<cfif (NOT IsDefined("Request.SleLogger")) OR (Request.SleSystemName IS NOT Request.SBALogSystemName)>
	<cfif Request.SBALogIsOff>
		<cfif NOT IsDefined("Request.SleLogger")>
			<cfinclude template="get_SleLoggerOff.cfm">
		</cfif>
		<cfset Request.SleIsDebugEnabled				= "No">
		<cfset Request.SleIsInfoEnabled					= "No">
		<cfset Request.SleIsWarnEnabled					= "No">
		<cfset Request.SleIsErrorEnabled				= "No">
		<cfset Request.SleIsFatalEnabled				= "No">
	<cfelse>
		<!--- SBALogSystemName can change within a request, so make sure SleLogger is for the current system: --->
		<cfset Request.SleLogger						= Request.SBALogger.getInstance(Request.SBALogSystemName)>
		<cfset Request.SleIsDebugEnabled				= Request.SleLogger.isDebugEnabled()>
		<cfset Request.SleIsInfoEnabled					= Request.SleLogger.isInfoEnabled()>
		<cfset Variables.SBALogPriorities				= CreateObject("Java", "org.apache.log4j.Priority")>
		<cfset Request.SleIsWarnEnabled					= Request.SleLogger.isEnabledFor(Variables.SBALogPriorities.WARN)>
		<cfset Request.SleIsErrorEnabled				= Request.SleLogger.isEnabledFor(Variables.SBALogPriorities.ERROR)>
		<cfset Request.SleIsFatalEnabled				= Request.SleLogger.isEnabledFor(Variables.SBALogPriorities.FATAL)>
	</cfif>
	<cfset Request.SleSystemName						= Request.SBALogSystemName>
	<cfset Request.SleHT								= Chr(9)><!--- Saves on Chr calls. --->
	<cfset Request.SleScriptName						= CGI.Script_Name>
	<cfif NOT IsDefined("Request.SlafServerInstanceName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfif Request.SlafServerInstanceName IS "coldfusion"><!--- That is, if a standalone installation of CF: --->
		<cfset Request.SleScriptName					&= " (cf)">
	<cfelse>
		<cfset Request.SleScriptName					&= " ("
														& Replace(Request.SlafServerInstanceName, "instance", "i", "ONE")
														& ")">
	</cfif>
	<cfif		IsDefined("Variables.OrigUserId")>		<cfset Request.SleUser	= Variables.OrigUserId>
	<cfelse>											<cfset Request.SleUser	= Replace(CGI.Remote_Addr, "165.110.", "", "ONE")></cfif>
	<!--- DB, EntityName, ExeutionTime and LogText vary with each call, so don't set defaults for them. --->
</cfif>

<cfif Variables.SBALogSelfTestMode>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfloop index="Key" list="#StructKeyList(Server)#">
			<!--- Dump only Server variables beginning with "SBALog": --->
			<cfif (Len(Key) GE 6) AND (Left(Key,6) IS "SBALog")>
				<cfif IsSimpleValue(Evaluate("Server.#Key#"))>
					<cfoutput>
Server.#Key# = &quot;#Evaluate("Server.#Key#")#&quot;<br></cfoutput>
				<cfelse>
					<cfdump var="#Evaluate('Server.'&Key)#" label="Server.#Key#">
				</cfif>
			</cfif>
		</cfloop>
	</cflock>
	<cfdump var="#Request#"		label="Request">
	<cfdump var="#Variables#"	label="Variables">
	<cfset Request.SleLogger.debug("Attempt to call function in a struct.")>
	<cfoutput><br>Attempt to call function in a struct succeeded.</cfoutput>
</cfif>

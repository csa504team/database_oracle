<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				06/18/2008
DESCRIPTION:		Shared routine to save Variables scope data and processing time. Always saves processing time. 
					Crashes if called by a Web Service or by a Web page with no Session scope. But never crashes if 
					called by a Web page that has Session scope. (Saves 0 as the processing time if inc_starttickcount 
					wasn't included earlier.) 
NOTES:				Cfinclude this page at the end of a validation page just before the cflocation to the update page. 
INPUT:				Variables scope data. 
OUTPUT:				Session.SlafActPageVariables. 
REVISION HISTORY:	06/18/2008, SRS:	Original Implementation. 
--->

<cfif NOT IsDefined("Variables.SlafPrevTickCounts")>
	<cfset Variables.SlafPrevTickCounts							= 0>
</cfif>
<cfset Variables.SlafUpdPageTickCount							= -1><!--- -1 = "tick count unavailable in upd page". --->
<cfif IsDefined("Request.RequestStartTickCount")>
	<cfset Variables.SlafUpdPageTickCount						= (GetTickCount() - Request.RequestStartTickCount)>
	<cfset Variables.SlafPrevTickCounts							= Variables.SlafPrevTickCounts
																+ Variables.SlafUpdPageTickCount>
</cfif>
<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfif NOT IsDefined("Variables.SlafActPageTickCount")>
		<cfset Variables.SlafActPageTickCount					= 0>
		<cfif IsDefined("Session.SlafActPageVariables.SlafActPageTickCount")>
			<cfset Variables.SlafActPageTickCount				= Session.SlafActPageVariables.SlafActPageTickCount>
		</cfif>
	</cfif>
	<cfif IsDefined("Session.SlafActPageVariables")>
		<!--- Liberate Session memory ASAP: --->
		<cfset StructClear(Session.SlafActPageVariables)>
	<cfelse>
		<cfset Session.SlafActPageVariables						= StructNew()>
	</cfif>
	<!--- From upd page to dsp page, Session.SlafActPageVariables is much smaller, containing only the following: --->
	<cfset Session.SlafActPageVariables.SlafActPageTickCount	= Variables.SlafActPageTickCount>
	<cfset Session.SlafActPageVariables.SlafUpdPageTickCount	= Variables.SlafUpdPageTickCount>
	<cfset Session.SlafActPageVariables.SlafPrevTickCounts		= Variables.SlafPrevTickCounts>
</cflock>

<!---
AUTHOR:				Steve Seaquist
DATE:				07/24/2007
DESCRIPTION:		Builds fake loger so that we are for-sure not going through log4j when logging's off. 
INPUT:				None. 
OUTPUT:				Request.SleLogger ("system logger"). 
REVISION HISTORY:	05/29/2014, SRS:	To guard against defining the same UDF twice, offloaded the actual definition of 
										SleLoggerDummyFunction to bld_SleLoggerDummyFunction. 
					07/24/2007, SRS:	Original implementation. 
--->

<cfif NOT IsDefined("SleLoggerDummyFunction")>
	<cfinclude template="bld_SleLoggerDummyFunction.cfm">
</cfif>

<!---
If logging is off, SleLogger is a Struct with dummy logging functions that we duplicate to create SleLogger. 
The dummy functions LOOK like methods of a Category/Logger object, because they're referenced the same way, 
to wit: Variables.SleLogger.debug(string). 
--->
<cfset Request.SleLogger								= StructNew()>
<cfset Request.SleLogger.debug							= SleLoggerDummyFunction>
<cfset Request.SleLogger.info							= SleLoggerDummyFunction>
<cfset Request.SleLogger.warn							= SleLoggerDummyFunction>
<cfset Request.SleLogger.error							= SleLoggerDummyFunction>
<cfset Request.SleLogger.fatal							= SleLoggerDummyFunction>

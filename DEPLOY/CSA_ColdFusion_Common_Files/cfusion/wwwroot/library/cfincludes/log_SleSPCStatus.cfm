<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs a bad statusCode returned in an SPC file. 
NOTES:				Called by generated SPC files. 
INPUT:				Request.SBALogSystemName, maybe. If so, also Variables.SleEntityName (generated). 
OUTPUT:				If currently configured to log it, StatusCode log entry ("SleStatusCode"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<cfif IsDefined("Variables.db")>
			<cfset Variables.SleDB				= Variables.db>
		<cfelse><!--- Probable cause of the CFCatch, don'tcha think? --->
			<cfset Variables.SleDB				= "(undefined)">
		</cfif>
		<cfif NOT IsDefined("Variables.SleEntityName")>
			<cfset Variables.SleEntityName		= "(not from a generated SPC file)">
		</cfif>
		<cfif IsDefined("CFStoredProc.ExecutionTime")>
			<cfset Variables.SleExecutionTime	= CFStoredProc.ExecutionTime>
		<cfelse>
			<cfset Variables.SleExecutionTime	= "">
		</cfif>
		<cfif IsDefined("CFStoredProc.statusCode")>
			<cfset Variables.SleStatusCode		= CFStoredProc.statusCode>
		<cfelse>
			<cfset Variables.SleStatusCode		= "(not from a generated SPC file)">
		</cfif>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleStatusCode"				& Request.SleHT
										& Request.SleScriptName			& Request.SleHT 
										& Request.SleUser				& Request.SleHT 
										& Variables.SleDB				& Request.SleHT 
										& Variables.SleEntityName		& Request.SleHT
										& Variables.SleExecutionTime	& Request.SleHT
										& Variables.SleStatusCode)>
	</cfif>
</cfif>

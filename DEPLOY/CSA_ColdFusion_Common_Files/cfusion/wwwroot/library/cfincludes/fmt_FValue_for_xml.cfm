<!---
AUTHOR:				Steve Seaquist
DATE:				03/29/2001
DESCRIPTION:		Variables.FValue for inclusion between 2 XML tags. 
NOTES:				None
INPUT:				Variables.FValue
OUTPUT:				Variables.FValue
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	03/29/2001, SRS:	Original implementation.
--->

<CFSET Variables.FValue	= Replace(Variables.FValue, "&",	"&amp;",	"ALL")>
<CFSET Variables.FValue	= Replace(Variables.FValue, "<",	"&lt;",		"ALL")>
<CFSET Variables.FValue	= Replace(Variables.FValue, ">",	"&gt;",		"ALL")>
<CFSET Variables.FValue	= Replace(Variables.FValue, "'",	"&apos;",	"ALL")>
<CFSET Variables.FValue	= Replace(Variables.FValue, """",	"&quot;",	"ALL")>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Defines SleStripHTCRLF if it hasn't already been defined. 
NOTES:				None. 
INPUT:				None. 
OUTPUT:				SleStripHTCRLF. 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif NOT IsDefined("Request.SleStripHTCRLFIsDefined")>
	<cfinclude template="get_SleStripHTCRLF_cfscript.cfm">
	<cfset Request.SleStripHTCRLFIsDefined				= "Yes">
</cfif>

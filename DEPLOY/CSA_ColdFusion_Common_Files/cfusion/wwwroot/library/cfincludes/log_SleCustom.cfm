<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs Custom log entry. 
NOTES:				Called by manual include. 
INPUT:				Request.SBALogSystemName, maybe. If that's given, Request.SBALogText is mandatory. 
OUTPUT:				If currently configured to log it, Custom log entry ("SleCustom"). 
REVISION HISTORY:	04/19/2007, SRS:	Added SleCustomLogLevel logic. Do not add this logic for other log_Sle routines. 
										Only SleCustom needs it. The rest should be logged at unchanging, known levels. 
										Also fixed the final logger call. It was checking SleIsErrorEnabled, but it was 
										calling Request.SleLogger.debug. Changed this to call Request.SleLogger.error. 
					10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif IsDefined("Request.SBALogText")>
		<cfif IsDefined("Request.SleCustomLogLevel") AND (Len(Request.SleCustomLogLevel) GT 0)>
			<!---
			We test on all caps, like the entry in the log file or the symbolic literal in the Priorities object. 
			Reset to nullstring to return to SleCustom's default log level (debug). 
			--->
			<cfswitch expression="#UCase(Request.SleCustomLogLevel)#">
			<cfcase value="DEBUG">
				<cfif Request.SleIsDebugEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
					<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
					<cfset Request.SleLogger.debug	("SleCustom"			& Request.SleHT
													& Request.SleScriptName	& Request.SleHT 
													& Request.SleUser		& RepeatString(Request.SleHT, 4)
													& Request.SBALogText)>
				</cfif>
			</cfcase>
			<cfcase value="INFO">
				<cfif Request.SleIsInfoEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
					<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
					<cfset Request.SleLogger.info	("SleCustom"			& Request.SleHT
													& Request.SleScriptName	& Request.SleHT 
													& Request.SleUser		& RepeatString(Request.SleHT, 4)
													& Request.SBALogText)>
				</cfif>
			</cfcase>
			<cfcase value="WARN">
				<cfif Request.SleIsWarnEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
					<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
					<cfset Request.SleLogger.warn	("SleCustom"			& Request.SleHT
													& Request.SleScriptName	& Request.SleHT 
													& Request.SleUser		& RepeatString(Request.SleHT, 4)
													& Request.SBALogText)>
				</cfif>
			</cfcase>
			<cfcase value="ERROR">
				<cfif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
					<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
					<cfset Request.SleLogger.error	("SleCustom"			& Request.SleHT
													& Request.SleScriptName	& Request.SleHT 
													& Request.SleUser		& RepeatString(Request.SleHT, 4)
													& Request.SBALogText)>
				</cfif>
			</cfcase>
 			<cfcase value="FATAL">
				<cfif Request.SleIsFatalEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
					<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
					<cfset Request.SleLogger.fatal	("SleCustom"			& Request.SleHT
													& Request.SleScriptName	& Request.SleHT 
													& Request.SleUser		& RepeatString(Request.SleHT, 4)
													& Request.SBALogText)>
				</cfif>
			</cfcase>
			</cfswitch>
		<cfelse><!--- Default to logging at the debug level: --->
			<cfif Request.SleIsDebugEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
				<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
				<cfset Request.SleLogger.debug	("SleCustom"			& Request.SleHT
												& Request.SleScriptName	& Request.SleHT 
												& Request.SleUser		& RepeatString(Request.SleHT, 4)
												& Request.SBALogText)>
			</cfif>
		</cfif>
	<cfelseif Request.SleIsErrorEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.error	("SleCustom"			& Request.SleHT
										& Request.SleScriptName	& Request.SleHT 
										& Request.SleUser		& RepeatString(Request.SleHT, 4)
										& "(log_SleCustom called without first setting Request.SBALogText)")>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Provides consistent handling of e-tran error messages. 
NOTES:				None. 
INPUT:				Variables.EtranErrMsg. 
OUTPUT:				Variables.ErrMsg and Variables.SaveMe. 
REVISION HISTORY:	08/15/2012, SRS:	Added a master switch, FeemDisabled ("Feem" = "FormatETranErrMsg" namespace prefix), 
										that essentially turns this include file off. Used in act_LoanAppTbl in Servicing 
										when the user specifies <Loan action="locate">. Modernized code with &=, locase. 
					12/15/2000, SRS:	Moved just about every e-tran file to the new etranshared subdirectory. 
										Added this comment header. 
--->

<cfparam name="Variables.FeemDisabled"		default="No">

<cfif NOT Variables.FeemDisabled>
	<cfif IsDefined("Variables.WSAuthorized") AND Variables.WSAuthorized>
		<cfif Find(Variables.MsgDelimiter,Variables.EtranErrMsg) EQ 0 >
			<cfset Variables.EtranErrMsg	&= Variables.MsgDelimiter>
		</cfif>
		<cfif Find("Errors occurred while attempting to validate",Variables.EtranErrMsg) EQ 0 >
			<cfset Variables.ErrMsg			&= Variables.AppMsg & Variables.EtranErrMsg>
		<cfelse>
			<cfset Variables.ErrMsg			&= Variables.AppMsg>
		</cfif>
		<cfset Variables.ErrMsgDelimiter	= Chr(13) & Chr(10)>
		<cfset Variables.ErrMsg				= REReplace(Variables.ErrMsg, Variables.ErrMsgDelimiter, "", "ALL")>
		<cfset Variables.ErrMsg				= REReplace(Variables.ErrMsg, "<li>", "", "ALL")>
		<cfset Variables.ErrMsg				= REReplace(Variables.ErrMsg, "</li>", "", "ALL")>
	<cfelse>
		<cfset Variables.ErrMsg				&= Variables.AppMsg
											& Chr(13) & Chr(10) 
											& Variables.EtranLineIndicator
											& Variables.EtranErrMsg>
	</cfif>
	<cfset Variables.SaveMe					= "No">
</cfif>

<!--- Before FeemDisabled, this was always done, so continue doing it, in case it messes stuff up elsewhere not to: --->
<cfset Variables.AppMsg						= "">

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		SleTimeThisArray maintenance for End-of-Request "TimeThis" logging. 
NOTES:				Called by manual include. 
INPUT:				Request.SBALogSystemName, maybe. If so, Request.SBALogTimeThisName is required. 
OUTPUT:				SleTimeThisArray maintenance only. Unless there's a call error, nothing should be logged. 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfset Variables.SleIncludeFileName			= "SleTimeEndAccum">
	<cfinclude template="log_SleTimeEndShared_DoNotCallDirectly.cfm">
</cfif>


// Begin sbalookandfeelframe.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 07/10/2009
//
// NOTE:				Include this into pages that reside in frames of SBA Look-and-Feel. 
//
// Revision History:	01/26/2010, SRS:	Added document ready for MSIE versions before 8.0. 
//						07/10/2009, SRS:	Original implementation. Cannibalized sbalookandfeel.js. 

// Configuration Parameters:
//
// ((currently none))

// Globals to save on calls to document.getElementById:
//
// ((currently none))

$(document).ready(function()
	{
	if	($.browser.msie && ($.browser.version < 8.0))
		$(".inlineblock,.inlinetable,"
		+ "div.disableddata,div.manddata,div.optdata,div.reqddata,div.viewdata,"
		+ "table.disableddata,table.manddata,table.optdata,table.reqddata,table.viewdata")
			.css({display:"inline"});		// MSIE 6 and 7 don't handle inline-block or inline-table correctly. 
	});

$(window).load(function()				// Do this at window.load time, not at document.ready time. 
	{
	$(".HideUntilAllFullyLoadedBlock")		.removeClass("HideUntilAllFullyLoadedBlock");
	$(".HideUntilAllFullyLoadedInlBlk")		.removeClass("HideUntilAllFullyLoadedInlBlk");
	$(".HideUntilAllFullyLoadedInline")		.removeClass("HideUntilAllFullyLoadedInline");
	$(".HideUntilAllFullyLoadedTblRowGrp")	.removeClass("HideUntilAllFullyLoadedTblRowGrp");
	$(".HideUntilAllFullyLoadedTblRow")		.removeClass("HideUntilAllFullyLoadedTblRow");
	$(".ShowUntilAllFullyLoadedBlock")		.removeClass("ShowUntilAllFullyLoadedBlock");
	$(".ShowUntilAllFullyLoadedInlBlk")		.removeClass("ShowUntilAllFullyLoadedInlBlk");
	$(".ShowUntilAllFullyLoadedInline")		.removeClass("ShowUntilAllFullyLoadedInline");
	$(".ShowUntilAllFullyLoadedTblRowGrp")	.removeClass("ShowUntilAllFullyLoadedTblRowGrp");
	$(".ShowUntilAllFullyLoadedTblRow")		.removeClass("ShowUntilAllFullyLoadedTblRow");
	});

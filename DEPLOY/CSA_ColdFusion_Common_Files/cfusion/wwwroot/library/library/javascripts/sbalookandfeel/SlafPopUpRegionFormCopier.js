
// Begin SlafMenuPopUpFormCopier.js (in case cfincluded)
// Created by Steve Seaquist, 03/13/2007
//
// When you call SlafPopUpRegion("AppInfoAndAppData") or SlafPopUpRegion("Print"), these 2 routines execute in the 
// popped up window (/library/html/printer_friendly.html), to copy current form element contents. Necessary because 
// non-MSIE browsers copy only the HTML, not the current form contents, when you copy the form using innerHTML. 
// (One of the few things MSIE does well, keeping innerHTML current.) Also, no browser, not even MSIE, copies over 
// the contents of rich text editors. 
//
// Revision History:	10/16/2013, SRS:	Added text-align:left to dynamic css of _tbl. (MSIE misbehaved again.) 
//						08/23/2013, SRS:	Made SlafPopUpRegionRichTextCopier operate on gSlafRichTextEditors (instantiated 
//											rich text editors), not SlafGetRichTextEditors() (candidates for becoming RTEs). 
//											In the process, began using logical accessor routine (SbaGetValue), not 
//											physical methods (DOM navigation dependent on the RTE being used). 
//						04/26/2013, SRS:	jQuery removed support for $.browser in jquery-1.9.0, so stopped relying on it 
//											to decide what to copy in an FCKEditor instance. 
//						02/17/2009, SRS:	Per Ron Whalen, ripped out support for ggedit. 
//						01/19/2009, SRS:	Added support for FCKEditor (easier and better rich text editor). 
//						09/11/2008, SRS:	Added support for ggedit (clunky, hard to use rich text editor). 
//						03/13/2007, SRS:	Original implementation. 


var	gSlafPopUpRegionFormCopierDebug			= false;
var	gSlafPopUpRegionRichTextCopierDebug		= false;
var	kBrowserIsMSIE							= ((navigator.appName == "Microsoft Internet Explorer") 
											&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
											) ? true : false;


// ************************************************************************************************

// SlafPopUpRegionFormCopier copies just standard form data, not rich text editor data. SlafPopUpRegionRichTextCopier, 
// below, does rich text editors. 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionFormCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionFormCopier			(pRegion, pDebug)
	{
	if	(kBrowserIsMSIE)					// MSIE copies current form data, so this code is unneeded. 
		return;								// MSIE copies current form data, so this code is unneeded. 
	if	((!document.forms) || (document.forms.length == 0))
		return;								// If there isn't any form data, this code is also unneeded. 
	switch (pRegion)
		{
		case "AppData":
		case "AppInfoAndAppData":
		case "Print":
			break;
		default:
			return;							// Copy form data only for AppData. 
		}
	var	sAppDataDoc							= (opener.top.AppData) ? opener.top.AppData.document : opener.top.document;
	if	((!sAppDataDoc.forms) || (sAppDataDoc.forms.length == 0))
		return;								// If there isn't any form data, this code is also unneeded. 
	for	(var i = 0; i < document.forms.length; i++)
		{
		var	sAppDataForm					= null;
		var	sAppDataName					= null;
		var	sAppDataElts					= null;
		var	sThisPageForm					= document.forms[i];
		var	sThisPageName					= (sThisPageForm.name ? sThisPageForm.name : '');
		var	sThisPageElts					= sThisPageForm.elements;
		var	sFound							= -1;
		for	(j = 0; j < sAppDataDoc.forms.length; j++)
			{
			sAppDataForm					= sAppDataDoc.forms[j];
			sAppDataName					= (sAppDataForm.name ? sAppDataForm.name : '');
			sAppDataElts					= sAppDataForm.elements;
			if	(sAppDataElts.length == sThisPageElts.length)
				{
				// If the number of elements is the same, and the name is the same, we have a solid indication that 
				// it's the same form: 
				if	((sAppDataName.length > 0) && (sAppDataName == sThisPageName))
					{
					sFound					= j;
					if	(pDebug)			alert("Form found on the basis of name.");
					break;
					}
				// If the number of elements is the same, and there isn't any other form that also has that number of 
				// elements, then infer that it's the same form. But if two forms have the same number of elements as 
				// this one, we have no basis to infer which one is the right one: 
				if	(sFound == -1)
					sFound					= j;
				else
					{
					sFound					= -1;
					break;
					}
				}
			}
		if	(sFound >= 0)
			{
			sAppDataForm					= sAppDataDoc.forms[sFound];
			sAppDataElts					= sAppDataForm.elements;
			for	(j = 0; j < sAppDataElts.length; j++)
				{
				var	sAppDataElt				= sAppDataElts	[j];
				var	sThisPageElt			= sThisPageElts	[j];
			//	opener.top.DumpObject(sAppDataElt, "sAppDataElts["+j+"]", 20);
				switch (sAppDataElt.type.toLowerCase())	// Should already be lower case, but better safe than sorry. 
					{
					case "checkbox":
					case "radio":
						sThisPageElt.checked					= sAppDataElt.checked;
						break;
					case "select-one":
						sThisPageElt.selectedIndex				= sAppDataElt.selectedIndex;
						break;
					case "select-multiple":
						for	(var k = 0; k < sAppDataElt.options.length; k++)
							sThisPageElt.options[k].selected	= sAppDataElt.options[k].selected;
						break;
					case "text":
					case "textarea":
						sThisPageElt.value						= sAppDataElt.value;
						break;
					}
				}
			}
		// Keep looping. Copy all forms that we can infer are the same. 
		}
	}


// ************************************************************************************************

// Do not call SlafPopUpRegionRichTextCopier from SlafPopUpRegionFormCopier, above, because SlafPopUpRegionFormCopier 
// sometimes does an "early return". Instead, call SlafPopUpRegionRichTextCopier from /library/html/printer_friendly.html, 
// so that it will always be done. SlafPopUpRegionRichTextCopier was added to support the Rich Text Editor (ggedit.js) 
// and later FCKEditor. 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionRichTextCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionRichTextCopier		(pDebug)
	{
	if	(pDebug)												alert("SlafPopUpRegionRichTextCopier was called.");
	var	sRTEWindow												= null;	// Just defining it in one place, at this point. 
	// Note: SlafGetRichTextEditors is a function defined in jquery.FCKEditors.SBA.js. 
	if		(opener.gSlafRichTextEditors)
		{
		sRTEWindow												= opener;
		if	(pDebug)											alert("Found gSlafRichTextEditors in opener.");
		}
	else if	(opener.top.gSlafRichTextEditors)
		{
		sRTEWindow												= opener.top;
		if	(pDebug)											alert("Found gSlafRichTextEditors in opener.top.");
		}
	else if	((opener.top.AppData) && (opener.top.AppData.gSlafRichTextEditors))
		{
		sRTEWindow												= opener.top.AppData;
		if	(pDebug)											alert("Found gSlafRichTextEditors in opener.top.AppData.");
		}
	if	(sRTEWindow)
		{
		var	sRichTextEditors									= sRTEWindow.gSlafRichTextEditors;
		for	(var i = 0; i < sRichTextEditors.length; i++) 
			{
			if	(pDebug)										alert("Loop index i = "+i+".");
			SlafPseudoSbaSetValue(pDebug, sRichTextEditors[i]);
			}
		}
	else if	(pDebug)											alert("Opener doesn't have any rich text editors.")
	return "SlafPopUpRegionRichTextCopier done.";
	}


// ************************************************************************************************

// When SlafPopUpRegionRichTextCopier copies over textareas, the SbaSetValue property doesn't get copied. We could 
// reinstantiate everything, but that might actually ENCOURAGE folks to forget that this is a popup page. We don't want 
// to seem too similar to the original page. So instead, we navigate the rich text editor's DOM structure and simulate 
// the behavior of SbaSetValue. 

function SlafPseudoSbaSetValue				(pDebug, pOriginalDocumentTextarea)
	{
	var	sTextarea												= pOriginalDocumentTextarea;
	if	(!sTextarea.SbaGetValue)								// Should be defined in all of our RTEs. 
		{
		if	(pDebug)											alert("Unknown Rich Text Editor, ignored.");
		return;
		}
	var	sId														= sTextarea.SbaEditorId;
	var	sType													= sTextarea.SbaEditorType;
	var	sValue													= sTextarea.SbaGetValue();
	if	(pDebug)												alert("Rich text editor with "
																	+ "sId = '"		+ sId		+ "', "
																	+ "sType = '"	+ sType		+ "' and "
																	+ "sValue '"	+ sValue	+ "'.");
	if	(sType == "mce")// Do before "fck" because much more likely to be tinyMCE in the future. 
		{
		if	(pDebug)											alert("tinyMCE.");
		$("#"+sId+"_tbl")
			.empty()
			.html
				(												"<tbody><tr>"
																+ "<td valign='top' height='100%' width='100%'>"
																	+ "<div style='overflow:auto; padding: 2px;'>"
																		+ sValue
																	+ "</div>"
																+ "</td>"
																+ "</tr></tbody>"
				)
			.css(
				{
				"background-color":								"white",
				"border":										"1px solid black",
				"text-align":									"left",
				"visibility":									"visible"
				});
		if	(pDebug)											alert("End SlafPseudoSbaSetValue.");
		return;
		}
	if	(sType == "fck")
		{
		if	(pDebug)											alert("FCKEditor.");
		if	(sTextarea.fck)										// fck is a standard object installed by jquery.FCKEditor. 
			$("textarea#"+sTextarea.fck.InstanceName).each(function()
				{
				this.value										= sValue;
				if	(pDebug)									alert("Set the FCKEditor way.");
				});
		else if	(pDebug)										alert("Although FCKEditor, didn't have the fck property.");
		if	(pDebug)											alert("End SlafPseudoSbaSetValue.");
		return;
		}
	if	(pDebug)												alert("Unknown Rich Text Editor type, ignored.");
	}

// End SlafMenuPopUpFormCopier.js


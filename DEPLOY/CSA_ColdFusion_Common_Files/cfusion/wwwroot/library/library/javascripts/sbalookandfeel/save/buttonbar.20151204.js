
// Begin buttonbar.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 10/16/2015.
//
// This JavaScript should be called ONLY by cf_sbabuttonbar. If any other frame or inline region needs these
// variables or functions, they can always reference them as top.whatever. Therefore, it's okay to force the
// caller to the topmost window frame:
//
// Revision History:	12/04/2015, SMJ:	Added support for CLS System level help menu and moved DoExit() functionality to the Home Icon. 
//						11/09/2015, SRS:	Original implementation. Cannibalized sbalookandfeel.strict.js.

if	(self != top.self)										// Prevents accidentally nested frames.
	top.location.href										= self.location.href;

// Configuration Parameters:


// Globals to save on calls to document.getElementById. Not using "var" to highlight that they're intentionally global:
// Some of these are also defined in sbalookandfeel.strict.js, but no hurry getting rid of them just yet.

gBBMenuSettings												= null;

$(function()
	{
	$("div.buttonbar div.icon").each(function()
		{
		var	s$Menu											= $("div.menu", this);
		var	sMenu											= null;
		if	(s$Menu.length === 0)
			return;// The .each() loop keeps running. A return here is like a continue.
		sMenu												= s$Menu[0];
		this.HideMyMenu										= function()
			{
			sMenu.style.display								= "none";
			};
		this.ShowMyMenu										= function()
			{
			sMenu.style.display								= "";
			};
		$([this, sMenu]).hover(this.ShowMyMenu, this.HideMyMenu);
		});
	});

function BBMenuHideSettings									()
	{
	if	(!gBBMenuSettings)
		{
		var	sIcon											= $("div.buttonbar div.icon.settings");
		if	(sIcon.length > 0)
			gBBMenuSettings									= sIcon[0];
		}
	if	(gBBMenuSettings)
		if	(gBBMenuSettings.HideMyMenu)
			gBBMenuSettings.HideMyMenu();
		else
			console.log("gBBMenuSettings.HideMyMenu not defined.");
	else
		console.log("gBBMenuSettings not defined.");
	}

function BBMenuSelect										(pActionCode)
	{
	// Although this function doesn't use gDiv globals, the file opened by pActionCode "Print" (printer_friendly.html) does.
	// The problem is that printer_friendly.html can't test for the existence of the SBA look-and-feel globals if the user
	// is using the Opera browser for Windows. So it falls to this routine to make sure that the gDiv globals' contents are
	// properly initialized. (Sometimes they won't be if the calling page trashes the onLoad.)
	if	(window.SlafInitGlobals)							// Implies we're in an SBA Look-and-Feel window.
		if	(!gAllGlobalsInitialized)						// This function uses gDiv globals, so make sure their contents are initialized.
			SlafInitGlobals();								// This function uses gDiv globals, so make sure their contents are initialized.
	var	sWindow												= null;
	var	sWindIntName										= "PoputWindowCreatedAtMilliseconds" + (new Date()).getTime();
	switch (pActionCode)
		{
		case "CFDebug":
			if	(window.SlafToggleEnvelope)					// Might not exist on non-SBA-Look-and-Feel pages.
				SlafToggleEnvelope();
			break;
		case "Home":
			if	(top.MainNav && top.MainNav.DoExit)
				top.MainNav.DoExit(top.MainNav.document.FormMainNav);
			else if	(top.DoExit)
				top.DoExit(top.document.FormMainNav);
			// else do nothing because no script available. 
			break;
		case "Help":
			// Intentionally creating a global (if not already defined by SlafInitGlobals), hence the "g" prefix (scope notation).
			gLastPopUp										= window.open("/library/html/sba_look_and_feel_help.html",
															sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
			gLastPopUp.focus();
			break;
		case "HelpClick":
			if	(top.MainNav && top.MainNav.DoHelp)
				top.MainNav.DoHelp(top.MainNav.document.FormMainNav);
			else if	(top.DoHelp)
				top.DoHelp(top.document.FormMainNav);
			// else do nothing because no script available. 
			break;
		case "PrintDirect":
			if	(top.MainNav && top.MainNav.DoPrint)
				top.MainNav.DoPrint(top.MainNav.document.FormMainNav);
			else if	(top.DoPrint)
				top.DoPrint(top.document.FormMainNav);
			// else do nothing because no script available. 
			break;
		case "Print":
			// Intentionally creating a global (if not already defined by SlafInitGlobals), hence the "g" prefix (scope notation).
			gLastPopUp										= window.open("/library/html/printer_friendly.html?CachedAsOf=2013-09-05T19:21",
															sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
			break;
		case "Show/Hide":
			if	(window.SlafToggleAppData)					// Might not exist on non-SBA-Look-and-Feel pages.
				SlafToggleAppData();
			break;
		case "TextOnly":
			alert("Please set Text-Only Black and White Theme instead. That's how it's done now.");
			// The form submission should preclude executing any JavaScript afterwards, but it doesn't hurt:
			break;
		default:
			alert("No action defined for this hotlink yet.");
		}
	BBMenuHideSettings();
	}

// End buttonbar.js

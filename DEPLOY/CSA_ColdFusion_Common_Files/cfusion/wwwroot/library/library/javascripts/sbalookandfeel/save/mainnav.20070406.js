
// Begin mainnav.js
// Created by Steve Seaquist

var	gMainNavGlobalsInitialized			= false;	// ... until MainNavInitGlobals() is called. 
var	gMainNavReadyLight					= null;

function MainNavInitGlobals				()
{
gMainNavReadyLight						= document.getElementById("readylight");
gMainNavGlobalsInitialized				= true;
}

function MainNavDoThisOnLoad			()
{
MainNavInitGlobals();					// Only situation where MainNavInitGlobals is executed unconditionally. 
if	(top.MainNav)						// Only if MainNav is in a frame: 
	gThisPageIsFullyLoaded				= true;
top.gFrameIsFullyLoadedMainNav			= true;
top.SetReadyIfAllFullyLoaded();
}

function SetReadyLightToLoading			()
{
if	(!gMainNavGlobalsInitialized)		// This function uses gMainNav globals, so make sure their contents are initialized. 
	MainNavInitGlobals();				// (Sometimes they won't be if the calling page trashes the onLoad.) 
// Graphic versions of the ReadyLight looked crappy in new look-and-feel. So nowadays we're always using text. 
// Keep the following HTML in sync with the HTML generated for the ReadyLight in cf_mainnav. 
if	(gMainNavReadyLight)				// Is this page using the ReadyLight feature? If so, set "Loading": 
	gMainNavReadyLight.innerHTML		= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
										+ '<tr><td style="background-color:#ffffff;" align="center" valign="middle">'
										+ '<span title="Please wait. Parts of this page are still loading.">Loading</span>'
										+ '</td></tr>'
										+ '</table>';
}

function SetReadyLightToReady			()
{
if	(!gMainNavGlobalsInitialized)		// This function uses gMainNav globals, so make sure their contents are initialized. 
	MainNavInitGlobals();				// (Sometimes they won't be if the calling page trashes the onLoad.) 
// Graphic versions of the ReadyLight looked crappy in new look-and-feel. So nowadays we're always using text. 
if	(gMainNavReadyLight)				// Is this page using the ReadyLight feature? If so, set "Ready": 
	gMainNavReadyLight.innerHTML		= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
										+ '<tr><td style="background-color:#bbe1ea;" align="center" valign="middle">'
										+ '<span style="color:#90d8a9; font-weight:bold;" '
										+ 'title="Ready. Page is fully loaded.">Ready</span>'
										+ '</td></tr>'
										+ '</table>';
}

// End mainnav.js


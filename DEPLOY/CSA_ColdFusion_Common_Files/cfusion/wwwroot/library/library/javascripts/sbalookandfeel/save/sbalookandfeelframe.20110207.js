
// Begin sbalookandfeelframe.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 07/10/2009
//
// NOTE:				Include this into pages that reside in frames of SBA Look-and-Feel. 
//
// Revision History:	12/01/2010, SRS:	Reverse the effects of ShowUntilAllFullyLoaded and HideUntilAllFullyLoaded 
//											when the page is fully loaded. (Not too many using those classes, because this 
//											is actually a MAJOR bug that managed to go unnoticed for quite some time.) 
//											Removed document ready, because that's now handled with conditional CSS. 
//											New function SlafCenterInAppData. See also sba.css (style #CenterThisInAppData). 
//						01/26/2010, SRS:	Added document ready for MSIE versions before 8.0. 
//						07/10/2009, SRS:	Original implementation. Cannibalized sbalookandfeel.js. 

var	gDivCenterThisInAppData					= null;

$(window).load(function()					// Do this at window.load time, not at document.ready time. 
	{
	$(".HideUntilAllFullyLoadedBlock")		.removeClass("HideUntilAllFullyLoadedBlock")	.css({display:"block"});
	$(".HideUntilAllFullyLoadedInline")		.removeClass("HideUntilAllFullyLoadedInline")	.css({display:"inline"});
	// gSlafInlineBlock *SHOULD* have been defined by custom tags or other inclusions, but don't error if it wasn't: 
	var	sInlineBlock						= (gSlafInlineBlock		? gSlafInlineBlock
											: (top.gSlafInlineBlock	? top.gSlafInlineBlock
											: "inline-block"));// Should never get this far. 
	var	sInlineTable						= (gSlafInlineTable		? gSlafInlineTable
											: (top.gSlafInlineTable	? top.gSlafInlineTable
											: "inline-table"));// Should never get this far. 
	$(".HideUntilAllFullyLoadedInlBlk")		.removeClass("HideUntilAllFullyLoadedInlBlk")	.css({display:sInlineBlock});
	$(".HideUntilAllFullyLoadedInlTbl")		.removeClass("HideUntilAllFullyLoadedInlTbl")	.css({display:sInlineTable});
	$(".HideUntilAllFullyLoadedTblRowGrp")	.removeClass("HideUntilAllFullyLoadedTblRowGrp").css({display:"table-row-group"});
	$(".HideUntilAllFullyLoadedTblRow")		.removeClass("HideUntilAllFullyLoadedTblRow")	.css({display:"table-row"});
	$(".ShowUntilAllFullyLoadedBlock")		.removeClass("ShowUntilAllFullyLoadedBlock")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInline")		.removeClass("ShowUntilAllFullyLoadedInline")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInlBlk")		.removeClass("ShowUntilAllFullyLoadedInlBlk")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInlTbl")		.removeClass("ShowUntilAllFullyLoadedInlTbl")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedTblRowGrp")	.removeClass("ShowUntilAllFullyLoadedTblRowGrp").addClass("hide");
	$(".ShowUntilAllFullyLoadedTblRow")		.removeClass("ShowUntilAllFullyLoadedTblRow")	.addClass("hide");
	});

function SlafCenterInAppData				()
	{
	if	(top.AppData)// Make sure this routine is used only when AppData is in a frame. Otherwise, this routine does nothing. 
		{
		var	sDiv							= $(gDivCenterThisInAppData);
		var	sWin							= $(window);// sbalookandfeel.js uses $(gDivAppData). 
		var	sDivHeight						= sDiv.height();
		var	sDivWidth						= sDiv.width();
		var	sWinHeight						= sWin.height();
		var	sWinWidth						= sWin.width();
		var	sLeft							= Math.floor((sWinWidth		- sDivWidth)	/ 2);
		var	sTop							= Math.floor((sWinHeight	- sDivHeight)	/ 2);
		sDiv.css({left:sLeft,top:sTop});
		}
	}

function SlafDoThisOnLoad					()
	{
	gDivCenterThisInAppData					= document.getElementById("CenterThisInAppData");// Probably null;
	SlafDoThisOnResize();
	}

function SlafDoThisOnResize					()
	{
	if	(gDivCenterThisInAppData)
		SlafCenterInAppData();
	}

$(window).load(SlafDoThisOnLoad);
$(window).resize(SlafDoThisOnResize);


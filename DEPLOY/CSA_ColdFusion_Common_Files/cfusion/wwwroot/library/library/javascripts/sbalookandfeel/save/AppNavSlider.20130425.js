
// Begin AppNavSlider.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 04/25/2013
// Notes:
//		If we try to track the mouse (using SliderMouseMove), the underlying frames that may exist make it too painfully 
//		slow for the user. So instead, we animate a doubling or halving of the width of AppNav. 
//
// Revision History:	04/25/2013, SRS:	Original implementation. Recovered from sbalookandfeel.20060811.cfm backup in 
//											/library/customtags/save, the last version before the inline JS was removed. 
//											Adapted code not to use ColdFusion variables, so it'll be a usable *.js file. 

// Configuration Parameters:

kAppNavOrigWidth							= 196;
kAppNavExpandedWidth						= kAppNavOrigWidth * 2;	// Arbitrary. 
kSliderControlLeftOffset					= 2;					// Arbitrary, keep in sync with width. 
kSliderControlOrigCenter					= 197;					// Always between AppNav width and AppData left.
kSliderControlWidth							= "5px; ";				// Arbitrary, but should be odd number. 
kTooltipContract							= "Click to contract.";
kTooltipExpand								= "Click to expand.";

// Globals:

gAnimationMilliseconds						= 1000;
g$AppData									= null;
g$AppNav									= null;
g$Doc										= null;
gEnvelopeOffset								= null;
gSliderControl								= null;
g$SliderControl								= null;
gSliderIsActive								= false;

function ActivateSlider						()
	{
	g$Doc			.on("mousemove",		SliderMouseMove);
	gSliderIsActive							= true;
	}

function DeactivateSlider					()
	{
	g$Doc			.off("mousemove",		SliderMouseMove);
	gSliderIsActive							= false;
	}

function SliderClick						(pEvent)
	{
	if	(g$AppNav.width() == kAppNavOrigWidth)
		{
		g$AppData.animate({left:			(kAppNavExpandedWidth + 2)	+ "px"}, gAnimationMilliseconds);
		g$AppNav.animate({width:			kAppNavExpandedWidth		+ "px"}, gAnimationMilliseconds);
		g$SliderControl.css({left:			((kAppNavExpandedWidth + 1) - kSliderControlLeftOffset)	+ "px"})
						.each(function()	{this.title = kTooltipContract});
		}
	else
		{
		g$AppData.animate({left:			(kAppNavOrigWidth + 2)		+ "px"}, gAnimationMilliseconds);
		g$AppNav.animate({width:			kAppNavOrigWidth			+ "px"}, gAnimationMilliseconds);
		g$SliderControl.css({left:			(kSliderControlOrigCenter - kSliderControlLeftOffset)	+ "px"})
						.each(function()	{this.title = kTooltipExpand});
		}
// Slider code (where user drags the mouse) is disabled by the following comments. Uncomment if you want to try to make 
// it acceptably fast, but I doubt that it can be. 
//
//	if	(gSliderIsActive)
//		DeactivateSlider();
//	else
//		ActivateSlider();
	}

function SliderMouseEnter					(pEvent)
	{
	if	(g$AppNav.width() == kAppNavOrigWidth)
		gSliderControl.style.cursor			= "e-resize";	// "east" = points to the right. Not much browser support. 
	else
		gSliderControl.style.cursor			= "w-resize";	// "west" = points to the left. Not much browser support. 
// Slider code (where user drags the mouse) is disabled by the following comments. Uncomment if you want to try to make 
// it acceptably fast, but I doubt that it can be. 
//
//	gSliderControl.style.cursor				= "col-resize";	// Both "east" and "west" result in "col-resize", usually. 
	}

function SliderMouseLeave					(pEvent)
	{
	gSliderControl.style.cursor				= "auto";
	}

function SliderMouseMove					(pEvent)
	{
	var	sNewCenter							= pEvent.pageX - gEnvelopeOffset.left;
	if	(sNewCenter < kSliderControlOrigCenter)
		{
		DeactivateSlider();
		return;
		}
	if	(sNewCenter > (kSliderControlOrigCenter * 2))	// Arbitrary cutoff. 
		{
		DeactivateSlider();
		return;
		}
	gSliderControl.style.left				= ((sNewCenter - kSliderControlLeftOffset)	+ "px");
	gDivSBALogo.style.width					= 
	gDivAppNav.style.width					= ((sNewCenter - 1)							+ "px");
	gDivMainNav.style.left					= 
	gDivAppData.style.left					= ((sNewCenter + 1)							+ "px");
	}

$(function()
	{
	var	s$Envelope, s$SliderControl;
	// Don't depend on the $(document).ready() in sbalookandfeel.strict.js to have already been done: 
	g$AppData								= $("#DivAppData");
	g$AppNav								= $("#DivAppNav");
	g$Doc									= $(document);
	s$Envelope								= $("#DivEnvelope");
	gEnvelopeOffset							= s$Envelope.offset();
	s$Envelope
		.append("<div id='SliderControl' style='"
				+"position:"				+"absolute; "
				+"cursor:"					+"auto; "
				+"top:"						+"105px; "
				+"width:"					+kSliderControlWidth
				+"bottom:"					+"25px; "
				+"left:"					+(kSliderControlOrigCenter - kSliderControlLeftOffset)+"px; "
				+"z-index:"					+"3;"
				+"' title='"+kTooltipExpand+"'></div>");	
// Slider code (where user drags the mouse) is disabled by the following comments. Uncomment if you want to try to make 
// it acceptably fast, but I doubt that it can be. 
//
//				+"' title='First click begins moving boundary, second click stops it.'></div>");
	gSliderControl							= document.getElementById("SliderControl");
	g$SliderControl							= $(gSliderControl);
	g$SliderControl	.on("click",			SliderClick);
	g$SliderControl	.on("mouseenter",		SliderMouseEnter);
	g$SliderControl	.on("mouseleave",		SliderMouseLeave);
	});

// End AppNavSlider.js



// Begin sbalookandfeel.js (in case cfincluded)
// Created by Steve Seaquist, 11/16/2006
//
// Revision History:	02/23/2007, SRS:	Added SlafMenu code, which is currently just a prototype to try it out as 
//											an interface option. Per Ron Whalen, modified SlafPopUpRegion to accept 
//											AppInfoAndAppData (or logical synonym Print), for use in DoPrint scripts. 
//											Also per Ron, made BotMost participate in SlafToggleAppData. 
//						11/16/2006, SRS:	Original implementation, as part of CSS-P version of SBA look-and-feel. 
//
// This JavaScript should be called ONLY by cf_sbalookandfeel. If any other frame or inline region needs these 
// variables or functions, they can always reference them as top.whatever. Therefore, it's okay to force the 
// caller to the topmost window frame: 

if	(self != top.self)			// Prevents accidentally nested frames. 
	top.location.href			= self.location.href;

// Configuration Parameters:

var	kBrowserIsMSIE						= ((navigator.appName == "Microsoft Internet Explorer") 
									//	&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
									//	&& (navigator.platform == "Win32")				// Mac has same broken box model. 
										) ? true : false;
var	kEnvelopeBorder						= 1;
var	kEnvelopeCellSpacing				= 2;
var	kEnvelopeMargin						= 10;
var	kFudgeFactorMSIE					= kBrowserIsMSIE ? 2 : 0;	// Required in backward-compatibility mode (broken box). 
var	kHeightAppDataMin					= 100;
var	kHeightAppName						= 24;
var	kHeightAzureGrowthGraph				= 101;
var	kHeightBotMost						= 24;
var	kHeightSBALogo						= 77;
var	kHeightTop4Regions					= kHeightSBALogo		+ kEnvelopeCellSpacing + kHeightAppName;
var	kHeightTopOfAppData					= kHeightTop4Regions	+ kEnvelopeCellSpacing;
var	kHeightWindowMin					= kHeightTopOfAppData
										+ kHeightAppDataMin
										+ kHeightBotMost
										+ (2 * kEnvelopeBorder)
										+ (2 * kEnvelopeMargin);
var	kHeightWindowDefault				= 607;	// based on MSIE for Windows and 1024x768 resolution
var	kWidthAzureGrowthGraph				= 434;
var	kWidthSBALogo						= 196;
var	kWidthLeftOfAppData					= kWidthSBALogo			+ kEnvelopeCellSpacing;
var	kWidthWindowMin						= kWidthSBALogo
										+ kEnvelopeCellSpacing
										+ kWidthAzureGrowthGraph
										+ (2 * kEnvelopeBorder)
										+ (2 * kEnvelopeMargin);
var	kWidthWindowDefault					= 984;	// based on MSIE for Windows and 1024x768 resolution

// Globals to save on calls to document.getElementById:

var	gDivAppData							= null;
var	gDivAppInfo							= null;
var	gDivAppName							= null;
var	gDivAppNav							= null;
var	gDivBotMost							= null;
var	gDivEnvelope						= null;
var	gDivMainNav							= null;
var	gDivMarginT							= null;
var	gDivMarginL							= null;
var	gDivMarginB							= null;
var	gDivMarginR							= null;
var	gDivSBALogo							= null;
var	gDivSlafMenu						= null;
var	gDivWindow							= null;
var	gDspAppInfo							= "";	// Optional region. This will be "" or "none". 
var	gDspAppNav							= "";	// Optional region. This will be "" or "none". 
var	gFrameIsFullyLoadedAppData			= false;
var	gFrameIsFullyLoadedAppInfo			= true; // Per Ron Whalen, no longer affects ReadyLight. 
var	gFrameIsFullyLoadedAppNav			= false;
var	gFrameIsFullyLoadedMainNav			= false;
var	gFrmAppData							= null;	// gFrm vars not used, provided for backwards compatability. 
var	gFrmAppHidden						= null;	// gFrm vars not used, provided for backwards compatability. 
var	gFrmAppInfo							= null;	// gFrm vars not used, provided for backwards compatability. 
var	gFrmAppNav							= null;	// gFrm vars not used, provided for backwards compatability. 
var	gFrmMainNav							= null;	// gFrm vars not used, provided for backwards compatability. 
var	gLastPopUp							= null;	// Set by most recent call to SlafPopUpRegion. 
var	gPrevHeight							= 0;
var	gPrevWidth							= 0;
// Initialize all gRefs to top.self for now. That's because this script is included in the head, before the 
// screen regions have been defined. They won't be defined for-sure until SlafDoThisOnLoad, which sets their 
// true values. Note that, unlike gFrm vars, these are always usable, even when the region is not a frame: 
var	gRefAppData							= top.self;
var	gRefAppHidden						= top.self;
var	gRefAppInfo							= top.self;
var	gRefAppNav							= top.self;
var	gRefMainNav							= top.self;
var	gThisPageIsFullyLoaded				= false;

function DoThisOnLoad					()	// DoThisOnLoad not used, provided for backwards compatability. 
{
SlafDoThisOnLoad();
}

function SlafDoThisOnLoad				()
{
					gDivAppData			= document.getElementById("DivAppData");
					gDivAppInfo			= document.getElementById("DivAppInfo");
					gDivAppName			= document.getElementById("DivAppName");
					gDivAppNav			= document.getElementById("DivAppNav");
					gDivBotMost			= document.getElementById("DivBotMost");
					gDivEnvelope		= document.getElementById("DivEnvelope");
					gDivMainNav			= document.getElementById("DivMainNav");
					gDivMarginT			= document.getElementById("DivMarginT");
					gDivMarginL			= document.getElementById("DivMarginL");
					gDivMarginB			= document.getElementById("DivMarginB");
					gDivMarginR			= document.getElementById("DivMarginR");
					gDivSBALogo			= document.getElementById("DivSBALogo");
					gDivSlafMenu		= document.getElementById("DivSlafMenu");
					gDivWindow			= document.getElementById("DivWindow");
					gDspAppInfo			= gDivAppInfo.style.display;
					gDspAppNav			= gDivAppNav.style.display;
if	(top.AppData)	gFrmAppData			= gRefAppData	= top.AppData;	else gFrameIsFullyLoadedAppData	= true;
					gFrmAppHidden		= gRefAppHidden	= top.AppHidden;	// always a frame
if	(top.AppInfo)	gFrmAppInfo			= gRefAppInfo	= top.AppInfo;	else gFrameIsFullyLoadedAppInfo	= true;
if	(top.AppNav)	gFrmAppNav			= gRefAppNav	= top.AppNav;	else gFrameIsFullyLoadedAppNav	= true;
if	(top.MainNav)	gFrmMainNav			= gRefMainNav	= top.MainNav;	else gFrameIsFullyLoadedMainNav	= true;
gThisPageIsFullyLoaded					= true;

if	(window.location.href.indexOf("?Color") > -1)	// Hidden debug feature. 
	{
	gDivMarginT.style.backgroundColor	= "yellow";
	gDivMarginR.style.backgroundColor	= "orange";
	gDivMarginB.style.backgroundColor	= "red";
	gDivMarginL.style.backgroundColor	= "green";
	gDivAppData.style.backgroundColor	= "#ffccff";
	}

if		(self.innerHeight)
	{	// (all except Explorer)
	gPrevHeight							= self.innerHeight;
	gPrevWidth							= self.innerWidth;
	}
else if	(document.documentElement && document.documentElement.clientHeight)
	{	// (Explorer 6 in Strict Mode)
	gPrevHeight							= document.documentElement.clientHeight;
	gPrevWidth							= document.documentElement.clientWidth;
	}
else if	(document.body)
	{	// (other Explorers)
	gPrevHeight							= document.body.clientHeight;
	gPrevWidth							= document.body.clientWidth;
	}
else
	{	// (just in case some funky, non-compliant browser comes along)
	gPrevHeight							= kHeightWindowDefault;
	gPrevWidth							= kWidthWindowDefault;
	}
//	DumpObject(navigator, "navigator");
SlafDoThisOnResize();
SetReadyIfAllFullyLoaded();
}

function SlafDoThisOnResize				(pDebug)
{
var	sDebug								= (SlafDoThisOnResize.arguments.length > 0) ? pDebug : false;
if	(location.href.indexOf("DebugResize") >= 0) sDebug	= true;
if	(sDebug)							alert("Entering SlafDoThisOnResize.");
var	sHeightWindow						= "";
var	sWidthWindow						= "";
// Haven't yet figured out how to calculate AppData's scrolled content height, so that overflow:visible can 
// push down BotMost. But save the code, if only to serve as memory of what DOESN'T work: 
//if	(gDivAppData.style.overflow == "visible")
//	{
//	if	(sDebug) alert("Getting scrolled height and width, which is never less than that of the window.")
//	if	(document.body.scrollHeight > document.body.offsetHeight)// All browsers support both. Use the larger. 
//		{	// all but Explorer Mac
//		sHeightWindow					= document.body.scrollHeight;
//		sWidthWindow					= document.body.scrollWidth;
//		}
//	else
//		{	// Explorer Mac, but would also work in Explorer 6 Strict, Mozilla and Safari
//		sHeightWindow					= document.body.offsetHeight;
//		sWidthWindow					= document.body.offsetWidth;
//		}
//	}
//else
//	{
	if	(sDebug) alert("Getting window height and width.")
	if		(self.innerHeight)
		{	// (all except Explorer)
		sHeightWindow					= self.innerHeight;
		sWidthWindow					= self.innerWidth;
		}
	else if	(document.documentElement && document.documentElement.clientHeight)
		{	// (Explorer 6 in Strict Mode)
		sHeightWindow					= document.documentElement.clientHeight;
		sWidthWindow					= document.documentElement.clientWidth;
		}
	else if	(document.body)
		{	// (other Explorers)
		sHeightWindow					= document.body.clientHeight;
		sWidthWindow					= document.body.clientWidth;
		}
	else
		{	// (just in case some funky, non-compliant browser comes along)
		sHeightWindow					= kHeightWindowDefault;
		sWidthWindow					= kWidthWindowDefault;
		}
//	}

if	(sDebug) alert("Raw: sHeightWindow = "+sHeightWindow+", sWidthWindow = "+sWidthWindow+".");

var	sExpandingHeight					= (sHeightWindow	> gPrevHeight)	? true : false;
var	sExpandingWidth						= (sWidthWindow		> gPrevWidth)	? true : false;
gPrevHeight								= sHeightWindow;
gPrevWidth								= sWidthWindow;

//	If the user sizes the window too small, to heck with trying to prevent scrollbars: 
if	(sHeightWindow						< kHeightWindowMin)
	sHeightWindow						= kHeightWindowMin;
if	(sWidthWindow						< kWidthWindowMin)
	sWidthWindow						= kWidthWindowMin;
if	(sDebug) alert("Adjusted: sHeightWindow = "+sHeightWindow+", sWidthWindow = "+sWidthWindow+".");

//	Adjust for DivMargin divs:
var	sHeightEnvelope						= sHeightWindow		- (2 * kEnvelopeMargin);
var	sWidthEnvelope						= sWidthWindow		- (2 * kEnvelopeMargin);
if	(sDebug) alert("Envelope: sHeightEnvelope = "+sHeightEnvelope+", sWidthEnvelope = "+sWidthEnvelope+".");

//	Adjust for DivEnvelope's 1 pixel border (on both sides): 
var	sHeightContent						= sHeightEnvelope	- (2 * kEnvelopeBorder);
var	sWidthContent						= sWidthEnvelope	- (2 * kEnvelopeBorder);
if	(sDebug) alert("Content: sHeightContent = "+sHeightContent+", sWidthContent = "+sWidthContent+".");

gDivMarginL.style.height				=
gDivMarginR.style.height				= sHeightEnvelope						- kFudgeFactorMSIE	+ "px";
gDivMarginB.style.top					= sHeightEnvelope	+ kEnvelopeMargin	- kFudgeFactorMSIE	+ "px";
gDivMarginT.style.width					=
gDivMarginB.style.width					= sWidthEnvelope	+ (2 * kEnvelopeMargin)					+ "px";
gDivMarginR.style.left					= sWidthEnvelope	+ kEnvelopeMargin	- kFudgeFactorMSIE	+ "px";
if	(sDebug) alert("Margins done.");

if	(sExpandingHeight)
	gDivEnvelope.style.height			= sHeightContent											+ "px";
if	(sExpandingWidth)
	gDivEnvelope.style.width			= sWidthContent												+ "px";
if	(sDebug) alert("Envelope expansions done.");

sHeightContent							-= kFudgeFactorMSIE;
sWidthContent							-= kFudgeFactorMSIE;
if	(kBrowserIsMSIE && sDebug) alert("Adjusted contents for standards non-compliant browser: "
									+ "sHeightContent = "+sHeightContent+", sWidthContent = "+sWidthContent+".");

if	(gDivSBALogo.style.display == "none")
	{
	with (gDivAppData.style)
		{
		height							= sHeightContent;
		width							= sWidthContent;
		}
	}
else
	{
	var	sAppDataHeight					= sHeightContent	- (kHeightTopOfAppData	+ kHeightBotMost);
	var	sAppDataWidthUsually			= sWidthContent		- (kWidthSBALogo		+ kEnvelopeCellSpacing);
	var	sAppDataWidth					= (gDspAppNav == "none") ? sWidthContent : sAppDataWidthUsually;
	if	(sDebug) alert("AppData: sAppDataHeight = "+sAppDataHeight+", sAppDataWidthUsually = "+sAppDataWidthUsually
					+ ", sAppDataWidth = "+sAppDataWidth+".");
	gDivAppData.style.width				= sAppDataWidth												+ "px";
	gDivAppInfo.style.width				=
	gDivMainNav.style.width				= sAppDataWidthUsually										+ "px";
	gDivBotMost.style.width				= sWidthContent												+ "px";
	gDivAppData.style.height			=
	gDivAppNav.style.height				= sAppDataHeight											+ "px";
	gDivBotMost.style.top				= sAppDataHeight	+ kHeightTopOfAppData					+ "px";
	if	(sDebug) alert("Inner regions done.");
	}

sHeightContent							+= kFudgeFactorMSIE;
sWidthContent							+= kFudgeFactorMSIE;
if	(kBrowserIsMSIE && sDebug) alert("Reverted contents for standards non-compliant browser: "
									+ "sHeightContent = "+sHeightContent+", sWidthContent = "+sWidthContent+".");

if	(!sExpandingHeight)
	gDivEnvelope.style.height			= sHeightContent							+ "px";
if	(!sExpandingWidth)
	gDivEnvelope.style.width			= sWidthContent								+ "px";
if	(sDebug)							alert("Envelope contractions done.");
return;
}

function SlafMenuHide					()
{
gDivSlafMenu.style.display				= "none";
}

function SlafMenuSelect					(pIndex)
{
var	sWindow								= null;
switch (pIndex)
	{
	case "Help":
		sWindow							= window.open("/library/html/sba_look_and_feel_help.html",
										"newWin", "menubar,resizable,scrollbars,titlebar,toolbar");
		sWindow.focus();
		break;
	case "Print":
		SlafPopUpRegion("Print");
		gLastPopUp.print();
		break;
	case "Show/Hide":
		SlafToggleAppData();
		break;
	default:
		alert("No action defined for this hotlink yet.");
	}
SlafMenuHide();
}

function SlafMenuShow					()
{
gDivSlafMenu.style.display				= "block";
}

function SlafPopUpRegion				(pRegion)	// pRegion is a string: "AppData", "AppNav", etc.
{
var	sWindow								= null;
switch (pRegion)
	{
	case "AppData":
	case "AppHidden":
	case "AppInfo":
	case "AppInfoAndAppData":
	case "AppName":
	case "AppNav":
	case "BotMost":
	case "MainNav":
	case "Print":						// logical synonym for AppInfoAndAppData
	case "SBALogo":
		break;
	default:
		alert("SlafPopUpRegion called with unknown region \"" + pRegion + "\".\n\n"
			+ "Allowable region names are \"AppData\", \"AppHidden\", \"AppInfo\", \"AppInfoAndAppData\", "
			+ "\"AppName\", \"AppNav\", \"BotMost\", \"MainNav\", \"Print\" or \"SBALogo\".");
		return;
	}
if	(eval("top."+pRegion))				// That is, if it's a frame:
	sWindow								= window.open(eval("top."+pRegion+".location.href"),
										"newWin", "menubar,resizable,scrollbars,titlebar,toolbar");
else									// That is, if it's inline or a non-region name that's allowed:
	{
	sWindow								= window.open("",
										"newWin", "menubar,resizable,scrollbars,titlebar,toolbar");
	if	(sWindow != null)
		{
		sWindow.document.writeln('<html lang="en-US"><head><title>'+pRegion+'</title>');
		sWindow.document.writeln('<link href="/library/css/sba.css" rel="stylesheet" type="text/css">');
		sWindow.document.writeln('<script src="/library/javascripts/sbalookandfeel/SlafPopUpRegionFormCopier.js"></script>');
		sWindow.document.writeln('</head>');
		sWindow.document.write('<body onload="SlafPopUpRegionFormCopier(\'' + pRegion + '\');" ');
		switch (pRegion)
			{
			case "AppInfo":	sWindow.document.writeln('class="inthead">');					break;
			case "AppName":	sWindow.document.writeln('style="background-color:#1f4f73;">');	break;
			case "MainNav":	sWindow.document.writeln('class="headernav">');					break;
			default:		sWindow.document.writeln('class="normal">');
			}
		switch (pRegion)
			{
			case "AppInfoAndAppData":
			case "Print":
				sWindow.document.writeln('<table><tr><td><div id="AppInfo" style="position:relative;">');
				if	(top.AppInfo)
					sWindow.document.writeln("\t" + top.AppInfo.document.body.innerHTML);
				else
					sWindow.document.writeln("\t" + gDivAppInfo.innerHTML);
				sWindow.document.writeln('</div></td></tr>');
				sWindow.document.writeln('<tr><td><div id="AppData" style="position:relative;">');
				if	(top.AppData)
					sWindow.document.writeln('\t' + top.AppData.document.body.innerHTML);
				else
					sWindow.document.writeln('\t' + gDivAppData.innerHTML);
				sWindow.document.writeln('</div></td></tr></table>');
				break;
			default:
				if	(eval('top.' + pRegion))
					sWindow.document.writeln(eval('top.'+pRegion).document.body.innerHTML);
				else
					sWindow.document.writeln(eval('gDiv'+pRegion).innerHTML);
			}
		sWindow.document.writeln('</body>');
		sWindow.document.writeln('</html>');
		sWindow.document.close();
		}
	}
gLastPopUp								= sWindow;	// Allows caller to manipulate popup. 
}

function SetReadyIfAllFullyLoaded		()
{
if	(top.gFrameIsFullyLoadedAppData
&&	top.gFrameIsFullyLoadedAppNav
&&	top.gFrameIsFullyLoadedMainNav)
	if	(top.MainNav)
		top.MainNav.SetReadyLightToReady();
	else
		top.SetReadyLightToReady();
}

function SlafToggleAppData				()
{
if	(gDivMarginT.title.substring(0,3) == "Max")
	{
	gDivMarginT.title					=
	gDivMarginL.title					=
	gDivMarginB.title					=
	gDivMarginR.title					= "Min" + gDivMarginT.title.substring(3,gDivMarginT.title.length);
	gDivSBALogo.style.display			=
	gDivMainNav.style.display			=
	gDivAppName.style.display			=
	gDivAppInfo.style.display			=
	gDivAppNav.style.display			=
	gDivBotMost.style.display			= "none";
	with (gDivAppData.style)
		{
		top								= "0px";
		left							= "0px";
		height							= (parseInt(height,10) + kHeightTopOfAppData + kHeightBotMost)	+ "px";
		width							= (parseInt(width, 10)
										+ ((gDspAppNav == "none")? 0 : kWidthSBALogo))					+ "px";
		}
	}
else
	{
	gDivMarginT.title					=
	gDivMarginL.title					=
	gDivMarginB.title					=
	gDivMarginR.title					= "Max" + gDivMarginT.title.substring(3,gDivMarginT.title.length);
	gDivSBALogo.style.display			=
	gDivMainNav.style.display			=
	gDivAppName.style.display			=
	gDivBotMost.style.display			= "";
	gDivAppInfo.style.display			= gDspAppInfo;
	gDivAppNav.style.display			= gDspAppNav;
	with (gDivAppData.style)
		{
		top								= kHeightTopOfAppData											+ "px";
		left							= ((gDspAppNav == "none") ? 0 : kWidthLeftOfAppData)			+ "px";
		height							= (parseInt(height,10) - kHeightTopOfAppData - kHeightBotMost)	+ "px";
		width							= (parseInt(width, 10)
										- ((gDspAppNav == "none")? 0 : kWidthSBALogo))					+ "px";
		}
	}
}

function SlafToggleTextOnly				()
{
if	(top.document.SlafToggleTextOnlyForm)
	with (top.document.SlafToggleTextOnlyForm)
		{
		JavaScriptOn.value				= "Yes";
		submit();
		return;
		}
// If the form doesn't exist, to the same thing using the URL. The following will probably never be done: 
var	sHRef								= "";
var	sSearch								= top.location.search;
if	(sSearch.length > 1)
	{
	var	sString							= unescape(sSearch);
	var	sArray							= sString.substring(1,sString.length).split("&");		// Strip initial "?"
	var	sAlreadyHasToggleTextOnly		= false;
	for	(var i = 0; i < sArray.length; i++)
		{
		var	sElt						= sArray[i];
		if	((sElt.length > 18) && (sElt.substring(0,18) == "SlafToggleTextOnly"))
			{
			sAlreadyHasToggleTextOnly	= true;
			break;
			}
		}
	if	(!sAlreadyHasToggleTextOnly)
		sSearch							+= "&SlafToggleTextOnly=Yes";
	}
else
	sSearch								= "?SlafToggleTextOnly=Yes";

with (top.location)
	{
	sHRef								= protocol;
	sHRef								+= "//";
	sHRef								+= host;
	sHRef								+= pathname;
	sHRef								+= sSearch;
	if	(hash.length > 0)
		sHRef							+= hash;
	}
top.location.href						= sHRef;
}

function AutoResize						(pToggleTextOnly)		// for compatability with old look-and-feel
{
if	(pToggleTextOnly)
	SlafToggleTextOnly					();
}

// End sbalookandfeel.js


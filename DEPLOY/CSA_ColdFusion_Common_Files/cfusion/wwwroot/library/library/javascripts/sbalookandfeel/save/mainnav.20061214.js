
// Begin mainnav.js
// Created by Steve Seaquist

var	gRefReadyLight						= null;

function MainNavDoThisOnLoad			()
{
gRefReadyLight							= document.getElementById("readylight");
if	(top.MainNav)						// Only if MainNav is in a frame: 
	gThisPageIsFullyLoaded				= true;
top.gFrameIsFullyLoadedMainNav			= true;
top.SetReadyIfAllFullyLoaded();
}

function SetReadyLightToLoading			()
{// Graphics look crappy in new look-and-feel. Now always using text for ReadyLight. Keep in sync with cf_mainnav: 
if	(gRefReadyLight != null)
	gRefReadyLight.innerHTML	= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td style="background-color:#ffffff;" align="center" valign="middle">'
								+ '<span title="Please wait. Parts of this page are still loading.">Loading</span>'
								+ '</td></tr>'
								+ '</table>';
}

function SetReadyLightToReady			()
{// Graphics look crappy in new look-and-feel. Now always using text for ReadyLight: 
if	(gRefReadyLight != null)
	gRefReadyLight.innerHTML	= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td style="background-color:#bbe1ea;" align="center" valign="middle">'
								+ '<span style="color:#90d8a9; font-weight:bold;" '
								+ 'title="Ready. Page is fully loaded.">Ready</span>'
								+ '</td></tr>'
								+ '</table>';
}

// End mainnav.js


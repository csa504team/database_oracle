<cfif IsDefined("Request.RequestStartTickCount")><!--- Don't crash. --->
	<cfset Variables.RequestTotalTickCount		= GetTickCount() - Request.RequestStartTickCount>
	<cfset Variables.ApplicationName			= "None">
	<cfset Variables.RequestLoggingIsOn			= "No">
	<cflock name="APPLICATION" type="READONLY" timeout="30">
		<!---
		Note: Do ***NOT*** use scope="APPLICATION", in case there wasn't any CFAPPLICATION tag. Using name="APPLICATION" is 
		essentially server-wide, but limited to this context (logging for the current application, if any). The utility that 
		sets Application.RequestLoggingIsOn should also lock name="APPLICATION", but with type="EXCLUSIVE". 
		--->
		<cfif IsDefined("Application.ApplicationName")>
			<cfset Variables.ApplicationName	= Application.ApplicationName>
		</cfif>
		<cfif IsDefined("Application.RequestLoggingIsOn")>
			<cfset Variables.RequestLoggingIsOn	= Application.RequestLoggingIsOn>
		</cfif>
	</cflock>
	<cfif Variables.RequestLoggingIsOn>
		<!---
		Add logging here. Do as much as possible outside of cflock. Then, just before appending to the log file, do a 
		<cflock name="#Variables.ApplicationName#" type="EXCLUSIVE" timeout="30">. 
		--->
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/06/2008
DESCRIPTION:		Shared routine to copy Variables.GLS into Session.GLS and assorted other Session variable names. 
NOTES:				New apps should only be using the GLS structure and its substructures, not those assorted other 
					Session variables. At some point, we would like to have only Session.GLS in the Session scope, 
					but we can't do that as long as people keep assuming that its substructures are also defined. 
INPUT:				Variables.GLS. 
OUTPUT:				Session.GLS and other Session variables (for now). 
REVISION HISTORY:	05/27/2014, SRS:	Changes to mirror Session.GLS in Session.CLS until such time as CLS is viable. 
					07/02/2008, SRS:	Changes to support saving memory. 
					03/06/2008, SRS:	Original Implementation. 
--->

<!---
This routine will be called ONLY in the context of a Web page, where we have Session variables. It will NEVER be called 
in the context of a Web Service. Therefore, we can call TrimArrayUserRoles("No") to build Session.ArrayUserRoles too: 
--->
<cfif NOT IsDefined("TrimArrayUserRoles")>
	<cfinclude template="/library/udf/bld_SecurityUDFs.cfm">
</cfif>
<cfset TrimArrayUserRoles("No")>
<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfset Session.ArrayUserRoles					= Variables.ArrayUserRoles>
	<cfset Session.GLS								= Duplicate(Variables.GLS)>
	<!--- The following loops are temporary, until we can all stop referencing GLS data outside of Session.GLS: --->
	<cfloop index="Level1Key"						list="#StructKeyList(Variables.GLS)#">
		<cfif IsStruct(Variables.GLS[Level1Key])>
			<cfset Session[Level1Key]				= Duplicate(Variables.GLS[Level1Key])>
			<cfloop index="Level2Key"				list="#StructKeyList(Variables.GLS[Level1Key])#">
				<cfset Session[Level2Key]			= Variables.GLS[Level1Key][Level2Key]>
			</cfloop>
		<cfelse>
			<cfswitch expression="#UCase(Level1Key)#">
			<cfcase value="ARRAYUSERROLES,ARRAYALLUSERROLES">
				<!---
				Session.ArrayUserRoles has already been defined, above. And we will henceforth keep ArrayAllUserRoles 
				only in Session.GLS.ArrayAllUserRoles. (BIG waste of memory to save it anywhere else.) 
				--->
			</cfcase>
			<cfdefaultcase>
				<cfset Session[Level1Key]			= Variables.GLS[Level1Key]>
			</cfdefaultcase>
			</cfswitch>
		</cfif>
	</cfloop>
</cflock>

<!---
Mirror GLS in CLS if the environment has them on the same machine. That would be in Test and Prod initially. Hence, the 
cfif says 'is not "Dev"'. Later, when catweb exists (Distributed GLS passoff), change that cfif to say 'is "Prod"'. Finally, 
when all 3 environments are distributed, remove all of the following mirroring code: 
--->
<cfif NOT IsDefined("Request.SlafDevTestProd")><!--- Probably always will be. Just being extra careful. --->
	<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
</cfif>
<cfif Request.SlafDevTestProd is not "Dev">
	<cfapplication	name								= "CLS"
					sessionmanagement					= "Yes"
					sessiontimeout						= #CreateTimeSpan(0,1,0,0)#
					setclientcookies					= "No" />
	<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
		<cfset Session.ArrayUserRoles					= Variables.ArrayUserRoles>
		<cfset Session.CLS								= Duplicate(Variables.CLS)>
		<!--- The following loops are temporary, until we can all stop referencing GLS data outside of Session.GLS: --->
		<cfloop index="Level1Key"						list="#StructKeyList(Variables.CLS)#">
			<cfif IsStruct(Variables.CLS[Level1Key])>
				<cfset Session[Level1Key]				= Duplicate(Variables.CLS[Level1Key])>
				<cfloop index="Level2Key"				list="#StructKeyList(Variables.CLS[Level1Key])#">
					<cfset Session[Level2Key]			= Variables.CLS[Level1Key][Level2Key]>
				</cfloop>
			<cfelse>
				<cfswitch expression="#UCase(Level1Key)#">
				<cfcase value="ARRAYUSERROLES,ARRAYALLUSERROLES">
					<!---
					Session.ArrayUserRoles has already been defined, above. And we will henceforth keep ArrayAllUserRoles 
					only in Session.CLS.ArrayAllUserRoles. (BIG waste of memory to save it anywhere else.) 
					--->
				</cfcase>
				<cfdefaultcase>
					<cfset Session[Level1Key]			= Variables.CLS[Level1Key]>
				</cfdefaultcase>
				</cfswitch>
			</cfif>
		</cfloop>
	</cflock>
	<!--- Return the caller back into the GLS application: --->
	<cfapplication	name								= "GLS"
					sessionmanagement					= "Yes"
					sessiontimeout						= #CreateTimeSpan(0,1,0,0)#
					setclientcookies					= "No" />
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs End-of-"TimeThis"-passage. 
NOTES:				Called by manual include. 
INPUT:				Request.SBALogSystemName, maybe. If so, Request.SBALogTimeThisName is required. 
OUTPUT:				If currently configured to log it, End-of-"TimeThis"-passage log entry ("SleTimeEnd"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfset Variables.SleIncludeFileName			= "SleTimeEnd">
	<cfinclude template="log_SleTimeEndShared_DoNotCallDirectly.cfm">
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				06/21/2001, rewritten as a cfscript-based user-defined function 08/14/2003. 
DESCRIPTION:		Performs the same data validation functions as the EditMask JavaScript, only server-side, and in CFML.
NOTES:

	The following characters in pMask are used to validate pValue:
	1     - any digit  in the range of 0 - 1.
	2     - any digit  in the range of 0 - 2.
	3     - any digit  in the range of 0 - 3.
	4     - any digit  in the range of 0 - 4.
	5     - any digit  in the range of 0 - 5.
	6     - any digit  in the range of 0 - 6.
	7     - any digit  in the range of 0 - 7.
	8     - any digit  in the range of 0 - 8.
	9     - any digit  in the range of 0 - 9.
	#     - any digit  in the range of 0 - 9,          or '.' (no more than 2 digits after the '.').
	$     - any digit  in the range of 0 - 9, '$', ',' or '.' (no more than 2 digits after the '.').
	%     - any digit  in the range of 0 - 9, '%'      or '.' (no more than 2 digits after the '.').
	A     - any letter in the range of A - Z or a - z.
	B     - any letter in the range of A - Z or a - z, or ' '.
	E     - (e-mail) - letter/number/'-'/'.'/'_' combo, followed by '@', followed by letter/number/'-'/'.'/'_' combo.
	                   At least one '@' is required, and the field must end in a US domain (".com", ".net", etc).
	G     - (gender) - M, m, F or f.
	I     - (IP addr)- 0 - 9 or '.', exactly 3 periods
	N     - [alpha]numeric - A - Z, a - z or 0 - 9
	P     - (Sybase password) - any printable character.
	S     - same as '$', but also allows '-'.  Used at start of money mask to allow negative dollar amount (as in Net Worth)
	U     - URL.
	W     - Web URL (http or https only).
	X     - any character.
	Y     - (yes/no) - Y, y, N, n, X or x.  
	                   X and x are equivalent to N and n, but thought of as "not applicable".
	Other - must match that character exactly (including case if it's a letter).

	pValue characters are matched to pMask characters, character for character, until the end of pMask, whereupon the last 
	pMask character repeats. So 999-999-9999 is our pMask for phone numbers, which can be shortened to 999-999-9, if it weren't 
	for the fact that the shortened mask would confuse end users. 

	If Lst="Yes", pValue is treated as a comma-delimited list, and the mask is applied to each of its items. 
	Consequently, there is no "EditList" custom tag. If you use Lst="Yes", make sure Eng is of a form where, to refer to 
	them collectively, you could add "(s)" and to refer to a particular one, you could add a number. For example, 
	Eng="NAICS Code" is good, because collectively "NAICS Code(s)" and individually "NAICS Code #3" are good. 

INPUT:				Parameters
OUTPUT:				String
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	08/14/2003, SRS:	Converted to cfscript-based user-defined function. 
					06/22/2001, SRS:	Added EditList logic. 
					06/21/2001, SRS:	Original implementation.
--->

<cfscript>

// Constants: 
Variables.EditMaskOkayTLDs					= "com,edu,gov,int,mil,net,org,cc,nu,to,us,ws,aero,biz,coop,info,name,museum,pro";
Variables.EditMaskOkayTLDsPrettyList		= Replace(Variables.EditMaskOkayTLDs, ",", ", ", "ALL");

// Convenience variables returned to the caller:
Variables.EditMaskCallNbr					= 0;	// bumped up on each call
Variables.PeriodWasEncountered				= 0;
Variables.ValueAfterPeriod					= 0;
Variables.ValueBeforePeriod					= 0;
Variables.ValueInCents						= 0;

function EditMask
	(
	pName,		// Name of field to appear in error message. If it's of the form 'anchorname:fieldname', a hotlink will be generated that the user can use to jump to the field, assuming you defined the named anchor there.
	pValue,		// Value of the field to be validated.
	pMask,		// Same as the mask characters in EditMask.js, except that you can also use digits 1 through 8 to mean 'any digit in the range of 0 to this digit'. For example, the mask character 4 matches any digit in the range of 0 to 4.
	pRequired,	// "No" = optional, "Yes" = mandatory
	pType,		// Type of form element, in case it needs to be described in the error message. 1 = text or textarea, 2 = radio button, 3 = drop-down menu, 4 = hidden, 5 = checkbox.
	pMin,		// If Len(pValue) GT 0, minimum number of characters it must contain.
	pMax		// If Len(pValue) GT 0, maximum number of characters it may contain.
	)

{
// All local variables must be grouped at the top of the function body: 
var	sAllDollars								= "Yes";	// This refers to whether the mask is all one type. 
var	sAllEMail								= "Yes";	// This refers to whether the mask is all one type. 
var	sAllIPAddr								= "Yes";	// This refers to whether the mask is all one type. 
var	sAllNumber								= "Yes";	// This refers to whether the mask is all one type. 
var	sAllPercents							= "Yes";	// This refers to whether the mask is all one type. 
var	sAllURL									= "Yes";	// This refers to whether the mask is all one type. 
var	sAllWs									= "Yes";	// This refers to whether the mask is all one type. 
var	sAtSignWasEncountered					= "No";
var	sDigitsAfterPeriod						= 0;
var	sErrMsg									= "";
var	sFAsc									= 0;
var	sFChar									= "";
var	sFLen									= Len(pValue);
var	sIdx									= 0;
var	sLPI									= 0;		// Last period index. 
var	sMChar									= "";
var	sMLen									= Len(pMask);
var	sNameArray								= ListToArray(pName, ":");
var	sName									= "";
var	sOkay									= "No";
var	sServerIdx								= 0;
var	sTLD									= "(none)";
var	sValueIsNegative						= "No";

Variables.EditMaskCallNbr					= Variables.EditMaskCallNbr + 1;
Variables.PeriodWasEncountered				= 0;
Variables.ValueAfterPeriod					= 0;
Variables.ValueBeforePeriod					= 0;
Variables.ValueInCents						= 0;

if	(ArrayLen(sNameArray) IS 1)
	sName									= pName;
else
	sName									= "<a href=""###sNameArray[1]#"">#sNameArray[2]#</a>";

if	(sFLen IS 0)
	if	(pRequired)
		{
		Variables.ErrMsg					= Variables.ErrMsg & "<li>Mandatory field missing.  ";
		switch (pType)
			{
			case 2:	{Variables.ErrMsg		= Variables.ErrMsg & "You must select one of the #sName# option buttons.</li>";					break;}
			case 3:	{Variables.ErrMsg		= Variables.ErrMsg & "You must select one of the items from the #sName# drop-down menu.</li>";	break;}
			case 4:	{Variables.ErrMsg		= Variables.ErrMsg & "The hidden #sName# field was somehow lost.</li>";							break;}
			default:{Variables.ErrMsg		= Variables.ErrMsg & "You must enter something into the #sName# field.</li>";}
			}
		return	"No";
		}

if	(sFLen LT pMin)
	{
	if	(pRequired)
		Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# ";
	else
		Variables.ErrMsg					= Variables.ErrMsg & "<li>If given, #sName# ";
	if	(pMin IS pMax)
		Variables.ErrMsg					= Variables.ErrMsg & "must be exactly #pMin# character(s) long.</li>";
	else
		Variables.ErrMsg					= Variables.ErrMsg & "must be at least #pMin# character(s) long.</li>";
	return	"No";
	}

if	(sFLen GT pMax)

	{
	if	(pRequired)
		Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# ";
	else
		Variables.ErrMsg					= Variables.ErrMsg & "<li>If given, #sName# ";
	if	(pMin IS pMax)
		Variables.ErrMsg					= Variables.ErrMsg & "must be exactly #pMax# character(s) long.</li>";
	else
		Variables.ErrMsg					= Variables.ErrMsg & "cannot be more than #pMax# character(s) long.</li>";
	return	"No";
	}

for	(sIdx=1; sIdx LE sFLen; sIdx = sIdx + 1)
	{
	sFChar									= Mid(pValue, sIdx, 1);
	sFAsc									= Asc(sFChar);
	if	(sIdx GT sMLen)
		sMChar								= Right(pMask, 1);
	else
		sMChar								= Mid(pMask, sIdx, 1);

	if	( sFChar IS ".")					sLPI			= sIdx;
	if	( sMChar IS NOT "##")				sAllNumber		= "No";
	if	((sMChar IS NOT "$")
	AND	 (sMChar IS NOT "S"))				sAllDollars		= "No";
	if	( sMChar IS NOT "%")				sAllPercents	= "No";
	if	( sMChar IS NOT "E")				sAllEMail		= "No";
	if	( sMChar IS NOT "I")				sAllIPAddr		= "No";
	if	((sMChar IS NOT "U")
	AND	 (sMChar IS NOT "W"))				sAllURL			= "No";
	if	( sMChar IS NOT "W")				sAllWs			= "No";

	// sErrMsg is being built just in case it's needed, not because anything bad has happened yet: 
	sErrMsg									= "<li>#sName# ";

	if		(	pMask IS "##")	sErrMsg		= sErrMsg & "is not a valid number.  ";
	else if	(	pMask IS "$")	sErrMsg		= sErrMsg & "is not a valid dollar amount.  ";
	else if	(	pMask IS "%")	sErrMsg		= sErrMsg & "is not a valid percentage.  ";
	else if	(	pMask IS "9")	sErrMsg		= sErrMsg & "is not a valid number.  ";
	else if	(	pMask IS "A")	sErrMsg		= sErrMsg & "is not alphabetic.  ";
	else if	(	pMask IS "E")	sErrMsg		= sErrMsg & "is not a valid Internet e-mail address.  ";
	else if	(	pMask IS "I")	sErrMsg		= sErrMsg & "is not a valid full IP address.  ";
	else if	(	pMask IS "N")	sErrMsg		= sErrMsg & "is not alphanumeric.  ";
	else if	(	pMask IS "P")	sErrMsg		= sErrMsg & "is not a valid Sybase password.  ";
	else if	(	pMask IS "S##")	sErrMsg		= sErrMsg & "is not a valid signed floating-point number.  ";
	else if	(	pMask IS "S$")	sErrMsg		= sErrMsg & "is not a valid signed dollar amount.  ";
	else if	(	pMask IS "S9")	sErrMsg		= sErrMsg & "is not a valid signed number.  ";
	else if	(	pMask IS "U")	sErrMsg		= sErrMsg & "is not a valid URL.  ";
	else if	(	pMask IS "W")	sErrMsg		= sErrMsg & "is not a valid Web page URL.  ";
	else						sErrMsg		= sErrMsg & "must be of the form '#pMask#'.  ";

								sErrMsg		= sErrMsg & "Failure was at character #sIdx# ('#sFChar#'), ";

	// Double-quote is prohibited in all fields: 
	if	(sFChar IS Chr(34))
		{
		Variables.ErrMsg					= Variables.ErrMsg & sErrMsg
											& "which cannot be a double-quote, because double-quote interferes "
											& "with VALUE clauses in Web page forms. Prohibited in all text input fields.</li>";
		return "No";
		}

	// cfscript switch doesn't work with multiple values in case clause, so use if .. elseif logic: 
	if	(((sMChar GE "1") AND (sMChar LE "9"))
	or	  (sMChar IS "##")
	or	  (sMChar IS "$")
	or	  (sMChar IS "%"))
		{
		if	(IsNumeric(sMChar))
			if	(NOT IsNumeric(sFChar))
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been numeric.</li>";
				return "No";
				}
			else if	( sFChar GT sMChar)
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which must not exceed #sMChar#.</li>";
				return "No";
				}
		else
			if	(NOT IsNumeric(sFChar))
				{
				if	(		(sMChar IS "##")
				AND (NOT	(IsNumeric(sFChar)))
				AND (NOT	(sFChar IS ".")))
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "which should have been numeric or '.'.</li>";
					return "No";
					}
				else if	(	(sMChar IS "$")
					AND (NOT(IsNumeric(sFChar)))
					AND (NOT((sFChar IS ".") OR (sFChar IS "$") OR (sFChar IS ","))))
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "which should have been numeric, '$', ',' or '.'.</li>";
					return "No";
					}
				else if	(	(sMChar IS "%")
					AND (NOT(IsNumeric(sFChar)))
					AND (NOT((sFChar IS ".") OR (sFChar IS "%"))))
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "which should have been numeric, '.' or '%'.</li>";
					return "No";
					}
				else if	(	(sMChar IS "S")
					AND (NOT(IsNumeric(sFChar)))
					AND (NOT((sFChar IS "+") OR (sFChar IS "-"))))
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "which should have been numeric, '+' or '-'.</li>";
					return "No";
					}

				if	(sFChar IS ".")
					if	(Variables.PeriodWasEncountered GT 0)
						{
						Variables.ErrMsg= Variables.ErrMsg & sErrMsg & "because you can't have 2 decimal points.</li>";
						return "No";
						}
					else
						Variables.PeriodWasEncountered	= Variables.PeriodWasEncountered + 1;

				else if	( sFChar IS "-")				// Can get here only if MChar is "S", so no need to test MChar. 
					Variables.ValueIsNegative			= "Yes";
				else if	( IsNumeric(sFChar))
					if	(Variables.PeriodWasEncountered GT 0)
						{
						Variables.DigitsAfterPeriod		= Variables.DigitsAfterPeriod + 1;
						Variables.ValueAfterPeriod		= (Variables.ValueAfterPeriod	* 10) + sFChar;
						}
					else
						Variables.ValueBeforePeriod		= (Variables.ValueBeforePeriod	* 10) + sFChar;
				}
		}

	else if	((sMChar IS "A") OR (sMChar IS "B"))	// Alphabetic, or Alphabetic-including-blank:
		{
		if	( (Variables.FAsc LT Asc("A"))
		OR	 ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
		OR	  (Variables.FAsc GT Asc("z")))// That is, if not alphabetic: 
			if	(sMChar IS "A")
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been alphabetic.";
				return "No";
				}
			else if	( sFChar IS NOT " ")
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been alphabetic or ' '.";
				return "No";
				}
		}

	else if (sMChar IS "E")							// E-mail address: 
		{
		if	( (Variables.FAsc LT Asc("0"))
		OR	 ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
		OR	 ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
		OR	  (Variables.FAsc GT Asc("z")))// That is, if not alphabetic or numeric: 
			{
			if	((Variable.FChar IS NOT "&")
			AND	 (Variable.FChar IS NOT "-")
			AND	 (Variable.FChar IS NOT ".")
			AND	 (Variable.FChar IS NOT "@")
			AND	 (Variable.FChar IS NOT "_"))
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been alphabetic, numeric, "
										& "ampersand, minus, period, '@' or underscore.";
				return "No";
				}
			if	(sFChar IS "@")
				if	(Variables.AtSignWasEncountered)
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "because you can't have 2 @-signs.";
					return "No";
					}
				else
					Variables.AtSignWasEncountered	= "Yes";
			}
		}

	else if (sMChar IS "G")							// Gender: 
		{
		if	((sFChar IS NOT "M")
		AND	 (sFChar IS NOT "F"))
			{
			Variables.ErrMsg			= Variables.ErrMsg & sErrMsg & "which should have been 'M' or 'F'.";
			return "No";
			}
		}

	else if (sMChar IS "I")	// IP address: 
		{
		if	((Variables.FAsc GE Asc("0"))
		AND	 (Variables.FAsc LE Asc("9")))			// That is, if numeric: 
			Variables.ValueBeforePeriod		= (Variables.ValueBeforePeriod	* 10) + sFChar;
		else	// That is, if not numeric: 
			if	(sFChar IS ".")
				{
				Variables.PeriodWasEncountered	= Variables.PeriodWasEncountered + 1;
				if	(Variables.ValueBeforePeriod GT 255)
					{
					Variables.ErrMsg	= Variables.ErrMsg & sErrMsg & "because the value before this '.' is over 255.";
					return "No";
					}
				else
					Variables.ValueBeforePeriod	= 0;
				}
			else
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been numeric or '.'.";
				return "No";
				}
		}

	else if (sMChar IS "N")						// Alphabetic or numeric: 
		{
		if	( (Variables.FAsc LT Asc("0"))
		OR	 ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
		OR	 ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
		OR	  (Variables.FAsc GT Asc("z")))	// That is, if not alphabetic or numeric: 
			{
			Variables.ErrMsg			= Variables.ErrMsg & sErrMsg & "which should have been alphabetic or numeric.";
			return "No";
			}
		}

	else if (sMChar IS "P")							// Printable = Sybase Password: 
		{
		if	((Variables.FAsc LT Asc(" "))
		OR	 (Variables.FAsc GT Asc("~")))
			{
			Variables.ErrMsg			= Variables.ErrMsg & sErrMsg & "which should have been a printable character.";
			return "No";
			}
		}

	else if ((sMChar IS "U") OR (sMChar IS "W"))		// URL, or WWW URL: 
		{
		if	( (Variables.FAsc LT Asc("0"))
		OR	 ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
		OR	 ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
		OR	  (Variables.FAsc GT Asc("z")))	// That is, if not alphabetic or numeric: 
			if	((sFChar IS NOT "##")
			AND  (sFChar IS NOT "$")
			AND  (sFChar IS NOT "%")
			AND  (sFChar IS NOT "&")
			AND  (sFChar IS NOT "+")
			AND  (sFChar IS NOT "-")
			AND  (sFChar IS NOT ".")
			AND  (sFChar IS NOT "/")
			AND  (sFChar IS NOT ":")
			AND  (sFChar IS NOT "=")
			AND  (sFChar IS NOT "?")
			AND  (sFChar IS NOT "@")
			AND  (sFChar IS NOT "_")
			AND  (sFChar IS NOT "~"))
				{
				Variables.ErrMsg		= Variables.ErrMsg & sErrMsg & "which should have been alphabetic, numeric, "
										& "'##', '$', '%', ampersand, plus, minus, period, '/', ':', '=', '?', "
										& "'@', underscore or tilde.";
				return "No";
				}
		}

	else if (sMChar IS "Y")						// 'Y' or 'N': 
		{
		if	((sFChar IS NOT "Y")
		AND	 (sFChar IS NOT "N"))
			{
			Variables.ErrMsg			= Variables.ErrMsg & sErrMsg & "which should have been 'Y' or 'N'.";
			return "No";
			}
		}

	else	// If not one off the mask characters above, and if not "X", FChar must equal MChar: 
		{
		if	((sMChar IS NOT "X")
		AND	 (sFChar IS NOT sMChar))
			{
			Variables.ErrMsg			= Variables.ErrMsg & sErrMsg & "which should have been '#sMChar#'.";
			return "No";
			}
		}
	}

// If we get here, we compared correctly to the edit mask. But we could still fail on special edits: 
if	(sAllDollars OR sAllNumber OR sAllPercents)	// Type-specific special edits. 
	{
	switch (Variables.DigitsAfterPeriod)
		{
		case 0:		{Variables.ValueOfCentsOnly	= 0;								break;}
		case 1:		{Variables.ValueOfCentsOnly	= Variables.ValueAfterPeriod * 10;	break;}
		case 2:		{Variables.ValueOfCentsOnly	= Variables.ValueAfterPeriod;		break;}
		default:	{Variables.ValueInCents		= 0;
					 Variables.ValueOfCentsOnly	= 0;}
		}
	Variables.ValueInCents					= (Variables.ValueBeforePeriod * 100) +	Variables.ValueOfCentsOnly;
	Variables.ValueOfCentsOnly				=										Variables.ValueOfCentsOnly;
	if	(Variables.ValueIsNegative)
		Variables.ValueInCents				= 0 - Variables.ValueInCents;
	if	(Variables.DigitsAfterPeriod GT 2)
		{
		Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# cannot have more than 2 digits after the decimal point.</li>";
		return "No";
		}
	}

else if	(sAllPercents AND (Variables.DigitsAfterPeriod GT 2))	// Type-specific special edits. 
	{
	Variables.ErrMsg						= Variables.ErrMsg & "<li>#sName# cannot have more than 2 digits after the decimal point.";
	if	(Variables.ValueBeforePeriod IS 0)
		Variables.ErrMsg					= Variables.ErrMsg & "  (express as a percentage, not as a fraction of 1.0000)";
	Variables.ErrMsg						= Variables.ErrMsg & ".</li>";
	return "No";
	}

else if	(sAllEMail)							// Type-specific special edits. 
	{
	if	(Variables.AtSignWasEncountered IS 0)
		Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# must contain '@'.</li>";
	else
		{
		sTLD								= "(none)";
		sOkay								= "No";
		if	(sLPI GT 0)
			{
			sTLD							= Right(pValue, sFLen - sLPI);
			if	(ListFind(Variables.EditMaskOkayTLDs, sTLD) GT 0)
				sOkay						= "Yes";
			}
		if	(NOT sOkay)
			{
			Variables.ErrMsg				= Variables.ErrMsg
											& "<li>#sName# has a 'Top Level Domain name' of '#sTLD#'. "
											& "PRO-Net e-mail addresses must end in a United States 'Top Level Domain name': "
											& "#Variables.EditMaskOkayTLDsPrettyList#.</li>";
			return "No";
			}
		}
	}

else if	(sAllIPAddr)						// Type-specific special edits. 
	{
	if	(Variables.PeriodWasEncountered IS NOT 3)
		{
		Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# must contain exactly 3 periods. </li>";
		return "No";
		}
	}

else if	(sAllURL OR sAllWs)	// Type-specific special edits. 
	{
	// Could potentially generate 2 special edit error messages. First is bad protocol: 
	sServerIdx								= 0;
	if	(Variables.AllWs)
		{
		if	((Left(pValue, 7) IS "http://"))	sServerIdx	=  8;
		if	((Left(pValue, 8) IS "https://"))	sServerIdx	=  9;
		if	(sServerIdx IS 0)
			{
			Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# must begin with "
												& "'http://' or 'https://'.</li>";
			return "No";
			}
		}
	else
		{
		if	((Left(pValue, 6) IS "ftp://"))		sServerIdx	=  7;
		if	((Left(pValue, 9) IS "gopher://"))	sServerIdx	= 10;
		if	((Left(pValue, 7) IS "http://"))	sServerIdx	=  8;
		if	((Left(pValue, 8) IS "https://"))	sServerIdx	=  9;
		if	((Left(pValue, 9) IS "telnet://"))	sServerIdx	= 10;
		if	(sServerIdx IS 0)
			{
			Variables.ErrMsg					= Variables.ErrMsg & "<li>#sName# must begin with "
												& "'ftp://', 'gopher://', 'http://', 'https://' or 'telnet://'.</li>";
			return "No";
			}
		}

	// Could potentially generate 2 special edit error messages. Second is bad TLD: 
	sLPI										= 0;	// Last period index. 
	for	(sIdx=sServerIdx; sIdx LE sFLen; sIdx = sIdx + 1)
		{
		sFChar									= Mid(pValue, Idx, 1);
		if		(sFChar IS ".")					sLPI	= Idx;
		else if	(sFChar IS "/")					break;
		}
	sTLD										= "(none)";
	sOkay										= "No";
	if	(sLPI GT 0)
		{
		sTLD									= Right(pValue, sFLen - sLPI);
		if	(ListFind(Variables.EditMaskOkayTLDs, sTLD) GT 0)
			sOkay								= "Yes";
		}
	if	(NOT sOkay)
		{
		Variables.ErrMsg						= Variables.ErrMsg
												& "<li>#sName# has a 'Top Level Domain name' of '#sTLD#'. "
												& "PRO-Net Internet addresses must end in a United States 'Top Level Domain name': "
												& "#Variables.EditMaskOkayTLDsPrettyList#.</li>";
		return "No";
		}
	}

return	"Yes";
}	// End of EditMask definition
</cfscript>

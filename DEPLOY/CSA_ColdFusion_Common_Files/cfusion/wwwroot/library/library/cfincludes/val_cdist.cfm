<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) IS NOT 2>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 2 characters.</li>">
	<cfelseif (NOT IsNumeric(Variables.FVal)) AND (Variables.FVal IS NOT "AL")>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 2 digits or ""AL"".</li>">
	</cfif>
</cfif>

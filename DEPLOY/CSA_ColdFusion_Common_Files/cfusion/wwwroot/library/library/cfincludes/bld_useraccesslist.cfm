<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc. 
DATE:				07/06/2010
DESCRIPTION:		Builds various logic variables used to make MainNav buttons into external system passoffs. 
NOTES:				Must be called before bld_buttonlist, which uses these variables. 
INPUT:				Variables.ArrayUserRoles (context sensitive, trimmed down version of ArrayAllUserRoles, from GLS). 
OUTPUT:				Various logic variables used to make MainNav buttons into external system passoffs. 
REVISION HISTORY:	08/15/2011, NNI:	Replaced Chron button with CLCS.
					08/15/2011, SRv:	Changed RequestedSubSystemDir name for Post Servicing to Post.
					08/02/2011, SRv:	Changed to setting role by system name, not role name. 
					08/01/2011, SRS:	Added ability of changesystem hotlinks to log system changes. This required saving 
										the highest powered role that triggered the change of system. 
					07/06/2010, SRv:	Original implementation. Include routine to build Access List. 
--->

<!--- Configuration Parameters: --->

<cfset Variables.ChgSysCrypticPassoff					= "Yes"><!--- Using AURIdx is cryptic. Using RoleName is not. --->
<cfset Variables.ExternalSystemAbbrsList				= "CLCS,ElendOrig,ELendServicing,Elips,GPTS,Lana,PostServicing">

<!--- Initializations of defaults: --->

<cfloop index="Idx" list="#Variables.ExternalSystemAbbrsList#">
	<cfset Variables["CanSee"	& Idx]					= "No">
	<cfset Variables["RoleIdx"	& Idx]					= "">
	<cfset Variables["RoleNm"	& Idx]					= "">
	<cfset Variables["RoleParm"	& Idx]					= "">
</cfloop>
<cfset Variables.UserCanSeeSys							= "">
<cfset Variables.ShowButtonList							= "">

<cfif NOT IsDefined("SetCurrentRoleForSys")>			<!--- (Highest power we care about that user has, that is.) --->
	<cffunction name="SetCurrentRoleForSys"				returntype="any"><!--- Actually returntype="void", if that existed. --->
		<cfargument name="SysAbbr"						required="Yes" type="string">
		<cfset Variables["RoleIdx"	& Arguments.SysAbbr]= Idx>
		<cfset Variables["RoleNm"	& Arguments.SysAbbr]= Variables.ChgSysRoleNm>
	</cffunction>
</cfif>

<!--- Accumulate actual data: --->

<cfloop index="Idx" from="1" to="#ArrayLen(Variables.ArrayUserRoles)#">
	<!--- 13 is the column number of the login credential's assurance level (how trusted is this current login): --->
	<cfif Variables.ArrayUserRoles[Idx][13] LTE Variables.OrigLoginLevel>
		<!--- 1 is the column number of the role name itself: --->
		<cfset Variables.ChgSysRoleNm					= Trim(Variables.ArrayUserRoles[Idx][1])>
		<!--- 5 is the column number of the system abbreviation: --->
		<cfset Variables.ChgSysNm						= Trim(Variables.ArrayUserRoles[Idx][5])>
		<cfswitch expression="#Variables.ChgSysNm#">
			<cfcase value="CLCS">
				<cfset Variables.CanSeeCLCS				= "Yes">
				<cfset SetCurrentRoleForSys				("CLCS")>
			</cfcase>
			<cfcase value="ELIPS">
				<cfset Variables.CanSeeElips			= "Yes">
				<cfset SetCurrentRoleForSys				("Elips")>
			</cfcase>
			<cfcase value="GPTS">
				<cfset Variables.CanSeeGPTS				= "Yes">
				<cfset SetCurrentRoleForSys				("GPTS")>
			</cfcase>
			<cfcase value="LANA">
				<cfset Variables.CanSeeLANA				= "Yes">
				<cfset SetCurrentRoleForSys				("Lana")>
			</cfcase>
			<cfcase value="LoanOrig">
				<cfset Variables.CanSeeElendOrig		= "Yes">
				<cfset SetCurrentRoleForSys				("ELendOrig")>
			</cfcase>
			<cfcase value="LoanPostServ">
				<cfset Variables.CanSeePostServicing	= "Yes">
				<cfset SetCurrentRoleForSys				("PostServicing")>
			</cfcase>
			<cfcase value="LoanServ">
				<cfset Variables.CanSeeElendServicing		= "Yes">
				<cfset SetCurrentRoleForSys				("ELendServicing")>
			</cfcase>
		</cfswitch>
	</cfif>
</cfloop>
<cfif Variables.CanSeeElendOrig>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Origination")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"applications")>
</cfif>
<cfif Variables.CanSeeElendServicing>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Servicing")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"servicing")>
</cfif>
<cfif Variables.CanSeeLana>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"LANA")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"LANA")>
</cfif>
<cfif Variables.CanSeeCLCS>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"CLCS")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"CLCS")>
</cfif>
<cfif Variables.CanSeeGPTS>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"GPTS")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"GPTS")>
</cfif>
<cfif Variables.CanSeeElips>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"ELIPS")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"ELIPS")>
</cfif>
<cfif Variables.CanSeePostServicing>
	<Cfset Variables.UserCanSeeSys						= ListAppend(Variables.UserCanSeeSys,"Post Servicing")>
	<Cfset Variables.ShowButtonList						= ListAppend(Variables.ShowButtonList,"Post")>
</cfif>

<cfloop index="Idx" list="#Variables.ExternalSystemAbbrsList#">
	<cfif Variables.ChgSysCrypticPassoff><!--- Configuration Parameter, above. --->
		<cfset Variables["RoleParm"&Idx]				= "AURIdx="		& URLEncodedFormat(Variables["RoleIdx"&Idx])>
	<cfelse>
		<cfset Variables["RoleParm"&Idx]				= "RoleName="	& URLEncodedFormat(Variables["RoleNm"&Idx])>
	</cfif>
</cfloop>
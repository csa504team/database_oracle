<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/06/2008
DESCRIPTION:		Shared routine to call the GLS Login Web Service correctly for the current machine. 
NOTES:

	This file assumes that /library/cfincludes/get_sbashared_variables.cfm has been cfincluded. Usually this is done 
	in Application.cfm by cfincluding /library/cfincludes/get_sbalookandfeel_variables.cfm, but not necessarily. 
	This code can just as easily be called from within a Web Service, where possibly only get_sbashared_variables 
	has been called, because there isn't any Session scope. As a safeguard, this file will do its own cfinclude of 
	get_sbashared_variables if it discovers that the necessary shared variables are not defined. 

	If GLS is local to the current machine, this file cfincludes /cls/ws/wbs_login.cfm. If GLS is NOT local to the 
	current machine, this file does a cfinvoke to the login server associated with the current machine. Either way, 
	the caller is left with Variables.CLS defined. 

	If the caller is in a context that requires establishing Session.CLS, the caller should also cfinclude the 
	companion file to this one, /library/cfincludes/act_login_establish_session_vars.cfm, which essentially copies 
	the Variables.CLS structure into the Session.CLS structure and assorted other Session variable names. But new 
	apps should only be using the GLS structure and its substructures, not those assorted other Session variables. 

	The actual work will be done by /cls/ws/wbs_login.cfm. You might want to read the comment header notes at the 
	top of that file as well. It gives an example, and it documents what the various input Variables mean. 

INPUT:				Request.SlafLogin variables		from get_sbashared_variables
					Variables.LoginType				always
					Variables.LoginUsername			if LoginType is "C", "G" or "L"
					Variables.LoginPassword			if LoginType is "C", "G", "L" or "R"
					Variables.LoginUserAttrs		if LoginType is "E"
					Variables.LoginRoles			optional, only if you want to restrict the roles returned. 
OUTPUT:				Variables.CLS. 
REVISION HISTORY:	08/27/2015, SJK:	Replaced GLS text to user with CLS
					12/23/2008, SRS:	Got rid of LoginType = "A". (Doesn't work under multiserver configuration.) 
					12/11/2008, SRS:	Changes to support multiserver configuration of ColdFusion. 
					07/02/2008, SRS:	Changes to support saving memory. 
					05/22/2008, SRS:	Added CFTry and CFCatch to the CFInvokes. 
					03/07-14/2008, SRS:	Original Implementation. 
--->

<cfset Variables.CLS								= StructNew()>
<cfset Variables.CLS.CLSAuthorized					= "No">
<cfset Variables.CLS.CLSCommentary					= "">
<cfset Variables.CLS.CLSErrCode						= 999><!--- Always hard-coded 999, do not change. --->
<cfset Variables.CLS.CLSErrMsg						= "">
<cfset Variables.CLS.CLSMustActivate				= "No">
<cfset Variables.CLS.CLSMustChgPass					= "No">
<cfparam name="Variables.LoginRoles"				default="">

<cfif NOT IsDefined("Request.SlafLoginLocally")>
	<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
</cfif>
<cfif Request.SlafLoginLocally><!---  AND (CGI.Remote_Addr IS NOT "165.110.40.166") --->
	<cfinclude template="#Request.SlafLoginInclude#">
	<cfif	IsDefined("Variables.CLS.CLSErrCode")
		AND	(Variables.CLS.CLSErrCode				IS 7734)><!--- Special GLSErrCode used for debugging. --->
		<cfoutput>
CLS Login is temporarily down to debug an error. Please try back in 5-10 minutes. 
</cfoutput>
<cf_setrequesttimeout seconds="240">
<cfdump var="#Variables#" label="Variables">
<cfdump var="#Request#" label="Request">
<cfabort>
	</cfif>
<cfelseif Variables.LoginType IS "A">
	<cfset Variables.CLS.CLSErrCode					= 1><!--- Always hard-coded 999, do not change. --->
	<cfset Variables.CLS.CLSErrMsg					= "LoginType=""A"" no longer supported in multiserver configuration "
													& "of ColdFusion Server. Use LoginType=""R"" instead. ">
<cfelseif Variables.LoginType IS "E">
	<cftry>
		<cfinvoke
			webservice="#Request.SlafLoginWSDL#"
			method="Login_EAuth"
			returnvariable="Variables.CLS">
			<cfinvokeargument name="LoginUserAttrs"	value="#Variables.LoginUserAttrs#">
			<cfinvokeargument name="LoginRoles"		value="#Variables.LoginRoles#">
		</cfinvoke>
		<cfinclude template="/library/cfincludes/act_login_deserialize_arrays.cfm">
		<cfcatch type="Any">
			<cfset Variables.CLS.CLSErrMsg			= "INTERNAL ERROR. "
													& "An internal error occurred invoking the Login_EAuth Web Service. "
													& "Please contact the SBA developers. "
													& "The following information may help: "
													& CFCatch.Message & " " & CFCatch.Detail>
		</cfcatch>
	</cftry>
<cfelse>
	<cftry>
		<cfinvoke
			webservice="#Request.SlafLoginWSDL#"
			method="Login"
			returnvariable="Variables.CLS">
			<cfinvokeargument name="LoginType"		value="#Variables.LoginType#">
			<cfinvokeargument name="LoginUsername"	value="#Variables.LoginUsername#">
			<cfinvokeargument name="LoginPassword"	value="#Variables.LoginPassword#">
			<cfinvokeargument name="LoginRoles"		value="#Variables.LoginRoles#">
		</cfinvoke>
		<cfinclude template="/library/cfincludes/act_login_deserialize_arrays.cfm">
		<cfcatch type="Any">
			<cfset Variables.CLS.CLSErrMsg			= "INTERNAL ERROR. "
													& "An internal error occurred invoking the Login Web Service. "
													& "Please contact the SBA developers. "
													& "The following information may help: "
													& CFCatch.Message & " " & CFCatch.Detail>
		</cfcatch>
	</cftry>
</cfif>

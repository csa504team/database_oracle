<!---
AUTHOR:				Steve Seaquist
DATE:				06/27/2006
DESCRIPTION:		Requests 401 challenge data from the browser. 
NOTES:
	Use get_401AuthData.cfm to retrieve HttpHeaderUsername and HttpHeaderPassword from a previous 401 challenge. 
	Example usage (using parentheses instead of angle brackets): 

					(cfinclude template="/library/cfincludes/get_401AuthData.cfm")
					(cfinclude template="/library/cfincludes/dsp_401AuthDialog.cfm")
					(!--- At this point, both HttpHeaderUsername and HttpHeaderPassword will contain data, for sure. ---)

INPUT:				Variables.HttpHeaderUsername, Variables.HttpHeaderPassword. Possibly also HttpHeaderRealm. 
OUTPUT:				401 response if either of them is the nullstring. 
REVISION HISTORY:	06/27/2006, SRS:	Original implementation.
--->

<cfparam name="Variables.HttpHeaderRealm"			default="Your Windows Login">
<cfif	(Len(Variables.HttpHeaderUsername) IS 0)
	OR	(Len(Variables.HttpHeaderPassword) IS 0)>
	<cfheader statuscode="401" statustext="Authorization Required">
	<cfheader name="WWW-Authenticate" value="Basic realm=""#Variables.HttpHeaderRealm#""">
	<cfoutput>Not Authorized</cfoutput>
	<cfabort>
</cfif>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/22/2015. 
DESCRIPTION:		Shared display of user profile (dsp_addcustomer, dsp_profile and dsp_user all cfinclude this file). 
NOTES:

	This page coalesces 3 other pages that were written independently of one another. So we cannot rely on logic variables 
	created in the /security/user directory if the user is actually in /cls, right? Regardless of how logic variables were 
	set up in the calling page's directory, this page has to behave correctly. 

	Note the liberal use of cfparam and checking whether or not something already exists. For example, look how HelpTopic 
	checks Show before appending "Help" to Show and checks JSInline before appending the help() script to JSInline. If different 
	calling pages are **TOO** different in how they do things, this technique allows passing versions of the same variables that 
	already contain what the calling page needs. This preserves our freedom to put anything we need onto any of the calling pages. 
	Understand this before you modify this shared page, please. 

INPUT:				CGI.Script_Name (to establish context of calling page). 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Original implementation. Cannibalized code from /cls/dsp_addcustomer (the reference 
										standard for field order, per SMM), then /cls/dsp_profile, then /security/user/dsp_user. 
										(So, safest to most dangerous.) 
--->

<!--- ************************************************************************************************************ --->

<!--- Configuration Parameters: --->

<cfset Variables.db											= "oracle_housekeeping"><!--- dbtype already contains "Oracle80" --->
<cfset Variables.FormLabelWidth								= ""><!--- If not given, "262px" is SBA Look-and-Feel default. --->
<cfset Variables.ListFieldsetsAll							= "login,identity,contact,usertype,infoassociated,creds,verify">

<!--- ************************************************************************************************************ --->

<!--- Verify Minimal Inputs: --->

<cfparam name="Variables.ErrMsg"							default="">
<cfparam name="Variables.TxnErr"							default="No">

<cfif NOT Variables.TxnErr><!--- Keep indention low. --->
	<cfif		(NOT IsDefined("Variables.EndUserData.IMUserId"))
		or		(NOT IsNumeric( Variables.EndUserData.IMUserId ))>
		<cfset Variables.ErrMsg								&="ERROR. The routine to set data on the page was not passed "
															& "sufficient information to display data.">
		<cfset Variables.TxnErr								= "Yes">
	<cfelseif	(NOT IsDefined("Variables.EndUserData.MayEdit"))
		or		(NOT IsBoolean( Variables.EndUserData.MayEdit ))>
		<cfset Variables.ErrMsg								&="ERROR. The routine to set data on the page was not passed "
															& "sufficient information about permissions.">
		<cfset Variables.TxnErr								= "Yes">
	</cfif>
</cfif><!--- /TxtErr --->

<cfif Variables.TxnErr>
	<cfoutput>#Request.SlafHead#
<title>SBA - Error Requesting Page</title>#Request.SlafTopOfHead#
</head>
<body class="pad10">
</cfoutput><cfinclude template="#Variables.LibIncURL#/dsp_errmsg.cfm"><cfoutput>
</body>
</html>
</cfoutput>
</cfif>

<!--- ************************************************************************************************************ --->

<!--- Initializations: --->

<cfif NOT IsDefined("spcn")>
	<cfinclude template="#Variables.LibUdfURL#/bld_dbutils_for_dbtype.cfm">
</cfif>
<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="Server" type="ReadOnly" timeout="30">
	<!---
	If the user's values for any of the following are for a row that's been "enddated", the old value won't show up as checked 
	or selected. This forces the user to pick a new value to save the profile, which is normally what we would want to do. 
	HOWEVER: If there's ever a value that we want to continue to allow a user to keep, despite its being enddated, use "All" 
	in the dsp page to allow the grandfathered value, and disallow a change **TO** that value with "Actv" in the act page. 
	--->
	<cfset Variables.ActvIMAsurncLvlTbl						= Server.Scq.security.ActvIMAsurncLvlTbl>
	<cfset Variables.ActvIMJobTitlTypTbl					= Server.Scq.security.ActvIMJobTitlTypTbl>
	<cfset Variables.ActvIMUserSuspRsnTbl					= Server.Scq.security.ActvIMUserSuspRsnTbl>
	<cfset Variables.ActvIMUserTypTbl						= Server.Scq.security.ActvIMUserTypTbl>
	<cfset Variables.ScqActvCountries						= Server.Scq.ActvIMCntryCdTbl>	<!--- Oddly, not used by addcustomer. --->
	<cfset Variables.ScqActvStates							= Server.Scq.ActvStTbl>			<!--- Oddly, not used by addcustomer. --->
</cflock>
<cfif NOT IsDefined("Defaults")>
	<cfinclude template="bld_DefaultsUDF.cfm"><!--- Same directory as this file, for now. --->
</cfif>
<cfif NOT IsDefined("LoadOrig")>
	<cfinclude template="#Variables.LibUdfURL#/bld_DspPageLoadTimeOrigsUDFs.cfm"><!--- Safer than hidden fields. --->
</cfif>
<cfif NOT IsDefined("Request.CurrIPNelliSalatova")>
	<cfinclude template="#Variables.LibIncURL#/bld_CurrIPs.cfm">
</cfif>

<cfset		   Variables.ActionPage							= Replace(Variables.PageName, "dsp_", "act_",			"One")>
<cfif CGI.Remote_Addr is Request.CurrIPNelliSalatova>
	<cfset	   Variables.ActionPage							= Replace(Variables.PageName, ".cfm", "_shared.cfm",	"One")>
</cfif><!---   Other different action pages are also possible. Contact Steve S. if needed. --->
<cfparam name="Variables.AppDataInline"						default="">
<cfparam name="Variables.EndUserData.IMUserId"				default="0">		<!--- Default is least power = new user. --->
<cfparam name="Variables.FormName"							default="UpdateProfile">
<cfparam name="Variables.HelpFile"							default="#Variables.AppURL#/help/help.cfm">
<cfparam name="Variables.HelpTopic"							default="">		<!--- Calling page has to ask for help by topic. --->
<cfparam name="Variables.JSInline"							default="">
<cfparam name="Variables.ListFieldsetsShow"					default="">
<cfparam name="Variables.ListFieldsetsSkip"					default="">
<cfparam name="Variables.MightNotGetSent"					default="">
<cfparam name="Variables.PageTitle"							default="">
<cfparam name="Variables.Show"								default="">
<cfif Len(Variables.ListFieldsetsShow) is 0>
	<cfset Variables.ListFieldsetsShow						= Variables.ListFieldsetsAll>
</cfif>
<cfif	(Len	(CGI.Script_Name)	gt  9)
	and	(Left	(CGI.Script_Name,		9) is "/security")>
	<cfset Variables.DirectoryIsCLS							= "No">
	<cfset Variables.DirectoryIsSecurity					= "Yes">
	<cfset Variables.WindowTitle							= "SBA - Security System">
<cfelse>
	<cfset Variables.DirectoryIsCLS							= "Yes">
	<cfset Variables.DirectoryIsSecurity					= "No">
	<cfset Variables.WindowTitle							= "SBA - CLS">
</cfif>
<cfif Len(Variables.PageTitle) gt 0>
	<cfset Variables.WindowTitle							&= " - #Variables.PageTitle#">
</cfif>
<cfset Variables.PageIsAddCustomer							= "No">
<cfset Variables.PageIsProfile								= "No">
<cfset Variables.PageIsUser									= "No">
<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/cls/dsp_addcustomer.cfm,/cls/dsp_addcustomer2.cfm">
	<cfset Variables.PageIsAddCustomer						= "Yes">
</cfcase>
<cfcase value="/cls/dsp_profile.cfm,/cls/dsp_profile2.cfm">
	<cfset Variables.PageIsProfile							= "Yes">
</cfcase>
<cfcase value="/security/user/dsp_user.cfm,/security/user/dsp_user2.cfm">
	<cfset Variables.PageIsUser								= "Yes">
</cfcase>
</cfswitch>

<cfset Variables.ReadOnlyProfile							= (NOT Variables.EndUserData.MayEdit)>

<cfset Variables.PageNames									= Variables.PageName><!--- Set in get_sbashared_variables. --->
<cfif Variables.PageIsProfile>
	<cfset Variables.PageNames								&= ",dsp_choosefunction.cfm">
</cfif>

<!--- FormDataRecovery: --->

<cfset Variables.DisplayingFormDataEditedByUser				= (IsDefined("URL.PrevErr") and Variables.FormDataRecovered)>
<cfset Variables.DisplayingFormDataFromDatabase				= (NOT Variables.DisplayingFormDataEditedByUser)>

<cfif Variables.DisplayingFormDataEditedByUser><!--- Subsequent entries into this page: --->
	<cfset GetDspPageLoadTimeOrigs()>
	<cfset Variables.MayEdit								= Variables.Origs.MayEdit>
<cfelse><!--- That is, if DisplayingFormDataFromDatabase, as guaranteed by the NOT that defined it: --->
	<cfinclude template="bld_Origs.cfm">
</cfif>

<!--- ************************************************************************************************************ --->

<!---
IMPLEMENTATION NOTE: 

In the dsp_userprofile.((fieldsetname)).cfm files, and here in dsp_userprofile.cfm, we append to AppDataInline first 
and the append to JSInline. The reason is, we generate fieldnames (using Defaults()) in AppDataInline. So it makes sense 
to append to JSInline afterwards, after we know what the fieldnames are. It's also in aphabetical order, not that it 
matters that much. 

But it's important to remember, when the page goes out to the user, JSInline will be in the head, before AppDataInline. 

HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS 
HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS 
HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS 
HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS HEADERS 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<div class="pad10"></cfoutput>
	<cfif NOT Variables.ReadOnlyProfile>
		<cfoutput>
<form method="post" name="#Variables.FormName#" action="#Variables.ActionPage#" onsubmit="return DoThisOnSubmit(this)">
<input type="Hidden" name="PageNames"						value="#Variables.PageNames#">
<input type="Hidden" name="MightNotGetSent"					value="#Variables.MightNotGetSent#">
</cfoutput>
	</cfif>
</cfsavecontent><!--- /AppDataInline, unconditional header --->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#</cfoutput>
	<cfif NOT Variables.ReadOnlyProfile>
		<cfoutput>
#Request.SlafTopOfHeadClearForm#<!--- Used by this file. --->
#Request.SlafTopOfHeadEditMask#<!--- Could be used by any, so include here. (Logic to prevent dupe too complex otherwise.) --->
#Request.SlafTopOfHeadEditTin#<!--- Could be used by usertype or infoassociated, so include here. --->
<script>

function isMand												(pThis)
	{
	return $(pThis).parent(".manddata,.reqddata").length;
	}

</script>
</cfoutput>
	</cfif><!--- /ReadOnlyProfile --->

	<cfif	(Len(Variables.FormLabelWidth)					gt 0)
		and	(Variables.FormLabelWidth						is not "262px")><!--- "262px" is SBA Look-and-Feel default. --->
		<cfoutput>
<style>
.formlabel
	{
	width:													#Variables.FormLabelWidth#;
	}
</style>
</cfoutput>
	</cfif><!--- /FormLabelWidth --->
</cfsavecontent>

<!--- ************************************************************************************************************ --->

<!---
INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES 
INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES 
INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES 
INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES INCLUDES 
--->

<!--- ************************************************************************************************************ --->

<cfset Variables.ListFieldsetsShow							= LCase(Variables.ListFieldsetsShow)><!--- (No ListFindNoCase) --->
<cfset Variables.ListFieldsetsSkip							= LCase(Variables.ListFieldsetsSkip)><!--- (No ListFindNoCase) --->
<cfloop index="fieldset" list="#Variables.ListFieldsetsAll#">
	<cfif	(ListFind(Variables.ListFieldsetsShow, fieldset) gt 0)
		and	(ListFind(Variables.ListFieldsetsSkip, fieldset) is 0)>
		<cfif FileExists(ExpandPath("#	Variables.LibIncURL#/userprofile/dsp_userprofile.#fieldset#.cfm"))>
			<cfinclude template="#		Variables.LibIncURL#/userprofile/dsp_userprofile.#fieldset#.cfm">
		<cfelse>
			<cfsavecontent variable="Variables.AppDataInline">
				<cfoutput>#Variables.AppDataInline#
<p>
	NOTE: dsp_userprofile.#fieldset#.cfm doesn't exist yet, so it couldn't be included (obviously). 
</p>
</cfoutput>
			</cfsavecontent>
		</cfif><!--- /FileExists --->
	</cfif><!--- /ListFinds --->
</cfloop><!--- /ListFieldsetsAll --->

<!--- ************************************************************************************************************ --->

<!---
FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS 
FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS 
FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS 
FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS FOOTERS 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#<!--- Probably last append, but caller could append more, if it wants to. --->
<p align="center"></cfoutput>
	<cfif Variables.ReadOnlyProfile>
		<cfif Variables.PageIsProfile>
			<cfoutput>
  When done, press the "Choose Function" button at top of page (if available). 
</cfoutput>
		</cfif>
		<cfif Variables.PageIsUser>
			<cfoutput>
  When done, use the left-side navigation tree to go elsewhere in the Security System, 
  or the "Exit" button at top of page to return to Choose Function. 
</cfoutput>
		</cfif>
	<cfelse>
		<cfif Variables.PageIsAddCustomer>					<cfset Variables.CancelPage = "dsp_login.cfm">
		<cfelseif Variables.PageIsUser>						<cfset Variables.CancelPage = "dsp_search.cfm">
		<cfelse>											<cfset Variables.CancelPage = "dsp_choosefunction.cfm"></cfif>'
		<!--- Also, act pages go to same destinations in case JavaScript is off, but button value MUST be "Cancel"!!! --->
		<cfoutput>
  <!-- Define buttons in "safest to most destructive" order: -->
  <input type="Submit" name="SubmitButton" value="Submit" title="Submit form.">
  <input type="Reset"  name="ResetButton"  value="Reset"  title="Reset values to most recent load of this page.">
  <input type="Submit" name="SubmitButton" value="Cancel" title="Discard changes." onClick="
  this.form.action = '#Variables.CancelPage#';
  this.form.method = 'get';</cfoutput>
		<!--- Don't let "required" fields keep this Cancel button from working: --->
		<cfif Len(Variables.ListAllReqdFields) gt 0>
			<cfloop index="Variables.ColName" list="#Variables.ListAllReqdFields#">
				<cfoutput>
  this.form.#Variables.ColName#.onchange = false;</cfoutput>
			</cfloop>
		</cfif>
		<cfoutput>
  return true;
  "></cfoutput>
	</cfif><!--- /ReadOnlyProfile --->
	<cfoutput>
</p></cfoutput>
	<cfif NOT Variables.ReadOnlyProfile>
		<cfoutput>
</form><!-- /#Variables.FormName# -->
</cfoutput>
	</cfif>
	<cfoutput>
</div><!-- /pad10 --><!--- From start of unconditonal header. --->
</cfoutput>
</cfsavecontent><!--- /AppDataInline, unconditional footer --->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#</cfoutput>
	<cfif NOT Variables.ReadOnlyProfile>
		<cfoutput>
<script>

function DoThisOnSubmit										(pThis)
	{<cfif Len(Variables.ListAllReqdFields) gt 0><cfloop index="Variables.ColName" list="#Variables.ListAllReqdFields#">
	if	(pThis.#Variables.ColName# && pThis.#Variables.ColName#.onchange)
		if	(!pThis.#Variables.ColName#.onchange())
			return false;</cfloop></cfif>
	return true;
	}

</script>
</cfoutput>
	</cfif><!--- /ReadOnlyProfile --->
</cfsavecontent>

<!--- ************************************************************************************************************ --->

<!---
Save off Variables.Origs.ArrayColsOnDspPage. 

In our standard model for dsp page and act page interatction, FormDataRecovered and IsDefined("URL.PrevErr") are not true 
on initial page load, but true on subsequent returns to the page due to validation errors in the act page. So we set up a 
logic variable DisplayingFormDataFromDatabase based on those conditions and call PutDspPageLoadTimeOrigs() when 
DisplayingFormDataFromDatabase. Makes sense so far, but why are we doing so at the very END of this file? 

The answer is that the Defaults UDF continues to append to Variables.Origs.ArrayColsOnDspPage, a special array to aid coding 
the act page. We aren't really done calling LoadOrig (or its local surrogate, LoadBUPI) until we're done including all the 
fieldset files, in other words. 

CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP 
CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP 
CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP 
CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP 
--->

<cfif Variables.DisplayingFormDataFromDatabase><!--- Original page load? --->
	<cfset PutDspPageLoadTimeOrigs()> <!--- Yes: Save Variables.Origs to Session after all LoadOrigs calls (for sure). --->
</cfif>

<!--- ************************************************************************************************************ --->

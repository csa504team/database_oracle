<!---
AUTHOR:				Steve Seaquist
DATE:				05/01-28/2002
DESCRIPTION:		Saves all form data to a session variable. 
NOTES:				Called by act files when returning to the dsp due to presave validation. 
INPUT:				Form data. 
OUTPUT:				Session variable containing form data. 
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	05/01-28/2002, SRS:	Original implementation.
--->

<CFLOCK SCOPE="Session" TYPE="Exclusive" TIMEOUT="30">
	<CFIF IsDefined("Session.SaveForm")>
		<CFIF StructClear(Session.SaveForm)></CFIF>
	<CFELSE>
		<CFSET Session.SaveForm					= StructNew()>
	</CFIF>
	<CFLOOP INDEX="FNam" LIST="#StructKeyList(Form)#">
		<CFSET "Session.SaveForm.#FNam#"		= Evaluate("Form.#FNam#")>
	</CFLOOP>
	<CFSET Session.SaveForm.ErrMsg				= Variables.ErrMsg>

</CFLOCK>

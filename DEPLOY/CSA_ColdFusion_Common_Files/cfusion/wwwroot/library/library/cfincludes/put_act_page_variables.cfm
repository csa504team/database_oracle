<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				06/18/2008
DESCRIPTION:		Shared routine to save Variables scope data and processing time. Always saves processing time. 
					Crashes if called by a Web Service or by a Web page with no Session scope. But never crashes if 
					called by a Web page that has Session scope. (Saves 0 as the processing time if inc_starttickcount 
					wasn't included earlier.) 
NOTES:				Cfinclude this page at the end of a validation page just before the cflocation to the update page. 
INPUT:				Variables scope data. 
OUTPUT:				Session.SlafActPageVariables. 
REVISION HISTORY:	06/18/2008, SRS:	Original Implementation. 
--->

<cfif NOT IsDefined("Variables.SlafPrevTickCounts")>
	<cfset Variables.SlafPrevTickCounts				= 0>
</cfif>
<cfset Variables.SlafActPageTickCount				= -1><!--- -1 = "tick count unavailable in act page". --->
<cfif IsDefined("Request.RequestStartTickCount")>
	<cfset Variables.SlafActPageTickCount			= (GetTickCount() - Request.RequestStartTickCount)>
	<cfset Variables.SlafPrevTickCounts				= Variables.SlafPrevTickCounts
													+ Variables.SlafActPageTickCount>
</cfif>
<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfif IsDefined("Session.SlafActPageVariables")>
		<!--- Throw away garbage from previous calls to this routine: --->
		<cfset StructClear(Session.SlafActPageVariables)>
	</cfif>
	<cfset Session.SlafActPageVariables				= Duplicate(Variables)>
</cflock>

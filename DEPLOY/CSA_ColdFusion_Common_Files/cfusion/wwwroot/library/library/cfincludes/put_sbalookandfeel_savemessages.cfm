<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Saves just Commentary and ErrMsg to the Session scope. Use in "form data recovery". 
NOTES:				Called by put_sbalookandfeel_data.cfm, but also can be called directly by users of Automatic Form Data. 
					Recovery. DO NOT INCLUDE THIS FILE FROM WITHIN ANY SESSION LOCK. To do so would result in nested locks of 
					the Session scope, which causes a deadlock. Instead use the "_nolock" version. 
INPUT:				Variables.Commentary, Variables.ErrMsg. 
OUTPUT:				Variables in the Session scope. 
REVISION HISTORY:	09/28/2005, SRS:	Original implementation. 
--->

<cflock scope="Session" type="Exclusive" timeout="30">
	<cfinclude template="put_sbalookandfeel_savemessages_nolock.cfm">
</cflock>

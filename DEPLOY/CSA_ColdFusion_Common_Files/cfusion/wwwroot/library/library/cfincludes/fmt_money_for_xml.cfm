<!---
AUTHOR:				Steve Seaquist
DATE:				04/03/2001
DESCRIPTION:		Formats QName.FName as <XName>value</XName>, with value formatted for XML. 
NOTES:				Same as fmt_for_xml, except that it assumes the value is money, and doesn't output the XML if null.
INPUT:				Variables.QName, Variables.FName, Variables.XName, Variables.Indent, Variables.CRLF, Variables.Idx
OUTPUT:				Variables.XmlErrDetail, Variables.XmlString
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	04/03/2001, SRS:	Original implementation.
--->
<CFIF IsDefined("#Variables.QName#.#Variables.FName#")>
	<CFSET Variables.FValue						= Evaluate("#Variables.QName#.#Variables.FName#[1]")>
	<CFIF Len(Variables.FValue) GT 0>
		<CFSET Variables.FValue					= Replace(NumberFormat(Variables.FValue),	",","", "ALL")>	<!--- (Strip cents) --->
		<CFSET Variables.XmlString				= Variables.XmlString 
												& Variables.Indent
												& "<#Variables.XName#>#Variables.FValue#</#Variables.XName#>"
												& Variables.CRLF>
	</CFIF>
<CFELSE>
	<CFSET Variables.XmlErrDetail				= Variables.XmlErrDetail 
												& "#Variables.QName#.#Variables.FName# not defined."
												& Chr(13) & Chr(10)>
</CFIF>

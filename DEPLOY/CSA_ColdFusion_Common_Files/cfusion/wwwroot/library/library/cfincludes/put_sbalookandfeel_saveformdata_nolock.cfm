<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Saves the Form scope to the Session scope. Used in "form data recovery". 
NOTES:				Assumes a Session lock is active. This MUST be an Exclusive lock. 
OUTPUT:				Variables in the Session scope. 
REVISION HISTORY:	06/16/2010, SRS:	Instead of cfloop, use Duplicate(Form) for speed improvements, now that it works. 
					09/28/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Session.SlafSaveFormData")>
	<cfset StructClear(Session.SlafSaveFormData)><!--- Let Java clean up its now-unreferenced contents. --->
</cfif>
<cfset Session.SlafSaveFormData			= Duplicate(Form)>
<cfinclude template="put_sbalookandfeel_savemessages_nolock.cfm">

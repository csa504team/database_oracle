<!---
AUTHOR:				Greg Eoyang. 
DATE:				08/18/2003. 
DESCRIPTION:		Calls system routine to change password. 
NOTES:				The input and output variable names are the ancient /elend/ws/etran.cfc interface. (Original implementation.) 
INPUT:				Variables scope:	OrigUserId, OrigPassword, NewPassword. 
OUTPUT:				Variables scope:	SLevel, EtranErrMsg. 
REVISION HISTORY:	02/02/2017, SMJ:	(OPSMDEV-JAME) Fixed a couple of serious regressions introduced by the earlier change.
					01/03/2017, SMJ:	(OPSMDEV-1271) Added a check to prevent already set wbs_login.cfc variables from being overwritten.
					06/17/2015, SRS:	Got rid of JBoss in favor of the Login Web Service's LoginPasswordNew feature. Moved code 
										to /library/cfincludes to be shared across all PasswordUpdate web services. 
					12/29/2008, NNI:	Modified Java object instantiation to use library code. 
					10/24/2003,    :	Modified to account for improper call with CFPARM 
										also allow for password warning messages error 31. 
					08/20/2003, GRG:	Implement Web Services logic. 
--->

<!--- Configuration Parameters: --->

<cfset Variables.LibURL					= "/library">
<cfset Variables.LibIncURL				= "#Variables.LibURL#/cfincludes">

<!--- Initialize ancient interface to etran.cfc: --->

<cfset Variables.SLevel					= 99>
<cfset Variables.EtranErrMsg			= "Status variables not changed from their original values.">
<cfparam name="Variables.OrigUserId"	default="">
<cfparam name="Variables.OrigPassword"	default="">
<cfparam name="Variables.newpassword"	default="">

<!--- Initialize modern interface to wbs_login.cfc: --->

<cfif not IsDefined("Variables.LoginType")>
	<cfset Variables.LoginType		= "C" />
</cfif>

<cfif not IsDefined("Variables.LoginUsername")>
	<cfset Variables.LoginUsername		= Variables.OrigUserId>
</cfif>

<cfif not IsDefined("Variables.LoginPassword")>
	<cfset Variables.LoginPassword		= Variables.OrigPassword>
</cfif>

<cfif not IsDefined("Variables.LoginPasswordNew")>
	<cfset Variables.LoginPasswordNew	= Variables.NewPassword>
</cfif>

<cfif not IsDefined("Variables.LoginRoles")>
	<cfset Variables.LoginRoles			= "">
</cfif>

<cfset Variables.CLS.CLSAuthorized		= "No">
<cfset Variables.CLS.CLSErrMsg			= "Error message not set by login system.">

<!---
We can't use /library/cfincludes/act_login_web_service.cfm, becuase it only knows how to cfinvoke Login Web Service's "Login" 
method, which doesn't have a cfinvokeargument for LoginPasswordNew. This cfinclude-only technique will have to do until we 
expand /library/cfincludes/act_login_web_service.cfm to know how to call "Login_Other_String" or "Login_Other_Struct":  
--->

<cfif NOT IsDefined("Request.SlafLoginLocally")>
	<cfinclude template="#Variables.LibIncURL#/get_sbashared_variables.cfm">
</cfif>
<cfinclude template="#Request.SlafLoginInclude#">

<!--- Pass back whatever the login web service said: --->

<cfif Variables.CLS.CLSAuthorized>
	<cfset Variables.SLevel				= 0>
	<cfset Variables.EtranErrMsg		= "Password change successful.">
	<cfif Len(Variables.CLS.CLSChgPassMsg) gt 0>
		<cfset Variables.SLevel			= 31>
		<cfset Variables.EtranErrMsg	&= " "
										& Variables.CLS.CLSChgPassMsg>
	</cfif>
<cfelse>
	<cfset Variables.SLevel				= 99>
	<cfset Variables.EtranErrMsg		= Variables.CLS.CLSErrMsg>
</cfif>

<!---
AUTHOR:             Sheen Mathew Justin, TMS Inc., for the US Small Business Administration.
DATE:               12/21/2015
DESCRIPTION:        Display splash graphic for application based on input parameters
NOTES:				      None.
INPUT:              6 Parameters below.
OUTPUT:             Splash graphic.
REVISION HISTORY:   08/18/2015, SMJ:	Original implementation.
--->

<!--- Input Parameters --->
<cfparam name = "SystemName"           default = "SBA" />
<cfparam name = "SubSystemName"        default = "Security" />

<cfparam name = "TopLeftBGColor"       default = "##f1f1f1" />
<cfparam name = "BottomRightBGColor"   default = "##205493" />

<cfparam name = "TopLeftTextColor"     default = "##205493" />
<cfparam name = "BottomRightTextColor" default = "##f1f1f1" />

<cfparam name = "BottomRightBGColorTh2"   default = "##7086A9" />
<cfparam name = "TopLeftTextColorTh2"     default = "##7086A9" />

<cfparam name = "BottomRightBGColorTh3"   default = "##FAC" />
<cfparam name = "TopLeftTextColorTh3"     default = "##FAC" />

<cfparam name = "BottomRightBGColorTh4"   default = "##AFC" />
<cfparam name = "TopLeftTextColorTh4"     default = "##AFC" />

<cfparam name = "BottomRightBGColorTh5"   default = "##ACF" />
<cfparam name = "TopLeftTextColorTh5"     default = "##ACF" />

<cfparam name = "BottomRightBGColorTh6"   default = "##000" />
<cfparam name = "TopLeftTextColorTh6"     default = "##000" />

<cfparam name = "TopLeftTextColorBlack"     default = "##000" />
<cfparam name = "BottomRightTextColorBlack" default = "##000" />
<cfoutput>

	<!--- Set Styling --->
	<style>

		##container {
			border: 2px solid ##333;
			border-radius: 2px;
			height: 140px;
			width: 220px;
			box-shadow: 1px 2px 5px ##333;
			font-size: 15px;
		}

		##triangle-topleft {
		  width: 0;
		  height: 0;
		  border-top: 140px solid #TopLeftBGColor#;
		  border-right: 220px solid transparent;
		}
		
		##triangle-bottomright {
			height: 140px;
			width: 220px;
			overflow: hidden;
			background-color: #BottomRightBGColor#;
			position: relative;
		}
		
		##SystemName{
			position: absolute;
			top: 1em;
			left: 1em;
			color: #TopLeftTextColor#;
			max-width: 100px;
		}

		 ##SubSystemName{
			position: absolute;
			bottom: 0;
			right: 0;
			color: #BottomRightTextColor#;
			max-width: 100px;
			
		}
		
		body.th2 ##triangle-bottomright {
			background-color: #BottomRightBGColorTh2#;
		}

		body.th2 ##SystemName{
			color: #TopLeftTextColorTh2#;
		}

		body.th3 ##triangle-bottomright {
			background-color: #BottomRightBGColorTh3#;
		}

		body.th3 ##SystemName{
			color: #TopLeftTextColorBlack#;
		}
		
		body.th3 ##SubSystemName{
			color: #BottomRightTextColorBlack#;
		}
		
		body.th4 ##triangle-bottomright {
			background-color: #BottomRightBGColorTh4#;
		}

		body.th4 ##SystemName{
			color: #TopLeftTextColorTh4#;
		}
		
		body.th4 ##SystemName{
			color: #TopLeftTextColorBlack#;
		}
		
		body.th4 ##SubSystemName{
			color: #BottomRightTextColorBlack#;
		}
		
		body.th5 ##triangle-bottomright {
			background-color: #BottomRightBGColorTh5#;
		}

		body.th5 ##SystemName{
			color: #TopLeftTextColorTh5#;
		}
		
		body.th5 ##SystemName{
			color: #TopLeftTextColorBlack#;
		}
		
		body.th5 ##SubSystemName{
			color: #BottomRightTextColorBlack#;
		}
		
		body.th6 ##triangle-bottomright {
			background-color: #BottomRightBGColorTh6#;
		}

		body.th6 ##SystemName{
			color: #TopLeftTextColorTh6#;
		}
	</style>

	<!--- HTML --->
	<div id = "container">

		<div id = "triangle-bottomright">

			<div id = "triangle-topleft"></div>

			<div id = "SystemName">
				#SystemName#
			</div>

			<div id = "SubSystemName">
				#SubSystemName#
			</div>

		</div>

	</div>

</cfoutput>

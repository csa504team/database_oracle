<!--- Called by Custom Tags cf_sbalookandfeel and cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->
<!--- Everything derives from the Alt attributes. --->
<cfset Variables.ListStdAlts						= "Reports,"
													& "E-Tran,"
													& "Search,"
													& "Admin,"
													& "New Application,"
													& "Copy,"
													& "Exit,"
													& "Help">
<cfset Variables.ListStdButtons						= Replace(Replace(Variables.ListStdAlts," ","","ALL"),"-","","ALL")>
<cfset Variables.ListStdImages						= LCase(Variables.ListStdButtons)>

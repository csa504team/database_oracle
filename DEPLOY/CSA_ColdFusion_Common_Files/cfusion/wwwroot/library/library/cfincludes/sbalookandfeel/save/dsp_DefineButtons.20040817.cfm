<!--- Called by Custom Tag cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->

<cfoutput><!--- GetResolution(this.SlafResolution, this.SlafClientHeight, this.SlafClientWidth); --->
<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onSubmit="return false;">
<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) -->
<input type="Hidden" name="TextOnly"			value="#Attributes.TextOnly#"><!--- 
<input type="Hidden" name="SlafResolution"		value="1024">
<input type="Hidden" name="SlafClientHeight"	value="">
<input type="Hidden" name="SlafClientWidth"		value=""> ---><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>
<table border="0" cellpadding="0" cellspacing="0">
<tr></cfoutput>

<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListStdButtons#"><!--- Defined in dsp_DefineJavaScriptsForButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowButtons, Button)>
	<cfif Found GT 0>
		<cfset Variables.Alt						= ListGetAt	(Variables.ListStdAlts,		CurrIdx)>
		<cfif Attributes.TextOnly>
			<cfoutput>
	<td>&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</td></cfoutput>
		<cfelse>
			<cfset Variables.Image					= ListGetAt(Variables.ListStdImages,	CurrIdx)>
			<cfset Variables.FilenameLo				= "#Attributes.LibURL#/images/#SelfName#/#Image#_lo.gif">
			<cfset Variables.FilenameHi				= "#Attributes.LibURL#/images/#SelfName#/#Image#_hi.gif">
			<cfoutput>
	<td><!--- Height and width now defined in mainnav. Makes it possible to test/override configuration if buttons change. --->
		<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#" border="0"
			src="#FilenameLo#" alt="#Alt#" title="#Alt#" value="#Image#"
			onMouseOver="if (gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
			onmouseout="if (gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
			onfocus="this.onmouseover();"
			onblur="this.onmouseout();"
			onclick="Do#Button#(this.form);">
	</td></cfoutput>
			<!--- (Note: The title attribute is to show tooltips in Netscape. All you need is the alt attribute for MSIE.) --->
		</cfif>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowButtons#"><!--- Defined in dsp_DefineJavaScriptsForButtons.cfm --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdButtons, Button)>
	<cfif Found IS 0>
		<cfset Variables.Alt						= ListGetAt(Variables.ListShowAlts,		CurrIdx)>
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages,	CurrIdx)>
		<cfset Variables.FilenameLo					= "#Attributes.LibURL#/images/applookandfeel/#Image#_lo.gif">
		<cfset Variables.FilenameHi					= "#Attributes.LibURL#/images/applookandfeel/#Image#_hi.gif">
		<cfif NOT Variables.FirstNonStdButtonSeen>
			<cfoutput>
</tr>
<tr></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfif Attributes.TextOnly>
			<cfoutput>
	<td>&nbsp;<a href="javascript:Do#Button#(document.FormMainNav);" class="menuitem">#Alt#</a>&nbsp;</td></cfoutput>
		<cfelse>
			<cfoutput>
	<td>
		<input type="Image" name="SubmitButton" height="#Variables.ImageHeight#" width="#Variables.ImageWidth#" border="0"
			src="#FilenameLo#" alt="#Alt#" title="#Alt#" value="#Image#"
			onMouseOver="if (gThisPageIsFullyLoaded) this.src=g#Button#Hi.src;"
			onmouseout="if (gThisPageIsFullyLoaded) this.src=g#Button#Lo.src;"
			onfocus="this.onmouseover();"
			onblur="this.onmouseout();"
			onclick="Do#Button#(this.form);">
	</td></cfoutput>
		</cfif>
	</cfif>
</cfloop>

<cfoutput>
</tr>
</table>
</form></cfoutput>

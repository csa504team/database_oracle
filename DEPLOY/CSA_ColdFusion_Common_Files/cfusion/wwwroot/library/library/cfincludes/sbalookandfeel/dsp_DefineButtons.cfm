<!---
AUTHOR:				Steve Seaquist. 
DATE:				unknown. 
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:

	Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory. 

	The following takes some explanation. In Netscape 4, a div with absolute or relative positioning is rendered using 
	layers. That results in Netscape treating the contents of the div as if it were separate document. Therefore, when 
	MainNav is inline, you can't just reference gThisPageIsFullyLoaded (for example), you have to reference 
	top.gThisPageIsFullyLoaded to get to the topmost document containing the div. In all other browsers, the "top." 
	part is unnecessary, but doesn't hurt anything. 

INPUT:				Attributes-scope and Variables-scope configuration variables. 
OUTPUT:				Buttons and/or Menus on the web page (regardless of inline or frame). 
REVISION HISTORY:	12/28/2012, SRS:	If Request.SlafMainNavMenus is defined, use it to define a menu bar at the bottom 
										of MainNav. Anything that's handled by the Request.SlafMainNavMenus structure will 
										no longer get a button because it will already be on the page in (or as) a menu. 
					06/29/2012, SRS:	Made divmainnavsubmit divs use inlineblock, so that it can vary according to MSIE 8+ 
										boundary, not MSIE 10+ boundary. (Solves "the margin-bottom problem".) 
					07/20/2011, SRS:	Adapted to Request.SlafShowButtonOverrides. (See /library/udf/bld_SlafUDFs.cfm.) 
										Renamed lists so that the logic would be more comprehensible/readable. 
					11/09/2010, SRS:	Now that CSS buttons are proven to work, made code more efficient by moving hover 
										JavaScript to mainnav.js. 
					09/29/2010, SRS:	Section 508: Added role="button" (WAI-ARIA explanation) to pseudo-button hotlinks 
										only. Indented buttons using padding-left, not using a spacer div, so that if the 
										buttons wrap, they wrap indented. Got rid of submit buttons, which weren't being 
										used to submit the form anyway. (Hotlinks are more consistent and maintainable.) 
					09/08/2010, SRS:	Section 508: Made MSIE links tabbable. 
					07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation. 
--->

<cfif NOT IsDefined("ShowButtonHasOverrides")>
	<cfinclude template="/library/udf/bld_SlafUDFs.cfm"><!--- Defines ShowButtonHasOverrides(), and ShowButtonInMainNavMenu(). --->
</cfif>
<cfparam name="Variables.DoingMainNavMenus"			default="No">

<cfif Attributes.InFrame>							<cfset Variables.FrameContainingDoScripts	= "">
<cfelse>											<cfset Variables.FrameContainingDoScripts	= "top."></cfif>
<cfif	(Len(Variables.ListShowBtnsPacked)			gt 120)
	or	(ListLen(Variables.ListShowBtnsPacked)		gt 12)>
	<!--- Although it looks good at 9pt, text-shadow looks really bad at 7pt: --->
	<cfset Variables.ImageStyle						&= " font-size:8pt !important; text-shadow:none !important;">
</cfif>

<cfoutput>#Request.SlafEOL#
#indent#<form name="FormMainNav" action="#Attributes.ActionURL#" method="post" onsubmit="return false;">
#indent#<!-- (OnSubmit returns false to avoid unnecessary server hit if JavaScript is on.) --><cfif Len(Attributes.MainNavHiddens) GT 0>
#Attributes.MainNavHiddens#</cfif>#Request.SlafEOL#
#indent#<div class="inlineblock" style="#Variables.SkipOverAppName#"></cfoutput>

<cfset Variables.FirstStdButtonSeen					= "No">
<cfset Variables.CurrIdx							= 0><!--- Reset to 0 later. --->
<cfloop index="Button" list="#Variables.ListStdBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind	(Variables.ListShowBtnsPacked, Button)><!--- Also packed. --->
	<cfif Found GT 0><!--- One of the standard buttons. --->
		<cfset Variables.FirstStdButtonSeen			= "Yes">
		<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListStdBtnsFull,	CurrIdx)><!--- Full name. --->
		<cfif ShowButtonIsInMainNavMenu(Variables.CanonicalName, "No")><!--- "No" = "Just tell us if we shouldn't image the button". --->
			<cfcontinue><!--- Skip this Button of the ListStdBtnsPacked cfloop, because it's in a MainNavMenu. --->
		</cfif>
		<cfset Variables.href						= "javascript:"
													& Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav);">
		<cfset Variables.pseudobutton				= "mainnavsubmit">
		<cfset Variables.target						= "">
		<cfset Variables.title						= Variables.CanonicalName>
		<cfif ShowButtonHasOverrides(Button)>
			<cfif IsDefined("Variables.Overrides.href")>
				<cfset Variables.href				= Variables.Overrides.href>
			</cfif>
			<cfif IsDefined("Variables.Overrides.external")
				and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
				<cfset Variables.pseudobutton		= "mainnavexternalsys">
				<cfif IsDefined("Variables.Overrides.target")>
					<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
				<cfelse>
					<cfset Variables.target			= " target=""_blank""">
				</cfif>
			<cfelseif IsDefined("Variables.Overrides.target")>
				<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
			</cfif>
			<cfif IsDefined("Variables.Overrides.title")>
				<cfset Variables.title				= Variables.Overrides.title>
			</cfif>
		</cfif>
		<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit inlineblock"><a class="#Variables.pseudobutton#"#Variables.target# href="#Variables.href#"
#indent#	role="button" style="#ImageStyle#" title="#Variables.title#">#Variables.CanonicalName#</a></div></cfoutput>
	</cfif>
</cfloop>

<cfset Variables.FirstNonStdButtonSeen				= "No">
<cfset Variables.CurrIdx							= 0>
<cfloop index="Button" list="#Variables.ListShowBtnsPacked#"><!--- Packed name, trimmed of " " and "-". --->
	<cfset Variables.CurrIdx						= Variables.CurrIdx + 1>
	<cfset Found									= ListFind(Variables.ListStdBtnsPacked, Button)><!--- Also packed. --->
	<cfif Found IS 0><!--- NOT one of the standard buttons, since the standard buttons row has already been defined. --->
		<cfset Variables.CanonicalName				= ListGetAt	(Variables.ListShowBtnsFull,	CurrIdx)><!--- Full name. --->
		<cfif ShowButtonIsInMainNavMenu(Variables.CanonicalName, "No")><!--- "No" = "Just tell us if we shouldn't image the button". --->
			<cfcontinue><!--- Skip this Button of the ListShowBtnsPacked cfloop, because it's in a MainNavMenu. --->
		</cfif>
		<cfif CompareNoCase(Button, "br") is 0><!--- Special keyword that means to do a break for a new row of buttons. --->
			<cfoutput>#Request.SlafEOL#
#indent#<br/></cfoutput>
			<cfcontinue>
		</cfif>
		<cfset Variables.href						= "javascript:"
													& Variables.FrameContainingDoScripts
													& "Do#Button#(document.FormMainNav);">
		<cfset Variables.pseudobutton				= "mainnavsubmit">
		<cfset Variables.target						= "">
		<cfset Variables.title						= Variables.CanonicalName>
		<cfif ShowButtonHasOverrides(Button)>
			<cfif IsDefined("Variables.Overrides.href")>
				<cfset Variables.href				= Variables.Overrides.href>
			</cfif>
			<cfif IsDefined("Variables.Overrides.external")
				and	(Variables.Overrides.external)><!--- Value must also be "Yes", "true" or equivalent. --->
				<cfset Variables.pseudobutton		= "mainnavexternalsys">
				<cfif IsDefined("Variables.Overrides.target")>
					<cfset Variables.target			= " target=""#Variables.Overrides.target#""">
				<cfelse>
					<cfset Variables.target			= " target=""_blank""">
				</cfif>
			<cfelseif IsDefined("Variables.Overrides.target")>
				<cfset Variables.target				= " target=""#Variables.Overrides.target#""">
			</cfif>
			<cfif IsDefined("Variables.Overrides.title")>
				<cfset Variables.title				= Variables.Overrides.title>
			</cfif>
		</cfif>
		<cfif	Variables.FirstStdButtonSeen
			and	(NOT Variables.FirstNonStdButtonSeen)>
			<cfoutput><br/>
#indent#<!-- Second row --></cfoutput>
			<cfset Variables.FirstNonStdButtonSeen	= "Yes">
		</cfif>
		<cfoutput>#Request.SlafEOL#
#indent#<div class="divmainnavsubmit inlineblock"><a class="#Variables.pseudobutton#"#Variables.target# href="#Variables.href#"
#indent#	role="button" style="#ImageStyle#" title="#Variables.title#">#Variables.CanonicalName#</a></div></cfoutput>
	</cfif>
</cfloop>

<cfset Variables.SetUpRecursion						= "No">
<cfif Variables.DoingMainNavMenus>
	<cfoutput>
#indent#<div id="DivMainNavMenu" class="tbl">
#indent#  <div class="tbl_row">
#indent#    <div class="tbl_cell first"></div></cfoutput>
	<cfloop index="i" from="1" to="#ArrayLen(Variables.MainNavMenuNames)#">
		<cfset Variables.StructMenu					= Variables.MainNavMenuNames[i]>
		<cfswitch expression="#Variables.StructMenu.type#"><!--- Crash (on purpose) if not defined. --->
		<cfcase value="link">
			<cfoutput>
#indent#    <label for="#Variables.StructMenu.menu#" class="link"></cfoutput>
			<cfif StructKeyExists(Variables.StructMenu, "DoScript")>
				<!--- Padding doesn't look right in this situation. Use &nbsp; (and text-decoration:none) instead: --->
				<cfoutput>
#indent#    <a href="javascript:#Variables.StructMenu.DoScript#">&nbsp;&nbsp;#Variables.StructMenu.menu#&nbsp;&nbsp;</a></cfoutput>
			<cfelseif StructKeyExists(Variables.StructMenu, "href")>
				<!--- Specifying target only makes sense if there's a non-"javascript:" href, so test for it only here. --->
				<cfif		StructKeyExists	(Variables.StructMenu, "target")>
					<cfset Variables.Target			= " target=""#Variables.StructMenu.target#""">
				<cfelseif	StructKeyExists	(Variables.StructMenu, "external")
					and		IsBoolean		(Variables.StructMenu.external)
					and						 Variables.StructMenu.external>
					<cfset Variables.Target			= " target=""_blank"""><!--- Externals pop up new windows. --->
				<cfelse>
					<cfset Variables.Target			= "">
				</cfif>
				<cfoutput>
#indent#      <a#Variables.Target# href="#Variables.StructMenu.href#">#Variables.StructMenu.menu#</a></cfoutput>
			<cfelse>
				<cfset Variables.DoScript			= "Do"
													& Replace(Replace(Variables.StructMenu.menu,"-","","All")," ","","All")
													& "(document.FormMainNav);">
				<cfoutput>
#indent#      <a href="javascript:#Variables.DoScript#">&nbsp;&nbsp;#Variables.StructMenu.menu#&nbsp;&nbsp;</a></cfoutput>
			</cfif>
			<cfoutput>
#indent#    </label></cfoutput>
		</cfcase>
		<cfcase value="menu">
			<cfoutput>
#indent#    <input  id="#Variables.StructMenu.menu#" class="menu" type="checkbox" name="MainNavMenuState" value="#i#" style="display:none;">
#indent#    <label for="#Variables.StructMenu.menu#" class="menu">
#indent#      &nbsp;&nbsp;#Variables.StructMenu.menu#&nbsp;&nbsp;
#indent#      <div class="menu"></cfoutput>
			<cfloop index="j" from="1" to="#ArrayLen(Variables.StructMenu.items)#">
				<cfset Variables.StructItem			= Variables.StructMenu.items[j]>
				<cfswitch expression="#Variables.StructItem.type#">
				<cfcase value="item">
					<cfif StructKeyExists(Variables.StructItem, "DoScript")>
						<cfoutput>
#indent#      <a class="menuitem" href="javascript:#Variables.StructItem.DoScript#">#Variables.StructItem.item#</a></cfoutput>
					<cfelseif StructKeyExists(Variables.StructItem, "href")>
						<!--- Specifying target only makes sense if there's a non-"javascript:" href, so test for it only here. --->
						<cfif		StructKeyExists	(Variables.StructItem, "target")>
							<cfset Variables.Target	= " target=""#Variables.StructItem.target#""">
						<cfelseif	StructKeyExists	(Variables.StructItem, "external")
							and		IsBoolean		(Variables.StructItem.external)
							and						 Variables.StructItem.external>
							<cfset Variables.Target	= " target=""_blank"""><!--- Externals pop up new windows. --->
						<cfelse>
							<cfset Variables.Target	= "">
						</cfif>
						<cfoutput>
#indent#      <a class="menuitem"#Variables.Target# href="#Variables.StructItem.href#">#Variables.StructItem.item#</a></cfoutput>
					<cfelse>
						<cfset Variables.DoScript	= "Do"
													& Replace(Replace(Variables.StructItem.item,"-","","All")," ","","All")
													& "(document.FormMainNav);">
						<cfoutput>
#indent#      <a class="menuitem" href="javascript:#Variables.DoScript#">#Variables.StructItem.item#</a></cfoutput>
					</cfif>
				</cfcase>
				<cfcase value="text">
					<cfoutput>
#indent#      #Variables.StructItem.item#</cfoutput>
				</cfcase>
				</cfswitch>
			</cfloop>
			<cfoutput>
#indent#      </div><!-- /menu items -->
#indent#    </label><!-- /menu control --></cfoutput>
		</cfcase>
		<cfcase value="page">
			<cfoutput>
#indent#    <input  id="#Variables.StructMenu.menu#" class="menu" type="checkbox" name="MainNavMenuState" value="#i#" style="display:none;">
#indent#    <label for="#Variables.StructMenu.menu#" class="menu">
#indent#      &nbsp;&nbsp;#Variables.StructMenu.menu#&nbsp;&nbsp;
#indent#      <div class="menu"><!--- (page segments) ---></div>
#indent#    </label><!-- /menu control --></cfoutput>
			<cfset Variables.SetUpRecursion			= "Yes">
		</cfcase>
		</cfswitch>
		<cfoutput>
#indent#    <div class="tbl_cell spacer"></div></cfoutput>
	</cfloop>
	<cfoutput>
#indent#  </div><!-- /tbl_row -->
#indent#</div><!-- /tbl = DivMainNavMenu --></cfoutput>
</cfif>

<cfif Variables.SetUpRecursion>
	<cfoutput>
#indent#<script>
#indent#function CallPageSegmentGrabberRecursively	()
#indent#	{</cfoutput>
	<cfloop index="i" from="1" to="#ArrayLen(Variables.MainNavMenuNames)#">
		<cfset Variables.StructMenu					= Variables.MainNavMenuNames[i]>
		<cfif Variables.StructMenu.type				is "page">
			<cfoutput>
#indent#	PageSegmentsGrabber(
#indent#		{
#indent#		url:										"#JSStringFormat(Variables.StructMenu.url)#",
#indent#		selector:									"##DivMainNavMenu label.menu[for='#Variables.StructMenu.menu#'] div.menu"
#indent#		});</cfoutput>
		</cfif>
	</cfloop>
	<cfoutput>
#indent#	}
#indent#</script></cfoutput>
</cfif>

<cfoutput>#Request.SlafEOL#
#indent#</div>
#indent#</form>
</cfoutput>

<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines JavaScripts, substituting /library/javascripts/sbalookandfeel defaults if files don't exist. 
NOTES:				Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.
INPUT:				Variables.ListShowBtnsPacked, Attributes.MainNavJSURL. 
OUTPUT:				Script tags. 
REVISION HISTORY:	12/03/2015, SRS:	Added optionally calling AppendCachedAsOf by defining Request.SlafMainNavCacheBuster. 
										Repeated this solution across all 3 situations in which we generate <script src="...">. 
					07/20/2011, SRS:	Adapted to Request.SlafShowButtonOverrides. (See /library/udf/bld_SlafUDFs.cfm.) 
										If button has scriptsrc override, use that instead. If button has href override, 
										and does NOT have scriptsrc overried, don't generate any script tag at all. 
					07/21/2010, SRS:	"New Look-and-Feel". Removed preload of /library/images jpegs. Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfparam name="Request.SlafMainNavCacheBuster"				default="No">
<cfif Request.SlafMainNavCacheBuster>
	<cfif NOT IsDefined("AppendCachedAsOf")>
		<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
	</cfif>
</cfif>
<cfif NOT IsDefined("ShowButtonHasOverrides")>
	<cfinclude template="/library/udf/bld_SlafUDFs.cfm">
</cfif>

<cfloop index="Button" list="#Variables.ListShowBtnsPacked#">
	<cfif ShowButtonHasOverrides(Button)>
		<cfif IsDefined("Variables.Overrides.scriptsrc")>
			<cfif Request.SlafMainNavCacheBuster>
				<cfoutput>
<script language="JavaScript" src="#AppendCachedAsOf(Variables.Overrides.scriptsrc)#"></script></cfoutput>
			<cfelse>
				<cfoutput>
<script language="JavaScript" src="#Variables.Overrides.scriptsrc#"></script></cfoutput>
			</cfif>
			<cfcontinue>
		<cfelseif IsDefined("Variables.Overrides.href")>
			<cfcontinue>
		</cfif>
	</cfif>
	<cfif	(ListFind(Variables.ListStdBtnsPacked, Button) GT 0)
		AND	(NOT FileExists(ExpandPath("#Attributes.MainNavJSURL#/Do#Button#.js")))>
		<!--- The /library/javascripts/sbalookandfeel/Do#Button#.js may not exist either, but at least we tried: --->
		<cfif Request.SlafMainNavCacheBuster>
			<cfoutput>
<script language="JavaScript" src="#AppendCachedAsOf(Attributes.LibURL & '/javascripts/sbalookandfeel/Do' & Button & '.js')#"></script></cfoutput>
		<cfelse>
			<cfoutput>
<script language="JavaScript" src="#Attributes.LibURL#/javascripts/sbalookandfeel/Do#Button#.js"></script></cfoutput>
		</cfif>
	<cfelse>
		<cfif Request.SlafMainNavCacheBuster>
			<cfoutput>
<script language="JavaScript" src="#AppendCachedAsOf(Attributes.MainNavJSURL & '/Do' & Button & '.js')#"></script></cfoutput>
		<cfelse>
			<cfoutput>
<script language="JavaScript" src="#Attributes.MainNavJSURL#/Do#Button#.js"></script></cfoutput>
		</cfif>
	</cfif>
</cfloop>

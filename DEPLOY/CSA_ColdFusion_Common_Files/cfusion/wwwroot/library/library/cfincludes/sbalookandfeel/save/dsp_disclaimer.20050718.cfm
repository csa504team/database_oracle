<CFOUTPUT><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML LANG="Eng"><HEAD>
 <link href="/library/css/sba.css" rel="stylesheet" type="text/css">
<TITLE>SBA Disclaimer</TITLE>

<!--- Begin unique HEAD code --->

<!--- End unique HEAD code --->
</HEAD>
<BODY background="/library/images/background.gif" LINK="Blue" VLINK="Maroon" ALINK="Red" TOPMARGIN=0>
<table border="0" width="770" cellpadding="0" cellspacing="1" align="center">
  <tr>
    <td colspan="2" background="/library/images/header_back_title.gif"><table border="0" cellpadding="0" cellspacing="0" width="760">
        <tr>
          <td><a href="##main" tabindex="1"><img src="/library/images/shim.gif" alt="Skip to the main content." height="4" width="4" border="0"></a><img src="/library/images/header_title.gif" alt="United States Small Business Administration" width="378" height="24" border="0"></td>
          <td align="right">&nbsp;</td>
        </tr>
    </table></td>
  </tr>

  <tr>
    <td width="196"><a href="http://www.sba.gov/"><img src="/library/images/header_sba_logo.gif" alt="SBA" width="196" height="55" border="0"></a></td>
    <td width="570"  valign="top" class="headernav"><!-- begin main navigation table -->
        <table border="0" cellpadding="0" cellspacing="0" width="570">
          <tr>
            <td><img src="/library/images/shim.gif" alt="for layout only" width="1" height="32" border="0"></td>
            <td align="right" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
          </tr>
          <tr>
            <td width="113" height="21"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td align="center"><!--DWLayoutEmptyCell-->&nbsp;</td>
          </tr>
      </table></td>
  </tr>
</table>

<table border="0" width="770" cellpadding="0" cellspacing="1" align="center">
  <tr>
  	<td class="titlehead" width="196" height="25" align="center">SBA Disclaimer</td>
	<td class="inthead"><table width="570"><tr>
	    <td class="inthead" width="440">&nbsp;<b>#DateFormat(Now(), "mm-dd-yyyy")#</b></td>
	    <td class="inthead" width="130"><div align="center"><a HREF="javascript:history.back()"><font color="white"><b>Return</b></font></a></div></td>
	</tr></table></td>
  </tr>
 
  <tr><td colspan=2>
		<CENTER><table BORDER=3 SUMMARY="Warning Message">
			<TR><TD class="errormsg">
		          <h1>Warning!</h1></TD></TR>
		</TABLE></CENTER>
  </td></tr>
  <tr><td  colspan=2 topclass="subnavcol">
<p>This is a Federal system and is the property of the United States Government. It is for authorized use only.</p>
<ul type=disc>
 <li>Users (authorized or unauthorized) have no explicit or implicit expectation of privacy in anything viewed, created, 
	downloaded, or stored in this system, including e-mail. Any or all uses of this system (including output media) and 
	all usage of this system may be intercepted, monitored, read, captured, recorded, disclosed, copied, audited, and/or 
	inspected by authorized Small Business Administration (SBA) personnel, the Office of Inspector General (OIG), and/or 
	other law enforcement personnel, as well as authorized officials of other agencies, both domestic and foreign.</li>
<br><br><li>Access or use of this system by any person, whether authorized or unauthorized, constitutes consent to such interception, 
	monitoring, reading, capturing, recording, disclosure, copying, auditing, and/or inspection at the discretion of authorized 
	SBA personnel, law enforcement personnel (including the OIG), and/or authorized officials of other agencies, both domestic 
	and foreign.</li>
 <br><br><li>Unauthorized use of, or exceeding authorized access to, this system is prohibited and may constitute a violation 
	of 18 U.S.C. � 1030 or other Federal laws and regulations and may result in criminal, civil, and/or administrative action.
	By using this system, users indicate awareness of, and consent to, these terms and conditions and acknowledge that there is no
	reasonable expectation of privacy in the access or use of this system.</li>
</ul>
</td></tr></table>
<!--- End unique BODY code --->
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><FONT size=-1><EM><cf_lastmodified></EM></FONT><br>
        <table border="0" cellpadding="0" cellspacing="0" width="760">
          <tr>
            <td></td>
            <td align="center" class="footlinks">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="52"></td>
            <td align="center" class="footlinks">
				<a href="http://www.sba.gov/">Home</a> |
				<a href="http://www.sba.gov/ada/">Accessibility</a> |
				<a href="http://www.sba.gov/privacy.html">Privacy & Security</a> |
				<a href="http://www.sba.gov/foia/">FOIA</a> |
				<a href="http://www.sba.gov/asksba/">Ask SBA</a> |
				<a href="http://app1.sba.gov/faqs/">FAQ</a> |
				<a href="http://app1.sba.gov/glossary/">Glossary</a> |
				<a href="mailto:Stephen.Kucharski@sba.gov">Comments</a> <br>
                <img src="/library/images/shim.gif" alt="for layout only" width="1" height="10" border="0">
                <div class="footnote" align="center"></div></td>
            <td width="52">&nbsp;</td>
          </tr>
      </table></td>
  </tr>
</table>

</body>
</html>
</CFOUTPUT> 



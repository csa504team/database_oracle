<cfoutput>
<!--- Keep in sync with HTML definition of the "readylight" div or image (also "Loading"), in mainnav.cfm. --->
function SetReadyLightToLoading	()
{<cfif Attributes.TextOnly OR "Yes"><!--- Graphics look crappy in new look-and-feel. Now always using text for ReadyLight. --->
var	sReadyLight					= document.getElementById("readylight");
sReadyLight.innerHTML			= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td style="background-color:##ffffff;" align="center" valign="middle">'
								+ '<span title="Please wait. Parts of this page are still loading.">Loading</span>'
								+ '</td></tr>'
								+ '</table>';<cfelse>
document.readylight.alt			= "Please wait. Parts of this page are still loading.";
document.readylight.src			= "#Attributes.LibURL#/images/#SelfName#/loading.gif";</cfif><!--- TextOnly --->
}

function SetReadyLightToReady	()
{<cfif Attributes.TextOnly OR "Yes"><!--- Graphics look crappy in new look-and-feel. Now always using text for ReadyLight. --->
var	sReadyLight					= document.getElementById("readylight");
sReadyLight.innerHTML			= '<table border="0" cellpadding="0" cellspacing="0" height="25" width="60">'
								+ '<tr><td style="background-color:##bbe1ea;" align="center" valign="middle">'
								+ '<span style="color:##90d8a9; font-weight:bold;" title="Ready. Page is fully loaded.">Ready</span>'
								+ '</td></tr>'
								+ '</table>';<cfelse>
document.readylight.alt			= "Ready. Page is fully loaded.";
document.readylight.src			= "#Attributes.LibURL#/images/#SelfName#/ready_azure.gif";</cfif><!--- TextOnly --->
}
</cfoutput>

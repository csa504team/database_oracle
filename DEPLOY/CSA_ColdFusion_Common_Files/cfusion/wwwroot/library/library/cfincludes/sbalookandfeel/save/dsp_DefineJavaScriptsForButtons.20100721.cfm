<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines JavaScripts, substituting /library/javascripts/sbalookandfeel defaults if files don't exist. 
NOTES:				Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.
INPUT:				Variables.ListShowButtons, Attributes.MainNavJSURL. 
OUTPUT:				Script tags. 
REVISION HISTORY:	07/21/2010, SRS:	"New Look-and-Feel". Removed preload of /library/images jpegs. Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfloop index="Button" list="#Variables.ListShowButtons#">
	<cfif	(ListFind(Variables.ListStdButtons, Button) GT 0)
		AND	(NOT FileExists(ExpandPath("#Attributes.MainNavJSURL#/Do#Button#.js")))>
		<cfoutput><!--- The /library/javascripts/sbalookandfeel/Do#Button#.js may not exist either, but at least we tried: --->
<script language="JavaScript" src="#Attributes.LibURL#/javascripts/sbalookandfeel/Do#Button#.js"></script></cfoutput>
	<cfelse>
		<cfoutput>
<script language="JavaScript" src="#Attributes.MainNavJSURL#/Do#Button#.js"></script></cfoutput>
	</cfif>
</cfloop>

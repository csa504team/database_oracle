<!--- Called by Custom Tags cf_sbalookandfeel and cf_mainnav. Must be in the sbalookandfeelincludes subdirectory. --->

<cfloop index="Button" list="#Variables.ListShowButtons#">
	<cfif	(ListFind(Variables.ListStdButtons, Button) GT 0)
		AND	(NOT FileExists(ExpandPath("#Attributes.MainNavJSURL#/Do#Button#.js")))>
		<cfoutput>
<script language="JavaScript" src="#Attributes.LibURL#/javascripts/sbalookandfeel/Do#Button#.js"></script></cfoutput>
	<cfelse>
		<cfoutput>
<script language="JavaScript" src="#Attributes.MainNavJSURL#/Do#Button#.js"></script></cfoutput>
	</cfif>
</cfloop>

<cfif (NOT Attributes.TextOnly) AND (Len(Variables.ListShowButtons) GT 0)>
	<cfoutput>
<script language="JavaScript">
<!--
</cfoutput>
	<cfset Variables.CurrIdx						= 0>
	<cfloop index="Button" list="#Variables.ListShowButtons#">
		<cfset Variables.CurrIdx					= Variables.CurrIdx + 1>
		<cfif ListFind(Variables.ListStdButtons, Button)>
			<cfset Variables.Dir					= "#Attributes.LibURL#/images/sbalookandfeel">
		<cfelse>
			<cfset Variables.Dir					= "#Attributes.LibURL#/images/applookandfeel">
		</cfif>
		<cfset Variables.Image						= ListGetAt(Variables.ListShowImages, CurrIdx)>
		<cfoutput>
var	g#Button#Lo	= new Image(#Variables.ImageWidth#,#Variables.ImageHeight#); g#Button#Lo.src	= "#Dir#/#Image#_lo.jpg";
var	g#Button#Hi	= new Image(#Variables.ImageWidth#,#Variables.ImageHeight#); g#Button#Hi.src	= "#Dir#/#Image#_hi.jpg";</cfoutput>
	</cfloop>
	<cfoutput>

// -->
</script>
</cfoutput>
</cfif><!--- (NOT Attributes.TextOnly) AND (Len(Variables.ListShowButtons) GT 0) --->

<!---
AUTHOR:				Steve Seaquist, Base Technologies, Inc, for the US Small Business Administration. 
DATE:				03/28/2013
DESCRIPTION:		Outputs the web page in Desktop View. 
NOTES:				None. 
INPUT:				None. 
OUTPUT:				None. 
REVISION HISTORY:	04/27/2013, SRS:	Added DivSliderControl (for user complaints that not all of AppNav was visible). 
										Removed "AllowSlider" logic because now everyone has the feature. 
					04/08/2013, SRS:	Original implementation. Cannibalized /pro-net/experiments/steve version (the 
										original jQuery Mobile prototype). Adapted to the move of jQuery Mobile support 
										into the /library directory. Fixed crashes when Request.SlafApplicationName is 
										undefined, for some reason (such as the /library/experiments directory). 
--->

<cfoutput>#Request.SlafHead#
<title>#Attributes.WindowTitle#</title>#Request.SlafTopOfHead#
<link  href="#Attributes.LibURL#/images/sbalookandfeel/favicon.ico"                                  rel="icon" type="image/vnd.microsoft.icon"/><cfif Request.SlafHTML5>
<link  href="#Attributes.LibURL#/css/sbalookandfeel.strict.css?CachedAsOf=2013-04-27T00:38"          rel="stylesheet" type="text/css" media="all"/><cfif Len(Attributes.MainNavURL) IS 0>
<link  href="#Attributes.LibURL#/css/mainnav.strict.css?CachedAsOf=2012-06-29T14:07"                 rel="stylesheet" type="text/css" media="all"/></cfif><cfelse>
<link  href="#Attributes.LibURL#/css/sbalookandfeel.quirks.css?CachedAsOf=2012-03-29T21:15"          rel="stylesheet" type="text/css" media="all"/><cfif Len(Attributes.MainNavURL) IS 0>
<link  href="#Attributes.LibURL#/css/mainnav.quirks.css?CachedAsOf=2012-06-29T14:07"                 rel="stylesheet" type="text/css" media="all"/></cfif></cfif><cfif Variables.jQueryMobileAware>
<link  href="#Attributes.LibURL#/css/jquery.mobile/sba.dtv.css?CachedAsOf=2012-06-20T22:15"          rel="stylesheet" type="text/css" media="all"/></cfif>
<script src="#Attributes.LibURL#/javascripts/DumpObject.js"></script><cfif Len(Attributes.jQueryURL) GT 0>
<script src="#Attributes.jQueryURL#"></script><cfelse>#Request.SlafTopOfHeadjQuery#</cfif><cfif Request.SlafHTML5><!--- STRICT MODE --->
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/sbalookandfeel.strict.js?CachedAsOf=2013-04-27T02:07"></script><cfelse><!--- QUIRKS MODE --->
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/sbalookandfeel.quirks.js?CachedAsOf=2013-02-14T15:59"></script></cfif><cfif Len(Attributes.MainNavURL) IS 0>
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/mainnav.js?CachedAsOf=2010-11-09T17:12"></script><cfif Variables.jQueryMobileAware>
<script src="#Attributes.LibURL#/javascripts/jquery/jquery.mobile/sba.jqm.js?CachedAsOf=2013-03-28T16:11"></script><!-- Needed by DoMobileView(). --></cfif>
<!--[if lte IE 9]><cfif Request.SlafButtons3D>
<link  href="#Attributes.LibURL#/css/mainnav.msie.pie3d.css?CachedAsOf=2012-06-29T13:24"             rel="stylesheet" type="text/css" media="all"/><cfelse>
<link  href="#Attributes.LibURL#/css/mainnav.msie.plain.css?CachedAsOf=2011-08-09T16:33"             rel="stylesheet" type="text/css" media="all"/></cfif>
<![endif]--></cfif><cfif Attributes.TextOnly>
<link  href="#Attributes.LibURL#/css/mainnav.textonly.css?CachedAsOf=2011-08-09T15:46"               rel="stylesheet" type="text/css" media="all"/></cfif>
<script><!--- We want to do as much as possible in cached JS files. But the following are dynamic and cannot be cached: --->
gSlafAppNavIsVisible					= <cfif Variables.AppNavIsVisible	>true<cfelse>false</cfif>;
gSlafDebug								= <cfif Attributes.Debug			>true<cfelse>false</cfif>;
gSlafRenderingMode						= "dtv"; // "Desktop View"<cfif IsDefined("Request.SlafApplicationName")>
gSlafSessionGroupId						= "#Hash(UCase(Request.SlafApplicationName))#";<cfelse>
gSlafSessionGroupId						= "";</cfif><cfif IsDefined("Request.SlafSuppressRTE")>
gSlafSuppressRTE						= <cfif Request.SlafSuppressRTE		>true<cfelse>false</cfif>;<cfelse>
gSlafSuppressRTE						= false;</cfif>
$(document).ready(function				()
	{<cfif Len(Attributes.MainNavURL) IS 0>
	MainNavDoThisOnLoad();				// Note, call MainNavDoThisOnLoad BEFORE SlafDoThisOnLoad. </cfif>
	SlafDoThisOnLoad();					// Always.<cfif Attributes.Debug>
	alert("Debug: #JSStringFormat(Variables.DebugString)#");</cfif>
	});<!--- #Request.SlafBrowser.TouchBasedJSToShowTooltips# --->
</script></cfoutput>
	<cfif Len(Attributes.JSInline) GT 0>
		<cfoutput>
<!-- JSInline, begins. -->
#Attributes.JSInline#
<!-- JSInline, ends. --></cfoutput>
	</cfif>
	<cfif Len(Attributes.MainNavURL) IS 0>
		<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_DefineJavaScriptsForButtons.cfm">
	</cfif>
	<cfif Attributes.AutoAlignTbls>
		<!---
		The following included scripts (jquery.findClosest.js, jquery.findNestedFirst.js and jquery.alignPseudoTables.js) 
		have to be defined AFTER jquery.js, or else they'll crash when they try to reference jQuery to register themselves 
		as plug-ins. If it weren't for the need to include jquery.js first, it would have been better to define this code 
		in Request.SlafTopOfHead, so that pages that run in frames would get their tbls automatically aligned too. Instead, 
		pages that run in frames have to cfinclude /library/cfincludes/get_sbalookandfeel_autoaligntbls.cfm sometime after 
		jquery.js in the head. The scripts come out of the /jquery.msie subdirectory because they're only needed in MSIE. 
		--->
		<cfoutput>
<!--[if lte IE 7]><cfif Variables.AutoIncFindClosest>
<script src="#Attributes.LibURL#/javascripts/jquery/jquery.msie/jquery.findClosest.js"></script></cfif><cfif Variables.AutoIncFindNestedFirst>
<script src="#Attributes.LibURL#/javascripts/jquery/jquery.msie/jquery.findNestedFirst.js"></script></cfif><cfif Variables.AutoIncAlignPseudoTables>
<script src="#Attributes.LibURL#/javascripts/jquery/jquery.msie/jquery.alignPseudoTables.js"></script></cfif>
<script>
$(document).ready(function()
	{
	$(document).findNestedFirst(".tbl").alignPseudoTables(".tbl", ".tbl_row", ".tbl_cell");
	});
</script>
<![endif]--></cfoutput>
	</cfif>
	<cfoutput>
<!-- Per GSA instructions, the best place for this script is just before /head: -->
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/Federated-Analytics.js"></script>
</head>
<body class="dtv"><div class="hide"><!-- Without this div tag, some browsers add whitespace, because form is a block element. -->
<form name="SlafToggleTextOnlyForm" target="_top" action="#Variables.SlafToggleTextOnlyFormAct#" method="post" onsubmit="
this.JavaScriptOn.value							= 'Yes';
return true;
"><!-- Use of this form is now deprecated. Use SBA Look-and-Feel menu ('question mark menu'), Edit Preferences, instead. -->
<input type="Hidden" name="JavaScriptOn"		value="No">
<input type="Hidden" name="SlafToggleTextOnly"	value="Yes">
</form></div
><div			id="DivWindow">
	<div		id="DivMarginT"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginR"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginB"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginL"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivEnvelope"><!-- Put the next few greater-thans on subsequent lines to eliminate white space: -->
		<div	id="DivEnvelopeTop"
		><cfif Variables.ShowSkipLinks><div id="DivSkipLinks" class="inlineblock"
		><a id="LnkSkipNav" title="Skip Navigation" href="##DivAppData" onclick="
		document.getElementById('DivAppData').focus();	// Focusable because of the tabindex='-1' hack. See DivAppData. 
		return false;									// Setting focus resets tab order, so don't follow JS-off href. 
		">Skip Navigation</a<cfif IsDefined("Request.SlafApplicationName")>
		>&nbsp;&nbsp;&nbsp;&nbsp;<a title="Accessibility Options (opens a new window)"
			href="#Attributes.LibURL#/callbacks/dsp_preferences.cfm?SessionGroupId=#Hash(UCase(Request.SlafApplicationName))#"
			target="_blank">Accessibility Options</a</cfif>
		></div></cfif><cfif NOT Attributes.TextOnly><img src="#Attributes.LibURL#/images/sbalookandfeel/background.bluesteel.png" alt="decorative pattern" border="0"
		></cfif><!-- Entire DivSBALogo is a hotlink (if browser supports it): --><a href="http://www.sba.gov/"
		><div	id="DivSBALogo"		title="Go To SBA Home Page" role="navigation"
		><!-- But browser may not support it, so repeat for contents of div too: --><a href="http://www.sba.gov/"
			><cfif Attributes.TextOnly>#Variables.SBALogoInline#<cfelse
			><!--- STRICT MODE ---><cfif Request.SlafHTML5
			><img src="#Variables.SBALogoURL#" id="SBALogo" class="#Request.SlafDevTestProd#" alt="Go To SBA Home Page" height="40"><cfelse
			><img src="#Variables.SBALogoURL#" style="#Variables.SBALogoPosition#" alt="Go To SBA Home Page" height="40"></cfif></cfif></a><!-- logo/hotlink only --></div
		></a><!-- DivSBALogo and logo/hotlink --><cfif Request.SlafHTML5><!--- STRICT MODE --->
		<div	id="DivMainNav"		role="navigation"><cfelse>
		<div	id="DivMainNav"		style="height:#Variables.MainNavHeight#;" role="navigation"></cfif></cfoutput>
	<cfif Len(Attributes.MainNavURL) GT 0>
		<cfoutput><iframe name="MainNav" id="FrmMainNav" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('MainNav');" src="#Attributes.MainNavURL#"#Variables.IFrameAttribs# scrolling="No" title="Main Navigation">#Variables.NoIFrames#</iframe></cfoutput>
	<cfelse>
		<cfif Variables.ShowSkipLinks or (NOT Attributes.TextOnly)>
			<!--- That is, if we need to push down the display of MainNav pseudo-buttons/hotlinks: --->
			<cfoutput><div id="DivMainNavButtons"></cfoutput>
		</cfif>
		<cfmodule template="#Attributes.LibURL#/customtags/mainnav.cfm"
			ActionURL								= "#Attributes.ActionURL#"
			Configs									= "#Attributes.Configs#"
			Debug									= "#Attributes.Debug#"
			Height									= "100%"
			InFrame									= "No"
			LibURL									= "#Attributes.LibURL#"
			MainNavHiddens							= "#Attributes.MainNavHiddens#"
			MainNavJSURL							= "#Attributes.MainNavJSURL#"
			ReadyLight								= "#Attributes.ReadyLight#"
			Show									= "#Variables.ListShowBtnsFull#"
			TextOnly								= "#Attributes.TextOnly#"
			Width									= "100%">
		<cfif Variables.ShowSkipLinks or (NOT Attributes.TextOnly)>
			<!--- That is, if we need to push down the display of MainNav pseudo-buttons/hotlinks: --->
			<cfoutput></div></cfoutput>
		</cfif>
	</cfif>
	<cfoutput>#Request.SlafEOL#
		</div><!-- DivMainNav -->
		<div	id="DivAppName"></div><!-- DivAppName just draws 2 vertical lines. -->
		<div	id="DivAppNameText" class="inlineblock">#Attributes.AppName#</div><!-- DivAppNameText -->
		</div><!-- DivTopOfEnvelope --><cfif Request.SlafHTML5><!--- STRICT MODE --->
		<div	id="DivAppInfo"		class="inthead"><cfelse>
		<div	id="DivAppInfo"		class="inthead" style="display:#Variables.AppInfoDisplay#;"></cfif></cfoutput>
	<cfif Len(Attributes.AppInfoURL) GT 0>
		<!---
		Per Director of OISS, AppInfo isn't involved in the setting the ReadyLight, which is one of the main things that 
		SlafSetFrameLoaded does. However, a reload of the AppInfo frame DOES prevent session timeout, so there's no harm 
		in calling SlafSetFrameLoaded in the frame's onload. (Refreshing the session timeout countdown timer is something 
		else that SlafSetFrameLoaded also does.) However, do ***NOT*** call SlafSetReadyLightToLoading on the AppInfo 
		frame's onunload just because the other frames are doing so. If you do, it will set the ReadyLight to "Loading" 
		and nothing will ever set it back to "Ready". (This would be a case where symmetry is not good.) 
		--->
		<cfoutput><iframe name="AppInfo" id="FrmAppInfo" onload="SlafSetFrameLoaded.call(this,this);" src="#Attributes.AppInfoURL#"#Variables.IFrameAttribs# scrolling="No" title="Info about current page">#Variables.NoIFrames#</iframe></cfoutput>
	<cfelse>
		<cfoutput>#Attributes.AppInfoInline#</cfoutput>
	</cfif>
	<cfoutput></div><!-- DivAppInfo --><cfif Request.SlafHTML5><!--- STRICT MODE --->
		<div	id="DivAppNav"		#Variables.MaybeNoAppNavClass#role="navigation"><cfelse>
		<div	id="DivAppNav"		style="#Variables.AppNavStyle#" role="navigation"></cfif></cfoutput>
	<cfif Len(Attributes.AppNavURL) GT 0>
		<cfoutput><iframe name="AppNav" id="FrmAppNav" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('AppNav');" src="#Attributes.AppNavURL#"#Variables.IFrameAttribs# title="Application Navigation">#Variables.NoIFrames#</iframe></cfoutput>
	<cfelse>
		<cfoutput>#Attributes.AppNavInline#</cfoutput>
	</cfif>
	<cfoutput></div><!-- /DivAppNav --></cfoutput>
	<cfif Request.SlafHTML5><!--- STRICT MODE --->
		<cfif Variables.AppNavIsVisible>
			<cfoutput>
		<div	id="DivSliderControl" onclick="SlafToggleAppNav();" title="Click to expand."></div></cfoutput>
		</cfif><!--- /AppNavIsVisible --->
		<cfoutput>
		<div	id="DivAppData"		#Variables.MaybeNoAppNavClass#role="main" tabindex="-1"></cfoutput>
	<cfelse><!--- /STRICT MODE, begin QUIRKS MODE --->
		<cfoutput>
		<div	id="DivAppData"		style="#Variables.AppDataStyle#" role="main" tabindex="-1"></cfoutput>
	</cfif><!--- /QUIRKS MODE --->
	<cfif Len(Attributes.AppDataURL) GT 0>
		<cfoutput><iframe name="AppData" id="FrmAppData" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('AppData');" src="#Attributes.AppDataURL#"#Variables.IFrameAttribs# title="Data Entry">#Variables.NoIFrames#</iframe></div><!-- DivAppData --></cfoutput>
	<cfelse>
		<cfif IsDefined("Attributes.AppDataInline")>
			<cfoutput>#Attributes.AppDataInline#</cfoutput>
		</cfif>
		<!---
		Note: thisTag.GeneratedContent is inline AppData HTML between <cf_sbalookandfeel> and </cf_sbalookandfeel>. 
		It's only defined when thisTag.executionMode IS "End". 
		--->
		<cfoutput>#thisTag.GeneratedContent#
		</div><!-- DivAppData --></cfoutput>
		<cfset thisTag.GeneratedContent				= ""><!--- After use, thisTag.GeneratedContent should be cleared. --->
	</cfif>
	<cfif Request.SlafHTML5><!--- STRICT MODE --->	<cfset a = "botmostgovlinks">	<cfset b = "botmostsitelinks">
	<cfelse>										<cfset a = "gov">				<cfset b = "site"></cfif>
	<cfoutput>
		<div	id="DivBotMost" role="navigation">#Variables.LastModified#
			<ul	id="#a#">
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.firstgov.gov/">&gt; FirstGov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/omb/egov/">&gt; E-Gov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.regulations.gov/">&gt; Regulations.gov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/">&gt; White House</a></li>
			</ul>
			<ul	id="#b#" role="navigation">
				<li><a href="http://www.sba.gov/privacysecurity/index.html">* Privacy &amp; Security</a></li>
				<li><a href="http://www.sba.gov/information/index.html">* Information Quality</a></li>
				<li><a href="http://www.sba.gov/foia/index.html">* FOIA</a></li>
				<li><a href="http://www.sba.gov/eeo/">* No Fear Act</a></li>
				<li><a href="http://www.sba.gov/ada/index.html">* ADA</a></li>
			</ul>
		</div><!-- DivBotMost -->
	</div><!-- DivEnvelope -->
	<!-- Define DivSlafMenuControl after DivEnvelope, and DivSlafMenu after DivSlafMenuControl, for higher z-order. -->
	<div		id="DivSlafMenuControl" title="Template Menu"></div>
	<div		id="DivSlafMenu" role="navigation">
		<a class="menuitem" href="#Attributes.LibURL#/html/sbalookandfeel_help.html"
			target="_blank" onClick="
		SlafMenuHide();
		return true;
		">Template Help</a><br/><cfif IsDefined("Request.SlafApplicationName")>
		<a class="menuitem" href="#Attributes.LibURL#/callbacks/dsp_preferences.cfm?SessionGroupId=#Hash(UCase(Request.SlafApplicationName))#"
			target="_blank" onclick="
		SlafMenuHide();
		return true;
		">Edit Preferences ...</a><br/></cfif>
		<a class="menuitem" href="javascript:SlafMenuSelect('Print');">Printer-Friendly Print</a><br/>
		<a class="menuitem" href="javascript:SlafMenuSelect('Show/Hide');">Show/Hide Navigation</a><cfif "No"><br/>
		<!--
		<a class="menuitem" href="javascript:{top.document.SlafToggleTextOnlyForm.submit();SlafMenuHide();}"
		>Switch to <cfif Attributes.TextOnly>Graphics<cfelse>Text-Only</cfif> Mode (Reloads Page)</a>
		NOTE: SlafMenuSelect('TextOnly') exists, but the previous href does it inline to give non-standards-compliant 
		browsers some time to get the most recent sbalookandfeel.js. Use SlafMenuSelect after, say, 08/31/2010. 
		--></cfif><cfif Variables.SlafIPAddrIsOISS><br/>
		<a class="menuitem" href="javascript:SlafMenuSelect('CFDebug');"
		>OISS-Only Debug: Show/Hide *ALL* (except ?).</a><br/>
		<span class="menuitem">
			OISS-Only Debug: 
			#Request.SlafDevTestProd#, 
			#Request.SlafServerGroup#, 
			#Request.SlafServerName#, 
			#Request.SlafServerInstanceName#
		</span></cfif>
	</div>
	<div		id="DivAppHidden">
		<!--
		Hiding with style="display:none" allows debug display by typing the following URL into the address/location bar: 
			javascript:alert(gDivAppHidden.style.display="block"); // overlays entire window contents
			javascript:alert(gDivAppHidden.style.display="none");  // resumes hiding it, revealing previous window contents
		-->
		<iframe id="FrmAppHidden" name="AppHidden" src="#Attributes.AppHiddenURL#" 
			frameborder="0" marginheight="0" marginwidth="0" title="Hidden Frame, sometimes used for server callbacks"
			width="100%">#Variables.NoIFrames#</iframe>
	</div><!-- DivAppHidden --><cfif Variables.DeveloperOfSlaf>
	<div		id="DivHighlightCursor"></div></cfif>
</div><!-- DivWindow -->
</body>
</html>
</cfoutput>

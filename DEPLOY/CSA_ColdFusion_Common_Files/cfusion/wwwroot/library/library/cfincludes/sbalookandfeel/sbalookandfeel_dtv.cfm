<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				03/28/2013.
DESCRIPTION:		Outputs the web page in Desktop View.
NOTES:				None.
INPUT:				None.
OUTPUT:				None.
REVISION HISTORY:	10/07/2015 -
					01/08/2016, SRS&NS:	Reworked lots of stuff to be "themeable" and simpler because we no longer have 
										to support MSIE 8: Got rid of files for quirks mode, which we haven't generated 
										in over 4 years, to relieve that maintenance burden. (SRS) Added "Email Us". (NS) 
										Added support for MainNavMenus (included calling PageSegmentsGrabber). 
					03/04/2015, SRS:	As part of making the color scheme considerably lighter, ditched bluesteel.
					10/16/2013, SRS:	Got rid of hardcoded references to sbalookandfeel.*.js in favor of variables
										defined in get_sbalookandfeel_topofhead. Allows modifying both mobile and nomobile
										versions of customtag in only one place.
					04/27/2013, SRS:	Added DivSliderControl (for user complaints that not all of AppNav was visible).
										Removed "AllowSlider" logic because now everyone has the feature.
					04/08/2013, SRS:	Original implementation. Cannibalized /pro-net/experiments/steve version (the
										original jQuery Mobile prototype). Adapted to the move of jQuery Mobile support
										into the /library directory. Fixed crashes when Request.SlafApplicationName is
										undefined, for some reason (such as the /library/experiments directory).
--->

<cfset Variables.EnvLC										= LCase(Request.SlafDevTestProd)>
<cfset Variables.BodyLoadTimeClasses						= "dtv #Variables.EnvLC#"><!--- These won't change. --->
<cfset Variables.LogoURL									= "#Attributes.LibURL#/images/sbalookandfeel/sba_logo.sba.png">
<cfif ListFind("dev,test", Variables.EnvLC) gt 0><!--- "Default to production, override to test" --->
	<cfset Variables.LogoURL								= "#Attributes.LibURL#/images/sbalookandfeel/sba_logo.#Variables.EnvLC#.png">
</cfif>
<cfset Variables.PrefsCallbackURL							= "#Attributes.LibURL#/callbacks/jquery/act_ManagePreferences.jquery.cfm">

<cfoutput>#Request.SlafHead#
<title>#Attributes.WindowTitle#</title>#Request.SlafTopOfHead#
#Request.SlafTopOfHeadFavicon#
#Request.SlafTopOfHeadCSSSlafStrict#<cfif Len(Attributes.MainNavURL) is 0>
#Request.SlafTopOfHeadCSSMainNavStrict#</cfif><cfif Variables.jQueryMobileAware>
#Request.SlafTopOfHeadCSS_dtv#</cfif>
#Request.SlafTopOfHeadDumpObject#<cfif Len(Attributes.jQueryURL) gt 0>
<script src="#Attributes.jQueryURL#"></script><cfelse>
#Request.SlafTopOfHeadjQuery#</cfif>
#Request.SlafTopOfHeadSlafStrict#<cfif Len(Attributes.MainNavURL) is 0>
#Request.SlafTopOfHeadMainNav#<cfif Variables.jQueryMobileAware><!--- Note: jQueryMobileAWARE (the following is just to allow toggle). --->
<script src="#Attributes.LibURL#/javascripts/jquery/jquery.mobile/sba.jqm.js?CachedAsOf=2013-03-28T16:11"></script><!-- Needed by DoMobileView(). --></cfif>
<!--[if lte IE 9]><cfif Request.SlafButtons3D>
#Request.SlafTopOfHeadCSSMainNavMSIEPie3d#<cfelse>
#Request.SlafTopOfHeadCSSMainNavMSIEPlain#</cfif>
<![endif]--></cfif>
<script>
gSlafAllThemeNames								= "#Variables.NamedThemes#";
gTrimRE											= /^(\s|\u00A0)+|(\s|\u00A0)+$/g; // Note: "g" prefix means global in prefix notation.
function SlafSetThemeForAllFrames				(pThemeName)
	{// First set top and its frames, then do AJAX callback asynchronously. That ordering improves the perception of faster response time. 
	// The following is dependent on which regions of the screen are frames (controlled externally), so it can't be cached in a *.js file. 
	// Also, SlafSetSharedAncestorClass in a frame COULD be done asynchronously, even if frame doesn't include jQuery. 
												top			.SlafSetSharedAncestorClass(gSlafAllThemeNames, pThemeName);<cfif		 Len(Attributes.AppNavURL) gt 0>
	if	(top.AppNav	.SlafSetSharedAncestorClass)top.AppNav	.SlafSetSharedAncestorClass(gSlafAllThemeNames, pThemeName);</cfif><cfif Len(Attributes.AppDataURL) gt 0>
	if	(top.AppData.SlafSetSharedAncestorClass)top.AppData	.SlafSetSharedAncestorClass(gSlafAllThemeNames, pThemeName);</cfif><cfif Len(Attributes.AppInfoURL) gt 0>
	if	(top.AppInfo.SlafSetSharedAncestorClass)top.AppInfo	.SlafSetSharedAncestorClass(gSlafAllThemeNames, pThemeName);</cfif><cfif Len(Attributes.MainNavURL) gt 0>
	if	(top.MainNav.SlafSetSharedAncestorClass)top.MainNav	.SlafSetSharedAncestorClass(gSlafAllThemeNames, pThemeName);</cfif>
	$.ajax("#Variables.PrefsCallbackURL#?Action=SetTheme&ThemeName=" + pThemeName,
		{
		accepts:	"text/plain",
		async:		true,
		cache:		false,
		complete:	function(pXHR, pStatus)
						{
						var	sResponseTextTrimmed;
						if	(pStatus !== "success")
							alert("Unable to make theme display persistently. Status: " + pStatus);
						else
							{
							sResponseTextTrimmed			= pXHR.responseText.replace(gTrimRE, "");
							if	(sResponseTextTrimmed == "Success!")
								gSlafThemeName				= pThemeName;
							else
								alert(sResponseTextTrimmed);
							}
						},
		dataType:	"text",
		type:		"GET",
		timeout:	30000 // 30 seconds
		});
	return false;// In menu link, href is bogus. Don't follow it.
	}
</script>
<script><!--- We want to do as much as possible in cached JS files. But the following are dynamic and cannot be cached: --->
gSlafAppNavIsVisible					= <cfif Variables.AppNavIsVisible	>true<cfelse>false</cfif>;
gSlafDebug								= <cfif Attributes.Debug			>true<cfelse>false</cfif>;
gSlafRenderingMode						= "dtv"; // "Desktop View"<cfif IsDefined("Request.SlafApplicationName")>
gSlafSessionGroupId						= "#Hash(UCase(Request.SlafApplicationName))#";<cfelse>
gSlafSessionGroupId						= "";</cfif><cfif IsDefined("Request.SlafSuppressRTE")>
gSlafSuppressRTE						= <cfif Request.SlafSuppressRTE		>true<cfelse>false</cfif>;<cfelse>
gSlafSuppressRTE						= false;</cfif>
$(document).ready(function				()
	{<cfif Len(Attributes.MainNavURL) is 0>
	MainNavDoThisOnLoad();				// Note, call MainNavDoThisOnLoad BEFORE SlafDoThisOnLoad. </cfif>
	SlafDoThisOnLoad();					// Always.<cfif Attributes.Debug>
	alert("Debug: #JSStringFormat(Variables.DebugString)#");</cfif>
	});<!--- #Request.SlafBrowser.TouchBasedJSToShowTooltips# --->
</script></cfoutput>
	<cfif Len(Attributes.JSInline) gt 0>
		<cfoutput>
<!-- JSInline, begins. -->
#Attributes.JSInline#
<!-- JSInline, ends. --></cfoutput>
	</cfif>
	<cfif Len(Attributes.MainNavURL) is 0>
		<cfinclude template="#Attributes.LibURL#/cfincludes/#Variables.SelfName#/dsp_DefineJavaScriptsForButtons.cfm">
	</cfif>
	<cfoutput>
<!-- Per GSA instructions, the best place for this script is just before /head: -->
<script src="#Attributes.LibURL#/javascripts/sbalookandfeel/Federated-Analytics.js"></script>
</head>
<body class="#Variables.BodyLoadTimeClasses#"
><div			id="DivWindow">
	<div		id="DivMarginT"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginR"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginB"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivMarginL"		onclick="SlafToggleAppData();" title="Maximize data entry region of screen."></div>
	<div		id="DivEnvelope"
	><!-- Put the next few greater-thans on subsequent lines to eliminate white space: --><cfif Variables.ShowSkipLinks><div id="DivSkipLinks" class="inlineblock"
		><a id="LnkSkipNav" class="hiddenlink" title="Skip Navigation" href="##DivAppData" onclick="
		document.getElementById('DivAppData').focus();	// Focusable because of the tabindex='-1' hack. See DivAppData.
		return false;									// Setting focus resets tab order, so don't follow JS-off href.
		">Skip Navigation</a><cfif IsDefined("Request.SlafApplicationName")>
		&nbsp;&nbsp;<a class="hiddenlink" title="Accessibility Options (opens a new window)"
			href="#Attributes.LibURL#/callbacks/dsp_preferences.cfm?SessionGroupId=#Hash(UCase(Request.SlafApplicationName))#"
			target="_blank">Accessibility Options</a></cfif>
		</div><!-- /DivSkipLinks. --></cfif>

		<div	id="DivEnvelopeTop"
		><!-- The **entire** DivSBALogo is a hotlink, but IE won't let us use <a> tag: --><div id="DivSBALogo" role="navigation" title="Go To SBA Home Page"
		onclick="top.location.href = 'http://www.sba.gov/'"><span></span><div class="server">#ListFirst(CGI.Server_Name, ".")#</div></div
		><!-- /DivSBALogo -->
		<div	id="DivMainNav"		role="navigation"></cfoutput>
	<cfif Len(Attributes.MainNavURL) GT 0>
		<cfif IsDefined("Request.SlafShowButtonsInMainNavMenu")
			or	(	(Len(Attributes.MainNavMenuURL)	gt 0)
				and	FileExists(ExpandPath(Attributes.MainNavMenuURL))
				)>
			<cfoutput><div id="DivMainNavInline"	role="navigation"></div>
		<script src="#Attributes.LibURL#/javascripts/jquery/PageSegmentsGrabber.jquery.js?CachedAsOf=2016-01-08T12:55"></script>
		<script>
		$(document).ready(function()
			{
			MainNavGrabber("#JSStringFormat(Attributes.MainNavURL)#");
			});
		</script></cfoutput>
		<cfelse>
			<cfoutput><iframe name="MainNav" id="FrmMainNav" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('MainNav');" src="#Attributes.MainNavURL#"#Variables.IFrameAttribs# scrolling="No" title="Main Navigation">#Variables.NoIFrames#</iframe></cfoutput>
		</cfif><!--- /(SlafShowButtonsInMainNavMenu or MainNavMenuURL) --->
	<cfelse>
		<cfif Variables.ShowSkipLinks or (NOT Attributes.TextOnly)>
			<!--- That is, if we need to push down the display of MainNav pseudo-buttons/hotlinks: --->
			<cfoutput><div id="DivMainNavButtons"></cfoutput>
		</cfif>
		<cf_mainnav
			ActionURL								= "#Attributes.ActionURL#"
			Configs									= "#Attributes.Configs#"
			Debug									= "#Attributes.Debug#"
			Height									= "100%"
			InFrame									= "No"
			LibURL									= "#Attributes.LibURL#"
			MainNavHiddens							= "#Attributes.MainNavHiddens#"
			MainNavJSURL							= "#Attributes.MainNavJSURL#"
			MainNavMenuURL							= "#Attributes.MainNavMenuURL#"
			ReadyLight								= "#Attributes.ReadyLight#"
			Show									= "#Variables.ListShowBtnsFull#"
			TextOnly								= "#Attributes.TextOnly#"
			Width									= "100%">
		<cfif Variables.ShowSkipLinks or (NOT Attributes.TextOnly)>
			<!--- That is, if we need to push down the display of MainNav pseudo-buttons/hotlinks: --->
			<cfoutput></div></cfoutput>
		</cfif>
	</cfif>
	<cfoutput>#Request.SlafEOL#
		</div><!-- /DivMainNav -->
		<div	id="DivAppName"></div><!-- DivAppName just draws 2 vertical lines. -->
		<div	id="DivAppNameText" class="inlineblock">#Attributes.AppName#</div><!-- /DivAppNameText --><cf_buttonbar RightSide="#Attributes.Icons#">
		</div><!-- /DivEnvelopeTop -->
		<div	id="DivAppInfo"		class="inthead"></cfoutput>
	<cfif Len(Attributes.AppInfoURL) GT 0>
		<!---
		Per Director of OISS, AppInfo isn't involved in the setting the ReadyLight, which is one of the main things that
		SlafSetFrameLoaded does. However, a reload of the AppInfo frame DOES prevent session timeout, so there's no harm
		in calling SlafSetFrameLoaded in the frame's onload. (Refreshing the session timeout countdown timer is something
		else that SlafSetFrameLoaded also does.) However, do ***NOT*** call SlafSetReadyLightToLoading on the AppInfo
		frame's onunload just because the other frames are doing so. If you do, it will set the ReadyLight to "Loading"
		and nothing will ever set it back to "Ready". (This would be a case where symmetry is not good.)
		--->
		<cfoutput><iframe name="AppInfo" id="FrmAppInfo" onload="SlafSetFrameLoaded.call(this,this);" src="#Attributes.AppInfoURL#"#Variables.IFrameAttribs# scrolling="No" title="Info about current page">#Variables.NoIFrames#</iframe></cfoutput>
	<cfelse>
		<cfoutput>#Attributes.AppInfoInline#</cfoutput>
	</cfif>
	<cfoutput></div><!-- /DivAppInfo -->
		<div	id="DivAppNav"		#Variables.MaybeNoAppNavClass#role="navigation"></cfoutput>
	<cfif Len(Attributes.AppNavURL) gt 0>
		<cfoutput><iframe name="AppNav" id="FrmAppNav" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('AppNav');" src="#Attributes.AppNavURL#"#Variables.IFrameAttribs# title="Application Navigation">#Variables.NoIFrames#</iframe></cfoutput>
	<cfelse>
		<cfoutput>#Attributes.AppNavInline#</cfoutput>
	</cfif>
	<cfoutput></div><!-- /DivAppNav --></cfoutput>
	<cfif Variables.AppNavIsVisible>
		<cfoutput>
		<div	id="DivSliderControl" onclick="SlafToggleAppNav();" title="Click to expand."></div></cfoutput>
	</cfif><!--- /AppNavIsVisible --->
	<cfoutput>
		<div	id="DivAppData"		#Variables.MaybeNoAppNavClass#role="main" tabindex="-1"></cfoutput>
	<cfif Len(Attributes.AppDataURL) GT 0>
		<cfoutput><iframe name="AppData" id="FrmAppData" onload="SlafSetFrameLoaded.call(this,this);" onunload="SlafSetFrameLoading('AppData');" src="#Attributes.AppDataURL#"#Variables.IFrameAttribs# title="Data Entry">#Variables.NoIFrames#</iframe></div><!-- DivAppData --></cfoutput>
	<cfelse>
		<cfif IsDefined("Attributes.AppDataInline")>
			<cfoutput>#Attributes.AppDataInline#</cfoutput>
		</cfif>
		<!---
		Note: thisTag.GeneratedContent is inline AppData HTML between <cf_sbalookandfeel> and </cf_sbalookandfeel>.
		It's only defined when thisTag.executionMode IS "End".
		--->
		<cfoutput>#thisTag.GeneratedContent#
		</div><!-- /DivAppData --></cfoutput>
		<cfset thisTag.GeneratedContent				= ""><!--- After use, thisTag.GeneratedContent should be cleared. --->
	</cfif>
	<cfoutput>
		<div	id="DivBotMost" role="navigation">#Variables.LastModified#
			<ul	id="botmostgovlinks">
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.firstgov.gov/">&gt; FirstGov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/omb/egov/">&gt; E-Gov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.regulations.gov/">&gt; Regulations.gov</a></li>
				<li><a href="http://www.sba.gov/cgi-bin/byebye_st.pl?TO=http://www.whitehouse.gov/">&gt; White House</a></li>
			</ul>
			<ul	id="botmostsitelinks" role="navigation">
				<li><a href="http://www.sba.gov/privacysecurity/index.html">* Privacy &amp; Security</a></li>
				<li><a href="http://www.sba.gov/information/index.html">* Information Quality</a></li>
				<li><a href="http://www.sba.gov/foia/index.html">* FOIA</a></li>
				<li><a href="http://www.sba.gov/eeo/">* No Fear Act</a></li>
				<li><a href="http://www.sba.gov/ada/index.html">* ADA</a></li>
			</ul>
		</div><!-- /DivBotMost -->
	</div><!-- /DivEnvelope -->
	<div		id="DivAppHidden">
		<!--
		Hiding with style="display:none" allows debug display by typing the following URL into the address/location bar:
			javascript:alert(gDivAppHidden.style.display="block"); // overlays entire window contents
			javascript:alert(gDivAppHidden.style.display="none");  // resumes hiding it, revealing previous window contents
		-->
		<iframe id="FrmAppHidden" name="AppHidden" src="#Attributes.AppHiddenURL#"
			frameborder="0" marginheight="0" marginwidth="0" title="Hidden Frame, sometimes used for server callbacks"
			width="100%">#Variables.NoIFrames#</iframe>
	</div><!-- /DivAppHidden --><cfif Variables.DeveloperOfSlaf>
	<div		id="DivHighlightCursor"></div></cfif>
</div><!-- /DivWindow -->
</body>
</html>
</cfoutput>

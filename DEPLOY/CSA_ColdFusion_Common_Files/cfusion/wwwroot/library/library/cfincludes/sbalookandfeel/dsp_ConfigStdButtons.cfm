<!---
AUTHOR:				Steve Seaquist
DATE:				unknown
DESCRIPTION:		Defines configuration values that need to be kept in sync across cf_sbalookandfeel and cf_mainnav. 
NOTES:				Called by cf_sbalookandfeel and cf_mainnav. Must be in /library/cfincludes/sbalookandfeel subdirectory.
INPUT:				None. 
OUTPUT:				Variables-scope configuration variables. 
REVISION HISTORY:	06/20/2012, SRS:	Added Desktop View and Mobile View to the list of standard buttons. (FIRST!!) 
					07/20/2011, SRS:	Adapted to Request.SlafShowButtonOverrides. (See /library/udf/bld_SlafUDFs.cfm.) 
										Renamed lists so that the logic would be more comprehensible/readable. 
					10/04/2010, SRS:	Indented buttons using padding-left, not using a spacer div. 
					07/15/2010, SRS:	"New Look-and-Feel". Added this header. 
					unknown , SRS:		Original implementation.
--->

<cfset Variables.ImageHeight						= 25>
<cfset Variables.ImageWidth							= 75>
<cfset Variables.ImageStyle							= "border:0px;">
<cfset Variables.SkipOverAppName					= "padding-left:48px;">
<!--- Everything else derives from the button names. --->
<cfset Variables.ListStdBtnsFull					= "Desktop View,"
													& "Mobile View,"
													& "Reports,"
													& "Search,"
													& "Admin,"
													& "New Application,"
													& "Copy,"
													& "Print,"
													& "Exit,"
													& "Help">
<cfset Variables.ListStdBtnsPacked					= Replace(Replace(Variables.ListStdBtnsFull," ","","ALL"),"-","","ALL")>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2016, SRS:	OPSMDEV-1085: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvARMMarTypTBL"			datasource="#Variables.db#">
select		ARMMarTypInd,
			ARMMarTypDescTxt,
			ARMMarTypOrderByNmb,
			ARMMarTypInd								as code,
			ARMMarTypDescTxt							as description,
			ARMMarTypOrderByNmb							as displayorder<cfif Variables.Sybase>
from		sbaref..ARMMarTypTbl
where		(	datediff(dd,ARMMarTypStrtDt,			getdate()) >= 0)
and			(				ARMMarTypEndDt				is null
			or	datediff(dd,ARMMarTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.ARMMarTypTbl
where		(	(sysdate -	ARMMarTypStrtDt)			>= 0)
and			(				ARMMarTypEndDt				is null
			or	(sysdate -	ARMMarTypEndDt)				<= 0)</cfif>
order by	displayorder,
			description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2016, SRS:	OPSMDEV-1085: Fixed cfquery name. Ordered by OrderByNmb column. 
					08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/07/2013, RSD:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPymtFreqCdTbl"			datasource="#Variables.db#">
select		PymtFreqCd,
			PymtFreqDescTxt,
			PymtFreqOrderByNmb,
			PymtFreqCd									as code,
			PymtFreqDescTxt								as description,
			PymtFreqOrderByNmb							as displayorder<cfif Variables.Sybase>
from		sbaref..PymtFreqCdTbl
where		(	datediff(dd,PymtFreqStrtDt,				getdate()) >= 0)
and			(				PymtFreqEndDt				is null
			or	datediff(dd,PymtFreqEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.PymtFreqCdTbl
where		(	(sysdate -	PymtFreqStrtDt)				>= 0)
and			(				PymtFreqEndDt				is null
			or	(sysdate -	PymtFreqEndDt)				<= 0)</cfif>
order by	displayorder,
			description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

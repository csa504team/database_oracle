<!---
AUTHOR:				Nirish Namilae. 
DATE:				02/02/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	02/02/2012, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllLoanPymtTransCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanPymtTransCd,
			LoanPymtTransDescTxt,
			LoanPymtTransCd									AS code,
			LoanPymtTransDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..LoanPymtTransCdTbl<cfelse>
from		sbaref.LoanPymtTransCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

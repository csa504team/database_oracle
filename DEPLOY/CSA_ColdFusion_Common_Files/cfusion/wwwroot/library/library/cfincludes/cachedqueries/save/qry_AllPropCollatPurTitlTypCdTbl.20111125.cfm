<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllPropCollatPurTitlTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	   select
      	PropCollatPurTitlTypCd,
	    PropCollatPurTitlTypDescTxt,
	    PropCollatPurTitlTypStrtDt,
	    PropCollatPurTitlTypEndDt,
	    PropCollatPurTitlTypCd								AS code,
		PropCollatPurTitlTypDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..PropCollatPurTitlTypCdTbl
	   <cfelse>
	   from	sbaref.PropCollatPurTitlTypCdTbl
       </cfif>
 	   order by code	
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

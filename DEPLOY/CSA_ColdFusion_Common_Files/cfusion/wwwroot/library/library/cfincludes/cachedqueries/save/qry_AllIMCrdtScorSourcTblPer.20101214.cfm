<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllIMCrdtScorSourcTblPer"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMCrdtScorSourcCd,
			IMCrdtScorSourcDescTxt,
			IMCrdtScorSourcLowNmb,
			IMCrdtScorSourcHighNmb,
			IMCrdtScorSourcStrtDt,
			IMCrdtScorSourcEndDt,
			case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
			when IMCrdtScorSourcCd = 12					then 1
			when IMCrdtScorSourcCd = 13					then 2
			when IMCrdtScorSourcCd = 11					then 3
			when IMCrdtScorSourcCd = 14					then 4
			when IMCrdtScorSourcCd = 15					then 5
			else										6
			end											AS displayorder,
			IMCrdtScorSourcCd							AS code,
			IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMCrdtScorSourcTbl<cfelse>
from		sbaref.IMCrdtScorSourcTbl</cfif>
where		(IMCrdtScorSourcBusPerInd					= 'P')
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/24/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/24/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.AllIMUserPrefThemeCdTbl"	datasource="#Variables.db#">
select		IMUserPrefThemeCd,
			IMUserPrefThemeDescTxt,
			IMUserPrefThemeStrtDt,
			IMUserPrefThemeEndDt,
			IMUserPrefThemeCd									as displayorder,
			IMUserPrefThemeCd									as code,
			IMUserPrefThemeDescTxt								as description<cfif Variables.Sybase>
from		security..IMUserPrefThemeCdTbl<cfelse>
from		security.IMUserPrefThemeCdTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

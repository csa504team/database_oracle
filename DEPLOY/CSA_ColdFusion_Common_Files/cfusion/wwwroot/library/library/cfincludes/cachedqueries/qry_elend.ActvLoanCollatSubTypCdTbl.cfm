<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				11/22/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/22/2016, SRS:	OPSMDEV-1085: Original implementation. **TEMPORARILY** uses the old 
										table (loan.LAC_Cond_TypCd_Tbl) for description text because the new 
										table hasn't been created yet. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanCollatSubTypCdTbl"	datasource="#Variables.db#">
select		new.LoanCollatTypCd,
			new.LoanCollatSubTypCd,
			old.LAC_Cond_Typ_Desc_Txt					LoanCollatSubTypDescTxt,
			new.LoanCollatSubTypCd						code,
			old.LAC_Cond_Typ_Desc_Txt					description<cfif Variables.Sybase>
from		loanapp.LoanCollatTypMapTbl					new
inner join	loan.LAC_Cond_TypCd_Tbl						old on (new.LoanCollatSubTypCd	= old.LAC_Cond_Typ_Cd)
where		(	datediff(dd,LoanCollatMapStrtDt,		getdate()) >= 0)
and			(				LoanCollatMapEndDt			is null
			or	datediff(dd,LoanCollatMapEndDt,			getdate()) <= 0)<cfelse>
from		loanapp.LoanCollatTypMapTbl					new
inner join	loan.LAC_Cond_TypCd_Tbl						old on (new.LoanCollatSubTypCd	= old.LAC_Cond_Typ_Cd)
where		(	(sysdate -	LoanCollatMapStrtDt)		>= 0)
and			(				LoanCollatMapEndDt			is null
			or	(sysdate -	LoanCollatMapEndDt)			<= 0)</cfif>
order by	upper(old.LAC_Cond_Typ_Desc_Txt)
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

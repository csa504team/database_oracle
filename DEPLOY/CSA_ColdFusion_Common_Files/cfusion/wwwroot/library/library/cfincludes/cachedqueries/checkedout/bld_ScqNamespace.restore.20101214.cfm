<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Restores the caller's variables that are not in the Variables.Scq namespace. Please keep in sync 
					with the ".save.cfm" file. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs. 
INPUT:				Variables.ScqXXXX variables. 
					Request.Slaf variables (from get_sbashared_variables). 
OUTPUT:				Maybe Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), etc. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfif IsDefined("Variables.ScqDB")>
	<cfset Variables.db									= Variables.ScqDB>
<cfelse>
	<cfset StructDelete(Variables, "db")>
</cfif>
<cfif IsDefined("Variables.ScqDBType")>
	<cfset Variables.dbtype								= Variables.ScqDBType>
<cfelse>
	<cfset StructDelete(Variables, "dbtype")>
</cfif>
<cfif IsDefined("Variables.ScqSybase")>
	<cfset Variables.Sybase								= Variables.ScqSybase>
<cfelse>
	<cfset StructDelete(Variables, "Sybase")>
</cfif>
<cfset StructDelete(Variables, "ScqCoopTesting")>
<cfset StructDelete(Variables, "ScqDBDefault")>
<cfset StructDelete(Variables, "ScqGetFiles")>
<cfset StructDelete(Variables, "ScqLists")>
<cfset StructDelete(Variables, "ScqListKey")>
<cfset StructDelete(Variables, "ScqQueryName")>
<cfset StructDelete(Variables, "ScqSubstructureName")>
<!--- Leave Variables.Scq itself defined! That's the whole point! --->

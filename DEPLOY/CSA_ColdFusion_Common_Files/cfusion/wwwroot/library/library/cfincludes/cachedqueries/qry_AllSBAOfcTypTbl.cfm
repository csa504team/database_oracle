<!---
AUTHOR:				Sushil Inaganti, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				02/11/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	02/11/2016, SI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllSBAOfcTypTbl" datasource="#Variables.db#">
 select SBAOFCTYPCD       AS code,
        SBAOFCTYPCD,
        SBAOFCTYPABBVCD,
        SBAOFCTYPTXT       AS description,
        SBAOFCTYPTXT,
        SBAOFCTYPSTRTDT,
        SBAOFCTYPENDDT, 
        SBAOFCTYPCREATUSERID,    
        SBAOFCTYPCREATDT
        <cfif Variables.Sybase>
  from        sbaref..SBAOfcTypTbl<cfelse>
  from        sbaref.SBAOfcTypTbl</cfif>
  order by     code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

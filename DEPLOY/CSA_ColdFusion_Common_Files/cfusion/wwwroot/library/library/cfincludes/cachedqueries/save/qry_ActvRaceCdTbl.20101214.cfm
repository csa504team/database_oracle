<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvRaceCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		RaceCd,
			RaceTxt,
			RaceMFCd,
			RaceMFTxt,
			RaceCd										AS code,
			RaceTxt										AS description<cfif Variables.Sybase>
from		sbaref..RaceCdTbl
where		(	datediff(dd,RaceStrtDt,					getdate()) >= 0)
and			(				RaceEndDt					is null
			or	datediff(dd,RaceEndDt,					getdate()) <= 0)<cfelse>
from		sbaref.RaceCdTbl
where		(	(sysdate -	RaceStrtDt)					>= 0)
and			(				RaceEndDt					is null
			or	(sysdate -	RaceEndDt)					<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc., for the US Small Business Administration. 
DATE:				09/23/2014. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/23/2014, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvUCCFileTypCdTbl"	datasource="#Variables.db#">
select		UCCFileTypCd,
			UCCFileTypDescTxt,
			case UCCFileTypCd
			when 'O' then 1
			when 'R' then 2
			when 'A' then 3
			when 'C' then 4
			when 'D' then 5
			else null
			end											as displayorder,
			UCCFileTypCd								as code,
			UCCFileTypDescTxt							as description<cfif Variables.Sybase>
from		sbaref..UCCFileTypCdTbl
where		(	datediff(dd,UCCFileTypStrtDt,			getdate()) >= 0)
and			(				UCCFileTypEndDt				is null
			or	datediff(dd,UCCFileTypEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.UCCFileTypCdTbl
where		(	(sysdate -	UCCFileTypStrtDt)			>= 0)
and			(				UCCFileTypEndDt				is null
			or	(sysdate -	UCCFileTypEndDt)			<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCitznshipCdTbl"		datasource="#Variables.db#">
select		CitznshipCd,
			CitznshipTxt,
			CitznshipStrtDt,
			CitznshipEndDt,
			case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
			when CitznshipCd = 'US'						then 1
			when CitznshipCd = 'RA'						then 2
			when CitznshipCd = 'NR'						then 3
			when CitznshipCd = 'IA'						then 4
			else										5
			end											AS displayorder,
			CitznshipCd									AS code,
			CitznshipTxt								AS description<cfif Variables.Sybase>
from		sbaref..CitznshipCdTbl<cfelse>
from		sbaref.CitznshipCdTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

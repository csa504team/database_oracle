<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanMFDisastrTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanMFDisastrCd,
			LoanMFDisastrDescTxt,
			LoanMFDisastrCd								AS code,
			LoanMFDisastrDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..LoanMFDisastrTypTbl<cfelse>
from		sbaref.LoanMFDisastrTypTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

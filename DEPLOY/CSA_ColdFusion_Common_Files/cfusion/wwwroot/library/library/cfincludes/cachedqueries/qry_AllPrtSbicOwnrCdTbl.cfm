<!---
AUTHOR:				Elena Spratling. 
DATE:				06/10/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					06/10/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllPrtSbicOwnrCdTbl"				datasource="#Variables.db#">
select		PRTSBICOWNRCD			AS code,
			PRTSBICOWNRDESC		AS description<cfif Variables.Sybase>
from		sbaref..PRTSBICOWNRCDTBL<cfelse>
from		sbaref.PRTSBICOWNRCDTBL</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">



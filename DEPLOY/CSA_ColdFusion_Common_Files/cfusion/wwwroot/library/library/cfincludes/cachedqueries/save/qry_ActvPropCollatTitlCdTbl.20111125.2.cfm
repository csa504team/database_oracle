<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropCollatTitlCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropCollatTitlCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	PropCollatTitlCd,
	    PropCollatTitlDescTxt,
	    PropCollatTitlCd									AS code,
		PropCollatTitlDescTxt								AS description <cfif Variables.Sybase>
	   from	sbaref..PropCollatTitlCdTbl
	   where (datediff(dd,PropCollatTitlStrtDt,				getdate()) >= 0)
		   and	(PropCollatTitlEndDt						is null
		   or datediff(dd,PropCollatTitlEndDt,				getdate()) <= 0)<cfelse>
	   from	sbaref.PropCollatTitlCdTbl
       where		((sysdate -	PropCollatTitlStrtDt)		>= 0)
       		and		(PropCollatTitlEndDt					is null
			or	(sysdate -	PropCollatTitlEndDt)			<= 0)</cfif> 	
       order by code	
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
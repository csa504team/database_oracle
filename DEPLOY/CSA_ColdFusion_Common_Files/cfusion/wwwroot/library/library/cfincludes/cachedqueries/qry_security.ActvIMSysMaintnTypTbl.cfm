<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/11/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/11/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMSysMaintnTypTbl"	datasource="#Variables.db#">
select		IMSysMaintnTypCd,
			IMSysMaintnTypDescTxt,
			IMSysMaintnTypCd								as code,
			IMSysMaintnTypDescTxt							as description,
			upper(IMSysMaintnTypDescTxt)					as displayorder<cfif Variables.Sybase>
from		security..IMSysMaintnTypTbl
where		(	datediff(dd,IMSysMaintnTypStrtDt,			getdate()) >= 0)
and			(				IMSysMaintnTypEndDt				is null
			or	datediff(dd,IMSysMaintnTypEndDt,			getdate()) <= 0)<cfelse>
from		security.IMSysMaintnTypTbl
where		(	(sysdate -	IMSysMaintnTypStrtDt)			>= 0)
and			(				IMSysMaintnTypEndDt				is null
			or	(sysdate -	IMSysMaintnTypEndDt)			<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

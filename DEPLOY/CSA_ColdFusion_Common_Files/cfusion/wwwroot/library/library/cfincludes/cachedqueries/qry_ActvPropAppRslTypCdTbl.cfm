<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NNI:	Original implementation.
								NS:		Changed PropAppRslTypDesc to PropAppRslTypDescTxt
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPropAppRslTypCdTbl"			datasource="#Variables.db#">
	select		PropAppRslTypCd,
				PropAppRslTypDescTxt,
				PropAppRslTypCd									AS code,
				PropAppRslTypDescTxt							AS description<cfif Variables.Sybase>
	from		sbaref..PropAppRslTypCdTbl
	where		(	datediff(dd,PropAppRslTypStrtDt,			getdate()) >= 0)
	and			(				PropAppRslTypEndDt				is null
				or	datediff(dd,PropAppRslTypEndDt,				getdate()) <= 0)<cfelse>
	from		sbaref.PropAppRslTypCdTbl
	where		(	(sysdate -	PropAppRslTypStrtDt)			>= 0)
	and			(				PropAppRslTypEndDt				is null
				or	(sysdate -	PropAppRslTypEndDt)				<= 0)</cfif>
	order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

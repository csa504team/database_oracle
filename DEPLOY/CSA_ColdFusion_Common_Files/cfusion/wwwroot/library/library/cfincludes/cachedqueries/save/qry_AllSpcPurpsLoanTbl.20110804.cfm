<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/04/2011, NNI:	Added RecovInd, SpcpurpsLoanStrtDt and SpcpurpsLoanEndDt to output. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllSpcPurpsLoanTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		SpcPurpsLoanCd,
			SpcPurpsLoanDesc,
			SpcPrgrmCd,
			RecovInd,
			SpcpurpsLoanStrtDt,
			SpcpurpsLoanEndDt,
			SpcPurpsLoanCd								AS code,
			SpcPurpsLoanDesc							AS description<cfif Variables.Sybase>
from		sbaref..SpcPurpsLoanTbl<cfelse>
from		sbaref.SpcPurpsLoanTbl</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

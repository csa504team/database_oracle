<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMEPCOperCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMEPCOperCd,
			IMEPCOperDescTxt,
			IMEPCOperCd									AS code,
			IMEPCOperDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..IMEPCOperCdTbl
where		(	datediff(dd,IMEPCOperCdStrtDt,			getdate()) >= 0)
and			(				IMEPCOperCdEndDt			is null
			or	datediff(dd,IMEPCOperCdEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.IMEPCOperCdTbl
where		(	(sysdate -	IMEPCOperCdStrtDt)			>= 0)
and			(				IMEPCOperCdEndDt			is null
			or	(sysdate -	IMEPCOperCdEndDt)			<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

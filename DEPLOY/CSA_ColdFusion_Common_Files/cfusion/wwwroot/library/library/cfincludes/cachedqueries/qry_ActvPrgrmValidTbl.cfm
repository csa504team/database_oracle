<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<!--- Because this table is never displayed, it doesn't have code and description columns. --->
<cfquery name="Variables.Scq.ActvPrgrmValidTbl"			datasource="#Variables.db#">
select		PrgrmCd,
			PrcsMthdCd,
			SpcPurpsLoanCd,
			ProcdTypCd,
			SubPrgrmMFCd,
			LoanMFSpcPurpsCd,
			LoanMFPrcsMthdCd,
			LoanMFSTARInd,
			LoanMFDisastrCd<cfif Variables.Sybase>
from		sbaref..PrgrmValidTbl
where		(	datediff(dd,PrgrmValidStrtDt,			getdate()) >= 0)
and			(				PrgrmValidEndDt				is null
			or	datediff(dd,PrgrmValidEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.PrgrmValidTbl
where		(	(sysdate -	PrgrmValidStrtDt)			>= 0)
and			(				PrgrmValidEndDt				is null
			or	(sysdate -	PrgrmValidEndDt)			<= 0)</cfif>
order by	PrgrmValidSeqNmb
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

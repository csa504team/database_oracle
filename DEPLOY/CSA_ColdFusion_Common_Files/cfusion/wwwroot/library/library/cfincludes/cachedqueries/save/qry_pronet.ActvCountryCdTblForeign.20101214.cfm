<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.pronet.ActvCountryCdTblForeign"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CountryCd									code,
			CountryNm									description<cfif Variables.Sybase>
from		pronet..CountryCdTbl<cfelse>
from		viewpronet.CountryCdTbl</cfif>
where		DomForInd									> 0 /* indicating foreign */
and			PubBanInd									= 0
and			PvtBanInd									= 0
order by	CountryNm
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

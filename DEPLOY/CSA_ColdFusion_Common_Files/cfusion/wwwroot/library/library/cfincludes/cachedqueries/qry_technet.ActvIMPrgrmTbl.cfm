<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.technet.ActvIMPrgrmTbl"	datasource="#Variables.db#">
select		IMPrgrmId,
			upper(IMPrgrmDescTxt)						IMPrgrmDescTxt<cfif Variables.Sybase>
from		technet..IMPrgrmTbl<cfelse>
from		technet.IMPrgrmTbl</cfif>
order by	IMPrgrmId
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

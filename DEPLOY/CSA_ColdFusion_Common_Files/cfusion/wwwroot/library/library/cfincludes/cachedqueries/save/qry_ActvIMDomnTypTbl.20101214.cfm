<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMDomnTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select 		IMDomnTypCd									AS code,
			IMDomnTypDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..IMDomnTypTbl
where 		(	datediff(dd,IMDomnTypStrtDt,			getdate()) >= 0)
and			(				IMDomnTypEndDt				is null
			or	datediff(dd,IMDomnTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.IMDomnTypTbl
where		(	(sysdate -	IMDomnTypStrtDt)			>= 0)
and			(				IMDomnTypEndDt				is null
			or	(sysdate -	IMDomnTypEndDt)				<= 0)</cfif>
order by	IMDomnTypCd
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

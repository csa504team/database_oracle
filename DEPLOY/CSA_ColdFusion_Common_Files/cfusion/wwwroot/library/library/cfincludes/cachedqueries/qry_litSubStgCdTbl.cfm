<!---
AUTHOR:				Durga Gadela, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/28/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/28/2016, DG:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.LitSubStgCdTbl" datasource="#Variables.db#">
select		LitSubStgCd,
			LitSubStgDescTxt, 
			LitSubStgStrtDt,
			LitSubStgEndDt,
			CreatUserId,
			CreatDt,
			LitSubStgCd			as code,
			LitSubStgDescTxt	as description
from		sbaref.LitSubStgCdTbl
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
					11/27/2011, NS: changed order by to code
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllLoanCollatTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select		LoanCollatTypCd,
				LoanCollatTypDescTxt,
				LoanCollatLqdPct,
				LoanCollatTypStrtDt,
				LoanCollatTypEndDt,
				LoanCollatTypCd								AS code,
				LoanCollatTypDescTxt						AS description<cfif Variables.Sybase>
	from		sbaref..LoanCollatTypCdTbl
	<cfelse>
	from		sbaref.LoanCollatTypCdTbl
	</cfif>
	order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Chitralekha Pustakala, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/30/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/13/2016, PCL:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvRePayInstlTypCdTbl" datasource="#Variables.db#">
select		RePayInstallTypCd,
		    RePayInstallTypDesc,
     		RePayInstAllTypStrtDt,
		    RePayInstallTypEndDt,
		    CreatUserId,
		    CreatDt,
			RePayInstallTypCd		as code,
		    RePayInstallTypDesc		as description<cfif Variables.Sybase>
from		sbaref..RePayInstlTypCdTbl
where		(	datediff(dd,RePayInstAllTypStrtDt,				getdate()) >= 0)
and			(				RePayInstallTypEndDt				is null
			or	datediff(dd,RePayInstallTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.RePayInstlTypCdTbl
where		(	(sysdate -	RePayInstAllTypStrtDt)				>= 0)
and			(				RePayInstallTypEndDt				is null
			or	(sysdate -	RePayInstallTypEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc, for the US Small Business Administration. 
DATE:				09/23/2014. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/23/2014, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanBusApprCdTbl"	datasource="#Variables.db#">
select		LoanBusApprCd,
			LoanBusApprDescTxt,
            LoanBusApprCd								code,
			LoanBusApprDescTxt							description<cfif Variables.Sybase>
from		sbaref..LoanBusApprCdTbl
where		(	datediff(dd,LoanBusApprCdStrtDt,			getdate()) >= 0)
and			(				LoanBusApprCdEndDt				is null
			or	datediff(dd,LoanBusApprCdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanBusApprCdTbl
where		(	(sysdate -	LoanBusApprCdStrtDt)			>= 0)
and			(				LoanBusApprCdEndDt				is null
			or	(sysdate -	LoanBusApprCdEndDt)				<= 0)</cfif>
order by	LoanBusApprCd
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

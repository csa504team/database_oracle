<!---
AUTHOR:				Chitralekha Pustakala, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/30/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/13/2016, PCL:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvPropApprslTypCdTbl" datasource="#Variables.db#">
select		PropApprslTypCd,
		    PropApprslTypDescTxt,
     		PropApprslTypStrtDt,
		    PropApprslTypEndDt,
		    CreatUserId,
		    CreatDt,
			PropApprslTypCd			as code,
		    PropApprslTypDescTxt	as description<cfif Variables.Sybase>
from		sbaref..PropApprslTypCdTbl
where		(	datediff(dd,PropApprslTypStrtDt,				getdate()) >= 0)
and			(				PropApprslTypEndDt				is null
			or	datediff(dd,PropApprslTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.PropApprslTypCdTbl
where		(	(sysdate -	PropApprslTypStrtDt)				>= 0)
and			(				PropApprslTypEndDt				is null
			or	(sysdate -	PropApprslTypEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

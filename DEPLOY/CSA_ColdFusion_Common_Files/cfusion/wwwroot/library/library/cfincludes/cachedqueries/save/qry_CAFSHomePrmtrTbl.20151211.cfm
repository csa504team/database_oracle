<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.CAFSHomePrmtrTbl" datasource="#Variables.db#">
SELECT a.IMPrmtrId,
       b.IMPrmtrValTxt,
       a.IMPrmtrId as code,
       IMPrmtrValTxt as description,
       IMPrmtrValLrgTxt as longdescription,
       b.IMPrmtrValEndDt
 FROM sbaref.IMPrmtrTbl a, sbaref.IMPrmtrValTbl b
 WHERE a.IMPrmtrId IN ('BrwsrVrsn','CAFSHomeNews','CAFSEnvRqmt')
       AND a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
       AND IMPrmtrValStrtDt <= SYSDATE
       AND (IMPrmtrValEndDt IS NULL OR SYSDATE < IMPrmtrValEndDt + 1)                                         
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

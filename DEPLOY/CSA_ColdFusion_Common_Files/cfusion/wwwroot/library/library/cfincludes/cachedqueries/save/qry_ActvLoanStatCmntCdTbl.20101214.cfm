<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanStatCmntCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanStatCmntCd,
			LoanStatCdCmntDesc,
			LoanStatCmntCd								AS code,
			LoanStatCdCmntDesc							AS description<cfif Variables.Sybase>
from		sbaref..LoanStatCmntCdTbl
where		(	datediff(dd,LoanStatCmntCdStrtDt,		getdate()) >= 0)
and			(				LoanStatCmntCdEndDt			is null
			or	datediff(dd,LoanStatCmntCdEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.LoanStatCmntCdTbl
where		(	(sysdate -	LoanStatCmntCdStrtDt)		>= 0)
and			(				LoanStatCmntCdEndDt			is null
			or	(sysdate -	LoanStatCmntCdEndDt)		<= 0)</cfif>
order by	LoanStatCmntCd
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

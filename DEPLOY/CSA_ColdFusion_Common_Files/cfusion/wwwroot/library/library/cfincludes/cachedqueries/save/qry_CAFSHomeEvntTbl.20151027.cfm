<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.CAFSHomeEvntTbl" datasource="#Variables.db#">
SELECT EVNTNM,
       EVNTDESCTXT,
       EVNTSTRTDT,
       EVNTENDDT,
       EVNTNM as code,
       EVNTDESCTXT as description
 FROM SBAREF.EVNTTBL
 WHERE TRUNC(NVL(EVNTENDDT,EVNTSTRTDT))>= trunc(sysdate)
 ORDER BY EVNTSTRTDT
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

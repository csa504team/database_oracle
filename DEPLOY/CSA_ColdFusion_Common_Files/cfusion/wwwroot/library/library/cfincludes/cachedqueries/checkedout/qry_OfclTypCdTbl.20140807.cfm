<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/28/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/28/2011, SRv:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.OfclTypCdTbl" datasource="#Variables.db#">
select	    OfclTypCd,
      		OfclTypDescTxt,
     		OfclTypStrtDt ,
      		OfclTypEndDt,
      		CreatUserId,
      		CreatDt,
        	OfclTypCd			as code,
        	OfclTypDescTxt		as description<cfif Variables.Sybase>
from		sbaref..OfclTypCdTbl<cfelse>
from		sbaref.OfclTypCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

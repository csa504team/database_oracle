<!---
AUTHOR:				Robert Doyle
DATE:				03/01/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. In this instance, for the front door photo gallery.
INPUT:				Variables.db, Variables.dbtype, ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	03/17/2016, RD:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.object.ActvDocTbl" datasource="#Variables.db#">
 select     a.DOCID,
            UTL_COMPRESS.lz_uncompress(a.DOCData) DOCData,
            b.DocNm
        from SecurityDocs.DocFileUploadTbl a, SecurityDocs.DocTbl b
        where a.DocId = b.DocID
        and DOCStrtDt <= SYSDATE
        and SYSDATE < NVL(DOCEndDt,SYSDATE) + 1
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
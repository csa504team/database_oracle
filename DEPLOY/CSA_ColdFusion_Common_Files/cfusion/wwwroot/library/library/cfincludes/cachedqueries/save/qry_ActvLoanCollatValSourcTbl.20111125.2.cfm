<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NNI:	Original implementation. 
								NS:		Changed LoanCollatValSourcDesc to LoanCollatValSourcDescTxt
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanCollatValSourcTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanCollatValSourcCd,
			LoanCollatValSourcDescTxt,
			LoanCollatValSourcCd									AS code,
			LoanCollatValSourcDescTxt								AS description <cfif Variables.Sybase>
from		sbaref..LoanCollatValSourcTbl
where		(	datediff(dd,LoanCollatValSourcStrtDt,				getdate()) >= 0)
and			(				LoanCollatValSourcEndDt					is null
			or	datediff(dd,LoanCollatValSourcEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanCollatValSourcTbl
where		(	(sysdate -	LoanCollatValSourcStrtDt)				>= 0)
and			(				LoanCollatValSourcEndDt					is null
			or	(sysdate -	LoanCollatValSourcEndDt)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

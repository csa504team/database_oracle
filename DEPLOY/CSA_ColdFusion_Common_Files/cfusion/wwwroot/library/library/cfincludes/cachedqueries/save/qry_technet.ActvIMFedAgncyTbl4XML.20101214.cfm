<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl4XML"	datasource="#db#" dbtype="#dbtype#">
select		IMFedAgncySeqNmb,
			IMFedAgncyNm,
			IMFedAgncyNm								code,<!--- XML uses letter codes --->
			IMFedAgncyNm								description<cfif Variables.Sybase>
from		technet..IMFedAgncyTbl<cfelse>
from		technet.IMFedAgncyTbl</cfif>
where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
and			IMFedAgncyActvInactvInd						 = 0
and			IMFedAgncyNm								not in ('DOI','NRC')<!--- No longer submitting awards. --->
order by	IMFedAgncyNm
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					08/04/2011, NNI:	Added RecovInd, SpcpurpsLoanStrtDt and SpcpurpsLoanEndDt to output. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSpcPurpsLoanTbl"		datasource="#Variables.db#">
select		SpcPurpsLoanCd,
			SpcPurpsLoanDesc,
			SpcPrgrmCd,
			RecovInd,
			SpcpurpsLoanStrtDt,
			SpcpurpsLoanEndDt,
			SpcPurpsLoanCd								AS code,
			SpcPurpsLoanDesc							AS description<cfif Variables.Sybase>
from		sbaref..SpcPurpsLoanTbl
where		(	datediff(dd,SpcPurpsLoanStrtDt,			getdate()) >= 0)
and			(				SpcPurpsLoanEndDt			is null
			or	datediff(dd,SpcPurpsLoanEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.SpcPurpsLoanTbl
where		(	(sysdate -	SpcPurpsLoanStrtDt)			>= 0)
and			(				SpcPurpsLoanEndDt			is null
			or	(sysdate -	SpcPurpsLoanEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

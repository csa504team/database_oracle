<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSrvrTbl"				datasource="#Variables.db#">
select		distinct
			rtrim(s.SrvrNm)								as SrvrNm,
			s.InstanceNm,
			s.InstanceId,
			s.SrvrGrpTxt,
			case s.GLSInd
			when 'Y'	then							'Yes'
			else										'No'
			end											as CLSInd,
			case s.LognInd
			when 'Y'	then							'Yes'
			else										'No'
			end											as LognInd,
			rtrim(s.EnvCd)								as EnvCd,
			rtrim(e.EnvTypCd)							as EnvTypCd,
			case EnvTypCd
			when 'Dev'	then							0
			when 'Test'	then							1
			else										2
			end											as DevTestProdInd,
			e.LognURLTxt,<cfif Variables.Sybase>
			rtrim(s.SrvrNm)+'.'+s.InstanceNm			as code,		-- likely to change
			rtrim(s.SrvrNm)+'.'+s.InstanceNm			as description	-- likely to change
from						sbaref..SrvrTbl				s
			left outer join	sbaref..EnvTbl				e on (s.EnvCd = e.EnvCd)
where		(	datediff(dd,s.SrvrStrtDt,getdate())		>= 0)
and			(				s.SrvrEndDt					is null
			or	datediff(dd,s.SrvrEndDt, getdate())		<= 0)<cfelse>
			rtrim(s.SrvrNm)||'.'||s.InstanceNm			as code,		-- likely to change
			rtrim(s.SrvrNm)||'.'||s.InstanceNm			as description	-- likely to change
from						sbaref.SrvrTbl				s
			left outer join	sbaref.EnvTbl				e on (s.EnvCd = e.EnvCd)
where		(	(sysdate -	s.SrvrStrtDt)				>= 0)
and			(				s.SrvrEndDt					is null
			or	(sysdate -	s.SrvrEndDt)				<= 0)</cfif>
order by	SrvrGrpTxt,
			SrvrNm,
			InstanceNm
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

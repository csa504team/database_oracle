<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				04/11/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					04/11/2012, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanSchdlTypTbl"		datasource="#Variables.db#">
select		LoanSchdlTypCd,
			LoanSchdlTypTxt,
			LoanSchdlTypCd								AS code,
			LoanSchdlTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanSchdlTypTbl
where		(	datediff(dd,LoanSchdlTypStrtDt,			getdate()) >= 0)
and			(				LoanSchdlTypEndDt			is null
			or	datediff(dd,LoanSchdlTypEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanSchdlTypTbl
where		(	(sysdate -	LoanSchdlTypStrtDt)			>= 0)
and			(				LoanSchdlTypEndDt			is null
			or	(sysdate -	LoanSchdlTypEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

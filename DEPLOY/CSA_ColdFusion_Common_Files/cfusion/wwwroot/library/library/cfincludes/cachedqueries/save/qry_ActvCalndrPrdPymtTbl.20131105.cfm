<!---
AUTHOR:				Robert Doyle, Solutions By Design, Inc., for the US Small Business Administration. 
DATE:				11/04/2013
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/04/2013, RSD:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCalndrPrdPymtTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select	CalndrPrdPymtCd,
		CalndrPrdPymtDesc,
		CalndrPrdPymtOrd,
		CalndrPrdPymtCd							AS code,
		CalndrPrdPymtDesc							AS description
from		sbaref.CalndrPrdPymtTbl
where		(	(sysdate -	CalndrPrdPymtStrtDt)			>= 0)
and			(				CalndrPrdPymtEndDt				is null
			or	(sysdate -	CalndrPrdPymtEndDt)				<= 0)
order by	CalndrPrdPymtOrd       
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
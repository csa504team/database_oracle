<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation.
								NS:		Changed PropCondDesc to PropCondCDDescTxt, prefix PropCond changed to PropCondCd
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvPropCondCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select		PropCondCd,
				PropCondCdDescTxt,
				PropCondCd										AS code,
				PropCondCdDescTxt								AS description <cfif Variables.Sybase>
	from		sbaref..PropCondCdTbl
	where		(	datediff(dd,PropCondCdStrtDt,				getdate()) >= 0)
	and			(				PropCondCdEndDt					is null
				or	datediff(dd,PropCondCdEndDt,				getdate()) <= 0)<cfelse>
	from		sbaref.PropCondCdTbl
	where		(	(sysdate -	PropCondCdStrtDt)				>= 0)
	and			(				PropCondCdEndDt					is null
				or	(sysdate -	PropCondCdEndDt)				<= 0)</cfif>
	order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

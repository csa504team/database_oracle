<!---
AUTHOR:				Steve Seaquist, Alpha Omega Integration, Inc., for the US Small Business Administration. 
DATE:				09/08/2017. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/08/2017, SRS:	Original implementation. Cannibalized IMRtTypSelTSP Identifier 4. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMRtTypTbl"			datasource="#Variables.db#">
select		distinct
			IMRtTypCd,
			IMRtTypDescTxt,
			IMIntRtLkupURL,
			IMRtTypCd									as code,
			IMRtTypDescTxt								as description
from		sbaref.IMRtTypTbl
where		IMRtTypStrtDt								<= sysdate
and			(	IMRtTypEndDt							is null
			or	IMRtTypEndDt							>  sysdate
			)
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

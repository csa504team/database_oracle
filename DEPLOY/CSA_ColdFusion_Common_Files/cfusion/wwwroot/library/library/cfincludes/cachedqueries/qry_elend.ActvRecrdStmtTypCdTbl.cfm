<!---
AUTHOR:				Chitralekha Pustakala, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				09/28/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/28/2016, PCL:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvRecrdStmtTypCdTbl"					datasource="#Variables.db#">
select		RecrdStmtTypCd,
			RecrdStmtTypDescTxt
			RecrdStmtTypCdStrtDt,
			RecrdStmtTypCdEndDt,
            CreatUserId,
		    CreatDt,
			RecrdStmtTypCd										AS code,
			RecrdStmtTypDescTxt									AS description<cfif Variables.Sybase>
from		sbaref..RecrdStmtTypCdTbl
where		(	datediff(dd,RecrdStmtTypCdStrtDt,				getdate()) >= 0)
and			(				RecrdStmtTypCdEndDt				is null
			or	datediff(dd,RecrdStmtTypCdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.RecrdStmtTypCdTbl
where		(	(sysdate -	RecrdStmtTypCdStrtDt)				>= 0)
and			(				RecrdStmtTypCdEndDt				is null
			or	(sysdate -	RecrdStmtTypCdEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">


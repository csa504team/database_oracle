<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvEnvTypTbl"				datasource="#Variables.db#">
select		rtrim(EnvTypCd)								as EnvTypCd,
			EnvTypDescTxt,
			rtrim(EnvTypCd)								as code,
			EnvTypDescTxt								as description,
			case EnvTypCd
			when 'Dev'	then							0
			when 'Test'	then							1
			else										2
			end											as DevTestProdInd<cfif Variables.Sybase>
from		sbaref..EnvTypTbl
where		(	datediff(dd,EnvTypStrtDt,				getdate()) >= 0)
and			(				EnvTypEndDt					is null
			or	datediff(dd,EnvTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.EnvTypTbl
where		(	(sysdate -	EnvTypStrtDt)				>= 0)
and			(				EnvTypEndDt					is null
			or	(sysdate -	EnvTypEndDt)				<= 0)</cfif>
order by	DevTestProdInd
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

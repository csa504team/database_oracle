<!---
AUTHOR:				Nirish Namilae. 
DATE:				09/08/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/08/2011, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllLoanSchdlTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanSchdlTypCd,
			LoanSchdlTypTxt,
			LoanSchdlTypStrtDt,
			LoanSchdlTypEndDt,
			LoanSchdlTypCd								AS code,
			LoanSchdlTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..LoanSchdlTypTbl<cfelse>
from		sbaref.LoanSchdlTypTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

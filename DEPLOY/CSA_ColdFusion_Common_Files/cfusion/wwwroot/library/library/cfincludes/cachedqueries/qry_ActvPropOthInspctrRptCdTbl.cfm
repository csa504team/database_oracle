<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropOthInspctrRptCdTbl)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropOthInspctrRptCdTbl" datasource="#Variables.db#">
	  SELECT
      	PropOthInspctrRptCd,
      	PropOthInspctrRptDescTxt,
		PropOthInspctrRptCd										AS code,
		PropOthInspctrRptDescTxt								AS description <cfif Variables.Sybase>	  
	   from	sbaref..PropOthInspctrRptCdTbl
	   where (datediff(dd,PropOthInspctrRptStrtDt,				getdate()) >= 0)
		   and	(PropOthInspctrRptEndDt							is null
		   or datediff(dd,PropOthInspctrRptEndDt,				getdate()) <= 0)<cfelse>
	   from	sbaref.PropOthInspctrRptCdTbl
       where		((sysdate -	PropOthInspctrRptStrtDt)		>= 0)
       		and		(PropOthInspctrRptEndDt	is null
			or	(sysdate -	PropOthInspctrRptEndDt)				<= 0)</cfif>
       order by code	  
</cfquery>

<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

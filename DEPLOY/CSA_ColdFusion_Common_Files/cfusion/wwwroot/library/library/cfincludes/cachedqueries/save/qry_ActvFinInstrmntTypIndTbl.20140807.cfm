<!---
AUTHOR:				Elena Spratling. 
DATE:				08/07/2014
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, EAS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvFinInstrmntTypIndTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">

select FININSTRMNTTYPIND		AS code,
	   FININSTRMNTTYPDESCTXT	AS description 	<cfif Variables.Sybase>
from   sbaref..fininstrmnttypindtbl
where		(	datediff(dd,FININSTRMNTTYPSTRTDT,		getdate()) >= 0)
and			(				FININSTRMNTTYPENDDT				is null
			or	datediff(dd,FININSTRMNTTYPENDDT,	getdate()) <= 0)<cfelse>
from		sbaref.fininstrmnttypindtbl
where		(	(sysdate -	FININSTRMNTTYPSTRTDT)			>= 0)
and			(				FININSTRMNTTYPENDDT				is null
			or	(sysdate -	FININSTRMNTTYPENDDT)				<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">





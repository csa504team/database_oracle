<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.pronet.ActvCCRBusTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select			c.CCRBusTypCd							code,
				c.CCRBusTypDescTxt						description,
				c.CCRBusTypCd,
				c.CCRBusTypDescTxt,
				c.IMMinCd,
				case
				when c.CCRBusTypCd in ('05','1B','8U','HK')	/* ANC-, Tribal-, NHO- and CDC-owned, respectively. */
											then		c.CCRBusTypDescTxt
				when c.IMMinCd is not null	then		m.IMMinDescTxt
				else									null
				end										as DSBSSearchName,
				c.BusStatInactRsnCd,
				c.CCRBusTypExpt,
				c.IMOrgTypCd<cfif Variables.Sybase>
from			pronet..CCRBusTypCdTbl					c
left outer join	pronet..IMMinCdTbl						m on (c.IMMinCd = m.IMMinCd)<cfelse>
from			viewpronet.CCRBusTypCdTbl				c
left outer join	viewpronet.IMMinCdTbl					m on (c.IMMinCd = m.IMMinCd)</cfif>
order by		DSBSSearchName,
				c.CCRBusTypCd
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<cfset Variables.ScqLists								= StructNew()>
<!--- Keep the following struct key names in sync with values of DSBS's OtherOwnershipCheckboxValues: --->
<cfloop index="ScqListKey"								list="Min,Nat,NatTri,NatAla,NatHaw,NatOth,MinOth,CDC,SCS,SDV,Vet,Wom,QMS">
	<cfset Variables.ScqLists[ScqListKey]				= "">
</cfloop>
<cfloop query="Variables.Scq.pronet.ActvCCRBusTypCdTbl">
	<cfif Len(IMMinCd) GT 0>
		<cfswitch expression="#UCase(IMMinCd)#">
		<cfcase value="NAT">
			<cfset Variables.ScqLists.Min				= ListAppend(Variables.ScqLists.Min,			"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.Nat				= ListAppend(Variables.ScqLists.Nat,			"'#CCRBusTypCd#'")>
			<cfswitch expression="#CCRBusTypCd#">
			<!---
			Because of the one-to-many relationship from IMMinCd to CCRBusTypCd (as opposed to many-to-many), 
			Tribally-Owned, Alaskan Native Corp Owned, Native Hawaiian Org Owned codes have to be hard-coded 
			for now. Technically, all 3 are tribally-owned, but ANC and NHO aren't called tribes. 
			--->
			<cfcase value="1B"><cfset Variables.ScqLists.NatTri	= ListAppend(Variables.ScqLists.NatTri,	"'1B'")></cfcase>
			<cfcase value="05"><cfset Variables.ScqLists.NatAla	= ListAppend(Variables.ScqLists.NatAla,	"'05'")></cfcase>
			<cfcase value="8U"><cfset Variables.ScqLists.NatHaw	= ListAppend(Variables.ScqLists.NatHaw,	"'8U'")></cfcase>
			<cfdefaultcase>	   <cfset Variables.ScqLists.NatOth	= ListAppend(Variables.ScqLists.NatOth,	"'#CCRBusTypCd#'")></cfdefaultcase>
			</cfswitch>
		</cfcase>
		<cfcase value="SCS">
			<cfset Variables.ScqLists[IMMinCd]			= ListAppend(Variables.ScqLists[IMMinCd],		"'#CCRBusTypCd#'")>
		</cfcase>
		<cfcase value="SDV,VET,WOM"><!--- That is, if minority code participates in Quick Market Search. --->
			<cfset Variables.ScqLists[IMMinCd]			= ListAppend(Variables.ScqLists[IMMinCd],		"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.QMS				= ListAppend(Variables.ScqLists.QMS,			"'#CCRBusTypCd#'")>
		</cfcase>
		<cfdefaultcase>
			<cfset Variables.ScqLists.Min				= ListAppend(Variables.ScqLists.Min,			"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.MinOth			= ListAppend(Variables.ScqLists.MinOth,			"'#CCRBusTypCd#'")>
		</cfdefaultcase>
		</cfswitch>
	<cfelseif CCRBusTypCd IS "HK"><!--- A CDC is not necessarily a minority and has no minority code. --->
		<cfset Variables.ScqLists.CDC					= ListAppend(Variables.ScqLists.CDC,			"'HK'")>
	</cfif>
</cfloop>
<cfset Variables.Scq.pronet["CbtLists"]					= Duplicate(Variables.ScqLists)>
<cfset StructDelete(Variables, "ScqLists")>

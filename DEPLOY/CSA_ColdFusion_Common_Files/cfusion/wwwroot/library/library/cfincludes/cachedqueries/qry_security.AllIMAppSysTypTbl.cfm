<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/10/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2015, SRS:	Original implementation. First query for CacheQryCd "security". 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.AllIMAppSysTypTbl"	datasource="#Variables.db#">
select		IMAppSysTypCd,
			IMAppSysTypNm,
			IMAppSysTypDescTxt,
			IMDBPltfrmTypCd,
			IMAppURLSeqNmb,
			IMAppSysPOEMailAdrTxt,
			IMAllowMultiOfcInd,
			IMAllowMultiLocInd,
			IMAllowMultiRoleInd,
			IMAgencyAppId,
			IMAppSysTypStrtDt,
			IMAppSysTypEndDt,
			SrvrLocCd,
			IMAppSysTypDescTxt								AS displayorder,
			IMAppSysTypNm									AS code,
			IMAppSysTypDescTxt								AS description<cfif Variables.Sybase>
from		security..IMAppSysTypTbl<cfelse>
from		security.IMAppSysTypTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

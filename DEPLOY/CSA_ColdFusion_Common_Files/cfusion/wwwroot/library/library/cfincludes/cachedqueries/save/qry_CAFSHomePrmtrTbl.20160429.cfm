<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	04/24/2016, SMJ:	Changed the sort order
					10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.CAFSHomePrmtrTbl" datasource="#Variables.db#">
select  a.IMPrmtrId,
        b.IMPrmtrValTxt,
        a.IMPrmtrId as code,
        IMPrmtrValTxt as description,
        IMPrmtrValLrgTxt as longdescription,
        b.IMPrmtrValEndDt,
        case a.ImPrmtrId
            when 'CAFSHomeNews' then 1
            when 'BrwsrVrsn'    then 2
            when 'CAFSEnvRqmt'  then 3
            else 4
        end as priority
from sbaref.IMPrmtrTbl a, sbaref.IMPrmtrValTbl b
where a.IMPrmtrId in ('BrwsrVrsn','CAFSHomeNews','CAFSEnvRqmt')
        and a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
        and IMPrmtrValStrtDt <= SYSDATE
        and (IMPrmtrValEndDt is null OR SYSDATE < IMPrmtrValEndDt + 1)
order by    priority
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

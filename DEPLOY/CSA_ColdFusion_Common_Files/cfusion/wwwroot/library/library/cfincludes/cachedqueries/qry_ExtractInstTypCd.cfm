<!---
AUTHOR:				Vidya Rajan
DATE:				09/13/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/13/2016, VR:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname			= "Variables.Scq.ExtractInstTypCd">
<cfset Variables.Identifier			= "4">
<cfparam name="Variables.username"	default="">
<cfparam name="Variables.password"	default="">
<cfinclude template="/cfincludes/oracle/loan/spc_LAC_INST_TYP_CD_SELTSP.cfm" />
<cfif Variables.TxnErr>
<cfset Variables.Scq.ExtractInstTypCd = Variables.ErrMsg />
</cfif>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

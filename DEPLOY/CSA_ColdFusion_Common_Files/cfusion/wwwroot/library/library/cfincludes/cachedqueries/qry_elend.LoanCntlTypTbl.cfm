<!---
AUTHOR:				Sherry Liu
DATE:				08/11/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	
--->

<cfinclude template="bld_preprocessing.cfm">

<cfset Variables.cfprname			= "Variables.Scq.LoanCntlTypTbl">
<cfset Variables.Identifier			= "4">
<cfinclude template="/cfincludes/oracle/sbaref/spc_LOANCNTLINTTYPCDSELTSP.PUBLIC.cfm" />

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

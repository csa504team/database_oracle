<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NNI:	Original implementation.
								NS:  Fixed order by
--->


<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCollatPurTypCdTbl"	datasource="#Variables.db#">
	select		CollatPurTypCd,
				CollatPurTypDescTxt,
				CollatPurTypStartDt,
				CollatPurTypEndDt,
				CollatPurTypCd									AS code,
				CollatPurTypDescTxt								AS description<cfif Variables.Sybase>
	from		sbaref..CollatPurTypCdTbl<cfelse>
	from		sbaref.CollatPurTypCdTbl</cfif>
	order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

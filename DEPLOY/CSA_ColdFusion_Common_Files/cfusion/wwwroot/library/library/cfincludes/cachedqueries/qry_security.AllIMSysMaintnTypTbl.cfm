<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/11/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/11/2015, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.AllIMSysMaintnTypTbl"	datasource="#Variables.db#">
select		IMSysMaintnTypCd,
			IMSysMaintnTypDescTxt,
			IMSysMaintnTypStrtDt,
			IMSysMaintnTypEndDt,
			IMSysMaintnTypCd								as code,
			IMSysMaintnTypDescTxt							as description,
			upper(IMSysMaintnTypDescTxt)					as displayorder<cfif Variables.Sybase>
from		security..IMSysMaintnTypTbl<cfelse>
from		security.IMSysMaintnTypTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Nirish Namilae. 
DATE:				02/02/2012
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	02/02/2012, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanPymtTransCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanPymtTransCd,
			LoanPymtTransDescTxt,
			LoanPymtTransCd									AS code,
			LoanPymtTransDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..LoanPymtTransCdTbl
where		(	datediff(dd,LoanPymtTransStrtDt,				getdate()) >= 0)
and			(				LoanPymtTransEndDt				is null
			or	datediff(dd,LoanPymtTransEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.LoanPymtTransCdTbl
where		(	(sysdate -	LoanPymtTransStrtDt)				>= 0)
and			(				LoanPymtTransEndDt				is null
			or	(sysdate -	LoanPymtTransEndDt)				<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

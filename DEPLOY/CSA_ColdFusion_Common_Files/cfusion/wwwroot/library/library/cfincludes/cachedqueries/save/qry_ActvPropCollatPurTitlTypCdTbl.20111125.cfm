<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropCollatTitlTypCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropCollatPurTitlTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
      select
      	PropCollatPurTitlTypCd,
	    PropCollatPurTitlTypDescTxt,
	    PropCollatPurTitlTypCd								AS code,
		PropCollatPurTitlTypDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..PropCollatPurTitlTypCdTbl
	   where (datediff(dd,PropCollatPurTitlTypStrtDt,		getdate()) >= 0)
		   and	(PropCollatPurTitlTypEndDt					is null
		   or datediff(dd,PropCollatPurTitlTypEndDt,		getdate()) <= 0)<cfelse>
	   from	sbaref.PropCollatPurTitlTypCdTbl
       where		((sysdate -	PropCollatPurTitlTypStrtDt)>= 0)
       		and		(PropCollatPurTitlTypEndDt				is null
			or	(sysdate -	PropCollatPurTitlTypEndDt)		<= 0)</cfif>
 	   order by code	  
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblBus"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		IMCrdtScorSourcCd,
			IMCrdtScorSourcDescTxt,
			IMCrdtScorSourcLowNmb,
			IMCrdtScorSourcHighNmb,
			IMCrdtScorSourcCd							AS code,
			IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMCrdtScorSourcTbl
where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
and			(				IMCrdtScorSourcEndDt		is null
			or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.IMCrdtScorSourcTbl
where		(	(sysdate -	IMCrdtScorSourcStrtDt)		>= 0)
and			(				IMCrdtScorSourcEndDt		is null
			or	(sysdate -	IMCrdtScorSourcEndDt)		<= 0)</cfif>
and			(IMCrdtScorSourcBusPerInd					= 'B')
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

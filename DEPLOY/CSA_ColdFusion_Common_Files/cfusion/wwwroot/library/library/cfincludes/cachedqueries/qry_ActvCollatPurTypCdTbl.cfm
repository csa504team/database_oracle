<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NNI:	Original implementation.
								NS:     Changed CollatPurTypStrtDt to CollatPurTypStartDt
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCollatPurTypCdTbl"			datasource="#Variables.db#">
	select		CollatPurTypCd,
				CollatPurTypDescTxt,
				CollatPurTypCd									AS code,
				CollatPurTypDescTxt								AS description<cfif Variables.Sybase>
	from		sbaref..CollatPurTypCdTbl
	where		(	datediff(dd,CollatPurTypStartDt,			getdate()) >= 0)
	and			(				CollatPurTypEndDt				is null
				or	datediff(dd,CollatPurTypEndDt,				getdate()) <= 0)<cfelse>
	from		sbaref.CollatPurTypCdTbl
	where		(	(sysdate -	CollatPurTypStartDt)			>= 0)
	and			(				CollatPurTypEndDt				is null
				or	(sysdate -	CollatPurTypEndDt)				<= 0)</cfif>
	order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/22/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/22/2016, SRS:	OPSMDEV-1085: Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvRepayInstlTypCdTbl"		datasource="#Variables.db#">
select		RepayInstallTypCd,
			RepayInstallTypDesc,
			RepayInstallTypCd								as code,
			RepayInstallTypDesc								as description<cfif Variables.Sybase>
from		sbaref..RepayInstlTypCdTbl
where		(	datediff(dd,RepayInstallTypStrtDt,getdate())>= 0)
and			(				RepayInstallTypEndDt			is null
			or	datediff(dd,RepayInstallTypEndDt, getdate())<= 0)<cfelse>
from		sbaref.RepayInstlTypCdTbl
where		(	(sysdate -	RepayInstallTypStrtDt)			>= 0)
and			(				RepayInstallTypEndDt			is null
			or	(sysdate -	RepayInstallTypEndDt)			<= 0)</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

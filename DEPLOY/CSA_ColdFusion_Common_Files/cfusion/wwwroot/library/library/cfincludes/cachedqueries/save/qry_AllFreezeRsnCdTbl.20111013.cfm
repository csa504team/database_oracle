<!---
AUTHOR:				Nirish Namilae. 
DATE:				10/12/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/12/2011, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllFreezeRsnCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
SELECT		FreezeRsnCd,
			FreezeRsnDescTxt,
			FreezeRsnStrtDt,
			FreezeRsnEndDt,
			CreatUserId,
			CreatDt,
			FreezeRsnCd							AS code,
			FreezeRsnDescTxt					AS description<cfif Variables.Sybase>
from		sbaref..FreezeRsnCdTbl<cfelse>
from		sbaref.FreezeRsnCdTbl</cfif>
order by	code
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

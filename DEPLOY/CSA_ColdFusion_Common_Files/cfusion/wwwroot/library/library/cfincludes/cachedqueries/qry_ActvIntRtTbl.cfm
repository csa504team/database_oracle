<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	10/22/2015, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIntRtTbl"			datasource="#Variables.db#">
  SELECT distinct r.IMRtTypCd "code",
         r.IMRtTypDescTxt description,
         r.IMRtTypInd,
         CASE
            WHEN (r.IMRtTypCd != 'LIB') THEN d.IMPrimeRtPct
            ELSE d.IMPrimeRtPct + TO_NUMBER (v.IMPrmtrValTxt)
         END
            AS IMPrimeRtPct
    FROM SBAREF.IMRTTYPTBL r
         LEFT OUTER JOIN SBAREF.IMRTTBL d
            ON (    r.IMRtTypCd = d.IMRtTypCd
                AND d.IMRtStrtDt <= SYSDATE
                AND (d.IMRtEndDt IS NULL OR d.IMRtEndDt >= SYSDATE))
         INNER JOIN LOANAPP.IMPRMTRTBL p ON (1 = 1)
         INNER JOIN LOANAPP.IMPRMTRVALTBL v
            ON (    p.IMPrmtrSeqNmb = v.IMPrmtrSeqNmb
                AND v.IMPrmtrValStrtDt <= SYSDATE
                AND (v.IMPrmtrValEndDt IS NULL OR v.IMPrmtrValEndDt >= SYSDATE))
   WHERE (r.PrcsMthdCd in ('504','7AG','MLD'))
         AND (r.IMRtTypStrtDt <= SYSDATE)
         AND ( (r.IMRtTypEndDt IS NULL) OR (r.IMRtTypEndDt >= SYSDATE))
         AND (p.IMPrmtrId IN ('LIBBasRtAddlPct'))
         AND d.IMPrimeRtPct IS NOT NULL
ORDER BY description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<!---
AUTHOR:				Sushil Inaganti, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/03/2016
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	
--->
<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllLoanDomCdTbl"		datasource="#Variables.db#">
select		LOANDOMCD,
			LOANDOMCDDESCTXT,
			LOANDOMCD							AS code,
			LOANDOMCDDESCTXT					AS description<cfif Variables.Sybase>
from		sbaref..LOANDOMCDTBL<cfelse>
from		sbaref.LOANDOMCDTBL</cfif>
order by	code	
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

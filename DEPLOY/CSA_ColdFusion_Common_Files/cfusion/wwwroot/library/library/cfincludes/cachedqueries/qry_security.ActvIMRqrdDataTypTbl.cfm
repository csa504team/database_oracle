<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/10/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2015, SRS:	Original implementation. First query for CacheQryCd "security". 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMRqrdDataTypTbl"		datasource="#Variables.db#">
select		IMRqrdDataTypCd,
			IMRqrdDataTypDescTxt,
			IMRqrdDataTypStrtDt,
			IMRqrdDataTypEndDt,
			IMRqrdDataTypDescTxt								AS displayorder,
			IMRqrdDataTypCd										AS code,
			IMRqrdDataTypDescTxt								AS description<cfif Variables.Sybase>
from		security..IMRqrdDataTypTbl
where		(	datediff(dd,IMRqrdDataTypStrtDt,				getdate()) >= 0)
and			(				IMRqrdDataTypEndDt					is null
			or	datediff(dd,IMRqrdDataTypEndDt,					getdate()) <= 0)<cfelse>
from		security.IMRqrdDataTypTbl
where		(	(sysdate -	IMRqrdDataTypStrtDt)				>= 0)
and			(				IMRqrdDataTypEndDt					is null
			or	(sysdate -	IMRqrdDataTypEndDt)					<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllCollatSalePrcTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  select
      	CollatSalePrcTypCd,
	    CollatSalePrcTypCdDescTxt,
	    CollatSalePrcTypCdStrtDt,
	    CollatSalePrcTypCdEndDt,
	    CollatSalePrcTypCd									AS code,
		CollatSalePrcTypCdDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..CollatSalePrcTypCdTbl
	   <cfelse>
	   from	sbaref.CollatSalePrcTypCdTbl
       </cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

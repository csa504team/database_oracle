<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvCollatStatCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvCollatStatCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	COLLATSTATCD,
	    COLLATSTATDESCTXT,	    
	    COLLATSTATCD								AS code,
		COLLATSTATDESCTXT							AS description <cfif Variables.Sybase>	  
	   from	sbaref..COLLATSTATCDTbl
	   where (datediff(dd,COLLATSTATSTARTDT,		getdate()) >= 0)
		   and	(COLLATSTATENDDT		is null
		   or datediff(dd,COLLATSTATENDDT,		getdate()) <= 0)<cfelse>
	   from	sbaref.COLLATSTATCDTbl
       where		((sysdate -	COLLATSTATSTARTDT)		>= 0)
       		and		(COLLATSTATENDDT	is null
			or	(sysdate -	COLLATSTATENDDT)		<= 0)</cfif>
      order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NNI:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCollatPurLossBalCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CollatPurLossBalCd,
			CollatPurLossBalDescTxt,
			CollatPurLossBalStrtDt,
			CollatPurLossBalEndDt,
			CollatPurLossBalCd									AS code,
			CollatPurLossBalDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..CollatPurLossBalCdTbl<cfelse>
from		sbaref.CollatPurLossBalCdTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

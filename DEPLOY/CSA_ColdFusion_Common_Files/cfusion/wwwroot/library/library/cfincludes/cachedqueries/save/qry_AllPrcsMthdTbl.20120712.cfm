<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/07/2012, NNI:	Added two new list for direct and guaranty set.
					07/01/2011, NNI:	Added IMSysPrvlgNm.
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		PrcsMthdCd,
			PrcsMthdDesc,
			PrcsMthdStrtDt,
			PrcsMthdEndDt,
			PrcsMthdCd									AS code,
			PrcsMthdDesc								AS description,
			LoanTypInd,
			PrcsMthdRvwrRqrdNmb,
			PrcsMthdExprsInd,
			PrcsMthdUndrwritngBy,
			PrgrmCd,
			IMSysPrvlgNm,
			PrgrmAuthAreaInd<cfif Variables.Sybase>
from		sbaref..PrcsMthdTbl<cfelse>
from		sbaref.PrcsMthdTbl</cfif>
order by	description
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
<cfset Variables.ScqLists								= StructNew()>
<cfloop index="ScqListKey"								list="AllPrcsMthdDirectSet,AllPrcsMthdGuarantySet">
	<cfset Variables.ScqLists[ScqListKey]				= "">
</cfloop>
<cfloop query="Variables.Scq.AllPrcsMthdTbl">
	<cfswitch expression="#Variables.Scq.AllPrcsMthdTbl.LoanTypInd#">
	<cfcase value="D">		<cfset Variables.ScqLists.AllPrcsMthdDirectSet			= ListAppend(Variables.ScqLists.AllPrcsMthdDirectSet,		PrcsMthdCd)></cfcase>
	<cfcase value="G">		<cfset Variables.ScqLists.AllPrcsMthdGuarantySet		= ListAppend(Variables.ScqLists.AllPrcsMthdGuarantySet,		PrcsMthdCd)></cfcase>
	</cfswitch>
</cfloop>
<cfset Variables.Scq["PrcsMthdAllLists"]					= Duplicate(Variables.ScqLists)>
<cfset StructDelete(Variables, "ScqLists")>

<!---
AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc., for the US Small Business Administration. 
DATE:				09/23/2014. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/23/2014, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.elend.ActvLoanFinanclStmtFreqTbl" datasource="#Variables.db#">
select		LoanFinanclStmtFreqCd,
			LoanFinanclStmtFreqDescTxt,
			case LoanFinanclStmtFreqCd
			when 'A' then 1
			when 'S' then 2
			when 'Q' then 3
			when 'M' then 4
			else null
			end											as displayorder,
			LoanFinanclStmtFreqCd						AS code,
			LoanFinanclStmtFreqDescTxt					AS description<cfif Variables.Sybase>
from		sbaref..LoanFinanclStmtFreqTbl<cfelse>
from		sbaref.LoanFinanclStmtFreqTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

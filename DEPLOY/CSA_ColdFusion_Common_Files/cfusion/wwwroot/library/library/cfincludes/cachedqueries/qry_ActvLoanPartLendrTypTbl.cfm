<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanPartLendrTypTbl"	datasource="#Variables.db#">
select		LoanPartLendrTypCd,
			LoanPartLendrTypDescTxt,
			case<!--- Force Participating to the top and Others to the end: --->
			when LoanPartLendrTypCd = 'P'				then 1
			when LoanPartLendrTypCd = 'I'				then 2
			when LoanPartLendrTypCd = 'G'				then 3
			when LoanPartLendrTypCd = 'O'				then 4
			else										5
			end											AS displayorder,
			LoanPartLendrTypCd							AS code,
			LoanPartLendrTypDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..LoanPartLendrTypTbl
where		(	datediff(dd,LoanPartLendrTypStrtDt,		getdate()) >= 0)
and			(				LoanPartLendrTypEndDt		is null
			or	datediff(dd,LoanPartLendrTypEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.LoanPartLendrTypTbl
where		(	(sysdate -	LoanPartLendrTypStrtDt)		>= 0)
and			(				LoanPartLendrTypEndDt		is null
			or	(sysdate -	LoanPartLendrTypEndDt)		<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

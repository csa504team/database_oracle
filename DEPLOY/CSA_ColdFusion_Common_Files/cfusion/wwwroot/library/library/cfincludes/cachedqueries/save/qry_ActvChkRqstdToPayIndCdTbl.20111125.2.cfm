<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvChkRqstdToPayIndCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvChkRqstdToPayIndCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	ChkRqstdToPayIndCd,
	    ChkRqstdToPayIndDescTxt,
	    ChkRqstdToPayIndCd									AS code,
		ChkRqstdToPayIndDescTxt								AS description <cfif Variables.Sybase>	  
	   from	sbaref..ChkRqstdToPayIndCdTbl
	   where (datediff(dd,ChkRqstdToPayIndStrtDt, 			getdate()) >= 0)
		   and	(ChkRqstdToPayIndEndDt						is null
		   or datediff(dd,ChkRqstdToPayIndEndDt, getdate()) <= 0)<cfelse>
	   from	sbaref.ChkRqstdToPayIndCdTbl
       where ((sysdate -	ChkRqstdToPayIndStrtDt)			>= 0)
       		and	 (ChkRqstdToPayIndEndDt						is null
			or	(sysdate -	ChkRqstdToPayIndEndDt)			<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
	
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				03/08/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					03/08/2011, SRS:	Original implementation. To make the entries come out in the order asked-for by the 
										Program Office, this query is unusual in that it's ordered by code. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCnselngTypTbl"		datasource="#Variables.db#">
select		IMCnselngTypCd,
			IMCnselngTypDescTxt,
			IMCnselngTypCd								AS code,
			IMCnselngTypDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..IMCnselngTypTbl
where		(	datediff(dd,IMCnselngTypStrtDt,			getdate()) >= 0)
and			(				IMCnselngTypEndDt			is null
			or	datediff(dd,IMCnselngTypEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.IMCnselngTypTbl
where		(	(sysdate -	IMCnselngTypStrtDt)			>= 0)
and			(				IMCnselngTypEndDt			is null
			or	(sysdate -	IMCnselngTypEndDt)			<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

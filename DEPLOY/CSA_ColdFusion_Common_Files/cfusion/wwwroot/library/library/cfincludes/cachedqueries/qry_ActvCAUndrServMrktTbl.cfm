<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				04/06/2016. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	04/06/2016, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCAUndrServMrktTbl"				datasource="#Variables.db#">
select		UndrServMrktCd,
			UndrServMrktDesc,
			case UndrServMrktCd when 99 then 2 else 1 end as	NoneOfTheAboveComesLast,
			UndrServMrktCd										AS code,
			UndrServMrktDesc									AS description<cfif Variables.Sybase>
from		sbaref..CAUndrServMrktTbl
where		(	datediff(dd,UndrServMrktStrtDt,					getdate()) >= 0)
and			(				UndrServMrktEndDt					is null
			or	datediff(dd,UndrServMrktEndDt,					getdate()) <= 0)<cfelse>
from		sbaref.CAUndrServMrktTbl
where		(	(sysdate -	UndrServMrktStrtDt)					>= 0)
and			(				UndrServMrktEndDt					is null
			or	(sysdate -	UndrServMrktEndDt)					<= 0)</cfif>
order by	NoneOfTheAboveComesLast,
			description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

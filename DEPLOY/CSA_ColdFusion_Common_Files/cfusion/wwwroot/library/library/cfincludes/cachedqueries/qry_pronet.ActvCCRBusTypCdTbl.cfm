<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					03/29/2011, SRS:	Added support for WOSB FCP types (and new lists). Per Stephanie King, although all 
										WOSB FCP types are women-owned small businesses, and therefore theoretically ought 
										to imply A2, they don't get included in WOB (A2's list), so that they don't show up 
										in WOSB FCP searches. Also, per Diane Heal, all SDVs are now to be considered VETs, 
										and included in the VET list, because CCR isn't enforcing it (as they should have). 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.pronet.ActvCCRBusTypCdTbl"				datasource="#Variables.db#">
select			c.CCRBusTypCd										code,
				c.CCRBusTypDescTxt									description,
				c.CCRBusTypCd,
				c.CCRBusTypDescTxt,
				c.IMMinCd,
				case
				when c.CCRBusTypCd in ('05','1B','8U','HK')			/* ANC-, Tribal-, NHO- and CDC-owned, respectively. */
											then					c.CCRBusTypDescTxt
				when c.IMMinCd is not null	then					m.IMMinDescTxt
				else												null
				end													as DSBSSearchName,
				c.BusStatInactRsnCd,
				c.CCRBusTypExpt,
				c.IMOrgTypCd<cfif Variables.Sybase>
from			pronet..CCRBusTypCdTbl								c
left outer join	pronet..IMMinCdTbl									m on (c.IMMinCd = m.IMMinCd)<cfelse>
from			viewpronet.CCRBusTypCdTbl							c
left outer join	viewpronet.IMMinCdTbl								m on (c.IMMinCd = m.IMMinCd)</cfif>
order by		DSBSSearchName,
				c.CCRBusTypCd
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<cfset Variables.ScqLists											= StructNew()>
<!--- Keep the following struct key names in sync with values of DSBS's OtherOwnershipCheckboxValues: --->
<cfset Variables.ScqListKeysCBT										= "Min,Nat,NatTri,NatAla,NatHaw,NatOth,MinOth,"
																	& "Wom,Wosb,EdWosb,WosbJV,EdWosbJV,"
																	& "CDC,SCS,SDV,Vet,Wom,QMS">
<cfloop index="ScqListKey"											list="#Variables.ScqListKeysCBT#">
	<cfset Variables.ScqLists[ScqListKey]							= "">
</cfloop>
<cfloop query="Variables.Scq.pronet.ActvCCRBusTypCdTbl">
	<cfif Len(IMMinCd) GT 0>
		<cfswitch expression="#UCase(IMMinCd)#">
		<cfcase value="NAT">
			<cfset Variables.ScqLists.Min							= ListAppend(Variables.ScqLists.Min,		"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.Nat							= ListAppend(Variables.ScqLists.Nat,		"'#CCRBusTypCd#'")>
			<cfswitch expression="#CCRBusTypCd#">
			<!---
			Because of the one-to-many relationship from IMMinCd to CCRBusTypCd (as opposed to many-to-many), 
			Tribally-Owned, Alaskan Native Corp Owned, Native Hawaiian Org Owned codes have to be hard-coded 
			for now. Technically, all 3 are tribally-owned, but ANC and NHO aren't called tribes. 
			--->
			<cfcase value="1B">	<cfset Variables.ScqLists.NatTri	= ListAppend(Variables.ScqLists.NatTri,		"'#CCRBusTypCd#'")></cfcase>
			<cfcase value="05">	<cfset Variables.ScqLists.NatAla	= ListAppend(Variables.ScqLists.NatAla,		"'#CCRBusTypCd#'")></cfcase>
			<cfcase value="8U">	<cfset Variables.ScqLists.NatHaw	= ListAppend(Variables.ScqLists.NatHaw,		"'#CCRBusTypCd#'")></cfcase>
			<cfdefaultcase>		<cfset Variables.ScqLists.NatOth	= ListAppend(Variables.ScqLists.NatOth,		"'#CCRBusTypCd#'")></cfdefaultcase>
			</cfswitch>
		</cfcase>
		<cfcase value="SCS">
			<cfset Variables.ScqLists[IMMinCd]						= ListAppend(Variables.ScqLists[IMMinCd],	"'#CCRBusTypCd#'")>
		</cfcase>
		<cfcase value="SDV"><!--- Participates in Quick Market Search. --->
			<cfset Variables.ScqLists.QMS							= ListAppend(Variables.ScqLists.QMS,		"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.SDV							= ListAppend(Variables.ScqLists.SDV,		"'#CCRBusTypCd#'")>
			<!--- Per Diane Heal, all SDVs are to be treated as VETs, because CCR isn't enforcing that rule: --->
			<cfset Variables.ScqLists.VET							= ListAppend(Variables.ScqLists.VET,		"'#CCRBusTypCd#'")>
		</cfcase>
		<cfcase value="VET"><!--- Participates in Quick Market Search. --->
			<cfset Variables.ScqLists.QMS							= ListAppend(Variables.ScqLists.QMS,		"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.VET							= ListAppend(Variables.ScqLists.VET,		"'#CCRBusTypCd#'")>
		</cfcase>
		<cfcase value="WOM"><!--- Participates in Quick Market Search. --->
			<cfset Variables.ScqLists.QMS							= ListAppend(Variables.ScqLists.QMS,		"'#CCRBusTypCd#'")>
			<!---
			Again, because of the one-to-many relationship from IMMinCd to CCRBusTypCd (as opposed to many-to-many), 
			these codes have to be hard-coded for now. 
			--->
			<cfswitch expression="#CCRBusTypCd#">
			<cfcase value="8W">	<cfset Variables.ScqLists.Wosb		= ListAppend(Variables.ScqLists.Wosb,		"'#CCRBusTypCd#'")></cfcase>
			<cfcase value="8E">	<cfset Variables.ScqLists.EdWosb	= ListAppend(Variables.ScqLists.EdWosb,		"'#CCRBusTypCd#'")></cfcase>
			<cfcase value="8C">	<cfset Variables.ScqLists.WosbJV	= ListAppend(Variables.ScqLists.WosbJV,		"'#CCRBusTypCd#'")></cfcase>
			<cfcase value="8D">	<cfset Variables.ScqLists.EdWosbJV	= ListAppend(Variables.ScqLists.EdWosbJV,	"'#CCRBusTypCd#'")></cfcase>
			<!--- The following code is scheduled for elimination, because it's too easily confused with 8(a): --->
			<cfcase value="8A">	<cfset Variables.ScqLists.Wosb		= ListAppend(Variables.ScqLists.Wosb,		"'#CCRBusTypCd#'")></cfcase>
			<cfdefaultcase>		<cfset Variables.ScqLists.Wom		= ListAppend(Variables.ScqLists.Wom,		"'#CCRBusTypCd#'")></cfdefaultcase>
			</cfswitch>
		</cfcase>
		<cfdefaultcase>
			<cfset Variables.ScqLists.Min							= ListAppend(Variables.ScqLists.Min,		"'#CCRBusTypCd#'")>
			<cfset Variables.ScqLists.MinOth						= ListAppend(Variables.ScqLists.MinOth,		"'#CCRBusTypCd#'")>
		</cfdefaultcase>
		</cfswitch>
	<cfelseif CCRBusTypCd IS "HK"><!--- A CDC is not necessarily a minority and has no minority code. --->
		<cfset Variables.ScqLists.CDC								= ListAppend(Variables.ScqLists.CDC,		"'#CCRBusTypCd#'")>
	</cfif>
</cfloop>
<cfset Variables.Scq.pronet["CbtLists"]								= Duplicate(Variables.ScqLists)>
<cfset StructDelete(Variables, "ScqLists")>

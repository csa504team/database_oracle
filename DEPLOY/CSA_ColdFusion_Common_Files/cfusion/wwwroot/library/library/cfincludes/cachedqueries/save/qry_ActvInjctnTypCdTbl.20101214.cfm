<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvInjctnTypCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		InjctnTypCd,
			InjctnTypTxt,
			case<!--- Reorder cash types first, then non-cash, then standby debt, then other: --->
			when InjctnTypCd = 'C'						then 1
			when InjctnTypCd = 'G'						then 2
			when InjctnTypCd = 'D'						then 3
			when InjctnTypCd = 'A'						then 4
			when InjctnTypCd = 'S'						then 5
			when InjctnTypCd = 'O'						then 6
			else										7
			end											AS displayorder,
			InjctnTypCd									AS code,
			InjctnTypTxt								AS description<cfif Variables.Sybase>
from		sbaref..InjctnTypCdTbl
where		(	datediff(dd,InjctnTypStrtDt,			getdate()) >= 0)
and			(				InjctnTypEndDt				is null
			or	datediff(dd,InjctnTypEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.InjctnTypCdTbl
where		(	(sysdate -	InjctnTypStrtDt)			>= 0)
and			(				InjctnTypEndDt				is null
			or	(sysdate -	InjctnTypEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

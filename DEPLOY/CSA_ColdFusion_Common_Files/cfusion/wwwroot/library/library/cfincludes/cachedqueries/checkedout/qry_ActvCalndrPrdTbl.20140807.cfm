<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/21/2013, SRS:	Completely revamped displayorder based on a whole new set of codes. 
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="#Variables.db#">
select		CalndrPrdCd,
			CalndrPrdDesc,
			case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
			when CalndrPrdCd = 'A'						then 1
			when CalndrPrdCd = 'U'						then 2
			when CalndrPrdCd = 'X'						then 3
			when CalndrPrdCd = 'N'						then 4
			when CalndrPrdCd = 'L'						then 5
			when CalndrPrdCd = 'Q'						then 6
			when CalndrPrdCd = 'S'						then 7
			when CalndrPrdCd = 'M'						then 8
			when CalndrPrdCd = 'D'						then 9
			else										10
			end											AS displayorder,
			CalndrPrdCd									AS code,
			CalndrPrdDesc								AS description<cfif Variables.Sybase>
from		sbaref..CalndrPrdTbl
where		(	datediff(dd,CalndrPrdStrtDt,			getdate()) >= 0)
and			(				CalndrPrdEndDt				is null
			or	datediff(dd,CalndrPrdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CalndrPrdTbl
where		(	(sysdate -	CalndrPrdStrtDt)			>= 0)
and			(				CalndrPrdEndDt				is null
			or	(sysdate -	CalndrPrdEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

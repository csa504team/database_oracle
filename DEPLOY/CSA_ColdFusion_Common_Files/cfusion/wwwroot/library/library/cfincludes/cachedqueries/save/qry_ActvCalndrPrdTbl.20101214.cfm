<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CalndrPrdCd,
			CalndrPrdDesc,
			case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
			when CalndrPrdCd = 'M'						then 1
			when CalndrPrdCd = 'Q'						then 2
			when CalndrPrdCd = 'S'						then 3
			when CalndrPrdCd = 'A'						then 4
			else										5
			end											AS displayorder,
			CalndrPrdCd									AS code,
			CalndrPrdDesc								AS description<cfif Variables.Sybase>
from		sbaref..CalndrPrdTbl
where		(	datediff(dd,CalndrPrdStrtDt,			getdate()) >= 0)
and			(				CalndrPrdEndDt				is null
			or	datediff(dd,CalndrPrdEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CalndrPrdTbl
where		(	(sysdate -	CalndrPrdStrtDt)			>= 0)
and			(				CalndrPrdEndDt				is null
			or	(sysdate -	CalndrPrdEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

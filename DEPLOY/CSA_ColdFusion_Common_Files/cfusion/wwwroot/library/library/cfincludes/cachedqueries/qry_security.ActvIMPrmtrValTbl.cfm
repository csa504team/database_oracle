<!---
AUTHOR:				Steve Seaquist, Alpha Omega Integration, Inc., for the US Small Business Administration. 
DATE:				11/02/2017. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/02/2017, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.ActvIMPrmtrValTbl"	datasource="#Variables.db#">
select		p.IMPrmtrId,
			p.IMPrmtrDescTxt,
			p.IMPrmtrValDataTypCd,
			p.IMPrmtrUserModInd,
			p.IMPrmtrId										"code",
			p.IMPrmtrDescTxt								"description",
			v.IMPrmtrSeqNmb,
			v.IMPrmtrValSeqNmb,
			v.IMPrmtrValTxt,
			to_char(v.IMPrmtrValLrgTxt)						"IMPrmtrValLrgTxt"
from		security.IMPrmtrTbl								p
inner join	security.IMPrmtrValTbl							v on (p.IMPrmtrSeqNmb = v.IMPrmtrSeqNmb)
where		(	(sysdate -	IMPrmtrValStrtDt)				>= 0)
and			(				IMPrmtrValEndDt					is null
			or	(sysdate -	IMPrmtrValEndDt)				<= 0)
order by	p.IMPrmtrId,
			v.IMprmtrValSeqNmb
</cfquery>

<!--- Define a structure for the current values of commonly-used IMPrmtrIds that take only one value: --->
<cfset Variables.Scq.security["Parameters"]					= StructNew()>
<cfloop query="Variables.Scq.security.ActvIMPrmtrValTbl">
	<cfswitch expression="#Variables.Scq.security.ActvIMPrmtrValTbl.IMPrmtrId#">
	<cfcase value="CLSAcctMaxPwdExpDays,CLSAcctSuspndDays,CLSFailedLoginWindow,CLSLoginMaxAttempts,CLSMsgAgeOutDays,CLSPasswordMinCharTypes,CLSPasswordMinChars">
		<cfset Variables.Scq.security["Parameters"]			[ Variables.Scq.security.ActvIMPrmtrValTbl.IMPrmtrId]
															= Variables.Scq.security.ActvIMPrmtrValTbl.IMPrmtrValTxt>
	</cfcase>
	<cfcase value="BrwsrVrsn,CAFSEnvRqmt">
		<cfset Variables.Scq.security["Parameters"]			[ Variables.Scq.security.ActvIMPrmtrValTbl.IMPrmtrId]
															= Variables.Scq.security.ActvIMPrmtrValTbl.IMPrmtrValLrgTxt>
	</cfcase>
	</cfswitch>
</cfloop>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

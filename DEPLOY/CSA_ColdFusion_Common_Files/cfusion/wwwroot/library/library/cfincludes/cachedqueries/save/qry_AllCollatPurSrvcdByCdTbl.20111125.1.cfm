<!---
AUTHOR:				Nirish Namilae
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	11/25/2011, NNI:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllCollatPurSrvcdByCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CollatPurSrvcdByCd,
			CollatPurSrvcdByDescTxt,
			CollatPurSrvcdByStrtDt,
			CollatPurSrvcdByEndDt,
			CollatPurSrvcdByCd									AS code,
			CollatPurSrvcdByDescTxt								AS description<cfif Variables.Sybase>
from		sbaref..CollatPurSrvcdByCdTbl<cfelse>
from		sbaref.CollatPurSrvcdByCdTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				(Originally)Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/22/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files. 
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	07/25/2016, SRS:	Corrected handling of config parameter's struct keys. 
					12/15/2015, NNI:	Added drwadown count. 
					11/06/2015, SMJ:	Created a config parameter in order to increase maintainability. 
					10/22/2015, NNI:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfset Variables.FinCountNames								= "BondSbgBidFyNmb,"
															& "BondSbgFnlFyNmb,"
															& "BondPsbBidFyNmb,"
															& "BondPsbFnlFyNmb,"
															& "LoanDebsPrgcNmb,"
															& "LoanDisatrLnNmb,"
															& "LoanMicroLnsNmb,"
															& "LoanSBICLNNmb,"
															& "Loan7AFYNmb,"
															& "LoanMicroBrNmb,"
															& "FinanFyTotNmb,"
															& "FinanFyTotAmt" />

<!--- Initializations: --->

<cfloop list = "#Variables.FinCountNames#" index = "ScqVarKey">
	<cfset Variables[ScqVarKey]								= "" />
</cfloop>

<!--- Build the Variables: --->

<cfinclude template="bld_preprocessing.cfm">
<cfset Variables.cfprname									= "getFinCount" />
<cfset Variables.LogAct										= "get financial counts" />
<cfset ScqVarKey											= Variables.TxnErr><!--- For-sure not in use at the moment. Save-and-restore TxnErr. --->
<cfinclude template="/cfincludes/oracle/loan/spc_FINANCOUNTSELCSP.PUBLIC.cfm" />
<cfif Variables.TxnErr>
	<cfset Variables.Scq.CAFSHomeVars = Variables.ErrMsg />
<cfelse>
<!--- Copy them into the structure: --->
<cfset Variables.ScqVars									= StructNew()>
<cfloop list="#Variables.FinCountNames#" index="ScqVarKey">
	<cfset Variables.ScqVars[ScqVarKey]						= Variables[ScqVarKey] />
</cfloop>

<!--- Cleanup: --->
<cfset Variables.Scq["CAFSHomeVars"]						= Duplicate(Variables.ScqVars)>
<cfset StructDelete(Variables, "ScqVars")>
<cfset StructDelete(Variables, "ScqVarKey")>
</cfif>

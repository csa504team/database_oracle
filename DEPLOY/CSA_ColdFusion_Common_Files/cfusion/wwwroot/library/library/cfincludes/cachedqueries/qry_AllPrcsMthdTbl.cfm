<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/29/2012, SRS:	Removed PrcsMthdUndrwritngBy support. PrcsMthdUndrwritngBy got moved to 
										IMPrgrmAuthOthAgrmtTbl.UndrwritngBy. 
					12/07/2012, NNI:	Added two new list for direct and guaranty set.
					07/01/2011, NNI:	Added IMSysPrvlgNm.
					12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.db#">
select		PrcsMthdCd,
			PrcsMthdDesc,
			PrcsMthdStrtDt,
			PrcsMthdEndDt,
			PrcsMthdCd									AS code,
			PrcsMthdDesc								AS description,
			LoanTypInd,
			PrcsMthdRvwrRqrdNmb,
			PrcsMthdExprsInd,
			PrgrmCd,
			IMSysPrvlgNm,
			PrgrmAuthAreaInd,
			PrcsMthdExprtInd<cfif Variables.Sybase>
from		sbaref..PrcsMthdTbl<cfelse>
from		sbaref.PrcsMthdTbl</cfif>
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">
<cfset Variables.ScqLists								= StructNew()>
<cfloop index="ScqListKey"								list="AllPrcsMthdDirectSet,AllPrcsMthdGuarantySet">
	<cfset Variables.ScqLists[ScqListKey]				= "">
</cfloop>
<cfloop query="Variables.Scq.AllPrcsMthdTbl">
	<cfswitch expression="#Variables.Scq.AllPrcsMthdTbl.LoanTypInd#">
	<cfcase value="D">		<cfset Variables.ScqLists.AllPrcsMthdDirectSet			= ListAppend(Variables.ScqLists.AllPrcsMthdDirectSet,		PrcsMthdCd)></cfcase>
	<cfcase value="G">		<cfset Variables.ScqLists.AllPrcsMthdGuarantySet		= ListAppend(Variables.ScqLists.AllPrcsMthdGuarantySet,		PrcsMthdCd)></cfcase>
	</cfswitch>
</cfloop>
	
<cfset Variables.Scq["PrcsMthdAllLists"]				= Duplicate(Variables.ScqLists)>
<cfset StructDelete(Variables, "ScqLists")>

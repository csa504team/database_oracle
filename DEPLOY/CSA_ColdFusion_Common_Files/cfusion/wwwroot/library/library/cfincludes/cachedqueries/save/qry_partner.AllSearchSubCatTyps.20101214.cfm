<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.partner.AllSearchSubCatTyps"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		v.ValidSBAPrtPrimCatTyp,
			c.ValidPrtPrimCatTypDesc,
			v.ValidPrtSubCatTyp,
			v.ValidPrtSubCatDesc,
			v.RolePrivilegePrgrmId<cfif Variables.Sybase>
from		partner..ValidPrtSubCatTbl					v,
			partner..ValidPrtPrimCatTbl					c<cfelse>
from		partner.ValidPrtSubCatTbl					v,
			partner.ValidPrtPrimCatTbl					c</cfif>
where		v.ValidSBAPrtPrimCatTyp						= c.ValidPrtPrimCatTyp
order by	c.ValidPrtPrimCatDispOrdNmb,
			c.ValidPrtPrimCatTypDesc,
			v.ValidPrtSubCatDesc
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Nelli Salatova
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching.
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName.
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr.
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NS:	Original implementation.
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.AllCollatStatCdTbl"		datasource="#Variables.db#">
	  select
      	CollatStatCd,
	    CollatStatDescTxt,
	    CollatStatStartDt,
	    CollatStatEndDt,	    
	    CollatStatCd								AS code,
		CollatStatDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..CollatStatCdTbl
	  <cfelse>
	   from	sbaref.CollatStatCdTbl
       </cfif>
      order by code
</cfquery>

<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

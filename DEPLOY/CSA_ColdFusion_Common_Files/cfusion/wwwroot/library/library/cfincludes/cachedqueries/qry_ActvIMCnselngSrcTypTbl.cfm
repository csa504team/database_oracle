<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				03/08/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					03/08/2011, SRS:	Original implementation. To make the entries come out in the order asked-for by the 
										Program Office, this query is unusual in that it's ordered by code. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvIMCnselngSrcTypTbl"	datasource="#Variables.db#">
select		IMCnselngSrcTypCd,
			IMCnselngSrcTypDescTxt,
			IMCnselngSrcTypCd							AS code,
			IMCnselngSrcTypDescTxt						AS description<cfif Variables.Sybase>
from		sbaref..IMCnselngSrcTypTbl
where		(	datediff(dd,IMCnselngSrcTypStrtDt,		getdate()) >= 0)
and			(				IMCnselngSrcTypEndDt		is null
			or	datediff(dd,IMCnselngSrcTypEndDt,		getdate()) <= 0)<cfelse>
from		sbaref.IMCnselngSrcTypTbl
where		(	(sysdate -	IMCnselngSrcTypStrtDt)		>= 0)
and			(				IMCnselngSrcTypEndDt		is null
			or	(sysdate -	IMCnselngSrcTypEndDt)		<= 0)</cfif>
order by	code
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

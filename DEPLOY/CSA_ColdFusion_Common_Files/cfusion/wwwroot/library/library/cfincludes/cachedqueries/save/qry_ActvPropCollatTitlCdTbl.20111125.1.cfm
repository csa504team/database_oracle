<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropCollatTitlCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropCollatTitlCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	  SELECT
      	PROPCOLLATTITLCD,
	    PROPCOLLATTITLDESCTXT,
	    PROPCOLLATTITLCD									AS code,
		PROPCOLLATTITLDESCTXT								AS description <cfif Variables.Sybase>
	   from	sbaref..PROPCOLLATTITLCDTbl
	   where (datediff(dd,PROPCOLLATTITLSTRTDT,		getdate()) >= 0)
		   and	(PROPCOLLATTITLENDDT		is null
		   or datediff(dd,PROPCOLLATTITLENDDT,		getdate()) <= 0)<cfelse>
	   from	sbaref.PROPCOLLATTITLCDTbl
       where		((sysdate -	PROPCOLLATTITLSTRTDT)		>= 0)
       		and		(PROPCOLLATTITLENDDT	is null
			or	(sysdate -	PROPCOLLATTITLENDDT)		<= 0)</cfif> 	
      order by code	
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
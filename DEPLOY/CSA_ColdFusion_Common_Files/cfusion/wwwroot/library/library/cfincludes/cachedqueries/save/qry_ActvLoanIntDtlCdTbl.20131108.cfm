<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/08/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/08/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvLoanIntDtlCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		LoanIntDtlCd,
			LoanIntDtlDescTxt,
			LoanIntDtlCd								AS code,
			LoanIntDtlDescTxt							AS description<cfif Variables.Sybase>
from		sbaref..LoanIntDtlCdTbl
where		(	datediff(dd,LoanIntDtlStrtDt,			getdate()) >= 0)
and			(				LoanIntDtlEndDt				is null
			or	datediff(dd,LoanIntDtlEndDt,			getdate()) <= 0)<cfelse>
from		sbaref.LoanIntDtlCdTbl
where		(	(sysdate -	LoanIntDtlStrtDt)			>= 0)
and			(				LoanIntDtlEndDt				is null
			or	(sysdate -	LoanIntDtlEndDt)			<= 0)</cfif>
order by	LoanIntDtlDispOrdNmb
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

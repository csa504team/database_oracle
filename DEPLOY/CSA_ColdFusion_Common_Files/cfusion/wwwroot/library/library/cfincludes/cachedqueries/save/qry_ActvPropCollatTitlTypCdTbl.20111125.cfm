<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvPropCollatTitlTypCdTbl)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvPropCollatTitlTypCdTbl" datasource="#Variables.db#" dbtype="#Variables.dbtype#">
      select
      	PROPCOLLATPURTITLTYPCD,
	    PROPCOLLATPURTITLTYPDESCTXT,
	    PROPCOLLATPURTITLTYPCD									AS code,
		PROPCOLLATPURTITLTYPDESCTXT								AS description <cfif Variables.Sybase>	  
	   from	sbaref..PROPCOLLATPURTITLTYPCDTbl
	   where (datediff(dd,PROPCOLLATPURTITLTYPSTRTDT,		getdate()) >= 0)
		   and	(PROPCOLLATPURTITLTYPENDDT		is null
		   or datediff(dd,PROPCOLLATPURTITLTYPENDDT,		getdate()) <= 0)<cfelse>
	   from	sbaref.PROPCOLLATPURTITLTYPCDTbl
       where		((sysdate -	PROPCOLLATPURTITLTYPSTRTDT)		>= 0)
       		and		(PROPCOLLATPURTITLTYPENDDT	is null
			or	(sysdate -	PROPCOLLATPURTITLTYPENDDT)		<= 0)</cfif>
 	order by code	  
</cfquery>

<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
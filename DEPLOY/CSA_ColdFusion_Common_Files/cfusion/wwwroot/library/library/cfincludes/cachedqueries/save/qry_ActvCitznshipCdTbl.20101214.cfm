<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvCitznshipCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		CitznshipCd,
			CitznshipTxt,
			case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
			when CitznshipCd = 'US'						then 1
			when CitznshipCd = 'RA'						then 2
			when CitznshipCd = 'NR'						then 3
			when CitznshipCd = 'IA'						then 4
			else										5
			end											AS displayorder,
			CitznshipCd									AS code,
			CitznshipTxt								AS description<cfif Variables.Sybase>
from		sbaref..CitznshipCdTbl
where		(	datediff(dd,CitznshipStrtDt,			getdate()) >= 0)
and			(				CitznshipEndDt				is null
			or	datediff(dd,CitznshipEndDt,				getdate()) <= 0)<cfelse>
from		sbaref.CitznshipCdTbl
where		(	(sysdate -	CitznshipStrtDt)			>= 0)
and			(				CitznshipEndDt				is null
			or	(sysdate -	CitznshipEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

<!---
AUTHOR:				Steve Seaquist, Alpha Omega Integration, Inc., for the US Small Business Administration. 
DATE:				09/08/2017. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files. 
					Not used since ARC processing method retired. May be used again someday as "Loan Reasons". 
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	09/08/2017, SRS:	Original implementation. Cannibalized ARCLoanRsnSelTSP Identifier 4. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvARCLoanRsnTbl"			datasource="#Variables.db#">
select		ARCLoanRsnCd,
			ARCLoanRsnDescTxt,
			ARCLoanRsnCd								as code,
			ARCLoanRsnDescTxt							as description
from		sbaref.ARCLoanRsnTbl
where		ARCLoanRsnStrtDt							<= sysdate
and			(	ARCLoanRsnEndDt							is null
			or	ARCLoanRsnEndDt							>  sysdate
			)
order by	description
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

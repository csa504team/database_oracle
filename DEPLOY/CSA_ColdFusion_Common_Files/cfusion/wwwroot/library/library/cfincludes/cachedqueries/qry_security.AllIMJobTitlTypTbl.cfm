<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				06/10/2015. 
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	06/10/2015, SRS:	Original implementation. First query for CacheQryCd "security". 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.security.AllIMJobTitlTypTbl"	datasource="#Variables.db#">
select		JobTitlCd,
			JobTitlDescTxt,
			IMUserTypCd,
			JobTitlStrtDt,
			JobTitlEndDt,
			case JobTitlDescTxt
			when 'Other' then 'zzz' else JobTitlDescTxt end	AS displayorder,
			JobTitlCd										AS code,
			JobTitlDescTxt									AS description<cfif Variables.Sybase>
from		security..IMJobTitlTypTbl<cfelse>
from		security.IMJobTitlTypTbl</cfif>
order by	displayorder
</cfquery>
<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

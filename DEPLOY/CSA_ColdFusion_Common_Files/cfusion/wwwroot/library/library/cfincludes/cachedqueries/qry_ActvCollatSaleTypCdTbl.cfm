<!---
AUTHOR:				Nelli Salatova, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				11/25/2011
DESCRIPTION:		Does a single query into Variables.Scq.((ActvCollatPurPctCdTbl)) for subsequent caching. 
NOTES:				Called by Library's cfincludes/bld_ServerCachedQueries and udf/bld_CachedQueryUDFs files.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility. 
					11/25/2011, NS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">

<cfquery name="Variables.Scq.ActvCollatSaleTypCdTbl" datasource="#Variables.db#">
	   select
      	CollatSaleTypCd,
	    CollatSaleTypCdDescTxt,
	    CollatSaleTypCd									AS code,
		CollatSaleTypCdDescTxt							AS description <cfif Variables.Sybase>	  
	   from	sbaref..CollatSaleTypCdTbl
	   where (datediff(dd,CollatSaleTypCdStrtDt,		getdate()) >= 0)
		   and	(CollatSaleTypCdEndDt					is null
		   or datediff(dd,CollatSaleTypCdEndDt,			getdate()) <= 0)<cfelse>
	   from	sbaref.CollatSaleTypCdTbl
       where ((sysdate -	CollatSaleTypCdStrtDt)		>= 0)
       		and	(CollatSaleTypCdEndDt					is null
			or	(sysdate -	CollatSaleTypCdEndDt)		<= 0)</cfif>
       order by code
</cfquery>

<cfinclude template="#Variables.LogURL#/log_SleQuery.cfm">

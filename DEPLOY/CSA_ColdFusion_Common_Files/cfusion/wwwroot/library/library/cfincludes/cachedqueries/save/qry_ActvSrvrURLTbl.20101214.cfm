<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				12/14/2010
DESCRIPTION:		Does a single query into Variables.Scq.((ScqQueryName)) for subsequent caching. 
NOTES:				Called /library/cfincludes/bld_ServerCachedQueries and/or /library/udf/bld_CachedQueryUDFs.
INPUT:				Variables.db, Variables.dbtype, Variables.Sybase (inferred from dbtype), ScqQueryName. 
OUTPUT:				Variables.Scq.((ScqQueryName)) query object. Possibly ErrMsg and TxnErr. 
REVISION HISTORY:	12/14/2010, SRS:	Original implementation. 
--->

<cfinclude template="bld_preprocessing.cfm">
<cfquery name="Variables.Scq.ActvSrvrURLTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select		distinct
			rtrim(EnvCd)								as EnvCd,
			SrvrLocCd,
			SrvrURLCd,
			SrvrURLDescTxt,<cfif Variables.Sybase>
			rtrim(EnvCd)+'.'+convert(varchar,SrvrLocCd)	as displayorder,<cfelse>
			rtrim(EnvCd)||'.'||SrvrLocCd				as displayorder,</cfif>
			SrvrURLCd									as code,
			SrvrURLDescTxt								as description<cfif Variables.Sybase>
from		sbaref..SrvrURLTbl
where		(	datediff(dd,SrvrURLStrtDt,getdate())	>= 0)
and			(				SrvrURLEndDt				is null
			or	datediff(dd,SrvrURLEndDt, getdate())	<= 0)<cfelse>
from		sbaref.SrvrURLTbl
where		(	(sysdate -	SrvrURLStrtDt)				>= 0)
and			(				SrvrURLEndDt				is null
			or	(sysdate -	SrvrURLEndDt)				<= 0)</cfif>
order by	displayorder
</cfquery>
<cfinclude template="/library/cfincludes/log_SleQuery.cfm">

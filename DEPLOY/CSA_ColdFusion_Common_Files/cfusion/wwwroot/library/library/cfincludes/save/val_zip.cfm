<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) IS NOT 10>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 10 characters.</li>">
	<cfelseif	(NOT IsNumeric	(Mid(Variables.FVal, 1, 5)))
		OR						(Mid(Variables.FVal, 6, 1) IS NOT "-")
		OR		(NOT IsNumeric	(Mid(Variables.FVal, 7, 4)))>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not in 99999-9999 format.</li>">
	</cfif>
</cfif>

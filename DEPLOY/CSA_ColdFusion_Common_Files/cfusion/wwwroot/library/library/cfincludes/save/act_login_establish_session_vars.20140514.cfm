<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/06/2008
DESCRIPTION:		Shared routine to copy Variables.CLS into Session.CLS and assorted other Session variable names. 
NOTES:				New apps should only be using the GLS structure and its substructures, not those assorted other 
					Session variables. At some point, we would like to have only Session.CLS in the Session scope, 
					but we can't do that as long as people keep assuming that its substructures are also defined. 
INPUT:				Variables.CLS. 
OUTPUT:				Session.CLS and other Session variables (for now). 
REVISION HISTORY:	07/02/2008, SRS:	Changes to support saving memory. 
					03/06/2008, SRS:	Original Implementation. 
--->

<!---
This routine will be called ONLY in the context of a Web page, where we have Session variables. It will NEVER be called 
in the context of a Web Service. Therefore, we can call TrimArrayUserRoles("No") to build Session.ArrayUserRoles too: 
--->
<cfif NOT IsDefined("TrimArrayUserRoles")>
	<cfinclude template="/library/udf/bld_SecurityUDFs.cfm">
</cfif>
<cfset TrimArrayUserRoles("No")>
<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfset Session.ArrayUserRoles					= Variables.ArrayUserRoles>
	<cfset Session.CLS								= Duplicate(Variables.CLS)>
	<!--- The following loops are temporary, until we can all stop referencing GLS data outside of Session.CLS: --->
	<cfloop index="Level1Key"						list="#StructKeyList(Variables.CLS)#">
		<cfif IsStruct(Variables.CLS[Level1Key])>
			<cfset Session[Level1Key]				= Duplicate(Variables.CLS[Level1Key])>
			<cfloop index="Level2Key"				list="#StructKeyList(Variables.CLS[Level1Key])#">
				<cfset Session[Level2Key]			= Variables.CLS[Level1Key][Level2Key]>
			</cfloop>
		<cfelse>
			<cfswitch expression="#UCase(Level1Key)#">
			<cfcase value="ARRAYUSERROLES,ARRAYALLUSERROLES">
				<!---
				Session.ArrayUserRoles has already been defined, above. And we will henceforth keep ArrayAllUserRoles 
				only in Session.CLS.ArrayAllUserRoles. (BIG waste of memory to save it anywhere else.) 
				--->
			</cfcase>
			<cfdefaultcase>
				<cfset Session[Level1Key]			= Variables.CLS[Level1Key]>
			</cfdefaultcase>
			</cfswitch>
		</cfif>
	</cfloop>
</cflock>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 120>
	<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 120 characters. "
							& "(Currently, it's #Len(Variables.FVal)# characters long.) </li>">
</cfif>

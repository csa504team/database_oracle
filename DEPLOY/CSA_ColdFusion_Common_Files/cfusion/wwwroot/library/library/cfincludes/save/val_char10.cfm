<!---
AUTHOR:				Steve Seaquist
DATE:				12/15/2000
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR10
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	01/15/2002, SRS:	Change of methodology: Do everything in the val files, including setting the cleaned-up 
										variable into Variables.#FNam#, building the URLString and setting BlankScreen. 
					12/15/2000, SRS:	Original implementation.
--->
<CFSET Variables.FVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFIF ISDEFINED("Variables.Clean") and Variables.Clean IS "Y">
		<CFSET Variables.FVal					= Replace(Variables.FVal, "-", "", "ALL")>
	</CFIF>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFIF Len(Variables.FVal) GT 10>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot be more than 10 characters long.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

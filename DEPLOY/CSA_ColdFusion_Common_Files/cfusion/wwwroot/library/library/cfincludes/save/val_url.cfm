<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) GT 255>
		<cfset Variables.ErrMsg			= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 255 characters.</li>">
	<cfelseif Len(Variables.FVal) LT 9>
		<cfset Variables.ErrMsg			= "#Variables.ErrMsg# <li>#Variables.FName# must be at least 9 characters.</li>">
	<cfelse>
		<cfset Variables.ProtocolOK		= "Yes">
		<cfif		Left(Variables.FVal,7) IS "http://">	<cfset Variables.ServerOn	= RemoveChars(Variables.FVal, 1, 7)>
		<cfelseif	Left(Variables.FVal,8) IS "https://">	<cfset Variables.ServerOn	= RemoveChars(Variables.FVal, 1, 8)>
		<cfelseif	Left(Variables.FVal,6) IS "ftp://">		<cfset Variables.ServerOn	= RemoveChars(Variables.FVal, 1, 6)>
		<cfelseif	Left(Variables.FVal,9) IS "gopher://">	<cfset Variables.ServerOn	= RemoveChars(Variables.FVal, 1, 9)>
		<cfelseif	Left(Variables.FVal,9) IS "telnet://">	<cfset Variables.ServerOn	= RemoveChars(Variables.FVal, 1, 9)>
		<cfelse>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must begin with ""http://"", "
										& """https://"", ""ftp://"", ""gopher://"" or ""telnet://"".</li>">
			<cfset Variables.ProtocolOK	= "No">
		</cfif>
		<cfif Variables.ProtocolOK>
			<cfset Variables.Split1		= ListToArray(Variables.ServerOn, "/")>			<!--- Strip path, etc, if any. --->
			<cfset Variables.Split2		= ListToArray(Variables.Split1[1], ":")>		<!--- Strip port number, if any. --->
			<cfset Variables.Split3		= ListToArray(Variables.Split2[1], ".")>
			<cfset Variables.TLDN		= Variables.Split3[ArrayLen(Variables.Split3)]>	<!--- "Top Level Domain name". --->
			<cfif NOT(	(Variables.TLDN IS "com")
					 OR	(Variables.TLDN IS "edu")
					 OR	(Variables.TLDN IS "gov")
					 OR	(Variables.TLDN IS "int")
					 OR	(Variables.TLDN IS "mil")
					 OR	(Variables.TLDN IS "net")
					 OR	(Variables.TLDN IS "org")
					 OR	(Variables.TLDN IS "cc")
					 OR	(Variables.TLDN IS "nu")
					 OR	(Variables.TLDN IS "to")
					 OR	(Variables.TLDN IS "us"))>
				<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>#Variables.FName#'s server name must end in a United States "
										& "Top Level Domain name (com, edu, gov, int, mil, net, org, cc, nu, to or us).</li>">
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif NOT IsNumeric(Variables.FVal)>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not numeric.</li>">
	<cfelseif Variables.FVal LT 0>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot be a negative number.</li>">
	<cfelseif Variables.FVal GT 1>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 2 digits.</li>">
	<cfelse>
		<cfset "Variables.#FNam#"	= NumberFormat(Variables.FVal, "0")>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel, cf_setrequesttimeout, etc. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm right after <cfapplication>, it will include this file for you. 
	Therefore, you would normally NOT include this file directly. It has been separated out of get_sbalookandfeel_variables 
	for use in Web Services, apps without <cfapplication> (and hence don't have Session variables) and other situations where 
	you would not be including get_sbalookandfeel_variables. However, because the output variables will most often be used in 
	the SBA look-and-feel context, they retain the "Slaf" prefix. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!! 

INPUT:				None. 
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other 
					variables defined by the application. Set RequestTimeout from URL.RequestTimeout for upward compatibility. 
					(Can be overridden later by caller.) 
REVISION HISTORY:	01/31/2006, SRS:	Original implementation. 
--->

<cfset Request.SlafServerName								= "">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.SlafServerName")>
		<cfset Request.SlafServerName						= Server.SlafServerName>
	</cfif>
	<cfset Request.SlafServerOsName							= Trim(Server.OS.Name)>
	<cfset Request.SlafVersionList							= Server.ColdFusion.ProductVersion><!--- e.g.: "7,0,0,91690" --->
	<cfset Request.SlafVersionMajor							= ListGetAt(Request.SlafVersionList, 1)>
	<cfset Request.SlafVersionMinor							= ListGetAt(Request.SlafVersionList, 2)>
	<cfset Request.SlafVersion								= "#Request.SlafVersionMajor#.#Request.SlafVersionMinor#">
</cflock>
<cfif Len(Request.SlafServerName) IS 0>
	<cfobject action="CREATE" type="JAVA" name="Variables.InetAddress" class="java.net.InetAddress">
	<cfset Request.SlafServerName							= Trim(Variables.InetAddress.getLocalHost().getHostName())>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafServerName						= Request.SlafServerName>
	</cflock>
</cfif>
<cfset Request.SlafLocalHost								= "#Request.SlafServerName#.sba.gov">
<!--- Note: In the following, "EOL" is short for the platform-specific "end of line" characters that terminate text lines. --->
<cfif		FindNoCase("Windows",	Request.SlafServerOsName) GT 0>
	<cfset Request.SlafEOL									= Chr(13)&Chr(10)><!--- CRLF (Windows) --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafWindows								= "Yes">
	<cfset Request.SlafUnix									= "No">
<cfelseif	FindNoCase("Mac",		Request.SlafServerOsName) GT 0><!--- CFMX 7.01(+) supports Mac OS X(+). --->
	<!--- NOTE: The SBA doesn't have any Mac servers yet, but coding for compatibility keeps that option open. --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Same as Unix, because Mac OS X is Unix). --->
	<cfset Request.SlafMacintosh							= "Yes">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
<cfelse><!--- Because it's the "else" condition, Unix is the default: --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Unix). --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
</cfif>

<!---
If you call Now() repeatedly near midnight, it's possible for the displayed date not to match up with the displayed time. 
Therefore we grab it once and use it for all calculated date/time values. That way, they'll all match up. 
--->
<cfset Request.SlafDateTimeNow								= Now()>
<!--- Calculate current Federal Fiscal Year: --->
<cfset Request.SlafCurrFY									= Year(Request.SlafDateTimeNow)>
<cfif Month(Request.SlafDateTimeNow) GT 9>
	<cfset Request.SlafCurrFY								= Request.SlafCurrFY + 1>
</cfif>
<!--- Request.SlafDateTimeNow, formatted for display in a consistent manner: --->
<cfset Request.SlafDateTimeNowDisplay						= DateFormat(Request.SlafDateTimeNow, "YYYY-MM-DD")
															& " "
															& TimeFormat(Request.SlafDateTimeNow, "hh:mm:ss tt")>

<cfif IsDefined("URL.RequestTimeout")>
	<cf_SetRequestTimeout seconds="#URL.RequestTimeout#">
</cfif>

<!--- SBA-Only IP Restriction, for applications that need it: --->

<cfset Request.SlafIPAddrIsSBA								= "No"><!--- Affects lots of stuff, including security. Very important. --->
<cfset Variables.SlafArrayRemote_Addr						= ListToArray(CGI.Remote_Addr, ".")>
<cfswitch expression="#Variables.SlafArrayRemote_Addr[1]#"><!--- The cfcases are ordered numerically for easy lookup: --->
<cfcase value="10"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
	<cfset Request.SlafIPAddrIsSBA							= "Yes">
</cfcase>
<cfcase value="63"><!--- 63.241.202.* are our proxy servers, including the VPN server. --->
	<cfif	(Variables.SlafArrayRemote_Addr[2] IS 241)
		AND	(Variables.SlafArrayRemote_Addr[3] IS 202)>
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfif>
</cfcase>
<cfcase value="127"><!--- Need to be at SBA to access datasources, so localhost must be SBA user. --->
	<cfif	(Variables.SlafArrayRemote_Addr[2] IS 0)
		AND	(Variables.SlafArrayRemote_Addr[3] IS 0)>
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfif>
</cfcase>
<cfcase value="165"><!--- According to Chuda, we own the entire 165.110 Class B network. --->
	<cfif	Variables.SlafArrayRemote_Addr[2] IS "110">
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfif>
</cfcase>
<cfcase value="172"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
	<cfif	(Variables.SlafArrayRemote_Addr[2] GE 16)
		AND	(Variables.SlafArrayRemote_Addr[2] LE 31)>	<!--- 172.16/12 --->
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfif>
</cfcase>
<cfcase value="192"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
	<cfif	Variables.SlafArrayRemote_Addr[2] IS 168>
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfif>
</cfcase>
</cfswitch>

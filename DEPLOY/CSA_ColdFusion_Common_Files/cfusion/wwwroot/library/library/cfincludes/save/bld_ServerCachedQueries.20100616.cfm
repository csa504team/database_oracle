<!---
AUTHOR:				Steve Seaquist
DATE:				04/28/2006
DESCRIPTION:		Makes sure that all "server cached queries" ("Scq") variables are defined in the Server scope. 
NOTES:

	Cfinclude this file before attempting to retrieve Scq variables from the Server scope. 

	Don't be misled by this file's name. It's a "build" routine, not a "get" routine. All it does is build server cached 
	queries in the SERVER scope, and only if necessary. THIS FILE DOES NOT ACTUALLY COPY SCQ VARIABLES INTO THE VARIABLES 
	OR REQUEST SCOPE. It just makes sure they're defined so that YOU can copy them into the Variables or Request scope, 
	because only you know which ones you actually need. 

	The purpose of the "Scq" prefix (as with the Slaf prefix in SBA look-and-feel) is to avoid naming conflicts with your 
	own variables. When you copy them into the Variables or Request scope, uou can keep their names the same (to show their 
	origin) or rename them (to make your code more readable). In the following example, Server.Scq.ActvStTbl is renamed to 
	[Variables.]getStates for readability: 

		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cflock scope="SERVER" type="READONLY" timeout="30">
			<cfset getStates					= Server.Scq.ActvStTbl>
		</cflock>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use the datasource "public_sbaref" (which uses the login "sbaselect"), you cannot include this file inside 
	a cftransaction that uses a different datasource or login. But it doesn't really have to be close to the code that locks 
	the Server scope. Just include this file at the beginning of your CFM file, among its initializations. 

	Note that code and description columns are available in 2 ways: (1) as their actual database column names, and (2) as 
	"code" and "description". If you don't want to use "code" and "description" (because they aren't unique across queries), 
	then don't use them. No harm done. They exist for use by shared routines (eg: to populate a variety of drop-down menus). 

INPUT:				None. 
OUTPUT:				Server scope variables prefixed with "Scq" (short for "server cached queries"), so as not to conflict 
					with other variables defined by the application. If the current user happens to be the one who takes 
					the I/O hit to define the queries, the "Scq" variables will also be defined in the Variables scope. 
					Generally, this will only happen when this routine is executed on its own as a Scheduled Task. 
REVISION HISTORY:	06/16/2010, SRS:	Added Variables.cfpra to our save-and-restore code, with StructDelete, so that it 
										won't mess up our SPC calls if it's defined. 
					04/07/2010, NNI:	Added AllBusTypTbl, AllCitznshipCdTbl, AllEthnicCdTbl, AllGndrTbl, AllIMCrdtScorSourcTblBus, 
										AllIMCrdtScorSourcTblPer, AllIMEPCOperCdTbl, AllLoanStatCmntCdTbl, AllVetTbl
					04/07/2010, NNI:	Added sbaref.OutlawTbl.
					03/21/2010, NNI:	Replaced oracle_analytic_publicread to use oracle_transaction_publicread after dsn was modified on server.
					03/09/2010, NNI:	Removed CohortCd from ActvPrgrmValidTbl. 
					02/01/2010, NNI:	Modified LoanStatCdTbl to add ColsonStatCd column. 
					12/10/2009, NNI:	Modified PrcsMthdTbl and PrgrmValidTbl generation to account for changes to table stucture. 
					09/10/2009, SRS:	Added Scq.pronet.CbtLists.QMS (Quick Market Search). 
					07/10/2009, NNI:	Added ActvACHAcctTypTbl.
					06/09/2009, SRS:	Added lists based on ActvPrcsMthdTbl (similar to those built in ActvCCRBusTypCdTbl). 
										Improved building of lists, so that they won't bleed over between cfcases. 
					06/04/2009, NNI:	Added AllAppropIndTbl and three new columns to ActvPrcsMthdTbl and AllPrcsMthdTbl. 
					03/04/2009, SRS:	Added pronet.ActvCCRBusTypCdTbl and pronet.ActvIMMinCdTbl. 
					02/19/2009, SRS:	Added a "cascading forced rebuild" capability. Specifically, if the caller is forcing a 
										rebuild of Server.Scq, we pass that forced rebuild along to get_sbashared_variables, 
										which will in turn cascade to a forced rebuild of get_actual_server_name. 
					02/09/2009, NNI:	Added PrgrmCd to the Processing Method tables.
					02/05/2009, SRS:	In self-test mode, eliminated the cfdump unless specifically requested by "?Debug=Yes". 
					01/25/2009, SRS:	Got rid of special test for eweb1 server names which was messing up in the case 
										of niagara (new eweb1 server). 
					01/25/2009, SRS:	Changed over partner to use Oracle, because PROD1's hard drive got hosed. 
					11/26/2008, SRS:	Redid server-related queries using real Sybase tables. (Sybase only for now.) 
					10/24/2008, SRS:	Added server-related queries, first as pseudo-tables. 
					10/14/2008, SRS:	Added technet structure to amazon/missouri for the TECH-Net release. 
					09/16/2008, SRS:	Added tests for "colorado" and "redriver". 
					05/29/2008, SRS:	Added AllSpcPurpsLoanTbl. 
					04/30/2008, SRS:	Added support for "COOP testing", which has only Sybase available to it. This meant 
										making EVERY request dual Sybase/Oracle. 
					02/21/2008, SRS:	Added support for the Scq.technet substructure and defined 9 new queries to be stored 
										there. Alphabetized all substructures for easy lookup. Reinstated earlier version's 
										"save and restore" variables, plus some extras, so that this file will actually use all 
										of the official, standard names (db, dbtype, ErrMsg, etc). This allows use of SPC files 
										and /library/udf/bld_dbutils_for_dbtype.cfm, if necessary. Defined OracleSchemaName 
										(another official, standard name) and based its value on the current substructure name 
										(partner, pronet, technet or none). This allows queries to be done in any order, 
										not neccesarily grouped by substructure. But mainly, it allows rapidly changing the 
										OracleSchemaName, as we will have to do soon with the advent of viewtechnet. 
					01/09/2008, NNI:	Made changes to code to point to Sybase db server in case of partner database calls. 
					01/07/2008, NNI:	Made changes to code to point to Oracle db servers. public_sbaref, pronet and partner datasources 
										have been changed to oracle_webanalytic_publicread. 
					08/20/2007, DKC:	Added support for partner database cached queries (replaces static html file used in PIMS). 
					06/04/2007, DKC:	Added VetGrpCd to the Scq.ActvVetTbl.
					04/25/2007, SRS:	Per Dileep and Sheri, saved and restored db and dbtype (if pronet is being built). 
					04/04/2007, SRS:	Per Sheri McConville, added ActvSBAOfcTbl result set. Also allowed for AllSBAOfcTbl 
										result set, but because it had 430 rows, removed it from ScqAvailableResultSetNames. 
										(That effectively comments it out / doesn't cache the query.) Added StrtDt and EndDt 
										to all previously-defined "All" result sets, if they have those columns. (It was an 
										oversight not to have included them in the first place.) 
					03/28/2007, SRS:	Per Ron Whalen, added AllPrcsMthdTbl and AllPrgrmTbl result sets for loan servicing. 
					12/07/2006, SRS:	Restricted Scq.pronet loads to explicitly named servers. (Previously, it was loaded 
										by default on all servers except devyesapp and yesapp.) 
					10/03/2006, SRS:	Special exception for Scq.pronet.ActvFrmlDtrmntnCtgryTbl extended to enile.
					09/27/2006, DKC:	Allowed it to be directly executed out of stagelibrary and build the same display.
					09/14/2006, DKC:	Added support for new table IMDomnTypTbl.
					08/11/2006, SRS:	Added support for pronet database cached queries (formerly cached in the Application 
										scope of PRO-Net when PRO-Net was not part of GLS). Also added "new logging". 
					06/14/2006, SRS:	Added 32 new cached query types. Allowed controlling what queries get defined 
										by controlling ScqAvailableResultSetNames. Per Ron Whalen, don't include Scq.ActvCMSACdTbl 
										in ScqAvailableResultSetNames, because it appear not to be in use. 
					05/12/2006, SRS:	Added IMCntryDialCd to ScqActvCountries, for use with dsp_IMCntryCdToIMCntryDialCd. 
					04/28/2006, SRS:	Original implementation. Used cfqueries initially, knowing that we will someday 
										rewrite it to use a stored procedure call (SPC) file. That's why there isn't any 
										cftry/cfcatch code. That'll be automatically added when we call the SPC file. 
--->

<!--- Initializations --->

<cfset		   Variables.ServerCachedQueriesSelfTestMode			= (CGI.Script_Name IS "/library/cfincludes/bld_ServerCachedQueries.cfm")>
<cfparam name="Variables.ServerCachedQueriesForceRebuild"	default="No"><!--- Allows rebuild even if not self test. --->
<cfif Variables.ServerCachedQueriesSelfTestMode>
	<cfset Variables.ServerCachedQueriesForceRebuild				= "Yes">
</cfif>
<cfif Variables.ServerCachedQueriesForceRebuild>
	<cfset Variables.SbaSharedVariablesForceRebuild					= "Yes">
	<cfinclude template="get_sbashared_variables.cfm"><!--- This will cascade to get_actual_server_name too. --->
</cfif>

<!--- First, check the master variable that defines whether or not all the others are defined: --->

<cfif NOT IsDefined("Variables.PageName")>
	<cfset Variables.PageName										= ListGetAt(CGI.Script_Name,
																		ListLen(CGI.Script_Name,"/"),
																								"/")>
</cfif>
<cfset Variables.ScqIsDefined										= "No">
<cfif NOT Variables.ServerCachedQueriesForceRebuild>
	<!---
	When this file is cfincluded (the usual case), the following check will be done. But when executed as a Scheduled Task, 
	or when you manually go to /library/cfincludes/bld_ServerCachedQueries.cfm as a test, the following check will NOT be done. 
	(This forces the queries to be refreshed from the database.) The Scheduled Task is done around 5:00 AM. 
	--->
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq")>
			<cfset Variables.ScqIsDefined							= "Yes">
		</cfif>
	</cflock>
</cfif>

<!--- Build the queries if they don't exist, or if we were told to (by calling this page directly): --->

<cfif NOT Variables.ScqIsDefined>
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqStartTickCount							= GetTickCount()>
	</cfif>
	<cfset Variables.CoopTesting									= "No">
	<cfif FileExists(ExpandPath("/coop.txt"))>
		<cfset Variables.CoopTesting								= "Yes">
	</cfif>
	<cfif NOT IsDefined("Request.SlafServerInstanceName")>
		<cfinclude template="get_actual_server_name.cfm">
	</cfif>
	<cfif NOT IsDefined("Request.SlafServerCachedQueryGroups")>
		<cfinclude template="get_sbashared_variables.cfm">
	</cfif>
	<cf_setrequesttimeout seconds="180"><!--- Must be called AFTER get_sbashared_variables! --->
	<cfset Variables.ScqAvailableResultSetNames						= "">
	<cfif ListFind(Request.SlafServerCachedQueryGroups, "sbaref")	GT 0>
		<!---
		When the Env and Srvr tables exist in Oracle, do a global search and replace from ".sybsbaref" to "" 
		to get rid of sybsbaref structure. Also remove the (cfcase value="Scq.sybsbaref"). 
		--->
		<cfset Variables.ScqAvailableResultSetNames					= ListAppend(Variables.ScqAvailableResultSetNames,
																	"Scq.ActvACHAcctTypTbl"
																	& ",Scq.ActvBusAgeCdTbl"
																	& ",Scq.ActvBusTypTbl"
																	& ",Scq.ActvCacheQryTbl"
																	& ",Scq.ActvCalndrPrdTbl"
																	& ",Scq.ActvCitznshipCdTbl"
																	& ",Scq.ActvCohortCdTbl"
																	& ",Scq.ActvEconDevObjctCdTbl"
																	& ",Scq.ActvEnvTbl"
																	& ",Scq.ActvEnvTypTbl"
																	& ",Scq.ActvIMCntryCdTbl"
																	& ",Scq.ActvIMCrdtScorSourcTblBus"
																	& ",Scq.ActvIMCrdtScorSourcTblPer"
																	& ",Scq.ActvIMCrdtScorSourcTbl"
																	& ",Scq.AllIMCrdtScorSourcTbl"
																	& ",Scq.ActvIMDomnTypTbl"
																	& ",Scq.ActvIMEPCOperCdTbl"
																	& ",Scq.ActvEthnicCdTbl"
																	& ",Scq.ActvGndrTbl"
																	& ",Scq.ActvGndrMFCdTbl"
																	& ",Scq.ActvInjctnTypCdTbl"
																	& ",Scq.ActvLoanCollatTypCdTbl"
																	& ",Scq.ActvLoanCollatValSourcTbl"
																	& ",Scq.ActvLoanCrdtUnavRsnCdTbl"
																	& ",Scq.ActvLoanFinanclStmtSourcTbl"
																	& ",Scq.ActvLoanMFDisastrTypTbl"
																	& ",Scq.ActvLoanMFPrcsMthdTypTbl"
																	& ",Scq.ActvLoanPartLendrTypTbl"
																	& ",Scq.ActvLoanPckgSourcTypTbl"
																	& ",Scq.ActvLoanPrevFinanStatTbl"
																	& ",Scq.ActvLoanProcdTypTbl"
																	& ",Scq.ActvLoanStatCdTbl"
																	& ",Scq.ActvLoanStatCmntCdTbl"
																	& ",Scq.ActvMfSubPrgrmCdTbl"
																	& ",Scq.ActvOutLawTbl"
																	& ",Scq.ActvPrcsMthdTbl"
																	& ",Scq.ActvPrgrmTbl"
																	& ",Scq.ActvPrgrmValidTbl"
																	& ",Scq.ActvRaceCdTbl"
																	& ",Scq.ActvSBAOfcTbl"
																	& ",Scq.ActvSpcPurpsLoanTbl"
																	& ",Scq.ActvSrvrCacheQryTbl"
																	& ",Scq.ActvSrvrLocTbl"
																	& ",Scq.ActvSrvrTbl"
																	& ",Scq.ActvSrvrURLTbl"
																	& ",Scq.ActvStatCdTbl"
																	& ",Scq.ActvStTbl"
																	& ",Scq.ActvVetTbl"
																	& ",Scq.AllAppropIndTbl"
																	& ",Scq.AllBusTypTbl"
																	& ",Scq.AllCitznshipCdTbl"
																	& ",Scq.AllEthnicCdTbl"
																	& ",Scq.AllGndrTbl"
																	& ",Scq.AllIMCrdtScorSourcTblBus"
																	& ",Scq.AllIMCrdtScorSourcTblPer"
																	& ",Scq.AllIMEPCOperCdTbl"
																	& ",Scq.AllLoanStatCmntCdTbl"
																	& ",Scq.AllOutLawTbl"
																	& ",Scq.AllPrcsMthdTbl"
																	& ",Scq.AllPrgrmTbl"
																	& ",Scq.AllRaceCdTbl"
																	& ",Scq.AllSpcPurpsLoanTbl"
																	& ",Scq.AllVetTbl")>
	</cfif>
	<cfif ListFind(Request.SlafServerCachedQueryGroups, "partner")	GT 0>
		<cfset Variables.ScqAvailableResultSetNames					= ListAppend(Variables.ScqAvailableResultSetNames,
																	   "Scq.partner"
																	& ",Scq.partner.AllPrgrmTbl"
																	& ",Scq.partner.AllSearchSubCatTyps"
																	& ",Scq.partner.AllValidLocTypTbl")>
	</cfif>
	<cfif ListFind(Request.SlafServerCachedQueryGroups, "pronet")	GT 0>
		<cfset Variables.ScqAvailableResultSetNames					= ListAppend(Variables.ScqAvailableResultSetNames,
																	   "Scq.pronet"
																	& ",Scq.pronet.ActvCCRBusTypCdTbl"
																	& ",Scq.pronet.ActvCountryCdTblForeign"
																	& ",Scq.pronet.ActvExportMainActvtyTypTbl"
																	& ",Scq.pronet.ActvExportMrktTypTbl"
																	& ",Scq.pronet.ActvFrmlDtrmntnCtgryTbl"
																	& ",Scq.pronet.ActvIMAreaTypCdTbl"
																	& ",Scq.pronet.ActvIMMinCdTbl"
																	& ",Scq.pronet.ActvIMQAStdCdTbl"
																	& ",Scq.pronet.ActvMntrAreaTypTbl"
																	& ",Scq.pronet.AllCountryCdTbl"
																	& ",Scq.pronet.AllIMQAStdCdTbl")>
	</cfif>
	<cfif ListFind(Request.SlafServerCachedQueryGroups, "technet")	GT 0>
		<cfset Variables.ScqAvailableResultSetNames					= ListAppend(Variables.ScqAvailableResultSetNames,
																	   "Scq.technet"
																	& ",Scq.technet.ActvIMAwrdInvestSaleTypCdTbl"
																	& ",Scq.technet.ActvIMAwrdMentrTypCdTbl"
																	& ",Scq.technet.ActvIMAwrdSECdTbl"
																	& ",Scq.technet.ActvIMFedAgncyTbl"
																	& ",Scq.technet.ActvIMFedAgncyTbl4XML"
																	& ",Scq.technet.ActvIMFedAgncyBrTbl"
																	& ",Scq.technet.ActvIMFedAgncyBrTbl4XML"
																	& ",Scq.technet.ActvIMPrgrmTbl"
																	& ",Scq.technet.ActvIMSTTRPrtCatTbl"
																	& ",Scq.technet.ActvIMSTTRPrtTypTbl")>
	</cfif>
	<!---
	Since the caller may already have defined db, dbtype, TxnErr and/or ErrMsg, save them, 
	so as not to mess up the caller. We will restore them at the end of this cfif, 
	assuming we don't crash, of course. 
	--->
	<cfif IsDefined("Variables.cfpra")>
		<cfset Variables.ScqCFPRA									= Variables.cfpra>
		<cfset ArrayClear(Variables.cfpra)>
		<cfset StructDelete(Variables, "cfpra")>
	</cfif>
	<cfif IsDefined("Variables.cfprname")>
		<cfset Variables.ScqCFPRName								= Variables.cfprname>
	</cfif>
	<cfif IsDefined("Variables.db")>
		<cfset Variables.ScqDB										= Variables.db>
	</cfif>
	<cfif IsDefined("Variables.dbtype")>
		<cfset Variables.ScqDBType									= Variables.dbtype>
	</cfif>
	<cfif IsDefined("Variables.ErrMsg")>
		<cfset Variables.ScqErrMsg									= Variables.ErrMsg>
	</cfif>
	<cfif IsDefined("Variables.LogAct")>
		<cfset Variables.ScqLogAct									= Variables.LogAct>
	</cfif>
	<cfif IsDefined("Variables.Sybase")>
		<cfset Variables.ScqSybase									= Variables.Sybase>
	</cfif>
	<cfif IsDefined("Variables.TxnErr")>
		<cfset Variables.ScqTxnErr									= Variables.TxnErr>
	</cfif>
	<!--- Set the datasource based on actual server name: --->
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfswitch expression="#Request.SlafServerGroup#"><!--- No hole in the firewall for eweb1 servers: --->
	<cfcase value="eweb1">				<cfset Variables.db_default	= "oracle_webanalytic_publicread">	</cfcase>
	<cfdefaultcase>						<cfset Variables.db_default	= "oracle_transaction_publicread">		</cfdefaultcase>
	</cfswitch>
	<cfset Variables.db												= Variables.db_default>
	<cfset Variables.dbtype											= "Oracle80">
	<cfset Variables.ErrMsg											= "Problems occurred while attempting "
																	& "to cache database queries: ">
	<cfset Variables.TxnErr											= "No">
	<cfif Variables.CoopTesting>
		<cfset Variables.db											= "sbaref">
		<cfset Variables.db_default									= "sbaref">
		<cfset Variables.dbtype										= "Sybase11">
	</cfif>
	<!---
	Do the queries outside of the exclusive cflock of the Server scope, so as to minimize exclusive lock time. Also, 
	because some servers have no need of some cached queries, define the queries in a loop. This allows creating or 
	not creating a query simply by altering the contents of ScqAvailableResultSetNames on a server-by-server basis. 
	--->
	<cfset Variables.Scq											= StructNew()>
	<cfloop index="ScqQueryName" list="#Variables.ScqAvailableResultSetNames#">
		<!--- The following code makes sure that db, etc, is ALWAYS correct for the current substructure. --->
		<cfset Variables.ScqQueryNameArray							= ListToArray(ScqQueryName, ".")>
		<cfif ArrayLen(Variables.ScqQueryNameArray) GT 2>
			<cfset Variables.ScqSubstructureName					= Variables.ScqQueryNameArray[2]>
		<cfelse>
			<cfset Variables.ScqSubstructureName					= "">
		</cfif>
		<cfif Variables.CoopTesting>
			<cfswitch expression="#Variables.ScqSubstructureName#">
			<cfcase value="partner">	<cfset Variables.db			= "partner_scheduled">	</cfcase>
			<cfdefaultcase>				<cfset Variables.db			= "sbaref">				</cfdefaultcase>
			</cfswitch>
			<cfset Variables.dbtype									= "Sybase11">
		<cfelse>
			<cfswitch expression="#Variables.ScqSubstructureName#">
			<cfcase value="partner">
				<cfset Variables.db									= Variables.db_default>
				<cfset Variables.dbtype								= "Oracle80">
			</cfcase>
			<cfcase value="pronet">
				<cfset Variables.db									= "oracle_webanalytic_publicread">
				<cfset Variables.dbtype								= "Oracle80">
			</cfcase>
			<cfcase value="technet">
				<cfset Variables.db									= "oracle_webtransaction_publicread">
				<cfset Variables.dbtype								= "Oracle80">
			</cfcase><!--- Soon: viewtechnet!!! --->
			<cfdefaultcase>
				<cfset Variables.db									= Variables.db_default>
				<cfset Variables.dbtype								= "Oracle80">
			</cfdefaultcase>
			</cfswitch>
		</cfif>
		<cfset Variables.Sybase										= (Left(Variables.dbtype,6) IS "Sybase")>
		<!---
		Although ScqAvailableResultSetNames is defined alphabetically (all Actvs, followed by all Alls), it's easier 
		to maintain the result sets if the same table's result sets are adjacent. So the ordering below defines 
		ActvPrgrmTbl, immediately followed by AllPrgrmTbl, for example. (Knowing this makes it easier to find stuff.) 
		--->
		<cfswitch expression="#ScqQueryName#">
		<cfcase				  value="Scq.ActvACHAcctTypTbl">
			<cfquery name="Variables.Scq.ActvACHAcctTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ACHAcctTypCd,
						ACHAcctTypDescTxt,
						ACHAcctTypCd								AS code,
						ACHAcctTypDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..ACHAcctTypTbl<cfelse><!--- else = default = Oracle: --->
			from		sbaref.ACHAcctTypTbl</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllAppropIndTbl">
			<cfquery name="Variables.Scq.AllAppropIndTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		AppropInd,
						AppropIndDescTxt,
						AppropInd									AS code,
						AppropIndDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..AppropIndTbl<cfelse><!--- else = default = Oracle: --->
			from		sbaref.AppropIndTbl</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvBusAgeCdTbl">
			<cfquery name="Variables.Scq.ActvBusAgeCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		BusAgeCd,
						BusAgeDesc,
						BusMFAgeCd,
						BusAgeCd									AS code,
						BusAgeDesc									AS description<cfif Variables.Sybase>
			from		sbaref..BusAgeCdTbl
			where		(	datediff(dd,BusAgeStrtDt,				getdate()) >= 0)
			and			(				BusAgeEndDt					is null
						or	datediff(dd,BusAgeEndDt,				getdate()) <= 0)<cfelse><!--- else = default = Oracle: --->
			from		sbaref.BusAgeCdTbl
			where		(	(sysdate -	BusAgeStrtDt)				>= 0)
			and			(				BusAgeEndDt					is null
						or	(sysdate -	BusAgeEndDt)				<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvBusTypTbl">
			<cfquery name="Variables.Scq.ActvBusTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		BusTypCd,
						BusTypTxt,
						BusTypMFCd,
						BusTypMFTxt,
						BusTypCd									AS code,
						BusTypTxt									AS description<cfif Variables.Sybase>
			from		sbaref..BusTypTbl
			where		(	datediff(dd,BusTypStrtDt,				getdate()) >= 0)
			and			(				BusTypEndDt					is null
						or	datediff(dd,BusTypEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.BusTypTbl
			where		(	(sysdate -	BusTypStrtDt)				>= 0)
			and			(				BusTypEndDt					is null
						or	(sysdate -	BusTypEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllBusTypTbl">
			<cfquery name="Variables.Scq.AllBusTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		BusTypCd,
						BusTypTxt,
						BusTypMFCd,
						BusTypMFTxt,
						BusTypStrtDt,
						BusTypEndDt,
						BusTypCd									AS code,
						BusTypTxt									AS description<cfif Variables.Sybase>
			from		sbaref..BusTypTbl<cfelse>
			from		sbaref.BusTypTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCacheQryTbl">
			<cfquery name="Variables.Scq.ActvCacheQryTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		rtrim(CacheQryCd)							as CacheQryCd,
						CacheQryDescTxt,
						rtrim(CacheQryCd)							as code,
						CacheQryDescTxt								as description<cfif Variables.Sybase>
			from		sbaref..CacheQryTbl
			where		(	datediff(dd,CacheQryStrtDt,				getdate()) >= 0)
			and			(				CacheQryEndDt				is null
						or	datediff(dd,CacheQryEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.CacheQryTbl
			where		(	(sysdate -	CacheQryStrtDt)				>= 0)
			and			(				CacheQryEndDt				is null
						or	(sysdate -	CacheQryEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCalndrPrdTbl">
			<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CalndrPrdCd,
						CalndrPrdDesc,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CalndrPrdCd = 'M'						then 1
						when CalndrPrdCd = 'Q'						then 2
						when CalndrPrdCd = 'S'						then 3
						when CalndrPrdCd = 'A'						then 4
						else										5
						end											AS displayorder,
						CalndrPrdCd									AS code,
						CalndrPrdDesc								AS description<cfif Variables.Sybase>
			from		sbaref..CalndrPrdTbl
			where		(	datediff(dd,CalndrPrdStrtDt,			getdate()) >= 0)
			and			(				CalndrPrdEndDt				is null
						or	datediff(dd,CalndrPrdEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.CalndrPrdTbl
			where		(	(sysdate -	CalndrPrdStrtDt)			>= 0)
			and			(				CalndrPrdEndDt				is null
						or	(sysdate -	CalndrPrdEndDt)				<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCitznshipCdTbl">
			<cfquery name="Variables.Scq.ActvCitznshipCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CitznshipCd,
						CitznshipTxt,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CitznshipCd = 'US'						then 1
						when CitznshipCd = 'RA'						then 2
						when CitznshipCd = 'NR'						then 3
						when CitznshipCd = 'IA'						then 4
						else										5
						end											AS displayorder,
						CitznshipCd									AS code,
						CitznshipTxt								AS description<cfif Variables.Sybase>
			from		sbaref..CitznshipCdTbl
			where		(	datediff(dd,CitznshipStrtDt,			getdate()) >= 0)
			and			(				CitznshipEndDt				is null
						or	datediff(dd,CitznshipEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.CitznshipCdTbl
			where		(	(sysdate -	CitznshipStrtDt)			>= 0)
			and			(				CitznshipEndDt				is null
						or	(sysdate -	CitznshipEndDt)				<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllCitznshipCdTbl">
			<cfquery name="Variables.Scq.AllCitznshipCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CitznshipCd,
						CitznshipTxt,
						CitznshipStrtDt,
						CitznshipEndDt,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CitznshipCd = 'US'						then 1
						when CitznshipCd = 'RA'						then 2
						when CitznshipCd = 'NR'						then 3
						when CitznshipCd = 'IA'						then 4
						else										5
						end											AS displayorder,
						CitznshipCd									AS code,
						CitznshipTxt								AS description<cfif Variables.Sybase>
			from		sbaref..CitznshipCdTbl<cfelse>
			from		sbaref.CitznshipCdTbl</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCMSACdTbl">
			<cfquery name="Variables.Scq.ActvCMSACdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CMSACd,
						CMSANm,
						CMSACd										AS code,
						CMSANm										AS description<cfif Variables.Sybase>
			from		sbaref..CMSACdTbl<cfelse>
			from		sbaref.CMSACdTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCohortCdTbl">
			<cfquery name="Variables.Scq.ActvCohortCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CohortCd,
						CohortDesc,
						CohortCd									AS code,
						CohortDesc									AS description<cfif Variables.Sybase>
			from		sbaref..CohortCdTbl
			where		(	datediff(dd,CohortStrtDt,				getdate()) >= 0)
			and			(				CohortEndDt					is null
						or	datediff(dd,CohortEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.CohortCdTbl
			where		(	(sysdate -	CohortStrtDt)				>= 0)
			and			(				CohortEndDt					is null
						or	(sysdate -	CohortEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEconDevObjctCdTbl">
			<cfquery name="Variables.Scq.ActvEconDevObjctCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		EconDevObjctCd,
						EconDevObjctTxt,
						EconDevObjctCd								AS code,
						EconDevObjctTxt								AS description<cfif Variables.Sybase>
			from		sbaref..EconDevObjctCdTbl
			where		(	datediff(dd,EconDevObjctStrtDt,			getdate()) >= 0)
			and			(				EconDevObjctEndDt			is null
						or	datediff(dd,EconDevObjctEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.EconDevObjctCdTbl
			where		(	(sysdate -	EconDevObjctStrtDt)			>= 0)
			and			(				EconDevObjctEndDt			is null
						or	(sysdate -	EconDevObjctEndDt)			<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEnvTbl">
			<cfquery name="Variables.Scq.ActvEnvTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		rtrim(EnvCd)								as EnvCd,
						EnvDescTxt,
						rtrim(EnvTypCd)								as EnvTypCd,
						LognURLTxt,
						rtrim(EnvCd)								as code,
						EnvDescTxt									as description,
						case EnvTypCd
						when 'Dev'	then							0
						when 'Test'	then							1
						else										2
						end											as DevTestProdInd<cfif Variables.Sybase>
			from		sbaref..EnvTbl
			where		(	datediff(dd,EnvStrtDt,					getdate()) >= 0)
			and			(				EnvEndDt					is null
						or	datediff(dd,EnvEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.EnvTbl
			where		(	(sysdate -	EnvStrtDt)					>= 0)
			and			(				EnvEndDt					is null
						or	(sysdate -	EnvEndDt)					<= 0)</cfif>
			order by	DevTestProdInd,
						EnvCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEnvTypTbl">
			<cfquery name="Variables.Scq.ActvEnvTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		rtrim(EnvTypCd)								as EnvTypCd,
						EnvTypDescTxt,
						rtrim(EnvTypCd)								as code,
						EnvTypDescTxt								as description,
						case EnvTypCd
						when 'Dev'	then							0
						when 'Test'	then							1
						else										2
						end											as DevTestProdInd<cfif Variables.Sybase>
			from		sbaref..EnvTypTbl
			where		(	datediff(dd,EnvTypStrtDt,				getdate()) >= 0)
			and			(				EnvTypEndDt					is null
						or	datediff(dd,EnvTypEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.EnvTypTbl
			where		(	(sysdate -	EnvTypStrtDt)				>= 0)
			and			(				EnvTypEndDt					is null
						or	(sysdate -	EnvTypEndDt)				<= 0)</cfif>
			order by	DevTestProdInd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCntryCdTbl">
			<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCntryCd,
						IMCntryNm,
						IMCntryDialCd,
						IMCntryCd									AS code,
						IMCntryNm									AS description<cfif Variables.Sybase>
			from		sbaref..IMCntryCdTbl
			where		(	datediff(dd,IMCntryStrtDt,				getdate()) >= 0)
			and			(				IMCntryEndDt				is null
						or	datediff(dd,IMCntryEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.IMCntryCdTbl
			where		(	(sysdate -	IMCntryStrtDt)				>= 0)
			and			(				IMCntryEndDt				is null
						or	(sysdate -	IMCntryEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblBus">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblBus"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl
			where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.IMCrdtScorSourcTbl
			where		(	(sysdate -	IMCrdtScorSourcStrtDt)		>= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(sysdate -	IMCrdtScorSourcEndDt)		<= 0)</cfif>
			and			(IMCrdtScorSourcBusPerInd					= 'B')
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllIMCrdtScorSourcTblBus">
			<cfquery name="Variables.Scq.AllIMCrdtScorSourcTblBus"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl<cfelse>
			from		sbaref.IMCrdtScorSourcTbl</cfif>
			where		(IMCrdtScorSourcBusPerInd					= 'B')
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblPer">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblPer"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
						when IMCrdtScorSourcCd = 12					then 1
						when IMCrdtScorSourcCd = 13					then 2
						when IMCrdtScorSourcCd = 11					then 3
						when IMCrdtScorSourcCd = 14					then 4
						when IMCrdtScorSourcCd = 15					then 5
						else										6
						end											AS displayorder,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl
			where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.IMCrdtScorSourcTbl
			where		(	(sysdate -	IMCrdtScorSourcStrtDt)		>= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(sysdate -	IMCrdtScorSourcEndDt)		<= 0)</cfif>
			and			(IMCrdtScorSourcBusPerInd					= 'P')
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTbl">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl
			where		(	datediff(dd,IMCrdtScorSourcStrtDt,		getdate()) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	datediff(dd,IMCrdtScorSourcEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.IMCrdtScorSourcTbl
			where		(	(sysdate -	IMCrdtScorSourcStrtDt)		>= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(sysdate -	IMCrdtScorSourcEndDt)		<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllIMCrdtScorSourcTbl">
			<cfquery name="Variables.Scq.AllIMCrdtScorSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl<cfelse>
			from		sbaref.IMCrdtScorSourcTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllIMCrdtScorSourcTblPer">
			<cfquery name="Variables.Scq.AllIMCrdtScorSourcTblPer"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcStrtDt,
						IMCrdtScorSourcEndDt,
						case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
						when IMCrdtScorSourcCd = 12					then 1
						when IMCrdtScorSourcCd = 13					then 2
						when IMCrdtScorSourcCd = 11					then 3
						when IMCrdtScorSourcCd = 14					then 4
						when IMCrdtScorSourcCd = 15					then 5
						else										6
						end											AS displayorder,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..IMCrdtScorSourcTbl<cfelse>
			from		sbaref.IMCrdtScorSourcTbl</cfif>
			where		(IMCrdtScorSourcBusPerInd					= 'P')
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMDomnTypTbl">
			<cfquery name="Variables.Scq.ActvIMDomnTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select 		IMDomnTypCd									AS code,
						IMDomnTypDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..IMDomnTypTbl
			where 		(	datediff(dd,IMDomnTypStrtDt,			getdate()) >= 0)
			and			(				IMDomnTypEndDt				is null
						or	datediff(dd,IMDomnTypEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.IMDomnTypTbl
			where		(	(sysdate -	IMDomnTypStrtDt)			>= 0)
			and			(				IMDomnTypEndDt				is null
						or	(sysdate -	IMDomnTypEndDt)				<= 0)</cfif>
			order by	IMDomnTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMEPCOperCdTbl">
			<cfquery name="Variables.Scq.ActvIMEPCOperCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMEPCOperCd,
						IMEPCOperDescTxt,
						IMEPCOperCd									AS code,
						IMEPCOperDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..IMEPCOperCdTbl
			where		(	datediff(dd,IMEPCOperCdStrtDt,			getdate()) >= 0)
			and			(				IMEPCOperCdEndDt			is null
						or	datediff(dd,IMEPCOperCdEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.IMEPCOperCdTbl
			where		(	(sysdate -	IMEPCOperCdStrtDt)			>= 0)
			and			(				IMEPCOperCdEndDt			is null
						or	(sysdate -	IMEPCOperCdEndDt)			<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllIMEPCOperCdTbl">
			<cfquery name="Variables.Scq.AllIMEPCOperCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMEPCOperCd,
						IMEPCOperDescTxt,
						IMEPCOperCdStrtDt,
						IMEPCOperCdEndDt,
						IMEPCOperCd									AS code,
						IMEPCOperDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..IMEPCOperCdTbl<cfelse>
			from		sbaref.IMEPCOperCdTbl</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEthnicCdTbl">
			<cfquery name="Variables.Scq.ActvEthnicCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		EthnicCd,
						EthnicDesc,
						EthnicCd									AS code,
						EthnicDesc									AS description<cfif Variables.Sybase>
			from		sbaref..EthnicCdTbl
			where		(	datediff(dd,EthnicStrtDt,				getdate()) >= 0)
			and			(				EthnicEndDt					is null
						or	datediff(dd,EthnicEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.EthnicCdTbl
			where		(	(sysdate -	EthnicStrtDt)				>= 0)
			and			(				EthnicEndDt					is null
						or	(sysdate -	EthnicEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllEthnicCdTbl">
			<cfquery name="Variables.Scq.AllEthnicCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		EthnicCd,
						EthnicDesc,
						EthnicStrtDt,
						EthnicEndDt,
						EthnicCd									AS code,
						EthnicDesc									AS description<cfif Variables.Sybase>
			from		sbaref..EthnicCdTbl<cfelse>
			from		sbaref.EthnicCdTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrTbl">
			<cfquery name="Variables.Scq.ActvGndrTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		GndrCd,
						GndrDesc,
						GndrCd										AS code,
						GndrDesc									AS description<cfif Variables.Sybase>
			from		sbaref..GndrCdTbl
			where		(	datediff(dd,GndrStrtDt,					getdate()) >= 0)
			and			(				GndrEndDt					is null
						or	datediff(dd,GndrEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.GndrCdTbl
			where		(	(sysdate -	GndrStrtDt)					>= 0)
			and			(				GndrEndDt					is null
						or	(sysdate -	GndrEndDt)					<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllGndrTbl">
			<cfquery name="Variables.Scq.AllGndrTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		GndrCd,
						GndrDesc,
						GndrStrtDt,
						GndrEndDt,
						GndrCd										AS code,
						GndrDesc									AS description<cfif Variables.Sybase>
			from		sbaref..GndrCdTbl<cfelse>
			from		sbaref.GndrCdTbl</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrMFCdTbl">
			<cfquery name="Variables.Scq.ActvGndrMFCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		GndrMFCd,
						GndrMFDescTxt,
						GndrCd,
						GndrMFCd									AS code,
						GndrMFDescTxt								AS description<cfif Variables.Sybase>
			from		sbaref..GndrMFCdTbl
			where		(	datediff(dd,GndrMFStrtDt,				getdate()) >= 0)
			and			(				GndrMFEndDt					is null
						or	datediff(dd,GndrMFEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.GndrMFCdTbl
			where		(	(sysdate -	GndrMFStrtDt)				>= 0)
			and			(				GndrMFEndDt					is null
						or	(sysdate -	GndrMFEndDt)				<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvInjctnTypCdTbl">
			<cfquery name="Variables.Scq.ActvInjctnTypCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		InjctnTypCd,
						InjctnTypTxt,
						case<!--- Reorder cash types first, then non-cash, then standby debt, then other: --->
						when InjctnTypCd = 'C'						then 1
						when InjctnTypCd = 'G'						then 2
						when InjctnTypCd = 'D'						then 3
						when InjctnTypCd = 'A'						then 4
						when InjctnTypCd = 'S'						then 5
						when InjctnTypCd = 'O'						then 6
						else										7
						end											AS displayorder,
						InjctnTypCd									AS code,
						InjctnTypTxt								AS description<cfif Variables.Sybase>
			from		sbaref..InjctnTypCdTbl
			where		(	datediff(dd,InjctnTypStrtDt,			getdate()) >= 0)
			and			(				InjctnTypEndDt				is null
						or	datediff(dd,InjctnTypEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.InjctnTypCdTbl
			where		(	(sysdate -	InjctnTypStrtDt)			>= 0)
			and			(				InjctnTypEndDt				is null
						or	(sysdate -	InjctnTypEndDt)				<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatTypCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCollatTypCd,
						LoanCollatTypDescTxt,
						LoanCollatTypCd								AS code,
						LoanCollatTypDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanCollatTypCdTbl
			where		(	datediff(dd,LoanCollatTypStrtDt,		getdate()) >= 0)
			and			(				LoanCollatTypEndDt			is null
						or	datediff(dd,LoanCollatTypEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.LoanCollatTypCdTbl
			where		(	(sysdate -	LoanCollatTypStrtDt)		>= 0)
			and			(				LoanCollatTypEndDt			is null
						or	(sysdate -	LoanCollatTypEndDt)			<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatValSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatValSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCollatValSourcCd,
						LoanCollatValSourcDescTxt,
						LoanCollatValSourcCd						AS code,
						LoanCollatValSourcDescTxt					AS description<cfif Variables.Sybase>
			from		sbaref..LoanCollatValSourcTbl
			where		(	datediff(dd,LoanCollatValSourcStrtDt,	getdate()) >= 0)
			and			(				LoanCollatValSourcEndDt		is null
						or	datediff(dd,LoanCollatValSourcEndDt,	getdate()) <= 0)<cfelse>
			from		sbaref.LoanCollatValSourcTbl
			where		(	(sysdate -	LoanCollatValSourcStrtDt)	>= 0)
			and			(				LoanCollatValSourcEndDt		is null
						or	(sysdate -	LoanCollatValSourcEndDt)	<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCrdtUnavRsnCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCrdtUnavRsnCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCrdtUnavRsnCd,
						LoanCrdtUnavRsnDescTxt,
						LoanCrdtUnavRsnCd							AS code,
						LoanCrdtUnavRsnDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanCrdtUnavRsnCdTbl
			where		(	datediff(dd,LoanCrdtUnavRsnStrtDt,		getdate()) >= 0)
			and			(				LoanCrdtUnavRsnEndDt		is null
						or	datediff(dd,LoanCrdtUnavRsnEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.LoanCrdtUnavRsnCdTbl
			where		(	(sysdate -	LoanCrdtUnavRsnStrtDt)		>= 0)
			and			(				LoanCrdtUnavRsnEndDt		is null
						or	(sysdate -	LoanCrdtUnavRsnEndDt)		<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanFinanclStmtSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanFinanclStmtSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanFinanclStmtSourcCd,
						LoanFinanclStmtSourcDescTxt,
						LoanFinanclStmtSourcCd						AS code,
						LoanFinanclStmtSourcDescTxt					AS description<cfif Variables.Sybase>
			from		sbaref..LoanFinanclStmtSourcTbl
			where		(	datediff(dd,LoanFinanclStmtSourcStrtDt,	getdate()) >= 0)
			and			(				LoanFinanclStmtSourcEndDt	is null
						or	datediff(dd,LoanFinanclStmtSourcEndDt,	getdate()) <= 0)<cfelse>
			from		sbaref.LoanFinanclStmtSourcTbl
			where		(	(sysdate -	LoanFinanclStmtSourcStrtDt)	>= 0)
			and			(				LoanFinanclStmtSourcEndDt	is null
						or	(sysdate -	LoanFinanclStmtSourcEndDt)	<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFDisastrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFDisastrTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanMFDisastrCd,
						LoanMFDisastrDescTxt,
						LoanMFDisastrCd								AS code,
						LoanMFDisastrDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanMFDisastrTypTbl<cfelse>
			from		sbaref.LoanMFDisastrTypTbl</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFPrcsMthdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFPrcsMthdTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanMFPrcsMthdCd,
						LoanMFPrcsMthdDescTxt,
						LoanMFPrcsMthdCd							AS code,
						LoanMFPrcsMthdDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanMFPrcsMthdTypTbl<cfelse>
			from		sbaref.LoanMFPrcsMthdTypTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPartLendrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPartLendrTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPartLendrTypCd,
						LoanPartLendrTypDescTxt,
						case<!--- Force Participating to the top and Others to the end: --->
						when LoanPartLendrTypCd = 'P'				then 1
						when LoanPartLendrTypCd = 'I'				then 2
						when LoanPartLendrTypCd = 'G'				then 3
						when LoanPartLendrTypCd = 'O'				then 4
						else										5
						end											AS displayorder,
						LoanPartLendrTypCd							AS code,
						LoanPartLendrTypDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanPartLendrTypTbl
			where		(	datediff(dd,LoanPartLendrTypStrtDt,		getdate()) >= 0)
			and			(				LoanPartLendrTypEndDt		is null
						or	datediff(dd,LoanPartLendrTypEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.LoanPartLendrTypTbl
			where		(	(sysdate -	LoanPartLendrTypStrtDt)		>= 0)
			and			(				LoanPartLendrTypEndDt		is null
						or	(sysdate -	LoanPartLendrTypEndDt)		<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPckgSourcTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPckgSourcTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPckgSourcTypCd,
						LoanPckgSourcTypDescTxt,
						LoanPckgSourcTypCd							AS code,
						LoanPckgSourcTypDescTxt						AS description<cfif Variables.Sybase>
			from		sbaref..LoanPckgSourcTypTbl
			where		(	datediff(dd,LoanPckgSourcTypStrtDt,		getdate()) >= 0)
			and			(				LoanPckgSourcTypEndDt		is null
						or	datediff(dd,LoanPckgSourcTypEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.LoanPckgSourcTypTbl
			where		(	(sysdate -	LoanPckgSourcTypStrtDt)		>= 0)
			and			(				LoanPckgSourcTypEndDt		is null
						or	(sysdate -	LoanPckgSourcTypEndDt)		<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPrevFinanStatTbl">
			<cfquery name="Variables.Scq.ActvLoanPrevFinanStatTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPrevFinanStatCd,
						LoanPrevFinanStatDescTxt,
						LoanPrevFinanStatCd							AS code,
						LoanPrevFinanStatDescTxt					AS description<cfif Variables.Sybase>
			from		sbaref..LoanPrevFinanStatTbl
			where		(	datediff(dd,LoanPrevFinanStatStrtDt,	getdate()) >= 0)
			and			(				LoanPrevFinanStatEndDt		is null
						or	datediff(dd,LoanPrevFinanStatEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.LoanPrevFinanStatTbl
			where		(	(sysdate -	LoanPrevFinanStatStrtDt)	>= 0)
			and			(				LoanPrevFinanStatEndDt		is null
						or	(sysdate -	LoanPrevFinanStatEndDt)		<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanProcdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanProcdTypTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanProcdTypCd,
						LoanProcdTypDescTxt,
						ProcdTypCd,<!--- Ignoring LoanProcdTypCmnt for now, because it's always null. --->
						ProcdTypCd || LoanProcdTypCd				AS code,
						LoanProcdTypDescTxt							AS description<cfif Variables.Sybase>
			from		sbaref..LoanProcdTypTbl
			where		(	datediff(dd,LoanProcdTypCdStrtDt,		getdate()) >= 0)
			and			(				LoanProcdTypCdEndDt			is null
						or	datediff(dd,LoanProcdTypCdEndDt,		getdate()) <= 0)<cfelse>
			from		sbaref.LoanProcdTypTbl
			where		(	(sysdate -	LoanProcdTypCdStrtDt)		>= 0)
			and			(				LoanProcdTypCdEndDt			is null
						or	(sysdate -	LoanProcdTypCdEndDt)		<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanStatCdTbl">
			<cfquery name="Variables.Scq.ActvLoanStatCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanStatCd,
						ColsonStatCd,
						LoanStatDescTxt,
						LoanStatCd									AS code,
						LoanStatDescTxt								AS description<cfif Variables.Sybase>
			from		sbaref..LoanStatCdTbl
			where		(	datediff(dd,LoanStatStrtDt,				getdate()) >= 0)
			and			(				LoanStatEndDt				is null
						or	datediff(dd,LoanStatEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.LoanStatCdTbl
			where		(	(sysdate -	LoanStatStrtDt)				>= 0)
			and			(				LoanStatEndDt				is null
						or	(sysdate -	LoanStatEndDt)				<= 0)</cfif>
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvMfSubPrgrmCdTbl">
			<cfquery name="Variables.Scq.ActvMfSubPrgrmCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		MfSubPrgrmCd,
						MfSubPrgrmDesc,
						MfSubPrgrmCd								AS code,
						MfSubPrgrmDesc								AS description<cfif Variables.Sybase>
			from		sbaref..MfSubPrgrmCdTbl
			where		(	datediff(dd,MfSubPrgrmStrtDt,			getdate()) >= 0)
			and			(				MfSubPrgrmEndDt				is null
						or	datediff(dd,MfSubPrgrmEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.MfSubPrgrmCdTbl
			where		(	(sysdate -	MfSubPrgrmStrtDt)			>= 0)
			and			(				MfSubPrgrmEndDt				is null
						or	(sysdate -	MfSubPrgrmEndDt)			<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanStatCmntCdTbl">
			<cfquery name="Variables.Scq.ActvLoanStatCmntCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanStatCmntCd,
						LoanStatCdCmntDesc,
						LoanStatCmntCd							AS code,
						LoanStatCdCmntDesc						AS description<cfif Variables.Sybase>
			from		sbaref..LoanStatCmntCdTbl
			where		(	datediff(dd,LoanStatCmntCdStrtDt,	getdate()) >= 0)
			and			(				LoanStatCmntCdEndDt		is null
						or	datediff(dd,LoanStatCmntCdEndDt,	getdate()) <= 0)<cfelse>
			from		sbaref.LoanStatCmntCdTbl
			where		(	(sysdate -	LoanStatCmntCdStrtDt)	>= 0)
			and			(				LoanStatCmntCdEndDt		is null
						or	(sysdate -	LoanStatCmntCdEndDt)	<= 0)</cfif>
			order by	LoanStatCmntCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllLoanStatCmntCdTbl">
			<cfquery name="Variables.Scq.AllLoanStatCmntCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanStatCmntCd,
						LoanStatCdCmntDesc,
						LoanStatCmntCdStrtDt,
						LoanStatCmntCdEndDt,
						LoanStatCmntCd							AS code,
						LoanStatCdCmntDesc						AS description<cfif Variables.Sybase>
			from		sbaref..LoanStatCmntCdTbl<cfelse>
			from		sbaref.LoanStatCmntCdTbl</cfif>
			order by	LoanStatCmntCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvOutLawTbl">
			<cfquery name="Variables.Scq.ActvOutLawTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		OutLawCd,
						OutLawDesc,
						OutLawStrtDt,
						OutLawEndDt,
						OutLawCd								AS code,
						OutLawDesc								AS description<cfif Variables.Sybase>
			from		sbaref..OutLawTbl
			where		(	datediff(dd,OutLawStrtDt,			getdate()) >= 0)
			and			(				OutLawEndDt				is null
						or	datediff(dd,OutLawEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.OutLawTbl
			where		(	(sysdate -	OutLawStrtDt)			>= 0)
			and			(				OutLawEndDt				is null
						or	(sysdate -	OutLawEndDt)			<= 0)</cfif>
			order by	OutLawDispOrd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllOutLawTbl">
			<cfquery name="Variables.Scq.AllOutLawTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		OutLawCd,
						OutLawDesc,
						OutLawStrtDt,
						OutLawEndDt,
						OutLawCd								AS code,
						OutLawDesc								AS description<cfif Variables.Sybase>
			from		sbaref..OutLawTbl<cfelse>
			from		sbaref.OutLawTbl</cfif>
			order by	OutLawDispOrd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrcsMthdTbl">
			<cfquery name="Variables.Scq.ActvPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description,
						LoanTypInd,
						PrcsMthdRvwrRqrdNmb,
						PrcsMthdExprsInd,
						PrcsMthdUndrwritngBy,
						PrgrmCd,
						PrgrmAuthAreaInd<cfif Variables.Sybase>
			from		sbaref..PrcsMthdTbl
			where		(	datediff(dd,PrcsMthdStrtDt,				getdate()) >= 0)
			and			(				PrcsMthdEndDt				is null
						or	datediff(dd,PrcsMthdEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.PrcsMthdTbl
			where		(	(sysdate -	PrcsMthdStrtDt)				>= 0)
			and			(				PrcsMthdEndDt				is null
						or	(sysdate -	PrcsMthdEndDt)				<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqLists								= StructNew()>
			<cfloop index="ScqListKey"								list="PrcsMthd7ExpSet,PrcsMthdDirectSet,PrcsMthdGuarantySet,PrcsMthdRev1Set,PrcsMthdRev2Set,PrcsMthdRev3Set,PrcsMthdUndrwritngByLndr,PrcsMthdUndrwritngBySBA">
				<cfset Variables.ScqLists[ScqListKey]				= "">
			</cfloop>
			<cfloop query="Variables.Scq.ActvPrcsMthdTbl">
				<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.LoanTypInd#">
				<cfcase value="D">		<cfset Variables.ScqLists.PrcsMthdDirectSet			= ListAppend(Variables.ScqLists.PrcsMthdDirectSet,			PrcsMthdCd)></cfcase>
				<cfcase value="G">		<cfset Variables.ScqLists.PrcsMthdGuarantySet		= ListAppend(Variables.ScqLists.PrcsMthdGuarantySet,		PrcsMthdCd)></cfcase>
				</cfswitch>
				<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.PrcsMthdRvwrRqrdNmb#">
				<cfcase value="1">		<cfset Variables.ScqLists.PrcsMthdRev1Set			= ListAppend(Variables.ScqLists.PrcsMthdRev1Set,			PrcsMthdCd)></cfcase>
				<cfcase value="2">		<cfset Variables.ScqLists.PrcsMthdRev2Set			= ListAppend(Variables.ScqLists.PrcsMthdRev2Set,			PrcsMthdCd)></cfcase>
				<cfcase value="3">		<cfset Variables.ScqLists.PrcsMthdRev3Set			= ListAppend(Variables.ScqLists.PrcsMthdRev3Set,			PrcsMthdCd)></cfcase>
				</cfswitch>
				<!--- Don't really need cfswitch for only 1 case, but it's just as fast as a cfif, and it makes the code more readable: --->
				<cfswitch expression="#Variables.Scq.ActvPrcsMthdTbl.PrcsMthdExprsInd#">
				<cfcase value="Y">		<cfset Variables.ScqLists.PrcsMthd7ExpSet			= ListAppend(Variables.ScqLists.PrcsMthd7ExpSet,			PrcsMthdCd)></cfcase>
				</cfswitch>
				<cfswitch expression="#UCase(Trim(Variables.Scq.ActvPrcsMthdTbl.PrcsMthdUndrwritngBy))#">
				<cfcase value="LNDR">	<cfset Variables.ScqLists.PrcsMthdUndrwritngByLndr	= ListAppend(Variables.ScqLists.PrcsMthdUndrwritngByLndr,	PrcsMthdCd)></cfcase>
				<cfcase value="SBA">	<cfset Variables.ScqLists.PrcsMthdUndrwritngBySBA	= ListAppend(Variables.ScqLists.PrcsMthdUndrwritngBySBA,	PrcsMthdCd)></cfcase>
				</cfswitch>
			</cfloop>
			<cfset Variables.Scq["PrcsMthdLists"]					= Duplicate(Variables.ScqLists)>
			<cfset StructDelete(Variables, "ScqLists")>
		</cfcase>
		<cfcase				  value="Scq.AllPrcsMthdTbl">
			<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdStrtDt,
						PrcsMthdEndDt,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description,
						LoanTypInd,
						PrcsMthdRvwrRqrdNmb,
						PrcsMthdExprsInd,
						PrcsMthdUndrwritngBy,
						PrgrmCd,
						PrgrmAuthAreaInd<cfif Variables.Sybase>
			from		sbaref..PrcsMthdTbl<cfelse>
			from		sbaref.PrcsMthdTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmTbl">
			<cfquery name="Variables.Scq.ActvPrgrmTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description<cfif Variables.Sybase>
			from		sbaref..PrgrmTbl
			where		(	datediff(dd,PrgrmStrtDt,				getdate()) >= 0)
			and			(				PrgrmEndDt					is null
						or	datediff(dd,PrgrmEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.PrgrmTbl
			where		(	(sysdate -	PrgrmStrtDt)				>= 0)
			and			(				PrgrmEndDt					is null
						or	(sysdate -	PrgrmEndDt)					<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllPrgrmTbl">
			<cfquery name="Variables.Scq.AllPrgrmTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmStrtDt,
						PrgrmEndDt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description<cfif Variables.Sybase>
			from		sbaref..PrgrmTbl<cfelse>
			from		sbaref.PrgrmTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmValidTbl">
			<!--- Because this table is never displayed, it doesn't have code and description columns. --->
			<cfquery name="Variables.Scq.ActvPrgrmValidTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrcsMthdCd,
						SpcPurpsLoanCd,
						ProcdTypCd,
						SubPrgrmMFCd,
						LoanMFSpcPurpsCd,
						LoanMFPrcsMthdCd,
						LoanMFSTARInd,
						LoanMFDisastrCd<cfif Variables.Sybase>
			from		sbaref..PrgrmValidTbl
			where		(	datediff(dd,PrgrmValidStrtDt,			getdate()) >= 0)
			and			(				PrgrmValidEndDt				is null
						or	datediff(dd,PrgrmValidEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.PrgrmValidTbl
			where		(	(sysdate -	PrgrmValidStrtDt)			>= 0)
			and			(				PrgrmValidEndDt				is null
						or	(sysdate -	PrgrmValidEndDt)			<= 0)</cfif>
			order by	PrgrmValidSeqNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvRaceCdTbl">
			<cfquery name="Variables.Scq.ActvRaceCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		RaceCd,
						RaceTxt,
						RaceMFCd,
						RaceMFTxt,
						RaceCd										AS code,
						RaceTxt										AS description<cfif Variables.Sybase>
			from		sbaref..RaceCdTbl
			where		(	datediff(dd,RaceStrtDt,					getdate()) >= 0)
			and			(				RaceEndDt					is null
						or	datediff(dd,RaceEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.RaceCdTbl
			where		(	(sysdate -	RaceStrtDt)					>= 0)
			and			(				RaceEndDt					is null
						or	(sysdate -	RaceEndDt)					<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllRaceCdTbl">
			<cfquery name="Variables.Scq.AllRaceCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		RaceCd,
						RaceTxt,
						RaceMFCd,
						RaceMFTxt,
						RaceStrtDt,
						RaceEndDt,
						RaceCd										AS code,
						RaceTxt										AS description<cfif Variables.Sybase>
			from		sbaref..RaceCdTbl<cfelse>
			from		sbaref.RaceCdTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvSBAOfcTbl">
			<cfquery name="Variables.Scq.ActvSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description<cfif Variables.Sybase>
			from		sbaref..SBAOfcTbl
			where		(	datediff(dd,SBAOfcStrtDt,				getdate()) >= 0)
			and			(				SBAOfcEndDt					is null
						or	datediff(dd,SBAOfcEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.SBAOfcTbl
			where		(	(sysdate -	SBAOfcStrtDt)				>= 0)
			and			(				SBAOfcEndDt					is null
						or	(sysdate -	SBAOfcEndDt)				<= 0)</cfif>
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllSBAOfcTbl">
			<cfquery name="Variables.Scq.AllSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcStrtDt,
						SBAOfcEndDt,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description<cfif Variables.Sybase>
			from		sbaref..SBAOfcTbl<cfelse>
			from		sbaref.SBAOfcTbl</cfif>
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!---
		IMRtTbl (current interest rates) vary over time, so the contents of that table aren't very static. Also, they're often 
		accessed in the past. (For example, if electronic lending has a received-date, LoanAppRecvDt, it grabs the interest rate 
		for that date, rather than today's date.) Therefore a snapshot of today's interest rates is not very useful. Even when 
		today's rate **IS** needed, a snapshot from 5:00 AM (when these cached queries are refreshed) is liable to be wrong, 
		because rates are updated during the day. Furthermore, IMRtTypTbl is generally only accessed in joins to IMRtTbl. 
		Therefore, we aren't caching that table either. 				Steve Seaquist
		--->
		<cfcase				  value="Scq.ActvSpcPurpsLoanTbl">
			<cfquery name="Variables.Scq.ActvSpcPurpsLoanTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SpcPurpsLoanCd,
						SpcPurpsLoanDesc,
						SpcPrgrmCd,
						SpcPurpsLoanCd								AS code,
						SpcPurpsLoanDesc							AS description<cfif Variables.Sybase>
			from		sbaref..SpcPurpsLoanTbl
			where		(	datediff(dd,SpcPurpsLoanStrtDt,			getdate()) >= 0)
			and			(				SpcPurpsLoanEndDt			is null
						or	datediff(dd,SpcPurpsLoanEndDt,			getdate()) <= 0)<cfelse>
			from		sbaref.SpcPurpsLoanTbl
			where		(	(sysdate -	SpcPurpsLoanStrtDt)			>= 0)
			and			(				SpcPurpsLoanEndDt			is null
						or	(sysdate -	SpcPurpsLoanEndDt)			<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllSpcPurpsLoanTbl">
			<cfquery name="Variables.Scq.AllSpcPurpsLoanTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SpcPurpsLoanCd,
						SpcPurpsLoanDesc,
						SpcPrgrmCd,
						SpcPurpsLoanCd								AS code,
						SpcPurpsLoanDesc							AS description<cfif Variables.Sybase>
			from		sbaref..SpcPurpsLoanTbl<cfelse>
			from		sbaref.SpcPurpsLoanTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvSrvrCacheQryTbl">
			<cfquery name="Variables.Scq.ActvSrvrCacheQryTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		distinct
						rtrim(SrvrNm)								as SrvrNm,
						InstanceNm,
						rtrim(CacheQryCd)							as CacheQryCd,<cfif Variables.Sybase>
						rtrim(SrvrNm)+'.'+InstanceNm				as code,		-- likely to change<cfelse>
						rtrim(SrvrNm)||'.'||InstanceNm				as code,		-- likely to change</cfif>
						case CacheQryCd
						when 'sbaref'	then						1
						when 'partner'	then						2
						when 'pronet'	then						3
						when 'technet'	then						4
						else										5
						end											as displayorder,
						rtrim(CacheQryCd)							as description<cfif Variables.Sybase>
			from		sbaref..SrvrCacheQryTbl
			where		(	datediff(dd,SrvrCacheQryStrtDt,getdate())	>= 0)
			and			(				SrvrCacheQryEndDt				is null
						or	datediff(dd,SrvrCacheQryEndDt, getdate())	<= 0)<cfelse>
			from		sbaref.SrvrCacheQryTbl
			where		(	(sysdate -	SrvrCacheQryStrtDt)				>= 0)
			and			(				SrvrCacheQryEndDt				is null
						or	(sysdate -	SrvrCacheQryEndDt)				<= 0)</cfif>
			order by	code,
						displayorder
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.ActvSrvrLocTbl">
			<cfquery name="Variables.Scq.ActvSrvrLocTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		distinct
						SrvrLocCd,
						SrvrLocDescTxt,
						SrvrLocCd									as code,
						SrvrLocDescTxt								as description<cfif Variables.Sybase>
			from		sbaref..SrvrLocTbl<cfelse>
			from		sbaref.SrvrLocTbl</cfif>
			order by	code
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.ActvSrvrTbl">
			<cfquery name="Variables.Scq.ActvSrvrTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		distinct
						rtrim(s.SrvrNm)								as SrvrNm,
						s.InstanceNm,
						s.InstanceId,
						s.SrvrGrpTxt,
						case s.CLSInd
						when 'Y'	then							'Yes'
						else										'No'
						end											as GLSInd,
						case s.LognInd
						when 'Y'	then							'Yes'
						else										'No'
						end											as LognInd,
						rtrim(s.EnvCd)								as EnvCd,
						rtrim(e.EnvTypCd)							as EnvTypCd,
						case EnvTypCd
						when 'Dev'	then							0
						when 'Test'	then							1
						else										2
						end											as DevTestProdInd,
						e.LognURLTxt,<cfif Variables.Sybase>
						rtrim(s.SrvrNm)+'.'+s.InstanceNm			as code,		-- likely to change
						rtrim(s.SrvrNm)+'.'+s.InstanceNm			as description	-- likely to change
			from						sbaref..SrvrTbl				s
						left outer join	sbaref..EnvTbl				e on (s.EnvCd = e.EnvCd)
			where		(	datediff(dd,s.SrvrStrtDt,getdate())		>= 0)
			and			(				s.SrvrEndDt					is null
						or	datediff(dd,s.SrvrEndDt, getdate())		<= 0)<cfelse>
						rtrim(s.SrvrNm)||'.'||s.InstanceNm			as code,		-- likely to change
						rtrim(s.SrvrNm)||'.'||s.InstanceNm			as description	-- likely to change
			from						sbaref.SrvrTbl				s
						left outer join	sbaref.EnvTbl				e on (s.EnvCd = e.EnvCd)
			where		(	(sysdate -	s.SrvrStrtDt)				>= 0)
			and			(				s.SrvrEndDt					is null
						or	(sysdate -	s.SrvrEndDt)				<= 0)</cfif>
			order by	SrvrGrpTxt,
						SrvrNm,
						InstanceNm
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.ActvSrvrURLTbl">
			<cfquery name="Variables.Scq.ActvSrvrURLTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		distinct
						rtrim(EnvCd)								as EnvCd,
						SrvrLocCd,
						SrvrURLCd,
						SrvrURLDescTxt,<cfif Variables.Sybase>
						rtrim(EnvCd)+'.'+convert(varchar,SrvrLocCd)	as displayorder,<cfelse>
						rtrim(EnvCd)||'.'||SrvrLocCd				as displayorder,</cfif>
						SrvrURLCd									as code,
						SrvrURLDescTxt								as description<cfif Variables.Sybase>
			from		sbaref..SrvrURLTbl
			where		(	datediff(dd,SrvrURLStrtDt,getdate())	>= 0)
			and			(				SrvrURLEndDt				is null
						or	datediff(dd,SrvrURLEndDt, getdate())	<= 0)<cfelse>
			from		sbaref.SrvrURLTbl
			where		(	(sysdate -	SrvrURLStrtDt)				>= 0)
			and			(				SrvrURLEndDt				is null
						or	(sysdate -	SrvrURLEndDt)				<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfquery name="Variables.Scq.ThisSrvrURLTbl"			dbtype="query">
			select		*
			from		Variables.Scq.ActvSrvrURLTbl
			where		EnvCd										= '#Request.SlafServerEnvironment#'
			order by	displayorder
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.ActvStatCdTbl">
			<cfquery name="Variables.Scq.ActvStatCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		StatCd,
						StatTxt,
						StatCdDispInd,
						StatCdDispOrdNmb							AS displayorder,
						StatCd										AS code,
						StatTxt										AS description<cfif Variables.Sybase>
			from		sbaref..StatCdTbl
			where		(	datediff(dd,StatCdStrtDt,				getdate()) >= 0)
			and			(				StatCdEndDt					is null
						or	datediff(dd,StatCdEndDt,				getdate()) <= 0)<cfelse>
			from		sbaref.StatCdTbl
			where		(	(sysdate -	StatCdStrtDt)				>= 0)
			and			(				StatCdEndDt					is null
						or	(sysdate -	StatCdEndDt)				<= 0)</cfif>
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStTbl">
			<cfquery name="Variables.Scq.ActvStTbl"					datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		StCd,
						StNm,
						StCd										AS code,
						StNm										AS description<cfif Variables.Sybase>
			from		sbaref..StTbl
			where		(	datediff(dd,StStrtDt,					getdate()) >= 0)
			and			(				StEndDt						is null
						or	datediff(dd,StEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.StTbl
			where		(	(sysdate -	StStrtDt)					>= 0)
			and			(				StEndDt						is null
						or	(sysdate -	StEndDt)					<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvVetTbl">
			<cfquery name="Variables.Scq.ActvVetTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		VetCd,
						VetTxt,
						VetMFCd,
						VetMFDescTxt,
						VetGrpCd,
						VetCd										AS code,
						VetTxt										AS description<cfif Variables.Sybase>
			from		sbaref..VetTbl
			where		(	datediff(dd,VetStrtDt,					getdate()) >= 0)
			and			(				VetEndDt					is null
						or	datediff(dd,VetEndDt,					getdate()) <= 0)<cfelse>
			from		sbaref.VetTbl
			where		(	(sysdate -	VetStrtDt)					>= 0)
			and			(				VetEndDt					is null
						or	(sysdate -	VetEndDt)					<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllVetTbl">
			<cfquery name="Variables.Scq.AllVetTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		VetCd,
						VetTxt,
						VetMFCd,
						VetMFDescTxt,
						VetGrpCd,
						VetStrtDt,
						VetEndDt,
						VetCd										AS code,
						VetTxt										AS description<cfif Variables.Sybase>
			from		sbaref..VetTbl<cfelse>
			from		sbaref.VetTbl</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.partner") is just a placeholder pseudo-queryname, used to create the partner substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.partner.querynames: 
		--->
		<cfcase				  value="Scq.partner">
			<cfset Variables.Scq["partner"]							= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.partner.AllPrgrmTbl">
			<cfquery name="Variables.Scq.partner.AllPrgrmTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmId,
						PrgrmDesc,
						PrgrmDocReqdInd,
						PrgrmEffDt,
						PrgrmFormId,
						ValidPrgrmTyp,
						PrgrmAreaOfOperReqdInd<cfif Variables.Sybase>
			from		partner..PrgrmTbl<cfelse>
			from		partner.PrgrmTbl</cfif>
			where		PrgrmDispOrdNmb								!= 0
			order by	PrgrmDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.partner.AllSearchSubCatTyps">
			<cfquery name="Variables.Scq.partner.AllSearchSubCatTyps"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		v.ValidSBAPrtPrimCatTyp,
						c.ValidPrtPrimCatTypDesc,
						v.ValidPrtSubCatTyp,
						v.ValidPrtSubCatDesc,
						v.RolePrivilegePrgrmId<cfif Variables.Sybase>
			from		partner..ValidPrtSubCatTbl					v,
						partner..ValidPrtPrimCatTbl					c<cfelse>
			from		partner.ValidPrtSubCatTbl					v,
						partner.ValidPrtPrimCatTbl					c</cfif>
			where		v.ValidSBAPrtPrimCatTyp						= c.ValidPrtPrimCatTyp
			order by	c.ValidPrtPrimCatDispOrdNmb,
						c.ValidPrtPrimCatTypDesc,
						v.ValidPrtSubCatDesc
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.partner.AllValidLocTypTbl">
			<cfquery name="Variables.Scq.partner.AllValidLocTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ValidLocTyp,
						ValidLocTypDesc<cfif Variables.Sybase>
			from		partner..ValidLocTypTbl<cfelse>
			from		partner.ValidLocTypTbl</cfif>
			where		ValidLocTypDispOrdNmb						!= 0
			order by	ValidLocTypDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.pronet") is just a placeholder pseudo-queryname, used to create the pronet substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.pronet.querynames. 
		For the remaining Scq.pronet.querynames, the cfswitch at the very top of this cfloop sets db, etc, correctly. 
		--->
		<cfcase				  value="Scq.pronet">
			<cfset Variables.Scq["pronet"]							= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvCCRBusTypCdTbl">
			<cfquery name="Variables.Scq.pronet.ActvCCRBusTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select			c.CCRBusTypCd							code,
							c.CCRBusTypDescTxt						description,
							c.CCRBusTypCd,
							c.CCRBusTypDescTxt,
							c.IMMinCd,
							case
							when c.CCRBusTypCd in ('05','1B','8U','HK')	/* ANC-, Tribal-, NHO- and CDC-owned, respectively. */
														then		c.CCRBusTypDescTxt
							when c.IMMinCd is not null	then		m.IMMinDescTxt
							else									null
							end										as DSBSSearchName,
							c.BusStatInactRsnCd,
							c.CCRBusTypExpt,
							c.IMOrgTypCd<cfif Variables.Sybase>
			from			pronet..CCRBusTypCdTbl					c
			left outer join	pronet..IMMinCdTbl						m on (c.IMMinCd = m.IMMinCd)<cfelse>
			from			viewpronet.CCRBusTypCdTbl				c
			left outer join	viewpronet.IMMinCdTbl					m on (c.IMMinCd = m.IMMinCd)</cfif>
			order by		DSBSSearchName,
							c.CCRBusTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqLists								= StructNew()>
			<!--- Keep the following struct key names in sync with values of DSBS's OtherOwnershipCheckboxValues: --->
			<cfloop index="ScqListKey"								list="Min,Nat,NatTri,NatAla,NatHaw,NatOth,MinOth,CDC,SCS,SDV,Vet,Wom,QMS">
				<cfset Variables.ScqLists[ScqListKey]				= "">
			</cfloop>
			<cfloop query="Variables.Scq.pronet.ActvCCRBusTypCdTbl">
				<cfif Len(IMMinCd) GT 0>
					<cfswitch expression="#UCase(IMMinCd)#">
					<cfcase value="NAT">
						<cfset Variables.ScqLists.Min				= ListAppend(Variables.ScqLists.Min,			"'#CCRBusTypCd#'")>
						<cfset Variables.ScqLists.Nat				= ListAppend(Variables.ScqLists.Nat,			"'#CCRBusTypCd#'")>
						<cfswitch expression="#CCRBusTypCd#">
						<!---
						Because of the one-to-many relationship from IMMinCd to CCRBusTypCd (as opposed to many-to-many), 
						Tribally-Owned, Alaskan Native Corp Owned, Native Hawaiian Org Owned codes have to be hard-coded 
						for now. Technically, all 3 are tribally-owned, but ANC and NHO aren't called tribes. 
						--->
						<cfcase value="1B"><cfset Variables.ScqLists.NatTri	= ListAppend(Variables.ScqLists.NatTri,	"'1B'")></cfcase>
						<cfcase value="05"><cfset Variables.ScqLists.NatAla	= ListAppend(Variables.ScqLists.NatAla,	"'05'")></cfcase>
						<cfcase value="8U"><cfset Variables.ScqLists.NatHaw	= ListAppend(Variables.ScqLists.NatHaw,	"'8U'")></cfcase>
						<cfdefaultcase>	   <cfset Variables.ScqLists.NatOth	= ListAppend(Variables.ScqLists.NatOth,	"'#CCRBusTypCd#'")></cfdefaultcase>
						</cfswitch>
					</cfcase>
					<cfcase value="SCS">
						<cfset Variables.ScqLists[IMMinCd]			= ListAppend(Variables.ScqLists[IMMinCd],		"'#CCRBusTypCd#'")>
					</cfcase>
					<cfcase value="SDV,VET,WOM"><!--- That is, if minority code participates in Quick Market Search. --->
						<cfset Variables.ScqLists[IMMinCd]			= ListAppend(Variables.ScqLists[IMMinCd],		"'#CCRBusTypCd#'")>
						<cfset Variables.ScqLists.QMS				= ListAppend(Variables.ScqLists.QMS,			"'#CCRBusTypCd#'")>
					</cfcase>
					<cfdefaultcase>
						<cfset Variables.ScqLists.Min				= ListAppend(Variables.ScqLists.Min,			"'#CCRBusTypCd#'")>
						<cfset Variables.ScqLists.MinOth			= ListAppend(Variables.ScqLists.MinOth,			"'#CCRBusTypCd#'")>
					</cfdefaultcase>
					</cfswitch>
				<cfelseif CCRBusTypCd IS "HK"><!--- A CDC is not necessarily a minority and has no minority code. --->
					<cfset Variables.ScqLists.CDC					= ListAppend(Variables.ScqLists.CDC,			"'HK'")>
				</cfif>
			</cfloop>
			<cfset Variables.Scq.pronet["CbtLists"]					= Duplicate(Variables.ScqLists)>
			<cfset StructDelete(Variables, "ScqLists")>
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvCountryCdTblForeign">
			<cfquery name="Variables.Scq.pronet.ActvCountryCdTblForeign"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CountryCd									code,
						CountryNm									description<cfif Variables.Sybase>
			from		pronet..CountryCdTbl<cfelse>
			from		viewpronet.CountryCdTbl</cfif>
			where		DomForInd									> 0 /* indicating foreign */
			and			PubBanInd									= 0
			and			PvtBanInd									= 0
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllCountryCdTbl">
			<cfquery name="Variables.Scq.pronet.AllCountryCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CountryCd									code,
						CountryNm									description,
						DomForInd,
						PubBanInd,
						PvtBanInd<cfif Variables.Sybase>
			from		pronet..CountryCdTbl<cfelse>
			from		viewpronet.CountryCdTbl</cfif>
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvFrmlDtrmntnCtgryTbl">
			<cfquery name="Variables.Scq.pronet.ActvFrmlDtrmntnCtgryTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CtgryCd										code,
						CtgryNm										description<cfif Variables.Sybase>
			from		pronet..FrmlDtrmntnCtgryTbl
			where		(	datediff(dd,StartDt,					getdate()) >= 0)
			and			(				EndDt						is null
						or	datediff(dd,EndDt,						getdate()) <= 0)<cfelse>
			from		viewpronet.FrmlDtrmntnCtgryTbl
			where		(	(sysdate -	StartDt)					>= 0)
			and			(				EndDt						is null
						or	(sysdate -	EndDt)						<= 0)</cfif>
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMainActvtyTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMainActvtyTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ExportMainActvtyCd							code,
						ExportMainActvtyDescTxt						description<cfif Variables.Sybase>
			from		pronet..ExportMainActvtyTypTbl<cfelse>
			from		viewpronet.ExportMainActvtyTypTbl</cfif>
			order by	ExportMainActvtyCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMrktTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMrktTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ExportMrktTypCd								code,
						ExportMrktTypDescTxt						description<cfif Variables.Sybase>
			from		pronet..ExportMrktTypTbl<cfelse>
			from		viewpronet.ExportMrktTypTbl</cfif>
			order by	ExportMrktTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMAreaTypCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.LogAct									= "retrieve DSBS area codes for 'server cached queries'">
			<cfset Variables.cfprname								= "Variables.ScqIMAreaTypCdTbl"><cfif Variables.Sybase>
			<cfinclude template="/cfincludes/pronet/spc_IMAreaTypCdSelTSP.sbarefdatasource.cfm"><cfelse>
			<cfinclude template="/cfincludes/oracle/viewpronet/spc_#Ucase('IMAreaTypCdSelTSP')#.cfm"></cfif>
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<!--- Code and description are not returned by the stored procedure, so we need to do a query-of-queries: --->
			<cfquery name="Variables.Scq.pronet.ActvIMAreaTypCdTbl"	dbtype="query">
			select		IMAreaTypCd									code,
						IMAreaTypDescTxt							description
			from		Variables.ScqIMAreaTypCdTbl
			order by	IMAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMMinCdTbl">
			<cfquery name="Variables.Scq.pronet.ActvIMMinCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMMinCd										code,
						IMMinDescTxt								description,
						IMMinCd,
						IMMinDescTxt<cfif Variables.Sybase>
			from		pronet..IMMinCdTbl<cfelse>
			from		viewpronet.IMMinCdTbl</cfif>
			order by	IMMinDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMQAStdCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.LogAct									= "retrieve DSBS quality assurance standard codes for 'server cached queries'">
			<cfset Variables.cfprname								= "Variables.ScqIMQAStdCdTbl"><cfif Variables.Sybase>
			<cfinclude template="/cfincludes/pronet/spc_IMQAStdCdSelTSP.sbarefdatasource.cfm"><cfelse>
			<cfinclude template="/cfincludes/oracle/viewpronet/spc_#UCase('IMQAStdCdSelTSP')#.cfm"></cfif>
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<!--- Eliminate deleted codes, because there isn't any active/inactive indicator: --->
			<cfquery name="Variables.Scq.pronet.ActvIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		Variables.ScqIMQAStdCdTbl
			where		IMQAStdCd									in ('B','A','I','F','E')
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllIMQAStdCdTbl">	<!--- Requires ActvIMQAStdCdTbl (for Variables.ScqIMQAStdCdTbl) --->
			<cfquery name="Variables.Scq.pronet.AllIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		Variables.ScqIMQAStdCdTbl
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvMntrAreaTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvMntrAreaTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		MntrAreaTypCd								code,
						MntrAreaTypDescTxt							description<cfif Variables.Sybase>
			from		pronet..MntrAreaTypTbl<cfelse>
			from		viewpronet.MntrAreaTypTbl</cfif>
			order by	MntrAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.technet") is just a placeholder pseudo-queryname, used to create the technet substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.technet.querynames. 
		For the remaining Scq.technet.querynames, the cfswitch at the very top of this cfloop sets db, etc, correctly. 
		--->
		<cfcase				  value="Scq.technet">
			<cfset Variables.Scq["technet"]							= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdInvestSaleTypCdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdInvestSaleTypCdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdInvestSaleTypCd,
						IMAwrdInvestSaleSourcCd,
						IMAwrdInvestSaleDescTxt<cfif Variables.Sybase>
			from		technet..IMAwrdInvestSaleTypCdTbl
			where		(				IMAwrdInvestSaleStrtDt		is not null)
			and			(	datediff(dd,IMAwrdInvestSaleStrtDt,		getdate()) >= 0)
			and			(				IMAwrdInvestSaleEndDt		is null
						or	datediff(dd,IMAwrdInvestSaleEndDt,		getdate()) <= 0)<cfelse>
			from		technet.IMAwrdInvestSaleTypCdTbl
			where		(				IMAwrdInvestSaleStrtDt		is not null)
			and			(	(sysdate -	IMAwrdInvestSaleStrtDt)		>= 0)
			and			(				IMAwrdInvestSaleEndDt		is null
						or	(sysdate -	IMAwrdInvestSaleEndDt)		<= 0)</cfif>
			order by	DsplyOrd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdMentrTypCdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdMentrTypCdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdMentrTypCd,
						IMAwrdMentrDescTxt,
						IMAwrdMentrTypCd							code,
						IMAwrdMentrDescTxt							description<cfif Variables.Sybase>
			from		technet..IMAwrdMentrTypCdTbl
			where		(				IMAwrdMentrStrtDt			is not null)
			and			(	datediff(dd,IMAwrdMentrStrtDt,			getdate()) >= 0)
			and			(				IMAwrdMentrEndDt			is null
						or	datediff(dd,IMAwrdMentrEndDt,			getdate()) <= 0)<cfelse>
			from		technet.IMAwrdMentrTypCdTbl
			where		(				IMAwrdMentrStrtDt			is not null)
			and			(	(sysdate -	IMAwrdMentrStrtDt)			>= 0)
			and			(				IMAwrdMentrEndDt			is null
						or	(sysdate -	IMAwrdMentrEndDt)			<= 0)</cfif>
			order by	IMAwrdMentrDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdSECdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdSECdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdSECd,
						IMAwrdSEDescTxt,
						IMAwrdSECd									code,
						IMAwrdSEDescTxt								description<cfif Variables.Sybase>
			from		technet..IMAwrdSECdTbl
			where		(				IMAwrdSEStrtDt				is not null)
			and			(	datediff(dd,IMAwrdSEStrtDt,				getdate()) >= 0)
			and			(				IMAwrdSEEndDt				is null
						or	datediff(dd,IMAwrdSEEndDt,				getdate()) <= 0)<cfelse>
			from		technet.IMAwrdSECdTbl
			where		(				IMAwrdSEStrtDt				is not null)
			and			(	(sysdate -	IMAwrdSEStrtDt)				>= 0)
			and			(				IMAwrdSEEndDt				is null
						or	(sysdate -	IMAwrdSEEndDt)				<= 0)</cfif>
			order by	IMAwrdSEDescTxt
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyTbl">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyNm,
						IMFedAgncySeqNmb							code,
						IMFedAgncyNm								description<cfif Variables.Sybase>
			from		technet..IMFedAgncyTbl<cfelse>
			from		technet.IMFedAgncyTbl</cfif>
			where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
			and			IMFedAgncyActvInactvInd						 = 0
			order by	IMFedAgncyNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyTbl4XML">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl4XML"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyNm,
						IMFedAgncyNm								code,<!--- XML uses letter codes --->
						IMFedAgncyNm								description<cfif Variables.Sybase>
			from		technet..IMFedAgncyTbl<cfelse>
			from		technet.IMFedAgncyTbl</cfif>
			where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
			and			IMFedAgncyActvInactvInd						 = 0
			and			IMFedAgncyNm								not in ('DOI','NRC')<!--- No longer submitting awards. --->
			order by	IMFedAgncyNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyBrTbl">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyBrTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyBrSeqNmb,
						IMFedAgncyBrNm,
						IMFedAgncyBrActvInactvInd,
						IMFedAgncyBrCreatUserId,
						IMFedAgncyBrCreatDt,
						upper(IMFedAgncyBrNm)						UpperBrNm,
						IMFedAgncyBrSeqNmb							code,
						IMFedAgncyBrNm								description<cfif Variables.Sybase>
			from		technet..IMFedAgncyBrTbl<cfelse>
			from		technet.IMFedAgncyBrTbl</cfif>
			where		IMFedAgncyBrActvInactvInd					= 0
			order by	UpperBrNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyBrTbl4XML">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyBrTbl4XML"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyBrSeqNmb,
						IMFedAgncyBrNm,
						IMFedAgncyBrActvInactvInd,
						IMFedAgncyBrCreatUserId,
						IMFedAgncyBrCreatDt,
						upper(IMFedAgncyBrNm)						UpperBrNm,
						upper(IMFedAgncyBrNm)						code,<!--- XML uses letter codes --->
						IMFedAgncyBrNm								description<cfif Variables.Sybase>
			from		technet..IMFedAgncyBrTbl<cfelse>
			from		technet.IMFedAgncyBrTbl</cfif>
			where		IMFedAgncyBrActvInactvInd					= 0
			order by	UpperBrNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMPrgrmTbl">
			<cfquery name="Variables.Scq.technet.ActvIMPrgrmTbl"				datasource="#db#" dbtype="#dbtype#">
			select		IMPrgrmId,
						upper(IMPrgrmDescTxt)			IMPrgrmDescTxt<cfif Variables.Sybase>
			from		technet..IMPrgrmTbl<cfelse>
			from		technet.IMPrgrmTbl</cfif>
			order by	IMPrgrmId
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMSTTRPrtCatTbl">
			<cfquery name="Variables.Scq.technet.ActvIMSTTRPrtCatTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMSTTRPrtCatCd,
						IMSTTRPrtCatDescTxt,
						IMSTTRPrtCatCd								code,
						IMSTTRPrtCatDescTxt							description<cfif Variables.Sybase>
			from		technet..IMSTTRPrtCatTbl<cfelse>
			from		technet.IMSTTRPrtCatTbl</cfif>
			order by	IMSTTRPrtCatDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMSTTRPrtTypTbl">
			<cfquery name="Variables.Scq.technet.ActvIMSTTRPrtTypTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMSTTRPrtTypCd,
						IMSTTRPrtTypDescTxt,
						IMSTTRPrtTypCd								code,
						IMSTTRPrtTypDescTxt							description<cfif Variables.Sybase>
			from		technet..IMSTTRPrtTypTbl<cfelse>
			from		technet.IMSTTRPrtTypTbl</cfif>
			order by	IMSTTRPrtTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		</cfswitch>
	</cfloop>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq.LastUpdateOfThisStructure")>
			<cfset Variables.Scq["PrevUpdateOfThisStructure"]		= Server.Scq.LastUpdateOfThisStructure>
		<cfelse>
			<cfset Variables.Scq["PrevUpdateOfThisStructure"]		= "N/A">
		</cfif>
	</cflock>
	<cfset Variables.Scq["LastUpdateOfThisStructure"]				= DateFormat(Now(), "YYYY-MM-DD")
																	& " "
																	& TimeFormat(Now(), "HH:mm:ss")>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.Scq											= Duplicate(Variables.Scq)>
		<!--- The following are temporary. (Convert all references, then get rid of them.) --->
		<cfif	(ListFind(Request.SlafServerCachedQueryGroups, "sbaref") GT 0)
			AND	(ListFind(Request.SlafServerCachedQueryGroups, "pronet") GT 0)>
			<cfset Server.ScqActvCountries							= Variables.Scq.ActvIMCntryCdTbl>	<!--- Temp!!! --->
			<cfset Server.ScqActvStates								= Variables.Scq.ActvStTbl>			<!--- Temp!!! --->
		</cfif>
	</cflock>
	<!---
	NOTE: We don't do any cftry/cfcatch blocks above (except those that may have been done in pronet SPC files). 
	So if there was a problem above, we PROBABLY crashed. Therefore, if control gets here, we're totally done 
	using the following save-and-restore Scq variables. In particular, there's no need to embed Variables.ErrMsg 
	messages from this file into Variables.ErrMsg from the caller. So do nothing more elaborate than simply restoring 
	the caller's definitions of the save-and-restore Scq variables (if any). Also, eliminate all remnances of our 
	presence in the Variables scope, in case that would mess up the caller's use of SPC files: 
	--->
	<cfif IsDefined("Variables.ScqCFPRA")>
		<cfset Variables.cfpra										= Variables.ScqCFPRA>
		<!--- We didn't create it, so we don't need to StructDelete it. We just kept it from messing up our SPC calls. --->
	</cfif>
	<cfif IsDefined("Variables.ScqCFPRName")>
		<cfset Variables.cfprname									= Variables.ScqCFPRName>
	<cfelse>
		<cfset StructDelete(Variables, "cfprname")>
	</cfif>
	<cfif IsDefined("Variables.ScqDB")>
		<cfset Variables.db											= Variables.ScqDB>
	<cfelse>
		<cfset StructDelete(Variables, "db")>
	</cfif>
	<cfif IsDefined("Variables.ScqDBType")>
		<cfset Variables.dbtype										= Variables.ScqDBType>
	<cfelse>
		<cfset StructDelete(Variables, "dbtype")>
	</cfif>
	<cfif IsDefined("Variables.ScqErrMsg")>
		<cfset Variables.ErrMsg										= Variables.ScqErrMsg>
	<cfelse>
		<cfset StructDelete(Variables, "ErrMsg")>
	</cfif>
	<cfif IsDefined("Variables.ScqLogAct")>
		<cfset Variables.LogAct										= Variables.ScqLogAct>
	<cfelse>
		<cfset StructDelete(Variables, "LogAct")>
	</cfif>
	<cfif IsDefined("Variables.ScqSybase")>
		<cfset Variables.Sybase										= Variables.ScqSybase>
	<cfelse>
		<cfset StructDelete(Variables, "Sybase")>
	</cfif>
	<cfif IsDefined("Variables.ScqTxnErr")>
		<cfset Variables.TxnErr										= Variables.ScqTxnErr>
	<cfelse>
		<cfset StructDelete(Variables, "TxnErr")>
	</cfif>
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqElapsedMilliseconds						= GetTickCount() - Variables.ScqStartTickCount>
		<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>Build Server Cached Queries on #Request.SlafServerName#, #Request.SlafServerInstanceName#</title>
<link href="/library/css/sba.css" rel="stylesheet" type="text/css">
</head>
<body class="normal pad20">
<h3 align="center">Build Server Cached Queries on #Request.SlafServerName#, #Request.SlafServerInstanceName#</h3>
<p>
	End bld_ServerCachedQueries database I/O after #Variables.ScqElapsedMilliseconds# milliseconds. 
</p>
</cfoutput>
	</cfif>
	<!---
	The following were only needed temporarily during the loop. Delete them to discourage their use by the caller. 
	(Query names that aren't specifically named in ScqAvailableResultSetNames are not guaranteed to exist.) 
	--->
	<cfif IsDefined("Variables.ScqIMAreaTypCdTbl")>
		<cfset StructDelete(Variables, "ScqIMAreaTypCdTbl")>
	</cfif>
	<cfif IsDefined("Variables.ScqIMQAStdCdTbl")>
		<cfset StructDelete(Variables, "ScqIMQAStdCdTbl")>
	</cfif>
	<cfif IsDefined("Variables.ScqLists")>
		<cfset StructDelete(Variables, "ScqLists")>
	</cfif>
	<cfif IsDefined("Variables.ScqListKey")>
		<cfset StructDelete(Variables, "ScqListKey")>
	</cfif>
	<!---
	If this file was called directly (example: http://danube.sba.gov/library/cfincludes/bld_ServerCachedQueries.cfm), 
	go ahead and dump Variables.Scq. In the context of a scheduled task, the dump will be ignored, but in the context 
	of a manual browser execution, it can be used to verify that everything worked correctly. 
	--->
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqStartTickCount							= GetTickCount()>
		<cfif IsDefined("URL.Debug") AND (URL.Debug IS "Yes")>
			<cfdump label="Server Cached Queries"					var="#Variables.Scq#">
		<cfelse>
			<cfoutput>
<p>
	To speed up direct calls to bld_ServerCachedQueries, 
	unless you say <b>"?Debug=Yes"</b>, 
	we now display only a summary listing. 
</p>
<p>
	To get a full cfdump of everything, click this hotlink: 
</p>
<p><div align="center">
	<a href="#CGI.Script_Name#?Debug=Yes">#CGI.Script_Name#?Debug=Yes</a>
</div></p>
<p>
	That said, here's the much shorter, much faster confirmation that everything got done: <ul></cfoutput>
			<cfloop index="Variables.ScqKey" list="#ListSort(StructKeyList(Variables.Scq), 'textnocase', 'asc')#">
				<cfset Variables.ScqElt								= Variables.Scq[Variables.ScqKey]>
				<cfif IsQuery(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "#Variables.ScqElt.RecordCount# row(s)">
				<cfelseif IsStruct(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "structure">
					<cfoutput><br></cfoutput>
				<cfelseif IsDate(Variables.ScqElt)>
					<cfset Variables.ScqComment						= DateFormat(Variables.ScqElt, "mm/dd/yyyy")
																	& " "
																	& TimeFormat(Variables.ScqElt, "HH:mm:ss")>
				<cfelseif IsSimpleValue(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "&quot;#Variables.ScqElt#&quot;">
				<cfelse>
					<cfset Variables.ScqComment						= "type unknown">
				</cfif>
				<cfoutput>
	<li>#Variables.ScqKey# (#Variables.ScqComment#)</li></cfoutput>
				<cfif IsStruct(Variables.ScqElt)>
					<cfoutput><ul></cfoutput>
					<cfloop index="Variables.ScqSubKey" list="#ListSort(StructKeyList(Variables.Scq[Variables.ScqKey]), 'textnocase', 'asc')#">
						<cfset Variables.ScqSubElt					= Variables.Scq[Variables.ScqKey][Variables.ScqSubKey]>
						<cfif IsQuery(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= "#Variables.ScqSubElt.RecordCount# row(s)">
						<cfelseif IsStruct(Variables.ScqSubElt)>
							<cfoutput><ul></cfoutput>
							<cfloop index="Variables.ScqSubSubKey" list="#ListSort(StructKeyList(Variables.ScqSubElt), 'textnocase', 'asc')#">
								<cfset Variables.ScqSubSubElt				= Variables.ScqSubElt[Variables.ScqSubSubKey]>
								<cfif IsQuery(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= "#Variables.ScqSubElt.RecordCount# row(s)">
								<cfelseif IsStruct(Variables.ScqSubSubElt)>
									<cfset Variables.ScqSubComment			= "structure">
								<cfelseif IsDate(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= DateFormat(Variables.ScqSubElt, "mm/dd/yyyy")
																			& " "
																			& TimeFormat(Variables.ScqSubElt, "HH:mm:ss")>
								<cfelseif IsSimpleValue(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= "&quot;#Variables.ScqSubSubElt#&quot;">
								<cfelse>
									<cfset Variables.ScqComment				= "type unknown">
								</cfif>
								<cfoutput>
			<li>#Variables.ScqKey#.#Variables.ScqSubKey#.#Variables.ScqSubSubKey# (#Variables.ScqComment#)</li></cfoutput>
							</cfloop>
							<cfoutput></ul></cfoutput>
						<cfelseif IsDate(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= DateFormat(Variables.ScqSubElt, "mm/dd/yyyy")
																	& " "
																	& TimeFormat(Variables.ScqSubElt, "HH:mm:ss")>
						<cfelseif IsSimpleValue(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= "&quot;#Variables.ScqSubElt#&quot;">
						<cfelse>
							<cfset Variables.ScqComment				= "type unknown">
						</cfif>
						<cfif NOT IsStruct(Variables.ScqSubElt)><!--- Structs have already been dumped. --->
							<cfoutput>
		<li>#Variables.ScqKey#.#Variables.ScqSubKey# (#Variables.ScqComment#)</li></cfoutput>
						</cfif>
					</cfloop>
					<cfoutput></ul><br></cfoutput>
				</cfif>
			</cfloop>
			<cfoutput>
	</ul>
</p>
</cfoutput>
			<cfset StructDelete(Variables, "ScqComment")>
			<cfset StructDelete(Variables, "ScqElt")>
			<cfset StructDelete(Variables, "ScqKey")>
			<cfif IsDefined("Variables.ScqSubKey")>
				<cfset StructDelete(Variables, "ScqSubElt")>
				<cfset StructDelete(Variables, "ScqSubKey")>
				<cfif IsDefined("Variables.ScqSubSubKey")>
					<cfset StructDelete(Variables, "ScqSubSubElt")>
					<cfset StructDelete(Variables, "ScqSubSubKey")>
				</cfif>
			</cfif>
		</cfif>
		<cfset Variables.ScqElapsedMilliseconds						= GetTickCount() - Variables.ScqStartTickCount>
		<cfoutput>
<p>
	End bld_ServerCachedQueries results listing after #Variables.ScqElapsedMilliseconds# milliseconds. 
</p>
</body>
</html>
</cfoutput>
		<cfset StructDelete(Variables, "ScqElapsedMilliseconds")>
		<cfset StructDelete(Variables, "ScqStartTickCount")>
	</cfif>
</cfif>

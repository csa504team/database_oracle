<!---
AUTHOR:				Nirish Namilae
DATE:				07/10/2008
DESCRIPTION:		This code can be used to replace getCFResultSet method. 
NOTES:				This costructs recordset using the output from a java method. 
INPUT:				Variables.QryResVar
OUTPUT:				Variables.QryResRecdSet. 
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/11/2008, NNI:	Original Implementation. 
 --->	
<cfset Variables.QryResMetaData						= QryResVar.getMetaData()>
<cfset Variables.QryResColNmList					= "">
<cfset Variables.QryResColTypeList					= "">
<cfparam name="Variables.QryResColOptDataTyp"	default	="N">
<cfset Variables.QryResColumnCount					= Variables.QryResMetaData.getColumnCount()>

<cfloop from="1" to="#Variables.QryResColumnCount#" index="idx">
	<cfset Variables.QryResColNmList	= ListAppend(Variables.QryResColNmList,Variables.QryResMetaData.getColumnName(idx))>
	<cfif Variables.QryResColOptDataTyp EQ "Y">
		<cfswitch expression="#Variables.QryResMetaData.getColumnType(idx)#">
			<cfcase value="93"><cfset Variables.QryResColTypeListVlu	= "Date"></cfcase>
			<cfdefaultcase><cfset Variables.QryResColTypeListVlu		= "VarChar"></cfdefaultcase>
		</cfswitch>
		<cfset Variables.QryResColTypeList	= ListAppend(Variables.QryResColTypeList,Variables.QryResColTypeListVlu)>
	</cfif>
</cfloop>

<cfif Variables.QryResColOptDataTyp EQ "Y">
	<cfset Variables.QryResRecdSet 		= QueryNew(Variables.QryResColNmList, Variables.QryResColTypeList)>
<cfelse>
	<cfset Variables.QryResRecdSet 		= QueryNew(Variables.QryResColNmList)>
</cfif>

<cfloop condition="QryResVar.next()">
 	<cfset newRow = QueryAddRow(QryResRecdSet, 1)>	
	<cfloop list="#Variables.QryResColNmList#" index="ColIdx">
		<cfset temp = QuerySetCell(QryResRecdSet, "#ColIdx#", QryResVar.getString("#ColIdx#"))>
	</cfloop>
</cfloop>

<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Saves the Form scope to the Session scope. Used in "form data recovery". 
NOTES:				Assumes a Session lock is active. This MUST be an Exclusive lock. 
OUTPUT:				Variables in the Session scope. 
REVISION HISTORY:	09/28/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Session.SlafSaveFormData")>
	<cfset StructClear(Session.SlafSaveFormData)>
<cfelse>
	<cfset Session.SlafSaveFormData							= StructNew()>
</cfif>
<!--- For some reason, Session.SlafSaveFormData = Duplicate(Form) doesn't work. --->
<cfloop index="SlafKey" list="#StructKeyList(Form)#">
	<cfset "Session.SlafSaveFormData.#SlafKey#"				= Evaluate("Form.#SlafKey#")>
</cfloop>
<cfinclude template="put_sbalookandfeel_savemessages_nolock.cfm">

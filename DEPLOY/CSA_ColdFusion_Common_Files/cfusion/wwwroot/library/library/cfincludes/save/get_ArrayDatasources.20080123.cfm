<cfif CGI.Script_Name IS "/library/cfincludes/get_ArrayDatasources.cfm"><!--- Debug mode ---><cfsetting enablecfoutputonly="Yes"></cfif><!---
AUTHOR:				Steve Seaquist
DATE:				10/30/2006
DESCRIPTION:		Defines Variables.ArrayDatasources. 
NOTES:				None. 
INPUT:				Server data, neo-query.xml file. 
OUTPUT:				Variables.ArrayDatasources. 
REVISION HISTORY:	01/23/2008, SRS:	Added support for ColdFusion 8, which keeps datasources in neo-datasource.xml. 
					09/14/2007, SRS:	Adapted to run under Developer Utilities, so developers can see what's defined. 
					06/27/2007, SRS:	Corrected "Bad" (bld_ArrayDatasources, old name) to "Gad" (get_ArrayDatasources, 
										new name) in an upward compatible way. Allowed retrieving server info in debug 
										mode as well by allowing URL.GadIncludeServerInfo. 
					02/27/2007, SRS:	If Variables.BadIncludeServerInfo is defined and "Yes", include server info. 
					01/04/2007, SRS:	Fix to allow hyphens in datasource names. 
					10/30/2006, SRS:	Original implementation. 
--->

<cfparam name="Variables.BadIncludeServerInfo"						default="No"><!--- "Bad = "bld_ArrayDatasources" --->
<cfparam name="Variables.GadIncludeServerInfo"						default="No"><!--- "Gad = "get_ArrayDatasources" --->
<cfparam name="Variables.GadDebugMode"								default="No"><!--- "Gad = "get_ArrayDatasources" --->
<cfif	(Variables.BadIncludeServerInfo IS "Yes")
	OR	(IsDefined("URL.GadIncludeServerInfo") AND (URL.GadIncludeServerInfo IS "Yes"))>
	<cfset Variables.GadIncludeServerInfo							= "Yes">
</cfif>
<cfif CGI.Script_Name IS "/library/cfincludes/get_ArrayDatasources.cfm">
	<cfset Variables.GadDebugMode									= "Yes">
</cfif>
<cflock scope="Server" type="ReadOnly" timeout="30"><!--- Don't assume get_sbashared_variables was called. --->
	<cfset Variables.NqOSName										= Server.OS.Name>
	<cfset Variables.NqProductVersion								= Server.ColdFusion.ProductVersion>
	<cfset Variables.NqRootDir										= Server.ColdFusion.RootDir>
</cflock>
<cfswitch expression="#ListGetAt(Variables.NqProductVersion,1)#">
<cfcase value="6,7">
	<!---
	Note that the "Nq" prefix is short for "neo-query", the source of everything in ArrayDatasources. Also note that 
	we can't use ExpandPath to resolve the address of neo-query, because it isn't under the document root. Hence we 
	have to do platform-specific path resolution ourselves, as follows: 
	--->
	<cfif FindNoCase("Windows", Variables.NqOSName) GT 0>			<!--- Windows: --->
		<cfset Variables.NqPathAndFile								= "#Variables.NqRootDir#\lib\neo-query.xml">
	<cfelse>														<!--- Unix and Mac: --->
		<cfset Variables.NqPathAndFile								= "#Variables.NqRootDir#/lib/neo-query.xml">
	</cfif>
	<cfset Variables.NqIndexOfDatasources							= 3>
</cfcase>
<cfcase value="8">
	<!---
	In ColdFusion 8, datasources were moved into the neo-datasource.xml file. We continue to use the "Nq" prefix 
	here, even though it'ss short for "neo-query", so we don't have to modify this whole file to use "Nd" instead 
	of "Nq". 
	--->
	<cfif FindNoCase("Windows", Variables.NqOSName) GT 0>			<!--- Windows: --->
		<cfset Variables.NqPathAndFile								= "#Variables.NqRootDir#\lib\neo-datasource.xml">
	<cfelse>														<!--- Unix and Mac: --->
		<cfset Variables.NqPathAndFile								= "#Variables.NqRootDir#/lib/neo-datasource.xml">
	</cfif>
	<cfset Variables.NqIndexOfDatasources							= 1>
</cfcase>
</cfswitch>
<cffile action="Read" file="#Variables.NqPathAndFile#" variable="Variables.NqXML">
<cfwddx action="WDDX2CFML" input="#Variables.NqXML#" output="Variables.NqDOM">
<cfset Variables.ArrayDatasources									= ArrayNew(2)>
<cfset Variables.NqS3												= Variables.NqDOM[Variables.NqIndexOfDatasources]>
<cfset Variables.NqS3Len											= 0>
<cfloop index="NqS3ItemName" list="#StructKeyList(Variables.NqS3)#">
	<cfset Variables.NqS3Len										= Variables.NqS3Len + 1>
	<cfset Variables.NqS3ItemValue									= Variables.NqS3[NqS3ItemName]>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][1]			= Variables.NqS3ItemValue.Name>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][2]			= Variables.NqS3ItemValue.Driver>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][3]			= Variables.NqS3ItemValue.Username>
	<cfif Variables.GadIncludeServerInfo>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][4]		= Variables.NqS3ItemValue.Password>
		<cfset Variables.NqS3ConnProps								= Variables.NqS3ItemValue.urlmap.ConnectionProps>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][5]		= Variables.NqS3ConnProps.Host>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][6]		= Variables.NqS3ConnProps.Port>
		<cfif IsDefined("Variables.NqS3ConnProps.Database")>		<!-- Might not be defined -->
			<cfset Variables.ArrayDatasources[Variables.NqS3Len][7]	= Variables.NqS3ConnProps.Database>
		<cfelse>
			<cfset Variables.ArrayDatasources[Variables.NqS3Len][7]	= "">
		</cfif>
	</cfif>
</cfloop>
<cfset StructDelete(Variables, "NqDOM")>
<cfset StructDelete(Variables, "NqOSName")>
<cfset StructDelete(Variables, "NqPathAndFile")>
<cfset StructDelete(Variables, "NqRootDir")>
<cfset StructDelete(Variables, "NqS3")>
<cfset StructDelete(Variables, "NqS3ConnProps")>
<cfset StructDelete(Variables, "NqS3ItemName")>
<cfset StructDelete(Variables, "NqS3ItemValue")>
<cfset StructDelete(Variables, "NqS3Len")>
<cfset StructDelete(Variables, "NqXML")>

<cfscript>
function ArraySortDatasourcesByDatasource	(p1, p2)
{
return CompareNoCase(p1[1], p2[1]);
}
function ArraySortDatasourcesByDBType		(p1, p2)
{
return (CompareNoCase(p1[2], p2[2]) * 2) + CompareNoCase(p1[1], p2[1]);
}
</cfscript>

<cfif Variables.GadDebugMode>
	<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">
	<cfif Request.SlafIPAddrIsSBA OR (Request.SlafDevTestProd IS NOT "Prod")><!--- Only SBA can see prod datasources. --->
		<cfinclude template="/library/udf/bld_ArraySortByCallback.cfm">
		<cfset Variables.ArrayDatasourcesSorted		= ArraySortByCallback(Variables.ArrayDatasources, 
													ArraySortDatasourcesByDatasource)>
		<cfswitch expression="#Request.SlafDevTestProd#">
		<cfcase value="Dev"> <cfset UsualDBServers	= "wocs01,wocs04,wocs06"></cfcase>
		<cfcase value="Test"><cfset UsualDBServers	= "wocs04,wocs05,wocs07"></cfcase>
		<cfcase value="Prod"><cfset UsualDBServers	= "wocs04,wocs08,wocs31,wocs34,wocs35,wocs36,wocs55"></cfcase>
		</cfswitch>
		<!--- Don't put temptation in the path of honest folks, but allow turning it on quickly in an emergency: --->
		<cfset Variables.ShowEncryptedPassword		= "No">
		<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head>
<title>Dump Datasources</title>
<link href="/library/css/sba.css" rel="stylesheet" type="text/css" media="screen">
<style>td {font-size: 10pt;}</style>
</head>
<body class="normal pad10">
<div align="center">
	<h3>Datasources on #Request.SlafServerName#, sorted by datasource name:</h3>
	<p>
		Datasources in <b><font color="##990000">bold and red</font></b> may be inappropriate 
		for the <b><font color="##990000">#Request.SlafDevTestProd#</font></b> environment.
	</p>
	<table border="1" summary="Sorted Datasources">
	<tr>
		<th>Datasource</th>
		<th>DBMS</th>
		<th>Username</th></cfoutput>
		<cfif Variables.GadIncludeServerInfo>
			<cfif Variables.ShowEncryptedPassword>
				<cfoutput>
		<th>Encrypted PW</th></cfoutput>
			</cfif>
			<cfoutput>
		<th>Server</th>
		<th>Port</th>
		<th>Default DB</th></cfoutput>
		</cfif>
		<cfoutput>
	</tr></cfoutput>
		<cfloop index="i" from="1" to="#ArrayLen(Variables.ArrayDatasourcesSorted)#">
			<cfset Variables.Col1					= Variables.ArrayDatasourcesSorted[i][1]>
			<cfset Variables.Col2					= Variables.ArrayDatasourcesSorted[i][2]>
			<cfset Variables.Col3					= Variables.ArrayDatasourcesSorted[i][3]>
			<cfif Variables.GadIncludeServerInfo>
				<cfset Variables.Col4				= Variables.ArrayDatasourcesSorted[i][4]>
				<cfset Variables.Col5				= Variables.ArrayDatasourcesSorted[i][5]>
				<cfset Variables.Col6				= Variables.ArrayDatasourcesSorted[i][6]>
				<cfswitch expression="#Variables.Col2#">
				<cfcase value="Oracle">
					<cfset Variables.Col7			= "(N/A)">
				</cfcase>
				<cfcase value="pointbase">
					<cfif Request.SlafDevTestProd IS NOT "Dev">
						<cfset Variables.Col1		= "<b><font color=""##990000"">#Variables.Col1#</font></b>">
						<cfset Variables.Col2		= "<b><font color=""##990000"">#Variables.Col2#</font></b>">
					</cfif>
					<cfset Variables.Col7			= "(N/A)">
				</cfcase>
				<cfdefaultcase>
					<cfset Variables.Col7			= Variables.ArrayDatasourcesSorted[i][7]>
				</cfdefaultcase>
				</cfswitch>
				<cfif	(Len	(Variables.Col5)	GT 0)
					AND	(ListFindNoCase(UsualDBServers,Variables.Col5)	IS 0)>
					<cfset Variables.Col1			= "<b><font color=""##990000"">#Variables.Col1#</font></b>">
					<cfset Variables.Col5			= "<b><font color=""##990000"">#Variables.Col5#</font></b>">
				</cfif>
			</cfif>
			<cfoutput>
	<tr class="AlternatingRowBGC4Form#Evaluate(1-(((i-1)\3) mod 2))#"><!-- From sba.css -->
		<td><cfif Len(Variables.Col1) GT 0>#Variables.Col1#<cfelse>&nbsp;</cfif></td>
		<td><cfif Len(Variables.Col2) GT 0>#Variables.Col2#<cfelse>&nbsp;</cfif></td>
		<td><cfif Len(Variables.Col3) GT 0>#Variables.Col3#<cfelse>&nbsp;</cfif></td></cfoutput>
			<cfif Variables.GadIncludeServerInfo>
				<cfif Variables.ShowEncryptedPassword>
					<cfoutput>
		<td><cfif Len(Variables.Col4) GT 0>#Variables.Col4#<cfelse>&nbsp;</cfif></td></cfoutput>
				</cfif>
				<cfoutput>
		<td><cfif Len(Variables.Col5) GT 0>#Variables.Col5#<cfelse>&nbsp;</cfif></td>
		<td><cfif Len(Variables.Col6) GT 0>#Variables.Col6#<cfelse>&nbsp;</cfif></td>
		<td><cfif Len(Variables.Col7) GT 0>#Variables.Col7#<cfelse>&nbsp;</cfif></td></cfoutput>
			</cfif>
			<cfoutput>
	</tr></cfoutput>
		</cfloop>
		<cfoutput>
	</table>
</div>
</body>
</html>
</cfoutput>
	</cfif>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
</cfif>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 25>
	<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 25 characters. "
							& "(Currently, it's #Len(Variables.FVal)# characters long.) </li>">
</cfif>

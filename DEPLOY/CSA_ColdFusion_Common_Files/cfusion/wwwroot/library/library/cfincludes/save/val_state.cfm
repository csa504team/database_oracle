<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) IS NOT 2>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 2 letters.</li>">
	<cfelse>
		<cfswitch expression="#FVal#">
		<cfcase value="AL,AK,AS,AZ,AR,CA,CO,CT,DE,DC,FL,GA,GU,HI,ID,IL,IN,IA,KS,KY,LA,ME,MH,MD,MA,MI,FM,MN,MS,MO,MT,MP,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PW,PA,PR,RI,SC,SD,TN,TX,UT,VT,VI,VA,WA,WV,WI,WY"><!--- Do nothing, it passed validation. ---></cfcase>
		<cfdefaultcase>
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>#Variables.FName# is not one of the 59 USPS codes "
									& "for US states and territories.</li>">
		</cfdefaultcase>
		</cfswitch>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel, cf_setrequesttimeout, etc.
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm right after <cfapplication>, it will include this file for you.
	Therefore, you would normally NOT include this file directly. It has been separated out of get_sbalookandfeel_variables
	for use in Web Services, apps without <cfapplication> (and hence don't have Session variables) and other situations where
	you would not be including get_sbalookandfeel_variables. However, because the output variables will most often be used in
	the SBA look-and-feel context, they retain the "Slaf" prefix. THIS CODE DOES NOT ASSUME THAT THE SESSION SCOPE EXISTS!!

INPUT:				None.
OUTPUT:				Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other
					variables defined by the application. Set RequestTimeout from URL.RequestTimeout for upward compatibility.
					(Can be overridden later by caller.) 
REVISION HISTORY:	06/03/2015, SRS:	Expanded Request.SlafIPAddrIsOISS (and OCA) to include subnet 95 following mass exodus 
										to the Concourse level. 
					05/22/2015, SRS:	Added URL.AllInstances code to touch flg_sbashared_variables. 
					05/07/2014, SRS:	Since this file can be executed by /library/ws/act_directorywatcher.cfc, it's subject to 
										a new problem with directory watchers, namely, that ColdFusion mapping are no longer 
										being respected. template="/xxx" doesn't work. template="/opt/iplanet/servers/docs/xxx" 
										doesn't work. The only thing that works is template="../xxx". What a nuisance! Also, 
										since OCA servers no longer use JBoss to log in (UserLoginSelCSP instead), made it so 
										that Request.SlafLoginLocally doesn't depend on it anymore. 
					09/24/2014, SRS:	It now appears that subnet 85 is getting dynamic IP addresses. Rather than continuing
										to edit and edit and edit, giving all of subnet 85 SlafIPAddrIsOCA/OISS privileges.
										See bld_CurrIPs or the save subdirectory if we need to go back to picking and choosing.
					08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility.
					05/15/2014, NNI:	Replaced datasource oracle_webanalytic_publicread to oracle_transaction_publicread.
					03/13/2014, SRS:	More subnet 85 maintenance.
					12/12/2013, SRS:	More subnet 85 maintenance. Also added Request.SlafDateTimeNowIso8601.
					07/26/2013, SRS:	More subnet 85 maintenance. Added SlafIPAddrIsOCA.
					04/04/2013, SRS:	More subnet 85 maintenance.
					01/30/2013, SRS:	Updated Request.SlafDefaultNAICSYrNmb (and Curr and Prev). Added some subnet 85 IPs
										to SlafIPAddrIsOISS. Added 165.193.93.233 to SlafIPAddrIsSBA.
					06/28/2012, SRS:	Added more SlafBrowser.Mobile browser detection, especiallly Android-related.
					03/12/2012, SRS:	Added support for SlafBrowser.Mobile. Got rid of 199.171.55 ("Class C") and 206.66.57
										(DMZ) IPs as criteria that set SlafIPAddrIsOISS/SBA, because they're all 165.110 now.
										Added five 165.110.85 IPs to set SlafIPAddrIsOISS.
					07/20/2010, SRS:	"New Look-and-Feel": Added more Request.SlafBrowser variables (primarily for MSIE).
					07/06/2010, SRS:	06/20 code was an emergency caused by the Flex Gateway. It was entered in haste
										during a release weekend. Cleaned it up (mainly the Query_String logic).
					06/16-20/2010, SRS:	Added code to define Request.SlafPageName and Request.SlafPathName for use in new
										path-sensitive Form Data Recovery. Calling pages can still use Variables.PageName,
										but these new names are preferred internally, within Library.
					04/12/2010, SRS:	Added "browser sniffing" code. Also added documentation of 06/24/2009 changes, which
										weren't added earlier.
					06/24/2009, SRS:	When Oracle went down today, no one could do anything. Made it so that we can establish
										a default environment to keep poeple productive. This default environment is necessarily
										not as dynamic as the sbaref Srvr, Env and CacheQry tables. But at least we can work.
					03/06/2009, SRS:	Changed inference of SlafURLProtocol for "sp" servers. Added SlafIPAddrIsOISS.
					02/19/2009, SRS:	Added a "cascading forced rebuild" capability. Specifically, if the caller is forcing a
										rebuild of Server.Gssv, we pass that forced rebuild along to get_actual_server_name.
					01/27/2009, SRS:	Revamped SlafServerName4SrvrTbls and SlafServerInstanceName4SrvrTbls logic so that
										wocs09 and wocs41 will function under New Distributed GLS without releasing all of
										library.
					12/04/2008, SRS:	Adapted to change of Jaguar package name from "gov.sba.CLS" to "gov.sba.security".
					11/05-21/2008, SRS:	Minor cleanup. Tested for presence of Jaguar by actually trying to call it inside a
										cftry/cfcatch block. Adapted to new database tables and columns.
								IAC:	Modified query based on database changes table to allow developers to access GLS.
					10/24/2008, SRS:	Adapted to new (fake) server-related queries in bld_ServerCachedQueries.
					09/16/2008, SRS:	Request.SlafURL variables (all of which are associated with and inferrd from the URL).
										Also added support for "colorado" (new name of wocs41), without removing support for
										wocs41. This allows the renaming to occur anytime. (We can remove wocs41 afterwards.)
					07/03/2008, SRS:	Defined Request.SlafLoginServerURL for use in Distributed GLS.
					11/20/2007, SRS:	Configured newdanube and newyukon as potential server names.
					11/20/2007, SRS:	Added new production server volga (runs under iweb).
					10/23/2007, SRS:	Added 206.66.57.* ("the DMZ") as one of valid SBA IP address criteria. Allowed eweb
										and enile callers to call the Login Web Service(s) without hitting the https problem.
					10/22/2007, DKC:	Added 199.171.55.* ("the Class C") as one of valid SBA IP address criteria.
					10/20/2007, SRS:	Added SlafLoginWSDL to allow logging in via Web Services.
					09/28/2007, SRS:	Added SlafDefaultNAICSYrNmb for use with 2007 NAICS conversion. Also added new logic
										to prevent existing sessions from crashing because they never defined a Server variable.
										Also added SlafMustLoginURL and SlafVersionRevision.
					03/21/2007, SRS:	Added SlafLoginURL, for use with "Distributed GLS".
					07/25/2006, SRS:	Added support for SlafIPAddrIsNAT (10/8, 172.16/12, 192.168/16). Required by PRO-Net,
										which allows all SBA users with NAT IP addresses to search for non-active firms.
										Corrected the logic for localhost to be the entire Class A range beginning with 127.
					07/17/2006, SRS:	Moved Variables.PageName code from get_sbalookandfeel_variables, so that we can set
										a new variable, namely, Request.SlafCFC.
					07/03/2006, SRS:	Just in case we ever get IPv6 or no IP address in CGI.Remote_Addr, added a check to
										make sure that ArrayLen(Variables.SlafArrayRemote_Addr) is 4. This means that all
										IPv6 and absent Remote_Addr addresses will be treated as NOT being SBA. We will
										definitely have to modify this code after the conversion to IPv6 infrastructure.
					06/26/2006, SRS:	Added Request.SlafIPAddrIsSBA code. (Essentially, just copied from PRO-Net and
										stripped of PRO-Net-specific code.)
					01/31/2006, SRS:	Original implementation.
--->

<!--- Global, SBA-Wide Configuration Parameters: --->

<cfset Request.SlafDefaultJaguarPort						= 9000><!--- Default host set below. --->
<cfset Request.SlafDefaultNAICSYrNmb						= "2012">
<cfset Request.SlafDefaultNAICSYrNmbCurr					= Request.SlafDefaultNAICSYrNmb>
<cfset Request.SlafDefaultNAICSYrNmbPrev					= "2007">

<!--- Initializations --->

<cfset		   Variables.SbaSharedVariablesSelfTestMode		= (CGI.Script_Name IS "/library/cfincludes/get_sbashared_variables.cfm")>
<cfparam name="Variables.SbaSharedVariablesForceRebuild"	default="No"><!--- Allows rebuild even if not self test. --->
<cfif Variables.SbaSharedVariablesSelfTestMode>
	<cfif IsDefined("URL.AllInstances")>
		<cfif		FileExists("/usr/ccs/bin/touch")><!--- Solaris 11 and above --->
			<cfexecute name="/usr/ccs/bin/touch" arguments=" #ExpandPath('flg_sbashared_variables.cfm')#" timeout="30" />
			<cfoutput><p>/usr/ccs/bin/touch flg_sbashared_variables.cfm<br/>Done.</p></cfoutput>
		<cfelseif	FileExists("/bin/touch")><!--- Solaris 10 and below --->
			<cfexecute name="/bin/touch" arguments=" #ExpandPath('flg_sbashared_variables.cfm')#" timeout="30" />
			<cfoutput><p>/bin/touch flg_sbashared_variables.cfm<br/>Done.</p></cfoutput>
		<cfelse>
			<cfoutput><p>Neither /usr/ccs/bin/touch nor /bin/touch exists. Unable to perform request. </p></cfoutput>
		</cfif>
		<cfabort>
	</cfif>
	<cfset Variables.SbaSharedVariablesForceRebuild			= "Yes">
</cfif>
<cfif Variables.SbaSharedVariablesForceRebuild>
	<cfset Variables.GetActualServerForceRebuild			= "Yes">
	<cfinclude template="get_actual_server_name.cfm"><!--- Probably safe to assume same directory. --->
</cfif>

<cfset StructDelete(Request, "Gssv")><!--- Make sure Request.Gssv is defined ONLY if we copied it from Server struct. --->
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif NOT Variables.SbaSharedVariablesForceRebuild>
		<cfif	IsDefined("Server.SlafGssvStruct")
			AND	IsStruct  (Server.SlafGssvStruct)><!--- "Gssv" = "get_sbashared_variables" --->
			<cfset Request.Gssv								= Duplicate(Server.SlafGssvStruct)>
		</cfif>
		<cfif IsDefined("Server.SlafServerInstanceName")>
			<cfset Request.SlafServerInstanceName			= Server.SlafServerInstanceName>
		</cfif>
		<cfif IsDefined("Server.SlafServerName")>
			<cfset Request.SlafServerName					= Server.SlafServerName>
		</cfif>
	</cfif>
	<cfset Request.SlafServerOSName							= Trim(Server.OS.Name)>
	<cfset Request.SlafVersionList							= Server.ColdFusion.ProductVersion><!--- e.g.: "7,0,2,142559" --->
</cflock>
<cfset Request.SlafVersionMajor								= ListGetAt(Request.SlafVersionList, 1)><!--- "7" --->
<cfset Request.SlafVersionMinor								= ListGetAt(Request.SlafVersionList, 2)><!--- "0" --->
<cfset Request.SlafVersionRevision							= ListGetAt(Request.SlafVersionList, 3)><!--- "2" --->
<cfset Request.SlafVersion									= Request.SlafVersionMajor
															& "."
															& Request.SlafVersionMinor>
<cfif Request.SlafVersionRevision IS NOT "0"><!--- "8.0.0" should be displayed as only "8.0" --->
	<cfset Request.SlafVersion								= Request.SlafVersion
															& "."
															& Request.SlafVersionRevision>
</cfif>
<cfif NOT IsDefined("Request.Gssv")>
	<cfset Request.Gssv										= StructNew()>
	<cfset Variables.GssvUsesSybase							= "No"><!--- Save Sybase code for quick fallback: --->
	<cfif (NOT IsDefined("Request.SlafServerName")) OR (NOT IsDefined("Request.SlafServerInstanceName"))>
		<cfinclude template="get_actual_server_name.cfm"><!--- Probably safe to assume same directory. --->
	</cfif>
	<cfif NOT IsDefined("NewSBAJavaObject")>
		<cfinclude template="../udf/bld_JavaUDFs.cfm">
	</cfif>
	<cfif IsDefined("Variables.ErrMsg")>
		<cfset Variables.SeeIfThisServerHasJaguarErrMsg		= Variables.ErrMsg>
	</cfif>
	<cfif IsDefined("Variables.TxnErr")>
		<cfset Variables.SeeIfThisServerHasJaguarTxnErr		= Variables.TxnErr>
	</cfif>
	<cfset Variables.ErrMsg									= "">
	<cfset Variables.TxnErr									= "No">
	<cfset Request.Gssv.SlafServerHasJaguar					= "Yes">
	<!--- "Authorization" is enough, but be EXTRA sure we can get to "security.Authorization": --->
	<cfset Variables.SeeIfThisServerHasJaguar				= NewSBAJavaObject("security.Authorization")>
	<cfif Variables.TxnErr OR (Len(Variables.ErrMsg) GT 0)>
		<cfset Request.Gssv.SlafServerHasJaguar				= "No">
		<cfset Request.Gssv.SlafServerHasJaguarErrMsg		= Variables.ErrMsg>
		<cfset Request.Gssv.SlafServerHasJaguarTxnErr		= Variables.TxnErr>
	<cfelse>
		<cfif IsDefined("Variables.SeeIfThisServerHasJaguarErrMsg")>
			<cfset Variables.ErrMsg							= Variables.SeeIfThisServerHasJaguarErrMsg>
		</cfif>
		<cfif IsDefined("Variables.SeeIfThisServerHasJaguarTxnErr")>
			<cfset Variables.TxnErr							= Variables.SeeIfThisServerHasJaguarTxnErr>
		</cfif>
		<cfset Variables.SeeIfThisServerHasJaguarErrMsg		= JavaCast("null", "")><!--- Discard testing object. --->
		<cfset Variables.SeeIfThisServerHasJaguarTxnErr		= JavaCast("null", "")><!--- Discard testing object. --->
	</cfif>
	<cfset Variables.SeeIfThisServerHasJaguar				= JavaCast("null", "")><!--- Discard testing object. --->
	<cfset Request.SlafServerName4SrvrTbls					= Request.SlafServerName>
	<cfset Request.SlafServerInstanceName4SrvrTbls			= Request.SlafServerInstanceName>
	<cftry><!--- There doesn't exist a SrvrSelTSP identifier for this: --->
		<cfquery name="GssvGetServers" datasource="oracle_transaction_publicread">
		select		distinct
					s.InstanceId,
					case s.GLSInd
					when 'Y'	then						'Yes'
					else									'No'
					end										as CLSInd,
					case s.LognInd
					when 'Y'	then						'Yes'
					else									'No'
					end										as LognInd,
					s.EnvCd,
					s.SrvrGrpTxt,
					e.EnvTypCd,
					e.LognURLTxt<cfif Variables.GssvUsesSybase>
		from						SrvrTbl					s
					left outer join	EnvTbl					e on (s.EnvCd = e.EnvCd)<cfelse><!--- else = Oracle, the default: --->
		from						SBAREF.SrvrTbl			s
					left outer join	SBAREF.EnvTbl			e on (s.EnvCd = e.EnvCd)</cfif>
		where		s.SrvrNm								= '#Request.SlafServerName4SrvrTbls#'
		and			s.InstanceNm							= '#Request.SlafServerInstanceName4SrvrTbls#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,s.SrvrStrtDt,getdate())>= 0)
		and			(	(			 s.SrvrEndDt			is null)
					or	(datediff(dd,s.SrvrEndDt, getdate())<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	s.SrvrStrtDt)			>= 0)
		and			(	(			s.SrvrEndDt				is null)
					or	((sysdate -	s.SrvrEndDt)			<= 0)
					)</cfif>
		</cfquery>
		<cfcatch type="Any">
			<cfoutput>INTERNAL ERROR. Unable to establish the server environment.
The following information may help: #CFCatch.Message# #CFCatch.Detail#</cfoutput>
			<cfabort>
		</cfcatch>
	</cftry>
	<cfif GssvGetServers.RecordCount IS 0>
		<!---
		If the current computer doesn't exist in SrvrTbl, it's probably a developer running from their work PC.
		If so, treat it the same as 'tiber' / 'instance1'. (= "Developers' PCs are development environment.")
		The following SQL is, and should remain, IDENTICAL to the previous SQL, except for the different values
		in SlafServerName4SrvrTbls and SlafServerInstanceName4SrvrTbls and the slightly different ErrMsg.
		--->
		<cfset Request.SlafServerName4SrvrTbls				= "tiber">		<!--- Treat same as development. --->
		<cfset Request.SlafServerInstanceName4SrvrTbls		= "instance1">	<!--- Treat same as development. --->
		<cftry><!--- There doesn't exist a SrvrSelTSP identifier for this: --->
			<cfquery name="GssvGetServers" datasource="oracle_transaction_publicread">
			select		distinct
						s.InstanceId,
						case s.GLSInd
						when 'Y'	then					'Yes'
						else								'No'
						end									as CLSInd,
						case s.LognInd
						when 'Y'	then					'Yes'
						else								'No'
						end									as LognInd,
						s.EnvCd,
						s.SrvrGrpTxt,
						e.EnvTypCd,
						e.LognURLTxt<cfif Variables.GssvUsesSybase>
			from						SrvrTbl				s
						left outer join	EnvTbl				e on (s.EnvCd = e.EnvCd)<cfelse><!--- else = Oracle, the default: --->
			from						SBAREF.SrvrTbl		s
						left outer join	SBAREF.EnvTbl		e on (s.EnvCd = e.EnvCd)</cfif>
			where		s.SrvrNm							= '#Request.SlafServerName4SrvrTbls#'
			and			s.InstanceNm						= '#Request.SlafServerInstanceName4SrvrTbls#'<cfif Variables.GssvUsesSybase>
			and			(	 datediff(dd,s.SrvrStrtDt,getdate())>= 0)
			and			(	(			 s.SrvrEndDt		is null)
						or	(datediff(dd,s.SrvrEndDt, getdate())<= 0)
						)<cfelse><!--- else = Oracle, the default: --->
			and			(	( sysdate -	s.SrvrStrtDt)		>= 0)
			and			(	(			s.SrvrEndDt			is null)
						or	((sysdate -	s.SrvrEndDt)		<= 0)
						)</cfif>
			</cfquery>
			<cfcatch type="Any">
				<cfoutput>INTERNAL ERROR. Unable to establish a development server environment.
The following information may help: #CFCatch.Message# #CFCatch.Detail#</cfoutput>
				<cfabort>
			</cfcatch>
		</cftry>
	</cfif>
	<cfif GssvGetServers.RecordCount IS 0>
		<cfoutput>INTERNAL ERROR. Unable to establish the server environment. (Nothing found.) </cfoutput>
		<cfabort>
	</cfif>
	<!--- Values taken directly from the database (or Java): --->
	<cfset Request.Gssv.SlafDevTestProd						= Trim(	GssvGetServers.EnvTypCd)>
	<cfset Request.Gssv.SlafLoginURL						=		GssvGetServers.LognURLTxt>
	<cfset Request.Gssv.SlafServerEnvironment				= Trim(	GssvGetServers.EnvCd)>
	<cfset Request.Gssv.SlafServerGroup						=		GssvGetServers.SrvrGrpTxt>
	<cfset Request.Gssv.SlafServerHasTrueCLS				=		GssvGetServers.CLSInd>
	<cfset Request.Gssv.SlafServerInstanceId				=		GssvGetServers.InstanceId>
	<cfset Request.Gssv.SlafServerInstanceLogn				=		GssvGetServers.LognInd>
	<cfset Request.Gssv.SlafServerInstanceName				= Request.SlafServerInstanceName>
	<cfset Request.Gssv.SlafServerInstanceName4SrvrTbls		= Request.SlafServerInstanceName4SrvrTbls>
	<cfset Request.Gssv.SlafServerName						= Request.SlafServerName>
	<cfset Request.Gssv.SlafServerName4SrvrTbls				= Request.SlafServerName4SrvrTbls>
	<!--- Second pass through the database: --->
	<cftry><!--- There don't exist SrvrSelTSP or SrvrCacheQrySelTSP identifiers for this: --->
		<cfquery name="GssvGetHosts" datasource="oracle_transaction_publicread">
		select		distinct
					SrvrNm<cfif Variables.GssvUsesSybase>
		from		SrvrTbl<cfelse>
		from		SBAREF.SrvrTbl</cfif>
		where		SrvrGrpTxt								= '#Request.Gssv.SlafServerGroup#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,SrvrStrtDt,getdate())	>= 0)
		and			(	(			 SrvrEndDt				is null)
					or	(datediff(dd,SrvrEndDt, getdate())	<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	SrvrStrtDt)				>= 0)
		and			(	(			SrvrEndDt				is null)
					or	((sysdate -	SrvrEndDt)				<= 0)
					)</cfif>
		order by	SrvrNm
		</cfquery>
		<cfquery name="GssvGetScqs" datasource="oracle_transaction_publicread">
		select		distinct
					case CacheQryCd
					when 'sbaref'	then					1
					else									2
					end										as SbarefFirst,
					CacheQryCd<cfif Variables.GssvUsesSybase>
		from		SrvrCacheQryTbl<cfelse>
		from		SBAREF.SrvrCacheQryTbl</cfif>
		where		SrvrNm									= '#Request.SlafServerName4SrvrTbls#'
		and			InstanceNm								= '#Request.SlafServerInstanceName4SrvrTbls#'<cfif Variables.GssvUsesSybase>
		and			(	 datediff(dd,SrvrCacheQryStrtDt,getdate())	>= 0)
		and			(	(			 SrvrCacheQryEndDt				is null)
					or	(datediff(dd,SrvrCacheQryEndDt, getdate())	<= 0)
					)<cfelse><!--- else = Oracle, the default: --->
		and			(	( sysdate -	SrvrCacheQryStrtDt)				>= 0)
		and			(	(			SrvrCacheQryEndDt				is null)
					or	((sysdate -	SrvrCacheQryEndDt)				<= 0)
					)</cfif>
		order by	SbarefFirst,
					CacheQryCd
		</cfquery>
		<cfcatch type="Any">
			<cfoutput>INTERNAL ERROR. Unable to establish the server environment.
The following information may help: #CFCatch.Message# #CFCatch.Detail#</cfoutput>
			<cfabort>
		</cfcatch>
	</cftry>
	<cfset Request.Gssv.SlafLoginHostList					= Replace(ValueList(GssvGetHosts.SrvrNm),	" ","","ALL")>
	<cfset Request.Gssv.SlafServerCachedQueryGroups			= Replace(ValueList(GssvGetScqs.CacheQryCd)," ","","ALL")>
	<!--- Inferred values: --->
	<cfswitch expression="#Request.Gssv.SlafDevTestProd#">
	<cfcase value="Dev">									<cfset Request.Gssv.SlafDevTestProdInd = 0></cfcase>
	<cfcase value="Test">									<cfset Request.Gssv.SlafDevTestProdInd = 1></cfcase>
	<cfcase value="Prod">									<cfset Request.Gssv.SlafDevTestProdInd = 2></cfcase>
	<cfdefaultcase>											<cfset Request.Gssv.SlafDevTestProdInd = 0></cfdefaultcase>
	</cfswitch>
	<cfset Request.Gssv.SlafLoginProtocol					= ListGetAt(Request.Gssv.SlafLoginURL,		1,":")>
	<cfset Request.Gssv.SlafLoginServer						= ListGetAt(Request.Gssv.SlafLoginURL,		2,":")>
	<cfset Request.Gssv.SlafLoginServer						= ListGetAt(Request.Gssv.SlafLoginServer,	1,"/")>
	<cfset Request.Gssv.SlafLoginServerURL					= Request.Gssv.SlafLoginProtocol
															& "://"
															& Request.Gssv.SlafLoginServer>
	<cfset Request.Gssv.SlafLoginWSDL						= "#Request.Gssv.SlafLoginServerURL#/cls/ws/wbs_login.wsdl">
	<cfset Request.Gssv.SlafMustLoginURL					= "#Request.Gssv.SlafLoginServerURL#/cls/dsp_mustlogin.cfm">
	<cfif	Request.Gssv.SlafServerHasTrueCLS
		and	Request.Gssv.SlafServerInstanceLogn><!--- Removed SlafServerHasJaguar (using UserLoginSelCSP now). --->
		<cfset Request.Gssv.SlafLoginInclude				= "/cls/ws/wbs_login.cfm">
		<cfset Request.Gssv.SlafLoginLocally				= "Yes">
	<cfelse>
		<cfset Request.Gssv.SlafLoginInclude				= "">
		<cfset Request.Gssv.SlafLoginLocally				= "No">
	</cfif>
	<!---
	The following prevents "connection refused" errors if the current instance is on the server group as its (https)
	login server. This consideration affects only eweb, enile and enilesp, and only when the Login Web Service is
	called as a Web Service. This cannot be part of the preceding cfif, because enilesp has SlafLoginLocally set to
	"No" (must do the callback as a Web Service), but, according to Chuda, the callback from enilesp to enile will fail
	if it uses https. The ".wsdl" file forces Web Service calls to use https, so we have to avoid it.
	--->
	<cfif Compare("#Request.Gssv.SlafServerGroup#.sba.gov",	Request.Gssv.SlafLoginServer) IS 0>
		<cfset Request.Gssv.SlafLoginWSDL					= Replace(Replace(Request.Gssv.SlafLoginWSDL,
																"https",	"http",		"ONE"),
																".wsdl",	".cfc?WSDL","ONE")>
	</cfif>
	<cfset Request.Gssv.SlafLocalHost						= "#Request.Gssv.SlafServerName#.sba.gov">
	<cfset Request.Gssv.SlafDefaultJaguarHost				= Request.Gssv.SlafLocalHost>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SlafGssvStruct.LastUpdateOfThisStructure")>
			<cfset Request.Gssv.PrevUpdateOfThisStructure	= Server.SlafGssvStruct.LastUpdateOfThisStructure>
		<cfelse>
			<cfset Request.Gssv.PrevUpdateOfThisStructure	= "N/A">
		</cfif>
	</cflock>
	<cfset Request.Gssv.LastUpdateOfThisStructure			= DateFormat(Now(), "YYYY-MM-DD")
															& " "
															& TimeFormat(Now(), "HH:mm:ss")>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SlafGssvStruct						= Duplicate(Request.Gssv)>
	</cflock>
</cfif>
<cfloop index="GssvKeyName" list="#StructKeyList(Request.Gssv)#">
	<cfswitch expression="#GssvKeyName#">
	<cfcase value="LastUpdateOfThisStructure,PrevUpdateOfThisStructure"><!--- Don't copy. ---></cfcase>
	<cfdefaultcase>
		<cfset Request[GssvKeyName]							= Request.Gssv[GssvKeyName]>
	</cfdefaultcase>
	</cfswitch>
</cfloop>
<cfif NOT Variables.SbaSharedVariablesSelfTestMode>
	<cfset StructClear (Request.Gssv)>
	<cfset StructDelete(Request,	"Gssv")>
	<cfset StructDelete(Variables,	"GssvGetHosts")>
	<cfset StructDelete(Variables,	"GssvGetServers")>
	<cfset StructDelete(Variables,	"GssvGetScqs")>
	<cfset StructDelete(Variables,	"SbaSharedVariablesForceRebuild")>
</cfif>

<!---
The following provide a canonical case (specifically, lowercase) for http/https and server name, in case the user typed
them in mixed case. Note that path and filename must be in the case given by the user, because Unix is case sensitive.
Note that we cannot infer the protocol from CGI.HTTPS, CGI.Server_Port or CGI.Server_Protocol, because the underlying
servers don't know that the user is referencing https. So we infer it from Request.SlafURLServerShortName instead.
--->

<cfset Request.SlafURLServerName							= LCase(CGI.Server_Name)>
<cfset Request.SlafURLServerShortName						= Replace(Request.SlafURLServerName, ".sba.gov", "", "ALL")>
<cfswitch expression="#Request.SlafURLServerShortName#">
<cfcase value="enile,enilesp,eweb,eweb1,eweb1sp,inile,inilesp,iweb">
	<cfset Request.SlafURLProtocol							= "https">
</cfcase>
<cfdefaultcase>
	<cfset Request.SlafURLProtocol							= "http">
</cfdefaultcase>
</cfswitch>
<cfset Request.SlafURLProtocolServerAndScriptName			= "#Request.SlafURLProtocol#://#Request.SlafURLServerName#"
															& CGI.Script_Name><!--- (already contains initial slash) --->

<!---
Note: In the following, "EOL" is short for the platform-specific "end of line" characters that terminate text lines.
--->

<cfif		FindNoCase("Windows",	Request.SlafServerOSName) GT 0>
	<cfset Request.SlafEOL									= Chr(13)&Chr(10)><!--- CRLF (Windows) --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafWindows								= "Yes">
	<cfset Request.SlafUnix									= "No">
<cfelseif	FindNoCase("Mac",		Request.SlafServerOSName) GT 0><!--- CFMX 7.01(+) supports Mac OS X(+). --->
	<!--- NOTE: The SBA doesn't have any Mac servers yet, but coding for compatibility keeps that option open. --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Same as Unix, because Mac OS X is Unix). --->
	<cfset Request.SlafMacintosh							= "Yes">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
<cfelse><!--- Because it's the "else" condition, Unix is the default: --->
	<cfset Request.SlafEOL									= Chr(10)><!--- LF (Unix). --->
	<cfset Request.SlafMacintosh							= "No">
	<cfset Request.SlafUnix									= "Yes">
	<cfset Request.SlafWindows								= "No">
</cfif>

<!---
If you call Now() repeatedly near midnight, it's possible for the displayed date not to match up with the displayed time.
Therefore we grab it once and use it for all calculated date/time values. That way, they'll all match up.
--->

<cfset Request.SlafDateTimeNow								= Now()>
<!--- Calculate current Federal Fiscal Year: --->
<cfset Request.SlafCurrFY									= Year(Request.SlafDateTimeNow)>
<cfif Month(Request.SlafDateTimeNow) GT 9>
	<cfset Request.SlafCurrFY								= Request.SlafCurrFY + 1>
</cfif>
<!--- Request.SlafDateTimeNow, formatted for display in a consistent manner: --->
<cfset Request.SlafDateTimeNowDisplay						= DateFormat(Request.SlafDateTimeNow, "YYYY-MM-DD")
															& " "
															& TimeFormat(Request.SlafDateTimeNow, "hh:mm:ss tt")>
<cfset Request.SlafDateTimeNowIso8601						= DateFormat(Request.SlafDateTimeNow, "YYYY-MM-DD")
															& "T"
															& TimeFormat(Request.SlafDateTimeNow, "HH:mm:ss.sss")>

<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<cfif	(Request.SlafDevTestProd IS "Prod")
	AND	(DateDiff("n", Request.SlafDateTimeNow, ParseDateTime("10/01/2007 6:00 AM")) GT 0)>
	<cfset Request.SlafDefaultNAICSYrNmb					= "2002">
	<cfset Request.SlafDefaultNAICSYrNmbCurr				= Request.SlafDefaultNAICSYrNmb>
	<cfset Request.SlafDefaultNAICSYrNmbPrev				= "1997">
</cfif>
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->
<!--- TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP TEMP --->

<cfif IsDefined("URL.RequestTimeout")>
	<cf_SetRequestTimeout seconds="#URL.RequestTimeout#">
</cfif>

<!--- SBA-Only IP Restriction, for applications that need it: --->

<cfset Request.SlafIPAddrIsNAT								= "No">
<cfset Request.SlafIPAddrIsOCA								= "No"><!--- Central Office 8th floor (selected IPs). --->
<cfset Request.SlafIPAddrIsOISS								= "No"><!--- Central Office 4th floor, OCA or Herndon. --->
<cfset Request.SlafIPAddrIsSBA								= "No">
<cfset Variables.SlafArrayRemote_Addr						= ListToArray(CGI.Remote_Addr, ".")>
<cfset Variables.SlafArrayRemote_AddrLen					= ArrayLen(Variables.SlafArrayRemote_Addr)>
<cfif Variables.SlafArrayRemote_AddrLen IS 4>
	<!--- Use cfswitches at all depths for speed: --->
	<cfswitch expression="#Variables.SlafArrayRemote_Addr[1]#"><!--- The cfcases are ordered numerically for easy lookup: --->
	<cfcase value="10"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfset Request.SlafIPAddrIsNAT						= "Yes">
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="63"><!--- 63.241.202.* are our proxy servers, including the VPN server. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 241)
			AND	(Variables.SlafArrayRemote_Addr[3] IS 202)>
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="127"><!--- Need to be at SBA to access datasources, so localhost must be SBA user. --->
		<cfset Request.SlafIPAddrIsSBA						= "Yes">
	</cfcase>
	<cfcase value="165"><!--- According to Chuda, we own the entire 165.110 Class B network. --->
		<cfswitch expression="#Variables.SlafArrayRemote_Addr[2]#">
		<cfcase value="110">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
			<cfswitch expression="#Variables.SlafArrayRemote_Addr[3]#">
			<cfcase value="39,40,45"><!--- Central Office 4th floor. --->
				<cfset Request.SlafIPAddrIsOISS				= "Yes">
			</cfcase>
			<cfcase value="85,95"><!--- Central Office, north side of 8th floor and Concourse level. --->
				<cfset Request.SlafIPAddrIsOCA				= "Yes">
				<cfset Request.SlafIPAddrIsOISS				= "Yes">
			</cfcase>
			</cfswitch>
		</cfcase>
		<cfcase value="193">
			<!---
			Some Denver folks who set up their own domain. They weren't very nice about it, and they told us only one IP
			address, 165.193.93.233. Okay, if that's the way they want it, give them only that one IP address for now:
			--->
			<cfif	(Variables.SlafArrayRemote_Addr[3] IS "93")
				and	(Variables.SlafArrayRemote_Addr[4] IS "233")>
				<cfset Request.SlafIPAddrIsSBA				= "Yes">
			</cfif>
		</cfcase>
		</cfswitch>
	</cfcase>
	<cfcase value="172"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] GE  16)
			AND	(Variables.SlafArrayRemote_Addr[2] LE  31)>	<!--- 172.16/12 --->
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	<cfcase value="192"><!--- Illegal on Internet, so could only be local, usually via VPN. --->
		<cfif	(Variables.SlafArrayRemote_Addr[2] IS 168)>
			<cfset Request.SlafIPAddrIsNAT					= "Yes">
			<cfset Request.SlafIPAddrIsSBA					= "Yes">
		</cfif>
	</cfcase>
	</cfswitch>
</cfif>

<!---
The following is "browser sniffing" needed to generate correct SBA Look-and-Feel code for certain specific browsers.
For example, TouchBased browsers cannot honor ":hover" in CSS nor can they show tooltips (title attributes) when the user
hovers over them. (There's no mouse cursor, so there's no such thing as hovering. Either you're touching or not.)
--->

<cfset Request.SlafBrowser									= StructNew()>
<cfset Request.SlafBrowser["Android"]						= (FindNoCase("Android",		CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["BlackBerry"]					= (FindNoCase("BlackBerry",		CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["Chrome"]						= (FindNoCase("Chrome",			CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["Firefox"]						= (REFind("Firefox|Firebird|Phoenix|SeaMonkey",
																							CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["Mobile"]						= (FindNoCase("Mobile",			CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["MSIE"]							= (FindNoCase("MSIE",			CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["Opera"]							= (REFind("Opera|Presto",		CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["Safari"]						= (FindNoCase("Safari",			CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["TouchBased"]					= (REFind("iPad|iPhone|iPod",	CGI.HTTP_User_Agent) gt 0)>
<cfset Request.SlafBrowser["WebKit"]						= (FindNoCase("WebKit",			CGI.HTTP_User_Agent) gt 0)>
<!--- Some browsers report other browsers' names in their string. The following cfifs filter out that sort of thing: --->
<cfif Request.SlafBrowser.Android>
	<cfset Request.SlafBrowser.Mobile						= "Yes">
	<cfset Request.SlafBrowser.Safari						= "No">
</cfif>
<cfif Request.SlafBrowser.Chrome>
	<cfset Request.SlafBrowser.Safari						= "No">
</cfif>
<cfif Request.SlafBrowser.Opera>
	<cfset Request.SlafBrowser.MSIE							= "No">
</cfif>
<!--- Pages that tend to have the "disappearing scrollbars" problem can use this variable to recommend 2-finger swipe: --->
<cfset Request.SlafBrowser["MobileScrollingProblem"]		= (Request.SlafBrowser.Mobile and Request.SlafBrowser.WebKit)>
<!--- Tooltips for touch-based browsers (jQuery Mobile may make this code unnecessary, so maybe delete!): --->
<cfset Request.SlafBrowser["TouchBasedJSToShowTooltips"]	= ""><!--- Nullstring if not a touch-based browser. --->
<cfif Request.SlafBrowser.TouchBased>
	<!--- If touch-based, this is autoincluded in the top window. (Manually include it after jQuery in frames.) --->
	<cfsavecontent variable="Request.SlafBrowser.TouchBasedJSToShowTooltips">
		<cfoutput>
// Allow touch-based browsers to press-and-hold to see tooltips. This normally brings up the context menu. Instead,
// the following brings up the tooltip and gives the user the option to Cancel the context menu or press OK to see it:
$(document).ready(function				()
	{
	$("[title]").bind("contextmenu",	function()
		{// If the HTML element doesn't normally take the title attribute, "this.title" will be undefined. Therefore:
		var	sTitle						= (this.title ? this.title : this.getAttribute("title"));
		return confirm(sTitle			+ "\n\n"
					+ "The message above is the 'tooltip' associated with this element. If that's all you wanted to see, "
					+ "press Cancel. But if you actually wanted to get the 'context menu', press OK instead.");
		});
	});</cfoutput>
	</cfsavecontent>
</cfif>

<!--- Detect whether request was to a ColdFusion Component (CFC file): --->

<cfif Len(CGI.Script_Name) GT 0>
	<!---
	Example using "/cls/dsp_login.cfm", so that you'll know what you're getting when you use these Request variables:
	Regular expression ".*/" matches everything up to and INCLUDING the last slash. So the REReplace results in setting
	SlafPageName to "dsp_login.cfm", which is 13 characters long. We then remove the rightmost 14 characters with Left,
	which ALSO removes the last slash, which sets SlafPathName to "/cls".
	--->
	<cfset Request.SlafPageName								= REReplace (CGI.Script_Name, ".*/", "", "ALL")>
	<cfif (Len(CGI.Script_Name)-(Len(Request.SlafPageName)+1)) LT 1>
		<!---
		The Flex gateway passes component notation to access CFCs (name.name.cfcname). If that's what this call is,
		the regular expression ".*/" matches nothing, and the entire string becomes SlafPageName. For right now, don't
		do anything to deal with that situation except prevent a crash referencing Left(CGI.Script_Name, 0):
		--->
		<cfset Request.SlafPathName							= "">
	<cfelse>
		<cfset Request.SlafPathName							= Left(CGI.Script_Name,
																	Len(CGI.Script_Name)-(Len(Request.SlafPageName)+1))>
	</cfif>
<cfelse>
	<cfset Request.SlafPageName								= "index.cfm">
	<cfset Request.SlafPathName								= "">
</cfif>
<cfset Request.SlafQueryString								= CGI.Query_String>
<cfset Request.SlafQueryStringLen							= Len(Request.SlafQueryString)>
<cfset Request.SlafCFC										=	(Len				(Request.SlafPageName) GT 4)
															AND	(Compare(LCase(Right(Request.SlafPageName, 4)), ".cfc") IS 0)>
<!--- Keep old code working that may still reference variables in the Variables scope: --->
<cfset Variables.PageName									= Request.SlafPageName>
<cfset Variables.QueryString								= Request.SlafQueryString>
<cfset Variables.QueryStringLen								= Request.SlafQueryStringLen>

<!--- Self-Test Mode: --->

<cfif Variables.SbaSharedVariablesSelfTestMode>
	<cfdump var="#Request#"		label="Request">
	<cfdump var="#Variables#"	label="Variables">
	<cfdump var="#CGI#"			label="CGI">
</cfif>

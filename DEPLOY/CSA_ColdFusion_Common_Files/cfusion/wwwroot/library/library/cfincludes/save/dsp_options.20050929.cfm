<!---
AUTHOR:				Steve Seaquist
DATE:				09/29/2005
DESCRIPTION:		Given a query, displays <option> tags. Works with <select> and <select multiple>. 
NOTES:				None. 
INPUT:				Variables scope: DspOptsCodeName, DspOptsDescName, DspOptsNotSelText, DspOptsQueryName, DspOptsSelList, 
					DspOptsShowCode, DspOptsTabs. "DspOpts" prefix is to avoid conflicts with caller variables. 
OUTPUT:				<option [selected] value="code">description</option> for every row in the query. 
REVISION HISTORY:	09/29/2005, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfparam name="DspOptsNotSelText"						default="Not Yet Selected">
<cfparam name="DspOptsTabs"								default="">
<cfparam name="DspOptsShowCode"							default="No">

<cfif Len(DspOptsSelList) IS 0>							<cfset DspOptsSel	= "selected">
<cfelse>												<cfset DspOptsSel	= "        "></cfif>
<cfoutput>
#DspOptsTabs#<option #DspOptsSel# value="">#DspOptsNotSelText#</option></cfoutput>

<cfloop query="#DspOptsQueryName#">
	<cfset DspOptsCode									= Evaluate("#DspOptsQueryName#.#DspOptsCodeName#")>
	<cfset DspOptsDesc									= Evaluate("#DspOptsQueryName#.#DspOptsDescName#")>
	<cfif ListFind(DspOptsSelList, DspOptsCode) GT 0>	<cfset DspOptsSel	= "selected">
	<cfelse>											<cfset DspOptsSel	= "        "></cfif>
	<cfoutput>
#DspOptsTabs#<option #DspOptsSel# value="#DspOptsCode#"><cfif DspOptsShowCode>#DspOptsCode# - </cfif>#DspOptsDesc#</option></cfoutput>
</cfloop>

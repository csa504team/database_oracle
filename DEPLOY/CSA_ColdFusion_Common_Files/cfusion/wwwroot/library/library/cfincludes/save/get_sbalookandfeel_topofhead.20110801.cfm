<cfif CGI.Script_Name is "/library/cfincludes/get_sbalookandfeel_topofhead.cfm"><cfsetting enablecfoutputonly="Yes">
	<!---
	In order for DOCTYPE to be the *very* first line of the output, we need for enablecfoutputonly to occur right away, 
	before there has been any chance for a blank line to sneak into the output. Hence, cfsetting was done above, on the 
	same line as our check for self-test mode. This will allow easier HTML5 testing and conversion in the future. 
	--->
	<cfset Request.SlafTopOfHeadSelfTestMode		= "Yes">
<cfelse>
	<cfset Request.SlafTopOfHeadSelfTestMode		= "No">
</cfif>
<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds Request.SlafHead and Request.SlafTopOfHead for use by cf_sbalookandfeel, reports, pages in 
					frames, etc. Request.SlafTopOfHead is the important one. It provides a standard, correct inclusion 
					sequence of SBA CSS files and JavaScript variables useful with CSS manipulation. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm, it will include this file for you. Therefore, you would normally 
	NOT include this file directly. It exists outside of get_sbalookandfeel_variables so that the cf_sbalookandfeel can 
	override some of the generated code, based on Attributes passed to it in the custom tag call. 

	If you want to define your own DOCTYPE, html tag, head tag and title tag first, cfoutput #Request.SlafTopOfHead# just 
	after the title tag. You would then continue on to define other system-specific CSS, page-specific JavaScript and the 
	/head. 

	If you don't care to define DOCTYPE, html and head yourself, simply cfoutput #Request.Head# instead. You would then 
	continue on to define the title, #Request.SlafTopOfHead#, system-specific CSS, page-specific JavaScript and the /head. 

	And if you don't mind defining the title tag lower down, simply cfoutput #Request.SlafHeadAndTopOfHead#, then title, 
	system-specific CSS, page-specific javascript and the /head. 

	When a custom tag calls a cfinclude, the Attributes scope is not passed to the cfinclude. So cf_sbalookandfeel passes 
	Attributes namespaced as Request.SlafTopOfHead variables to prevent name conflicts with the caller's variables. 

	You do NOT have to be using SBA Look-and-Feel to use Request.SlafHead or SlafTopOfHead. In particular, developers of 
	<cfdocument format="pdf" ... > reports and other formatted reports can use it, so that the report will pick up the 
	SBA's standard fonts and styles. In order for any page to be able to use these variables, DumpObject.js, jquery.js and 
	sbalookandfeel.js are not defined here. (Instead, they're defined in cf_sbalookandfeel.) 

INPUT:				Request.SlafApplicationName, Request.SlafTopOfHead variables (all optional, normally used only by 
					cf_sbalookandfeel). ApplicationName may or may not have been defined by get_sbalookandfeel_variables. 
					Assumes all get_sbashared_variables are defined. 
OUTPUT:				Request.SlafHead, Request.SlafTopOfHead and Request.SlafHeadAndTopOfHead. 
REVISION HISTORY:	07/29/2011, SRS:	Defined new .mainnavexternalsys class for standard appearance of inter-system 
										main navigation buttons. (Bumped up CachedAsOf on 3 CSS files.) 
					03/28/2011, SRS:	(Same check-out as 11/09.) Fixed IE 9's non-support of inline-block and other 
										things they said they'd support by now by basing the inclusion of MSIE css classes 
										on IE alone, not "lt IE 9" anymore. 
					11/09/2010, SRS:	Updated CachedAsOf times for more efficient css and js files. 
					10/12/2010, SRS:	Added support for sba.msie.noscript.css. Consolidated MSIE conditional comments 
										into only one (with links and script together). Updated CachedAsOf times. 
					09/22/2010, SRS:	Added ability to use AppendCachedAsOf (for greatly improves reliability after 
										JS/CSS edits), but not using it just yet. Improved HTML5 support. 
					09/08/2010, SRS:	11th hour fix for MSIE 8. Treat it the same as MSIE 6 and 7. 
					08/10/2010, SRS:	Began prototyping HTML5. (Transparent to everyone but me, SRS.) 
					07/16/2010, SRS:	"New Look-and-Feel". Made Request.SlafTextOnly affect SlafTopOfHeadTextOnly. 
					06/10/2010, SRS:	Changed Variables.AppicationName to Request.SlafApplicationName, so that this 
										routine won't mess up if cfincluded from within a custom tag. 
					03/22/2010, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfif NOT IsDefined("Request.SlafHTML5")><!--- Set (cf_sbalookandfeel html5="Yes") if you need an HTML5 feature. --->
	<cfset Request.SlafHTML5						= (CGI.Remote_Addr is "165.110.40.166")><!--- DeveloperOfSlaf's PC --->
	<cfset Request.SlafHTML5						= "No"><!--- Turn off HTML5, even on DeveloperOfSlaf's PC. --->
	<!---
	Synopsis of HTML5 problems so far: 
	* Firefox 3.6.8 messes up cf_sbatree display. 
	* MSIE 7 messes up GLS Choose Function: MainNav graphics and center justifies list of roles (across 2 columns)
	--->
</cfif>

<!--- Runtime Options: --->

<cfparam name="Request.SlafTopOfHeadLang"			default="en-US">
<cfparam name="Request.SlafTopOfHeadLibURL"			default="/library">
<cfif IsDefined("Request.SlafTextOnly")>
	<cfset Request.SlafTopOfHeadTextOnly			= Request.SlafTextOnly>
<cfelse>
	<cfparam name="Request.SlafTopOfHeadTextOnly"	default="No">
</cfif>

<cfparam name="Request.SlafButtons3D"				default="Yes"><!--- Until MSIE 9 is commonplace. --->

<!--- Initializations: --->

<cfif NOT IsDefined("AppendCachedAsOf")><!--- Tricks browsers to assure that we pick up edited JS/CSS right away. --->
	<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
</cfif>
<cfif NOT IsDefined("Request.SlafDevTestProd")><!--- Passed to the user, below, in the generated JavaScript. --->
	<cfinclude template="get_sbashared_variables.cfm"><!--- Safe to omit path. We're both in /library/cfincludes. --->
</cfif>

<!--- Generate the variables: --->

<cfsavecontent variable="Request.SlafHead">
	<cfif Request.SlafHTML5>
		<!--- Currently, the HTML5 header messes up cf_sbatree, but not much else: --->
		<cfoutput><!doctype html>
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr">
<head>
<meta charset="utf-8"></cfoutput>
	<cfelse>
		<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></cfoutput>
	</cfif>
</cfsavecontent>

<cfsavecontent variable="Request.SlafTopOfHead">
	<cfoutput><cfif IsDefined("Request.SlafApplicationName") AND (Request.SlafApplicationName IS "CLS")>
<link		href="/cls/dsp_choosefunction.cfm"	accesskey="1"
			rel="Home"			title="Home (Return to GLS Choose Function)"></cfif>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.css?CachedAsOf=2011-07-29T1143"
			rel="stylesheet"	type="text/css" media="all"/>
<noscript>
	<link	href="#Request.SlafTopOfHeadLibURL#/css/sba.noscript.css?CachedAsOf=2010-10-14T19:23"
			rel="stylesheet"	type="text/css" media="all"/>
</noscript><cfif Request.SlafTopOfHeadTextOnly>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.textonly.css?CachedAsOf=2010-07-19T17:38:03"
			rel="stylesheet"	type="text/css" media="all"/></cfif>
<script>
var	gSlafDevTestProd					= "#Request.SlafDevTestProd#";
var	gSlafDevTestProdInd					= "#Request.SlafDevTestProdInd#";
var	gSlafInlineBlock					= "inline-block";
var	gSlafInlineTable					= "inline-table";
var	gSlafMSIE6or7						= false;	// Actually includes MSIE 8 as well. Keep the name the same for now. 
</script>
<!--[if IE]><cfif Request.SlafButtons3D>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.msie6and7.css3pie.css?CachedAsOf=2011-08-01T1401"
			rel="stylesheet"	type="text/css" media="all"/><cfelse>
<link		href="#Request.SlafTopOfHeadLibURL#/css/sba.msie6and7.css?CachedAsOf=2011-07-29T1143"
			rel="stylesheet"	type="text/css" media="all"/></cfif><cfif Request.SlafHTML5>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/html5/html5shiv.js?CachedAsOf=2010-09-30T19:39:04"></script></cfif>
<noscript>
	<link	href="#Request.SlafTopOfHeadLibURL#/css/sba.msie.noscript.css?CachedAsOf=2010-10-12T12:04"
			rel="stylesheet"	type="text/css" media="all"/>
</noscript>
<script>
gSlafInlineBlock						= "inline";
gSlafInlineTable						= "inline";
gSlafMSIE6or7							= true;		// Actually includes MSIE 8 as well. Keep the name the same for now. 
</script>
<![endif]--><cfif IsDefined("Request.SlafSuppressRTE") and Request.SlafSuppressRTE>
<style>
.rte									{visibility: visible	!important;}
</style></cfif>
</cfoutput><!--- Do NOT define DumpObject.js, jquery.js or sbalookandfeel.js here. Instead, cf_sbalookandfeel does that. --->
</cfsavecontent>

<cfsavecontent variable="Request.SlafHeadAndTopOfHead">
	<cfoutput>#Request.SlafHead##Request.SlafTopOfHead#</cfoutput>
</cfsavecontent>

<!--- Self-Test Mode: --->

<cfif Request.SlafTopOfHeadSelfTestMode>
	<!--- Note that we "eat our own dogfood" (use the variables we create, in addition to displaying them): --->
	<cfoutput>#Request.SlafHead#
<title>SBA - Library - get_sbalookandfeel_topofhead Self-Test Mode</title>#Request.SlafTopOfHead#
</head>
<body class="pad20">
<p align="center" class="title1">SBA - Library - get_sbalookandfeel_topofhead Self-Test Mode</p>
<pre>
<span style="color:##900; font-weight:bold;">Request.SlafHead:</span>

#Replace(Request.SlafHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafTopOfHead:</span>

#Replace(Request.SlafTopOfHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafHeadAndTopOfHead:</span>

#Replace(Request.SlafHeadAndTopOfHead, "<", "&lt;", "ALL")#
</pre>
</body>
</html>
</cfoutput>
</cfif>

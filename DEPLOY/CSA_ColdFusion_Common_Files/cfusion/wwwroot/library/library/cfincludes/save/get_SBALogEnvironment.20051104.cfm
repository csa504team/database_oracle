<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Gets Request.SBALogger, our shared Category object cached in the Server scope. Then instantiates 
					a logger for the current Request.SBALogSystemName if it hasn't already been instantiated. 
NOTES:				Assumes IsDefined("Request.SBALogSystemName"), so that we don't have to keep testing it in each 
					nested include. Included at the start of all logging routines before writing to the log. 
INPUT:				None. 
OUTPUT:				Request.SBALogger. 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfset Variables.SBALogConfigInitialized				= "No">
<cflock scope="SERVER" type="READONLY" timeout="30">
	<cfif IsDefined("Server.SBALogConfigInitialized")>
		<cfset Variables.SBALogConfigInitialized		= Server.SBALogConfigInitialized>
	</cfif>
</cflock>
<cfif NOT Variables.SBALogConfigInitialized>
	<!---
	Or, <cfset CreateObject("Java", "org.apache.log4j.xml.DOMConfigurator").configure(ExpandPath("/logging/log4j.xml"))> 
	--->
	<cfset Variables.SleConfigurator					= CreateObject("Java", "org.apache.log4j.xml.DOMConfigurator")>
	<cfset Variables.SleConfigurator.configure(ExpandPath("/logging/log4j.xml"))>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.SBALogConfigInitialized			= "Yes">
		<!---
		According to the documentation, we don't have to reinstantiate Request.SBALogger here to pick up the new config. 
		Of course, that's assuming that the documentation is correct. So consider the possibility that we may need to 
		reinstantiate here. 
		--->
		<cfset Server.SBALogger							= CreateObject("Java", "org.apache.log4j.Category")>
		<cfset Request.SBALogger						= Server.SBALogger>
	</cflock>
	<!--- If Variables.SleConfigurator is defined, that's the sign that the current request did the reconfiguration. --->
</cfif>
<cfif NOT IsDefined("Request.SBALogger")>
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.SBALogger")>
			<cfset Request.SBALogger					= Server.SBALogger>
		</cfif>
	</cflock>
	<cfif NOT IsDefined("Request.SBALogger")>
		<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
			<cfset Server.SBALogger						= CreateObject("Java", "org.apache.log4j.Category")>
			<cfset Request.SBALogger					= Server.SBALogger>
		</cflock>
	</cfif>
</cfif>
<!--- Request.SBALogSystemName can change within a request, so make sure Request.SleLogger is for the current system: --->
<cfif (NOT IsDefined("Request.SleLogger")) OR (Request.SleSystemName IS NOT Request.SBALogSystemName)>
	<cfset Request.SleLogger							= Request.SBALogger.getInstance(Request.SBALogSystemName)>
	<cfset Request.SleSystemName						= Request.SBALogSystemName>
	<cfset Request.SleHT								= Chr(9)><!--- Saves on Chr calls. --->
	<cfset Request.SleIsDebugEnabled					= Request.SleLogger.isDebugEnabled()>
	<cfset Request.SleIsInfoEnabled						= Request.SleLogger.isInfoEnabled()>
	<cfset Variables.Priorities							= CreateObject("Java", "org.apache.log4j.Priority")>
	<cfset Request.SleIsWarnEnabled						= Request.SleLogger.isEnabledFor(Variables.Priorities.WARN)>
	<cfset Request.SleIsErrorEnabled					= Request.SleLogger.isEnabledFor(Variables.Priorities.ERROR)>
	<cfset Request.SleIsFatalEnabled					= Request.SleLogger.isEnabledFor(Variables.Priorities.FATAL)>
	<cfset Request.SleScriptName						= CGI.Script_Name>
	<cfif		IsDefined("Variables.OrigUserId")>		<cfset Request.SleUser	= Variables.OrigUserId>
	<cfelseif	IsDefined("Variables.username") AND (Variables.username IS NOT "sbaupdate")>
														<cfset Request.SleUser	= Variables.username>
	<cfelseif	IsDefined("Variables.CompanyUserId")>	<cfset Request.SleUser	= Variables.CompanyUserId>
	<cfelse>											<cfset Request.SleUser	= ""></cfif>
	<!--- DB, EntityName, ExeutionTime and LogText vary with each call, so don't set defaults for them. --->
</cfif>

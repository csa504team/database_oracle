<!---
AUTHOR:				Steve Seaquist
DATE:				08/19/2005
DESCRIPTION:		Builds hidden form elements for use by get_sbalookandfeel_variables.cfm. 
NOTES:				<cfinclude template="get_sbalookandfeel_variables.cfm"> right after <cfapplication>. 
					If you don't, Request.SlafTextOnly (below) won't be defined and your page will crash. 
INPUT:				None. 
OUTPUT:				Hidden form elements. By the way, they're prefixed with "Slaf" (= "sbalookandfeel"), 
					so as not to conflict with other variables defined by the application. 
REVISION HISTORY:	08/19/2005, SRS:	Original implementation. 
--->

<cfoutput>
<input type="Hidden" name="SlafClientHeight"	value="">
<input type="Hidden" name="SlafClientWidth"		value="">
<input type="Hidden" name="SlafResolution"		value="1024x768"><!--- Always make sure it's a lowercase "x". --->
<input type="Hidden" name="SlafTextOnly"		value="#Request.SlafTextOnly#">
</cfoutput>

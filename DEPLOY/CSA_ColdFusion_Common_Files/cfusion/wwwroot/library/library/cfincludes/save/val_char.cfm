<!---
AUTHOR:				Steve Seaquist
DATE:				12/15/2000
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR40
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form), Variables.MaxCharLength
OUTPUT:				Variables.FVal, Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/10/2002, JBB:	Modified the existing val_chars to create just one where
										the required length can be passed
--->
<CFSET Variables.FVal							= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal						= Evaluate("Form.#Variables.FNam#")>
	<CFIF Len(Variables.FVal) GT 0>
		<CFSET Variables.BlankScreen			= "No">
		<CFIF Len(Variables.FVal) GT #Variables.MaxCharLength#>
			<CFSET Variables.FieldErrMsg		= Variables.FName & " cannot be more than #Variables.MaxCharLength# characters long.">
			<CFINCLUDE TEMPLATE="val_FormatErrMsg.cfm">
		</CFIF>
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"				= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString						= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

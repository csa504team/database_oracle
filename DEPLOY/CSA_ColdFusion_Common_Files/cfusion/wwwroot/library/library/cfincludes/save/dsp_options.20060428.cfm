<!---
AUTHOR:				Steve Seaquist
DATE:				09/29/2005
DESCRIPTION:		Given a query, displays <option> tags. Works with <select> and <select multiple>. 
NOTES:

	Example usage: Suppose that you want to build a drop-down menu of countries a firm exports to. (It would probably be 
	better to use checkboxes, but suppose you're using a select-multiple drop-down to save screen space.) Suppose further 
	that you have the country codes the firm currently exports to in a query called getCurrExportCountries. And suppose 
	you retrieved the query of all countries from server cached queries as Variables.ScqActvCountries. And suppose you 
	want to indent the options with 3 tabs before each line. Last but not least, suppose that you're not even ALLOWED to 
	display Afghanistan, Iran, North Korea or Syria. The following illustrates how to use this cfinclude file: 

		<cfset Variables.DspOptsQueryName					= "Variables.ScqActvCountries">
		<cfset Variables.DspOptsSelList						= ValueList(getCurrExportCountries.CountryCd)>
		<cfset Variables.DspOptsSkipList					= "AF,IR,KN,SY">
		<cfset Variables.DspOptsTabs						= RepeatString(Chr(9), 3)>
		<select name="Countries" multiple size="10">
		<cfinclude template="/library/cfincludes/dsp_options.cfm">
		</select>

	Remember that old values for these DspOpts variables may still be defined if you include this file more than once. 

INPUT:				Variables scope: DspOptsCodeName, DspOptsDescName, DspOptsNotSelText, DspOptsQueryName, DspOptsSelList, 
					DspOptsShowCode, DspOptsTabs. "DspOpts" prefix is to avoid conflicts with caller variables. 
OUTPUT:				<option [selected] value="code">description</option> for every row in the query. 
REVISION HISTORY:	04/28/2006, SRS:	Added cfparams for code and description names, with defaults to the names used by 
										bld_ServerCachedQueries. Added SkipList feature because server cached queries may 
										contain more than some apps want to display. Also allowed DspOptsNotSelText to be 
										"Suppress", which suppresses the generation of the Not Yet Selected option. And 
										added a self-test mode. 
					09/29/2005, SRS:	Original implementation. 
--->

<cfif CGI.Script_Name IS "/library/cfincludes/dsp_options.cfm">
	<!--- Calling this file directly from a browser is self-test mode, which is largely the same as the example above: --->
	<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm"><!--- Only in self-test. Otherwise, caller calls. --->
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfset Variables.ScqActvCountries					= Server.ScqActvCountries>
	</cflock>
	<cfset Variables.DspOptsQueryName						= "Variables.ScqActvCountries">
	<cfset Variables.DspOptsSelList							= "AG,AR">
	<cfset Variables.DspOptsSkipList						= "AF,IR,KN,SY">
	<cfset Variables.DspOptsTabs							= RepeatString(Chr(9), 3)>
	<cfoutput>
<html lang="en-US"><head><title>dsp_options self-test mode</title></head>
<body bgcolor="##ffffff">
	If dsp_options is working correctly, Afghanistan is not visible, and Algeria and Argentina are pre-selected: 
	<form action="javascript:void()">
		<select name="Countries" multiple size="10"></cfoutput><!--- Don't cfinclude in self-test mode! Just fall through! --->
</cfif>

<!--- Configuration Parameters: --->

<cfparam name="Variables.DspOptsCodeName"					default="code">			<!--- See bld_ServerCachedQueries. --->
<cfparam name="Variables.DspOptsDescName"					default="description">	<!--- See bld_ServerCachedQueries. --->
<cfparam name="Variables.DspOptsNotSelText"					default="Not Yet Selected">
<cfparam name="Variables.DspOptsQueryName">											<!--- No default = MANDATORY. --->
<cfparam name="Variables.DspOptsSelList"					default="">				<!--- Only one if not select-multiple! --->
<cfparam name="Variables.DspOptsShowCode"					default="No">
<cfparam name="Variables.DspOptsSkipList"					default="">				<!--- Skip these codes, if any. --->
<cfparam name="Variables.DspOptsTabs"						default="">				<!--- Nullstring = don't indent. --->

<cfif Variables.DspOptsNotSelText IS NOT "Suppress">
	<cfif Len(Variables.DspOptsSelList) IS 0>				<cfset Variables.DspOptsSel	= "selected">
	<cfelse>												<cfset Variables.DspOptsSel	= "        "></cfif>
	<cfoutput>
#Variables.DspOptsTabs#<option #Variables.DspOptsSel# value="">#Variables.DspOptsNotSelText#</option></cfoutput>
</cfif>

<cfloop query="#Variables.DspOptsQueryName#">
	<cfset Variables.DspOptsCode							= Evaluate("#DspOptsQueryName#.#DspOptsCodeName#")>
	<cfset Variables.DspOptsDesc							= Evaluate("#DspOptsQueryName#.#DspOptsDescName#")>
	<cfif ListFind(Variables.DspOptsSkipList, Variables.DspOptsCode) IS 0>
		<cfif ListFind(Variables.DspOptsSelList, Variables.DspOptsCode) GT 0>	<cfset Variables.DspOptsSel	= "selected">
		<cfelse>																<cfset Variables.DspOptsSel	= "        "></cfif>
		<cfoutput>
#Variables.DspOptsTabs#<option #Variables.DspOptsSel# value="#Variables.DspOptsCode#"><cfif Variables.DspOptsShowCode>#Variables.DspOptsCode# - </cfif>#Variables.DspOptsDesc#</option></cfoutput>
	</cfif>
</cfloop>

<cfif CGI.Script_Name IS "/library/cfincludes/dsp_options.cfm">
	<!--- Clean up generated Web page in self-test mode: --->
	<cfoutput>
		</select>
	</form>
	Use View Source to see the &lt;option&gt; tags indented 3 tabs deep. 
</body>
</html>
</cfoutput>
</cfif>

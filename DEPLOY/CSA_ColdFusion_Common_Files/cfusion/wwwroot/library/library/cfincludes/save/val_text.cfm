<!---
AUTHOR:				Steve Seaquist
DATE:				05/01/2002
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR150
NOTES:				This val file never returns an error, it's for fields that will be stored in text columns or repeating 
					rows in a varchar(255) column. Thus, there is no maximum size. Nevertheless, it preserves the structure 
					and methodology of the other val files. 
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	05/01/2002, SRS:	Original implementation.
--->
<CFSET Variables.FVal						= "">
<CFSET Variables.CVal						= "">
<CFIF IsDefined("Form.#Variables.FNam#")>
	<CFSET Variables.FVal					= Evaluate("Form.#Variables.FNam#")>
	<CFSET Variables.CVal					= Replace(Variables.FVal,	",", "", "ALL")>
	<CFIF Len(Variables.CVal) GT 0>
		<CFSET Variables.BlankScreen		= "No">
	</CFIF>
</CFIF>
<CFSET "Variables.#Variables.FNam#"			= Variables.FVal><!--- Little-known technique --->
<CFSET Variables.URLString					= Variables.URLString & "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

<!---
AUTHOR:				Steve Seaquist
DATE:				08/19/2005
DESCRIPTION:		Builds variables in the Request scope for use by cf_sbalookandfeel and cf_mainnav. 
NOTES:				<cfinclude template="get_sbalookandfeel_variables.cfm"> right after <cfapplication>. 
					See also: 
						/library/cfincludes/dsp_sbalookandfeel_variables.cfm (include to define form variables)
						/library/javascripts/SetSbalookandfeelVariables.js   (JavaScript to set form variables)
INPUT:				Variables in the Session and/or Form scopes. 
OUTPUT:
	Request scope variables prefixed with "Slaf" (short for "sbalookandfeel"), so as not to conflict with other variables 
	defined by the application. Possible changes to Session scope variables with the same names. 

	Variables scope variables: Always defines Variables.FormDataRecovered and Variables.PageName, which contains the current 
	page name, including ".cfm". If form data was automatically recovered, Variables.FormDataRecovered will be "Yes" and the 
	Variables scope will also contain data from the last Form scope saved by put_sbalookandfeel_saveformdata. But if 
	Variables.FormDataRecovered is "No", no form data was copied to the Variables scope. Since form data recovery is normally 
	used by the application, not by custom tags, "Slaf" is NOT prepended to form data recovery fields. 

REVISION HISTORY:	08/08/2011 -
					04/03/2012, SRS:	Defined Session.Slaf (per standards, actually) to hold all User Preferences. Moved 
										SlafButtons3D, SlafSuppressRTE, SlafTextOnly and (new variable) SlafHTML5 there. 
					06/10/2010, SRS:	Defined Request.SlafSuppressRTE for Section 508 compliance. Made Form Data Recovery 
										sensitive to Form.PathNames as well as Form.PageNames, so as to deal with conflicts 
										where page names are the same ("dsp_search.cfm" comes to mind). 
					03/22/2010, SRS:	Defined Request.SlafTopOfHead as a standard way for frames documents, report pages 
										and SBA Look-and-Feel itelf to establish standard, correct CSS and JS inclusions. 
										(Code is in get_sbalookandfeel_topofhead, but added a cfinclude to it here.) 
					09/25/2008, SRS:	Fixed J2EE Session Variable logic. (Previously, we didn't have it turned on on any of 
										our machines. Now that we do, the code finally could be tested and debugged.) 
					09/15/2008, SRS:	Defined URLServerShortName to eliminate case and ".sba.gov" from CGI.Server_Name 
										testing. Added "inile" (doesn't exist yet) and "iweb" to the list of server short 
										names that require https. 
					08/15/2008, IAC:	Added /sbtn/sbat to the directories that do not require GLS login. 
					06/12/2008, SRS:	Added Session.LastScriptName and Session.LastScriptTime. 
					05/09/2008, SRS:	Fixed setting of Variables.OrigUserId, added setting of Variables.OrigUsername. 
					03/22/2007, SRS:	Added /subnet to the GLSDirsNotRequiringGLSLogin exceptions list. 
					03/05/2007, SRS:	Fixed the checking of the GLSDirsNotRequiringGLSLogin exceptions list. 
					02/14/2007, SRS:	Added logging to Session Swap Detection and Prevention code, so that we can research 
										users who drop their sessions (due to cookie handling, perhaps). Removed old "pixel 
										counting on the server" code, now that sbalookandfeel.js does all pixel counting. 
					08/15/2006, SRS:	Created GLSDirsNotRequiringGLSLogin as a configuration variable. Added /ccr2pronet, 
										/pro-net and /pro-net/search to the list. 
					07/18/2006, SRS:	Removed SlafCFC code as part of an experiment. 
					07/17/2006, SRS:	Moved Variables.PageName code to get_sbashared_variables, so that it could set 
										Request.SlafCFC. Used SlafCFC to suppress session swap detection in Web Services. 
					07/05/2006, SRS:	Added "Session Swap Detection and Prevention" code. Also added a rudementary version 
										of "Must Be Logged In To Be In This Directory" code. It's rudementary in the sense 
										that it's not based on Session.ArrayUserRoles. 
					01/31/2006, SRS:	Moved Server scope logic to get_sbashared_variables.cfm. 
					12/16/2005, SRS:	Added SlafLogoWidth (for resizing left side elements). 
					11/16/2005, SRS:	Added determination of SlafLocalHost, SlafServerOsName and various associated 
										platform-related variables. 
					09/26/2005, SRS:	Added "automatic form data recovery". 
					08/19/2005, SRS:	Original implementation. 
--->

<!--- Configuration Variables: --->

<!---
Rationales for directories to be on the exceptions list: 
	/cls, /ccr2pronet, /eauth and /ldap:	You can't require the user to ALREADY be logged in, when the purpose of the 
											directory is to log the user in. 
	/dsbs, /pro-net, /sstory, tech-net:		The public isn't required to log in to use search functions. 
--->
<cfset Variables.CLSDirsNotRequiringGLSLogin				=  "/cls"
															& ",/ccr2pronet"
															& ",/eauth"
															& ",/ldap"
															& ",/dsbs"
															& ",/hubzone"
															& ",/hubzone/internet"
															& ",/hubzone/internet/general"
															& ",/pro-net"
															& ",/pro-net/search"
															& ",/sbtn/sbat"
															& ",/sstory"
															& ",/subnet"
															& ",/tech-net/docrootpages"
															& ",/tech-net/public">

<!--- Predefine Request scope variables that are not technically look-and-feel related, but which look-and-feel uses: --->

<cfinclude template="/library/cfincludes/get_sbashared_variables.cfm">

<!--- Predefine Request scope output variables, so that they'll be guaranteed to exist after calling this routine: --->

<cfset Request.SlafButtons3D								= "Yes">								<!--- User Preference! --->
<cfset Request.SlafClientHeight								= "">	<!--- "Slaf" = "sbalookandfeel". No longer in use. --->
<cfset Request.SlafClientWidth								= "">	<!--- "Slaf" = "sbalookandfeel". No longer in use. --->
<cfset Request.SlafDoFDR									= "No">	<!--- Internal use. "Slaf" avoids caller conflict. --->
<cfset Request.SlafHTML5									= "No">									<!--- User Preference! --->
<cfset Request.SlafHTML5									= (Request.SlafDevTestProd is not "Prod")><!--- User Preference! --->
<cfset Request.SlafLogoWidth								= "">	<!--- "Slaf" = "sbalookandfeel". No longer in use. --->
<cfset Request.SlafResolution								= "">	<!--- "Slaf" = "sbalookandfeel". No longer in use. --->
<cfset Request.SlafSessionControl							= "No">	<!--- Will be set to "ColdFusion" or "Java" if on. --->
<cfset Request.SlafSuppressRTE								= "No">									<!--- User Preference! --->
<cfset Request.SlafTextOnly									= "No">									<!--- User Preference! --->

<!--- Now go get their real values from Form, URL or Session scope (whichever exists), and maintain Session scope values. --->

<cfset Variables.ApplicationName							= "">
<cfset Variables.FormDataRecovered							= "No">
<cfset Variables.CLSAuthorized								= "No">
<cfset Variables.OrigUserId									= "">
<cfset Variables.SessionVariablesExist						= "No">
<cftry>
	<cflock scope="SESSION" type="READONLY" timeout="30">
		<cfif IsDefined("Session.SessionId")>
			<cfif Find("_", Session.SessionId) GT 0>
				<!--- CF session control has Session.SessionId format: appname_CFID_CFToken, so "_" would be found. --->
				<cfset Request.SlafSessionControl			= "ColdFusion">
				<cfset Variables.CFID						= Session.CFID>			<!--- Must exist. Crash if it doesn't. --->
				<cfset Variables.CFToken					= Session.CFToken>		<!--- Must exist. Crash if it doesn't. --->
				<cfif IsDefined("Session.JSessionId")>
					<cfset Variables.JSessionId				= Session.JSessionId>	<!--- Optional. --->
				</cfif>
			<cfelse>
				<!--- Java session control has (100% numeric) jsessionid as Session.SessionId, so "_" would NOT be found. --->
				<cfset Request.SlafSessionControl			= "Java">
				<cfset Variables.JSessionId					= Session.SessionId>	<!--- Session.JSessionId won't exist, only Session.SessionId. --->
				<cfif IsDefined("Session.CFID")>
					<cfset Variables.CFID					= Session.CFID>			<!--- Optional. --->
				</cfif>
				<cfif IsDefined("Session.CFToken")>
					<cfset Variables.CFToken				= Session.CFToken>		<!--- Optional. --->
				</cfif>
			</cfif>
			<cfset Variables.SessionVariablesExist			= "Yes">
			<cfif IsDefined("Session.ApplicationName")>
				<cfset Variables.ApplicationName			= Session.ApplicationName>
			</cfif>
			<cfif IsDefined("Session.OrigUserId")>
				<cfset Variables.OrigUserId					= Session.OrigUserId><!--- From IMUserTbl.IMUserNm --->
			</cfif>
			<cfif IsDefined("Session.OrigUsername")>
				<cfset Variables.OrigUsername				= Session.OrigUsername><!--- From IMSBACrdntlTbl.IMSBACrdntlLognNm --->
			</cfif>
			<cfif IsDefined("Session.CLSAuthorized")>
				<cfset Variables.CLSAuthorized				= Session.CLSAuthorized>
			</cfif>
			<cfif IsDefined("Session.Slaf") and IsStruct(Session.Slaf)>
				<cfset StructAppend(Request, Session.Slaf, "Yes")><!--- "Yes" = overwrite defaults, set above. --->
			</cfif>
			<!---
			*** PART ONE OF AUTOMATIC FORM DATA RECOVERY. *** Automatic form data recovery is done in two places. 
			This one is in a readonly lock where we only retrieve saved form data into the Variables scope. 
			The other is in an exclusive lock where we're both saving and loading data into the Variables scope. 
			If Form.PageNames is defined, we don't want to do the recovery in the readonly cflock. In other words: 

				"FORM DATA RECOVERY RECOVERS THE CONTENTS OF THE LAST POST THAT DEFINED FORM.PAGENAMES."

			Example in DSBS: dsp_search posts to dsp_profilelist which repeatedly posts to itself over and over again 
			to retrieve paged results. The only thing passed between paged results (profiles 1-25, profiles 26-50, etc) 
			is Form.StartRow. For all other search criteria, dsp_profilelist recovers the data posted by dsp_search. 
			All that dsp_profilelist has to do is NOT define all of the dsp_search fields (and in particular, NOT define 
			PageNames). This trick saves us an enoromous amount of hidden form field maintenance in paged result pages. 
			--->
			<cfif NOT IsDefined("Form.PageNames")>
				<!--- New Rule: In addition to matching PageNames, the path has to also match PathNames (if defined): --->
				<cfif IsDefined("Session.SlafSaveFormData.PathNames")>
					<cfif ListFind(Session.SlafSaveFormData.PathNames, Request.SlafPathName) GT 0>
						<cfif IsDefined("Session.SlafSaveFormData.PageNames")>
							<cfif ListFind(Session.SlafSaveFormData.PageNames, Request.SlafPageName) GT 0>
								<cfset Request.SlafDoFDR	= "Yes"><!--- Both page and path match. --->
							</cfif>
						<cfelse>
							<cfset Request.SlafDoFDR		= "Yes"><!--- Only path needed to match, and it does. --->
						</cfif>
					</cfif>
				<cfelseif IsDefined("Session.SlafSaveFormData.PageNames")>
					<cfif ListFind(Session.SlafSaveFormData.PageNames, Request.SlafPageName) GT 0>
						<cfset Request.SlafDoFDR			= "Yes"><!--- Only page needed to match, and it does. --->
					</cfif>
				</cfif>
				<cfif Request.SlafDoFDR>
					<cfinclude template="get_sbalookandfeel_saveformdata_nolock.cfm">
					<cfset Variables.FormDataRecovered		= "Yes">
				</cfif>
			</cfif>
		</cfif>
	</cflock>
	<cfcatch type="Any">
		<!---
		Do nothing. If we can't lock the Session scope, that just means that there ain't no Session scope. 
		<cfoutput>#CFCatch.Message#<br>#CFCatch.Detail#</cfoutput><cfabort>
		--->
	</cfcatch>
</cftry>

<cfif IsDefined("URL.SlafToggleTextOnly") OR IsDefined("Form.SlafToggleTextOnly")>
	<!--- This could be the page request that toggles graphics/textonly modes. (Will be saved later in exclusive lock.) --->
	<cfset Request.SlafTextOnly								= (NOT Request.SlafTextOnly)>
</cfif>

<!--- Start of "Session Swap Detection and Prevention" code. --->

<!---
<cfoutput>#Cookie.JSessionId#</cfoutput>
<cfdump var="#Cookie#" label="Cookie">
<cfdump var="#Variables#" label="Variables">
<cfdump var="#Request#" label="Request"><cfabort>
If cookies are not defined, set them. Note that none of this will be done if the appropriate cookies ARE defined: 
--->

<cfif	Variables.SessionVariablesExist
AND	(	((Request.SlafSessionControl IS "ColdFusion")	AND ((NOT IsDefined("Cookie.CFID")) OR (NOT IsDefined("Cookie.CFToken"))))
	OR	((Request.SlafSessionControl IS "Java")			AND (NOT IsDefined("Cookie.JSessionId")))
	)>
	<cfset Variables.FirstKeyCopied							= "No">
	<cfset Variables.ReturnURL								= Request.SlafURLProtocolServerAndScriptName>
	<cfset Variables.SessionTokensStripped					= "No">
	<cfset Variables.TriedToSetCookies						= "No">
	<cfif Len(CGI.Query_String) GT 0>
		<cfloop index="Key" list="#LCase(StructKeyList(URL))#">
			<cfswitch expression="#Key#">
			<cfcase value="cfid,cftoken,jsessionid">
				<cfset Variables.SessionTokensStripped		= "Yes">
			</cfcase>
			<cfcase value="cleaningpass">
				<cfif Evaluate("URL.#Key#") IS "Cookies">
					<cfset Variables.TriedToSetCookies		= "Yes">
				</cfif>
			</cfcase>
			<cfdefaultcase>
				<cfset URLEncodedValue						= URLEncodedFormat(Evaluate("URL.#Key#"))>
				<cfif Variables.FirstKeyCopied>
					<cfset Variables.ReturnURL				= "#Variables.ReturnURL#&#Key#=#URLEncodedValue#">
				<cfelse>
					<cfset Variables.ReturnURL				= "#Variables.ReturnURL#?#Key#=#URLEncodedValue#">
					<cfset Variables.FirstKeyCopied			= "Yes">
				</cfif>
			</cfdefaultcase>
			</cfswitch>
		</cfloop>
	</cfif>
	<cfif Variables.TriedToSetCookies><!--- Situation 3 --->
		<!--- Whenever we have to take a session establishment action, log it (if logging is on at the trace level): --->
		<cfset Variables.SleSessionTraceAction				= "no cookies"><!--- Destinguishes this call from others. --->
		<cfinclude template="/library/cfincludes/log_SleSessionTrace.cfm">
		<cfset Variables.WindowTitle						= "SBA - Could Not Set Session Cookies">
		<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>#Variables.WindowTitle#</title></head>
<body bgcolor="##ffffff">
<h3 align="center">#Variables.WindowTitle#</h3>

<p>
Your attempt to go to <a href="#Variables.ReturnURL#">#Variables.ReturnURL#</a> failed. 
Usually this is because your browser does not have &quot;Session Cookies&quot; enabled. 
It can also occur if you have set your browser to prompt you, and you've chosen to disallow the cookie(s) requested. 
In either case, you must allow the SBA to set Session Cookies to use SBA applications. 
</p>

<p>
Please enable Session Cookies, then try again using the hotlink above. 
</p>

</body>
</html>
</cfoutput>
		<cfabort>
	<cfelseif Variables.SessionTokensStripped><!--- Situation 1: --->
		<cfset Variables.CleaningPassString					= "CleaningPass=URL">
		<cfif Variables.FirstKeyCopied>
			<cfset Variables.ReturnURL						= "#Variables.ReturnURL#&#Variables.CleaningPassString#">
		<cfelse>
			<cfset Variables.ReturnURL						= "#Variables.ReturnURL#?#Variables.CleaningPassString#">
			<cfset Variables.FirstKeyCopied					= "Yes">
		</cfif>
		<cfset Variables.WindowTitle						= "SBA - Allocating A New Session - Stalled">
	<cfelse><!--- Situation 2: --->
		<!--- Since session tokens were not stripped from the incoming URL, we can trust Session.CFID, etc: --->
		<cfif Request.SlafSessionControl IS "Java">
			<cfcookie name="JSessionId"						value="#Variables.JSessionId#">
			<cfif IsDefined("Variables.CFID")>
				<cfcookie name="CFID"						value="#Variables.CFID#">
			</cfif>
			<cfif IsDefined("Variables.CFToken")>
				<cfcookie name="CFToken"					value="#Variables.CFToken#">
			</cfif>
		<cfelse>
			<cfcookie name="CFID"							value="#Variables.CFID#">
			<cfcookie name="CFToken"						value="#Variables.CFToken#">
			<cfif IsDefined("Variables.JSessionId")>
				<cfcookie name="JSessionId"					value="#Variables.JSessionId#">
			</cfif>
		</cfif>
		<cfset Variables.CleaningPassString					= "CleaningPass=Cookies">
		<cfif Variables.FirstKeyCopied>
			<cfset Variables.ReturnURL						= "#Variables.ReturnURL#&#Variables.CleaningPassString#">
		<cfelse>
			<cfset Variables.ReturnURL						= "#Variables.ReturnURL#?#Variables.CleaningPassString#">
			<cfset Variables.FirstKeyCopied					= "Yes">
		</cfif>
		<cfset Variables.WindowTitle						= "SBA - Setting Cookies - Stalled">
	</cfif>
	<!--- Whenever we have to take a session establishment action, log it (if logging is on at the trace level): --->
	<cfset Variables.SleSessionTraceAction					= Variables.CleaningPassString><!--- Destinguishes this call from others. --->
	<cfinclude template="/library/cfincludes/log_SleSessionTrace.cfm">
	<!--- Reputedly, you can't set a cookie and then do a cflocation, but a manually coded 302 redirect can, so: --->
	<!--- Redirect for situations 1 and 2: --->
	<cfheader name="Location"								value="#Variables.ReturnURL#">
	<cfheader statuscode="302"								statustext="Moved Temporarily">
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>#Variables.WindowTitle#</title>
<meta http-equiv="Refresh" content="0;URL=#Variables.ReturnURL#"></head>
<body bgcolor="##ffffff" onLoad="top.location.href = '#Variables.ReturnURL#';">
<h3 align="center">#Variables.WindowTitle#</h3>

If you are not immediately redirected, follow <a href="#Variables.ReturnURL#">this hotlink</a>. 

</body>
</html>
</cfoutput>
	<cfabort>
</cfif>

<!--- If control gets here, the cookies, if meant to exist, exist. Just one last bit of cleanup, however: --->

<cfif	Variables.SessionVariablesExist
	AND	IsDefined("URL.CleaningPass")><!--- Situation 4: --->
	<cfset Variables.FirstKeyCopied							= "No">
	<cfset Variables.ReturnURL								= Request.SlafURLProtocolServerAndScriptName>
	<cfloop index="Key" list="#LCase(StructKeyList(URL))#">
		<cfswitch expression="#Key#">
		<cfcase value="cfid,cftoken,jsessionid,cleaningpass">
			<!--- Ignore (don't copy to ReturnURL). --->
		</cfcase>
		<cfdefaultcase>
			<cfset URLEncodedValue							= URLEncodedFormat(Evaluate("URL.#Key#"))>
			<cfif Variables.FirstKeyCopied>
				<cfset Variables.ReturnURL					= "#Variables.ReturnURL#&#Key#=#URLEncodedValue#">
			<cfelse>
				<cfset Variables.ReturnURL					= "#Variables.ReturnURL#?#Key#=#URLEncodedValue#">
				<cfset Variables.FirstKeyCopied				= "Yes">
			</cfif>
		</cfdefaultcase>
		</cfswitch>
	</cfloop>
	<!--- Whenever we have to take a session establishment action, log it (if logging is on at the trace level): --->
	<cfset Variables.SleSessionTraceAction					= "get rid of CleaningPass on URL"><!--- Destinguishes this call from others. --->
	<cfinclude template="/library/cfincludes/log_SleSessionTrace.cfm">
	<!--- Reputedly, you can't set a cookie and then do a cflocation, but a manually coded 302 redirect can, so: --->
	<!--- Redirect for situation 4: --->
	<cfheader name="Location"								value="#Variables.ReturnURL#">
	<cfheader statuscode="302"								statustext="Moved Temporarily">
	<cfset Variables.WindowTitle							= "SBA - Final Redirection - Stalled">
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>#Variables.WindowTitle#</title>
<meta http-equiv="Refresh" content="0;URL=#Variables.ReturnURL#"></head>
<body bgcolor="##ffffff" onLoad="top.location.href = '#Variables.ReturnURL#';">
<h3 align="center">#Variables.WindowTitle#</h3>

If you are not immediately redirected, follow <a href="#Variables.ReturnURL#">this hotlink</a>. 

</body>
</html>
</cfoutput>
	<cfabort>
</cfif>

<!--- End of "Session Swap Detection and Prevention" code. --->

<!--- Start of "Must Be Logged In To Be In This Directory" code. --->

<cfif	Variables.SessionVariablesExist
	AND	(Len(Variables.ApplicationName) IS 0)>
	<!--- In order for the Session scope to exist, the Application scope MUST exist, so no need for cftry/cfcatch: --->
	<cflock scope="APPLICATION" type="READONLY" timeout="30">
		<cfif IsDefined("Application.ApplicationName")>		<!--- Set by <cfapplication> tag's name attribute. --->
			<cfset Variables.ApplicationName				= Application.ApplicationName>
		<cfelseif IsDefined("Application.Name")>			<!--- Set by application.cfc, if used. --->
			<cfset Variables.ApplicationName				= Application.Name>
		</cfif>
	</cflock>
	<cfif Len(Variables.ApplicationName) GT 0>
		<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
			<cfset Session.ApplicationName					= Variables.ApplicationName>
		</cflock>
	</cfif>
</cfif>
<cfset Request.SlafApplicationName							= Variables.ApplicationName><!--- For custom tags. --->
<cfif	(Variables.ApplicationName IS "CLS")
	AND	((NOT IsDefined("Variables.CLSAuthorized")) OR (NOT Variables.CLSAuthorized))>
	<cfif Find("/", CGI.Script_Name, 2) GT 0>				<!--- Page has a directory in its path: --->
		<cfset Variables.InExceptionDirectory				= "No">
		<cfloop index="ExcDir" list="#Variables.CLSDirsNotRequiringGLSLogin#">
			<cfset ExcDirLen								= Len(ExcDir)>
			<cfif	(Len	(CGI.Script_Name)				GE ExcDirLen)
				AND	(Left	(CGI.Script_Name, ExcDirLen)	IS ExcDir)>
				<cfset Variables.InExceptionDirectory		= "Yes">
				<cfbreak>
			</cfif>
		</cfloop>
		<cfif NOT Variables.InExceptionDirectory>
			<cfset Variables.ErrMsg							= "You must be logged into GLS to go to the page just requested.">
			<cflocation url="/cls/dsp_mustlogin.cfm?ErrMsg=#URLEncodedFormat(Variables.ErrMsg)#" addtoken="No">
		</cfif>
	</cfif>
</cfif>

<!--- End of "Must Be Logged In To Be In This Directory" code. --->

<cfif Variables.SessionVariablesExist>
	<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
		<cfif NOT IsDefined("Session.Slaf")>
			<cfset Session.Slaf								= StructNew()>
		</cfif>
		<cfset Session.Slaf.SlafButtons3D					= Request.SlafButtons3D>
		<cfset Session.Slaf.SlafHTML5						= Request.SlafHTML5>
		<cfset Session.Slaf.SlafSuppressRTE					= Request.SlafSuppressRTE>
		<cfset Session.Slaf.SlafTextOnly					= Request.SlafTextOnly>
		<!---
		*** PART TWO OF AUTOMATIC FORM DATA RECOVERY. *** Automatic form data recovery is done in two places. 
		See part one in the readonly session lock above for a more detailed explanation and example usage. 
		--->
		<cfset Request.SlafDoFDR							= "No"><!--- Reset to "No" in case set to "Yes" in part one. --->
		<cfif IsDefined("Form.PageNames")>
			<cfinclude template="put_sbalookandfeel_saveformdata_nolock.cfm">
			<!--- New Rule: In addition to matching PageNames, the path has to also match PathNames (if defined): --->
			<cfif IsDefined("Session.SlafSaveFormData.PathNames")>
				<cfif ListFind(Session.SlafSaveFormData.PathNames, Request.SlafPathName) GT 0>
					<cfif IsDefined("Session.SlafSaveFormData.PageNames")>
						<cfif ListFind(Session.SlafSaveFormData.PageNames, Request.SlafPageName) GT 0>
							<cfset Request.SlafDoFDR		= "Yes"><!--- Both page and path match. --->
						</cfif>
					<cfelse>
						<cfset Request.SlafDoFDR			= "Yes"><!--- Only path needed to match, and it does. --->
					</cfif>
				</cfif>
			<cfelseif IsDefined("Session.SlafSaveFormData.PageNames")>
				<cfif ListFind(Session.SlafSaveFormData.PageNames, Request.SlafPageName) GT 0>
					<cfset Request.SlafDoFDR				= "Yes"><!--- Only page needed to match, and it does. --->
				</cfif>
			</cfif>
			<cfif Request.SlafDoFDR>
				<cfinclude template="get_sbalookandfeel_saveformdata_nolock.cfm">
				<cfset Variables.FormDataRecovered			= "Yes">
			</cfif>
		</cfif>
		<cfset Session.LastScriptName						= CGI.Script_Name>
		<cfset Session.LastScriptTime						= Request.SlafDateTimeNowDisplay>
	</cflock>
</cfif>

<!--- Make it easier for pages in frames to support standard CSS stylesheets and jQuery properly: --->

<cfinclude template="/library/cfincludes/get_sbalookandfeel_topofhead.cfm">

<!--- The following is for testing and debugging. If this file is called directly (not cfincluded), dump what we did: --->

<cfif CGI.Script_Name IS "/library/cfincludes/get_sbalookandfeel_variables.cfm">
	<cfoutput><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en-US"><head><title>get_sbalookandfeel_variables.cfm</title></head>
<body bgcolor="##ffffff">

<p>Variables scope: <cfdump var="#Variables#"></p>
<p>Request scope: <cfdump var="#Request#"></p>

</body>
</html>
</cfoutput>
</cfif>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) LT 8>
		<cfset Variables.ErrMsg					= "#Variables.ErrMsg# <li>#Variables.FName# must be at least 8 characters.</li>">
	<cfelseif Len(Variables.FVal) GT 30>
		<cfset Variables.ErrMsg					= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 30 characters.</li>">
	<cfelse>
		<cfset Variables.AllPrintable			= "Yes">
		<cfset Variables.AllSpaces				= "Yes"><!--- Prevented in get_FVal, but test in case get_FVal changed. --->
		<cfloop index="i" from="1" to="#Len(Variables.FVal)#">
			<cfset Variables.FValCh				= Asc(Mid(Variables.FVal, i, 1))>
			<cfif (Variables.FValCh LT 32) OR (Variables.FValCh GT 126)>
				<cfset Variables.AllPrintable	= "No">
				<cfbreak>
			<cfelseif Variables.FValCh GT 32>
				<cfset Variables.AllSpaces		= "No">
			</cfif>
		</cfloop>
		<cfif NOT Variables.AllPrintable>
			<cfset Variables.ErrMsg				= "#Variables.ErrMsg# <li>#Variables.FName# must be composed entirely of "
												& "printable, unaccented characters. "
												& "Character #i# (&quot;#Chr(Variables.FValCh)#&quot;) is not allowed.</li>">
		<cfelseif Variables.AllSpaces>
			<cfset Variables.ErrMsg				= "#Variables.ErrMsg# <li>#Variables.FName# cannot be all spaces.</li>">
		</cfif>
	</cfif>
</cfif>

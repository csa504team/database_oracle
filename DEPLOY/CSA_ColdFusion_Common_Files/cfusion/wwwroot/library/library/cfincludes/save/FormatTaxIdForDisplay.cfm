<!---
AUTHOR:				Steve Seaquist
DATE:				10/07/2000
DESCRIPTION:		Reformats Variables.Temp from database format to human format
NOTES:				None
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	05/07/2002, SRS:	Eliminated references to Variables.TaxId in favor of Variables.Temp, as originally 
										intended/designed. 
					12/15/2000, SRS:	Added this comment header. 
					10/07/2000, SRS:	Original implementation
--->
<CFIF Len(Variables.Temp) IS 10>
	<CFIF		Left(Variables.Temp,1) IS Variables.PrefixEIN>
		<CFSET Variables.Temp							= Mid(Variables.Temp,2,2) 
														& "-" 
														& Right(Variables.Temp,7)>
	<CFELSEIF	Left(Variables.Temp,1) IS Variables.PrefixSSN>
		<CFSET Variables.Temp							= Mid(Variables.Temp,2,3) 
														& "-" 
														& Mid(Variables.Temp,5,2) 
														& "-" 
														& Right(Variables.Temp,4)>
	<CFELSE>
		<CFSET Variables.Temp							= Right(Variables.Temp, 9)>
	</CFIF>
</CFIF>

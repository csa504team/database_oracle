<!---
AUTHOR:				Steve Seaquist
DATE:				04/28/2006
DESCRIPTION:		Makes sure that all "server cached queries" ("Scq") variables are defined in the Server scope. 
NOTES:

	DIRECTLY REFERENCING THIS ROUTINE OR THE SERVER.SCQ STRUCTURE ARE NOW ***** DEPRECATED *****. 

	Instead, you should be using /library/udf/bld_CachedQueryUDFs.cfm. For example: 

		<cfif NOT IsDefined("GetCachedQuery")>
			<cfinclude template="/library/udf/bld_CachedQueryUDFs.cfm">
		</cfif>
		<cfset getStates						= GetCachedQuery("ActvStTbl")>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use datasources with generic logins that have select permissions, you cannot call the cached query UDFs 
	inside a cftransaction that uses a different datasource or login. Therefore, it's better to call GetCachedQuery or 
	GetCachedQueries near the beginning of your CFM file, among its initializations. 

	The cached query UDFs are currently stepping stones to get the data out of the Server scope. But they won't always be. 
	Once all references to the Server.Scq structure have been removed from people's pages, we will be able to use more 
	modern, less memory-wasteful ways of caching queries. That's why we want to get rid of all references to Server.Scq. 

	The tech buzzword for recoding to change how we do things is "refactoring". We're refactoring how we chache queries. 

HISTORICAL NOTES:

	Cfinclude this file before attempting to retrieve Scq variables from the Server scope. 

	Unlike GetCachedQuery and GetCachedQueries, this is a "build" routine, not a "get" routine. All it does is build server 
	cached queries in the Server scope, and only if necessary. It does not actually copy Scq variables into the Variables 
	or Request scope. It just makes sure they're defined so that the CachedQueryUDFs can copy them into the Variables or 
	Request scope, according to how they're called, because only you know which queries you actually need. 

	The purpose of the "Scq" prefix (as with the Slaf prefix in SBA look-and-feel) is to avoid naming conflicts with your 
	own variables. When you copy them into the Variables or Request scope, you can keep their names the same (to show their 
	origin) or rename them (to make your code more readable). In the following example, Server.Scq.ActvStTbl is renamed to 
	[Variables.]getStates for readability: 

		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cflock scope="SERVER" type="READONLY" timeout="30">
			<cfset getStates					= Server.Scq.ActvStTbl>
		</cflock>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use datasources with generic logins that have select permissions, you cannot include this file inside 
	a cftransaction that uses a different datasource or login. But it doesn't really have to be close to the code that 
	locks the Server scope. Just include this file at the beginning of your CFM file, among its initializations. 

	In the /library/cfincludes/cachedqueries/qry_*.cfm files, note that code and description columns are generally 
	available in 2 ways: (1) as their actual database column names, and (2) as "code" and "description". If you don't 
	want to use "code" and "description" (because they aren't unique across queries), then don't use them. No harm done. 
	They exist for use by shared routines (most notably, /library/cfincludes/dsp_options.cfm for drop-down menus). 

	Like numerous other library routines, browsing directly to this file in a browser triggers "self-test mode". Self-test 
	mode should NOT be used to refresh the cached queries, however, because it executes only on the instance you happen to 
	end up on. (The other instances don't get their cached queries refreshed.) To refresh all server cached queries in all 
	instances, touch the /library/cfincludes/flg_ServerCachedQueries.cfm file instead. "Director watchers" in each of the 
	instances will respond to the date/timestamp change by calling this file to rebuild the queries. ColdFusion Admins 
	have a CF/Logging Admin screen that will do this correctly on each of the servers. 

INPUT:				None. 
OUTPUT:				Server scope structure "Scq" (short for "server cached queries"), so as not to conflict with other 
					variables defined by the application. If the current user happens to be the one who takes the I/O hit 
					to define the queries, the "Scq" variables will also be defined in the Variables scope. Generally, 
					this will only happen when this routine is executed on its own as a Scheduled Task. 
REVISION HISTORY:	12/17/2010, SRS:	Moved queries into separate files of /library/cfincludes/cachedqueries. In the 
										process, got rid of all stored procedure calls and queries of queries. (SPCs define 
										ErrMsg and TxnErr, which can interfere with the caller of this file. And queries of 
										queries are the slowest thing ColdFusion does other than cfdump.) Allowed for minimal 
										rebuilds of only certain queries via new Variables.ServerCachedQueryNames feature. 
					09/09/2010, SRS:	Converted to use SlafHead and SlafTopOfHead to pick up correct CSS file sequence 
										Now that several weeks have passed, delete 2 queries that Ian had commented out. 
					08/26/2010, IAC:	Two TechNet tables have been dropped from the database. Commented them out from here. 
										Steve can remove the code later.
					06/16/2010, SRS:	Added Variables.cfpra to our save-and-restore code, with StructDelete, so that it 
										won't mess up our SPC calls if it's defined. 
					04/07/2010, NNI:	Added AllBusTypTbl, AllCitznshipCdTbl, AllEthnicCdTbl, AllGndrTbl, AllIMCrdtScorSourcTblBus, 
										AllIMCrdtScorSourcTblPer, AllIMEPCOperCdTbl, AllLoanStatCmntCdTbl, AllVetTbl
					04/07/2010, NNI:	Added sbaref.OutlawTbl.
					03/21/2010, NNI:	Replaced oracle_analytic_publicread to use oracle_transaction_publicread after dsn was modified on server.
					03/09/2010, NNI:	Removed CohortCd from ActvPrgrmValidTbl. 
					02/01/2010, NNI:	Modified LoanStatCdTbl to add ColsonStatCd column. 
					12/10/2009, NNI:	Modified PrcsMthdTbl and PrgrmValidTbl generation to account for changes to table stucture. 
					09/10/2009, SRS:	Added Scq.pronet.CbtLists.QMS (Quick Market Search). 
					07/10/2009, NNI:	Added ActvACHAcctTypTbl.
					06/09/2009, SRS:	Added lists based on ActvPrcsMthdTbl (similar to those built in ActvCCRBusTypCdTbl). 
										Improved building of lists, so that they won't bleed over between cfcases. 
					06/04/2009, NNI:	Added AllAppropIndTbl and three new columns to ActvPrcsMthdTbl and AllPrcsMthdTbl. 
					03/04/2009, SRS:	Added pronet.ActvCCRBusTypCdTbl and pronet.ActvIMMinCdTbl. 
					02/19/2009, SRS:	Added a "cascading forced rebuild" capability. Specifically, if the caller is forcing a 
										rebuild of Server.Scq, we pass that forced rebuild along to get_sbashared_variables, 
										which will in turn cascade to a forced rebuild of get_actual_server_name. 
					02/09/2009, NNI:	Added PrgrmCd to the Processing Method tables.
					02/05/2009, SRS:	In self-test mode, eliminated the cfdump unless specifically requested by "?CFDump=Yes". 
					01/25/2009, SRS:	Got rid of special test for eweb1 server names which was messing up in the case 
										of niagara (new eweb1 server). 
					01/25/2009, SRS:	Changed over partner to use Oracle, because PROD1's hard drive got hosed. 
					11/26/2008, SRS:	Redid server-related queries using real Sybase tables. (Sybase only for now.) 
					10/24/2008, SRS:	Added server-related queries, first as pseudo-tables. 
					10/14/2008, SRS:	Added technet structure to amazon/missouri for the TECH-Net release. 
					09/16/2008, SRS:	Added tests for "colorado" and "redriver". 
					05/29/2008, SRS:	Added AllSpcPurpsLoanTbl. 
					04/30/2008, SRS:	Added support for "COOP testing", which has only Sybase available to it. This meant 
										making EVERY request dual Sybase/Oracle. 
					02/21/2008, SRS:	Added support for the Scq.technet substructure and defined 9 new queries to be stored 
										there. Alphabetized all substructures for easy lookup. Reinstated earlier version's 
										"save and restore" variables, plus some extras, so that this file will actually use all 
										of the official, standard names (db, dbtype, ErrMsg, etc). This allows use of SPC files 
										and /library/udf/bld_dbutils_for_dbtype.cfm, if necessary. Defined OracleSchemaName 
										(another official, standard name) and based its value on the current substructure name 
										(partner, pronet, technet or none). This allows queries to be done in any order, 
										not neccesarily grouped by substructure. But mainly, it allows rapidly changing the 
										OracleSchemaName, as we will have to do soon with the advent of viewtechnet. 
					01/09/2008, NNI:	Made changes to code to point to Sybase db server in case of partner database calls. 
					01/07/2008, NNI:	Made changes to code to point to Oracle db servers. public_sbaref, pronet and partner datasources 
										have been changed to oracle_transaction_publicread. 
					08/20/2007, DKC:	Added support for partner database cached queries (replaces static html file used in PIMS). 
					06/04/2007, DKC:	Added VetGrpCd to the Scq.ActvVetTbl.
					04/25/2007, SRS:	Per Dileep and Sheri, saved and restored db and dbtype (if pronet is being built). 
					04/04/2007, SRS:	Per Sheri McConville, added ActvSBAOfcTbl result set. Also allowed for AllSBAOfcTbl 
										result set, but because it had 430 rows, removed it from ScqAvailableResultSetNames. 
										(That effectively comments it out / doesn't cache the query.) Added StrtDt and EndDt 
										to all previously-defined "All" result sets, if they have those columns. (It was an 
										oversight not to have included them in the first place.) 
					03/28/2007, SRS:	Per Ron Whalen, added AllPrcsMthdTbl and AllPrgrmTbl result sets for loan servicing. 
					12/07/2006, SRS:	Restricted Scq.pronet loads to explicitly named servers. (Previously, it was loaded 
										by default on all servers except devyesapp and yesapp.) 
					10/03/2006, SRS:	Special exception for Scq.pronet.ActvFrmlDtrmntnCtgryTbl extended to enile.
					09/27/2006, DKC:	Allowed it to be directly executed out of stagelibrary and build the same display.
					09/14/2006, DKC:	Added support for new table IMDomnTypTbl.
					08/11/2006, SRS:	Added support for pronet database cached queries (formerly cached in the Application 
										scope of PRO-Net when PRO-Net was not part of GLS). Also added "new logging". 
					06/14/2006, SRS:	Added 32 new cached query types. Allowed controlling what queries get defined 
										by controlling ScqAvailableResultSetNames. Per Ron Whalen, don't include Scq.ActvCMSACdTbl 
										in ScqAvailableResultSetNames, because it appear not to be in use. 
					05/12/2006, SRS:	Added IMCntryDialCd to ScqActvCountries, for use with dsp_IMCntryCdToIMCntryDialCd. 
					04/28/2006, SRS:	Original implementation. Used cfqueries initially, knowing that we will someday 
										rewrite it to use a stored procedure call (SPC) file. That's why there isn't any 
										cftry/cfcatch code. That'll be automatically added when we call the SPC file. 
--->

<!--- Initializations --->

<cfset Variables.ServerCachedQueriesCFDumpMode						= "No">
<cfset Variables.ServerCachedQueriesSelfTestMode					=
	(CGI.Script_Name IS "/library/cfincludes/bld_ServerCachedQueries.cfm")>
<cfparam name="Variables.ServerCachedQueriesForceRebuild"			default="No"><!--- Allows rebuild even if not self test. --->
<cfparam name="Variables.ServerCachedQueryNames"					default="">
<cfif Variables.ServerCachedQueriesSelfTestMode>
	<cfset Variables.ServerCachedQueriesForceRebuild				= "Yes">
	<cfif IsDefined("URL.CFDump") AND (URL.CFDump IS "Yes")>
		<cfset Variables.ServerCachedQueriesCFDumpMode				= "Yes">
	</cfif>
	<cfif IsDefined("URL.ServerCachedQueryNames")>
		<cfset Variables.ServerCachedQueryNames						= URL.ServerCachedQueryNames>
	</cfif>
</cfif>
<cfif Variables.ServerCachedQueriesForceRebuild>
	<cfset Variables.SbaSharedVariablesForceRebuild					= "Yes">
	<cfinclude template="get_sbashared_variables.cfm"><!--- This will cascade to get_actual_server_name too. --->
	<cfif Variables.ServerCachedQueriesCFDumpMode>
		<cf_setrequesttimeout seconds="180"><!--- CFDump takes FREAKISHLY long. --->
	</cfif>
</cfif>

<!--- First, check the master variable that defines whether or not all the others are defined: --->

<cfif NOT IsDefined("Variables.PageName")>
	<cfset Variables.PageName										= ListGetAt(CGI.Script_Name,
																		ListLen(CGI.Script_Name,"/"),
																								"/")>
</cfif>
<!---
A cflock name="something" lock is very close to being a Server scope lock, but it's not affected by named locks that use 
other names. It's unlikely, but it COULD make a difference if someone else is accessing other unrelated stuff in the Server 
scope (get_sbashared_variables, for example). Named locks hang only on references to the same name. For this reason, we use 
the name="ServerCachedQueries" for all cflocks in this file: 
--->
<cflock name="ServerCachedQueries" type="READONLY" timeout="30">
	<cfset Variables.ScqIsDefined									= IsDefined("Server.Scq")>
</cflock>
<!---
If we're forcing a rebuild, and Server.Scq doesn't exist, we cannot restrict the forced rebuild to only a few queries. 
Otherwise, we would end up with a Server.Scq structure that doesn't contain all queries, and callers would crash trying 
to get queries that don't exist. This also prevents a crash in the exclusive cflock, below: 
--->
<cfif	Variables.ServerCachedQueriesForceRebuild
	and	(NOT Variables.ScqIsDefined)
	and	(Len(Variables.ServerCachedQueryNames)						gt 0)>
	<cfset Variables.ServerCachedQueryNames							= "">
</cfif>


<cfif (NOT Variables.ScqIsDefined) or Variables.ServerCachedQueriesForceRebuild>
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqStartTickCount							= GetTickCount()>
	</cfif>
	<cfif NOT IsDefined("Request.SlafServerCachedQueryGroups")>
		<cfinclude template="get_sbashared_variables.cfm">
	</cfif>
	<cfinclude template="/library/cfincludes/cachedqueries/bld_ScqNamespace.save.cfm">
	<cfdirectory
		action														= "LIST"
		directory													= "#ExpandPath('/library/cfincludes/cachedqueries')#"
		filter														= "qry_*.cfm"
		name														= "Variables.ScqGetFiles"
		sort														= "name">
	<cfloop query="Variables.ScqGetFiles">
		<cfset Variables.ScqQueryName								= Replace(Replace(Variables.ScqGetFiles.name,
																		"qry_","","ONE"),
																		".cfm","","ONE")>
		<cfif ListLen(Variables.ScqQueryName, ".") gt 1>
			<cfset Variables.ScqSubstructureName					= ListGetAt(Variables.ScqQueryName, 1, ".")>
			<cfif ListFind(Request.SlafServerCachedQueryGroups, Variables.ScqSubstructureName) is 0>
				<cfcontinue><!--- CF 9 --->
			</cfif>
		<cfelse>
			<cfset Variables.ScqSubstructureName					= "">
			<!--- If there isn't a substructure name, "sbaref" is implied: --->
			<cfif ListFind(Request.SlafServerCachedQueryGroups, "sbaref") is 0>
				<cfcontinue><!--- CF 9 --->
			</cfif>
		</cfif>
		<cfif	(Len		(Variables.ServerCachedQueryNames)								gt 0)
			and	(ListFind	(Variables.ServerCachedQueryNames,		Variables.ScqQueryName)	is 0)>
				<cfcontinue><!--- CF 9 --->
		</cfif>
		<cfinclude template="/library/cfincludes/cachedqueries/qry_#Variables.ScqQueryName#.cfm">
	</cfloop>
	<!--- Save Variables.Scq to the Server scope if we got here without crashing. --->
	<cflock name="ServerCachedQueries" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq.LastUpdateOfThisStructure")>
			<cfset Variables.Scq["PrevUpdateOfThisStructure"]		= Server.Scq.LastUpdateOfThisStructure>
		<cfelse>
			<cfset Variables.Scq["PrevUpdateOfThisStructure"]		= "N/A">
		</cfif>
	</cflock>
	<cfset Variables.Scq["LastUpdateOfThisStructure"]				= DateFormat(Now(), "YYYY-MM-DD")
																	& " "
																	& TimeFormat(Now(), "HH:mm:ss")>
	<cflock name="ServerCachedQueries" type="EXCLUSIVE" timeout="30">
		<cfif Len(Variables.ServerCachedQueryNames) gt 0><!--- Rebuilding just a subset? --->
			<!--- Yes: copy over only the named queries, and crash if Server.Scq is not defined: --->
			<cfset StructAppend(Server.Scq, Variables.Scq, "Yes")>
		<cfelse>
			<!--- No: Completely overlay Server.Scq if it exists: --->
			<cfset Server.Scq										= Duplicate(Variables.Scq)>
		</cfif>
		<!--- The following are temporary. (Convert all references, then get rid of them.) --->
		<cfif	(ListFind(Request.SlafServerCachedQueryGroups, "sbaref") GT 0)
			AND	(ListFind(Request.SlafServerCachedQueryGroups, "pronet") GT 0)>
			<cfif IsDefined("Variables.Scq.ActvIMCntryCdTbl")><!--- Might not be defined if rebuilding just a subset. --->
				<cfset Server.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>	<!--- Temp!!! --->
			</cfif>
			<cfif IsDefined("Variables.Scq.ActvStTbl")><!--- Might not be defined if rebuilding just a subset. --->
				<cfset Server.ScqActvStates							= Variables.Scq.ActvStTbl>			<!--- Temp!!! --->
			</cfif>
		</cfif>
	</cflock>
	<cfinclude template="/library/cfincludes/cachedqueries/bld_ScqNamespace.restore.cfm">
	<!--- End of normal processing. The rest is just for self-test mode. --->
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqElapsedMilliseconds						= GetTickCount() - Variables.ScqStartTickCount>
		<cfif NOT IsDefined("Request.SlafHead")>
			<cfinclude template="get_sbalookandfeel_topofhead.cfm">
		</cfif>
		<cfoutput>#Request.SlafHead#
<title>Build Server Cached Queries on #Request.SlafServerName#, #Request.SlafServerInstanceName#</title>
#Request.SlafTopOfHead#
</head>
<body class="normal pad20">
<h3 align="center">Build Server Cached Queries on #Request.SlafServerName#, #Request.SlafServerInstanceName#</h3>
<p>
	End bld_ServerCachedQueries database I/O after #Variables.ScqElapsedMilliseconds# milliseconds. 
</p>
</cfoutput>
	</cfif>
	<!---
	If this file was called directly (example: http://danube.sba.gov/library/cfincludes/bld_ServerCachedQueries.cfm), 
	go ahead and dump Variables.Scq. In the context of a scheduled task, the dump will be ignored, but in the context 
	of a manual browser execution, it can be used to verify that everything worked correctly. 
	--->
	<cfif Variables.ServerCachedQueriesSelfTestMode>
		<cfset Variables.ScqStartTickCount							= GetTickCount()>
		<cfif Variables.ServerCachedQueriesCFDumpMode>
			<cfif Len(Variables.ServerCachedQueryNames) gt 0>
				<cfdump label="Variables.Scq"						var="#Variables.Scq#" expand="Yes">
				<!--- Now that it's been cfdumped, make sure that the quick listing, below, is of what still exists: --->
				<cflock name="ServerCachedQueries" type="READONLY" timeout="30">
					<cfset Variables.Scq							= Duplicate(Server.Scq)>
				</cflock>
				<cfoutput>
<p>
	The following is what Server.Scq looks like. 
</p></cfoutput>
			<cfelse>
				<cfdump label="Server Cached Queries"				var="#Variables.Scq#" expand="No">
			</cfif>
		</cfif>
		<cfif	(NOT Variables.ServerCachedQueriesCFDumpMode)
			or	(Len(Variables.ServerCachedQueryNames) gt 0)>
			<cfoutput>
<p>
	To speed up direct calls to bld_ServerCachedQueries, 
	unless you say <b>"?CFDump=Yes"</b>, 
	we now display only a summary listing. 
</p>
<p>
	To get a full cfdump of everything, click this hotlink: 
</p>
<p><div align="center">
	<a href="#CGI.Script_Name#?CFDump=Yes">#CGI.Script_Name#?CFDump=Yes</a>
</div></p>
<p>
	That said, here's the much shorter, much faster confirmation that everything got done: <ul></cfoutput>
			<cfloop index="Variables.ScqKey" list="#ListSort(StructKeyList(Variables.Scq), 'textnocase', 'asc')#">
				<cfset Variables.ScqElt								= Variables.Scq[Variables.ScqKey]>
				<cfif IsQuery(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "#Variables.ScqElt.RecordCount# row(s)">
				<cfelseif IsStruct(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "structure">
					<cfoutput><br></cfoutput>
				<cfelseif IsDate(Variables.ScqElt)>
					<cfset Variables.ScqComment						= DateFormat(Variables.ScqElt, "mm/dd/yyyy")
																	& " "
																	& TimeFormat(Variables.ScqElt, "HH:mm:ss")>
				<cfelseif IsSimpleValue(Variables.ScqElt)>
					<cfset Variables.ScqComment						= "&quot;#Variables.ScqElt#&quot;">
				<cfelse>
					<cfset Variables.ScqComment						= "type unknown">
				</cfif>
				<cfoutput>
	<li>#Variables.ScqKey# (#Variables.ScqComment#)</li></cfoutput>
				<cfif IsStruct(Variables.ScqElt)>
					<cfoutput><ul></cfoutput>
					<cfloop index="Variables.ScqSubKey" list="#ListSort(StructKeyList(Variables.Scq[Variables.ScqKey]), 'textnocase', 'asc')#">
						<cfset Variables.ScqSubElt					= Variables.Scq[Variables.ScqKey][Variables.ScqSubKey]>
						<cfif IsQuery(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= "#Variables.ScqSubElt.RecordCount# row(s)">
						<cfelseif IsStruct(Variables.ScqSubElt)>
							<cfoutput><ul></cfoutput>
							<cfloop index="Variables.ScqSubSubKey" list="#ListSort(StructKeyList(Variables.ScqSubElt), 'textnocase', 'asc')#">
								<cfset Variables.ScqSubSubElt				= Variables.ScqSubElt[Variables.ScqSubSubKey]>
								<cfif IsQuery(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= "#Variables.ScqSubElt.RecordCount# row(s)">
								<cfelseif IsStruct(Variables.ScqSubSubElt)>
									<cfset Variables.ScqSubComment			= "structure">
								<cfelseif IsDate(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= DateFormat(Variables.ScqSubElt, "mm/dd/yyyy")
																			& " "
																			& TimeFormat(Variables.ScqSubElt, "HH:mm:ss")>
								<cfelseif IsSimpleValue(Variables.ScqSubSubElt)>
									<cfset Variables.ScqComment				= "&quot;#Variables.ScqSubSubElt#&quot;">
								<cfelse>
									<cfset Variables.ScqComment				= "type unknown">
								</cfif>
								<cfoutput>
			<li>#Variables.ScqKey#.#Variables.ScqSubKey#.#Variables.ScqSubSubKey# (#Variables.ScqComment#)</li></cfoutput>
							</cfloop>
							<cfoutput></ul></cfoutput>
						<cfelseif IsDate(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= DateFormat(Variables.ScqSubElt, "mm/dd/yyyy")
																	& " "
																	& TimeFormat(Variables.ScqSubElt, "HH:mm:ss")>
						<cfelseif IsSimpleValue(Variables.ScqSubElt)>
							<cfset Variables.ScqComment				= "&quot;#Variables.ScqSubElt#&quot;">
						<cfelse>
							<cfset Variables.ScqComment				= "type unknown">
						</cfif>
						<cfif NOT IsStruct(Variables.ScqSubElt)><!--- Structs have already been dumped. --->
							<cfoutput>
		<li>#Variables.ScqKey#.#Variables.ScqSubKey# (#Variables.ScqComment#)</li></cfoutput>
						</cfif>
					</cfloop>
					<cfoutput></ul><br></cfoutput>
				</cfif>
			</cfloop>
			<cfoutput>
	</ul>
</p>
</cfoutput>
			<cfset StructDelete(Variables, "ScqComment")>
			<cfset StructDelete(Variables, "ScqElt")>
			<cfset StructDelete(Variables, "ScqKey")>
			<cfif IsDefined("Variables.ScqSubKey")>
				<cfset StructDelete(Variables, "ScqSubElt")>
				<cfset StructDelete(Variables, "ScqSubKey")>
				<cfif IsDefined("Variables.ScqSubSubKey")>
					<cfset StructDelete(Variables, "ScqSubSubElt")>
					<cfset StructDelete(Variables, "ScqSubSubKey")>
				</cfif>
			</cfif>
		</cfif>
		<cfset Variables.ScqElapsedMilliseconds						= GetTickCount() - Variables.ScqStartTickCount>
		<cfoutput>
<p>
	End bld_ServerCachedQueries results listing after #Variables.ScqElapsedMilliseconds# milliseconds. 
</p>
</body>
</html>
</cfoutput>
		<cfset StructDelete(Variables, "ScqElapsedMilliseconds")>
		<cfset StructDelete(Variables, "ScqStartTickCount")>
	</cfif>
</cfif>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) GT 80>
		<cfset Variables.ErrMsg			= "#Variables.ErrMsg# <li>#Variables.FName# cannot exceed 80 characters.</li>">
	<cfelse>
		<cfset Variables.Split1			= ListToArray(Variables.FVal, "@")>
		<cfif ArrayLen(Variables.Split1) IS NOT 2>
			<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must contain exactly one ""@"".</li>">
		<cfelse>
			<cfset Variables.Split2		= ListToArray(Variables.Split1[2], ".")>
			<cfset Variables.TLDN		= Variables.Split2[ArrayLen(Variables.Split2)]><!--- "Top Level Domain name". --->
			<cfif NOT(	(Variables.TLDN IS "aero")
					 OR	(Variables.TLDN IS "biz")
					 OR	(Variables.TLDN IS "com")
					 OR	(Variables.TLDN IS "coop")
					 OR	(Variables.TLDN IS "edu")
					 OR	(Variables.TLDN IS "gov")
					 OR	(Variables.TLDN IS "info")
					 OR	(Variables.TLDN IS "int")
					 OR	(Variables.TLDN IS "mil")
					 OR	(Variables.TLDN IS "museum")
					 OR	(Variables.TLDN IS "name")
					 OR	(Variables.TLDN IS "net")
					 OR	(Variables.TLDN IS "org")
					 OR	(Variables.TLDN IS "pro")
					 OR	(Variables.TLDN IS "us")
					 OR	(Variables.TLDN IS "ws")
					 OR	(Variables.TLDN IS "cc")
					 OR	(Variables.TLDN IS "nu")
					 OR	(Variables.TLDN IS "to"))>
				<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>#Variables.FName# must end in a United States Top Level "
										& "Domain name (aero, biz, com, coop, edu, gov, info, int, mil, museum, name, net, org, "
										& "us or ws), or a foreign country code being used by ISPs in the United States "
										& "(cc, nu or to).</li>">
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfif CGI.Script_Name is "/library/cfincludes/get_sbalookandfeel_topofjqm.cfm">
	<cfsetting enablecfoutputonly="Yes">
	<cfset Request.SlafTopOfHeadSelfTestMode		= "Yes">
<cfelse>
	<cfset Request.SlafTopOfHeadSelfTestMode		= "No">
</cfif>
<!---
AUTHOR:				Steve Seaquist
DATE:				05/23/2012
DESCRIPTION:		Builds Request.SlafHead and Request.SlafTopOfHead for use by cf_sbalookandfeel in jQueryMobileView. 
					Request.SlafTopOfHead is the important one. It provides a standard, correct inclusion sequence of SBA 
					CSS files and JavaScript variables useful with CSS manipulation. 
NOTES:

	jQuery Mobile requires HTML5, so all of that quirks mode stuff is gone in the jqm version. 

	If you cfinclude get_sbalookandfeel_variables.cfm, it will include this file for you. Therefore, you would normally 
	NOT include this file directly. It exists outside of get_sbalookandfeel_variables so that the cf_sbalookandfeel can 
	override some of the generated code, based on Attributes passed to it in the custom tag call. 

	And if you don't mind defining the title tag lower down, simply cfoutput #Request.SlafHeadAndTopOfHead#, then title, 
	system-specific CSS, page-specific javascript and the /head. 

	When a custom tag calls a cfinclude, the Attributes scope is not passed to the cfinclude. So cf_sbalookandfeel passes 
	Attributes namespaced as Request.SlafTopOfHead variables to prevent name conflicts with the caller's variables. 

INPUT:				Request.SlafApplicationName, Request.SlafTopOfHead variables (all optional, normally used only by 
					cf_sbalookandfeel). ApplicationName may or may not have been defined by get_sbalookandfeel_variables. 
					Assumes all get_sbashared_variables are defined. 
OUTPUT:				Request.SlafHead, Request.SlafTopOfHead and Request.SlafHeadAndTopOfHead. 
REVISION HISTORY:	05/23/2012, SRS:	Original implementation. Cannibalized get_sbalookandfeel_topofhead.cfm. 
--->

<!--- Configuration Parameters: --->

<cftry>
	<cflock scope="SESSION" type="READONLY" timeout="30">
		<cfif IsDefined("Session.Slaf") and IsStruct(Session.Slaf)>
			<cfset StructAppend(Request, Session.Slaf, "Yes")>
		</cfif>
	</cflock>
	<cfcatch type="Any"><!--- If Session scope doesn't exist, no problem, do nothing . ---></cfcatch>
</cftry>


<!--- Runtime Options: --->

<cfparam name="Request.SlafButtons3D"				default="Yes"><!--- Until MSIE 9 is commonplace. --->
<cfif NOT IsDefined("Request.SlafRenderingMode")>
	<!--- Default dev and mobile to "jqm". All others get "dtv". --->
	<cfif Request.SlafDevTestProd is "Dev">			<cfset Request.SlafRenderingMode	= "jqm">
	<cfelseif Request.SlafBrowser.Mobile>			<cfset Request.SlafRenderingMode	= "jqm">
	<cfelse>										<cfset Request.SlafRenderingMode	= "dtv"></cfif>
</cfif>
<cfparam name="Request.SlafRenderingMode"			default="jqm"><!--- Defaults mobile to jQueryMobileView. --->
<cfparam name="Request.SlafTopOfHeadLang"			default="en-US">
<cfparam name="Request.SlafTopOfHeadLibURL"			default="/library">

<!--- Initializations: --->

<cfif NOT IsDefined("AppendCachedAsOf")><!--- Tricks browsers to assure that we pick up edited JS/CSS right away. --->
	<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
</cfif>
<cfif NOT IsDefined("Request.SlafDevTestProd")><!--- Passed to the user, below, in the generated JavaScript. --->
	<cfinclude template="get_sbashared_variables.cfm"><!--- Safe to omit path. We're both in /library/cfincludes. --->
</cfif>

<!--- Generate the variables: --->

<cfsavecontent variable="Request.SlafHead">
	<cfoutput><!doctype html>
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr" class="jqm">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"></cfoutput>
</cfsavecontent>

<cfsavecontent variable="Request.SlafTopOfHead">
	<cfoutput><cfif IsDefined("Request.SlafApplicationName") AND (Request.SlafApplicationName IS "CLS")>
<link  href="/cls/dsp_choosefunction.cfm" accesskey="1" rel="Home" title="Home (Return to GLS Choose Function)"></cfif>
<link  href="#Request.SlafTopOfHeadLibURL#/css/jquery.mobile/jquery.mobile.css?CachedAsOf=2012-04-16T18:19" rel="stylesheet" type="text/css" media="all" /><!-- jqm 1.1.0 -->
<link  href="#Request.SlafTopOfHeadLibURL#/css/jquery.mobile/sba.jqm.css?CachedAsOf=2012-06-28T12:20"       rel="stylesheet" type="text/css" media="all" /><!-- local code -->
<link  href="#Request.SlafTopOfHeadLibURL#/css/sba.strict.css?CachedAsOf=2012-02-28T16:27"                  rel="stylesheet" type="text/css" media="all" />
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery.js?CachedAsOf=2012-03-22T12:17"></script><!-- 1.7.2 -->
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery.mobile/sba.jqm.js?CachedAsOf=2012-07-09T17:11"></script><!-- local code -->
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery.mobile/jquery.mobile.js?CachedAsOf=2012-04-16T18:21"></script><!-- jqm 1.1.0 -->
<script>
var	gSlafDevTestProd					= "#Request.SlafDevTestProd#";
var	gSlafDevTestProdInd					= "#Request.SlafDevTestProdInd#";
var	gSlafInlineBlock					= "inline-block";
var	gSlafInlineTable					= "inline-table";
var	gSlafMSIE6or7						= false;
</script><cfif IsDefined("Request.SlafSuppressRTE") and Request.SlafSuppressRTE>
<style>
.rte									{visibility: visible	!important;}
</style></cfif><!--- /SlafSuppressRTE --->
</cfoutput><!--- Do NOT define DumpObject.js, jquery.js or sbalookandfeel.js here. Instead, cf_sbalookandfeel does that. --->
</cfsavecontent>

<cfset Request.SlafHeadAndTopOfHead		= Request.SlafHead & Request.SlafTopOfHead>

<!--- Self-Test Mode: --->

<cfif Request.SlafTopOfHeadSelfTestMode>
	<!--- Note that we "eat our own dogfood" (use the variables we create, in addition to displaying them): --->
	<cfoutput>#Request.SlafHead#
<title>SBA - Library - get_sbalookandfeel_topofjqm Self-Test Mode</title>#Request.SlafTopOfHead#
</head>
<body>
<div class="pad20">
<p align="center" class="title1">SBA - Library - get_sbalookandfeel_topofjqm Self-Test Mode</p>
<pre>
<span style="color:##900; font-weight:bold;">Request.SlafHead:</span>

#Replace(Request.SlafHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafTopOfHead:</span>

#Replace(Request.SlafTopOfHead, "<", "&lt;", "ALL")#

<span style="color:##900; font-weight:bold;">Request.SlafHeadAndTopOfHead:</span>

#Replace(Request.SlafHeadAndTopOfHead, "<", "&lt;", "ALL")#
</pre>
</div><!-- /pad20 -->
</body>
</html>
</cfoutput>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Builds variables in the Variables scope that were last saved from the Form scope ("form data recovery"). 
NOTES:				Assumes a Session lock is active. This can be a ReadOnly lock. 
INPUT:				Variables in the Session scope. 
OUTPUT:				Variables in the Variables scope. 
REVISION HISTORY:	09/26/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Session.SlafSaveFormData")>
	<!--- Do the MightNotGetSent loop before the SlafSaveForm loop, so that actual form data overrides. --->
	<cfif IsDefined("Session.SlafSaveFormData.MightNotGetSent")>
		<cfloop index="SlafKey" list="#Session.SlafSaveFormData.MightNotGetSent#">
			<!--- Quotes left of the equals sign may seem a bit weird, but it works (and Evaluate doesn't): --->
			<cfset "Variables.#SlafKey#"					= "">
		</cfloop>
	</cfif>
	<cfloop index="SlafKey" list="#StructKeyList(Session.SlafSaveFormData)#">
		<cfset "Variables.#SlafKey#"						= Evaluate("Session.SlafSaveFormData.#SlafKey#")>
	</cfloop>
</cfif>

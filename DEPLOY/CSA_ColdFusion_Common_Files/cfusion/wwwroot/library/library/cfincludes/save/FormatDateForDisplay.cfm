<!---
AUTHOR:				Steve Seaquist
DATE:				Unknown
DESCRIPTION:		Reformats Variables.Temp from database format to human format
NOTES:				None
INPUT:				Variables.Temp
OUTPUT:				Variables.Temp
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	12/15/2000, SRS:	Added this comment header. 
--->
<CFIF Len(Variables.Temp) GT 0>
	<CFIF Len(Variables.Temp) GE 11><!--- Eliminate Sybase format bug on Unix: --->
		<CFSET	Variables.TempDate	= ParseDateTime	(Left	(Variables.Temp, 11))>
	<CFELSE>
		<CFSET	Variables.TempDate	= ParseDateTime			(Variables.Temp)>
	</CFIF>
	<CFSET	Variables.Temp			= DateFormat			(Variables.TempDate, "MM/DD/YYYY")>
	<CFIF	Variables.Temp IS "01/01/1900">
		<CFSET	Variables.Temp		= "">
	</CFIF>
</CFIF>

<!---
AUTHOR:				Steve Seaquist
DATE:				10/30/2006
DESCRIPTION:		Defines Variables.ArrayDatasources. 
NOTES:				None. 
INPUT:				Server data, neo-query.xml file. 
OUTPUT:				Variables.ArrayDatasources. 
REVISION HISTORY:	02/27/2007, SRS:	If Variables.BadIncludeServerInfo is defined and "Yes", include server info. 
					01/04/2007, SRS:	Fix to allow hyphens in datasource names. 
					10/30/2006, SRS:	Original implementation. 
--->

<cfparam name="Variables.BadIncludeServerInfo"						default= "No"><!--- "Bad = "bld_ArrayDatasources" --->
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.NqOSName										= Server.OS.Name>
	<cfset Variables.NqRootDir										= Server.ColdFusion.RootDir>
</cflock>
<cfif FindNoCase("Windows", Variables.NqOSName) GT 0>				<!--- Windows: --->
	<cfset Variables.NqPathAndFile									= "#Variables.NqRootDir#\lib\neo-query.xml">
<cfelse>															<!--- Unix and Mac: --->
	<cfset Variables.NqPathAndFile									= "#Variables.NqRootDir#/lib/neo-query.xml">
</cfif>
<cffile action="Read" file="#Variables.NqPathAndFile#" variable="Variables.NqXML">
<cfwddx action="WDDX2CFML" input="#Variables.NqXML#" output="Variables.NqDOM">
<cfset Variables.ArrayDatasources									= ArrayNew(2)>
<cfset Variables.NqS3												= Variables.NqDOM[3]>
<cfset Variables.NqS3Len											= 0>
<cfloop index="NqS3ItemName" list="#StructKeyList(Variables.NqS3)#">
	<cfset Variables.NqS3Len										= Variables.NqS3Len + 1>
	<cfset Variables.NqS3ItemValue									= Variables.NqS3[NqS3ItemName]>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][1]			= Variables.NqS3ItemValue.Name>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][2]			= Variables.NqS3ItemValue.Driver>
	<cfset Variables.ArrayDatasources[Variables.NqS3Len][3]			= Variables.NqS3ItemValue.Username>
	<cfif Variables.BadIncludeServerInfo>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][4]		= Variables.NqS3ItemValue.Password>
		<cfset Variables.NqS3ConnProps								= Variables.NqS3ItemValue.urlmap.ConnectionProps>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][5]		= Variables.NqS3ConnProps.Host>
		<cfset Variables.ArrayDatasources[Variables.NqS3Len][6]		= Variables.NqS3ConnProps.Port>
		<cfif IsDefined("Variables.NqS3ConnProps.Database")>		<!-- Might not be defined -->
			<cfset Variables.ArrayDatasources[Variables.NqS3Len][7]	= Variables.NqS3ConnProps.Database>
		<cfelse>
			<cfset Variables.ArrayDatasources[Variables.NqS3Len][7]	= "">
		</cfif>
	</cfif>
</cfloop>
<cfset StructDelete(Variables, "NqDOM")>
<cfset StructDelete(Variables, "NqOSName")>
<cfset StructDelete(Variables, "NqPathAndFile")>
<cfset StructDelete(Variables, "NqRootDir")>
<cfset StructDelete(Variables, "NqS3")>
<cfset StructDelete(Variables, "NqS3ConnProps")>
<cfset StructDelete(Variables, "NqS3ItemName")>
<cfset StructDelete(Variables, "NqS3ItemValue")>
<cfset StructDelete(Variables, "NqS3Len")>
<cfset StructDelete(Variables, "NqXML")>

<cfscript>
function ArraySortDatasourcesByDatasource	(p1, p2)
{
return CompareNoCase(p1[1], p2[1]);
}
function ArraySortDatasourcesByDBType		(p1, p2)
{
return (CompareNoCase(p1[2], p2[2]) * 2) + CompareNoCase(p1[1], p2[1]);
}
</cfscript>

<cfif CGI.Script_Name IS "/library/cfincludes/get_ArrayDatasources.cfm">
	<cfinclude template="/library/udf/bld_ArraySortByCallback.cfm">
	<cfoutput><html lang="en-US">
<head><title>Dump Datasources</title></head>
<body bgcolor="##ffffff">
</body>

<cfdump
	var		= "#Variables.ArrayDatasources#"
	label	= "ArrayDatasources">
<cfdump
	var		= "#ArraySortByCallback(Variables.ArrayDatasources, ArraySortDatasourcesByDatasource)#"
	label	= "ArrayDatasources, Sorted">

</body>
</html>
</cfoutput>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				03/02/2007
DESCRIPTION:		Defines Request.CurrIPxxx variables (logical names to control temporarily IP-restricted code). 
NOTES:				In general, code should be restricted by GLS Role. These logical names are for those occassions 
					where defining a GLS Role would be overkill, such as in Developer Utilities. Also, all of these 
					IP addresses are subject to change, hence the prefix "Curr". But then, that's exactly why we need 
					logical names. By coding to these names, rather than the actual IP address constants, we can 
					maintain the values in only one place. These names are normally tested against CGI.Remote_Addr. 
INPUT:				None. 
OUTPUT:				Request.CurrIPxxx variables. 
REVISION HISTORY:	03/13/2014 -		Changed/added many addresses, since fixed IP addresses are linked to MAC address, 
					09/03/2014, SRS:	and the new Windows 7 machines have new network cards, hence new MAC addresses. 
					01/17/2013, SRS:	Changed Nelli's address. Added Melissa Kidd and Nancy Maynard. 
					05/14/2013, NNI:	Added Nirish with a new IPaddress.
					03/13/2013, SRS:	Removed Frank, Nirish, Ramana, Sirisha and Sridhar. Changed Robert, Ron and Steve's 
										to their 8th floor IP addresses. 
					08/14/2012, SRS:	Removed Raja Bhandarkar. 
					??/??/2012, ???:	Changed Ian's primary IP from ".40.117" to ".45.3". 
					04/04/2012, SRS:	Added Sirisha, Nelli, Frank and Ramana. 
					12/16/2011, NNI:	Updated values for Nirish and Sheri. 
					03/29/2011, SRS:	Added Sridhar Ravinuthala. 
					11/15/2010 -
					12/20/2010, SRS:	Added Raja Bhandarkar (twice). 
					07/20/2010, SRS:	Edited Jame George's IP to what it is now. 
					10/14/2008, SRS:	Added Asad Chaklader and Robert Doyle. 
					09/05/2008, SRS:	Restored Jame George. 
					07/11/2007, SRS:	Added Nirish Namilae. 
					04/30,2008, SRS:	Added Matt Forman. 
					03/21,2008, SRS:	Changes to support some folks' leaving and others' returning to 4th floor. 
					05/17-21,2007, SRS:	Changes to support 5 folks' moves to Concourse level of Central Office. 
					03/02/2007, SRS:	Original implementation.
--->

<!--- Alphabetical first-then-last for easier lookup by humans: --->
<cfset Request.CurrIPAsadChaklader					= "165.110.85.29"><!--- win 7 --->
<cfset Request.CurrIPAsadChaklader2					= "165.110.85.128"><!--- win xp --->
<cfset Request.CurrIPAdamGeorge						= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPChuda							= "165.110.40.101">
<cfset Request.CurrIPDileep							= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPFrankMijares					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPGunitaMakkar					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPIanClark						= "165.110.85.16"><!--- Was 45.3. --->
<cfset Request.CurrIPJameGeorge						= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPMattForman						= "165.110.40.185">
<cfset Request.CurrIPMelissaKidd					= "165.110.85.174">
<cfset Request.CurrIPNancyMaynard					= "165.110.85.22">
<cfset Request.CurrIPNancyMaynard2					= "165.110.85.18"><!--- Was 10, changed without explanation. --->
<cfset Request.CurrIPNancyMaynard3					= "165.110.85.111"><!--- Used only in /developer for Recursive Grep --->
<cfset Request.CurrIPNelliSalatova					= "165.110.85.16"><!--- win 7, formerly Ian's --->
<cfset Request.CurrIPNelliSalatova2					= "165.110.85.183"><!--- win xp --->
<cfset Request.CurrIPNirishNamilae					= "165.110.85.117"><!--- win 7 --->
<cfset Request.CurrIPNirishNamilae2					= "165.110.85.109"><!--- win xp --->
<cfset Request.CurrIPNirishNamilae3					= "165.110.85.7"><!--- old --->
<cfset Request.CurrIPRahulGundala					= "165.110.85.179">
<cfset Request.CurrIPRajaBhandarkar					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRajaBhandarkarToo				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRamanaBandreddi				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPRobertDoyle					= "165.110.85.35"><!--- win 7 --->
<cfset Request.CurrIPRobertDoyle2					= "165.110.85.33"><!--- win xp --->
<cfset Request.CurrIPRonWhalen						= "165.110.85.112">
<cfset Request.CurrIPSheriMcConville				= "165.110.85.153"><!--- Was 16, then 35, changed without explanation. --->
<cfset Request.CurrIPSirishaRavula					= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSridharRavinuthala				= "no longer here"><!--- Still defined in case still used somewhere. --->
<cfset Request.CurrIPSruthiPenmetsa					= "165.110.85.34">
<cfset Request.CurrIPSteveSeaquist					= "165.110.85.129"><!--- Was 165.110.40.166 on 4th floor. --->
<cfset Request.CurrIPSteveSeaquist2					= "165.110.85.14"><!--- Used only in /developer for Recursive Grep --->
<cfset Request.CurrIPTimalynFranklin				= "165.110.85.158">

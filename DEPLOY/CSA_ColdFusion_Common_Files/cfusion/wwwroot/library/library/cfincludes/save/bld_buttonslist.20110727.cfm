<!---
AUTHOR:				Sirisha Ravula, Trusted Mission Solutions, Inc. 
DATE:				07/06/2011
DESCRIPTION:		
NOTES: 	
INPUT:				
OUTPUT:				
REVISION HISTORY:	07/06/2011, SRv:	Original implementation. 
--->
<!--- Variables.Temp is to include Steve's override for Buttons code. --->
<cfset Request.SlafShowButtonOverrides	= ArrayNew(1)>
<cfset Variables.JSChngSysURL				= "/library/javascripts/sbalookandfeel/changesystem">
<cfif isDefined("Variables.Show")>
	<cfloop list="#Variables.ShowButtonList#" index="i">		
		<cfif trim(ucase(i)) NEQ ucase(Variables.RequestedSubsystemDir)>			
			<cfswitch expression="#i#">
				<cfcase value="applications">
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/elend/applications/dsp_routing.cfm">					
					<cfset Variables.Temp.title     = "Opens a new window to Electronic Lending - Origination.">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoOrigination.js">
					<cfset Variables.Temp.show      = "Origination">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show	= ListAppend(Variables.Show, "Origination")>
				</cfcase>							
				<cfcase value="servicing">					   
				  	<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/elend/servicing/dsp_routing.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to Electronic Lending - Servicing.">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoServicing.js">
					<cfset Variables.Temp.show      = "Servicing">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show	= ListAppend(Variables.Show, "Servicing")>
				</cfcase>							
				<cfcase value="LANA">				 					
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/lana/applications/dsp_routing.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to Loan Associated Names and Addresses (LANA).">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoLANA.js">
					<cfset Variables.Temp.show      = "LANA">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>
					<cfset Variables.Show	= ListAppend(Variables.Show, "#i#")>
				</cfcase>							
				<cfcase value="Chron">					
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/clcs/dsp_search.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to Centralized Loan Chron System (CLCS).">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoChron.js">
					<cfset Variables.Temp.show      = "Chron">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>		
					<cfset Variables.Show	= ListAppend(Variables.Show, "#i#")>		
				</cfcase>							
				<cfcase value="GPTS">					
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/gpts/dsp_search.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to Guarantee Purchase Tracking System (GPTS).">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoGPTS.js">
					<cfset Variables.Temp.show      = "GPTS">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>							
					<cfset Variables.Show	= ListAppend(Variables.Show, "#i#")>							
				</cfcase>							
				<cfcase value="Elips">					
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external  = "Yes">
					<cfset Variables.Temp.href		= "/elipsapps/index.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to ELIPS.">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoELIPS.js">					
					<cfset Variables.Temp.show      = "ELIPS">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>										
					<cfset Variables.Show	= ListAppend(Variables.Show, "#i#")>
				</cfcase>							
				<cfcase value="PostServicing">
					<cfset Variables.Temp           = StructNew()>					
					<cfset Variables.Temp.external	= "Yes">
					<cfset Variables.Temp.href		= "/psas/dsp_routing.cfm">
					<cfset Variables.Temp.title     = "Opens a new window to Electronic Lending - Post Servicing (ETRAN).">
					<cfset Variables.scriptsrc		= "#Variables.JSChngSysURL#/DoPostServicing.js">
					<cfset Variables.Temp.show      = "PostServicing">
					<cfset ArrayAppend(Request.SlafShowButtonOverrides, Duplicate(Variables.Temp))>		
					<cfset Variables.Show	= ListAppend(Variables.Show, "#i#")>			
				</cfcase>
		</cfswitch>
		</cfif> 
	</cfloop>
</cfif>  
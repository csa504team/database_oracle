<!---
AUTHOR:				Steve Seaquist
DATE:				04/28/2006
DESCRIPTION:		Makes sure that all "server cached queries" ("Scq") variables are defined in the Server scope. 
NOTES:

	Cfinclude this file before attempting to retrieve Scq variables from the Server scope. 

	Don't be misled by this file's name. It's a "build" routine, not a "get" routine. All it does is build server cached 
	queries in the SERVER scope, and only if necessary. THIS FILE DOES NOT ACTUALLY COPY SCQ VARIABLES INTO THE VARIABLES 
	OR REQUEST SCOPE. It just makes sure they're defined so that YOU can copy them into the Variables or Request scope, 
	because only you know which ones you actually need. 

	The purpose of the "Scq" prefix (as with the Slaf prefix in SBA look-and-feel) is to avoid naming conflicts with your 
	own variables. When you copy them into the Variables or Request scope, uou can keep their names the same (to show their 
	origin) or rename them (to make your code more readable). In the following example, Server.Scq.ActvStTbl is renamed to 
	[Variables.]getStates for readability: 

		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
		<cflock scope="SERVER" type="READONLY" timeout="30">
			<cfset getStates					= Server.Scq.ActvStTbl>
		</cflock>
		<cfloop query="getStates">
			...
		</cfloop>

	Because we use the datasource "public_sbaref" (which uses the login "sbaselect"), you cannot include this file inside 
	a cftransaction that uses a different datasource or login. But it doesn't really have to be close to the code that locks 
	the Server scope. Just include this file at the beginning of your CFM file, among its initializations. 

	Note that code and description columns are available in 2 ways: (1) as their actual database column names, and (2) as 
	"code" and "description". If you don't want to use "code" and "description" (because they aren't unique across queries), 
	then don't use them. No harm done. They exist for use by shared routines (eg: to populate a variety of drop-down menus). 

INPUT:				None. 
OUTPUT:				Server scope variables prefixed with "Scq" (short for "server cached queries"), so as not to conflict 
					with other variables defined by the application. If the current user happens to be the one who takes 
					the I/O hit to define the queries, the "Scq" variables will also be defined in the Variables scope. 
					Generally, this will only happen when this routine is executed on its own as a Scheduled Task. 
REVISION HISTORY:	02/21/2008, SRS:	Added support for the Scq.technet substructure and defined 9 new queries to be stored 
										there. Alphabetized all substructures for easy lookup. Reinstated earlier version's 
										"save and restore" variables, plus some extras, so that this file will actually use all 
										of the official, standard names (db, dbtype, ErrMsg, etc). This allows use of SPC files 
										and /library/udf/bld_dbutils_for_dbtype.cfm, if necessary. Defined OracleSchemaName 
										(another official, standard name) and based its value on the current substructure name 
										(partner, pronet, technet or none). This allows queries to be done in any order, 
										not neccesarily grouped by substructure. But mainly, it allows rapidly changing the 
										OracleSchemaName, as we will have to do soon with the advent of viewtechnet. 
					01/09/2008, NNI:	Made changes to code to point to Sybase db server in case of partner database calls. 
					01/07/2008, NNI:	Made changes to code to point to Oracle db servers. public_sbaref, pronet and partner datasources 
										have been changed to oracle_webanalytic_publicread. 
					08/20/2007, DKC:	Added support for partner database cached queries (replaces static html file used in PIMS). 
					06/04/2007, DKC:	Added VetGrpCd to the Scq.ActvVetTbl.
					04/25/2007, SRS:	Per Dileep and Sheri, saved and restored db and dbtype (if pronet is being built). 
					04/04/2007, SRS:	Per Sheri McConville, added ActvSBAOfcTbl result set. Also allowed for AllSBAOfcTbl 
										result set, but because it had 430 rows, removed it from ScqAvailableResultSetNames. 
										(That effectively comments it out / doesn't cache the query.) Added StrtDt and EndDt 
										to all previously-defined "All" result sets, if they have those columns. (It was an 
										oversight not to have included them in the first place.) 
					03/28/2007, SRS:	Per Ron Whalen, added AllPrcsMthdTbl and AllPrgrmTbl result sets for loan servicing. 
					12/07/2006, SRS:	Restricted Scq.pronet loads to explicitly named servers. (Previously, it was loaded 
										by default on all servers except devyesapp and yesapp.) 
					10/03/2006, SRS:	Special exception for Scq.pronet.ActvFrmlDtrmntnCtgryTbl extended to enile.
					09/27/2006, DKC:	Allowed it to be directly executed out of stagelibrary and build the same display.
					09/14/2006, DKC:	Added support for new table IMDomnTypTbl.
					08/11/2006, SRS:	Added support for pronet database cached queries (formerly cached in the Application 
										scope of PRO-Net when PRO-Net was not part of GLS). Also added "new logging". 
					06/14/2006, SRS:	Added 32 new cached query types. Allowed controlling what queries get defined 
										by controlling ScqAvailableResultSetNames. Per Ron Whalen, don't include Scq.ActvCMSACdTbl 
										in ScqAvailableResultSetNames, because it appear not to be in use. 
					05/12/2006, SRS:	Added IMCntryDialCd to ScqActvCountries, for use with dsp_IMCntryCdToIMCntryDialCd. 
					04/28/2006, SRS:	Original implementation. Used cfqueries initially, knowing that we will someday 
										rewrite it to use a stored procedure call (SPC) file. That's why there isn't any 
										cftry/cfcatch code. That'll be automatically added when we call the SPC file. 
--->

<!--- Configuration Variables: --->

<cfset Variables.ScqAvailableResultSetNames							=  "Scq.ActvBusAgeCdTbl"
																	& ",Scq.ActvBusTypTbl"
																	& ",Scq.ActvCalndrPrdTbl"
																	& ",Scq.ActvCitznshipCdTbl"
																	& ",Scq.ActvCohortCdTbl"
																	& ",Scq.ActvEconDevObjctCdTbl"
																	& ",Scq.ActvIMCntryCdTbl"
																	& ",Scq.ActvIMCrdtScorSourcTblBus"
																	& ",Scq.ActvIMCrdtScorSourcTblPer"
																	& ",Scq.ActvIMDomnTypTbl"
																	& ",Scq.ActvIMEPCOperCdTbl"
																	& ",Scq.ActvEthnicCdTbl"
																	& ",Scq.ActvGndrTbl"
																	& ",Scq.ActvGndrMFCdTbl"
																	& ",Scq.ActvInjctnTypCdTbl"
																	& ",Scq.ActvLoanCollatTypCdTbl"
																	& ",Scq.ActvLoanCollatValSourcTbl"
																	& ",Scq.ActvLoanCrdtUnavRsnCdTbl"
																	& ",Scq.ActvLoanFinanclStmtSourcTbl"
																	& ",Scq.ActvLoanMFDisastrTypTbl"
																	& ",Scq.ActvLoanMFPrcsMthdTypTbl"
																	& ",Scq.ActvLoanPartLendrTypTbl"
																	& ",Scq.ActvLoanPckgSourcTypTbl"
																	& ",Scq.ActvLoanPrevFinanStatTbl"
																	& ",Scq.ActvLoanProcdTypTbl"
																	& ",Scq.ActvLoanStatCdTbl"
																	& ",Scq.ActvMfSubPrgrmCdTbl"
																	& ",Scq.ActvPrcsMthdTbl"
																	& ",Scq.ActvPrgrmTbl"
																	& ",Scq.ActvPrgrmValidTbl"
																	& ",Scq.ActvRaceCdTbl"
																	& ",Scq.ActvSBAOfcTbl"
																	& ",Scq.ActvSpcPurpsLoanTbl"
																	& ",Scq.ActvStatCdTbl"
																	& ",Scq.ActvStTbl"
																	& ",Scq.ActvVetTbl"
																	& ",Scq.AllPrcsMthdTbl"
																	& ",Scq.AllPrgrmTbl">
<!---
In the following, use cfcase with multiple values, because it's more efficient than cfif OR OR OR OR, etc: 
Logical server names (enile, eweb, pro-net, etc) should never appear, but it doesn't hurt to be paranoid. 
--->
<cfif NOT IsDefined("Request.SlafServerName")>
	<cfinclude template="get_actual_server_name.cfm"><!--- Probably safe to assume same directory. --->
</cfif>
<cfswitch expression="#Request.SlafServerName#">
<cfcase value="danube,enile,eweb,newdanube,newyukon,riogrande,rouge,wocs41,yukon">
	<cfset Variables.ScqAvailableResultSetNames						= Variables.ScqAvailableResultSetNames
																	& ",Scq.partner"
																	& ",Scq.partner.AllPrgrmTbl"
																	& ",Scq.partner.AllSearchSubCatTyps"
																	& ",Scq.partner.AllValidLocTypTbl">
</cfcase>
</cfswitch>
<cfswitch expression="#Request.SlafServerName#">
<cfcase value="amazon,danube,dsbs,enile,eweb,eweb1,missouri,newdanube,newyukon,pronet,pro-net,riogrande,rouge,wocs41,yukon">
	<cfset Variables.ScqAvailableResultSetNames						= Variables.ScqAvailableResultSetNames
																	& ",Scq.pronet"
																	& ",Scq.pronet.ActvCountryCdTblForeign"
																	& ",Scq.pronet.ActvExportMainActvtyTypTbl"
																	& ",Scq.pronet.ActvExportMrktTypTbl"
																	& ",Scq.pronet.ActvFrmlDtrmntnCtgryTbl"
																	& ",Scq.pronet.ActvIMAreaTypCdTbl"
																	& ",Scq.pronet.ActvIMQAStdCdTbl"
																	& ",Scq.pronet.ActvMntrAreaTypTbl"
																	& ",Scq.pronet.AllCountryCdTbl"
																	& ",Scq.pronet.AllIMQAStdCdTbl">
</cfcase>
</cfswitch>
<cfswitch expression="#Request.SlafServerName#">
<cfcase value="amazon,danube,enile,eweb,localhost,missouri,newdanube,newyukon,riogrande,rouge,technet,tech-net,wocs41,yukon">
	<cfset Variables.ScqAvailableResultSetNames						= Variables.ScqAvailableResultSetNames
																	& ",Scq.technet"
																	& ",Scq.technet.ActvIMAwrdInvestSaleTypCdTbl"
																	& ",Scq.technet.ActvIMAwrdMentrTypCdTbl"
																	& ",Scq.technet.ActvIMAwrdSECdTbl"
																	& ",Scq.technet.ActvIMFedAgncyTbl"
																	& ",Scq.technet.ActvIMFedAgncyTbl4XML"
																	& ",Scq.technet.ActvIMFedAgncyBrTbl"
																	& ",Scq.technet.ActvIMFedAgncyBrTbl4XML"
																	& ",Scq.technet.ActvIMPrgrmTbl"
																	& ",Scq.technet.ActvIMSTTRPrtCatTbl"
																	& ",Scq.technet.ActvIMSTTRPrtTypTbl">
</cfcase>
</cfswitch>

<!--- First, check the master variable that defines whether or not all the others are defined: --->

<cfif NOT IsDefined("Variables.PageName")>
	<cfset Variables.PageName										= ListGetAt(CGI.Script_Name,
																				ListLen(CGI.Script_Name,"/"),
																				"/")>
</cfif>
<cfset Variables.ScqIsDefined										= "No">
<cfif Variables.PageName IS NOT "bld_ServerCachedQueries.cfm">
	<!---
	When this file is cfincluded (the usual case), the following check will be done. But when executed as a Scheduled Task, 
	or when you manually go to /library/cfincludes/bld_ServerCachedQueries.cfm as a test, the following check will NOT be done. 
	(This forces the queries to be refreshed from the database.) The Scheduled Task is done around 5:00 AM. 
	--->
	<cflock scope="SERVER" type="READONLY" timeout="30">
		<cfif IsDefined("Server.Scq")>
			<cfset Variables.ScqIsDefined							= "Yes">
		</cfif>
	</cflock>
</cfif>

<!--- Build the queries if they don't exist, or if we were told to (by calling this page directly): --->

<cfif NOT Variables.ScqIsDefined>
	<!---
	Since the caller may already have defined db, dbtype, TxnErr and/or ErrMsg, save them, 
	so as not to mess up the caller. We will restore them at the end of this cfif, 
	assuming we don't crash, of course. 
	--->
	<cfif IsDefined("Variables.cfprname")>
		<cfset Variables.ScqCFPRName								= Variables.cfprname>
	</cfif>
	<cfif IsDefined("Variables.db")>
		<cfset Variables.ScqDB										= Variables.db>
	</cfif>
	<cfif IsDefined("Variables.dbtype")>
		<cfset Variables.ScqDBType									= Variables.dbtype>
	</cfif>
	<cfif IsDefined("Variables.ErrMsg")>
		<cfset Variables.ScqErrMsg									= Variables.ErrMsg>
	</cfif>
	<cfif IsDefined("Variables.LogAct")>
		<cfset Variables.ScqLogAct									= Variables.LogAct>
	</cfif>
	<cfif IsDefined("Variables.OracleSchemaName")><!--- See /library/udf/bld_dbutils_for_dbtype.cfm. --->
		<cfset Variables.ScqOracleSchemaName						= Variables.OracleSchemaName>
	</cfif>
	<cfif IsDefined("Variables.TxnErr")>
		<cfset Variables.ScqTxnErr									= Variables.TxnErr>
	</cfif>
	<!--- Set the datasource based on actual server name: --->
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfswitch expression="#Request.SlafServerName#">
	<cfcase value="amazon,missouri">	<cfset Variables.db_default	= "oracle_webanalytic_publicread">	</cfcase>
	<cfdefaultcase>						<cfset Variables.db_default	= "oracle_analytic_publicread">		</cfdefaultcase>
	</cfswitch>
	<cfset Variables.db												= Variables.db_default>
	<cfset Variables.dbtype											= "Oracle80">
	<cfset Variables.ErrMsg											= "Problems occurred while attempting "
																	& "to cache database queries: ">
	<cfset Variables.TxnErr											= "No">
	<!---
	Do the queries outside of the exclusive cflock of the Server scope, so as to minimize exclusive lock time. Also, 
	because some servers have no need of some cached queries, define the queries in a loop. This allows creating or 
	not creating a query simply by altering the contents of ScqAvailableResultSetNames on a server-by-server basis. 
	--->
	<cfset Variables.Scq											= StructNew()>
	<cfloop index="ScqQueryName" list="#Variables.ScqAvailableResultSetNames#">
		<!--- The following code makes sure that OracleSchemaName is ALWAYS correct for the current substructure. --->
		<cfset Variables.ScqQueryNameArray							= ListToArray(ScqQueryName, ".")>
		<cfif ArrayLen(Variables.ScqQueryNameArray) GT 2>
			<cfset Variables.ScqSubstructureName					= Variables.ScqQueryNameArray[2]>
		<cfelse>
			<cfset Variables.ScqSubstructureName					= "">
		</cfif>
		<cfswitch expression="#Variables.ScqSubstructureName#">
		<cfcase value="partner">
			<cfset Variables.OracleSchemaName						= "N/A"><!--- Still Sybase. --->
			<!--- partner's datasource is hard-coded for now, so don't change db unnecessarily. --->
		</cfcase>
		<cfcase value="pronet">
			<cfset Variables.OracleSchemaName						= "viewpronet">
			<cfset Variables.db										= "oracle_webanalytic_publicread">
		</cfcase>
		<cfcase value="technet">
			<cfset Variables.OracleSchemaName						= "technet"><!--- Will soon be viewtechnet --->
			<cfset Variables.db										= "oracle_webtransaction_publicread">
		</cfcase><!--- Soon: viewtechnet!!! --->
		<cfdefaultcase>
			<cfset Variables.OracleSchemaName						= "sbaref">
			<cfset Variables.db										= Variables.db_default>
		</cfdefaultcase>
		</cfswitch>
		<!---
		Although ScqAvailableResultSetNames is defined alphabetically (all Actvs, followed by all Alls), it's easier 
		to maintain the result sets if the same table's result sets are adjacent. So the ordering below defines 
		ActvPrgrmTbl, immediately followed by AllPrgrmTbl, for example. (Knowing this makes it easier to find stuff.) 
		--->
		<cfswitch expression="#ScqQueryName#">
		<cfcase				  value="Scq.ActvBusAgeCdTbl">
			<cfquery name="Variables.Scq.ActvBusAgeCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		BusAgeCd,
						BusAgeDesc,
						BusMFAgeCd,
						BusAgeCd									AS code,
						BusAgeDesc									AS description
			from		#Variables.OracleSchemaName#.BusAgeCdTbl
			where		(	(SYSDATE-BusAgeStrtDt) >= 0)
			and			(				BusAgeEndDt					is null
						or	(SYSDATE-BusAgeEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvBusTypTbl">
			<cfquery name="Variables.Scq.ActvBusTypTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		BusTypCd,
						BusTypTxt,
						BusTypMFCd,
						BusTypMFTxt,
						BusTypCd									AS code,
						BusTypTxt									AS description
			from		#Variables.OracleSchemaName#.BusTypTbl
			where		(	(SYSDATE-BusTypStrtDt) >= 0)
			and			(				BusTypEndDt					is null
						or	(SYSDATE-BusTypEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCalndrPrdTbl">
			<cfquery name="Variables.Scq.ActvCalndrPrdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CalndrPrdCd,
						CalndrPrdDesc,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CalndrPrdCd = 'M'						then 1
						when CalndrPrdCd = 'Q'						then 2
						when CalndrPrdCd = 'S'						then 3
						when CalndrPrdCd = 'A'						then 4
						else										5
						end											AS displayorder,
						CalndrPrdCd									AS code,
						CalndrPrdDesc								AS description
			from		#Variables.OracleSchemaName#.CalndrPrdTbl
			where		(	(SYSDATE-CalndrPrdStrtDt) >= 0)
			and			(				CalndrPrdEndDt				is null
						or	(SYSDATE-CalndrPrdEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCitznshipCdTbl">
			<cfquery name="Variables.Scq.ActvCitznshipCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CitznshipCd,
						CitznshipTxt,
						case<!--- Calculate most likely to least likely answers, order them like that in drop-downs: --->
						when CitznshipCd = 'US'						then 1
						when CitznshipCd = 'RA'						then 2
						when CitznshipCd = 'NR'						then 3
						when CitznshipCd = 'IA'						then 4
						else										5
						end											AS displayorder,
						CitznshipCd									AS code,
						CitznshipTxt								AS description
			from		#Variables.OracleSchemaName#.CitznshipCdTbl
			where		(	(SYSDATE-CitznshipStrtDt) >= 0)
			and			(				CitznshipEndDt				is null
						or	(SYSDATE-CitznshipEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCMSACdTbl">
			<cfquery name="Variables.Scq.ActvCMSACdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CMSACd,
						CMSANm,
						CMSACd										AS code,
						CMSANm										AS description
			from		#Variables.OracleSchemaName#.CMSACdTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvCohortCdTbl">
			<cfquery name="Variables.Scq.ActvCohortCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CohortCd,
						CohortDesc,
						CohortCd									AS code,
						CohortDesc									AS description
			from		#Variables.OracleSchemaName#.CohortCdTbl
			where		(	(SYSDATE-CohortStrtDt) >= 0)
			and			(				CohortEndDt					is null
						or	(SYSDATE-CohortEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEconDevObjctCdTbl">
			<cfquery name="Variables.Scq.ActvEconDevObjctCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		EconDevObjctCd,
						EconDevObjctTxt,
						EconDevObjctCd								AS code,
						EconDevObjctTxt								AS description
			from		#Variables.OracleSchemaName#.EconDevObjctCdTbl
			where		(	(SYSDATE-EconDevObjctStrtDt) >= 0)
			and			(				EconDevObjctEndDt			is null
						or	(SYSDATE-EconDevObjctEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCntryCdTbl">
			<cfquery name="Variables.Scq.ActvIMCntryCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCntryCd,
						IMCntryNm,
						IMCntryDialCd,
						IMCntryCd									AS code,
						IMCntryNm									AS description
			from		#Variables.OracleSchemaName#.IMCntryCdTbl
			where		(	(SYSDATE-IMCntryStrtDt) >= 0)
			and			(				IMCntryEndDt				is null
						or	(SYSDATE-IMCntryEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			<cfset Variables.ScqActvCountries						= Variables.Scq.ActvIMCntryCdTbl>
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblBus">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblBus"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		#Variables.OracleSchemaName#.IMCrdtScorSourcTbl
			where		(	(SYSDATE-IMCrdtScorSourcStrtDt) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(SYSDATE-IMCrdtScorSourcEndDt) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'B')
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMCrdtScorSourcTblPer">
			<cfquery name="Variables.Scq.ActvIMCrdtScorSourcTblPer"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMCrdtScorSourcCd,
						IMCrdtScorSourcDescTxt,
						IMCrdtScorSourcLowNmb,
						IMCrdtScorSourcHighNmb,
						case<!--- Alphabetical by description, but with Blended and Other forced to the end: --->
						when IMCrdtScorSourcCd = 12					then 1
						when IMCrdtScorSourcCd = 13					then 2
						when IMCrdtScorSourcCd = 11					then 3
						when IMCrdtScorSourcCd = 14					then 4
						when IMCrdtScorSourcCd = 15					then 5
						else										6
						end											AS displayorder,
						IMCrdtScorSourcCd							AS code,
						IMCrdtScorSourcDescTxt						AS description
			from		#Variables.OracleSchemaName#.IMCrdtScorSourcTbl
			where		(	(SYSDATE-IMCrdtScorSourcStrtDt) >= 0)
			and			(				IMCrdtScorSourcEndDt		is null
						or	(SYSDATE-IMCrdtScorSourcEndDt) <= 0)
			and			(IMCrdtScorSourcBusPerInd					= 'P')
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMDomnTypTbl">
			<cfquery name="Variables.Scq.ActvIMDomnTypTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select 		IMDomnTypCd									AS code,
						IMDomnTypDescTxt							AS description
			from 		#Variables.OracleSchemaName#.IMDomnTypTbl
			where 		(	(SYSDATE-IMDomnTypStrtDt) >= 0)
			and			(				IMDomnTypEndDt				is null
						or	(SYSDATE-IMDomnTypEndDt) <= 0)
			order by	IMDomnTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvIMEPCOperCdTbl">
			<cfquery name="Variables.Scq.ActvIMEPCOperCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		IMEPCOperCd,
						IMEPCOperDescTxt,
						IMEPCOperCd									AS code,
						IMEPCOperDescTxt							AS description
			from		#Variables.OracleSchemaName#.IMEPCOperCdTbl
			where		(	(SYSDATE-IMEPCOperCdStrtDt) >= 0)
			and			(				IMEPCOperCdEndDt			is null
						or	(SYSDATE-IMEPCOperCdEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvEthnicCdTbl">
			<cfquery name="Variables.Scq.ActvEthnicCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		EthnicCd,
						EthnicDesc,
						EthnicCd									AS code,
						EthnicDesc									AS description
			from		#Variables.OracleSchemaName#.EthnicCdTbl
			where		(	(SYSDATE-EthnicStrtDt) >= 0)
			and			(				EthnicEndDt					is null
						or	(SYSDATE-EthnicEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrTbl">
			<cfquery name="Variables.Scq.ActvGndrTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		GndrCd,
						GndrDesc,
						GndrCd										AS code,
						GndrDesc									AS description
			from		#Variables.OracleSchemaName#.GndrCdTbl
			where		(	(SYSDATE-GndrStrtDt) >= 0)
			and			(				GndrEndDt					is null
						or	(SYSDATE-GndrEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvGndrMFCdTbl">
			<cfquery name="Variables.Scq.ActvGndrMFCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		GndrMFCd,
						GndrMFDescTxt,
						GndrCd,
						GndrMFCd									AS code,
						GndrMFDescTxt								AS description
			from		#Variables.OracleSchemaName#.GndrMFCdTbl
			where		(	(SYSDATE-GndrMFStrtDt) >= 0)
			and			(				GndrMFEndDt					is null
						or	(SYSDATE-GndrMFEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvInjctnTypCdTbl">
			<cfquery name="Variables.Scq.ActvInjctnTypCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		InjctnTypCd,
						InjctnTypTxt,
						case<!--- Reorder cash types first, then non-cash, then standby debt, then other: --->
						when InjctnTypCd = 'C'						then 1
						when InjctnTypCd = 'G'						then 2
						when InjctnTypCd = 'D'						then 3
						when InjctnTypCd = 'A'						then 4
						when InjctnTypCd = 'S'						then 5
						when InjctnTypCd = 'O'						then 6
						else										7
						end											AS displayorder,
						InjctnTypCd									AS code,
						InjctnTypTxt								AS description
			from		#Variables.OracleSchemaName#.InjctnTypCdTbl
			where		(	(SYSDATE-InjctnTypStrtDt) >= 0)
			and			(				InjctnTypEndDt				is null
						or	(SYSDATE-InjctnTypEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatTypCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatTypCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCollatTypCd,
						LoanCollatTypDescTxt,
						LoanCollatTypCd								AS code,
						LoanCollatTypDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanCollatTypCdTbl
			where		(	(SYSDATE-LoanCollatTypStrtDt) >= 0)
			and			(				LoanCollatTypEndDt			is null
						or	(SYSDATE-LoanCollatTypEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCollatValSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanCollatValSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCollatValSourcCd,
						LoanCollatValSourcDescTxt,
						LoanCollatValSourcCd						AS code,
						LoanCollatValSourcDescTxt					AS description
			from		#Variables.OracleSchemaName#.LoanCollatValSourcTbl
			where		(	(SYSDATE-LoanCollatValSourcStrtDt) >= 0)
			and			(				LoanCollatValSourcEndDt		is null
						or	(SYSDATE-LoanCollatValSourcEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanCrdtUnavRsnCdTbl">
			<cfquery name="Variables.Scq.ActvLoanCrdtUnavRsnCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanCrdtUnavRsnCd,
						LoanCrdtUnavRsnDescTxt,
						LoanCrdtUnavRsnCd							AS code,
						LoanCrdtUnavRsnDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanCrdtUnavRsnCdTbl
			where		(	(SYSDATE-LoanCrdtUnavRsnStrtDt) >= 0)
			and			(				LoanCrdtUnavRsnEndDt		is null
						or	(SYSDATE-LoanCrdtUnavRsnEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanFinanclStmtSourcTbl">
			<cfquery name="Variables.Scq.ActvLoanFinanclStmtSourcTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanFinanclStmtSourcCd,
						LoanFinanclStmtSourcDescTxt,
						LoanFinanclStmtSourcCd						AS code,
						LoanFinanclStmtSourcDescTxt					AS description
			from		#Variables.OracleSchemaName#.LoanFinanclStmtSourcTbl
			where		(	(SYSDATE-LoanFinanclStmtSourcStrtDt) >= 0)
			and			(				LoanFinanclStmtSourcEndDt	is null
						or	(SYSDATE-LoanFinanclStmtSourcEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFDisastrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFDisastrTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanMFDisastrCd,
						LoanMFDisastrDescTxt,
						LoanMFDisastrCd								AS code,
						LoanMFDisastrDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanMFDisastrTypTbl
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanMFPrcsMthdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanMFPrcsMthdTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanMFPrcsMthdCd,
						LoanMFPrcsMthdDescTxt,
						LoanMFPrcsMthdCd							AS code,
						LoanMFPrcsMthdDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanMFPrcsMthdTypTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPartLendrTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPartLendrTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPartLendrTypCd,
						LoanPartLendrTypDescTxt,
						case<!--- Force Participating to the top and Others to the end: --->
						when LoanPartLendrTypCd = 'P'				then 1
						when LoanPartLendrTypCd = 'I'				then 2
						when LoanPartLendrTypCd = 'G'				then 3
						when LoanPartLendrTypCd = 'O'				then 4
						else										5
						end											AS displayorder,
						LoanPartLendrTypCd							AS code,
						LoanPartLendrTypDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanPartLendrTypTbl
			where		(	(SYSDATE-LoanPartLendrTypStrtDt) >= 0)
			and			(				LoanPartLendrTypEndDt		is null
						or	(SYSDATE-LoanPartLendrTypEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPckgSourcTypTbl">
			<cfquery name="Variables.Scq.ActvLoanPckgSourcTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPckgSourcTypCd,
						LoanPckgSourcTypDescTxt,
						LoanPckgSourcTypCd							AS code,
						LoanPckgSourcTypDescTxt						AS description
			from		#Variables.OracleSchemaName#.LoanPckgSourcTypTbl
			where		(	(SYSDATE-LoanPckgSourcTypStrtDt) >= 0)
			and			(				LoanPckgSourcTypEndDt		is null
						or	(SYSDATE-LoanPckgSourcTypEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanPrevFinanStatTbl">
			<cfquery name="Variables.Scq.ActvLoanPrevFinanStatTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanPrevFinanStatCd,
						LoanPrevFinanStatDescTxt,
						LoanPrevFinanStatCd							AS code,
						LoanPrevFinanStatDescTxt					AS description
			from		#Variables.OracleSchemaName#.LoanPrevFinanStatTbl
			where		(	(SYSDATE-LoanPrevFinanStatStrtDt) >= 0)
			and			(				LoanPrevFinanStatEndDt		is null
						or	(SYSDATE-LoanPrevFinanStatEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanProcdTypTbl">
			<cfquery name="Variables.Scq.ActvLoanProcdTypTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanProcdTypCd,
						LoanProcdTypDescTxt,
						ProcdTypCd,<!--- Ignoring LoanProcdTypCmnt for now, because it's always null. --->
						ProcdTypCd || LoanProcdTypCd					AS code,
						LoanProcdTypDescTxt							AS description
			from		#Variables.OracleSchemaName#.LoanProcdTypTbl
			where		(	(SYSDATE-LoanProcdTypCdStrtDt) >= 0)
			and			(				LoanProcdTypCdEndDt			is null
						or	(SYSDATE-LoanProcdTypCdEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvLoanStatCdTbl">
			<cfquery name="Variables.Scq.ActvLoanStatCdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		LoanStatCd,
						LoanStatDescTxt,
						LoanStatCd									AS code,
						LoanStatDescTxt								AS description
			from		#Variables.OracleSchemaName#.LoanStatCdTbl
			where		(	(SYSDATE-LoanStatStrtDt) >= 0)
			and			(				LoanStatEndDt				is null
						or	(SYSDATE-LoanStatEndDt) <= 0)
			order by	code
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvMfSubPrgrmCdTbl">
			<cfquery name="Variables.Scq.ActvMfSubPrgrmCdTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		MfSubPrgrmCd,
						MfSubPrgrmDesc,
						MfSubPrgrmCd								AS code,
						MfSubPrgrmDesc								AS description
			from		#Variables.OracleSchemaName#.MfSubPrgrmCdTbl
			where		(	(SYSDATE-MfSubPrgrmStrtDt) >= 0)
			and			(				MfSubPrgrmEndDt				is null
						or	(SYSDATE-MfSubPrgrmEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrcsMthdTbl">
			<cfquery name="Variables.Scq.ActvPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description
			from		#Variables.OracleSchemaName#.PrcsMthdTbl
			where		(	(SYSDATE-PrcsMthdStrtDt) >= 0)
			and			(				PrcsMthdEndDt				is null
						or	(SYSDATE-PrcsMthdEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllPrcsMthdTbl">
			<cfquery name="Variables.Scq.AllPrcsMthdTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrcsMthdCd,
						PrcsMthdDesc,
						PrcsMthdStrtDt,
						PrcsMthdEndDt,
						PrcsMthdCd									AS code,
						PrcsMthdDesc								AS description
			from		#Variables.OracleSchemaName#.PrcsMthdTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmTbl">
			<cfquery name="Variables.Scq.ActvPrgrmTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description
			from		#Variables.OracleSchemaName#.PrgrmTbl
			where		(	(SYSDATE-PrgrmStrtDt) >= 0)
			and			(				PrgrmEndDt					is null
						or	(SYSDATE-PrgrmEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllPrgrmTbl">
			<cfquery name="Variables.Scq.AllPrgrmTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrgrmDesc,
						PrgrmMFCd,
						PrgrmMFTxt,
						PrgrmStrtDt,
						PrgrmEndDt,
						PrgrmCd										AS code,
						PrgrmDesc									AS description
			from		#Variables.OracleSchemaName#.PrgrmTbl
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvPrgrmValidTbl">
			<cfquery name="Variables.Scq.ActvPrgrmValidTbl"			datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		PrgrmCd,
						PrcsMthdCd,
						SpcPurpsLoanCd,
						ProcdTypCd,
						PIMSPrgrmId,
						SubPrgrmMFCd,
						CohortCd,
						LoanMFSpcPurpsCd,
						LoanMFPrcsMthdCd,
						LoanMFSTARInd,
						LoanMFDisastrCd,
						PrgrmAuthOthAgrmtInd,
						PrgrmAuthAreaInd<!--- Because this table is never displayed, it doesn't have code and description columns. --->
			from		#Variables.OracleSchemaName#.PrgrmValidTbl
			where		(	(SYSDATE-PrgrmValidStrtDt) >= 0)
			and			(				PrgrmValidEndDt				is null
						or	(SYSDATE-PrgrmValidEndDt) <= 0)
			order by	PrgrmValidSeqNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvRaceCdTbl">
			<cfquery name="Variables.Scq.ActvRaceCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		RaceCd,
						RaceTxt,
						RaceMFCd,
						RaceMFTxt,
						RaceCd										AS code,
						RaceTxt										AS description
			from		#Variables.OracleSchemaName#.RaceCdTbl
			where		(	(SYSDATE-RaceStrtDt) >= 0)
			and			(				RaceEndDt					is null
						or	(SYSDATE-RaceEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvSBAOfcTbl">
			<cfquery name="Variables.Scq.ActvSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description
			from		#Variables.OracleSchemaName#.SBAOfcTbl
			where		(	(SYSDATE-SBAOfcStrtDt) >= 0)
			and			(				SBAOfcEndDt					is null
						or	(SYSDATE-SBAOfcEndDt) <= 0)
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.AllSBAOfcTbl">
			<cfquery name="Variables.Scq.AllSBAOfcTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SBAOfcCd,
						StCd,
						ZipCd5,
						ZipCd4,
						SBAOfcTypCd,
						SBAOfc1Nm,
						SBAOfc2Nm,
						SBAOfcStrNmb,
						SBAOfcStrNm,
						SBAOfcStr2Nm,
						SBAOfcStrSfxNm,
						SBAOfcCtyNm,
						SBAOfcVoicePhnNmb,
						SBAOfcFaxPhnNmb,
						SBAOfcParntOfcCd,
						SBAOfcStrtDt,
						SBAOfcEndDt,
						SBAOfcCd									AS code,
						SBAOfc1Nm									AS description
			from		#Variables.OracleSchemaName#.SBAOfcTbl
			order by	description
			</cfquery><!--- If Ofc1Nm isn't unique enough, may have to modify the definition of description. --->
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!---
		IMRtTbl (current interest rates) vary over time, so the contents of that table aren't very static. Also, they're often 
		accessed in the past. (For example, if electronic lending has a received-date, LoanAppRecvDt, it grabs the interest rate 
		for that date, rather than today's date.) Therefore a snapshot of today's interest rates is not very useful. Even when 
		today's rate **IS** needed, a snapshot from 5:00 AM (when these cached queries are refreshed) is liable to be wrong, 
		because rates are updated during the day. Furthermore, IMRtTypTbl is generally only accessed in joins to IMRtTbl. 
		Therefore, we aren't caching that table either. 				Steve Seaquist
		--->
		<cfcase				  value="Scq.ActvSpcPurpsLoanTbl">
			<cfquery name="Variables.Scq.ActvSpcPurpsLoanTbl"		datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		SpcPurpsLoanCd,
						SpcPurpsLoanDesc,
						SpcPrgrmCd,
						SpcPurpsLoanCd								AS code,
						SpcPurpsLoanDesc							AS description
			from		#Variables.OracleSchemaName#.SpcPurpsLoanTbl
			where		(	(SYSDATE-SpcPurpsLoanStrtDt) >= 0)
			and			(				SpcPurpsLoanEndDt			is null
						or	(SYSDATE-SpcPurpsLoanEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStatCdTbl">
			<cfquery name="Variables.Scq.ActvStatCdTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		StatCd,
						StatTxt,
						StatCdDispInd,
						StatCdDispOrdNmb							AS displayorder,
						StatCd										AS code,
						StatTxt										AS description
			from		#Variables.OracleSchemaName#.StatCdTbl
			where		(	(SYSDATE-StatCdStrtDt) >= 0)
			and			(				StatCdEndDt					is null
						or	(SYSDATE-StatCdEndDt) <= 0)
			order by	displayorder
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvStTbl">
			<cfquery name="Variables.Scq.ActvStTbl"					datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		StCd,
						StNm,
						StCd										AS code,
						StNm										AS description
			from		#Variables.OracleSchemaName#.StTbl
			where		(	(SYSDATE-StStrtDt) >= 0)
			and			(				StEndDt						is null
						or	(SYSDATE-StEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.ActvVetTbl">
			<cfquery name="Variables.Scq.ActvVetTbl"				datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		VetCd,
						VetTxt,
						VetMFCd,
						VetMFDescTxt,
						VetGrpCd,
						VetCd										AS code,
						VetTxt										AS description
			from		#Variables.OracleSchemaName#.VetTbl
			where		(	(SYSDATE-VetStrtDt) >= 0)
			and			(				VetEndDt					is null
						or	(SYSDATE-VetEndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.partner") is just a placeholder pseudo-queryname, used to create the partner substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.partner.querynames: 
		--->
		<cfcase				  value="Scq.partner">
			<cfset Variables.Scq.partner							= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.partner.AllPrgrmTbl">
			<cfquery name="Variables.Scq.partner.AllPrgrmTbl"		datasource="partner_scheduled" dbtype="Sybase11">
			select		PrgrmId,
						PrgrmDesc,
						PrgrmDocReqdInd,
						PrgrmEffDt,
						PrgrmFormId,
						ValidPrgrmTyp,
						PrgrmAreaOfOperReqdInd
			from		PrgrmTbl
			where		PrgrmDispOrdNmb								!= 0
			order by	PrgrmDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.partner.AllSearchSubCatTyps">
			<cfquery name="Variables.Scq.partner.AllSearchSubCatTyps"	datasource="partner_scheduled" dbtype="Sybase11">
			select		v.ValidSBAPrtPrimCatTyp,
						c.ValidPrtPrimCatTypDesc,
						v.ValidPrtSubCatTyp,
						v.ValidPrtSubCatDesc,
						v.RolePrivilegePrgrmId
			from		ValidPrtSubCatTbl							v,
						ValidPrtPrimCatTbl							c
			where		v.ValidSBAPrtPrimCatTyp						= c.ValidPrtPrimCatTyp
			order by	c.ValidPrtPrimCatDispOrdNmb,
						c.ValidPrtPrimCatTypDesc,
						v.ValidPrtSubCatDesc
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.partner.AllValidLocTypTbl">
			<cfquery name="Variables.Scq.partner.AllValidLocTypTbl"	datasource="partner_scheduled" dbtype="Sybase11">
			select		ValidLocTyp,
						ValidLocTypDesc
			from		ValidLocTypTbl
			where		ValidLocTypDispOrdNmb						!= 0
			order by	ValidLocTypDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.pronet") is just a placeholder pseudo-queryname, used to create the pronet substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.pronet.querynames. 
		For the remaining Scq.pronet.querynames, the cfswitch at the very top of this cfloop sets OracleSchemaName correctly. 
		--->
		<cfcase				  value="Scq.pronet">
			<cfset Variables.Scq.pronet								= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvCountryCdTblForeign">
			<cfquery name="Variables.Scq.pronet.ActvCountryCdTblForeign"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CountryCd									code,
						CountryNm									description
			from		#Variables.OracleSchemaName#.CountryCdTbl
			where		DomForInd									> 0 /* indicating foreign */
			and			PubBanInd									= 0
			and			PvtBanInd									= 0
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllCountryCdTbl">
			<cfquery name="Variables.Scq.pronet.AllCountryCdTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CountryCd									code,
						CountryNm									description,
						DomForInd,
						PubBanInd,
						PvtBanInd
			from		#Variables.OracleSchemaName#.CountryCdTbl
			order by	CountryNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvFrmlDtrmntnCtgryTbl">
			<cfquery name="Variables.Scq.pronet.ActvFrmlDtrmntnCtgryTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		CtgryCd										code,
						CtgryNm										description
			from		#Variables.OracleSchemaName#.FrmlDtrmntnCtgryTbl
			where		(	(SYSDATE-StartDt) >= 0)
			and			(				EndDt						is null
						or	(SYSDATE-EndDt) <= 0)
			order by	description
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMainActvtyTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMainActvtyTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ExportMainActvtyCd							code,
						ExportMainActvtyDescTxt						description
			from		#Variables.OracleSchemaName#.ExportMainActvtyTypTbl
			order by	ExportMainActvtyCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvExportMrktTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvExportMrktTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		ExportMrktTypCd								code,
						ExportMrktTypDescTxt						description
			from		#Variables.OracleSchemaName#.ExportMrktTypTbl
			order by	ExportMrktTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMAreaTypCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.LogAct									= "retrieve DSBS area codes for 'server cached queries'">
			<cfset Variables.cfprname								= "Variables.ScqIMAreaTypCdTbl">
			<cfinclude template="/cfincludes/oracle/#Variables.OracleSchemaName#/spc_#Ucase('IMAreaTypCdSelTSP')#.cfm">
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<!--- Code and description are not returned by the stored procedure, so we need to do a query-of-queries: --->
			<cfquery name="Variables.Scq.pronet.ActvIMAreaTypCdTbl"	dbtype="query">
			select		IMAreaTypCd									code,
						IMAreaTypDescTxt							description
			from		Variables.ScqIMAreaTypCdTbl
			order by	IMAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvIMQAStdCdTbl">
			<cfset Variables.Identifier								= 4>
			<cfset Variables.LogAct									= "retrieve DSBS quality assurance standard codes for 'server cached queries'">
			<cfset Variables.cfprname								= "Variables.ScqIMQAStdCdTbl">
			<cfinclude template="/cfincludes/oracle/#Variables.OracleSchemaName#/spc_#UCase('IMQAStdCdSelTSP')#.cfm">
			<cfif Variables.TxnErr><cfinclude template="dsp_errmsg.cfm"><cfabort></cfif>
			<!--- Eliminate deleted codes, because there isn't any active/inactive indicator: --->
			<cfquery name="Variables.Scq.pronet.ActvIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		Variables.ScqIMQAStdCdTbl
			where		IMQAStdCd									in ('B','A','I','F','E')
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.AllIMQAStdCdTbl">	<!--- Requires ActvIMQAStdCdTbl (for Variables.ScqIMQAStdCdTbl) --->
			<cfquery name="Variables.Scq.pronet.AllIMQAStdCdTbl"	dbtype="query">
			select		IMQAStdCd									code,
						IMQAStdDescTxt								description
			from		Variables.ScqIMQAStdCdTbl
			order by	IMQAStdDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.pronet.ActvMntrAreaTypTbl">
			<cfquery name="Variables.Scq.pronet.ActvMntrAreaTypTbl"	datasource="#Variables.db#" dbtype="#Variables.dbtype#">
			select		MntrAreaTypCd								code,
						MntrAreaTypDescTxt							description
			from		#Variables.OracleSchemaName#.MntrAreaTypTbl
			order by	MntrAreaTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<!--- ********************************************************************************************************** --->
		<!---
		The following ("Scq.technet") is just a placeholder pseudo-queryname, used to create the technet substructure at the 
		proper time in the build of Variables.Scq. Therefore, it must be defined before all other Scq.technet.querynames. 
		For the remaining Scq.technet.querynames, the cfswitch at the very top of this cfloop sets OracleSchemaName correctly. 
		--->
		<cfcase				  value="Scq.technet">
			<cfset Variables.Scq.technet							= StructNew()>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdInvestSaleTypCdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdInvestSaleTypCdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdInvestSaleTypCd,
						IMAwrdInvestSaleSourcCd,
						IMAwrdInvestSaleDescTxt
			from		#Variables.OracleSchemaName#.IMAwrdInvestSaleTypCdTbl
			where		(		(IMAwrdInvestSaleStrtDt				is not null)
						and	(	(IMAwrdInvestSaleStrtDt				- sysdate) <= 0)
						and	(	(IMAwrdInvestSaleEndDt				is null)
							or ((IMAwrdInvestSaleEndDt				- sysdate) >= 0)
							)
						)
			order by	DsplyOrd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdMentrTypCdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdMentrTypCdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdMentrTypCd,
						IMAwrdMentrDescTxt,
						IMAwrdMentrTypCd							code,
						IMAwrdMentrDescTxt							description
			from		#Variables.OracleSchemaName#.IMAwrdMentrTypCdTbl
			where		(		(IMAwrdMentrStrtDt					is not null)
						and	(	(IMAwrdMentrStrtDt					- sysdate) <= 0)
						and	(	(IMAwrdMentrEndDt					is null)
							or ((IMAwrdMentrEndDt					- sysdate) >= 0)
							)
						)
			order by	IMAwrdMentrDispOrdNmb
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMAwrdSECdTbl">
			<cfquery name="Variables.Scq.technet.ActvIMAwrdSECdTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMAwrdSECd,
						IMAwrdSEDescTxt,
						IMAwrdSECd									code,
						IMAwrdSEDescTxt								description
			from		#Variables.OracleSchemaName#.IMAwrdSECdTbl
			where		(		(IMAwrdSEStrtDt						is not null)
						and	(	(IMAwrdSEStrtDt						- sysdate) <= 0)
						and	(	(IMAwrdSEEndDt						is null)
							or ((IMAwrdSEEndDt						- sysdate) >= 0)
							)
						)
			order by	IMAwrdSEDescTxt
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyTbl">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyNm,
						IMFedAgncySeqNmb							code,
						IMFedAgncyNm								description
			from		#Variables.OracleSchemaName#.IMFedAgncyTbl
			where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
			and			IMFedAgncyActvInactvInd						 = 0
			order by	IMFedAgncyNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyTbl4XML">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyTbl4XML"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyNm,
						IMFedAgncyNm								code,<!--- XML uses letter codes --->
						IMFedAgncyNm								description
			from		#Variables.OracleSchemaName#.IMFedAgncyTbl
			where		IMFedAgncySeqNmb							!= 99 <!--- 99 = SBA, which has no awards --->
			and			IMFedAgncyActvInactvInd						 = 0
			and			IMFedAgncyNm								not in ('DOI','NRC')<!--- No longer submitting awards. --->
			order by	IMFedAgncyNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyBrTbl">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyBrTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyBrSeqNmb,
						IMFedAgncyBrNm,
						IMFedAgncyBrActvInactvInd,
						IMFedAgncyBrCreatUserId,
						IMFedAgncyBrCreatDt,
						upper(IMFedAgncyBrNm)						UpperBrNm,
						IMFedAgncyBrSeqNmb							code,
						IMFedAgncyBrNm								description
			from		#Variables.OracleSchemaName#.IMFedAgncyBrTbl
			where		IMFedAgncyBrActvInactvInd					= 0
			order by	UpperBrNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMFedAgncyBrTbl4XML">
			<cfquery name="Variables.Scq.technet.ActvIMFedAgncyBrTbl4XML"	datasource="#db#" dbtype="#dbtype#">
			select		IMFedAgncySeqNmb,
						IMFedAgncyBrSeqNmb,
						IMFedAgncyBrNm,
						IMFedAgncyBrActvInactvInd,
						IMFedAgncyBrCreatUserId,
						IMFedAgncyBrCreatDt,
						upper(IMFedAgncyBrNm)						UpperBrNm,
						upper(IMFedAgncyBrNm)						code,<!--- XML uses letter codes --->
						IMFedAgncyBrNm								description
			from		#Variables.OracleSchemaName#.IMFedAgncyBrTbl
			where		IMFedAgncyBrActvInactvInd					= 0
			order by	UpperBrNm
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMPrgrmTbl">
			<cfquery name="Variables.Scq.technet.ActvIMPrgrmTbl"				datasource="#db#" dbtype="#dbtype#">
			select		IMPrgrmId,
						upper(IMPrgrmDescTxt)			IMPrgrmDescTxt
			from		#Variables.OracleSchemaName#.IMPrgrmTbl
			order by	IMPrgrmId
			</cfquery>
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMSTTRPrtCatTbl">
			<cfquery name="Variables.Scq.technet.ActvIMSTTRPrtCatTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMSTTRPrtCatCd,
						IMSTTRPrtCatDescTxt,
						IMSTTRPrtCatCd								code,
						IMSTTRPrtCatDescTxt							description
			from		#Variables.OracleSchemaName#.IMSTTRPrtCatTbl
			order by	IMSTTRPrtCatDescTxt
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		<cfcase				  value="Scq.technet.ActvIMSTTRPrtTypTbl">
			<cfquery name="Variables.Scq.technet.ActvIMSTTRPrtTypTbl"	datasource="#db#" dbtype="#dbtype#">
			select		IMSTTRPrtTypCd,
						IMSTTRPrtTypDescTxt,
						IMSTTRPrtTypCd								code,
						IMSTTRPrtTypDescTxt							description
			from		#Variables.OracleSchemaName#.IMSTTRPrtTypTbl
			order by	IMSTTRPrtTypCd
			</cfquery>
			<cfinclude template="/library/cfincludes/log_SleQuery.cfm">
		</cfcase>
		</cfswitch>
	</cfloop>
	<cflock scope="SERVER" type="EXCLUSIVE" timeout="30">
		<cfset Server.Scq											= Duplicate(Variables.Scq)>
		<!--- The following are temporary. (Convert all references, then get rid of them.) --->
		<cfset Server.ScqActvCountries								= Variables.Scq.ActvIMCntryCdTbl>	<!--- Temp!!! --->
		<cfset Server.ScqActvStates									= Variables.Scq.ActvStTbl>			<!--- Temp!!! --->
	</cflock>
	<!---
	NOTE: We don't do any cftry/cfcatch blocks above (except those that may have been done in pronet SPC files). 
	So if there was a problem above, we PROBABLY crashed. Therefore, if control gets here, we're totally done 
	using the following save-and-restore Scq variables. In particular, there's no need to embed Variables.ErrMsg 
	messages from this file into Variables.ErrMsg from the caller. So do nothing more elaborate than simply restoring 
	the caller's definitions of the save-and-restore Scq variables (if any). Also, eliminate all remnances of our 
	presence in the Variables scope, in case that would mess up the caller's use of SPC files: 
	--->
	<cfif IsDefined("Variables.ScqCFPRName")>
		<cfset Variables.cfprname									= Variables.ScqCFPRName>
	<cfelse>
		<cfset StructDelete(Variables, "cfprname")>
	</cfif>
	<cfif IsDefined("Variables.ScqDB")>
		<cfset Variables.db											= Variables.ScqDB>
	<cfelse>
		<cfset StructDelete(Variables, "db")>
	</cfif>
	<cfif IsDefined("Variables.ScqDBType")>
		<cfset Variables.dbtype										= Variables.ScqDBType>
	<cfelse>
		<cfset StructDelete(Variables, "dbtype")>
	</cfif>
	<cfif IsDefined("Variables.ScqErrMsg")>
		<cfset Variables.ErrMsg										= Variables.ScqErrMsg>
	<cfelse>
		<cfset StructDelete(Variables, "ErrMsg")>
	</cfif>
	<cfif IsDefined("Variables.ScqLogAct")>
		<cfset Variables.LogAct										= Variables.ScqLogAct>
	<cfelse>
		<cfset StructDelete(Variables, "LogAct")>
	</cfif>
	<cfif IsDefined("Variables.ScqOracleSchemaName")>
		<cfset Variables.OracleSchemaName							= Variables.ScqOracleSchemaName>
	<cfelse>
		<cfset StructDelete(Variables, "OracleSchemaName")>
	</cfif>
	<cfif IsDefined("Variables.ScqTxnErr")>
		<cfset Variables.TxnErr										= Variables.ScqTxnErr>
	<cfelse>
		<cfset StructDelete(Variables, "TxnErr")>
	</cfif>
	<!---
	The following were only needed temporarily during the loop. Delete them to discourage their use by the caller. 
	(Query names that aren't specifically named in ScqAvailableResultSetNames are not guaranteed to exist.) 
	--->
	<cfif IsDefined("Variables.ScqIMAreaTypCdTbl")>
		<cfset StructDelete(Variables, "ScqIMAreaTypCdTbl")>
	</cfif>
	<cfif IsDefined("Variables.ScqIMQAStdCdTbl")>
		<cfset StructDelete(Variables, "ScqIMQAStdCdTbl")>
	</cfif>
	<!---
	If this file was called directly (example: http://danube.sba.gov/library/cfincludes/bld_ServerCachedQueries.cfm), 
	go ahead and dump Variables.Scq. In the context of a scheduled task, the dump will be ignored, but in the context 
	of a manual browser execution, it can be used to verify that everything worked correctly. 
	--->
	<cfif Variables.PageName IS "bld_ServerCachedQueries.cfm">
		<cfdump label="Server Cached Queries"						var="#Variables.Scq#">
	</cfif>
</cfif>

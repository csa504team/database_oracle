<!---
AUTHOR:				Sheen Mathew Justin, Trusted Mission Solutions, Inc. for the US Small Business Administration.
DATE:				12/01/2016.
DESCRIPTION:		Saves new non-LADS loan repayment screen.
NOTES:				None.
INPUT:				Form Variables.
OUTPUT:				Saves Screen to Database
REVISION HISTORY:	12/14/2016, SMJ:	Original implementation
					12/22/2016, HA:		(OPSMDEV - 1314)Added support for validation routines for Loan Servicing Repayment Module 
--->

<cfcomponent>
	
	<cffunction name = "getPartnerByPartnerID" returnType = "query">
		<cfargument name = "PrtId" 		default = "" />
		<cfargument name = "username" 	default = "" />
		<cfargument name = "password" 	default = "" />
		<cfargument name = "db" 		default = "" />
		
		<cfquery name="getPartnerByPartnerID" username="#Arguments.username#" password="#Arguments.password#" datasource="#Arguments.db#">
			Select 1 from partner.prttbl prt
			where prt.prtid = <cfqueryparam value = "#Arguments.PrtId#" CFSQLType = "CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>
	
	<cffunction name = "getPartnerByProgram" returnType = "query">
		
		<cfargument name = "prgrmId" 	default = "" />
		<cfargument name = "operState" 	default = "" />
		<cfargument name = "ofcCd" 		default = "" />
		<cfargument name = "cntyCd" 	default = "" />
		<cfargument name = "prtStatus" 	default = "" />
		<cfargument name = "username" 	default = "" />
		<cfargument name = "password" 	default = "" />
		<cfargument name = "db" 		default = "" />
		
		<cfset Variables.prgrmId 		= Arguments.prgrmId />
		<cfset Variables.operState 		= Arguments.operState />
		<cfset Variables.ofcCd 			= Arguments.ofcCd />
		<cfset Variables.cntyCd 		= Arguments.cntyCd />
		<cfset Variables.prtStatus 		= Arguments.prtStatus />
		<cfset Variables.username 		= Arguments.username />
		<cfset Variables.password 		= Arguments.password />
		<cfset Variables.db 			= Arguments.db />
		
		<cfif IsNumeric(Variables.cntyCd)>
			<cfset Variables.cntyCd		= NumberFormat(Variables.cntyCd, "999") />
		<cfelse>
			<cfset Variables.cntyCd		= UCase(Variables.cntyCd) />
		</cfif>
		
		<cfquery name="getPartnerByProgram" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
            SELECT DISTINCT  p.PrtId
                            ,p.PrtLglNm
                            ,p.ValidPrtPrimCatTyp
                            ,p.PrtLocNm
                            ,p.ValidLocTyp
                            ,p.PhyAddrStr1Txt
                            ,p.PhyAddrCtyNm
                            ,p.PhyAddrStCd
                            ,p.PhyAddrPostCd
                            ,p.LocId
                            ,p.PrtLocFIRSNmb
                    FROM     partner.PrtSrchTbl p
                            ,partner.PrtAgrmtTbl pa
                            
                            <cfif Variables.operState neq "">
                            ,partner.PrtAreaOfOperTbl ao
                            </cfif>
                            
                            <cfif Variables.cntyCd neq "" or ( Variables.OfcCd neq "" and Variables.operState neq "" )>
                            ,sbaref.CntyTbl c
                            </cfif>
                            
                            <cfif Variables.ofcCd neq "" and Variables.operState eq "">
                            ,partner.PrtAreaOfOperTbl ao
                            ,sbaref.CntyTbl c
                            </cfif>
                            
                    WHERE    pa.PrtId          = p.PrtId
                    AND      pa.PrgrmId        = '#Variables.prgrmId#'
                    
                    <cfif Variables.operState neq "">
                    AND      pa.PrtId          = ao.PrtId
                    AND      pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
                    AND      ao.StCd           = '#Variables.operState#'
                    </cfif>
                    
                    <!--- Only include Active Areas of Operation --->
                    <cfif Variables.operState neq "" or ( Variables.OfcCd neq "" and Variables.operState eq "" )>
                    AND      ao.PrtAreaOfOperCntyEndDt IS NULL
                    </cfif>
                    
                    <!--- County --->
                    <cfif Variables.cntyCd neq "">
                    <cfif IsNumeric(Variables.cntyCd)>
                    AND      ao.CntyCd         = '#Variables.cntyCd#'
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    <cfelse>
                    AND      c.CntyNm like '#Variables.cntyCd#%'
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    </cfif>
                    </cfif>
                    
                    <cfif Variables.ofcCd neq "" and Variables.operState eq "">
                    AND      pa.PrtId          = ao.PrtId
                    AND      pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    AND      c.OrignOfcCd      = '#Variables.ofcCd#'
                    <cfelseif  Variables.ofcCd neq "" and Variables.operState neq "">
                    <cfif Variables.cntyCd eq "">
                    AND     (ao.CntyCd         = c.CntyCd AND ao.StCd = c.StCd)
                    AND      c.OrignOfcCd      = '#Variables.ofcCd#'
                    <cfelse>
                    AND      c.OrignOfcCd      = '#Variables.ofcCd#'
                    </cfif>
                    </cfif>
                    
                    <cfif Variables.statusCd eq "A">
                    AND      pa.PrtAgrmtEffDt <= sysdate
                    AND (
                            (pa.PrtAgrmtExprDt>= sysdate OR pa.PrtAgrmtExprDt is NULL)
                            AND
                            (pa.PrtAgrmtTermDt > sysdate OR pa.PrtAgrmtTermDt is NULL)
                        )
                    <cfelseif Variables.statusCd eq "I">
                    AND (
                            pa.PrtAgrmtExprDt < sysdate
                            AND pa.PrtAgrmtTermDt is NULL
                            AND
                            pa.PrtAgrmtSeqNmb = (
                                                    SELECT Max(b.PrtAgrmtSeqNmb)
                                                    FROM   partner.PrtAgrmtTbl b
                                                    WHERE pa.PrtId = b.PrtId
                                                    AND b.PrgrmId = '#Variables.prgrmId#'
                                                )
                        )
                    <cfelseif  Variables.statusCd eq "T">
                    AND     pa.PrtAgrmtTermDt<= sysdate
                    <cfelseif  Variables.statusCd eq "P">
                    AND     pa.PrtAgrmtEffDt  > sysdate
                    </cfif>
                    
                    <cfif Variables.prtStatus neq "">
                    AND     p.PrtCurStatCd = '#Variables.prtStatus#'
                    </cfif>
                    
                    ORDER BY p.PrtLglNm
		</cfquery>
		
		<cfreturn getPartnerByProgram />
	</cffunction>
	
	<cffunction name = "searchPartner" returnType = "query">
		
		<cfargument name="PartnerName" 				default="" />
		<cfargument name="PartnerNameScope" 		default="" />
		<cfargument name="PartnerType" 				default="" />
		<cfargument name="PartnerCityName" 			default="" />
		<cfargument name="PartnerCityNameScope" 	default="" />
		<cfargument name="PartnerStateCode" 		default="" />
		<cfargument name="PartnerZipCode" 			default="" />
		<cfargument name="PartnerZipCodeScope" 		default="" />
		<cfargument name="PrevLastPrtNm" 			default="" />
		<cfargument name="PrevLastPrtId" 			default="" />
		<cfargument name="QueryTyp" 				default="" />
		<cfargument name="MaxRows" 					default="" />
		<cfargument name="username" 				default="" />
		<cfargument name="password" 				default="" />
		<cfargument name="db" 						default="" />
		
		<cfset Variables.searchPartner 				= this.SearchAllPartner(	 Arguments.PartnerName
																				,Arguments.PartnerNameScope
																				,Arguments.PartnerType
																				,Arguments.PartnerCityName
																				,Arguments.PartnerCityNameScope
																				,Arguments.PartnerStateCode
																				,Arguments.PartnerZipCode
																				,Arguments.PartnerZipCodeScope
																				,Arguments.PrevLastPrtNm
																				,Arguments.PrevLastPrtId
																				,Arguments.QueryTyp
																				,Arguments.MaxRows
																				,""
																				,Arguments.username
																				,Arguments.password
																				,Arguments.db 
																			) 
		/>

		<cfreturn Variables.searchPartner />
		
	</cffunction>
	
	<cffunction name = "searchAllPartner" returnType = "query">
        
        <cfargument name="PartnerName"				default="" />
        <cfargument name="PartnerNameScope"			default="" />
        <cfargument name="PartnerType"				default="" />
        <cfargument name="PartnerCityName"			default="" />
        <cfargument name="PartnerCityNameScope"		default="" />
        <cfargument name="PartnerStateCode"			default="" />
        <cfargument name="PartnerZipCode"			default="" />
        <cfargument name="PartnerZipCodeScope"		default="" />
        <cfargument name="PrevLastPrtNm"			default="" />
        <cfargument name="PrevLastPrtId"			default="" />
        <cfargument name="QueryTyp"					default="" />
        <cfargument name="MaxRows"					default="" />
        <cfargument name="CurrentStatusCode"		default="" />
        <cfargument name="username"					default="" />
        <cfargument name="password"					default="" />
        <cfargument name="db"						default="" />
        
        <cfset Variables.PartnerName				= Arguments.PartnerName />
        <cfset Variables.PartnerNameScope			= Arguments.PartnerNameScope />
        <cfset Variables.PartnerType				= Arguments.PartnerType />
        <cfset Variables.PartnerCityName			= Arguments.PartnerCityName />
        <cfset Variables.PartnerCityNameScope		= Arguments.PartnerCityNameScope />
        <cfset Variables.PartnerStateCode			= Arguments.PartnerStateCode />
        <cfset Variables.PartnerZipCode				= Arguments.PartnerZipCode />
        <cfset Variables.PartnerZipCodeScope		= Arguments.PartnerZipCodeScope />
        <cfset Variables.PrevLastPrtNm				= Arguments.PrevLastPrtNm />
        <cfset Variables.PrevLastPrtId				= Arguments.PrevLastPrtId />
        <cfset Variables.QueryTyp					= Arguments.QueryTyp />
        <cfset Variables.MaxRows					= Arguments.MaxRows />
        <cfset Variables.CurrentStatusCode			= Arguments.CurrentStatusCode />
        <cfset Variables.username					= Arguments.username />
        <cfset Variables.password					= Arguments.password />
        <cfset Variables.db							= Arguments.db />
        
        <cfquery name="PartnerAllCategory" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#">
        Select distinct VALIDPRTPRIMCATTYP
                        From Partner.PrtSrchTbl
        </cfquery>
        <cfset Variables.PrtCategoryTp = ValueList(PartnerAllCategory.VALIDPRTPRIMCATTYP)>
        <cfset Variables.PrtCategoryTp = FindNoCase(Variables.PartnerType, Variables.PrtCategoryTp)>
                    
        <cfquery name="SearchAllPartner" username="#Variables.username#" password="#Variables.password#" datasource="#Variables.db#" maxrows="#Variables.MaxRows#">
            SELECT   ROWNUM
                    ,p.PrtId
                    ,p.PrtLglNm
                    ,p.ValidPrtPrimCatTyp
                    ,p.ValidPrtSubCatTyp
                    ,p.PrtLocNm
                    ,p.ValidLocTyp
                    ,p.PhyAddrStr1Txt
                    ,p.PhyAddrCtyNm
                    ,p.PhyAddrStCd
                    ,p.PhyAddrPostCd
                    ,p.LocId
                    ,p.PrtLocFIRSNmb
                    ,p.LocQty
                    ,p.PrtLglSrchNm
                    ,p.PHYADDRCTYSRCHNM
                    ,p.prtCurStatCd
            FROM     Partner.PrtSrchTbl p
            WHERE    1=1
            
            <cfif Variables.PartnerName NEQ ""><!---   Partner Name   --->
            <cfif Variables.PartnerNameScope EQ "S">
            AND      PRTLGLSRCHNM LIKE UPPER('#Variables.PartnerName#%')
            <cfelseif Variables.PartnerNameScope EQ "C">
            AND      PRTLGLSRCHNM LIKE UPPER('%#Variables.PartnerName#%')
            <cfelseif Variables.PartnerNameScope EQ "E">
            AND      PRTLGLSRCHNM = UPPER('#Variables.PartnerName#')
            </cfif>
            </cfif>
            
            <cfif Variables.PartnerType NEQ "null"><!---              Partner Type      --->
            <cfif Variables.PrtCategoryTp EQ 0>
            AND      VALIDPRTSUBCATTYP = '#Variables.PartnerType#'
            <cfelse>
            AND      VALIDPRTPRIMCATTYP = '#Variables.PartnerType#'
            </cfif>
            </cfif>
            
            <cfif Variables.PartnerCityName NEQ ""><!---            City        --->
                <cfif Variables.PartnerCityNameScope EQ "S">
                    AND PHYADDRCTYSRCHNM LIKE UPPER('#Variables.PartnerCityName#%')
                <cfelseif Variables.PartnerCityNameScope EQ "C">
                    AND PHYADDRCTYSRCHNM LIKE UPPER('%#Variables.PartnerCityName#%')
                <cfelseif Variables.PartnerCityNameScope EQ "E">
                    AND PHYADDRCTYSRCHNM = UPPER('#Variables.PartnerCityName#')
                </cfif>
            </cfif>
            
            <cfif Variables.PartnerStateCode NEQ ""><!---State--->
            AND PHYADDRSTCD = '#Variables.PartnerStateCode#'
            </cfif>
            
            <cfif Variables.CurrentStatusCode NEQ ""><!--- partner state code --->
            AND prtCurStatCd in (<cfqueryparam value = "#Variables.CurrentStatusCode#" CFSQLType = "CF_SQL_CHAR" list = "yes">)
            </cfif>
            
            <cfif Variables.PartnerZipCode NEQ ""><!--- ZIP --->
                <cfif Variables.PartnerZipCodeScope EQ "S">
                    AND PHYADDRPOSTCD LIKE '#Variables.PartnerZipCode#%'
                <cfelseif Variables.PartnerZipCodeScope EQ "C">
                    AND PHYADDRPOSTCD LIKE '%#Variables.PartnerZipCode#%'
                <cfelseif Variables.PartnerZipCodeScope EQ "E">
                    AND PHYADDRPOSTCD = '#Variables.PartnerZipCode#'
                </cfif>
            </cfif>
        </cfquery>
        
        <cfreturn SearchAllPartner />
        
    </cffunction>

</cfcomponent>

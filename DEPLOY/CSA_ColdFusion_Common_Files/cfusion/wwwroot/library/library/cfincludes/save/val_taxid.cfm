<!---
AUTHOR:				Steve Seaquist
DATE:				05/08/2002
DESCRIPTION:		Validates that Variables.FNam can be stored in a CF_SQL_[VAR]CHAR10 and converts to SBA TaxId format. 
NOTES:				None
INPUT:				Variables.FName (English), Variables.FNam (Form)
OUTPUT:				Variables.FVal, Variables.ErrMsg
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	07/07/2004, SRS:	Modified to detect bogus EINs. Reorganized code. Made it not depend on TaxIdInd. 
					09/03/2002, JBB:	Added code for validating the SSN digits as allocated by SSA
					05/08/2002, SRS:	Original implementation.
--->
<cfset Variables.FVal								= "">
<cfparam name="Variables.TaxIdLongMessages"			default="Yes"><!--- Configuration parameter --->
<cfif IsDefined("Form.#Variables.FNam#")>
	<cfset Variables.FVal							= Evaluate("Form.#Variables.FNam#")>
	<cfif Len(Variables.FVal) GT 0>
		<cfset Variables.BlankScreen				= "No">
		<cfif		(Len(Variables.FVal)			IS 10)
				AND	(IsNumeric(Left	(Variables.FVal, 2)))
				AND	(Mid			(Variables.FVal, 3, 1)	IS "-")
				AND	(IsNumeric(Right(Variables.FVal, 7)))>
			<!--- Okay, it's in 99-9999999 format, but does it appear to be bogus? --->
			<cfset Variables.FValCloseDigits		= 0>
			<cfset Variables.FValPrevDigit			= Left(Variables.FVal, 1)>
			<cfloop index="FValIdx" from="2" to="#Len(Variables.FVal)#">
				<cfset Variables.FValDigit			= Mid(Variables.FVal, FValIdx, 1)>
				<cfif Variables.FValDigit IS NOT "-">
					<cfset Variables.FValDiff		= Variables.FValDigit - Variables.FValPrevDigit>
					<cfif (Variables.FValDiff GE -1) AND (Variables.FValDiff LE 1)>
						<cfset Variables.FValCloseDigits
													= Variables.FValCloseDigits + 1>
					</cfif>
					<cfset Variables.FValPrevDigit	= Variables.FValDigit>
				</cfif>
			</cfloop>
			<cfif Variables.FValCloseDigits GE 6>
				<cfset Variables.FieldErrMsg		= Variables.FName & " is an invalid EIN.">
				<cfinclude template="val_FormatErrMsg.cfm">
			<cfelse>
				<cfset Variables.FVal				= Variables.PrefixEIN
													& Left	(Variables.FVal, 2)
													& Right	(Variables.FVal, 7)>
			</cfif>
		<CFELSEIF	(Len(Variables.FVal)		IS 11)
				AND	(IsNumeric(Left	(Variables.FVal, 3)))
				AND	(Mid(Variables.FVal, 4, 1)	IS "-")
				AND	(IsNumeric(Mid	(Variables.FVal, 5, 2)))
				AND	(Mid(Variables.FVal, 7, 1)	IS "-")
				AND	(IsNumeric(Right(Variables.FVal, 4)))>
			<!--- Okay, it's in 999-99-9999 format, but does it appear to be bogus? --->
			<cfset SArea							= Int(Left(Variables.FVal,3))>
			<cfif NOT (((SArea GE 1) AND (SArea LE 728)) OR ((SArea GE 750) AND (SArea LE 772)))>
				<cfif Variables.TaxIdLongMessages>
					<cfset Variables.FieldErrMsg	= "Although #Variables.FName# is in SSN format (999-99-9999), "
													& "it does not begin with a valid 'area' (first 3 digits), "
													& "as allocated by the Social Security Administration.">
				<cfelse>
					<cfset Variables.FieldErrMsg	= Variables.FName & " is an invalid SSN.">
				</cfif>
				<cfinclude template="val_FormatErrMsg.cfm">
			<cfelseif Mid(Variables.FVal,5,2)		IS "00">
				<cfif Variables.TaxIdLongMessages>
					<cfset Variables.FieldErrMsg	= "Although #Variables.FName# is in SSN format (999-99-9999), "
													& "it does not contain a valid 'group' (middle 2 digits), "
													& "as allocated by the Social Security Administration.">
				<cfelse>
					<cfset Variables.FieldErrMsg	= Variables.FName & " is an invalid SSN.">
				</cfif>
				<cfinclude template="val_FormatErrMsg.cfm">
			<cfelseif Right(Variables.FVal,4)		IS "0000">
				<cfif Variables.TaxIdLongMessages>
					<cfset Variables.FieldErrMsg	= "Although #Variables.FName# is in SSN format (999-99-9999), "
													& "it does not end with a valid last 4 digits, "
													& "as allocated by the Social Security Administration.">
				<cfelse>
					<cfset Variables.FieldErrMsg	= Variables.FName & " is an invalid SSN.">
				</cfif>
				<cfinclude template="val_FormatErrMsg.cfm">
			<cfelse>
				<cfset Variables.FVal				= Variables.PrefixSSN
													& Left	(Variables.FVal, 3)
													& Mid	(Variables.FVal, 5, 2)
													& Right	(Variables.FVal, 4)>
			</cfif>		
		<cfelse>
			<cfset Variables.FieldErrMsg			= Variables.FName & " must be in 99-9999999 or 999-99-9999 format.">
			<cfinclude template="val_FormatErrMsg.cfm">
		</cfif>
	</cfif>
</cfif>
<cfset "Variables.#Variables.FNam#"					= Variables.FVal><!--- Little-known technique --->
<cfset Variables.URLString							= Variables.URLString
													& "&#Variables.FNam#=#URLEncodedFormat(Variables.FVal)#">

	

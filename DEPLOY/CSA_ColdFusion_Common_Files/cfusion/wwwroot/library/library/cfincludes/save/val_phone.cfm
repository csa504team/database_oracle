<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) IS NOT 12>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 12 characters.</li>">
	<cfelseif	(NOT IsNumeric	(Mid(Variables.FVal, 1, 3)))
		OR						(Mid(Variables.FVal, 4, 1) IS NOT "-")
		OR		(NOT IsNumeric	(Mid(Variables.FVal, 5, 3)))
		OR						(Mid(Variables.FVal, 8, 1) IS NOT "-")
		OR		(NOT IsNumeric	(Mid(Variables.FVal, 9, 4)))>
		<cfset Variables.ErrMsg		= "#Variables.ErrMsg# <li>#Variables.FName# is not in 999-999-9999 format.</li>">
	</cfif>
</cfif>

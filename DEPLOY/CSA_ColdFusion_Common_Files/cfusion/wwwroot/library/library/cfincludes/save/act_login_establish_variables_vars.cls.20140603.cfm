<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/06/2008
DESCRIPTION:		Shared routine to copy Variables.CLS into assorted other Variables variable names. 
NOTES:

	This file is basically for Application.cfm files that want to define all manner of GLS variables at 
	the Variables scope level (not just leave them inside the Variables.CLS structure). So it's basically 
	a stepping stone for older apps that don't make good use of Variables.CLS as-is. 

	For example, Variables.CLS.IMUserTbl contains information about the currently logged-in user, such as 
	Variables.CLS.IMUserTbl.IMUserFirstNm, Variables.CLS.IMUserTbl.IMUserLastNm, etc. Apparently, typing 
	"GLS." is too much effort, so many apps reference Variables.IMUserTbl.IMUserFirstNm, etc, instead, or 
	even Variables.IMUserFirstNm, etc. This routine copies the GLS structure and all of its substructures 
	(one level further down) into the Variables scope. It wastes memory, but it keeps old code working. 

	Typical usage in an Application.cfm file (using parentheses to avoid confusing CF Server): 

		(cfset Variables.CLS						= StructNew())
		(cfset Variables.CLS.CLSAuthorized			= "No")
		(cflock scope="Session" type="Readonly" timeout="30")
			(cfif IsDefined("Session.CLS"))
				(cfset Variables.CLS				= Duplicate(Session.CLS))
			(/cfif)
		(/cflock)
		(cfif NOT Variables.CLS.CLSAuthorized)
			(!--- Kick the user out of your directory and into a login page, usually /cls/dsp_login.cfm. ---)
		(/cfif)
		(cfinclude template="/library/cfincludes/act_login_establish_variables_vars.cfm")

	New apps should only be using the GLS structure and its substructures, not the assorted other Variables 
	variables created below. At some point, we would like to have only Variables.CLS in the Variables scope, 
	but we can't do that as long as people keep assuming that its substructures are also defined in Variables. 

	In addition, if you modify GLS variables in the Session scope, you should modify just Session.CLS.whatever. 
	There is no need to modify Session.whatever too. Session.CLS should always be the most trustworthy data. 

INPUT:				Variables.CLS. 
OUTPUT:				Other Variables variables (for now). 
REVISION HISTORY:	07/02/2008, SRS:	Changes to support saving memory. 
					03/06/2008, SRS:	Original Implementation. 
--->

<cfloop index="Level1Key"							list="#StructKeyList(Variables.CLS)#">
	<cfif IsStruct(Variables.CLS[Level1Key])>
		<cfset Variables[Level1Key]					= Duplicate(Variables.CLS[Level1Key])>
		<cfloop index="Level2Key"					list="#StructKeyList(Variables.CLS[Level1Key])#">
			<cfset Variables[Level2Key]				= Variables.CLS[Level1Key][Level2Key]>
		</cfloop>
	<cfelseif CompareNoCase(Level1Key,"ArrayAllUserRoles") IS NOT 0>
		<!---
		Never copy Variables.CLS.ArrayAllUserRoles to Variables.ArrayAllUserRoles. No library routine will ever 
		refernece it as anything other than Variables.CLS.ArrayAllUserRoles. Big waste of memory to copy it. 
		--->
		<cfset Variables[Level1Key]					= Variables.CLS[Level1Key]>
	</cfif>
</cfloop>
<cfif NOT IsDefined("Variables.ArrayUserRoles")>
	<cfif NOT IsDefined("TrimArrayUserRoles")>
		<cfinclude template="/library/udf/bld_SecurityUDFs.cls.cfm">
	</cfif>
	<!---
	We don't know in this included routine whether or not the caller is a Web Service. Therefore, include roles 
	with the Web Service indicator, just in case they're needed. The caller will probably build ArrayUserRoles 
	explicitly anyway. This prevents crashes in Web Services that haven't been modified to adapt to trimmed down 
	GLS structure. 
	--->
	<cfset TrimArrayUserRoles("Yes")><!--- "Yes" = "include Web Service only roles". --->
</cfif>

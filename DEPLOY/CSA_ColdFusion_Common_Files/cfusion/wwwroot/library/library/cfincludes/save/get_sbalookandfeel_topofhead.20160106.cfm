<cfif CGI.Script_Name is "/library/cfincludes/get_sbalookandfeel_topofhead.cfm"><cfsetting enablecfoutputonly="Yes">
	<!---
	In order for DOCTYPE to be the *very* first line of the output, we need for enablecfoutputonly to occur right away, 
	before there has been any chance for a blank line to sneak into the output. Hence, cfsetting was done above, on the 
	same line as our check for self-test mode. This will allow easier HTML5 testing and conversion in the future. 
	--->
	<cfset Request.SlafTopOfHeadSelfTestMode		= "Yes">
<cfelse>
	<cfset Request.SlafTopOfHeadSelfTestMode		= "No"> 
</cfif>
<!---
AUTHOR:				Steve Seaquist
DATE:				01/31/2006
DESCRIPTION:		Builds Request.SlafHead and Request.SlafTopOfHead for use by cf_sbalookandfeel, reports, pages in 
					frames, etc. Request.SlafTopOfHead is the important one. It provides a standard, correct inclusion 
					sequence of SBA CSS files and JavaScript variables useful with CSS manipulation. 
NOTES:

	If you cfinclude get_sbalookandfeel_variables.cfm, it will include this file for you. Therefore, you would normally 
	NOT include this file directly. It exists outside of get_sbalookandfeel_variables so that the cf_sbalookandfeel can 
	override some of the generated code, based on Attributes passed to it in the custom tag call. 

	If you want to define your own DOCTYPE, html tag, head tag and title tag first, cfoutput #Request.SlafTopOfHead# just 
	after the title tag. You would then continue on to define other system-specific CSS, page-specific JavaScript and the 
	/head. 

	If you don't care to define DOCTYPE, html and head yourself, simply cfoutput #Request.Head# instead. You would then 
	continue on to define the title, #Request.SlafTopOfHead#, system-specific CSS, page-specific JavaScript and the /head. 

	And if you don't mind defining the title tag lower down, simply cfoutput #Request.SlafHeadAndTopOfHead#, then title, 
	system-specific CSS, page-specific javascript and the /head. 

	When a custom tag calls a cfinclude, the Attributes scope is not passed to the cfinclude. So cf_sbalookandfeel passes 
	Attributes namespaced as Request.SlafTopOfHead variables to prevent name conflicts with the caller's variables. 

	You do NOT have to be using SBA Look-and-Feel to use Request.SlafHead or SlafTopOfHead. In particular, developers of 
	<cfdocument format="pdf" ... > reports and other formatted reports can use it, so that the report will pick up the 
	SBA's standard fonts and styles. In order for any page to be able to use these variables, DumpObject.js, jquery.js and 
	sbalookandfeel.js are not defined here. (Instead, they're defined in cf_sbalookandfeel.) 

INPUT:				Request.SlafApplicationName, Request.SlafTopOfHead variables (all optional, normally used only by 
					cf_sbalookandfeel). ApplicationName may or may not have been defined by get_sbalookandfeel_variables. 
					Assumes all get_sbashared_variables are defined. 
OUTPUT:				Request.SlafHead, Request.SlafTopOfHead and Request.SlafHeadAndTopOfHead. 
REVISION HISTORY:	12/04/2015, SRS:	Added th1 as an override of default, now called Home Page Theme. 
					11/09/2015, SRS:	Reworked lots of stuff to be "themeable" and simpler because we no longer have 
										to support MSIE 8: Got rid of files for quirks mode, which we haven't generated 
										in over 4 years, to relieve that maintenance burden. 
					08/27/2015, SJK:	Replaced GLS text to user with CLS. 
					07/16/2015 -
					08/05/2015, SRS:	Added cachebusters for EditDateMinYearsOld, LookupCountryData and SetFormEltValue. 
					03/04/2015, SRS:	Made the color scheme considerably lighter, colors chosen by team consensus. This 
										implied setting new CachedAsOf cachebusters for 9 CSS files. Also, added cachebuster 
										for EditLoanNmb. 
					05/23/2014, SRS:	Added SlafTopOfHeadSlaf variables, which recently changed. Also SlafTopOfHead itself, 
										due to changes to sba.quirks/strict.css. 
					01/30/2014, SRS:	Added SlafTopOfHeadHiGrpUtils, which recently changed. 
					10/16/2013, SRS:	Added numerous sbalookandfeel/mainnav variables. 
					08/22/2013, SRS:	Added support for a new standard variable: Request.SlafTopOfHeadRTE. If you want 
										to convert <textarea class="rte"> tags into rich text editors, cfoutput it to the 
										web page after Request.SlafTopOfHeadjQuery. Also defined Lookup routines variables. 
					04/27/2013, SRS:	In preparation for the release of mobile, got rid of defaulting SlafRenderingMode 
										to "jqm" in development. Now, in all environments, we set the default based on the 
										user's actual browser, not just what we temporarily wanted to test a lot. 
					10/02/2012 -		Changed CachedAsOf cachebusters for jQuery 1.8.2 and jQuery Mobile 1.2.0. 
					01/09/2013, SRS:	Section 508: Changed CachedAsOf for new fieldset.radio and legend.radio classes. 
					09/04/2012, SRS:	Set default for SlafHTML5 to "Yes" to turn on Strict Mode, even in production. 
					09/15/2011 -
					06/28/2012, SRS:	In preparation for MSIE 10, which will TOTALLY mess us up with regard to QuirksMode, 
										began turning HTML5 (strict mode), and began coding to fix strict mode related 
										problems. Moved MainNav TextOnly logic to cf_sbalookandfeel and cf_mainnav. Added 
										a new variable, Request.SlafjQuery, for more jquery.js with CachedAsOf appended. 
										Also added support for SlafRenderingMode (mobile's default rendering mode). Made the 
										conditional comment that controls sba.msie.css use "lte IE 7", which works for all 
										3 components of that css file (inlineblock, fieldset and tbl classes). 
					08/01/2011, SRS:	Defined new .mainnavexternalsys class for standard appearance of inter-system 
										main navigation buttons. (Bumped up CachedAsOf on 3 CSS files.) 
					03/28/2011, SRS:	(Same check-out as 11/09.) Fixed IE 9's non-support of inline-block and other 
										things they said they'd support by now by basing the inclusion of MSIE css classes 
										on IE alone, not "lt IE 9" anymore. 
					11/09/2010, SRS:	Updated CachedAsOf times for more efficient css and js files. 
					10/12/2010, SRS:	Added support for sba.msie.noscript.css. Consolidated MSIE conditional comments 
										into only one (with links and script together). Updated CachedAsOf times. 
					09/22/2010, SRS:	Added ability to use AppendCachedAsOf (for greatly improves reliability after 
										JS/CSS edits), but not using it just yet. Improved HTML5 support. 
					09/08/2010, SRS:	11th hour fix for MSIE 8. Treat it the same as MSIE 6 and 7. 
					08/10/2010, SRS:	Began prototyping HTML5. (Transparent to everyone but me, SRS.) 
					07/16/2010, SRS:	"New Look-and-Feel". Made Request.SlafTextOnly affect SlafTopOfHeadTextOnly. 
					06/10/2010, SRS:	Changed Variables.AppicationName to Request.SlafApplicationName, so that this 
										routine won't mess up if cfincluded from within a custom tag. 
					03/22/2010, SRS:	Original implementation. 
--->

<!--- Configuration Parameters: --->

<cfif NOT IsDefined("Request.SlafHTML5")>
	<!---
	Request.SlafHTML5 should have been defined by get_sbalookandfeel_variables, which was apparently not called. Therefore, 
	we set a default and override it with whatever's in the Session scope, as get_sbalookandfeel_variables would have done. 
	To quickly default development to "Yes", move the line with the boolean expression below the "No". 
	To quickly revert to the "No" default, move the line with the boolean expression back up above the "No". 
	--->
	<cfset Request.SlafHTML5						= "Yes"><!--- Not used anymore. (Always "Yes".) --->
	<cftry>
		<cflock scope="SESSION" type="READONLY" timeout="30">
			<cfif IsDefined("Session.Slaf") and IsStruct(Session.Slaf)>
				<cfset StructAppend(Request, Session.Slaf, "Yes")>
			</cfif>
		</cflock>
		<cfcatch type="Any"><!--- If Session scope doesn't exist, no problem, do nothing . ---></cfcatch>
	</cftry>
</cfif>

<!--- Runtime Options: --->

<cfif NOT IsDefined("Request.SlafDevTestProd")><!--- Passed to the user, below, in the generated JavaScript. --->
	<cfinclude template="get_sbashared_variables.cfm"><!--- Safe to omit path. We're both in /library/cfincludes. --->
</cfif>

<cfparam name="Request.SlafButtons3D"				default="Yes"><!--- Until MSIE 9 is commonplace. --->
<cfif NOT IsDefined("Request.SlafRenderingMode")>
	<cfif Request.SlafBrowser.Mobile>				<cfset Request.SlafRenderingMode	= "jqm">
	<cfelse>										<cfset Request.SlafRenderingMode	= "dtv"></cfif>
</cfif>
<cfparam name="Request.SlafRenderingMode"			default="dtv"><!--- Desktop View is still the default. --->
<cfparam name="Request.SlafTopOfHeadjQ1Ver"			default="">
<cfparam name="Request.SlafTopOfHeadjQ2Ver"			default="none"><!--- Temporary jQuery 2.0 suppression code, for now. --->
<cfparam name="Request.SlafTopOfHeadjQM"			default="No">
<cfparam name="Request.SlafTopOfHeadjQMVer"			default="">
<cfif Request.SlafTopOfHeadjQMVer is ".latest"><!--- = 1.3.1. Facilitate 1.3.1 testing by setting recommended jQuery: --->
	<!--- Only version 1.9.1 is recommended at http://jquerymobile.com/blog/2013/04/10/announcing-jquery-mobile-1-3-1/ --->
	<cfset Request.SlafTopOfHeadjQ1Ver				= "-1.9.1.min">
	<cfset Request.SlafTopOfHeadjQ2Ver				= "none">
</cfif>
<cfparam name="Request.SlafTopOfHeadLang"			default="en-US">
<cfparam name="Request.SlafTopOfHeadLibURL"			default="/library">
<cfparam name="Request.SlafTopOfHeadRTEType"		default="MCE"><!--- "FCK" (FCKEditor) or "MCE" (tinyMCE) --->

<!--- Get Self-Test Mode URL parameters (to test multiple features): --->

<cfif Request.SlafTopOfHeadSelfTestMode>
	<cfif IsDefined("URL.jQ1Ver")>
		<cfset Request.SlafTopOfHeadjQ1Ver			= URL.jQ1Ver>
	</cfif>
	<cfif IsDefined("URL.jQ2Ver")>
		<cfset Request.SlafTopOfHeadjQ2Ver			= URL.jQ2Ver>
	</cfif>
	<cfif IsDefined("URL.jQMVer")>
		<cfset Request.SlafTopOfHeadjQM				= "Yes">
		<cfset Request.SlafTopOfHeadjQMVer			= URL.jQMVer>
		<cfset Request.SlafRenderingMode			= "jqm">
	</cfif>
	<cfif IsDefined("URL.Render")>
		<cfset Request.SlafRenderingMode			= URL.Render>
	</cfif>
	<cfif IsDefined("URL.RTEType")>
		<cfset Request.SlafTopOfHeadRTEType			= URL.RTEType>
	</cfif>
</cfif>

<!--- Generate the variables: --->

<cfsavecontent variable="Request.SlafHead">
	<cfoutput><!doctype html>
<html lang="#Request.SlafTopOfHeadLang#" dir="ltr">
<head>
<meta charset="utf-8"><cfif Request.SlafRenderingMode is "jqm">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"></cfif>
<meta http-equiv="X-UA-Compatible" content="IE=Edge"></cfoutput>
</cfsavecontent>

<!--- jQuery: --->

<cfsavecontent variable="Request.SlafTopOfHeadjQuery">
	<cfset Variables.SlafTopOfHeadjQ1URL			= "#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery">
	<cfif Len(Request.SlafTopOfHeadjQ1Ver) is 0>
		<cfset Variables.SlafTopOfHeadjQ1URL		&= ".js?CachedAsOf=2012-09-21T15:37"><!--- 1.8.2 --->
	<cfelse><!--- AppendCachedAsOf is significantly more expensive than hardcoded, so only use in overrides: --->
		<cfif NOT IsDefined("AppendCachedAsOf")>
			<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
		</cfif>
		<cfset Variables.SlafTopOfHeadjQ1URL		&= "#Request.SlafTopOfHeadjQ1Ver#.js">
		<cfset Variables.SlafTopOfHeadjQ1URL		= AppendCachedAsOf(Variables.SlafTopOfHeadjQ1URL)>
	</cfif>
	<cfif Request.SlafTopOfHeadjQ2Ver is "none">
		<cfoutput>
<script src="#Variables.SlafTopOfHeadjQ1URL#"></script><cfif Len(Request.SlafTopOfHeadjQ1Ver) is 0><!-- 1.8.2 --></cfif></cfoutput>
	<cfelse>
		<cfset Variables.SlafTopOfHeadjQ2URL		= "#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery">
		<cfif Len(Request.SlafTopOfHeadjQ2Ver) is 0>
			<cfset Variables.SlafTopOfHeadjQ2URL	&= "2.js?CachedAsOf=2013-04-18T23:32"><!--- 2.0.0 --->
		<cfelse>
			<cfset Variables.SlafTopOfHeadjQ2URL	&= "#Request.SlafTopOfHeadjQ2Ver#.js">
			<!--- AppendCachedAsOf is significantly more expensive than hardcoded, so use only in overrides (testing): --->
			<cfif NOT IsDefined("AppendCachedAsOf")>
				<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
			</cfif>
			<cfset Variables.SlafTopOfHeadjQ2URL	= AppendCachedAsOf(Variables.SlafTopOfHeadjQ2URL)>
		</cfif>
		<!--- Define Variables in case needed by caller in other situations: --->
		<cfset Variables.MSIEjQBeg					= "<!--[if lt IE 9]>">
		<cfset Variables.MSIEjQEnd					= "<![endif]-->">
		<cfset Variables.NonMSIEjQBeg				= "<!--[if gte IE 9]><!-->">
		<cfset Variables.NonMSIEjQEnd				= "<!--<![endif]-->">
		<cfoutput>#Request.SlafEOL#
#Variables.MSIEjQBeg#
<script src="#Variables.SlafTopOfHeadjQ1URL#"></script>
#Variables.MSIEjQEnd##Request.SlafEOL#
#Variables.NonMSIEjQBeg#
<script src="#Variables.SlafTopOfHeadjQ2URL#"></script>
#Variables.NonMSIEjQEnd#</cfoutput>
	</cfif>
</cfsavecontent>

<!--- Rich Text Editors (instantiated with jQuery), included after jQuery: --->

<cfsavecontent variable="Request.SlafTopOfHeadRTE">
	<cfoutput><!--- Shared embellishments to all SBA rich text editors: --->
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.AllRichTextEditors.SBA.js?CachedAsOf=2013-12-17T22:10"></script></cfoutput>
	<cfswitch expression="#Request.SlafTopOfHeadRTEType#">
	<cfcase value="FCK">
		<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.MetaData_and_FCKEditor.CF.js?CachedAsOf=2013-08-23T14:29"></script>
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.FCKEditor.SBA.js?CachedAsOf=2013-12-17T21:41"></script></cfoutput>
	</cfcase>
	<cfcase value="MCE404">
		<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/rte/tinymce_4.0.4_jquery/js/tinymce/jquery.tinymce.min.js?CachedAsOf=2013-08-21T18:14"></script>
<script>
gMCEDirectory									= "/library/rte/tinymce_4.0.4_jquery/js/tinymce/";
gMCEScript										= "tinymce.min.js?CachedAsOf=2013-08-21T18:14";
// Warning: We don't have a plugins/spellchecker/rpc.cfm for 4.0.4 yet. 
</script>
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.tinymce.SBA.js?CachedAsOf=2013-12-17T21:41"></script></cfoutput>
	</cfcase>
	<cfdefaultcase><!--- "MCE" = version 3.5.8, also documented in HTML comment. --->
		<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.tinymce.js?CachedAsOf=2013-08-21T18:16"></script><!-- 3.5.8 -->
<script src="#Request.SlafTopOfHeadLibURL#/rte/jquery.tinymce.SBA.js?CachedAsOf=2013-12-17T21:41"></script></cfoutput>
	</cfdefaultcase>
	</cfswitch>
</cfsavecontent>

<!--- jQuery Mobile: --->

<cfif Request.SlafTopOfHeadjQM>
	<cfset Variables.SlafTopOfHeadjQMDirCss			= "#Request.SlafTopOfHeadLibURL#/css/jquery.mobile">
	<cfset Variables.SlafTopOfHeadjQMDirJs			= "#Request.SlafTopOfHeadLibURL#/javascripts/jquery/jquery.mobile">
	<cfif Request.SlafRenderingMode is "jqm">
		<cfset Variables.SlafTopOfHeadjQMURLCss		= "#Variables.SlafTopOfHeadjQMDirCss#/jquery.mobile">
		<cfset Variables.SlafTopOfHeadjQMURLJs		= "#Variables.SlafTopOfHeadjQMDirJs#/jquery.mobile">
		<cfif Len(Request.SlafTopOfHeadjQMVer) is 0>
			<cfset Variables.SlafTopOfHeadjQMURLCss	&= ".css?CachedAsOf=2012-10-02T17:11"><!--- jqm 1.2.0 --->
			<cfset Variables.SlafTopOfHeadjQMURLJs	&= ".js?CachedAsOf=2012-10-02T17:11"><!--- jqm 1.2.0 --->
		<cfelse>
			<cfset Variables.SlafTopOfHeadjQMURLCss	&= "#Request.SlafTopOfHeadjQMVer#.css">
			<cfset Variables.SlafTopOfHeadjQMURLJs	&= "#Request.SlafTopOfHeadjQMVer#.js">
			<!--- AppendCachedAsOf is significantly more expensive than hardcoded, so use only in overrides (testing): --->
			<cfif NOT IsDefined("AppendCachedAsOf")>
				<cfinclude template="/library/udf/bld_AppendCachedAsOf.cfm">
			</cfif>
			<cfset Variables.SlafTopOfHeadjQMURLCss	= AppendCachedAsOf(Variables.SlafTopOfHeadjQMURLCss)>
			<cfset Variables.SlafTopOfHeadjQMURLJs	= AppendCachedAsOf(Variables.SlafTopOfHeadjQMURLJs)>
		</cfif>
	</cfif>
</cfif>

<!--- LookupZipToDropdown, etc (various utility routines): --->

<cfsavecontent variable="Request.SlafTopOfHeadAjax"><!--- Required by SBA .ajax.js routines (before jQuery AJAX existed). --->
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ajax/GetXMLHttpRequest.js?CachedAsOf=2006-07-06T19:36"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCalendarPopup">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/CalendarPopup.js?CachedAsOf=2013-04-26T12:26"></script>
<script>
gCalendarPopup									= new CalendarPopup();
gCalendarPopup.showNavigationDropdowns();		// Popups will have month and day drop-down menus. 
</script>
</cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadClearForm">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ClearForm.js?CachedAsOf=2013-08-23T18:08"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSS_dtv">
	<cfoutput><!--- sba.dtv.css is also defined in Request.SlafTopOfHead, below, when rendering mode is not "jqm". --->
<link  href="#Request.SlafTopOfHeadLibURL#/css/jquery.mobile/sba.dtv.css?CachedAsOf=2012-06-20T22:15"          rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSFontAwesome">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/font-awesome.css?CachedAsOf=2015-10-28T15:54"                   rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSFontsGoogle">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/fonts-google.css?CachedAsOf=2015-12-04T19:04"                   rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSMainNavMSIEPie3d">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/mainnav.msie.pie3d.css?CachedAsOf=2015-10-09T21:05"             rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSMainNavMSIEPlain">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/mainnav.msie.plain.css?CachedAsOf=2011-08-09T16:33"             rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSMainNavStrict">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/mainnav.strict.css?CachedAsOf=2016-01-04T21:13"                 rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadCSSSlafStrict">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/css/sbalookandfeel.strict.css?CachedAsOf=2015-12-16T17:11"          rel="stylesheet" type="text/css" media="all"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadDumpObject">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/DumpObject.js?CachedAsOf=2013-10-10T18:30"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditDate">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditDate.js?CachedAsOf=2004-03-15T01:00"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditDateMinYearsOld">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditDateMinYearsOld.js?CachedAsOf=2006-05-16T20:16"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditDateNonFuture">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditDateNonFuture.js?CachedAsOf=2006-11-19T22:59"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditLoanNmb">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditLoanNmb.js?CachedAsOf=2007-05-22T21:32"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditMask">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditMask.js?CachedAsOf=2011-11-29T20:04"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditState">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditState.js?CachedAsOf=2006-08-21"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadEditTin">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/EditTin.js?CachedAsOf=2013-03-05T18:25"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadFavicon">
	<cfoutput>
<link  href="#Request.SlafTopOfHeadLibURL#/images/sbalookandfeel/favicon.ico"                                  rel="icon" type="image/vnd.microsoft.icon"/></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadFederatedAnalytics">
	<cfoutput>
<!-- Per GSA instructions, the best place for this script is just before /head: -->
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/Federated-Analytics.js?CachedAsOf=2013-02-14T17:36"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadFormSynopsis">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/FormSynopsis.js?CachedAsOf=2012-01-24T17:20"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadHiGrpUtils">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/jquery/HiGrpUtils.js?CachedAsOf=2014-01-30T14:42"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadLookupCountryData">
	<cfoutput><!--- For quick fallback, use LookupCountryData.jquery.js?CachedAsOf=2015-07-17T15:53 if symbolic links is to the .max.js file. --->
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/jquery/LookupCountryData.jquery.js?CachedAsOf=2015-08-05T17:38"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadLookupNAICSAjax"><!--- No AppHidden version. --->
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ajax/LookupNAICSDescTxt.ajax.js?CachedAsOf=2007-09-14T13:56"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadLookupStateCountiesAjax"><!--- No AppHidden version. --->
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ajax/LookupStateCounties.ajax.js?CachedAsOf=2013-04-16T22:02"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadLookupZipAppHidden">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/LookupZipToDropdown.js?CachedAsOf=2013-10-07T17:35"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadLookupZipAjax">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ajax/LookupZipToDropdown.ajax.js?CachedAsOf=2015-07-16T21:42"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadMainNav">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/mainnav.js?CachedAsOf=2016-01-05T12:31"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadResetForm">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/ResetForm.js?CachedAsOf=2013-08-23T18:07"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadSetFormEltValue">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/SetFormEltValue.js?CachedAsOf=2010-10-14T20:40"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadSba">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/sba.js?CachedAsOf=2015-10-06T14:36"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadSlafFrame">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/sbalookandfeelframe.js?CachedAsOf=2014-05-23T16:49"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadSlafStrict">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/sbalookandfeel/sbalookandfeel.strict.js?CachedAsOf=2015-10-07T20:15"></script></cfoutput>
</cfsavecontent>
<cfsavecontent variable="Request.SlafTopOfHeadSortableTables">
	<cfoutput>
<script src="#Request.SlafTopOfHeadLibURL#/javascripts/SortableTables.js?CachedAsOf=2012-05-21T12:43"></script></cfoutput>
</cfsavecontent>

<!--- TopOfHead itself: --->

<cfsavecontent variable="Request.SlafTopOfHead">
	<cfif IsDefined("Request.SlafApplicationName") AND (Request.SlafApplicationName IS "CLS")>
		<cfoutput>
<link     href="/cls/dsp_choosefunction.cfm" accesskey="1" rel="Home" title="Home (Return to CLS Choose Function)"></cfoutput>
	</cfif>
	<cfif Request.SlafTopOfHeadjQM>
		<cfif Request.SlafRenderingMode is "jqm">
			<cfoutput>
<link     rel="stylesheet" type="text/css" media="all" href="#Variables.SlafTopOfHeadjQMURLCss#"/>
<link     rel="stylesheet" type="text/css" media="all" href="#Variables.SlafTopOfHeadjQMDirCss#/sba.jqm.css?CachedAsOf=2012-07-09T17:10"/><!-- local code --></cfoutput>
		<cfelse>
			<cfoutput>
<link     rel="stylesheet" type="text/css" media="all" href="#Variables.SlafTopOfHeadjQMDirCss#/sba.dtv.css?CachedAsOf=2012-06-20T22:15"/><!-- local code --></cfoutput>
		</cfif>
	</cfif>
	<cfoutput>
#Request.SlafTopOfHeadCSSFontsGoogle#
<link     rel="stylesheet" type="text/css" media="all" href="#Request.SlafTopOfHeadLibURL#/css/sba.strict.css?CachedAsOf=2016-01-06T16:04"/></cfoutput>
	<cfif Request.SlafTopOfHeadjQM>
		<cfoutput>#Request.SlafTopOfHeadjQuery#<!--- Only situation where topofhead automatically inserts jQuery. --->
<script src="#Variables.SlafTopOfHeadjQMDirJs#/sba.jqm.js?CachedAsOf=2013-03-28T16:11"></script><!-- local code --></cfoutput>
		<cfif Request.SlafRenderingMode is "jqm">
			<cfoutput>
<script src="#Variables.SlafTopOfHeadjQMURLJs#"></script></cfoutput>
		</cfif>
	</cfif>
	<!---
	If you ever wondered whether a "cfinclude loop" was possible, it is. If bld_ServerCachedQueries 
	is being run in its own self-test mode, it calls on this file to build SlafHead/SlafTopOfHead. 
	So if this file were to include bld_ServerCachedQueries unconditionally, the result would be an 
	infinite loop of cfincludes in bld_ServerCachedQueries' self-test mode. That explains this cfif: 
	--->
	<cfif NOT IsDefined("Variables.ServerCachedQueriesSelfTestMode")>
		<cfinclude template="/library/cfincludes/bld_ServerCachedQueries.cfm">
	</cfif>
	<!---
	P.S.: bld_ServerCachedQueries's cfinclude of this file is also subordinate to a cfif IsDefined, 
	preventing this file's self-test mode from initiating the same infinite loop from the other end. 
	--->
	<cflock scope="Server" type="Readonly" timeout="30">
		<cfset Variables.GetThemes							= Server.Scq.security.ActvIMUserPrefThemeCdTbl>
		<cfset Variables.NamedThemes						= Server.Scq.security.ActvIMUserPrefListNamedThemes>
	</cflock>
	<cfoutput>
<noscript>
    <link rel="stylesheet" type="text/css" media="all" href="#Request.SlafTopOfHeadLibURL#/css/sba.noscript.css?CachedAsOf=2010-10-14T19:23"/>
</noscript>
#Request.SlafTopOfHeadSba#
<script>
// Note: "g" prefix means global in scope notation. No need for "var" because we're not trying to prevent creation of globals here: 
gSlafDevTestProd											= "#Request.SlafDevTestProd#";// "Slaf" = "SBA Look-and-Feel" related. 
gSlafDevTestProdInd											= "#Request.SlafDevTestProdInd#";
gSlafInlineBlock											= "inline-block";
gSlafInlineTable											= "inline-table";
gSlafMSIE6or7												= false;
gSlafRenderingMode											= "#Request.SlafRenderingMode#";<cfif IsDefined("Request.SlafServerUniqueCode")>
gSlafServerUniqueCode										= "#Request.SlafServerUniqueCode#";</cfif><cfif IsDefined("Request.SlafUserPrefs.ThemeCd")>
gSlafThemeName												= "th#Request.SlafUserPrefs.ThemeCd#";// See also top.gSlafAllThemeNames. <cfelse>
gSlafThemeName												= "th1";// See also top.gSlafAllThemeNames. </cfif>

SlafQueueOnReadyStateChange(function()
	{
	SlafSetSharedAncestorClass("#Variables.NamedThemes#", gSlafThemeName);
	});
</script>
</cfoutput>
	<cfif IsDefined("Request.SlafSuppressRTE") and Request.SlafSuppressRTE>
		<cfoutput>
<style>
.rte									{visibility: visible	!important;}
</style>
</cfoutput>
</cfif>
	<!--- Do NOT define DumpObject.js, jquery.js or sbalookandfeel.js here. Instead, cf_sbalookandfeel does that. --->
</cfsavecontent>

<cfsavecontent variable="Request.SlafHeadAndTopOfHead">
	<cfoutput>#Request.SlafHead##Request.SlafTopOfHead#</cfoutput>
</cfsavecontent>

<!--- Self-Test Mode: --->

<cfif Request.SlafTopOfHeadSelfTestMode>
	<!--- Note that we "eat our own dogfood" (use the variables we create, in addition to displaying them): --->
	<cfoutput>#Request.SlafHead#
<title>SBA - Library - get_sbalookandfeel_topofhead Self-Test Mode</title>#Request.SlafTopOfHead#
<style>
.subtitle
	{
	color:				##900;
	font-weight:		bold;
	}
</style>
</head>
<body class="pad20">
<h1 align="center" class="title1">SBA - Library - get_sbalookandfeel_topofhead Self-Test Mode</h1>
<pre>
<span class="subtitle">Request.SlafHead:</span>
#Replace(Request.SlafHead, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHead:</span>
#Replace(Request.SlafTopOfHead, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafHeadAndTopOfHead:</span>
#Replace(Request.SlafHeadAndTopOfHead, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadjQuery:</span>
#Replace(Request.SlafTopOfHeadjQuery, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadRTE:</span>
#Replace(Request.SlafTopOfHeadRTE, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadAjax (defines XHR object, required by .ajax.js routines):</span>
#Replace(Request.SlafTopOfHeadAjax, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadLookupNAICSAjax:</span>
#Replace(Request.SlafTopOfHeadLookupNAICSAjax, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadLookupStateCountiesAjax:</span>
#Replace(Request.SlafTopOfHeadLookupStateCountiesAjax, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadLookupZipAppHidden:</span>
#Replace(Request.SlafTopOfHeadLookupZipAppHidden, "<", "&lt;", "ALL")#

<span class="subtitle">Request.SlafTopOfHeadLookupZipAjax:</span>
#Replace(Request.SlafTopOfHeadLookupZipAjax, "<", "&lt;", "ALL")#
</pre>
<p class="subtitle">
	Reload this page with runtime overrides to set other options (note: URL parameters only affect Self-Test Mode): 
</p>
<ul>
	<li><a href="get_sbalookandfeel_topofhead.cfm?jQMVer=&Render=dtv"				>jQuery Mobile Aware, Desktop View</a></li>
	<li><a href="get_sbalookandfeel_topofhead.cfm?jQMVer="							>jQuery Mobile Aware, Mobile View</a></li>
	<li><a href="get_sbalookandfeel_topofhead.cfm?jQ1Ver=&jQ2Ver=&jQMVer=.latest"	>jQuery Mobile 1.3.1 (latest)</a></li>
	<li><a href="get_sbalookandfeel_topofhead.cfm?RTEType=FCK"						>FCKEditor as the RichTextEditor</a></li>
	<li><a href="get_sbalookandfeel_topofhead.cfm?RTEType=MCE"						>tinyMCE as the RichTextEditor</a></li>
</ul>
</body>
</html>
</cfoutput>
	<cfsetting showdebugoutput="No">
</cfif>

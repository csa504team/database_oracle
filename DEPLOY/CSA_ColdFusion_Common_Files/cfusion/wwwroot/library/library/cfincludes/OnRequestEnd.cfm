<!---
AUTHOR:				Steve Seaquist
DATE:				10/20/2005
DESCRIPTION:		Shared termination routines. Will also be used in SBA logging, if enabled. 
NOTES:				None. 
INPUT:				Variables.Debug (optional), Variables.DebugMsg (optional). 
OUTPUT:				Unless told not to (that is, unless IsDefined("Variables.Debug") AND (Variables.Debug IS "Yes")), 
					turn off debug output when we reach end-of-page normally. In addition, if Variables.DebugMsg 
					is defined and not the nullstring, display it, regardless of whether we're turning debug output off. 
REVISION HISTORY:	09/14/2007, SRS:	Don't crash if caller hasn't established the Session scope. 
					02/22/2007, SRS:	People were STILL not using OnRequestEnd, so made it possible to turn debugging on 
										and off with Form and Session variables as well. 
					06/20/2006, SRS:	As requested by Chuda, who wanted to be able to force a debug display externally, 
										added the ability to pull in URL.Debug. Since the IP address of the browser has 
										to be already defined in ColdFusion Administrator as a Debugging IP address, this 
										new feature does not present any security vulnerabilities. 
					10/20/2005, SRS:	Original implementation. 
--->

<cfif NOT IsDefined("Variables.Debug")>
	<cfif			IsDefined("URL.Debug")>			<cfset Variables.Debug			= URL.Debug>
	<cfelseif		IsDefined("Form.Debug")>		<cfset Variables.Debug			= Form.Debug>
	<cfelse>
		<!---
		Don't use Session.Debug, because some app may already be using it for some other purpose. Instead, 
		use Session.SlafDebug, which is settable using Developer Utilities in development. 
		--->
		<cftry>
			<cflock scope="SESSION" type="READONLY" timeout="30">
				<cfif IsDefined("Session.SlafDebug")>	<cfset Variables.Debug		= Session.SlafDebug></cfif>
			</cflock>
			<cfcatch type="Any"><!--- Ignore. Just don't crash if Session scope is not established. ---></cfcatch>
		</cftry>
	</cfif>
</cfif>
<cfif (NOT IsDefined("Variables.Debug")) OR (Variables.Debug IS NOT "Yes")>
	<cfsetting showdebugoutput="No">
</cfif>
<cfif IsDefined("Variables.DebugMsg") AND (Len(Variables.DebugMsg) GT 0)>
	<cfoutput>
<br><br><br><br><br><b>Variables.DebugMsg:</b><br>#Variables.DebugMsg#<br><br><br><br><br></cfoutput>
</cfif>
<cfinclude template="log_SleEnd.cfm">

This directory is for shared document upload code.
There are also some css files that we might not keep so for now I am placing them here.
All of the javascript files should also be in library.
The three files here are:
fleUploandscript.cfm - should be loaded in the header section of the page that the upload will be done in
fileUploadForm.cfm - should be included in the upload page where you want the upload button and listing to appear.
act_fileupload.cfm - action page to process the file and write it to the database.

At the moment I have commented out the database portion
as it works and I am working on the user interface. Updating the existing document list and determining what fields to pass.
The action page sends a JSON Object back to the browser with the results of the upload and information needed to update the document listing
Such as the newly generated DocId.

<cfsetting enablecfoutputonly="true">
<cfsetting showDebugOutput="No">
<!---
AUTHOR:				Ian Clark
DATE:				03/15/2016
DESCRIPTION:		action page for file upload.
NOTES:				This was based on a sample of code using JQuery File Upload Library. I have expanded it to write to the
					database and to send back a json response. The counter does not get set when the check is sent.
					So going with a session counter.
INPUT:
OUTPUT:
		REVISION HISTORY:	06/17/2016, VRVK:(OPSMDEV-903)SBG centralized upload changes
--->

<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) gt 0>
	<CFSET Variables.dbtypeTEMP	= "oracle_transaction">
	<CFSET Variables.dbTEMP		= "oracle">
	<cfset Variables.ToList 	= "nelli.salatova@sba.gov">
	<cfset Variables.FrmList	= Variables.ToList>
<cfelse>
	<cfset Variables.ToList 	= "vidya.veena@sba.gov">
	<cfset Variables.FrmList	= Variables.ToList>
</cfif>
<cfset Variables.SaveMe 		= "Yes">
<cfset Variables.ErrMsg 		= "Yes">
<cfset Variables.MaxFilSize 	= 10*1024*1024>

<cfset Variables.FileUploadComplete = false>
<cfif isDefined("form.ORIGUSERID")>
	<cfset Variables.ORIGUSERID = trim(form.ORIGUSERID)>
<cfelse>
 <cfset Variables.ORIGUSERID 		= 	"Missing">
</cfif>
<CFSET Variables.DESTINATION_DIR 	= 	"/opt/iplanet/servers/docs/applicdocs">
<CFSET Variables.DESTINATION_DIR 	= variables.DESTINATION_DIR & "/" & Variables.ORIGUSERID>

<!--- for Orig,Serv,Post,GPTS --->
<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) gt 0>
	<cfif  DirectoryExists('#Variables.DESTINATION_DIR#/tmp')>
		<CFDIRECTORY ACTION="Delete" DIRECTORY="#DESTINATION_DIR#/tmp">
	</cfif>
	<cfif  DirectoryExists('#Variables.DESTINATION_DIR#')>
		<cfdirectory action="list" directory="#Variables.DESTINATION_DIR#"  name="FileList" mode="777">
		<cfloop query="FileList">
			<cffile action = "delete" file = "#Variables.DESTINATION_DIR#/#FileList.NAME#">
		</cfloop>
		<CFDIRECTORY ACTION="Delete" DIRECTORY="#Variables.DESTINATION_DIR#">
	</cfif>
</cfif>


<CFIF NOT DirectoryExists('#DESTINATION_DIR#')>
	<CFDIRECTORY ACTION="Create" DIRECTORY="#DESTINATION_DIR#" mode="777">
	<CFDIRECTORY ACTION="Create" DIRECTORY="#DESTINATION_DIR#/tmp" mode="777">
<CFELSE>
	<CFIF NOT DirectoryExists('#DESTINATION_DIR#/tmp/')><!--- need this for large files --->
		<CFDIRECTORY ACTION="Create" DIRECTORY="#DESTINATION_DIR#/tmp" mode="777">
	</CFIF>
</CFIF>

<cfset Variables.tmpFilelength = 0>
<cfif isDefined("FORM.trequests")>
	<cfset Variables.trequests = FORM.trequests>
<cfelse>
<cfset Variables.trequests = 1>
</cfif>
<cfif isDefined("FORM.Row")>
	<cfset Variables.Row = FORM.Row>
<cfelse>
<cfset Variables.Row = 1>
</cfif>
<cfif Not IsDefined("session.counter")>
	<cfset session.counter = 1>
</cfif>
<cfscript>
// This will finally give me the file name I need for the rest of the code to work....
function getClientFileName(fieldName) {
	var tmpPartsArray 	= Form.getPartsArray();
	var clientFileName 	= "";

	if (IsDefined("tmpPartsArray")) {
		for (local.tmpPart in tmpPartsArray) {
			if (local.tmpPart.isFile() AND local.tmpPart.getName() EQ arguments.fieldName) {
				return local.tmpPart.getFileName();
				}
			}
		}

	return "";
	}

UploadedFileName = getClientFileName("file") ;

if(structcount(FORM))
{
  //Directory allocation
  destination = variables.DESTINATION_DIR; //DESTINATION WHERE TO SAVE THE FILE
  destinationtemp = destination & "/tmp/";
  //Chunk of file
  filename = "#UploadedFileName#_#session.sessionid#_#session.counter#";
  //File file already exist
  if( fileExists('#destination##UploadedFileName#')){   //counter == 1 &&
    fileDelete('#destination##UploadedFileName#');
  } else{

  }
  if( fileExists('#destinationtemp##filename#')){
			session.counter=session.counter + 1;
			filename = "#UploadedFileName#_#session.sessionid#_#session.counter#";
		}
   if(trequests == 1 || trequests == 'NAN')
    {
      path = destination;
      desFile = trim(UploadedFileName);
      session.counter=1;
    }else{
      path = destinationtemp;
      desFile = filename;
   }
  newFile = createObject("java","java.io.File").init(path,desFile);
   fileobj = createObject("java","java.io.File").init(FORM.file);
   isDone = fileobj.renameTo(newFile);
  if(trequests == session.counter && trequests != 1){
     objOutputStream = FileOpen("#destination#/#UploadedFileName#", "append");
     for(index = 1; index <= trequests; index++)
    {
     filename = "#UploadedFileName#_#session.sessionid#_#index#";
     chunk = fileReadBinary('#destinationtemp##filename#');
     filewrite(objOutputStream, chunk);
     fileDelete('#destinationtemp#/#filename#');
    }
     fileclose(objOutputStream);
	 Variables.FileUploadComplete = true;
	   session.counter=1;
}else{
		 if(trequests == 1 and FileExists("#destination#/#UploadedFileName#")){
		 	Variables.FileUploadComplete = true;
		 	  session.counter=1;
		 }
}
}
</cfscript>

<cfif isDefined("Variables.FileUploadComplete") AND  Variables.FileUploadComplete><!--- Successful file upload to server. --->
	<cfset variables.FileName = destination &"/" & UploadedFileName>
    <cfif IsDefined("form.DescTxt")>
    <cfset variables.DescTxt	= form.DescTxt>
    <cfelse>
    <cfset variables.DescTxt	= "not defined">
    </cfif>
<!---
	<CFIF Ucase(TRIM(RIGHT(variables.FileName,4))) EQ ".PDF">
		<cftry>
		<cftransaction>
			<!--- File is ready to be processed --->
		<CFSET Variables.dbtypeTEMP				= Variables.dbtype	>
		<CFSET Variables.dbTEMP				= Variables.db	>
		<CFSET Variables.dbtype				= "Oracle">
		<CFSET Variables.db					= "oracle_transaction_object">   <!--- "oracle_scheduled" --->
		<CFSET Variables.LogAct		 		= "call DOCINSTSP">
		<CFSET Variables.cfprname	 		= "DOCINSERT">
		<cfset Variables.Identifier  		= "0">
		<cfset Variables.DOCACTVINACTIND	= "A">
		<cfset Variables.DOCSTATDT 			= Now()>
		<cfset Variables.DOCSTATCD 	 		= 5><!--- Micro Loan does not have a process to finalize docuemnts --->
		<cfset Variables.CREATUSERID 		= Session.OrigUserId>
		<cfset Variables.LASTUPDUSERID 		= Session.OrigUserId>
		<cfset Variables.DOCNM  	 		= variables.UploadedFileName>
		<cfset Variables.DOCTYPCd			= FORM.FILETYPCD	>
		<cfset Variables.BusPrcsTypCd		= 7>
		<cfset variables.loanappnmb			=10312461><!--- IAC will do a call to loan.LoanGntyTbl later. There is a trigger so it needs
															to be a valid loanAppnmb. Might require data clean up --->
		<cfset Variables.FILESIZE = GetFileInfo("#destination#/#UploadedFileName#").Size />
		<!--- Need to call stored procedure here... --->
		<!--- <cfdump var=#variables#><cfabort> --->
	 					<cfinclude template = "/cfincludes/oracle/loandocs/spc_DOCINSTSP.PUBLIC.cfm">
	 						<cfif Variables.TxnErr>
									<CFMAIL from="etran@sba.gov" to="#Variables.GetEmailFileUploadErrList#" subject="ERROR inserting MicroLoan document information" type="html">
									<cfif isdefined("variables.UploadedFileName")>
											<cfdump var="variables.FileName=#variables.UploadedFileName#"><br>
									</cfif>
									<cfif isdefined("variables.loanappnmb")>
										<cfdump var=" variables.loanappnmb=#variables.loanappnmb#"><br>
									</cfif>
									<cfif isdefined("Variables.ErrMsg")>
											<cfdump var="errors=#Variables.ErrMsg#"><br>
									</cfif>
									<CFSET RESULT="No">
								</CFMAIL>
									<cfoutput>
									{"files":[
										{
											"name":"#UploadedFileName#",
											"size":"#Variables.FILESIZE#",
											"error":"Could Not insert file Information."
											}
									]}
									</cfoutput>
									<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
					 				<CFSET Variables.db 				= Variables.dbTEMP	>
									<cfabort>
								</cfif>
						<cffile action = "readbinary" file = "#variables.FileName#" variable="DOCDATA" >

						<cfinclude template = "/cfincludes/oracle/loandocs/spc_DOCFILEUPLOADINSTSP.PUBLIC.cfm">
						<cfif Variables.TxnErr>
							<cfoutput>
							{"files":[
										{
											"name":"#UploadedFileName#",
											"size":"#Variables.FILESIZE#",
											"error":"Could Not insert file Object. #Variables.ErrMsg#"
										}
							]}
							</cfoutput>
							<cftransaction action = "rollback"/>
							<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
							<CFSET Variables.db 				= Variables.dbTEMP	>
							<cfabort>
						</cfif>

						<cfoutput>
						{"files":[
								{
								"name":"#UploadedFileName#",
								"size":"#Variables.FILESIZE#",
								"DocId":"#Variables.DOCID#",
								"DocTypeCd":"# Variables.DOCTYPCd#"
								}
							]}
						</cfoutput>
 					<cftransaction action = "commit"/>
			</cftransaction>
			<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
			<CFSET Variables.db 				= Variables.dbTEMP	>
				<cfquery name="getFileType" dataSource="#Db#" password="#Password#" username="#Username#" >
						SELECT DOCTYPCD from SBAREF.DOCTYPTBL WHERE BUSPRCSTYPCD =7 AND PRCSMTHDCD='10'
				</cfquery>
				<cfif isDefined("getFileType.RecordCount") AND getFileType.RecordCount EQ 1>
						<cfset Variables.fileTypCd =getFileType.DOCTYPCD >
				<cfelse>
				  <cfset Variables.fileTypCd =548 ><!--- need to do look up as might not be the same in all environments. --->
				</cfif>
				<cfif  Trim(Variables.DOCTYPCd) EQ trim(Variables.fileTypCd) >
					<cfset ArrayAppend(Session.MrfLlrfRptFileSeqNmbs, Variables.DOCID) />
				<cfelse>
					<cfset ArrayAppend(Session.AnnualRptFileSeqNmbs, Variables.DOCID) />
				</cfif>
					<cffile action="delete" file="#destination#/#UploadedFileName#" />
		<cfcatch>
			{files:[{error:1,file:"#UploadedFileName#",size:'#Variables.FILESIZE#'}]}
		</cfcatch>
		</cftry>
			<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
			<CFSET Variables.db 				= Variables.dbTEMP	>
	<cfelse>
		<cfoutput>
		{"files":[
				{
				"name":"#UploadedFileName#",
				"size":"#Variables.FILESIZE#",
				"error":"The uploaded file is not a PDF file"
				}
		]}
		</cfoutput>
	</cfif>
--->
		<CFIF Ucase(TRIM(RIGHT(variables.FileName,4))) EQ ".PDF">
			<cfdirectory action="list" directory="#Variables.DESTINATION_DIR#"  name="PDFFileList"/>
			<cfloop query="PDFFileList">
				<cfif PDFFileList.type eq "File">
				     <CFIF NOT IsPDFFile("#Variables.DESTINATION_DIR#/#PDFFileList.NAME#")>
						<CFSET Variables.ErrMsg	= Variables.ErrMsg & "The " & PDFFileList.name & " is a corrupted PDF File. Upload Failed.">
						<cfset Variables.SaveMe = "No">
						<cffile action = "delete" file = "#Variables.DESTINATION_DIR#/#PDFFileList.NAME#">
					<CFELSEIF PDFFileList.SIZE GT Variables.MaxFilSize>
						<cffile action = "delete" file = "#Variables.DESTINATION_DIR#/#PDFFileList.NAME#">
						<CFSET Variables.ErrMsg	= Variables.ErrMsg & "The " & PDFFileList.name & " file Exceeds Size Limit. Upload Failed.">
						<cfset Variables.SaveMe = "No">
					</cfif>
				</cfif>
			</cfloop>
		</CFIF>
	<cfif Variables.SaveMe>
		<cftry>
		<cftransaction>
			<!--- File is ready to be processed --->
		<cfif listfindnocase("SBGBUS,SBGBND,PSB,Claims",trim(form.SubSystemName)) gt 0>
	        <CFSET Variables.dbtypeTEMP				= "#form.dbtype#">
			<CFSET Variables.dbtype					= "Oracle">
			<CFSET Variables.db						= "oracle_transaction_object">   <!--- "oracle_scheduled" --->
	        <CFSET Variables.dbTEMP					= Variables.db>
	       	<cfset Variables.username				= "">
	       	<cfset Variables.password				= "">
	        <CFSET Variables.Identifier				= "0">
			<CFSET Variables.LogAct					= "call BUSDOCINSTSP">
	        <CFSET Variables.cfprname				= "DocInsert">
	        <cfset Variables.FILESIZE 				= "" />
	        <CFSET Variables.DOCID					= "">
	        <cfset Variables.DOCTYPCd				= FORM.FILETYPCD>
	        <cfif form.SubSystemName EQ "SBGBUS">
				<CFSET Variables.DOCACTVINACTIND	= "0">
	            <CFSET Variables.DOCUPLDRNM			= "#form.FirstNm# #form.LastNm#">
	            <CFSET Variables.BUSID				= "#form.BUSID#">
	            <CFSET Variables.DOCUPLOADDT		= DateFormat(Now(), "dd-mmm-yyyy")>
	            <CFSET Variables.DOCBUSTYPCD		= Variables.DOCTYPCd>
	            <CFSET Variables.DOCNM				= "#variables.UploadedFileName#">
	            <CFSET Variables.DOCUPLDRUSRTYP		= "#form.DOCUPLDRUSRTYP#">
	            <CFSET Variables.CREATUSERID		= "#form.CREATUSERID#">
	            <CFSET Variables.DOCCMNTTXT			= "#form.DescTxt#">
	             <CFSET Variables.BNDAPPCOMNSEQNMB	= "#form.BNDAPPCOMNSEQNMB#">
	            <CFSET Variables.BNDAPPSUBNMB		= "#form.BNDAPPSUBNMB#">
	        <cfelseif form.SubSystemName EQ "SBGBND">
				<CFSET Variables.BUSID				= "">
	            <CFSET Variables.DOCACTVINACTIND	= "0">
	            <CFSET Variables.DOCUPLDRNM			= "#form.FirstNm# #form.LastNm#">
	            <CFSET Variables.DOCUPLOADDT		= DateFormat(Now(), "dd-mmm-yyyy")>
	            <CFSET Variables.DOCBUSTYPCD		= Variables.DOCTYPCd>
	            <CFSET Variables.DOCNM				= "#variables.UploadedFileName#">
	            <CFSET Variables.DOCUPLDRUSRTYP		= "#form.DOCUPLDRUSRTYP#">
	            <CFSET Variables.CREATUSERID		= "#form.CREATUSERID#">
	            <CFSET Variables.DOCCMNTTXT			= "#form.DescTxt#">
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= "#form.BNDAPPCOMNSEQNMB#">
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= BNDAPPCOMNSEQNMB.Split(",")>
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= Variables.BNDAPPCOMNSEQNMB[1]>
	            <CFSET Variables.BNDAPPSUBNMB		= "#form.BNDAPPSUBNMB#">
	            <CFSET Variables.BNDAPPSUBNMB		= BNDAPPSUBNMB.Split(",")>
	            <CFSET Variables.BNDAPPSUBNMB		= Variables.BNDAPPSUBNMB[1]>
			<cfelseif form.SubSystemName EQ "PSB">
				<CFSET Variables.BUSID				= "">
	            <CFSET Variables.DOCACTVINACTIND	= "0">
	            <CFSET Variables.DOCUPLDRNM			= "#form.FirstNm# #form.LastNm#">
	            <CFSET Variables.DOCUPLOADDT		= DateFormat(Now(), "dd-mmm-yyyy")>
	            <CFSET Variables.DOCBUSTYPCD		= Variables.DOCTYPCd>
	            <CFSET Variables.DOCNM				= "#variables.UploadedFileName#">
	            <CFSET Variables.DOCUPLDRUSRTYP		= "#form.DOCUPLDRUSRTYP#">
	            <CFSET Variables.CREATUSERID		= "#form.CREATUSERID#">
	            <CFSET Variables.DOCCMNTTXT			= "#form.DescTxt#">
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= "#form.BNDAPPCOMNSEQNMB#">
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= BNDAPPCOMNSEQNMB.Split(",")>
	            <CFSET Variables.BNDAPPCOMNSEQNMB	= Variables.BNDAPPCOMNSEQNMB[1]>
	            <CFSET Variables.BNDAPPSUBNMB		= "0">
			<cfelseif form.SubSystemName EQ "Claims">
				<CFSET Variables.Identifier			= "11">
				<CFSET Variables.BUSID				= "">
	            <CFSET Variables.SBGNmb				= "#form.SBGNmb#">
	            <CFSET Variables.DOCUPLDSEQNMB		= "0">
	            <CFSET Variables.DOCUPLDRNM			= "#form.FirstNm# #form.LastNm#">
				<CFSET Variables.DOCNM				= "#variables.UploadedFileName#">
				<CFSET Variables.DOCUPLDRUSRTYP		= "">
				<CFSET Variables.DOCCMNTTXT			= "#form.DescTxt#">
				<CFSET Variables.DOCUPLOADDT		= DateFormat(Now(), "dd-mmm-yyyy")>
	            <CFSET Variables.DOCBUSTYPCD		= Variables.DOCTYPCd>
				<CFSET Variables.CREATUSERID		= "#form.CREATUSERID#">
			</cfif>
		<cfelseif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) gt 0>
				<CFSET Variables.dbtype				= "Oracle">
				<CFSET Variables.db					= "oracle_transaction_object">
				<CFSET Variables.LogAct		 		= "call DOCINSTSP">
				<CFSET Variables.cfprname	 		= "DOCINSERT">
				<cfset Variables.Identifier  		= "0">
				<cfset Variables.DOCACTVINACTIND	= "A">
				<cfset Variables.DOCSTATDT 			= Now()>
				<cfset Variables.DOCSTATCD 	 		= 5>
				<cfset Variables.CREATUSERID 		= Variables.OrigUserId>
				<cfset Variables.DOCNM  	 		= Variables.UploadedFileName>
				<cfset Variables.DOCTYPCd			= FORM.FileTypCd>
				<cfset Variables.BusPrcsTypCd		= form.BusPrcsTypCd>
				<cfset Variables.loanappnmb			= form.loanappnmb>

				<cfset Variables.FILESIZE = GetFileInfo("#destination#/#UploadedFileName#").Size />
				<!--- <cfdump var=#variables#><cfabort> --->
	 			<cfinclude template = "/cfincludes/oracle/loandocs/spc_DOCINSTSP.PUBLIC.cfm">
		</cfif>
		<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) eq 0>
			<cfset Variables.FILESIZE = GetFileInfo("#destination#/#UploadedFileName#").Size />
		</cfif>
		<!--- Need to call stored procedure here... --->
        <cffile action = "readbinary" file = "#Variables.FileName#" variable="DOCDATA" >
				<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) gt 0>
					<cfinclude template = "/cfincludes/oracle/loandocs/spc_DOCFILEUPLOADINSTSP.PUBLIC.cfm">
				<cfelse>
					<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_DOCUPLDINSTSP.cfm">
				</cfif>
				<cfif Variables.TxnErr>
					<cfoutput>
					{"files":[
								{
									"name":"#UploadedFileName#",
									"size":"#Variables.FILESIZE#",
									"error":"Could Not insert file Object. #Variables.ErrMsg#"
								}
					]}
					</cfoutput>
					<cftransaction action = "rollback"/>
					<CFSET Variables.dbtype 			= Variables.dbtypeTEMP>
					<CFSET Variables.db 				= Variables.dbTEMP>
					<cfabort>
				</cfif>
    		<!---<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_DOCUPLDINSTSP.cfm">	--->
                <cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) eq 0>
					<cfset Variables.DOCID	= Variables.DOCUPLDSEQNMB>
				</cfif>
				<cfif Variables.TxnErr>
					<CFMAIL from="#Variables.FrmList#" to="#Variables.ToList#" subject="#Variables.ErrMsg#" type="html">
						<cfif isdefined("variables.UploadedFileName")>
								<cfdump var="variables.FileName=#variables.UploadedFileName#"><br>
						</cfif>
						<cfif isdefined("Variables.ErrMsg")>
								<cfdump var="errors=#Variables.ErrMsg#"><br>
						</cfif>
						<CFSET RESULT="No">
					</CFMAIL>
					<cfoutput>
					{"files":[
						{
							"name":"#UploadedFileName#",
							"size":"#Variables.FILESIZE#",
							"error":"Could Not insert file Information."
							}
					]}
					</cfoutput>
					<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
	 				<CFSET Variables.db 				= Variables.dbTEMP	>
					<cfabort>
			</cfif>

						<!---<cfoutput>
						{"files":[
								{
								"name":"#UploadedFileName#",
								"size":"#Variables.FILESIZE#",
								"DocId":"#Variables.DOCID#",
								"DocTypeCd":"#Variables.DOCTYPCD#"
								}
							]}
						</cfoutput>--->
 		<cftransaction action = "commit"/>
		</cftransaction>
			<cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) eq 0>
				<CFSET Variables.dbtype 			= Variables.dbtypeTEMP>
				<CFSET Variables.db 				= Variables.dbTEMP>
            	<CFSET Variables.PRCSMTHDCD 		= form.PRCSMTHDCD>
				<cfquery name="getFileType" dataSource="#Db#" password="#Password#" username="#Username#" >
						SELECT DOCTYPCD from SBAREF.DOCTYPTBL WHERE BUSPRCSTYPCD =#Variables.DOCBUSTYPCD# AND PRCSMTHDCD='#Variables.PRCSMTHDCD#'
				</cfquery>
			</cfif>
				<!---<cfif isDefined("getFileType.RecordCount") AND getFileType.RecordCount EQ 1>
						<cfset Variables.fileTypCd =getFileType.DOCTYPCD >
				<cfelse>
				  <cfset Variables.fileTypCd =548 ><!--- need to do look up as might not be the same in all environments. --->
				</cfif>--->
				<!---<cfif  Trim(Variables.DOCTYPCd) EQ trim(Variables.fileTypCd) >
					<cfset ArrayAppend(Session.MrfLlrfRptFileSeqNmbs, Variables.DOCID) />
				<cfelse>
					<cfset ArrayAppend(Session.AnnualRptFileSeqNmbs, Variables.DOCID) />
				</cfif>--->
					<cffile action="delete" file="#destination#/#UploadedFileName#" />
		<cfcatch>
			{files:[{error:1,file:"#UploadedFileName#",size:'#Variables.FILESIZE#'}]}
		</cfcatch>
		</cftry>
			<CFSET Variables.dbtype 			= Variables.dbtypeTEMP	>
			<CFSET Variables.db 				= Variables.dbTEMP	>
	<cfelse>
		<cfoutput>
		{"files":[
				{
				"name":"#UploadedFileName#",
				"size":"#Variables.FILESIZE#",
				"error":"Variables.ErrMsg"
				}
		]}
		</cfoutput><!--- The uploaded file is not a PDF file --->
	</cfif>

	<!--- <cfif listfindnocase("Orig,Serv,Post,GPTS",trim(form.SubSystemName)) eq 0> --->
		<cfset Variables.DOCTYPCd = FORM.FILETYPCD>
	<!--- </cfif> --->
	<!---<cfset Variables.FILESIZE = GetFileInfo("#destination#/#UploadedFileName#").Size />--->

	<cfoutput><!--- IAC note: 2222 is hard coded until I reactive the writing to the database at which point it will be the DOCID --->
						{"files":[
								{
								"Row":"#Variables.Row#",
								"name":"#UploadedFileName#",
								"size":"#Variables.FILESIZE#",
								"DocTypeCd":"#Variables.DOCTYPCd#",
								"DOCID":"#Variables.DOCID#",
								"href":"dsp_dwnldletter.cfm?DOCID=#Variables.DOCID#",
                                "comments":"#Variables.DescTxt#"
								}
							]}
						</cfoutput>
<cfelse>
	<cfset Variables.FILESIZE = GetFileInfo("#destinationtemp#/#fileName#").Size />
	<cfoutput>{"files":[{"name":"#UploadedFileName#","size":"#Variables.FILESIZE#"}]}</cfoutput>
</cfif>

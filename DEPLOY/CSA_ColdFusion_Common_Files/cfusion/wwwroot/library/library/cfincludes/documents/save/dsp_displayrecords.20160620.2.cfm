
<!---
AUTHOR:				Ian Clark
DATE:				03/15/2016
DESCRIPTION:		Form for Jquery File Upload
NOTES:				This was based on a sample of code using JQuery File Upload Library. This is the form for defining
                    what gets displayed for the files uploaded and what gets passed to the server. This requires fileUploadscript.cfm
                    be included in the header section of the calling page.
INPUT:
OUTPUT:
		REVISION HISTORY:	06/17/2016, VRVK:(OPSMDEV-903)SBG centralized upload changes
					03/15/2016 IAC Original Implementation
--->

<cfoutput>
<cfif IsDefined("URL.OnRefresh")><cfif URL.OnRefresh EQ "Yes">
<cfset Variables.OnRefresh= URL.OnRefresh><cfinclude template="set_uploadvariables_sbg.cfm"></cfif></cfif>
<cfif IsDefined("BusDocSearch")> <cfif BusDocSearch.RecordCount GT 0>
<table id="existingFiles"  align="center" border="1"  >
					<thead>
						<tr>
							<th  style="width: 10px" align="right">
								<p>Row</p>
    						</th>
							<cfif Variables.ShowDeleteCheck>
								<th style="width: 40px" align="center">
					    	        <p>Delete?</p>
						        </th>
							</cfif>
							<th  style="width: 200px" align="center">
								<p>File Name</p>
							</th>
							<th style="width: 200px">
								<p>Document type</p>
							</th>
							<cfif Variables.ShowFinalize>
								<th style="width: 80px" align="center">
									<p>Finalize?</p>
								</th>
							</cfif>
							<th style="width: 80px" align="center">
					    	        <p>Comment</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Uploader</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Date Uploaded</p>
						        </th>
<cfif Variables.ShowFinalize>
		 <th style="width: 80px" >
            <p>Upload Status</p>
        </th></cfif>
    </tr>
	</thead> 
    <cfset Row =0>	
    <cfif IsDefined("BusDocSearch")>	
		<cfloop query="BusDocSearch">
        <cfset row= row+1>
		<tr>
			<td align="right"  style="width: 10px" >#row#</td>
			<input type="hidden" name="DOCID1" value="1111"><!--- dummy data for now --->

			<cfif Variables.ShowDeleteCheck>
            <cfif Variables.SubSystemName EQ "SBGBUS">
				<td align="center" class="optlabel"  style="width: 40px" >
					<label class="optlabel nowrap">
						<!---<input type="checkbox" name="DelBus" onClick="chk_FinalDel('1','Del','Final');" value="0">--->
                        <input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#" onClick="chk_FinalDel('1','Del','Final');">
						Del
					</label></cfif>
                    <cfif Variables.SubSystemName EQ "SBGBND">
						<CFIF NOT (Variables.CanSBGEAAgent)>
							<td align="center" class="optlabel"  style="width: 40px" ><input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#"></td>
												<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
						<cfelse>
												<cfif Variables.BndAppSubNmb EQ  BusDocSearch.BndAppSubNmb>
													<td align="center" class="optdata"><input type="checkbox" name="DelBnd#BndRow#" value="#DocUpldSeqNmb#">
													<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
												<cfelse>
													<td align="center" class="optdata">*</td>
												</cfif>
						</cfif>
										
					</CFIF>
				</td>
			</cfif>
			<td>
            <cfif Variables.SubSystemName EQ "SBGBUS">
            <a href="dsp_opendoc.cfm?bus=#DocUpldSeqNmb#">#DocNm#</a>
            <cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims">
            <a href= "dsp_opendoc.cfm?bnd=#DocUpldSeqNmb#&Sub=#BndDocSearch.BndAppSubNmb#">#DocNm#</a>
            </cfif>
        	</td>
		 <td>
          <!--- <select name="DOCTYPCD1" onChange="chk_FinalDel('1','DOCTYPCD','Final');" style="width: 350px">
		<option          value="">Select</option>
			<!--- CF dislikes nested query references. --->
			<cfset Variables.DocTypCd								= "463">
			<cfloop query="Doclist">
				<cfif Variables.DocTypCd is Doclist.DocTypCd>	<cfset Variables.Selected = "selected">
				<cfelse>											<cfset Variables.Selected = "        "></cfif>
				<option #Variables.Selected# value="#Doclist.DocTypCd#">#Doclist.DocTypDescTxt#</option>
			</cfloop>

		</select>--->
        #DocBusDescTxt#
        </td>

		<cfif Variables.ShowFinalize>
			<td align="center">
				<label class="optlabel nowrap"  style="width: 80px" >
					<input type="checkbox" name="Final1" onClick="chk_FinalDel('1','Final','Del');" value="0">
					Fin
				</label>
    	    </td>
		</cfif>
        <td align="center" style="width: 80px">
				#DocCmntTxt#
    	    </td>
            <td align="center" style="width: 80px">
				#DocUpldrNm#&nbsp;
												<cfif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "C">[Contractor]
												<cfelseif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "A">[Agent]
												<cfelse>[SBA]
												</cfif>
    	    </td>
          <td align="center" style="width: 80px">
				#dateFormat(CreatDt,"mm/dd/yyyy")#<br>
												#TimeFormat(CreatDt, "hh:mm:ss tt")#&nbsp;
    	    </td>
       <cfif Variables.ShowFinalize><td align="center" style="width: 80px">&nbsp;&nbsp;&nbsp;</td></cfif>
		</tr>
        </cfloop></cfif>
        <cfset Variables.RecCount = #row#>
        <input type="hidden" name="RecCount" 		value="#RecCount#">
	</table>
    <cfif Variables.ShowDeleteCheck><div align="center">
	<!---<input type="button" value="Clear" onClick="ClearForm(this.form, 0);">
	&nbsp;--->
	<input type="Reset"  value="Reset">
	&nbsp;
	<input type="submit" name="DelDoc" value="Save" onClick="return BusDelChk();"></cfif>
</div>
    			
<cfelse><table align="center" width="90%">
		
		<tr bgcolor="#Variables.RowColorB#">
			<td colspan="5" align="center" class="mandlabel">No Documents uploaded yet</td>
		</tr>
	</table>
</cfif></cfif>
	</form>

</cfoutput>
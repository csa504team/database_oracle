<!---
AUTHOR:				NS
DATE:				06/23/2016
DESCRIPTION:		ETRAN system setup for file upload
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	06/23/2016, NS:(OPSMDEV-936) ELEND centralized upload variables setup
--->
<cfset Variables.db			 		= "oracle_transaction_object">

<cfif IsDefined("url.goToSys")>
	<cfset Variables.SubSystemCd = trim(url.goToSys)>
</cfif>

<cfif Variables.SubSystemCd eq 1>
	<cfset  Variables.SubSystemName = "Orig">
	<cfset Variables.AppSchemaName 	= "loandocs" >
	<cfset Variables.AppSPCUrl 		= "/cfincludes/oracle/loanapp">
	<cfset Variables.PageTitle		= "Origination Upload - Documents for SBA Application Number " & Variables.CurrAppId >
	<cfset Variables.BusPrcsTypCd	= "1">
<cfelseif Variables.SubSystemCd eq 2>
	<cfset Variables.AppSchemaName 	= "loan" >
	<cfset Variables.AppSchemaName 	= "loan" >
	<cfset Variables.AppSPCUrl 		= "/cfincludes/oracle/loan">
	<cfset Variables.SubSystemName	= "Serv">
	<cfset Variables.PageTitle		= "Servicing Upload - Documents for SBA Loan Number " & Session.LoanNmb>
	<cfset Variables.BusPrcsTypCd	= "2">
<cfelseif Variables.SubSystemCd eq 3>
	<cfset Variables.AppSchemaName 	= "loan" >
	<cfset Variables.AppSchemaName 	= "loan" >
	<cfset Variables.AppSPCUrl 		= "/cfincludes/oracle/loan">
	<cfset Variables.SubSystemName	= "Post">
	<cfset Variables.PageTitle		= "PostServicing Upload - Documents for SBA Loan Number " & Session.LoanNmb>
	<cfset Variables.BusPrcsTypCd	= "3">
</cfif>

<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/act_deletedoc.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">
<cfset Variables.GoBackUrl			= "/gpts/dataentry/documents/dsp_doclist.cfm">
<cfset Variables.checkboxname		= "DelDocId">
<cfset Variables.GoBackUrl	= "/elend/documents/dsp_viewdoclist.cfm">
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "loandocs" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/loandocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DocSelCSP" />

<!--- get all uploaded documents for the loan --->
<cfset Variables.LoanAppNmb				= Variables.CurrAppId>
<CFSET Variables.LogAct					= "find documents">
<CFSET Variables.cfprname				= "GetDocsAll">
<cfset Variables.Identifier				= "14">

<cfinclude template="#Variables.NewSPCURL#/spc_DOCSELTSP.cfm">
<cfif Variables.TxnErr>
	#Variables.ErrMsg#<cfabort>
</cfif>
<cfif listfindnocase("1,2,3,4", Variables.BUSPRCSTYPCD) gt 0>
	<cfquery name="GetDocs" dbtype="query">
		select * from GetDocsAll where BUSPRCSTYPCD = #Variables.BUSPRCSTYPCD#
	</cfquery>
</cfif>

	<cfset Variables.Count				= GetDocs.RecordCount>
	<cfset Variables.ShowDeleteCheck	= "Yes">
	<cfset Variables.ShowComments		= "No">
	<cfset Variables.ShowFinalize		= "No">

<cfset Variables.CreatUserId = session.imuserid>

<cfif isDefined("GetDocs.RecordCount")>
	<cfset Variables.RecordCnt			= GetDocs.RecordCount>
</cfif>
		<!---REVISION HISTORY:	06/17/2016, VRVK:(OPSMDEV-903)SBG centralized upload changes--->


<cfparam name = "Variables.PageTitle" default = "#Variables.PageTitle#" />

<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.db"					default = "oracle_spc_caob1" />
<cfparam name = "Variables.BusPrcsTypCd"		default = "#Variables.BusPrcsTypCd#" /><!--- limit the result set to the approprate business process --->
<cfparam name = "Variables.PrcsMthdCd"			default = "#Variables.PrcsMthdCd#" />

<cfparam name = "Variables.ShowDeleteCheck"		default = "#Variables.ShowDeleteCheck#" />
<cfparam name = "Variables.ShowComments"		default = "#Variables.ShowComments#" />
<cfparam name = "Variables.ShowFinalize"		default = "#Variables.ShowFinalize#" />

<cfparam name = "Variables.OracleSchemaName"	default = "#Variables.OracleSchemaName#" />
<cfparam name = "Variables.SPCUrl"      		default = "#Variables.SPCUrl#" />
<cfparam name = "Variables.AppSchemaName"		default = "#Variables.AppSchemaName#" />
<cfparam name = "Variables.AppSPCUrl"      		default = "#Variables.AppSPCUrl#" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "#Variables.UploadedDocsSPC#" />
<cfset Variables.RowColorB = "E7E7E7">
<cfif NOT IsDefined("spcn")>
	<cfinclude template = "/library/udf/bld_dbutils_for_dbtype.cfm" />
</cfif>

<!--- Get Documents List --->
<cfset Variables.LogAct		 	= "call DOCTYPSELTSP">
<cfset Variables.cfprname	 	= "Doclist">
<cfinclude template 			= "#spcn('DOCTYPSELTSP.PUBLIC', 'sbaref')#" />
<cfif Variables.TxnErr>
	#Variables.ErrMsg#<cfabort>
</cfif>
<cfoutput>
#Request.SlafHead#
<title>#Variables.PageTitle#</title>
#Request.SlafTopOfHead#
#Request.SlafTopOfHeadJQuery#

<cfinclude template="dsp_fileuploadscript.cfm">

<style>
	table, th, td {
    	border: 1px solid black;
    	border-collapse: collapse;
	    padding: 5px;
	    text-align: center;
	}
</style>

<body>
	<div class = "pad20">
		<h1 class = "title1" align = "center">
			#Variables.PageTitle#
		</h1>
<form method="post" name="FormRight" onSubmit="return DoSomethingDifferentOnSubmit(this);" action="/library/cfincludes/documents/set_uploadvariables_sbg.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">						<input type="Hidden" name="dbtype"				value = "Oracle">
<input type="Hidden" name="LASTUPDTUSERID"				value = "#Variables.Origuserid#">
<input type="Hidden" name="username"				value = "#Variables.username#">
<input type="Hidden" name="password"				value = "#Variables.password#">
			<input type="Hidden" name="PageNames"				value="dsp_outstanding.cfm">
			<input type="hidden" name="count"					value="1"   id="count">
			<input type="hidden" name="DocTypCdChanged"			value="No" 	id="DocTypCdChanged">
			<input type="hidden" name="DocFinalChanged"			value="No" 	id="DocFinalChanged">
			<input type="hidden" name="DocDelChanged"			value="No" 	id="DocDelChanged">
			<input type="hidden" name="DocTypCdChanged1"	value="No" id="DocTypCdChanged1">
			<input type="hidden" name="DocFinalChanged1"	value="No" id="DocFinalChanged1">
			<input type="hidden" name="DocDelChanged1"		value="No" id="DocDelChanged1">
			<!---<div><cfif IsDefined("BusDocSearch")> <cfif BusDocSearch.RecordCount GT 0>
<table id="existingFiles"  align="center" border="1"  >
					<thead>
						<tr>
							<th  style="width: 10px" align="right">
								<p>Row</p>
    						</th>
							<cfif Variables.ShowDeleteCheck>
								<th style="width: 40px" align="center">
					    	        <p>Delete?</p>
						        </th>
							</cfif>
							<th  style="width: 200px" align="center">
								<p>File Name</p>
							</th>
							<th style="width: 200px">
								<p>Document type</p>
							</th>
							<cfif Variables.ShowFinalize>
								<th style="width: 80px" align="center">
									<p>Finalize?</p>
								</th>
							</cfif>
							<th style="width: 80px" align="center">
					    	        <p>Comment</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Uploader</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Date Uploaded</p>
						        </th>
<cfif Variables.ShowFinalize>
		 <th style="width: 80px" >
            <p>Upload Status</p>
        </th></cfif>
    </tr>
	</thead> 
    <cfset Row =0>	
    <cfif IsDefined("BusDocSearch")>	
		<cfloop query="BusDocSearch">
        <cfset row= row+1>
		<tr>
			<td align="right"  style="width: 10px" >#row#</td>
			<input type="hidden" name="DOCID1" value="1111"><!--- dummy data for now --->

			<cfif Variables.ShowDeleteCheck>
            <cfif Variables.SubSystemName EQ "SBGBUS">
				<td align="center" class="optlabel"  style="width: 40px" >
					<label class="optlabel nowrap">
						<!---<input type="checkbox" name="DelBus" onClick="chk_FinalDel('1','Del','Final');" value="0">--->
                        <input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#" onClick="chk_FinalDel('1','Del','Final');">
						Del
					</label></cfif>
                    <cfif Variables.SubSystemName EQ "SBGBND">
						<CFIF NOT (Variables.CanSBGEAAgent)>
							<td align="center" class="optlabel"  style="width: 40px" ><input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#"></td>
												<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
						<cfelse>
												<cfif Variables.BndAppSubNmb EQ  BusDocSearch.BndAppSubNmb>
													<td align="center" class="optdata"><input type="checkbox" name="DelBnd#BndRow#" value="#DocUpldSeqNmb#">
													<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
												<cfelse>
													<td align="center" class="optdata">*</td>
												</cfif>
						</cfif>
										
					</CFIF>
				</td>
			</cfif>
			<td>
            <cfif Variables.SubSystemName EQ "SBGBUS">
            <a href="dsp_opendoc.cfm?bus=#DocUpldSeqNmb#">#DocNm#</a>
            <cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims">
            <a href= "dsp_opendoc.cfm?bnd=#DocUpldSeqNmb#&Sub=#BndDocSearch.BndAppSubNmb#">#DocNm#</a>
            </cfif>
        	</td>
		 <td>
          <!--- <select name="DOCTYPCD1" onChange="chk_FinalDel('1','DOCTYPCD','Final');" style="width: 350px">
		<option          value="">Select</option>
			<!--- CF dislikes nested query references. --->
			<cfset Variables.DocTypCd								= "463">
			<cfloop query="Doclist">
				<cfif Variables.DocTypCd is Doclist.DocTypCd>	<cfset Variables.Selected = "selected">
				<cfelse>											<cfset Variables.Selected = "        "></cfif>
				<option #Variables.Selected# value="#Doclist.DocTypCd#">#Doclist.DocTypDescTxt#</option>
			</cfloop>

		</select>--->
        #DocBusDescTxt#
        </td>

		<cfif Variables.ShowFinalize>
			<td align="center">
				<label class="optlabel nowrap"  style="width: 80px" >
					<input type="checkbox" name="Final1" onClick="chk_FinalDel('1','Final','Del');" value="0">
					Fin
				</label>
    	    </td>
		</cfif>
        <td align="center" style="width: 80px">
				#DocCmntTxt#
    	    </td>
            <td align="center" style="width: 80px">
				#DocUpldrNm#&nbsp;
												<cfif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "C">[Contractor]
												<cfelseif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "A">[Agent]
												<cfelse>[SBA]
												</cfif>
    	    </td>
          <td align="center" style="width: 80px">
				#dateFormat(CreatDt,"mm/dd/yyyy")#<br>
												#TimeFormat(CreatDt, "hh:mm:ss tt")#&nbsp;
    	    </td>
       <cfif Variables.ShowFinalize><td align="center" style="width: 80px">&nbsp;&nbsp;&nbsp;</td></cfif>
		</tr>
        </cfloop></cfif>
        <cfset Variables.RecCount = #row#>
        <input type="hidden" name="RecCount" 		value="#RecCount#">
	</table>
    <cfif Variables.ShowDeleteCheck><div align="center">
	<input type="button" value="Clear" onClick="ClearForm(this.form, 0);">
	&nbsp;
	<input type="Reset"  value="Reset">
	&nbsp;
	<input type="submit" name="DelDoc" value="Save" onClick="return BusDelChk();"></cfif>
</div>
    			
<cfelse><table align="center" width="90%">
		
		<tr bgcolor="#Variables.RowColorB#">
			<td colspan="5" align="center" class="mandlabel">No Documents uploaded yet</td>
		</tr>
	</table>
</cfif></cfif>
	</form>
	</div>--->
    <div id = "displayrecords"><!---<cfinclude template="dsp_displayrecords.cfm">--->
    <cfif IsDefined("BusDocSearch")> <cfif BusDocSearch.RecordCount GT 0>
<table id="existingFiles"  align="center" border="1"  >
					<thead>
						<tr>
							<th  style="width: 10px" align="right">
								<p>Row</p>
    						</th>
							<cfif Variables.ShowDeleteCheck>
								<th style="width: 40px" align="center">
					    	        <p>Delete?</p>
						        </th>
							</cfif>
							<th  style="width: 200px" align="center">
								<p>File Name</p>
							</th>
							<th style="width: 200px">
								<p>Document type</p>
							</th>
							<cfif Variables.ShowFinalize>
								<th style="width: 80px" align="center">
									<p>Finalize?</p>
								</th>
							</cfif>
							<th style="width: 80px" align="center">
					    	        <p>Comment</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Uploader</p>
						        </th>
                                <th style="width: 80px" align="center">
					    	        <p>Date Uploaded</p>
						        </th>
<cfif Variables.ShowFinalize>
		 <th style="width: 80px" >
            <p>Upload Status</p>
        </th></cfif>
    </tr>
	</thead> 
    <cfset Row =0>	
    <cfif IsDefined("BusDocSearch")>	
		<cfloop query="BusDocSearch">
        <cfset row= row+1>
		<tr>
			<td align="right"  style="width: 10px" >#row#</td>
			<input type="hidden" name="DOCID1" value="1111"><!--- dummy data for now --->

			<cfif Variables.ShowDeleteCheck>
            <cfif Variables.SubSystemName EQ "SBGBUS">
				<td align="center" class="optlabel"  style="width: 40px" >
					<label class="optlabel nowrap">
						<!---<input type="checkbox" name="DelBus" onClick="chk_FinalDel('1','Del','Final');" value="0">--->
                        <input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#" onClick="chk_FinalDel('1','Del','Final');">
						Del
					</label></cfif>
                    <cfif Variables.SubSystemName EQ "SBGBND">
						<CFIF NOT (Variables.CanSBGEAAgent)>
							<td align="center" class="optlabel"  style="width: 40px" ><input type="checkbox" name="DelBndBus#row#" value="#DocUpldSeqNmb#"></td>
												<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
						<cfelse>
												<cfif Variables.BndAppSubNmb EQ  BusDocSearch.BndAppSubNmb>
													<td align="center" class="optdata"><input type="checkbox" name="DelBnd#BndRow#" value="#DocUpldSeqNmb#">
													<input type="hidden" name="DelSubNmb#row#" 		value="#BusDocSearch.BndAppSubNmb#"> 
												<cfelse>
													<td align="center" class="optdata">*</td>
												</cfif>
						</cfif>
										
					</CFIF>
				</td>
			</cfif>
			<td>
            <cfif Variables.SubSystemName EQ "SBGBUS">
            <a href="dsp_opendoc.cfm?bus=#DocUpldSeqNmb#">#DocNm#</a>
            <cfelseif Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB" OR Variables.SubSystemName EQ "Claims">
            <a href= "dsp_opendoc.cfm?bnd=#DocUpldSeqNmb#&Sub=#BndDocSearch.BndAppSubNmb#">#DocNm#</a>
            </cfif>
        	</td>
		 <td>
          <!--- <select name="DOCTYPCD1" onChange="chk_FinalDel('1','DOCTYPCD','Final');" style="width: 350px">
		<option          value="">Select</option>
			<!--- CF dislikes nested query references. --->
			<cfset Variables.DocTypCd								= "463">
			<cfloop query="Doclist">
				<cfif Variables.DocTypCd is Doclist.DocTypCd>	<cfset Variables.Selected = "selected">
				<cfelse>											<cfset Variables.Selected = "        "></cfif>
				<option #Variables.Selected# value="#Doclist.DocTypCd#">#Doclist.DocTypDescTxt#</option>
			</cfloop>

		</select>--->
        #DocBusDescTxt#
        </td>

		<cfif Variables.ShowFinalize>
			<td align="center">
				<label class="optlabel nowrap"  style="width: 80px" >
					<input type="checkbox" name="Final1" onClick="chk_FinalDel('1','Final','Del');" value="0">
					Fin
				</label>
    	    </td>
		</cfif>
        <td align="center" style="width: 80px">
				#DocCmntTxt#
    	    </td>
            <td align="center" style="width: 80px">
				#DocUpldrNm#&nbsp;
												<cfif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "C">[Contractor]
												<cfelseif Ucase(trim(BusDocSearch.DocUpldrUsrTyp)) EQ "A">[Agent]
												<cfelse>[SBA]
												</cfif>
    	    </td>
          <td align="center" style="width: 80px">
				#dateFormat(CreatDt,"mm/dd/yyyy")#<br>
												#TimeFormat(CreatDt, "hh:mm:ss tt")#&nbsp;
    	    </td>
       <cfif Variables.ShowFinalize><td align="center" style="width: 80px">&nbsp;&nbsp;&nbsp;</td></cfif>
		</tr>
        </cfloop></cfif>
        <cfset Variables.RecCount = #row#>
        <input type="hidden" name="RecCount" 		value="#RecCount#">
	</table>
    <cfif Variables.ShowDeleteCheck><div align="center">
	<!---<input type="button" value="Clear" onClick="ClearForm(this.form, 0);">
	&nbsp;--->
	<input type="Reset"  value="Reset">
	&nbsp;
	<input type="submit" name="DelDoc" value="Save" onClick="return BusDelChk();"></cfif>
</div>
    			
<cfelse><table align="center" width="90%">
		
		<tr bgcolor="#Variables.RowColorB#">
			<td colspan="5" align="center" class="mandlabel">No Documents uploaded yet</td>
		</tr>
	</table>
</cfif></cfif>
	</form>
</div>
	<br>
	<br>
<!--- <cfset Variables.fileTypCd=10> --->
<cfinclude template="dsp_fileuploadform.cfm">
</div>
</body>

<script language="JavaScript">
function BusDelChk(){
	var k = 0;
	for (var i=1; i<=#Variables.RecCount#; i++) {
			if (eval("document.FormRight.DelBndBus"+i+".checked")) {
				k = 1;
			}
		}
	if (k == 0) {
	   	alert ("No document has been selected for deletion.");
		return false;
   	}else{
		var answer = confirm ("Selected documents will be deleted.\n Click on OK to continue, or CANCEL")
		if (answer){
			ColdFusion.Window.show('LoadWindow');
			return true
		}
		return false
	}
	
}
function BndDelChk(){
	var k = 0;
	for (var i=1; i<=#Variables.RecCount#; i++) {
			if (eval("document.FormRight.DelBndBus"+i+".checked")) {
				k = 1;
			}
		}
	if (k == 0) {
	   	alert ("No document has been selected for deletion.");
		return false;
   	}else{
		var answer = confirm ("Selected documents will be deleted.\n Click on OK to continue, or CANCEL")
		if (answer){
			ColdFusion.Window.show('LoadWindow');
			return true
		}
		return false
	}
	
}
function DoSomethingDifferentOnSubmit	(pThis)
		{
		<!---pThis.CollatLienChanged.value				= "No";
		if	(gCollatLienSynopsis					!= FormSynopsis(pThis, "+LoanCollatLien"))
			pThis.CollatLienChanged.value			= "Yes";
		return DoThisOnSubmit(pThis);--->
		}
</script>
</html>
</cfoutput>

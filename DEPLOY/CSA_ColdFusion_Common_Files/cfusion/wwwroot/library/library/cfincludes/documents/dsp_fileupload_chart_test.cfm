<style>
	table.noborder, td.noborder {
    	border: 0;
		padding: 0;
		margin:0;
	}
</style>

<cfparam name="Variables.SubSystemCd" default="">

<cfset aDoclist = "Application, Lender's Credit Memorandum, Draft Authorization,Personal Information (Owner/Operator/Guarantor), Other Processing Documents, Business Financial Statements, Affiliate Financial Statements,  Supporting Docs (Eligibility), Supporting Docs (Collateral), Supporting Docs (Other)">
<cfset aCount = "3,3,1,3,4,9,4,3,3,1">
<cfset aFileLoadedCount = "3,2,1,3,3,3,0,3,1,0">

<cfset qDoc = queryNew("Name,totalCount,uploadedCount")>
<cfset newRow = QueryAddRow(qDoc, 10)> 
 
<!--- Set the values of the cells in the query --->
<cfloop from="1" to="10" index="i" >
	<cfset temp = QuerySetCell(qDoc, "Name", "Tab " & i & " - " & ListGetAt(aDoclist,i), i)> 
    <cfset temp = QuerySetCell(qDoc, "totalCount", ListGetAt(aCount,i), i)> 
    <cfset temp = QuerySetCell(qDoc, "uploadedCount", ListGetAt(aFileLoadedCount,i), i)>
</cfloop>


<cfset bShowDetailChart = 0>
<cfif isDefined("url.step")>
	<cfquery name="qThisTab" dbtype="query">
    	SELECT * FROM qDoc WHERE Name = '#url.step#'
    </cfquery>
    <cfset bShowDetailChart = 1>
</cfif>

<cfoutput>
<table cellpadding="0" cellspacing="0" class="noborder">	
	<tr>
    	<td class="noborder" align="right">
        	<cfchart show3d="no" format="html" showlegend="no" showborder="no" chartwidth="600" chartheight="500" url="#cgi.SCRIPT_NAME#?goToSys=#Variables.SubSystemCd#&step=$SERIESLABEL$" fontsize="9" pieslicestyle="solid" 
            title="" foregroundcolor="##333333" tipbgcolor="##CCCCCC">
                <cfchartseries type="pie" paintstyle="shade" serieslabel="<b>Documents needed for each tab</b>"> 
                    <cfloop query="qDoc">
                        <cfset thisText = ListGetAt(Name, 1, "-") & "-">
                        <cfset thisTextPart2 = ListGetAt(Name, 2, "-")>
                        <cfset thisText = thisText & "\n" & left(thisTextPart2,24)>
						<cfif len(thisTextPart2) GT 24>
                            <cfset thisText = thisText & "\n" & mid(thisTextPart2,25,25)>
                        </cfif>
                        <cfchartdata item="#thisText#" value="#totalCount#">
                    </cfloop>
                </cfchartseries>
            </cfchart>
		</td>
        
        <cfif bShowDetailChart>
        	<td class="noborder" width="50%" valign="middle" align="left">           	
                <cfset nPercentUploaded = numberformat(qThisTab.uploadedCount / qThisTab.totalCount * 100, "__")>
                <cfset nFileNeed = qThisTab.totalCount - qThisTab.uploadedCount>
				<cfset nPercentNeeded = numberformat(nFileNeed / qThisTab.totalCount*100, "__")>
            	
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
                <link rel="stylesheet" href="/resources/demos/style.css">
                  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
				  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                   <style>
					  ##progressbar .ui-progressbar-value {
						background-color: ##66CC66;
					  }
					  .ui-progressbar {
						position: relative;
						margin-top:-280px;
						margin-right:100px;
					  }
					  .progress-label {
						position: absolute;
						left: 30%;
						top: 4px;
						font-weight: bold;
						text-shadow: 1px 1px 0 ##fff;
					  }
					  .progress-title {
						  position: relative;						
						top: -300px;
						font-weight: bold;
					  }
				  </style>
                  <script>
                  $( function() {
                    $( "##progressbar" ).progressbar({
                      value: #nPercentUploaded#					 
                    });
                  } );
                  </script>
                 
                <div class="progress-title">File(s) uploaded for : #url.step#</div>
                <div id="progressbar"><div class="progress-label">Document Upload Completed: #nPercentUploaded#%</div></div>
                
            	<!--- <cfchart format="html" showlegend="no" showborder="no" chartwidth="600" chartheight="500"  foregroundcolor="##333333" tipbgcolor="##CCCCCC" title="" style="chartstyle.xml">
    				
                    <cfchartseries type="pie" colorlist="##66CC66,##CCCCCC" serieslabel="<strong>File(s) uploaded for : #url.step#</strong>"> 
                        <cfif qThisTab.uploadedCount GT 0>                        	
                            <cfchartdata item="Uploaded" value="#nPercentUploaded#">
                        <cfelse>
                        	<cfchartdata item="" value="#qThisTab.uploadedCount#">
                        </cfif>
                        
                        <cfif nFileNeed>                        	
                        	<cfchartdata item="Needed" value="#nPercentNeeded#">
                        <cfelse>
                        	<cfchartdata item="" value="#nFileNeed#">
                        </cfif>
                    </cfchartseries>                   
                </cfchart> --->
            </td>
        </cfif>
    </tr>
    
    <cfif bShowDetailChart>
    	<tr>
        	<td colspan="2" class="noborder">
            	
            </td>
        </tr>
    </cfif>
 </table>
 </cfoutput>
<!---
AUTHOR:				Vidya
DATE:				05/27/2016
DESCRIPTION:		System setup
NOTES:				None
INPUT:
OUTPUT:				Setup
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
		REVISION HISTORY:	06/17/2016, VRVK:(OPSMDEV-903)PIMS centralized upload changes
--->

<cfset Variables.db					= "oracle_transaction_object">
<cfif IsDefined("Session.SubSystemName")>
<cfset Variables.SubSystemName	= Session.SubSystemName>
</cfif>
<cfif IsDefined("URL.SubSystemName")>
<cfset Variables.SubSystemName	= URL.SubSystemName>
</cfif>
<cfset Variables.URLToDeleteFile	= "/library/cfincludes/documents/set_uploadvariables_pims.cfm?act=Delete&SubSystemName=#Variables.SubSystemName#">
<cfif IsDefined("Variables.SubSystemName")>
	<CFSWITCH EXPRESSION="#Variables.SubSystemName#">
		<CFCASE VALUE="PIMS">
			<cfparam name = "Variables.BusPrcsTypCd"		default = 0 /><!--- limit the result set to the approprate business process --->
			<cfparam name = "Variables.PrcsMthdCd"			default = "" />
		</CFCASE>
	</CFSWITCH>
</cfif>

<!---<cfset Variables.BndRecCount = 0>--->
<cfset Variables.RecCount = 0>
       <cfif IsDefined("Variables.SubSystemName")> <cfif Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ "SBGBND">
<cfif IsDefined("Variables.db_docupload")>
	<cfset Variables.Db						= Variables.db_docupload>
</cfif></cfif></cfif>
<cfif IsDefined("Variables.SubSystemName")><cfif Ucase(trim(Variables.SubSystemName)) EQ "PIMS">
		<cfset Variables.Identifier				= "13">
		<CFSET Variables.LogAct				= "call DOCSELTSP">
		<cfset Variables.LocId				= session.lid>
        <cfset Variables.BusPrcsTypCd			= 0>
		<CFSET Variables.cfprname			= "PimsDocSearch">
		<cfinclude template="/cfincludes/oracle/pimsdocs/spc_DOCSELTSP.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		</cfif>
		<CFSET Variables.CanPimsDelete = "YES">
        <cfset Variables.ShowDeleteCheck	= Variables.CanPimsDelete></cfif>
<!---<CFIF NOT IsDefined("Url.act")>
<cfif IsDefined("Variables.SubSystemName")>
<cfif Ucase(trim(Variables.SubSystemName)) EQ "SBGBUS" OR Ucase(trim(Variables.SubSystemName)) EQ "SBGBND">
<CFIF Variables.CanSBGEAAgent>
	<CFSET Variables.CanBusDelete = "NO">
	<cfif IsDefined("Session.SBGAppInfoSubmit") AND (Session.SBGAppInfoSubmit EQ "A" OR Len(trim(Session.SBGAppInfoSubmit)) EQ 0)>
		<cfif IsDefined("Session.SBGAppInfoStatus") AND Len(trim(Session.SBGAppInfoStatus)) EQ 0>
			<CFSET Variables.CanBndDelete = "YES">
		<cfelseif isDefined("Session.SBGACTIONTYP") AND Session.SBGACTIONTYP EQ "Edit">
			<CFSET Variables.CanBndDelete = "YES">
		</cfif>
	</cfif>
<CFELSEIF Variables.CanSBGEAContractor>
	<CFSET Variables.CanBusDelete = "NO">
<CFELSEIF Variables.CanSBGPartner>
	<CFSET Variables.CanBusDelete = "NO">
<cfelse>
	<CFSET Variables.CanBndDelete = "YES">
</CFIF> </cfif></cfif>
	<cfif IsDefined("Variables.SubSystemName")><cfif Ucase(trim(Variables.SubSystemName)) EQ "SBGBUS">
		<CFSET Variables.Identifier			= "0">
		<CFSET Variables.LogAct				= "call BusDocSelCSP">
		<CFSET Variables.cfprname			= "BusDocSearch">
          <CFINCLUDE template = "#Variables.SPIncludesURL#/sbg/spc_#UCase('DocUpldSelCSP')#.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		</cfif>
        <cfset Variables.ShowDeleteCheck	= Variables.CanBusDelete>
	<cfelseif Ucase(trim(Variables.SubSystemName)) EQ "SBGBND">
		<CFSET Variables.Identifier			= "2">
		<CFSET Variables.LogAct				= "call BusDocSelCSP">
		<CFSET Variables.cfprname			= "BndDocSearchAll">
		<CFINCLUDE template = "#Variables.SPIncludesURL#/sbg/spc_#UCase('DocUpldSelCSP')#.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		<cfelse>
			<cfquery name="BndDocSearch" dbtype="query">
				SELECT * FROM BndDocSearchAll
					where BNDTYPCD = 'SBG Underwriting - SBG'
			</cfquery>
		</cfif>
        <cfset Variables.ShowDeleteCheck	= Variables.CanBndDelete>
        <cfelseif Ucase(trim(Variables.SubSystemName)) EQ "PSB">
        <CFSET Variables.BNDAPPCOMNSEQNMB	= Session.BNDAPPCOMNSEQNMB>
		<CFSET Variables.Identifier			= "2">
		<CFSET Variables.LogAct				= "call BusDocSelCSP">
		<CFSET Variables.cfprname			= "BndDocSearchAll">
		<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_#UCase('DocUpldSelCSP')#.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		<cfelse>
			<cfquery name="BndDocSearch" dbtype="query">
				SELECT * FROM BndDocSearchAll
					where BNDTYPCD = 'SBG Underwriting - PSB'
			</cfquery>
		</cfif><cfset Variables.ShowDeleteCheck	= "No">
        <cfelseif Ucase(trim(Variables.SubSystemName)) EQ "Claims">
        <cfif NOT IsDefined("Session.SBGNmb")>
        <cflocation url="/orasbgapps/claims/alarm/dsp_alarm.cfm" addtoken="No">
        </cfif>
        <CFSET Variables.SBGNmb				= Session.SBGNmb>
        <CFSET Variables.CreatUserId		= Variables.origuserid>
		<CFSET Variables.Identifier			= "3">
		<CFSET Variables.LogAct				= "call BusDocSelCSP">
		<CFSET Variables.cfprname			= "BndDocSearchAll">
		<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_#UCase('DocUpldSelCSP')#.cfm">
		<cfif Variables.TxnErr>
			<cfdump var=#Variables.ErrMsg#><cfabort>
		<cfelse>
			<cfquery name="BndDocSearch" dbtype="query">
				SELECT * FROM BndDocSearchAll
					where BNDTYPCD = 'SBG Claim - SBG'
			</cfquery>
		</cfif><cfset Variables.ShowDeleteCheck	= "No">
	</cfif></cfif>

    <cfif Variables.SubSystemName EQ "SBGBUS">
				<cfset Variables.checkboxname	= "DelBndBus">
                <cfset Variables.checkboxvalue	= "DocumentResult.DocUpldSeqNmb">
			<cfelseif Variables.SubSystemName EQ "SBGBND">
				<CFIF NOT (Variables.CanSBGEAAgent)>
						<cfset Variables.checkboxname	= "DelBndBus">
                        <cfset Variables.checkboxvalue	= "DocumentResult.DocUpldSeqNmb">
                        <cfset Variables.hiddenvariablename	= "DelSubNmb">
                        <cfset Variables.hiddenvariablevalue	= "DocumentResult.BndAppSubNmb">
				<cfelse>
					<cfif Variables.BndAppSubNmb EQ  DocumentResult.BndAppSubNmb>
                        <cfset Variables.checkboxname	= "DelBnd">
                        <cfset Variables.checkboxvalue	= "DocumentResult.DocUpldSeqNmb">
                        <cfset Variables.hiddenvariablename	= "DelSubNmb">
                        <cfset Variables.hiddenvariablevalue	= "DocumentResult.BndAppSubNmb">
					</cfif>
				</cfif>
			</CFIF>

    <cfif IsDefined("Variables.SubSystemName")> <cfif Variables.SubSystemName EQ "SBGBUS" OR Variables.SubSystemName EQ "SBGBND">
<cfset Variables.DocUpldrUsrTyp = "S">
<cfif Variables.CanSBGEAContractor>
	<cfset Variables.DocUpldrUsrTyp = "C">
<cfelseif Variables.CanSBGEAAgent>
	<cfset Variables.DocUpldrUsrTyp = "A">
</cfif></cfif></cfif>
 <cfif IsDefined("Variables.SubSystemName")><cfif Variables.SubSystemName EQ "PSB">
<cfset Variables.DocUpldrUsrTyp = "S">
<cfif Variables.CANSBGPSBPARTNER>
	<cfset Variables.DocUpldrUsrTyp = "A">
</cfif></cfif></cfif>--->
<cfset Variables.tmpdb				= Variables.db />
<cfset Variables.db					= "oracle_spc_caob1" />
<cfset Variables.Identifier			= "11" />
<cfset Variables.ShowComments		= "Yes" /><!---
<cfset Variables.ShowDeleteCheck	= "Yes" />--->
<cfset Variables.ShowFinalize	= "Yes" />
<cfparam name = "Variables.dbtype"				default = "Oracle" />
<cfparam name = "Variables.OracleSchemaName"	default = "sbaref" />
<cfparam name = "Variables.SPCUrl"      		default = "/cfincludes/oracle/sbaref" />
<cfparam name = "Variables.AppSchemaName"		default = "sbg" />
<cfparam name = "Variables.AppSPCUrl"      		default = "/cfincludes/oracle/pimsdocs" />
<cfparam name = "Variables.UploadedDocsSPC"		default = "DOCSELTSP" />
<cfif IsDefined("Variables.SubSystemName") AND Variables.SubSystemName EQ "PIMS">
<cfset Variables.PageTitle			= "PIMS Upload Documents">
</cfif>

<CFIF NOT IsDefined("Variables.OnRefresh")>
<cfset Variables.DocumentResult	= Variables.PimsDocSearch>
<cfinclude template = "/library/cfincludes/documents/dsp_fileupload.cfm" />
</CFIF>
<cfset Variables.db = Variables.tmpdb />
</cfif>



<!--- Deleting records --->
<cfset Variables.db					= "oracle_spc_caob1" />
<CFIF IsDefined("Url.act") AND Url.act EQ "Delete">
<cfset Variables.USERNAME					= form.USERNAME />
<cfset Variables.PASSWORD					= form.PASSWORD />
<CFSET Variables.BusId		= "">
<CFSET Variables.BusDel		= "NO">
<CFSET Variables.BndDel		= "NO">
<CFSET Variables.BndAppComnSeqNmb		= "">
<CFSET Variables.BndAppSubNmb= "">
<cfset Variables.SubSystemName = "Bus">
<cfset Variables.SaveMe	= "Yes">
<CFIF IsDefined("Url.SubSystemName")>
	<cfset Variables.SubSystemName						= url.SubSystemName>
</cfif>
<cfif IsDefined("Variables.db_docupload")>
	<cfset Variables.Db						= Variables.db_docupload>
</cfif>
<!---<CFIF IsDefined("Url.BusDel")>---><CFIF IsDefined("Variables.SubSystemName") AND Variables.SubSystemName EQ "SBGBUS">
	<cfset Variables.BusDel						= "Yes">
	<cfif IsDefined ("Session.Ein_Ssn") AND IsDefined ("Session.BusID")>
		<cfif Ucase(trim(Session.Ein_Ssn)) EQ "EIN">
			<CFSET Variables.BusId		= "1#Session.BusID#">
		<cfelse>
			<CFSET Variables.BusId		= "2#Session.BusID#">
		</cfif>
	</cfif>
</cfif>
<!---<CFIF IsDefined("Url.BndDel")>---><CFIF IsDefined("Variables.SubSystemName") AND (Variables.SubSystemName EQ "SBGBND" OR Variables.SubSystemName EQ "PSB")>
	<cfset Variables.BndDel						= "Yes">
	<cfif IsDefined ("Session.BndAppCmnSeqNm")>
		<CFSET Variables.BndAppComnSeqNmb		= Session.BndAppCmnSeqNm>
	</cfif>
	<cfif IsDefined ("Session.SubNm")>
		<CFSET Variables.BndAppSubNmb		= Session.SubNm>
	</cfif>
	<cfif len(trim(Variables.BndAppSubNmb)) EQ 0>
		<CFSET Variables.BndAppSubNmb		= 0>
	</cfif>
</cfif>
<cfset Variables.SelectedDelRecord = "">
<cfset Variables.ErrMsg	= "Error while processing the request:<BR>">
	<cfif Variables.BusDel>
		<cfloop index="Idx" from="1" to="#FORM.RecCount#">
			<cfif IsDefined("DelBndBus#Idx#")>
				<cfset Variables.SelectedDelRecord = #evaluate('form.DelBndBus'&Idx)#>
				<CFSET Variables.DOCUPLDSEQNMB		= Variables.SelectedDelRecord>
				<CFSET Variables.Identifier			= "1">
				<CFSET Variables.LogAct				= "call BusDocUpdTSP">
				<CFSET Variables.cfprname			= "DocumentDelete">
                <CFSET Variables.LASTUPDTUSERID			= form.LASTUPDTUSERID>

				<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_#UCase('DOCUPLDUPDTSP')#.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe	= "NO">
					<cfinclude template="/library/cfincludes/put_sbalookandfeel_saveformdata.cfm">
					<cflocation url="/orasbgapps/underWriting/upload/dsp_centralizedupload.cfm?SubSystemName=#Variables.SubSystemName#&DisplayMsg=#Variables.ErrMsg#" addtoken="No">
				</cfif>
			</cfif>
		</cfloop>
	<cfelseif Variables.BndDel>
		<cfloop index="Idx" from="1" to="#FORM.RecCount#">
			<cfif IsDefined("DelBndBus#Idx#")>
				<cfset Variables.SelectedDelRecord = #evaluate('form.DelBndBus'&Idx)#>
				<cfset Variables.SelectedBndAppSubNmb = #evaluate('form.DelSubNmb'&Idx)#>
				<CFSET Variables.BndAppSubNmb			= Variables.SelectedBndAppSubNmb>
				<CFSET Variables.DOCUPLDSEQNMB			= Variables.SelectedDelRecord>
				<CFSET Variables.Identifier			= "1">
				<CFSET Variables.LogAct				= "call BndDocUpdTSP">
				<CFSET Variables.cfprname			= "DocumentDelete">
				<CFINCLUDE template = "/cfincludes/oracle/sbg/spc_#UCase('DOCUPLDUPDTSP')#.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe	= "NO">
					<cfinclude template="/library/cfincludes/put_sbalookandfeel_saveformdata.cfm">
					<cflocation url="/orasbgapps/underWriting/upload/dsp_centralizedupload.cfm?SubSystemName=#Variables.SubSystemName#&DisplayMsg=#Variables.ErrMsg#" addtoken="No">
				</cfif>
			</cfif>
		</cfloop>
	</cfif>

<cfif len(Variables.SelectedDelRecord) EQ 0>
	<cfinclude template="/library/cfincludes/put_sbalookandfeel_saveformdata.cfm">
		<cfset Variables.ErrMsg = Variables.ErrMsg
									  & "<BR><Li>No Document has been selected.</LI><br><br>">
		<cflocation url="/orasbgapps/underWriting/upload/dsp_centralizedupload.cfm?SubSystemName=#Variables.SubSystemName#&DisplayMsg=#Variables.ErrMsg#" addtoken="No">
	<cfelseif Variables.SaveMe>
		<cfset Variables.DisplayMsg = "Selected Documents have been deleted.">
		<cflocation url="/orasbgapps/underWriting/upload/dsp_centralizedupload.cfm?SubSystemName=#Variables.SubSystemName#&DisplayMsg=#Variables.DisplayMsg#" addtoken="No">
	</cfif>
</CFIF>
<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/06/2008
DESCRIPTION:		Shared routine to copy Variables.GLS into assorted other Variables variable names. 
NOTES:

	This file is basically for Application.cfm files that want to define all manner of GLS variables at 
	the Variables scope level (not just leave them inside the Variables.GLS structure). So it's basically 
	a stepping stone for older apps that don't make good use of Variables.GLS as-is. 

	For example, Variables.GLS.IMUserTbl contains information about the currently logged-in user, such as 
	Variables.GLS.IMUserTbl.IMUserFirstNm, Variables.GLS.IMUserTbl.IMUserLastNm, etc. Apparently, typing 
	"GLS." is too much effort, so many apps reference Variables.IMUserTbl.IMUserFirstNm, etc, instead, or 
	even Variables.IMUserFirstNm, etc. This routine copies the GLS structure and all of its substructures 
	(one level further down) into the Variables scope. It wastes memory, but it keeps old code working. 

	Typical usage in an Application.cfm file (using parentheses to avoid confusing CF Server): 

		(cfset Variables.GLS						= StructNew())
		(cfset Variables.GLS.GLSAuthorized			= "No")
		(cflock scope="Session" type="Readonly" timeout="30")
			(cfif IsDefined("Session.GLS"))
				(cfset Variables.GLS				= Duplicate(Session.GLS))
			(/cfif)
		(/cflock)
		(cfif NOT Variables.GLS.GLSAuthorized)
			(!--- Kick the user out of your directory and into a login page, usually /gls/dsp_login.cfm. ---)
		(/cfif)
		(cfinclude template="/library/cfincludes/act_login_establish_variables_vars.cfm")

	New apps should only be using the GLS structure and its substructures, not the assorted other Variables 
	variables created below. At some point, we would like to have only Variables.GLS in the Variables scope, 
	but we can't do that as long as people keep assuming that its substructures are also defined in Variables. 

	In addition, if you modify GLS variables in the Session scope, you should modify just Session.GLS.whatever. 
	There is no need to modify Session.whatever too. Session.GLS should always be the most trustworthy data. 

INPUT:				Variables.GLS. 
OUTPUT:				Other Variables variables (for now). 
REVISION HISTORY:	07/02/2008, SRS:	Changes to support saving memory. 
					03/06/2008, SRS:	Original Implementation. 
--->

<cfloop index="Level1Key"							list="#StructKeyList(Variables.GLS)#">
	<cfif IsStruct(Variables.GLS[Level1Key])>
		<cfset Variables[Level1Key]					= Duplicate(Variables.GLS[Level1Key])>
		<cfloop index="Level2Key"					list="#StructKeyList(Variables.GLS[Level1Key])#">
			<cfset Variables[Level2Key]				= Variables.GLS[Level1Key][Level2Key]>
		</cfloop>
	<cfelseif CompareNoCase(Level1Key,"ArrayAllUserRoles") IS NOT 0>
		<!---
		Never copy Variables.GLS.ArrayAllUserRoles to Variables.ArrayAllUserRoles. No library routine will ever 
		refernece it as anything other than Variables.GLS.ArrayAllUserRoles. Big waste of memory to copy it. 
		--->
		<cfset Variables[Level1Key]					= Variables.GLS[Level1Key]>
	</cfif>
</cfloop>
<cfif NOT IsDefined("Variables.ArrayUserRoles")>
	<cfif NOT IsDefined("TrimArrayUserRoles")>
		<cfinclude template="/library/udf/bld_SecurityUDFs.cfm">
	</cfif>
	<!---
	We don't know in this included routine whether or not the caller is a Web Service. Therefore, include roles 
	with the Web Service indicator, just in case they're needed. The caller will probably build ArrayUserRoles 
	explicitly anyway. This prevents crashes in Web Services that haven't been modified to adapt to trimmed down 
	GLS structure. 
	--->
	<cfset TrimArrayUserRoles("Yes")><!--- "Yes" = "include Web Service only roles". --->
</cfif>

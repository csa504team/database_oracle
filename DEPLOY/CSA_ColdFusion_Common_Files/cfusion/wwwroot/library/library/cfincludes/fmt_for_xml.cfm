<!---
AUTHOR:				Steve Seaquist
DATE:				02/25/2001
DESCRIPTION:		Formats QName.FName as <XName>value</XName>, with value formatted for XML. 
NOTES:				None
INPUT:				Variables.QName, Variables.FName, Variables.XName, Variables.Indent, Variables.CRLF, Variables.Idx
OUTPUT:				Variables.XmlErrDetail, Variables.XmlString
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	03/29/2001, SRS:	Extracted out fmt_FValue_for_xml. 
					02/25/2001, SRS:	Original implementation.
--->
<CFIF IsDefined(Variables.QName & "." & Variables.FName)>
	<CFSET Variables.FValue						= Evaluate(Variables.QName & "." & Variables.FName)>
	<CFIF Len(Variables.FValue) GT 0>
		<CFINCLUDE TEMPLATE="fmt_FValue_for_xml.cfm">
	</CFIF>
	<CFSET Variables.XmlString					= Variables.XmlString 
												& Variables.Indent
												& "<#Variables.XName#>#Variables.FValue#</#Variables.XName#>"
												& Variables.CRLF>
<CFELSE>
	<CFSET Variables.XmlErrDetail				= Variables.XmlErrDetail 
												& "#Variables.QName#.#Variables.FName# not defined."
												& Chr(13) & Chr(10)>
</CFIF>

<cfinclude template="get_FVal.cfm">
<cfif Len(Variables.FVal) GT 0>
	<cfif Len(Variables.FVal) IS NOT 5>
		<cfset Variables.ErrMsg					= "#Variables.ErrMsg# <li>#Variables.FName# must be exactly 5 characters.</li>">
	<cfelse>
		<cfset Variables.FValValid				= "Yes">
		<!--- The following assures that FVal is in 9NNN9 format, where N is alphabetic or numeric: --->
		<cfloop index="i" from="1" to="#Len(Variables.FVal)#">
			<cfset Variables.FValCh	= Asc(Mid(Variables.FVal, i, 1))>
			<cfif (i IS 1) OR (i IS 5)>
				<!--- Slightly faster than IsNumeric: --->
				<cfif NOT	(	(Variables.FValCh GE 48) AND (Variables.FValCh LE  57))>
					<cfset Variables.FValValid	= "No">
					<cfbreak>
				</cfif>
			<cfelse>
				<!--- The problem with ... GT "A" AND ... LT "Z" is that they aren't case-sensitive: --->
				<cfif NOT	(	(Variables.FValCh GE 48) AND (Variables.FValCh LE  57)
							OR	(Variables.FValCh GE 65) AND (Variables.FValCh LE  90)
							OR	(Variables.FValCh GE 97) AND (Variables.FValCh LE 122))>
					<cfset Variables.FValValid	= "No">
					<cfbreak>
				</cfif>
			</cfif>
		</cfloop>
		<cfif NOT Variables.FValValid>
			<cfset Variables.ErrMsg				= "#Variables.ErrMsg# <li>#Variables.FName# is not in 9NNN9 format "
												& "(CAGE code format), where 9 is numeric and N is alphabetic or numeric.</li>">
		</cfif>
	</cfif>
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				09/26/2005
DESCRIPTION:		Saves the Form scope to the Session scope. Use in "form data recovery". 
NOTES:				Called by act files when returning to the associated dsp file due to failed presave validation. Although 
					get_sbalookandfeel_variables.cfm also does this, it only does so if the PageNames mechanism is in use. 
					This include is for manually controlling form data recovery, or to resave the Form scope after it has been 
					modified. DO NOT INCLUDE THIS FILE FROM WITHIN ANY SESSION LOCK. To do so would result in nested locks of 
					the Session scope, which causes a deadlock. Instead use the "_nolock" version. 
INPUT:				Variables in the Form scope. 
OUTPUT:				Variables in the Session scope. 
REVISION HISTORY:	09/28/2005, SRS:	Original implementation. 
--->

<cflock scope="Session" type="Exclusive" timeout="30">
	<cfinclude template="put_sbalookandfeel_saveformdata_nolock.cfm">
</cflock>


// Start of dsp_formdata_userroles.js		// (in case included inline)

// These routines are separate from dsp_formdata_userroles.cfm so that they can be included in the caller's JSInline. 
// Because they're in a separate *.js file, they can be cached by the browser, which speeds up page load times. That 
// was the main reason for separating out into a separate file. 
//
// But there are other advantages to having these routines in a separate file: We don't have to double the pound-signs 
// in "#RolesFor", which makes the code more readable. Also, when viewing this file in HomeSite or Dreamweaver, the code 
// will be color-coded (syntax-highlighted) according to JavaScript syntax, which also makes it more readable. 
//
//											Steve Seaquist
//											Trusted Mission Solutions, Inc.
//											01/06/2010
//
// REVISION HISTORY:	10/25/2011, SRS:	Greatly simplified Show/Hide hotlinks (new function ToggleRoleContents) with 
//											a new enclosing <div id="RoleContents_xx">. Made ToggleRoleContents always 
//											toggle show/hide, regardless of checkbox checked/unchecked (also simpler). Made 
//											role checkbox script (ToggleRole) force Show if checked or Hide if unchecked. 
//						01/06/2010, SRS:	Original implementation. 

// Configuration Parameters (In scope notation, "k" prefix is short for German "Konstant", hence, static global variable): 

var	kFolderIconFileClosed					= "sbawfolderclosed.gif";
var	kFolderIconFileOpen						= "sbawfolderopen.gif";
var	kFolderIconSrcClosed					= "/library/images/sbatree/" + kFolderIconFileClosed;
var	kFolderIconSrcOpen						= "/library/images/sbatree/" + kFolderIconFileOpen;

// Functions: 

function ToggleRole							(pThis, pPrevRequests, pShowAuthMsg, pRoleNumber)
	{
	if	(pPrevRequests > 0)
		if	(pThis.checked)
			pThis.checked					= confirm("You have previously requested access for "
													+ "'"+pThis.title+"' "+pPrevRequests+" time(s). "
													+ "Those requests were not acted upon. "
													+ "Are you sure you want to request access again?");
	if	(pShowAuthMsg)
		if	(pThis.checked)
			//pThis.checked					= confirm("You have requested access to a Federal System which "
			//										+ "requires prior authorization from the program office "
			//										+ "and from your supervisor. If you have not received "
			//										+ "prior authorization, you must click Cancel1.");
	        pThis.checked	  			    = confirm("You are requesting access to a Federal System."
			+" Once you click Submit at the bottom of this page,"
			+" your request will go to the appropriate SBA offices for approval."
			+" You will later get an email, usually within 1-3 business days,"
			+" telling you whether the access has been granted or not.");
	var	sRoleContents						= document.getElementById("RoleContents_"	+ pRoleNumber);
	var	sRolePrivs							= document.getElementById("RolePrivs_"		+ pRoleNumber);
	if	(sRoleContents)						// Do nothing if sRoleContents not defined. 
		sRoleContents.style.display			= (pThis.checked ? "" : "none");
	if	(sRolePrivs)						// Do nothing if sRolePrivs not defined. 
		sRolePrivs.style.display			= "";	// Once we've done a role-level show/hide, privs default to shown. 
	}

function ToggleRoleContents					(pRoleNumber)
	{
	var	sRoleContents						= document.getElementById("RoleContents_"	+ pRoleNumber);
	var	sRolePrivs							= document.getElementById("RolePrivs_"		+ pRoleNumber);
	// The following takes care of the initial load state of contents shown but privs hidden (because no privs checked): 
	if ((sRolePrivs)
	&&	(sRolePrivs.style)
	&&	(sRolePrivs.style.display)
	&&	(sRolePrivs.style.display			== "none"))
		{
		sRolePrivs.style.display			= "";	// Once we've done a role-level show/hide, privs default to shown. 
		// Since sRolePrivs were hidden, merely showing them is a change, so don't risk hiding sRoleContents: 
		if	(sRoleContents)					// Do nothing if sRoleContents not defined. 
			sRoleContents.style.display		= "";
		return;
		}
	// Now that that's taken care of, toggle sRoleContents and force sRolePrivs to shown: 
	if	(sRoleContents)						// Do nothing if sRoleContents not defined. 
		{
		if ((!sRoleContents.style)			// If sRoleContents.style			is not defined, it's the same as show. 
		||	(!sRoleContents.style.display)	// If sRoleContents.style.display	is not defined, it's the same as show. 
		||	(sRoleContents.style.display	== ""))
			sRoleContents.style.display		= "none";
		else
			sRoleContents.style.display		= "";
		}
	if	(sRolePrivs)						// Do nothing if sRolePrivs not defined. 
		sRolePrivs.style.display			= "";	// Once we've done a role-level show/hide, privs default to shown. 
	}

function ToggleSystem						(pSysTypCd)
	{
	var	sSystemContents						= document.getElementById('SystemContents_'	+ pSysTypCd);
	var	sSystemFolder						= document.getElementById("SystemFolder_"	+ pSysTypCd);
	if	(!sSystemContents)
		{
		alert("ERROR. Please report this error to the SBA's Office of the CIO: "
			+ "System number '" + pSysTypCd + "' has a broken show/hide script.");
		return;
		}
	if	(!sSystemFolder)
		{
		alert("ERROR. Please report this error to the SBA's Office of the CIO: "
			+ "Folder image number '" + pSysTypCd + "' not found.");
		return;
		}
	//	sImg will be defined in all browsers, but sSystemContents.style.display might not be, so test sImg.src: 
	if	(sSystemFolder.src.substring(sSystemFolder.src.length - kFolderIconFileOpen.length, sSystemFolder.src.length) == kFolderIconFileOpen)
		{
		sSystemContents.style.display		= "none";
		sSystemFolder.src					= kFolderIconSrcClosed;
		}
	else
		{
		sSystemContents.style.display		= "";
		sSystemFolder.src					= kFolderIconSrcOpen;
		}
	}

function UncheckAll(pSysTypCd)				// Clear all checkboxes in a single-role system when any role is clicked. 
	{// (The caller is responsible for saving and restoring the role checkbox that triggered this function call.) 
	$("#SystemContents_" + pSysTypCd + " input[type=checkbox]")// All checkboxes in system, regardless of role or privilege. 
		.each(function()
			{
			this.checked					= false;
			});
	}

// End of dsp_formdata_userroles.js			// (in case included inline)

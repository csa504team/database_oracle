<!---
AUTHOR:				Steve Seaquist
DATE:				05/16/2006
DESCRIPTION:		Displays the onSubmit JavaScript associated with dsp_formdata_contact. 
NOTES:				Since this file may be cfincluded into the onSubmit handler of a form tag (and hence be delimited by 
					double-quotes), do not use double-quotes to delimit non-numeric literals. Use single-quotes instead. 
INPUT:				None. 
OUTPUT:				Displays the onSubmit JavaScript associated with dsp_formdata_contact. 
REVISION HISTORY:	10/17/2012, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to changes in dsp_formdata_contact. 
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables. Allowed this file to be included inside 
										the onsubmit handler of a form tag (default), or in external function. 
					05/16/2006, SRS:	Original implementation.
--->

<cfinclude template="bld_readonly_contact.cfm">
<cfparam name="Variables.OnSubmitFormName"				default="this"><!--- Appropriate in form tag onsubmit handler. --->

<cfif NOT Variables.ReadOnlyIMCntryCd>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMCntryCd.onchange())			return false;
if	(#Variables.OnSubmitFormName#.IMCntryCd.options[#Variables.OnSubmitFormName#.IMCntryCd.selectedIndex].value == 'US')
	{</cfoutput>
	<cfif NOT Variables.ReadOnlyIMUserZipCd>
		<cfoutput>
	if	(!#Variables.OnSubmitFormName#.Zip5Cd.onchange())			return false;
	if	(!#Variables.OnSubmitFormName#.Zip4Cd.onblur())				return false;</cfoutput>
	</cfif>
	<cfif NOT Variables.ReadOnlyStCd>
		<cfoutput>
	if	(!#Variables.OnSubmitFormName#.StCd.onchange())				return false;</cfoutput>
	</cfif>
	<cfoutput>
	}
else
	{</cfoutput>
	<cfif NOT Variables.ReadOnlyIMUserPostCd>
		<cfoutput>
	if	(!#Variables.OnSubmitFormName#.IMUserPostCd.onchange())		return false;</cfoutput>
	</cfif>
	<cfif NOT Variables.ReadOnlyIMUserStNm>
		<cfoutput>
	if	(!#Variables.OnSubmitFormName#.IMUserStNm.onchange())		return false;</cfoutput>
	</cfif>
	<cfoutput>
	}</cfoutput>
<cfelseif Variables.IMCntryCd IS "US">
	<cfif NOT Variables.ReadOnlyIMUserZipCd>
		<cfoutput>
if	(!#Variables.OnSubmitFormName#.Zip5Cd.onchange())				return false;
if	(!#Variables.OnSubmitFormName#.Zip4Cd.onblur())					return false;</cfoutput>
	</cfif>
	<cfif NOT Variables.ReadOnlyStCd>
		<cfoutput>
if	(!#Variables.OnSubmitFormName#.StCd.onchange())					return false;</cfoutput>
	</cfif>
<cfelse>
	<cfif NOT Variables.ReadOnlyIMUserPostCd>
		<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserPostCd.onchange())			return false;</cfoutput>
	</cfif>
	<cfif NOT Variables.ReadOnlyIMUserStNm>
		<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserStNm.onchange())			return false;</cfoutput>
	</cfif>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserCtyNm>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserCtyNm.onchange())			return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserStr1Txt>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserStr1Txt.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserStr2Txt>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserStr2Txt.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserPhnCntryCd>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserPhnCntryCd.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserPhnAreaCd>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserPhnAreaCd.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserPhnLclNmb>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserPhnLclNmb.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserPhnExtnNmb>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserPhnExtnNmb.onchange())		return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserEmailAdrTxt>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserEmailAdrTxt.onchange())	return false;
if	(!#Variables.OnSubmitFormName#.IMUserEmailAdrTxt2.onchange())	return false;
//if(#Variables.OnSubmitFormName#.IMUserEmailAdrTxt.value			!= #Variables.OnSubmitFormName#.IMUserEmailAdrTxt2.value)
//	{
//	alert('ERROR. E-Mail Address and Re-enter E-Mail don\'t match. Correct whichever one is in error.');
//	return false;
//	}</cfoutput>
</cfif>

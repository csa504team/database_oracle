<!---
	AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
	DATE:				07/30/2015.
	DESCRIPTION:		Currently just a validation strategy tester. Evolving into the real thing.
	NOTES:				None.
	INPUT:				Form variables, Variables.CancellationPage, Session.Origs.
	OUTPUT:				Save profile to the security schema.
	REVISION HISTORY:	08/01/2015, NS: Added validations,SPs calls and emails
						07/30/2015,	SRS:	Original implementation as part of the new act_userprofile that consolidates all user
	profile act pages. We now have no prior knowledge as to which which columns a caller may
	chose to make uneditable, so we have to rely on the Origs structure built in the initial
	load of dsp_userprofile from the database.
	--->

<cffunction name="SomethingChanged"							returntype="boolean">
	<cfargument name="ColumnList"							type="String" required="Yes">
	<cfset var Local										= {}>
	<cfloop index="Local.ColName" list="#Arguments.ColumnList#">
		<cfif Variables[Local.ColName]						is not Variables.Origs[Local.ColName]>
			<cfreturn true>
		</cfif>
	</cfloop>
	<cfreturn false>
</cffunction>
<!--- <cfdump var="#Session#"><cfabort> --->
<!--- Configuration Parameters: --->
<!--- <cfdump var="#form#"><cfdump var="#Variables.Security.SelectedUser#"><cfdump var="#Variables.Origs#"><cfdump var="#request#"><cfabort>--->
<cfparam name="Variables.CancellationPage"	type="String">
<!--- No default attribute, therefore, a mandatory input. --->
<cfset Variables.DisplayPage								= Replace(Variables.PageName, "act_", "dsp_", "One")>
<cfparam name="Variables.SuccessPage"		type="String"	default="#Variables.DisplayPage#">
<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfparam name="Variables.UpdateTrace"						default="No">
<!--- Check whether user should even be at this page: --->
<cfif		NOT IsDefined("Form.FieldNames")>
	<cflocation addtoken="No" url="#Variables.DisplayPage#">
	<!--- Just go there, without PrevErr and without ErrMsg. --->
<cfelseif	IsDefined("Form.SubmitButton")
		and		(Form.SubmitButton is "Cancel")>
	<!--- Keep the cancellation SubmitButton value in sync with dsp_userprofile. --->
	<cflocation addtoken="No" url="#Variables.CancellationPage#">
</cfif>
<cfinclude template="#Variables.LibUdfURL#/bld_DspPageLoadTimeOrigsUDFs.cfm">
<cfinclude template="#Variables.LibUdfURL#/bld_PasswordUDFs.cfm">
<cfset GetDspPageLoadTimeOrigs()>
<!--- Retrieve Session.Origs. We mainly want "MayEdit" right now. --->
<cfif NOT Variables.OrigsSuccess>
	<!---
		If OrigsSuccess isn't "Yes", it probably means that Session.DspPageLoadTimeOrigs.DspPage_Handshake wasn't defined,
		or, if it was, the Handshake didn't reference the right display page. Either way, it's rather equivalent to detecting
		NOT IsDefined("Form.FieldNames"). So treat it the exact same way as above:
		--->
	<cflocation addtoken="No" url="#Variables.DisplayPage#">
	<!--- Just go there, without PrevErr and without ErrMsg. --->
</cfif>
<cfif NOT Variables.Origs.MayEdit>
	<!--- We WANT to crash if MayEdit is undefined (to debug), so just assume it is defined. --->
	<!---
		This condition will probably never happen, because the dsp page should set ReadOnlyProfile, which should result in
		its not generating the form nor its submit buttons. But theoretically, a hacker could manufacture a form submission.
		So there's no harm done in planning for it and telling the hacker "shame on you":
		--->
	<cfset Variables.ErrMsg									= "Error: You are not allowed to save this profile to the database. ">
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>
<!--- Initializations: --->
<cfparam name="Variables.Commentary"						default="">
<cfset Variables.ErrMsgBase									= "Error(s) occurred that prevented saving the page's data: ">
<cfset Variables.ErrMsg										= Variables.ErrMsgBase>
<cfset Variables.SaveMe										= "Yes">
<cfset Variables.TxnErr										= "No">
<cfset Variables.PageIsAddCustomer							= "No">
<cfset Variables.PageIsProfile								= "No">
<cfset Variables.PageIsUser									= "No">
<cfset Variables.SendEmailUserTypeFlag 						= false>
<cfset Variables.SendEmailAddrEmailFlag 					= false>
<cfset Variables.SendEmailSupervisorFlag 					= false>
<cfset Variables.SendHQLocEmailFlag							= false>
<cfset Variables.JobTitleChanged 							= false>
<cfset Variables.ErrSeqNmb									= "0">
<cfset Variables.PageTitle 									= "">
<cfset Variables.DevTeamEml 								= ",nelli.salatova@sba.gov,rahul.gundala@sba.gov,matthew.forman@sba.gov">
<cfswitch expression="#CGI.Script_Name#">
	<cfcase value="/cls/act_addcustomer.cfm,/cls/act_addcustomer2.cfm">
		<cfset Variables.PageIsAddCustomer						= "Yes">
		<cfset Variables.SuccessPage							= Request.SlafLoginURL>
	</cfcase>
	<cfcase value="/cls/act_profile.cfm,/cls/act_profile2.cfm">
		<cfset Variables.PageIsProfile							= "Yes">
		<cfset Variables.SuccessPage							= "dsp_choosefunction.cfm">
	</cfcase>
	<cfcase value="/security/user/act_user.cfm,/security/user/act_user2.cfm">
		<cfset Variables.PageIsUser								= "Yes">
	</cfcase>
</cfswitch>
<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.ActvIMAsurncLvlTbl						= Server.Scq.security.ActvIMAsurncLvlTbl>
	<cfset Variables.ActvIMJobTitlTypTbl					= Server.Scq.security.ActvIMJobTitlTypTbl>
	<cfset Variables.ActvIMUserSuspRsnTbl					= Server.Scq.security.ActvIMUserSuspRsnTbl>
	<cfset Variables.ActvIMUserTypTbl						= Server.Scq.security.ActvIMUserTypTbl>
	<cfset Variables.ScqActvCountries						= Server.Scq.ActvIMCntryCdTbl>
	<!--- Oddly, not used by addcustomer. --->
	<cfset Variables.ScqActvStates							= Server.Scq.ActvStTbl>
	<!--- Oddly, not used by addcustomer. --->
</cflock>
<!--- Simple Validity Edits: --->
<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "About to call val_calls.<br/>">
</cfif>
<cfinclude template="act_userprofile.val_calls.cfm">
<!--- Does it's own cflocation if SaveMe is "No". --->
<cfif Variables.UpdateTrace <!--- or "Yes" --->>
	<!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed simple validity edits.<br/>">
</cfif>
<cfset Variables.CallValidateIMUserSuprvCSP					= "No">
<cfset Variables.CallPasswordSetRoutine						= "No">
<cfset Variables.SendEmailWithTempRswd 						= "No">
<cfset Variables.Variables.SendEmailVerify 					= "No">
<cfset Variables.TempPswd 									= "">
<cfset Variables.IMUserSuspRsnCdOverr						= Variables.IMUserSuspRsnCd>
<cfset Variables.ColumnListIMUserTbl						= "IMUserNm,IMUserTypCd,"
															& "IMUserPrtNm,IMUserFirstNm,IMUserLastNm,IMUserMidNm,IMUserSfxNm,"
															& "IMUserDOBDt,IMUserLast4DgtSSNNmb,"
															& "IMUserStr1Txt,IMUserStr2Txt,IMUserCtyNm,StCd,Zip5Cd,Zip4Cd,IMCntryCd,IMUserStNm,IMUserPostCd,"
															& "IMUserPhnCntryCd,IMUserPhnAreaCd,IMUserPhnLclNmb,IMUserPhnExtnNmb,"
															& "IMUserEmailAdrTxt,IMUserEmailAdrTxt2,"
															& "HQLocId,LocId,PrtId,PrtLocNm,SBAOfcCd,SBAOfc1Nm,IMAsurncLvlCd,"
															& "IMAsurncLvlCd,IMUserActvInactInd,IMUserSuspRsn,IMUserSuspRsnCd,IMUserSuspRsnDt,"
															& "IMUserSuprvId,IMUserSuprvFirstNm,IMUserSuprvMidNm,IMUserSuprvLastNm,IMUserSuprvEmailAdrTxt,IMUserSuprvUserId">
<cfset Variables.ColumnListBusinesses						= "IMUserBusSeqNmb,IMUserBusEINSSNInd,IMUserBusTaxId,IMUserBusLocDUNSNmb,BusEINCertInd">
<!--- <cfloop index="i" from="1" to="#Variables.RecordCountUserBus#"><!--- Rows --->
<cfset Variables.ColumnListBusinessesIndexed		= "">
<cfloop index="Variables.Key" list="#Variables.ColumnListBusinesses#">
	<!--- Columns --->
	<cfset Variables.IndexedKey						= "#Variables.Key#_#i#">
	<cfset Variables[Variables.Key]					= Variables[Variables.IndexedKey]>
	<!--- For saving to the database if changed. --->
	<cfset Variables.ColumnListBusinessesIndexed	= ListAppend(Variables.ColumnListBusinessesIndexed, Variables.IndexedKey)>
</cfloop>
<!--- Now ColumnListBusinessesIndexed has the same column names as ColumnListBusinesses, but with "_#i#" suffixed. --->
<cfif SomethingChanged(Variables.ColumnListBusinessesIndexed)>
	<!--- Columns have been loaded in Variables.Key loop, so just call update stored procedure. --->
</cfif>
</cfloop>
<cfabort>
--->
<cfif Variables.ImUserId gt 0>
	<cfif len(Variables.IMUserSuspRsn) GT 0 <!--- and form.SUSPENDUSERACT eq "Yes" --->>
		<cfset Variables.IMUserSuspRsn ="#Variables.IMUserSuspRsn# BYSUSP:#Variables.Origs.IMUserFirstNm# #Variables.Origs.IMUserLastNm#">
	<cfelse>
		<cfset Variables.IMUserSuspRsn = "">
	</cfif>
</cfif>
<cfif Variables.IMUserEmailAdrTxt NEQ Variables.IMUserEmailAdrTxt2>
	<cfset Variables.SaveMe						= "No">
	<cfset Variables.ErrMsg						= Variables.ErrMsg & "<li>Email Addresses do not match.</li>">
</cfif>
<!--- Check for length of SBA user name only if it is a new  user--->
<cfif NOT Variables.IMUserId>
	<cfif Len(Variables.IMSBACrdntlLognNm) LT 8>
		<cfset Variables.SaveMe					= "No">
		<cfset Variables.ErrMsg					&= "<li>SBA Username must be at least 8 characters long</li>">
	</cfif>
</cfif>
<!--- Cross Edits Go Here: --->
<!--- check valid job title for user type --->
<cfif Len(Variables.IMUserTypCd) gt 0>
	<cfif ListFind(ValueList(Variables.ActvIMUserTypTbl.code), Variables.IMUserTypCd) is 0>
		<cfset Variables.FieldErrMsg						= "Invalid User Type.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelseif Len(Variables.JobTitlCd) gt 0>
		<cfloop index="Job" list="#Variables.JobTitlCd#">
			<cfset Variables.JobAllowed						= "No">
			<cfset Variables.JobDescription					= "Job Classification code #Job#">
			<cfloop query="Variables.ActvIMJobTitlTypTbl">
				<cfif Variables.ActvIMJobTitlTypTbl.code is Job>
					<cfset Variables.JobDescription			= Variables.ActvIMJobTitlTypTbl.description>
					<cfif Variables.ActvIMJobTitlTypTbl.IMUserTypCd is Variables.IMUserTypCd>
						<cfset Variables.JobAllowed			= "Yes">
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>
			<cfif NOT Variables.JobAllowed>
				<cfset Variables.FieldErrMsg				= "#Variables.JobDescription# is not a valid Job Classification for User Type.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfloop>
	</cfif>
	<!--- check Verify field dsppageloadtimeorigs--->
	<cfif Variables.PageIsAddCustomer><!--- <cfdump var="#session#"><cfabort> --->

		<cfif len(Variables.Verify) gt 0>
			<cflock scope="Session" type="Readonly" timeout="30">
				<cfset Variables.VerifyToCompare = session.Origs.Verify>
			</cflock>
			<cfif Variables.Verify is not Variables.VerifyToCompare>
				<cfset Variables.FieldErrMsg	= "Verify test answer failed. Please retry.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfset Variables.Origs.Verify="">
				<cfdump var="FieldErrMsg=#Variables.FieldErrMsg#">
			</cfif>
		<cfelse>
			<cfset Variables.FieldErrMsg		= "Please answer CAPTCHA test question.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfif>
	<!--- Password check --->
	<cfif Variables.PageIsAddCustomer>
		<cfif Len(Variables.NewPass1) IS 0 and Len(Variables.NewPass2) IS 0>
			<cfset Variables.FieldErrMsg	= "Enter a new password.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
		<!--- If fields changed and passwords do not match--->
		<cfif Len(Variables.NewPass1) gt 0 or Len(Variables.NewPass2) gt 0>
			<cfif Compare(Variables.NewPass1,Variables.NewPass2)  IS NOT 0>
				<cfset Variables.FieldErrMsg		 = "The 2 passwords did not match. Please try again">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			<cfelseif not PasswordValidate("cls", Variables.NewPass1)>
				<!--- CHECK FOR INVALID CHARS  --->
				<cfset Variables.FieldErrMsg		 = Variables.PWUDF.ErrMsg>
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			<cfelse>
				<cfset  Variables.CallPasswordSetRoutine ="Yes">
			</cfif>
		</cfif>
	</cfif>
	<cfif Variables.IMUserTypCd eq "N">
		<cfif isDefined("Variables.JobTitlCd")>
			<!--- have 2 possible codes 7,8 if it is not defined then none was selected. Should be caught by dsp page--->
			<cfif listfind(Variables.JobTitlCd, 7)>
				<cfset Variables.Taxid  = "1" & Variables.TaxId1 &  Variables.TaxId2 &  Variables.TaxId3>
				<cfset Variables.TaxidRetypd  = "1" & Variables.TaxId4 &  Variables.TaxId5 &  Variables.TaxId6>
				<cfif Variables.TaxidRetypd is not Variables.Taxid>
					<cfset Variables.FieldErrMsg		 = "Re-entered SSN does not match with entered SSN">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<cfif Len(Variables.LoanNmb) gt 0>
					<cfset Variables.LoanNmbForVal			= Trim(Variables.LoanNmb)>
					<!--- Forgive spaces. --->
					<cfswitch expression="#Len(Variables.LoanNmbForVal)#">
						<cfcase value="8">
							<cfif IsNumeric(Variables.LoanNmbForVal)>
								<cfset Variables.LoanNmb		= gen_chkdgt(Variables.LoanNmbForVal)>
								<!--- Returns 10 digits. --->
							<cfelse>
								<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
									& "9999999999 or 99999999-99 format.">
								<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
							</cfif>
						</cfcase>
						<cfcase value="10">
							<cfif IsNumeric(Variables.LoanNmbForVal)>
								<cfset Variables.LoanNmb		= Variables.LoanNmbForVal>
							<cfelse>
								<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
									& "9999999999 or 99999999-99 format.">
								<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
							</cfif>
						</cfcase>
						<cfcase value="11">
							<cfif	IsNumeric	(Mid(Variables.LoanNmbForVal, 1, 8))
								and				(Mid(Variables.LoanNmbForVal, 9, 1) is "-")
								and	IsNumeric	(Mid(Variables.LoanNmbForVal,10, 2))>
								<cfset Variables.LoanNmb		= Replace(Variables.LoanNmbForVal, "-", "", "All")>
							<cfelse>
								<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
									& "9999999999 or 99999999-99 format.">
								<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
							</cfif>
						</cfcase>
						<cfdefaultcase>
							<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
								& "9999999999 or 99999999-99 format.">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfdefaultcase>
					</cfswitch>
				<cfelse>
					<cfset Variables.FieldErrMsg		 = "SBA Loan Number is mandatory">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<!--- end loannmb  length check --->
				<!--- Check if disaster loan first --->
				<cfquery  name="GetPrgmCd" datasource="#Variables.db#" username="#Variables.username#" password="#Variables.password#" dbtype="#Variables.dbtype#">
					SELECT PRGMCD
					FROM LOAN.LOANGNTYTBL
				    WHERE LOANNMB = <cfqueryparam value="#Variables.LOANNMB#" cfsqltype="CF_SQL_VARCHAR">
				</cfquery>
				<cfif GetPrgmCd.recordcount gt 0>
					<cfif trim(GetPrgmCd.PRGMCD) neq "H">
						<cfset Variables.FieldErrMsg		 = "Invalid Loan number. Must be a Disaster Loan">
						<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
					</cfif>
				<cfelse>
					<cfset Variables.FieldErrMsg		 = "Invalid Loan number.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<cfscript>
					Variables.Identifier					= "0";
					Variables.LogAct          				= "Get Info";
					Variables.cfpra							= "";
					Variables.LoanDisbColumns				= 1;
					Variables.cfpra							= ArrayNew(2);

					i								= 0;
					i = i + 1; Variables.cfpra[i][1]= "getLoanGnty";	 	Variables.cfpra[i][2] = i;
					i = i + 1; Variables.cfpra[i][1]= "getLoanBorr"; 		Variables.cfpra[i][2] = i;
					i = i + 1; Variables.cfpra[i][1]= "getLoanLst"; 		Variables.cfpra[i][2] = i;
					Variables.LogAct				= "retrieve disaster borr info dsp";
					include "/cfincludes/oracle/loan/spc_LOANNONPARTINFOSELCSP.cfm";
					if (Variables.TxnErr){
						Variables.SaveMe 		= "No";
						writeDump(Variables.ErrMsg);abort;
					}
				</cfscript>
				<cfif isDefined("Retval") and  (Retval eq 0)>
					<cfset Variables.FieldErrMsg		 = "Please enter valid Loan Number and SSN.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
			</cfif>
			<cfif listfind(Variables.JobTitlCd, 8)>
				<!--- SBG Customer. Change later to Variables.!!--->
				<cfset Variables.Taxid  = "0" & Variables.EIN1 &  Variables.EIN2>
				<cfset Variables.ReTaxid  = "0" & Variables.EIN3 &  Variables.EIN4>
				<cfif Variables.ReTaxid is not Variables.Taxid>
					<cfset Variables.FieldErrMsg		 = "Re-entered EIN does not match with entered EIN">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<!--- Check DUNS - handled already--->
				<cfif Variables.DevTestProdInd EQ 2>
					<cfif Variables.SaveMe>
						<!--- Need to check for an active Pronet profile for EIN and DUNS number --->
						<!--- cannot rely on web serivce as DSBS might time out. Use cfhttp instead --->
						<cfset Variables.WSDLURL = "https://eweb1.sba.gov/pro-net/ws/wbs_sbainternalservices.cfc?method=GetMiniProfileByDUNSOrTaxId">
						<cfhttp url="#Variables.WSDLURL#" method="POST" timeout="90">
							<cfhttpparam type="Formfield" value="#Variables.Taxid#" name="TaxId">
							<!--- value="012345678" --->
						</cfhttp>
						<cfset Variables.ReturnResponce = htmlcodeformat(cfhttp.filecontent)>
						<cfif findnocase("wddxPacket",Variables.ReturnResponce) GT 0>
							<!--- good call--->
							<cfset Variables.contentXML =xmlparse(cfhttp.filecontent)>
							<cfwddx action="wddx2cfml" input=#contentXML# output="contentXML_data"/>
							<!--- 	<cfdump var=#Variables.contentXML#><cfabort> --->
							<cfif isDefined("contentXML_data.DUNS")>
								<cfset variables.dsbsDUNS=Variables.contentXML_data.DUNS>
								<cfif Variables.DUNS EQ variables.dsbsDUNS and Variables.contentXML_data.STATUS EQ "A">
									<!--- Match with active profile --->
									<cfset variables.ValidEINDUNS = "Yes">
								<cfelse>
									<cfset Variables.DUNSErrMsg	= "<li>EIN & DUNS not found in DSBS</li>">
									<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
									<cfset Variables.SaveMe					= "No">
								</cfif>
							<cfelse>
								<cfset Variables.DUNSErrMsg	= "<li>Tax ID not found in DSBS</li>">
								<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
								<cfset Variables.SaveMe					= "No">
							</cfif>
						<cfelse>
							<cfset Variables.FieldErrMsg		 = "Tax ID not found in DSBS">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif>
						<!--- end wddxPacket --->
					</cfif>
				</cfif><!--- end Variables.DevTestProdInd EQ 2 --->
				<!--- end saveme --->
			</cfif>
			<!--- end job=8 --->
		<cfelse>
			<!--- Job title not defined (means neither was selected so display error. --->
			<cfset Variables.JobTitleErrMsg	= "<li>Borrowers must select Bond Borrower or Loan Borrower </li>">
			<cfset Variables.ErrMsg					&= Variables.JobTitleErrMsg>
			<cfset Variables.SaveMe					= "No">
		</cfif>
	</cfif>
	<!--- end Variables.IMUserTypCd eq "N" --->
	<!--- email check --->
	<cfif Variables.IMUserEmailAdrTxt NEQ Variables.IMUserEmailAdrTxt2>
		<cfset Variables.FieldErrMsg		 = "E-mail addresses did not match.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	</cfif>
	<!--- set ImUserId --->
	<cfset Variables.ImUserId 							= Variables.Origs.ImUserId>
	<cfif Variables.ImUserId eq 0>
		<cfset Variables.ImUserNm 						= Variables.IMSBACrdntlLognNm>
		<cfset Variables.NewOldUserInd					="N">
	<cfelse>
		<cfset Variables.ImUserNm 						=  Variables.Origs.ImUserNm>
		<cfset Variables.NewOldUserInd					="E">
	</cfif>
	<cfif len(Variables.IMUserEmailAdrTxt) GT 0 and listfindnocase("S,C",Variables.IMUserTypCd) gt 0>
		<cfinclude template="#Variables.CLSIncludesURL#/val_GovEmail.cfm">
	</cfif>
	<!--- validate supervisor --->
	<cfswitch expression="#Variables.IMUserTypCd#">
		<cfcase value="C,S">
			<cfif	(Len(Variables.IMUserSuprvFirstNm)			gt 0)
				and	(Len(Variables.IMUserSuprvLastNm)			gt 0)
				and	(Len(Variables.IMUserSuprvEmailAdrTxt)		gt 0)>
				<!--- But not IMUserSuprvId, IMUserSuprvMidNm or IMUserSuprvUserId (optional). --->
				<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
			<cfelse>
				<cfset Variables.FieldErrMsg					= "Please lookup and choose a supervisor.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfcase>
		<cfcase value="U">
			<cfif	(Len(Variables.IMUserSuprvFirstNm)			gt 0)
				and	(Len(Variables.IMUserSuprvLastNm)			gt 0)
				and	(Len(Variables.IMUserSuprvEmailAdrTxt)		gt 0)>
				<!--- But not IMUserSuprvId, IMUserSuprvMidNm or IMUserSuprvUserId (optional). --->
				<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
			<cfelse>
				<cfif Variables.pageIsAddCustomer>
					<!--- LAOs  may not have a supervisor--->
					<cfif not ( isDefined("Variables.JobTitlCd") and listfind(Variables.JobTitlCd, 3) gt 0)>
						<cfset Variables.FieldErrMsg		= "Please lookup and choose a supervisor.">
						<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
					</cfif>
				<cfelse>
					<cfset Variables.FieldErrMsg			= "Please lookup and choose a supervisor.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
			</cfif>
		</cfcase>
	</cfswitch>
	<!--- <cfdump var="#Variables.GetErrs#"><cfabort> --->
	<!--- <cfdump var="=#variables.origs.JOBTITLCD#"><cfdump var="#HQLOCID#"><cfdump var="#locid#"> --->
</cfif>
<!--- end usertypcd gt 0 --->
<!--- Add more here. Examples: --->
<!--- 1. If Password or Re-enter Password given, they have to pass PasswordValidate (in bld_PasswordUDFs) and match each other. --->
<!--- 2. If not done above, and if CallValidateIMUserSuprvCSP is yes, call ValidateIMUserSuprvCSP. --->
<!--- 3. Need to validate other fields that have reference tables against ServerCachedQueries. --->
<!--- Validate new account user name and email address uniquiness--->
<cfset Variables.LogAct									= "Validate Email/User Name Unique">
<cfset Variables.cfprname								= "GetErrs">
<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATEIMUSERACCTCSP.cfm">
<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
	<!--- validation errors --->
	<cfset Variables.SaveMe					= "No">
</cfif>
<cfif Variables.ImUserId neq 0>
	<cfif listfindnocase( "S,C,U", trim(Variables.IMUserTypCd) )  gt 0>
		<cfif Variables.CallValidateIMUserSuprvCSP>
			<cfset Variables.cfprname					= "GetErrs">
			<cfset Variables.LogAct						= "Validate Supervisor">
			<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATESUPRVCSP.cfm">
			<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0> <!--- validation errors --->
				<cfset Variables.SaveMe					= "No">
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfif isDefined("GetErrs.RecordCount") and GetErrs.RecordCount gt 0>
	<cfset Variables.SaveMe				= "No">
	<CFIF Variables.ErrMsg EQ "Error(s) occurred.">
		<cfset Variables.ErrMsg			= "#GetErrs.RecordCount# Validation error(s) occurred.">
		<CFELSE>
		<cfset Variables.FieldErrMsg	= "#GetErrs.RecordCount# Validation errors occurred.">
		<CFINCLUDE TEMPLATE = "/library/cfincludes/val_FormatErrMsg.cfm">
	</CFIF>
	<CFLOOP QUERY="GetErrs">
		<cfset Variables.FieldErrMsg = GetErrs.RecordCount & " - " & GetErrs.ErrCd & ": " & GetErrs.ErrTxt>
		<CFINCLUDE TEMPLATE = "/library/cfincludes/val_FormatErrMsg.cfm">
	</CFLOOP>
</cfif>

<cfif NOT Variables.SaveMe>
	<!--- ((in cross edits)) --->
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cfif (Variables.PageIsUser eq "Yes") and (Variables.NewOldUserInd eq "N")>
		<cfset Variables.IMUserFirstNm			= "">
		<cfset Variables.IMUserLastNm			= "">
		<cfset Variables.IMUserTypCd			= "">
		<cfset Variables.IMUserLastNm			= "">
	</cfif>
	<cfif (Variables.NewOldUserInd eq "N")>
		<cfset Variables.PageTitle 				="Add">
	</cfif>
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y&ErrMsg=#Variables.ErrMsg#&IMUserId=#Variables.ImUserId#&PageTitle=#Variables.PageTitle#&IMUserTypCd=#Variables.IMUserTypCd#&IMUserFirstNm=#URLEncodedFormat(Variables.IMUserFirstNm)#&IMUserLastNm=#URLEncodedFormat(Variables.IMUserLastNm)#">
	<!--- <cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y"> --->
</cfif>
<cfif Variables.UpdateTrace <!--- or "Yes" --->>
	<!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed cross edits.<br/>">
</cfif>
<!--- Simple mechanism for detecting changed data, based on current data structures: --->
<!--- Etc. --->
<!--- !!!! remove later, duplicate --->
<cfif Variables.ImUserId gt 0>
	<cfif SomethingChanged(Variables.ColumnListIMUserTbl)>
		<!--- Save these IMUserTbl columns here. --->
		<cfloop index="Local.ColName" list="#Variables.ColumnListIMUserTbl#">
			<cfif Variables[Local.ColName] is not Variables.Origs[Local.ColName]>
				<cfset nam = Local.ColName & "Changed">
				<cfset "Variables.#nam#" = true>
				<!--- <cfreturn true> --->
			</cfif>
		</cfloop>
	</cfif>
</cfif>
<!--- <cfdump var="#SomethingChanged(Variables.ColumnListIMUserTbl)#"><cfdump var="# Variables.SaveMe#"><cfabort> --->
<!--- <cfabort> --->
<cfif Variables.SaveMe>
	<!--- "No" essentially comments out this example usage of SomethingChanged. --->
	<cftransaction action="Begin">
		<cfset Variables.encryptionKey="BeMy01BstCstmrKyToAppl==">
		<!--- Comparing job titles --->
		<!--- <cfif not compareNoCase( listSort(Variables.JobTitlCd, 'numeric'), listSort(Variables.Origs.JobTitlCd, 'numeric') ) EQ 0>
			<cfset Variables.JobTitleChanged 			= true>
			</cfif> --->
		<cfif listfindnocase( "S,U", trim(Variables.IMUserTypCd) )  gt 0>
			<cfset Variables.JobTitlCdStr	 	= Variables.JobTitlCd>
		<cfelse>
			<cfset Variables.JobTitlCdStr	 	= "">
		</cfif>

		<!--- New User/Customer --->
		<cfif Variables.ImUserId eq 0>
			<!--- only new customers(not security users) must be verified --->
			<cfif Variables.PageIsAddCustomer>
				<cfset Variables.SendEmailVerify 		= "Yes">
			</cfif>
			<cfset Variables.ImUserNm 					= Variables.IMSBACrdntlLognNm>
			<cfset Variables.IMSBACrdntlCreatUserId 	= Variables.IMSBACrdntlLognNm>
			<cfset Variables.IMASURNCLVLCD 				= 1>
			<!--- generate random password for insert --->
			<cfset PasswordAvoidConfusingCharacters()>
			<cfset Variables.ClearTextPassword			= PasswordGenerate("clsweak")>
			<cfset Variables.IMSBACrdntlPasswrd			= Hash(Variables.ClearTextPassword, "SHA1")>
			<!--- <cfset Variables.username 				= "">
			<cfset Variables.password 				= ""> --->
			<cfif  Variables.PageIsAddCustomer>
				<cfset Variables.IMUserActvInactInd	= "I">
				<cfset Variables.IMUserSuspRsnCd	= "1">
				<cfset Variables.IMUserSuspRsnDt	= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
			<cfelse>
				<cfset Variables.IMUserActvInactInd	= "A">
			</cfif>
			<cfset Variables.cfprname				= "InsCust">
			<cfset Variables.NewOldUserInd 			= "N">
			<cfset Variables.IMUserLastUpdtUserId	= "clslogin">
			<cfset Variables.LogAct					= "save new user ">
			<cfinclude template="/cfincludes/oracle/security/spc_JB_IMUSERINSCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe            = "No">
			</cfif>
			<cfset Variables.OrigUserName 			= Variables.IMSBACrdntlLognNm>
			<cfset Variables.OrigPassword 			= Variables.ClearTextPassword>
			<!--- <cfdump var="p=#Variables.Password#"><cfdump var="u=#Variables.UserName#">
				<cfinclude template="/cls/act_setpass.cfm"> --->
		<cfelse>
			<!--- Existing Security User/Profile --->
			<cfif Variables.PageIsProfile>
				<cfset Variables.IMUserActvInactInd = Variables.Origs.IMUserActvInactInd>
				<cfset Variables.IMUserSuspRsnCd 	= Variables.Origs.IMUserSuspRsnCd>
			</cfif>
			<cfif isDefined("Variables.IMUserSuprvEmailAdrTxtChanged") and Variables.IMUserSuprvEmailAdrTxtChanged>
				<cfset Variables.SendEmailSupervisorFlag 	= true>
				<cfset Variables.IMUSERSUSPRSNCD		= 2>
			</cfif>
			<cfif isDefined("Variables.HQLocIdChanged") and Variables.HQLocIdChanged>
				<cfset Variables.SendHQLocEmailFlag 	= true>
				<cfset Variables.SendEmailVerify 		= "Yes">
				<cfset Variables.IMUSERSUSPRSNCD		= 3>
			</cfif>
			<cfif isDefined("Variables.IMUserEmailAdrTxtChanged") and Variables.IMUserEmailAdrTxtChanged>
				<cfset Variables.SendEmailAddrEmailFlag = true>
				<cfset Variables.SendEmailVerify 		= "Yes">
				<cfset Variables.IMUSERSUSPRSNCD		= 3>
			</cfif>
			<cfif isDefined("Variables.IMUserTypCdChanged") and Variables.IMUserTypCdChanged>
				<cfset Variables.SendEmailUserTypeFlag 	 = true>
				<cfset Variables.SendEmailVerify 		= "Yes">
				<cfset Variables.IMUSERSUSPRSNCD		= 3>
			</cfif>
			<!--- update User record --->
			<cfset Variables.NewOldUserInd 						= 'E'>
			<cfset Variables.Identifier							= "0">
			<cfset Variables.IMUserLastUpdtUserId	 			= Variables.ImUserNm>
			<cfset Variables.cfprname							= "UpdExistProfile">
			<cfset Variables.LogAct								= "Update Existing Profile">
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERUPDTSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SaveMe								= "No">
			</cfif>
			<cfif Variables.SendEmailSupervisorFlag
				or Variables.SendEmailUserTypeFlag
				or Variables.SendHQLocEmailFlag
				or (isDefined("Variables.IMUserSuspRsnCdChanged") and Variables.IMUserSuspRsnCdChanged)
			>
				<!--- get values to pass to the SP  --->
				<cfset Variables.Identifier								= "0">
				<cfset Variables.cfprname								= "getUserData">
				<cfset Variables.LogAct									= "Get User data">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERSELTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe								= "No">
					<cfdump var="Serious database error occured. Following information might help. #Variables.ErrMsg#">
					<cfabort>
				</cfif>
				<!--- <cfdump var="#getUserData#"> --->
				<!--- get IMUSERSUSPRSNCD to pass to SP --->
				<cfif getUserData.recordcount>
					<cfset Variables.IMUSERSUSPRSNCD		= trim(getUserData.IMUSERSUSPRSNCD)>
				</cfif>
				<!--- <cfdump var="#Variables.IMUSERSUSPRSNCD#"><cfabort> --->
				<cfif isDefined("Variables.IMUserSuspRsnCdChanged") and Variables.IMUserSuspRsnCdChanged>
					<cfif Variables.IMUSERSUSPRSNCDOverr neq Variables.Origs.IMUSERSUSPRSNCD>
						<cfset Variables.IMUSERSUSPRSNCD			 	= Variables.IMUSERSUSPRSNCDOverr>
						<cfif listfindnocase("1,3",Variables.IMUSERSUSPRSNCDOverr) gt 0 >
							<cfset Variables.SendEmailUserTypeFlag 		= true>
							<!--- conditional  to send email --->
						<cfelseif listfindnocase("2",Variables.IMUSERSUSPRSNCDOverr)>
							<cfset Variables.SendEmailSupervisorFlag 	= true>
							<!--- conditional to send email --->
						</cfif>
					</cfif>
				</cfif>
				<cfset Variables.Identifier							= "17">
				<cfset Variables.NewOldUserInd 						= 'E'>
				<cfset Variables.IMUserLastUpdtUserId	 			= Variables.ImUserNm>
				<cfset Variables.cfprname							= "UpdExistProfile">
				<cfset Variables.LogAct								= "Update Existing Profile">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe								= "No">
				</cfif>
			</cfif>
		</cfif><!--- end Variables.IMUserId check --->
		<!--- update job title --->
		<cfset Variables.Identifier				= 0>
		<cfset Variables.CreatUserId 			= Variables.ImUserNm>
		<cfset Variables.lastupdtuserid			= Variables.ImUserNm>
		<cfset Variables.LogAct					= "save user title">
		<cfinclude template="/cfincludes/oracle/security/spc_IMUSERTYPTITLINSUPDCSP.cfm">
		<cfif Variables.TxnErr>
			<cfset Variables.SaveMe             = "No">
		</cfif>

		<cfif Variables.CallPasswordSetRoutine>
			<!--- update with new password --->
			<cfset Variables.Identifier								= "11">
			<cfset Variables.RetVal									= "0">
			<!--- Initialize output parameter. --->
			<cfset Variables.IMSBACrdntlFirstLognInd				= "N">
			<cfset Variables.IMSBACrdntlPasswrd						= Hash(trim(Variables.NewPass1), "SHA1")>
			<cfset Variables.IMSBACrdntlPasswrdExprDt				= DateAdd("d", 90, Now())>
			<cfset Variables.IMSBACRDNTLLASTUPDTUSERID				=  Variables.ImUserNm>
			<!--- Variables.IMSBACrdntlLognNm --->
			<cfset Variables.cfprname								= "Ignored">
			<cfset Variables.LogAct									= "set the user's password">
			<cfinclude template="/cfincludes/oracle/security/spc_IMSBACRDNTLUPDTSP.cfm">
			<cfif Variables.TxnErr>
				<!--- Leave Variables.ErrMsg containing whatever got passed back from the SPC file. --->
			<cfelseif Variables.RetVal is 0>
				<!--- Failure --->
				<cfset Variables.ErrMsg								&= "User's profile not found.">
				<cfset Variables.SaveMe            = "No">
			</cfif>
		</cfif>
		<!--- <cfdump var="#form#"><cfabort> --->
		<!---<cfdump var="#Variables.Origs#"> <cfdump var="#Variables.ArrayNewBus#"><cfdump var="#form.TotalNewBus#"> --->
		<!--- set ErrSeqNmb for validate SPc --->

		<cfset Variables.ValidIndxLst 	= "">
		<cfif isDefined("form.TotalNewBus") and form.TotalNewBus gt 0>
			<!--- get list of not discurded(valid) array elements --->
			<cfloop from="1" to="#form.TotalNewBus#" index="NewBusIdx">
				<cfif StructKeyExists(Form, "NewBusWasNotDiscarded_#NewBusIdx#")>
					<cfset Variables.ValidIndxLst = ListAppend(Variables.ValidIndxLst, NewBusIdx)>
					<!--- If not defined, New Business was discarded, so don't require other fields to exist: --->
					<cfcontinue>
				</cfif>
			</cfloop>

			<!--- if there are valid entries --->
			<cfif len(Variables.ValidIndxLst) gt 0>
				<cfset Variables.IMUserBusStrtDt		= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
				<cfset Variables.IMUserBusLocStrtDt		= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
				<cfset Variables.IMUserBusCreatUserId  	= Variables.ImUserNm>
				<!--- Loop over number of valid entries = all new businesses - discurded new businesses --->
					<cfloop index="ValidIndx" list="#Variables.ValidIndxLst#">
						<cfset Variables.Identifier = 0>
						<cfif StructKeyExists(Form, "NEWBUSTAXID_#ValidIndx#")>
							<cfset Variables.IMUserBusTaxId = Form["NEWBUSTAXID_" & ValidIndx]>
						</cfif>
						<cfif StructKeyExists(Form, "NEWBUSDUNS_#ValidIndx#")>
							<cfset Variables.IMUserBusLocDUNSNmb = Form["NEWBUSDUNS_" & ValidIndx]>
						</cfif>
						<cfif Variables.DevTestProdInd EQ 2>
							<cfif Variables.SaveMe>
								<cfif len(Variables.IMUserBusTaxId) gt 0 and len(Variables.IMUserBusLocDUNSNmb) gt 0>
									<cfset Variables.IMUserBusTaxIdToPronet  = "0" & Variables.IMUserBusTaxId>
									<!--- Check DUNS - handled already--->
									<!--- Need to check for an active Pronet profile for EIN and DUNS number --->
									<!--- cannot rely on web serivce as DSBS might time out. Use cfhttp instead --->
									<cfset Variables.WSDLURL = "https://eweb1.sba.gov/pro-net/ws/wbs_sbainternalservices.cfc?method=GetMiniProfileByDUNSOrTaxId">
									<cfhttp url="#Variables.WSDLURL#" method="POST" timeout="90">
										<cfhttpparam type="Formfield" value="#Variables.IMUserBusTaxIdToPronet#" name="TaxId">
									</cfhttp>
									<cfset Variables.ReturnResponce = htmlcodeformat(cfhttp.filecontent)>
									<cfif findnocase("wddxPacket",Variables.ReturnResponce) GT 0>
										<!--- good call--->
										<cfset Variables.contentXML =xmlparse(cfhttp.filecontent)>
										<cfwddx action="wddx2cfml" input=#contentXML# output="contentXML_data"/>
										<!--- 	<cfdump var=#Variables.contentXML#><cfabort> --->
										<cfif isDefined("contentXML_data.DUNS")>
											<cfset variables.dsbsDUNS=Variables.contentXML_data.DUNS>
											<cfif Variables.IMUserBusLocDUNSNmb EQ variables.dsbsDUNS and Variables.contentXML_data.STATUS EQ "A">
												<!--- Match with active profile --->
												<cfset variables.ValidEINDUNS = "Yes">
											<cfelse>
												<cfset Variables.DUNSErrMsg	= "<li>EIN & DUNS not found in DSBS</li>">
												<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
												<cfset Variables.SaveMe					= "No">
											</cfif>
										<cfelse>
											<cfset Variables.DUNSErrMsg	= "<li>Tax ID not found in DSBS</li>">
											<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
											<cfset Variables.SaveMe					= "No">
										</cfif>
									<cfelse>
										<cfset Variables.FieldErrMsg		 = "Tax ID not found in DSBS">
										<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
										<cfset Variables.SaveMe            = "No">
									</cfif>
								</cfif>
							</cfif>
						</cfif><!--- end Variables.DevTestProdInd EQ 2 --->
					<cfif StructKeyExists(Form, "NewBusCertified_#ValidIndx#")>
						<cfset Variables.BusEINCertInd = Form["NewBusCertified_" & ValidIndx]>
						<cfif len(Variables.BusEINCertInd) gt 0>
							<cfset Variables.BusEINCertInd = left(Variables.BusEINCertInd,1)>
						<cfelse>
							<cfset Variables.BusEINCertInd = "">
						</cfif>
					</cfif>
					<cfif StructKeyExists(Form, "NEWBUSEINSSN_#ValidIndx#")>
						<cfset Variables.IMUserBusEINSSNInd = Form["NEWBUSEINSSN_" & ValidIndx]>
						<cfif len(Variables.IMUserBusEINSSNInd) gt 0>
							<cfset Variables.IMUserBusEINSSNInd = left(Variables.IMUserBusEINSSNInd,1)>
						<cfelse>
							<cfset Variables.IMUserBusEINSSNInd = "">
						</cfif>
					</cfif>
					<!--- Validate values to be inserted (called before insert)  --->
					<cfset Variables.cfprname					= "GetErrs">
					<cfset Variables.LogAct						= "Validate business inserted">
					<cfinclude template="/cfincludes/oracle/security/spc_NEW_BUSINSVALIDATECSP.cfm">
					<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
						<!--- validation errors --->
						<cfset Variables.SaveMe					= "No">
					</cfif>
					<cfset Variables.IMUserBusTaxId = Replace(Variables.IMUserBusTaxId, "-", "", "all")>
					<cfset Variables.cfprname								= "IMUSERBUSINSCSP">
					<cfset Variables.LogAct									= "Insert Business">
					<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSINSCSP.cfm">
					<cfif Variables.TxnErr>
						<cfset Variables.SaveMe            = "No">
					</cfif>
					<!--- validate business inserted --->
				</cfloop>
				<cfif isDefined("GetErrs")>
					<cfdump var="Insert">
					<cfdump var="#GetErrs#">
				</cfif>
			</cfif><!--- end of len(Variables.ValidIndxLst) gt 0> --->
		</cfif><!--- end of isDefined("form.TotalNewBus") and form.TotalNewBus gt 0 --->
		<!--- <cfdump var="ListBusDel=#Variables.ListBusDel#"><cfdump var="#Variables.Origs#"><cfdump var="#form#"><cfdump var="#session#"><cfabort>--->
	<!--- delete(update end date) businesses --->
	<cfif Arraylen(Variables.ArrayBusDel)>
		<!--- <cfdump var="#Variables.ArrayBusDel#"><cfabort> --->
		<cfset Variables.IMUserBusLocCreatUserId 	= Variables.ImUserNm>
		<cfset Variables.IMUserBusCreatUserId  		= Variables.ImUserNm>
		<cfset Variables.IMUserBusEndDt				= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>
		<cfset Variables.IMUserBusLocEndDt			= DateTimeFormat(now(), " mm/dd/yyyy HH:nn:ss tt")>

		<cfloop from="1" to="#arrayLen(Variables.ArrayBusDel)#" index="delarridx">
		  	  <cfset data = Variables.ArrayBusDel[delarridx]>
			  <cfloop collection="#data#" item="collkey">
			    <cfset "Variables.#collkey#" = data[collkey]>
			  </cfloop>

		  	<cfif Len(Variables.IMUserBusLocDUNSNmb) gt 0> <!--- validate business deleted (note: called before delete)--->
				<cfset Variables.cfprname					= "GetErrs">
				<cfset Variables.LogAct						= "Validate business deleted">
				<cfinclude template="/cfincludes/oracle/security/spc_NEW_BUSDELVALIDATECSP.cfm">
				<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0> <!--- validation errors --->
					<cfset Variables.SaveMe					= "No">
				</cfif>
				<cfset Variables.Identifier 				= "11">
				<cfset Variables.cfprname					= "IMUSERBUSLOCUPDTSP">
				<cfset Variables.LogAct						= "Delete(Update end date) Business Location Duns">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSLOCUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe            		= "No">
				</cfif> <!---  ((end-date IMUserBusLocTbl for Variables.IMUserId, Variables.IMUserBusSeqNmb, Variables.IMUserBusLocDUNSNmb)) --->
			<cfelse>
				<cfset Variables.Identifier 				= "11">
				<cfset Variables.cfprname					= "IMUSERBUSUPDTSP">
				<cfset Variables.LogAct						= "Delete(Update end date) Business">
				<cfinclude template="/cfincludes/oracle/security/spc_IMUSERBUSUPDTSP.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe            		= "No">
				</cfif> <!---  ((end-date IMUserBusTbl    for Variables.IMUserId, Variables.IMUserBusSeqNmb)) --->
			</cfif>
		</cfloop>

		<cfif isDefined("GetErrs")>
			<cfdump var="Delete">
			<cfdump var="#GetErrs#">
		</cfif>
	</cfif><!--- end Arraylen(Variables.ArrayBusDel) --->
	<!--- Server side BR validations - Variables.NewOldUserInd defined above --->

	<!--- Validate Location --->
	<cfif listfindnocase("U",Variables.IMUserTypCd) gt 0>
		<cfset Variables.JOBTITLCDSTR 			= Variables.JOBTITLCD>
		<cfset Variables.cfprname				= "GetErrs">
		<cfset Variables.LogAct					= "Validate HQLocId">
		<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATELOCCSP.cfm">
		<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0>
			<cfset Variables.SaveMe				= "No">
		</cfif>
	</cfif>
	<!--- Validate phone and address --->
	<cfset Variables.LogAct									= "ValidatePhone/Address">
	<cfset Variables.cfprname								= "GetErrs">
	<cfinclude template="/cfincludes/oracle/security/spc_NEW_VALIDATEPHNADDRCSP.cfm">
	<cfif Variables.TxnErr OR IsDefined("GetErrs.RecordCount") AND GetErrs.RecordCount GT 0> <!--- validation errors --->
		<cfset Variables.SaveMe					= "No">
	</cfif>

	<cfif isDefined("GetErrs")>
		<cfdump var="#GetErrs#">
	</cfif><!--- <cfabort> --->

	<cfif isDefined("GetErrs.RecordCount") and GetErrs.RecordCount gt 0>
		<cfset Variables.SaveMe				= "No">
		<CFIF Variables.ErrMsg EQ "Error(s) occurred.">
			<cfset Variables.ErrMsg			= "#GetErrs.RecordCount# Validation error(s) occurred.">
			<CFELSE>
			<cfset Variables.FieldErrMsg	= "#GetErrs.RecordCount# Validation errors occurred.">
			<CFINCLUDE TEMPLATE = "/library/cfincludes/val_FormatErrMsg.cfm">
		</CFIF>
		<CFLOOP QUERY="GetErrs">
			<cfset Variables.FieldErrMsg = GetErrs.RecordCount & " - " & GetErrs.ErrCd & ": " & GetErrs.ErrTxt>
			<CFINCLUDE TEMPLATE = "/library/cfincludes/val_FormatErrMsg.cfm">
		</CFLOOP>
	</cfif>
	<!--- <cfdump var="#ErrMsg#"><cfdump var="#Variables.SaveMe#"><cfabort> --->
	<cfif not Variables.SaveMe>
		<cftransaction action="Rollback" />
		<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
		<cfif <!--- (Variables.PageIsUser eq "Yes") and ---> (Variables.NewOldUserInd eq "N")>
			<cfset Variables.IMUserFirstNm			= "">
			<cfset Variables.IMUserLastNm			= "">
			<cfset Variables.ImUserId				= 0>
		</cfif>
		<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y&ErrMsg=#Variables.ErrMsg#&IMUserId=#Variables.ImUserId#&IMUserFirstNm=#URLEncodedFormat(Variables.IMUserFirstNm)#&IMUserLastNm=#URLEncodedFormat(Variables.IMUserLastNm)#">
	<cfelse>
		<cftransaction action="Commit" />
			<!--- send emails --->
			<cfif Variables.DevTestProdInd NEQ 2>
				<cfset Variables.IMUserEmailAdrTxt = Variables.IMUserEmailAdrTxt & Variables.DevTeamEml>
			</cfif>
			<!--- send login info for the user created in Security section --->
			<cfif (Variables.PageIsUser eq "Yes") and (Variables.NewOldUserInd eq "N")>
			<!--- <cfdump var='#(Variables.PageIsUser eq "Y") and (Variables.NewOldUserInd eq "N")#'>
			<cfdump var="#Variables.IMSBACrdntlPasswrd#"><cfabort> --->

				<cfif Len(Variables.ClearTextPassword) GT 0>
					<!--- send login info for the user created in Security section --->
					<!--- User Record successfuly added. --->
						<!--- <cfmail to="#Variables.IMUserEmailAdrTxt#" from="#Variables.ITSecEMailAddr#"
							subject="Original -> #Variables.SRV# - New account request complete" type="HTML">
						<br>
						You have been granted access to the SBA's Capital Access Login System (CLS) at
						&nbsp;&nbsp; <a href="#Request.SlafLoginURL#">#Request.SlafLoginURL#</a>&nbsp;&nbsp;
						with the following username:&nbsp;&nbsp; <strong>#addGLSUser['UserNm']#</strong>&nbsp;&nbsp; and password:&nbsp;&nbsp; <strong>#addGLSUser['Password']#</strong><br><br>
						Once you log in, you will be required to change your password.
						Your password must be complex and in compliance with SBA's password policy.
						A complex password must be a minimum of 8 characters long and contain at least
						three of the following four properties:<br><br>

						o	Upper Case Letters	A, B, C, ... Z<br>
						o	Lower Case Letters	a, b, c, ... z<br>
						o	Numerals 0, 1, 2, ... 9<br>
						o	Special Characters  { } [ ] < > : ? | \ ` ~ ! @ $ % ^ & * _ - + = . <br><br>
						<!--- Passwords expire every 90 days, please ensure that you change your password at least once
						every 90 days to avoid having your account disabled.For additional help please contact your
						Program Office representative at the U.S. Small Business Administration. --->
						Please change your password at least once every 90 days to avoid having it expire. If your password does expire, you can request a reset on the SBA CLS Login page
						<a href="#Variables.ServerPathForURL#/cls">#Variables.ServerPathForURL#/cls</a>
						. For additional help please contact your Program Office representative at the U.S. Small Business Administration.<br><br>
						</cfmail> --->
						<cfset Variables.username 				= "">
						<cfset Variables.password 				= "">
						<cfset Variables.cfprname		= "getEmail">
						<cfset Variables.LogAct			= "get email content">
						<cfset Variables.emailcdseqnmb	= "2">
						<cfset Variables.prmtr2			= Variables.ImUserNm>
						<cfset Variables.prmtr3			= Variables.ClearTextPassword>
						<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
						<cfif Variables.TxnErr>
							<cfset Variables.SaveMe		= "No">
							<cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
							<cfabort>
						</cfif>
						<cfif Variables.DevTestProdInd NEQ 2>
							<cfset Variables.EMAILRCPNT = Variables.EMAILRCPNT & Variables.DevTeamEml>
						</cfif>
						<cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
							type="HTML" subject="#Variables.EMAILSUBJ#">
							#Variables.emailtxt#
						</cfmail>

						<cfset Variables.Commentary				&= "User account has been successfully created.<br>"
												& "An email with the login information has been sent to the user.">
					</cfif>
				</cfif>

				<cfif Variables.SendEmailVerify>
					<cfset Variables.UPURLString  = Encrypt('imuserid=' & trim(Variables.imuserid), Variables.encryptionKey,"AES/CBC/PKCS5Padding", "hex")>
					<!--- <cfmail to="#Variables.IMUserEmailAdrTxt#" from="#Variables.ITSecEMailAddr#" subject="#Variables.SRV# - New account request verification" type="HTML">
						<strong>Dear #Variables.IMUserFirstNm# #Variables.IMUserLastNm#</strong>,<br><br>
						Your account had been created in the SBA's Capital Access Login System (CLS) at
						<a href="#request.slafloginserverurl#/cls">#request.slafloginserverurl#/cls</a>
						</strong>. &nbsp;&nbsp;
						<br><br>
						Please click on the link below to verify your email address:<br><br>
						<a href="#request.slafloginserverurl#/cls/dsp_clsuserconfrminfo.cfm?#UrlEncodedFormat(Variables.UPURLString)#&1=1">Verify Your Email</a> Verification must be completed within two hours of submitting or the account will be removed.<br><br>
						For additional help please contact your Program Office representative at the U.S. Small Business Administration.
						<br>
						<br><br>
					</cfmail> --->
				 	   <cfset Variables.cfprname				= "getEmail">
					   <cfset Variables.LogAct					= "get email content">
					   <cfif Variables.PageIsAddCustomer>
                       		<cfset Variables.emailcdseqnmb			= "11">
						<cfelse>
							<cfset Variables.emailcdseqnmb			= "8">
						</cfif>
                       <cfset Variables.prmtr3					= "#request.slafloginserverurl#/cls/dsp_clsuserconfrminfo.cfm?#UrlEncodedFormat(Variables.UPURLString)#&1=1">
                       <cfset Variables.USERURLDSPLY			= "#request.slafloginserverurl#/cls">
						<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
                       <cfif Variables.TxnErr>
                           <cfset Variables.SaveMe				= "No">
                           <cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
                           <cfabort>
                       </cfif>
						<cfif Variables.DevTestProdInd NEQ 2>
							<cfset Variables.EMAILRCPNT = Variables.EMAILRCPNT & Variables.DevTeamEml>
						</cfif>
                       <cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
                           type="HTML" subject="#Variables.EMAILSUBJ#">
                           #Variables.emailtxt#
                       </cfmail>
				</cfif>
				<cfif Variables.DevTestProdInd NEQ 2>
					<cfset Variables.IMUserEmailAdrTxt 		= Variables.IMUserEmailAdrTxt & Variables.DevTeamEml>
					<cfif listfindnocase( "S,U,C", trim(Variables.IMUserTypCd) )  gt 0>
						<cfset Variables.IMUserSuprvEmailAdrTxt = Variables.IMUserSuprvEmailAdrTxt & Variables.DevTeamEml>
					</cfif>
				</cfif>
				<!---  if only supervisor changed --->
				<cfif (Variables.SendEmailSupervisorFlag is true)
					and (Variables.SendEmailUserTypeFlag is false)
					and (Variables.SendEmailAddrEmailFlag is false)
					and (Variables.SendHQLocEmailFlag is false)
					>
					<!--- <cfmail to="#Variables.IMUserEmailAdrTxt#" from="#Variables.ITSecEMailAddr#" subject="#Variables.SRV# - Profile update confirmation" type="HTML">
						Dear #Variables.IMUserFirstNm#  #Variables.IMUserLastNm#,<br>
						This is a confirmation that your account has been updated and pending Supervisor Approval.
					</cfmail> --->
					<cfset Variables.cfprname				= "getEmail">
					<cfset Variables.LogAct					= "get email content">
                    <cfset Variables.emailcdseqnmb			= "6">
					<cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
                    <cfif Variables.TxnErr>
                        <cfset Variables.SaveMe				= "No">
                        <cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
                        <cfabort>
                    </cfif>
					<cfif Variables.DevTestProdInd NEQ 2>
						<cfset Variables.EMAILRCPNT = Variables.EMAILRCPNT & Variables.DevTeamEml>
					</cfif>
                    <cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
                        type="HTML" subject="#Variables.EMAILSUBJ#">
                        #Variables.emailtxt#
                    </cfmail>

					<!--- <cfmail to="#Variables.IMUserSuprvEmailAdrTxt#" from="#Variables.ITSecEMailAddr#" subject="#Variables.SRV# - Profile update approval pending" type="HTML">
						Dear #Variables.IMUserSuprvFirstNm#  #Variables.IMUserSuprvLastNm#,<br>
						The profile for #Variables.IMUserFirstNm#  #Variables.IMUserLastNm# (user name  #Variables.ImUserNm#) has been updated and waiting your approval.
						<br><br>
						Below are the steps to approve/decline new account requests and profile updates:<br>
	                       <li style="padding-left:5em">Login to Capital Access Login System (CLS) at <a href="#request.slafloginserverurl#/cls">#request.slafloginserverurl#/cls</a> </li>
	                       <li style="padding-left:5em">Click on Security System on Choose Function screen. </li>
	                       <li style="padding-left:5em">From the left hand menu, click on Authentication link under Pending Access Requests folder icon.</li>
	                       <li style="padding-left:5em">Upon performing decision, an automated email will be sent to the user.</li>
	                       <br /><br /><br />
	                       SBA CLS Staff
					</cfmail> --->
					<cfset Variables.cfprname				= "getEmail">
					<cfset Variables.LogAct					= "get email content">
                    <cfset Variables.emailcdseqnmb			= "5">
					<cfset Variables.USERURLDSPLY			= request.slafloginserverurl & "/cls">
					<cfset Variables.EMAILRCPNT				= Variables.IMUserSuprvEmailAdrTxt>
                    <cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
                    <cfif Variables.TxnErr>
                        <cfset Variables.SaveMe				= "No">
                        <cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
                        <cfabort>
                    </cfif>
					<cfif Variables.DevTestProdInd NEQ 2>
						<cfset Variables.EMAILRCPNT = Variables.EMAILRCPNT & Variables.DevTeamEml>
					</cfif>
                    <cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
                        type="HTML" subject="#Variables.EMAILSUBJ#">
                        #Variables.emailtxt#
                    </cfmail>
				</cfif>
				<!---  if email or user type changed --->
				<!--- <cfif Variables.SendEmailUserTypeFlag or Variables.SendEmailAddrEmailFlag or Variables.SendHQLocEmailFlag>
					<cfset Variables.UPURLString  = Encrypt('imuserid=' & trim(Variables.imuserid), Variables.encryptionKey,"AES/CBC/PKCS5Padding", "hex")>
					<cfmail to="#Variables.IMUserEmailAdrTxt#" from="#Variables.ITSecEMailAddr#" subject="#Variables.SRV# - Updated profile verification" type="HTML">
						<strong>Dear #Variables.IMUserFirstNm# #Variables.IMUserLastNm#</strong>,<br><br>
						Your profile had been updated in the SBA's Capital Access Login System (CLS) at
						<a href="#request.slafloginserverurl#/cls">#request.slafloginserverurl#/cls</a>
						</strong>. &nbsp;&nbsp;
						Your account had been suspended and needs email verification and Supervisor approval.<br>
						Please click on the link below to verify your email address:<br><br>
						<a href="#request.slafloginserverurl#/cls/dsp_clsuserconfrminfo.cfm?#UrlEncodedFormat(Variables.UPURLString)#&1=1">Verify Your Email</a><br><br>
						For additional help please contact your Program Office representative at the U.S. Small Business Administration.
						<br><br><br>
					</cfmail>
					<cfset Variables.Commentary	&= "An email has been sent."
					& "Please check your email.">

					<cfset Variables.cfprname				= "getEmail">
					 <cfset Variables.LogAct					= "get email content">
	                 <cfset Variables.emailcdseqnmb			= "8">
	                 <cfset Variables.prmtr3					= "#Variables.ServerPathForURL#/cls/dsp_clsuserconfrminfo.cfm?#UrlEncodedFormat(Variables.UPURLString)#&1=1">
	                 <cfinclude template="/cfincludes/oracle/security/spc_EMAILCDSELCSP.cfm">
	                 <cfif Variables.TxnErr>
	                 <cfset Variables.SaveMe				= "No">
	                 <cfoutput><p class="dem_errmsg">Serious database error occurred. Following information might help #Variables.ErrMsg#</p></cfoutput>
	                 <cfabort>
	                 </cfif>
	                 <cfif Variables.DevTestProdInd NEQ 2>
						<cfset Variables.EMAILRCPNT = Variables.EMAILRCPNT & ',nelli.salatova@sba.gov,rahul.gundala@sba.gov'>
					</cfif>
	                 <cfmail to="#Variables.EMAILRCPNT#" from="#Variables.ITSecEMailAddr#"
	                     type="HTML" subject="#Variables.EMAILSUBJ#">
	                     #Variables.emailtxt#
	                 </cfmail>
					</cfif>
				--->


				<!--- set session variable if account has been suspended --->
				<cfif (Variables.SendEmailUserTypeFlag or Variables.SendEmailAddrEmailFlag
				or Variables.SendEmailSupervisorFlag or Variables.SendHQLocEmailFlag) and Variables.SaveMe>
					<cfif Variables.PageIsUser>
						<cfif Variables.ImUserNm eq session.ImUserNm>
							<cflock scope="Session" type="Exclusive" timeout="30">
								<cfset Session.AccountIsSuspended = true>
							</cflock>
						</cfif>
					<cfelse>
						<cflock scope="Session" type="Exclusive" timeout="30">
							<cfset Session.AccountIsSuspended = true>
						</cflock>
					</cfif>
				</cfif>
			</cfif><!--- end commit  --->
		</cftransaction>
	</cfif><!--- end Variables.SaveMe --->
<!--- SaveMe check --->
<cfdump var="#Variables.SuccessPage#">
<cfif Variables.SaveMe>
	<cfif Variables.PageIsAddCustomer>
		<cfset Variables.Commentary	&= "Your account has been successfully created and email has been sent. "
			& "Please check your email.">
	<cfelseif Variables.PageIsProfile>
		<cfset Variables.Commentary = "Your profile information has been successfully updated. ">
		<cfif Variables.SendEmailVerify>
			<cfset Variables.Commentary	&= "An email has been sent. Please check your email.">
		</cfif>
	<cfelseif Variables.PageIsUser>
		<cfif Variables.NewOldUserInd eq "N">
			<cfset Variables.Commentary				&= "User account has been successfully created.<br>"
				& "An email with the login information has been sent to the user.">
		<cfelse>
			<cfset Variables.Commentary	&= "<li>User account has been successfully updated.</li>">
			<cfif Variables.SendEmailVerify>
				<cfset Variables.Commentary	&= "An email has been sent to the user.">
			</cfif>
		</cfif>
	</cfif>
</cfif>
<cfset Variables.ErrMsg										= "">
<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
<cflock scope="Session" type="Exclusive" timeout="30">
	<!---
	We no longer want Form Data Recovery to occur on the page we are cflocationing to. The following cfset prevents
	Form Data Recovery from occurring, as if we came from some other page:
	--->
	<cfset Session.SlafSaveFormData.PageNames		= "">
</cflock>
<cfif Variables.PageIsUser>
	<cfset Variables.URLString						= "/security/user/act_search.cfm"
													& "?NewUserProfile=Yes"
													& "&IMUserId=#URLEncodedFormat(Variables.IMUserId)#"
													& "&IMUserFirstNm=#URLEncodedFormat(Variables.IMUserFirstNm)#"
													& "&IMUserLastNm=#URLEncodedFormat(Variables.IMUserLastNm)#"
													& "&Commentary=#URLEncodedFormat(Variables.Commentary)#">
	<cflocation addtoken="No" url="#Variables.URLString#">
	<!--- Wherever caller told us to go. --->
<cfelse>
	<cflocation addtoken="No" url="#Variables.SuccessPage#?Commentary=#URLEncodedFormat(Variables.Commentary)#">
	<!--- Wherever caller told us to go. --->
</cfif>

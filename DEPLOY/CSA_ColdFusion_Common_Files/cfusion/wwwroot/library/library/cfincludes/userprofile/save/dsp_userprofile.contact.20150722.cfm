<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				05/16/2006. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				See dsp_userprofile.template.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Merged implementation. Cannibalized code from /library/cfincludes/userprofile. 
					07/08/2014, SRS:	Rescued from my hard drive after being trashed by someone who didn't know what he
										was doing. The version from the time of tiber-to-anacostia migration was in the
										middle of splitting up phone number into area code and exchange. Restored it and
										got ScqActvs from Server.Scq. (We don't have same ServerCachedQuery groups as OCIO).
					09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables.
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables.
					05/16/2006, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.AreaCdLabelContentDom						= "(Area Code)">
<cfset Variables.AreaCdLabelContentFor						= "(City Code)">

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>Contact Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfparam name="Variables.MandOptContact"				default="mand">
	<cfif Variables.MandOptContact is "view">
		<cfset Variables.MandOptIMCntryCd					= "view">
		<cfset Variables.MandOptIMUserZipCd					= "view">
		<cfset Variables.MandOptStCd						= "view">
		<cfset Variables.MandOptIMUserPostCd				= "view">
		<cfset Variables.MandOptIMUserStNm					= "view">
		<cfset Variables.MandOptIMUserCtyNm					= "view">
		<cfset Variables.MandOptIMUserStr1Txt				= "view">
		<cfset Variables.MandOptIMUserStr2Txt				= "view">
		<cfset Variables.MandOptIMUserPhnCntryCd			= "view">
		<cfset Variables.MandOptIMUserPhnAreaCd				= "view">
		<cfset Variables.MandOptIMUserPhnLclNmb				= "view">
		<cfset Variables.MandOptIMUserPhnExtnNmb			= "view">
		<cfset Variables.MandOptIMUserEmailAdrTxt			= "view">
		<cfset Variables.MandOptIMUserEmailAdrTxt2			= "skip">
	</cfif>
	<cfset Defaults("IMCntryCd",			"CountryCode",	"Country",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfset Variables.Domestic							=	(
																	(Len(Variables.VarValue)	is 0)
																or		(Variables.VarValue		is "US")
																)>
															<cfset Variables.ChkdD = "       ">
															<cfset Variables.ChkdF = "       ">
		<cfif Variables.Domestic>							<cfset Variables.ChkdD = "checked">
		<cfelse>											<cfset Variables.ChkdF = "checked"></cfif>
		<cfoutput>
    <input class="hide" type="radio" name="ShowHideAddr" id="domestic" #ChkdD# value="Dom"><label for="domestic"></label>
    <input class="hide" type="radio" name="ShowHideAddr" id="foreign"  #ChkdF# value="For"><label for="foreign"></label>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"></cfoutput><cfif Variables.MandOpt is "view"><cfoutput>
        #Variables.VarValue#</cfoutput><cfelse><cfoutput>
        <select #Variables.NameAndId# onchange="DoThisOnChangeCountry(this)"></cfoutput>
		<cfset Variables.DspOptsNotSelText					= "Not Yet Selected">
		<cfset Variables.DspOptsQueryName					= "Variables.ScqActvCountries">
		<cfset Variables.DspOptsSelList						= Variables.VarValue>
		<cfset Variables.DspOptsSkipList					= "">
		<cfset Variables.DspOptsTabs						= "#Request.SlafEOL#        ">
		<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
		<cfoutput>
        </select></cfoutput></cfif><cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("Zip5Cd",			"Zip",		"Zip",						"mand", "Zip5")>
	<cfset Defaults("Zip4Cd",			"ZipPlus4",	"+4",						"opt",  "Zip4")>
	<cfset Defaults("StCd",				"State",	"State",					"view", "St")>
	<cfif Variables.Domestic>
		<cfset Defaults("IMUserCtyNm",	"City",		"City",						"mand", "City")>
	<cfelse>
		<cfset Defaults("IMUserCtyNm",	"City",		"City",						"opt",  "City")>
	</cfif>
	<cfset Defaults("IMUserStr1Txt",	"Street1",	"Street Address Line 1",	"mand", "Str1")>
	<!--- Exceptions to Defaults: --->
	<cfif  Variables.MandOptSt is not "skip">
		<cfset Variables.MandOptSt							= "view"><!--- forced to "view" if not skipped. --->
	</cfif>
	<cfset Variables.LabelCity								= "<label for="""
															& Variables.VarNameCity
															& """ class=""hilab city "
															& Variables.MandOptCity
															& "label"">"
															& Variables.EngCity
															& "</label>"><!--- "hilab city" needed for HiGrpUtils --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.MandOptZip5 is not "skip">
		<cfoutput>
    <div class="tbl_row domestic">
      <div class="tbl_cell formlabel">
        #Variables.LabelZip5#<cfif Variables.MandOptZip4 is not "skip">#Variables.LabelZip4#</cfif>
      </div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptZip5#data"><cfif Variables.MandOptZip5 is "view">
        #Variables.VarValueZip5#<cfelse>
        <input type="Text" #Variables.NameIdAndValueZip5#
        size="5" maxlength="5" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngZip5)#', this.value, '99999', sMand, 5, 5))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptZip5#data --><cfif Variables.MandOptZip4 is not "skip">
        <div class="#Variables.MandOptZip4#data"><cfif Variables.MandOptZip4 is "view">
        #Variables.VarValueZip4#<cfelse>
        <input type="Text" #Variables.NameIdAndValueZip4#
        size="4" maxlength="4" onblur="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngZip4)#', this.value, '9999', sMand, 4, 4))
            {
            LookupZipToDropdown
                (
                this.form.#Variables.VarNameZip5#.value + '-' + this.form.#Variables.VarNameZip4#.value,
                this.form.ZipDropdown,
                this.form.#Variables.VarNameSt#,
                null,
                this.form.#Variables.VarNameCity#,
                this.form.#Variables.VarNameStr1#,
                null,
                null,
                null,
                LookupZipCompletionRoutine // Also calls DoThisOnChangeStCd.
                );
            return true;
            }
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptZip4#data --></cfif>
        <input type="Button" id="LookupZipButton" value="Lookup Zip" onclick="
        // The following silent calls to EditMask must always treat Zip5 and Zip4 as mandatory, so as to detect not-given. 
        gSilentEditMask = 1;
        //  gLookupZipToDropdownTrace                      = true;
        if  (EditMask('', this.form.#Variables.VarNameZip5#.value, '9', 1, 5, 5))
            {<cfif Variables.MandOptZip4 is not "skip">
            gSilentEditMask                                = 1;
            if  (EditMask('', this.form.#Variables.VarNameZip4#.value, '9', 1, 4, 4))
                LookupZipToDropdown
                    (
                    this.form.#Variables.VarNameZip5#.value + '-' + this.form.#Variables.VarNameZip4#.value,
                    this.form.ZipDropdown,
                    this.form.#Variables.VarNameSt#,
                    null,
                    this.form.#Variables.VarNameCity#,
                    this.form.#Variables.VarNameStr1#,
                    null,
                    null,
                    null,
                    LookupZipCompletionRoutine // Also calls DoThisOnChangeStCd.
                    );
            else</cfif><!--- /MandOptZip4 is not "skip" --->
                LookupZipToDropdown
                    (
                    this.form.#Variables.VarNameZip5#.value,
                    this.form.ZipDropdown,
                    this.form.#Variables.VarNameSt#,
                    null,
                    this.form.#Variables.VarNameCity#,
                    this.form.#Variables.VarNameStr1#,
                    null,
                    null,
                    null,
                    LookupZipCompletionRoutine // Also calls DoThisOnChangeStCd.
                    );
            }
        else
            alert ('You cannot do the lookup until you enter a 5-digit Zip code.');
        ">
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row domestic" id="ZipDropdownRow" style="display:none;">
      <div class="tbl_cell formlabel"><label class="mandlabel" for="ZipDropdown">Choose Result:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <select name="ZipDropdown" id="ZipDropdown" onchange="
        if  (this.selectedIndex >= 0)
            {
            var sOpt                                    = this.options[this.selectedIndex];
            // Relay the result of an assignment to the second argument of the function, like a = b = c:
            SetFormEltValue(this.form.#Variables.VarNameSt#, (document.getElementById('#Variables.VarNameSt#display').innerHTML = sOpt.StCd));
            SetFormEltValue(this.form.#Variables.VarNameCity#, sOpt.CtyNm);
            if  ((sOpt.StrNm.length + sOpt.StrSfx.length) == 0)
                SetFormEltValue(this.form.#Variables.VarNameStr1#, '');
            else
                SetFormEltValue(this.form.#Variables.VarNameStr1#, sOpt.StrNm + ' ' + sOpt.StrSfx);
            }
        ">
        <option value="">Enter a Zip, press Lookup Zip button and choose result here.</option>
        </select>
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /DivZipDropdownRow --></cfoutput>
	</cfif><!--- /MandOptZip5 is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserPostCd",			"Postal",	"Postal Code",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row foreign">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="20" maxlength="20" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserStNm",			"StateName",	"State/Province",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row foreign">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="60" maxlength="60" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.MandOptStr1 is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelStr1#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptStr1#data"><cfif Variables.MandOptStr1 is "view">
        #Variables.VarValueStr1#<cfelse>
        <input type="Text" #Variables.NameIdAndValueStr1#
        size="60" maxlength="80" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngStr1)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptStr1#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --><cfif Variables.MandOptStr1 is not "view">
    <div class="tbl_row domestic">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata">
        (Please add street number.)
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfif></cfoutput>
	</cfif><!--- /MandOptStr1 is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserStr2Txt",			"Street2",	"Street Address Line 2",	"opt", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="60" maxlength="80" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif (Variables.MandOptCity is not "skip") or (Variables.MandOptSt is not "skip")>
		<cfoutput>
    <div class="tbl_row"><!-- shared between foreign and domestic -->
      <div class="tbl_cell formlabel">#Variables.LabelCity#<span class="domestic">/#Variables.LabelSt#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="hibox city #Variables.MandOptCity#data"><cfif Variables.MandOptCity is "view">
        #Variables.VarValueCity#<cfelse>
        <input type="Text" #Variables.NameIdAndValueCity#
        class="hielt city" size="60" maxlength="60" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngCity)#', this.value, 'X', sMand, 1, this.maxLength))
            {
            //  In the following, notFoundIndex is an existence test. (It implies that at least one lookup was done/) 
            if  (gDomestic && this.form.ZipDropdown.notFoundIndex)
                this.form.ZipDropdown.selectedIndex     = this.form.ZipDropdown.notFoundIndex;
            return true;
            }
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptCity#data -->
        &nbsp;
        <div class="viewdata domestic" id="#Variables.VarNameSt#display">#Variables.VarValueSt#</div>
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="StCd" id="StCd" value="#Variables.VarValueSt#">
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.ReadonlyProfile>
		<cfset Variables.MandOptPhone						= "view">
	<cfelse>
		<cfparam name="Variables.MandOptPhone"				default="mand">
	</cfif>
	<cfif Variables.MandOptPhone is "view">
		<cfset Variables.MandOptIMUserPhnCntryCd			= "view">
		<cfset Variables.MandOptIMUserPhnAreaCd				= "view">
		<cfset Variables.MandOptIMUserPhnLclNmb				= "view">
		<cfset Variables.MandOptIMUserPhnExtnNmb			= "view">
	</cfif>
	<cfif Variables.MandOptPhone is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel #Variables.MandOptPhone#label">Phone Number</div>
      <div class="tbl_cell formdata nowrap"><!-- Side-by-Side --></cfoutput>
		<cfset Defaults("IMUserPhnCntryCd",		"PhoneCountry",	"Country",		"mand", "Cntry")>
		<cfif Variables.Domestic>
			<cfset Defaults("IMUserPhnAreaCd",	"AreaCode",		"Area Code",	"mand", "Area")>
		<cfelse>
			<cfset Defaults("IMUserPhnAreaCd",	"AreaCode",		"City Code",	"opt",  "Area")>
		</cfif>
		<cfset Defaults("IMUserPhnLclNmb",		"Phone",		"Phone Number",	"mand", "Lcl")>
		<cfset Defaults("IMUserPhnExtnNmb",		"PhoneExt",		"Extension",	"opt",  "Extn")>
		<!--- Exceptions to Defaults: --->
		<cfset Variables.LabelArea							= "<label for="""
															& Variables.VarNameArea
															& """ class=""hilab area "
															& Variables.MandOptArea
															& "label"">"
															& Variables.EngArea
															& "</label>"><!--- "hilab area" needed for HiGrpUtils --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOptCntry#data"><cfif Variables.MandOptCntry is "view">
          #Variables.VarValueCntry#<cfelse>
          <input type="Text" #Variables.NameIdAndValueCntry#
          size="6" maxlength="6" onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngCntry)#', this.value, '9', sMand, 1, this.maxLength))
              {
              if  ((this.value == '1') || (this.value == ''))
                  {
                  HiGrpMandatory('.area');
                  $('.hilab.area').each(function(){this.innerHTML = 'Area Code';});
                  this.form.#Variables.VarNameLcl#.placeholder = '999-9999';
                  }
              else
                  {
                  HiGrpOptional('.area');
                  $('.hilab.area').each(function(){this.innerHTML = 'City Code';});
                  this.form.#Variables.VarNameLcl#.placeholder = '';
                  }
              return true;
              }
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.LabelCntry#)
        </div><!-- /Vertical --></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
		<cfoutput>
        &nbsp;
        <div class="inlineblock"><!-- Vertical -->
          <div class="hibox area #Variables.MandOptArea#data"><cfif Variables.MandOptArea is "view">
        #Variables.VarValueArea#<cfelse>
          <input type="Text" #Variables.NameIdAndValueArea#
          class="hielt area" size="3" maxlength="5" onchange="
          var sMand											= isMand(this);
          if  (gDomestic) // Easiest way to toggle description. Do it twice. Criteria have to change for City Code anyway. 
              {
              if  (EditMask('Area Code', this.value, '9', sMand, 3, 3))
                  return true;
              }
          else
              {
              if  (EditMask('City Code', this.value, '9', sMand, 1, this.maxLength))
                  return true;
              }
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.LabelArea#)
        </div><!-- /Vertical --></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
		<cfoutput>
        &nbsp;
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOptLcl#data"><cfif Variables.MandOptLcl is "view">
        #Variables.VarValueLcl#<cfelse>
          <input type="Text" #Variables.NameIdAndValueLcl#
          size="8" maxlength="15"<cfif Variables.VarValueCntry is "1"> placeholder="999-9999"</cfif> onchange="
          var sMand                               = isMand(this);
          var sEditMaskResult;
          var sPhoneCountry                       = this.form.#Variables.VarNameCntry#.value;
          if  ((sPhoneCountry == '1') || (sPhoneCountry == ''))
              sEditMaskResult                     = EditMask('Phone Number', this.value, '999-9999', sMand, 8, 8);
          else
              sEditMaskResult                     = EditMask('Phone Number', this.value, 'X', sMand, 1, this.maxLength);
          if  (sEditMaskResult)
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.LabelLcl#)
        </div><!-- /Vertical --></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
		<cfoutput>
        &nbsp;
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOptExtn#data"><cfif Variables.MandOptExtn is "view">
          #Variables.VarValueExtn#<cfelse>
          <input type="Text" #Variables.NameIdAndValueExtn#
          size="6" maxlength="6" onchange="
          var sMand											= isMand(this);
          if  (EditMask('Extension', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.LabelExtn#)
        </div><!-- /Vertical -->
      </div><!-- /formdata = /Side-by-Side -->
    </div><!-- tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserEmailAdrTxt",			"EMail1",	"E-Mail Address",		"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="60" maxlength="255" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'E', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserEmailAdrTxt2",			"EMail2",	"Re-enter E-Mail Address",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="60" maxlength="255" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'E', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Contact Information --><br/><!-- Break because fieldset is class="inlineblock". -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<!--- Just scripts that require Variable.Var names. For generic scripts, use dsp_userprofile.js. --->
	<cfoutput>#Variables.JSInline#

<!-- Contact-Related: -->

#Request.SlafTopOfHeadAjax#
#Request.SlafTopOfHeadHiGrpUtils#
#Request.SlafTopOfHeadLookupCountryData#
#Request.SlafTopOfHeadLookupZipAjax#
#Request.SlafTopOfHeadSetFormEltValue#
<script>

gDomestic													= true;
gForeign													= false;
gRadioDomestic												= null;
gRadioForeign												= null;

function DoThisOnChangeCountry								(pThis)
	{
	var	sForm												= pThis.form;
	var	sOptions											= pThis.options;
	var	sOptionsLen											= sOptions.length;
	var	sSelIdx												= pThis.selectedIndex;
	var	sFIPSCountryCode									= ((sSelIdx < 1) ? "US" : sOptions[sSelIdx].value);
	if	(!gRadioDomestic)
		gRadioDomestic										= document.getElementById("domestic");	// So we can click() them later. 
	if	(!gRadioForeign)
		gRadioForeign										= document.getElementById("foreign");	// So we can click() them later. 
	if	(sFIPSCountryCode === "US")
		{
		gDomestic											= true;	// Used in JavaScript. 
		gForeign											= false;// Used in JavaScript. 
		gRadioDomestic.click();										// Used in CSS selector. 
		$(".hilab.area").each(function(){this.innerHTML = "Area Code"});
		HiGrpMandatory(".area");
		HiGrpMandatory(".city");
		}
	else
		{
		gDomestic											= false;// Used in JavaScript. 
		gForeign											= true;	// Used in JavaScript. 
		gRadioForeign.click();										// Used in CSS selector. 
		$(".hilab.area").each(function(){this.innerHTML = "City Code"});
		HiGrpOptional(".area");
		HiGrpOptional(".city");
		}
	DoThisOnChangeStCd();
	LookupCountryPhoneCode($, sFIPSCountryCode, sForm.#Variables.VarNameCntry#);
	return true;
	}

// Note that DoThisOnChangeStCd is called by both DoThisOnChangeCountry and LookupZipCompletionRoutine. 
// That's why it has to be a separate function. 
function DoThisOnChangeStCd()
	{
	var	sForm												= document.#Variables.FormName#;
	var	sStCd												= sForm.#Variables.VarNameSt#;
	var	sStCdDisplay										= document.getElementById("#Variables.VarNameSt#display");
	//	if	(gForeign)												// Don't throw away lookup result just because chose foreign. 
	//		sStCd.value										= "";	// Don't throw away lookup result just because chose foreign. 
	var	sVal												= sStCd.value;
	sStCdDisplay.innerHTML									= ((sVal.length == 2) ? sVal : "");
	}

function LookupZipCompletionRoutine()
	{
	var	sForm												= document.#Variables.FormName#;
	var	sZipDropdownRow										= document.getElementById("ZipDropdownRow");
	if	(sForm.ZipDropdown.options.length > 2)				// That is, if LookupZipToDropdown returned multiple rows. 
		{
		sForm.#Variables.VarNameSt#.value					= '';
		sZipDropdownRow.style.display						= '';
		}
	else// Otherwise, there LookupZipToDropdown already set CtnNm, StCd, etc, so don't bother the user with the dropdown.
		sZipDropdownRow.style.display						= 'none';
	DoThisOnChangeStCd();
	}

</script>
<style>

##domestic:checked ~ div.foreign,							/* If domestic, all foreign  sibling (tbl_row) divs. */
##foreign:checked  ~ div.domestic,							/* If foreign,  all domestic sibling (tbl_row) divs. */
##domestic:checked ~ div .foreign,							/* If domestic, all nested foreign  objects. */
##foreign:checked  ~ div .domestic							/* If foreign,  all nested domestic objects. */
	{
	display:												none;
	}

</style>
</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

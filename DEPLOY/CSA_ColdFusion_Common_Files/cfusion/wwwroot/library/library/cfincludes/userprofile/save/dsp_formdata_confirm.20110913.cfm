<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				09/01/2011
DESCRIPTION:		Displays the identity confirmation part of the user profile for use in a form. 
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html. 
INPUT:				Variables scope variables for each of the IMUserTbl columns supported. DisplayDOB. DisplayPIN. 
					ReadOnlyProfile and other flags. 
OUTPUT:				Form elements in "tbl tags". 
REVISION HISTORY:	09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables. 
					07/16/2010, IAC:	Changed 18 to 15 for age based on Terry Lewis Request. 
					09/09/2008, SRS:	Added date picker (CalendarPopup) to IMUserDOBDt. 
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables. 
					05/16/2006, SRS:	Original implementation.
--->

<!--- Initializations: --->

<cfparam name="Variables.DisplayDOB"					default="Yes">
<cfparam name="Variables.DisplayPIN"					default="Yes">
<cfinclude template="bld_readonly_confirm.cfm">

<!--- Display the data: --->

<cfif Variables.ReadOnlyIMUserDOBDt>
	<cfif Variables.DisplayDOB>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">Date of Birth:</div>
      <div class="tbl_cell formdata nowrap">
        <div class="viewdata">
        #Variables.IMUserDOBDt#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserDOBDt" value="#Variables.IMUserDOBDt#">
        </div><!-- /viewdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif><!--- Variables.DisplayDOB --->
<cfelse><!--- Variables.ReadOnlyIMUserDOBDt --->
	<cfif Variables.DisplayDOB>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserDOBDt">Date of Birth:</label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="manddata">
        <input type="Text" name="IMUserDOBDt" id="IMUserDOBDt" value="#Variables.IMUserDOBDt#"
        size="10" maxlength="10" title="mm/dd/yyyy" onChange="
        if  (EditDateMinYearsOld	('Date of Birth', this.value, 1, 15, ' to use SBA systems'))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata --></cfoutput>
		<!---
		Implementation notes: 
		This file is cfincluded from /eauth/dsp_prompted_activation.cfm and from /cls/dsp_addcustomer.cfm. 
		There are some inconsistencies between these 2 environments. Because EAuth calls the current form 
		"ActivationForm", but GLS calls it "requestid", we cannot reference "document.formname.IMUserDOBDt" as 
		the first parameter to gCalendarPopup.select(). Fortunately IMUserDOBDt is also the form element's id 
		attribute, so we can simply use "document.getElementById('IMUserDOBDt')" instead. Also, EAuth defines 
		LibImgURL, but GLS does not. Fortunately, both Application.cfm files define LibURL, so we just use that: 
		--->
		<cfoutput>
        &nbsp;
        <a name="AnchorIMUserDOBDt" id="AnchorIMUserDOBDt"
        href="javascript:gCalendarPopup.select(document.getElementById('IMUserDOBDt'),'AnchorIMUserDOBDt','MM/dd/yyyy');">
        <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
        alt="Pick a date using a popup calendar. (Opens a new window.)"
        title="Pick a date using a popup calendar. (Opens a new window.)">
        </a>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse><!--- Keep as hidden, even if not displaying. --->
		<cfoutput>
  <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
  <input type="Hidden" name="IMUserDOBDt" value="#Variables.IMUserDOBDt#"></cfoutput>
	</cfif><!--- /DisplayDOB --->
</cfif><!--- /ReadOnlyIMUserDOBDt --->

<cfif Variables.ReadOnlyIMUserLast4DgtSSNNmb>
	<cfif Variables.DisplayPIN>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel viewlabel">PIN:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata">
        #Variables.IMUserLast4DgtSSNNmb#
        <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
        <input type="Hidden" name="IMUserLast4DgtSSNNmb" value="#Variables.IMUserLast4DgtSSNNmb#">
        </div><!-- /viewdata -->
        &nbsp;
        (Personal Identification Number, Last 4 Digits of SSN)
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	</cfif><!--- /DisplayPIN --->
<cfelse><!--- Variables.ReadOnlyIMUserLast4DgtSSNNmb --->
	<cfif Variables.DisplayPIN>
		<cfoutput>
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel"><label for="IMUserLast4DgtSSNNmb">PIN:</label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="manddata">
        <input type="Text" name="IMUserLast4DgtSSNNmb" id="IMUserLast4DgtSSNNmb" value="#Variables.IMUserLast4DgtSSNNmb#"
        size="4" maxlength="4" onChange="
        if  (EditMask('PIN / Last 4 Digits of SSN', this.value, '9', 1, 4, 4))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
        &nbsp;
        (Personal Identification Number, Last 4 Digits of SSN)
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
	<cfelse><!--- Keep as hidden, even if not displaying. --->
		<cfoutput>
  <!-- Readonly hidden fields used for form data recovery only. (Not resaved.) -->
  <input type="Hidden" name="IMUserLast4DgtSSNNmb" value="#Variables.IMUserLast4DgtSSNNmb#"></cfoutput>
	</cfif><!--- /DisplayPIN --->
</cfif><!--- /ReadOnlyIMUserLast4DgtSSNNmb --->

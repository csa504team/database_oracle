<!---
AUTHOR:				Steve Seaquist
DATE:				06/06/2006
DESCRIPTION:		Builds Variables.ReadOnly((columnname)) variables from Variables.ReadOnlyProfile and defaults. 
NOTES:				Called in both the dsp_formdata and dsp_onsubmit files. 
INPUT:				Variables.ReadOnlyProfile. 
OUTPUT:				Variables.ReadOnly((columnname)) variables. 
REVISION HISTORY:	10/17/2012, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to changes in dsp_formdata_name (none). 
					06/06/2006, SRS:	Original implementation.
--->

<cfparam name="Variables.ReadOnlyProfile"				default="No">
<cfif Variables.ReadOnlyProfile>
	<cfset Variables.ReadOnlyIMUserFirstNm				= "Yes">
	<cfset Variables.ReadOnlyIMUserMidNm				= "Yes">
	<cfset Variables.ReadOnlyIMUserLastNm				= "Yes">
	<cfset Variables.ReadOnlyIMUserSfxNm				= "Yes">
<cfelse>
	<cfparam name="Variables.ReadOnlyIMUserFirstNm"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserMidNm"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserLastNm"		default="No">
	<cfparam name="Variables.ReadOnlyIMUserSfxNm"		default="No">
</cfif>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				09/01/2011
DESCRIPTION:		Displays the identity and contact parts of the user profile for use in a form. 
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html. 
INPUT:				Variables scope variables for each of the IMUserTbl columns supported. Only exception: IMUserZipCd, 
					which is IMUserTbl's Zip5Cd and/or Zip4Cd combined into 99999 or 99999-9999 format. 
					ReadOnlyProfile and other flags. 
OUTPUT:				Form elements in "tbl tags". 
REVISION HISTORY:	05/23/2014, SRS:	After GLS-to-CLS conversion, allowed caller to control inlineblock. 
					09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables. 
					05/16/2006, SRS:	Original implementation. 
--->

<cfparam name="Variables.DspFormDataInlineBlocks"	default="No">

<cfoutput>
<fieldset<cfif Variables.DspFormDataInlineBlocks> class="inlineblock"</cfif>><legend>Identity Information</legend>
</cfoutput>
<cfinclude template="dsp_formdata_name.cfm">
<cfinclude template="dsp_formdata_confirm.cfm">
<cfoutput>
</fieldset><cfif Variables.DspFormDataInlineBlocks><br/></cfif><!-- /Identity Information -->

<!-- **************************************************************************************** -->

<fieldset<cfif Variables.DspFormDataInlineBlocks> class="inlineblock"</cfif>><legend>Contact Information</legend>
</cfoutput>
<cfinclude template="dsp_formdata_contact.cfm">
<cfoutput>
</fieldset><cfif Variables.DspFormDataInlineBlocks><br/></cfif><!-- /Contact Information -->
</cfoutput>

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				Called by dsp_userprofile.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	Appended to the end of AppDataInline and JSInline. 
REVISION HISTORY:	07/30/2015, SRS:	Original implementation. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock TypeSensitive TypeC TypeN TypeS TypeU" style="display:none;"><!--- *NOT* TypeP (App-to-App) --->
  <legend><!--- No job titles for TypeP, so that's why it's disabled. --->
    <span class="TypeSensitive TypeC"						  style="display:none;">SBA Contractor</span>
    <span class="TypeSensitive TypeN"						  style="display:none;">Borrower</span>
    <span class="TypeSensitive TypeS"						  style="display:none;">SBA Employee</span>
    <span class="TypeSensitive TypeU"						  style="display:none;">Partner</span>
    Information
  </legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("LocId", "Loc",	"Current Location ID", "opt",  "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row TypeSensitive TypeC TypeS TypeU" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
        <a class="optlabel" target="_blank" href="/cls/cfincludes/dsp_lookup_lend.cfm?FField=#Variables.VarName#"
        title="Opens a new window with a lookup form. The results of this lookup are copied back into #Variables.Eng#.">Lookup</a>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.VarValueTypCd is "U">
		<cfset Defaults("HQLocId",	"HeadquartersLoc",	"Headquarters Location ID", "mand", "HQLoc")>
	<cfelse>
		<cfset Defaults("HQLocId",	"HeadquartersLoc",	"Headquarters Location ID", "opt",  "HQLoc")>
	</cfif>
	<cfset Variables.Label									= "<label for="""
															& Variables.VarName
															& """ class=""hilab hqloc "
															& Variables.MandOpt
															& "label"">"
															& Variables.Eng
															& "</label>">
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row TypeSensitive TypeU" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="hibox hqloc #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" class="hielt hqloc" #Variables.NameIdAndValue#
        size="7" maxlength="7" onchange="return DoThisOnChangeHeadquartersLoc(this);"></cfif>
        </div><!-- /#Variables.MandOpt#data -->
        <a class="optlabel" target="_blank" href="/cls/cfincludes/dsp_lookup_lend.cfm?FField=#Variables.VarName#"
        title="Opens a new window with a lookup form. The results of this lookup are copied back into #Variables.Eng#.">Lookup</a>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("SBAOfcCd",	"SBAOfficeCode", "Default Office Code", "opt", "")>
	<cfelse>
		<cfset Defaults("SBAOfcCd",	"SBAOfficeCode", "Default Office Code", "view", "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row TypeSensitive TypeC TypeS" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 4, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("JobTitlCd", "JobTitle", "Job Classification", "opt", "")>
	<cfset Variables.VarNameJobTitl							= Variables.VarName>	<!--- Needed later. --->
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data">
        <!-- Using style="display:none;" because jQuery 3.0+ .show()/.hide() will break on display:none in a class: --></cfoutput>
		<!---
		Interesting problem here: What if UserType is editable, but JobTitlCd isn't? You can't check a checkbox 
		if there isn't any checkbox. So we should only generate spans of viewdata if the user currently has the 
		JobTitlCd (and can't change it). 
		--->
		<cfloop query="Variables.ActvIMJobTitlTypTbl">
			<cfif Variables.MandOpt is "view">
				<cfif ListFindNoCase(Variables.VarValue, Variables.ActvIMJobTitlTypTbl.code) gt 0>
					<cfoutput>
        <span class="JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
          #Variables.ActvIMJobTitlTypTbl.description#<br/>
        </span></cfoutput>
				</cfif>
			<cfelse>
				<cfif ListFindNoCase(Variables.VarValue, Variables.ActvIMJobTitlTypTbl.code) gt 0>
					<cfset Variables.Checked				= "checked">
				<cfelse>
					<cfset Variables.Checked				= "       ">
				</cfif>
				<cfif Variables.ActvIMJobTitlTypTbl.code	is 6><!--- 6 = LSP --->
					<cfset Variables.Disabled				= "disabled">
				<cfelse>
					<cfset Variables.Disabled				= "        ">
				</cfif>
				<cfoutput>
        <label class="#Variables.MandOpt#label JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
          <input type="Checkbox" class="JobChk" name="#Variables.VarName#" #Variables.Checked# #Variables.Disabled# value="#Variables.ActvIMJobTitlTypTbl.code#" onclick="
          if  (this.checked)
              $('.Job'+this.value).show();
          else
              $('.Job'+this.value).hide();
          ">
          #Variables.ActvIMJobTitlTypTbl.description#<br/>
        </label></cfoutput>
			</cfif>
		</cfloop>
		<cfoutput>
        <div class="viewdata JobTitle TypeNone" style="display:none;">
          (none<cfif Variables.MandOpt is not "view"> for this type</cfif>)
        </div></cfoutput>
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /JobTitlCd --->
	<cfoutput>
  </div><!-- /tbl-->
</fieldset><!-- /User Information --><br class="TypeSensitive TypeC TypeN TypeS TypeU" style="display:none;" />

<div class="TypeSensitive TypeN" style="display:none;">

<fieldset class="inlineblock JobSensitive Job8" style="display:none;">
  <legend>Bond Borrower</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DUNS",	"DUNSNumber",	"DUNS Number", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
        <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data">
        <input type="Text" #Variables.NameIdAndValue# size="9" maxlength="9" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 9, 9))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DoEIN1",	"DoEIN1",	"EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
			<cfset Defaults("EIN1",	"EIN1",	"First 2 digits of EIN",	"mand", "Tax7")>
			<cfset Defaults("EIN2",	"EIN2",	"Last 7 digits of EIN",		"mand", "Tax8")>
			<cfoutput><cfif Variables.MandOptTax7 is "view">
        #Variables.VarValueTax7#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax7#">#Variables.EngTax7#</label>
        <input type="password" #Variables.NameIdAndValueTax7# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax7)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax8 is "view">
        #Variables.VarValueTax8#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax8#">#Variables.EngTax8#</label>
        <input type="Text" #Variables.NameIdAndValueTax8# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax8)#s',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DoEIN2",	"DoEIN2",	"Re-enter EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("EIN3",	"EIN3",	"Re-enter First 2 digits of EIN",	"mand", "Tax9")>
		<cfset Defaults("EIN4",	"EIN4",	"Re-enter Last 7 digits of EIN",	"mand", "Taxa")>
		<cfoutput><cfif Variables.MandOptTax9 is "view">
        #Variables.VarValueTax9#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax9#">#Variables.EngTax9#</label>
        <input type="password" #Variables.NameIdAndValueTax9# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax9)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTaxa is "view">
        #Variables.VarValueTaxa#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTaxa#">#Variables.EngTaxa#</label>
        <input type="Text" #Variables.NameIdAndValueTaxa# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTaxa)#',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN2 --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /JobSensitive Job8 (Bond Borrower) --><br class="JobSensitive Job8" style="display:none;" />

<fieldset class="inlineblock JobSensitive Job7" style="display:none;">
  <legend>Loan Borrower</legend>
  <div class="tbl"></cfoutput>
	<cfset Defaults("DoSSN1",	"DoSSN1",	"SSN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId1",	"TaxId1",	"First 3 digits of SSN",	"mand", "Tax1")>
		<cfset Defaults("TaxId2",	"TaxId2",	"Middle 2 digits of SSN",	"mand", "Tax2")>
		<cfset Defaults("TaxId3",	"TaxId3",	"Last 4 digits of SSN",		"mand", "Tax3")>
		<cfoutput><cfif Variables.MandOptTax1 is "view">
        #Variables.VarValueTax1#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax1#">#Variables.EngTax1#</label>
        <input type="password" #Variables.NameIdAndValueTax1# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax1)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax2 is "view">
        #Variables.VarValueTax2#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax2#">#Variables.EngTax2#</label>
        <input type="password" #Variables.NameIdAndValueTax2# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax2)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax3 is "view">
        #Variables.VarValueTax3#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax3#">#Variables.EngTax3#</label>
        <input type="Text" #Variables.NameIdAndValueTax3# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax3)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DoSSN2",	"DoSSN2",	"Re-enter SSN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId4",	"TaxId4",	"Re-enter First 3 digits of SSN",	"mand", "Tax4")>
		<cfset Defaults("TaxId5",	"TaxId5",	"Re-enter Middle 2 digits of SSN",	"mand", "Tax5")>
		<cfset Defaults("TaxId6",	"TaxId6",	"Re-enter Last 4 digits of SSN",	"mand", "Tax6")>
		<cfoutput><cfif Variables.MandOptTax4 is "view">
        #Variables.VarValueTax4#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax4#">#Variables.EngTax4#</label>
        <input type="password" #Variables.NameIdAndValueTax4# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax4)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax5 is "view">
        #Variables.VarValueTax5#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax5#">#Variables.EngTax5#</label>
        <input type="password" #Variables.NameIdAndValueTax5# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax5)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax6 is "view">
        #Variables.VarValueTax6#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax6#">#Variables.EngTax6#</label>
        <input type="Text" #Variables.NameIdAndValueTax6# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax6)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN2 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("LoanNmb",	"LoanNumber",	"Loan Number", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue# size="11" maxlength="11" onchange="
        var sMand											= isMand(this);
        if  (EditLoanNmb('#JSStringFormat(Variables.Eng)#', this.value, sMand, true))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /JobSensitive Job7 (Loan Borrower) --><br class="JobSensitive Job7" style="display:none;" />

</div><!-- /TypeSensitive TypeN -->

<fieldset class="inlineblock JobSensitive Job4" style="display:none;">
  <legend>LINC Areas of Interest</legend>
  <p>
    Now is a good time to enter the Areas of Interest for the LINC Program. 
  </p>
  <ul>
    <li>Selecting a state will popup a window with all the counties in that state.</li>
    <li>You can use the <strong>Select All</strong> button to check all counties. Or select individual counties.</li>
    <li>The <strong>Done</strong> button will return you to the list of states.</li>
    <li>When you have added all the states and counties you need. Press the <strong>Save Changes</strong> button.</li>
    <li>When the system has finshed processing the items an alert will tell you that the page can be safely closed.</li>
  </ul>
  <iframe src="#Variables.LincAreasURL#/areasofoperation.cfm?LENDRUSERNM=#Variables.Origs.IMUserSuprvId#"
          name="Upload IFrame" id="Upload Frame"
          width="680" height="440" marginwidth="2" marginheight="2">Please use a browser that supports IFrame.</iframe>
</fieldset><!-- /JobSensitive Job4 (LINC Contact) --><br class="JobSensitive Job4" style="display:none;" />

<fieldset class="inlineblock TypeSensitive TypeC TypeS TypeU" style="display:none;"><!--- Only the types with supervisors. --->
  <legend>
    <span class="TypeSensitive TypeC"						  style="display:none;">SBA Contractor's</span>
    <span class="TypeSensitive TypeS"						  style="display:none;">SBA</span>
    <span class="TypeSensitive TypeU"						  style="display:none;">Partner's</span>
    Supervisor (Lookup)
  </legend>
  <span class="TypeSensitive TypeC nowrap" style="display:none;">
    Also known as Contracting Officer Representative, or "COR".<br/>
    <hr/>
  </span>
  <span class="TypeSensitive TypeU nowrap" style="display:none;">
    Also known as Lender's Authorizing Official, or "LAO". Your LAO will have <br/>
    the same "#Variables.EngHQLoc#" as you, so please enter that first (above). <br/>
    <hr/>
  </span>
  <div class="inlineblock dem_errmsg" id="SupervisorInvalidMsg" style="display:<cfif Variables.Origs.SuprvInd is "A">none</cfif>;">
  Supervisor information is invalid for the currently selected User Type. <br/>
  Please use the following lookup form to find and select another supervisor. 
  </div>
  <div align="center" class="pad10">
    Note: In the following, "Partial" means "starts with": 
  </div>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Variables.SuprvEmail								= "">
	<cfset Variables.SuprvFirst								= "">
	<cfset Variables.SuprvLast								= "">
	<cfset Defaults("SuprvEmail", "SupervisorsEmail", "Supervisor's Partial E-Mail",     "opt", "SupE")>
	<cfset Defaults("SuprvFirst", "SupervisorsFirst", "Supervisor's Partial First Name", "opt", "SupF")>
	<cfset Defaults("SuprvLast",  "SupervisorsLast",  "Supervisor's Partial Last Name",  "opt", "SupL")>
	<cfif Variables.MandOptSupE is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupE#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOptSupE is "view">
        #Variables.VarValueSupE#<cfelse>
        <input type="Text" #Variables.NameIdAndValueSupE#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        // Don't use EditMask 'E' here, because it forces user to enter up through the @-sign: 
        if  (EditMask('#JSStringFormat(Variables.EngSupE)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptSupE#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOptSupE is not "skip" --->
	<cfif Variables.MandOptSupF is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupF#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptSupF#data"><cfif Variables.MandOptSupF is "view">
        #Variables.VarValueSupF#<cfelse>
        <input type="Text" #Variables.NameIdAndValueSupF#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngSupF)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptSupF#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<cfif Variables.MandOptSupL is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupL#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptSupL#data"><cfif Variables.MandOptSupL is "view">
        #Variables.VarValueSupL#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngSupL)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Button" name="JustAButton" id="LookupSupervisorButton" value="Lookup Supervisor" onclick="DoLookupSupervisor(this)">
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <select name="SupervisorDropdown" id="SupervisorDropdown" onchange="DoChooseSupervisor(this)">
        <option value="">Supervisors Appear Here After Lookup</option></cfoutput>
	<cfif	(Len(Variables.Origs.IMUserSuprvFirstNm)		gt 0)
		or	(Len(Variables.Origs.IMUserSuprvLastNm)			gt 0)>
		<cfset Variables.OptionValue						=  "#Variables.Origs.IMUserSuprvId#"
															& "|#Variables.Origs.IMUserSuprvFirstNm#"
															& "|#Variables.Origs.IMUserSuprvMidNm#"
															& "|#Variables.Origs.IMUserSuprvLastNm#"
															& "|#Variables.Origs.IMUserSuprvEmailAdrTxt#"
															& "|#Variables.Origs.IMUserSuprvUserId#">
		<cfset Variables.OptionText							= Variables.Origs.IMUserSuprvLastNm
															& ", "
															& Variables.Origs.IMUserSuprvFirstNm>
		<!--- Prebuilt OptionText doesn't have IMUserTypDescTxt returned by IMUserSuprvsrSelCSP in callback. (No big deal.) --->
		<cfoutput>
        <option selected value="#Variables.OptionValue#">#Variables.OptionText#</option></cfoutput>
	</cfif>
	<cfoutput>
        </select>
        <span id="SupervisorDropdownCount"></span>
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel">
        <span class="TypeSensitive TypeC" style="display:none;">Supervisor<br/>(Contracting Officer Representative)</span>
        <span class="TypeSensitive TypeS" style="display:none;">SBA Supervisor</span>
        <span class="TypeSensitive TypeU" style="display:none;">Supervisor<br/>(Lender's Authorizing Official)</span>
      </div>
      <div class="tbl_cell formdata manddata nowrap"><!-- Side-by-Side --></cfoutput>
		<cfset Variables.MandOptIMUserSuprvUserId			= "skip"><!--- Per SMM, never show it. --->
		<cfif Variables.PageIsUser>
			<cfset Defaults("IMUserSuprvId",				"SuperTextKey",	"Supervisor's ID",				"opt",	"SupId1")>
			<cfset Defaults("IMUserSuprvFirstNm",			"SuperFirst",	"Supervisor's First Name",		"mand",	"SupFirst")>
			<cfset Defaults("IMUserSuprvMidNm",				"SuperMiddle",	"Supervisor's Middle Name",		"opt",	"SupMid")>
			<cfset Defaults("IMUserSuprvLastNm",			"SuperLast",	"Supervisor's Last Name",		"mand",	"SupLast")>
			<cfset Defaults("IMUserSuprvEmailAdrTxt",		"SuperEMail",	"Supervisor's EMail Address",	"mand",	"SupEMail")>
			<cfset Defaults("IMUserSuprvUserId",			"SuperNumKey",	"Supervisor's ID2",				"skip",	"SupId2")>
		<cfelse>
			<cfset Defaults("IMUserSuprvId",				"SuperTextKey",	"Supervisor's ID",				"view",	"SupId1")>
			<cfset Defaults("IMUserSuprvFirstNm",			"SuperFirst",	"Supervisor's First Name",		"view",	"SupFirst")>
			<cfset Defaults("IMUserSuprvMidNm",				"SuperMiddle",	"Supervisor's Middle Name",		"view",	"SupMid")>
			<cfset Defaults("IMUserSuprvLastNm",			"SuperLast",	"Supervisor's Last Name",		"view",	"SupLast")>
			<cfset Defaults("IMUserSuprvEmailAdrTxt",		"SuperEMail",	"Supervisor's EMail Address",	"view",	"SupEMail")>
			<cfset Defaults("IMUserSuprvUserId",			"SuperNumKey",	"Supervisor's ID2",				"skip",	"SupId2")>
		</cfif>
		<!---
		SMM doesn't want to see "Super" or "Supervisor's" or "S." on the web page labels. But they really should be qualified in the "EngName" key generated by 
		Defaults. That way the JavaScript and val_xxx errors are qualified as referring to supervisor. So the following produces a web page label that's more 
		to Sheri's liking: 
		--->
		<cfloop index="Key" list="SupId1,SupFirst,SupMid,SupLast,SupEMail,SupId2">
			<!--- Label isn't built if the field is skipped. But that's okay, because then it also doesn't need to have a WebPageLabel: --->
			<cfif Variables["MandOpt#Key#"]					is not "skip">
				<cfset Variables["WebPageLabel#Key#"]		= Replace(Replace(Replace(Variables["Label#Key#"], 
															"Supervisor's ", "", "One"), " Name", "", "One"), " Address", "", "One")>
			</cfif>
		</cfloop>
		<cfoutput>
        <div style="background-color: white;">
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupId1#" class="#Variables.MandOptSupId1#data"><cfif Variables.MandOptSupId1 is "view">#Variables.VarValueSupId1#<cfelse>
          <input type="Text" data-type="text" maxlength="15" size="10"
          #Variables.NameIdAndValueSupId1# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupId1)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupId1#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupFirst#" class="#Variables.MandOptSupFirst#data"><cfif Variables.MandOptSupFirst is "view">#Variables.VarValueSupFirst#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupFirst# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupFirst)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupFirst#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupMid#" class="#Variables.MandOptSupMid#data"><cfif Variables.MandOptSupMid is "view">#Variables.VarValueSupMid#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupMid# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupMid)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupMid#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupLast#" class="#Variables.MandOptSupLast#data"><cfif Variables.MandOptSupLast is "view">#Variables.VarValueSupLast#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupLast# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupLast)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupLast#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupEMail#" class="#Variables.MandOptSupEMail#data"><cfif Variables.MandOptSupEMail is "view">#Variables.VarValueSupEMail#<cfelse>
          <input type="Text" data-type="text" maxlength="255" size="30"
          #Variables.NameIdAndValueSupEMail# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupEMail)#', this.value, 'E', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupEMail#)
        </div><!-- /Vertical --><cfif Variables.MandOptSupId2 is not "skip">
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupId2#" class="#Variables.MandOptSupId2#data"><cfif Variables.MandOptSupId2 is "view">#Variables.VarValueSupId2#<cfelse>
          <input type="Text" data-type="text" maxlength="10" size="5"
          #Variables.NameIdAndValueSupId2# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupId2)#', this.value, '9', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupId2#)
        </div><!-- /Vertical --></cfif><cfif								 Variables.MandOptSupId1   is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupId1  #></cfif><cfif Variables.MandOptSupFirst is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupFirst#></cfif><cfif Variables.MandOptSupMid   is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupMid  #></cfif><cfif Variables.MandOptSupLast  is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupLast #></cfif><cfif Variables.MandOptSupEMail is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupEMail#></cfif>
        <input type="Hidden" #Variables.NameIdAndValueSupId2  #>
        </div><!-- /background-color: white; -->
      </div><!-- /formdata, Side-by-Side -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</fieldset><!-- /Supervisor --><br class="TypeSensitive TypeC TypeS TypeU" style="display:none;" />
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#

<!-- User Type Related: -->

<script>

gLSP														= false;
gUserType													= "#Variables.VarValueTypCd#";

$(function() // Shorthand for the document ready event. 
	{
	ShowInfoForUserType();
	});

function ClearAllJobTitles									(pForm)
	{
	var	i, sArray;
	sArray													= pForm.#Variables.VarNameJobTitl#;
	for	(i = 0; i < sArray.length; i++)
		sArray[i].checked									= false;
	}

function DoChooseSupervisor									(pThis)
	{
	var	sArray, sSelIdx;
	sSelIdx													= pThis.selectedIndex;
	if	(sSelIdx > 0)
		{
		sArray												= pThis.options[sSelIdx].value.split("|");
		// The following 12 lines are actually just 6 statements. The overview of each pair of lines is a = b = c; which is laid out as 
		// a =
		// b = c;
		// so that we can visually verify correctness via the vertical alignment of names. <cfif							Variables.MandOptSupId1		is "view">
		document.getElementById("Box#Variables.VarNameSupId1  #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupId1  #").value		= ((sArray.length > 0) ? sArray[0] : "?");<cfif Variables.MandOptSupFirst	is "view">
		document.getElementById("Box#Variables.VarNameSupFirst#").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupFirst#").value		= ((sArray.length > 1) ? sArray[1] : "?");<cfif Variables.MandOptSupMid		is "view">
		document.getElementById("Box#Variables.VarNameSupMid  #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupMid  #").value		= ((sArray.length > 2) ? sArray[2] : "?");<cfif Variables.MandOptSupLast	is "view">
		document.getElementById("Box#Variables.VarNameSupLast #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupLast #").value		= ((sArray.length > 3) ? sArray[3] : "?");<cfif Variables.MandOptSupEMail	is "view">
		document.getElementById("Box#Variables.VarNameSupEMail#").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupEMail#").value		= ((sArray.length > 4) ? sArray[4] : "?");
		document.getElementById(   "#Variables.VarNameSupId2  #").value		= ((sArray.length > 5) ? sArray[5] : "?");
		// Since we got it by lookup, it has to be valid: 
		document.getElementById("SupervisorInvalidMsg").style.display	= "none";
		}
	}

function DoLookupSupervisor									(pThis)
	{
	var	sDropdown, sEMail, sEMailVal, sFirst, sFirstVal, sForm, sHQLoc, sHQLocVal, sLast, sLastVal, sURL;
	sForm													= pThis.form; // Cache frequently used variables. 
	sEMail													= sForm.#Variables.VarNameSupE#;
	sFirst													= sForm.#Variables.VarNameSupF#;
	sHQLoc													= sForm.#Variables.VarNameHQLoc#;
	sLast													= sForm.#Variables.VarNameSupL#;
	sEMailVal												= sEMail.value.replace(gTrimRE, "");
	sFirstVal												= sFirst.value.replace(gTrimRE, "");
	sHQLocVal												= sHQLoc.value.replace(gTrimRE, "");
	sLastVal												= sLast .value.replace(gTrimRE, "");
	if	(gUserType === "U")
		{
		if	(sHQLocVal === "")
			{
			alert("Please enter headquarters location.");
			return;
			}
		} // If User Type U has HQLoc, that's enough to do lookup. 
	else if	((sEMailVal === "") && (sFirstVal === "") && (sLastVal === ""))
		{
		alert("Please enter something in one of the Supervisor fields.");
		return;
		}
	sDropdown												= document.getElementById("SupervisorDropdown");
	sURL													= "#Variables.PageName#?Action=LookupSupervisor"
															+ "&Now=" + (new Date()).getTime() // Cachebuster for IE. 
															+ ((sEMailVal !== "") ? "&EMail="    + escape(sEMailVal) : "")
															+ ((sFirstVal !== "") ? "&First="    + escape(sFirstVal) : "")
															+ ((sHQLocVal !== "") ? "&HQLoc="    + escape(sHQLocVal) : "")
															+ ((sLastVal  !== "") ? "&Last="     + escape(sLastVal)  : "")
															+ ((gUserType !== "") ? "&UserType=" + escape(gUserType) : "");
//	alert("'" + sEMailVal + "', '" + sFirstVal + "', '" + sLastVal + "'.");
	$(sDropdown).load(sURL, function()
		{ // Defined as local ("var") here because first reference is in a nested function. (Some browsers may not recognize closure.) 
		var	sCount											= sDropdown.options.length - 1; // Assures sCount numeric, not in string expression. 
		if		(sCount < 1)
			{
			document.getElementById("SupervisorDropdownCount").innerHTML = "No supervisors found.";
			}
		else if	(sCount === 1)
			{
			sDropdown.selectedIndex							= 1;// Only row of actual data returned. 
			sDropdown.onchange();
			document.getElementById("SupervisorDropdownCount").innerHTML = "Exactly one supervisor found.";
			}
		else if	(sCount > 1)
			{
			document.getElementById("SupervisorDropdownCount").innerHTML = " " + sCount + " supervisors found.";
			}
		});
	}

function DoThisOnChangeHeadquartersLoc						(pThis)
	{
	var	kMaxErrorLen, sForm, sMand, sResponseNum, sResponseText;
	sForm													= pThis.form;
	sMand													= isMand(pThis);
	if	(EditMask("#JSStringFormat(Variables.Eng)#", pThis.value, "9", sMand, 1, pThis.maxLength))
		{
		if	(gUserType === 'U')<!--- Per RDW, do immediate lookup by HQLocId: --->
			{ // The following is a = b = c = "", spread over 3 lines: 
			sForm.#Variables.VarNameSupE#.value				=
			sForm.#Variables.VarNameSupF#.value				=
			sForm.#Variables.VarNameSupL#.value				= "";
			document.getElementById("LookupSupervisorButton").click();
			}
		else
			$.ajax(	{<cfset Variables.URLString				= Variables.PageName
															& "?Action=DetermineLSP"
															& "&Num=#URLEncodedFormat(Variables.EndUserData.IMUserId)#"
															& "&HQLoc=">
					dataType:								"text",<!--- What we expect back. --->
					url:									"#JSStringFormat(Variables.URLString)#" + escape(pThis.value),
					complete:
						function(jqXHR)
							{
							sResponseText					= jqXHR.responseText.replace(gTrimRE, "");
							sResponseNum					= parseInt(sResponseText, 10);
							kMaxErrorLen					= 500; // Don't annoy folks with **TOO** much alert. 
							if	(sResponseNum == NaN)
								alert("When trying to determine whether the #Variables.EngHQLoc# is that of an LSP, "
									+ "got back the following message: \n"
									+ (sResponseText.length > kMaxErrorLen ? sResponseText.substring(0,kMaxErrorLen) : sResponseText)
									);		// Probably a debugging message, so alert it. 
							else
								{
								gLSP						= (sResponseNum > 0);
								$(".JobChk[value=6]").each(function()
									{
									this.checked			= gLSP;
									this.onclick();
									});
								}
							}
					});
		return true;
		}
	this.focus();
	return false;
	}

function DoThisOnChangeUserType								()
	{<cfif ListFindNoCase("skip,view", Variables.MandOptTypCd) is 0><!--- And therefore the dropdown for UserType exists. --->
	var	i, sDropdownUserType, sForm, sOldTypeIdx, sOldTypeName, sOptions, sOptionsLen, sSelIdx, sValueSelected, sWarningMsg;
	// Do the alert(s) here and return out of the function if the user changes their mind about losing roles. 
	sForm													= document.#Variables.FormName#;
	sDropdownUserType										= sForm.#Variables.VarNameTypCd#;
	sOptions												= sDropdownUserType.options;
	sOptionsLen												= sOptions.length;
	sOldTypeIdx												= -1;
	sOldTypeName											= "the old user type";
	for	(i = 0; i < sOptionsLen; i++)
		if	(sOptions[i].value === gUserType)
			{
			sOldTypeIdx										= i;
			sOldTypeName									+= " (" + sOptions[i].text + ")";
			break;
			}
	sSelIdx													= sDropdownUserType.selectedIndex;
	sValueSelected											= ((sSelIdx > 0) ? sOptions[sSelIdx].value : "");
	sWarningMsg												= "";
	switch (sValueSelected)
		{
		case "C": case "S": case "U":
			sWarningMsg										+= "Please make sure the supervisor info is correct for the new user type.\n";
			break;
		default:
		}<cfif Variables.Origs.IMUserId is 0>
	if	(sWarningMsg !== "")
		alert(sWarningMsg);<cfelse>
	sWarningMsg												= "Changing the user type will delete all the existing roles that "
															+ "require SBA authorization and/or "
															+ sOldTypeName
															+ ".\n"
															+ sWarningMsg
															+ "Press OK to continue, or Cancel if that isn't what you wanted to happen.";
	if	(!confirm(sWarningMsg))
		{
		if	(sOldTypeIdx > -1)
			{
			sDropdownUserType.selectedIndex					= sOldTypeIdx;
			return false;
			}
		// The return false above normally prevents the following alert from ever happening. (Just in case.) 
		alert("Unable to restore previously selected User Type. Please cancel by changing back to the old value manually.");
		return false; // Don't set gUserType, etc. 
		}</cfif><!--- /PageIsAddCustomer --->
	gUserType												= sValueSelected;
	ClearAllJobTitles(sForm); // Change of user type implies loss of all job titles, even for the user type just left. <cfelse>
	// Even though we're not allowing a change of UserType, we still want to show all info it controls: </cfif>
	ShowInfoForUserType();
	return true;
	}

function DoValidateSupervisor								()
	{<cfif Variables.PageIsAddCustomer>
	$("##SupervisorInvalidMsg").hide();						// On this page, always. <cfelse>
	var	sResponseText;
	switch (gUserType)
		{
		case "C": // SBA Contractor requres supervisor. 
		case "S": // SBA Employee   requres supervisor. 
		case "U": // Partner        requres supervisor. 
			$.ajax(	{<cfset Variables.URLString				= Variables.PageName
															& "?Action=ValidateSupervisor"
															& "&UserName=#URLEncodedFormat(Variables.Origs.IMUserNm)#"
															& "&UserType=">
					dataType:								"text",<!--- What we expect back. --->
					url:									"#Variables.URLString#" + escape(gUserType),
					complete:
						function(jqXHR)
							{
							sResponseText					= jqXHR.responseText.replace(gTrimRE, "");
							if	(sResponseText.length		> 1)
								{
								alert("When trying to validate supervisor, got back the following message: \n"
									+ sResponseText);		// Probably a debugging message, so alert it. 
								$("##SupervisorInvalidMsg").show();	// Show invalid supervisor message. 
								}
							else if	(sResponseText			=== "A")
								$("##SupervisorInvalidMsg").hide();	// Don't display invalid supervisor message). 
							else
								$("##SupervisorInvalidMsg").show();	// Show invalid supervisor message. 
							}
					});
			break;
		default:
			$("##SupervisorInvalidMsg").hide();						// (User type doesn't require a supervisor.) 
		}</cfif>
	}

function ShowInfoForUserType								()
	{
	$(".TypeSensitive").hide().filter(".Type"+gUserType).show();
	if	($(".JobTitle").hide().filter(".Type"+gUserType).show().length == 0)
		$(".JobTitle.TypeNone").show();
	$(".JobSensitive").hide();
	$("input.JobChk:checked").each(function(){$(".Job"+this.value).show();});
	if	(gUserType === "U")	HiGrpMandatory	(".hqloc");
	else					HiGrpOptional	(".hqloc");
	DoValidateSupervisor();
	}

</script>
<style>

label.offscreen
	{
	position:							absolute;
	left:								-9999px; /* far offscreen so won't mess up sighted users */
	}

</style>
</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		AJAX routines for lookups and validations.
NOTES:

	This file is **NOT** included in dsp_userprofile.cfm's ListFieldsetsAll variable!!
	The last statement in this file is a cfabort!!
	This file should NEVER be cfincluded unless it's an AJAX callback!!

	It's just a separate for ease of maintenance. We're getting it out of the way so that we don't have to see it while
	working on dsp_userprofile. No need to check it out unless one's adding or modifying an AJAX function. Neat. Tidy.

INPUT:				URL.Action. Other URL variables that vary according to URL.Action.
OUTPUT:				Varies according to URL.Action.
REVISION HISTORY:	04/22/2016, NS: 	Added case for "RefreshCaptcha"
					08/21/2015, SRS:	Made DetermineLSP also determine location name, which also implied returning JSON.
					07/30/2015, SRS:	Original implementation.
--->

<!--- ************************************************************************************************************ --->

<!--- Configuration Parameters: --->

<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfset Variables.LibURL										= "/library">
<cfset Variables.LibIncURL									= "#Variables.LibURL#/cfincludes">
<cfif NOT IsDefined("Request.SlafDevTestProd")>
	<cfinclude template="#Variables.LibIncURL#/get_sbashared_variables.cfm">
</cfif>
<cfset Variables.password									= "">
<cfset Variables.username									= "">
<cfset Variables.ValidateSupervisorMaxLen					= 100>

<!--- ************************************************************************************************************ --->

<cfset Variables.ErrMsg										= "">
<cfset Variables.TxnErr										= "No">

<cfswitch expression="#URL.Action#">

<!--- DetermineLSP (deprecated): --->

	<cfcase value="DetermineLSP">
		<cfparam name="URL.HQLoc"							default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfparam name="URL.Num"								default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfset Variables.Obj								=
			{
			"ErrMsg"										= "",
			"LocCount"										= "-1",
			"LocName"										= "Location name not found.",
			"LSPCount"										= "-1"
			}>
		<cfif Len(URL.HQLoc) is 0>
			<cfset Variables.Obj.ErrMsg						= "Error: Headquarters Location missing.">
		<cfelseif NOT IsNumeric(URL.HQLoc)>
			<cfset Variables.Obj.ErrMsg						= "Error: Headquarters Location isn't numeric.">
		<cfelseif NOT IsNumeric(URL.Num)>
			<cfset Variables.Obj.ErrMsg						= "Error: User identifier missing.">
		<cfelse>
			<cfset Variables.HQLocId						= URL.HQLoc>
			<cfif URL.Num is 0>
				<cfset Variables.IMUserId					= "">
			<cfelse>
				<cfset Variables.IMUserId					= URL.Num>
			</cfif>
			<cfset Variables.LSPLocIdCnt					= ""><!--- Initialize output parameter, deliberately making it non-numeric. --->
			<cfset Variables.cfprname						= "NoResultSetReturned">
			<cfset Variables.LogAct							= "determine whether #Variables.HQLocId# is a Lender Service Provider">
			<cfinclude template="/cfincludes/oracle/security/spc_LSPLOCIDSELCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.Obj.ErrMsg					= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& "): "
															& Variables.ErrMsg><!--- Because it contains CFCatch info. --->
				<!--- ... and please tell us the "code" so we can look in the log files!!! --->
			<cfelse>
				<cfset Variables.LSPLocIdCnt				= Trim(Variables.LSPLocIdCnt)>
				<cfif IsNumeric(Variables.LSPLocIdCnt)>
					<cfset Variables.Obj.LSPCount			= Variables.LSPLocIdCnt>
					<cftry>
						<cfquery name="getLocNm"			datasource="#Variables.db#" username="#Variables.username#" password="#Variables.password#">
						select	PrtLocNm
						from	partner.PrtLocTbl
						where	LocId						= <cfqueryparam cfsqltype="cf_sql_integer" value="#URL.HQLoc#">
						</cfquery>
						<cfcatch type="Any">
							<cfset Variables.Obj.ErrMsg		= "An error occurred while trying to retrieve the Headquarters Location name. "
															& "The following information may help: #CFCatch.Message# #CFCatch.Detail#">
							<cfset Variables.TxnErr			= "Yes">
						</cfcatch>
					</cftry>
					<cfif	(NOT Variables.TxnErr)
						and	IsDefined("getLocNm.RecordCount")>
						<cfset Variables.Obj.LocCount		= getLocNm.RecordCount>
						<cfif getLocNm.RecordCount		gt 0>
							<cfset Variables.Obj.LocName	= getLocNm.PrtLocNm><!--- Just the first row if more than one row found. --->
						</cfif>
					</cfif>
				<cfelse>
					<cfset Variables.Obj.ErrMsg				= "Error: The attempt to #Variables.LogAct# did not return a number: '#Variables.LSPLocIdCnt#'.">
				</cfif>
			</cfif>
		</cfif>
		<cfcontent type="text/plain">
		<cfoutput>#SerializeJSON(Variables.Obj)#</cfoutput>
	</cfcase>

<!--- LookupHQLoc (multipurpose equivalent of DetermineLSP and LookupSupervisor combined, which will replace both, but only in HQLocId's onchange): --->

	<cfcase value="LookupHQLoc">
		<cfparam name="URL.HQLoc"							default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfparam name="URL.Num"								default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfparam name="URL.UserType"						default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfset Variables.Obj								=
			{
			"ErrMsg"										= "",
			"LocName"										= "",
			"LocValid"										= "",
			"LSPCount"										= "",
			"SupervisorOptions"								= []
			}>
		<cfif Len(URL.UserType) is 0>
			<cfset Variables.Obj.ErrMsg						= "Error: User Type must have been selected.">
		<cfelseif Len(URL.HQLoc) is 0>
			<cfset Variables.Obj.ErrMsg						= "Error: Headquarters Location missing.">
		<cfelseif NOT IsNumeric(URL.HQLoc)>
			<cfset Variables.Obj.ErrMsg						= "Error: Headquarters Location isn't numeric.">
		<cfelseif NOT IsNumeric(URL.Num)>
			<cfset Variables.Obj.ErrMsg						= "Error: User identifier missing."><!--- Cryptic way of saying "IMUserId missing". --->
		<cfelse>
			<cfset Variables.HQLocId						= URL.HQLoc>
			<cfif URL.Num is 0>
				<cfset Variables.IMUserId					= "">
			<cfelse>
				<cfset Variables.IMUserId					= URL.Num>
			</cfif>
			<cfset Variables.IMUserTypCd					= URL.UserType>
			<cfset Variables.HQLocIdValid					= ""><!--- Initialize output parameter. --->
			<cfset Variables.LSPLocIdCnt					= ""><!--- Initialize output parameter, deliberately making it non-numeric. --->
			<cfset Variables.PrtLocNm						= ""><!--- Initialize output parameter. --->
			<cfset Variables.cfprname						= "getSupers">
			<cfset Variables.LogAct							= "look up Headquarters Location #Variables.HQLocId#">
			<cfinclude template="/cfincludes/oracle/security/spc_HQLOCIDLOOKUPCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.Obj.ErrMsg					= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& "): "
															& Variables.ErrMsg><!--- Because it contains CFCatch info. --->
				<!--- ... and please tell us the "code" so we can look in the log files!!! --->
			<cfelse>
				<cfset Variables.Obj.LocName				= Trim(Variables.PrtLocNm)>
				<cfset Variables.Obj.LocValid				= Trim(Variables.HQLocIdValid)>
				<cfset Variables.Obj.LSPCount				= Trim(Variables.LSPLocIdCnt)>
				<!--- Because we aren't called via $.load(), as LookupSupervisor is, we have to do it COMPLETELY differently (for IE, of course): --->
				<cfset ArrayAppend(Variables.Obj.SupervisorOptions, ["", "Choose Your Supervisor"])>
				<cfloop query="getSupers">
					<cfset Variables.OptionValue			=  "#Trim(getSupers.IMUserNm)#"
															& "|#getSupers.IMUserFirstNm#"
															& "|#getSupers.IMUserMidNm#"
															& "|#getSupers.IMUserLastNm#"
															& "|#getSupers.IMUserEmailAdrTxt#"
															& "|#getSupers.IMUserId#">
					<cfset Variables.OptionText				=  "#getSupers.IMUserLastNm#"
															&", #getSupers.IMUserFirstNm#"
															&" (#getSupers.IMUserTypDescTxt#)">
					<cfset ArrayAppend(Variables.Obj.SupervisorOptions, [Variables.OptionValue, Variables.OptionText])>
				</cfloop>
			</cfif>
		</cfif>
		<cfcontent type="text/plain">
		<cfoutput>#SerializeJSON(Variables.Obj)#</cfoutput>
	</cfcase>

<!--- LookupSupervisor: --->

	<cfcase value="LookupSupervisor">
		<cfparam name="URL.EMail"							default="">
		<cfparam name="URL.First"							default="">
		<cfparam name="URL.HQLoc"							default="">
		<cfparam name="URL.Last"							default="">
		<cfparam name="URL.UserType"						default="">
		<cfset Variables.LookupSupervisorErrMsg				= "">
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)
			and	(Len(URL.UserType)							is 0)><!--- Probably won't ever happen. --->
			<cfset Variables.LookupSupervisorErrMsg			= "Error: User Type not selected.">
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)>
			<cfif			(URL.UserType					is "U")		or 		(URL.UserType					is "A")>
				<!--- IsNumeric already includes a length check, but do them separately for more helpful feedback: --->
				<cfif		(Len(URL.HQLoc)					is 0)>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Headquarters Location missing.">
				<cfelseif	(NOT IsNumeric(URL.HQLoc))>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Headquarters Location is not numeric.">
				</cfif>
			<cfelse>
				<!--- If UserType is not "U" (user is not a partner), HQLoc may *not* participate in the search: --->
				<cfset URL.HQLoc							= "">
				<cfif		(Len(URL.EMail)					is 0)
					and		(Len(URL.First)					is 0)
					and		(Len(URL.Last)					is 0)>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Enter something for E-Mail, First Name or Last Name.">
				</cfif>
			</cfif><!--- /UserType --->
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)>
			<cfset Variables.Identifier						= "0">
			<cfset Variables.IMUserId						= ""><!--- Not used by IMUserSuprvsrSelCSP yet. --->
			<cfset Variables.IMUserTypCd					= URL.UserType>
			<cfset Variables.HQLocId						= URL.HQLoc>
			<cfset Variables.IMUserEmailAdrTxt				= URL.EMail>
			<cfset Variables.IMUserFirstNm					= URL.First>
			<cfset Variables.IMUserLastNm					= URL.Last>
			<cfset Variables.cfprname						= "getSupers">
			<cfset Variables.ErrMsg							= "">
			<cfset Variables.LogAct							= "get supervisors by first name, last name and/or email">
			<cfset Variables.TxnErr							= "No">
			<!--- Not using dbutils_for_dbtype here, else this would be spcn('IMUserSuprvsrSelCSP', 'security') --->
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERSUPRVSRSELCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.LookupSupervisorErrMsg		= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ").">
				<!--- ... and please tell us the "code" so we can look in the log files!!! --->
			</cfif>
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)
			and	(getSupers.RecordCount						is 0)>
			<cfset Variables.LookupSupervisorErrMsg			= "Nothing found. (Please try again.)">
		</cfif>
		<!--- Don't forget, this Variables.Options code is also done in LookupHQLoc, above! --->
		<cfsavecontent variable="Variables.Options">
			<cfif Len(Variables.LookupSupervisorErrMsg)		is 0>
				<cfoutput>
		<option value="">Choose Your Supervisor</option></cfoutput>
				<cfloop query="getSupers">
					<cfset Variables.OptionValue			=  "#Trim(getSupers.IMUserNm)#"
															& "|#getSupers.IMUserFirstNm#"
															& "|#getSupers.IMUserMidNm#"
															& "|#getSupers.IMUserLastNm#"
															& "|#getSupers.IMUserEmailAdrTxt#"
															& "|#getSupers.IMUserId#">
					<cfset Variables.OptionText				= "#getSupers.IMUserLastNm#"
															& ", #getSupers.IMUserFirstNm#"
															& " (#getSupers.IMUserTypDescTxt#)">
					<cfoutput>
		<option value="#Variables.OptionValue#">#Variables.OptionText#</option></cfoutput>
				</cfloop>
			<cfelse>
				<cfoutput>
		<option value="">#Variables.LookupSupervisorErrMsg#</option></cfoutput>
			</cfif>
		</cfsavecontent>
		<cfcontent type="text/html">
		<cfoutput>#Variables.Options#</cfoutput>
	</cfcase>
<!--- Replace old CAPTCHA image with new one: --->
	<cfcase value="RefreshCaptcha">
		<cfif NOT IsDefined("PasswordGenerate")>
			<cfinclude template="#Variables.LibUdfURL#/bld_PasswordUDFs.cfm">
		</cfif>
		<cfset PasswordAvoidConfusingCharacters()>
		<cfif NOT IsDefined("LoadOrig")><!--- Probably defined in caller, but BldOrigs_SelfTestMode needs it too. --->
			<cfinclude template="/library/udf/bld_DspPageLoadTimeOrigsUDFs.cfm">
		</cfif>
		<cfset Variables.VerifyDifficulty							= "medium">
		<cfset Variables.VerifyLength								= RandRange(5,5)>
		<cfset Variables.VerifyStrength								= "clsweak">
		<cfset LoadOrig("Verify",Left( PasswordGenerate(Variables.VerifyStrength),Variables.VerifyLength) )>

		<cflock scope="Session" type="Exclusive" timeout="30">
		<!--- Don't use PutDspPageLoadTimeOrigs. That would trash original load-time values when PrevErr=Y. --->
			<cfset Session.DspPageLoadTimeOrigs.Verify				= Variables.Origs.Verify>
		</cflock>
		<cfsavecontent variable="Variables.NewCaptchaImg">
			<cfoutput>
				<cfimage action="captcha" height="40" width="250" text="#Variables.Verify#" difficulty="#Variables.VerifyDifficulty#"><br/>
			</cfoutput>
		</cfsavecontent>
		<cfcontent type="text/html">
		<cfoutput>#Variables.NewCaptchaImg#</cfoutput>
	</cfcase>

<!--- ValidateSupervisor: --->

	<cfcase value="ValidateSupervisor">
		<cfparam name="URL.UserName"						default="">
		<cfparam name="URL.UserType"						default="">
		<cfif		Len(URL.UserType)						is 0>
			<cfset Variables.SuprvInd						= "A"><!--- Supervisor AOk, because UserType "" doesn't need one. --->
		<cfelseif	ListFindNoCase("C,S,U", URL.UserType)	is 0>
			<cfset Variables.SuprvInd						= "A"><!--- Supervisor AOk, because UserType doesn't need one. --->
		<cfelseif	Len(URL.UserName)						is 0>
			<cfset Variables.SuprvInd						= "I"><!--- Supervisor invalid, probably because in addcustomer. --->
		<cfelse>
			<cfset Variables.IMUserNm						= URL.UserName>
			<cfset Variables.IMUserTypCd					= URL.UserType>
			<cfset Variables.SuprvInd						= ""><!--- Output parameter of ValidateIMUserSuprvCSP --->
			<cfset Variables.cfprname						= "Ignored">
			<cfset Variables.ErrMsg							= "">
			<cfset Variables.LogAct							= "validate supervisor (required for new User Type)">
			<cfset Variables.TxnErr							= "No">
			<!--- Not using dbutils_for_dbtype here, else this would be spcn('ValidateIMUserSuprvCSP', 'security') --->
			<cfinclude template="/cfincludes/oracle/security/spc_VALIDATEIMUSERSUPRVCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SuprvInd					= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ")."><!--- Treated as invalid, allows debug. --->
			<cfelseif Len(Variables.SuprvInd) is 0>
				<cfset Variables.SuprvInd					= "I"><!--- Supervisor invalid, probably because in addcustomer. --->
			<cfelseif ListFind("A,I", Variables.SuprvInd)	is 0>
				<!--- Try to avoid scaring the user, but still, try to see as much of p_SuprvInd as "MaxLen" allows: --->
				<cfif Len(Variables.SuprvInd)				gt Variables.ValidateSupervisorMaxLen>
					<cfset Variables.SuprvInd				= Left(Variables.SuprvInd, Variables.ValidateSupervisorMaxLen)>
				</cfif>
				<cfset Variables.SuprvInd					= "Error: An invalid lookup occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ", '#Variables.SuprvInd#')."><!--- Treated as invalid. --->
			<cfelse>
				<!---
				We sometimes get trailing space appended. Trim in case it helps, but usually it doesn't. (Apparently,
				jQuery's AJAX is appending it to jqXHR.responseText. So we have to trim it on the client too.) On the
				other hand, it doesn't **hurt** to trim it on the server too:
				--->
				<cfset Variables.SuprvInd					= Trim(Variables.SuprvInd)>
				<!--- Stored procedure's output p_SuprvInd was "A", "I" or "", so let that stand as our response. --->
			</cfif>
		</cfif>
		<cfcontent type="text/plain">
		<cfoutput>#Variables.SuprvInd#</cfoutput>
	</cfcase>

</cfswitch>

<!--- Shared return to the caller: --->

<cfsetting showdebugoutput="No">
<cfabort>

<!--- ************************************************************************************************************ --->

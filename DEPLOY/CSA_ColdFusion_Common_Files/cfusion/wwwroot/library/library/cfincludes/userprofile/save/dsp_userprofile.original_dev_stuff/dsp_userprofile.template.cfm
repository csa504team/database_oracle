<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/13/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:

	In the old /library/cfincludes/userprofile directory, there were separate files (to be included separately) even though 
	they were the same group. Example: bld_readonly_confirm, dsp_formdata_confirm and dsp_onsubmit_confirm, all of the same 
	group (confirm), but included separately. This made maintenance more difficult. 

	In the new code sharing strategy, all aspects of a group are coded together, in the same file. So if a group isn't in the 
	calling page's ListGroups, it doesn't get included. No processing and no memory impact, but with convenient maintenance. 

INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/13/2015, SRS:	Original implementation. Cannibalized code from /library/cfincludes/userprofile. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

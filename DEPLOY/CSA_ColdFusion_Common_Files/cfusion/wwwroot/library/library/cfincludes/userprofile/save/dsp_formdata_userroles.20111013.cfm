<!---
AUTHOR:				Dileep Chanda
DATE:				05/12/2006
DESCRIPTION:		Form data for User roles. 
NOTES:

	Made cfincludable as this code is used in /cls and /security. Should always be cfincluded AFTER get_userroles.cfm. 
	Currently generates 4 columns. 
	(1)		System Folder
	(2)		Role Checkbox
	(3)		Role Name
	(4)		Role Pending Approval message
	(3-4)	Privilege Checkbox and Name
	(3-4)	Required Data and nested table(s)

	However, because the caller typically puts out their own header, we let the caller set Variables.TotalColumns to 4. 

INPUT:				User info, getRoles, getPrivileges, getBusinesses, ArrayRoleOfcCds, ArrayRoleLocIds, ArrayRoleBusInfo. 
OUTPUT:				Form data for User roles. 
REVISION HISTORY:	10/13/2011, AJC:	Added support for new role restriction 'M'.
					10/04/2010, AJC:	Added support for new role restriction 'S'.
					02/03/2010, SRS:	Added an onclick handler to the system name, so that clicking the name calls the 
										same JavaScript as clicking on the system's folder icon. (Section 508 issue.) 
					01/08/2010, SRS:	Changes to support notifying the SBA supervisor when an SBA employee or contractor 
										requests a role, so that the supervisor has an opportunity to approve or decline 
										the role request. Simplified code considerably using jQuery. 
					12/27/2007, DKC:	Added check to see if there is an aged-out request for the same user/role 
										and display a warning message.
					10/17/2007, DKC:	Added support for the new "partner" type user.
					10/26/2006, DKC:	Added new role restriction 'T', for hubzone application.
					07/18/2006, DKC:	Modified the code to allow only one role per system per person if the 
										indicator IMAllowMultiRoleInd IS "N".
					05/12/2006, DKC:	Original Implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.SpacingRqrdData								= RepeatString("&nbsp;", 10)>

<!--- Think of getRoles2 as "getRoles, alphabetized": --->

<cfset Variables.UserIsRequestingOwnAccess						= (Left(CGI.Script_Name, 5) IS "/cls/")>
<cfif Variables.UserIsRequestingOwnAccess>
	<!--- When used within the gls directory, also remove roles that a user is not allowed to request: --->
	<cfquery name="getRoles2" dbtype="query">
	select		*
	from		getRoles
	where		IMAppRoleAccsRqstAllowInd						= 'Y'
	order by	IMAppSysTypDescTxt,
				IMAppRoleDescTxt
	</cfquery>
<cfelse>
	<cfquery name="getRoles2" dbtype="query">
	select		*
	from		getRoles
	order by	IMAppSysTypDescTxt,
				IMAppRoleDescTxt
	</cfquery>
</cfif>

<!--- Accumulate all info that needs to be aggregated at the system level into only one extra pass through getRoles2: --->

<cfset Variables.IMAppSysTypCd									= "">
<cfset Variables.ListRolesWithRqrdData							= "">
<cfloop query="getRoles2">
	<cfif getRoles2.IMAppSysTypCd IS NOT Variables.IMAppSysTypCd>
		<cfset Variables.IMAppSysTypCd							= getRoles2.IMAppSysTypCd>
		<cfset Variables.SysStruct								= StructNew()>
		<cfset "Variables.SysStruct#Variables.IMAppSysTypCd#"	= Variables.SysStruct>
		<cfset Variables.SysStruct.FirstRqstdRole				= "">
		<cfset Variables.SysStruct.OpenFolder					= "No">
		<cfset Variables.SysStruct.RoleCount					= 0>
		<cfset Variables.SysStruct.UserHasRoles					= "No">
	</cfif>
	<cfset Variables.SysStruct.RoleCount						= Variables.SysStruct.RoleCount + 1>
	<cfif	(getRoles2.IMUserRoleExistInd						IS "Y")>
		<cfset Variables.SysStruct.OpenFolder					= "Yes">
		<cfset Variables.SysStruct.UserHasRoles					= "Yes">
	</cfif>
	<cfif	(getRoles2.IMUserRoleRqstInd						IS "Y")>
		<cfset Variables.SysStruct.OpenFolder					= "Yes">
		<cfif Len(Variables.SysStruct.FirstRqstdRole) IS 0>
			<cfset Variables.SysStruct.FirstRqstdRole			= getRoles2.IMAppRoleId>
		</cfif>
	</cfif>
	<cfif	(Len(getRoles2.IMRqrdDataTypCd)						GT 0)>
		<cfif	(ListFind(Variables.ListRolesWithRqrdData,		getRoles2.IMAppRoleId) IS 0)>
			<cfset Variables.ListRolesWithRqrdData				= ListAppend(Variables.ListRolesWithRqrdData,
																getRoles2.IMAppRoleId)>
		</cfif>
	</cfif>
</cfloop>

<!--- Accumulate all info that needs to be aggregated at the role level into only one extra pass through getPrivileges: --->

<cfset Variables.ListRolesWithPrivs								= "">
<cfset Variables.ListRolesWithPrivsChecked						= "">
<cfset Variables.ListRolesWithPrivsOrRqrdData					= Variables.ListRolesWithRqrdData>
<cfloop query="getPrivileges">
	<cfif	(ListFind(Variables.ListRolesWithPrivs,				getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivs						= ListAppend(Variables.ListRolesWithPrivs,
																getPrivileges.IMAppRoleId)>
	</cfif>
	<!--- Test the less-likely condition first: --->
	<cfif	(getPrivileges.IMUserRolePrvlgExistInd				IS "Y")
		AND	(ListFind(Variables.ListRolesWithPrivsChecked,		getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivsChecked				= ListAppend(Variables.ListRolesWithPrivsChecked,
																getPrivileges.IMAppRoleId)>
	</cfif>
	<cfif	(ListFind(Variables.ListRolesWithPrivsOrRqrdData,	getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivsOrRqrdData			= ListAppend(Variables.ListRolesWithPrivsOrRqrdData,
																getPrivileges.IMAppRoleId)>
	</cfif>
</cfloop>

<!--- If the user is an SBA employee, retrieve the user's manager name: --->

<cfinclude template="/cls/cfincludes/dsp_formdata_ldap.cfm">

<!--- Build the display: --->

<cfset Variables.HiGrpIdx										= 0>
<cfset Variables.IMAppRoleDescTxt								= "">
<cfset Variables.IMAppSysTypCd									= "">
<cfset Variables.IMAppSysTypDescTxt								= "">
<cfset Variables.TBodyIsOpen									= "No">

<cfoutput>
<input type="Hidden" name= "IMUserId"               value="#Variables.IMUserId#">
<input type="Hidden" name= "IMUserTypCd"            value="#Variables.IMUserTypCd#">
<input type="Hidden" name= "IMUserFirstNm"          value="#Variables.IMUserFirstNm#">
<input type="Hidden" name= "IMUserLastNm"           value="#Variables.IMUserLastNm#">
<input type="Hidden" name= "IMUserPrtNm"            value="#Variables.IMUserPrtNm#">
<input type="Hidden" name= "IMPOUserId"             value="#Variables.IMPOUserId#">
<input type="Hidden" name= "Identifier"             value="#Variables.Identifier#"></cfoutput>

<!--- Security System has already displayed its own header --->

<cfloop query="getRoles2">
	<cfif getRoles2.IMAppSysTypCd IS NOT Variables.IMAppSysTypCd>
		<!--- Within this loop, we use tbody to hold all roles associated with a system: --->
		<cfif Variables.TBodyIsOpen>
			<cfoutput>
</tbody></cfoutput>
		</cfif>
		<cfset Variables.TBodyIsOpen							= "No">
		<cfset Variables.IMAppSysTypCd							= getRoles2.IMAppSysTypCd>
		<cfset Variables.SysStruct								= Evaluate("Variables.SysStruct#Variables.IMAppSysTypCd#")>
		<cfif Variables.SysStruct.OpenFolder>
			<cfset Variables.DisplayRoles						= ""><!--- Error in Mozilla, but displays correctly. --->
			<cfset Variables.ImgURL								= "#Variables.ImagesLibURL#/sbatree/ftv2folderopen.gif">
		<cfelse>
			<cfset Variables.DisplayRoles						= "none">
			<cfset Variables.ImgURL								= "#Variables.ImagesLibURL#/sbatree/ftv2folderclosed.gif">
		</cfif>
		<cfset Variables.SystemComment							= "">
		<cfif	(getRoles2.IMAllowMultiRoleInd IS "N")
			AND	(Variables.SysStruct.RoleCount GT 1)>
			<cfset Variables.SystemComment						= "&nbsp;(<u><em>"
																& "You can select only one role for this system."
																& "</em></u>)">
		</cfif>
		<!--- System Folder: --->
		<cfoutput>
<tbody>
	<tr><td colspan="#Variables.TotalColumns#">&nbsp;</td></tr><!--- Precede each system folder with a blank line. --->
	<tr>
		<td><!--- Table column of System Folder icon. Only time it ever contains anything. --->
			<img src="#Variables.ImgURL#" alt="Open/Close" width="24" height="22" border="0"
				name="Img#getRoles2.IMAppSysTypCd#" onclick="ToggleSystem('#getRoles2.IMAppSysTypCd#');">
		</td>
		<td colspan="#Variables.TotalColumnsMinus1#" class="optlabel">
			&nbsp;<span onclick="ToggleSystem('#getRoles2.IMAppSysTypCd#');">#getRoles2.IMAppSysTypDescTxt#</span><cfif Len(Variables.SystemComment) GT 0>
			#Variables.SystemComment#</cfif>
		</td>
	</tr>
</tbody>
<tbody id="RolesFor#getRoles2.IMAppSysTypCd#" style="display:#Variables.DisplayRoles#;"></cfoutput>
		<cfset Variables.TBodyIsOpen							= "Yes">
	</cfif>
	<cfif Len(getRoles2.IMAppSysPOEmailAdrTxt) GT 0>
		<cfset Variables.IMAppSysPOEmailAdrTxt					= getRoles2.IMAppSysPOEmailAdrTxt>
	<cfelse>
		<cfset Variables.IMAppSysPOEmailAdrTxt					= "sheri.mcconville@sba.gov">
	</cfif>
	<cfset Variables.Disabled									= "        ">
	<cfset Variables.Checked									= "       ">
	<cfset Variables.DispRqrdData								= "none">
	<cfif getRoles2.IMUserRoleExistInd IS "Y">
		<cfset Variables.Checked								= "checked">
		<cfset Variables.DispRqrdData							= ""><!--- Error in Mozilla, but displays correctly. --->
	</cfif>
	<cfif Len(getRoles2.IMRqrdDataTypCd) GT 0>
		<cfset Variables.IMRqrdDataTypCd						= getRoles2.IMRqrdDataTypCd>
		<!---
		Per Ron Whalen on 2010-01-15, if a role has required data, show its required data form elements, even if that 
		role has not been requested yet. Of course, in that case, the required data form elements will be empty: 
		--->
		<cfset Variables.DispRqrdData							= ""><!--- Error in Mozilla, but displays correctly. --->
	<cfelse>
		<!--- 'N' stands for null. We need some value in here other than empty string as we do ListtoArray in the act page --->
		<cfset Variables.IMRqrdDataTypCd						= "N">
	</cfif>
	<cfif getRoles2.IMUserRoleRqstInd IS "Y">
		<cfset Variables.Disabled								= "disabled">
	</cfif>
	<!---
	In ToggleRole, we change the column names as they appear to the user, so as not to reveal too much about our 
	database names. To make the code more readable, use the same names here, even though it's server-side: 
	--->
	<cfif	Variables.UserIsRequestingOwnAccess
		AND	(getRoles2.IMAppRoleSBAAuthRqrdInd					IS "Y")>
		<cfset Variables.ShowAuthMsg							= "true">
	<cfelse>
		<cfset Variables.ShowAuthMsg							= "false">
	</cfif>
	<cfset Variables.CallUncheckAll								= "No">
	<cfset Variables.MaySeeThisRole								= "No">
	<cfset Variables.PrevRequests								= 0>
	<cfset Variables.ShowHideHotlink							= "">
	<cfif getRoles2.IMAllowMultiRoleInd IS "N">
		<cfif	(Len(Variables.SysStruct.FirstRqstdRole) IS 0)
			OR	(Variables.SysStruct.FirstRqstdRole IS getRoles2.IMAppRoleId)>
			<cfset Variables.CallUncheckAll						= "Yes">
			<cfset Variables.MaySeeThisRole						= "Yes">
			<!--- For some reason, only single-role systems get the previous request count message: --->
			<cfset Variables.PrevRequests						= getRoles2.UserRoleRqstCount>
		</cfif>
	<cfelse>
		<cfset Variables.MaySeeThisRole							= "Yes">
	</cfif>
	<cfif ListFind(Variables.ListRolesWithPrivsOrRqrdData, getRoles2.IMAppRoleId) GT 0>
		<cfset Variables.ShowHideHotlink						= " (<a href=""javascript:TogglePrivsAndRqrdDataRows"
																& "(#getRoles2.IMAppRoleId#);"">Show/Hide</a>)">
	</cfif>
	<cfif Variables.MaySeeThisRole>
		<!--- Role Checkbox: --->
		<cfoutput>
	<tr>
		<td>&nbsp;</td><!--- Skip table column of System Folder. --->
		<td>
			<input type="Hidden" name="IMAppRoleId"             value="#getRoles2.IMAppRoleId#">
			<input type="Hidden" name="IMAppRoleDescTxt"        value="#Replace(getRoles2.IMAppRoleDescTxt,		',','|','ALL')#">
			<input type="Hidden" name="IMAppSysTypDescTxt"      value="#Replace(getRoles2.IMAppSysTypDescTxt,	',','|','ALL')#">
			<input type="Hidden" name="IMAppRoleSBAAuthRqrdInd" value="#getRoles2.IMAppRoleSBAAuthRqrdInd#">
			<input type="Hidden" name="IMRqrdDataTypCd"         value="#Variables.IMRqrdDataTypCd#">
			<input type="Hidden" name="IMAppSysPOEmailAdrTxt"   value="#Variables.IMAppSysPOEmailAdrTxt#">
			<input type="Hidden" name="RoleExistInd_#getRoles2.IMAppRoleId#" value="#getRoles2.IMUserRoleExistInd#">
			<input type="Hidden" name="OldRoleRqst_#getRoles2.IMAppRoleId#"  value="#getRoles2.IMUserRoleRqstInd#">
			<input type="Checkbox" name="RoleRqst_#getRoles2.IMAppRoleId#" id="RoleRqst_#getRoles2.IMAppRoleId#"
			data-rolenumber="#getRoles2.IMAppRoleId#" data-sysnumber="#getRoles2.IMAppSysTypCd#" #Variables.Checked# #Variables.Disabled#
			title="#getRoles2.IMAppRoleDescTxt#" value="Y" onclick="<cfif Variables.CallUncheckAll>
			var	sSaveThisDotChecked	= this.checked
			UncheckAll(#getRoles2.IMAppSysTypCd#);
			this.checked			= sSaveThisDotChecked;</cfif>
			ToggleRole(this, #Variables.PrevRequests#, #Variables.ShowAuthMsg#);
			">
		</td>
		<td class="optlabel" nowrap>
			<label for="RoleRqst_#getRoles2.IMAppRoleId#">
				#getRoles2.IMAppRoleDescTxt#
			</label>
			#Variables.ShowHideHotlink#
		</td>
		<td class="optlabel"><cfif getRoles2.IMUserRoleRqstInd IS "Y">
			(Request for this role is pending approval. 
			<a target="_blank" href="/cls/dsp_pending_approvals.alias.cfm">Show all</a>
			in new window.)<cfelse>
			&nbsp;</cfif>
		</td>
	</tr></cfoutput>
		<cfset Variables.IMAppRoleId							= getRoles2.IMAppRoleId>
		<cfif ListFind(Variables.ListRolesWithPrivs, getRoles2.IMAppRoleId) GT 0>
			<cfif ListFind(Variables.ListRolesWithPrivsChecked, getRoles2.IMAppRoleId) GT 0>
				<cfset Variables.DispPrvlgs						= ""><!--- Error in Mozilla, but displays correctly. --->
			<cfelse>
				<cfset Variables.DispPrvlgs						= "none">
			</cfif>
			<cfloop query="getPrivileges">
				<cfif getPrivileges.IMAppRoleId IS Variables.IMAppRoleId>
					<cfif getPrivileges.IMUserRolePrvlgExistInd IS "Y">
						<cfset Variables.Checked				= "checked">
					<cfelse>
						<cfset Variables.Checked				= "       ">
					</cfif>
					<!--- Privileges: --->
					<cfoutput>
	<tr data-rolenumber="#Variables.IMAppRoleId#" style="display:#Variables.DispPrvlgs#;">
		<td colspan="2">&nbsp;</td><!--- Skip table columns of System Folder and Role Checkbox. --->
		<td colspan="2" nowrap valign="top">
			<input type="Hidden"   name="IMSysPrvlgCd_#getPrivileges.IMAppRoleId#" value="#getPrivileges.IMSysPrvlgCd#">
			<input type="Hidden"   name="PrvlgExistInd_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#" value="#getPrivileges.IMUserRolePrvlgExistInd#">
			<input type="Checkbox" name="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#"
			id="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#" #Variables.Checked# value="Y">
			<label class="optlabel" for="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#">
				#getPrivileges.IMSysPrvlgDescTxt#
			</label>
		</td>
	</tr></cfoutput>
				</cfif>
			</cfloop>
		</cfif>
		<!--- "RqrdData" (only one of these): --->
		<cfswitch expression="#getRoles2.IMRqrdDataTypCd#">
		<cfcase value="O">
			<cfset RowIdx										= 0>
			<cfoutput>
	<tr data-rolenumber="#Variables.IMAppRoleId#" style="display:#Variables.DispRqrdData#;">
		<td colspan="2">&nbsp;</td><!--- Skip table columns of System Folder and Role Checkbox. --->
		<td colspan="2" nowrap valign="top">
			<label class="mandlabel" for="RoleOffice">
				Office Code
			</label>
			#Variables.SpacingRqrdData#
			<table class="inlinetable" summary="You must enter at least one SBA Office Code"></cfoutput>
			<cfloop index="RoleOfcIdx" from="1" to="#ArrayLen(Variables.ArrayRoleOfcCds)#">
				<cfif Variables.ArrayRoleOfcCds[RoleOfcIdx][1] EQ getRoles2.IMAppRoleId>
					<cfset RowIdx								= RowIdx + 1>
					<cfoutput><cfif (RowIdx Mod 3) IS 1>
			<tr></cfif>
				<td><cfset Variables.HiGrpIdx					= Variables.HiGrpIdx + 1>
					<table class="disableddata hibox grp#Variables.HiGrpIdx#"><tr><td>
					<input type="Text" name="RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#" 
					class="hielt grp#Variables.HiGrpIdx#" disabled value="#Variables.ArrayRoleOfcCds[RoleOfcIdx][2]#" 
					size="4" maxlength="4" onchange="
					if	(EditMask('SBA Office Code', this.value, '9', 1, 4, 4))
						return true;	// Do not redisable, or else the value won't be submitted with the form. 
					this.focus();
					return false;
					">
					<input type="Hidden" name="OldRoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#" value="#Variables.ArrayRoleOfcCds[RoleOfcIdx][2]#">
					<input type="Hidden" name="OfcRowIdx_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#" value="#RowIdx#">
					</td></tr></table>
				</td>
				<td>
					<a href="javascript:HiGrpEnable('.grp#Variables.HiGrpIdx#');HiGrpMandatory('.grp#Variables.HiGrpIdx#');">
						<img src="#Variables.LibURL#/images/edit_icon.gif" width="20" height="20" border="0"
						alt="Edit Office Code" title="Edit Office Code">
					</a>
				</td>
				<td class="optlabel">
					<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_ofccd.cfm?FField=RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#"
					>Look Up</a>
				</td><cfif (RowIdx Mod 3) IS 0>
			</tr></cfif></cfoutput>
				</cfif>
			</cfloop>
			<cfif getRoles2.IMAllowMultiOfcInd IS "Y" OR RowIdx IS 0>
				<cfset Variables.NoOfEmptyOfcsHold	= Variables.NoOfEmptyOfcs>
				<cfif getRoles2.IMAllowMultiOfcInd IS "Y">
					<cfset Variables.NoOfEmptyOfcs				= IIf(	Variables.RowIdx LT Variables.NoOfEmptyOfcsHold, 
																		Variables.NoOfEmptyOfcsHold - Variables.RowIdx,
																		3)>
				<cfelse>
					<cfset Variables.NoOfEmptyOfcs				= 1>
				</cfif>
				<cfloop index="EmptyOfcs" from="1" to="#Variables.NoOfEmptyOfcs#">
					<cfset RowIdx		= RowIdx + 1>
					<cfoutput><cfif (RowIdx Mod 3) IS 1>
			<tr></cfif>
				<td>
					<table class="manddata"><tr><td>
					<input type="Text" name="RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#" size="4" maxlength="4" onchange="
					if	(EditMask('SBA Office Code', this.value, '9', 1, 4, 4))
						return true;
					this.focus();
					return false;
					">
					<input type="Hidden" name="OfcRowIdx_#getRoles2.IMAppRoleId#" value="#RowIdx#">
					</td></tr></table>
				</td>
				<td>
					<img src="#Variables.LibURL#/images/add_icon.gif" width="20" height="20" border="0"
					alt="Add Office Code" title="Add Office Code">
				</td>
				<td class="optlabel">
					<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_ofccd.cfm?FField=RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#"
					>Look Up</a>
				</td><cfif (RowIdx Mod 3) IS 0>
			</tr></cfif></cfoutput>
				</cfloop>
				<cfset Variables.NoOfEmptyOfcs					= Variables.NoOfEmptyOfcsHold>
			</cfif>
			<cfoutput>
			</td></tr></table>
		</td>
	</tr></cfoutput>
		</cfcase>
		<cfcase value="L">
			<cfset RowIdx										= 0>
			<cfoutput>
	<tr data-rolenumber="#Variables.IMAppRoleId#" style="display:#Variables.DispRqrdData#;">
		<td colspan="2">&nbsp;</td><!--- Skip table columns of System Folder and Role Checkbox. --->
		<td colspan="2" nowrap valign="top">
			<label for="RoleLocation" class="mandlabel">
				Location Id
			</label>
			#Variables.SpacingRqrdData#
			<table class="inlinetable" summary="You must enter at least one Location Id"></cfoutput>
			<cfloop index="RoleLocIdx" from="1" to="#ArrayLen(Variables.ArrayRoleLocIds)#">
				<cfif Variables.ArrayRoleLocIds[RoleLocIdx][1] EQ getRoles2.IMAppRoleId>
					<cfset RowIdx								= RowIdx + 1>
					<cfoutput><cfif (RowIdx Mod 3) IS 1>
			<tr></cfif>
				<td><cfset Variables.HiGrpIdx					= Variables.HiGrpIdx + 1>
					<table class="disableddata hibox grp#Variables.HiGrpIdx#"><tr><td>
					<input type="Text"   name="RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
					class="hielt grp#Variables.HiGrpIdx#" disabled value="#Variables.ArrayRoleLocIds[RoleLocIdx][2]#"
					size="7" maxlength="7" onchange="
					if	(EditMask('Location Id', this.value, '9', 1, 1, 7))
						return true;	// Do not redisable, or else the value won't be submitted with the form. 
					this.focus();
					return false;
					">
					<input type="Hidden" name="OldRoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#" value="#Variables.ArrayRoleLocIds[RoleLocIdx][2]#">
					<input type="Hidden" name="LocRowIdx_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#" value="#RowIdx#">
					</td></tr></table>
				</td>
				<td>
					<a href="javascript:HiGrpEnable('.grp#Variables.HiGrpIdx#'); HiGrpMandatory('.grp#Variables.HiGrpIdx#');">
						<img src="#Variables.LibURL#/images/edit_icon.gif" width="20" height="20" border="0"
						alt="Edit Location Id" title="Edit Location Id">
					</a>
				</td>
				<td class="optlabel">
					<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_lend.cfm?FField=RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
					>Look Up</a>
				</td><cfif (RowIdx Mod 3) IS 0>
			</tr></cfif></cfoutput>
				</cfif>
			</cfloop>
			<cfif getRoles2.IMAllowMultiLocInd IS "Y" OR Variables.RowIdx IS 0>
				<cfset Variables.NoOfEmptyLocsHold				= Variables.NoOfEmptyLocs>
				<cfif getRoles2.IMAllowMultiLocInd IS "Y">
					<cfset Variables.NoOfEmptyLocs				= IIf(	Variables.RowIdx LT Variables.NoOfEmptyLocsHold, 
																		Variables.NoOfEmptyLocsHold - Variables.RowIdx,
																		3)>
				<cfelse>
					<cfset Variables.NoOfEmptyLocs				= 1>
				</cfif>
				<cfloop index="EmptyLocs" from="1" to="#Variables.NoOfEmptyLocs#">
					<cfset RowIdx		= RowIdx + 1>
					<cfoutput><cfif (RowIdx Mod 3) IS 1>
			<tr></cfif>
				<td>
					<table class="manddata"><tr><td>
					<input type="Text" name="RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#" size="7" maxlength="7" onchange="
					if	(EditMask('Location Id', this.value, '9', 1, 1, 7))
						return true;
					this.focus();
					return false;
					">
					<input type="Hidden" name="LocRowIdx_#getRoles2.IMAppRoleId#" value="#RowIdx#">
					</td></tr></table>
				</td>
				<td>
					<img src="#Variables.LibURL#/images/add_icon.gif" width="20" height="20" border="0"
					alt="Add Location Id">
				</td>
				<td class="optlabel">
					<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_lend.cfm?FField=RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#"
					>Look Up</a>
				</td><cfif (RowIdx Mod 3) IS 0>
			</tr></cfif></cfoutput>
				</cfloop>
				<cfset Variables.NoOfEmptyLocs					= Variables.NoOfEmptyLocsHold>
			</cfif>
			<cfoutput>
			</td></tr></table>
		</td>
	</tr></cfoutput>
		</cfcase>
		<cfcase value="B,T,M">
			<cfset Variables.TmpIMUserBusSeqNmb		= "">
			<cfloop index="RoleBusIdx" from="1" to="#ArrayLen(Variables.ArrayRoleBusInfo)#">
				<cfif Variables.ArrayRoleBusInfo[RoleBusIdx][1] EQ getRoles2.IMAppRoleId>
					<cfoutput>
			<input type="Hidden" name="OldRoleBus_#getRoles2.IMAppRoleId#" value="#Variables.ArrayRoleBusInfo[RoleBusIdx][2]#"></cfoutput>
					<cfset Variables.TmpIMUserBusSeqNmb			= Variables.ArrayRoleBusInfo[RoleBusIdx][2]>
				</cfif>
			</cfloop>
			<cfoutput>
	<tr data-rolenumber="#Variables.IMAppRoleId#" style="display:#Variables.DispRqrdData#;">
		<td colspan="2">&nbsp;</td><!--- Skip table columns of System Folder and Role Checkbox. --->
		<td colspan="2" nowrap valign="top">
			<label class="mandlabel" for="RoleBus_#getRoles2.IMAppRoleId#">
				Business
			</label>
			#Variables.SpacingRqrdData#
			<table class="manddata"><tr><td><!--- Note, manddata is intrinsically inlinetable. --->
			<select name="RoleBus_#getRoles2.IMAppRoleId#"></cfoutput>
			<cfif getBusinesses.RecordCount>
				<cfoutput>
			<option value="">Select One</option></cfoutput>
				<cfloop query="getBusinesses">
					<cfset Variables.TmpIMUserBusTaxId				= FormatTaxIdForDisplay(getBusinesses.IMUserBusTaxId,
																							getBusinesses.IMUserBusEINSSNInd)>
					<cfif Variables.TmpIMUserBusSeqNmb EQ getBusinesses.IMUserBusSeqNmb>
						<cfset Variables.Selected					= "selected">
					<cfelse>
						<cfset Variables.Selected					= "        ">
					</cfif>
					<cfoutput>
			<option #Variables.Selected# value="#getBusinesses.IMUserBusSeqNmb#">#Variables.TmpIMUserBusTaxId#</option></cfoutput>
				</cfloop>
			<cfelse>
				<cfoutput>
			<option value="">No Business found in database</option></cfoutput>
			</cfif>
			<cfoutput>
			</select>
			</td></tr></table>
		</td>
	</tr></cfoutput>
		</cfcase>
		<cfcase value="P,S">
			<cfset Variables.TmpIMUserBusSeqNmb					= "">
			<cfloop index="RoleBusIdx" from="1" to="#ArrayLen(Variables.ArrayRoleBusInfo)#">
				<cfif Variables.ArrayRoleBusInfo[RoleBusIdx][1] EQ getRoles2.IMAppRoleId>
					<cfoutput>
		<input type="Hidden" name="OldRoleBus_#getRoles2.IMAppRoleId#" value="#Variables.ArrayRoleBusInfo[RoleBusIdx][2]#"></cfoutput>
					<cfset Variables.TmpIMUserBusSeqNmb			= Variables.ArrayRoleBusInfo[RoleBusIdx][2]>
				</cfif>
			</cfloop>
			<cfoutput>
	<tr data-rolenumber="#Variables.IMAppRoleId#" style="display:#Variables.DispRqrdData#;">
		<td colspan="2">&nbsp;</td><!--- Skip table columns of System Folder and Role Checkbox. --->
		<td colspan="2" nowrap valign="top">
			<label class="mandlabel" for="RoleBus_#getRoles2.IMAppRoleId#">
				Business
			</label>
			#Variables.SpacingRqrdData#
			<table class="manddata"><tr><td><!--- Note, manddata is intrinsically inlinetable. --->
			<select name="RoleBus_#getRoles2.IMAppRoleId#"></cfoutput>
			<cfif getBusinesses.RecordCount>
				<cfoutput>
			<option value="">Select One</option></cfoutput>
				<cfloop query="getBusinesses">
					<cfif Len(getBusinesses.IMUserBusLocDUNSNmb) GT 0>
						<cfset Variables.TmpIMUserBusTaxId		= FormatTaxIdForDisplay(getBusinesses.IMUserBusTaxId,
																						getBusinesses.IMUserBusEINSSNInd)>
						<cfif Variables.TmpIMUserBusSeqNmb EQ getBusinesses.IMUserBusSeqNmb>
							<cfset Variables.Selected			= "selected">
						<cfelse>
							<cfset Variables.Selected			= "        ">
						</cfif>
						<cfoutput>
			<option #Variables.Selected# value="#getBusinesses.IMUserBusSeqNmb#">#Variables.TmpIMUserBusTaxId# (DUNS - #getBusinesses.IMUserBusLocDUNSNmb#)</option></cfoutput>
					</cfif>
				</cfloop>
			<cfelse>
				<cfoutput>
			<option value="">No Business found in database</option></cfoutput>
			</cfif>
			<cfoutput>
			</select>
			</td></tr></table>
		</td>
	</tr></cfoutput>
		</cfcase>
		</cfswitch>
	</cfif><!--- MaySeeThisRole --->
</cfloop><!--- getRoles2 --->
<cfif Variables.TBodyIsOpen>
	<cfoutput>
</tbody></cfoutput>
</cfif>

<!---
AUTHOR:				Ian Clark
DATE:				08/04/2015
DESCRIPTION:		Security questions. Need code to retrieve questions from Database.
NOTES:
INPUT:				Needs the related javascript file to be included.
OUTPUT:				None
REVISION HISTORY:	08/04/2015,	IAC:	Original implementation

--->
<CFSET Variables.LogAct		= "call SECURQSTNSELTSP">
<CFSET Variables.cfprname	= "getqstn">
 <cfset Variables.TempDB = Variables.db>
<cfset Variables.db 									=  "oracle_housekeeping">
<cfset Variables.Identifier								= 4>
<CFINCLUDE template = "/cfincludes/oracle/security/spc_SECURQSTNSELTSP.PUBLIC.cfm"><!--- Change once it gets incorprated. --->
<cfif Variables.TxnErr>
	<cfset Variables.SaveMe								= "No">
	<cfoutput><p class="dem_errmsg">Serious database error occured. Following information might help #Variables.ErrMsg#</p></cfoutput>
	<cfabort>
</cfif>
<cfset Variables.db  = Variables.TempDB>
<cfoutput>
<cfif isDefined("variables.getqstn.recordcount") AND variables.getqstn.recordcount GT 0>
<div>
<fieldset class="inlineblock"><legend>Security Questions</legend>
<div class="tbl">
<div class="tbl_row">
<div class="tbl_cell formlabel"><label for="firstquestion" class="mandlabel">First Question:</label></div>
<div class="tbl_cell formdata nowrap">
<div class="manddata" name="divfstqstn">
<select class = "questions" id="fstqstn" name="fstqstn">
<option value="">Select Question</option>

<cfloop query="getqstn">
	<option value="#getqstn.SECURQSTNID#" <cfif (isDefined("Variables.Qstn1") AND Variables.Qstn1 EQ getqstn.SECURQSTNID) > SELECTED</cfif>>#getqstn.SECURQSTNTXT#?</option>
</cfloop>

</select>
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->

<div class="tbl_row" id="answer1" name="answer1" <cfif isDefined("Variables.ans1") AND Variables.ans1 NEQ "">style="display:none;"</cfif>>
<div class="tbl_cell formlabel"><label for="firstanswer" class="mandlabel">Answer:</label></div>
<div class="tbl_cell formdata nowrap">

<div class="manddata">
  <input type="Text" name="firstanswer" id="firstanswer" size="50" maxlenght="130"   >
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->
<div class="tbl_row" id="secondQuest" name="secondQuest" <cfif (isDefined("Variables.Qstn2") AND Variables.Qstn2 EQ "")>style="display:none;"</cfif>>
<div class="tbl_cell formlabel"><label for="secondquestion" class="mandlabel">Second Question:</label></div>
<div class="tbl_cell formdata nowrap">
<div class="manddata"  name="divsndqstn">
<!--- only need the first select populated javascript will fill this --->
<select class = "questions" id="sndqstn" name="sndqstn">
  <cfif (isDefined("Variables.Qstn1") AND Variables.Qstn1 NEQ "")>
		<cfloop query="getqstn">
		<cfif getqstn.SECURQSTNID NEQ Variables.Qstn1 >
		<option value="#getqstn.SECURQSTNID#" <cfif (isDefined("Variables.Qstn2") AND Variables.Qstn2 EQ getqstn.SECURQSTNID) > SELECTED</cfif>>#getqstn.SECURQSTNTXT#?</option>
		</cfif>
		</cfloop>
  </cfif>
</select>
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->
<div class="tbl_row" id="answer2" name="answer2" <cfif (isDefined("Variables.ans2") AND Variables.ans2 NEQ "")>style="display:none;"</cfif>>
<div class="tbl_cell formlabel"><label for="secondanswer" class="mandlabel">Answer:</label></div>
<div class="tbl_cell formdata nowrap">
<div class="manddata">
  <input type="Text" name="secondanswer" id="secondanswer" size="50" maxlenght="130" >
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->
<div class="tbl_row" id="thirdQuest"  name="thirdQuest" <cfif (isDefined("Variables.Qstn3") AND Variables.Qstn3 EQ "")>style="display:none;"</cfif>>
<div class="tbl_cell formlabel"><label for="thirdquestion" class="mandlabel">Third Question:</label></div>
<div class="tbl_cell formdata nowrap">
<div class="manddata"  name="divthrdqstn">
	<!--- only need the first select populated javascript will fill this --->
<select class = "questions" id="thrdqstn" name="thrdqstn">
<cfif (isDefined("Variables.Qstn2") AND Variables.Qstn2 NEQ "")>
		<cfloop query="getqstn">
		<cfif getqstn.SECURQSTNID NEQ Variables.Qstn1 AND  getqstn.SECURQSTNID NEQ Variables.Qstn2>
		<option value="#getqstn.SECURQSTNID#" <cfif (isDefined("Variables.Qstn3") AND Variables.Qstn3 EQ getqstn.SECURQSTNID) > SELECTED</cfif>>#getqstn.SECURQSTNTXT#?</option>
		</cfif>
		</cfloop>
  </cfif>
</select>
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->
<div class="tbl_row" id="answer3"  name="answer3" <cfif (isDefined("Variables.ans3") AND Variables.ans3 NEQ "")> style="display:none;"</cfif>>
<div class="tbl_cell formlabel"><label for="thirdanswer" class="mandlabel">Answer:</label></div>
<div class="tbl_cell formdata nowrap">
<div class="manddata">
  <input type="Text" name="thirdanswer" id="thirdanswer" size="50" maxlenght="130" >
</div>

</div><!-- tbl_cell -->
</div><!-- tbl_row -->
</div><!-- "tbl" -->

</fieldset>
</div>
</cfif>
</cfoutput>

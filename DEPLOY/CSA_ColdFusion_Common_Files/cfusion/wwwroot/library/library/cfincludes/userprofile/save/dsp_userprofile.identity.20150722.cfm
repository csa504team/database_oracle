<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				05/16/2006. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				See dsp_userprofile.template.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Merged implementation. Cannibalized code from /library/cfincludes/userprofile. Also, 
										now that we have display control down to the field level, merged name and confirm groups. 
					07/08/2014, SRS:	Rescued confirm from my hard drive after being trashed by someone who didn't know what he 
										was doing. The version from the time of tiber-to-anacostia migration was totally 
										garbaged with code from dsp_formdata_contact. Restored it and changed GLS to CLS. 
					09/01/2011, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables.
					07/16/2010, IAC:	Changed 18 to 15 for age based on Terry Lewis Request. 
					12/22/2008, NNI:	Changed javascript message text from Surname and Given Name to Last Name and
										First Names, respectively.
					09/09/2008, SRS:	Added date picker (CalendarPopup) to IMUserDOBDt. 
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables.
					05/16/2006, SRS:	Original implementation.
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>Identity Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.ReadonlyProfile>
		<cfset Variables.MandOptName						= "view">
	<cfelse>
		<cfparam name="Variables.MandOptName"				default="mand">
	</cfif>
	<cfif Variables.MandOptName is "view">
		<cfset Variables.MandOptIMUserFirstNm				= "view">
		<cfset Variables.MandOptIMUserMidNm					= "view">
		<cfset Variables.MandOptIMUserLastNm				= "view">
		<cfset Variables.MandOptIMUserSfxNm					= "view">
	</cfif>
	<cfif Variables.MandOptName is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel #Variables.MandOptName#label">Name:</div>
      <div class="tbl_cell formdata nowrap"><!-- Side-by-Side --></cfoutput>
		<cfset Defaults("IMUserFirstNm",		"FirstName",	"First",	"mand", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="30"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfset Defaults("IMUserMidNm",			"MiddleName",	"Middle",	"opt", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="10"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfset Defaults("IMUserLastNm",			"LastName",		"Last",		"mand", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="30"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfset Defaults("IMUserSfxNm",			"NameSuffix",	"Suffix",	"opt", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="30" size="10"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          <span title="Jr, Sr, I, II, etc.">(#Variables.Label#)</span>
        </div><!-- /Vertical -->
      </div><!-- /formdata = /Side-by-Side -->
    </div><!-- tbl_row --></cfoutput>
	</cfif><!--- /MandOptName is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserDOBDt",			"Birthdate",	"Date of Birth",	"mand", "")>
	<cfset Variables.SaveMandOptForDOB						= Variables.MandOpt><!--- Save for JSInline, below. --->
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#:</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="10" maxlength="10" title="mm/dd/yyyy" onchange="
        var sMand											= isMand(this);
        if  (EditDateMinYearsOld('#JSStringFormat(Variables.Eng)#', this.value, sMand, 15, ' to use SBA systems'))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;
        <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
        href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
        <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
        alt="Pick a date using a popup calendar. (Opens a new window.)"
        title="Pick a date using a popup calendar. (Opens a new window.)">
        </a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserLast4DgtSSNNmb",	"PIN",			"PIN",				"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask(#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><!--- Okay to show the following, even if MandOpt is "view": --->
        &nbsp;
        (Personal Identification Number, Last 4 Digits of SSN)
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Identity Information --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#<cfif Variables.SaveMandOptForDOB is not "skip">

<!-- Identity-Related: -->

#Request.SlafTopOfHeadCalendarPopup#
#Request.SlafTopOfHeadEditDate#
#Request.SlafTopOfHeadEditDateMinYearsOld#
#Request.SlafTopOfHeadEditDateNonFuture#</cfif>
</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

<!---
AUTHOR:				Ian Clark
DATE:				08/19/2015
DESCRIPTION:		common code for updating security questions
NOTES:				None
INPUT:
OUTPUT:
REVISION HISTORY:	08/19/2015 IAC  	08/19/2015 Original Implementation
--->
<!--- IAC check for questions before saving password go back if they are not filled out.
 ReReplace(mySpaceFilledString, "[[:space:]]","","ALL")
Lets remove all whitespace and make it lowercase before we hash it.
<cfset Variables.SECURQSTNANSWRTXT	=	Hash( LCase(ReReplace(FORM.FIRSTANSWER, "[[:space:]]","","ALL")), "SHA1")>
<cfset Variables.SECURQSTNANSWRTXT	=	Hash(FORM.FIRSTANSWER, "SHA1")>
If the security questions already exist
--->
<cfif IsDefined("Variables.db")>
	<cfset Variables.tempDB 								=  Variables.db >
</cfif>
<cfset Variables.db 									=  "oracle_housekeeping">
<CFSET Variables.cfprname	= "getans">
<cfif IsDefined("Session.IMUserNm") >
	<cfset Variables.IMUserNm = Session.IMUserNm>
</cfif>
<cfif isDefined("Session.IMUserNm")>
				<cfset Variables.db 									=  "oracle_housekeeping">
				<cfquery name="getIMUSERID"	datasource="#Variables.db#" >
				select IMUSERID
					from security.IMUserTbl WHERE IMUserNm = '#Session.IMUserNm#'
				</cfquery>
			<cfif getIMUSERID.RecordCount EQ 1>
				<cfset Variables.IMUserId = getIMUSERID.IMUserId >
			</cfif>
		<cfelse>
			<cfif isDefined("Session.IMUserId")>
				<cfset Variables.IMUserId = Session.IMUserId >
			</cfif>
</cfif>
	<cfif ((isDefined("FORM.firstanswer") and FORM.firstanswer NEQ "") OR (isDefined("FORM.ans1") and FORM.ans1 NEQ ""))
			AND ((isDefined("FORM.SECONDANSWER") and FORM.SECONDANSWER NEQ "") OR (isDefined("FORM.ans2") and FORM.ans2 NEQ ""))
			AND ((isDefined("FORM.THIRDANSWER") and FORM.THIRDANSWER NEQ "") OR (isDefined("FORM.ans3") and FORM.ans3 NEQ ""))><!--- IAC all three questions answered --->
				<cfset Variables.CREATUSERID		=	Variables.IMUserNm>
		<!--- process first question and answer or update or leave alone if no change --->
		<cfif (isDefined("FORM.firstanswer") and FORM.firstanswer NEQ "")>
			<cfset Variables.Process = "No" >
			<cfset Variables.TEMPSECURQSTNID 		=	FORM.FSTQSTN>
			<cfset Variables.SECURQSTNANSWRTXT	=	Hash(LCase(ReReplace(FORM.FIRSTANSWER, "[[:space:]]","","ALL")), "SHA1")>
		<cfif FORM.qstn1 NEQ "" AND Variables.TEMPSECURQSTNID  NEQ FORM.qstn1>	<!--- user changed the first question. Need to delete old one.--->
			<cfset Variables.Process = "Yes" >
			<cfset Variables.SECURQSTNID 		=	FORM.qstn1>
			<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRDELTSP.PUBLIC.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to delete the old first question.</li>">
				</cfif>
		<cfelse>
			<cfif (FORM.ans1 NEQ Variables.SECURQSTNANSWRTXT)><!--- Same question different answer if same question & answer do nothing--->
					<cfset Variables.Process = "Yes" >
			</cfif>
		</cfif>

			<cfif  Variables.Process>
				<cfset Variables.SECURQSTNID 		=	FORM.FSTQSTN>
				<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRINSCSP.PUBLIC.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to insert answer to the first question.</li>">
				</cfif>
				<cfif NOT Variables.SaveMe>
					<!--- <cfinclude template="#Variables.CfIncludeLibURL#/put_sbalookandfeel_savemessages.cfm">
					<cflocation	addtoken="No"					url="#Variables.ChgPassPage#PrevErr=Y"> --->
					<cfoutput>#Variables.ErrMsg#</cfoutput><cfabort>
				</cfif>
			</cfif>
		</cfif>
		<cfif (isDefined("FORM.SECONDANSWER") and FORM.SECONDANSWER NEQ "")>
			<cfset Variables.Process = "No" >
			<cfset Variables.TEMPSECURQSTNID 		=	FORM.SNDQSTN>
			<cfset Variables.SECURQSTNANSWRTXT	=	 Hash(LCase(ReReplace(FORM.SECONDANSWER, "[[:space:]]","","ALL")), "SHA1")>
			<cfif  FORM.qstn2 NEQ "" AND Variables.TEMPSECURQSTNID  NEQ FORM.qstn2>	<!--- user changed the second question. Need to delete old one.--->
			<cfset Variables.Process = "Yes" >
			<cfset Variables.SECURQSTNID 		=	FORM.qstn2>
			<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRDELTSP.PUBLIC.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to delete the old second question.</li>">
				</cfif>
		<cfelse>
			<cfif (FORM.ans2 NEQ Variables.SECURQSTNANSWRTXT)><!--- Same question different answer if same question & answer do nothing--->
					<cfset Variables.Process = "Yes" >
			</cfif>
		</cfif>
			<cfif  Variables.Process>
					<cfset Variables.SECURQSTNID 		=	FORM.SNDQSTN>
				<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRINSCSP.PUBLIC.cfm">
			<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to insert answer to the second question.</li>">
				</cfif>
				<cfif NOT Variables.SaveMe>
					<cfinclude template="#Variables.CfIncludeLibURL#/put_sbalookandfeel_savemessages.cfm">
					<cflocation	addtoken="No"					url="#Variables.ChgPassPage#PrevErr=Y">
				</cfif>
			</cfif>
		</cfif>
		<cfif (isDefined("FORM.THIRDANSWER") and FORM.THIRDANSWER NEQ "")>
				<cfset Variables.Process = "No" >
				<cfset Variables.TEMPSECURQSTNID 		=	FORM.THRDQSTN>
				<cfset Variables.SECURQSTNANSWRTXT	=	 Hash(LCase(ReReplace(FORM.THIRDANSWER, "[[:space:]]","","ALL")), "SHA1")>
			<cfif FORM.qstn3 NEQ "" AND Variables.TEMPSECURQSTNID  NEQ FORM.qstn3>	<!--- user changed the first question. Need to delete old one.--->
				<cfset Variables.Process = "Yes" >
				<cfset Variables.SECURQSTNID 		=	FORM.qstn3>
				<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRDELTSP.PUBLIC.cfm">
				<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to delete the old first question.</li>">
				</cfif>
			<cfelse>
				<cfif (FORM.ans3 NEQ Variables.SECURQSTNANSWRTXT)><!--- Same question different answer if same question & answer do nothing--->
					<cfset Variables.Process = "Yes" >
			</cfif>
		</cfif>

			<cfif  Variables.Process>
					<cfset Variables.SECURQSTNID 		=	FORM.THRDQSTN>
			<cfinclude template = "/cfincludes/oracle/security/spc_IMUSERSECURQSTNANSWRINSCSP.PUBLIC.cfm">
			<cfif Variables.TxnErr>
					<cfset Variables.SaveMe					= "No">
					<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>Database error occured trying to insert answer to the third question.</li>">
				</cfif>
				<cfif NOT Variables.SaveMe>
					<cfinclude template="#Variables.CfIncludeLibURL#/put_sbalookandfeel_savemessages.cfm">
					<cflocation	addtoken="No"					url="#Variables.ChgPassPage#PrevErr=Y">
				</cfif>
			</cfif>
		</cfif>
				<cfset Variables.Commentary				= "Your Security questions have been saved. ">
	<cfelse>
	<cfif (isDefined("FORM.ans1") and FORM.ans1 EQ "")
			OR (isDefined("FORM.ans2") and FORM.ans2 EQ "")
			OR (isDefined("FORM.ans3") and FORM.ans3 EQ "")><!--- IAC all three questions answered --->
		<cfset Variables.ErrMsg					= Variables.ErrMsg	& "<li>You must provide answers to three questions.</li>">
		<cfset Variables.SaveMe					= "No">
	</cfif>
		</cfif>
<cfif IsDefined("Variables.tempDB")>
	<cfset Variables.db 								=  Variables.tempDB >
</cfif>
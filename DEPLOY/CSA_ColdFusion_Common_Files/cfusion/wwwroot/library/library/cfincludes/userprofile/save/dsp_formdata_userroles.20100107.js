
// Start of dsp_formdata_userroles.js		// (in case included inline)

// These routines are separate from dsp_formdata_userroles.cfm so that they can be included in the caller's JSInline. 
// Because they're in a separate *.js file, they can be cached by the browser, which speeds up page load times. That 
// was the main reason for separating out into a separate file. 
//
// But there are other advantages to having these routines in a separate file: We don't have to double the pound-signs 
// in "#RolesFor", which makes the code more readable. Also, when viewing this file in HomeSite or Dreamweaver, the code 
// will be color-coded (syntax-highlighted) according to JavaScript syntax, which also makes it more readable. 
//
//											Steve Seaquist
//											Trusted Mission Solutions, Inc.
//											01/06/2010
//
// REVISION HISTORY:	01/06/2010, SRS:	Original implementation. 

function CachePrivsAndRqrdDataRows			(pThis)
	{
	// "pThis" is a Role Checkbox, which will have data-rolenumber and data-sysnumber attributes. 
	// Cache the jQuery object of subordinate priv/rqrd rows as a custom property of pThis. 
	// Caching makes the page snappier, more responsive to clicks once clicks have occurred. 
	if	(!(pThis.CachedPrivsAndRqrdDataRows))// (If already cached, does nothing.) 
		{
		var	sDRN							= pThis.getAttribute("data-rolenumber");
		var	sDSN							= pThis.getAttribute("data-sysnumber");
		pThis.CachedPrivsAndRqrdDataRows	= $("#RolesFor"+sDSN+" tr[data-rolenumber="+sDRN+"]");
		}
	}

function ShowHidePrivsAndRqrdDataRows		(pThis, pShow)
	{
	CachePrivsAndRqrdDataRows(pThis);
	// Use css(), not show()/hide(), because need to check current state in ToggleRolePrivsAndRqrdData. 
	if	(pShow)
		pThis.CachedPrivsAndRqrdDataRows	.css({display: ""});		// This is, .show(). 
	else
		pThis.CachedPrivsAndRqrdDataRows	.css({display: "none"});	// This is, .hide((). 
	}

function TogglePrivsAndRqrdDataRows			(pRoleNumber)
	{
	var	sCurrentlyHidden					= false;
	var	sRoleCheckbox						= document.getElementById("RoleRqst_" + pRoleNumber);
	// jQuery's .show() and .hide() methods might use CSS properties other than style.display to show and hide. 
	// For example, in MSIE, they might have chosen to use the visibility attribute instead, which is MSIE-only. 
	// By using .css({display:xxx;}) in the ShowHide routine, above, we make it explicit, so that we can test it: 
	if	(sRoleCheckbox)
		{
		CachePrivsAndRqrdDataRows			(sRoleCheckbox);
		sRoleCheckbox.CachedPrivsAndRqrdDataRows.each(function()
			{
			if	(this.style)
				if	(this.style.display)
					if	(this.style.display == "none")
						sCurrentlyHidden	= true;
			});
		// On initial load of the page, it's possible for privileges to be hidden and required data displayed. 
		// A consequence of this .each() loop is that, if ANY row is hidden, the first call of this function will 
		// act as a .show() on the hidden rows. Thereafter, it behaves to reverse the current state, all or nothing. 
		ShowHidePrivsAndRqrdDataRows		(sRoleCheckbox, sCurrentlyHidden);
		}
	else// The following should never happen, but it doesn't hurt to get intelligible feedback if it does: 
		alert("ERROR. Please report this error to the SBA's Office of the CIO that Role number '"
			+ sRoleNumber + "' has a broken show/hide hotlink.");
	}

function ToggleRole							(pThis, pPrevRequests, pShowAuthMsg)
	{
	if	(pPrevRequests > 0)
		if	(pThis.checked)
			pThis.checked					= confirm("You have previously requested access for "
													+ "'"+pThis.title+"' "+pPrevRequests+" time(s). "
													+ "Those requests were not acted upon. "
													+ "Are you sure you want to request access again?");
	if	(pShowAuthMsg)
		if	(pThis.checked)
			pThis.checked					= confirm("You have requested access to a Federal System which "
													+ "requires prior authorization from the program office "
													+ "and from your supervisor. If you have not received "
													+ "prior authorization, you must click Cancel.");
	ShowHidePrivsAndRqrdDataRows			(pThis, pThis.checked);
	}

function ToggleSystem						(pSysTypCd)
	{
	var	RoleElem							= document.getElementById('RolesFor'	+ pSysTypCd);
	var	ImgName								= "Img"									+ pSysTypCd;
	if	(RoleElem.style.display				!= "none")
		{
		document.images[ImgName].src		= "/library/images/sbatree/ftv2folderclosed.gif";
		RoleElem.style.display				= "none";
		}
	else
		{
		document.images[ImgName].src		= "/library/images/sbatree/ftv2folderopen.gif";
		RoleElem.style.display				= "";
		}
	}

function UncheckAll(pSysTypCd)
	{
	$("#RolesFor" + pSysTypCd + " input[type=checkbox]").each(function()
		{
		this.checked = false;
		});
	}

/// End of dsp_formdata_userroles.js		// (in case included inline)


<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Included "group" of dsp_userprofile.
NOTES:				Called by dsp_userprofile.cfm.
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control
					what to do about individual form elements.
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be
					the current user:	Appended to the end of AppDataInline and JSInline.
REVISION HISTORY:	09/24/2015, SRS:	Forced IMUserLast4DgtSSNNmb (PIN) to "skip". 
					09/11/2015, SRS:	Fixed ReadOnlyProfile's person name if not Application-to-Application user type. 
					07/30/2015, SRS:	Original implementation. Cannibalized code from /library/cfincludes/userprofile.
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#
<fieldset class="inlineblock">
  <legend>Identity Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("IMUserTypCd", "UserType", "User Type", "mand", "TypCd")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"></cfoutput>
		<cfif Variables.MandOpt is "view">
			<!---
			Query-of-Queries is the slowest thing that ColdFusion does. It essentially has to load an entire SQL parser,
			parse the pseudo-SQL and emulate a DBMS. It could literally add seconds to use a Query-of-Queries for this.
			This way, however, takes only milliseconds:
			--->
			<cfset Variables.Description					= "(none)">
			<cfloop query="Variables.ActvIMUserTypTbl">
				<cfif Variables.ActvIMUserTypTbl.code		is Variables.VarValue>
					<cfset Variables.Description			= Variables.ActvIMUserTypTbl.description>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfoutput>
        #Variables.Description#</cfoutput>
		<cfelse>
			<cfoutput>
        <select #Variables.NameAndId# onchange="return DoThisOnChangeUserType();"></cfoutput>
			<cfset Variables.DspOptsNotSelText				= "Not Yet Selected">
			<cfset Variables.DspOptsQueryName				= "Variables.ActvIMUserTypTbl">
			<cfset Variables.DspOptsSelList					= Variables.VarValue>
			<cfif Variables.PageIsUser>
				<cfset Variables.DspOptsSkipList			= ""><!--- Application-to-Application allowed. --->
			<cfelse>
				<cfset Variables.DspOptsSkipList			= "P"><!--- Application-to-Application not allowed. --->
			</cfif>
			<cfset Variables.DspOptsTabs					= "#Request.SlafEOL#        ">
			<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
			<cfoutput>
        </select></cfoutput>
		</cfif>
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /IMUserTypCd --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.VarValueTypCd is "P">
		<cfset Defaults("IMUserPrtNm",	"PartnerName",	"Partner Name",		"mand", "")>
	<cfelse>
		<cfset Defaults("IMUserPrtNm",	"PartnerName",	"Partner Name",		"opt", "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row TypeSensitive TypeP" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="30" maxlength="128" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.ReadOnlyProfile>
		<cfset Variables.MandOptName						= "view">
	<cfelse>
		<cfparam name="Variables.MandOptName"				default="mand"><!--- If displayed, it's mandatory. --->
	</cfif>
	<cfif Variables.MandOptName is "view">
		<cfset Variables.MandOptIMUserFirstNm				= "view">
		<cfset Variables.MandOptIMUserMidNm					= "view">
		<cfset Variables.MandOptIMUserLastNm				= "view">
		<cfset Variables.MandOptIMUserSfxNm					= "view">
	</cfif>
	<cfif Variables.MandOptName is not "skip">
		<cfoutput><!--- Note that "Type" handles initial .show() in AddCustomer, where gUserType is "": --->
    <div class="tbl_row TypeSensitive Type TypeC TypeN TypeS TypeU" style="display:<cfif Variables.VarValueTypCd is "P">none</cfif>;">
      <div class="tbl_cell formlabel #Variables.MandOptName#label">Name:</div>
      <div class="tbl_cell formdata nowrap"><!-- Side-by-Side --></cfoutput>
		<cfif Variables.VarValueTypCd is "P">
			<cfset Defaults("IMUserFirstNm",	"FirstName",	"First",	"opt",  "")>
		<cfelse>
			<cfset Defaults("IMUserFirstNm",	"FirstName",	"First",	"mand", "")>
		</cfif>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="30"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfset Defaults("IMUserMidNm",			"MiddleName",	"Middle",	"opt", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="10"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfif Variables.VarValueTypCd is "P">
			<cfset Defaults("IMUserLastNm",		"LastName",		"Last",		"opt",  "")>
		<cfelse>
			<cfset Defaults("IMUserLastNm",		"LastName",		"Last",		"mand", "")>
		</cfif>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="128" size="30"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          (#Variables.Label#)
        </div><!-- /Vertical --></cfoutput>
		<cfset Defaults("IMUserSfxNm",			"NameSuffix",	"Suffix",	"opt", "")>
		<cfoutput>
        <div class="inlineblock"><!-- Vertical -->
          <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
          #Variables.VarValue#<cfelse>
          <input type="Text" data-type="text" maxlength="30" size="10"
          #Variables.NameIdAndValue# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif>
          </div><br/>
          <span title="Jr, Sr, I, II, etc.">(#Variables.Label#)</span>
        </div><!-- /Vertical -->
      </div><!-- /formdata = /Side-by-Side -->
    </div><!-- tbl_row --></cfoutput>
	</cfif><!--- /MandOptName is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.VarValueTypCd is "P">
		<cfset Defaults("IMUserDOBDt",		"Birthdate",	"Date of Birth",	"opt",  "")>
	<cfelse>
		<cfset Defaults("IMUserDOBDt",		"Birthdate",	"Date of Birth",	"mand", "")>
	</cfif>
	<cfset Variables.SaveMandOptForDOB						= Variables.MandOpt><!--- Save for JSInline, below. --->
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row TypeSensitive TypeC TypeN TypeS TypeU" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#:</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="10" maxlength="10" placeholder="mm/dd/yyyy" onchange="
        var sMand											= isMand(this);
        if  (EditDateMinYearsOld('#JSStringFormat(Variables.Eng)#', this.value, sMand, 15, ' to use SBA systems'))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;
        <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
        href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
        <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
        alt="Pick a date using a popup calendar. (Opens a new window.)"
        title="Pick a date using a popup calendar. (Opens a new window.)">
        </a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt (for DOB) is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Variables.MandOptIMUserLast4DgtSSNNmb			= "skip"><!--- Quick and easy way to suppress PIN. --->
	<cfset Defaults("IMUserLast4DgtSSNNmb",	"PIN",			"PIN",				"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><!--- Okay to show the following, even if MandOpt is "view": --->
        &nbsp;
        (4-Digit Personal Identification Number)
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt (for PIN) is not "skip" --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Identity Information --><br/><!-- Break because preceding fieldset has class="inlineblock" -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#<cfif Variables.SaveMandOptForDOB is not "skip">

<!-- Identity-Related: -->

#Request.SlafTopOfHeadCalendarPopup#
#Request.SlafTopOfHeadEditDate#
#Request.SlafTopOfHeadEditDateMinYearsOld#
#Request.SlafTopOfHeadEditDateNonFuture#</cfif>
</cfoutput>
<!--- DoThisOnChangeUserType is defined in dsp_userprofile.usertype.cfm, because it needs to access JobTitl. --->
</cfsavecontent>

<!--- ************************************************************************************************************ --->

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Included "group" of dsp_userprofile.
NOTES:				Called by dsp_userprofile.cfm.

	Previous, non-consolidated version didn't make use of already written Library routines to do random string generation
	and presevation in the Session scope. Using "clsweak" because it has the same criteria (mixed case, must include a digit)
	as the previous version. Also avoiding confusing characters, as the previous version did. Because "clsweak" generates
	exactly 8 characters of password, we just take the Left() characters of whatever PasswordGenerate generates.

	Note that the LoadOrig to save off the generated value is NOT being done in bld_Origs, because we want to do this every
	time dsp_userprofile is displayed (even after error returns after server-side validation failure).

INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control
					what to do about individual form elements.
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be
					the current user:	Appended to the end of AppDataInline and JSInline.
REVISION HISTORY:	04/22/2016, NS: 	Added "Refresh Image" feature
					07/30/2015, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.VerifyDifficulty							= "medium">			<!--- Same as before consolidation. --->
<cfset Variables.VerifyEditMask								= "N">				<!--- Same as before consolidation. --->
<cfset Variables.VerifyLength								= RandRange(5,5)>	<!--- Same as before consolidation. --->
<cfset Variables.VerifyMaxLength							= 5>				<!--- Max supported by "clsweak". --->
<cfset Variables.VerifyStrength								= "clsweak">		<!--- Same as before consolidation. --->

<!--- Initializations: --->

<cfif NOT IsDefined("PasswordGenerate")>
	<cfinclude template="#Variables.LibUdfURL#/bld_PasswordUDFs.cfm">
</cfif>
<cfset PasswordAvoidConfusingCharacters()>										<!--- Same as before consolidation. --->
<cfset LoadOrig("Verify",Left(PasswordGenerate(Variables.VerifyStrength),
																Variables.VerifyLength
														))>
<cflock scope="Session" type="Exclusive" timeout="30">
	<!--- Don't use PutDspPageLoadTimeOrigs. That would trash original load-time values when PrevErr=Y. --->
	<cfset Session.DspPageLoadTimeOrigs.Verify				= Variables.Origs.Verify>
</cflock>

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#</cfoutput>
	<cfif NOT IsDefined("Defaults")>
		<cfinclude template="/library/udf/bld_DefaultsUDF.cfm"><!--- Same directory as this file, for now. --->
	</cfif>
	<cfset Defaults("Verify", "Verify", "Verify answer", "mand", "")><!--- Eng name from legend, not label. --->
	<cfif ListFindNoCase("view,skip", Variables.MandOpt)	is 0><!--- Readonly display of Verify makes no sense. --->
		<cfoutput>
			<fieldset class="inlineblock">
				<legend>Verify</legend>
				<div id="captchaDiv">
					<cfimage action="captcha" height="40" width="250" text="#Variables.Verify#" difficulty="#Variables.VerifyDifficulty#">
				</div><br><div>
					&nbsp;Can't read? &nbsp;<input type="button" id="reloadBtn" value="Refresh image"></div><br>
				<label for="Verify" class="#Variables.MandOpt#label">Please enter text shown in the image (case sensitive)</label>
				<div class="tbl">
					<div class="tbl_row">
						<div class="tbl_cell formlabel"></div>
						<div class="tbl_cell formdata nowrap">
							<div class="inlineblock #Variables.MandOpt#data">
						        <input type="text" #Variables.NameAndId# size="#Variables.VerifyMaxLength#" maxlength="#Variables.VerifyMaxLength#"
						        value="" onchange="
						        var sMand											= isMand(this);
						        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '#Variables.VerifyEditMask#', sMand, 1, this.maxLength))
						            return true;
						        this.focus();
						        return false;
						        ">
							</div><!-- /#Variables.MandOpt#data -->
						</div><!-- /formdata -->
					</div><!-- /tbl_row -->
				</div><!-- /tbl -->
			</fieldset><!-- /Verify --><br/><!-- Break because fieldset is class="inlineblock" -->
		</cfoutput>
	</cfif>

</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#
		<script>
		$(document).ready(function() {
			$("##reloadBtn").click(function(e) {
				$("##captchaDiv").load("#Variables.PageName#?NoCache=" + escape((new Date()).getTime()) + "&action=RefreshCaptcha" + "##captchaDiv");
				e.preventDefault();
			});
		});
		</script>
	</cfoutput>
</cfsavecontent>
<!--- ************************************************************************************************************ --->

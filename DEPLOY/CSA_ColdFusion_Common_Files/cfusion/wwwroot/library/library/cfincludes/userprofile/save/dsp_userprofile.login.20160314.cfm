<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Included "group" of dsp_userprofile.
NOTES:				Called by dsp_userprofile.cfm.
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control
					what to do about individual form elements.
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be
					the current user:	Appended to the end of AppDataInline and JSInline.
REVISION HISTORY:	03/11/2016, VRVK:	Added message as a paramter on CLS for Borrower User Type to contact Servicing Office 	 										to update SBA financial records'.
					03/11/2016, NS:     Changed IMUserLockInd checkbox to radio button set (as per client request)
					02/26/2016, NS:     Added support for IMUserLockInd
					02/01/2016, NS:     Added LastRecertDt form control (excludes N and P user types)
					01/08/2016, SL:		Made password expiration date editable for SecAdmin
					12/12/2015, DG:		Added Account Created Date in user profile page as Read only field.
					11/10/2015, NS:     Added support for IMUserActvInactInd change from active to inactive
					09/11/2015, SRS:	Fixed ReadOnlyProfile (don't try to call active/inactive onchange if it doesn't exist).
					07/30/2015, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->
<cfset Variables.AlternativeLoginNamesMax					= 3><!--- Number of alternative login names. --->
<cfparam name = "Variables.DisplayQuestions" 		default = "No" />
<cfparam name = "Variables.IMUserLockInd" 			default = "#Variables.Origs.DispIMUserLockInd#" />

<!--- ************************************************************************************************************ --->
<cfif Variables.PageIsProfile>
	<cfif IsDefined("Variables.CURRIMUSERTYPCD") AND Variables.CURRIMUSERTYPCD EQ "N">
		
		<cfset Variables.cfprname	   = "GetBorrowerMessage" />
		<cfset Variables.LogAct        = "get the borrower message" />
		<cfset Variables.Identifier    = 0 />
		<cfset Variables.IMPrmtrSeqNmb = 14 />
		
		<cfinclude template = "/cfincludes/oracle/security/spc_IMPRMTRSELTSP.cfm">
		
		<cfsavecontent variable="Variables.AppDataInline">
			<cfoutput>#Variables.AppDataInline#
				<br />
				<div>
					<div class = "inlineblock dem_warnmsg">
						<p style = "margin: 10px;">
							#GetBorrowerMessage.IMPrmtrDescTxt#
						</p>
					</div>
				</div>
			</cfoutput>
		</cfsavecontent>
		
	</cfif>
</cfif>
<cfif Variables.TxnErr>
	<cfset Variables.SaveMe = "No" />
	<p class="dem_errmsg">
		Serious database error occured. Following information might help #Variables.ErrMsg#
	</p>
	<cfabort />

</cfif>
<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#

		<cfif Variables.DisplayQuestions and Variables.PageIsProfile>
			<br />
			<cfif IsDefined("Variables.GetAns.RecordCount") AND Variables.GetAns.RecordCount GT 2>
				<div>
					<div class = "inlineblock dem_commentary">
						<p style = "margin: 10px;">
							You have already selected your three security questions.
							<br/>
							If you wish to change them, click on the link below.
							<br />
						</p>
						<p style = "margin: 10px;">
							<a href = "dsp_chgsecquestions.cfm?CameFromChgPass=Yes">Change Security Questions</a>
						</p>
					</div>
				</div>
			<cfelse>
				<div>
					<div class = "inlineblock dem_warnmsg">
						<p style = "margin: 10px;">
							You have not answered your three security questions.
							<br/>
							You will be unable to recover your username and/or password until you have done so.
							<br/>
							To set your security questions, click on the link below.
						</p>
						<p style = "margin: 10px;">
							<a href = "dsp_chgsecquestions.cfm?CameFromChgPass=Yes">Change Security Questions</a>
						</p>
					</div>
				</div>
			</cfif>
		</cfif>
<fieldset class="inlineblock">
  <legend>Login Information</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0>
		<cfset Defaults("IMSBACrdntlLognNm", "UserID", "User ID", "mand", "")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlLognNm", "UserID", "User ID", "view", "")>
	</cfif>
	<cfset Variables.EngLogn								= Variables.Eng>	<!--- Needed later. --->
	<cfset Variables.MandOptLogn							= Variables.MandOpt><!--- Needed later. --->
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata">
        <div class="inlineblock #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        <cfif Request.SlafIPAddrIsOCA><span title="#Variables.EndUserData.IMUserId#">#Variables.VarValue#</span><cfelse>#Variables.VarValue#</cfif><cfelse>
        <input type="text" #Variables.NameIdAndValue#
        size="15" maxlength="15" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'G', sMand, 8, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;<a href ="javascript:;" onclick="DisplaySBAUsernameRules();">SBA #Variables.Eng# Rules</a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0>
		<cfparam name="Variables.MandOptIMSBACrdntlPasswrdExprDt" default="skip">
	<cfelse>
		<cfparam name="Variables.MandOptIMSBACrdntlPasswrdExprDt" default="view">
	</cfif>
	<cfif Variables.MandOptIMSBACrdntlPasswrdExprDt is not "skip">
		<cfset Variables.MandOptIMSBACrdntlPasswrdExprDt	= "view">
	</cfif>
	<cfset Defaults("IMSBACrdntlPasswrdExprDt", "Expires", "Password Expires",	"view", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfset Variables.VarValue							= Variables.Origs.DispIMSBACrdntlPasswrdExprDt>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data">
			<cfif isDefined("Session.security.IsSecAdmin") AND Session.security.IsSecAdmin  AND NOT Variables.PageIsProfile>
                <input type="Text" #Variables.NameIdAndValue#
                    size="10" maxlength="10" placeholder="mm/dd/yyyy">
                     <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
                    href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
                    <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
                    alt="Pick a date using a popup calendar. (Opens a new window.)"
                    title="Pick a date using a popup calendar. (Opens a new window.)">
                    </a>
            <cfelse>
                #Variables.VarValue#
            </cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsAddCustomer>
		<cfset Defaults("IMSBACrdntlPasswrd", "NewPass1", "Password",			"mand",	"")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlPasswrd", "NewPass1", "Password",			"skip",	"")>
	</cfif>
	<cfif ListFindNoCase("view,skip", Variables.MandOpt) is 0><!--- Readonly display of Password makes no sense. --->
		<cfset Variables.VarValue							= "">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><!-- Always value="" -->
        <input type="Password" #Variables.NameAndId# value="" size="12" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 8, 9999))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;<a href ="javascript:;" onclick="DisplaySBAPasswordRules();">SBA #Variables.Eng# Rules</a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsAddCustomer>
		<cfset Defaults("IMSBACrdntlPasswrd2", "NewPass2", "Re-enter Password",	"mand",	"")>
	<cfelse>
		<cfset Defaults("IMSBACrdntlPasswrd2", "NewPass2", "Re-enter Password",	"skip",	"")>
	</cfif>
	<cfif ListFindNoCase("view,skip", Variables.MandOpt) is 0><!--- Readonly display of Re-enter Password makes no sense. --->
		<cfset Variables.VarValue							= "">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><!-- Always value="" -->
        <input type="Password" #Variables.NameAndId# value="" size="12" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 8, 9999))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif ListFindNoCase("view,skip", Variables.MandOptLogn) is 0><!--- Readonly display of other avail logins makes no sense. --->
		<cfif	(IsDefined("URL.LoginNm1") and (Len(URL.LoginNm1) gt 0))
			or	(IsDefined("URL.LoginNm2") and (Len(URL.LoginNm2) gt 0))
			or	(IsDefined("URL.LoginNm3") and (Len(URL.LoginNm3) gt 0))>
			<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel">These User IDs are available:</div>
      <div class="tbl_cell formdata">
        <div class="inlineblock optdata"></cfoutput>
			<cfloop index="Idx" from="1" to="#Variables.AlternativeLoginNamesMax#">
				<cfif IsDefined("URL.LoginNm#Idx#") and (Len(URL["LoginNm#Idx#"]) gt 0)>
					<cfset Variables.LoginNmBtnNm				= URL["LoginNm#Idx#"]>
					<cfoutput>
        <label class="optlabel" title="Choose &quot;#Variables.LoginNmBtnNm#&quot; as the User ID.">
          <input type="Radio" name="LoginNmOption" value="#Variables.LoginNmBtnNm#" onclick="
          this.form.#Variables.EngLogn#.value				= this.value;
          "> #Variables.LoginNmBtnNm#
        </label><cfif Idx lt Variables.AlternativeLoginNamesMax><br/></cfif></cfoutput>
				</cfif>
			</cfloop>
			<cfoutput>
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
		</cfif>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser OR Variables.PageIsProfile>
		<cfset Defaults("IMUserCreatDt", "UserCreatDt", "User Since",            "view", "")>
	<cfelse>
		<cfset Defaults("IMUserCreatDt", "UserCreatDt", "User Since",            "skip",  "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
              <div class="tbl_row">
                <div class="tbl_cell formlabel">#Variables.Label#</div>
                <div class="tbl_cell formdata nowrap">
                  <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
                  #Variables.Origs.DispIMUserCreatDt#<!--- Because always "view" here, go ahead and use "Disp", which exists. ---><cfelse>
                  <input type="Text" #Variables.NameIdAndValue#
                  size="10" maxlength="10" placeholder="mm/dd/yyyy" onchange="
                  var sMand											= isMand(this);
                  if  (EditDate('#JSStringFormat(Variables.Eng)#', this.value, sMand))
                      return true;
                  this.focus();
                  return false;
                  "></cfif>
                  </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
                  &nbsp;
                  <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
                  href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
                  <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
                  alt="Pick a date using a popup calendar. (Opens a new window.)"
                  title="Pick a date using a popup calendar. (Opens a new window.)">
                  </a></cfif>
                </div><!-- /formdata -->
              </div><!-- /tbl_row --></cfoutput>
	</cfif>

	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
    <cfif ( (Variables.PageIsUser and Variables.EndUserData.IMUserId gt 0) OR Variables.PageIsProfile )
			and listfindnocase("P,N", Variables.EndUserData.ImUserTypCd) eq 0>
	  	<cfset Defaults("LastRecertDt", "LastRecertDt", "Last Certified Date",            "view", "")>
    <cfelse>
        <cfset Defaults("LastRecertDt", "LastRecertDt", "Last Certified Date",            "skip",  "")>
    </cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
             <div class="tbl_row">
             <div class="tbl_cell formlabel">#Variables.Label#</div>
             <div class="tbl_cell formdata nowrap">
               <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
               #Variables.Origs.DispLastRecertDt#<cfelse>
               <input type="Text" #Variables.NameIdAndValue#
               size="10" maxlength="10" placeholder="mm/dd/yyyy" onchange="
               var sMand											= isMand(this);
               if  (EditDate('#JSStringFormat(Variables.Eng)#', this.value, sMand))
                   return true;
               this.focus();
               return false;
               "></cfif>
               </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
               &nbsp;
               <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
               href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
               <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
               alt="Pick a date using a popup calendar. (Opens a new window.)"
               title="Pick a date using a popup calendar. (Opens a new window.)">
               </a></cfif>
             </div><!-- /formdata -->
             </div><!-- /tbl_row -->
		</cfoutput>
	</cfif>

    <!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfif Variables.SelectedUser.ImUserId gt 0>
			<cfif Variables.IsSecAdmin or (Variables.IsSecurityUpdateProfile and Variables.CanReactUser)>
				<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"opt",	"ActInact")>
			<cfelse>
				<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"view",	"ActInact")>
			</cfif>
		<cfelse>
			<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"opt",	"ActInact")>
		</cfif>
	<cfelse>
		<cfset Defaults("IMUserActvInactInd", "Status", "Account Status",	"skip",	"ActInact")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view"><cfif Variables.VarValue is "A">
        Active<cfelse>
        Inactive</cfif><cfelse>
        <select #Variables.NameAndId# onchange="<cfif Variables.PageIsUser>CnfrmChngToInctv(this);</cfif> ShowSuspendAccountReason(this);">
        <option <cfif Variables.VarValue is "A">selected<cfelse>        </cfif> value="A">Active</option>
        <option <cfif Variables.VarValue is "I">selected<cfelse>        </cfif> value="I">Inactive</option>
        </select></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfif Variables.SelectedUser.ImUserId gt 0>
			<cfif Variables.IsSecAdmin or (Variables.IsSecurityUpdateProfile and Variables.CanReactUser)>
				<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"mand",	"")>
			<cfelse>
				<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"view",	"")>
			</cfif>
		<cfelse>
			<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"mand",	"")>
		</cfif>
	<cfelse>
		<cfset Defaults("IMUserSuspRsn", "ReasonText", "Inactivation Reason",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <!-- Using style="display:none;" because jQuery 3.0+ .show()/.hide() will break on display:none in a class: -->
    <div class="tbl_row" id="SuspendAccountReason" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="text" #Variables.NameIdAndValue#
        size="40" maxlength="255" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfif Variables.SelectedUser.ImUserId gt 0>
			<cfif Variables.IsSecAdmin or (Variables.IsSecurityUpdateProfile and Variables.CanUnsuspendUser)>
				<cfset Defaults("IMUserSuspRsnCd", "ReasonCode", "Automatic Suspense Reason",	"opt",	"")>
			<cfelse>
				<cfset Defaults("IMUserSuspRsnCd", "ReasonCode", "Automatic Suspense Reason",	"view",	"")>
			</cfif>
		</cfif>
	<cfelse>
		<cfset Defaults("IMUserSuspRsnCd", "ReasonCode", "Automatic Suspense Reason",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"></cfoutput>
		<cfif Variables.MandOpt is "view">
			<!---
			Query-of-Queries is the slowest thing that ColdFusion does. It essentially has to load an entire SQL parser,
			parse the pseudo-SQL and emulate a DBMS. It could literally add seconds to use a Query-of-Queries for this.
			This way, however, takes only milliseconds:
			--->
			<cfset Variables.Description					= "(none)">
			<cfloop query="Variables.ActvIMUserSuspRsnTbl">
				<cfif Variables.ActvIMUserSuspRsnTbl.code	is Variables.VarValue>
					<cfset Variables.Description			= Variables.ActvIMUserSuspRsnTbl.description>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfoutput>
        #Variables.Description#</cfoutput>
		<cfelse>
			<cfoutput>
        <select #Variables.NameAndId#></cfoutput>
			<cfset Variables.DspOptsNotSelText				= "Not Suspended">
			<cfset Variables.DspOptsQueryName				= "Variables.ActvIMUserSuspRsnTbl">
			<cfset Variables.DspOptsSelList					= Variables.VarValue>
			<cfset Variables.DspOptsSkipList				= "">
			<cfset Variables.DspOptsTabs					= "#Request.SlafEOL#        ">
			<cfinclude template="#Variables.LibIncURL#/dsp_options.cfm">
			<cfoutput>
        </select></cfoutput>
		</cfif><!--- /view --->
		<cfoutput>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip of IMUserSuspRsnCd --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("IMUserSuspRsnDt", "ReasonDate", "Automatic Suspense Date",	"view",	"")>
	<cfelse>
		<cfset Defaults("IMUserSuspRsnDt", "ReasonDate", "Automatic Suspense Date",	"skip",	"")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="10" maxlength="10" placeholder="mm/dd/yyyy" onchange="
        var sMand											= isMand(this);
        if  (EditDate('#JSStringFormat(Variables.Eng)#', this.value, sMand))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "view">
        &nbsp;
        <a name="Anchor#Variables.VarName#" id="Anchor#Variables.VarName#"
        href="javascript:gCalendarPopup.select(document.getElementById('#Variables.VarName#'),'Anchor#Variables.VarName#','MM/dd/yyyy');">
        <img src="#Variables.LibURL#/images/CalendarPopup.gif" border="0"
        alt="Pick a date using a popup calendar. (Opens a new window.)"
        title="Pick a date using a popup calendar. (Opens a new window.)">
        </a></cfif>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /skip of IMUserSuspRsnDt (nested with IMUserSuspRsnCd's skip) --->
<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfif Variables.SelectedUser.ImUserId gt 0>
			<cfif Variables.IsSecAdmin or (Variables.IsSecurityUpdateProfile and Variables.CanUnlockUser)>
				<cfset Defaults("IMUserLockInd", "IMUserLockInd", "Lock Status",	"opt",	"LockInd")>
			<cfelse>
				<cfset Defaults("IMUserLockInd", "IMUserLockInd", "Lock Status",	"view",	"LockInd")>
			</cfif>
		</cfif>
	<cfelse>
		<cfset Defaults("IMUserLockInd", "IMUserLockInd", "Lock Status", "skip",	"LockInd")>
	</cfif>

	<cfif Variables.MandOpt is not "skip">
		<!--- original load or probably validation error occured on the page, but check box was checked --->
			<cfoutput>
	    	<div class="tbl_row">
	      		<div class="tbl_cell formlabel">#Variables.Label#</div>
	      		<div class="tbl_cell formdata nowrap">
	        	<div class="#Variables.MandOpt#data">
	      	</cfoutput>
	      	<cfif Variables.MandOpt is "view">
	      		<cfoutput><cfif Variables.Origs.DispIMUserLockInd eq "Y">Locked<cfelse>Unlocked</cfif></cfoutput>
             <cfelse>
				<cfif Variables.Origs.DispIMUserLockInd eq "Y">
					<cfset Variables.Checked0		= "checked">
					<cfset Variables.Checked1		= " ">
					<cfset Variables.Disabled0		= " ">
					<cfset Variables.Disabled1		= " ">
				<cfelse>
					<cfset Variables.Checked0		= "">
					<cfset Variables.Checked1		= "checked">
					<cfset Variables.Disabled0		= "disabled">
					<cfset Variables.Disabled1		= "disabled">
				</cfif>
				<!--- if validation error --->
				<cfif Variables.IMUserLockInd eq 1>
					<cfset Variables.Checked0		= " ">
					<cfset Variables.Checked1		= "checked">
					<cfset Variables.Disabled0		= " ">
					<cfset Variables.Disabled1		= " ">
				</cfif>

				<cfoutput>
		        <label class="#Variables.MandOpt#label">
			    <input type="radio"	 class="IMUserLockIndRdo" name="#Variables.VarNameLockInd#"	#Variables.Checked0# #Variables.disabled0#	value="0">Locked
			    <input type="radio" class="IMUserLockIndRdo" name="#Variables.VarNameLockInd#" #Variables.Checked1# #Variables.disabled1# value="1">Unlock<cfif Variables.Origs.DispIMUserLockInd eq "Y"> Account<cfelse>ed</cfif>
		        <br/>
		        </label></cfoutput>
	    	</cfif>
	    <cfoutput>
        	</div><!-- /#Variables.MandOpt#data -->
     		</div><!-- /formdata -->
    	</div><!-- /tbl_row --></cfoutput>
	</cfif><!---  end of  Variables.MandOpt is not "skip" check --->

<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
        <cfif Variables.SelectedUser.ImUserId gt 0>
            <cfif Variables.IsSecAdmin or (Variables.IsSecurityUpdateProfile and Variables.CanUnlockUser)>
                <cfset Defaults("IMUserAcctCmntTxt", "IMUserAcctCmntTxt", "Comments",	"mand",	"CmntTxt")>
            <cfelse>
                <cfset Defaults("IMUserAcctCmntTxt", "IMUserAcctCmntTxt", "Comments",	"skip",	"CmntTxt")>
            </cfif>
        <cfelse>
            <cfset Defaults("IMUserAcctCmntTxt", "IMUserAcctCmntTxt", "Comments",	"skip",	"CmntTxt")>
        </cfif>
    <cfelse>
        <cfset Defaults("IMUserAcctCmntTxt", "IMUserAcctCmntTxt", "Comments",	"skip",	"CmntTxt")>
    </cfif>
    <cfif Variables.MandOpt is not "skip">
        <cfoutput>
    <div class="tbl_row" id="IMUserAcctCmntTxtDiv" <cfif  Variables.IMUserLockInd eq 1>style="display:;"<cfelse>style="display:none;"</cfif>>
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <textarea #Variables.NameIdAndValue#
       class="rte">#Variables.VarValueCmntTxt#</textarea></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
 	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /CLS Login --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#

<!-- Login-Related -->
<script src="/library/rte/jquery.AllRichTextEditors.SBA.js?CachedAsOf=2013-12-17T22:10"></script>
<script src="/library/rte/jquery.tinymce.js?CachedAsOf=2013-08-21T18:16"></script><!-- 3.5.8 -->
<script src="/library/rte/jquery.tinymce.SBA.js?CachedAsOf=2013-12-17T21:41"></script>
<script>

<cfif Variables.MandOptActInact is not "skip">
$(function() // Shorthand for the document ready event.
	{
	var	sElt												= document.getElementById("#Variables.VarNameActInact#");
	if	(sElt)// (Won't exist if readonly profile.)
		sElt.onchange();
	});</cfif>

function DisplaySBAPasswordRules							()
	{
	alert("Your password must be complex and in compliance with SBA's password policy.\n"
		+ "A complex password must be a minimum of 8 characters long and contain \n"
		+ "at least 3 of the following 4 properties:\n"
		+ "  (1) Upper Case Letters A, B, C, ... Z\n"
		+ "  (2) Lower Case Letters a, b, c, ... z\n"
		+ "  (3) Numerals 0, 1, 2, ... 9\n"
		+ "  (4) Special Characters  { } [ ] < > : ? | \ ` ~ ! @ $ % ^ & * _ - + = .");
	return false;
	}

function DisplaySBAUsernameRules							()
	{
	alert("Your login User ID must be 8 to 15 characters long.\n"
		+ "No <, >,\',\", & or accented characters. \n");
	return false;
	}

function CnfrmChngToInctv (pThis) {
	var	OrgnlVlu	= '#Variables.Origs.IMUserActvInactInd#';
	var	NewValue	= $('##Status').val();
	<cfif not isDefined('PrevErr')>
	if (OrgnlVlu ==='A' && NewValue ==='I') {
		var cnfrmBox = confirm('The account will become inactive.\n None of other changes will be saved. \n Click "OK" to continue');
		if (cnfrmBox == true) {
	    	return true;
		}
		else {
			$('##Status').val(OrgnlVlu);
	    	return false;
		}
	}
	</cfif>
}

$(function () {
   	$("input[name='IMUserLockInd']").click(function () {
      	var lockRdoChecked = $(this).val();
		if	(lockRdoChecked == '1') {
		 	$('##IMUserAcctCmntTxtDiv').show();
		}
		else {
			$('##IMUserAcctCmntTxtDiv').hide();
		}
    });
});

function ShowSuspendAccountReason							(pThis)
	{
	var	sValue												= "A";
	if	(pThis.selectedIndex >= 0)
		sValue												= pThis.options[pThis.selectedIndex].value;
	if	(sValue === "I")
		$("##SuspendAccountReason").show();
	else
		$("##SuspendAccountReason").hide();
	}

</script>
</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

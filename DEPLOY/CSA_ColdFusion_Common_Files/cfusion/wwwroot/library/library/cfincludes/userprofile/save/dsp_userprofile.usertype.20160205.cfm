<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Included "group" of dsp_userprofile.
NOTES:				Called by dsp_userprofile.cfm.
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control
					what to do about individual form elements.
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be
					the current user:	Appended to the end of AppDataInline and JSInline.
REVISION HISTORY:	02/05/2016, NS; 	Added support to make check boxes, hqlockis and lock id readonly or disabled when role requests is pending.
					02/03/2016, RSD: 	Added Variables.SSNIsNotFilledIn which will handle the SSN switching to a
										mandatory form element vs. a display item if the SSN is not filled in yet.
					01/15/2016, SL:		Added Location dropdown box for partners, And allow SecAdmin to change it.
					12/30/2015, NS:		Added Note message on a Supervisor change section for existing Users.
					12/11/2015, SL:		Added link for looking up SBA office code.
					09/16/2015, IAC&SRS:The linc areas of interest are loaded in an IFRAME that does not have access to the variables on the calling page.
										The old code set session data to get around this. I think it will be cleaner with encoded data on the url. So rework
										what needs to be past to the IFRAME source. (IAC) Added HQLocNm to HQLocId display. Per RDW, removed TypeC and TypeS
										from the tbl_row for LocId. That is, LocId is not a field for SBAUsers. Even partners see it only in dsp_profile,
										and even then, as readonly. (SRS)
					09/10/2015, SRS:	Don't do DetermineLSP AJAX callback if HQLocId length is 0. Added debug trace to
										resolve weird bahaviors with Job checkboxes and ReadOnlyProfile display.
					08/21/2015, SRS:	Made the DetermineLSP AJAX callback also determine location name.
					07/30/2015, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.LincKey									= "ACwtchorcrwth">

<!--- Trace entry into this page, in case in (or not in) ListFieldsetsShow and/or ListFieldsetsSkip: --->

<cfif Variables.DoTraceJobs>
	<cfset Variables.TraceTxt								&= "UserType. ">
</cfif>
<cfif Variables.EndUserData.IMUserId gt 0 and isDefined("Url.PrevErr") and trim(Url.PrevErr) eq "Y">
	<cfif not isDefined("Variables.JobTitlCdPendApprLst")>
		<cfset Variables.JobTitlCdPendApprLst = Variables.Origs.JobTitlCdPendApprLst>
	</cfif>
</cfif>
<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#

<fieldset class="inlineblock TypeSensitive TypeC TypeN TypeS TypeU" style="display:none;"><!--- *NOT* TypeP (App-to-App) --->
  <legend><!--- No job titles for TypeP, so that's why it's disabled. --->
    <span class="TypeSensitive TypeC"						  style="display:none;">SBA Contractor</span>
    <span class="TypeSensitive TypeN"						  style="display:none;">Borrower</span>
    <span class="TypeSensitive TypeS"						  style="display:none;">SBA Employee</span>
    <span class="TypeSensitive TypeU"						  style="display:none;">Partner</span>
    Information
  </legend>

  <div class="tbl"></cfoutput>

	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("HQLocNm",	"HeadquartersName",	"Headquarters Location Name",	"opt",	"HQLNm")><!--- Not saved to IMUserTbl. --->
	<cfset Defaults("JobTitlCd","JobTitle",			"Job Classification",			"opt",	"JobTitl")>
	<cfif Variables.VarValueTypCd is "U">
		<cfset Defaults("HQLocId",	"HeadquartersLoc",	"Headquarters Location ID",	"mand",	"HQLoc")>
	<cfelse>
		<cfset Defaults("HQLocId",	"HeadquartersLoc",	"Headquarters Location ID",	"opt",	"HQLoc")>
	</cfif>
	<!--- Note: Did HQLocId last so that unprefixed generated names will be for HQLocId: --->
	<cfset Variables.Label									= "<label for="""
															& Variables.VarName
															& """ class=""hilab hqloc "
															& Variables.MandOpt
															& "label"">"
															& Variables.Eng
															& "</label>">
	<cfif Variables.MandOpt is not "skip">
		<cfif Request.SlafDevTestProd is "Dev">
			<cfset Variables.NameIdAndValueHQLoc			&= " title=""Dev-only msg: 9551 is valid; 9550 isn't; "
															&  "608278, 608302, 608277 and 608301 are valid and LSPs.""">
		</cfif>
		<!--- DispHQLocNm is different from VarValueHQLNm. DispHQLocNm may contain " (LSP)" and is also shown to user. --->
		<cfset Variables.DispHQLocNm						= Variables.VarValueHQLNm>
		<cfif ListFind(Variables.VarValueJobTitl, 6) gt 0><!--- Per RGG, presence of job 6 is enough, no s.p. call needed. --->
			<cfset Variables.DispHQLocNm					&= " (LSP)">
		</cfif>

		<cfoutput>

    <div class="tbl_row TypeSensitive TypeU" style="display:none;">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="hibox hqloc #Variables.MandOptHQLoc#data"><cfif Variables.MandOptHQLoc is "view">
        #Variables.VarValueHQLoc#<cfelse>
		<!--- make it readonly if some role requests are pending --->
		<cfif Variables.EndUserData.IMUserId gt 0
			  and listfindnocase( Variables.JobTitlCdPendApprLst, 3) gt 0
		>
			<cfset Variables.HqLocIdToReadonly = "readonly = true">
		<cfelse>
			<cfset Variables.HqLocIdToReadonly = "">
		</cfif>
       <input type="Text" class="hielt hqloc" #Variables.NameIdAndValueHQLoc#  #Variables.HqLocIdToReadonly#
        size="7" maxlength="7" onchange="return DoThisOnChange#Variables.VarNameHQLoc#(this);"></cfif>
        </div><!-- /#Variables.MandOptHQLoc#data --><cfif Variables.MandOpt is not "view">
        <a class="optlabel" target="_blank" href="/cls/cfincludes/dsp_lookup_lend.cfm?FField=#Variables.VarNameHQLoc#"
        title="Opens a new window with a lookup form. The results of this lookup are copied back into #Variables.EngHQLoc#.">Lookup</a></cfif>
        <b id="#Variables.VarNameHQLoc#Name">#Variables.DispHQLocNm#</b>
        <input type="Hidden" #Variables.NameIdAndValueHQLNm#>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt (for HQLocId) is not "skip" --->
    <!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
    <cfset Variables.MandOpt = Variables.PageIsProfile ? "opt" : "skip">

    <cfset Variables.CanUpdateLoc = 0>
    <cfif isDefined("Variables.IsSecAdmin") AND Variables.IsSecAdmin>
    	<cfset Variables.CanUpdateLoc = 1>
        <cfset Variables.MandOpt = "opt">

        <cfset Variables.Identifier = 3>
        <cfinclude template="/cls/cfincludes/get_userroles.cfm">

        <!--- <cfif getRoleLoc.recordcount LT 2> <!--- Don't show the dropdown when there is no or only 1 location --->
        	<cfset Variables.MandOpt = "skip">
        </cfif> --->

    	<cfif isDefined("url.PageTitle") AND url.PageTitle EQ "Add"> <!--- Don't show the dropdown when adding new user --->
			<cfset Variables.MandOpt = "skip">
        </cfif>

        <cfif isDefined("Variables.SelectedUser.HQLocId")>
        	<!--- <cfif find('165.110.95.90',cgi.REMOTE_ADDR)>
                <cfdump var="#Variables.SelectedUser#" label="Variables.SelectedUser"><br />            </cfif> --->

            <cfif Variables.VarValueHQLoc NEQ Variables.SelectedUser.HQLocId>
                <!--- SecAdmin is trying to change the HQ LOCATION, go ahead skip the location --->
                <cfset Variables.MandOpt = "skip">
            </cfif>
        </cfif>
    </cfif>

	<cfset Defaults("LocId", "LocId",	"Current Location", Variables.MandOpt, "")>

	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
            <div class="tbl_row TypeSensitive TypeU" style="display:none;">
              <div class="tbl_cell formlabel" id="div_LocLable">#Variables.Label#</div>
              <div class="tbl_cell formdata nowrap">
                <div class="#Variables.MandOpt#data">
					<cfif Variables.MandOpt is "opt">
                    	<cfif Variables.CanUpdateLoc>
							<!--- make it disabled if some role requests are pending --->
							<cfif 	Variables.EndUserData.IMUserId gt 0
									and listfindnocase( Variables.JobTitlCdPendApprLst, 3) gt 0
							>
								<cfset Variables.LocIdToDisable = "disabled = true">
							<cfelse>
								<cfset Variables.LocIdToDisable = "  ">
							</cfif>
                            <Select #Variables.NameAndId#  #Variables.LocIdToDisable#>
                                <option value="">Not Selected Yet </option>
                                <cfif getRoleLoc.recordcount>
                                    <cfset temp = "">
                                    <cfset tempList = "">
                                    <cfloop query="getRoleLoc">
                                        <cfif NOT listFind(tempList,getRoleLoc.LOCID)>
                                        <cfset tempList = listAppend(tempList,LOCID)>
                                        <option value="#getRoleLoc.LOCID#"  <cfif Variables.VarValue EQ getRoleLoc.LOCID>selected="selected"</cfif>>#getRoleLoc.PRTLOCNM# (#getRoleLoc.LOCID#)</option>
                                        </cfif>
                                    </cfloop>
                                </cfif>
                            </select>
							<!--- since select box is disabled...  --->
							<cfif listfindnocase( Variables.JobTitlCdPendApprLst, 3) gt 0 and Variables.ImUserTypCd eq "U">
								<input type="hidden" #Variables.NameIdAndValue#>
							</cfif>
                        <cfelse>
                        	#Variables.VarValue#
                        </cfif>
                    <cfelse>
                        <input type="Text" #Variables.NameIdAndValue#
                        size="7" maxlength="7" onchange="
                        var sMand											= isMand(this);
                        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 1, this.maxLength))
                            return true;
                        this.focus();
                        return false;
                        ">
                    </cfif>
                </div><!-- /#Variables.MandOpt#data --><cfif Variables.MandOpt is not "opt">
                <a class="optlabel" target="_blank" href="/cls/cfincludes/dsp_lookup_lend.cfm?FField=#Variables.VarName#"
                title="Opens a new window with a lookup form. The results of this lookup are copied back into #Variables.Eng#.">Lookup</a></cfif>
              </div><!-- /formdata -->
            </div><!-- /tbl_row -->
		</cfoutput>
	</cfif><!--- /MandOpt (for LocId) is not "skip" --->

	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.PageIsUser>
		<cfset Defaults("SBAOfcCd",	"SBAOfficeCode", "Default Office Code", "opt",	"")><!--- SecAdmins may enter, but optional. --->
	<cfelseif Variables.PageIsProfile>
		<cfset Defaults("SBAOfcCd",	"SBAOfficeCode", "Default Office Code", "view",	"")><!--- Existing can't enter, but allowed to see. --->
	<cfelse>
		<cfset Defaults("SBAOfcCd",	"SBAOfficeCode", "Default Office Code", "skip",	"")><!--- New can't enter and can't already have one. --->
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
            <div class="tbl_row TypeSensitive TypeC TypeS" style="display:none;">
              <div class="tbl_cell formlabel">#Variables.Label#</div>
              <div class="tbl_cell formdata nowrap">
                <div class="#Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
                #Variables.VarValue#<cfelse>
                <input type="Text" #Variables.NameIdAndValue#
                size="4" maxlength="4" onchange="
                var sMand											= isMand(this);
                if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 4, this.maxLength))
                    return true;
                this.focus();
                return false;
                "></cfif>
                </div><!-- /#Variables.MandOpt#data -->
                <cfif Variables.MandOpt is not "view">
                <a class="optlabel" target="_blank" href="/cls/cfincludes/dsp_lookup_ofccd.cfm?FField=#Variables.VarName#"
                title="Opens a new window with a lookup form. The results of this lookup are copied back into #Variables.Eng#.">Lookup</a></cfif>
              </div><!-- /formdata -->
            </div><!-- /tbl_row -->
		</cfoutput>
	</cfif><!--- /MandOpt (for SBAOfcCd) is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.MandOptJobTitl is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelJobTitl#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptJobTitl#data">
        <!-- Using style="display:none;" because jQuery 3.0+ .show()/.hide() will break on display:none in a class: --></cfoutput>
		<cfif Variables.DoTraceJobs>
			<cfset Variables.TraceTxt						&= "Jobs at load: '#Variables.VarValueJobTitl#'. ">
			<cfif IsDefined('GetTitl.JobTitlCd')>
				<cfset Variables.TraceTxt					&= "GetTitl.RecordCount: '#GetTitl.RecordCount#'. ">
			</cfif>
		</cfif>
		<!---
		Interesting problem here: What if UserType is editable, but JobTitlCd isn't? You can't check a checkbox
		if there isn't any checkbox. So we should only generate spans of viewdata if the user currently has the
		JobTitlCd (and can't change it).
		--->
		<cfloop query="Variables.ActvIMJobTitlTypTbl">
			<cfset Variables.DisplayPendLblStl = "none;">

			<cfif Variables.MandOptJobTitl is "view">
				<cfif ListFindNoCase(Variables.VarValueJobTitl, Variables.ActvIMJobTitlTypTbl.code) gt 0>
					<cfoutput>
				        <span class="JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
				          #Variables.ActvIMJobTitlTypTbl.description#<br/>
				        </span>
					</cfoutput>
				</cfif>
			<cfelse>
				<cfif ListFindNoCase(Variables.VarValueJobTitl, Variables.ActvIMJobTitlTypTbl.code) gt 0>
					<cfset Variables.Checked				= "checked">
				<cfelse>
					<cfset Variables.Checked				= "       ">
				</cfif>
				<cfif Variables.ActvIMJobTitlTypTbl.code	is 6><!--- 6 = LSP --->
					<cfset Variables.ReadOnly				= "readonly">
					<cfsavecontent variable="Variables.MaybeForceReadonly">
						<cfoutput>// In case checkbox readonly is not supported by the browser, such as Firefox:
          		this.checked = ((gUserType === 'U') ? gLSP : false); // Job Classification 6 is Partner-only.
          		// Because this readonly behavior isn't otherwise explained, give an explanation if someone clicks here a lot, but only *real* clicks:
          		if  (typeof arguments[0] !== 'string')// In JS, call with .onclick('bypass explanation'), so typeof will be 'string'.
              	  if  ((++gJobCat6ClickCount) > 3)	// In real clicks, typeof will be 'object'. Only real clicks bump gJobCat6ClickCount and maybe do alert.
                  alert('Clicking repeatedly on the #JSStringFormat(Variables.ActvIMJobTitlTypTbl.description)# checkbox won\'t cause it to get checked or unchecked. \n'
                      + 'This Job Classification is set automatically (and only) if the #JSStringFormat(Variables.EngHQLoc)# is an LSP.');
          		// In case checked/unchecked has show/hide consequences: </cfoutput>
					</cfsavecontent>
				<cfelseif Variables.ActvIMJobTitlTypTbl.code is 2 and listfindnocase( Variables.JobTitlCdPendApprLst, 2) gt 0><!--- 2 SBA official --->
					<cfif Variables.Checked  eq "Checked">
		          		<cfset Variables.DisplayPendLblStl = "">
		          	</cfif>
					<cfset Variables.ReadOnly				= "readonly">
					<cfsavecontent variable="Variables.MaybeForceReadonly">
				<cfoutput>
				this.checked = ((gUserType === 'S') ? true : false );
	          	if  (typeof arguments[0] !== 'string')
	              if  ((++gJobCat2ClickCount) > 3)
	                  alert('Clicking repeatedly on the #JSStringFormat(Variables.ActvIMJobTitlTypTbl.description)# checkbox won\'t cause it to get checked or unchecked. \n'
	                      + 'This checkbox is readonly until respective role requests will be approved for #JSStringFormat(Variables.EngHQLoc)#');
	          	</cfoutput>
	          	</cfsavecontent>
	          	<cfelseif Variables.ActvIMJobTitlTypTbl.code is 1 and listfindnocase( Variables.JobTitlCdPendApprLst, 1) gt 0><!--- 1 - COTR --->
	          		<cfif Variables.Checked  eq "Checked">
		          		<cfset Variables.DisplayPendLblStl = "">
		          	</cfif>
					<cfset Variables.ReadOnly			= "readonly">
					<cfsavecontent variable="Variables.MaybeForceReadonly">
					<cfoutput>
					this.checked = ((gUserType === 'S') ? true : false);
					if  (typeof arguments[0] !== 'string')
			              if  ((++gJobCat1ClickCount) > 3)
			                  alert('Clicking repeatedly on the #JSStringFormat(Variables.ActvIMJobTitlTypTbl.description)# checkbox won\'t cause it to get checked or unchecked. \n'
			                       + 'This checkbox is readonly until respective role requests will be approved for #JSStringFormat(Variables.EngHQLoc)#');
					</cfoutput>
					</cfsavecontent>
				<cfelseif Variables.ActvIMJobTitlTypTbl.code is 3 and listfindnocase( Variables.JobTitlCdPendApprLst, 3) gt 0><!--- 3 - AO --->
					<cfif Variables.Checked  eq "Checked">
		          		<cfset Variables.DisplayPendLblStl = "">
		          	</cfif>
					<cfset Variables.ReadOnly				= "readonly">
					<cfsavecontent variable="Variables.MaybeForceReadonly">
					<cfoutput>
					this.checked = ((gUserType === 'U') ? true : false);
					if  (typeof arguments[0] !== 'string')
			              if  ((++gJobCat3ClickCount) > 3)
			                  alert('Clicking repeatedly on the #JSStringFormat(Variables.ActvIMJobTitlTypTbl.description)# checkbox won\'t cause it to get checked or unchecked. \n'
			                      + 'This checkbox is readonly until respective role requests will be approved for #JSStringFormat(Variables.EngHQLoc)#');
					</cfoutput>
					</cfsavecontent>
				<cfelse>
					<cfset Variables.ReadOnly				= "        ">
					<cfset Variables.MaybeForceReadonly		= "// In case checked/unchecked has show/hide consequences: ">
				</cfif>
				<cfoutput>
        <label class="#Variables.MandOptJobTitl#label JobTitle Type#Variables.ActvIMJobTitlTypTbl.IMUserTypCd#" style="display:none;">
		<input type="Checkbox" class="JobChk" name="#Variables.VarNameJobTitl#" #Variables.Checked# #Variables.ReadOnly# value="#Variables.ActvIMJobTitlTypTbl.code#" onclick="
          #Variables.MaybeForceReadonly#
		  if  (this.checked){
              	$('.Job'+this.value).show();
				}
          else {
              	$('.Job'+this.value).hide();
			}
          ">
          #Variables.ActvIMJobTitlTypTbl.description# <span id="chkBx#Variables.ActvIMJobTitlTypTbl.code#" style="display: #Variables.DisplayPendLblStl#;background-color:##d3d3d3;">&nbsp; - &nbsp;Pending approval</span><br/>
        </label></cfoutput>
			</cfif>
		</cfloop>
		<input type="hidden" name="JobTitlCdPendApprLst" value="#Variables.JobTitlCdPendApprLst#">
		<cfoutput>
        <div class="viewdata JobTitle TypeNone" style="display:none;">
          (none<cfif Variables.MandOptJobTitl is not "view"> for this type</cfif>)
        </div></cfoutput>
		<cfoutput>
        </div><!-- /#Variables.MandOptJobTitl#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt (for JobTitlCd) is not "skip" --->
	<cfoutput>
  </div><!-- /tbl-->
</fieldset><!-- /User Information --><br class="TypeSensitive TypeC TypeN TypeS TypeU" style="display:none;" />

<div class="TypeSensitive TypeN" style="display:none;">

<fieldset class="inlineblock JobSensitive Job8" style="display:none;">
  <legend>Bond Borrower</legend>
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DUNS",	"DUNSNumber",	"DUNS Number", "mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
        <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data">
        <input type="Text" #Variables.NameIdAndValue# size="9" maxlength="9" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.Eng)#', this.value, '9', sMand, 9, 9))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt (for DUNS) is not "skip" --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DoEIN1",	"DoEIN1",	"EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
			<cfset Defaults("EIN1",	"EIN1",	"First 2 digits of EIN",	"mand", "Tax7")>
			<cfset Defaults("EIN2",	"EIN2",	"Last 7 digits of EIN",		"mand", "Tax8")>
			<cfoutput><cfif Variables.MandOptTax7 is "view">
        #Variables.VarValueTax7#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax7#">#Variables.EngTax7#</label>
        <input type="password" #Variables.NameIdAndValueTax7# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax7)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax8 is "view">
        #Variables.VarValueTax8#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax8#">#Variables.EngTax8#</label>
        <input type="Text" #Variables.NameIdAndValueTax8# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax8)#s',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Defaults("DoEIN2",	"DoEIN2",	"Re-enter EIN",	"mand", "")>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("EIN3",	"EIN3",	"Re-enter First 2 digits of EIN",	"mand", "Tax9")>
		<cfset Defaults("EIN4",	"EIN4",	"Re-enter Last 7 digits of EIN",	"mand", "Taxa")>
		<cfoutput><cfif Variables.MandOptTax9 is "view">
        #Variables.VarValueTax9#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax9#">#Variables.EngTax9#</label>
        <input type="password" #Variables.NameIdAndValueTax9# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTax9)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTaxa is "view">
        #Variables.VarValueTaxa#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTaxa#">#Variables.EngTaxa#</label>
        <input type="Text" #Variables.NameIdAndValueTaxa# size="7" maxlength="7" onchange="
        var sMand											= isMand(this);
        if  (EditMask	('#JSStringFormat(Variables.EngTaxa)#',this.value, '9', sMand, 7, 7))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(99-9999999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /EIN2 --->
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /JobSensitive Job8 (Bond Borrower) --><br class="JobSensitive Job8" style="display:none;" />

<fieldset class="inlineblock JobSensitive Job7" style="display:none;">
  <legend>Loan Borrower</legend>
  <div class="tbl"></cfoutput>
	<cfif isDefined("GetUSSN.IMUSERSSNNMB") and len(GetUSSN.IMUSERSSNNMB) gt 0>
		<cfset Variables.SSNIsNotFilledIn = "No">
	<cfelse>
		<cfset Variables.SSNIsNotFilledIn = "Yes">
	</cfif>
	<cfoutput><input type="hidden" name="SSNIsNotFilledIn" value="#Variables.SSNIsNotFilledIn#"></cfoutput>
	<cfif Variables.EndUserData.IMUserId is 0 OR Variables.SSNIsNotFilledIn><!--- dsp_addcustomer, or dsp_user entering a new user. --->
		<cfset Defaults("DoSSN1",	"DoSSN1",	"SSN",	"mand", "")>
	<cfelse>
		<cfset Defaults("DoSSN1",	"DoSSN1",	"SSN",	"view", "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfif Variables.EndUserData.IMUserId is 0 OR Variables.SSNIsNotFilledIn><!--- dsp_addcustomer, or dsp_user entering a new user. --->
			<cfset Defaults("TaxId1",	"TaxId1",	"First 3 digits of SSN",	"mand", "Tax1")>
			<cfset Defaults("TaxId2",	"TaxId2",	"Middle 2 digits of SSN",	"mand", "Tax2")>
			<cfset Defaults("TaxId3",	"TaxId3",	"Last 4 digits of SSN",		"mand", "Tax3")>
		<cfelse>
			<cfset Defaults("TaxId1",	"TaxId1",	"First 3 digits of SSN",	"view", "Tax1")>
			<cfset Defaults("TaxId2",	"TaxId2",	"Middle 2 digits of SSN",	"view", "Tax2")>
			<cfset Defaults("TaxId3",	"TaxId3",	"Last 4 digits of SSN",		"view", "Tax3")>
		</cfif>
		<cfoutput><cfif Variables.MandOptTax1 is "view">
        ***<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax1#">#Variables.EngTax1#</label>
        <input type="password" #Variables.NameIdAndValueTax1# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax1)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax2 is "view">
       **<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax2#">#Variables.EngTax2#</label>
        <input type="password" #Variables.NameIdAndValueTax2# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax2)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax3 is "view">
        #Variables.VarValueTax3#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax3#">#Variables.EngTax3#</label>
        <input type="Text" #Variables.NameIdAndValueTax3# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax3)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN1 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0 OR Variables.SSNIsNotFilledIn><!--- dsp_addcustomer, or dsp_user entering a new user. --->
		<cfset Defaults("DoSSN2",	"DoSSN2",	"Re-enter SSN",	"mand", "")>
	<cfelse>
		<cfset Defaults("DoSSN2",	"DoSSN2",	"Re-enter SSN",	"skip", "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="#Variables.MandOpt#label">#Variables.Eng#</span></div>
      <div class="tbl_cell formdata nowrap">
        <div class="inlineblock #Variables.MandOpt#data"></cfoutput>
		<cfset Defaults("TaxId4",	"TaxId4",	"Re-enter First 3 digits of SSN",	"mand", "Tax4")>
		<cfset Defaults("TaxId5",	"TaxId5",	"Re-enter Middle 2 digits of SSN",	"mand", "Tax5")>
		<cfset Defaults("TaxId6",	"TaxId6",	"Re-enter Last 4 digits of SSN",	"mand", "Tax6")>
		<cfoutput><cfif Variables.MandOptTax4 is "view">
        #Variables.VarValueTax4#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax4#">#Variables.EngTax4#</label>
        <input type="password" #Variables.NameIdAndValueTax4# size="3" maxlength="3" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax4)#',this.value, '9', sMand, 3, 3))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax5 is "view">
        #Variables.VarValueTax5#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax5#">#Variables.EngTax5#</label>
        <input type="password" #Variables.NameIdAndValueTax5# size="2" maxlength="2" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax5)#',this.value, '9', sMand, 2, 2))
            return true;
        this.focus();
        return false;
        "></cfif>-<cfif Variables.MandOptTax6 is "view">
        #Variables.VarValueTax6#<cfelse>
        <label class="offscreen" for="#Variables.VarNameTax6#">#Variables.EngTax6#</label>
        <input type="Text" #Variables.NameIdAndValueTax6# size="4" maxlength="4" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngTax6)#',this.value, '9', sMand, 4, 4))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!--- /manddata --->
        &nbsp;&nbsp;(999-99-9999)
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	</cfif><!--- /SSN2 --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif Variables.EndUserData.IMUserId is 0 OR Variables.SSNIsNotFilledIn><!--- dsp_addcustomer, or dsp_user entering a new user. --->
		<cfset Defaults("LoanNmb",	"LoanNumber",	"Loan Number", "mand", "")>
	<cfelse>
		<cfset Defaults("LoanNmb",	"LoanNumber",	"Loan Number", "view", "")>
	</cfif>
	<cfif Variables.MandOpt is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.Label#</div>
      <div class="tbl_cell formdata" style="white-space:nowrap;">
        <div class="inlineblock #Variables.MandOpt#data"><cfif Variables.MandOpt is "view">
        #Variables.VarValue#<cfelse>
        <input type="Text" #Variables.NameIdAndValue# size="11" maxlength="11">

		 <!--- removing the requirement that the loan number be an etran loan number
		 onchange="
        var sMand											= isMand(this);
        if  (EditLoanNmb('#JSStringFormat(Variables.Eng)#', this.value, sMand, true))
            return true;
        this.focus();
        return false;
        " --->

		</cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /JobSensitive Job7 (Loan Borrower) --><br class="JobSensitive Job7" style="display:none;" />

</div><!-- /TypeSensitive TypeN -->
</cfoutput>

<!--- Per RGG, display LINC areas of interest only if this is an existing profile: --->

<cfif Variables.EndUserData.IMUserId gt 0>
	<cfset Variables.Input									= "LENDRUSERNM=#Variables.Origs.IMUserNm#"
															& "&CreatUserId=#Variables.IMUserTbl.IMUSERNM#"
															& "&TYPE=1">
	<cfset Variables.URLinput								= URLEncodedFormat(Encrypt(Variables.Input, Variables.LincKey, "CFMX_COMPAT", "hex"))>
	<cfoutput>

<fieldset class="inlineblock JobSensitive Job4" style="display:none;">
  <legend>LINC Areas of Interest</legend>
  <p>
    Now is a good time to enter the Areas of Interest for the LINC Program.
  </p>
  <ul>
    <li>Selecting a state will popup a window with all the counties in that state.</li>
    <li>You can use the <strong>Select All</strong> button to check all counties. Or select individual counties.</li>
    <li>The <strong>Done</strong> button will return you to the list of states.</li>
    <li>When you have added all the states and counties you need. Press the <strong>Save Changes</strong> button.</li>
    <li>When the system has finshed processing the items an alert will tell you that the page can be safely closed.</li>
  </ul>

  <iframe src="#Variables.LincAreasURL#/areasofoperation.cfm?INPUT=#Variables.URLinput#"
          name="Upload IFrame" id="Upload Frame"
          width="680" height="440" marginwidth="2" marginheight="2">Please use a browser that supports IFrame.</iframe>
</fieldset><!-- /JobSensitive Job4 (LINC Contact) --><br class="JobSensitive Job4" style="display:none;" />
</cfoutput>
</cfif><!--- /EndUserData.IMUserId gt 0 --->

<cfoutput>

<fieldset class="inlineblock TypeSensitive TypeC TypeS TypeU" style="display:none;"><!--- Only the types with supervisors. --->
  <legend>
	<span class="TypeSensitive TypeC"      style="display:none;">SBA Contracting Officer Representative </span>
    <span class="TypeSensitive TypeS"      style="display:none;">SBA Official</span>
    <span class="TypeSensitive TypeU"      style="display:none;">Lender's Authorizing Official</span>
    (Lookup)
  </legend>
  <span class="TypeSensitive TypeC nowrap" style="display:none;">
    Also known as Contracting Officer Representative, or "COR".<br/>
    <hr/>
  </span>
  <span class="TypeSensitive TypeU nowrap" style="display:none;">
    Also known as Lender's Authorizing Official, or "LAO". Your LAO will have <br/>
    the same "#Variables.EngHQLoc#" as you, so please enter that first (above). <br/>
    <hr/>
  </span>
  <div class="inlineblock dem_errmsg" id="SupervisorInvalidMsg" style="display:<cfif Variables.Origs.SuprvInd is "A">none</cfif>;">
  Supervisor information is invalid for the currently selected User Type. <br/>
  Please use the following lookup form to find and select another supervisor.
  </div><cfif NOT Variables.ReadOnlyProfile>
  <div align="left" class="pad10">
    Note<cfif Variables.EndUserData.IMUserId gt 0>s</cfif>:
		<cfif Variables.EndUserData.IMUserId gt 0>
		<ol><li>This account will be temporarily suspended once the Supervisor Change request is submitted.<br>
		This account will be unsuspended once the newly selected Supervisor completes the Account Authentication process.</li><li></cfif>
		In the following, "Partial" means "starts with":<cfif Variables.EndUserData.IMUserId gt 0></li></ol></cfif>
  </div></cfif><!--- /NOT ReadOnlyProfile --->
  <div class="tbl"></cfoutput>
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfset Variables.SuprvEmail								= "">
	<cfset Variables.SuprvFirst								= "">
	<cfset Variables.SuprvLast								= "">
	<cfif Variables.ReadOnlyProfile>
		<cfset Defaults("SuprvEmail", "SupervisorsEmail", "Supervisor's Partial E-Mail",     "skip", "SupE")>
		<cfset Defaults("SuprvFirst", "SupervisorsFirst", "Supervisor's Partial First Name", "skip", "SupF")>
		<cfset Defaults("SuprvLast",  "SupervisorsLast",  "Supervisor's Partial Last Name",  "skip", "SupL")>
	<cfelse>
		<cfset Defaults("SuprvEmail", "SupervisorsEmail", "Supervisor's Partial E-Mail",     "opt", "SupE")>
		<cfset Defaults("SuprvFirst", "SupervisorsFirst", "Supervisor's Partial First Name", "opt", "SupF")>
		<cfset Defaults("SuprvLast",  "SupervisorsLast",  "Supervisor's Partial Last Name",  "opt", "SupL")>
	</cfif><!--- /ReadOnlyProfile --->
	<cfif Variables.MandOptSupE is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupE#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOpt#data"><cfif Variables.MandOptSupE is "view">
        #Variables.VarValueSupE#<cfelse>
        <input type="Text" #Variables.NameIdAndValueSupE#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        // Don't use EditMask 'E' here, because it forces user to enter up through the @-sign:
        if  (EditMask('#JSStringFormat(Variables.EngSupE)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptSupE#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOptSupE is not "skip" --->
	<cfif Variables.MandOptSupF is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupF#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptSupF#data"><cfif Variables.MandOptSupF is "view">
        #Variables.VarValueSupF#<cfelse>
        <input type="Text" #Variables.NameIdAndValueSupF#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngSupF)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOptSupF#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<cfif Variables.MandOptSupL is not "skip">
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">#Variables.LabelSupL#</div>
      <div class="tbl_cell formdata nowrap">
        <div class="#Variables.MandOptSupL#data"><cfif Variables.MandOptSupL is "view">
        #Variables.VarValueSupL#<cfelse>
        <input type="Text" #Variables.NameIdAndValue#
        size="40" maxlength="128" onchange="
        var sMand											= isMand(this);
        if  (EditMask('#JSStringFormat(Variables.EngSupL)#', this.value, 'X', sMand, 1, this.maxLength))
            return true;
        this.focus();
        return false;
        "></cfif>
        </div><!-- /#Variables.MandOpt#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /MandOpt is not "skip" --->
	<cfif NOT Variables.ReadOnlyProfile>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Button" name="JustAButton" id="LookupSupervisorButton" value="Lookup Supervisor" onclick="DoLookupSupervisor(this)">
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <select name="SupervisorDropdown" id="SupervisorDropdown" onchange="DoChooseSupervisor(this)">
        <option value="">Supervisors Appear Here After Lookup</option></cfoutput>
		<cfif	(Len(Variables.Origs.IMUserSuprvFirstNm)	gt 0)
			or	(Len(Variables.Origs.IMUserSuprvLastNm)		gt 0)>
			<cfset Variables.OptionValue					=  "#Variables.Origs.IMUserSuprvId#"
															& "|#Variables.Origs.IMUserSuprvFirstNm#"
															& "|#Variables.Origs.IMUserSuprvMidNm#"
															& "|#Variables.Origs.IMUserSuprvLastNm#"
															& "|#Variables.Origs.IMUserSuprvEmailAdrTxt#"
															& "|#Variables.Origs.IMUserSuprvUserId#">
			<cfset Variables.OptionText						= Variables.Origs.IMUserSuprvLastNm
															& ", "
															& Variables.Origs.IMUserSuprvFirstNm>
			<!--- Prebuilt OptionText doesn't have IMUserTypDescTxt returned by IMUserSuprvsrSelCSP in callback. (No big deal.) --->
			<cfoutput>
        <option selected value="#Variables.OptionValue#">#Variables.OptionText#</option></cfoutput>
		</cfif>
		<cfoutput>
        </select>
        <span id="SupervisorDropdownCount"></span>
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /NOT ReadOnlyProfile (= no form fields, can't do a lookup) --->
	<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel mandlabel">
        <span class="TypeSensitive TypeC" style="display:none;">Supervisor<br/>(SBA Contracting Officer Representative)</span>
        <span class="TypeSensitive TypeS" style="display:none;">SBA Official</span>
        <span class="TypeSensitive TypeU" style="display:none;">Supervisor<br/>(Lender's Authorizing Official)</span>
      </div>
      <div class="tbl_cell formdata manddata nowrap"><!-- Side-by-Side --></cfoutput>
		<cfset Variables.MandOptIMUserSuprvUserId			= "skip"><!--- Per SMM, never show it. --->
		<cfif Variables.PageIsUser>
			<cfset Defaults("IMUserSuprvId",				"SuperTextKey",	"Supervisor's ID",				"opt",	"SupId1")>
			<cfset Defaults("IMUserSuprvFirstNm",			"SuperFirst",	"Supervisor's First Name",		"mand",	"SupFirst")>
			<cfset Defaults("IMUserSuprvMidNm",				"SuperMiddle",	"Supervisor's Middle Name",		"opt",	"SupMid")>
			<cfset Defaults("IMUserSuprvLastNm",			"SuperLast",	"Supervisor's Last Name",		"mand",	"SupLast")>
			<cfset Defaults("IMUserSuprvEmailAdrTxt",		"SuperEMail",	"Supervisor's EMail Address",	"mand",	"SupEMail")>
			<cfset Defaults("IMUserSuprvUserId",			"SuperNumKey",	"Supervisor's ID2",				"skip",	"SupId2")>
		<cfelse>
			<cfset Defaults("IMUserSuprvId",				"SuperTextKey",	"Supervisor's ID",				"view",	"SupId1")>
			<cfset Defaults("IMUserSuprvFirstNm",			"SuperFirst",	"Supervisor's First Name",		"view",	"SupFirst")>
			<cfset Defaults("IMUserSuprvMidNm",				"SuperMiddle",	"Supervisor's Middle Name",		"view",	"SupMid")>
			<cfset Defaults("IMUserSuprvLastNm",			"SuperLast",	"Supervisor's Last Name",		"view",	"SupLast")>
			<cfset Defaults("IMUserSuprvEmailAdrTxt",		"SuperEMail",	"Supervisor's EMail Address",	"view",	"SupEMail")>
			<cfset Defaults("IMUserSuprvUserId",			"SuperNumKey",	"Supervisor's ID2",				"skip",	"SupId2")>
		</cfif>
		<!---
		SMM doesn't want to see "Super" or "Supervisor's" or "S." on the web page labels. But they really should be qualified in the "EngName" key generated by
		Defaults. That way the JavaScript and val_xxx errors are qualified as referring to supervisor. So the following produces a web page label that's more
		to Sheri's liking:
		--->
		<cfloop index="Key" list="SupId1,SupFirst,SupMid,SupLast,SupEMail,SupId2">
			<!--- Label isn't built if the field is skipped. But that's okay, because then it also doesn't need to have a WebPageLabel: --->
			<cfif Variables["MandOpt#Key#"]					is not "skip">
				<cfset Variables["WebPageLabel#Key#"]		= Replace(Replace(Replace(Variables["Label#Key#"],
															"Supervisor's ", "", "One"), " Name", "", "One"), " Address", "", "One")>
			</cfif>
		</cfloop>
		<cfoutput>
        <div style="background-color: white;">
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupId1#" class="#Variables.MandOptSupId1#data"><cfif Variables.MandOptSupId1 is "view">#Variables.VarValueSupId1#<cfelse>
          <input type="Text" data-type="text" maxlength="15" size="10"
          #Variables.NameIdAndValueSupId1# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupId1)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupId1#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupFirst#" class="#Variables.MandOptSupFirst#data"><cfif Variables.MandOptSupFirst is "view">#Variables.VarValueSupFirst#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupFirst# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupFirst)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupFirst#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupMid#" class="#Variables.MandOptSupMid#data"><cfif Variables.MandOptSupMid is "view">#Variables.VarValueSupMid#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupMid# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupMid)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupMid#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupLast#" class="#Variables.MandOptSupLast#data"><cfif Variables.MandOptSupLast is "view">#Variables.VarValueSupLast#<cfelse>
          <input type="Text" data-type="text" maxlength="50" size="20"
          #Variables.NameIdAndValueSupLast# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupLast)#', this.value, 'X', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupLast#)
        </div><!-- /Vertical -->
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupEMail#" class="#Variables.MandOptSupEMail#data"><cfif Variables.MandOptSupEMail is "view">#Variables.VarValueSupEMail#<cfelse>
          <input type="Text" data-type="text" maxlength="255" size="30"
          #Variables.NameIdAndValueSupEMail# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupEMail)#', this.value, 'E', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupEMail#)
        </div><!-- /Vertical --><cfif Variables.MandOptSupId2 is not "skip">
        <div class="inlineblock"><!-- Vertical -->
          <div id="Box#Variables.VarNameSupId2#" class="#Variables.MandOptSupId2#data"><cfif Variables.MandOptSupId2 is "view">#Variables.VarValueSupId2#<cfelse>
          <input type="Text" data-type="text" maxlength="10" size="5"
          #Variables.NameIdAndValueSupId2# onchange="
          var sMand											= isMand(this);
          if  (EditMask('#JSStringFormat(Variables.EngSupId2)#', this.value, '9', sMand, 1, this.maxLength))
              return true;
          this.focus();
          return false;
          "></cfif></div><br/>
          (#Variables.WebPageLabelSupId2#)
        </div><!-- /Vertical --></cfif><cfif								 Variables.MandOptSupId1   is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupId1  #></cfif><cfif Variables.MandOptSupFirst is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupFirst#></cfif><cfif Variables.MandOptSupMid   is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupMid  #></cfif><cfif Variables.MandOptSupLast  is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupLast #></cfif><cfif Variables.MandOptSupEMail is "view">
        <input type="Hidden" #Variables.NameIdAndValueSupEMail#></cfif>
        <input type="Hidden" #Variables.NameIdAndValueSupId2  #>
        </div><!-- /background-color: white; -->
      </div><!-- /formdata, Side-by-Side -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</fieldset><!-- /Supervisor --><br class="TypeSensitive TypeC TypeS TypeU" style="display:none;" />
</cfoutput>
</cfsavecontent>

<!---
The following allows us to set gLSP appropriately for the current HQLocId, if any. This is mainly important on returns
to this page after an error in the act page. Without the following code, gLSP won't reflect the LSP status of HQLocId,
and any click on Job Classification 6 will set "checked" incorrectly. (See the onclick of Job Classification 6.)
--->

<cfset Variables.HQLocIdIsLSP								= "false">
<cfif Variables.DisplayingFormDataEditedByUser>
	<!---
	We can only rely on Varibles.((columnname)) to exist when DisplayingFormDataFromDatabase (= first pass).
	Here's a technique to restore them when DisplayingFormDataEditedByUser (= after return from act page):
	--->
	<cfset Variables.IMUserTypCd							= Variables.VarValueTypCd><!--- from Form Data Recovery by way of Defaults UDF. --->
	<cfset Variables.HQLocId								= Variables.VarValueHQLoc><!--- from Form Data Recovery by way of Defaults UDF. --->
	<cfset Variables.IMUserId								= Variables.EndUserData.IMUserId><!--- not subject to Form Data Recovery, not a form element. --->
</cfif>
<cfif	(Variables.IMUserTypCd is "U")
	and	(IsNumeric(Variables.HQLocId))
	and	(IsNumeric(Variables.IMUserId))>
	<cfif Variables.IMUserId is "0">
		<cfset Variables.IMUserId							= ""><!--- What the stored procedure wants. --->
	</cfif>
	<cfset Variables.LSPLocIdCnt							= ""><!--- Initialize output parameter, deliberately making it non-numeric. --->
	<cfset Variables.cfprname								= "NoResultSetReturned">
	<cfset Variables.LogAct									= "determine whether #Variables.HQLocId# is a Lender Service Provider">
	<cfinclude template="/cfincludes/oracle/security/spc_LSPLOCIDSELCSP.cfm">
	<cfif Variables.IMUserId is ""><!--- What the stored procedure wanted, but not what our ColdFusion code likes. --->
		<cfset Variables.IMUserId							= "0"><!--- Restore the zero. --->
	</cfif>
	<cfif	(NOT Variables.TxnErr)
		and	(IsNumeric(Variables.LSPLocIdCnt))
		and	(Variables.LSPLocIdCnt gt 0)>
		<cfset Variables.HQLocIdIsLSP						= "true">
	</cfif>
</cfif>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#

<!-- User Type Related: -->

<script>

gJobCat1ClickCount											= 0;
gJobCat2ClickCount											= 0;
gJobCat3ClickCount											= 0;
gJobCat6ClickCount											= 0;
gLSP														= #Variables.HQLocIdIsLSP#;
gUserType													= "#Variables.VarValueTypCd#";

$(function() // Shorthand for the document ready event.
	{
	ShowInfoForUserType();
	});

function ClearAllJobTitles									(pForm)
	{
	var	i, sArray, sTrace;
	sArray													= pForm.#Variables.VarNameJobTitl#;<cfif Variables.DoTraceJobs>
	sTrace													= "Jobs trace: ClearAllJobTitles called when codes were '";
	for	(i = 0; i < sArray.length; i++)
		if	(sArray[i].checked)
			sTrace											+= sArray[i].value + ",";
	sTrace													+= "done'. ";
	alert (sTrace);
	</cfif>
	for	(i = 0; i < sArray.length; i++)
		sArray[i].checked									= false;
	if	(gUserType !== "U")
		{
		pForm					.#Variables.VarNameHQLoc#		.value		= "";
		document.getElementById("#Variables.VarNameHQLoc#Name")	.innerHTML	= "";
		}
	pForm		.#Variables.VarNameHQLoc#.onchange();		// Only way to set LSP is #Variables.VarNameHQLoc#.onchange().
	}

function ClearAllSupervisorFields							(pForm)
	{
	// See multiline comment in DoChooseSupervisor. <cfif								Variables.MandOptSupId1		is "view">
	document.getElementById("Box#Variables.VarNameSupId1  #").innerHTML		=</cfif>
	document.getElementById(   "#Variables.VarNameSupId1  #").value			= "";<cfif	Variables.MandOptSupFirst	is "view">
	document.getElementById("Box#Variables.VarNameSupFirst#").innerHTML		=</cfif>
	document.getElementById(   "#Variables.VarNameSupFirst#").value			= "";<cfif	Variables.MandOptSupMid		is "view">
	document.getElementById("Box#Variables.VarNameSupMid  #").innerHTML		=</cfif>
	document.getElementById(   "#Variables.VarNameSupMid  #").value			= "";<cfif	Variables.MandOptSupLast	is "view">
	document.getElementById("Box#Variables.VarNameSupLast #").innerHTML		=</cfif>
	document.getElementById(   "#Variables.VarNameSupLast #").value			= "";<cfif	Variables.MandOptSupEMail	is "view">
	document.getElementById("Box#Variables.VarNameSupEMail#").innerHTML		=</cfif>
	document.getElementById(   "#Variables.VarNameSupEMail#").value			= "";
	document.getElementById(   "#Variables.VarNameSupId2  #").value			= "";
	}

function DoChooseSupervisor									(pThis)
	{
	var	sArray, sSelIdx;
	sSelIdx													= pThis.selectedIndex;
	if	(sSelIdx > 0)
		{
		sArray												= pThis.options[sSelIdx].value.split("|");
		// The following 12 lines are actually just 6 statements. The overview of each pair of lines is a = b = c; which is laid out as
		// a =
		// b = c;
		// so that we can visually verify correctness via the vertical alignment of names. <cfif							Variables.MandOptSupId1		is "view">
		document.getElementById("Box#Variables.VarNameSupId1  #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupId1  #").value		= ((sArray.length > 0) ? sArray[0] : "?");<cfif Variables.MandOptSupFirst	is "view">
		document.getElementById("Box#Variables.VarNameSupFirst#").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupFirst#").value		= ((sArray.length > 1) ? sArray[1] : "?");<cfif Variables.MandOptSupMid		is "view">
		document.getElementById("Box#Variables.VarNameSupMid  #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupMid  #").value		= ((sArray.length > 2) ? sArray[2] : "?");<cfif Variables.MandOptSupLast	is "view">
		document.getElementById("Box#Variables.VarNameSupLast #").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupLast #").value		= ((sArray.length > 3) ? sArray[3] : "?");<cfif Variables.MandOptSupEMail	is "view">
		document.getElementById("Box#Variables.VarNameSupEMail#").innerHTML	=</cfif>
		document.getElementById(   "#Variables.VarNameSupEMail#").value		= ((sArray.length > 4) ? sArray[4] : "?");
		document.getElementById(   "#Variables.VarNameSupId2  #").value		= ((sArray.length > 5) ? sArray[5] : "?");
		// Since we got it by lookup, it has to be valid:
		document.getElementById("SupervisorInvalidMsg").style.display		= "none";
		}
	}

function DoLookupSupervisor									(pThis)
	{
	var	sDropdown, sEMail, sEMailVal, sFirst, sFirstVal, sForm, sHQLoc, sHQLocVal, sLast, sLastVal, sURL;
	sForm													= pThis.form; // Cache frequently used variables.
	sEMail													= sForm.#Variables.VarNameSupE#;
	sFirst													= sForm.#Variables.VarNameSupF#;
	sHQLoc													= sForm.#Variables.VarNameHQLoc#;
	sLast													= sForm.#Variables.VarNameSupL#;
	sEMailVal												= sEMail.value.replace(gTrimRE, "");
	sFirstVal												= sFirst.value.replace(gTrimRE, "");
	sHQLocVal												= sHQLoc.value.replace(gTrimRE, "");
	sLastVal												= sLast .value.replace(gTrimRE, "");
	if	(gUserType === "U")
		{
		if	(sHQLocVal === "")
			{
			alert("Please enter headquarters location.");
			return;
			}
		} // If User Type U has HQLoc, that's enough to do lookup.
	else if	((sEMailVal === "") && (sFirstVal === "") && (sLastVal === ""))
		{
		alert("Please enter something in one of the Supervisor fields.");
		return;
		}
	sDropdown												= document.getElementById("SupervisorDropdown");
	sURL													= "#Variables.PageName#?Action=LookupSupervisor"
															+ "&Now=" + (new Date()).getTime() // Cachebuster for IE.
															+ ((sEMailVal !== "") ? "&EMail="    + escape(sEMailVal) : "")
															+ ((sFirstVal !== "") ? "&First="    + escape(sFirstVal) : "")
															+ ((sHQLocVal !== "") ? "&HQLoc="    + escape(sHQLocVal) : "")
															+ ((sLastVal  !== "") ? "&Last="     + escape(sLastVal)  : "")
															+ ((gUserType !== "") ? "&UserType=" + escape(gUserType) : "");
//	alert("'" + sEMailVal + "', '" + sFirstVal + "', '" + sLastVal + "'.");
	$(sDropdown).load(sURL, DoLookupSupervisorCompletion);
	}

function DoLookupSupervisorCompletion						()
	{
	var	sDropdown, sDropdownCountFeedback, sNumericCount;
	sDropdown												= document.getElementById("SupervisorDropdown");
	sDropdownCountFeedback									= document.getElementById("SupervisorDropdownCount");
	sNumericCount											= sDropdown.options.length - 1; // Subtract out first option with value="".
	if		(sNumericCount < 1)
		{
		sDropdownCountFeedback.innerHTML					= "No supervisors found.";
		ClearAllSupervisorFields();
		}
	else if	(sNumericCount === 1)
		{
		sDropdown.selectedIndex								= 1;
		sDropdown.onchange();								// Propagate exactly 1 supervisor's info automatically.
		sDropdownCountFeedback.innerHTML					= "Exactly one supervisor found.";
		}
	else if	(sNumericCount > 1)
		{
		sDropdownCountFeedback.innerHTML 					= " " + sNumericCount + " supervisors found.";
		ClearAllSupervisorFields();
		}
	}

function DoThisOnChange#Variables.VarNameHQLoc#(pThis)

	{
	var	sDropdown, sDropdownCountFeedback, sForm, sMand, sName, sNameHidden, sOption;
	sDropdown												= document.getElementById("SupervisorDropdown");
	sDropdownCountFeedback									= document.getElementById("SupervisorDropdownCount");
	sName													= document.getElementById("#Variables.VarNameHQLoc#Name");
	sNameHidden												= document.getElementById("#Variables.VarNameHQLNm#");
	// Once #Variables.EngHQLoc# is changed, the only way to get location's name is a valid lookup (below).
	sName.innerHTML											= "";
	// If the User Type is Partner, once #Variables.EngHQLoc# is changed, the only way to get dropdown populated is a valid lookup (below).
	if	(gUserType === "U")
		{
		sDropdown.options.length							= 0;
		sOption												= document.createElement('option');
		sOption.value										= "";
		sOption.text										= "Supervisors Appear Here After Lookup";
		sDropdown.appendChild(sOption);
		sDropdownCountFeedback.innerHTML					= "";
		}
	sForm													= pThis.form;
	sMand													= isMand(pThis);
	if	(EditMask("#JSStringFormat(Variables.EngHQLoc)#", pThis.value, "9", sMand, 1, pThis.maxLength))
		{
		if	(pThis.value.length > 0)
			{
			var sLocDropdown, sLocDropdownLable, sOptionLoc;
			sLocDropdown											= document.getElementById("LocId");
			sLocDropdownLable										= document.getElementById("div_LocLable");

			//remove Location information
			sLocDropdown.options.length							= 0;
			sOptionLoc											= document.createElement('option');
			sOptionLoc.value									= "";
			sOptionLoc.text										= "";
			sDropdown.appendChild(sOptionLoc);
			sLocDropdown.style.display                  		= 'none';
			sLocDropdownLable	.style.display                  = 'none';

			$.ajax(	{<cfset Variables.URLString				= Variables.PageName
															& "?Action=LookupHQLoc"
															& "&Num=#URLEncodedFormat(Variables.EndUserData.IMUserId)#"
															& "&HQLoc=">
					dataType:								"text",<!--- What we expect back. --->
					url:									"#JSStringFormat(Variables.URLString)#" + escape(pThis.value)
															+ "&UserType="							+ escape(gUserType)
															+ "&Cachebuster="						+ (new Date()).getTime(),
					complete:
						function(jqXHR)
							{
							var	kAlertBegins, kMaxErrorLen, sResponseNum, sResponseObj, sResponseText;
							kAlertBegins					= "While trying to look up #Variables.EngHQLoc#, ";
							kMaxErrorLen					= 500; // Don't annoy folks with **TOO** much alert.
							sResponseText					= jqXHR.responseText.replace(gTrimRE, "");
							try	{
								sResponseObj				= $.parseJSON(sResponseText);
								}
							catch (e)
								{
							//	DumpObject(e, "e");
								alert(kAlertBegins			+ "was unable to decode the following: \n"
															+ (sResponseText.length > kMaxErrorLen ? sResponseText.substring(0,kMaxErrorLen) : sResponseText)
															);		// Probably a debugging message, so alert it.
								sResponseObj				= null;
								};
							if	(sResponseObj)
								{
								if	(sResponseObj.ErrMsg.length	> 0)
									alert(kAlertBegins		+ "got back the following error message: \n"
										+ (sResponseObj.ErrMsg.length > kMaxErrorLen ? sResponseObj.ErrMsg.substring(0,kMaxErrorLen) : sResponseObj.ErrMsg)
										);		// Probably a debugging message, so alert it.
								else if	(sResponseObj.LocValid !== "Y")
									sName.innerHTML			= pThis.value + " is not valid as a #Variables.EngHQLoc#.";
								else // Valid lookup!
									{
									sResponseNum			= parseInt(sResponseObj.LSPCount, 10);
									if	(sResponseNum == NaN)
										alert(kAlertBegins	+ "got back '" + sResponseObj.LSPCount + "' as the LSP count instead of a numeric value.");
									else
										{
										gLSP				= (sResponseNum > 0);
										$(".JobChk[value=6]").each(function()
											{// Now that gLSP is set, the Job Classification 6 onclick will force "checked" appropriately.
											this.onclick("bypass explanation");	// Bypassing explanation because not a real click. (See checkbox.)
											});
										}
									sName.innerHTML			= sResponseObj.LocName + (gLSP ? " (LSP)" : "");// Additional feedback doesn't hurt.
									sNameHidden.value		= sResponseObj.LocName;// Preserve in case we return to this page after error.
									if	(gUserType === "U")
										{
										// sDropdown.innerHTML	= "<option ...> ..."; doesn't work under IE (surprise, surprise).
										sDropdown.options.length = 0;// Get rid of existing options.
										for	(var i = 0; i < sResponseObj.SupervisorOptions.length; i++)
											{
											sOption			= document.createElement('option');
											sOption.value	= sResponseObj.SupervisorOptions[i][0];
											sOption.text	= sResponseObj.SupervisorOptions[i][1];
											sDropdown.appendChild(sOption);
											}
										DoLookupSupervisorCompletion();
										}
									} // end of valid #Variables.EngHQLoc#.
								} // end sResponseObj.
							} // end completion function.
					}); // end $.ajax call arguments object (and call).
			} // end pThis.value.length > 0.
		return true;
		} // end of EditMask success block.
	this.focus();
	return false;
	} // end DoThisOnChange#Variables.VarNameHQLoc#


function DoThisOnChangeUserType								()
	{<cfif ListFindNoCase("skip,view", Variables.MandOptTypCd) is 0><!--- And therefore the dropdown for UserType exists. --->
	var	i, sDropdownUserType, sForm, sOldTypeIdx, sOldTypeName, sOptions, sOptionsLen, sSelIdx, sValueSelected, sWarningMsg;
	// Do the alert(s) here and return out of the function if the user changes their mind about losing roles. <cfif Variables.DoTraceJobs>
	alert("Jobs trace: DoThisOnChangeUserType called.")</cfif>
	sForm													= document.#Variables.FormName#;
	sDropdownUserType										= sForm.#Variables.VarNameTypCd#;
	sOptions												= sDropdownUserType.options;
	sOptionsLen												= sOptions.length;
	sOldTypeIdx												= -1;
	sOldTypeName											= "the old user type";
	for	(i = 0; i < sOptionsLen; i++)
		if	(sOptions[i].value === gUserType)
			{
			sOldTypeIdx										= i;
			sOldTypeName									+= " (" + sOptions[i].text + ")";
			break;
			}
	sSelIdx													= sDropdownUserType.selectedIndex;
	sValueSelected											= ((sSelIdx > 0) ? sOptions[sSelIdx].value : "");
	sWarningMsg												= "";
	switch (sValueSelected)
		{
		case "C": case "S": case "U":
			sWarningMsg										+= "Please make sure the supervisor info is correct for the new user type.\n";
			break;
		default:
		}<cfif Variables.EndUserData.IMUserId is 0>
	if	(sWarningMsg !== "")
		alert(sWarningMsg);<cfelse>
	sWarningMsg												= "Changing the user type will delete all the existing roles that "
															+ "require SBA authorization and/or "
															+ sOldTypeName
															+ ".\n"
															+ sWarningMsg
															+ "Press OK to continue, or Cancel if that isn't what you wanted to happen.";
	if	(!confirm(sWarningMsg))
		{
		if	(sOldTypeIdx > -1)
			{
			sDropdownUserType.selectedIndex					= sOldTypeIdx;
			return false;
			}
		// The return false above normally prevents the following alert from ever happening. (Just in case.)
		alert("Unable to restore previously selected User Type. Please cancel by changing back to the old value manually.");
		return false; // Don't set gUserType, etc.
		}</cfif><!--- /IMUserId --->
	gUserType												= sValueSelected;
	ClearAllJobTitles(sForm); // Change of user type implies loss of all job titles, even for the user type just left. <cfelse>
	// Even though we're not allowing a change of UserType, we still want to show all info it controls: </cfif>
	ShowInfoForUserType();
	return true;
	}

function DoValidateSupervisor								()
	{<cfif Variables.EndUserData.IMUserId is 0>
	$("##SupervisorInvalidMsg").hide();						// When creating a new account, always hide. <cfelseif Variables.ReadOnlyProfile>
	$("##SupervisorInvalidMsg").hide();						// When displaying a readonly profile, always hide. <cfelse>
	var	sResponseText;
	switch (gUserType)
		{
		case "C": // SBA Contractor requres supervisor.
		case "S": // SBA Employee   requres supervisor.
		case "U": // Partner        requres supervisor.
			$.ajax(	{<cfset Variables.URLString				= Variables.PageName
															& "?Action=ValidateSupervisor"
															& "&UserName=#URLEncodedFormat(Variables.Origs.IMUserNm)#"
															& "&UserType=">
					dataType:								"text",<!--- What we expect back. --->
					url:									"#Variables.URLString#" + escape(gUserType),
					complete:
						function(jqXHR)
							{
							sResponseText					= jqXHR.responseText.replace(gTrimRE, "");
							if	(sResponseText.length		> 1)
								{
								alert("When trying to validate supervisor, got back the following message: \n"
									+ sResponseText);		// Probably a debugging message, so alert it.
								$("##SupervisorInvalidMsg").show();	// Show invalid supervisor message.
								}
							else if	(sResponseText			=== "A")
								$("##SupervisorInvalidMsg").hide();	// Don't display invalid supervisor message).
							else
								$("##SupervisorInvalidMsg").show();	// Show invalid supervisor message.
							}
					});
			break;
		default:
			$("##SupervisorInvalidMsg").hide();						// (User type doesn't require a supervisor.)
		}</cfif>
	}

function ShowInfoForUserType								()
	{
	$(".TypeSensitive").hide().filter(".Type"+gUserType).show();
	if	($(".JobTitle").hide().filter(".Type"+gUserType).show().length == 0)
		$(".JobTitle.TypeNone").show();
	$(".JobSensitive").hide();
	$("input.JobChk:checked").each(function(){$(".Job"+this.value).show();});
	if	(gUserType === "U")	HiGrpMandatory	(".hqloc");
	else					HiGrpOptional	(".hqloc");
	DoValidateSupervisor();
	}

</script>
<style>

label.offscreen
	{
	position:							absolute;
	left:								-9999px; /* far offscreen so won't mess up sighted users */
	}

</style>
</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

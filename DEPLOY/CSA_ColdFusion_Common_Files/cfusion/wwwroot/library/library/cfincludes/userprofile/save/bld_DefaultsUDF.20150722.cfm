<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/22/2015. 
DESCRIPTION:		Defines the "Defaults" UDF, which is needed in dsp_userprofile, regardless of whether FormDataRecovery is 
					"Yes" or "No". 
NOTES:

	The whole point of Defaults is to allow external overrides by the calling pages, according to naming conventions unique to 
	this cfincluded profile page. It sets several output variables into the Variables scope, which the immediately-subsequent 
	tbl_row uses to image the form element. It's a much simpler way to do runtime overrides of pMandOpt than cf_sbaformelt, 
	which hasn't been officially approved yet. It's capable of being much simpler because it isn't trying to be general purpose 
	and usable by all pages. It's pretty much tailored to the special needs of this cfinclude. And it makes the code SOOOO 
	much simpler. 

	There are some situations it doesn't handle well, notably checkboxes and radio buttons. In that situation, the official 
	label is in formdata, not in formlabel. The solution is simple, don't use the output variable Variables.Label in formlabel. 
	Use "reqd" sparingly, because SMM doesn't like the concept of a form element that prevents the form from being submitted. 
	It's needed if the underlying database column is defined as NOT NULL, but she would rather get a server-side error message 
	in that situation. 

	The Self-Test Mode of bld_Origs has to test bld_Defaults too, because Defaults appends to Variables.Origs.ArrayColsOnDspPage. 
	So browse to /library/cfincludes/userprofile/bld_Origs.cfm to test this file. 

INPUT:				pColName					Database column name, which is our official name for the column on the server. 
					pEltName					Form element name (not implemented yet). So hackers won't know our column names. 
					pEngName					English name for labels, EditMask and val routines. 
					pMandOpt					"reqd", "mand", "opt", "view" or "skip". 
					pSaveSuffix					Usually nullstring, but if not, make copies of outputs with this suffix. 
					Variables.MandOptXXXX		EXTERNAL OVERRIDE: Caller's override of pMandOpt for pColName "XX". 
					Variables.ReadOnlyProfile	EXTERNAL OVERRIDE: Override of pMandOpt because user doesn't have MayEdit. 

OUTPUT:				Variables.Eng				Usually pEng, but maybe not if it has a "Disp" variable. 
					Variables.Label				<label for="#VarName#" class="#MandOpt#label">#Eng#</label>
					Variables.MandOpt			Usually pMandOpt, but maybe not if it has an external override. 
					Variables.NameAndId			name="#VarName#" id="#VarName#", possibly with "required" attribute appended. 
					Variables.NameIdAndValue	same as NameAndId, but with value="#VarValue#" appended. 
					Variables.VarName			Currently, pColName, but someday pEltName for obfuscation. 
					Variables.VarValue			Value of Variables[VarName] (value from database, as possibly edited by user). 
					Variables.Origs.ArrayColsOnDspPage (if NOT FormDataRecovered). 

REVISION HISTORY:	07/22/2015,	SRS:	Original implementation as part of the new dsp_userprofile that consolidates all user 
										profile pages. We now have no prior knowledge as to which which columns a caller may 
										chose to make uneditable. 
--->

<!--- Configuration Parameters: --->


<!--- ************************************************************************************************************ --->


<cfscript>

Variables.ListAllReqdFields									= "";		// Used by DoThisOnSubmit. 

function Defaults											(
															pColName,	// Our database column name, hidden from user. 
															pEltName,	// Name the browser sees. (not implemented yet.) 
															pEngName,	// English name for labels and EditMask. 
															pMandOpt,	// 
															pSaveSuffix	// Usually "", but if not, make copies with this suffix. 
															)
	{
	var	sLocal												= {};		// Structure to hold all local variables. 
	Variables.Eng											= pEngName;	// Initialize to prevent artifacts from previous calls. 
	Variables.Label											= "";		// Initialize to prevent artifacts from previous calls. 
	Variables.MandOpt										= pMandOpt;	// Initialize to prevent artifacts from previous calls. 
	Variables.NameAndId										= "";		// Initialize to prevent artifacts from previous calls. 
	Variables.NameIdAndValue								= "";		// Initialize to prevent artifacts from previous calls. 
	Variables.VarName										= pColName;	// Initialize to prevent artifacts from previous calls. 
	Variables.VarValue										= "";		// Initialize to prevent artifacts from previous calls. 
	// External overrides of pMandOpt: 
	if	(StructKeyExists(Variables, "MandOpt" & pColName))				// Note that this imposes a NAMING CONVENTION. 
		Variables.MandOpt									= Variables["MandOpt" &	pColName];
	// ReadOnlyProfile doesn't override "skip"
	if	(Variables.ReadOnlyProfile and (Variables.MandOpt is not "skip"))
		Variables.MandOpt									= "view";	// But ReadOnlyProfile overrides everything else. 
	// The following switch is PROBABLY no longer needed, due to logic at the end of LoadOrigs. (See end of bld_Origs.cfm.) 
	switch (pColName)
		{
		case "IMSBACrdntlPasswrdExprDt":
		case "IMUserDOBDt":
		case "IMUserPhnLclNmb":
		case "LINCInd":
			if	(NOT Variables.FormDataRecovered)											// Initial load from the database:? 
				Variables.VarName							= "Disp" & pColName;			// Yes: Override to Disp version. 
			break;
		default:
		}
	Variables.VarValue										= Variables[Variables.VarName];
	switch (Variables.MandOpt)
		{
		case "reqd":
		case "mand":
		case "opt":
			Variables.Label									= "<label for="""
															& Variables.VarName
															& """ class="""
															& Variables.MandOpt
															& "label"">"
															& Variables.Eng
															& "</label>";
			Variables.NameAndId								= "name=""#Variables.VarName#"" id=""#Variables.VarName#""";
			if	(Variables.MandOpt is "reqd")
				Variables.NameAndId							&= " required"; // In case browser supports it. 
			Variables.NameIdAndValue						= Variables.NameAndId
															& " value=""#Variables.VarValue#""";
			break;
		case "view":
			Variables.Label									= "<span class="""
															& Variables.MandOpt
															& "label"">"
															& Variables.Eng
															& "</span>";
			// The rest won't be used. 
			break;
		}
	if	(Len(pSaveSuffix) gt 0)
		{
		Variables["Eng"				& pSaveSuffix]			= Variables.Eng;
		Variables["Label"			& pSaveSuffix]			= Variables.Label;
		Variables["MandOpt"			& pSaveSuffix]			= Variables.MandOpt;
		Variables["NameAndId"		& pSaveSuffix]			= Variables.NameAndId;
		Variables["NameIdAndValue"	& pSaveSuffix]			= Variables.NameIdAndValue;
		Variables["VarName"			& pSaveSuffix]			= Variables.VarName;
		Variables["VarValue"		& pSaveSuffix]			= Variables.VarValue;
		}

	// ListAllReqdFields is used by DoThisOnSubmit to prevent submit on browsers that don't support "required" attribute: 
	if	(Variables.MandOpt is "reqd")
		Variables.ListAllReqdFields							= ListAppend(Variables.ListAllReqdFields, Variables.VarName);

	// As seen above, we can USE Variables.Origs on any display of the dsp page. But we can only MODIFY Variables.Origs 
	// when we're still building it, at NOT-FormDataRecovered-time. If we append to the end of ArrayColsOnDspPage at any 
	// other time (after return from act page), we would be appending duplicates!! 
	if	(NOT Variables.FormDataRecovered)
		{
		ArrayAppend(Variables.Origs.ArrayColsOnDspPage,
					{
					"ColName"								= pColName,
					"EltName"								= pEltName,
					"EngName"								= pEngName,
					"MandOpt"								= Variables.MandOpt,
					"VarName"								= Variables.VarName
					});
		}

	// For debugging weirdness in this UDF, modify the HTML comment, then remove the 2 slashes before the writeOutput:  
	// writeOutput("#Request.SlafEOL#    <!-- Display your misbehaving variables here, inside the comment. -->");
	}

</cfscript>

<!---
AUTHOR:				Steve Seaquist, based on an HTML <table>-based version by Asad Chaklader.
DATE:				09/02/2011.
DESCRIPTION:		Displays Fed Agency and Job Title dropdown menus.
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html.
					Asad's version assumed <table> tag in calling page. This page does the outermost <div class="tbl"> itself.
					Currently in development, but will eventually be called by Section 508 compliant versions of:
						/cls/dsp_addcustomer.cfm
						/cls/dsp_profile.cfm
						/security/user/dsp_user.cfm
INPUT:				Server:				AddBusNewGlsAct, IMAgncyCd, IMUserTypCd, JobTitlCd, SBADeptAgncCd.
					Browser:			gDivSBASupervisor. This implies that the caller must also include dsp_formdata_ldap
										inside a container that gets loaded into gDivSBASupervisor. This allows the container
										to group SBA Supervisor fields in their own fieldset, per Section 508.
OUTPUT:				2 drop-down menus (agency and job title).
REVISION HISTORY:	04/07/2015, NS:     Temporary check in for Demo
					04/03/2015, NS:     Added support for non-partners. Removed DDL Agency for OCA
					08/07/2014, SRS:	Removed dbtype attribute for CF11 compatibility.
					09/01/2011, SRS:	Original library implementation. Cannibalized /cls/cfincludes version, copied to
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables.
										Got rid of all of the queries-of-queries, which we now know are much too slow.
					10/14/2010, AJC:	Initial Implementation.
--->

<cfset Variables.LogAct									= "retrieve Federal employee job titles">
<cfset Variables.cfprname								= "AgencyJobTitleSort">
<cfset Variables.Identifier								= "11"><!--- Same as Identifier 4, only alphabetized. --->
<cfset Variables.dbBeforeIMFedEmpJobTitlTypSelTSP		= Variables.db>
<cfset Variables.db										= "oracle_scheduled">
<cfinclude template="/cfincludes/oracle/sbaref/spc_IMFEDEMPJOBTITLTYPSELTSP.PUBLIC.cfm">
<cfset Variables.db										= Variables.dbBeforeIMFedEmpJobTitlTypSelTSP>
<cfif Variables.TxnErr>
	<cfoutput>#Variables.ErrMsg#</cfoutput>
	<cfabort>
</cfif>
<cfloop query="AgencyJobTitleSort">
	<cfif AgencyJobTitleSort.JobTitlCd EQ 0>
		<cfoutput>
  <input type="hidden" name="PosOtherRow" value="#AgencyJobTitleSort.CurrentRow#"></cfoutput>
		<cfbreak>
	</cfif>
</cfloop>

<cftry>
	<cfset Variables.LogAct								= "retrieve a list of all current Federal Government departments">
	<cfquery name="GetFedDepartments"					datasource="oracle_transaction_publicread">
	select		DeptId || '-' || AgncyId				as code,
				DeptNm,
				DeptId
	from		sbaref.IMFedAgncyTbl
	where		(StrtDt									< sysdate)
	and			(	(EndDt								is null)
				or	(EndDt								>= sysdate)
				)<cfif IsDefined ("Variables.IMUserTypCd") AND Variables.IMUserTypCd IS "S"><!--- SBA --->
	and			(DeptId || '-' || AgncyId				= '#Variables.SBADeptAgncCd#')</cfif>
	and			(DeptId									= AgncyId)
	order by	DeptNm
	</cfquery>
	<cfset Variables.LogAct								= "retrieve a list of all current Federal Government agencies">
	<cfquery name="GetFedAgencies"						datasource="oracle_transaction_publicread">
	select		DeptId || '-' || AgncyId				as code,
				AgncyNm,
				DeptId
	from		sbaref.IMFedAgncyTbl
	where		(StrtDt									< sysdate)
	and			(	(EndDt								is null)
				or	(EndDt								>= sysdate)
				)<cfif IsDefined ("Variables.IMUserTypCd") AND Variables.IMUserTypCd IS "S"><!--- SBA --->
	and			(DeptId || '-' || AgncyId				= '#Variables.SBADeptAgncCd#')</cfif>
	and			(DeptId									!= AgncyId)
	order by	DeptId,<!--- Top level sort column used to save on processing in the GetFedAgencies cfloop, below. --->
				AgncyNm
	</cfquery>
	<cfcatch type="Any">
		<cfset Variables.ErrMsg							= "ERROR. A database error occurred while trying to "
														& "#Variables.LogAct#. The following information may help:"
														& CFCatch.Message & " " & CFCatch.Detail>
		<cfset Variables.TxnErr							= "Yes">
	</cfcatch>
</cftry>
<cfif Variables.TxnErr>
	<cfoutput>#Variables.ErrMsg#</cfoutput>
	<cfabort>
</cfif>

<cfoutput>
  <div style="display:none;"><!-- Make extra sure that the browser doesn't display the JS text between the script tags. -->
  <script>
  // The following are not static. They depend on database conditions. So they cannot be moved to a separate .js file:
  function ConfirmTitleChange                           (pThis, pComplainAboutMandatory)
    {<cfif IsDefined("Variables.IMUserId") AND Variables.IMUserId>
    var	sIndex                                          = pThis.selectedIndex;
    if  ((sIndex <= 0) && pComplainAboutMandatory)      // Nothing selected, or the "Select One" option is selected.
        {
        alert("ERROR. Job Title is mandatory.");
        return false;
        }
    var sPrev                                         = "the previous job title";
    for (var i = 1; i < pThis.options.length; i++)
        if  (pThis.options[i].value == "'#Variables.JobTitlCd#'")
            {
            sPrev                                       = pThis.options[i].text;
            break;
            }
    if  (!confirm("Changing Job Title will delete all of your existing roles that require a specific Job Title. "
                + "Press OK to continue or Cancel to revert back to " + sPrev + "."))
        {
        // Revert JobTitlCd to '#Variables.JobTitlCd#', the value it was at the time it left the server:
        for (var i = 1; i < pThis.options.length; i++)
            if  (pThis.options[i].value == '#Variables.JobTitlCd#')
                {
                pThis.selectedIndex                     = i;
                return true;
                }
        pThis.selectedIndex                             = 0;
        }<cfelse>
    // No login yet, so selecting title cannot result in a loss of roles. Nothing to confirm. </cfif>
    return true;
    }
    <!--- present in dsp_addcustomer.cfm outside CLS login --->
  <cfif listfindnocase("/cls/dsp_addcustomer.cfm",CGI.SCRIPT_NAME) eq 0>
  function ShowSBASupervisorEltsIfNeeded                (pThis, pComplainAboutMandatory)
    {<cfif IsDefined("Variables.AddBusNewGlsAct") AND Variables.AddBusNewGlsAct>
    var	sIndex                                          = pThis.selectedIndex;
    if  ((sIndex <= 0) && pComplainAboutMandatory)      // Nothing selected, or the "Select One" option is selected.
        {
        alert("ERROR. Agency is mandatory.");
        return false;
        }
    var sValue                                          = pThis.options[sIndex].value;
    if  (sValue == "#Variables.SBADeptAgncCd#")
        gDivSBASupervisor.style.display                 = '';
    else
        gDivSBASupervisor.style.display                 = 'none';<cfelse>
    // SBA Supervisor was determined at the time the account was created. </cfif>
    return true;
    }
	</cfif>
  </script>
  </div><!-- /style="display:none;" -->
  <div class="tbl" id="DivFedAgencyAndTitle">
</cfoutput>
<!--- No Agency display outside CLS login --->
<cfif not ( listfindnocase("/cls/dsp_addcustomer.cfm",CGI.SCRIPT_NAME) gt 0 )
	and not ( listfindnocase("/security/user/dsp_user.cfm",CGI.SCRIPT_NAME) gt 0 )>
<cfoutput>
   <div class="tbl_row">
      <div class="tbl_cell formlabel"><label class="mandlabel" for="IMAgncyCd">Select Agency:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <select name="IMAgncyCd" id="IMAgncyCd" onchange="return ShowSBASupervisorEltsIfNeeded(this, true);">
          <option value="">Select One</option></cfoutput>
<cfloop query="GetFedDepartments">
	<cfif	IsDefined("Variables.IMAgncyCd")
		and	(Variables.IMAgncyCd EQ "#GetFedDepartments.code#")>
		<cfset Variables.Selected						= "selected">
	<cfelse>
		<cfset Variables.Selected						= "        ">
	</cfif>
	<cfoutput>
          <option #Variables.Selected# value="#GetFedDepartments.code#">#GetFedDepartments.DeptNm#</option></cfoutput>
	<cfset Variables.DeptIdOfParent						= GetFedDepartments.DeptId>
	<cfset Variables.DeptIdSeen							= "No">
	<cfloop query="GetFedAgencies">
		<cfif GetFedAgencies.DeptId is Variables.DeptIdOfParent>
			<cfset Variables.DeptIdSeen					= "Yes">
			<cfif	IsDefined("Variables.IMAgncyCd")
				and	(Variables.IMAgncyCd EQ "#GetFedAgencies.code#")>
				<cfset Variables.Selected				= "selected">
			<cfelse>
				<cfset Variables.Selected				= "        ">
			</cfif>
			<cfoutput>
          <option #Variables.Selected# value="#GetFedAgencies.code#">&nbsp;&nbsp;--&nbsp;#GetFedAgencies.AgncyNm#</option></cfoutput>
		<cfelseif Variables.DeptIdSeen>
			<!--- Because outer sort of GetFedAgencies is DeptId, once we exhaust existing sub-agencies, we're done. --->
			<cfbreak>
		</cfif>
	</cfloop>
</cfloop>
<cfoutput>
        </select>
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
</cfif>
<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label class="mandlabel" for="JobTitlCd">Select Job Title:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <select name="JobTitlCd" id="JobTitlCd" onchange="return ConfirmTitleChange(this, true);">
          <option value="">Select One</option></cfoutput>
<cfloop query="AgencyJobTitleSort">
	<cfif	IsDefined("Variables.JobTitlCd")
		and	(Variables.JobTitlCd EQ AgencyJobTitleSort.JobTitlCd)>
		<cfset Variables.Selected						= "selected">
	<cfelse>
		<cfset Variables.Selected						= "        ">
	</cfif>
	<cfoutput>
          <option #Variables.Selected# value="#AgencyJobTitleSort.JobTitlCd#" >#AgencyJobTitleSort.JobTitlDescTxt#</option></cfoutput>
</cfloop>
<cfoutput>
        </select>
        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</cfoutput>

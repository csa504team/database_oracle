<cfif CGI.Script_Name IS "/cls/cfincludes/dsp_formdata_ldap.cfm"><cfsetting enablecfoutputonly="Yes"></cfif>
<!---
AUTHOR:				Steve Seaquist
DATE:				09/06/2011
DESCRIPTION:		Displays form fields relating to LDAP for SBA Employee users. Also acts as its own AJAX callback file. 
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html. 
					CFIncluded by dsp_formfields_userroles, so we assume the same existence of variables as that file. 
					Currently in development, but will eventually be called by Section 508 compliant versions of: 
						/cls/dsp_access.cfm
						/cls/dsp_addcustomer.cfm
						/security/user/dsp_profile.cfm
						/security/user/dsp_user.cfm
INPUT:				See /cls/cfincludes/dsp_formfields_userroles.cfm. 
OUTPUT:				LDAP-related form fields. 
REVISION HISTORY:	09/06/2011, SRS:	Original library implementation. Cannibalized /cls/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to use "tbl" classes instead of tables. 
					02/08/2011, ajc:	Added support for GLS account request. (SRS: Fixed LDAPUserId lookup.) 
					04/19/2010, SRS:	Some SBA Employees have been going through IT Security to change their supervisor 
										info often. To prevent problems that causes, disallowed editing of supervisor info. 
										(Displayed read-only.) 
					02/03/2010, SRS:	Provide more feedback to end users about what to do. 
					02/02/2010, SRS:	On AJAX lookups, require URL.LDAPUserId to be in LDAP. 
					01/28/2010.2, SRS:	Eliminated first name lookups, added last name shortening to last name lookups, 
										and made "Windows Login" fields explicitly "Your/User's" or "Super's". 
					01/28/2010.1, SRS:	Avoided getting messed up by IT Security user's IMUserTypCd and displaying fields 
										for non-SBA users in Security System > End Users > Update User Access - IT. 
					01/26/2010, SRS:	Original implementation.
--->

<!--- Configuration Parameters: --->

<cfset Variables.AllowGivenNameInLookups					= "No">
<cfset Variables.AllowITSecurityToDoArbitraryLookups		= "No">
<cfset Variables.LDAPServerName								= "newyukon.sba.gov">
<cfparam name="Variables.LibIncURL"							default="/library/cfincludes">
<cfset Variables.SelfBase									= "#Variables.LibIncURL#/userprofile/dsp_formdata_ldap">
<cfset Variables.SelfName									= "#Variables.SelfBase#.cfm"><!--- For AJAX callbacks to self. --->
<cfset Variables.SelfNameStaticCSS							= "#Variables.SelfBase#.css?CachedAsOf=2011-09-09T18:53">
<cfset Variables.SelfNameStaticScripts						= "#Variables.SelfBase#.js?CachedAsOf=2011-09-09T18:52">
<cfset Variables.ShowUserTheirOwnWindowsLogin				= "No">
<cfset Variables.UserIsRequestingOwnAccess					= (Left(CGI.Script_Name,5) IS "/cls/")>

<!-- **************************************************************************************** -->

<!--- AJAX Callback(s): --->

<cfif CGI.Script_Name IS Variables.SelfName><!--- AJAX callbacks are to dsp_formdata_ldap, not to the caller. --->
	<cfif	IsDefined("URL.Given")
		OR	IsDefined("URL.Family")
		OR	IsDefined("URL.LDAPUserId")
		OR	IsDefined("URL.IMUserSuprvId")>
		<cfif IsDefined("URL.RequestTimeout")>
			<cf_setrequesttimeout seconds="#URL.RequestTimeout#">
		</cfif>
		<cfif IsDefined("URL.Given") OR IsDefined("URL.Family")>
			<cfparam name="URL.Given"						default="">
			<cfparam name="URL.Family"						default="">
			<cfset Variables.Filter							= "(&(objectclass=person)">
			<cfset Variables.GetAll							= ((IsDefined("URL.GetAll")) AND (URL.GetAll is "Yes"))>
			<cfif Variables.GetAll>
				<cfset Variables.ColumnNames				= "uid,cn,givenName,initials,sn,mail">
			<cfelse>
				<cfset Variables.ColumnNames				= "uid,cn">
			</cfif>
			<cfset Variables.Done							= "No"><!--- Keep indention low. --->
			<cfif	(NOT Variables.Done)
				AND	(Len(URL.Given) GT 0)
				AND	(Len(URL.Family) GT 0)>
				<cfldap
					action									= "QUERY"
					name									= "getUidAndCn"
					server									= "#Variables.LDAPServerName#"
					username								= "uid=readuser,ou=people,dc=sba,dc=gov"
					password								= "readuser10"
					filter									= "#Variables.Filter#(sn=#Variables.FamilyFragment#*))"
					attributes								= "#Variables.ColumnNames#"
					sort									= "cn"
					start									= "ou=people,dc=sba,dc=gov">
				<cfif getUidAndCn.RecordCount GT 1>
					<cfset Variables.Done					= "Yes">
				</cfif>
			</cfif>
			<cfif	(NOT Variables.Done)
				AND	(Len(URL.Given) GT 0)>
				<cfset Variables.Filter						&= "(givenName=#URL.Given#*)">
			</cfif>
			<cfif	(NOT Variables.Done)
				AND	(Len(URL.Family) GT 0)>
				<cfset Variables.FamilyLen					= Len(URL.Family)>
				<cfset Variables.FamilyEnd					= Variables.FamilyLen - 2>
				<cfif Variables.FamilyEnd LT 1>
					<cfset Variables.FamilyEnd				= 1>
				</cfif>
				<cfloop index="NameLenIdx" from="#Variables.FamilyLen#" step="-1" to="#Variables.FamilyEnd#">
					<cfset Variables.FamilyFragment			= Left(URL.Family, NameLenIdx)>
					<cfldap
						action								= "QUERY"
						name								= "getUidAndCn"
						server								= "#Variables.LDAPServerName#"
						username							= "uid=readuser,ou=people,dc=sba,dc=gov"
						password							= "readuser10"
						filter								= "#Variables.Filter#(sn=#Variables.FamilyFragment#*))"
						attributes							= "#Variables.ColumnNames#"
						sort								= "cn"
						start								= "ou=people,dc=sba,dc=gov">
					<cfif getUidAndCn.RecordCount GT 1>
						<cfset Variables.Done				= "Yes">
						<cfbreak>
					</cfif>
				</cfloop>
			<cfelse>
				<cfset Variables.Filter						&= ")">
				<cfldap
					action									= "QUERY"
					name									= "getUidAndCn"
					server									= "#Variables.LDAPServerName#"
					username								= "uid=readuser,ou=people,dc=sba,dc=gov"
					password								= "readuser10"
					filter									= "#Variables.Filter#"
					attributes								= "#Variables.ColumnNames#"
					sort									= "cn"
					start									= "ou=people,dc=sba,dc=gov">
				<cfif getUidAndCn.RecordCount GT 1>
					<cfset Variables.Done					= "Yes">
				</cfif>
			</cfif>
			<cfcontent type="text/html">
			<cfif getUidAndCn.RecordCount GT 0>
				<cfoutput>
						<option value="">Choose An SBA Employee</option></cfoutput>
				<cfloop query="getUidAndCn">
					<cfset Variables.OptionValue			= getUidAndCn.uid>
					<cfif Variables.GetAll>
						<cfset Variables.OptionValue		&=	"|#getUidAndCn.givenName#"
															&	"|#getUidAndCn.initials#"
															&	"|#getUidAndCn.sn#"
															&	"|#getUidAndCn.mail#">
					</cfif>
					<cfoutput>
						<option value="#Variables.OptionValue#">#getUidAndCn.cn#</option></cfoutput>
				</cfloop>
			<cfelse>
				<cfoutput>
						<option value="">Nothing found. (Please try again.)</option></cfoutput>
			</cfif>
		<cfelseif IsDefined("URL.LDAPUserId")>
			<cfset Variables.IMUserSuprvId					= "">
			<cfset Variables.IMUserSuprvFirstNm				= "">
			<cfset Variables.IMUserSuprvMidNm				= "">
			<cfset Variables.IMUserSuprvLastNm				= "">
			<cfset Variables.IMUserSuprvEmailAdrTxt			= "">
			<cfset Variables.ManagerDescription				= "">
			<cfldap
				action										= "QUERY"
				name										= "getManagerDescription"
				server										= "#Variables.LDAPServerName#"
				username									= "uid=readuser,ou=people,dc=sba,dc=gov"
				password									= "readuser10"
				filter										= "(&(objectclass=person)(uid=#URL.LDAPUserId#))"
				attributes									= "manager"
				start										= "ou=people,dc=sba,dc=gov">
			<cfif getManagerDescription.RecordCount GT 0>
				<!--- Since the lookup was by uid, there should be at most 1 row returned (safe to get [1]): --->
				<cfset Variables.Idx						= FindNoCase(",OU",getManagerDescription.manager[1])>
				<cfif Variables.Idx GT 1>
					<cfset Variables.ManagerDescription		= Left(getManagerDescription.manager[1], Variables.Idx - 1)>
					<cfldap
						action								= "QUERY"
						name								= "getManagerData"
						server								= "#Variables.LDAPServerName#"
						username							= "uid=readuser,ou=people,dc=sba,dc=gov"
						password							= "readuser10"
						filter								= "(&(objectclass=person)(#Variables.ManagerDescription#))"
						attributes							= "uid,givenName,initials,sn,mail"
						start								= "ou=people,dc=sba,dc=gov">
				<cfelse>
					<cfset Variables.ManagerDescription		= getManagerDescription.manager[1]>
					<cfldap
						action								= "QUERY"
						name								= "getManagerData"
						server								= "#Variables.LDAPServerName#"
						username							= "uid=readuser,ou=people,dc=sba,dc=gov"
						password							= "readuser10"
						filter								= "(&(objectclass=person)(description=#Variables.ManagerDescription#))"
						attributes							= "uid,givenName,initials,sn,mail"
						start								= "ou=people,dc=sba,dc=gov">
				</cfif>
				<cfif getManagerData.RecordCount GT 0>
					<cfset Variables.IMUserSuprvId			= getManagerData.uid>
					<cfset Variables.IMUserSuprvFirstNm		= getManagerData.givenName>
					<cfset Variables.IMUserSuprvMidNm		= getManagerData.initials>
					<cfset Variables.IMUserSuprvLastNm		= getManagerData.sn>
					<cfset Variables.IMUserSuprvEmailAdrTxt	= getManagerData.mail>
				</cfif>
				<cfcontent type="application/json"><!--- **NOT** text/html! --->
				<!---
				According to http://json.org, single-quotes are passed as-is (no backslashes). So therefore, in order to escape 
				double-quotes without escaping single-quotes, use Replace() instead of JSStringFormat(): 
				--->
				<cfoutput>{"ErrMsg":""</cfoutput>
				<cfoutput>,"IMUserSuprvId":"#				Replace(Variables.IMUserSuprvId,								"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvFirstNm":"#			Replace(Variables.IMUserSuprvFirstNm,							"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvMidNm":"#			Replace(Variables.IMUserSuprvMidNm,								"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvLastNm":"#			Replace(Variables.IMUserSuprvLastNm,							"""","\""","ALL")#"</cfoutput>
				<cfoutput>,"IMUserSuprvEmailAdrTxt":"#		Replace(Variables.IMUserSuprvEmailAdrTxt,						"""","\""","ALL")#"</cfoutput>
				<!--- But ManagerDescription is different in that it usually contains \, which must be escaped first: --->
				<cfoutput>,"ManagerDescription":"#			Replace(Replace(Variables.ManagerDescription,	"\","\\","ALL"),"""","\""","ALL")#"}</cfoutput>
			<cfelse>
				<cfcontent type="application/json"><!--- **NOT** text/html! --->
				<!--- There shouldn't be a double-quote in what the user typed, but in case there is, don't crash: --->
				<cfoutput>{"ErrMsg":"Windows Login '#Replace(URL.LDAPUserId,"""","\""","ALL")#' not found. Contact HQ HelpDesk."</cfoutput>
				<cfoutput>,"IMUserSuprvId":""</cfoutput>
				<cfoutput>,"IMUserSuprvFirstNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvMidNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvLastNm":""</cfoutput>
				<cfoutput>,"IMUserSuprvEmailAdrTxt":""</cfoutput>
				<cfoutput>,"ManagerDescription":""}</cfoutput>
			</cfif>
		<cfelseif IsDefined("URL.IMUserSuprvId")>
			<cfset Variables.IMUserSuprvFirstNm				= "">
			<cfset Variables.IMUserSuprvMidNm				= "">
			<cfset Variables.IMUserSuprvLastNm				= "">
			<cfset Variables.IMUserSuprvEmailAdrTxt			= "">
			<cfldap
				action										= "QUERY"
				name										= "getManagerData"
				server										= "#Variables.LDAPServerName#"
				username									= "uid=readuser,ou=people,dc=sba,dc=gov"
				password									= "readuser10"
				filter										= "(&(objectclass=person)(uid=#URL.IMUserSuprvId#))"
				attributes									= "givenName,initials,sn,mail"
				start										= "ou=people,dc=sba,dc=gov">
			<cfif getManagerData.RecordCount GT 0>
				<cfset Variables.IMUserSuprvFirstNm			= getManagerData.givenName>
				<cfset Variables.IMUserSuprvMidNm			= getManagerData.initials>
				<cfset Variables.IMUserSuprvLastNm			= getManagerData.sn>
				<cfset Variables.IMUserSuprvEmailAdrTxt		= getManagerData.mail>
			</cfif>
			<cfcontent type="application/json"><!--- **NOT** text/html! --->
			<!--- See comment above about why we use Replace() instead of JSStringFormat(). --->
			<cfoutput>{"IMUserSuprvFirstNm":"#		Replace(Variables.IMUserSuprvFirstNm,							"""","\""","ALL")#"</cfoutput>
			<cfoutput>,"IMUserSuprvMidNm":"#		Replace(Variables.IMUserSuprvMidNm,								"""","\""","ALL")#"</cfoutput>
			<cfoutput>,"IMUserSuprvLastNm":"#		Replace(Variables.IMUserSuprvLastNm,							"""","\""","ALL")#"</cfoutput>
			<cfoutput>,"IMUserSuprvEmailAdrTxt":"#	Replace(Variables.IMUserSuprvEmailAdrTxt,						"""","\""","ALL")#"}</cfoutput>
		</cfif>
		<cfsetting showdebugoutput="No">
		<cfabort>
	</cfif>
	<!--- No URL variables, hence, bad call: --->
	<cfcontent type="application/json"><!--- **NOT** text/html! --->
	<cfoutput>{"ErrMsg":"AJAX callback didn't pass parameters on the URL."}</cfoutput>
	<cfsetting showdebugoutput="No">
	<cfabort>
</cfif>

<!-- **************************************************************************************** -->

<!--- Initializations (for generating form data): --->

<cfset Variables.db											= "oracle_scheduled">
<cfset Variables.LDAPUserId									= "">
<cfset Variables.IMUserSuprvId								= "">
<cfset Variables.IMUserSuprvFirstNm							= "">
<cfset Variables.IMUserSuprvMidNm							= "">
<cfset Variables.IMUserSuprvLastNm							= "">
<cfset Variables.IMUserSuprvEmailAdrTxt						= "">

<!--- IMUserId is of the user being displayed, but IMUserTypCd may be of IT Security user. Therefore, don't trust it: --->
<cfquery name="GetUserTypCd"								datasource="#Variables.db#" dbtype="#Variables.dbtype#">
select	IMUserTypCd											as IMUserTypCdOfUser
from	security.IMUserTbl
where	IMUserId											= #Variables.IMUserId#
</cfquery>
<cfset Variables.IMUserTypCdOfUser							= "">
<cfif GetUserTypCd.RecordCount GT 0>
	<cfset Variables.IMUserTypCdOfUser						= GetUserTypCd.IMUserTypCdOfUser[1]>
</cfif>
<cfset Variables.DispFormFields								= "No"><!--- Only meaningful if GenFormFields is "Yes", obviously. --->
<cfset Variables.GenFormFields								= "No">
<cfswitch expression="#Variables.IMUserTypCdOfUser#"><!--- Allow easy expansion to include SBA Contractors as well: --->
<cfcase value="S"><!--- To include SBA Contractors, make the value attribute "S,C". --->
	<cfset Variables.DispFormFields							= "Yes">
	<cfset Variables.GenFormFields							= "Yes">
</cfcase>
<cfdefaultcase>
	<cfif NOT Variables.UserIsRequestingOwnAccess><!--- Security System > End Users > Update User Profile: --->
		<cfset Variables.GenFormFields						= "Yes">
	<cfelseif CGI.Script_Name IS Variables.SelfName><!--- AJAX callback, in case we ever want to add URL.GetFormFields: --->
		<cfset Variables.GenFormFields						= "Yes">
	</cfif>
</cfdefaultcase>
</cfswitch>
<cfif IsDefined("Variables.AddBusNewGlsAct") AND Variables.AddBusNewGlsAct>
	<cfset Variables.DispFormFields							= "Yes">
	<cfset Variables.GenFormFields							= "Yes">
</cfif>

<!-- **************************************************************************************** -->

<!--- Form Data --->

<cfif Variables.GenFormFields>
	<!--- Same IMUserId can have multiple 'SBA-LDAP' IMCrdntlUserIds. Sort to get most recent one first: --->
	<cfquery name="GetLDAPCredential"						datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select		IMCrdntlUserId
	from		security.IMCrdntlTbl
	where		IMUserId									= #Variables.IMUserId#
	and			IMCrdntlServPrvdrId							= 'SBA-LDAP'
	order by	IMCrdntlCreatDt DESC
	</cfquery>
	<cfif GetLDAPCredential.RecordCount GT 0><!--- Get most recent one: --->
		<cfset Variables.LDAPUserId							= Trim(GetLDAPCredential.IMCrdntlUserId		[1])>
	</cfif>
	<cfquery name="getManagerData"							datasource="#Variables.db#" dbtype="#Variables.dbtype#">
	select	IMUserSuprvId,
			IMUserSuprvFirstNm,
			IMUserSuprvMidNm,
			IMUserSuprvLastNm,
			IMUserSuprvEmailAdrTxt
	from	security.IMUserTbl
	where	IMUserId										= #Variables.IMUserId#
	</cfquery>
	<cfif getManagerData.RecordCount GT 0>
		<cfset Variables.IMUserSuprvId						= Trim(getManagerData.IMUserSuprvId			[1])>
		<cfset Variables.IMUserSuprvFirstNm					= Trim(getManagerData.IMUserSuprvFirstNm	[1])>
		<cfset Variables.IMUserSuprvMidNm					= Trim(getManagerData.IMUserSuprvMidNm		[1])>
		<cfset Variables.IMUserSuprvLastNm					= Trim(getManagerData.IMUserSuprvLastNm		[1])>
		<cfset Variables.IMUserSuprvEmailAdrTxt				= Trim(getManagerData.IMUserSuprvEmailAdrTxt[1])>
	</cfif>
	<cfoutput>
<link href="#Variables.SelfNameStaticCSS#" rel="stylesheet" type="text/css" media="all" />
<script src="#Variables.SelfNameStaticScripts#"></script>
<div id="DivSbaEmpElts" style="display:<cfif NOT Variables.DispFormFields>none</cfif>;"></cfoutput>
	<cfif Variables.UserIsRequestingOwnAccess>
		<!--- End User's view: --->
		<cfif	(Len(Variables.LDAPUserId)					IS 0)
			OR	(Len(Variables.IMUserSuprvId)				IS 0)
			OR	(Len(Variables.IMUserSuprvFirstNm)			IS 0)
			OR	(Len(Variables.IMUserSuprvLastNm)			IS 0)
			OR	(Len(Variables.IMUserSuprvEmailAdrTxt)		IS 0)>
			<cfif NOT IsDefined("Variables.AddBusNewGlsAct")>
				<cfoutput>
  <div class="optlabel">
    <span class="mandlabel">New Security Requirements</span>
    require that your Windows login username be registered in the Security System. 
    You are required to provide the information below this time only. 
    (Any correction must be addressed to IT Security.) 
    <ol>
      <li>Enter your Windows Login in the blue box that has "Your Windows Login" underneath.</li>
      <li>Tab out of the field or press the "Lookup Super by Hierarchy" button.</li>
      <li>If all of the supervisor information is entered into the "Super" fields, 
          you're done (skip ahead to step 6). 
      </li>
      <li>If a message appears to the right of the "Lookup Super by Hierarchy" button that says 
          "Windows Login (something) not found.", do the following:
          <ol style="list-style-type: lower-alpha;">
            <li>If there's a typo in your Windows Login, correct it and return to step 1, above.</li>
            <li>If you entered your Windows Login correctly, contact IT Security and ask them to 
                "propagate my Windows Login from Active Directory to LDAP". 
                They should know what that means. 
            </li>
          </ol>
      </li>
      <li>If a message appears to the right of the "Lookup Super by Hierarchy" button that says 
          "Supervisor (something) not found. Switching to name lookup.", do the following:
          <ol style="list-style-type: lower-alpha;">
            <li>Enter your supervisor's full last name into the Last Name box that just appeared. 
                Press the "Lookup Super by Name" button. 
            </li>
            <li>If the dropdown menu says "Nothing found. (Please try again.)", shorten your supervisor's 
                last name by taking 1 letter off of the end of the name. (For example, if you just searched 
                for "Smith", shorten it to "Smit".) Press the "Lookup Super by Name" button again. 
            </li>
            <li>If the dropdown menu still says "Nothing found. (Please try again.)", shorten your supervisor's 
                last name by taking another letter off of the end of the name and so on. (For example, if 
                you just searched for "Smit", shorten it to "Smi".) Press the "Lookup Super by Name" button 
                again. 
            </li>
            <li>Continue this process until the dropdown menu says "Choose An SBA Employee".</li>
            <li>Choose your supervisor's name from the dropdown menu, 
                which will fill in your supervisor information.
            </li>
          </ol>
      </li>
      <li>If you want to request some roles, you may do so before pressing the "Submit" button at bottom-of-page. </li>
      <li>Alternatively, if you want to make sure that the information you just entered gets saved, 
          you may press "Submit" at bottom of page right away. This will take you back to the Choose Function 
          page again. When you press the Access button again, you will see your supervisor information displayed 
          read-only (grayed out), which tells you that you're free to request roles, because your SBA 
          supervisor information is on file. 
      </li>
    </ol>
  </div><!--- /optlabel (that info isn't available) ---></cfoutput>
				</cfif><!--- /AddBusNewGlsAct --->
			<!--- Don't allow lookup here. User should know their own Windows login username by heart. --->
		<cfelse>
			<cfif NOT IsDefined("Variables.SRV")>
				<cfswitch expression="#Request.SlafDevTestProd#">
				<cfcase value="Dev">	<cfset Variables.SRV= "DEVELOPMENT"	></cfcase>
				<cfcase value="Test">	<cfset Variables.SRV= "TEST"		></cfcase>
				<cfdefaultcase>			<cfset Variables.SRV= "PRODUCTION"	></cfdefaultcase>
				</cfswitch>
			</cfif>
			<cfset Variables.ITSecurityHotlinkSubject		= "#Variables.SRV# - For GLS Login '#Variables.OrigUserId#', "
															& "SBA Supervisor information is incorrect.">
			<cfset Variables.ITSecurityHotlinkBody			= "((Enter information about your correct supervisor here.))">
			<cfset Variables.ITSecurityHotlingHRef			= "mailto:ITSecurity@sba.gov?Subject="
															& URLEncodedFormat(Variables.ITSecurityHotlinkSubject)
															& "&Body="
															& URLEncodedFormat(Variables.ITSecurityHotlinkBody)>
			<cfif Variables.ShowUserTheirOwnWindowsLogin>
				<cfoutput>
  <div class="viewlabel">
    The next box should contain your Windows login username. 
    If incorrect, please contact IT Security. <br>
    <div class="viewdata">
    <input type="Text" name="LDAPUserId" size="15" disabled value="#Variables.LDAPUserId#">
    </div>
  </div><!--- /viewlabel ---></cfoutput>
			</cfif><!--- /ShowUserTheirOwnWindowsLogin --->
			<cfoutput>
  <div class="viewlabel">
    The next box should contain information about your supervisor. 
    If incorrect, please contact IT Security by using 
    <a href="#Variables.ITSecurityHotlingHRef#">this hotlink</a>
    or by calling (202) 481-2946. <br>
    <div class="viewdata" id="DivViewSuprvNames1 nowrap"><!-- Side-by-Side -->
      <!-- Make id unique with "Span...", because MSIE's getElementById also finds form element names: -->
      <div class="viewdata">
        <span class="hdr">Super's Windows Login</span><br/>
        <span class="dtl">#Variables.IMUserSuprvId#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">First</span><br/>
        <span class="dtl">#Variables.IMUserSuprvFirstNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">Middle</span><br/>
        <span class="dtl">#Variables.IMUserSuprvMidNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">Last</span><br/>
        <span class="dtl">#Variables.IMUserSuprvLastNm#</span>
      </div>
      <div class="viewdata">
        <span class="hdr">E-Mail</span><br/>
        <span class="dtl">#Variables.IMUserSuprvEmailAdrTxt#</span>
      </div>
    </div><!--- /DivViewSuprvNames1 = /Side-by-Side --->
  </div><!--- /viewlabel ---></cfoutput>
		</cfif><!--- We already have all 5 Supervisor fields. --->
	<cfelse><!--- Else of UserIsRequestingOwnAccess --->
		<!--- IT Security's view: --->
		<cfoutput>
  <div class="optlabel"><cfif Len(Variables.LDAPUserId) IS 0>
    <span class="mandlabel">New Requirement:</span>
    Supervisor information has not yet been entered into the Security System. Enter <cfelse>
    Supervisor information has already been entered into the Security System. To change, enter </cfif>
    the Windows login user name or you can use the drop-down menu: <br/>
    &nbsp;
  </div><!--- /optlabel --->
  <div class="tbl"></cfoutput>
		<cfif Variables.AllowITSecurityToDoArbitraryLookups>
			<cfif Variables.AllowGivenNameInLookups>
				<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        These 2 lookup fields accept partial "Starts With" name, which may work better than full name: 
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="LDAPLookupUserGiven" class="optlabel">First Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupUserGiven" id="LDAPLookupUserGiven" value="#Variables.IMUserFirstNm#" size="20">
        </div>
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
			<cfelse>
				<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        This lookup accepts partial "Starts With" name, which may work better than full name: 
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
			</cfif><!--- /AllowGivenNameInLookups --->
			<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel"><label for="LDAPLookupUserFamily" class="optlabel">Last Name:</label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Text" name="LDAPLookupUserFamily" value="#Variables.IMUserLastNm#" size="20">
        </div>
        &nbsp;
        <input type="Button" value="Lookup User by Name" onclick="
        this.form.EltDropdownLookupUser.options[0].text = 'Doing lookup. Please wait.';
        $(gEltDropdownLookupUser)
            .load('#Variables.SelfName#?'<cfif Variables.AllowGivenNameInLookups>
                + 'Given='  + escape(this.form.LDAPLookupUserGiven.value)+'&'</cfif>
                + 'Family=' + escape(this.form.LDAPLookupUserFamily.value),
                null,       // No data object because all parameters are on the URL = request will be a 'get'. 
                function()  {MaybeSelectFirstOption(gEltDropdownLookupUser);} // AJAX call completion routine
                );
        "></cfoutput>
			<cfsavecontent variable="Variables.AlreadyLookedUpOptions">
				<cfoutput>
        <option value="">Lookup not done yet. Results will appear here.</option></cfoutput>
			</cfsavecontent>
		<cfelse><!--- Else of AllowITSecurityToDoArbitraryLookups --->
			<cfset Variables.IMUserLastNmLen				= Len(Variables.IMUserLastNm)>
			<cfset Variables.IMUserLastNmEnd				= Variables.IMUserLastNmLen - 2>
			<cfif Variables.IMUserLastNmEnd LT 1>
				<cfset Variables.IMUserLastNmEnd			= 1>
			</cfif>
			<cfloop index="NameLenIdx" from="#Variables.IMUserLastNmLen#" step="-1" to="#Variables.IMUserLastNmEnd#">
				<cfset Variables.IMUserLastNmFragment		= Left(Variables.IMUserLastNm, NameLenIdx)>
				<cfldap
					action									= "QUERY"
					name									= "getUidAndCn"
					server									= "#Variables.LDAPServerName#"
					username								= "uid=readuser,ou=people,dc=sba,dc=gov"
					password								= "readuser10"
					filter									= "(&(objectclass=person)(sn=#Variables.IMUserLastNmFragment#*))"
					attributes								= "uid,cn"
					sort									= "cn"
					start									= "ou=people,dc=sba,dc=gov">
				<cfif getUidAndCn.RecordCount GT 1>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfsavecontent variable="Variables.AlreadyLookedUpOptions">
				<cfif	IsDefined("getUidAndCn")
					AND	(getUidAndCn.RecordCount GT 1)>
					<cfoutput>
        <option value="">Choose An SBA Employee</option></cfoutput>
					<cfloop query="getUidAndCn">
						<cfoutput>
        <option value="#getUidAndCn.uid#">#getUidAndCn.cn#</option></cfoutput>
					</cfloop>
				<cfelse>
					<cfoutput>
        <option value="">Nothing found. (Please investigate Active Directory.)</option></cfoutput>
				</cfif><!--- /getUidAndCn --->
			</cfsavecontent>
			<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel">&nbsp;</div>
      <div class="tbl_cell formdata optlabel">
        The following drop-down menu is based on a lookup of '#Variables.IMUserLastNm#' in Active Directory: 
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
		</cfif><!--- AllowITSecurityToDoArbitraryLookups --->
		<cfoutput>
    <div class="tbl_row" id="DivLookupUser">
      <div class="tbl_cell formlabel optlabel"><label for="EltDropdownLookupUser" class="optlabel">Choose User:</label></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <select name="EltDropdownLookupUser" id="EltDropdownLookupUser" onchange="
        if  (this.selectedIndex > 0)
            {
            this.form.LDAPUserId.value                  = this.options[this.selectedIndex].value;
            this.form.LDAPUserId.onchange();
            }
        ">#Variables.AlreadyLookedUpOptions#
        </select>
        </div><!--- /optdata --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
  </div><!--- /tbl ---></cfoutput>
	</cfif><!--- /UserIsRequestingOwnAccess --->

	<!---
	Most of the preceding was just to put up info for the user, or a dropdown for ITSecurity that eases populating 
	LDAPUserId. This is the point where it starts getting interesting: 
	--->

	<cfif	(Len(Variables.LDAPUserId)						IS 0)
		OR	(Len(Variables.IMUserSuprvId)					IS 0)
		OR	(Len(Variables.IMUserSuprvFirstNm)				IS 0)
		OR	(Len(Variables.IMUserSuprvLastNm)				IS 0)
		OR	(Len(Variables.IMUserSuprvEmailAdrTxt)			IS 0)
		OR	(NOT Variables.UserIsRequestingOwnAccess)>
		<cfif Variables.UserIsRequestingOwnAccess>
			<cfset NameUser									= "Your">
			<cfset NameUserJS								= "Your">
		<cfelse>
			<cfset NameUser									= "User's">
			<cfset NameUserJS								= "User\'s">
		</cfif>
		<cfoutput>
  <div class="tbl" style="width:100%;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
    <div class="tbl_row" style="width:100%;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPUserId" class="mandlabel">#NameUser# Windows Login</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <input type="Text" name="LDAPUserId" id="LDAPUserId" value="#Variables.LDAPUserId#"
        size="15" maxlength="15" onchange="
        if  (EditMask('#NameUserJS# Windows Login', this.value, 'X', 1, 1, this.maxlength))
            {
            var sForm                                   = this.form;
            gDivLookupSuprvStatus.innerHTML             = 'Looking up supervisor information.';
            $.getJSON
                ('#Variables.SelfName#?LDAPUserId=' + escape(this.value),
                null,            // No data object because all parameters are on the URL = request will be a 'get'. 
                function(pData)  // AJAX call completion routine: 
                    {
                    if  (pData.ErrMsg.length == 0)
                        {
                        CopyToSpanAndHidden(sForm, 'IMUserSuprvId',         pData.IMUserSuprvId);
                        CopyToSpanAndHidden(sForm, 'IMUserSuprvFirstNm',    pData.IMUserSuprvFirstNm);
                        CopyToSpanAndHidden(sForm, 'IMUserSuprvMidNm',      pData.IMUserSuprvMidNm);
                        CopyToSpanAndHidden(sForm, 'IMUserSuprvLastNm',     pData.IMUserSuprvLastNm);
                        CopyToSpanAndHidden(sForm, 'IMUserSuprvEmailAdrTxt',pData.IMUserSuprvEmailAdrTxt);
                        if  (   (pData.IMUserSuprvId           == '')
                            &&  (pData.IMUserSuprvFirstNm      == '')
                            &&  (pData.IMUserSuprvMidNm        == '')
                            &&  (pData.IMUserSuprvLastNm       == '')
                            &&  (pData.IMUserSuprvEmailAdrTxt  == '')
                            )
                            {
                            gDivLookupSuprv.style.display               =<cfif Variables.AllowGivenNameInLookups>
                            gDivLookupSuprvGiven.style.display          =</cfif>
                            gDivLookupSuprvFamily.style.display         = '';// a = b = c syntax.
                            gDivLookupSuprvStatus.innerHTML             = 'Supervisor \''	+ pData.ManagerDescription + '\''
                                                                        + 'not found. Use name lookup, then Choose Supervisor:';
                            }
                        else
                            {
                            gDivLookupSuprv.style.display               =<cfif Variables.AllowGivenNameInLookups>
                            gDivLookupSuprvGiven.style.display          =</cfif>
                            gDivLookupSuprvFamily.style.display         = 'none';// a = b = c syntax.
                            gDivLookupSuprvStatus.innerHTML             = 'Supervisor found:';
                            gDivViewSuprvNames2.style.display           = '';
                            }
                        }
                    else // Error occurred on the server:<cfif Request.SlafDevTestProd IS "Dev">
                        {// User's Windows login was not found, but that's allowed in development: 
                        gDivLookupSuprvStatus.innerHTML                 = pData.ErrMsg + '<br/>\n'
                                                                        + '(Okay in development.'<cfif Variables.UserIsRequestingOwnAccess>
                                                                        + ' Use name lookup, then Choose Supervisor.'</cfif>
                                                                        + ')';
                        gDivLookupSuprv.style.display                   =<cfif Variables.AllowGivenNameInLookups>
                        gDivLookupSuprvGiven.style.display              =</cfif>
                        gDivLookupSuprvFamily.style.display             = '';// a = b = c syntax.
                        }<cfelse>
                        gDivLookupSuprvStatus.innerHTML                 = pData.ErrMsg;</cfif>
                    }
                );// End of $.getJSON(). 
            return true;
            }
        this.focus();
        return false;
        ">
        <input type="Hidden" name="SaveLDAPUserId" value="Yes">
        </div><!--- /manddata ---><cfif Variables.UserIsRequestingOwnAccess>
        &nbsp;<!--- Per Ron Whalen, Lookup Supervisor by Hierarchy button does the same thing as the preceding onchange. --->
        <input type="Button"	value="Lookup Supervisor by Hierarchy" onclick="this.form.LDAPUserId.onchange();"></cfif><br/>
        <span id="DivLookupSuprvStatus" class="optlabel">&nbsp;</span>
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---><cfif Variables.AllowGivenNameInLookups>
    <div class="tbl_row" id="DivLookupSuprvGiven" style="width:100%;display:none;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvGiven" class="optlabel">Supervisor's First Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupSuprvGiven"  id="LDAPLookupSuprvGiven"  value="" size="20">
        </div><!--- /optdata --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfif>
    <div class="tbl_row" id="DivLookupSuprvFamily" style="width:100%;display:none;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="LDAPLookupSuprvFamily" class="optlabel">Supervisor's Last Name:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="LDAPLookupSuprvFamily" id="LDAPLookupSuprvFamily" value="" size="20">
        &nbsp;
        <input type="Button" value="Lookup Supervisor by Name" onclick="
        gEltDropdownLookupSuprv.options[0].text       = 'Doing lookup. Please wait.';
        $(gEltDropdownLookupSuprv)
            .load('#Variables.SelfName#?'<cfif Variables.AllowGivenNameInLookups>
                + 'Given=' + escape(this.form.LDAPLookupSuprvGiven.value)+'&'</cfif>
                + 'Family='+ escape(this.form.LDAPLookupSuprvFamily.value)+'&'
                + 'GetAll=Yes',
                null,      // No data object because all parameters are on the URL = request will be a 'get'. 
                function() {MaybeSelectFirstOption(gEltDropdownLookupSuprv);} // AJAX call completion routine
                );
        ">
        </div><!--- /optdata --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
    <div class="tbl_row" id="DivLookupSuprv" style="width:100%;display:none;"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel"><label for="EltDropdownLookupSuprv" class="mandlabel">Choose Supervisor:</label></div>
      <div class="tbl_cell formdata">
        <div class="manddata">
        <select name="EltDropdownLookupSuprv" id="EltDropdownLookupSuprv" onchange="
        if  (this.selectedIndex > 0)
            {
            var sForm                                 = this.form;
            var sValue                                = this.options[this.selectedIndex].value.split('|');
            CopyToSpanAndHidden(sForm, 'IMUserSuprvId',          sValue[0]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvFirstNm',     sValue[1]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvMidNm',       sValue[2]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvLastNm',      sValue[3]);
            CopyToSpanAndHidden(sForm, 'IMUserSuprvEmailAdrTxt', sValue[4]);
            gDivViewSuprvNames2.style.display                         = '';
            }
        ">
        <option value="">Lookup not done yet. Results will appear here.</option>
        </select>
        </div><!--- /manddata --->
          <input type="Hidden" name="IMUserSuprvId"             value="#Variables.IMUserSuprvId#">
          <input type="Hidden" name="IMUserSuprvFirstNm"        value="#Variables.IMUserSuprvFirstNm#">
          <input type="Hidden" name="IMUserSuprvMidNm"          value="#Variables.IMUserSuprvMidNm#">
          <input type="Hidden" name="IMUserSuprvLastNm"         value="#Variables.IMUserSuprvLastNm#">
          <input type="Hidden" name="IMUserSuprvEmailAdrTxt"    value="#Variables.IMUserSuprvEmailAdrTxt#">
      </div><!--- /formdata --->
    </div><!--- /tbl_row ---></cfoutput>
	<cfset Variables.ShowDivViewSuprvNames2				=	(Len(Variables.IMUserSuprvId			) gt 0)
														or	(Len(Variables.IMUserSuprvFirstNm		) gt 0)
														or	(Len(Variables.IMUserSuprvMidNm			) gt 0)
														or	(Len(Variables.IMUserSuprvLastNm		) gt 0)
														or	(Len(Variables.IMUserSuprvEmailAdrTxt	) gt 0)>
	
	<cfoutput>
    <div class="tbl_row" id="DivViewSuprvNames2" style="width:100%;<cfif NOT Variables.ShowDivViewSuprvNames2> display:none;</cfif>"><!--- style="width:100%;" necessary for formlabel/formdata alignment. --->
      <div class="tbl_cell formlabel viewlabel">Supervisor:</div>
      <div class="tbl_cell formdata">
        <div class="viewdata nowrap"><!-- Side-by-Side -->
          <!-- Make id unique with "Span...", because MSIE's getElementById also finds form element names: -->
          <div class="viewdata">
            <span class="hdr"                                >Super's Windows Login</span><br/>
            <span class="dtl" id="SpanIMUserSuprvId"         >#Variables.IMUserSuprvId#</span>
          </div>
          <div class="viewdata">
            <span class="hdr"                                >First</span><br/>
            <span class="dtl" id="SpanIMUserSuprvFirstNm"    >#Variables.IMUserSuprvFirstNm#</span>
          </div>
          <div class="viewdata">
            <span class="hdr"                                >Middle</span><br/>
            <span class="dtl" id="SpanIMUserSuprvMidNm"      >#Variables.IMUserSuprvMidNm#</span>
          </div>
          <div class="viewdata">
            <span class="hdr"                                >Last</span><br/>
            <span class="dtl" id="SpanIMUserSuprvLastNm"     >#Variables.IMUserSuprvLastNm#</span>
          </div>
          <div class="viewdata">
            <span class="hdr"                                >E-Mail</span><br/>
            <span class="dtl" id="SpanIMUserSuprvEmailAdrTxt">#Variables.IMUserSuprvEmailAdrTxt#</span>
          </div>
        </div><!--- /DivViewSuprvNames2 = /Side-by-Side --->
      </div><!--- /formdata --->
    </div><!--- /tbl_row --->
  </div><!--- /tbl ---></cfoutput>
	</cfif>
	<cfoutput>
</div><!--- /id="DivSbaEmpElts" ---></cfoutput>
</cfif><!--- /Variables.GenFormFields --->
<!---
/security/user/dsp_user.cfm doesn't use enablecfoutputonly="Yes". But the first line of this file does that only in 
case of AJAX callback. All AJAX callbacks terminate in cfaborts, so if control got this far, we never called cfsetting. 
Therefore, there's no need to revert enablecfoutputonly back to "No". 
--->

<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		AJAX routines for lookups and validations. 
NOTES:

	This file is **NOT** included in dsp_userprofile.cfm's ListFieldsetsAll variable!! 
	The last statement in this file is a cfabort!! 
	This file should NEVER be cfincluded unless it's an AJAX callback!! 

	It's just a separate for ease of maintenance. We're getting it out of the way so that we don't have to see it while 
	working on dsp_userprofile. No need to check it out unless one's adding or modifying an AJAX function. Neat. Tidy. 

INPUT:				URL.Action. Other URL variables that vary according to URL.Action. 
OUTPUT:				Varies according to URL.Action. 
REVISION HISTORY:	07/30/2015, SRS:	Original implementation. 
--->

<!--- ************************************************************************************************************ --->

<!--- Configuration Parameters: --->

<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfset Variables.LibURL										= "/library">
<cfset Variables.LibIncURL									= "#Variables.LibURL#/cfincludes">
<cfif NOT IsDefined("Request.SlafDevTestProd")>
	<cfinclude template="#Variables.LibIncURL#/get_sbashared_variables.cfm">
</cfif>
<cfset Variables.password									= "">
<cfset Variables.username									= "">
<cfset Variables.ValidateSupervisorMaxLen					= 100>

<!--- ************************************************************************************************************ --->

<cfset Variables.ErrMsg										= "">
<cfset Variables.TxnErr										= "No">

<cfswitch expression="#URL.Action#">

<!--- DetermineLSP: --->

	<cfcase value="DetermineLSP">
		<cfparam name="URL.HQLoc"							default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfparam name="URL.Num"								default=""><!--- Deliberately not the same as our column name, for crypticity. --->
		<cfset Variables.ReturnString						= "">
		<cfif NOT IsNumeric(URL.HQLoc)>
			<cfset Variables.ReturnString					= "Error: Headquarters Location missing.">
		<cfelseif NOT IsNumeric(URL.Num)>
			<cfset Variables.ReturnString					= "Error: User identifier missing.">
		<cfelse>
			<cfset Variables.HQLocId						= URL.HQLoc>
			<cfif URL.Num is 0>
				<cfset Variables.IMUserId					= "">
			<cfelse>
				<cfset Variables.IMUserId					= URL.Num>
			</cfif>
			<cfset Variables.LSPLocIdCnt					= ""><!--- Initialize output parameter, deliberately making it non-numeric. --->
			<cfset Variables.cfprname						= "NoResultSetReturned">
			<cfset Variables.LogAct							= "determine whether #Variables.HQLocId# is an LSP">
			<cfinclude template="/cfincludes/oracle/security/spc_LSPLOCIDSELCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.ReturnString				= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& "): "
															& Variables.ErrMsg><!--- Because it contains CFCatch info. --->
				<!--- ... and please tell us the "code" so we can look in the log files!!! --->
			<cfelse>
				<cfset Variables.LSPLocIdCnt				= Trim(Variables.LSPLocIdCnt)>
				<cfif IsNumeric(Variables.LSPLocIdCnt)>
					<cfset Variables.ReturnString			= Variables.LSPLocIdCnt><!--- SUCCESS! --->
				<cfelse>
					<cfset Variables.ReturnString			= "Error: The attempt to #Variables.LogAct# did not return a number: '#Variables.LSPLocIdCnt#'.">
				</cfif>
			</cfif>
		</cfif>
		<cfcontent type="text/plain">
		<cfoutput>#Variables.ReturnString#</cfoutput>
	</cfcase>

<!--- LookupSupervisor: --->

	<cfcase value="LookupSupervisor">
		<cfparam name="URL.EMail"							default="">
		<cfparam name="URL.First"							default="">
		<cfparam name="URL.HQLoc"							default="">
		<cfparam name="URL.Last"							default="">
		<cfparam name="URL.UserType"						default="">
		<cfset Variables.LookupSupervisorErrMsg				= "">
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)
			and	(Len(URL.UserType)							is 0)><!--- Probably won't ever happen. --->
			<cfset Variables.LookupSupervisorErrMsg			= "Error: User Type not selected.">
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)>
			<cfif			(URL.UserType					is "U")>
				<!--- IsNumeric already includes a length check, but do them separately for more helpful feedback: --->
				<cfif		(Len(URL.HQLoc)					is 0)>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Headquarters Location missing.">
				<cfelseif	(NOT IsNumeric(URL.HQLoc))>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Headquarters Location is not numeric.">
				</cfif>
			<cfelse>
				<!--- If UserType is not "U" (user is not a partner), HQLoc may *not* participate in the search: --->
				<cfset URL.HQLoc							= "">
				<cfif		(Len(URL.EMail)					is 0)
					and		(Len(URL.First)					is 0)
					and		(Len(URL.Last)					is 0)>
					<cfset Variables.LookupSupervisorErrMsg	= "Error: Enter something for E-Mail, First Name or Last Name.">
				</cfif>
			</cfif><!--- /UserType --->
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)>
			<cfset Variables.Identifier						= "0">
			<cfset Variables.IMUserId						= ""><!--- Not used by IMUserSuprvsrSelCSP yet. --->
			<cfset Variables.IMUserTypCd					= URL.UserType>
			<cfset Variables.HQLocId						= URL.HQLoc>
			<cfset Variables.IMUserEmailAdrTxt				= URL.EMail>
			<cfset Variables.IMUserFirstNm					= URL.First>
			<cfset Variables.IMUserLastNm					= URL.Last>
			<cfset Variables.cfprname						= "getSupers">
			<cfset Variables.ErrMsg							= "">
			<cfset Variables.LogAct							= "get supervisors by first name, last name and/or email">
			<cfset Variables.TxnErr							= "No">
			<!--- Not using dbutils_for_dbtype here, else this would be spcn('IMUserSuprvsrSelCSP', 'security') --->
			<cfinclude template="/cfincludes/oracle/security/spc_IMUSERSUPRVSRSELCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.LookupSupervisorErrMsg		= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ").">
				<!--- ... and please tell us the "code" so we can look in the log files!!! --->
			</cfif>
		</cfif>
		<cfif	(Len(Variables.LookupSupervisorErrMsg)		is 0)
			and	(getSupers.RecordCount						is 0)>
			<cfset Variables.LookupSupervisorErrMsg			= "Nothing found. (Please try again.)">
		</cfif>
		<cfsavecontent variable="Variables.Options">
			<cfif Len(Variables.LookupSupervisorErrMsg)		is 0>
				<cfoutput>
		<option value="">Choose Your Supervisor</option></cfoutput>
				<cfloop query="getSupers">
					<cfset Variables.OptionValue			=  "#Trim(getSupers.IMUserNm)#"
															& "|#getSupers.IMUserFirstNm#"
															& "|#getSupers.IMUserMidNm#"
															& "|#getSupers.IMUserLastNm#"
															& "|#getSupers.IMUserEmailAdrTxt#"
															& "|#getSupers.IMUserId#">
					<cfset Variables.OptionText				= "#getSupers.IMUserLastNm#"
															& ", #getSupers.IMUserFirstNm#"
															& " (#getSupers.IMUserTypDescTxt#)">
					<cfoutput>
		<option value="#Variables.OptionValue#">#Variables.OptionText#</option></cfoutput>
				</cfloop>
			<cfelse>
				<cfoutput>
		<option value="">#Variables.LookupSupervisorErrMsg#</option></cfoutput>
			</cfif>
		</cfsavecontent>
		<cfcontent type="text/html">
		<cfoutput>#Variables.Options#</cfoutput>
	</cfcase>

<!--- ValidateSupervisor: --->

	<cfcase value="ValidateSupervisor">
		<cfparam name="URL.UserName"						default="">
		<cfparam name="URL.UserType"						default="">
		<cfif		Len(URL.UserType)						is 0>
			<cfset Variables.SuprvInd						= "A"><!--- Supervisor AOk, because UserType "" doesn't need one. --->
		<cfelseif	ListFindNoCase("C,S,U", URL.UserType)	is 0>
			<cfset Variables.SuprvInd						= "A"><!--- Supervisor AOk, because UserType doesn't need one. --->
		<cfelseif	Len(URL.UserName)						is 0>
			<cfset Variables.SuprvInd						= "I"><!--- Supervisor invalid, probably because in addcustomer. --->
		<cfelse>
			<cfset Variables.IMUserNm						= URL.UserName>
			<cfset Variables.IMUserTypCd					= URL.UserType>
			<cfset Variables.SuprvInd						= ""><!--- Output parameter of ValidateIMUserSuprvCSP --->
			<cfset Variables.cfprname						= "Ignored">
			<cfset Variables.ErrMsg							= "">
			<cfset Variables.LogAct							= "validate supervisor (required for new User Type)">
			<cfset Variables.TxnErr							= "No">
			<!--- Not using dbutils_for_dbtype here, else this would be spcn('ValidateIMUserSuprvCSP', 'security') --->
			<cfinclude template="/cfincludes/oracle/security/spc_VALIDATEIMUSERSUPRVCSP.cfm">
			<cfif Variables.TxnErr>
				<cfset Variables.SuprvInd					= "Error: A database error occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ")."><!--- Treated as invalid, allows debug. --->
			<cfelseif Len(Variables.SuprvInd) is 0>
				<cfset Variables.SuprvInd					= "I"><!--- Supervisor invalid, probably because in addcustomer. --->
			<cfelseif ListFind("A,I", Variables.SuprvInd)	is 0>
				<!--- Try to avoid scaring the user, but still, try to see as much of p_SuprvInd as "MaxLen" allows: --->
				<cfif Len(Variables.SuprvInd)				gt Variables.ValidateSupervisorMaxLen>
					<cfset Variables.SuprvInd				= Left(Variables.SuprvInd, Variables.ValidateSupervisorMaxLen)>
				</cfif>
				<cfset Variables.SuprvInd					= "Error: An invalid lookup occurred "
															& "(code "
															& LCase(Left(Request.SlafDevTestProd, 1))
															& Request.SlafServerUniqueCode
															& ", '#Variables.SuprvInd#')."><!--- Treated as invalid. --->
			<cfelse>
				<!---
				We sometimes get trailing space appended. Trim in case it helps, but usually it doesn't. (Apparently, 
				jQuery's AJAX is appending it to jqXHR.responseText. So we have to trim it on the client too.) On the 
				other hand, it doesn't **hurt** to trim it on the server too: 
				--->
				<cfset Variables.SuprvInd					= Trim(Variables.SuprvInd)>
				<!--- Stored procedure's output p_SuprvInd was "A", "I" or "", so let that stand as our response. --->
			</cfif>
		</cfif>
		<cfcontent type="text/plain">
		<cfoutput>#Variables.SuprvInd#</cfoutput>
	</cfcase>

</cfswitch>

<!--- Shared return to the caller: --->

<cfsetting showdebugoutput="No">
<cfabort>

<!--- ************************************************************************************************************ --->

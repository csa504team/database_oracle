<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration.
DATE:				07/30/2015.
DESCRIPTION:		Currently just a validation strategy tester. Evolving into the real thing.
NOTES:				None.
INPUT:				Form variables, Variables.CancellationPage, Session.Origs.
OUTPUT:				Save profile to the security schema.
REVISION HISTORY:	07/30/2015,	SRS:	Original implementation as part of the new act_userprofile that consolidates all user
										profile act pages. We now have no prior knowledge as to which which columns a caller may
										chose to make uneditable, so we have to rely on the Origs structure built in the initial
										load of dsp_userprofile from the database.
--->

<!--- Configuration Parameters: --->
<cfdump var="#form#">
<cfparam name="Variables.CancellationPage"	type="String">	<!--- No default attribute, therefore, a mandatory input. --->
<cfset Variables.DisplayPage								= Replace(Variables.PageName, "act_", "dsp_", "One")>
<cfparam name="Variables.SuccessPage"		type="String"	default="#Variables.DisplayPage#">
<cfset Variables.db											= "oracle_housekeeping">
<cfset Variables.dbtype										= "Oracle80">
<cfparam name="Variables.UpdateTrace"						default="No">

<!--- Check whether user should even be at this page: --->

<cfif		NOT IsDefined("Form.FieldNames")>
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
<cfelseif	IsDefined("Form.SubmitButton")
	and		(Form.SubmitButton is "Cancel")><!--- Keep the cancellation SubmitButton value in sync with dsp_userprofile. --->
	<cflocation addtoken="No" url="#Variables.CancellationPage#">
</cfif>

<cfinclude template="#Variables.LibUdfURL#/bld_DspPageLoadTimeOrigsUDFs.cfm">
<cfinclude template="#Variables.LibUdfURL#/bld_PasswordUDFs.cfm">
<cfset GetDspPageLoadTimeOrigs()><!--- Retrieve Session.Origs. We mainly want "MayEdit" right now. --->
<cfif NOT Variables.OrigsSuccess>
	<!---
	If OrigsSuccess isn't "Yes", it probably means that Session.DspPageLoadTimeOrigs.DspPage_Handshake wasn't defined,
	or, if it was, the Handshake didn't reference the right display page. Either way, it's rather equivalent to detecting
	NOT IsDefined("Form.FieldNames"). So treat it the exact same way as above:
	--->
	<cflocation addtoken="No" url="#Variables.DisplayPage#"><!--- Just go there, without PrevErr and without ErrMsg. --->
</cfif>

<cfif NOT Variables.Origs.MayEdit><!--- We WANT to crash if MayEdit is undefined (to debug), so just assume it is defined. --->
	<!---
	This condition will probably never happen, because the dsp page should set ReadOnlyProfile, which should result in
	its not generating the form nor its submit buttons. But theoretically, a hacker could manufacture a form submission.
	So there's no harm done in planning for it and telling the hacker "shame on you":
	--->
	<cfset Variables.ErrMsg									= "Error: You are not allowed to save this profile to the database. ">
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<!--- Initializations: --->

<cfparam name="Variables.Commentary"						default="">
<cfset Variables.ErrMsgBase									= "Error(s) occurred that prevented saving the page's data: ">
<cfset Variables.ErrMsg										= Variables.ErrMsgBase>
<cfset Variables.SaveMe										= "Yes">
<cfset Variables.TxnErr										= "No">
<cfset Variables.PageIsAddCustomer							= "No">
<cfset Variables.PageIsProfile								= "No">
<cfset Variables.PageIsUser									= "No">
<cfswitch expression="#CGI.Script_Name#">
<cfcase value="/cls/act_addcustomer.cfm,/cls/act_addcustomer2_shared.cfm">
	<cfset Variables.PageIsAddCustomer						= "Yes">
</cfcase>
<cfcase value="/cls/act_profile.cfm,/cls/act_profile2_shared.cfm">
	<cfset Variables.PageIsProfile							= "Yes">
</cfcase>
<cfcase value="/security/user/act_user.cfm,/security/user/act_user2_shared.cfm">
	<cfset Variables.PageIsUser								= "Yes">
</cfcase>
</cfswitch>

<cfinclude template="#Variables.LibIncURL#/bld_ServerCachedQueries.cfm">
<cflock scope="Server" type="ReadOnly" timeout="30">
	<cfset Variables.ActvIMAsurncLvlTbl						= Server.Scq.security.ActvIMAsurncLvlTbl>
	<cfset Variables.ActvIMJobTitlTypTbl					= Server.Scq.security.ActvIMJobTitlTypTbl>
	<cfset Variables.ActvIMUserSuspRsnTbl					= Server.Scq.security.ActvIMUserSuspRsnTbl>
	<cfset Variables.ActvIMUserTypTbl						= Server.Scq.security.ActvIMUserTypTbl>
	<cfset Variables.ScqActvCountries						= Server.Scq.ActvIMCntryCdTbl>	<!--- Oddly, not used by addcustomer. --->
	<cfset Variables.ScqActvStates							= Server.Scq.ActvStTbl>			<!--- Oddly, not used by addcustomer. --->
</cflock>

<!--- Simple Validity Edits: --->

<cfif Variables.UpdateTrace>
	<cfset Variables.Commentary								&= "About to call val_calls.<br/>">
</cfif>
<cfinclude template="act_userprofile.val_calls_shared.cfm"><!--- Does it's own cflocation if SaveMe is "No". --->
<cfif Variables.UpdateTrace or "Yes"><!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed simple validity edits.<br/>">
</cfif>

<!--- Cross Edits Go Here: --->

<cfset Variables.CallValidateIMUserSuprvCSP					= "No">
<cfif Len(Variables.IMUserTypCd) gt 0>
	<cfif ListFind(ValueList(Variables.ActvIMUserTypTbl.code), Variables.IMUserTypCd) is 0>
		<cfset Variables.FieldErrMsg						= "Invalid User Type.">
		<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	<cfelseif Len(Variables.JobTitlCd) gt 0>
		<cfloop index="Job" list="#Variables.JobTitlCd#">
		<cfif job neq 6><!--- temp --->
			<cfset Variables.JobAllowed						= "No">
			<cfset Variables.JobDescription					= "Job Classification code #Job#">
			<cfloop query="Variables.ActvIMJobTitlTypTbl">
				<cfif Variables.ActvIMJobTitlTypTbl.code is Job>
					<cfset Variables.JobDescription			= Variables.ActvIMJobTitlTypTbl.description>
					<cfif Variables.ActvIMJobTitlTypTbl.IMUserTypCd is Variables.IMUserTypCd>
						<cfset Variables.JobAllowed			= "Yes">
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>
			<cfif NOT Variables.JobAllowed>
				<cfset Variables.FieldErrMsg				= "#Variables.JobDescription# is not a valid Job Classification for User Type.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfif>
		</cfloop>
	</cfif>
	<cfswitch expression="#Variables.IMUserTypCd#">
	<cfcase value="C,S,U">
		<cfif	(Len(Variables.IMUserSuprvFirstNm)			gt 0)
			and	(Len(Variables.IMUserSuprvLastNm)			gt 0)
			and	(Len(Variables.IMUserSuprvEmailAdrTxt)		gt 0)><!--- But not IMUserSuprvId, IMUserSuprvMidNm or IMUserSuprvUserId (optional). --->
			<cfset Variables.CallValidateIMUserSuprvCSP		= "Yes">
			<!--- Could call ValidateIMUserSuprvCSP here, or could delay it and use CallValidateIMUserSuprvCSP. --->
		<cfelse>
			<cfset Variables.FieldErrMsg					= "Please lookup and choose a supervisor.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfcase>
	</cfswitch>

	<cfif Variables.PageIsAddCustomer>
		<cfif len(Variables.Verify) gt 0>
			<cfdump var="Verify=#Variables.Verify#"><cfdump var="Origs.Verify=#Variables.Origs.Verify#">
			<cfif Variables.Verify is not Variables.Origs.Verify>
				<cfset Variables.FieldErrMsg	= "Verify test answer failed. Please retry.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				<cfdump var="FieldErrMsg=#Variables.FieldErrMsg#">
			</cfif>
		<cfelse>
			<cfset Variables.FieldErrMsg		= "Please answer CAPTCHA test question.">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>
	</cfif>
	<!--- <cfdump var="ErrMsg=#Variables.ErrMsg#"><cfabort> --->
	<cfif Variables.PageIsAddCustomer or Variables.PageIsProfile>
		<cfif Variables.PageIsAddCustomer>
			<cfif Len(Variables.NewPass1) IS 0 or Len(Variables.NewPass2) IS 0>
				<cfset Variables.FieldErrMsg	= "Enter a new password.">
				<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
			</cfif>
		</cfif>
		<!--- If fields changed and passwords do not match--->
		<cfif Compare(Variables.NewPass1,Variables.NewPass2)  IS NOT 0>
			<cfset Variables.FieldErrMsg		 = "The 2 passwords did not match. Please try again">
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		<cfelseif not PasswordValidate("cls", Variables.NewPass1)><!--- CHECK FOR INVALID CHARS  --->
			<cfset Variables.FieldErrMsg		 = Variables.PWUDF.ErrMsg>
			<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
		</cfif>

	</cfif>
	<!--- <cfdump var="#Variables.oRIGS#"> --->
	<cfif Variables.IMUserTypCd eq "N"><!--- IAC 07/01/2015 Need to look at Job title --->
	 	<cfif isDefined("Variables.JobTitlCd")><!--- have 2 possible codes 7,8 if it is not defined then none was selected. Should be caught by dsp page--->
			<cfif listfind(Variables.JobTitlCd, 7)>
				<cfset Variables.Taxid  = "1" & Variables.TaxId1 &  Variables.TaxId2 &  Variables.TaxId3>
				<cfset Variables.TaxidRetypd  = "1" & Variables.TaxId4 &  Variables.TaxId5 &  Variables.TaxId6>
				<cfif Variables.TaxidRetypd is not Variables.Taxid>
					<cfset Variables.FieldErrMsg		 = "Re-entered SSN does not match with entered SSN">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>

				<cfif Len(Variables.LoanNmb) gt 0>
					<cfset Variables.LoanNmbForVal			= Trim(Variables.LoanNmb)><!--- Forgive spaces. --->
					<cfswitch expression="#Len(Variables.LoanNmbForVal)#">
					<cfcase value="8">
						<cfif IsNumeric(Variables.LoanNmbForVal)>
							<cfset Variables.LoanNmb		= gen_chkdgt(Variables.LoanNmbForVal)><!--- Returns 10 digits. --->
						<cfelse>
							<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
																	& "9999999999 or 99999999-99 format.">
								<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif>
					</cfcase>
					<cfcase value="10">
						<cfif IsNumeric(Variables.LoanNmbForVal)>
							<cfset Variables.LoanNmb		= Variables.LoanNmbForVal>
						<cfelse>
							<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
																	& "9999999999 or 99999999-99 format.">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif>
					</cfcase>
					<cfcase value="11">
						<cfif	IsNumeric	(Mid(Variables.LoanNmbForVal, 1, 8))
							and				(Mid(Variables.LoanNmbForVal, 9, 1) is "-")
							and	IsNumeric	(Mid(Variables.LoanNmbForVal,10, 2))>
							<cfset Variables.LoanNmb		= Replace(Variables.LoanNmbForVal, "-", "", "All")>
						<cfelse>
							<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
																	& "9999999999 or 99999999-99 format.">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif>
					</cfcase>
					<cfdefaultcase>
						<cfset Variables.FieldErrMsg		 = "SBA Loan number must be in 99999999, "
																	& "9999999999 or 99999999-99 format.">
						<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
					</cfdefaultcase>
					</cfswitch>
				<cfelse>
					<cfset Variables.FieldErrMsg		 = "SBA Loan Number is mandatory">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif><!--- end loannmb  length check --->

				<!--- Check if disaster loan first --->
				<cfquery  name="GetPrgmCd" datasource="#Variables.db#" username="#Variables.username#" password="#Variables.password#" dbtype="#Variables.dbtype#">
					SELECT PRGMCD
					FROM LOAN.LOANGNTYTBL
				    WHERE LOANNMB = <cfqueryparam value="#Variables.LOANNMB#" cfsqltype="CF_SQL_VARCHAR">
				</cfquery>
				<cfif GetPrgmCd.recordcount gt 0>
					<cfif trim(GetPrgmCd.PRGMCD) neq "H">
						<cfset Variables.FieldErrMsg		 = "Invalid Loan number.">
						<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
					</cfif>
				<cfelse>
					<cfset Variables.FieldErrMsg		 = "Invalid Loan number.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>


				<cfscript>
					Variables.Identifier					= "0";
					Variables.LogAct          				= "Get Info";
					Variables.cfpra							= "";
					Variables.LoanDisbColumns				= 1;
					Variables.cfpra							= ArrayNew(2);

					i								= 0;
					i = i + 1; Variables.cfpra[i][1]= "getLoanGnty";	 	Variables.cfpra[i][2] = i;
					i = i + 1; Variables.cfpra[i][1]= "getLoanBorr"; 		Variables.cfpra[i][2] = i;
					i = i + 1; Variables.cfpra[i][1]= "getLoanLst"; 		Variables.cfpra[i][2] = i;
					Variables.LogAct				= "retrieve disaster borr info dsp";
					include "/cfincludes/oracle/loan/spc_LOANNONPARTINFOSELCSP.cfm";
					if (Variables.TxnErr){
						Variables.SaveMe 		= "No";
						writeDump(Variables.ErrMsg);abort;
					}
				</cfscript>
				<cfif isDefined("Retval") and  (Retval eq 0)>
	            	<cfset Variables.FieldErrMsg		 = "Please enter valid Loan Number and SSN.">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
	            </cfif>
			</cfif>

			<cfif listfind(Variables.JobTitlCd, 8)><!--- SBG Customer. Change later to Variables.!!--->
				<cfset Variables.Taxid  = "0" & form.EIN1 &  form.EIN2>
				<cfset Variables.ReTaxid  = "0" & form.EIN3 &  form.EIN4>

				<cfif Variables.ReTaxid is not Variables.Taxid>
					<cfset Variables.FieldErrMsg		 = "Re-entered EIN does not match with entered EIN">
					<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
				</cfif>
				<!--- Check DUNS - handled already--->
				<cfif Variables.SaveMe><!--- Need to check for an active Pronet profile for EIN and DUNS number --->
					<!--- cannot rely on web serivce as DSBS might time out. Use cfhttp instead --->
					<cfset Variables.WSDLURL = "https://eweb1.sba.gov/pro-net/ws/wbs_sbainternalservices.cfc?method=GetMiniProfileByDUNSOrTaxId">
					<cfhttp url="#Variables.WSDLURL#" method="POST" timeout="90">
						<cfhttpparam type="Formfield" value="#Variables.Taxid#" name="TaxId"><!--- value="012345678" --->
					</cfhttp>
					<cfset Variables.ReturnResponce = htmlcodeformat(cfhttp.filecontent)>
						<cfif findnocase("wddxPacket",Variables.ReturnResponce) GT 0><!--- good call--->
							<cfset Variables.contentXML =xmlparse(cfhttp.filecontent)>
							<cfwddx action="wddx2cfml" input=#contentXML# output="contentXML_data"/>
							<!--- 	<cfdump var=#Variables.contentXML#><cfabort> --->
							<cfif isDefined("contentXML_data.DUNS")>
								<cfset variables.dsbsDUNS=Variables.contentXML_data.DUNS>

								<cfif Variables.DUNS EQ variables.dsbsDUNS and Variables.contentXML_data.STATUS EQ "A"><!--- Match with active profile --->
											<cfset variables.ValidEINDUNS = "Yes">
								<cfelse>
									<cfset Variables.DUNSErrMsg	= "<li>EIN & DUNS not found in DSBS</li>">
									<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
									<cfset Variables.SaveMe					= "No">
								</cfif>
							<cfelse>
								<cfset Variables.DUNSErrMsg	= "<li>Tax ID not found in DSBS</li>">
								<cfset Variables.ErrMsg					&= Variables.DUNSErrMsg>
								<cfset Variables.SaveMe					= "No">
							</cfif>
						<cfelse>
							<cfset Variables.FieldErrMsg		 = "Tax ID not found in DSBS">
							<cfinclude template="/library/cfincludes/val_FormatErrMsg.cfm">
						</cfif><!--- end wddxPacket --->
				</cfif><!--- end saveme ---> <!--- <cfdump var=#variables#><cfabort> --->
			</cfif><!--- end job=8 --->
		<cfelse><!--- Job title not defined (means neither was selected so display error. --->
				<cfset Variables.JobTitleErrMsg	= "<li>Borrowers must select Bond Borrower or Loan Borrower </li>">
				<cfset Variables.ErrMsg					&= Variables.JobTitleErrMsg>
                <cfset Variables.SaveMe					= "No">
		</cfif>
	</cfif><!--- end Variables.IMUserTypCd eq "N" --->

</cfif><!--- end usertypcd gt 0 --->
<!--- Add more here. Examples: --->
	<!--- 1. If Password or Re-enter Password given, they have to pass PasswordValidate (in bld_PasswordUDFs) and match each other. --->
	<!--- 2. If not done above, and if CallValidateIMUserSuprvCSP is yes, call ValidateIMUserSuprvCSP. --->
	<!--- 3. Need to validate other fields that have reference tables against ServerCachedQueries. --->

<cfif NOT Variables.SaveMe><!--- ((in cross edits)) --->
	<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
	<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
</cfif>

<cfif Variables.UpdateTrace or "Yes"><!--- For now, always add this. Later, remove 'or "Yes"' to add comment only if developer wants an UpdateTrace. --->
	<cfset Variables.Commentary								&= "The page passed cross edits.<br/>">
</cfif>

<!--- Simple mechanism for detecting changed data, based on current data structures: --->

<cffunction name="SomethingChanged"							returntype="boolean">
	<cfargument name="ColumnList"							type="String" required="Yes">
	<cfset var Local										= {}>
	<cfloop index="Local.ColName" list="#Arguments.ColumnList#">
		<cfif Variables[Local.ColName]						is not Variables.Origs[Local.ColName]>
			<cfreturn true>
		</cfif>
	</cfloop>
	<cfreturn false>
</cffunction>

<cfset Variables.ColumnListIMUserTbl						= "IMUserNm,IMUserTypCd,"
															& "IMUserPrtNm,IMUserFirstNm,IMUserLastNm,IMUserMidNm,IMUserSfxNm,"
															& "IMUserDOBDt,IMUserLast4DgtSSNNmb,"
															& "IMUserStr1Txt,IMUserStr2Txt,IMUserCtyNm,StCd,Zip5Cd,Zip4Cd,IMCntryCd,IMUserStNm,IMUserPostCd,"
															& "IMUserPhnCntryCdIMUserPhnAreaCd,IMUserPhnLclNmb,IMUserPhnExtnNmb,"
															& "IMUserEmailAdrTxt,IMUserEmailAdrTxt2,"
															& "HQLocId,LocId,PrtId,PrtLocNm,SBAOfcCd,SBAOfc1Nm,IMAsurncLvlCd,"
															& "IMAsurncLvlCd,IMUserActvInactInd,IMUserSuspRsn,IMUserSuspRsnCd,IMUserSuspRsnDt,"
															& "IMUserSuprvId,IMUserSuprvFirstNm,IMUserSuprvMidNm,IMUserSuprvLastNm,IMUserSuprvEmailAdrTxt,IMUserSuprvUserId">
<cfset Variables.ColumnListBusinesses						= "IMUserBusSeqNmb,IMUserBusEINSSNInd,IMUserBusTaxId,IMUserBusLocDUNSNmb,BusEINCertInd">
<!--- Etc. --->

<!--- !!!! remove later, duplicate --->
	<cfif SomethingChanged(Variables.ColumnListIMUserTbl)>
		<!--- Save these IMUserTbl columns here. --->
		<cfloop index="Local.ColName" list="#Variables.ColumnListIMUserTbl#">
			<cfif Variables[Local.ColName] is not Variables.Origs[Local.ColName]>
				<cfset nam = Local.ColName & "Changed">
				<cfset "Variables.#nam#" = true><cfoutput>Variables.#nam#</cfoutput>
				<cfreturn true>
			</cfif>
		</cfloop>
	</cfif>
	<cfdump var="#SomethingChanged(Variables.ColumnListIMUserTbl)#"><cfabort>

<cfif "No"><!--- "No" essentially comments out this example usage of SomethingChanged. --->
<cftransaction action="Begin">

	<cfif SomethingChanged(Variables.ColumnListIMUserTbl)>
		<!--- Save these IMUserTbl columns here. --->
	</cfif>

	<!--- Example of how to use SomethingChanged with an existing one-to-many table, such as user businesses in profile or user pages: --->
	<cfloop index="i" from="1" to="#Variables.RecordCountUserBus#"><!--- Rows --->
		<cfset Variables.ColumnListBusinessesIndexed		= "">
		<cfloop index="Variables.Key" list="#Variables.ColumnListBusinesses#"><!--- Columns --->
			<cfset Variables.IndexedKey						= "#Variables.Key#_#i#">
			<cfset Variables[Variables.Key]					= Variables[Variables.IndexedKey]><!--- For saving to the database if changed. --->
			<cfset Variables.ColumnListBusinessesIndexed	= ListAppend(Variables.ColumnListBusinessesIndexed, Variables.IndexedKey)>
		</cfloop>
		<!--- Now ColumnListBusinessesIndexed has the same column names as ColumnListBusinesses, but with "_#i#" suffixed. --->
		<cfif SomethingChanged(Variables.ColumnListBusinessesIndexed)>
			<!--- Columns have been loaded in Variables.Key loop, so just call update stored procedure. --->
		</cfif>
	</cfloop>

	<!--- Etc. --->

	<cfif Variables.TxnErr>
		<cftransaction action="Rollback" />
		<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
		<cflocation addtoken="No" url="#Variables.DisplayPage#?PrevErr=Y">
	<cfelse>
		<cftransaction action="Commit" />
		<cfset Variables.Commentary							= "Data saved to database. ">
	</cfif>

</cftransaction>
</cfif><!--- /"No" --->

<cfset Variables.ErrMsg										= "">
<cfinclude template="#Variables.LibIncURL#/put_sbalookandfeel_savemessages.cfm">
<cflocation addtoken="No" url="#Variables.SuccessPage#?Commentary=#URLEncodedFormat(Variables.Commentary)#"><!--- Wherever caller told us to go. --->

<!---
AUTHOR:				Dileep Chanda
DATE:				05/12/2006
DESCRIPTION:		Form data for User roles. 
NOTES:

	Made cfincludable as this code is used in /cls and /security. Should always be cfincluded AFTER get_userroles.cfm. 
	Previously generated table rows with 4 columns, with the caller defining the table tag and setting TotalColumns to 4: 
	(1)		System Folder
	(2)		Role Checkbox
	(3)		Role Name
	(4)		Role Pending Approval message
	(3-4)	Privilege Checkbox and Name
	(3-4)	Required Data and nested table(s)
	Now it's more free-form without tables. Elements are kept side-by-side using inlineblock and consistent spacing. 

INPUT:				User info, getRoles, getPrivileges, getBusinesses, ArrayRoleOfcCds, ArrayRoleLocIds, ArrayRoleBusInfo. 
OUTPUT:				Form data for User roles. 
REVISION HISTORY:	10/17/2011, SRS:	Section 508 improvements. Cannibalized /cls/cfincludes version of this file, with 
										new version residing in /library/cfincludes/userprofile, because it's shared across 
										/cls and /security. Hence, it belongs in Library. (See directory's ReadMe.html.) 
					10/13/2011, AJC:	Added support for new role restriction 'M'. 
					10/04/2011, AJC:	Added support for new role restriction 'S'. 
					02/03/2010, SRS:	Added an onclick handler to the system name, so that clicking the name calls the 
										same JavaScript as clicking on the system's folder icon. (Section 508 issue.) 
					01/08/2010, SRS:	Changes to support notifying the SBA supervisor when an SBA employee or contractor 
										requests a role, so that the supervisor has an opportunity to approve or decline 
										the role request. Simplified code considerably using jQuery. 
					12/27/2007, DKC:	Added check to see if there is an aged-out request for the same user/role 
										and display a warning message.
					10/17/2007, DKC:	Added support for the new "partner" type user.
					10/26/2006, DKC:	Added new role restriction 'T', for hubzone application.
					07/18/2006, DKC:	Modified the code to allow only one role per system per person if the 
										indicator IMAllowMultiRoleInd IS "N".
					05/12/2006, DKC:	Original Implementation.
--->

<!--- Configuration Parameters: --->

<cfparam name="Variables.LibIncURL"								default="/library/cfincludes">
<cfset Variables.FDURBase										= "#Variables.LibIncURL#/userprofile/dsp_formdata_userroles">
<cfset Variables.FDURSelfName									= "#Variables.FDURBase#.cfm">
<cfset Variables.FDURStaticCSS									= "#Variables.FDURBase#.css?CachedAsOf=2011-10-26T00:46">
<cfset Variables.FDURStaticScripts								= "#Variables.FDURBase#.js?CachedAsOf=2011-10-26T01:13">
<cfset Variables.NbrCellsForOffices								= 3>
<cfset Variables.NbrCellsForLocIds								= 3>
<cfset Variables.SortGetRoles									= "Yes"><!--- Will remove old code when stable. --->
<cfset Variables.SpacingRqrdData								= RepeatString("&nbsp;", 10)>
<cfset Variables.TblAndTblRowExtraClass							= ""><!--- " inlineblock" messes up in MSIE. --->

<!--- Think of getRoles2 as "getRoles, alphabetized": --->

<cfset Variables.UserIsRequestingOwnAccess						= (Left(CGI.Script_Name, 5) IS "/cls/")>
<cfif Variables.UserIsRequestingOwnAccess>
	<cfset Variables.UsersYour									= "Your">
<cfelse>
	<cfset Variables.UsersYour									= "User's">
</cfif>
<cfif Variables.SortGetRoles>
	<!--- Unfortunately, we have to do a query-of-queries to reorder the output of get_userroles (security.UserRoleSelCSP). --->
	<cfquery name="getRoles2" dbtype="query">
	select		*
	from		getRoles
	order by	IMAppSysTypDescTxt,
				IMAppRoleDescTxt
	</cfquery>
<cfelse>
	<cfset getRoles2											= getRoles><!--- Shallow copy, very fast. --->
</cfif>

<!--- Accumulate all info that needs to be aggregated at the system level into only one extra pass through getRoles2: --->

<cfset Variables.IMAppSysTypCd									= "">
<cfset Variables.ListRolesWithRqrdData							= "">
<cfloop query="getRoles2">
	<cfif getRoles2.IMAppSysTypCd IS NOT Variables.IMAppSysTypCd>
		<cfset Variables.IMAppSysTypCd							= getRoles2.IMAppSysTypCd>
		<cfset Variables.SysStruct								= StructNew()>
		<cfset "Variables.SysStruct#Variables.IMAppSysTypCd#"	= Variables.SysStruct>
		<cfset Variables.SysStruct.FirstRqstdRole				= "">
		<cfset Variables.SysStruct.OpenFolder					= "No">
		<cfset Variables.SysStruct.RoleCount					= 0>
		<cfset Variables.SysStruct.UserHasRoles					= "No">
	</cfif>
	<cfset Variables.SysStruct.RoleCount						= Variables.SysStruct.RoleCount + 1>
	<cfif	(getRoles2.IMUserRoleExistInd						IS "Y")>
		<cfset Variables.SysStruct.OpenFolder					= "Yes">
		<cfset Variables.SysStruct.UserHasRoles					= "Yes">
	</cfif>
	<cfif	(getRoles2.IMUserRoleRqstInd						IS "Y")>
		<cfset Variables.SysStruct.OpenFolder					= "Yes">
		<cfif Len(Variables.SysStruct.FirstRqstdRole) IS 0>
			<cfset Variables.SysStruct.FirstRqstdRole			= getRoles2.IMAppRoleId>
		</cfif>
	</cfif>
	<cfif	(Len(getRoles2.IMRqrdDataTypCd)						GT 0)>
		<cfif	(ListFind(Variables.ListRolesWithRqrdData,		getRoles2.IMAppRoleId) IS 0)>
			<cfset Variables.ListRolesWithRqrdData				= ListAppend(Variables.ListRolesWithRqrdData,
																getRoles2.IMAppRoleId)>
		</cfif>
	</cfif>
</cfloop><!--- /getRoles2 --->

<!--- Accumulate all info that needs to be aggregated at the role level into only one extra pass through getPrivileges: --->

<cfset Variables.ListRolesWithPrivs								= "">
<cfset Variables.ListRolesWithPrivsChecked						= "">
<cfset Variables.ListRolesWithPrivsOrRqrdData					= Variables.ListRolesWithRqrdData>
<cfloop query="getPrivileges">
	<cfif	(ListFind(Variables.ListRolesWithPrivs,				getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivs						= ListAppend(Variables.ListRolesWithPrivs,
																getPrivileges.IMAppRoleId)>
	</cfif>
	<!--- Test the less-likely condition first: --->
	<cfif	(getPrivileges.IMUserRolePrvlgExistInd				IS "Y")
		AND	(ListFind(Variables.ListRolesWithPrivsChecked,		getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivsChecked				= ListAppend(Variables.ListRolesWithPrivsChecked,
																getPrivileges.IMAppRoleId)>
	</cfif>
	<cfif	(ListFind(Variables.ListRolesWithPrivsOrRqrdData,	getPrivileges.IMAppRoleId) IS 0)>
		<cfset Variables.ListRolesWithPrivsOrRqrdData			= ListAppend(Variables.ListRolesWithPrivsOrRqrdData,
																getPrivileges.IMAppRoleId)>
	</cfif>
</cfloop><!--- /getPrivileges --->

<!--- If the user is an SBA employee, retrieve the user's manager name: --->

<cfinclude template="/library/cfincludes/userprofile/dsp_formdata_ldap.cfm">

<!--- Build the display: --->

<cfset Variables.HiGrpIdx										= 0>
<cfset Variables.IMAppRoleDescTxt								= "">
<cfset Variables.IMAppSysTypCd									= "">
<cfset Variables.IMAppSysTypDescTxt								= "">
<cfset Variables.SystemContentsDivIsOpen						= "No">

<cfoutput>
<link href="#Variables.FDURStaticCSS#" rel="stylesheet" type="text/css" media="all" />
<script src="#Variables.FDURStaticScripts#"></script>
<input type="Hidden" name= "IMUserId"               value="#Variables.IMUserId#">
<input type="Hidden" name= "IMUserTypCd"            value="#Variables.IMUserTypCd#">
<input type="Hidden" name= "IMUserFirstNm"          value="#Variables.IMUserFirstNm#">
<input type="Hidden" name= "IMUserLastNm"           value="#Variables.IMUserLastNm#">
<input type="Hidden" name= "IMUserPrtNm"            value="#Variables.IMUserPrtNm#">
<input type="Hidden" name= "IMPOUserId"             value="#Variables.IMPOUserId#">
<input type="Hidden" name= "Identifier"             value="#Variables.Identifier#"></cfoutput>

<!--- Security System has already displayed its own header --->

<cfloop query="getRoles2">
	<cfif	Variables.UserIsRequestingOwnAccess
		and	(getRoles2.IMAppRoleAccsRqstAllowInd				is not "Y")>
		<cfcontinue>
	</cfif>
	<cfif getRoles2.IMAppSysTypCd IS NOT Variables.IMAppSysTypCd>
		<!--- Within this loop, we use a gls_system_folder_contents div to hold all roles associated with a system: --->
		<cfif Variables.SystemContentsDivIsOpen>
			<cfoutput>
</div><!-- /class="gls_system_folder_contents" --></cfoutput>
		</cfif>
		<cfset Variables.DispSysContents						= ' style="display:none;"'>
		<cfset Variables.SystemContentsDivIsOpen				= "No">
		<cfset Variables.IMAppSysTypCd							= getRoles2.IMAppSysTypCd>
		<cfset Variables.SysStruct								= Evaluate("Variables.SysStruct#Variables.IMAppSysTypCd#")>
		<cfif Variables.SysStruct.OpenFolder>
			<cfset Variables.DispSysContents					= "">
			<cfset Variables.DisplayRoles						= ""><!--- Error in Mozilla, but displays correctly. --->
			<cfset Variables.ImgURL								= "#Variables.ImagesLibURL#/sbatree/sbawfolderopen.gif">
		<cfelse>
			<cfset Variables.DisplayRoles						= "none">
			<cfset Variables.ImgURL								= "#Variables.ImagesLibURL#/sbatree/sbawfolderclosed.gif">
		</cfif>
		<cfset Variables.SystemComment							= "">
		<cfif	(getRoles2.IMAllowMultiRoleInd IS "N")
			AND	(Variables.SysStruct.RoleCount GT 1)>
			<cfset Variables.SystemComment						= "&nbsp;(<u><em>"
																& "You can select only one role for this system."
																& "</em></u>)">
		</cfif>
		<cfoutput>
<div class="gls_system">
	<div class="gls_system_box inlineblock" onclick="ToggleSystem('#getRoles2.IMAppSysTypCd#');">
		<img id="SystemFolder_#getRoles2.IMAppSysTypCd#" src="#Variables.ImgURL#" alt="Open/Close" width="24" height="22" border="0">
	</div>
	<span onclick="ToggleSystem('#getRoles2.IMAppSysTypCd#');">
		#getRoles2.IMAppSysTypDescTxt#
	</span><cfif Len(Variables.SystemComment) GT 0>
	#Variables.SystemComment#</cfif>
</div>
<div class="gls_system_contents" id="SystemContents_#getRoles2.IMAppSysTypCd#"#Variables.DispSysContents#></cfoutput>
		<cfset Variables.SystemContentsDivIsOpen				= "Yes">
	</cfif>
	<cfif Len(getRoles2.IMAppSysPOEmailAdrTxt) GT 0>
		<cfset Variables.IMAppSysPOEmailAdrTxt					= getRoles2.IMAppSysPOEmailAdrTxt>
	<cfelse>
		<cfset Variables.IMAppSysPOEmailAdrTxt					= "sheri.mcconville@sba.gov">
	</cfif>
	<cfset Variables.Checked									= "       ">
	<cfset Variables.Disabled									= "        ">
	<cfset Variables.DispRoleContents							= ' style="display:none;"'>
	<cfset Variables.DispRolePrivs								= ' style="display:none;"'>
	<cfif ListFind(Variables.ListRolesWithPrivsChecked,			getRoles2.IMAppRoleId) GT 0>
		<cfset Variables.DispRoleContents						= "">
		<cfset Variables.DispRolePrivs							= "">
	</cfif>
	<cfif getRoles2.IMUserRoleExistInd IS "Y">
		<cfset Variables.Checked								= "checked">
		<cfset Variables.DispRoleContents						= "">
	</cfif>
	<cfif Len(getRoles2.IMRqrdDataTypCd) GT 0>
		<cfset Variables.IMRqrdDataTypCd						= getRoles2.IMRqrdDataTypCd>
		<!---
		Per Ron Whalen on 2010-01-15, if a role has required data, show its required data form elements, even if that 
		role has not been requested yet. Of course, in that case, the required data form elements will be empty: 
		--->
		<cfset Variables.DispRoleContents						= "">
	<cfelse>
		<!--- 'N' stands for null. We need some value in here other than empty string as we do ListtoArray in the act page --->
		<cfset Variables.IMRqrdDataTypCd						= "N">
	</cfif>
	<cfif getRoles2.IMUserRoleRqstInd IS "Y">
		<cfset Variables.Disabled								= "disabled">
	</cfif>
	<!---
	In ToggleRole, we change the column names as they appear to the user, so as not to reveal too much about our 
	database names. To make the code more readable, use the same names here, even though it's server-side: 
	--->
	<cfif	Variables.UserIsRequestingOwnAccess
		AND	(getRoles2.IMAppRoleSBAAuthRqrdInd					IS "Y")>
		<cfset Variables.ShowAuthMsg							= "true">
	<cfelse>
		<cfset Variables.ShowAuthMsg							= "false">
	</cfif>
<cfset Variables.CallUncheckAll								= "Yes"> 
<!---	<cfset Variables.CallUncheckAll								= "NO"> --->
	<cfset Variables.MaySeeThisRole								= "No">
	<cfset Variables.PrevRequests								= 0>
	<cfset Variables.ShowHideHotlink							= "">
	<cfif getRoles2.IMAllowMultiRoleInd IS "N">
		<cfif	(Len(Variables.SysStruct.FirstRqstdRole) IS 0)
			OR	(Variables.SysStruct.FirstRqstdRole IS getRoles2.IMAppRoleId)>
			<cfset Variables.CallUncheckAll						= "Yes">
			<cfset Variables.MaySeeThisRole						= "Yes">
			<!--- For some reason, only single-role systems get the previous request count message: --->
			<cfset Variables.PrevRequests						= getRoles2.UserRoleRqstCount>
		</cfif>
	<cfelse>
		<cfset Variables.MaySeeThisRole							= "Yes">
	</cfif>
	<cfif ListFind(Variables.ListRolesWithPrivsOrRqrdData, getRoles2.IMAppRoleId) GT 0>
		<cfset Variables.ShowHideHotlink						= " (<a href=""javascript:ToggleRoleContents"
																& "(#getRoles2.IMAppRoleId#);"">Show/Hide</a>)">
	</cfif>
	<cfif Variables.MaySeeThisRole>
		<!--- Role Checkbox: --->
		<cfoutput>
	<div class="gls_role">
		<input type="Hidden" name="IMAppRoleId"             value="#getRoles2.IMAppRoleId#">
		<input type="Hidden" name="IMAppRoleDescTxt"        value="#Replace(getRoles2.IMAppRoleDescTxt,		',','|','ALL')#">
		<input type="Hidden" name="IMAppSysTypDescTxt"      value="#Replace(getRoles2.IMAppSysTypDescTxt,	',','|','ALL')#">
		<input type="Hidden" name="IMAppRoleSBAAuthRqrdInd" value="#getRoles2.IMAppRoleSBAAuthRqrdInd#">
		<input type="Hidden" name="IMRqrdDataTypCd"         value="#Variables.IMRqrdDataTypCd#">
		<input type="Hidden" name="IMAppSysPOEmailAdrTxt"   value="#Variables.IMAppSysPOEmailAdrTxt#">
		<input type="Hidden" name="RoleExistInd_#getRoles2.IMAppRoleId#" value="#getRoles2.IMUserRoleExistInd#">
		<input type="Hidden" name="OldRoleRqst_#getRoles2.IMAppRoleId#"  value="#getRoles2.IMUserRoleRqstInd#">
		<div class="gls_role_box inlineblock">
			<input type="Checkbox" name="RoleRqst_#getRoles2.IMAppRoleId#" id="RoleRqst_#getRoles2.IMAppRoleId#"
			data-rolenumber="#getRoles2.IMAppRoleId#" data-sysnumber="#getRoles2.IMAppSysTypCd#" #Variables.Checked# #Variables.Disabled#
			title="#getRoles2.IMAppRoleDescTxt#" value="Y" onclick="<cfif Variables.CallUncheckAll>
			var	sChecked				= this.checked
			UncheckAll(#getRoles2.IMAppSysTypCd#);
			this.checked				= sChecked;</cfif>
			ToggleRole(this, #Variables.PrevRequests#, #Variables.ShowAuthMsg#, #getRoles2.IMAppRoleId#);
			">
		</div>
		<label for="RoleRqst_#getRoles2.IMAppRoleId#" class="optlabel">
			#getRoles2.IMAppRoleDescTxt#
		</label>
		#Variables.ShowHideHotlink#<cfif getRoles2.IMUserRoleRqstInd IS "Y">
		(Request for this role is pending approval. 
		<a target="_blank" href="/cls/dsp_pending_approvals.alias.cfm">Show all</a>
		in new window.)</cfif>
	</div>
	<div class="gls_role_contents" id="RoleContents_#getRoles2.IMAppRoleId#"#Variables.DispRoleContents#></cfoutput>
		<cfset Variables.IMAppRoleId							= getRoles2.IMAppRoleId>
		<cfif ListFind(Variables.ListRolesWithPrivs, getRoles2.IMAppRoleId) GT 0>
			<cfoutput>
		<div class="gls_role_privs" id="RolePrivs_#getRoles2.IMAppRoleId#"#Variables.DispRolePrivs#></cfoutput>
			<cfset Variables.FirstPriv							= "Yes">
			<cfloop query="getPrivileges">
				<cfif getPrivileges.IMAppRoleId IS Variables.IMAppRoleId>
					<cfif getPrivileges.IMUserRolePrvlgExistInd IS "Y">
						<cfset Variables.Checked				= "checked">
					<cfelse>
						<cfset Variables.Checked				= "       ">
					</cfif>
					<cfoutput><cfif NOT Variables.FirstPriv><br/></cfif>
			<input type="Hidden"   name="IMSysPrvlgCd_#getPrivileges.IMAppRoleId#" value="#getPrivileges.IMSysPrvlgCd#">
			<input type="Hidden"   name="PrvlgExistInd_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#" value="#getPrivileges.IMUserRolePrvlgExistInd#">
			<label class="optlabel" for="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#">
				<input type="Checkbox" name="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#"
				id="PrvlgRqst_#getPrivileges.IMAppRoleId#_#getPrivileges.IMSysPrvlgCd#" #Variables.Checked# value="Y">
				#getPrivileges.IMSysPrvlgDescTxt#
			</label></cfoutput>
					<cfset Variables.FirstPriv					= "No">
				</cfif>
			</cfloop><!--- /getPrivileges --->
			<cfoutput>
		</div><!-- /cls_role_privs --></cfoutput>
		</cfif>
		<cfswitch expression="#getRoles2.IMRqrdDataTypCd#"><!--- "RqrdData" (at most, only one of these): --->
		<cfcase value="O">
			<cfset RowIdx										= 0>
			<cfoutput>
		<fieldset class="gls_role_reqd inlineblock">
			<legend>Office Code</legend>
			<div class="tbl#Variables.TblAndTblRowExtraClass#"></cfoutput>
			<cfloop index="RoleOfcIdx" from="1" to="#ArrayLen(Variables.ArrayRoleOfcCds)#">
				<cfif Variables.ArrayRoleOfcCds[RoleOfcIdx][1] EQ getRoles2.IMAppRoleId>
					<cfset RowIdx								= RowIdx + 1>
					<cfoutput><cfif (Variables.RowIdx Mod Variables.NbrCellsForOffices) IS 1>
				<div class="tbl_row#Variables.TblAndTblRowExtraClass#">
					<div class="tbl_cell nowrap"><cfelse>
					<div class="tbl_cell nowrap">
						&nbsp;&nbsp;&nbsp;</cfif><cfset Variables.HiGrpIdx = Variables.HiGrpIdx + 1>
						<div class="disableddata hibox grp#Variables.HiGrpIdx#">
						<input type="Text" name="RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#" id="RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#"
						class="hielt grp#Variables.HiGrpIdx#" disabled value="#Variables.ArrayRoleOfcCds[RoleOfcIdx][2]#" 
						size="4" maxlength="4" onchange="
						if	(EditMask('SBA Office Code', this.value, '9', 1, 4, 4))
							return true;	// Do not redisable, or else the value won't be submitted with the form. 
						this.focus();
						return false;
						">
						</div><!-- /disableddata -->
						<label for="RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#">
							Office Code
							<input type="Hidden" name="OldRoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#" value="#Variables.ArrayRoleOfcCds[RoleOfcIdx][2]#">
							<input type="Hidden" name="OfcRowIdx_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#" value="#RowIdx#">
						</label>
						&nbsp;
						<a href="javascript:HiGrpEnable('.grp#Variables.HiGrpIdx#');HiGrpMandatory('.grp#Variables.HiGrpIdx#');"
						><img src="#Variables.LibURL#/images/edit_icon.gif" width="20" height="20" border="0"
						alt="Edit Office Code" title="Edit Office Code"></a>
					 	&nbsp;Del:
						<input type="Checkbox" name="RoleOfficeDEL_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#"
						id="RoleOfficeDEL_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#">  
						&nbsp;
						<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_ofccd.cfm?FField=RoleOffice_#Variables.ArrayRoleOfcCds[RoleOfcIdx][1]#_#RowIdx#"
						>Lookup</a>
					</div><!-- /tbl_cell nowrap --><cfif (Variables.RowIdx Mod Variables.NbrCellsForOffices) IS 0>
				</div><!-- /tbl_row --></cfif></cfoutput>
				</cfif>
			</cfloop><!--- ArrayRoleOfcCds --->
			<cfif getRoles2.IMAllowMultiOfcInd IS "Y" OR RowIdx IS 0>
				<cfset Variables.NoOfEmptyOfcsHold	= Variables.NoOfEmptyOfcs>
				<cfif getRoles2.IMAllowMultiOfcInd IS "Y">
					<cfset Variables.NoOfEmptyOfcs				= IIf(	Variables.RowIdx LT Variables.NoOfEmptyOfcsHold, 
																		Variables.NoOfEmptyOfcsHold - Variables.RowIdx,
																		3)>
				<cfelse>
					<cfset Variables.NoOfEmptyOfcs				= 1>
				</cfif>
				<cfloop index="EmptyOfcs" from="1" to="#Variables.NoOfEmptyOfcs#">
					<cfset RowIdx								= RowIdx + 1>
					<cfoutput><cfif (Variables.RowIdx Mod Variables.NbrCellsForOffices) IS 1>
				<div class="tbl_row#Variables.TblAndTblRowExtraClass#">
					<div class="tbl_cell nowrap"><cfelse>
					<div class="tbl_cell nowrap">
						&nbsp;&nbsp;&nbsp;</cfif>
						<div class="manddata">
						<input type="Text" name="RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#" id="RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#"
						size="4" maxlength="4" onchange="
						if	(EditMask('SBA Office Code', this.value, '9', 1, 4, 4))
							return true;
						this.focus();
						return false;
						">
						</div><!-- /manddata -->
						<label for="RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#">
							Office Code
							<input type="Hidden" name="OfcRowIdx_#getRoles2.IMAppRoleId#" value="#RowIdx#">
						</label>
						&nbsp;
						<img src="#Variables.LibURL#/images/add_icon.gif" width="20" height="20" border="0"
						alt="Add Office Code" title="Add Office Code">
						&nbsp;Del:
						<input type="Checkbox" name="RoleOfficeDEL_#getRoles2.IMAppRoleId#_#RowIdx#"
						id="RoleOfficeDEL_#getRoles2.IMAppRoleId#_#RowIdx#">
						&nbsp;
						<a target="_blank" href="#GLSIncludesURL#/dsp_lookup_ofccd.cfm?FField=RoleOffice_#getRoles2.IMAppRoleId#_#RowIdx#"
						>Lookup</a>
					</div><!-- /tbl_cell nowrap --><cfif (Variables.RowIdx Mod Variables.NbrCellsForOffices) IS 0>
				</div><!-- /tbl_row --></cfif></cfoutput>
				</cfloop><!--- EmptyOfcs --->
				<cfset Variables.NoOfEmptyOfcs					= Variables.NoOfEmptyOfcsHold>
			</cfif>
			<cfoutput>
			</div><!-- /tbl -->
		</fieldset></cfoutput>
		</cfcase>
		<cfcase value="L,M">
			<cfset RowIdx										= 0><!--- Row in ArrayRoleLocIds (becomes tbl_cell). --->
			<cfoutput>
		<fieldset class="gls_role_reqd inlineblock">
			<legend>Location Id</legend>
			<div class="tbl#Variables.TblAndTblRowExtraClass#"></cfoutput>
			<cfloop index="RoleLocIdx" from="1" to="#ArrayLen(Variables.ArrayRoleLocIds)#">
				<cfif Variables.ArrayRoleLocIds[RoleLocIdx][1] EQ getRoles2.IMAppRoleId>
					<cfset RowIdx								= RowIdx + 1>
					<cfoutput><cfif (Variables.RowIdx Mod Variables.NbrCellsForLocIds) IS 1>
				<div class="tbl_row#Variables.TblAndTblRowExtraClass#">
					<div class="tbl_cell nowrap"><cfelse>
					<div class="tbl_cell nowrap">
						&nbsp;&nbsp;&nbsp;</cfif><cfset Variables.HiGrpIdx = Variables.HiGrpIdx + 1>
						<div class="disableddata hibox grp#Variables.HiGrpIdx#">
						<input type="Text" name="RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
						id="RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
						class="hielt grp#Variables.HiGrpIdx#" disabled value="#Variables.ArrayRoleLocIds[RoleLocIdx][2]#"
						size="7" maxlength="7" onchange="
						if	(EditMask('Location Id', this.value, '9', 1, 1, 7))
							return true;	// Do not redisable, or else the value won't be submitted with the form. 
						this.focus();
						return false;
						"><!-- /disableddata -->
						<label for="RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#">Location Id</label>
						<input type="Hidden" name="OldRoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#" value="#Variables.ArrayRoleLocIds[RoleLocIdx][2]#">
						<input type="Hidden" name="LocRowIdx_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#" value="#RowIdx#">
						</div>
						&nbsp;
						<a href="javascript:HiGrpEnable('.grp#Variables.HiGrpIdx#'); HiGrpMandatory('.grp#Variables.HiGrpIdx#');"
						><img src="#Variables.LibURL#/images/edit_icon.gif" width="20" height="20" border="0"
						alt="Edit Location Id" title="Edit Location Id"></a>
						&nbsp;Del:
						<input type="Checkbox" name="RoleLocationDEL_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
						id="RoleLocationDEL_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#">
						&nbsp;
						<a target="_blank" href="#Variables.CLSIncludesURL#/dsp_lookup_lend.cfm?FField=RoleLocation_#Variables.ArrayRoleLocIds[RoleLocIdx][1]#_#RowIdx#"
						>Lookup</a>
					</div><!-- /tbl_cell --><cfif (Variables.RowIdx Mod Variables.NbrCellsForLocIds) IS 0>
				</div><!-- /tbl_row --></cfif></cfoutput>
				</cfif>
			</cfloop><!--- /ArrayRoleLocIds --->
			<cfif getRoles2.IMAllowMultiLocInd IS "Y" OR Variables.RowIdx IS 0>
				<cfset Variables.NoOfEmptyLocsHold				= Variables.NoOfEmptyLocs>
				<cfif getRoles2.IMAllowMultiLocInd IS "Y">
					<cfset Variables.NoOfEmptyLocs				= IIf(	Variables.RowIdx LT Variables.NoOfEmptyLocsHold, 
																		Variables.NoOfEmptyLocsHold - Variables.RowIdx,
																		3)>
				<cfelse>
					<cfset Variables.NoOfEmptyLocs				= 1>
				</cfif>
				<cfloop index="EmptyLocs" from="1" to="#Variables.NoOfEmptyLocs#">
					<cfset RowIdx		= RowIdx + 1>
					<cfoutput><cfif (Variables.RowIdx Mod Variables.NbrCellsForLocIds) IS 1>
				<div class="tbl_row#Variables.TblAndTblRowExtraClass#">
					<div class="tbl_cell nowrap"><cfelse>
					<div class="tbl_cell nowrap">
						&nbsp;&nbsp;&nbsp;</cfif>
						<div class="manddata">
						<input type="Text" name="RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#"
						id="RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#"
						size="7" maxlength="7" onchange="
						if	(EditMask('Location Id', this.value, '9', 1, 1, 7))
							return true;
						this.focus();
						return false;
						"><!-- /manddata -->
						<label for="RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#">Location Id</label>
						<input type="Hidden" name="LocRowIdx_#getRoles2.IMAppRoleId#" value="#RowIdx#">
						</div>
						&nbsp;
						<img src="#Variables.LibURL#/images/add_icon.gif" width="20" height="20" border="0"
						alt="Add Location Id">
						&nbsp;Del:
						<input type="Checkbox" name="RoleLocationDEL_#getRoles2.IMAppRoleId#_#RowIdx#"
						id="RoleLocationDEL_#getRoles2.IMAppRoleId#_#RowIdx#">
						&nbsp;
						<a target="_blank" href="#Variables.CLSIncludesURL#/dsp_lookup_lend.cfm?FField=RoleLocation_#getRoles2.IMAppRoleId#_#RowIdx#"
						>Lookup</a>
					</div><!-- /tbl_cell --><cfif (Variables.RowIdx Mod Variables.NbrCellsForLocIds) IS 0>
				</div><!-- /tbl_row --></cfif></cfoutput>
				</cfloop><!--- /EmptyLocs --->
				<cfset Variables.NoOfEmptyLocs					= Variables.NoOfEmptyLocsHold>
			</cfif>
			<cfoutput>
			</div><!-- /tbl -->
		</fieldset></cfoutput>
		</cfcase>
		<cfcase value="B,T">
			<cfset Variables.TmpIMUserBusSeqNmb					= "">
			<cfloop index="RoleBusIdx" from="1" to="#ArrayLen(Variables.ArrayRoleBusInfo)#">
				<cfif Variables.ArrayRoleBusInfo[RoleBusIdx][1] EQ getRoles2.IMAppRoleId>
					<cfoutput>
			<input type="Hidden" name="OldRoleBus_#getRoles2.IMAppRoleId#" value="#Variables.ArrayRoleBusInfo[RoleBusIdx][2]#"></cfoutput>
					<cfset Variables.TmpIMUserBusSeqNmb			= Variables.ArrayRoleBusInfo[RoleBusIdx][2]>
					<cfbreak>
				</cfif>
			</cfloop><!--- /ArrayRoleBusInfo --->
			<cfoutput>
		<fieldset class="gls_role_reqd inlineblock">
			<legend>Business In #Variables.UsersYour# Profile</legend>
			<div class="manddata">
			<select name="RoleBus_#getRoles2.IMAppRoleId#" id="RoleBus_#getRoles2.IMAppRoleId#"></cfoutput>
			<cfif getBusinesses.RecordCount>
				<cfoutput>
			<option value="">Select One</option></cfoutput>
				<cfset Variables.ListBusSeqNmbs					= ""><!--- Prevent duplicates (due to DUNS). --->
				<cfloop query="getBusinesses">
					<cfif ListFind(Variables.ListBusSeqNmbs,	getBusinesses.IMUserBusSeqNmb) gt 0><!--- prevent dupes --->
						<cfcontinue>																<!--- prevent dupes --->
					</cfif>																			<!--- prevent dupes --->
					<cfset Variables.ListBusSeqNmbs				= ListAppend(Variables.ListBusSeqNmbs,
																getBusinesses.IMUserBusSeqNmb)>		<!--- prevent dupes --->
					<cfset Variables.TmpIMUserBusTaxId			= FormatTaxIdForDisplay(getBusinesses.IMUserBusTaxId,
																						getBusinesses.IMUserBusEINSSNInd)>
					<cfif Variables.TmpIMUserBusSeqNmb EQ getBusinesses.IMUserBusSeqNmb>
						<cfset Variables.Selected				= "selected">
					<cfelse>
						<cfset Variables.Selected				= "        ">
					</cfif>
					<cfoutput>
			<option #Variables.Selected# value="#getBusinesses.IMUserBusSeqNmb#">#Variables.TmpIMUserBusTaxId#</option></cfoutput>
				</cfloop><!--- /getBusinesses --->
			<cfelse>
				<cfoutput>
			<option value="">No businesses found in your profile.</option></cfoutput>
			</cfif>
			<cfoutput>
			</select>
			</div><!-- /manddata --><!-- The following label has display:none. For use by screen readers only: -->
			<label for="RoleBus_#getRoles2.IMAppRoleId#">Choose a business in your profile.</label>
		</fieldset></cfoutput>
		</cfcase>
		<cfcase value="P,S">
			<cfset Variables.TmpIMUserBusSeqNmb					= "">
			<cfloop index="RoleBusIdx" from="1" to="#ArrayLen(Variables.ArrayRoleBusInfo)#">
				<cfif Variables.ArrayRoleBusInfo[RoleBusIdx][1] EQ getRoles2.IMAppRoleId>
					<cfoutput>
		<input type="Hidden" name="OldRoleBus_#getRoles2.IMAppRoleId#" value="#Variables.ArrayRoleBusInfo[RoleBusIdx][2]#"></cfoutput>
					<cfset Variables.TmpIMUserBusSeqNmb			= Variables.ArrayRoleBusInfo[RoleBusIdx][2]>
					<cfbreak>
				</cfif>
			</cfloop><!--- /ArrayRoleBusInfo --->
			<cfoutput>
		<fieldset class="gls_role_reqd inlineblock">
			<legend>Business with DUNS In #Variables.UsersYour# Profile</legend>
			<div class="manddata">
			<select name="RoleBus_#getRoles2.IMAppRoleId#" id="RoleBus_#getRoles2.IMAppRoleId#"></cfoutput>
			<cfif getBusinesses.RecordCount>
				<cfoutput>
			<option value="">Select One</option></cfoutput>
				<!--- First, produce ListBusSeqNmbDupes (BusSeqNmbs that have duplicates due to DUNS): --->
				<cfset Variables.ListBusSeqNmbs					= ""><!--- Prevent duplicates (due to DUNS). --->
				<cfset Variables.ListBusSeqNmbDupes				= ""><!--- Prevent duplicates (due to DUNS). --->
				<cfloop query="getBusinesses">
					<cfif Len(getBusinesses.IMUserBusLocDUNSNmb) is 0>
						<cfcontinue>
					</cfif>
					<cfif ListFind(Variables.ListBusSeqNmbs,	getBusinesses.IMUserBusSeqNmb) is 0><!--- prevent dupes --->
						<cfset Variables.ListBusSeqNmbs			= ListAppend(Variables.ListBusSeqNmbs,
																getBusinesses.IMUserBusSeqNmb)>		<!--- prevent dupes --->
						<cfcontinue>																<!--- prevent dupes --->
					</cfif>																			<!--- prevent dupes --->
					<cfif ListFind(Variables.ListBusSeqNmbDupes,getBusinesses.IMUserBusSeqNmb) gt 0><!--- prevent dupes --->
						<cfcontinue>																<!--- prevent dupes --->
					</cfif>																			<!--- prevent dupes --->
					<cfset Variables.ListBusSeqNmbDupes			= ListAppend(Variables.ListBusSeqNmbDupes,
																getBusinesses.IMUserBusSeqNmb)>		<!--- prevent dupes --->
				</cfloop><!--- /getBusinesses --->
				<!--- Next, use ListBusSeqNmbDupes to decide whether to display "multiple" or DUNS: --->
				<cfset Variables.ListBusSeqNmbs					= ""><!--- Prevent duplicates (due to DUNS). --->
				<cfloop query="getBusinesses">
					<cfif Len(getBusinesses.IMUserBusLocDUNSNmb) is 0>
						<cfcontinue>
					</cfif>
					<!--- This part is the same as the duplicate detection in cfcase value="B,T", above: --->
					<cfif ListFind(Variables.ListBusSeqNmbs,	getBusinesses.IMUserBusSeqNmb) gt 0><!--- prevent dupes --->
						<cfcontinue>																<!--- prevent dupes --->
					</cfif>																			<!--- prevent dupes --->
					<cfset Variables.ListBusSeqNmbs				= ListAppend(Variables.ListBusSeqNmbs,
																getBusinesses.IMUserBusSeqNmb)>		<!--- prevent dupes --->
					<cfset Variables.TmpIMUserBusTaxId			= FormatTaxIdForDisplay(getBusinesses.IMUserBusTaxId,
																						getBusinesses.IMUserBusEINSSNInd)>
					<cfif Variables.TmpIMUserBusSeqNmb EQ getBusinesses.IMUserBusSeqNmb>
						<cfset Variables.Selected				= "selected">
					<cfelse>
						<cfset Variables.Selected				= "        ">
					</cfif>
					<!--- And displaying DUNS is what makes cfcase value="P,S" different from cfcase value="B,T": --->
					<cfif ListFind(Variables.ListBusSeqNmbDupes,getBusinesses.IMUserBusSeqNmb) is 0>
						<cfset Variables.TmpIMUserBusLocDUNSNmb	= getBusinesses.IMUserBusLocDUNSNmb>
					<cfelse>
						<cfset Variables.TmpIMUserBusLocDUNSNmb	= "multiple">
					</cfif>
					<cfoutput>
			<option #Variables.Selected# value="#getBusinesses.IMUserBusSeqNmb#">#Variables.TmpIMUserBusTaxId# (DUNS - #Variables.TmpIMUserBusLocDUNSNmb#)</option></cfoutput>
				</cfloop><!--- /getBusinesses --->
			<cfelse>
				<cfoutput>
			<option value="">No businesses with DUNS found in your profile.</option></cfoutput>
			</cfif>
			<cfoutput>
			</select>
			</div><!-- /manddata --><!-- The following label has display:none. For use by screen readers only: -->
			<label for="RoleBus_#getRoles2.IMAppRoleId#">Choose a business with DUNS in your profile.</label>
		</fieldset></cfoutput>
		</cfcase>
		</cfswitch>
		<cfoutput>
	</div><!-- /cls_role_contents --></cfoutput>
	</cfif><!--- /MaySeeThisRole --->
</cfloop><!--- /getRoles2 --->
<cfif Variables.SystemContentsDivIsOpen>
	<cfoutput>
</div><!-- /cls_system_contents" --></cfoutput>
</cfif>

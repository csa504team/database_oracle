<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/30/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				Called by dsp_userprofile.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	Appended to the end of AppDataInline and JSInline. 
REVISION HISTORY:	07/30/2015, SRS:	Original implementation. Previously was hardcoded in calling pages (not an include). 
										Made tabular data into a table for Section 508 reasons. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#</cfoutput>
	<cfparam name="Variables.MandOptBusAdd"					default="opt">
	<cfparam name="Variables.MandOptBusDel"					default="opt">
	<cfif	(Variables.Origs.RecordCountUserBus				gt 0)
		or	(ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0)>
		<cfoutput>
<fieldset class="inlineblock">
  <legend>Businesses Associated with Account</legend>
  <div class="tbl"></cfoutput>
	<cfif Variables.Origs.RecordCountUserBus gt 0>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="viewlabel">Business Information</span></div>
      <div class="tbl_cell formdata nowrap">
        <table border="1" cellpadding="2" summary="Business Information">
        <thead>
          <tr>
            <th>##</th>
            <th><span title="Tax ID type ('EIN' or 'SSN')">E/S</span></th>
            <th>Tax ID</th>
            <th>DUNS</th><cfif ListFindNoCase("skip,view", Variables.MandOptBusDel) is 0>
            <th>Maintenance</th></cfif>
          </tr>
        </thead>
        <tbody></cfoutput>
		<cfif NOT IsDefined("AlternatingRowClassName")>
			<cfinclude template="#Variables.LibUdfURL#/bld_SearchUDFs.cfm">
		</cfif>
		<cfloop index="Idx" from="1" to="#Variables.Origs.RecordCountUserBus#">
			<cfset Variables.RemoveTitle					= "Remove the business on this row with ">
			<cfif Len(Variables.Origs["DispTaxId_"			& Idx]) gt 0>
				<cfset Variables.RemoveTitle				&= "Tax ID #Variables.Origs['DispTaxId_'			& Idx]#">
				<cfif Len(Variables.Origs["IMUserBusLocDUNSNmb_" & Idx]) gt 0>
					<cfset Variables.RemoveTitle			&= " and ">
				</cfif>
			</cfif>
			<cfif Len(Variables.Origs["IMUserBusLocDUNSNmb_"& Idx]) gt 0>
				<cfset Variables.RemoveTitle				&= "DUNS #Variables.Origs['IMUserBusLocDUNSNmb_'	& Idx]#">
			</cfif>
			<cfset Variables.RemoveTitle					&= ".">
			<cfoutput>
          <tr class="#AlternatingRowClassName(Idx)#">
            <td align="right">#Idx#</td>
            <td>#Variables.Origs["DispEINSSN_"			& Idx]#</td>
            <td>#Variables.Origs["DispTaxId_"			& Idx]#</td>
            <td>#Variables.Origs["IMUserBusLocDUNSNmb_"	& Idx]#</td><cfif ListFindNoCase("skip,view", Variables.MandOptBusDel) is 0>
            <td>
              <label class="#Variables.MandOptBusDel#label" title="#Variables.RemoveTitle#">
                <input type="Checkbox" name="BusDel" value="#Variables.Origs['IMUserBusSeqNmb_' & Idx]#">
                Remove <cfif Len(Variables.Origs["IMUserBusLocDUNSNmb_" & Idx]) gt 0>DUNS<cfelse>Business</cfif>
              </label>
            </td></cfif>
          </tr></cfoutput>
		</cfloop>
		<cfoutput>
        </tbody>
        </table>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /Variables.Origs.RecordCountUserBus --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>
		<cfparam name="Variables.TotalNewBus"				default="0">
		<cfset Variables.TotalNewBusPlus1					= Variables.TotalNewBus + 1>
		<cfoutput>
    <input type="Hidden" name="TotalNewBus" value="#Variables.TotalNewBus#">
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Button" value="Add New Business" onclick="
        var sIdx											= parseInt(this.form.TotalNewBus.value, 10) + 1;
        var sFieldset										= document.createElement('fieldset');
        sFieldset.className									= 'inlineblock';
        sFieldset.innerHTML									= gNewBusTemplate.replace(/\^0/g, ''+sIdx);
        gDivNewFieldsets.appendChild(sFieldset);
        gDivNewFieldsets.appendChild(document.createElement('br'));
        this.form.TotalNewBus.value							= sIdx;
        document.getElementById('NewBusAdded').innerHTML	= '<span class=\'title1\'>New Business ##' + sIdx + '</span> added, below.';
        "><!-- Without the following feedback, new business could've been added out of scroll range. (No visual feedback for sighted users): -->
        <span id="NewBusAdded"></span>
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Businesses Currently Associated with Profile --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
	</cfif><!--- /Variables.Origs.RecordCountUserBus or MandOptBusAdd --->
	<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>
		<cfoutput>
<div id="NewFieldsets">
</cfoutput>
		<cfloop index="i" from="1" to="#Variables.TotalNewBusPlus1#">
			<cfset Variables.ThisCheckedEIN					= "       ">
			<cfset Variables.ThisCheckedSSN					= "       ">
			<cfset Variables.ThisTaxId						= "">
			<cfset Variables.ThisShowCert					= "none">
			<cfset Variables.ThisCert						= "       ">
			<cfset Variables.ThisDUNS						= "">
			<cfif i lt Variables.TotalNewBusPlus1>
				<cfif NOT IsDefined("Variables.NewBusWasNotDiscarded_#i#")>
					<!--- If not defined, New Business was discarded, so don't reimage it: --->
					<cfcontinue>
				</cfif>
				<cfif IsDefined("Variables.NewBusEINSSN_#i#")>
					<cfif Variables["NewBusEINSSN_#i#"]		is "E">
						<cfset Variables.ThisCheckedEIN		= "checked">
						<cfset Variables.ThisShowCert		= "">
						<cfif IsDefined("Variables.NewBusCertified_#i#")
							and	(Variables["NewBusCertified_#i#"] is "Yes")>
							<cfset Variables.ThisCert		= "checked">
						</cfif>
					<cfelseif Variables["NewBusEINSSN_#i#"]	is "S">
						<cfset Variables.ThisCheckedSSN		= "checked">
					</cfif>
				</cfif>
				<cfif IsDefined("Variables.NewBusTaxId_#i#")>
					<cfset Variables.ThisTaxId				= Variables["NewBusTaxId_#i#"]>
				</cfif>
				<cfif IsDefined("Variables.NewBusDUNS_#i#")>
					<cfset Variables.ThisDUNS				= Variables["NewBusDUNS_#i#"]>
				</cfif>
				<cfset ii									= i>
				<cfoutput>
<fieldset class="inlineblock"></cfoutput>
			<cfelse>
				<cfset ii									= "^0">
				<cfoutput>
<script type="text/sba_template" id="newbus_template" style="display:none;"></cfoutput>
			</cfif>
			<cfoutput>
  <legend>New Business ###ii#</legend>
  <input type="Hidden" name="NewBusWasNotDiscarded_#ii#" value="Track whether New Business was on the screen.">
  Enter Tax ID, DUNS or both. 
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel #Variables.MandOptBusAdd#label">EIN/SSN</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptBusAdd#data">
          <fieldset class="radio"><legend class="radio">Tax ID Type</legend>
          <label><input type="Radio" name="NewBusEINSSN_#ii#" id="NewBusEINSSN_#ii#_EIN" value="E" #Variables.ThisCheckedEIN# onclick="
          document.getElementById('ShowNewBusCertified_#ii#').style.display = '';
          this.form.NewBusCertified_#ii#.checked = false;
          this.form.NewBusTaxId_#ii#.placeholder = '99-9999999';
          if  (this.form.NewBusTaxId_#ii#.value.length > 0)
              this.form.NewBusTaxId_#ii#.onchange();
          "> EIN </label>
          <label><input type="Radio" name="NewBusEINSSN_#ii#" id="NewBusEINSSN_#ii#_SSN" value="S" #Variables.ThisCheckedSSN# onclick="
          document.getElementById('ShowNewBusCertified_#ii#').style.display = 'none';
          this.form.NewBusCertified_#ii#.checked = false;
          this.form.NewBusTaxId_#ii#.placeholder = '999-99-9999';
          if  (this.form.NewBusTaxId_#ii#.value.length > 0)
              this.form.NewBusTaxId_#ii#.onchange();
          "> SSN </label>
          </fieldset>
        </div><!-- /#Variables.MandOptBusAdd#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="NewBusTaxId_#ii#" class="#Variables.MandOptBusAdd#label">Tax ID</label></div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptBusAdd#data">
        <input type="Text" name="NewBusTaxId_#ii#" id="NewBusTaxId_#ii#" value="#Variables.ThisTaxId#"
        size="11" maxlength="11" onchange="
        var sMand											= isMand(this);
        if  (EditTin('New Business ###ii# Tax ID', this.value, sMand, 
                     document.getElementById('NewBusEINSSN_#ii#_EIN').checked ? 'EIN' : 
                   ( document.getElementById('NewBusEINSSN_#ii#_SSN').checked ? 'SSN' : 'TIN')))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOptBusAdd#data -->
        <label class="optlabel" id="ShowNewBusCertified_#ii#" style="display:#Variables.ThisShowCert#;">
          <input type="Checkbox" name="NewBusCertified_#ii#" #Variables.ThisCert# value="Yes">
          I certify this EIN is correct. 
        </label>
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="NewBusDUNS_#ii#" class="optlabel">DUNS</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata">
        <input type="Text" name="NewBusDUNS_#ii#" id="NewBusDUNS_#ii#" value="#Variables.ThisDUNS#"
        size="11" maxlength="11" placeholder="9 digit number" onchange="
        var sMand											= isMand(this);
        if  (EditMask('New Business ###ii# DUNS', this.value, '9', sMand, 9, 9)
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel optlabel"></div>
      <div class="tbl_cell formdata">
        <input type="Button" name="JustAButton" value="Discard New Business ###ii#" onclick="
        $(this).closest('fieldset').remove();
        ">
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl --></cfoutput>
			<cfif i lt Variables.TotalNewBusPlus1>
				<cfoutput>
</fieldset><!-- /New Business ###ii# --><br/>
</cfoutput>
			<cfelse>
				<cfoutput>
</script>
</cfoutput>
			</cfif>
		</cfloop>
		<cfoutput>
</div><!-- /id="NewFieldsets" -->
</cfoutput>
	</cfif>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>

<!-- Info-Currently-Assoicated-Related: -->

#Request.SlafTopOfHeadEditLoanNmb#
<script>

gDivNewFieldsets											= null;
gNewBusTemplate												= null; 

$(function()
	{
	gDivNewFieldsets										= document.getElementById("NewFieldsets");				// reference 
	gNewBusTemplate											= document.getElementById("newbus_template").innerHTML;	// contents 
	});

</script>
</cfif><!--- /MandOptBusAdd --->
</cfoutput>
</cfsavecontent>

<!--- ************************************************************************************************************ --->

<!---
AUTHOR:				Steve Seaquist
DATE:				05/16/2006
DESCRIPTION:		Displays the onSubmit JavaScript associated with dsp_formdata_confirm. 
NOTES:				Since this file may be cfincluded into the onSubmit handler of a form tag (and hence be delimited by 
					double-quotes), do not use double-quotes to delimit non-numeric literals. Use single-quotes instead. 
INPUT:				None. 
OUTPUT:				Displays the onSubmit JavaScript associated with dsp_formdata_confirm. 
REVISION HISTORY:	10/17/2012, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to changes in dsp_formdata_confirm (none). 
					06/06/2006, SRS:	Adapted to ReadOnly((columnname)) variables. Allowed this file to be included inside 
										the onsubmit handler of a form tag (default), or in external function. 
					05/16/2006, SRS:	Original implementation.
--->

<cfinclude template="bld_readonly_confirm.cfm">
<cfparam name="Variables.OnSubmitFormName"				default="this"><!--- Appropriate in form tag onsubmit handler. --->

<cfif NOT Variables.ReadOnlyIMUserDOBDt>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserDOBDt.onchange())			return false;</cfoutput>
</cfif>

<cfif NOT Variables.ReadOnlyIMUserLast4DgtSSNNmb>
	<cfoutput>
if	(!#Variables.OnSubmitFormName#.IMUserLast4DgtSSNNmb.onchange())	return false;</cfoutput>
</cfif>

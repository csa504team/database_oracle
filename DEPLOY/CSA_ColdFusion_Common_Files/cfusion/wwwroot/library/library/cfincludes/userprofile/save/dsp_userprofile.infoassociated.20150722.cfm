<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc, for the US Small Business Administration. 
DATE:				07/22/2015. 
DESCRIPTION:		Included "group" of dsp_userprofile. 
NOTES:				See dsp_userprofile.template.cfm. 
INPUT:				CGI.Script_Name (to establish context of calling page). Variables scope "MandOpt" variables that control 
					what to do about individual form elements. 
OUTPUT:				Variables scope variables containing info needed to display a user profile, which may not necessarily be 
					the current user:	AppDataInline, JSInline and Show. 
REVISION HISTORY:	07/22/2015, SRS:	Original implementation. Previously was hardcoded in calling pages (not an include). 
										Made tabular data into a table for Section 508 reasons. 
--->

<!--- ************************************************************************************************************ --->

<cfsavecontent variable="Variables.AppDataInline">
	<cfoutput>#Variables.AppDataInline#</cfoutput>
	<cfparam name="Variables.MandOptBusAdd"					default="opt">
	<cfparam name="Variables.MandOptBusDel"					default="opt">
	<cfif	(Variables.Origs.RecordCountUserBus				gt 0)
		or	(ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0)>
		<cfoutput>
<fieldset class="inlineblock">
  <legend>Information Currently Associated with Profile</legend>
  <div class="tbl"></cfoutput>
	<cfif Variables.Origs.RecordCountUserBus gt 0>
		<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><span class="viewlabel">Business Information</span></div>
      <div class="tbl_cell formdata nowrap">
        <table border="1" cellpadding="2" summary="Business Information">
        <thead>
          <tr>
            <th>##</th>
            <th>E/S</th>
            <th>Tax ID</th>
            <th>DUNS</th><cfif ListFindNoCase("skip,view", Variables.MandOptBusDel) is 0>
            <th>Maintenance</th></cfif>
          </tr>
        </thead>
        <tbody></cfoutput>
		<cfif NOT IsDefined("AlternatingRowClassName")>
			<cfinclude template="#Variables.LibUdfURL#/bld_SearchUDFs.cfm">
		</cfif>
		<cfloop index="Idx" from="1" to="#Variables.Origs.RecordCountUserBus#">
			<cfset Variables.RemoveTitle					= "Remove the business on this row with ">
			<cfif Len(Variables.Origs["DispTaxId_"			& Idx]) gt 0>
				<cfset Variables.RemoveTitle				&= "Tax ID #Variables.Origs['DispTaxId_'			& Idx]#">
				<cfif Len(Variables.Origs["IMUserBusLocDUNSNmb_" & Idx]) gt 0>
					<cfset Variables.RemoveTitle			&= " and ">
				</cfif>
			</cfif>
			<cfif Len(Variables.Origs["IMUserBusLocDUNSNmb_"& Idx]) gt 0>
				<cfset Variables.RemoveTitle				&= "DUNS #Variables.Origs['IMUserBusLocDUNSNmb_'	& Idx]#">
			</cfif>
			<cfset Variables.RemoveTitle					&= ".">
			<cfoutput>
          <tr class="#AlternatingRowClassName(Idx)#">
            <td align="right">#Idx#</td>
            <td>#Variables.Origs["DispEINSSN_"			& Idx]#</td>
            <td>#Variables.Origs["DispTaxId_"			& Idx]#</td>
            <td>#Variables.Origs["IMUserBusLocDUNSNmb_"	& Idx]#</td><cfif ListFindNoCase("skip,view", Variables.MandOptBusDel) is 0>
            <td>
              <label class="#Variables.MandOptBusDel#label" title="#Variables.RemoveTitle#">
                <input type="Checkbox" name="BusDel" value="#Variables.Origs['IMUserBusSeqNmb_' & Idx]#"> Remove Business
              </label>
            </td></cfif>
          </tr></cfoutput>
		</cfloop>
		<cfoutput>
        </tbody>
        </table>
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif><!--- /Variables.Origs.RecordCountUserBus --->
	<!--- /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\ --->
	<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>
		<cfoutput>
    <input type="Hidden" name="TotalNewBus" value="0">
    <div class="tbl_row">
      <div class="tbl_cell formlabel"></div>
      <div class="tbl_cell formdata nowrap">
        <div class="optdata">
        <input type="Button" value="Add New Business" onclick="
        var sIdx											= parseInt(this.form.TotalNewBus.value, 10) + 1;
        var sFieldset										= document.createElement('fieldset');
        sFieldset.className									= 'inlineblock';
        sFieldset.innerHTML									= gNewBusTemplate.replace(/\^0/g, ''+sIdx);
        gDivNewFieldsets.appendChild(sFieldset);
        gDivNewFieldsets.appendChild(document.createElement('br'));
        this.form.TotalNewBus.value							= sIdx;
        ">
        </div><!-- /optdata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row --></cfoutput>
	</cfif>
	<cfoutput>
  </div><!-- /tbl -->
</fieldset><!-- /Information Currently Associated with Profile --><br/><!-- Break because fieldset is class="inlineblock" -->
</cfoutput>
	</cfif><!--- /Variables.Origs.RecordCountUserBus or MandOptBusAdd --->
	<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>
		<cfoutput>
<div id="NewFieldsets"></div>
</cfoutput>
	</cfif>
</cfsavecontent>

<cfsavecontent variable="Variables.JSInline">
	<cfoutput>#Variables.JSInline#<cfif ListFindNoCase("skip,view", Variables.MandOptBusAdd) is 0>

<!-- Info-Currently-Assoicated-Related: -->

#Request.SlafTopOfHeadEditLoanNmb#
<script>

gDivNewFieldsets											= null;
gNewBusTemplate												= null; 

$(function()
	{
	gDivNewFieldsets										= document.getElementById("NewFieldsets");				// reference 
	gNewBusTemplate											= document.getElementById("newbus_template").innerHTML;	// contents 
	document.#Variables.FormName#.TotalNewBus.value			= "0";
	});

</script>
<script type="text/sba_template" id="newbus_template">
  <legend>New Business ##^0</legend>
  Enter Tax ID, DUNS or both. 
  <div class="tbl">
    <div class="tbl_row">
      <div class="tbl_cell formlabel #Variables.MandOptBusAdd#label">EIN/SSN</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptBusAdd#data">
          <fieldset class="radio"><legend class="radio">Tax ID Type</legend>
          <label><input type="Radio" name="NewBusEINSSN_^0" id="NewBusEINSSN_^0_EIN" value="E" onclick="
          this.form.NewBusTaxId_^0.placeholder = '99-9999999';
          "> EIN </label>
          <label><input type="Radio" name="NewBusEINSSN_^0" id="NewBusEINSSN_^0_SSN" value="S" onclick="
          this.form.NewBusTaxId_^0.placeholder = '999-99-9999';
          "> SSN </label>
          </fieldset>
        </div><!-- /#Variables.MandOptBusAdd#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="NewBusTaxId_^0" class="#Variables.MandOptBusAdd#label">Tax ID</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptBusAdd#data">
        <input type="Text" name="NewBusTaxId_^0" id="NewBusTaxId_^0" value=""
        size="11" maxlength="11" onchange="
        var sMand											= isMand(this);
        if  (EditTin('New Business ##^0 Tax ID', this.value, sMand, 
                     document.getElementById('NewBusEINSSN_^0_EIN').checked ? 'EIN' : 
                   ( document.getElementById('NewBusEINSSN_^0_SSN').checked ? 'SSN' : 'TIN')))
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOptBusAdd#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label for="NewBusDUNS_^0" class="#Variables.MandOptBusAdd#label">DUNS</div>
      <div class="tbl_cell formdata">
        <div class="#Variables.MandOptBusAdd#data">
        <input type="Text" name="NewBusDUNS_^0" id="NewBusDUNS_^0" value=""
        size="11" maxlength="11" placeholder="9 digit number" onchange="
        var sMand											= isMand(this);
        if  (EditMask('New Business ##^0 DUNS', this.value, '9', sMand, 9, 9)
            return true;
        this.focus();
        return false;
        ">
        </div><!-- /#Variables.MandOptBusAdd#data -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</script>
</cfif><!--- /MandOptBusAdd --->
</cfoutput>
</cfsavecontent>

<!--- Possibly also append to Variables.Show here. --->

<!--- ************************************************************************************************************ --->

<!---
AUTHOR:				Steve Seaquist
DATE:				06/06/2006
DESCRIPTION:		Builds Variables.ReadOnly((columnname)) variables from Variables.ReadOnlyProfile and defaults. 
NOTES:				Called in both the dsp_formdata and dsp_onsubmit files. 
INPUT:				Variables.ReadOnlyProfile. 
OUTPUT:				Variables.ReadOnly((columnname)) variables. 
REVISION HISTORY:	10/17/2012, SRS:	Original library implementation. Cannibalized /eauth/cfincludes version, copied to 
										/library/cfincludes/userprofile, adapted to changes in dsp_formdata_confirm (none). 
					06/06/2006, SRS:	Original implementation.
--->

<cfparam name="Variables.ReadOnlyProfile"				default="No">
<cfif Variables.ReadOnlyProfile>
	<cfset Variables.ReadOnlyIMUserDOBDt				= "Yes">
	<cfset Variables.ReadOnlyIMUserLast4DgtSSNNmb		= "Yes">
<cfelse>
	<cfparam name="Variables.ReadOnlyIMUserDOBDt"			default="No">
	<cfparam name="Variables.ReadOnlyIMUserLast4DgtSSNNmb"	default="No">
</cfif>

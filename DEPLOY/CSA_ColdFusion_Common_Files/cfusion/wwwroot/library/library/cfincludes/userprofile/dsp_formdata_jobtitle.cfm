<!---
AUTHOR:				Steve Seaquist, based on an HTML <table>-based version by Asad Chaklader.
DATE:				09/02/2011.
DESCRIPTION:		Displays Fed Agency and Job Title dropdown menus.
NOTES:				See the ReadMe.html file in this directory, which can also be accessed by the symbolic link index.html.
					Asad's version assumed <table> tag in calling page. This page does the outermost <div class="tbl"> itself.
					Currently in development, but will eventually be called by Section 508 compliant versions of:
						/cls/dsp_addcustomer.cfm
						/cls/dsp_profile.cfm
						/security/user/dsp_user.cfm
INPUT:				Server:				AddBusNewGlsAct, IMAgncyCd, IMUserTypCd, JobTitlCd, SBADeptAgncCd.
					Browser:			gDivSBASupervisor. This implies that the caller must also include dsp_formdata_ldap
										inside a container that gets loaded into gDivSBASupervisor. This allows the container
										to group SBA Supervisor fields in their own fieldset, per Section 508.
OUTPUT:				2 drop-down menus (agency and job title).
REVISION HISTORY:	04/21/2015, RSD:	Modified getJobTtlTyp Identifier to 12. (by current user type)
					04/21/2015, NNI:   	Initial Implementation.
--->
<cfparam name="Variables.ImUserTypCd" default="">
<cfset Variables.JobTitlCd								= "0">
<cfset Variables.LogAct									= "retrieve Federal employee job titles">
<cfset Variables.cfprname								= "getJobTtlTyp">
<cfset Variables.Identifier								= "12"><!--- Same as Identifier 4, only alphabetized. --->
<cfset Variables.ImUserTypCd							= Variables.ImUserTypCd>
<cfset Variables.dbBeforeIMFedEmpJobTitlTypSelTSP		= Variables.db>
<cfset Variables.db										= "oracle_housekeeping">
<cfinclude template="/cfincludes/oracle/security/spc_IMJOBTITLTYPSELTSP.cfm">
<cfset Variables.db										= Variables.dbBeforeIMFedEmpJobTitlTypSelTSP>
<cfif Variables.TxnErr>
	<cfoutput>#Variables.ErrMsg#</cfoutput>
	<cfabort>
</cfif>
<cfloop query="getJobTtlTyp">
	<cfif getJobTtlTyp.JobTitlCd EQ 0>
		<cfoutput>
  <input type="hidden" name="PosOtherRow" value="#getJobTtlTyp.CurrentRow#"></cfoutput>
		<cfbreak>
	</cfif>
</cfloop>
<cfoutput>
  <div style="display:none;"><!-- Make extra sure that the browser doesn't display the JS text between the script tags. -->
  <script>
  // The following are not static. They depend on database conditions. So they cannot be moved to a separate .js file:
  function ConfirmTitleChange                           (pThis, pComplainAboutMandatory)
    {<cfif IsDefined("Variables.IMUserId") AND Variables.IMUserId>
    var	sIndex                                          = pThis.selectedIndex;
    if  ((sIndex <= 0) && pComplainAboutMandatory)      // Nothing selected, or the "Select One" option is selected.
        {
        alert("ERROR. Job Title is mandatory.");
        return false;
        }
    var sPrev                                         = "the previous job title";
    for (var i = 1; i < pThis.options.length; i++)
        if  (pThis.options[i].value == "'#Variables.JobTitlCd#'")
            {
            sPrev                                       = pThis.options[i].text;
            break;
            }
    if  (!confirm("Changing Job Title will delete all of your existing roles that require a specific Job Title. "
                + "Press OK to continue or Cancel to revert back to " + sPrev + "."))
        {
        // Revert JobTitlCd to '#Variables.JobTitlCd#', the value it was at the time it left the server:
        for (var i = 1; i < pThis.options.length; i++)
            if  (pThis.options[i].value == '#Variables.JobTitlCd#')
                {
                pThis.selectedIndex                     = i;
                return true;
                }
        pThis.selectedIndex                             = 0;
        }<cfelse>
    // No login yet, so selecting title cannot result in a loss of roles. Nothing to confirm. </cfif>
    return true;
    }
    <!--- present in dsp_addcustomer.cfm outside CLS login --->
<cfif listfindnocase("/cls/dsp_addcustomer.cfm",CGI.SCRIPT_NAME) eq 0>
  function ShowSBASupervisorEltsIfNeeded                (pThis, pComplainAboutMandatory)
    {<cfif IsDefined("Variables.AddBusNewGlsAct") AND Variables.AddBusNewGlsAct>
    var	sIndex                                          = pThis.selectedIndex;
    if  ((sIndex <= 0) && pComplainAboutMandatory)      // Nothing selected, or the "Select One" option is selected.
        {
        alert("ERROR. Agency is mandatory.");
        return false;
        }
    var sValue                                          = pThis.options[sIndex].value;
    if  (sValue == "#Variables.SBADeptAgncCd#")
        gDivSBASupervisor.style.display                 = '';
    else
        gDivSBASupervisor.style.display                 = 'none';<cfelse>
    // SBA Supervisor was determined at the time the account was created. </cfif>
    return true;
    }
</cfif>

function jobTitleChange(cb){
	//alert(cb.value);
		if(cb.value==4){
				var	sStyledspLincAreas				= document.getElementById("dspLincAreas").style;
			if(cb.checked){
				sStyledspLincAreas.display  = '';
			}else{
				sStyledspLincAreas.display  = 'none';
			}
		}
		if(cb.value==7){
				var	sStyleDivNonPartner				= document.getElementById("DivNonPartner").style;
				if(cb.checked){
				sStyleDivNonPartner.display  = '';
			}else{
				sStyleDivNonPartner.display  = 'none';
			}
			}
				if(cb.value==8){
				var	sStyleDivBondPartner				= document.getElementById("DivBondPartner").style;
				if(cb.checked){
				sStyleDivBondPartner.display  = '';
			}else{
				sStyleDivBondPartner.display  = 'none';
			}
			}

}

  </script>
  </div><!-- /style="display:none;" -->
  <div class="tbl" id="DivFedAgencyAndTitle">
</cfoutput>

<cfoutput>
    <div class="tbl_row">
      <div class="tbl_cell formlabel"><label class="optlabel" for="JobTitlCd" id="JobTitlelbl">Job Classification:</label></div>
      <div class="tbl_cell formdata">
        <div class="optdata" id="DivJobTitle">


	<!--- This is temporary code because of a problem with the DOM, the page isn't loading properly with the ajax call' --->
	<cfif pagename is "dsp_addcustomer.cfm" or pagename is "dsp_profile.cfm">
			<cfloop query="getAllUserType">
			        <ul id="JobTitlCdcont#getAllUserType.ImUserTypCd#" style="list-style-type: none;
								padding: 0px;
								margin: 0px;">
				<cfset Variables.LogAct									= "retrieve Federal employee job titles">
				<cfset Variables.ImUserTypCd							= getAllUserType.ImUserTypCd>
				<cfset Variables.cfprname								= "titleList">
				<cfset Variables.Identifier								= "12"><!--- Same as Identifier 4, only alphabetized. --->
				<cfset Variables.db										= "oracle_housekeeping">
				<cfset Variables.JobTitlCd 								= 0>
				<cfinclude template="/cfincludes/oracle/security/spc_IMJOBTITLTYPSELTSP.PUBLIC.cfm">
				<cfloop query="titleList">
					<li><input name="JobTitlCd" value="#titleList.JobTitlCd#" id="JobTitlCd#titleList.JobTitlCd#" type="checkbox" onchange="jobTitleChange(this);"><label for="JobTitlCd#titleList.JobTitlCd#">#titlelist.JobTitlDescTxt#</label></li>
				</cfloop>
				</ul>
			</cfloop>
	<cfelse>
	        <ul id="JobTitlCdcont" style="list-style-type: none;
								padding: 0px;
								margin: 0px;">
			</ul>
	</cfif>




</cfoutput>
		<!--- <select name="JobTitlCd" id="JobTitlCd" onchange="return ConfirmTitleChange(this, true);">
          <option value="">Select One</option>
<cfloop query="getJobTtlTyp">
	<cfif	IsDefined("Variables.JobTitlCd")
		and	(Variables.JobTitlCd EQ getJobTtlTyp.JobTitlCd)>
		<cfset Variables.Selected						= "selected">
	<cfelse>
		<cfset Variables.Selected						= "        ">
	</cfif>
	<cfoutput>
          <option #Variables.Selected# value="#getJobTtlTyp.JobTitlCd#" >#getJobTtlTyp.JobTitlDescTxt#</option></cfoutput>
</cfloop> </select>--->
<cfoutput>

        </div><!-- /manddata -->
      </div><!-- /formdata -->
    </div><!-- /tbl_row -->
  </div><!-- /tbl -->
</cfoutput>

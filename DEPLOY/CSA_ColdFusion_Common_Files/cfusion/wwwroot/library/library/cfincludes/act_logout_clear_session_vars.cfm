<!---
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc. 
DATE:				03/14/2008
DESCRIPTION:		Shared routine to copy Variables.CLS into Session.CLS and assorted other Session variable names. 
NOTES:				New apps should only be using the GLS structure and its substructures, not those assorted other 
					Session variables. At some point, we would like to have only Session.CLS in the Session scope, 
					but we can't do that as long as people keep assuming that its substructures are also defined. 
INPUT:				Session.CLS. If Session.CLS has been deleted, but Variables.CLS exists, use Variables.CLS to 
					make darn sure the variables OUTSIDE of Session.CLS also got deleted. 
OUTPUT:				Clears Session.CLS and (for now) other Session variables based on Session.CLS. 
REVISION HISTORY:	07/02/2008, SRS:	Standards call for developers to keep all Session variables unique to their 
										GLS Systems in a structure called Session.XXXX, where XXXX is the GLS System 
										name. Simply clearing the Session.CLS structure was leaving other GLS System 
										structures still defined. So users would sometimes log out and log back in again, 
										but retain the same privileges in those other structures. So now, we clear ALL 
										Session variables except those defined by ColdFusion itself. 
					06/09/2008, SRS:	Added a call to setMaxInactiveInterval(1). This times out the current session 
										after 1 second, but Session scope memory won't actually be liberated until the 
										next page request. Therefore, continue clearing Session scope variables anyway, 
										so that ColdFusion Server will get the memory back right away. 
					03/14/2008, SRS:	Original Implementation. 
--->

<cflock scope="SESSION" type="EXCLUSIVE" timeout="30">
	<cfset Session.setMaxInactiveInterval(1)>
	<cfloop index="Key"					list="#StructKeyList(Session)#">
		<cfswitch expression="#UCase(Key)#">
		<cfcase value="CFID,CFTOKEN,JSESSIONID,SESSIONID,URLTOKEN">	<!--- Leave these defined. --->		</cfcase>
		<cfdefaultcase>												<cfset StructDelete(Session, Key)>	</cfdefaultcase>
		</cfswitch>
	</cfloop>
</cflock>

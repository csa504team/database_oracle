<!---
AUTHOR:				Rushan Ahmed
DATE:				01/27/2016
DESCRIPTION:		Displays the issue indicator flag based on the loan number & its status code.
NOTES:

	Developers want to set issue indicator flag based on the action code (e.g. chargeoff, notes)�on Loan related applications when LoanGntyPreTbl exists so that users are informed of servicing/post pending action on the loan.
	
INPUT:				Variables.LoanNmb. 
OUTPUT:				Displays the TxnErr.msg. 
REVISION HISTORY:	02/02/2016, RA Initial version.
--->
<!--- Loan Information screen issue identified --->
<cfset Variables.BannStr 							= "">
<!--- Get issues for the loan for --->
<cfset Variables.Identifier							= "12">
<cfset Variables.cfprname							= "getloanIssueCd">
<cfset Variables.LogAct								= "get all loan Issue record">
<cfinclude template = "#SPIncludesURL#/oracle/loan/spc_#UCase('LOANGNTYISSSELTSP')#.cfm">
<cfif Variables.TxnErr>
	<cfdump var="#Variables.ErrMsg#">
	<cfabort>
</cfif>
<cfinclude template="/elend/udf/bld_IssueBannerUDF.cfm">
<cfset Variables.BannStr = IssueBanner(getloanIssueCd,"<li>")>
<cfif len(Variables.BannStr) gt 0>
	<cfoutput>
		<div>
		<TABLE Summary="Mandatory" align="center"><TR><TD class="dem_errmsg">
			WARNING. Outstanding Loan Issues: #Variables.BannStr#
		</TD></TR></TABLE>
		</div>
	</cfoutput>
</cfif>
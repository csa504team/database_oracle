<!---
AUTHOR:				Steve Seaquist
DATE:				06/22/2001
DESCRIPTION:		Performs the same data validation functions as the EditCdist JavaScript, only server-side, and in CFML.
NOTES:				A congressional district must be exactly 2 characters, and must be either numeric or "AL" for "at large". 
INPUT:				Attributes
OUTPUT:				Caller variables
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	06/22/2001, SRS:	Original implementation.
--->

<!--- Constants: --->
<CFINCLUDE TEMPLATE="LocalMachineSettingsEM.cfm">

<!--- Input Initializations: --->
<CFPARAM NAME="Attributes.Sco"				DEFAULT="Form">	<!--- Field's scope (almost always "Form"). --->
<CFPARAM NAME="Attributes.Fld">								<!--- Field's name (in the preceding scope). --->
<CFPARAM NAME="Attributes.Def"				DEFAULT="">		<!--- Only optional attribute, specifies a default for Form.#Attributes.Fld#. --->
<CFPARAM NAME="Attributes.Eng">								<!--- English name of field, if contains colon, generate hotlink. --->
<CFPARAM NAME="Attributes.Req"				DEFAULT="No">	<!--- Whether or not the field is required. --->
<CFPARAM NAME="Caller.EditCdistCallNbr"		DEFAULT="0">
<CFPARAM NAME="Caller.ErrMsg"				DEFAULT="">
<CFPARAM NAME="Caller.InternalErrMsg"		DEFAULT="">
<CFPARAM NAME="Caller.URLEncodedFormFields"	DEFAULT="">
<CFPARAM NAME="#Attributes.Sco#.#Attributes.Fld#"	DEFAULT="#Attributes.Def#">
<CFIF Len(Caller.InternalErrMsg) GT 0>
	<CFEXIT METHOD="EXITTAG"><!--- Internal errors are severe... only one per customer. --->
</CFIF>
<CFSET Caller.EditCdistCallNbr				= Caller.EditCdistCallNbr + 1>
<CFIF Len(Attributes.Fld) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Fld attribute (call #Caller.EditCdistCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Eng) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Eng attribute (call #Caller.EditCdistCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Req) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditCdist called with empty Req attribute (call #Caller.EditCdistCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>

<!--- The following allows hotlinking to the field in error. Assumes A NAME=xxx is defined in the dsp page displaying ErrMsg. --->
<CFSET Variables.EngArray					= ListToArray(Attributes.Eng,	":")>
<CFIF ArrayLen(Variables.EngArray) IS 1>	<CFSET Variables.HotFNam		= Attributes.Eng>
<CFELSE>									<CFSET Variables.HotFNam		= "<A HREF=""###Variables.EngArray[1]#"">#Variables.EngArray[2]#</A>"></CFIF>

<CFSET Variables.FVal						= Evaluate("#Attributes.Sco#.#Attributes.Fld#")>
<CFSET Caller.URLEncodedFormFields			= Caller.URLEncodedFormFields
											& "#URLEncodedFormat(Attributes.Fld)#=#URLEncodedFormat(Variables.FVal)#&">

<CFIF (NOT Attributes.Req) AND (Len(Variables.FVal) IS 0)>
	<CFEXIT METHOD="EXITTAG"><!--- SUCCESS. No Caller.ErrMsg returned. --->
<CFELSEIF Len(Variables.FVal) IS NOT 2>
	<CFIF Attributes.Req>
		<CFSET Caller.ErrMsg				= Caller.ErrMsg & "<LI>#Variables.HotFNam# must be exactly 2 character(s) long."			& Variables.CRLF>
	<CFELSE>
		<CFSET Caller.ErrMsg				= Caller.ErrMsg & "<LI>If given, #Variables.HotFNam# must be exactly 2 character(s) long."	& Variables.CRLF>
	</CFIF>
	<CFEXIT METHOD="EXITTAG">
<CFELSEIF (Variables.FVal IS NOT "AL") AND (NOT IsNumeric(Variables.FVal))>
	<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# must numeric or 'AL' (for 'at large')."			& Variables.CRLF>
</CFIF>

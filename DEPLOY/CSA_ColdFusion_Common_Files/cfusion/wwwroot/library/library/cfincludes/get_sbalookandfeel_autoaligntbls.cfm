<!---
AUTHOR:				Steve Seaquist
DATE:				02/25/2011
DESCRIPTION:		Just some stock HTML for frames that need to do the same thing as <cf_sbalookandfeel AutoAlignTbls="Yes">. 
NOTES:				Include at the HTML level, not inside a script tag, sometime *AFTER* including jquery.js. Scripts come out 
					of the /jquery.msie subdirectory because they're MSIE-only. Also, MSIE-only "conditional comments" prevent 
					both the inclusions and the $(document).ready() if the user is in a different browser. 
INPUT:				None. 
OUTPUT:				Plain HTML, targeted to MSIE 6, 7 or 8. Does nothing if the user is in a different browser. 
REVISION HISTORY:	02/25/2011, SRS:	Original implementation. 
---><cfoutput>
<!--[if lt IE 9]>
<script src="/library/javascripts/jquery/jquery.msie/jquery.findClosest.js"></script>
<script src="/library/javascripts/jquery/jquery.msie/jquery.findNestedFirst.js"></script>
<script src="/library/javascripts/jquery/jquery.msie/jquery.alignPseudoTables.js"></script>
<script>
$(document).ready(function()
	{
	$(document).findNestedFirst(".tbl").alignPseudoTables(".tbl", ".tbl_row", ".tbl_cell");
	});
</script>
<![endif]-->
</cfoutput>

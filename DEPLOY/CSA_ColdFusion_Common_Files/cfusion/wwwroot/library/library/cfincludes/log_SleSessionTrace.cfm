<!---
AUTHOR:				Steve Seaquist
DATE:				02/14/2007
DESCRIPTION:		Logs Trace log entry for session establishment actions. 
NOTES:				Called by get_sbalookandfeel_variables after Request.SlafSessionControl was defined. 
INPUT:				Request.SBALogSystemName, maybe. If that's given, Variables.SleSessionTraceAction is mandatory. 
OUTPUT:				If currently configured to log it, Trace log entry ("SleTrace"). 
REVISION HISTORY:	02/14/2007, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfset Request.SBALogText								= "Session Establishment Trace (#Variables.SleSessionTraceAction#): ">
	<cfif Len(Variables.OrigUserId) GT 0>
		<cfset Request.SBALogText							= Request.SBALogText & Variables.OrigUserId & ", ">
	<cfelse>
		<cfset Request.SBALogText							= Request.SBALogText & "(unknown user), ">
	</cfif>
	<cfset Request.SBALogText								= Request.SBALogText & "#Request.SlafSessionControl#, ">
	<cfif IsDefined("Cookie.CFID")>
		<cfset Request.SBALogText							= Request.SBALogText & "C.CFID=#Cookie.CFID#, ">
	</cfif>
	<cfif IsDefined("Cookie.CFToken")>
		<cfset Request.SBALogText							= Request.SBALogText & "C.CFToken=#Cookie.CFToken#, ">
	</cfif>
	<cfif IsDefined("Cookie.JSessionId")>
		<cfset Request.SBALogText							= Request.SBALogText & "C.JSessionId=#Cookie.JSessionId#, ">
	</cfif>
	<cfif IsDefined("URL.CFID")>
		<cfset Request.SBALogText							= Request.SBALogText & "U.CFID=#URL.CFID#, ">
	</cfif>
	<cfif IsDefined("URL.CFToken")>
		<cfset Request.SBALogText							= Request.SBALogText & "U.CFToken=#URL.CFToken#, ">
	</cfif>
	<cfif IsDefined("URL.JSessionId")>
		<cfset Request.SBALogText							= Request.SBALogText & "U.JSessionId=#URL.JSessionId#, ">
	</cfif>
	<cfif IsDefined("Variables.CFID")>
		<cfset Request.SBALogText							= Request.SBALogText & "V.CFID=#Variables.CFID#, ">
	</cfif>
	<cfif IsDefined("Variables.CFToken")>
		<cfset Request.SBALogText							= Request.SBALogText & "V.CFToken=#Variables.CFToken#, ">
	</cfif>
	<cfif IsDefined("Variables.JSessionId")>
		<cfset Request.SBALogText							= Request.SBALogText & "V.JSessionId=#Variables.JSessionId#, ">
	</cfif>
	<cfset Request.SBALogText								= Request.SBALogText & "Remote_Addr=#CGI.Remote_Addr#.">
	<cfinclude template="/library/cfincludes/log_SleTrace.cfm">
</cfif>

<!---
AUTHOR:				Steve Seaquist
DATE:				06/21/2001
DESCRIPTION:		Performs the same data validation functions as the EditMask JavaScript, only server-side, and in CFML.
NOTES:

	The following characters in Attributes.Msk are used to validate Form.#Attributes.Fld#:
	1     - any digit  in the range of 0 - 1.
	2     - any digit  in the range of 0 - 2.
	3     - any digit  in the range of 0 - 3.
	4     - any digit  in the range of 0 - 4.
	5     - any digit  in the range of 0 - 5.
	6     - any digit  in the range of 0 - 6.
	7     - any digit  in the range of 0 - 7.
	8     - any digit  in the range of 0 - 8.
	9     - any digit  in the range of 0 - 9.
	#     - any digit  in the range of 0 - 9,          or '.' (no more than 2 digits after the '.').
	$     - any digit  in the range of 0 - 9, '$', ',' or '.' (no more than 2 digits after the '.').
	%     - any digit  in the range of 0 - 9, '%'      or '.' (no more than 2 digits after the '.').
	A     - any letter in the range of A - Z or a - z.
	B     - any letter in the range of A - Z or a - z, or ' '.
	E     - (e-mail) - letter/number/'-'/'.'/'_' combo, followed by '@', followed by letter/number/'-'/'.'/'_' combo.
	                   At least one '@' is required, and the field must end in a US domain (".com", ".net", etc).
	G     - (gender) - M, m, F or f.
	I     - (IP addr)- 0 - 9 or '.', exactly 3 periods
	N     - [alpha]numeric - A - Z, a - z or 0 - 9
	P     - (Sybase password) - any printable character.
	S     - same as '$', but also allows '-'.  Used at start of money mask to allow negative dollar amount (as in Net Worth)
	U     - URL.
	W     - Web URL (http or https only).
	X     - any character.
	Y     - (yes/no) - Y, y, N, n, X or x.  
	                   X and x are equivalent to N and n, but thought of as "not applicable".
	Other - must match that character exactly (including case if it's a letter).

	Fld characters are matched to Msk characters, character for character, until the end of Msk, whereupon the last Msk 
	character repeats. So 999-999-9999 is our Msk for phone numbers, which can be shortened to 999-999-9, if it weren't 
	for the fact that the shortened mask would confuse end users. 

	If Lst="Yes", Form.#Attributes.Fld# is treated as a comma-delimited list, and the mask is applied to each of its items. 
	Consequently, there is no "EditList" custom tag. If you use Lst="Yes", make sure Eng is of a form where, to refer to 
	them collectively, you could add "(s)" and to refer to a particular one, you could add a number. For example, 
	Eng="NAICS Code" is good, because collectively "NAICS Code(s)" and individually "NAICS Code #3" are good. 

INPUT:				Attributes
OUTPUT:				Caller variables
CHECKED OUT TO:		N/A
CHECKED OUT DATE:	N/A
REVISION HISTORY:	06/22/2001, SRS:	Added EditList logic. 
					06/21/2001, SRS:	Original implementation.
--->

<!--- Constants: --->
<CFINCLUDE TEMPLATE="LocalMachineSettingsEM.cfm">
<CFSET Variables.OkayTLDs				= "com,edu,gov,int,mil,net,org,cc,nu,to,us,ws,aero,biz,coop,info,name,museum,pro">

<!--- Input Initializations: --->
<CFPARAM NAME="Attributes.Sco"				DEFAULT="Form">	<!--- Field's scope (almost always "Form"). --->
<CFPARAM NAME="Attributes.Fld">								<!--- Field's name (in the preceding scope). --->
<CFPARAM NAME="Attributes.Def"				DEFAULT="">		<!--- Specifies a default for Form.#Attributes.Fld#. --->
<CFPARAM NAME="Attributes.Eng">								<!--- English name of field, if contains colon, generate hotlink. --->
<CFPARAM NAME="Attributes.Msk">								<!--- Edit mask. Same as for EditMask JavaScript. --->
<CFPARAM NAME="Attributes.Req"				DEFAULT="No">	<!--- Whether or not the field is required. --->
<CFPARAM NAME="Attributes.Typ"				DEFAULT="1">	<!--- 1 = text, 2 = radio, 3 = drop-down, 4 = hidden. --->
<CFPARAM NAME="Attributes.Min"				DEFAULT="1">	<!--- If field entered by user, minimum number of characters. --->
<CFPARAM NAME="Attributes.Max"				DEFAULT="255">	<!--- If field entered by user, maximum number of characters. --->
<CFPARAM NAME="Attributes.Lst"				DEFAULT="No">	<!--- If yes, Form.#Attributes.Fld# is a comma-delimited list. --->
<CFPARAM NAME="Attributes.Tra"				DEFAULT="No">	<!--- If yes, build a trace listing in Caller.Trace. --->
<CFPARAM NAME="Caller.EditMaskCallNbr"		DEFAULT="0">
<CFPARAM NAME="Caller.ErrMsg"				DEFAULT="">		<!--- If this changes, the EditMask call failed due to bad data. --->
<CFPARAM NAME="Caller.InternalErrMsg"		DEFAULT="">		<!--- If this changes, the EditMask call failed due to bad call. --->
<CFIF Attributes.Tra>
	<CFPARAM NAME="Caller.Trace"			DEFAULT="">
</CFIF>
<CFPARAM NAME="Caller.URLEncodedFormFields"	DEFAULT="">
<CFSET Caller.ValueInCents					= 0>
<CFPARAM NAME="#Attributes.Sco#.#Attributes.Fld#"	DEFAULT="#Attributes.Def#">
<CFIF Len(Caller.InternalErrMsg) GT 0>
	<CFEXIT METHOD="EXITTAG"><!--- Internal errors are severe... limit one per customer. --->
</CFIF>
<CFSET Caller.EditMaskCallNbr				= Caller.EditMaskCallNbr + 1>
<CFIF Attributes.Tra><CFSET Caller.Trace	= "Call #Caller.EditMaskCallNbr#:#Variables.CRLF#"></CFIF>
<CFIF Len(Attributes.Fld) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Fld attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Eng) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Eng attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Msk) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Msk attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Req) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Req attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Typ) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Typ attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF NOT IsNumeric(Attributes.Typ)>		<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with non-numeric Typ attribute (call #Caller.EditMaskCallNbr#).">	<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Min) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Min attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF NOT IsNumeric(Attributes.Min)>		<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with non-numeric Min attribute (call #Caller.EditMaskCallNbr#).">	<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF Len(Attributes.Max) IS 0>				<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with empty Max attribute (call #Caller.EditMaskCallNbr#).">		<CFEXIT METHOD="EXITTAG"></CFIF>
<CFIF NOT IsNumeric(Attributes.Max)>		<CFSET Caller.InternalErrMsg	= "INTERNAL ERROR. CF_EditMask called with non-numeric Max attribute (call #Caller.EditMaskCallNbr#).">	<CFEXIT METHOD="EXITTAG"></CFIF>

<!---
If Eng contains ":", the following allows hotlinking to the field in error. This assumes A NAME=xxx is defined in the dsp page 
displaying ErrMsg. Also tack on "(s)" if comma-delimited list. 
--->
<CFSET Variables.EngArray					= ListToArray(Attributes.Eng,	":")>
<CFIF ArrayLen(Variables.EngArray) IS 1>
	<CFIF Attributes.Lst>					<CFSET Variables.HotFNam		= Attributes.Eng & "(s)">
	<CFELSE>								<CFSET Variables.HotFNam		= Attributes.Eng></CFIF>
<CFELSE>
	<CFIF Attributes.Lst>					<CFSET Variables.HotFNam		= "<A HREF=""###Variables.EngArray[1]#"">#Variables.EngArray[2]#(s)</A>">
	<CFELSE>								<CFSET Variables.HotFNam		= "<A HREF=""###Variables.EngArray[1]#"">#Variables.EngArray[2]#</A>"></CFIF>
</CFIF>

<!--- FValue (longer name) is the entire contents of the field: --->
<CFSET Variables.FValue						= Evaluate("#Attributes.Sco#.#Attributes.Fld#")>
<CFIF Attributes.Tra><CFSET Caller.Trace	= "#Caller.Trace#  #Attributes.Sco#.#Attributes.Fld#=""#Variables.FValue#""#Variables.CRLF#"></CFIF>
<CFSET Caller.URLEncodedFormFields			= Caller.URLEncodedFormFields
											& "#URLEncodedFormat(Attributes.Fld)#=#URLEncodedFormat(Variables.FValue)#&">
<CFIF Len(Variables.FValue) IS 0>
	<CFIF Attributes.Req>
		<CFSET Caller.ErrMsg						= Caller.ErrMsg & "<LI>Mandatory field missing.  ">
		<CFSWITCH EXPRESSION="#Attributes.Typ#">
		<CFCASE VALUE="2">	<CFSET Caller.ErrMsg	= Caller.ErrMsg & "You must select one of the #Variables.HotFNam# option buttons."></CFCASE>
		<CFCASE VALUE="3">	<CFSET Caller.ErrMsg	= Caller.ErrMsg & "You must select one of the items from the #Variables.HotFNam# drop-down menu."></CFCASE>
		<CFCASE VALUE="4">	<CFSET Caller.ErrMsg	= Caller.ErrMsg & "The hidden #Variables.HotFNam# field was somehow lost."></CFCASE>
		<CFDEFAULTCASE>		<CFSET Caller.ErrMsg	= Caller.ErrMsg & "You must enter something into the #Variables.HotFNam# field."></CFDEFAULTCASE>
		</CFSWITCH>
		<CFSET Caller.ErrMsg						= Caller.ErrMsg & Variables.CRLF>
	</CFIF>
	<CFEXIT METHOD="EXITTAG">
</CFIF>

<!--- FVal (shorter name) is the value to be validated, which could be an element of a list. Either way, use FValueArray: --->
<CFIF Attributes.Lst>
	<CFSET Variables.FValueArray					= ListToArray (Variables.FValue)>
<CFELSE>
	<CFSET Variables.FValueArray					= ArrayNew(1)>
	<CFSET Variables.FValueArray[1]					= Variables.FValue>
</CFIF>
<CFSET Variables.FValueArrayLen						= ArrayLen(Variables.FValueArray)><!--- Saves on evaluationg in CFLOOP. --->

<CFLOOP INDEX="Sub" FROM="1" TO="#Variables.FValueArrayLen#">
	<CFIF Attributes.Lst>
		<CFIF ArrayLen(Variables.EngArray) IS 1>
			<CFSET Variables.HotFNam				= Attributes.Eng & " ###Sub#">
		<CFELSE>
			<CFSET Variables.HotFNam				= "<A HREF=""###Variables.EngArray[1]#"">#Variables.EngArray[2]# ###Sub#</A>">
		</CFIF>
		<CFSET Variables.FVal						= Trim(Variables.FValueArray[Sub])>
	<CFELSE>
		<CFSET Variables.FVal						=	   Variables.FValueArray[Sub]>
	</CFIF>
	<CFIF Attributes.Tra><CFSET Caller.Trace		= "#Caller.Trace#  Variables.FVal=""#Variables.FVal#""#Variables.CRLF#"></CFIF>

	<CFIF Len(Variables.FVal) LT Attributes.Min>

		<CFIF Attributes.Req>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# ">
		<CFELSE>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>If given, #Variables.HotFNam# ">
		</CFIF>
		<CFIF Attributes.Min IS Attributes.Max>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "must be exactly #Attributes.Min# character(s) long."		& Variables.CRLF>
		<CFELSE>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "must be at least #Attributes.Min# character(s) long."	& Variables.CRLF>
		</CFIF>

	<CFELSEIF Len(Variables.FVal) GT Attributes.Max>

		<CFIF Attributes.Req>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# ">
		<CFELSE>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>If given, #Variables.HotFNam# ">
		</CFIF>
		<CFIF Attributes.Min IS Attributes.Max>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "must be exactly #Attributes.Max# character(s) long."		& Variables.CRLF>
		<CFELSE>
			<CFSET Caller.ErrMsg					= Caller.ErrMsg & "cannot be more than #Attributes.Max# character(s) long."	& Variables.CRLF>
		</CFIF>

	<CFELSE><!--- Len(FVal) is between Min and Max. --->

		<CFSET Variables.AllDollars					= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllEMail					= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllIPAddr					= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllNumber					= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllPercents				= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllURL						= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AllWs						= "Yes"><!--- This refers to whether the mask is all one type. --->
		<CFSET Variables.AtSignWasEncountered		= "No">
		<CFSET Variables.DigitsAfterPeriod			= 0>
		<CFSET Variables.ErrorInLoop				= "No"><!--- Refers only to inner loop, indexed by Idx. --->
		<CFSET Variables.FLen						= Len(Variables.FVal)>
		<CFSET Variables.LPI						= 0><!--- Last period index. --->
		<CFSET Variables.MLen						= Len(Attributes.Msk)>
		<CFSET Variables.PeriodWasEncountered		= 0>
		<CFSET Variables.ValueAfterPeriod			= 0>
		<CFSET Variables.ValueBeforePeriod			= 0>
		<CFSET Variables.ValueInCents				= 0>
		<CFSET Variables.ValueIsNegative			= "No">
		<CFIF Attributes.Tra><CFSET Caller.Trace	= "#Caller.Trace#  EditMask loop:"></CFIF>
		<CFLOOP INDEX="Variables.Idx" FROM="1" TO="#Variables.FLen#">
			<CFSET Variables.FChar					= Mid(Variables.FVal, Variables.Idx, 1)>
			<CFSET Variables.FAsc					= Asc(Variables.FChar)>
			<CFIF Variables.Idx GT Variables.MLen>
				<CFSET Variables.MChar				= Right(Attributes.Msk, 1)>
			<CFELSE>
				<CFSET Variables.MChar				= Mid(Attributes.Msk, Variables.Idx, 1)>
			</CFIF>
			<CFIF Variables.MChar					IS "##"><!--- '##' in CFCASE VALUE clause causes CF to barf, ... --->
				<CFSET Variables.MChar				= "PoundSign"><!--- ... so internally represent it as "PoundSign". --->
			</CFIF>
			<CFIF Attributes.Tra><CFSET Caller.Trace= "#Caller.Trace# ""#Variables.FChar#""/""#Variables.FAsc#""/""#Variables.MChar#"","></CFIF>
			<CFIF  Variables.FChar IS ".">											<CFSET Variables.LPI			= Variables.Idx></CFIF>
			<CFIF  Variables.MChar IS NOT "PoundSign">								<CFSET Variables.AllNumber		= "No"></CFIF>
			<CFIF (Variables.MChar IS NOT "$") AND (Variables.MChar IS NOT "S")>	<CFSET Variables.AllDollars		= "No"></CFIF>
			<CFIF  Variables.MChar IS NOT "%">										<CFSET Variables.AllPercents	= "No"></CFIF>
			<CFIF  Variables.MChar IS NOT "E">										<CFSET Variables.AllEMail		= "No"></CFIF>
			<CFIF  Variables.MChar IS NOT "I">										<CFSET Variables.AllIPAddr		= "No"></CFIF>
			<CFIF (Variables.MChar IS NOT "U") AND (Variables.MChar IS NOT "W")>	<CFSET Variables.AllURL			= "No"></CFIF>
			<CFIF  Variables.MChar IS NOT "W">										<CFSET Variables.AllWs			= "No"></CFIF>

			<!--- Variables.TempErrMsg is being built just in case it's needed, not because anything bad has happened yet: --->
			<CFSET Variables.TempErrMsg				= "<LI>#Variables.HotFNam# ">
			<CFIF		Attributes.Msk IS "##">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid number.  ">
			<CFELSEIF	Attributes.Msk IS "$">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid dollar amount.  ">
			<CFELSEIF	Attributes.Msk IS "%">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid percentage.  ">
			<CFELSEIF	Attributes.Msk IS "9">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid number.  ">
			<CFELSEIF	Attributes.Msk IS "A">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not alphabetic.  ">
			<CFELSEIF	Attributes.Msk IS "E">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid Internet e-mail address.  ">
			<CFELSEIF	Attributes.Msk IS "I">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid full IP address.  ">
			<CFELSEIF	Attributes.Msk IS "N">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not alphanumeric.  ">
			<CFELSEIF	Attributes.Msk IS "P">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid Sybase password.  ">
			<CFELSEIF	Attributes.Msk IS "S##">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid signed floating-point number.  ">
			<CFELSEIF	Attributes.Msk IS "S$">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid signed dollar amount.  ">
			<CFELSEIF	Attributes.Msk IS "S9">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid signed number.  ">
			<CFELSEIF	Attributes.Msk IS "U">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid URL.  ">
			<CFELSEIF	Attributes.Msk IS "W">
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "is not a valid Web page URL.  ">
			<CFELSE>
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "must be of the form '#Attributes.Msk#'.  ">
			</CFIF>
			<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "Failure was at character #Variables.Idx# ('#Variables.FChar#'), ">
			<!--- Double-quote is prohibited in all fields: --->
			<CFIF Variables.FChar IS Chr(34)>
				<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "which cannot be a double-quote, because double-quote interferes "
													& "with VALUE clauses in Web page forms. Prohibited in all text input fields."
													& Variables.CRLF>
				<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
			</CFIF>
			<CFSWITCH EXPRESSION="#MChar#">
			<CFCASE VALUE="1,2,3,4,5,6,7,8,9,'PoundSign','$','%','S'"><!--- Numeric, with possible other characters: --->
				<CFIF		IsNumeric(Variables.MChar)>
					<CFIF NOT IsNumeric(Variables.FChar)>
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been numeric."						& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					<CFELSEIF Variables.FChar GT Variables.MChar>
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which must not exceed #Variables.MChar#."				& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					</CFIF>
				<CFELSE>
					<CFIF NOT IsNumeric(Variables.FChar)>
						<CFIF		(Variables.MChar IS "PoundSign")
						  AND (NOT	(IsNumeric(Variables.FChar)))
						  AND (NOT	(Variables.FChar IS "."))>
							<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "which should have been numeric or '.'."			& Variables.CRLF>
							<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
						<CFELSEIF	(Variables.MChar IS "$")
						  AND (NOT	(IsNumeric(Variables.FChar)))
						  AND (NOT ((Variables.FChar IS ".") OR (Variables.FChar IS "$") OR (Variables.FChar IS ",")))>
							<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "which should have been numeric, '$', ',' or '.'."	& Variables.CRLF>
							<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
						<CFELSEIF	(Variables.MChar IS "%")
						  AND (NOT	(IsNumeric(Variables.FChar)))
						  AND (NOT ((Variables.FChar IS ".") OR (Variables.FChar IS "%")))>
							<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "which should have been numeric, '.' or '%'."		& Variables.CRLF>
							<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
						<CFELSEIF	(Variables.MChar IS "S")
						  AND (NOT	(IsNumeric(Variables.FChar)))
						  AND (NOT ((Variables.FChar IS "+") OR (Variables.FChar IS "-")))>
							<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "which should have been numeric, '+' or '-'."		& Variables.CRLF>
							<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
						</CFIF>
						<CFIF Variables.FChar IS ".">
							<CFIF Variables.PeriodWasEncountered GT 0>
								<CFSET Variables.TempErrMsg		= Variables.TempErrMsg & "because you can't have 2 decimal points."		& Variables.CRLF>
								<CFSET Variables.ErrorInLoop	= "Yes"><CFBREAK><!--- Inner loop only --->
							<CFELSE>
								<CFSET Variables.PeriodWasEncountered	= Variables.PeriodWasEncountered + 1>
							</CFIF>
						<CFELSEIF Variables.FChar IS "-"><!--- Can get here only if MChar is "S", so no need to test MChar. --->
							<CFSET Variables.ValueIsNegative			= "Yes">
						<CFELSEIF IsNumeric(Variables.FChar)>
							<CFIF Variables.PeriodWasEncountered GT 0>
								<CFSET Variables.DigitsAfterPeriod		= Variables.DigitsAfterPeriod + 1>
								<CFSET Variables.ValueAfterPeriod		= (Variables.ValueAfterPeriod	* 10) + Variables.FChar>
							<CFELSE>
								<CFSET Variables.ValueBeforePeriod		= (Variables.ValueBeforePeriod	* 10) + Variables.FChar>
							</CFIF>
						</CFIF>
					</CFIF>
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="A,B"><!--- Alphabetic, or Alphabetic-including-blank: --->
				<CFIF  (Variables.FAsc LT Asc("A"))
				   OR ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
				   OR  (Variables.FAsc GT Asc("z"))><!--- That is, if not alphabetic: --->
					<CFIF Variables.MChar IS "A">
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been alphabetic."				& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					<CFELSEIF Variables.FChar IS NOT " ">
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been alphabetic or ' '."		& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					</CFIF>
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="E"><!--- E-mail address: --->
				<CFIF  (Variables.FAsc LT Asc("0"))
				   OR ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
				   OR ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
				   OR  (Variables.FAsc GT Asc("z"))><!--- That is, if not alphabetic or numeric: --->
					<CFIF (Variable.FChar IS NOT "&")
					  AND (Variable.FChar IS NOT "-")
					  AND (Variable.FChar IS NOT ".")
					  AND (Variable.FChar IS NOT "@")
					  AND (Variable.FChar IS NOT "_")>
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been alphabetic, numeric, "
																& "ampersand, minus, period, '@' or underscore."							& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					</CFIF>
					<CFIF Variables.FChar IS "@">
						<CFIF Variables.AtSignWasEncountered>
							<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "because you can't have 2 @-signs."				& Variables.CRLF>
							<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
						<CFELSE>
							<CFSET Variables.AtSignWasEncountered	= "Yes">
						</CFIF>
					</CFIF>
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="G"><!--- Gender: --->
				<CFIF (Variables.FChar IS NOT "M")
				  AND (Variables.FChar IS NOT "F")>
					<CFSET Variables.TempErrMsg		= Variables.TempErrMsg & "which should have been 'M' or 'F'."				& Variables.CRLF>
					<CFSET Variables.ErrorInLoop	= "Yes"><CFBREAK><!--- Inner loop only --->
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="I"><!--- IP address: --->
				<CFIF  (Variables.FAsc GE Asc("0"))
				  AND  (Variables.FAsc LE Asc("9"))><!--- That is, if numeric: --->
					<CFSET Variables.ValueBeforePeriod			= (Variables.ValueBeforePeriod	* 10) + Variables.FChar>
				<CFELSE><!--- That is, if not numeric: --->
					<CFIF Variables.FChar IS ".">
						<CFSET Variables.PeriodWasEncountered	= Variables.PeriodWasEncountered + 1>
						<CFIF Variables.ValueBeforePeriod GT 255>
							<CFSET Variables.TempErrMsg			= Variables.TempErrMsg & "because the value before this '.' is over 255."	& Variables.CRLF>
							<CFSET Variables.ErrorInLoop		= "Yes"><CFBREAK><!--- Inner loop only --->
						<CFELSE>
							<CFSET Variables.ValueBeforePeriod	= 0>
						</CFIF>
					<CFELSE>
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been numeric or '.'."			& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					</CFIF>
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="N"><!--- Alphabetic or numeric: --->
				<CFIF  (Variables.FAsc LT Asc("0"))
				   OR ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
				   OR ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
				   OR  (Variables.FAsc GT Asc("z"))><!--- That is, if not alphabetic or numeric: --->
					<CFSET Variables.TempErrMsg					= Variables.TempErrMsg & "which should have been alphabetic or numeric."	& Variables.CRLF>
					<CFSET Variables.ErrorInLoop				= "Yes"><CFBREAK><!--- Inner loop only --->
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="P"><!--- Printable = Sybase Password: --->
				<CFIF  (Variables.FAsc LT Asc(" "))
				   OR  (Variables.FAsc GT Asc("~"))>
					<CFSET Variables.TempErrMsg					= Variables.TempErrMsg & "which should have been a printable character."	& Variables.CRLF>
					<CFSET Variables.ErrorInLoop				= "Yes"><CFBREAK><!--- Inner loop only --->
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="U,W"><!--- URL, or WWW URL: --->
				<CFIF  (Variables.FAsc LT Asc("0"))
				   OR ((Variables.FAsc GT Asc("9")) AND (Variables.FAsc LT Asc("A")))
				   OR ((Variables.FAsc GT Asc("Z")) AND (Variables.FAsc LT Asc("a")))
				   OR  (Variables.FAsc GT Asc("z"))><!--- That is, if not alphabetic or numeric: --->
					<CFIF (Variables.FChar IS NOT "##")
					  AND (Variables.FChar IS NOT "$")
					  AND (Variables.FChar IS NOT "%")
					  AND (Variables.FChar IS NOT "&")
					  AND (Variables.FChar IS NOT "+")
					  AND (Variables.FChar IS NOT "-")
					  AND (Variables.FChar IS NOT ".")
					  AND (Variables.FChar IS NOT "/")
					  AND (Variables.FChar IS NOT ":")
					  AND (Variables.FChar IS NOT "=")
					  AND (Variables.FChar IS NOT "?")
					  AND (Variables.FChar IS NOT "@")
					  AND (Variables.FChar IS NOT "_")
					  AND (Variables.FChar IS NOT "~")>
						<CFSET Variables.TempErrMsg				= Variables.TempErrMsg & "which should have been alphabetic, numeric, "
																& "'##', '$', '%', ampersand, plus, minus, period, '/', ':', '=', '?', "
																& "'@', underscore or tilde."												& Variables.CRLF>
						<CFSET Variables.ErrorInLoop			= "Yes"><CFBREAK><!--- Inner loop only --->
					</CFIF>
				</CFIF>
			</CFCASE>
			<CFCASE VALUE="Y"><!--- 'Y' or 'N': --->
				<CFIF (Variables.FChar IS NOT "Y")
				  AND (Variables.FChar IS NOT "N")>
					<CFSET Variables.TempErrMsg					= Variables.TempErrMsg & "which should have been 'Y' or 'N'."				& Variables.CRLF>
					<CFSET Variables.ErrorInLoop				= "Yes"><CFBREAK><!--- Inner loop only --->
				</CFIF>
			</CFCASE>
			<CFDEFAULTCASE><!--- If not one off the mask characters above, and if not "X", FChar must equal MChar: --->
				<CFIF (Variables.MChar IS NOT "X")
				  AND (Variables.FChar IS NOT Variables.MChar)>
					<CFSET Variables.TempErrMsg					= Variables.TempErrMsg & "which should have been '#Variables.MChar#'."		& Variables.CRLF>
					<CFSET Variables.ErrorInLoop				= "Yes"><CFBREAK><!--- Inner loop only --->
				</CFIF>
			</CFDEFAULTCASE>
			</CFSWITCH>
		</CFLOOP>

		<CFIF Variables.ErrorInLoop>
			<CFIF Attributes.Tra><CFSET Caller.Trace			= "#Caller.Trace# failure.#Variables.CRLF#"></CFIF>
			<CFSET Caller.ErrMsg								= Caller.ErrMsg & Variables.TempErrMsg>
		<CFELSE>
			<CFIF Attributes.Tra><CFSET Caller.Trace			= "#Caller.Trace# success.#Variables.CRLF#"></CFIF>

			<!--- If we get here, we compared correctly to the edit mask. But we could still fail on special edits: --->

			<CFIF Variables.AllDollars OR Variables.AllNumber OR Variables.AllPercents><!--- Type-specific special edits. --->

				<CFSWITCH EXPRESSION="#Variables.DigitsAfterPeriod#">
				<CFCASE VALUE="0">	<CFSET Variables.ValueOfCentsOnly	= 0>								</CFCASE>
				<CFCASE VALUE="1">	<CFSET Variables.ValueOfCentsOnly	= Variables.ValueAfterPeriod * 10>	</CFCASE>
				<CFCASE VALUE="2">	<CFSET Variables.ValueOfCentsOnly	= Variables.ValueAfterPeriod>		</CFCASE>
				<CFDEFAULTCASE>		<CFSET Variables.ValueInCents		= 0>
									<CFSET Variables.ValueOfCentsOnly	= 0>								</CFDEFAULTCASE>
				</CFSWITCH>
				<CFSET Caller.ValueInCents						= (Variables.ValueBeforePeriod * 100) +	Variables.ValueOfCentsOnly>
				<CFSET Caller.ValueOfCentsOnly					=										Variables.ValueOfCentsOnly>
				<CFIF Variables.ValueIsNegative>
					<CFSET Caller.ValueInCents					= 0 - Caller.ValueInCents>
				</CFIF>

				<CFIF Variables.DigitsAfterPeriod GT 2>
					<CFSET Caller.ErrMsg						= Caller.ErrMsg & "<LI>#Variables.HotFNam# cannot have more than 2 digits after the decimal point."			& Variables.CRLF>
				</CFIF>

			<CFELSEIF Variables.AllPercents AND (Variables.DigitsAfterPeriod GT 2)><!--- Type-specific special edits. --->

				<CFSET Caller.ErrMsg							= Caller.ErrMsg & "<LI>#Variables.HotFNam# cannot have more than 2 digits after the decimal point.">
				<CFIF Variables.ValueBeforePeriod IS 0>
					<CFSET Caller.ErrMsg						= Caller.ErrMsg & "  (express as a percentage, not as a fraction of 1.0000)">
				</CFIF>
				<CFSET Caller.ErrMsg							= Caller.ErrMsg & "."	& Variables.CRLF>

			<CFELSEIF Variables.AllEMail><!--- Type-specific special edits. --->

				<CFIF Variables.AtSignWasEncountered IS 0>
					<CFSET Caller.ErrMsg						= Caller.ErrMsg & "<LI>#Variables.HotFNam# must contain '@'."	& Variables.CRLF>
				<CFELSE>
					<CFSET Variables.TLD						= "(none)">
					<CFSET Variables.Okay						= "No">
					<CFSET Variables.OkayTLDArray				= ListToArray(Variables.OkayTLDs)>
					<CFIF Variables.LPI GT 0>
						<CFSET Variables.TLD					= Right(Variables.FVal, Variables.FLen - Variables.LPI)>
						<CFSET Variables.OkayTLDArrayLen		= ArrayLen(Variables.OkayTLDArray)>
						<CFLOOP INDEX="Idx" FROM="1" TO="#Variables.OkayTLDArrayLen#">
							<CFIF Variables.OkayTLDArray[Idx] IS Variables.TLD>
								<CFSET Variables.Okay			= "Yes"><CFBREAK><!--- Inner loop only --->
							</CFIF>
						</CFLOOP>
					</CFIF>
					<CFIF NOT Variables.Okay>
						<CFSET Variables.OkayTLDsPrettyList		= ArrayToList(Variables.OkayTLDArray, ", ")><!--- "Pretty" because of space after comma. --->
						<CFSET Caller.ErrMsg					= Caller.ErrMsg
																& "<LI>#Variables.HotFNam# has a 'Top Level Domain name' of '#Variables.TLD#'. "
																& "PRO-Net e-mail addresses must end in a United States 'Top Level Domain name': "
																& "#Variables.OkayTLDsPrettyList#."
																& Variables.CRLF>
					</CFIF>
				</CFIF>

			<CFELSEIF Variables.AllIPAddr><!--- Type-specific special edits. --->

				<CFIF Variables.PeriodWasEncountered IS NOT 3>
					<CFSET Caller.ErrMsg						= Caller.ErrMsg & "<LI>#Variables.HotFNam# must contain exactly 3 periods. "	& Variables.CRLF>
				</CFIF>

			<CFELSEIF Variables.AllURL OR Variables.AllWs><!--- Type-specific special edits. --->

				<!--- Could potentially generate 2 special edit error messages. First is bad protocol: --->
				<CFSET Variables.ServerIdx						= 0>
				<CFIF Variables.AllWs>
					<CFIF (Left(Variables.FVal, 7) IS "http://")>	<CFSET Variables.ServerIdx	=  8></CFIF>
					<CFIF (Left(Variables.FVal, 8) IS "https://")>	<CFSET Variables.ServerIdx	=  9></CFIF>
					<CFIF Variables.ServerIdx IS 0>
						<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# must begin with "
																& "'http://' or 'https://'."
																& Variables.CRLF>
					</CFIF>
				<CFELSE>
					<CFIF (Left(Variables.FVal, 6) IS "ftp://")>	<CFSET Variables.ServerIdx	=  7></CFIF>
					<CFIF (Left(Variables.FVal, 9) IS "gopher://")>	<CFSET Variables.ServerIdx	= 10></CFIF>
					<CFIF (Left(Variables.FVal, 7) IS "http://")>	<CFSET Variables.ServerIdx	=  8></CFIF>
					<CFIF (Left(Variables.FVal, 8) IS "https://")>	<CFSET Variables.ServerIdx	=  9></CFIF>
					<CFIF (Left(Variables.FVal, 9) IS "telnet://")>	<CFSET Variables.ServerIdx	= 10></CFIF>
					<CFIF Variables.ServerIdx IS 0>
						<CFSET Caller.ErrMsg					= Caller.ErrMsg & "<LI>#Variables.HotFNam# must begin with "
																& "'ftp://', 'gopher://', 'http://', 'https://' or 'telnet://'."
																& Variables.CRLF>
					</CFIF>
				</CFIF>
	
				<!--- Could potentially generate 2 special edit error messages. Second is bad TLD: --->
				<CFSET Variables.LPI	= 0><!--- Last period index. --->
				<CFLOOP INDEX="Idx" FROM="#Variables.ServerIdx#" TO="#Variables.FLen#">
					<CFSET Variables.FChar						= Mid(Variables.FVal, Idx, 1)>
					<CFIF		Variables.FChar IS ".">
						<CFSET	Variables.LPI					= Idx>
					<CFELSEIF	Variables.FChar IS "/">
						<CFBREAK><!--- Inner loop only --->
					</CFIF>
				</CFLOOP>
				<CFSET Variables.TLD							= "(none)">
				<CFSET Variables.Okay							= "No">
				<CFSET Variables.OkayTLDArray					= ListToArray(Variables.OkayTLDs)>
				<CFIF Variables.LPI GT 0>
					<CFSET Variables.TLD						= Right(Variables.FVal, Variables.FLen - Variables.LPI)>
					<CFSET Variables.OkayTLDArrayLen			= ArrayLen(Variables.OkayTLDArray)>
					<CFLOOP INDEX="Idx" FROM="1" TO="#Variables.OkayTLDArrayLen#">
						<CFIF Variables.OkayTLDArray[Idx] IS Variables.TLD>
							<CFSET Variables.Okay				= "Yes">
							<CFBREAK><!--- Inner loop only --->
						</CFIF>
					</CFLOOP>
				</CFIF>
				<CFIF NOT Variables.Okay>
					<CFSET Variables.OkayTLDsPrettyList			= ArrayToList(Variables.OkayTLDArray, ", ")><!--- "Pretty" because of space after comma. --->
					<CFSET Caller.ErrMsg						= Caller.ErrMsg
																& "<LI>#Variables.HotFNam# has a 'Top Level Domain name' of '#Variables.TLD#'. "
																& "PRO-Net Internet addresses must end in a United States 'Top Level Domain name': "
																& "#Variables.OkayTLDsPrettyList#."
																& Variables.CRLF>
				</CFIF>

			</CFIF><!--- Type-specific special edits. --->

		</CFIF><!--- CFIF NOT Variables.ErrorInLoop --->

	</CFIF><!--- Len(FVal) is between Min and Max. --->

</CFLOOP>

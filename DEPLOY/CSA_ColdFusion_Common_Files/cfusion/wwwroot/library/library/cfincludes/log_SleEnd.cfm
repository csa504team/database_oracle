<!---
AUTHOR:				Steve Seaquist
DATE:				10/21/2005
DESCRIPTION:		Logs End-of-Request. 
NOTES:				Called by inc_starttickcount.cfm. 
INPUT:				Request.SBALogSystemName, maybe. Otherwise, none. 
OUTPUT:				If currently configured to log it, End-of-Request log entry ("SleEnd"). 
REVISION HISTORY:	10/21/2005, SRS:	Original implementation. 
--->

<cfif IsDefined("Request.SBALogSystemName")><!--- Request.SBALogSystemName is the master switch that turns on logging. --->
	<cfinclude template="get_SBALogEnvironment.cfm">
	<cfif Request.SleIsInfoEnabled><!--- Saves the effort of building the log entry if it won't be logged anyway. --->
		<cfif IsDefined("Request.SleTimeThisArray")>
			<cfloop index="SleIdx" from="1" to="#ArrayLen(Request.SleTimeThisArray)#">
				<cfif Request.SleTimeThisArray[SleIdx][5]>
					<cfset Variables.SleEntityName			= Request.SleTimeThisArray[SleIdx][1]>
					<cfset Variables.SleExecutionTime		= Request.SleTimeThisArray[SleIdx][4]>
					<cfset Request.SleLogger.info	("SleTimeTot"					& Request.SleHT
													& Request.SleScriptName			& Request.SleHT 
													& Request.SleUser				& RepeatString(Request.SleHT, 2)
													& Variables.SleEntityName		& Request.SleHT
													& Variables.SleExecutionTime	& Request.SleHT)>
				</cfif>
			</cfloop>
		</cfif>
		<cfset Variables.SleExecutionTime					= GetTickCount()		- Request.RequestStartTickCount>
		<!--- 3 tabs already exist in the PatternLayout of our log files. There must be 9 tabs, total. Therefore: --->
		<cfset Request.SleLogger.info	("SleEnd"									& Request.SleHT
										& Request.SleScriptName						& Request.SleHT 
										& Request.SleUser							& RepeatString(Request.SleHT, 3)
										& Variables.SleExecutionTime				& Request.SleHT)>
	</cfif>
</cfif>

<cfcomponent output="false" name="Application">

	<!---
	AUTHOR:				Steve Seaquist, Solutions By Design 2, Inc, for the US Small Business Administration.
	DATE:				08/11/2014.
	DESCRIPTION:		Shared routines for any system that extends this component. That includes CLS, but systems not
						secured by CLS may extend this component too. This includes systems secured by LDAP, such as SBA
						Supervisor. No login rejection occurs in this CFC, but get_sbalookandfeel_variables might reject .
	NOTES:				Until further notice, please, no one but Steve Seaquist should work on this file. If it's not right,
						all of our systems crash. It impacts too many other developers to be casually edited. Thanks a bunch.
	INPUT:				None.
	OUTPUT:				Application and Session control.
	REVISION HISTORY:	12/02/2015, SRS:	Changed many IP addresses. Since that's the sort of thing that's liable to be ongoing and 
											common, let's agree that simple IP address changes in the 6, 85, 86, 87 and 95 subnets 
											no longer require a full check-in/check-out in the future, okay? Let this be the last one. 
						06/03/2015 -
						08/05/2015, SRS:	Massive changes due to everyone moving to the Concourse level. Defined new server 
											based logic variable, "InternalEnvironment" (= "not visible outside the firewall"). 
											and added this.enableRobustException in that situation. 
						11/05/2014, NS:		Added my new IP. 
						08/11-15/2014, SRS:	Original implementation. 
	--->

	<!--- ******************* APPLICATION CONTROL ******************* --->

	<cfset this.name										= "SBA"><!--- Anonymous Apps disabled. "SBA" for now. --->
	<cfset this.applicationTimeout							= CreateTimeSpan(1, 0, 0, 0)><!--- 1 day --->
	<cfset this.debuggingIPAddresses						= "165.110.6.48,"	<!--- Asad --->
															& "165.110.6.15,"	<!--- Bhargavi --->
															& "165.110.6.39,"	<!--- Chitra --->
															& "165.110.6.33,"	<!--- Chuda --->
															& "165.110.6.70,"	<!--- DanDu --->
															& "165.110.6.32,"	<!--- Durga --->
															& "165.110.6.46,"	<!--- Haseefa --->
															& "165.110.6.8,"	<!--- Ian --->
															& "165.110.6.17,"	<!--- Ike M. --->
															& "165.110.87.114,"	<!--- Jame --->
															& "165.110.87.117,"	<!--- Jay --->
															& "165.110.6.11,"	<!--- Matt --->
															& "165.110.6.21,"	<!--- Mikhail --->
															& "165.110.6.6,"	<!--- Nelli --->
															& "165.110.87.155,"	<!--- Nirish --->
															& "165.110.6.44,"	<!--- Prathyusha --->
															& "165.110.6.66,"	<!--- Priya --->
															& "165.110.6.24,"	<!--- Rahul --->
															& "165.110.6.7,"	<!--- Robert --->
															& "165.110.85.112,"	<!--- Ron --->
															& "165.110.6.45,"	<!--- Rushan --->
															& "165.110.6.31,"	<!--- Selvin --->
															& "165.110.6.90,"	<!--- Sheen --->
															& "165.110.85.153,"	<!--- Sheri --->
															& "165.110.6.36,"	<!--- Sherry Liu --->
															& "165.110.6.27,"	<!--- Steve S. old --->
															& "165.110.6.100,"	<!--- Steve S. new --->
															& "165.110.6.40,"	<!--- Sushil --->
															& "165.110.87.97,"	<!--- Timalyn --->
															& "165.110.6.43"	<!--- Vidya --->
															><!--- Because "per app" setting, independent of "CLS" app. --->
	<cfset this.ListInternalServerNames						= "cadweb.sba.gov,catweb.sba.gov,"
															& "ca1anacostia.sba.gov,ca2rouge.sba.gov,ca2yukon.sba.gov">
	<cfset this.InternalEnvironment							= (ListFindNoCase(this.ListInternalServerNames,	CGI.Server_Name) gt 0)>
	<cfif this.InternalEnvironment><!--- Just us web devs. Not visible to lenders, cfo or disaster. --->
		<cfset this.enableRobustException					= true><!--- Unfortunately, this doesn't work. --->
	</cfif>
	<cfset this.scriptProtect								= true>
	<cfset this.sessionManagement							= "Yes">
	<cfset this.sessionTimeout								= CreateTimeSpan(0, 1, 0, 0)><!--- 1 hour --->
	<cfset this.setClientCookies							= false>

	<!--- ******************* SERVER-GLOBAL INSTANCE VARIABLES (DO NOT OVERRIDE) ******************* --->

	<cfset this.LibURL										= "/library">

	<!--- ******************* INSTANCE VARIABLES FOR THIS APPLICATION.CFC ******************* --->

	<cfset this.SBALogSystemName							= "Library"><!--- Our log4j appender name is still GLS. --->
	<cfset this.AppURL										= "/library">
	<cfset this.SysURL										= "/library">
	<cfset this.OnRequestStartInclude						= "none">
	<cfset this.WindowTitleBase								= "SBA - Library">

	<!--- ******************* SERVER-GLOBAL CUSTOM METHODS ******************* --->

	<cffunction name="DefineStandardSubdirNames"			returnType="void">
		<cfargument name="prefix"							type="String" required=true/>
		<cfargument name="value"							type="String" required=true/>
		<!---
		Note: The directories themselves may not exist. But if they do exist, these should be their names. In the meantime,
		if a directory uses a different name, it has to define it in its own code, so that non-standard names are known
		and documented. That way, people know which names to use and which to avoid.
		--->
		<cfset Variables["#Arguments.prefix#URL"]			= Arguments.value>
		<cfset Variables["#Arguments.prefix#CbURL"]			= "#Arguments.value#/callbacks">
		<cfset Variables["#Arguments.prefix#CbAjaxURL"]		= "#Arguments.value#/callbacks/ajax">
		<cfset Variables["#Arguments.prefix#ClassURL"]		= "#Arguments.value#/classes">
		<cfset Variables["#Arguments.prefix#CssURL"]		= "#Arguments.value#/css">
		<cfset Variables["#Arguments.prefix#CustomTagURL"]	= "#Arguments.value#/customtags">
		<cfset Variables["#Arguments.prefix#ImgURL"]		= "#Arguments.value#/images">
		<cfset Variables["#Arguments.prefix#IncURL"]		= "#Arguments.value#/cfincludes">
		<cfset Variables["#Arguments.prefix#JsURL"]			= "#Arguments.value#/javascripts">
		<cfset Variables["#Arguments.prefix#JsAjaxURL"]		= "#Arguments.value#/javascripts/ajax">
		<cfset Variables["#Arguments.prefix#JsJqURL"]		= "#Arguments.value#/javascripts/jquery"><!--- plug-ins too --->
		<cfset Variables["#Arguments.prefix#JsSlafURL"]		= "#Arguments.value#/javascripts/sbalookandfeel">
		<cfset Variables["#Arguments.prefix#UdfURL"]		= "#Arguments.value#/udf">
		<cfset Variables["#Arguments.prefix#WsURL"]			= "#Arguments.value#/ws">
	</cffunction>

	<!--- ******************* APPLICATION.CFC STANDARD METHODS (GENERALLY NOT OVERRIDDEN) ******************* --->

	<!--- No onApplicationStart yet. --->
	<!--- No onCFCRequest yet. --->
	<!--- No onError yet. --->
	<!--- No onMissingTemplate yet. --->
	<!--- No onSessionStart yet. --->
	<!--- No onRequestStart yet.	That gets added in /cls/Application.cfc. --->
	<!--- No onRequest yet.			That gets added in /cls/Application.cfc. --->
	<!--- No onRequestEnd yet.		That gets added in /cls/Application.cfc. --->
	<!--- No onSessionEnd yet. --->
	<!--- No onApplicationEnd yet. --->

</cfcomponent>


// Begin DumpObject.js
//
// Created by Steve Seaquist, Base Technologies, Inc, for the US Small Business Administration. 
//
// Inputs:				pObject				MANDATORY, object to be dumped. 
//						pName				Optional, name to represent the object in the dump. If not given, "x" will be 
//											used (to prevent "undefined"), representing "the unknown" (name). 
//						pMaxDisplay			Optional, number of properties to dump per confirm call. 
//						pSort				Optional, sort order, if any. Here are the allowable values: 
//											"text asc"			- case-sensitive   ascending (DEFAULT). 
//											"text desc"			- case-sensitive   descending
//											"textnocase asc"	- case-insensitive ascending
//											"textnocase desc"	- case-insensitive descending
//											"none"				- no sort. Display in order returned by sProp in pObject. 
// Outputs:
// Revision History:	10/10/2013, SRS:	Added pSort, defaulting to ascending case-sensitive ascending sort. Got rid of 
//											never-actually-used routines DumpLongString and DumpObjectAndChildNodes. They're 
//											still available in the checkedout subdirectory if needed. 
//						11/20/2007, SRS:	Added 3rd parameter to break up alerts in MSIE. (Other browsers allow an alert 
//											box to scroll.) Switched from alert to confirm so that user can abort the loop. 
//						Unknown, SRS:		Original implementation. Date unknown, but it had to be before the creation of 
//											DumpObjectTester.html (05/22/2002). Maybe around that time. Maybe much older. 

function DumpObject							(pObject, pName, pMaxDisplay, pSort)
	{
	var	i, sArrayIn, sArrayOut, sArrayOutLen, sCtr, sLast, sMaxDisplay, sName, sProp, sRange, sSort, sString;
	// Initializations:
	sSort									= (pSort		? pSort.toLowerCase()	: "text asc");
	switch (sSort)
		{
		case "none":
		case "text asc":
		case "text desc":
		case "textnocase asc":
		case "textnocase desc":
			break;
		default:
			sSort							= "text asc";
		}
	// Always build the array, even if sSort is "none". (It simplifies the code.) 
	sArrayIn								= [];
	switch (sSort)
		{
		case "textnocase asc":
		case "textnocase desc":
			for	(sProp in pObject)
				sArrayIn[sArrayIn.length]	= [sProp.toLowerCase(),	sProp];
			break;
		default:
			for	(sProp in pObject)
				sArrayIn[sArrayIn.length]	= [sProp,				sProp];
		}
	// Possible sort:
	switch (sSort)
		{
		case "text asc":
		case "textnocase asc":
			sArrayOut						= sArrayIn.sort(function (p1, p2) {return (p1[0] < p2[0]) ? -1 : 1});
			break;
		case "text desc":
		case "textnocase desc":
			sArrayOut						= sArrayIn.sort(function (p1, p2) {return (p1[0] > p2[0]) ? -1 : 1});
			break;
		default:
			sArrayOut						= sArrayIn;
		}
	// Display:
	sArrayOutLen							= sArrayOut.length;
	if	(sArrayOutLen == 0)
		{
		alert("No properties found in '" + sName + "'.");
		return;
		}
	sMaxDisplay								= (isNaN(pMaxDisplay)	? sArrayOutLen	: Math.floor(pMaxDisplay));
	sName									= (pName				? pName					: "x");
	sString									= "";
	for	(i = 0; i < sArrayOutLen; i++)					// i	is 0-based (better for calculations)
		{
		sCtr								= i + 1;	// sCtr	is 1-based (better for display)
		if	((i % sMaxDisplay) == 0)
			{
			if	(i > 0)
				{
				if	(!confirm(sString + "\n(more)"))
					return;
				}
			if	(pMaxDisplay && (sMaxDisplay < sArrayOutLen))
				{
				sLast						= ((Math.floor(i / sMaxDisplay) + 1) * sMaxDisplay);
				if	(sLast > sArrayOutLen)
					sLast					= sArrayOutLen;
				sRange						= "" + sCtr + "-" + sLast + " of "	+ sArrayOutLen;
				}
			else
				sRange						= "" + sCtr + "-"					+ sArrayOutLen;
			sString							= "Properties " + sRange + " in '"	+ sName + "':\n"
											+ "(Sort: " + sSort + ".)\n"
											+ "\n";
			}
		sProp								= sArrayOut[i][1];
		sString								+= sCtr + ": " + sName + "." + sProp + " = " + pObject[sProp] + "\n";
		}
	alert(sString + "\nEnd of properties.");
	}

// End DumpObject.js


/*
AUTHOR:				Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
DATE:				10/21/2010
DESCRIPTION:		JavaScripts for the SBA's new Left-Side Navigation Tree. 
NOTES:				None. 
INPUT:				None. 
OUTPUT:				JavaScripts for the SBA's new Left-Side Navigation Tree. 
REVISION HISTORY:	10/09/2015, SRS:	Reworked lots of stuff to be "themeable". For cf_sbatree, that meant changing 
										all of the image tags (with src) to divs with CSS classes that loaded the same 
										URL as the background-image. This allowed dynamically switching to Text-Only 
										mode, which is now body.th6. So, so, cool. 
					10/19-25/2010, SRS:	Original implementation. 
*/

// We cannot assume that jQuery is loaded. The previous incarnation of the navigation tree didn't rely on jQuery. 
// If we're in a frame, jQuery will probably not be defined (in the frame). So do things the old fashioned way, 
// which is also a TINY bit faster: 

var	gSBATreeDiv								= null;	// (Custom tag will set correct value.) 
var	gSBATreeTotalFolders					= 0;	// (Custom tag will set correct value.) 
function ExpandCollapse						(pContext)	// pContext = "Expand", "Collapse" or "Load". 
	{
	var	i									= null;
	var	sContents							= null;
	var	sExpand								= null;
	var	sFolder								= null;
	var	sPlusMinus							= null;
	var	sRoot								= false;
	for	(i = 1; i <= gSBATreeTotalFolders; i++)
		{
		sContents							= document.getElementById("sbatreecontents_"	+ i);
		sFolder								= document.getElementById("sbatreefolder_"		+ i);
		sPlusMinus							= document.getElementById("sbatreeplusminus_"	+ i);
		if	(!sContents)
			{
			alert("ERROR. Unable to find the contents of folder number "+pNumber+". "
				+ "(Should never happen.) "
				+ "Please report this error to the SBA's Office of Information Systems Support.");
			break;
			}
		switch (pContext)
			{
			case "Expand":		sExpand		= "Yes";	break;
			case "Collapse":	sExpand		= "No";		break;
			default:
				if	(!(sExpand				= sContents.getAttribute("data-expand")))// Note! Assignment! "=", not "=="!
					sExpand					= "Yes";
			}
		// Never close root elements automatically. Only ToggleContent(pNumber), below, can do that. 
		if	(sRoot							= sContents.getAttribute("data-root"))	// Note! Assignment! "=", not "=="!
			if	(sRoot == "Yes")
				sExpand						= "Yes";
		if	(sExpand == "No")
			{
			sContents.style.display			= "none";
			if	(sFolder)					// Graphics Mode: 
				{
				sFolder.className			= sFolder.className.replace(/folderopen/, "folderclosed");
				if	(sPlusMinus)			// Root directories don't have a preceding +/- in graphics mode. 
					sPlusMinus.className	= sPlusMinus.className.replace(/sbawm/, "sbawp");
				}
			else							// Text-Only Mode: 
				sPlusMinus.innerHTML		= "+";
			}
		else// Default to seeing the folder's contents: 
			{
			sContents.style.display			= "";
			if	(sFolder)					// Graphics Mode: 
				{
				sFolder.className			= sFolder.className.replace(/folderclosed/, "folderopen");
				if	(sPlusMinus)			// Root directories don't have a preceding +/- in graphics mode. 
					sPlusMinus.className	= sPlusMinus.className.replace(/sbawp/, "sbawm");
				}
			else							// Text-Only Mode: 
				sPlusMinus.innerHTML		= "-";
			}
		}
	FixMSIEDisplay();
	return true;
	}

function FixMSIEDisplay						()
	{
	// When folders are nested in MSIE 7 (and possibly other versions), the first item after the nested folder in the 
	// folder being toggled doesn't move up or down as it should. This is yet another MSIE workaround that somehow 
	// fixes the problem (and doesn't hurt other browsers). No way to sniff out the bug, so we do it in all browsers: 
	if	(gSBATreeDiv)						// If this didn't get loaded window.onload time, we can't do anything. 
		{
		gSBATreeDiv.style.display			= "none";
		gSBATreeDiv.style.display			= "";
		}
	}

var	gWindowOnLoadBeforeNavTree				= window.onload;
window.onload								= function()
	{
	gSBATreeDiv								= document.getElementById("sbatree");
	// If JavaScript is off, the ExpandCollapse line will remain display:none, which is good, because the ExpandCollapse 
	// line can't possibly work if Javascript is off. Instead, when JavaScript is off, everything is visible (expanded). 
	document.getElementById("sbatreeexpandcollapse").style.display	= "";
	// Set all folder contents to reflect their data-expand attributes. 
	if	(gSBATreeDiv)
		{
		var	sExpandAll						= gSBATreeDiv.getAttribute("data-expandall");
		if	(sExpandAll)
			{
			switch (sExpandAll)
				{
				case "Yes":					ExpandCollapse("Expand");	break;
				case "No":					ExpandCollapse("Collapse");	break;
				default:					ExpandCollapse("Auto");
				}
			}
		else
			ExpandCollapse("Auto");
		}
	// Do previously-scheduled onload, if any: 
	if	(gWindowOnLoadBeforeNavTree)
		gWindowOnLoadBeforeNavTree();
	}

function ToggleContent						(pNumber)// Used by folders' onclick methods. 
	{
	var	sContents							= document.getElementById("sbatreecontents_"	+ pNumber);
	var	sFolder								= document.getElementById("sbatreefolder_"		+ pNumber);
	var	sPlusMinus							= document.getElementById("sbatreeplusminus_"	+ pNumber);
	var	sStyle								= null;
	if	(!sContents)
		{
		alert("ERROR. Unable to find folder's contents. "
			+ "(Should never happen.) "
			+ "Please report this error to the SBA's Office of Information Systems Support.");
		return;
		}
	if	(sContents.style.display != "none")
		{
		sContents.style.display				= "none";
		if	(sFolder)						// Graphics Mode: 
			{
			sFolder.className				= sFolder.className.replace(/folderopen/, "folderclosed");
			if	(sPlusMinus)				// Root directories don't have a preceding +/- in graphics mode. 
				sPlusMinus.className		= sPlusMinus.className.replace(/sbawm/, "sbawp");
			}
		else								// Text-Only Mode: 
			sPlusMinus.innerHTML			= "+"
		}
	else// Default to seeing the folder's contents: 
		{
		sContents.style.display				= "";
		if	(sFolder)						// Graphics Mode: 
			{
			sFolder.className				= sFolder.className.replace(/folderclosed/, "folderopen");
			if	(sPlusMinus)				// Root directories don't have a preceding +/- in graphics mode. 
				sPlusMinus.className		= sPlusMinus.className.replace(/sbawp/, "sbawm");
			}
		else								// Text-Only Mode: 
			sPlusMinus.innerHTML			= "-"
		}
	FixMSIEDisplay();
	}

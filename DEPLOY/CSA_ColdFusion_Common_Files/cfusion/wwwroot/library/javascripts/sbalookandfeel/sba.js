
// Begin sba.js (in case cfincluded into a page, which would normally never be done). 
// Created by Steve Seaquist, 10/07/2015. 
//
// NOTE:				Originally created to support change of themes, both in top and in frames. 
//						Included automatically by get_sbalookandfeel_topofhead. Because we could be 
//						in a frame and jQuery isn't required in frames (unless the developer wants it), 
//						***** DO NOT ASSUME THAT JQUERY IS AVAILABLE IN THIS FILE *****. 
//
// ALSO NOTE:			This will become a convenient place to load shared routines we always want to have in JavaScript. 
//
// Revision History:	10/07/2015, SRS:	Original implementation. 

// SlafListAppend is like CF's ListAppend. Also used for cross-browser className maintenance (not assuming classList exists). 

function SlafListAppend										(pList, pValue, pDelimiterIfNotComma)
	{
	if	(pList === "")
		return pValue;
	return pList + ((arguments.length < 3) ? "," : pDelimiterIfNotComma) + pValue;
	}

// This frame may or may not include jQuery, so don't assume it exists for $(document).ready(). Use the following instead: 

function SlafQueueOnReadyStateChange						(pFunc)
	{
	var	sCallerDefinedORSC, sDone;
	sCallerDefinedORSC										= document.onreadystatechange; // Most likely, this will be null. 
	// It's typical and normal for document.readyState to change multiple times ("loading", then "interactive", 
	// then "complete"). But typically pFunc would only ever want to process at the change to "interactive". 
	// If we went through all of the riggamarole that jQuery does with regard to document.addEventListener 
	// (or window.attachEvent in MSIE), we would have to also call document.removeEventListener (or 
	// window.detachEvent), as jQuery does. Instead, give pFunc a variable it can test to decide whether or not 
	// it has already done whatever it needs to do (closure variable, okay set it to true in pFunc when done): 
	sDone													= false;
	document.onreadystatechange								= function ()
		{
		if	(sCallerDefinedORSC)							// Was there already an onreadystatechange handler? 
			sCallerDefinedORSC();							// Yes: Execute queued functions in the order they were queued. 
		// Functions queued with this outer function get protection against a bug in MSIE. jQuery's comment 
		// said: "Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443)." 
		if	(document.body)									// Here we don't mean it in the sense of shared ancestor for CSS, so don't use kSharedAncestor. 
			pFunc();
		else
			setTimeout(pFunc, 1);
		};
	}

// We do cross-browser className maintenance in the following. (We can't assume that classList exists in all browsers we support.) 

function SlafSetSharedAncestorClass							(pPreRemoveList, pClassName)
	{
	// Note: "k" prefix means Konstant (German) in scope notation. DO NOT MOVE TO GLOBAL SCOPE, because 
	// document.body will almost certainly not exist at the time this script file is included. 
	var kSharedAncestor										= document.body;
	// We may want to change which ancestor element controls themeability, fonts, editors, etc. For now, it's body. 
	var	i, j, sNewClassString, sNotInPreRemoveArray, sOldClass, sOldClassArray, sPreRemoveArray;
	if	(SlafSetSharedAncestorClass.fDebugAlerts)
		alert("Before: '" + kSharedAncestor.className + "'.");
	// MSIE 9 doesn't support .classList, which is an array-like DOMTokenList. Also, sOldClassArray is more readable. 
	sOldClassArray											= kSharedAncestor.className.split(" ");// Space, because it's a className. 
	sNewClassString											= "";
	sPreRemoveArray											= pPreRemoveList.split(",");// Comma, because it's a CSV list from CF. 
	for	(i = 0; i < sOldClassArray.length; i++)
		{
		sOldClass											= sOldClassArray[i];
		if	(sOldClass === "")								// Did adjacent spaces cause a nullsting artifact? 
			continue;										// Yes: Ignore it. 
		sNotInPreRemoveArray								= true;
		for	(j = 0; j < sPreRemoveArray.length; j++)
			if	(sPreRemoveArray[j] === sOldClass)
				{
				sNotInPreRemoveArray						= false;
				break;										// No need to check the rest of sPreRemoveArray. 
				}
		if	(sNotInPreRemoveArray)
			sNewClassString									= SlafListAppend(sNewClassString, sOldClass,	" ");
		}
	// Note: Passing "" in pClassName is ALLOWED! It means "use the default". So don't SlafListAppend "". 
	if	(pClassName !== "")
		sNewClassString										= SlafListAppend(sNewClassString, pClassName,	" ");
	kSharedAncestor.className								= sNewClassString;	// Set new classNames for this frame's kSharedAncestor. 
	if	(SlafSetSharedAncestorClass.fDebugAlerts)
		alert("After: '" + kSharedAncestor.className + "'.");
	}
SlafSetSharedAncestorClass.fDebugAlerts						= false;// Treat function as object. "f" prefix indicates property. 

// End sba.js (in case cfincluded into a page, which would normally never be done). 

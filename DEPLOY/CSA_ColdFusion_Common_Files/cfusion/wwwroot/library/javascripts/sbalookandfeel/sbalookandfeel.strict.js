
// Begin sbalookandfeel.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 11/16/2006
//
// NOTE:				The next usage of jQuery should probably be replacing all /library references to window.onload 
//						with a $(document).ready function that binds $(window).load (for automatic queuing). This would 
//						allow us to ease body.onload and window.onload restrictions. That'll have to be done cautiously, 
//						so it's not part of the 12/09/2008 release. When the $(window).load logic is in place, move this 
//						explanation into the Revision History. 
//
// Revision History:	11/07/2016, SRS:	Bug fix: allowScrollingOnMobileSafari class for 4 divs that might contain iFrames. 
//						10/07/2015, SRS:	Reworked lots of stuff to be "themeable" and simpler because we no longer have 
//											to support MSIE 8: Got rid of files for quirks mode, which we haven't generated 
//											in over 4 years, to relieve that maintenance burden. 
//						05/23/2014, SRS:	Added SlafCenterInAppDataClass. See also sba.css (style .CenterThisInAppData). 
//						10/16/2013, SRS:	Added CachedAsOf to printer_friendly.html reference. 
//						02/26/2013, SRS:	Added ability to expand and contract AppNav, if it exists. Had to integrate 
//											with SlafToggleAppData or else the SliderControl gets obscured. Added SlafTrace. 
//						02/14/2013, SRS:	Added ability to sync up Session Timeout countdown timer across multiple windows. 
//						03/29/2012, SRS:	Added SlafToggleEnvelope, a new routine to Show/Hide the entire DivEnvelope, 
//											so that OISS developers can see ColdFusion debugging listings that may lie 
//											underneath. (The custom tag adds a call to it in the SlafMenu, maybe.) 
//						08/05/2011, SRS:	Cannibalized sbalookandfeel.js (quirks mode only version) to create 
//											sbalookandfeel2.js (strict mode only version). Will do parallel maintenance 
//											of these 2 files till we switch over to strict mode permanently. 
//						12/01/2010, SRS:	Reverse the effects of ShowUntilAllFullyLoaded and HideUntilAllFullyLoaded 
//											when the page is fully loaded. (Not too many using those classes, because this 
//											is actually a MAJOR bug that managed to go unnoticed for quite some time.) 
//											New function SlafCenterInAppData. See also sba.css (style #CenterThisInAppData). 
//						09/21/2010, SRS:	New look-and-feel is now stable, so moved JS from cf_sbalookandfeel to here. 
//						07/20/2010, SRS:	"New Look-and-Feel": AppInfo is now width:100%, so we no longer resize it. 
//											Also made AppTimeoutDangerZone a className in sba.ccs, so used it here. 
//						01/27/2010, SRS:	Added support for "AppTimeout" (setInterval-based display of approximate time 
//											till session timeout). Made it reset on frame reloads. 
//						01/22/2010, SRS:	We're sometimes throwing an error at document unload time if cf_sbalookandfeel 
//											was called with ReadyLight="No", because neither top.SetReadyLightToLoading nor 
//											top.MainNav.SetReadyLightToLoading is defined. Added test to prevent that. 
//											
//						07/10/2009, SRS:	Now that the cf_sbalookandfeel custom tag is queuing DoThisOnLoad using 
//											$(document).ready(), added a new function DoThisIfAllFullyLoaded. In addition 
//											to setting the ReadyLight, it also clears "UntilAllFullyLoaded" classes. 
//						05/29/2009, SRS:	Since cf_sbalookandfeel now uses $(window).resize(SlafDoThisOnResize), and 
//											since jQuery passes the event object to event handlers, had to set debugging 
//											flag using top.SlafDebug. Added SlafSetFrameLoaded (part of automatic support 
//											for ReadyLight that was added to cf_sbalookandfeel). 
//						03/19/2009, SRS:	Added gDivHighlightCursor and SlafHighlightCursor(pTurnHighlightingOn). 
//						12/09/2008, SRS:	Better feedback to users about margins via SlafMarginHi and SlafMarginLo. 
//											Animated SlafToggleAppData using jQuery's animate function. (Wow factor.) 
//											This is the first jQuery usage actually inside sbalookandfeel.js. As a 
//											result, we are now intrinsically committed to jQuery, and can henceforth 
//											use it more freely. (Fortunately, jQuery is awesome.) See NOTE, above. 
//						10/02/2007, SRS:	Forced the creation of a new window whenever the user requests a pop up 
//											such as Print or Help. (See references to sWindIntName, below.) 
//						02/23/2007, SRS:	Added SlafMenu code, which is currently just a prototype to try it out as 
//											an interface option. Per Ron Whalen, modified SlafPopUpRegion to accept 
//											AppInfoAndAppData (or logical synonym Print), for use in DoPrint scripts. 
//											Also per Ron, made BotMost participate in SlafToggleAppData. 
//						11/16/2006, SRS:	Original implementation, as part of CSS-P version of SBA look-and-feel. 
//
// This JavaScript should be called ONLY by cf_sbalookandfeel. If any other frame or inline region needs these 
// variables or functions, they can always reference them as top.whatever. Therefore, it's okay to force the 
// caller to the topmost window frame: 

if	(self != top.self)						// Prevents accidentally nested frames. 
	top.location.href						= self.location.href;

// Configuration Parameters:

kAnimationMilliseconds						= 1000;
kServerSessionTimeoutInSeconds				= 3600;	// One hour. 
kSliderTooltipContract						= "Click to contract.";
kSliderTooltipExpand						= "Click to expand.";

// Globals to save on calls to document.getElementById. Not using "var" to highlight that they're intentionally global:

gAllGlobalsInitialized						= false;	// ... until SlafInitGlobals() is called. 
g$DivAppData								= null;
g$DivAppNav									= null;
gDivAppData									= null;
gDivAppDataTop								= 0;		// Used by SlafToggleAppData. Defined in standard order for CSS. 
gDivAppDataRight							= 0;		// Used by SlafToggleAppData. 
gDivAppDataBottom							= 0;		// Used by SlafToggleAppData. 
gDivAppDataLeft								= 0;		// Used by SlafToggleAppData. 
gDivAppInfo									= null;
gDivAppName									= null;
gDivAppNameText								= null;
gDivAppNav									= null;
gDivAppNavIsExpanded						= false;
gDivBotMost									= null;
gDivCenterThisInAppData						= null;
gDivCmdHelp									= null;
gDivCmdSettings								= null;
gDivEnvelope								= null;
gDivMainNav									= null;
gDivMarginT									= null;	// Standard CSS order = top, right, bottom, left. 
gDivMarginR									= null;	// Standard CSS order = top, right, bottom, left. 
gDivMarginB									= null;	// Standard CSS order = top, right, bottom, left. 
gDivMarginL									= null;	// Standard CSS order = top, right, bottom, left. 
gDivSBALogo									= null;
gDivSliderControl							= null;
gDivWindow									= null;
gDspAppInfo									= "";	// Optional region. This will be "" or "none". 
gDspAppNav									= "";	// Optional region. This will be "" or "none". 
gFrameIsFullyLoadedAppData					= false;
gFrameIsFullyLoadedAppInfo					= true; // Per Ron Whalen, no longer affects ReadyLight. 
gFrameIsFullyLoadedAppNav					= false;
gFrameIsFullyLoadedMainNav					= false;
gFrmAppData									= null;	// gFrm vars not used, provided for backwards compatability. 
gFrmAppHidden								= null;	// gFrm vars not used, provided for backwards compatability. 
gFrmAppInfo									= null;	// gFrm vars not used, provided for backwards compatability. 
gFrmAppNav									= null;	// gFrm vars not used, provided for backwards compatability. 
gFrmMainNav									= null;	// gFrm vars not used, provided for backwards compatability. 
gLastPopUp									= null;	// Set by most recent call to SlafPopUpRegion. 
gLastPopUpRegion							= "";	// Set by most recent call to SlafPopUpRegion. 
gPageLoadTime								= new Date();
gPageLoadTimeMS								= gPageLoadTime.getTime();// Milliseconds since 01/01/1970. 
document.cookie								= "PageLoadTime="
											+ gPageLoadTimeMS
											+ "; path=/";// Allows sync across multiple windows to same login. 
// Initialize all gRefs to top.self for now. That's because this script is included in the head, before the 
// screen regions have been defined. They won't be defined for-sure until SlafDoThisOnLoad, which sets their 
// true values. Note that, unlike gFrm vars, these are always usable, even when the region is not a frame: 
gRefAppData									= top.self;
gRefAppHidden								= top.self;
gRefAppInfo									= top.self;
gRefAppNav									= top.self;
gRefMainNav									= top.self;
gSlafShowAppTimeoutIntervalMS				= 60000;	// 60,000 milliseconds = once a minute.
gSlafShowAppTimeoutNowID					= null;
gSlafShowAppTimeoutRef						= null;
gSlafTraceIsOn								= false;
if	(location.search.indexOf("SlafTrace") > -1)
	gSlafTraceIsOn							= true;
gSlafTraceShowsTimestamp					= false;
gSlafTraceData								= "";
gThisPageIsFullyLoaded						= false;

// Functions, in aphabetical order: 

function AutoResize							(pToggleTextOnly)		// For compatability with old look-and-feel
	{
	if	(pToggleTextOnly)
		SlafToggleTextOnly					();
	}

function DoThisIfAllFullyLoaded				()
	{
	// Called by SetReadyIfAllFullyLoaded, which waits until top and all frames have all hit the window.load event. 
	// In a frame, include sbalookandfeelframe.js instead, which waits only until $(window).load() to do the same things. 
	// First remove all "UntilAllFullyLoaded" classes (defined in sba.css and noscript.css). Must do this before setting the 
	// ReadyLight to Ready, because these affect the actual usability of the page. (SetReadyLightToReady is more likely to 
	// error than these jQuery commands. If SetReadyLightToReady has problems, we need to leave the page usable, at least. 
	// Relatively speaking, it's okay to have "Loading" remain on the screen. But it's NOT okay to leave the page unusable.) 

	$(".HideUntilAllFullyLoadedBlock")		.removeClass("HideUntilAllFullyLoadedBlock")	.css({display:"block"});
	$(".HideUntilAllFullyLoadedInline")		.removeClass("HideUntilAllFullyLoadedInline")	.css({display:"inline"});
	// gSlafInlineBlock *SHOULD* have been defined by custom tags or other inclusions, but don't error if it wasn't: 
	var	sInlineBlock						= (gSlafInlineBlock		? gSlafInlineBlock
											: (top.gSlafInlineBlock	? top.gSlafInlineBlock
											: "inline-block"));// Should never get this far. 
	var	sInlineTable						= (gSlafInlineTable		? gSlafInlineTable
											: (top.gSlafInlineTable	? top.gSlafInlineTable
											: "inline-table"));// Should never get this far. 
	$(".HideUntilAllFullyLoadedInlBlk")		.removeClass("HideUntilAllFullyLoadedInlBlk")	.css({display:sInlineBlock});
	$(".HideUntilAllFullyLoadedInlTbl")		.removeClass("HideUntilAllFullyLoadedInlTbl")	.css({display:sInlineTable});
	$(".HideUntilAllFullyLoadedTblRowGrp")	.removeClass("HideUntilAllFullyLoadedTblRowGrp").css({display:"table-row-group"});
	$(".HideUntilAllFullyLoadedTblRow")		.removeClass("HideUntilAllFullyLoadedTblRow")	.css({display:"table-row"});
	$(".ShowUntilAllFullyLoadedBlock")		.removeClass("ShowUntilAllFullyLoadedBlock")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInline")		.removeClass("ShowUntilAllFullyLoadedInline")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInlBlk")		.removeClass("ShowUntilAllFullyLoadedInlBlk")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedInlTbl")		.removeClass("ShowUntilAllFullyLoadedInlTbl")	.addClass("hide");
	$(".ShowUntilAllFullyLoadedTblRowGrp")	.removeClass("ShowUntilAllFullyLoadedTblRowGrp").addClass("hide");
	$(".ShowUntilAllFullyLoadedTblRow")		.removeClass("ShowUntilAllFullyLoadedTblRow")	.addClass("hide");
	if	(top.MainNav)
		{
		if	(top.MainNav.SetReadyLightToReady)			// In case (cf_sbalookandfeel ReadyLight="No"). 
			top.MainNav.SetReadyLightToReady();
		}
	else
		{
		if	(top.SetReadyLightToReady)					// In case (cf_sbalookandfeel ReadyLight="No"). 
			top.SetReadyLightToReady();
		}
	SlafTrace("DoThisIfAllFullyLoaded done.");
	}

function DoThisOnLoad						()			// No longer used, provided for backwards compatability. 
	{
	SlafDoThisOnLoad();
	}

function SetReadyIfAllFullyLoaded			()
	{
	if	(top.gFrameIsFullyLoadedAppData
	&&	top.gFrameIsFullyLoadedAppNav
	&&	top.gFrameIsFullyLoadedMainNav)
		DoThisIfAllFullyLoaded();
	}

function SlafCenterInAppData				()
	{
	if	(!top.AppData)// Make sure this routine is used only when AppData is inline. Otherwise, this routine does nothing. 
		{
		var	sDiv							= $(gDivCenterThisInAppData);
		var	sWin							= $(gDivAppData);// sbalookandfeelframe.js uses $(window). 
		var	sDivHeight						= sDiv.height();
		var	sDivWidth						= sDiv.width();
		var	sWinHeight						= sWin.height();
		var	sWinWidth						= sWin.width();
		var	sLeft							= Math.floor((sWinWidth		- sDivWidth)	/ 2);
		var	sTop							= Math.floor((sWinHeight	- sDivHeight)	/ 2);
		sDiv.css({left:sLeft,top:sTop});
		}
	SlafTrace("SlafCenterInAppData done.");
	}

function SlafCenterInAppDataClass			(pDiv)// Same as SlafCenterInAppData, but allows more than one in AppData. 
	{// See /cls/dsp_login.cfm for an example of how to use 2 centered divs (show/hide, but both centered). 
	if	(!top.AppData)// Make sure this routine is used only when AppData is inline. Otherwise, this routine does nothing. 
		{
		var	sWin							= $(gDivAppData);// sbalookandfeelframe.js uses $(window). 
		var	sDiv							= $(pDiv);
		var	sDivHeight						= sDiv.height();
		var	sDivWidth						= sDiv.width();
		var	sWinHeight						= sWin.height();
		var	sWinWidth						= sWin.width();
		var	sLeft							= Math.floor((sWinWidth		- sDivWidth)	/ 2);
		var	sTop							= Math.floor((sWinHeight	- sDivHeight)	/ 2);
		sDiv.css({left:sLeft,top:sTop});
		}
	SlafTrace("SlafCenterInAppDataClass done.");
	}

function SlafDoThisOnLoad					()			// Actually done at $(document).ready() time. See also DoThisOnWindowLoad. 
	{
	SlafInitGlobals();						// Only situation where SlafInitGlobals is executed unconditionally. 
	SlafShowAppTimeoutInit();				// May not do anything if document.getElementById("AppTimeout") is null. 
	$("div[id^=DivMargin]")					.hover(SlafMarginHi, SlafMarginLo);// Lets users know that the margin is a control. 
	// Avoid adding allowScrollingOnMobileSafari class if browser doesn't have the bug in question: 
	if	(						document.documentElement	// (existence test)
		&&	("ontouchstart" in	document.documentElement)	// Limit addClass to mobile browsers ... 
		&&	(/iPad|iPhone|iPod/g.test(navigator.userAgent))	// that SAY they're Mobile Safari ... 
		&&	(!window.MSStream)								// and aren't MSIE lying to GMail. 
		)
		{
		$("#DivMainNav>iframe,#DivAppInfo>iframe,#DivAppNav>iframe,#DivAppData>iframe")
			.parent() // selector's > selects the iframe, but we want to add class to parent #Div. 
				.addClass("allowScrollingOnMobileSafari");
		}
	// Do a "vertical-align:middle" that works across all browsers:
	var	s$DivAppNameText					= $(gDivAppNameText);// Don't rebuild jQuery object twice. 
	var	sTopForMiddleAlignment				= 43 - Math.floor(s$DivAppNameText.height() / 2);
	s$DivAppNameText.css({top:sTopForMiddleAlignment});
	SlafDoThisOnResize();
	SetReadyIfAllFullyLoaded();
	}

function SlafDoThisOnResize					()
	{
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	if	(gDivCenterThisInAppData)
		SlafCenterInAppData();
	$(".CenterThisInAppData").each(function()
		{
		SlafCenterInAppDataClass(this);
		});
	}

$(window).resize(SlafDoThisOnResize);

// The "DoSomethingDifferentOnLoad" mechanism is now deprecated. Instead, you should convert your onload routine 
// to use $(window).load or $(document).ready instead. Otherwise, you'll be redundantly calling MainNavDoThisOnLoad 
// and SlafDoThisOnLoad. (Doesn't hurt anything, but it's a waste of time and CPU.) This keeps old code running: 

$(window).load(function()					// Formerly DoThisOnWindowLoad. 
	{
	if	(top.DoSomethingDifferentOnLoad)	// Possibly defined by developer in JSInline. 
		top.DoSomethingDifferentOnLoad();
	});

function SlafInitGlobals					()
	{
	gDivAppData								= document.getElementById("DivAppData");
	gDivAppInfo								= document.getElementById("DivAppInfo");
	gDivAppName								= document.getElementById("DivAppName");
	gDivAppNameText							= document.getElementById("DivAppNameText");
	gDivAppNav								= document.getElementById("DivAppNav");
	gDivBotMost								= document.getElementById("DivBotMost");
	gDivCenterThisInAppData					= document.getElementById("CenterThisInAppData");// Probably null;
	gDivCmdHelp								= document.getElementById("DivCmdHelp");
	gDivCmdSettings							= document.getElementById("DivCmdSettings");
	gDivEnvelope							= document.getElementById("DivEnvelope");
	gDivMainNav								= document.getElementById("DivMainNav");
	gDivMarginT								= document.getElementById("DivMarginT");// Standard CSS order = top, right, bottom, left. 
	gDivMarginR								= document.getElementById("DivMarginR");// Standard CSS order = top, right, bottom, left. 
	gDivMarginB								= document.getElementById("DivMarginB");// Standard CSS order = top, right, bottom, left. 
	gDivMarginL								= document.getElementById("DivMarginL");// Standard CSS order = top, right, bottom, left. 
	gDivSBALogo								= document.getElementById("DivSBALogo");
	gDivSliderControl						= document.getElementById("DivSliderControl");
	gDivWindow								= document.getElementById("DivWindow");
	gDspAppInfo								= (gDivAppInfo	? gDivAppInfo.style.display	: "");
	gDspAppNav								= (gDivAppNav	? gDivAppNav.style.display	: "");
	if	(top.AppData)	gFrmAppData			= gRefAppData	= top.AppData;	else gFrameIsFullyLoadedAppData	= true;
						gFrmAppHidden		= gRefAppHidden	= top.AppHidden;	// always a frame
	if	(top.AppInfo)	gFrmAppInfo			= gRefAppInfo	= top.AppInfo;	else gFrameIsFullyLoadedAppInfo	= true;
	if	(top.AppNav)	gFrmAppNav			= gRefAppNav	= top.AppNav;	else gFrameIsFullyLoadedAppNav	= true;
	if	(top.MainNav)	gFrmMainNav			= gRefMainNav	= top.MainNav;	else gFrameIsFullyLoadedMainNav	= true;
	gThisPageIsFullyLoaded					= true;
	gAllGlobalsInitialized					= true;
	SlafTrace("SlafInitGlobals done.");
	}

function SlafMarginHi						()				// Highlight margin so that users realize that it's a control. 
	{
	var	sHighlightColor;
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	switch (top.gSlafThemeName)
		{
		case "th2":		sHighlightColor		= "#c1e3ed";	break;	// Azure
		case "th3":		sHighlightColor		= "#fac";		break;	// Pink
		case "th4":		sHighlightColor		= "#afc";		break;	// Green
		case "th5":		sHighlightColor		= "#c1e3ed";	break;	// Azure
		default:		sHighlightColor		= "#acf";				// Baby blue
		}
	gDivMarginT.style.backgroundColor		=
	gDivMarginR.style.backgroundColor		=
	gDivMarginB.style.backgroundColor		=
	gDivMarginL.style.backgroundColor		= sHighlightColor;
	}

function SlafMarginLo						()				// Lowlight margin so that users realize that it's a control. 
	{
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	gDivMarginT.style.backgroundColor		=
	gDivMarginR.style.backgroundColor		=
	gDivMarginB.style.backgroundColor		=
	gDivMarginL.style.backgroundColor		= "#fff";	// White. 
	}

function SlafPopUpRegion					(pRegion)	// pRegion is a string: "AppData", "AppNav", etc.
	{
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	var	sWindow								= null;
	var	sWindIntName						= "PoputWindowCreatedAtMilliseconds" + (new Date()).getTime();
	switch (pRegion)
		{
		case "AppData":
		case "AppHidden":
		case "AppInfo":
		case "AppInfoAndAppData":
		case "AppName":
		case "AppNav":
		case "BotMost":
		case "MainNav":
		case "Print":						// logical synonym for AppInfoAndAppData
		case "SBALogo":
			break;
		default:
			alert("SlafPopUpRegion called with unknown region \"" + pRegion + "\".\n\n"
				+ "Allowable region names are \"AppData\", \"AppHidden\", \"AppInfo\", \"AppInfoAndAppData\", "
				+ "\"AppName\", \"AppNav\", \"BotMost\", \"MainNav\", \"Print\" or \"SBALogo\".");
			return;
		}
	gLastPopUpRegion						= pRegion;
	if	(eval("top."+pRegion))				// That is, if it's a frame:
		sWindow								= window.open(eval("top."+pRegion+".location.href"),
											sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
	else									// That is, if it's inline or a non-region name that's allowed:
		sWindow								= window.open("/library/html/pop_up_region.html",
											sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
	gLastPopUp								= sWindow;	// Allows caller to manipulate popup. 
	}

function SlafSetReadyLightToLoading			()
	{
	if	(top.MainNav)
		{
		if	(top.MainNav.SetReadyLightToLoading)		// In case (cf_sbalookandfeel ReadyLight="No"). 
			top.MainNav.SetReadyLightToLoading();
		}
	else
		{
		if	(top.SetReadyLightToLoading)				// In case (cf_sbalookandfeel ReadyLight="No"). 
			top.SetReadyLightToLoading();
		}
	SlafTrace("SlafSetReadyLightToLoading done.");
	}

function SlafSetFrameLoaded					(pFrame)
	{
	var	sFrameName							= "unknown";
	if	(pFrame)							// Don't error on unexpected conditions. (Just do nothing.) 
		{
		if	(pFrame.name)					// Don't error on unexpected conditions. (Just do nothing.) 
			sFrameName						= pFrame.name;
		else if	(pFrame.id)					// Don't error on unexpected conditions. (Just do nothing.) 
			sFrameName						= pFrame.id;
		switch (sFrameName)					// Don't error on unexpected conditions. (Just do nothing.) 
			{
			case "AppData":	case "FrmAppData":	top.gFrameIsFullyLoadedAppData	= true; SetReadyIfAllFullyLoaded(); break;
			case "AppNav":	case "FrmAppNav":	top.gFrameIsFullyLoadedAppNav	= true; SetReadyIfAllFullyLoaded(); break;
			case "MainNav":	case "FrmMainNav":	top.gFrameIsFullyLoadedMainNav	= true; SetReadyIfAllFullyLoaded(); break;
			default:
				sFrameName					= "Unknown: '"+sFrameName+"'";
			}
		}
	// On every frame load, reset gPageLoadTime and gPageLoadTimeMS, in case timeout countdown is being displayed: 
	gPageLoadTime							= new Date();
	gPageLoadTimeMS							= gPageLoadTime.getTime();// Milliseconds since 01/01/1970. 
	document.cookie							= "PageLoadTime="
											+ gPageLoadTimeMS
											+ "; path=/";// Allows sync across multiple windows to same login. 
	if	(gSlafShowAppTimeoutRef != null)
		{
		if	(gSlafShowAppTimeoutIntervalMS	== 1000)// If once a second, fall back to once a minute: 
			{
			if	(gSlafShowAppTimeoutNowID)
				{
				window.clearInterval(gSlafShowAppTimeoutNowID);
				gSlafShowAppTimeoutNowID	= null;
				}
			gSlafShowAppTimeoutIntervalMS	= 60000;
			gSlafShowAppTimeoutNowID		= window.setInterval(SlafShowAppTimeoutNow, gSlafShowAppTimeoutIntervalMS);
			}
		gSlafShowAppTimeoutRef.className	= "";	// Removes AppTimeoutDangerZone if it was there. 
		SlafShowAppTimeoutNow();
		}
	SlafTrace("SlafSetFrameLoaded (" + sFrameName + ").");
	}

function SlafSetFrameLoading				(pFrameName)
	{
	switch (pFrameName)						// Don't error on unexpected conditions. (Just do nothing.) 
		{
		case "AppData":						top.gFrameIsFullyLoadedAppData	= false; break;
		case "AppNav":						top.gFrameIsFullyLoadedAppNav	= false; break;
		case "MainNav":						top.gFrameIsFullyLoadedMainNav	= false; break;
		}
	SlafSetReadyLightToLoading(); 
	SlafTrace("SlafSetFrameLoading (" + pFrameName + ").");
	}

function SlafShowAppTimeoutInit				()
	{
	gSlafShowAppTimeoutRef					= document.getElementById("AppTimeout");
	if	(gSlafShowAppTimeoutRef == null)
		return;								// If AppTimeout isn't defined, SlafShowAppTimeoutNow never gets executed. 
	gSlafShowAppTimeoutNowID				= window.setInterval(SlafShowAppTimeoutNow, gSlafShowAppTimeoutIntervalMS);
	SlafShowAppTimeoutNow();
	}

function SlafShowAppTimeoutNow				()
	{
	if	(gSlafShowAppTimeoutRef == null)	// Don't throw errors if executed in some nonstandard way. 
		{
		if	(gSlafShowAppTimeoutNowID)
			{
			window.clearInterval(gSlafShowAppTimeoutNowID);
			gSlafShowAppTimeoutNowID		= null;
			}
		return;
		}
	var	sParse								= /.*PageLoadTime=(\d+).*/.exec(document.cookie);
	if	(sParse)
		{
		var	sPageLoadTimeMS					= sParse[1];
		if	(sPageLoadTimeMS > gPageLoadTimeMS)
			gPageLoadTimeMS					= sPageLoadTimeMS;
		}
	var	sMillisecondsSincePageLoadTimeMS	= (new Date()).getTime()
											- gPageLoadTimeMS;
	var	sSecondsSincePageLoadTime			= sMillisecondsSincePageLoadTimeMS / 1000;
	var	sSecondsToSessionTimeout			= kServerSessionTimeoutInSeconds
											- sSecondsSincePageLoadTime;
	if	(sSecondsToSessionTimeout < 0)
		{
		gSlafShowAppTimeoutRef.innerHTML	= "Session probably timed out. ";
		if	(gSlafShowAppTimeoutNowID)
			{
			window.clearInterval(gSlafShowAppTimeoutNowID);
			gSlafShowAppTimeoutNowID		= null;
			}
		return;
		}
	var	sMinutesToSessionTimeout			= Math.floor(sSecondsToSessionTimeout / 60);
	if	(sMinutesToSessionTimeout > 5)
		{
		gSlafShowAppTimeoutRef.innerHTML	= "Session timeout in "
											+ sMinutesToSessionTimeout
											+ " minutes. ";
		return;
		}
	if	(gSlafShowAppTimeoutIntervalMS		== 60000)	// Initialized with 60000 milliseconds = once a minute.
		{
		if	(gSlafShowAppTimeoutNowID)		// Don't do it once a minute anymore. 
			{
			window.clearInterval(gSlafShowAppTimeoutNowID);
			gSlafShowAppTimeoutNowID		= null;
			}
		gSlafShowAppTimeoutIntervalMS		= 1000;		// Instead, switch to 1000 milliseconds = once a second.
		gSlafShowAppTimeoutNowID			= window.setInterval(SlafShowAppTimeoutNow, gSlafShowAppTimeoutIntervalMS);
		gSlafShowAppTimeoutRef.className	= "AppTimeoutDangerZone";	// Brick red, bold. 
		}
	var	sSecondsAfterMinutes				= Math.floor(sSecondsToSessionTimeout - (sMinutesToSessionTimeout * 60));
	if	(sSecondsAfterMinutes >= 60)		// Sometimes happens due to Math.floor(). Anti-alias it back to secular time. 
		{
		sMinutesToSessionTimeout			+= 1;		// Add one to minutes. 
		sSecondsAfterMinutes				-= 60;		// Subtract 60 from seconds. 
		}
	if	(sSecondsAfterMinutes < 10)			// Add leading zero so that it'll display as m:ss, not m:s. 
		sSecondsAfterMinutes				= ("y0" + sSecondsAfterMinutes).substring(1,3);
	gSlafShowAppTimeoutRef.innerHTML		= "Session timeout in "
											+ sMinutesToSessionTimeout
											+ ":"
											+ sSecondsAfterMinutes
											+ ". ";
	}

function SlafToggleAppData					()
	{
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	if	(!g$DivAppData)						// Don't initialize in SlafInitGlobals. (Executes too soon in MSIE 7.) 
		g$DivAppData						= $(gDivAppData);
	if	(gDivMarginT.title.substring(0,3) == "Max")
		{
		// The following are in standard order for CSS. Use multi-property css() of jQuery 1.9.1+ when available. 
		gDivAppDataTop						= g$DivAppData.css("top");
		gDivAppDataRight					= g$DivAppData.css("right");
		gDivAppDataBottom					= g$DivAppData.css("bottom");
		gDivAppDataLeft						= g$DivAppData.css("left");
		gDivMarginT.title					=
		gDivMarginR.title					=
		gDivMarginB.title					=
		gDivMarginL.title					= "Min" + gDivMarginT.title.substring(3,gDivMarginT.title.length);
		g$DivAppData.animate(
			{
			top:							"0px",
			right:							"0px",
			bottom:							"0px",
			left:							"0px"
			}, kAnimationMilliseconds);		// If parameter 2 is numeric, it's the duration in milliseconds. 
		SlafTrace("SlafToggleAppData (expanded).");
		}
	else
		{
		gDivMarginT.title					=
		gDivMarginR.title					=
		gDivMarginB.title					=
		gDivMarginL.title					= "Max" + gDivMarginT.title.substring(3,gDivMarginT.title.length);
		g$DivAppData.animate(
			{
			top:							gDivAppDataTop,
			right:							gDivAppDataRight,
			bottom:							gDivAppDataBottom,
			left:							gDivAppDataLeft
			}, kAnimationMilliseconds);		// If parameter 2 is numeric, it's the duration in milliseconds. 
		SlafTrace("SlafToggleAppData (contracted).");
		}
	}

function SlafToggleAppNav					()
	{
	if	(!gSlafAppNavIsVisible)				// Set in the custom tag. If there isn't any (visible) AppNav, 
		return;								// SlafToggleAppNav should never get executed, but don't error. 
	if	(!gDivSliderControl)				// Custom tag could also disable by removing div. If it doesn't exist, 
		return;								// SlafToggleAppNav should never get executed, but don't error. 
	if	(!g$DivAppData)						// Don't initialize in SlafInitGlobals. (Executes too soon in MSIE 7.) 
		g$DivAppData						= $(gDivAppData);
	if	(!g$DivAppNav)						// Don't initialize in SlafInitGlobals. (Executes too soon in MSIE 7.) 
		g$DivAppNav							= $(gDivAppNav);
	if	(gDivAppNavIsExpanded)
		{
		g$DivAppData.animate({left:			"198px"}, kAnimationMilliseconds);
		g$DivAppNav.animate({width:			"196px"}, kAnimationMilliseconds);
		gDivSliderControl.style.left		= "";	// Revert to css. 
		gDivSliderControl.style.cursor		= "";	// Revert to css. 
		gDivSliderControl.title				= kSliderTooltipExpand;
		SlafTrace("SlafToggleAppNav (contracted).");
		}
	else
		{
		g$DivAppData.animate({left:			"394px"}, kAnimationMilliseconds);
		g$DivAppNav.animate({width:			"392px"}, kAnimationMilliseconds);
		gDivSliderControl.style.left		= "391px";		// Style overrides css. 
		gDivSliderControl.style.cursor		= "w-resize";	// Style overrides css. 
		gDivSliderControl.title				= kSliderTooltipContract;
		SlafTrace("SlafToggleAppNav (expanded).");
		}
	gDivAppNavIsExpanded					= !gDivAppNavIsExpanded;
	}

function SlafToggleEnvelope					()
	{
	var	sBefore								= "";
	if	(!gAllGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		SlafInitGlobals();					// (Sometimes they won't be if the calling page trashes the onLoad.) 
	if	(gDivEnvelope && gDivEnvelope.style && gDivEnvelope.style.display)		// Almost certainly exists. 
		sBefore								= gDivEnvelope.style.display;
	gDivEnvelope.style.display				= ((sBefore === "") ? "none" : "");	// Now for sure it exists. 
	}

function SlafToggleTextOnly					()
	{
	if	(top.document.SlafToggleTextOnlyForm)
		with (top.document.SlafToggleTextOnlyForm)
			{
			JavaScriptOn.value				= "Yes";
			submit();
			return;
			}
	// If the form doesn't exist, to the same thing using the URL. The following will probably never be done: 
	var	sHRef								= "";
	var	sSearch								= top.location.search;
	if	(sSearch.length > 1)
		{
		var	sString							= unescape(sSearch);
		var	sArray							= sString.substring(1,sString.length).split("&");		// Strip initial "?"
		var	sAlreadyHasToggleTextOnly		= false;
		for	(var i = 0; i < sArray.length; i++)
			{
			var	sElt						= sArray[i];
			if	((sElt.length > 18) && (sElt.substring(0,18) == "SlafToggleTextOnly"))
				{
				sAlreadyHasToggleTextOnly	= true;
				break;
				}
			}
		if	(!sAlreadyHasToggleTextOnly)
			sSearch							+= "&SlafToggleTextOnly=Yes";
		}
	else
		sSearch								= "?SlafToggleTextOnly=Yes";

	with (top.location)
		{
		sHRef								= protocol;
		sHRef								+= "//";
		sHRef								+= host;
		sHRef								+= pathname;
		sHRef								+= sSearch;
		if	(hash.length > 0)
			sHRef							+= hash;
		}
	top.location.href						= sHRef;
	}

function SlafTrace							(pData, pDoAlert, pDoClear)// pDoAlert & pDoClear are optional, default false. 
	{
	// Can be dumped from the Location Bar. Example: "javascript:SlafTrace('', true);"
	var	sDoAlert							= (((arguments.length > 1) && (typeof arguments[1] === "boolean")) ? pDoAlert : false);
	var	sDoClear							= (((arguments.length > 2) && (typeof arguments[2] === "boolean")) ? pDoAlert : false);
	if	(gSlafTraceIsOn)
		gSlafTraceData						+= "\n"
											+ (gSlafTraceShowsTimestamp ? "hh:mm:ss.hhh: " : "") // Future feature.
											+ pData;
	if	(sDoAlert)
		alert("SBA Look-and-Feel Trace:\n"	+ gSlafTraceData);
	if	(sDoClear)
		gSlafTraceData						= "";
	}

SlafTrace("sbalookandfeel.strict.js fully loaded.");

// End sbalookandfeel.js


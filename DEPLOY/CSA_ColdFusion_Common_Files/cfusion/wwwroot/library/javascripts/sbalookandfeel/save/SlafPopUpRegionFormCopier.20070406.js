
// Begin SlafMenuPopUpFormCopier.js (in case cfincluded)
// Created by Steve Seaquist, 03/13/2007
//
// When you call SlafPopUpRegion("AppInfoAndAppData") or SlafPopUpRegion("Print"), 
// this routine executes in the popped up window, to copy current form element contents. 
// Necessary because non-MSIE browsers copy only the HTML, not the current form contents, 
// when you copy the form using innerHTML. 

var	gSlafPopUpRegionFormCopierDebug	= false;
var	kBrowserIsMSIE						= ((navigator.appName == "Microsoft Internet Explorer") 
										&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
										) ? true : false;

function SlafPopUpRegionFormCopier	(pRegion)
{
if	(kBrowserIsMSIE)				// MSIE copies current form data, so this code is unneeded. 
	return;							// MSIE copies current form data, so this code is unneeded. 
if	((!document.forms) || (document.forms.length == 0))
	return;							// If there isn't any form data, this code is also unneeded. 
switch (pRegion)
	{
	case "AppData":
	case "AppInfoAndAppData":
	case "Print":
		break;
	default:
		return;						// Copy form data only for AppData. 
	}
var	sAppDataDoc						= (opener.top.AppData) ? opener.top.AppData.document : opener.top.document;
if	((!sAppDataDoc.forms) || (sAppDataDoc.forms.length == 0))
	return;							// If there isn't any form data, this code is also unneeded. 
for	(var i = 0; i < document.forms.length; i++)
	{
	var	sAppDataForm				= null;
	var	sAppDataName				= null;
	var	sAppDataElts				= null;
	var	sThisPageForm				= document.forms[i];
	var	sThisPageName				= (sThisPageForm.name ? sThisPageForm.name : '');
	var	sThisPageElts				= sThisPageForm.elements;
	var	sFound						= -1;
	for	(j = 0; j < sAppDataDoc.forms.length; j++)
		{
		sAppDataForm				= sAppDataDoc.forms[j];
		sAppDataName				= (sAppDataForm.name ? sAppDataForm.name : '');
		sAppDataElts				= sAppDataForm.elements;
		if	(sAppDataElts.length == sThisPageElts.length)
			{
			// If the number of elements is the same, and the name is the same, we have a solid indication that 
			// it's the same form: 
			if	((sAppDataName.length > 0) && (sAppDataName == sThisPageName))
				{
				sFound				= j;
				if	(gSlafPopUpRegionFormCopierDebug)
					alert("Form found on the basis of name.");
				break;
				}
			// If the number of elements is the same, and there isn't any other form that also has that number of 
			// elements, then infer that it's the same form. But if two forms have the same number of elements as 
			// this one, we have no basis to infer which one is the right one: 
			if	(sFound == -1)
				sFound				= j;
			else
				{
				sFound				= -1;
				break;
				}
			}
		}
	if	(sFound >= 0)
		{
		sAppDataForm				= sAppDataDoc.forms[sFound];
		sAppDataElts				= sAppDataForm.elements;
		for	(j = 0; j < sAppDataElts.length; j++)
			{
			var	sAppDataElt			= sAppDataElts	[j];
			var	sThisPageElt		= sThisPageElts	[j];
		//	opener.top.DumpObject(sAppDataElt, "sAppDataElts["+j+"]", 20);
			switch (sAppDataElt.type.toLowerCase())	// Should already be lower case, but better safe than sorry. 
				{
				case "checkbox":
				case "radio":
					sThisPageElt.checked					= sAppDataElt.checked;
					break;
				case "select-one":
					sThisPageElt.selectedIndex				= sAppDataElt.selectedIndex;
					break;
				case "select-multiple":
					for	(var k = 0; k < sAppDataElt.options.length; k++)
						sThisPageElt.options[k].selected	= sAppDataElt.options[k].selected;
					break;
				case "text":
				case "textarea":
					sThisPageElt.value						= sAppDataElt.value;
					break;
				}
			}
		}
	// Keep looping. Copy all forms that we can infer are the same. 
	}
}

// End SlafMenuPopUpFormCopier.js


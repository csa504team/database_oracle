
// Begin SlafMenuPopUpFormCopier.js (in case cfincluded)
// Created by Steve Seaquist, 03/13/2007
//
// When you call SlafPopUpRegion("AppInfoAndAppData") or SlafPopUpRegion("Print"), these 2 routines execute in the 
// popped up window (/library/html/printer_friendly.html), to copy current form element contents. Necessary because 
// non-MSIE browsers copy only the HTML, not the current form contents, when you copy the form using innerHTML. 
// (One of the few things MSIE does well, keeping innerHTML current.) Also, no browser, not even MSIE, copies over 
// the contents of rich text editors. 
//
// Revision History:	02/17/2009, SRS:	Per Ron Whalen, ripped out support for ggedit. 
//						01/19/2009, SRS:	Added support for FCKEditor (easier and better rich text editor). 
//						09/11/2008, SRS:	Added support for ggedit (clunky, hard to use rich text editor). 
//						03/13/2007, SRS:	Original implementation. 


var	gSlafPopUpRegionFormCopierDebug		= false;
var	gSlafPopUpRegionRichTextCopierDebug	= false;
var	kBrowserIsMSIE						= ((navigator.appName == "Microsoft Internet Explorer") 
										&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
										) ? true : false;


// ************************************************************************************************

// SlafPopUpRegionFormCopier copies just standard form data, not rich text editor data. SlafPopUpRegionRichTextCopier, 
// below, does rich text editors. 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionFormCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionFormCopier		(pRegion, pDebug)
{
if	(kBrowserIsMSIE)					// MSIE copies current form data, so this code is unneeded. 
	return;								// MSIE copies current form data, so this code is unneeded. 
if	((!document.forms) || (document.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
switch (pRegion)
	{
	case "AppData":
	case "AppInfoAndAppData":
	case "Print":
		break;
	default:
		return;							// Copy form data only for AppData. 
	}
var	sAppDataDoc							= (opener.top.AppData) ? opener.top.AppData.document : opener.top.document;
if	((!sAppDataDoc.forms) || (sAppDataDoc.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
for	(var i = 0; i < document.forms.length; i++)
	{
	var	sAppDataForm					= null;
	var	sAppDataName					= null;
	var	sAppDataElts					= null;
	var	sThisPageForm					= document.forms[i];
	var	sThisPageName					= (sThisPageForm.name ? sThisPageForm.name : '');
	var	sThisPageElts					= sThisPageForm.elements;
	var	sFound							= -1;
	for	(j = 0; j < sAppDataDoc.forms.length; j++)
		{
		sAppDataForm					= sAppDataDoc.forms[j];
		sAppDataName					= (sAppDataForm.name ? sAppDataForm.name : '');
		sAppDataElts					= sAppDataForm.elements;
		if	(sAppDataElts.length == sThisPageElts.length)
			{
			// If the number of elements is the same, and the name is the same, we have a solid indication that 
			// it's the same form: 
			if	((sAppDataName.length > 0) && (sAppDataName == sThisPageName))
				{
				sFound					= j;
				if	(pDebug)			alert("Form found on the basis of name.");
				break;
				}
			// If the number of elements is the same, and there isn't any other form that also has that number of 
			// elements, then infer that it's the same form. But if two forms have the same number of elements as 
			// this one, we have no basis to infer which one is the right one: 
			if	(sFound == -1)
				sFound					= j;
			else
				{
				sFound					= -1;
				break;
				}
			}
		}
	if	(sFound >= 0)
		{
		sAppDataForm					= sAppDataDoc.forms[sFound];
		sAppDataElts					= sAppDataForm.elements;
		for	(j = 0; j < sAppDataElts.length; j++)
			{
			var	sAppDataElt				= sAppDataElts	[j];
			var	sThisPageElt			= sThisPageElts	[j];
		//	opener.top.DumpObject(sAppDataElt, "sAppDataElts["+j+"]", 20);
			switch (sAppDataElt.type.toLowerCase())	// Should already be lower case, but better safe than sorry. 
				{
				case "checkbox":
				case "radio":
					sThisPageElt.checked					= sAppDataElt.checked;
					break;
				case "select-one":
					sThisPageElt.selectedIndex				= sAppDataElt.selectedIndex;
					break;
				case "select-multiple":
					for	(var k = 0; k < sAppDataElt.options.length; k++)
						sThisPageElt.options[k].selected	= sAppDataElt.options[k].selected;
					break;
				case "text":
				case "textarea":
					sThisPageElt.value						= sAppDataElt.value;
					break;
				}
			}
		}
	// Keep looping. Copy all forms that we can infer are the same. 
	}
}


// ************************************************************************************************

// Do not call SlafPopUpRegionRichTextCopier from SlafPopUpRegionFormCopier, above, because SlafPopUpRegionFormCopier 
// sometimes does an "early return". Instead, call SlafPopUpRegionRichTextCopier from /library/html/printer_friendly.html, 
// so that it will always be done. SlafPopUpRegionRichTextCopier was added to support the Rich Text Editor (ggedit.js) 
// and later FCKEditor. 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionRichTextCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionRichTextCopier	(pDebug)
{
if	(pDebug)												alert("SlafPopUpRegionRichTextCopier was called.");
var	sRTEWindow												= null;	// Just defining it in one place, at this point. 
// Note: SlafGetRichTextEditors is a function defined in jquery.FCKEditors.SBA.js. 
if		(opener.SlafGetRichTextEditors)						sRTEWindow	= opener;
else if	(opener.top.SlafGetRichTextEditors)					sRTEWindow	= opener.top;
else if	(opener.top.AppData)
	if	(opener.top.AppData.SlafGetRichTextEditors)			sRTEWindow	= opener.top.AppData;
var	sRichTextEditors										= (sRTEWindow ? sRTEWindow.SlafGetRichTextEditors() : null);
if	(sRichTextEditors)
	{
	if	(pDebug)											alert("Opener has rich text editor(s).");
	for	(var i = 0; i < sRichTextEditors.length; i++) 
		{
		if	(pDebug)										alert("Loop index i = "+i+".");
		var	sHTMLElement									= sRichTextEditors[i];
		if	(sHTMLElement.fck)								// fck is a standard object installed by jquery.FCKEditor. 
			{
			var	sFCK										= sHTMLElement.fck;
			var	sId											= sFCK.InstanceName+"___Frame";
			var	sValue										= null;	// Just defining it in one place, at this point. 
			// **TRY** to use FCKeditorAPI.GetInstance(name), the way we do in ClearForm and ResetForm. 
			// That's the preferred way, because it probably won't change between versions of FCKEditor. 
			if	(sRTEWindow.FCKeditorAPI)					// Probably where FCKeditorAPI is defined. 
				{
				if	(pDebug)								alert("Using sRTEWindow.FCKeditorAPI to get HTML.");
				sValue										= sRTEWindow.FCKeditorAPI
																.GetInstance(sHTMLElement.name).GetHTML();
				}
			else
				{
				if	(pDebug)								alert("Using DOM navigation to get HTML.");
				// Compared to FCKeditorAPI's GetHTML, /CFIDE/scripts/ajax/FCKEditor/editor/fckeditor.html's element 
				// nesting could change much more easily in future versions. This is fallback DOM navigation code for 
				// fckeditor.html as of FCKEditor version 2.5 (ColdFusion 8 and 9), in case we can't find FCKeditorAPI: 
				var	sOuterFrame								= sRTEWindow.document.getElementById(sId);
				var	sInnerFrame								= sOuterFrame.contentWindow.document
																.getElementById("xEditingArea").firstChild;
				var	sInnerBody								= null;	// Just defining it in one place, at this point. 
				if	($.browser.msie)
					sInnerBody								= sInnerFrame.contentWindow.document.body;
				else
					sInnerBody								= sInnerFrame.contentDocument.body;
				sValue										= sInnerBody.innerHTML;
				}
			if	(pDebug)									alert("FCKEditor rich text editor with sId = '"
																+ sId + "' " + "and value '"	+ sValue+".");
			$("textarea#"+sFCK.InstanceName).each(function(){this.value			= sValue;	});
			}
		else
			alert("INTERNAL ERROR. Encountered unknown Rich Text Editor type. Ignored.");
		}
	}
else if	(pDebug)											alert("Opener doesn't have any rich text editor(s).")
return "Done.";
}

// End SlafMenuPopUpFormCopier.js


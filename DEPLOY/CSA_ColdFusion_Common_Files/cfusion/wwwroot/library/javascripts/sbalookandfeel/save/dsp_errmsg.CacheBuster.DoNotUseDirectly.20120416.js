
// Begin dsp_errmsg.CacheBuster.DoNotUseDirectly.js
// Created by Steve Seaquist, Trusted Mission Solutions, Inc., for the US Small Business Administration. 
//
// Should be needed only until the next release of sba.quirks.css. After that, get rid of the script
//
// Revision History:	04/16/2012, SRS:	Original implementation. 

(function()// Do it at the time this script file is included, not at window.onload time. (Prevents FOUC.) 
	{
	// Prevent the following variable names from getting defined in the global scope (window): 
	var	i, j, sRules, sSawErrMsgClass, sSheets, sSheet;
	sSheets									= document.styleSheets;
	sSawErrMsgClass							= false;
	if	(sSheets)
		for	(i = 0; i < sSheets.length; i++)
			{
			sSheet							= sSheets[i];
			sRules							= sSheet.rules		? sSheet.rules		: sSheet.cssRules;
			if	(sRules)
				for	(j = 0; j < sRules.length; j++)
					if	(sRules[j].selectorText == ".dem_errmsg")
						{
						sSawErrMsgClass		= true;
						break;
						}
			if	(sSawErrMsgClass)
				break;
			}
	if	(!sSawErrMsgClass)
		document.writeln('<link href="/library/css/dsp_errmsg.CacheBuster.DoNotUseDirectly.css" '
						+'rel="stylesheet" type="text/css" media="all"/>');
	})(); // "Self-executing anonymous function"

// End dsp_errmsg.CacheBuster.DoNotUseDirectly.js


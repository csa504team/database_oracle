
// Begin SlafMenuPopUpFormCopier.js (in case cfincluded)
// Created by Steve Seaquist, 03/13/2007
//
// When you call SlafPopUpRegion("AppInfoAndAppData") or SlafPopUpRegion("Print"), these 2 routines execute in the 
// popped up window (/library/html/printer_friendly.html), to copy current form element contents. Necessary because 
// non-MSIE browsers copy only the HTML, not the current form contents, when you copy the form using innerHTML. 
// (One of the few things MSIE does well, keeping innerHTML current.) Also, no browser, not even MSIE, copies over 
// the contents of rich text editors. 
//
// Revision History:	01/19/2009, SRS:	Added support for FCKEditor (easier and better rich text editor). 
//						09/11/2008, SRS:	Added support for ggedit (clunky, hard to use rich text editor). 
//						03/13/2007, SRS:	Original implementation. 


var	gSlafPopUpRegionFormCopierDebug		= false;
var	gSlafPopUpRegionRichTextCopierDebug	= false;
var	kBrowserIsMSIE						= ((navigator.appName == "Microsoft Internet Explorer") 
										&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
										) ? true : false;


// ************************************************************************************************

// SlafPopUpRegionFormCopier copies just standard form data, not rich text editor data. SlafPopUpRegionRichTextCopier, 
// below, does rich text editors. 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionFormCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionFormCopier		(pRegion, pDebug)
{
if	(kBrowserIsMSIE)					// MSIE copies current form data, so this code is unneeded. 
	return;								// MSIE copies current form data, so this code is unneeded. 
if	((!document.forms) || (document.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
switch (pRegion)
	{
	case "AppData":
	case "AppInfoAndAppData":
	case "Print":
		break;
	default:
		return;							// Copy form data only for AppData. 
	}
var	sAppDataDoc							= (opener.top.AppData) ? opener.top.AppData.document : opener.top.document;
if	((!sAppDataDoc.forms) || (sAppDataDoc.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
for	(var i = 0; i < document.forms.length; i++)
	{
	var	sAppDataForm					= null;
	var	sAppDataName					= null;
	var	sAppDataElts					= null;
	var	sThisPageForm					= document.forms[i];
	var	sThisPageName					= (sThisPageForm.name ? sThisPageForm.name : '');
	var	sThisPageElts					= sThisPageForm.elements;
	var	sFound							= -1;
	for	(j = 0; j < sAppDataDoc.forms.length; j++)
		{
		sAppDataForm					= sAppDataDoc.forms[j];
		sAppDataName					= (sAppDataForm.name ? sAppDataForm.name : '');
		sAppDataElts					= sAppDataForm.elements;
		if	(sAppDataElts.length == sThisPageElts.length)
			{
			// If the number of elements is the same, and the name is the same, we have a solid indication that 
			// it's the same form: 
			if	((sAppDataName.length > 0) && (sAppDataName == sThisPageName))
				{
				sFound					= j;
				if	(pDebug)			alert("Form found on the basis of name.");
				break;
				}
			// If the number of elements is the same, and there isn't any other form that also has that number of 
			// elements, then infer that it's the same form. But if two forms have the same number of elements as 
			// this one, we have no basis to infer which one is the right one: 
			if	(sFound == -1)
				sFound					= j;
			else
				{
				sFound					= -1;
				break;
				}
			}
		}
	if	(sFound >= 0)
		{
		sAppDataForm					= sAppDataDoc.forms[sFound];
		sAppDataElts					= sAppDataForm.elements;
		for	(j = 0; j < sAppDataElts.length; j++)
			{
			var	sAppDataElt				= sAppDataElts	[j];
			var	sThisPageElt			= sThisPageElts	[j];
		//	opener.top.DumpObject(sAppDataElt, "sAppDataElts["+j+"]", 20);
			switch (sAppDataElt.type.toLowerCase())	// Should already be lower case, but better safe than sorry. 
				{
				case "checkbox":
				case "radio":
					sThisPageElt.checked					= sAppDataElt.checked;
					break;
				case "select-one":
					sThisPageElt.selectedIndex				= sAppDataElt.selectedIndex;
					break;
				case "select-multiple":
					for	(var k = 0; k < sAppDataElt.options.length; k++)
						sThisPageElt.options[k].selected	= sAppDataElt.options[k].selected;
					break;
				case "text":
				case "textarea":
					sThisPageElt.value						= sAppDataElt.value;
					break;
				}
			}
		}
	// Keep looping. Copy all forms that we can infer are the same. 
	}
}


// ************************************************************************************************

// Do not call SlafPopUpRegionRichTextCopier from SlafPopUpRegionFormCopier, above, because SlafPopUpRegionFormCopier 
// sometimes does an "early return". Instead, call SlafPopUpRegionRichTextCopier from /library/html/printer_friendly.html, 
// so that it will always be done. SlafPopUpRegionRichTextCopier was added to support the Rich Text Editor (ggedit.js) 
// and later FCKEditor. Note that both (SBA versions) of ggedit and FCKEditor save HTML element objects in the array 
// opener.top.gSlafRichTextEditors. This allows them to coexist, even on the same page. So for each element in the array, 
// we have to check whether it's a ggedit HTML element (div) or an FCKEditor HTML element (textarea). 
//
// Special note, in case you need to debug: 
//
// Accepting pDebug from the parameter list allows you to debug from the location bar. In other words, you do **NOT** 
// have to edit any source code. Instead, just cancel the window.print() dialog and enter this into the location bar: 
//
//					javascript:alert(SlafPopUpRegionRichTextCopier(true)); // Copy and paste me!! 
//
// The alert keeps some browsers from trashing the page by treating the return value as a new URL and going there. 
// So we return "Done." at the very end, so as to give that alert something to display. You can throw in lots more 
// debugging with "if (pDebug)", secure in the knowledge that no one else will be bothered by your debug displays. This 
// technique also keeps debugging always available everywhere, even in production, where you can't edit the source code. 

function SlafPopUpRegionRichTextCopier	(pDebug)
{
if	(pDebug)												alert("SlafPopUpRegionRichTextCopier was called.");
if	(opener.top.gSlafRichTextEditors)
	{
	if	(pDebug)											alert("Opener has rich text editor(s).");
	var	sSlafRichTextEditors								= opener.top.gSlafRichTextEditors;
	for	(var i = 0; i < sSlafRichTextEditors.length; i++) 
		{
		if	(pDebug)										alert("Loop index i = "+i+".");
		var	sOpenerHTMLElement								= sSlafRichTextEditors[i];
		if	(sOpenerHTMLElement.fck)						// fck is a standard object installed by jquery.FCKEditor. 
			{
			var	sFCK										= sOpenerHTMLElement.fck;
			var	sOpenerId									= sFCK.InstanceName+"___Frame";
			var	sOpenerValue								= null;	// Just defining it in one place, at this point. 
			// **TRY** to use FCKeditorAPI.GetInstance(name), the way we do in ClearForm and ResetForm. 
			// That's the preferred way, because it probably won't change between versions of FCKEditor. 
			if	(opener.FCKeditorAPI)						// Probably where FCKeditorAPI is defined. 
				{
				if	(pDebug)								alert("Using opener.FCKeditorAPI to get HTML.");
				sOpenerValue								= opener.FCKeditorAPI
																.GetInstance(sOpenerHTMLElement.name).GetHTML();
				}
			else if	(opener.top.FCKeditorAPI)				// Probably NOT where FCKeditorAPI is defined. 
				{
				if	(pDebug)								alert("Using opener.top.FCKeditorAPI to get HTML.");
				sOpenerValue								= opener.top.FCKeditorAPI
																.GetInstance(sOpenerHTMLElement.name).GetHTML();
				}
			else
				{
				if	(pDebug)								alert("Using DOM navigation to get HTML.");
				// Compared to FCKeditorAPI's GetHTML, /CFIDE/scripts/ajax/FCKEditor/editor/fckeditor.html's element 
				// nesting could change much more easily in future versions. This is fallback DOM navigation code for 
				// fckeditor.html as of FCKEditor version 2.5 (ColdFusion 8 and 9), in case we can't find FCKeditorAPI: 
				var	sOpenerOuterFrame						= opener.document.getElementById(sOpenerId);
				var	sOpenerInnerFrame						= sOpenerOuterFrame.contentWindow.document
																.getElementById("xEditingArea").firstChild;
				var	sOpenerInnerBody						= null;	// Just defining it in one place, at this point. 
				if	($.browser.msie)
					sOpenerInnerBody						= sOpenerInnerFrame.contentWindow.document.body;
				else
					sOpenerInnerBody						= sOpenerInnerFrame.contentDocument.body;
				sOpenerValue								= sOpenerInnerBody.innerHTML;
				}
			if	(pDebug)									alert("FCKEditor rich text editor with sOpenerId = '"
																+ sOpenerId + "' " + "and value '"	+ sOpenerValue+".");
			$("textarea#"+sFCK.InstanceName).each(function(){this.value			= sOpenerValue;	});
			}
		else if	(sOpenerHTMLElement.getId)
			{// getId() is an SBA add-on to ggedit.js. 
			var	sOpenerId									= sOpenerHTMLElement.getId();
			var	sOpenerValue								= sOpenerHTMLElement.getValue();
			if	(pDebug)									alert("ggedit rich text editor with sOpenerId = '"+sOpenerId+"' "
																+ "and value '"+sOpenerValue+".");
			var	sThisPageElt								= document.getElementById(sOpenerId);
			if	(sThisPageElt)
				{
				if	(pDebug)								alert("sThisPageElt found.");
				var	sThisPageEltFrame						= sThisPageElt.lastChild;
				if	(sThisPageEltFrame)
					{
					if	(pDebug)							alert("sThisPageEltFrame found.");
					if	(pDebug)							opener.top.DumpObject(sThisPageEltFrame, "sThisPageEltFrame", 10);
					var	sThisPageEltDoc						= sThisPageEltFrame.contentDocument;
					if	(sThisPageEltDoc)
						{
						if	(pDebug)						alert("sThisPageEltDoc found. Copying to it.");
						sThisPageEltDoc.open();
						sThisPageEltDoc.write(opener.top.gSlafRichTextEditors[i].getValue());
						sThisPageEltDoc.close();
						if	(pDebug)						alert("sThisPageEltDoc.body.innerHTML after copy:\n\n"
																 + sThisPageEltDoc.body.innerHTML);
						}
					else if	(pDebug)						alert("sThisPageEltDoc not found. Not a frame. Ignored.");
					}
				else if	(pDebug)							alert("sThisPageEltFrame not found. Ignored.");
				}
			else if	(pDebug)								alert("sThisPageElt not found. Ignored.");
			}
		else
			alert("INTERNAL ERROR. Encountered unknown Rich Text Editor type. Ignored.");
		}
	}
else if	(pDebug)											alert("Opener doesn't have any rich text editor(s).")
return "Done.";
}

// End SlafMenuPopUpFormCopier.js


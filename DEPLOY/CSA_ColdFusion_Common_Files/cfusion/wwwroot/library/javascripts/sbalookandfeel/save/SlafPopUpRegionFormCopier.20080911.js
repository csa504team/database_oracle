
// Begin SlafMenuPopUpFormCopier.js (in case cfincluded)
// Created by Steve Seaquist, 03/13/2007
//
// When you call SlafPopUpRegion("AppInfoAndAppData") or SlafPopUpRegion("Print"), 
// this routine executes in the popped up window, to copy current form element contents. 
// Necessary because non-MSIE browsers copy only the HTML, not the current form contents, 
// when you copy the form using innerHTML. 
//
// Revision History:	09/11/2008, SRS:	Added support for rich text editors. 
//						03/13/2007, SRS:	Original implementation. 

var	gSlafPopUpRegionFormCopierDebug		= false;
var	gSlafPopUpRegionRichTextCopierDebug	= false;
var	kBrowserIsMSIE						= ((navigator.appName == "Microsoft Internet Explorer") 
										&& (navigator.userAgent.indexOf("Opera") < 0)	// Opera emulates IE. 
										) ? true : false;

// Do not call this routine from SlafPopUpRegionFormCopier, because SlafPopUpRegionFormCopier sometimes does an 
// early "return". Instead, call this routine from /library/html/printer_friendly.html, so that it will always 
// be done. The following routine was added to support the Rich Text Editor (ggedit.js): 
function SlafPopUpRegionRichTextCopier	(pDebug)
{// Accepting pDebug from the parameter list allows this routine to be executed with true from the location bar. 
if	(pDebug)												alert("SlafPopUpRegionRichTextCopier was called.");
if	(opener.top.gSlafRichTextEditors)
	{
	if	(pDebug)											alert("Opener has rich text editor(s).");
	for	(var i = 0; i < window.frames.length; i++) 
		{
		if	(pDebug)										alert("Loop index i = "+i+".");
		if	(i < opener.top.gSlafRichTextEditors.length)	// If corresponding rich text editor exists: 
			{// getId() is an SBA add-on to ggedit.js. Not currently in use, but available in case needed: 
			var	sId											= opener.top.gSlafRichTextEditors[i].getId();
			if	(pDebug)									alert("sId = '"+sId+"'.");
			if	(pDebug)									alert("document.getElementById('"+sId+"') = "
																 + document.getElementById(   sId   ));
			if	(pDebug)									alert("opener.top.gSlafRichTextEditors["+i+"].getValue():\n\n"
																 + opener.top.gSlafRichTextEditors[  i  ].getValue());
			window.frames[i].document.open();
			window.frames[i].document.write(opener.top.gSlafRichTextEditors[i].getValue());
			window.frames[i].document.close();
			if	(pDebug)									alert("window.frames["+i+"].document.body.innerHTML:\n\n"
																 + window.frames[  i  ].document.body.innerHTML);
			}
		}
	}
else if	(pDebug)											alert("Opener doesn't have any rich text editor(s).")
}

function SlafPopUpRegionFormCopier		(pRegion)
{
if	(kBrowserIsMSIE)					// MSIE copies current form data, so this code is unneeded. 
	return;								// MSIE copies current form data, so this code is unneeded. 
if	((!document.forms) || (document.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
switch (pRegion)
	{
	case "AppData":
	case "AppInfoAndAppData":
	case "Print":
		break;
	default:
		return;							// Copy form data only for AppData. 
	}
var	sAppDataDoc							= (opener.top.AppData) ? opener.top.AppData.document : opener.top.document;
if	((!sAppDataDoc.forms) || (sAppDataDoc.forms.length == 0))
	return;								// If there isn't any form data, this code is also unneeded. 
for	(var i = 0; i < document.forms.length; i++)
	{
	var	sAppDataForm					= null;
	var	sAppDataName					= null;
	var	sAppDataElts					= null;
	var	sThisPageForm					= document.forms[i];
	var	sThisPageName					= (sThisPageForm.name ? sThisPageForm.name : '');
	var	sThisPageElts					= sThisPageForm.elements;
	var	sFound							= -1;
	for	(j = 0; j < sAppDataDoc.forms.length; j++)
		{
		sAppDataForm					= sAppDataDoc.forms[j];
		sAppDataName					= (sAppDataForm.name ? sAppDataForm.name : '');
		sAppDataElts					= sAppDataForm.elements;
		if	(sAppDataElts.length == sThisPageElts.length)
			{
			// If the number of elements is the same, and the name is the same, we have a solid indication that 
			// it's the same form: 
			if	((sAppDataName.length > 0) && (sAppDataName == sThisPageName))
				{
				sFound					= j;
				if	(gSlafPopUpRegionFormCopierDebug)
					alert("Form found on the basis of name.");
				break;
				}
			// If the number of elements is the same, and there isn't any other form that also has that number of 
			// elements, then infer that it's the same form. But if two forms have the same number of elements as 
			// this one, we have no basis to infer which one is the right one: 
			if	(sFound == -1)
				sFound					= j;
			else
				{
				sFound					= -1;
				break;
				}
			}
		}
	if	(sFound >= 0)
		{
		sAppDataForm					= sAppDataDoc.forms[sFound];
		sAppDataElts					= sAppDataForm.elements;
		for	(j = 0; j < sAppDataElts.length; j++)
			{
			var	sAppDataElt				= sAppDataElts	[j];
			var	sThisPageElt			= sThisPageElts	[j];
		//	opener.top.DumpObject(sAppDataElt, "sAppDataElts["+j+"]", 20);
			switch (sAppDataElt.type.toLowerCase())	// Should already be lower case, but better safe than sorry. 
				{
				case "checkbox":
				case "radio":
					sThisPageElt.checked					= sAppDataElt.checked;
					break;
				case "select-one":
					sThisPageElt.selectedIndex				= sAppDataElt.selectedIndex;
					break;
				case "select-multiple":
					for	(var k = 0; k < sAppDataElt.options.length; k++)
						sThisPageElt.options[k].selected	= sAppDataElt.options[k].selected;
					break;
				case "text":
				case "textarea":
					sThisPageElt.value						= sAppDataElt.value;
					break;
				}
			}
		}
	// Keep looping. Copy all forms that we can infer are the same. 
	}
}

// End SlafMenuPopUpFormCopier.js



// Begin buttonbar.js (in case cfincluded, which normally shouldn't be done)
// Created by Steve Seaquist, 10/16/2015. 
//
// Revision History:	10/16/2015, SRS:	Original implementation. Cannibalized sbalookandfeel.strict.js. 
//
// This JavaScript should be called ONLY by cf_sbabuttonbar. If any other frame or inline region needs these 
// variables or functions, they can always reference them as top.whatever. Therefore, it's okay to force the 
// caller to the topmost window frame: 

if	(self != top.self)						// Prevents accidentally nested frames. 
	top.location.href						= self.location.href;

// Configuration Parameters:


// Globals to save on calls to document.getElementById. Not using "var" to highlight that they're intentionally global: 
// Some of these are also defined in sbalookandfeel.strict.js, but no hurry getting rid of them just yet. 

gBBGlobalsInitialized						= false;
gBBIconHelp									= null;
gBBIconSettings								= null;
gBBLastPopUp								= null;
gBBMenuHelp									= null;
gBBMenuSettings								= null;

// Functions, in aphabetical order: 

function BBInitGlobals					()
	{
	gBBMenuHelp								= document.getElementById("DivMenuHelp");// Doesn't exist yet. 
	gBBMenuSettings							= document.getElementById("DivMenuSettings");
	$(".sbb_help").each(function()
		{
		gBBIconHelp							= this; // If more than one exists, use the last one. Don't break out of each. 
		});
	$(".sbb_settings").each(function()
		{
		gBBIconSettings						= this; // If more than one exists, use the last one. Don't break out of each. 
		});
	gBBGlobalsInitialized					= true;
	}

function BBMenuHideHelp						()
	{
	if	(!gBBGlobalsInitialized)		// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();				// This function uses gDiv globals, so make sure their contents are initialized. 
	$(gBBMenuHelp).hide();
	}

function BBMenuHideSettings					()
	{
	if	(!gBBGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();					// This function uses gDiv globals, so make sure their contents are initialized. 
	$(gBBMenuSettings).hide();
	}

function BBMenuSelect						(pActionCode)
	{
	// Although this function doesn't use gDiv globals, the file opened by pActionCode "Print" (printer_friendly.html) does. 
	// The problem is that printer_friendly.html can't test for the existence of the SBA look-and-feel globals if the user 
	// is using the Opera browser for Windows. So it falls to this routine to make sure that the gDiv globals' contents are 
	// properly initialized. (Sometimes they won't be if the calling page trashes the onLoad.) 
	if	(!gBBGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();					// This function uses gDiv globals, so make sure their contents are initialized. 
	var	sWindow								= null;
	var	sWindIntName						= "PoputWindowCreatedAtMilliseconds" + (new Date()).getTime();
	switch (pActionCode)
		{
		case "CFDebug":
			if	(window.SlafToggleEnvelope)	// Might not exist on non-SBA-Look-and-Feel pages. 
				SlafToggleEnvelope();
			break;
		case "Help":
			gLastPopUp						= window.open("/library/html/sba_look_and_feel_help.html",
											sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
			gLastPopUp.focus();
			break;
		case "Print":
			gLastPopUp						= window.open("/library/html/printer_friendly.html?CachedAsOf=2013-09-05T19:21",
											sWindIntName, "location,menubar,resizable,scrollbars,titlebar,toolbar");
		//	gLastPopUp.print();				// Now handled by /library/html/printer_friendly.html, don't do it twice. 
			break;
		case "Show/Hide":
			if	(window.SlafToggleAppData)	// Might not exist on non-SBA-Look-and-Feel pages. 
				SlafToggleAppData();
			break;
		case "TextOnly":
			alert("Please set Text-Only Black and White Theme instead. That's how it's done now.");
			// The form submission should preclude executing any JavaScript afterwards, but it doesn't hurt: 
			break;
		default:
			alert("No action defined for this hotlink yet.");
		}
	BBMenuHideSettings();
	}

function BBMenuShowHelp						()
	{
	if	(!gBBGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();					// This function uses gDiv globals, so make sure their contents are initialized. 
	$(gBBMenuHelp).show();
	}

function BBMenuShowSettings					()
	{
	if	(!gBBGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();					// This function uses gDiv globals, so make sure their contents are initialized. 
	$(gBBMenuSettings).show();
	}

$(function()
	{
	if	(!gBBGlobalsInitialized)			// This function uses gDiv globals, so make sure their contents are initialized. 
		BBInitGlobals();					// This function uses gDiv globals, so make sure their contents are initialized. 
	if	(gBBIconHelp)						// If the icon's not on the screen, don't set up show/hide of its menu. 
		{
		$([gBBIconHelp,		gBBMenuHelp])	.hover(BBMenuShowHelp,		BBMenuHideHelp);
		BBMenuHideHelp	();
		}
	if	(gBBIconSettings)					// If the icon's not on the screen, don't set up show/hide of its menu. 
		{
		$([gBBIconSettings,	gBBMenuSettings]).hover(BBMenuShowSettings,	BBMenuHideSettings);
		BBMenuHideSettings();
		}
	});

// End buttonbar.js

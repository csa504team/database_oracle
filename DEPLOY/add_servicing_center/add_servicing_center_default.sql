define deploy_name=add_servicing_center
define package_name=default
define package_buildtime=20191119104613
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy add_servicing_center_default created on Tue 11/19/2019 10:46:15.88 by johnlow
prompt deploy scripts for deploy add_servicing_center_default created on Tue 11/19/2019 10:46:15.88 by johnlow
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy add_servicing_center_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Misc\insert_service_09-497S.sql 
Zeeshan Muhammad committed 4bd413b on Tue Nov 19 15:21:56 2019 +0000

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Misc\insert_service_09-497S.sql"
INSERT INTO cdconline.servcntrtbl
	(servcntrcdcid
	 , servcntrid
	 , cdcregncd
	 , cdcnmb
	 , creatuserid
	 , creatdt
	 , lastupdtuserid
	 , lastupdtdt)
VALUES      ( (SELECT MAX(servcntrcdcid) + 1
	  FROM   cdconline.servcntrtbl)
	 , 'sc02'
	 , '09'
	 , '497S'
	 , USER
	 , SYSDATE
	 , USER
	 , SYSDATE );

COMMIT; 
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

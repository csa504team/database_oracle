-- deploy scripts for deploy scrub_fixpdfviews created on Mon 06/24/2019 16:07:29.73 by johnlow
define deploy_name=scrub_fixpdfviews
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select global_name gn, user usrnm, to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors:' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1098PDFTBL.sql 
commit b7a9d684e7b0d0d846d08699612a8de4d6772ad9
Author: John low <jlow@selectcomputing.com>
Date:   Mon Jun 24 14:35:32 2019 -0400

    Fixed scrubbing views

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099INTPDFTBL.sql 
commit b7a9d684e7b0d0d846d08699612a8de4d6772ad9
Author: John low <jlow@selectcomputing.com>
Date:   Mon Jun 24 14:35:32 2019 -0400

    Fixed scrubbing views

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099MISCPDFTBL.sql 
commit b7a9d684e7b0d0d846d08699612a8de4d6772ad9
Author: John low <jlow@selectcomputing.com>
Date:   Mon Jun 24 14:35:32 2019 -0400

    Fixed scrubbing views

-- C:\CSA\database_oracle\CDCONLINE\Views\SC_TAXPDFPARSERLOGTBL.sql 
commit 391404c8930c5733a27d30b6b5ff737240623de5
Author: John low <jlow@selectcomputing.com>
Date:   Mon Jun 24 16:05:00 2019 -0400

    cleanup

*/
--
--
--
-- Deploy files start here:
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1098PDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1098PDFTBL 
  (
CDCREGNCD
,CDCNMB
,TAXYRNMB
,BORRNM
,BORRMAILADRSTR1NM
,BORRMAILADRSTR2NM
,BORRMAILADRCTYSTZIPCD
,LOANNMB
,TAXID
,INTPAIDAMT
,NOTEBALAMT
,SBAFEEAMT
,CDCFEEAMT
,CSAFEEAMT
,RPTBLIND
,TAX1098PDF
,PDFLOADLASTTIME
,BORRDESGNTN
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-24 12:17:00
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
CDCREGNCD
,CDCNMB
,TAXYRNMB
,BORRNM
,BORRMAILADRSTR1NM
,BORRMAILADRSTR2NM
,BORRMAILADRCTYSTZIPCD
,LOANNMB
,TAXID
,INTPAIDAMT
,NOTEBALAMT
,SBAFEEAMT
,CDCFEEAMT
,CSAFEEAMT
,RPTBLIND
/* TAX1098PDF */ ,cast(null as raw(1)) TAX1098PDF
,PDFLOADLASTTIME
,BORRDESGNTN
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1098PDFTBL ;
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099INTPDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1099INTPDFTBL 
  (
TAXYRNMB
,NAMADDRID
,TAX1099INTPDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-24 12:17:00
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
TAXYRNMB
,NAMADDRID
/* TAX1099INTPDF */ ,cast(null as raw(1)) TAX1099INTPFDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1099INTPDFTBL ;
/
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAX1099MISCPDFTBL.sql"
create or replace view CDCONLINE.SC_TAX1099MISCPDFTBL 
  (
TAXYRNMB
,NAMADDRID
,TAX1099MISCPDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-24 12:17:00
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
TAXYRNMB
,NAMADDRID
/* TAX1099MISCPDF */ ,cast(null as raw(1)) TAX1099MISCPDF
,PDFLOADLASTTIME
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAX1099MISCPDFTBL ;
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Views\SC_TAXPDFPARSERLOGTBL.sql"
create or replace view CDCONLINE.SC_TAXPDFPARSERLOGTBL 
  (
LOANNMB
,TAXYRNMB
,LASTUPDT
,PDFFILENM
,PDFFILEDT
,PDFTYP
,RESULT
,TAXREF
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-06-24 12:17:00
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,TAXYRNMB
,LASTUPDT
,PDFFILENM
,PDFFILEDT
,PDFTYP
/* RESULT */ ,cast(null as raw(1)) RESULT
,TAXREF
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.TAXPDFPARSERLOGTBL ;

set echo off
prompt '&&deploy_name deploy name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&DEPLOY_NAME._&&TSP..LST

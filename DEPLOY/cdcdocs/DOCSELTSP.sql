create or replace PROCEDURE                  cdcdocs.DOCSELTSP
(
p_Identifier number := null,
p_DOCId number := null,
p_CDCREGNCD char := null,
p_CDCNMB char := null,
p_BusPrcsTypCd number := null,
p_RetVal out number,
p_SelCur out sys_refcursor,
p_SelCur2 out sys_refcursor
)
as
begin
	-- returns document information based on doc id
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select
            DOCID,
			CDCREGNCD,
			CDCNMB,
            a.BusPrcsTypCd,
            (select BusPrcsTypDescTxt from sbaref.BusPrcsTypTbl z where z.BusPrcsTypCd = a.BusPrcsTypCd) as BusPrcsTypDescTxt,
            a.DOCTYPCd,
            b.DocTypDescTxt as TypeDescription,            
            DOCNM,
            ZIPFILENM,
            a.DOCSTATCD,
            c.DocStatDescTxt StatDescription,
            DOCSTATDT,
            a.DocActvInactInd,
            a.CREATUSERID,
            a.CREATDT,
            a.LASTUPDUSERID,
            a.LASTUPDDT
        from CDCDOCS.DocTbl a left outer join sbaref.DocTypTbl b on a.DocTypCd = b.DocTypCd
                      left outer join sbaref.DocStatCdTbl c on a.DocStatCd = c.DocStatCd
        where a.DocId = p_DocId       
        AND a.DOCACTVINACTIND = 'A'; 
        open p_SelCur2 for
        select
            COUNT(DOCID) AS RECORDCOUNT
        from CDCDOCS.DocTbl a left outer join sbaref.DocTypTbl b on a.DocTypCd = b.DocTypCd
                      left outer join sbaref.DocStatCdTbl c on a.DocStatCd = c.DocStatCd
        where a.DocId = p_DocId       
        AND a.DOCACTVINACTIND = 'A';                
        p_RetVal := SQL%ROWCOUNT;
    end;
	-- returns all active, uploaded documents by business process type code for ALL cdcs; this is used for the gov role
    elsif p_Identifier = 1 then
    begin
        open p_SelCur for
        SELECT
			src.cdcregncd AS cdcregncd,
			src.cdcnmb AS cdcnmb,
			b.doctypdesctxt,
			src.docid AS docid,
			c.docnm AS docnm,
			c.creatuserid AS creatuserid,
			c.creatdt AS creatdt 
		FROM
			(
				SELECT
					cdcregncd,
					cdcnmb,
					doctypcd,
					MAX(docid) AS docid,
					MAX(creatdt) AS maxcreatdt 
				FROM
					cdcdocs.doctbl 
				WHERE
					docstatcd = 4 
					AND docactvinactind = 'A'
					AND busprcstypcd = p_BusPrcsTypCd
				GROUP BY
					cdcregncd,
					cdcnmb,
					doctypcd 
				ORDER BY
					cdcregncd,
					cdcnmb,
					doctypcd
			)
			src 
			LEFT JOIN
				sbaref.doctyptbl b 
				ON src.doctypcd = b.doctypcd 
			LEFT JOIN
				cdcdocs.doctbl c 
				ON src.docid = c.docid;
        open p_SelCur2 for
        SELECT
			c.cdcregncd AS cdcregncd,
			c.cdcnmb AS cdcnmb,
			b.doctypdesctxt,
			c.docid AS docid,
			c.docnm AS docnm,
			c.creatuserid AS creatuserid,
			c.creatdt AS creatdt 
		FROM
			cdcdocs.doctbl c 
			LEFT JOIN
				sbaref.doctyptbl b 
				ON c.doctypcd = b.doctypcd 
		WHERE
			c.docstatcd = 4 
			AND c.docactvinactind = 'A' 
			AND c.docid NOT IN 
			(
				SELECT
					src.docid 
				FROM
					(
						SELECT
							cdcregncd,
							cdcnmb,
							doctypcd,
							MAX(docid) AS docid,
							MAX(creatdt) AS maxcreatdt 
						FROM
							cdcdocs.doctbl 
						WHERE
							docstatcd = 4 
							AND docactvinactind = 'A' 
							AND busprcstypcd = p_BusPrcsTypCd 
						GROUP BY
							cdcregncd,
							cdcnmb,
							doctypcd
					)
					src
			)
			AND c.busprcstypcd = p_BusPrcsTypCd 
		ORDER BY
			c.cdcregncd,
			c.cdcnmb,
			b.doctypdesctxt;
        p_RetVal := SQL%ROWCOUNT;
    end;
	-- returns all active, uploaded documents by business process type code, cdc number and cdc region; this is used for the cdc role
    elsif p_Identifier = 2 then
    begin
		open p_SelCur for
        SELECT
			src.cdcregncd AS cdcregncd,
			src.cdcnmb AS cdcnmb,
			b.doctypdesctxt,
			src.docid AS docid,
			c.docnm AS docnm,
			c.creatuserid AS creatuserid,
			c.creatdt AS creatdt 
		FROM
			(
				SELECT
					cdcregncd,
					cdcnmb,
					doctypcd,
					MAX(docid) AS docid,
					MAX(creatdt) AS maxcreatdt 
				FROM
					cdcdocs.doctbl 
				WHERE
					docstatcd = 4 
					AND docactvinactind = 'A'
					AND busprcstypcd = p_BusPrcsTypCd
					AND TRIM(CDCREGNCD) = TRIM(p_CDCREGNCD) AND TRIM(CDCNMB) = TRIM(p_CDCNMB)
				GROUP BY
					cdcregncd,
					cdcnmb,
					doctypcd 
				ORDER BY
					cdcregncd,
					cdcnmb,
					doctypcd
			)
			src 
			LEFT JOIN
				sbaref.doctyptbl b 
				ON src.doctypcd = b.doctypcd 
			LEFT JOIN
				cdcdocs.doctbl c 
				ON src.docid = c.docid;
        open p_SelCur2 for
        SELECT
			c.cdcregncd AS cdcregncd,
			c.cdcnmb AS cdcnmb,
			b.doctypdesctxt,
			c.docid AS docid,
			c.docnm AS docnm,
			c.creatuserid AS creatuserid,
			c.creatdt AS creatdt 
		FROM
			cdcdocs.doctbl c 
			LEFT JOIN
				sbaref.doctyptbl b 
				ON c.doctypcd = b.doctypcd 
		WHERE
			c.docstatcd = 4 
			AND c.docactvinactind = 'A' 
			AND c.docid NOT IN 
			(
				SELECT
					src.docid 
				FROM
					(
						SELECT
							cdcregncd,
							cdcnmb,
							doctypcd,
							MAX(docid) AS docid,
							MAX(creatdt) AS maxcreatdt 
						FROM
							cdcdocs.doctbl 
						WHERE
							docstatcd = 4 
							AND docactvinactind = 'A' 
							AND busprcstypcd = p_BusPrcsTypCd
							AND TRIM(CDCREGNCD) = TRIM(p_CDCREGNCD) AND TRIM(CDCNMB) = TRIM(p_CDCNMB)
						GROUP BY
							cdcregncd,
							cdcnmb,
							doctypcd
					)
					src
			)
			AND c.busprcstypcd = p_BusPrcsTypCd
			AND TRIM(c.CDCREGNCD) = TRIM(p_CDCREGNCD) AND TRIM(c.CDCNMB) = TRIM(p_CDCNMB)
		ORDER BY
			c.cdcregncd,
			c.cdcnmb,
			b.doctypdesctxt;
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;
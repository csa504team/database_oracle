create or replace PROCEDURE          CDCDOCS.DOCFILEUPLOADINSTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCID NUMBER := NULL,
p_DOCData Blob := NULL,
p_CREATUSERID VARCHAR2 := NULL
)
as
begin
    savepoint DocFileUploadInsTsp;
    if p_Identifier = 0 then
    begin
        insert into CDCDOCS.DocFileUploadTbl
        (
        DOCID,
        DOCData,    
        CREATUSERID,
        CREATDT
        )
        values
        (
        COALESCE(p_DOCID, CDCDOCS.DOCIDSEQ.NEXTVAL),
        UTL_COMPRESS.lz_compress(p_DOCData),
        p_CREATUSERID,
        sysdate
        );
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
exception when others then
begin
    rollback to DocFileUploadInsTsp;
    raise;
end;
end;

create or replace PROCEDURE          CDCDOCS.DOCUPDTSP
(
p_Identifier number := null,
p_DOCID number := null,
p_CDCREGNCD char := null,
p_CDCNMB char := null,
p_BusPrcsTypCd number := null,
p_DOCTYPCd number := null,
p_DOCNM varchar2 := null,
p_ZIPFILENM varchar2 := null,
p_DOCSTATCD number := null,
p_DOCSTATDT date := null,
p_DocActvInactInd char := null,
p_LASTUPDUSERID varchar2 := null
p_RetVal out number,
)
as
begin
    savepoint DocUpdTsp;
    if p_Identifier = 0 then
    begin
        update CDCDOCS.DocTbl set
        BusPrcsTypCd = NVL(p_BusPrcsTypCd, BusPrcsTypCd),
        DOCTYPCd = NVL(p_DocTypCd,DocTypCd),
        DOCNM = NVL(p_DocNm,DocNm),
        ZIPFILENM = NVL(p_ZIPFILENM,ZipFileNm),
        DOCSTATCD = NVL(p_DocStatCd,DocStatCd),
        DOCSTATDT = NVL(p_DocStatDt,DocStatDt),
        DocActvInactInd = p_DocActvInactInd,
        LASTUPDUSERID = p_LastUpdUSERID,
        LASTUPDDT = sysdate
        where DocId = p_DocId;        
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
exception when others then
begin
    rollback to DocUpdTsp;
    raise;
end;
end;

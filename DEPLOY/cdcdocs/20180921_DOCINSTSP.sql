create or replace PROCEDURE          CDCDOCS.DOCINSTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCID out number,
p_LocId number := null,
p_CDCREGNCD char,
p_CDCNMB char,
p_BusPrcsTypCd number := null,
p_DOCTYPCd number := null,
p_DOCNM varchar2 := null,
p_ZIPFILENM varchar2 := null,
p_DOCSTATCD number := null,
p_DOCSTATDT date := null,
p_DocActvInactInd char := null,
p_CREATUSERID char := null
)
as
begin
    savepoint DocInsTsp;
    p_DocId := DocIdSeq.nextval;
    if p_Identifier = 0 then
    begin
        insert into CDCDOCS.DocTbl
        (
        DOCID,
        LocId,
		CDCREGNCD,
		CDCNMB,
        BusPrcsTypCd,
        DOCTYPCd,
        DOCNM,
        ZIPFILENM,
        DOCSTATCD,
        DOCSTATDT,
        DocActvInactInd,
        CREATUSERID,
        CREATDT,
        LASTUPDUSERID,
        LASTUPDDT
        )
        values
        (
        p_DocId,
        p_LocId,
		p_CDCREGNCD,
		p_CDCNMB,
        p_BusPrcsTypCd,
        p_DOCTYPCd,
        p_DOCNM,
        p_ZIPFILENM,
        p_DOCSTATCD,
        p_DOCSTATDT,
        p_DocActvInactInd,
        p_CREATUSERID,
        sysdate,
        p_CREATUSERID,
        sysdate
        );
        p_RetVal := SQL%ROWCOUNT;       
    end;
    end if;
exception when others then
begin
    rollback to DocInsTsp;
    raise;
end;
end;

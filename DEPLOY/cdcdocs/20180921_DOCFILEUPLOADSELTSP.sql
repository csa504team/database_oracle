create or replace PROCEDURE          CDCDOCS.DOCFILEUPLOADSELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCID number := null,
p_SelCur out sys_refcursor
)
as
begin
    if p_Identifier = 0 then
    begin
        open p_SelCur for
        select
            a.DOCID,
            UTL_COMPRESS.lz_uncompress(DOCData) DOCData,
            b.DocNm,
            a.CREATUSERID,
            a.CREATDT            
        from CDCDOCS.DocFileUploadTbl a, CDCDOCS.DocTbl b
        where a.DocId = p_DocId
        and a.DocId = b.DocID
        AND b.DOCACTVINACTIND = 'A';        
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;

create or replace PROCEDURE          CDCDOCS.DOCDELTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCId number := null
)
as
begin
    if p_Identifier = 0 then
    begin
        delete CDCDOCS.DocFileUploadTbl
        where DocId = p_DocId;
        delete CDCDOCS.DocTbl
        where DocId = p_DocId;
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
end;

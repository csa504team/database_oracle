DELETE FROM sbaref.busprcstyptbl WHERE busprcstypdesctxt = 'Corporate Governance Module';

INSERT INTO sbaref.busprcstyptbl(busprcstypcd,busprcstypdesctxt,busprcstypstrtdt,busprcstypenddt,creatuserid,creatdt,imappsystypnm)
VALUES(
	12
	, 'Corporate Governance Module'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'CDCOnline'
);

COMMIT;

INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES(
	(SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl)
	, 'Articles of Incorporation'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
);

COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(
	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl)
	, 12
	, '504'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'O'
);

COMMIT;

INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES(
	(SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl)
	, 'Bylaws and Amendments'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
);

COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(
	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl)
	, 12
	, '504'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'O'
);

COMMIT;

INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES(
	(SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl)
	, 'D&O Insurance Binder'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
);

COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(
	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl)
	, 12
	, '504'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'O'
);

COMMIT;

INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES(
	(SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl)
	, 'E&O Insurance Binder'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
);

COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(
	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl)
	, 12
	, '504'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'O'
);

COMMIT;

INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES(
	(SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl)
	, 'Audited Financial Statements Packet'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
);

COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(
	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl)
	, 12
	, '504'
	, SYSDATE
	, NULL
	, USER
	, SYSDATE
	, 'O'
);

COMMIT;
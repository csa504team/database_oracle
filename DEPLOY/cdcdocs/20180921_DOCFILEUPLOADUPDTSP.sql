create or replace PROCEDURE          CDCDOCS.DOCFILEUPLOADUPDTSP
(
p_Identifier number := null,
p_RetVal out number,
p_DOCID NUMBER := NULL,
p_DOCData Blob := NULL,
p_CREATUSERID CHAR := NULL
)
as
begin
    savepoint DocFileUploadUpdTsp;
    if p_Identifier = 0 then
    begin
        update CDCDOCS.DocFileUploadTbl set
        DOCID = p_DOCID,
        DOCData = UTL_COMPRESS.lz_compress(p_DOCData),
        CREATUSERID = p_CREATUSERID,
        CreatDt = sysdate
        where DocId = p_DocId;        
        p_RetVal := SQL%ROWCOUNT;
    end;
    end if;
exception when others then
begin
    rollback to DocFileUploadUpdTsp;
    raise;
end;
end;

define deploy_name=Sep_ACH_Debit
define package_name=PROD_checks
define package_buildtime=20200824145051
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Sep_ACH_Debit_PROD_checks created on Mon 08/24/2020 14:50:52.52 by Jasleen Gorowada
prompt deploy scripts for deploy Sep_ACH_Debit_PROD_checks created on Mon 08/24/2020 14:50:52.52 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Sep_ACH_Debit_PROD_checks: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_in_PROD.sql 
Jasleenkaur Gorowada committed 027690e on Sun Aug 23 22:25:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script_for_deletion_PROD.sql 
Jasleenkaur Gorowada committed 027690e on Sun Aug 23 22:25:18 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_in_PROD.sql"
select LOANNMB,LOANDTLACHBNKACCT,LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL where LOANNMB in ('3161286006','6104094002');
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script_for_deletion_PROD.sql"
set echo on
set feedback on
select '*** testing to see if loan exisits for given Loan number(PROD)***' from dual;
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3722977009'        
 and rtrim(LOANDTLACHBNKACCT) ='30052671'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091900193';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3724177009'        
 and rtrim(LOANDTLACHBNKACCT) ='051133420'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3725187003'        
 and rtrim(LOANDTLACHBNKACCT) ='503517507'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3726377003'        
 and rtrim(LOANDTLACHBNKACCT) ='1400132078138'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='275979034';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3726647001'        
 and rtrim(LOANDTLACHBNKACCT) ='115008716'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091917351';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3729537009'        
 and rtrim(LOANDTLACHBNKACCT) ='1582987'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='075918017';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3731707000'        
 and rtrim(LOANDTLACHBNKACCT) ='862789'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='101108571';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3732477005'        
 and rtrim(LOANDTLACHBNKACCT) ='673405582'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='324172575';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3732867004'        
 and rtrim(LOANDTLACHBNKACCT) ='5001004919'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='062006505';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3733507003'        
 and rtrim(LOANDTLACHBNKACCT) ='1755413577'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='103000703';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3736197001'        
 and rtrim(LOANDTLACHBNKACCT) ='5795954790'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105320';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3736857009'        
 and rtrim(LOANDTLACHBNKACCT) ='389567030'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3737637004'        
 and rtrim(LOANDTLACHBNKACCT) ='5528010314'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3738347002'        
 and rtrim(LOANDTLACHBNKACCT) ='103105160'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086300012';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3739207008'        
 and rtrim(LOANDTLACHBNKACCT) ='502968156'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3740007004'        
 and rtrim(LOANDTLACHBNKACCT) ='61952153'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='283978425';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3742197008'        
 and rtrim(LOANDTLACHBNKACCT) ='0200302735'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='055003298';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3746357005'        
 and rtrim(LOANDTLACHBNKACCT) ='8118133472'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031000503';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3748117007'        
 and rtrim(LOANDTLACHBNKACCT) ='153569852681'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='125000105';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3758267010'        
 and rtrim(LOANDTLACHBNKACCT) ='21167258'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3755687001'        
 and rtrim(LOANDTLACHBNKACCT) ='1000515869'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='062206512';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3756677000'        
 and rtrim(LOANDTLACHBNKACCT) ='396161736'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3757267008'        
 and rtrim(LOANDTLACHBNKACCT) ='03200144131'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='211470225';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3764597000'        
 and rtrim(LOANDTLACHBNKACCT) ='955301601'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3762557006'        
 and rtrim(LOANDTLACHBNKACCT) ='270149987'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='026013958';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3763527010'        
 and rtrim(LOANDTLACHBNKACCT) ='446043123359'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='052001633';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3764197009'        
 and rtrim(LOANDTLACHBNKACCT) ='2401748081'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075900766';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3766707000'        
 and rtrim(LOANDTLACHBNKACCT) ='75362592'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='211170237';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3768627004'        
 and rtrim(LOANDTLACHBNKACCT) ='98086854'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3768927000'        
 and rtrim(LOANDTLACHBNKACCT) ='4286179100'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053902197';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3769557009'        
 and rtrim(LOANDTLACHBNKACCT) ='918000204'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091015143';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3770207006'        
 and rtrim(LOANDTLACHBNKACCT) ='5312552184'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042200910';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3772037007'        
 and rtrim(LOANDTLACHBNKACCT) ='0801004766'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='104001808';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3773567004'        
 and rtrim(LOANDTLACHBNKACCT) ='1361434312'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='255071981';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3776357006'        
 and rtrim(LOANDTLACHBNKACCT) ='64372382'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302325';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3782827001'        
 and rtrim(LOANDTLACHBNKACCT) ='500006108'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3778327001'        
 and rtrim(LOANDTLACHBNKACCT) ='66044670'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172238';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3779907000'        
 and rtrim(LOANDTLACHBNKACCT) ='058460403'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3780707007'        
 and rtrim(LOANDTLACHBNKACCT) ='7802127394'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043306826';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3802247002'        
 and rtrim(LOANDTLACHBNKACCT) ='003116034'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121142119';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3797357005'        
 and rtrim(LOANDTLACHBNKACCT) ='1110090356'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091915845';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3800237006'        
 and rtrim(LOANDTLACHBNKACCT) ='32040003924'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='123171955';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3801567002'        
 and rtrim(LOANDTLACHBNKACCT) ='1504052679'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='026013576';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3826157004'        
 and rtrim(LOANDTLACHBNKACCT) ='153153932923'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302150';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3804437004'        
 and rtrim(LOANDTLACHBNKACCT) ='1864320096'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3809387001'        
 and rtrim(LOANDTLACHBNKACCT) ='11135647'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='074901672';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3810537006'        
 and rtrim(LOANDTLACHBNKACCT) ='0100205430'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042212568';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3814527000'        
 and rtrim(LOANDTLACHBNKACCT) ='0964029'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091910196';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3816897010'        
 and rtrim(LOANDTLACHBNKACCT) ='41069988'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3821707004'        
 and rtrim(LOANDTLACHBNKACCT) ='610176171'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3825057007'        
 and rtrim(LOANDTLACHBNKACCT) ='8020965405'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122401778';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3828247000'        
 and rtrim(LOANDTLACHBNKACCT) ='2110003130'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081903867';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3832917003'        
 and rtrim(LOANDTLACHBNKACCT) ='505868296'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3841957006'        
 and rtrim(LOANDTLACHBNKACCT) ='1734921941'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3836377004'        
 and rtrim(LOANDTLACHBNKACCT) ='0082693939'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301028';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3836457004'        
 and rtrim(LOANDTLACHBNKACCT) ='99200821'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='074903670';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3837067007'        
 and rtrim(LOANDTLACHBNKACCT) ='1479736165'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3852437009'        
 and rtrim(LOANDTLACHBNKACCT) ='71566481'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='104902949';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3847837006'        
 and rtrim(LOANDTLACHBNKACCT) ='0100222213'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='074900657';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3847897002'        
 and rtrim(LOANDTLACHBNKACCT) ='1012668511'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061100606';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3848787006'        
 and rtrim(LOANDTLACHBNKACCT) ='9837097154'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3853597002'        
 and rtrim(LOANDTLACHBNKACCT) ='9243495'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121105156';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3861427008'        
 and rtrim(LOANDTLACHBNKACCT) ='00040037463'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='011200585';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3856367009'        
 and rtrim(LOANDTLACHBNKACCT) ='1333749'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065204443';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3857347005'        
 and rtrim(LOANDTLACHBNKACCT) ='601503565'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3860777006'        
 and rtrim(LOANDTLACHBNKACCT) ='20048130'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='291880330';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3861467401'        
 and rtrim(LOANDTLACHBNKACCT) ='10200001953004'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='291479592';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3861577007'        
 and rtrim(LOANDTLACHBNKACCT) ='8900103209'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302966';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3861627009'        
 and rtrim(LOANDTLACHBNKACCT) ='222260058'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3864497002'        
 and rtrim(LOANDTLACHBNKACCT) ='1776309998'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='283071788';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3866107005'        
 and rtrim(LOANDTLACHBNKACCT) ='1001991981'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238200';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3868317002'        
 and rtrim(LOANDTLACHBNKACCT) ='46035534'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3867157000'        
 and rtrim(LOANDTLACHBNKACCT) ='80216335'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124100857';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3867237000'        
 and rtrim(LOANDTLACHBNKACCT) ='600087365'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310521';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3868247005'        
 and rtrim(LOANDTLACHBNKACCT) ='751001874708'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324377202';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3875387010'        
 and rtrim(LOANDTLACHBNKACCT) ='722803829'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='324173626';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3876027009'        
 and rtrim(LOANDTLACHBNKACCT) ='6481984389'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172241';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3876197003'        
 and rtrim(LOANDTLACHBNKACCT) ='325176136'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3876407005'        
 and rtrim(LOANDTLACHBNKACCT) ='789430945996'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='325180977';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3889347010'        
 and rtrim(LOANDTLACHBNKACCT) ='751174758'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211589828';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3879557010'        
 and rtrim(LOANDTLACHBNKACCT) ='000065472475'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='264279567';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3884657010'        
 and rtrim(LOANDTLACHBNKACCT) ='80228087'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='111301737';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3888447003'        
 and rtrim(LOANDTLACHBNKACCT) ='2830004087'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171007';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3899457001'        
 and rtrim(LOANDTLACHBNKACCT) ='0011307361'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122041235';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3897647006'        
 and rtrim(LOANDTLACHBNKACCT) ='74000022967'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='231270353';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3898197004'        
 and rtrim(LOANDTLACHBNKACCT) ='220041743'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3898337009'        
 and rtrim(LOANDTLACHBNKACCT) ='015276902'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='123206914';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3899547004'        
 and rtrim(LOANDTLACHBNKACCT) ='1113617401'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075906171';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3900637005'        
 and rtrim(LOANDTLACHBNKACCT) ='080010390'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137726';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3902117006'        
 and rtrim(LOANDTLACHBNKACCT) ='900509922'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='096017382';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3904167003'        
 and rtrim(LOANDTLACHBNKACCT) ='4076672'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121107882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3905897001'        
 and rtrim(LOANDTLACHBNKACCT) ='1356278208'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='275071327';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3906587004'        
 and rtrim(LOANDTLACHBNKACCT) ='1071067864'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3909127404'        
 and rtrim(LOANDTLACHBNKACCT) ='440105706'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400172';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3916707007'        
 and rtrim(LOANDTLACHBNKACCT) ='7123117561'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031315036';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3913237002'        
 and rtrim(LOANDTLACHBNKACCT) ='0251061477'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051408949';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3913517003'        
 and rtrim(LOANDTLACHBNKACCT) ='1845043'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071108407';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3914657001'        
 and rtrim(LOANDTLACHBNKACCT) ='59252359'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='083001314';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3918157001'        
 and rtrim(LOANDTLACHBNKACCT) ='96293360'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='011500858';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3919047005'        
 and rtrim(LOANDTLACHBNKACCT) ='09140294'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124301025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3919617010'        
 and rtrim(LOANDTLACHBNKACCT) ='2104216870'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061205844';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3920007001'        
 and rtrim(LOANDTLACHBNKACCT) ='527070632'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3922187002'        
 and rtrim(LOANDTLACHBNKACCT) ='527018953'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3922447008'        
 and rtrim(LOANDTLACHBNKACCT) ='8512359187'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031000053';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3925897009'        
 and rtrim(LOANDTLACHBNKACCT) ='1025019405'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053208309';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3926747001'        
 and rtrim(LOANDTLACHBNKACCT) ='601401608'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3927877007'        
 and rtrim(LOANDTLACHBNKACCT) ='4062077'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='075902421';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3940327000'        
 and rtrim(LOANDTLACHBNKACCT) ='96333210'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='011500858';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3930067001'        
 and rtrim(LOANDTLACHBNKACCT) ='12000100020860'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='221979363';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3931677009'        
 and rtrim(LOANDTLACHBNKACCT) ='457031398567'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3933067007'        
 and rtrim(LOANDTLACHBNKACCT) ='1674282'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='074006674';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3938887005'        
 and rtrim(LOANDTLACHBNKACCT) ='2000032029'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='062203010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3940097003'        
 and rtrim(LOANDTLACHBNKACCT) ='415491'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='073922432';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3955677003'        
 and rtrim(LOANDTLACHBNKACCT) ='002000680229'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='321171731';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3950487006'        
 and rtrim(LOANDTLACHBNKACCT) ='3020006361'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122402382';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3954587009'        
 and rtrim(LOANDTLACHBNKACCT) ='766766'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='071123204';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3955617301'        
 and rtrim(LOANDTLACHBNKACCT) ='3200177859'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211470225';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3956877006'        
 and rtrim(LOANDTLACHBNKACCT) ='152637'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091812430';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3957327203'        
 and rtrim(LOANDTLACHBNKACCT) ='65011754'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='073922869';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3957367008'        
 and rtrim(LOANDTLACHBNKACCT) ='150192109'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='061104929';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3957757007'        
 and rtrim(LOANDTLACHBNKACCT) ='536986950'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3961327010'        
 and rtrim(LOANDTLACHBNKACCT) ='500529383'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071102568';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3961787008'        
 and rtrim(LOANDTLACHBNKACCT) ='519590191'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3962177004'        
 and rtrim(LOANDTLACHBNKACCT) ='702854'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='104913064';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3967037007'        
 and rtrim(LOANDTLACHBNKACCT) ='0000013728894'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='323075880';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3963437001'        
 and rtrim(LOANDTLACHBNKACCT) ='0211004917'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053103585';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3963737008'        
 and rtrim(LOANDTLACHBNKACCT) ='6446519757'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071001533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3965927010'        
 and rtrim(LOANDTLACHBNKACCT) ='1100000083573'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='291274108';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3967957001'        
 and rtrim(LOANDTLACHBNKACCT) ='325091439608'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3970377005'        
 and rtrim(LOANDTLACHBNKACCT) ='0111547592'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3972877006'        
 and rtrim(LOANDTLACHBNKACCT) ='21004304'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302325';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3973217009'        
 and rtrim(LOANDTLACHBNKACCT) ='0087890'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='082907134';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3973497001'        
 and rtrim(LOANDTLACHBNKACCT) ='50989'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='073914369';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3991657005'        
 and rtrim(LOANDTLACHBNKACCT) ='5571078642'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='231372691';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3980757005'        
 and rtrim(LOANDTLACHBNKACCT) ='0660000642625'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='324079115';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3981057009'        
 and rtrim(LOANDTLACHBNKACCT) ='232649100'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3986877007'        
 and rtrim(LOANDTLACHBNKACCT) ='4157293292'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='041000124';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3987667005'        
 and rtrim(LOANDTLACHBNKACCT) ='72014095'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='041209080';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3990747006'        
 and rtrim(LOANDTLACHBNKACCT) ='070059686'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='292970825';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3992237010'        
 and rtrim(LOANDTLACHBNKACCT) ='186314634'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='042309222';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3992857008'        
 and rtrim(LOANDTLACHBNKACCT) ='7905557793'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172241';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3993067009'        
 and rtrim(LOANDTLACHBNKACCT) ='8459641927'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3993777010'        
 and rtrim(LOANDTLACHBNKACCT) ='1830157444'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122045037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4003587008'        
 and rtrim(LOANDTLACHBNKACCT) ='1601442'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071926155';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3994977002'        
 and rtrim(LOANDTLACHBNKACCT) ='0201033569'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='302373011';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3996627006'        
 and rtrim(LOANDTLACHBNKACCT) ='1731403'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='104901652';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3997237009'        
 and rtrim(LOANDTLACHBNKACCT) ='5795024727'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105320';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4004657007'        
 and rtrim(LOANDTLACHBNKACCT) ='575253692'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4016227004'        
 and rtrim(LOANDTLACHBNKACCT) ='719883253'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4006447007'        
 and rtrim(LOANDTLACHBNKACCT) ='6199991974'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321270742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4010187004'        
 and rtrim(LOANDTLACHBNKACCT) ='7145042565'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4012527003'        
 and rtrim(LOANDTLACHBNKACCT) ='10000768077'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='211370558';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4013377008'        
 and rtrim(LOANDTLACHBNKACCT) ='6373397071'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071001533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4013797005'        
 and rtrim(LOANDTLACHBNKACCT) ='071455'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302788';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4020207001'        
 and rtrim(LOANDTLACHBNKACCT) ='000133398562'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='082900432';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4027157002'        
 and rtrim(LOANDTLACHBNKACCT) ='7001194964'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125108272';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4023367009'        
 and rtrim(LOANDTLACHBNKACCT) ='300717923'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='073903244';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4024007008'        
 and rtrim(LOANDTLACHBNKACCT) ='8100071409'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124002971';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4025037008'        
 and rtrim(LOANDTLACHBNKACCT) ='000027368114'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4028247007'        
 and rtrim(LOANDTLACHBNKACCT) ='12016614'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121125660';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4028657001'        
 and rtrim(LOANDTLACHBNKACCT) ='800184020'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044210403';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4029667006'        
 and rtrim(LOANDTLACHBNKACCT) ='7001194972'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125108272';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4030377010'        
 and rtrim(LOANDTLACHBNKACCT) ='6710097137'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4039707009'        
 and rtrim(LOANDTLACHBNKACCT) ='3700003787'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021406667';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4036447008'        
 and rtrim(LOANDTLACHBNKACCT) ='191001929'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400486';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4038037007'        
 and rtrim(LOANDTLACHBNKACCT) ='25005111'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310754';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4038157008'        
 and rtrim(LOANDTLACHBNKACCT) ='2653085399'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='103002691';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4040117006'        
 and rtrim(LOANDTLACHBNKACCT) ='7000097602'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122226076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4042637002'        
 and rtrim(LOANDTLACHBNKACCT) ='5049283'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121105156';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4042817008'        
 and rtrim(LOANDTLACHBNKACCT) ='210001681'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122242791';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4043277003'        
 and rtrim(LOANDTLACHBNKACCT) ='139625625'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086518477';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4043687008'        
 and rtrim(LOANDTLACHBNKACCT) ='2452010662'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107007074';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4044327007'        
 and rtrim(LOANDTLACHBNKACCT) ='1467537641'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125200057';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4050217005'        
 and rtrim(LOANDTLACHBNKACCT) ='9929819837'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='082900872';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4046547007'        
 and rtrim(LOANDTLACHBNKACCT) ='062598701'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4047817007'        
 and rtrim(LOANDTLACHBNKACCT) ='4820112300'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='065301948';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4050007001'        
 and rtrim(LOANDTLACHBNKACCT) ='8000135780'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4051547001'        
 and rtrim(LOANDTLACHBNKACCT) ='100024599'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071102568';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4060217009'        
 and rtrim(LOANDTLACHBNKACCT) ='5200197242'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053101121';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4055267008'        
 and rtrim(LOANDTLACHBNKACCT) ='826421'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='075906346';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4055697008'        
 and rtrim(LOANDTLACHBNKACCT) ='1118854'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400525';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4059617003'        
 and rtrim(LOANDTLACHBNKACCT) ='5131024423'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105278';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4075977006'        
 and rtrim(LOANDTLACHBNKACCT) ='71504140'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='101001306';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4069317000'        
 and rtrim(LOANDTLACHBNKACCT) ='384364'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='086507187';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4069637002'        
 and rtrim(LOANDTLACHBNKACCT) ='111052505'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121135045';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4070717010'        
 and rtrim(LOANDTLACHBNKACCT) ='1145629'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='041212983';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4074027001'        
 and rtrim(LOANDTLACHBNKACCT) ='139375947'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086518477';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4074307002'        
 and rtrim(LOANDTLACHBNKACCT) ='1893588657'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4077967007'        
 and rtrim(LOANDTLACHBNKACCT) ='2050021071'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091971533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4078237002'        
 and rtrim(LOANDTLACHBNKACCT) ='004727741'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122100024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4084247001'        
 and rtrim(LOANDTLACHBNKACCT) ='525405'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091915751';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4081397005'        
 and rtrim(LOANDTLACHBNKACCT) ='1020028380'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='055002367';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4082267003'        
 and rtrim(LOANDTLACHBNKACCT) ='8000124401'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4084067006'        
 and rtrim(LOANDTLACHBNKACCT) ='1001025848'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238200';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4084297005'        
 and rtrim(LOANDTLACHBNKACCT) ='325132578211'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4085647005'        
 and rtrim(LOANDTLACHBNKACCT) ='8043861'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091902023';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4087397006'        
 and rtrim(LOANDTLACHBNKACCT) ='060406659'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4089107004'        
 and rtrim(LOANDTLACHBNKACCT) ='252976228'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4095547003'        
 and rtrim(LOANDTLACHBNKACCT) ='330011644'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171007';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4090077005'        
 and rtrim(LOANDTLACHBNKACCT) ='227424'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302788';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4090097000'        
 and rtrim(LOANDTLACHBNKACCT) ='601380114'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4091957000'        
 and rtrim(LOANDTLACHBNKACCT) ='4034263'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='011302742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4095707003'        
 and rtrim(LOANDTLACHBNKACCT) ='536817908'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4098057004'        
 and rtrim(LOANDTLACHBNKACCT) ='157518908684'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121122676';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4100777008'        
 and rtrim(LOANDTLACHBNKACCT) ='000877041441'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4100267008'        
 and rtrim(LOANDTLACHBNKACCT) ='4372531389'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031201360';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4100647004'        
 and rtrim(LOANDTLACHBNKACCT) ='1514511557'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4100687005'        
 and rtrim(LOANDTLACHBNKACCT) ='126084'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='075903161';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4101777010'        
 and rtrim(LOANDTLACHBNKACCT) ='5797553327'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105320';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4104727001'        
 and rtrim(LOANDTLACHBNKACCT) ='6520896942'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310521';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4105517010'        
 and rtrim(LOANDTLACHBNKACCT) ='2101209985'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='082907273';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4107597005'        
 and rtrim(LOANDTLACHBNKACCT) ='060669868'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4107617009'        
 and rtrim(LOANDTLACHBNKACCT) ='9000303265'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221272439';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4109407009'        
 and rtrim(LOANDTLACHBNKACCT) ='0003191826'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105498';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4110147000'        
 and rtrim(LOANDTLACHBNKACCT) ='3807809145'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925444';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4111427003'        
 and rtrim(LOANDTLACHBNKACCT) ='601399760'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4111807010'        
 and rtrim(LOANDTLACHBNKACCT) ='019614506'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4119257005'        
 and rtrim(LOANDTLACHBNKACCT) ='9872464281'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101000695';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4114247003'        
 and rtrim(LOANDTLACHBNKACCT) ='407022016'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4114827000'        
 and rtrim(LOANDTLACHBNKACCT) ='16087701'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='063114577';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4115857000'        
 and rtrim(LOANDTLACHBNKACCT) ='6294565687'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071001533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4119277000'        
 and rtrim(LOANDTLACHBNKACCT) ='7945881220'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4119647004'        
 and rtrim(LOANDTLACHBNKACCT) ='20110003549'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='091214627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4119817007'        
 and rtrim(LOANDTLACHBNKACCT) ='880639943'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211370082';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4120377003'        
 and rtrim(LOANDTLACHBNKACCT) ='0031005411'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='055001070';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4122787001'        
 and rtrim(LOANDTLACHBNKACCT) ='110277001'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071922175';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4123027009'        
 and rtrim(LOANDTLACHBNKACCT) ='2062101673'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053102586';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4123067010'        
 and rtrim(LOANDTLACHBNKACCT) ='600867136'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4123297009'        
 and rtrim(LOANDTLACHBNKACCT) ='111014433'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400486';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4123617009'        
 and rtrim(LOANDTLACHBNKACCT) ='3709220770'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4124977003'        
 and rtrim(LOANDTLACHBNKACCT) ='586081520'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4129747003'        
 and rtrim(LOANDTLACHBNKACCT) ='5796177037'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121002042';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4126527001'        
 and rtrim(LOANDTLACHBNKACCT) ='597736351'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4126737005'        
 and rtrim(LOANDTLACHBNKACCT) ='6180108'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091914202';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4129527410'        
 and rtrim(LOANDTLACHBNKACCT) ='02301919'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='123206338';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4133577003'        
 and rtrim(LOANDTLACHBNKACCT) ='80007057492'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='321081669';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4131647007'        
 and rtrim(LOANDTLACHBNKACCT) ='1470002324385'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='051404260';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4131877006'        
 and rtrim(LOANDTLACHBNKACCT) ='1400133512945'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='275979034';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4131997007'        
 and rtrim(LOANDTLACHBNKACCT) ='325134969624'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4134707007'        
 and rtrim(LOANDTLACHBNKACCT) ='1697606'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='074006674';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4135247002'        
 and rtrim(LOANDTLACHBNKACCT) ='4327970623'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='036001808';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4135937008'        
 and rtrim(LOANDTLACHBNKACCT) ='00121141007504'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='124101555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4136137006'        
 and rtrim(LOANDTLACHBNKACCT) ='9190650052'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371641';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4136617008'        
 and rtrim(LOANDTLACHBNKACCT) ='8000149072'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4137387002'        
 and rtrim(LOANDTLACHBNKACCT) ='457041628117'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4137467002'        
 and rtrim(LOANDTLACHBNKACCT) ='9371907'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='211383901';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4139837010'        
 and rtrim(LOANDTLACHBNKACCT) ='1049206952'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4138637007'        
 and rtrim(LOANDTLACHBNKACCT) ='0094030835'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031301422';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4138797009'        
 and rtrim(LOANDTLACHBNKACCT) ='7960010416'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011304478';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4139197008'        
 and rtrim(LOANDTLACHBNKACCT) ='004000064266'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301772';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4140627003'        
 and rtrim(LOANDTLACHBNKACCT) ='8026416'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='042106580';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4140827004'        
 and rtrim(LOANDTLACHBNKACCT) ='157520185362'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122235821';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4140907004'        
 and rtrim(LOANDTLACHBNKACCT) ='0240090902'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302966';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4141797010'        
 and rtrim(LOANDTLACHBNKACCT) ='104786338749'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='091000022';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4143237010'        
 and rtrim(LOANDTLACHBNKACCT) ='852 533'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071900993';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4143557001'        
 and rtrim(LOANDTLACHBNKACCT) ='1307603646'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031306278';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4146017009'        
 and rtrim(LOANDTLACHBNKACCT) ='902848944'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4146307002'        
 and rtrim(LOANDTLACHBNKACCT) ='9073288293'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051400549';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4146707004'        
 and rtrim(LOANDTLACHBNKACCT) ='519831876'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4146877009'        
 and rtrim(LOANDTLACHBNKACCT) ='6618147638'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4146897004'        
 and rtrim(LOANDTLACHBNKACCT) ='777192670'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211070120';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4150607001'        
 and rtrim(LOANDTLACHBNKACCT) ='1001166964'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238200';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4152137006'        
 and rtrim(LOANDTLACHBNKACCT) ='8628002688'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4152397003'        
 and rtrim(LOANDTLACHBNKACCT) ='2050020941'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091971533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4156707008'        
 and rtrim(LOANDTLACHBNKACCT) ='5889569165'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4152727006'        
 and rtrim(LOANDTLACHBNKACCT) ='0170013809'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122402243';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4153437004'        
 and rtrim(LOANDTLACHBNKACCT) ='7594264017'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4155407805'        
 and rtrim(LOANDTLACHBNKACCT) ='456007635'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4158317002'        
 and rtrim(LOANDTLACHBNKACCT) ='4694801635'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4158407005'        
 and rtrim(LOANDTLACHBNKACCT) ='953312941'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011002343';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4158567007'        
 and rtrim(LOANDTLACHBNKACCT) ='7900037828'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='286573335';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4159477006'        
 and rtrim(LOANDTLACHBNKACCT) ='0230240577'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925402';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4160397003'        
 and rtrim(LOANDTLACHBNKACCT) ='4040901'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='011302742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4161967010'        
 and rtrim(LOANDTLACHBNKACCT) ='8379930566'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4164427007'        
 and rtrim(LOANDTLACHBNKACCT) ='00237167'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122244029';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4169347006'        
 and rtrim(LOANDTLACHBNKACCT) ='157502471301'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122235821';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4166407005'        
 and rtrim(LOANDTLACHBNKACCT) ='0165020180'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121142407';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4168417001'        
 and rtrim(LOANDTLACHBNKACCT) ='40000406864'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='071900760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4168787007'        
 and rtrim(LOANDTLACHBNKACCT) ='001106601867'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4169707007'        
 and rtrim(LOANDTLACHBNKACCT) ='2025898'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='061120547';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4169757000'        
 and rtrim(LOANDTLACHBNKACCT) ='7900026859'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071923349';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4172087001'        
 and rtrim(LOANDTLACHBNKACCT) ='017726'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091908412';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4172507007'        
 and rtrim(LOANDTLACHBNKACCT) ='0086720315'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103799';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4172547008'        
 and rtrim(LOANDTLACHBNKACCT) ='001064558024'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122037760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4173897010'        
 and rtrim(LOANDTLACHBNKACCT) ='8807921096'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125200057';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4173947001'        
 and rtrim(LOANDTLACHBNKACCT) ='501011806392'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324079555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4174217007'        
 and rtrim(LOANDTLACHBNKACCT) ='981889801'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4174987004'        
 and rtrim(LOANDTLACHBNKACCT) ='0601615463'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4175017008'        
 and rtrim(LOANDTLACHBNKACCT) ='063099626'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4175137009'        
 and rtrim(LOANDTLACHBNKACCT) ='6400339285'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122041235';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4176227003'        
 and rtrim(LOANDTLACHBNKACCT) ='1128876'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400525';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4176237006'        
 and rtrim(LOANDTLACHBNKACCT) ='11173599'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371120';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4180007010'        
 and rtrim(LOANDTLACHBNKACCT) ='591866030'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4176627005'        
 and rtrim(LOANDTLACHBNKACCT) ='1301840904'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137027';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4179027008'        
 and rtrim(LOANDTLACHBNKACCT) ='8118133472'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031000503';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4179727006'        
 and rtrim(LOANDTLACHBNKACCT) ='100014573'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091206101';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4180497006'        
 and rtrim(LOANDTLACHBNKACCT) ='402190426'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4181197001'        
 and rtrim(LOANDTLACHBNKACCT) ='4690816266'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4181227008'        
 and rtrim(LOANDTLACHBNKACCT) ='48355749'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122242869';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4181757003'        
 and rtrim(LOANDTLACHBNKACCT) ='1016058668'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='074903719';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4181857009'        
 and rtrim(LOANDTLACHBNKACCT) ='8007014072'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4182067010'        
 and rtrim(LOANDTLACHBNKACCT) ='1000043289'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='056009356';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4182107009'        
 and rtrim(LOANDTLACHBNKACCT) ='11033727'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302613';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4184057000'        
 and rtrim(LOANDTLACHBNKACCT) ='0212125168'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051408949';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4185367001'        
 and rtrim(LOANDTLACHBNKACCT) ='0070012505'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302966';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4186257005'        
 and rtrim(LOANDTLACHBNKACCT) ='702707303'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4186437000'        
 and rtrim(LOANDTLACHBNKACCT) ='200028629'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='053112466';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4187687007'        
 and rtrim(LOANDTLACHBNKACCT) ='1001210119'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221272303';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4187767007'        
 and rtrim(LOANDTLACHBNKACCT) ='325107991166'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4187807006'        
 and rtrim(LOANDTLACHBNKACCT) ='15019659'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122203950';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4188077003'        
 and rtrim(LOANDTLACHBNKACCT) ='127890145'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4190477004'        
 and rtrim(LOANDTLACHBNKACCT) ='000760915304'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4191057009'        
 and rtrim(LOANDTLACHBNKACCT) ='488092702255'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4191147001'        
 and rtrim(LOANDTLACHBNKACCT) ='9222950090'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105278';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4191337010'        
 and rtrim(LOANDTLACHBNKACCT) ='2552058402'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='072410013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4191367008'        
 and rtrim(LOANDTLACHBNKACCT) ='61068501'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='081506646';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4192847001'        
 and rtrim(LOANDTLACHBNKACCT) ='83220701'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='081506646';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4193287001'        
 and rtrim(LOANDTLACHBNKACCT) ='0846317'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='083902581';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4193687003'        
 and rtrim(LOANDTLACHBNKACCT) ='2010000664355'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='086500605';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4195737009'        
 and rtrim(LOANDTLACHBNKACCT) ='7592440083'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211384214';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4195557003'        
 and rtrim(LOANDTLACHBNKACCT) ='5559099323'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4195607005'        
 and rtrim(LOANDTLACHBNKACCT) ='601005009'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4195727006'        
 and rtrim(LOANDTLACHBNKACCT) ='6500623811'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172186';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4196517004'        
 and rtrim(LOANDTLACHBNKACCT) ='595265601'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4197467004'        
 and rtrim(LOANDTLACHBNKACCT) ='4686911172'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4197917008'        
 and rtrim(LOANDTLACHBNKACCT) ='2270005321'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371447';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4198557009'        
 and rtrim(LOANDTLACHBNKACCT) ='90004519'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='071920300';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4198687002'        
 and rtrim(LOANDTLACHBNKACCT) ='1030041642'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121201814';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4198717009'        
 and rtrim(LOANDTLACHBNKACCT) ='5590003575'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4198737004'        
 and rtrim(LOANDTLACHBNKACCT) ='551047930'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='103003632';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4199497006'        
 and rtrim(LOANDTLACHBNKACCT) ='2653090104'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='103002691';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4200277008'        
 and rtrim(LOANDTLACHBNKACCT) ='150000797791'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='211574642';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4200757010'        
 and rtrim(LOANDTLACHBNKACCT) ='3500018753'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071026576';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4200827007'        
 and rtrim(LOANDTLACHBNKACCT) ='153757809840'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121201694';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4200947008'        
 and rtrim(LOANDTLACHBNKACCT) ='40022825984'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271724';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201027005'        
 and rtrim(LOANDTLACHBNKACCT) ='42025538044'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271724';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201037008'        
 and rtrim(LOANDTLACHBNKACCT) ='8342873414'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105278';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201147006'        
 and rtrim(LOANDTLACHBNKACCT) ='166760944'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371227';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201187007'        
 and rtrim(LOANDTLACHBNKACCT) ='758018092'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201237009'        
 and rtrim(LOANDTLACHBNKACCT) ='7000076232'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071026628';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201247001'        
 and rtrim(LOANDTLACHBNKACCT) ='412148800'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4201627008'        
 and rtrim(LOANDTLACHBNKACCT) ='457041628117'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2858207006'        
 and rtrim(LOANDTLACHBNKACCT) ='7115016'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='107000372';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3298617006'        
 and rtrim(LOANDTLACHBNKACCT) ='8257295193'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211370545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3302977307'        
 and rtrim(LOANDTLACHBNKACCT) ='325107999838'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3306797200'        
 and rtrim(LOANDTLACHBNKACCT) ='1895265922'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137522';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3307157002'        
 and rtrim(LOANDTLACHBNKACCT) ='539670502'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122100024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3313537201'        
 and rtrim(LOANDTLACHBNKACCT) ='2804065216'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='082907273';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3319277207'        
 and rtrim(LOANDTLACHBNKACCT) ='610270664'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071901604';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3324747004'        
 and rtrim(LOANDTLACHBNKACCT) ='932985570'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3335237402'        
 and rtrim(LOANDTLACHBNKACCT) ='923029193'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='096010415';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3395897000'        
 and rtrim(LOANDTLACHBNKACCT) ='8000049051'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3396047005'        
 and rtrim(LOANDTLACHBNKACCT) ='358765961'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3447027207'        
 and rtrim(LOANDTLACHBNKACCT) ='0730502929'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063112786';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3447417206'        
 and rtrim(LOANDTLACHBNKACCT) ='9863175866'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031302955';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3447637202'        
 and rtrim(LOANDTLACHBNKACCT) ='207496407'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271724';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3448187200'        
 and rtrim(LOANDTLACHBNKACCT) ='1711091790'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124100417';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3449157204'        
 and rtrim(LOANDTLACHBNKACCT) ='153758760919'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121201694';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3451887210'        
 and rtrim(LOANDTLACHBNKACCT) ='325116980252'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3454527202'        
 and rtrim(LOANDTLACHBNKACCT) ='5827919787'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107002192';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3455747200'        
 and rtrim(LOANDTLACHBNKACCT) ='0000258317874'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='051404260';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3457277205'        
 and rtrim(LOANDTLACHBNKACCT) ='10000556598'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='226070306';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3457307201'        
 and rtrim(LOANDTLACHBNKACCT) ='900102942'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400525';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3457487209'        
 and rtrim(LOANDTLACHBNKACCT) ='0016002445'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121142119';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3457777202'        
 and rtrim(LOANDTLACHBNKACCT) ='2522539'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='067016574';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3487337004'        
 and rtrim(LOANDTLACHBNKACCT) ='2200002212214'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='102106569';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3492217008'        
 and rtrim(LOANDTLACHBNKACCT) ='0300009388'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071926731';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3492607410'        
 and rtrim(LOANDTLACHBNKACCT) ='1202108652'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124100417';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3566317007'        
 and rtrim(LOANDTLACHBNKACCT) ='104510599'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='102305098';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3566797000'        
 and rtrim(LOANDTLACHBNKACCT) ='1109839'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400525';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3569917005'        
 and rtrim(LOANDTLACHBNKACCT) ='6802166'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='073922432';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3570537006'        
 and rtrim(LOANDTLACHBNKACCT) ='325105894207'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3570607003'        
 and rtrim(LOANDTLACHBNKACCT) ='161104665'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086503424';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3571837004'        
 and rtrim(LOANDTLACHBNKACCT) ='5606085324'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3572677006'        
 and rtrim(LOANDTLACHBNKACCT) ='0056806004409'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3573937003'        
 and rtrim(LOANDTLACHBNKACCT) ='370963982'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3576787001'        
 and rtrim(LOANDTLACHBNKACCT) ='00004343050'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='103100881';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3628177009'        
 and rtrim(LOANDTLACHBNKACCT) ='325123351609'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3635567006'        
 and rtrim(LOANDTLACHBNKACCT) ='1643840'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='074006674';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3637487010'        
 and rtrim(LOANDTLACHBNKACCT) ='840985347'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211372239';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3639167001'        
 and rtrim(LOANDTLACHBNKACCT) ='1064388416'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3652017007'        
 and rtrim(LOANDTLACHBNKACCT) ='6003784'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='103100959';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3660997008'        
 and rtrim(LOANDTLACHBNKACCT) ='730839897'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='107000262';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3661017009'        
 and rtrim(LOANDTLACHBNKACCT) ='8022006608'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3661037004'        
 and rtrim(LOANDTLACHBNKACCT) ='1976028'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='083000564';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3661137010'        
 and rtrim(LOANDTLACHBNKACCT) ='325123063926'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3662817004'        
 and rtrim(LOANDTLACHBNKACCT) ='190002603306'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324377613';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4260467008'        
 and rtrim(LOANDTLACHBNKACCT) ='602180967'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4202247003'        
 and rtrim(LOANDTLACHBNKACCT) ='231176748'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4202667000'        
 and rtrim(LOANDTLACHBNKACCT) ='7514845968'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071926184';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4203867003'        
 and rtrim(LOANDTLACHBNKACCT) ='325120350438'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1062867005'        
 and rtrim(LOANDTLACHBNKACCT) ='4815290733'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071025661';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1102387009'        
 and rtrim(LOANDTLACHBNKACCT) ='933022712'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1201527401'        
 and rtrim(LOANDTLACHBNKACCT) ='11150728'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='074901672';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1322087407'        
 and rtrim(LOANDTLACHBNKACCT) ='57757798'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301390';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1396307404'        
 and rtrim(LOANDTLACHBNKACCT) ='7900038731'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071923349';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1452808000'        
 and rtrim(LOANDTLACHBNKACCT) ='002456907636'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1498927109'        
 and rtrim(LOANDTLACHBNKACCT) ='741831747'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1514397400'        
 and rtrim(LOANDTLACHBNKACCT) ='1676175768'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081009428';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1533047406'        
 and rtrim(LOANDTLACHBNKACCT) ='746007479041'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324377516';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1540118005'        
 and rtrim(LOANDTLACHBNKACCT) ='8100289829'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071901604';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1540587010'        
 and rtrim(LOANDTLACHBNKACCT) ='1430014033'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121101037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1575248009'        
 and rtrim(LOANDTLACHBNKACCT) ='2523030860'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122187445';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1588537800'        
 and rtrim(LOANDTLACHBNKACCT) ='0210012687'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061102400';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1656427010'        
 and rtrim(LOANDTLACHBNKACCT) ='1080053752'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1660667102'        
 and rtrim(LOANDTLACHBNKACCT) ='612757101'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1732617000'        
 and rtrim(LOANDTLACHBNKACCT) ='629651078305'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='067011760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1791817110'        
 and rtrim(LOANDTLACHBNKACCT) ='40079014'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='324377516';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1871687108'        
 and rtrim(LOANDTLACHBNKACCT) ='062839121'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1888867404'        
 and rtrim(LOANDTLACHBNKACCT) ='1200429396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='271291017';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='1917797407'        
 and rtrim(LOANDTLACHBNKACCT) ='1055862'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121144696';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2010377809'        
 and rtrim(LOANDTLACHBNKACCT) ='2910422852'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='114912275';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2012828007'        
 and rtrim(LOANDTLACHBNKACCT) ='5831431'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='101114303';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2027427807'        
 and rtrim(LOANDTLACHBNKACCT) ='510484'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091301585';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2090837009'        
 and rtrim(LOANDTLACHBNKACCT) ='0233161651'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051408949';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2162596002'        
 and rtrim(LOANDTLACHBNKACCT) ='16248'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='104902392';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2244297801'        
 and rtrim(LOANDTLACHBNKACCT) ='202010591'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121144340';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2256597010'        
 and rtrim(LOANDTLACHBNKACCT) ='1222146173'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='267084199';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2328637009'        
 and rtrim(LOANDTLACHBNKACCT) ='10023238'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='066009456';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2333387000'        
 and rtrim(LOANDTLACHBNKACCT) ='9361205801'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122106015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2333607005'        
 and rtrim(LOANDTLACHBNKACCT) ='9857381538'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031302955';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2338687006'        
 and rtrim(LOANDTLACHBNKACCT) ='006349'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091204080';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2396857105'        
 and rtrim(LOANDTLACHBNKACCT) ='0002967987'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071006486';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2405327004'        
 and rtrim(LOANDTLACHBNKACCT) ='0000000201444'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='042102160';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2408947008'        
 and rtrim(LOANDTLACHBNKACCT) ='902722623'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='072000326';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2489817005'        
 and rtrim(LOANDTLACHBNKACCT) ='0007390220'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301028';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2493127002'        
 and rtrim(LOANDTLACHBNKACCT) ='394005739383'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='011500010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2522467010'        
 and rtrim(LOANDTLACHBNKACCT) ='832511188'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371078';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2554987007'        
 and rtrim(LOANDTLACHBNKACCT) ='163071561266'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='091300023';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2561957007'        
 and rtrim(LOANDTLACHBNKACCT) ='9872376633'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101000695';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2568087009'        
 and rtrim(LOANDTLACHBNKACCT) ='0011222097'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211170101';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2583927010'        
 and rtrim(LOANDTLACHBNKACCT) ='769681094082'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='307070267';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2585337003'        
 and rtrim(LOANDTLACHBNKACCT) ='8260834281'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321270742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2591497001'        
 and rtrim(LOANDTLACHBNKACCT) ='0006056'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='263184488';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2598937408'        
 and rtrim(LOANDTLACHBNKACCT) ='129681026230'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='124101555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2599167001'        
 and rtrim(LOANDTLACHBNKACCT) ='591217000'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='022300173';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2599937007'        
 and rtrim(LOANDTLACHBNKACCT) ='31029002'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302503';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2624407004'        
 and rtrim(LOANDTLACHBNKACCT) ='11981161242787'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='221380790';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2626307002'        
 and rtrim(LOANDTLACHBNKACCT) ='858000082348'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='081904808';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2631707009'        
 and rtrim(LOANDTLACHBNKACCT) ='59406004446'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2639507002'        
 and rtrim(LOANDTLACHBNKACCT) ='7921950700'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='041002711';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2644057003'        
 and rtrim(LOANDTLACHBNKACCT) ='280569218'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021202337';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2647547003'        
 and rtrim(LOANDTLACHBNKACCT) ='325074'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='086507187';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2673367001'        
 and rtrim(LOANDTLACHBNKACCT) ='501018136805'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122400724';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2687677001'        
 and rtrim(LOANDTLACHBNKACCT) ='01153458126'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='072403473';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2688667000'        
 and rtrim(LOANDTLACHBNKACCT) ='4040619'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='103101628';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2688847006'        
 and rtrim(LOANDTLACHBNKACCT) ='754228646'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071001533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2725947001'        
 and rtrim(LOANDTLACHBNKACCT) ='8513079444'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2736347004'        
 and rtrim(LOANDTLACHBNKACCT) ='01060123971'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2739427010'        
 and rtrim(LOANDTLACHBNKACCT) ='4340544108'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005047';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2794697002'        
 and rtrim(LOANDTLACHBNKACCT) ='905992785'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2795787007'        
 and rtrim(LOANDTLACHBNKACCT) ='0070040569'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105320';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2797997004'        
 and rtrim(LOANDTLACHBNKACCT) ='329681298629'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021300077';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2798027008'        
 and rtrim(LOANDTLACHBNKACCT) ='329681298629'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021300077';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2801377010'        
 and rtrim(LOANDTLACHBNKACCT) ='13946125'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091408734';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2807757900'        
 and rtrim(LOANDTLACHBNKACCT) ='527558982'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2814197003'        
 and rtrim(LOANDTLACHBNKACCT) ='0028080'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005953';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2822737010'        
 and rtrim(LOANDTLACHBNKACCT) ='50277980'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091300159';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2830907002'        
 and rtrim(LOANDTLACHBNKACCT) ='520005828'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='281573259';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2835747905'        
 and rtrim(LOANDTLACHBNKACCT) ='457024824925'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2839907009'        
 and rtrim(LOANDTLACHBNKACCT) ='8102092966'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321370765';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2849017409'        
 and rtrim(LOANDTLACHBNKACCT) ='027125954'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2861087008'        
 and rtrim(LOANDTLACHBNKACCT) ='123028023'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086501578';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2874207006'        
 and rtrim(LOANDTLACHBNKACCT) ='1116234878'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925460';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2875787007'        
 and rtrim(LOANDTLACHBNKACCT) ='67100438'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='031301066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2885477001'        
 and rtrim(LOANDTLACHBNKACCT) ='3016302963'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='083000108';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2913547410'        
 and rtrim(LOANDTLACHBNKACCT) ='189427782'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137522';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2913867009'        
 and rtrim(LOANDTLACHBNKACCT) ='42289249'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='061207839';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2918177002'        
 and rtrim(LOANDTLACHBNKACCT) ='0000153196478'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='051404260';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2918497004'        
 and rtrim(LOANDTLACHBNKACCT) ='1102339514'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100621';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2920407009'        
 and rtrim(LOANDTLACHBNKACCT) ='1303145190'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124100417';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2923347010'        
 and rtrim(LOANDTLACHBNKACCT) ='8000052141'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2946957008'        
 and rtrim(LOANDTLACHBNKACCT) ='50028731'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='041209080';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2952167003'        
 and rtrim(LOANDTLACHBNKACCT) ='7024944220'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042000314';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2963497003'        
 and rtrim(LOANDTLACHBNKACCT) ='00569240'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='083903140';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2982487006'        
 and rtrim(LOANDTLACHBNKACCT) ='95614656'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='043318092';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2991747907'        
 and rtrim(LOANDTLACHBNKACCT) ='5311797806'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042200910';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3005827007'        
 and rtrim(LOANDTLACHBNKACCT) ='981092661'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3035107004'        
 and rtrim(LOANDTLACHBNKACCT) ='016782'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091401553';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3037107008'        
 and rtrim(LOANDTLACHBNKACCT) ='0003540846'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000288';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3038877007'        
 and rtrim(LOANDTLACHBNKACCT) ='4355234009'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053902197';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3041337010'        
 and rtrim(LOANDTLACHBNKACCT) ='309167135'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='072000326';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3051207010'        
 and rtrim(LOANDTLACHBNKACCT) ='050003399'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122244333';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3051287001'        
 and rtrim(LOANDTLACHBNKACCT) ='470031'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='073921161';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3051327000'        
 and rtrim(LOANDTLACHBNKACCT) ='470031'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='073921161';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3051477010'        
 and rtrim(LOANDTLACHBNKACCT) ='59126870'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='261170371';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3054897002'        
 and rtrim(LOANDTLACHBNKACCT) ='1339861655'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211070175';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3060657007'        
 and rtrim(LOANDTLACHBNKACCT) ='001064539798'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122037760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3067107009'        
 and rtrim(LOANDTLACHBNKACCT) ='4464927'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371337';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3067367006'        
 and rtrim(LOANDTLACHBNKACCT) ='102029903'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071902878';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3072667007'        
 and rtrim(LOANDTLACHBNKACCT) ='0160110641'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='242272463';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3075127004'        
 and rtrim(LOANDTLACHBNKACCT) ='1549260'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='074006674';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3080887003'        
 and rtrim(LOANDTLACHBNKACCT) ='1110345409'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091901202';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3100037004'        
 and rtrim(LOANDTLACHBNKACCT) ='286870628'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122100024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3104707001'        
 and rtrim(LOANDTLACHBNKACCT) ='12000093097'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='096001013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3115257003'        
 and rtrim(LOANDTLACHBNKACCT) ='3047776'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000288';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3117857010'        
 and rtrim(LOANDTLACHBNKACCT) ='862688517'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3140677209'        
 and rtrim(LOANDTLACHBNKACCT) ='20032620'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='073922869';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3140877210'        
 and rtrim(LOANDTLACHBNKACCT) ='022483374'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3142257205'        
 and rtrim(LOANDTLACHBNKACCT) ='402192666'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3159737007'        
 and rtrim(LOANDTLACHBNKACCT) ='5591244528'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005047';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3165187200'        
 and rtrim(LOANDTLACHBNKACCT) ='980850655'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3168277209'        
 and rtrim(LOANDTLACHBNKACCT) ='2200000886978'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103773';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3169237210'        
 and rtrim(LOANDTLACHBNKACCT) ='334063960348'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='061000052';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3170457007'        
 and rtrim(LOANDTLACHBNKACCT) ='220763657'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3171087201'        
 and rtrim(LOANDTLACHBNKACCT) ='1011108204'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='053202208';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3187127009'        
 and rtrim(LOANDTLACHBNKACCT) ='9005180228'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211871691';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3196837001'        
 and rtrim(LOANDTLACHBNKACCT) ='00509075'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091300159';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3198137105'        
 and rtrim(LOANDTLACHBNKACCT) ='611287308'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3223437104'        
 and rtrim(LOANDTLACHBNKACCT) ='1200121179402'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='042207308';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3227717004'        
 and rtrim(LOANDTLACHBNKACCT) ='271258839'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3238297004'        
 and rtrim(LOANDTLACHBNKACCT) ='857235019'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3255127001'        
 and rtrim(LOANDTLACHBNKACCT) ='8944557229'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105980';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3256067205'        
 and rtrim(LOANDTLACHBNKACCT) ='2913204299'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075900575';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3261157202'        
 and rtrim(LOANDTLACHBNKACCT) ='100000927903'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122287251';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3262897203'        
 and rtrim(LOANDTLACHBNKACCT) ='1001920964'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221272303';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3279297000'        
 and rtrim(LOANDTLACHBNKACCT) ='20001814834'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='265270413';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3335917002'        
 and rtrim(LOANDTLACHBNKACCT) ='00200316537'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='055003298';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3348587009'        
 and rtrim(LOANDTLACHBNKACCT) ='2283034557'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075900575';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3359977003'        
 and rtrim(LOANDTLACHBNKACCT) ='8407070816'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031000053';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3362867202'        
 and rtrim(LOANDTLACHBNKACCT) ='1016283092'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='074903719';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3375417010'        
 and rtrim(LOANDTLACHBNKACCT) ='507685912'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122100024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3398977006'        
 and rtrim(LOANDTLACHBNKACCT) ='932985570'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3405637008'        
 and rtrim(LOANDTLACHBNKACCT) ='100560'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091907125';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3479077009'        
 and rtrim(LOANDTLACHBNKACCT) ='873511823'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3485677204'        
 and rtrim(LOANDTLACHBNKACCT) ='5790373400'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121002042';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3499037005'        
 and rtrim(LOANDTLACHBNKACCT) ='001723945'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='104901652';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3499347004'        
 and rtrim(LOANDTLACHBNKACCT) ='1894819307'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137522';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3501377002'        
 and rtrim(LOANDTLACHBNKACCT) ='150000720354'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='211574642';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3518047004'        
 and rtrim(LOANDTLACHBNKACCT) ='00224693'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122244029';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3518837005'        
 and rtrim(LOANDTLACHBNKACCT) ='370533652'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3521537010'        
 and rtrim(LOANDTLACHBNKACCT) ='8253193408'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211370545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3524177004'        
 and rtrim(LOANDTLACHBNKACCT) ='059324574'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3526617009'        
 and rtrim(LOANDTLACHBNKACCT) ='3020006171'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122402382';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3526697000'        
 and rtrim(LOANDTLACHBNKACCT) ='6081266'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='051402518';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3533727005'        
 and rtrim(LOANDTLACHBNKACCT) ='62025'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310518';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3536787007'        
 and rtrim(LOANDTLACHBNKACCT) ='7083743'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091916378';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3537477010'        
 and rtrim(LOANDTLACHBNKACCT) ='00440520270479'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000737';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3541477002'        
 and rtrim(LOANDTLACHBNKACCT) ='31727025'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091408763';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3550207004'        
 and rtrim(LOANDTLACHBNKACCT) ='305271'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='061220353';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3559927004'        
 and rtrim(LOANDTLACHBNKACCT) ='056525835'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3578977003'        
 and rtrim(LOANDTLACHBNKACCT) ='193145'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='272485385';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3584357001'        
 and rtrim(LOANDTLACHBNKACCT) ='231 662 3'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='081203208';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3585547001'        
 and rtrim(LOANDTLACHBNKACCT) ='6011014321'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='092905249';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3588907008'        
 and rtrim(LOANDTLACHBNKACCT) ='058075649'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3592507401'        
 and rtrim(LOANDTLACHBNKACCT) ='1051078246'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000661';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3593777000'        
 and rtrim(LOANDTLACHBNKACCT) ='0220074968'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122401778';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3595967002'        
 and rtrim(LOANDTLACHBNKACCT) ='25006797'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310754';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3600007405'        
 and rtrim(LOANDTLACHBNKACCT) ='501011049336'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324079555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3602927000'        
 and rtrim(LOANDTLACHBNKACCT) ='4290979702'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='067014822';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3605027410'        
 and rtrim(LOANDTLACHBNKACCT) ='0192140605'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310576';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3605277001'        
 and rtrim(LOANDTLACHBNKACCT) ='058138314'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3605627010'        
 and rtrim(LOANDTLACHBNKACCT) ='0060907294'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='065106619';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3610037004'        
 and rtrim(LOANDTLACHBNKACCT) ='2040002273'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051409579';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3610917008'        
 and rtrim(LOANDTLACHBNKACCT) ='60045063'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='104903333';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3611607000'        
 and rtrim(LOANDTLACHBNKACCT) ='388813138'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3619717406'        
 and rtrim(LOANDTLACHBNKACCT) ='507505428'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211170208';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3621007001'        
 and rtrim(LOANDTLACHBNKACCT) ='8100670218'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071901604';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3624267004'        
 and rtrim(LOANDTLACHBNKACCT) ='1000034155'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='056009356';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3672877004'        
 and rtrim(LOANDTLACHBNKACCT) ='325139347157'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3674767010'        
 and rtrim(LOANDTLACHBNKACCT) ='4004688'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='011302742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3675477008'        
 and rtrim(LOANDTLACHBNKACCT) ='004008008236'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='286573322';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3678287005'        
 and rtrim(LOANDTLACHBNKACCT) ='0311091535'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101203641';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3679127005'        
 and rtrim(LOANDTLACHBNKACCT) ='6770242590'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105744';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3680307008'        
 and rtrim(LOANDTLACHBNKACCT) ='1000848919'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125100089';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3680607004'        
 and rtrim(LOANDTLACHBNKACCT) ='919010118'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103582';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3684247000'        
 and rtrim(LOANDTLACHBNKACCT) ='4153483742'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='041000124';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3687587003'        
 and rtrim(LOANDTLACHBNKACCT) ='3170486689'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3691247009'        
 and rtrim(LOANDTLACHBNKACCT) ='9435333159'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='323173313';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3692007009'        
 and rtrim(LOANDTLACHBNKACCT) ='100476084'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='265070435';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3692677000'        
 and rtrim(LOANDTLACHBNKACCT) ='4826137893'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071025661';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3707677004'        
 and rtrim(LOANDTLACHBNKACCT) ='1001015955'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122243994';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3710107009'        
 and rtrim(LOANDTLACHBNKACCT) ='141003391'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400486';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3710187000'        
 and rtrim(LOANDTLACHBNKACCT) ='8520811746'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3713857006'        
 and rtrim(LOANDTLACHBNKACCT) ='2032241'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='111025877';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3714077010'        
 and rtrim(LOANDTLACHBNKACCT) ='001064562306'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122037760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4247787005'        
 and rtrim(LOANDTLACHBNKACCT) ='615097059'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4250837008'        
 and rtrim(LOANDTLACHBNKACCT) ='237042246048'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='053000196';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4251317007'        
 and rtrim(LOANDTLACHBNKACCT) ='060491271'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4252317009'        
 and rtrim(LOANDTLACHBNKACCT) ='102408066'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='092102851';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4253077000'        
 and rtrim(LOANDTLACHBNKACCT) ='5354607291'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4253377007'        
 and rtrim(LOANDTLACHBNKACCT) ='201109892'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122228003';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4253697009'        
 and rtrim(LOANDTLACHBNKACCT) ='9002619270'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='275079714';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4254437003'        
 and rtrim(LOANDTLACHBNKACCT) ='030002656'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122244333';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4255847010'        
 and rtrim(LOANDTLACHBNKACCT) ='9151028'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='124301025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4256137000'        
 and rtrim(LOANDTLACHBNKACCT) ='11549443'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091408763';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4256767001'        
 and rtrim(LOANDTLACHBNKACCT) ='1099688'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121144696';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4256917009'        
 and rtrim(LOANDTLACHBNKACCT) ='0021161294'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4257557010'        
 and rtrim(LOANDTLACHBNKACCT) ='82876547'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091905444';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4258447003'        
 and rtrim(LOANDTLACHBNKACCT) ='10333243'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='071212128';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4258967006'        
 and rtrim(LOANDTLACHBNKACCT) ='21710777'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103676';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4259817009'        
 and rtrim(LOANDTLACHBNKACCT) ='603022317'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4260307006'        
 and rtrim(LOANDTLACHBNKACCT) ='7015454'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='072413104';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4262717004'        
 and rtrim(LOANDTLACHBNKACCT) ='457042277910'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4262857000'        
 and rtrim(LOANDTLACHBNKACCT) ='33228'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='321379410';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4263137009'        
 and rtrim(LOANDTLACHBNKACCT) ='8101087710'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321370765';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4263297000'        
 and rtrim(LOANDTLACHBNKACCT) ='28040000221'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='123171955';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4263537000'        
 and rtrim(LOANDTLACHBNKACCT) ='229044245893'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='063100277';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4264937004'        
 and rtrim(LOANDTLACHBNKACCT) ='8014011418'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4265207010'        
 and rtrim(LOANDTLACHBNKACCT) ='0375918943'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4265667008'        
 and rtrim(LOANDTLACHBNKACCT) ='01100334798'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4266777008'        
 and rtrim(LOANDTLACHBNKACCT) ='31017207'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='073905187';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4267327004'        
 and rtrim(LOANDTLACHBNKACCT) ='7913625914'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4267617008'        
 and rtrim(LOANDTLACHBNKACCT) ='1600058042'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='275971139';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4267987003'        
 and rtrim(LOANDTLACHBNKACCT) ='5409793836'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='072410013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4268837006'        
 and rtrim(LOANDTLACHBNKACCT) ='1056911414'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4268907003'        
 and rtrim(LOANDTLACHBNKACCT) ='6225838628'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071001533';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4269447009'        
 and rtrim(LOANDTLACHBNKACCT) ='591005367'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4269637007'        
 and rtrim(LOANDTLACHBNKACCT) ='1000260806194'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='063102152';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4270687008'        
 and rtrim(LOANDTLACHBNKACCT) ='000634872696'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4271337010'        
 and rtrim(LOANDTLACHBNKACCT) ='3310004555'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011402105';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4272597009'        
 and rtrim(LOANDTLACHBNKACCT) ='979275500'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4273067005'        
 and rtrim(LOANDTLACHBNKACCT) ='001874268527'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4273457004'        
 and rtrim(LOANDTLACHBNKACCT) ='1163633'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='021113125';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4275567006'        
 and rtrim(LOANDTLACHBNKACCT) ='0090712454'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301028';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4274177005'        
 and rtrim(LOANDTLACHBNKACCT) ='5399961352'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='325084426';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4274907010'        
 and rtrim(LOANDTLACHBNKACCT) ='597902037'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4275297008'        
 and rtrim(LOANDTLACHBNKACCT) ='01696335'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091310754';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4275847007'        
 and rtrim(LOANDTLACHBNKACCT) ='445616'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='102103106';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4278977006'        
 and rtrim(LOANDTLACHBNKACCT) ='0030100679'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238200';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4277487010'        
 and rtrim(LOANDTLACHBNKACCT) ='0305140393'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122243884';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4277667005'        
 and rtrim(LOANDTLACHBNKACCT) ='1054003289'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122287581';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4277897004'        
 and rtrim(LOANDTLACHBNKACCT) ='2841230144'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005047';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4279697007'        
 and rtrim(LOANDTLACHBNKACCT) ='0434625411'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='112200439';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4283537008'        
 and rtrim(LOANDTLACHBNKACCT) ='000149342455'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4280387005'        
 and rtrim(LOANDTLACHBNKACCT) ='3240536541'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122003396';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4281597000'        
 and rtrim(LOANDTLACHBNKACCT) ='4004396'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091800374';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4282617006'        
 and rtrim(LOANDTLACHBNKACCT) ='597069829'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4284207005'        
 and rtrim(LOANDTLACHBNKACCT) ='606729173'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4284277004'        
 and rtrim(LOANDTLACHBNKACCT) ='44010109'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274502';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4284367007'        
 and rtrim(LOANDTLACHBNKACCT) ='014663622'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4284587003'        
 and rtrim(LOANDTLACHBNKACCT) ='286507808'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4285467004'        
 and rtrim(LOANDTLACHBNKACCT) ='9882865661'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4285517006'        
 and rtrim(LOANDTLACHBNKACCT) ='488091210780'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4285727010'        
 and rtrim(LOANDTLACHBNKACCT) ='99393375'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371298';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4285977004'        
 and rtrim(LOANDTLACHBNKACCT) ='8524151147'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4293477007'        
 and rtrim(LOANDTLACHBNKACCT) ='5794059021'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122232109';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4289667002'        
 and rtrim(LOANDTLACHBNKACCT) ='6577382525'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211170282';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4290627009'        
 and rtrim(LOANDTLACHBNKACCT) ='483065964177'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000322';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4291927007'        
 and rtrim(LOANDTLACHBNKACCT) ='982485906'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4293827005'        
 and rtrim(LOANDTLACHBNKACCT) ='062098652'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4293927000'        
 and rtrim(LOANDTLACHBNKACCT) ='7932240471'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='044002161';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4296357001'        
 and rtrim(LOANDTLACHBNKACCT) ='009420126'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122239982';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4296697009'        
 and rtrim(LOANDTLACHBNKACCT) ='2200001374539'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103773';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4298817001'        
 and rtrim(LOANDTLACHBNKACCT) ='01182383466'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='072403473';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4299007007'        
 and rtrim(LOANDTLACHBNKACCT) ='361188953'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4299037005'        
 and rtrim(LOANDTLACHBNKACCT) ='157523570933'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121122676';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4300547003'        
 and rtrim(LOANDTLACHBNKACCT) ='06075762'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122201198';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4300647009'        
 and rtrim(LOANDTLACHBNKACCT) ='166791155'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371227';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4301157006'        
 and rtrim(LOANDTLACHBNKACCT) ='669804281'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172212';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4301307003'        
 and rtrim(LOANDTLACHBNKACCT) ='3083859618'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4303287003'        
 and rtrim(LOANDTLACHBNKACCT) ='1063390442'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4303537006'        
 and rtrim(LOANDTLACHBNKACCT) ='207508763'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271724';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4305417009'        
 and rtrim(LOANDTLACHBNKACCT) ='153353155911'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='123103729';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4306457001'        
 and rtrim(LOANDTLACHBNKACCT) ='116261683'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4306937003'        
 and rtrim(LOANDTLACHBNKACCT) ='596691888'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4308947010'        
 and rtrim(LOANDTLACHBNKACCT) ='0002148056'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121139287';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4310087000'        
 and rtrim(LOANDTLACHBNKACCT) ='59206006375'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4310767003'        
 and rtrim(LOANDTLACHBNKACCT) ='1742224015'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='291271240';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4311047001'        
 and rtrim(LOANDTLACHBNKACCT) ='602310796'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4311557001'        
 and rtrim(LOANDTLACHBNKACCT) ='3346479623'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4311587010'        
 and rtrim(LOANDTLACHBNKACCT) ='150000825948'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='211574642';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4312047003'        
 and rtrim(LOANDTLACHBNKACCT) ='603767580'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4312147009'        
 and rtrim(LOANDTLACHBNKACCT) ='661100042'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4312467000'        
 and rtrim(LOANDTLACHBNKACCT) ='325098508600'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4312927007'        
 and rtrim(LOANDTLACHBNKACCT) ='127474419'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4313227000'        
 and rtrim(LOANDTLACHBNKACCT) ='3077931'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='075905033';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4313587003'        
 and rtrim(LOANDTLACHBNKACCT) ='325119819650'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4314097000'        
 and rtrim(LOANDTLACHBNKACCT) ='59264195'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='083001314';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4316047000'        
 and rtrim(LOANDTLACHBNKACCT) ='025054341'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122243619';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4316177004'        
 and rtrim(LOANDTLACHBNKACCT) ='01780299224'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='041215032';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4316917001'        
 and rtrim(LOANDTLACHBNKACCT) ='8000080803'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4316927004'        
 and rtrim(LOANDTLACHBNKACCT) ='501009142178'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324079555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4317477002'        
 and rtrim(LOANDTLACHBNKACCT) ='3200168171'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211470225';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4317717002'        
 and rtrim(LOANDTLACHBNKACCT) ='001216413233'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4318407005'        
 and rtrim(LOANDTLACHBNKACCT) ='334046952651'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='061000052';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4318837005'        
 and rtrim(LOANDTLACHBNKACCT) ='1340010195135'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='053101121';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4318917005'        
 and rtrim(LOANDTLACHBNKACCT) ='1340010195143'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='053101121';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4319367008'        
 and rtrim(LOANDTLACHBNKACCT) ='502953539'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4319897003'        
 and rtrim(LOANDTLACHBNKACCT) ='355003453367'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='081000032';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4321297010'        
 and rtrim(LOANDTLACHBNKACCT) ='601412647'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4321347001'        
 and rtrim(LOANDTLACHBNKACCT) ='8616613297'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105980';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4321947004'        
 and rtrim(LOANDTLACHBNKACCT) ='612359601'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4323387006'        
 and rtrim(LOANDTLACHBNKACCT) ='01010000808'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='124301025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4323467006'        
 and rtrim(LOANDTLACHBNKACCT) ='3268168857'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021200025';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4323827007'        
 and rtrim(LOANDTLACHBNKACCT) ='2916106660'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925389';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4324297005'        
 and rtrim(LOANDTLACHBNKACCT) ='0006319447'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091914202';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4324337004'        
 and rtrim(LOANDTLACHBNKACCT) ='4051615'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='011302742';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4324437010'        
 and rtrim(LOANDTLACHBNKACCT) ='11000000646940'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='321177586';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4327217009'        
 and rtrim(LOANDTLACHBNKACCT) ='508126922'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4328537002'        
 and rtrim(LOANDTLACHBNKACCT) ='1000052629'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='056009356';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4328557008'        
 and rtrim(LOANDTLACHBNKACCT) ='7900033432'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071923349';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4329437009'        
 and rtrim(LOANDTLACHBNKACCT) ='3800326199'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122210406';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4329727002'        
 and rtrim(LOANDTLACHBNKACCT) ='638889790'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='074000010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4330377001'        
 and rtrim(LOANDTLACHBNKACCT) ='35500704'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091102807';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4331707006'        
 and rtrim(LOANDTLACHBNKACCT) ='3517181543'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4331887003'        
 and rtrim(LOANDTLACHBNKACCT) ='370186642'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4332567003'        
 and rtrim(LOANDTLACHBNKACCT) ='3550123396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113113392';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4332917001'        
 and rtrim(LOANDTLACHBNKACCT) ='611173912'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='061092387';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4334407005'        
 and rtrim(LOANDTLACHBNKACCT) ='1812674835'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4335497001'        
 and rtrim(LOANDTLACHBNKACCT) ='0040865156'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211370529';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4337937006'        
 and rtrim(LOANDTLACHBNKACCT) ='611563138'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4338527003'        
 and rtrim(LOANDTLACHBNKACCT) ='276004155'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='083002177';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4338627009'        
 and rtrim(LOANDTLACHBNKACCT) ='609739906'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4339037000'        
 and rtrim(LOANDTLACHBNKACCT) ='1960411039'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031000503';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4339617008'        
 and rtrim(LOANDTLACHBNKACCT) ='2121248272'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005047';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4356247006'        
 and rtrim(LOANDTLACHBNKACCT) ='4637815678'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4364907003'        
 and rtrim(LOANDTLACHBNKACCT) ='551313853'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='096010415';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4391187007'        
 and rtrim(LOANDTLACHBNKACCT) ='2200003118963'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='092900613';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4411317007'        
 and rtrim(LOANDTLACHBNKACCT) ='309895032'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4458307001'        
 and rtrim(LOANDTLACHBNKACCT) ='2008760'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071926058';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4481317002'        
 and rtrim(LOANDTLACHBNKACCT) ='000222356'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='065303360';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4509017804'        
 and rtrim(LOANDTLACHBNKACCT) ='613596391'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4673367000'        
 and rtrim(LOANDTLACHBNKACCT) ='8097090490'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101015101';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9371725006'        
 and rtrim(LOANDTLACHBNKACCT) ='706961351'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9653915002'        
 and rtrim(LOANDTLACHBNKACCT) ='1151192190'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081009428';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9716455004'        
 and rtrim(LOANDTLACHBNKACCT) ='008201611006'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='067010509';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9722965000'        
 and rtrim(LOANDTLACHBNKACCT) ='20119574'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='275971854';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9828847806'        
 and rtrim(LOANDTLACHBNKACCT) ='141112730'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124300327';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4203977001'        
 and rtrim(LOANDTLACHBNKACCT) ='15082911'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122201198';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4204717006'        
 and rtrim(LOANDTLACHBNKACCT) ='40017311'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='081906013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4205407009'        
 and rtrim(LOANDTLACHBNKACCT) ='605310199'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4205697004'        
 and rtrim(LOANDTLACHBNKACCT) ='3123305183'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4206197009'        
 and rtrim(LOANDTLACHBNKACCT) ='325117126031'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4206667008'        
 and rtrim(LOANDTLACHBNKACCT) ='62060199'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4206787009'        
 and rtrim(LOANDTLACHBNKACCT) ='1106987'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='073972110';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4206867009'        
 and rtrim(LOANDTLACHBNKACCT) ='4400394386'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371492';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4207377006'        
 and rtrim(LOANDTLACHBNKACCT) ='4694825151'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4207407002'        
 and rtrim(LOANDTLACHBNKACCT) ='251000410'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121142287';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4207437000'        
 and rtrim(LOANDTLACHBNKACCT) ='133484035'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='082900432';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4208287005'        
 and rtrim(LOANDTLACHBNKACCT) ='518002091648'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100045';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4208617008'        
 and rtrim(LOANDTLACHBNKACCT) ='1000164804'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='072414310';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4208647006'        
 and rtrim(LOANDTLACHBNKACCT) ='1076140'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091000132';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4209437004'        
 and rtrim(LOANDTLACHBNKACCT) ='1189468801'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='083904563';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4209567008'        
 and rtrim(LOANDTLACHBNKACCT) ='679792072'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4209717005'        
 and rtrim(LOANDTLACHBNKACCT) ='325138891345'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4209957007'        
 and rtrim(LOANDTLACHBNKACCT) ='0850083592'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4210567005'        
 and rtrim(LOANDTLACHBNKACCT) ='900810564'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='101001364';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4211367006'        
 and rtrim(LOANDTLACHBNKACCT) ='30475445'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400020';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4211417008'        
 and rtrim(LOANDTLACHBNKACCT) ='1313771101'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075906171';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4211947003'        
 and rtrim(LOANDTLACHBNKACCT) ='325069150401'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4212307001'        
 and rtrim(LOANDTLACHBNKACCT) ='062108329'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4212597007'        
 and rtrim(LOANDTLACHBNKACCT) ='599226906'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4213277007'        
 and rtrim(LOANDTLACHBNKACCT) ='599172217'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021202337';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4213337001'        
 and rtrim(LOANDTLACHBNKACCT) ='825493968'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4214247000'        
 and rtrim(LOANDTLACHBNKACCT) ='65063115'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301015';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4214277009'        
 and rtrim(LOANDTLACHBNKACCT) ='003208675'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121143037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4215997004'        
 and rtrim(LOANDTLACHBNKACCT) ='953312720'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='011002343';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4216587001'        
 and rtrim(LOANDTLACHBNKACCT) ='0005004500'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071110042';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4216727006'        
 and rtrim(LOANDTLACHBNKACCT) ='589665550'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='074000010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4216897000'        
 and rtrim(LOANDTLACHBNKACCT) ='11000000637926'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='321177586';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4217837006'        
 and rtrim(LOANDTLACHBNKACCT) ='0273018167'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051408949';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4218347003'        
 and rtrim(LOANDTLACHBNKACCT) ='3055685047'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='083000108';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219217001'        
 and rtrim(LOANDTLACHBNKACCT) ='41002010'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091903051';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219277008'        
 and rtrim(LOANDTLACHBNKACCT) ='6772840304'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321170538';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219387006'        
 and rtrim(LOANDTLACHBNKACCT) ='140150008073'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='101000925';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219427005'        
 and rtrim(LOANDTLACHBNKACCT) ='210698507'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219507005'        
 and rtrim(LOANDTLACHBNKACCT) ='8063012903'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219517008'        
 and rtrim(LOANDTLACHBNKACCT) ='5349914123'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219527000'        
 and rtrim(LOANDTLACHBNKACCT) ='0460010241'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219567001'        
 and rtrim(LOANDTLACHBNKACCT) ='8000029204'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219617003'        
 and rtrim(LOANDTLACHBNKACCT) ='1291181'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065400483';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219707006'        
 and rtrim(LOANDTLACHBNKACCT) ='8014011632'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4219747007'        
 and rtrim(LOANDTLACHBNKACCT) ='014373'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091017196';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227387006'        
 and rtrim(LOANDTLACHBNKACCT) ='429295701'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091301048';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4471317107'        
 and rtrim(LOANDTLACHBNKACCT) ='837379721'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4825667007'        
 and rtrim(LOANDTLACHBNKACCT) ='36040740187'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='031176110';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4923737803'        
 and rtrim(LOANDTLACHBNKACCT) ='608515869'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4939457001'        
 and rtrim(LOANDTLACHBNKACCT) ='1000839060'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081204540';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4962947102'        
 and rtrim(LOANDTLACHBNKACCT) ='8609988780'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5134327800'        
 and rtrim(LOANDTLACHBNKACCT) ='6798624703'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105278';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5231677105'        
 and rtrim(LOANDTLACHBNKACCT) ='1384163'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='063115505';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5297757108'        
 and rtrim(LOANDTLACHBNKACCT) ='1715020423'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124100417';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5436477903'        
 and rtrim(LOANDTLACHBNKACCT) ='01596576'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='071926155';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5665407004'        
 and rtrim(LOANDTLACHBNKACCT) ='591405553'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211373539';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5702877005'        
 and rtrim(LOANDTLACHBNKACCT) ='2804030278'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='082907273';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5923427003'        
 and rtrim(LOANDTLACHBNKACCT) ='1742027801'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043400036';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6033277107'        
 and rtrim(LOANDTLACHBNKACCT) ='3082417324'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6227507407'        
 and rtrim(LOANDTLACHBNKACCT) ='103684919535'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='102000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6234687402'        
 and rtrim(LOANDTLACHBNKACCT) ='2680067671'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6260047402'        
 and rtrim(LOANDTLACHBNKACCT) ='7460255982'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6280747800'        
 and rtrim(LOANDTLACHBNKACCT) ='325000597117'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6282887408'        
 and rtrim(LOANDTLACHBNKACCT) ='5643429201'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6294937407'        
 and rtrim(LOANDTLACHBNKACCT) ='8102913402'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321370765';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6297817902'        
 and rtrim(LOANDTLACHBNKACCT) ='063184691'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6352367107'        
 and rtrim(LOANDTLACHBNKACCT) ='001182517379'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6369147107'        
 and rtrim(LOANDTLACHBNKACCT) ='4157930765'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='041000124';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6408007104'        
 and rtrim(LOANDTLACHBNKACCT) ='1670561836'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925651';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6569737101'        
 and rtrim(LOANDTLACHBNKACCT) ='611389633'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6584077004'        
 and rtrim(LOANDTLACHBNKACCT) ='627151860'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6721657902'        
 and rtrim(LOANDTLACHBNKACCT) ='07503500'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122042807';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6723357006'        
 and rtrim(LOANDTLACHBNKACCT) ='9284000487'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='321171184';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6831437406'        
 and rtrim(LOANDTLACHBNKACCT) ='8431995060'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051400549';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6847427800'        
 and rtrim(LOANDTLACHBNKACCT) ='05001136307'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='062006505';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6849937107'        
 and rtrim(LOANDTLACHBNKACCT) ='255554874'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044202505';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6869147103'        
 and rtrim(LOANDTLACHBNKACCT) ='000669802663'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6876987302'        
 and rtrim(LOANDTLACHBNKACCT) ='61093820'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='021202337';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6909397103'        
 and rtrim(LOANDTLACHBNKACCT) ='164107859579'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000661';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6957317106'        
 and rtrim(LOANDTLACHBNKACCT) ='6732785343'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122105744';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7199907002'        
 and rtrim(LOANDTLACHBNKACCT) ='1002360876'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='075911852';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7352857008'        
 and rtrim(LOANDTLACHBNKACCT) ='0100027119'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7399357106'        
 and rtrim(LOANDTLACHBNKACCT) ='612650132'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7438467010'        
 and rtrim(LOANDTLACHBNKACCT) ='3655531022'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061000227';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7593175003'        
 and rtrim(LOANDTLACHBNKACCT) ='1002167442'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113025723';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7621107201'        
 and rtrim(LOANDTLACHBNKACCT) ='223168722'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7899697208'        
 and rtrim(LOANDTLACHBNKACCT) ='300726650'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='073903244';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8064775005'        
 and rtrim(LOANDTLACHBNKACCT) ='140005265'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121105156';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8169577003'        
 and rtrim(LOANDTLACHBNKACCT) ='0003000463'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011501705';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8257447402'        
 and rtrim(LOANDTLACHBNKACCT) ='5349341564'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8262647310'        
 and rtrim(LOANDTLACHBNKACCT) ='100085448'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='061104877';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8329687404'        
 and rtrim(LOANDTLACHBNKACCT) ='613705976'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8339377801'        
 and rtrim(LOANDTLACHBNKACCT) ='3440122881'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122003396';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8350687801'        
 and rtrim(LOANDTLACHBNKACCT) ='3440122881'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122003396';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8372467403'        
 and rtrim(LOANDTLACHBNKACCT) ='4671461937'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000248';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8455287403'        
 and rtrim(LOANDTLACHBNKACCT) ='461015182'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071112066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8618195000'        
 and rtrim(LOANDTLACHBNKACCT) ='661132001'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211589828';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8623265002'        
 and rtrim(LOANDTLACHBNKACCT) ='7560291838'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8631025000'        
 and rtrim(LOANDTLACHBNKACCT) ='21179833'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='031207898';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8645537102'        
 and rtrim(LOANDTLACHBNKACCT) ='759626419'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8693205001'        
 and rtrim(LOANDTLACHBNKACCT) ='116500111'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='055003340';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8701227000'        
 and rtrim(LOANDTLACHBNKACCT) ='100057219'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121144146';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8781537009'        
 and rtrim(LOANDTLACHBNKACCT) ='376511017'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8816167300'        
 and rtrim(LOANDTLACHBNKACCT) ='000526356378'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8842247903'        
 and rtrim(LOANDTLACHBNKACCT) ='1001742558'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238200';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8873225004'        
 and rtrim(LOANDTLACHBNKACCT) ='0124772701'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='026002794';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8932157403'        
 and rtrim(LOANDTLACHBNKACCT) ='8525395449'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8993347307'        
 and rtrim(LOANDTLACHBNKACCT) ='476273511'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122238420';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9052537101'        
 and rtrim(LOANDTLACHBNKACCT) ='062826102'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9066387007'        
 and rtrim(LOANDTLACHBNKACCT) ='0100015072'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122242571';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9089697303'        
 and rtrim(LOANDTLACHBNKACCT) ='007001204490'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='125108272';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9139447101'        
 and rtrim(LOANDTLACHBNKACCT) ='1030907'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302914';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9142287004'        
 and rtrim(LOANDTLACHBNKACCT) ='157523563573'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122235821';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9160847402'        
 and rtrim(LOANDTLACHBNKACCT) ='446041722846'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='052001633';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9193075008'        
 and rtrim(LOANDTLACHBNKACCT) ='4180615'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='103100881';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9201607909'        
 and rtrim(LOANDTLACHBNKACCT) ='6400339455'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122041235';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9216615008'        
 and rtrim(LOANDTLACHBNKACCT) ='2000140604'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031309945';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9360155006'        
 and rtrim(LOANDTLACHBNKACCT) ='466502122'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9414695000'        
 and rtrim(LOANDTLACHBNKACCT) ='6641409351'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9442085002'        
 and rtrim(LOANDTLACHBNKACCT) ='000701927545'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9446065004'        
 and rtrim(LOANDTLACHBNKACCT) ='6150000112'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274573';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9532085006'        
 and rtrim(LOANDTLACHBNKACCT) ='6095524861'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9532387103'        
 and rtrim(LOANDTLACHBNKACCT) ='2890175782'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925402';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9534047404'        
 and rtrim(LOANDTLACHBNKACCT) ='1750080696955'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='272480173';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9534787207'        
 and rtrim(LOANDTLACHBNKACCT) ='32002173'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='311372744';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9571877100'        
 and rtrim(LOANDTLACHBNKACCT) ='2891627563'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925402';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9572327809'        
 and rtrim(LOANDTLACHBNKACCT) ='157521510378'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121122676';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9584905005'        
 and rtrim(LOANDTLACHBNKACCT) ='0500620489'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071102568';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9769587810'        
 and rtrim(LOANDTLACHBNKACCT) ='0002200003392445'                                                   
  and rtrim(LOANDTLACHBNKROUTNMB) ='124103773';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9788185005'        
 and rtrim(LOANDTLACHBNKACCT) ='5041389'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='263184488';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9863967802'        
 and rtrim(LOANDTLACHBNKACCT) ='1064349783'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122037760';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9871127002'        
 and rtrim(LOANDTLACHBNKACCT) ='9108676206'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='052002166';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='9906375005'        
 and rtrim(LOANDTLACHBNKACCT) ='420054798'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='061092387';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4220447008'        
 and rtrim(LOANDTLACHBNKACCT) ='023948176'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4221237006'        
 and rtrim(LOANDTLACHBNKACCT) ='591005502'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122234149';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4221627005'        
 and rtrim(LOANDTLACHBNKACCT) ='592912213'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071000013';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4222317008'        
 and rtrim(LOANDTLACHBNKACCT) ='501011931138'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='324079555';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4222587008'        
 and rtrim(LOANDTLACHBNKACCT) ='6012800'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='071908160';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4223257005'        
 and rtrim(LOANDTLACHBNKACCT) ='325138726449'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4223687005'        
 and rtrim(LOANDTLACHBNKACCT) ='592783630'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000037';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4224347010'        
 and rtrim(LOANDTLACHBNKACCT) ='537031806'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4224907001'        
 and rtrim(LOANDTLACHBNKACCT) ='166789861'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371227';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4225017007'        
 and rtrim(LOANDTLACHBNKACCT) ='4680322748'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071921891';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4225417009'        
 and rtrim(LOANDTLACHBNKACCT) ='031109833'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122016066';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4225827003'        
 and rtrim(LOANDTLACHBNKACCT) ='05007463'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124302914';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4226347003'        
 and rtrim(LOANDTLACHBNKACCT) ='0005100171'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071902399';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4226817002'        
 and rtrim(LOANDTLACHBNKACCT) ='0129744'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091803274';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227247010'        
 and rtrim(LOANDTLACHBNKACCT) ='062173281'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122242843';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227377003'        
 and rtrim(LOANDTLACHBNKACCT) ='526163685'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227467006'        
 and rtrim(LOANDTLACHBNKACCT) ='0120150375'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000496';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227477009'        
 and rtrim(LOANDTLACHBNKACCT) ='160102000'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='281573259';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227497004'        
 and rtrim(LOANDTLACHBNKACCT) ='581521660'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227717009'        
 and rtrim(LOANDTLACHBNKACCT) ='596821030'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='074000010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227837010'        
 and rtrim(LOANDTLACHBNKACCT) ='325135671063'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227927002'        
 and rtrim(LOANDTLACHBNKACCT) ='8000154181'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322285781';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4227997001'        
 and rtrim(LOANDTLACHBNKACCT) ='1100007535'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100621';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4228907009'        
 and rtrim(LOANDTLACHBNKACCT) ='10200000418360'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='291479686';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4229357001'        
 and rtrim(LOANDTLACHBNKACCT) ='062191119'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121100782';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4230827008'        
 and rtrim(LOANDTLACHBNKACCT) ='325099407331'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4231517000'        
 and rtrim(LOANDTLACHBNKACCT) ='11192854'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='211371120';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4231727004'        
 and rtrim(LOANDTLACHBNKACCT) ='30085276'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='041215498';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4231777008'        
 and rtrim(LOANDTLACHBNKACCT) ='286626970'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='074000010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4231787000'        
 and rtrim(LOANDTLACHBNKACCT) ='286626970'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='074000010';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4231817007'        
 and rtrim(LOANDTLACHBNKACCT) ='325128809460'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4232427010'        
 and rtrim(LOANDTLACHBNKACCT) ='457042236595'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='122101706';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4233157003'        
 and rtrim(LOANDTLACHBNKACCT) ='1410005462441'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='053201607';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4233397005'        
 and rtrim(LOANDTLACHBNKACCT) ='03042693'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='083000726';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4233807008'        
 and rtrim(LOANDTLACHBNKACCT) ='00212687'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='102300297';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4233917006'        
 and rtrim(LOANDTLACHBNKACCT) ='325138654207'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4234097000'        
 and rtrim(LOANDTLACHBNKACCT) ='598187083'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4234987007'        
 and rtrim(LOANDTLACHBNKACCT) ='600019266'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4235067004'        
 and rtrim(LOANDTLACHBNKACCT) ='200494121'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172296';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4235687002'        
 and rtrim(LOANDTLACHBNKACCT) ='3561497912'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211381372';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4235957000'        
 and rtrim(LOANDTLACHBNKACCT) ='100014008'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021301115';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4236427007'        
 and rtrim(LOANDTLACHBNKACCT) ='602221712'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4236997003'        
 and rtrim(LOANDTLACHBNKACCT) ='103710760'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='071902878';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4237087003'        
 and rtrim(LOANDTLACHBNKACCT) ='325127037660'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4238237002'        
 and rtrim(LOANDTLACHBNKACCT) ='151373100'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4238377009'        
 and rtrim(LOANDTLACHBNKACCT) ='5288609497'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4238657010'        
 and rtrim(LOANDTLACHBNKACCT) ='7101638463'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='256074974';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4238777000'        
 and rtrim(LOANDTLACHBNKACCT) ='1099720'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121144696';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4238857000'        
 and rtrim(LOANDTLACHBNKACCT) ='164102612614'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='121000358';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4239247007'        
 and rtrim(LOANDTLACHBNKACCT) ='2148226'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='121139287';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240117000'        
 and rtrim(LOANDTLACHBNKACCT) ='3130242948'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240257007'        
 and rtrim(LOANDTLACHBNKACCT) ='7723722729'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240317001'        
 and rtrim(LOANDTLACHBNKACCT) ='58706007724'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='323371076';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240597004'        
 and rtrim(LOANDTLACHBNKACCT) ='9001954779'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031314503';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240877005'        
 and rtrim(LOANDTLACHBNKACCT) ='661031518'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122242843';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240927007'        
 and rtrim(LOANDTLACHBNKACCT) ='602631399'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='322271627';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4240977000'        
 and rtrim(LOANDTLACHBNKACCT) ='8018000987'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='322070381';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4242937003'        
 and rtrim(LOANDTLACHBNKACCT) ='8523382749'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='051403164';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4242977004'        
 and rtrim(LOANDTLACHBNKACCT) ='49513500'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122239270';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4242997010'        
 and rtrim(LOANDTLACHBNKACCT) ='2210018186'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011402105';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4243057001'        
 and rtrim(LOANDTLACHBNKACCT) ='1895491221'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121137522';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4243087010'        
 and rtrim(LOANDTLACHBNKACCT) ='2097899617'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='122000247';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4243397009'        
 and rtrim(LOANDTLACHBNKACCT) ='05733900'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='122042807';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4243417002'        
 and rtrim(LOANDTLACHBNKACCT) ='603629798'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4243577004'        
 and rtrim(LOANDTLACHBNKACCT) ='7592440017'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211384214';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4245257006'        
 and rtrim(LOANDTLACHBNKACCT) ='01100334730'                                                        
  and rtrim(LOANDTLACHBNKROUTNMB) ='044000024';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4246137007'        
 and rtrim(LOANDTLACHBNKACCT) ='910294909'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='075911616';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4246317002'        
 and rtrim(LOANDTLACHBNKACCT) ='0096432992'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301028';                           
                                                                                                    
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4247337010'        
 and rtrim(LOANDTLACHBNKACCT) ='9701238366'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107005047';                           
                                                                                                    

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

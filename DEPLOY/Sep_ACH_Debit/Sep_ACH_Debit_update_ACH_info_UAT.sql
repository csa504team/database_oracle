define deploy_name=Sep_ACH_Debit
define package_name=update_ACH_info_UAT
define package_buildtime=20200827102005
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Sep_ACH_Debit_update_ACH_info_UAT created on Thu 08/27/2020 10:20:06.46 by Jasleen Gorowada
prompt deploy scripts for deploy Sep_ACH_Debit_update_ACH_info_UAT created on Thu 08/27/2020 10:20:06.46 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Sep_ACH_Debit_update_ACH_info_UAT: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ach_info_UAT.sql 
Jasleenkaur Gorowada committed c288d5a on Tue Aug 25 16:37:14 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\remove_ach_info_UAT.sql 
Jasleenkaur Gorowada committed c288d5a on Tue Aug 25 16:37:14 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\add_ach_info_UAT.sql"
set echo on
set feedback on
select '*** update ACH information***' from dual;
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000007690       ',LOANDTLACHBNKROUTNMB='113025723      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2128946007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000031625       ',LOANDTLACHBNKROUTNMB='211871691      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2681936010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000036420       ',LOANDTLACHBNKROUTNMB='301171007      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6355315008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000178013       ',LOANDTLACHBNKROUTNMB='107007139      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8640654007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0000281203146    ',LOANDTLACHBNKROUTNMB='061000104      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7354644009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005201339873    ',LOANDTLACHBNKROUTNMB='053101121      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3404086000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0005793581314    ',LOANDTLACHBNKROUTNMB='113011258      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6916495006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00177949         ',LOANDTLACHBNKROUTNMB='303986096      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7932495000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0022434054       ',LOANDTLACHBNKROUTNMB='125108272      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7008985010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0023335306       ',LOANDTLACHBNKROUTNMB='066004367      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3413085006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='003000788        ',LOANDTLACHBNKROUTNMB='241270851      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3570505005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='00350001113439   ',LOANDTLACHBNKROUTNMB='041001039      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2600196001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='003670292229     ',LOANDTLACHBNKROUTNMB='063000047      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5110425003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0073084305       ',LOANDTLACHBNKROUTNMB='042000314      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8062885010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0077636996       ',LOANDTLACHBNKROUTNMB='063104668      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2590236006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0084700          ',LOANDTLACHBNKROUTNMB='114911807      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2901616000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='008982           ',LOANDTLACHBNKROUTNMB='101111704      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6890175005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='01 9204997       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4444725005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010183516505     ',LOANDTLACHBNKROUTNMB='066011392      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2968456005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='010212979505     ',LOANDTLACHBNKROUTNMB='066011392      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7605035005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='011269359        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7818255005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='011356901        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7570124000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='011652659        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3745825010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0121616523       ',LOANDTLACHBNKROUTNMB='063104668      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5147565003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0131839          ',LOANDTLACHBNKROUTNMB='061101197      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6195665008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='015281132        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8059995007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0185595          ',LOANDTLACHBNKROUTNMB='065200515      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5839975009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0186254          ',LOANDTLACHBNKROUTNMB='065200515      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6175075000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0194654          ',LOANDTLACHBNKROUTNMB='011701314      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7278755000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='027478769        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9332734010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='029118627        ',LOANDTLACHBNKROUTNMB='124000054      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7258854002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='029711004903     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8542534002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='030268117        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8168945002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='030268117        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8804255009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='030268117        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9128554007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0309204863       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6402445005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='031061141        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7808805000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='035368713        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5074374007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0361056065       ',LOANDTLACHBNKROUTNMB='304971932      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6605235001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0362182180       ',LOANDTLACHBNKROUTNMB='102300129      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2576306007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='041466462        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6547855000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='048 224839       ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2896356010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0500001193       ',LOANDTLACHBNKROUTNMB='063116177      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5202975001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0500001193       ',LOANDTLACHBNKROUTNMB='063116177      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7191625004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0500139472       ',LOANDTLACHBNKROUTNMB='063116177      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6670335003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0501664163       ',LOANDTLACHBNKROUTNMB='082902757      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8057235009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='050252089        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6163785004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='05096118         ',LOANDTLACHBNKROUTNMB='062202574      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2776976003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='052025829        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5993675000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0551102809       ',LOANDTLACHBNKROUTNMB='103003632      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4121395008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='05512307         ',LOANDTLACHBNKROUTNMB='123206338      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2511096004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0578340          ',LOANDTLACHBNKROUTNMB='061112364      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3192276006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0608009083       ',LOANDTLACHBNKROUTNMB='253271822      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7435415008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='061377821        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4193855003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='061488372        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7125735004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='061488372        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7125545006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='061531685        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7797175005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='063005433        ',LOANDTLACHBNKROUTNMB='021206249      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8793154000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='068127561        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3161286006'        
and ltrim(LOANDTLACHBNKACCT) IS NOT NULL and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0781106305       ',LOANDTLACHBNKROUTNMB='091302966      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7061005008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='08029237         ',LOANDTLACHBNKROUTNMB='121301772      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4655995005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0816222078       ',LOANDTLACHBNKROUTNMB='271183701      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8007425009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0850053846       ',LOANDTLACHBNKROUTNMB='071925567      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2306876003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='09986241         ',LOANDTLACHBNKROUTNMB='114902560      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9128994010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1000254555       ',LOANDTLACHBNKROUTNMB='111916326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4599495002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='100086355        ',LOANDTLACHBNKROUTNMB='091400554      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5116475008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='100086652        ',LOANDTLACHBNKROUTNMB='091400554      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5479955009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1002019595       ',LOANDTLACHBNKROUTNMB='211885250      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3355935002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1002344016       ',LOANDTLACHBNKROUTNMB='113025723      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6049135004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='10031729         ',LOANDTLACHBNKROUTNMB='011601087      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3338856007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='100646744        ',LOANDTLACHBNKROUTNMB='075911742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7599065006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1010001076484    ',LOANDTLACHBNKROUTNMB='062203984      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3222566001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1010071149       ',LOANDTLACHBNKROUTNMB='011301798      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2670806000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1010190105       ',LOANDTLACHBNKROUTNMB='067011812      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9063005000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1010748270       ',LOANDTLACHBNKROUTNMB='074903719      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9394764003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='10109696         ',LOANDTLACHBNKROUTNMB='041212983      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5323685004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1011529          ',LOANDTLACHBNKROUTNMB='111916180      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5499725007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1030047653       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6756845004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1033197987       ',LOANDTLACHBNKROUTNMB='124003116      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5528235002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='105285112        ',LOANDTLACHBNKROUTNMB='086300012      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9047535006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1059777          ',LOANDTLACHBNKROUTNMB='111922624      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4724735002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1060107140637    ',LOANDTLACHBNKROUTNMB='044102362      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5992625005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1070597131       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6829045002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='11000000449362   ',LOANDTLACHBNKROUTNMB='321177586      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3617474000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='11000000514283   ',LOANDTLACHBNKROUTNMB='321177586      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9092935007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1100035052       ',LOANDTLACHBNKROUTNMB='121101985      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6733535002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1110027400       ',LOANDTLACHBNKROUTNMB='114903284      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4149605001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='111042561        ',LOANDTLACHBNKROUTNMB='121135045      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3701885004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='111268389        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5906884000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='111692822        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8928225002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='112249280        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7689925008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1131346647       ',LOANDTLACHBNKROUTNMB='043000096      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4218545001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='115490019        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4529925005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1190915211       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5354915004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1210306506       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6889955006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1223380          ',LOANDTLACHBNKROUTNMB='111923607      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5071475003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1248218          ',LOANDTLACHBNKROUTNMB='065204443      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2588696005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='125076310        ',LOANDTLACHBNKROUTNMB='064209216      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5225035004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1251800538       ',LOANDTLACHBNKROUTNMB='211170253      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3578806002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1289496          ',LOANDTLACHBNKROUTNMB='065204443      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5340305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1300000297990    ',LOANDTLACHBNKROUTNMB='291479974      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5553435005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1310730381       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4741615003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1310756780       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4902095004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='133029492        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4256425001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='133103129        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4121935004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1340004427652    ',LOANDTLACHBNKROUTNMB='053101121      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5458805000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='134253965        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4639635002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1343491          ',LOANDTLACHBNKROUTNMB='063115505      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3737495007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='138233667        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8240965003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='139560019        ',LOANDTLACHBNKROUTNMB='086518477      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5291155003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1400000174151    ',LOANDTLACHBNKROUTNMB='241279616      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1932777007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='141233141        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4381535009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1431225          ',LOANDTLACHBNKROUTNMB='075917937      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6568465000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1440643992       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5100035000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='149896           ',LOANDTLACHBNKROUTNMB='101100906      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3035776010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1509202079       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3637735010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='152316024792     ',LOANDTLACHBNKROUTNMB='081000210      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6335995005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='154006025        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6350815006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='156377896        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6450205008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157384799        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7943405001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='157384799        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7943475000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='160029902        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4778395002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1615007052       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2538636000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1620011880       ',LOANDTLACHBNKROUTNMB='102103407      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4998415008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='163070507815     ',LOANDTLACHBNKROUTNMB='091300023      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2891466009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='170000638312     ',LOANDTLACHBNKROUTNMB='111301122      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6362655003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='170100154        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4941044007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1771128343       ',LOANDTLACHBNKROUTNMB='063107513      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4968565006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1881268047       ',LOANDTLACHBNKROUTNMB='111000753      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3690796001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='19114959         ',LOANDTLACHBNKROUTNMB='111310870      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2899666004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2000004065       ',LOANDTLACHBNKROUTNMB='061120479      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5720015006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2000011615       ',LOANDTLACHBNKROUTNMB='063116083      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6425045006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2000013223       ',LOANDTLACHBNKROUTNMB='067015313      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6290125008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2000030480669    ',LOANDTLACHBNKROUTNMB='111900659      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5165665002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='20013331         ',LOANDTLACHBNKROUTNMB='113111983      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7737874005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='20015982         ',LOANDTLACHBNKROUTNMB='091901202      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3292866003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='20051093         ',LOANDTLACHBNKROUTNMB='011300595      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9061225003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='201756615        ',LOANDTLACHBNKROUTNMB='101015282      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8470265000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='203296637        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2794436009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2090002237452    ',LOANDTLACHBNKROUTNMB='063107513      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7189894009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='21099556         ',LOANDTLACHBNKROUTNMB='113111983      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9228145006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2115002295       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3104136000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='237016578218     ',LOANDTLACHBNKROUTNMB='053000196      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7500454010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='239270490        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6735365003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2409202675       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6978515005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2428384272       ',LOANDTLACHBNKROUTNMB='211274450      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9042025007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='245003398        ',LOANDTLACHBNKROUTNMB='101101293      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7759034001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2489619          ',LOANDTLACHBNKROUTNMB='031309945      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6370255001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2500303535       ',LOANDTLACHBNKROUTNMB='074900356      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6451005009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='250374633        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1519056003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2506021          ',LOANDTLACHBNKROUTNMB='091915890      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1177897003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2506021          ',LOANDTLACHBNKROUTNMB='091915890      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2524517005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2506021          ',LOANDTLACHBNKROUTNMB='091915890      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9182865003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2509201981       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4947005003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2510320201       ',LOANDTLACHBNKROUTNMB='081503704      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2209346006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2531226545       ',LOANDTLACHBNKROUTNMB='113010547      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4772645004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='25500011910071   ',LOANDTLACHBNKROUTNMB='111901056      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4911565005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='256000883        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5165925008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='256000883        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4573775005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='266132           ',LOANDTLACHBNKROUTNMB='101101413      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4506725001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2677031136       ',LOANDTLACHBNKROUTNMB='324170085      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2602956008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2830001497       ',LOANDTLACHBNKROUTNMB='301171007      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9345105004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2964141838       ',LOANDTLACHBNKROUTNMB='111900659      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6601915007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='297000664939     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8182485002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='297001750656     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7395275008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3000029433       ',LOANDTLACHBNKROUTNMB='113103276      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5671385004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3000389377       ',LOANDTLACHBNKROUTNMB='031315544      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4277265000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3000788          ',LOANDTLACHBNKROUTNMB='241270851      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4574465008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3003140996       ',LOANDTLACHBNKROUTNMB='021502341      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4950715005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='300350328        ',LOANDTLACHBNKROUTNMB='211674775      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9209704006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3004522396       ',LOANDTLACHBNKROUTNMB='021502341      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7036355006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3004554549       ',LOANDTLACHBNKROUTNMB='021502341      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5195475005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3004841033       ',LOANDTLACHBNKROUTNMB='021502341      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6662415003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3004893181       ',LOANDTLACHBNKROUTNMB='021502341      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6196995004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3005120912       ',LOANDTLACHBNKROUTNMB='055002406      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6104094002'        
and ltrim(LOANDTLACHBNKACCT) IS NOT NULL and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3009001083       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3180236008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3009203010       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7963215000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='301202197        ',LOANDTLACHBNKROUTNMB='113103276      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8964774004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='305089668        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7615985001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='31003733         ',LOANDTLACHBNKROUTNMB='071922777      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8302305000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='31008110         ',LOANDTLACHBNKROUTNMB='311372744      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7615105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3115103564       ',LOANDTLACHBNKROUTNMB='083000108      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3540235006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='312018174        ',LOANDTLACHBNKROUTNMB='111901519      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9093545010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='316574           ',LOANDTLACHBNKROUTNMB='091210074      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6529545008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='319476           ',LOANDTLACHBNKROUTNMB='091210074      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6145245002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3210251740       ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3724905000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='321130584        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3424885005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='335077295        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2236926009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3509004884       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9046214006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3600009553       ',LOANDTLACHBNKROUTNMB='111322994      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4457185008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3609201585       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3912475010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='362074363        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5436225002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='362097133        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3820225008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='362247230        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8577485007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3628236894       ',LOANDTLACHBNKROUTNMB='113024915      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7149255007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='366020127        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3581396000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='367013222        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6051085001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='367067968        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8129765004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='375005089557     ',LOANDTLACHBNKROUTNMB='072000805      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4605584006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3809000052       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3965005002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='381954           ',LOANDTLACHBNKROUTNMB='091209933      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3415726008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='381954           ',LOANDTLACHBNKROUTNMB='091209933      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4516575008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='381954           ',LOANDTLACHBNKROUTNMB='091209933      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5004425010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='381954           ',LOANDTLACHBNKROUTNMB='091209933      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5648555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='395029966        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3346705001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3956231488       ',LOANDTLACHBNKROUTNMB='121042882      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3218086007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='3988215806       ',LOANDTLACHBNKROUTNMB='111900659      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8170305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4001770678       ',LOANDTLACHBNKROUTNMB='042102694      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4831305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4021842748       ',LOANDTLACHBNKROUTNMB='211274382      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9167275005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4045149137       ',LOANDTLACHBNKROUTNMB='103003632      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5939994005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4087823          ',LOANDTLACHBNKROUTNMB='101100016      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8850185002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='409000310124     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5363185001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='41998519         ',LOANDTLACHBNKROUTNMB='061207839      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2770096009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='41998519         ',LOANDTLACHBNKROUTNMB='061207839      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3716195001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4234057251       ',LOANDTLACHBNKROUTNMB='041000124      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3136866001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4360527598       ',LOANDTLACHBNKROUTNMB='031201360      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8333445010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4402000428       ',LOANDTLACHBNKROUTNMB='061202371      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1521617000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='47134349         ',LOANDTLACHBNKROUTNMB='111909993      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2395096006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='488038438839     ',LOANDTLACHBNKROUTNMB='111000025      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6412965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='4971682006       ',LOANDTLACHBNKROUTNMB='021000089      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3269546003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='500017743        ',LOANDTLACHBNKROUTNMB='114903284      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4934964003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='500020264        ',LOANDTLACHBNKROUTNMB='114903284      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5745715000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='500022038        ',LOANDTLACHBNKROUTNMB='114903284      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6168395004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='500164009        ',LOANDTLACHBNKROUTNMB='063116177      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7457645008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5006023642       ',LOANDTLACHBNKROUTNMB='043318500      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5270595007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5021104447       ',LOANDTLACHBNKROUTNMB='055002406      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3182846007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='50230103         ',LOANDTLACHBNKROUTNMB='091300159      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6553405001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='509975217        ',LOANDTLACHBNKROUTNMB='114000093      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4248595006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5109000435       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6217045001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='524017613        ',LOANDTLACHBNKROUTNMB='121142287      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9657285000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='530032409        ',LOANDTLACHBNKROUTNMB='114000093      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6318415004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5311028996       ',LOANDTLACHBNKROUTNMB='042200910      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3131556003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5325801263       ',LOANDTLACHBNKROUTNMB='054000030      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2709166007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='542008006518     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5584275008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='550087105        ',LOANDTLACHBNKROUTNMB='091916941      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8646265005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5501122591       ',LOANDTLACHBNKROUTNMB='113024164      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5222105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='560019892306     ',LOANDTLACHBNKROUTNMB='067011760      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6688695004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5693446          ',LOANDTLACHBNKROUTNMB='031302971      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8829384002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5777770321       ',LOANDTLACHBNKROUTNMB='063107513      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8210335001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='586022738971     ',LOANDTLACHBNKROUTNMB='111000025      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2767106009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='5920006782       ',LOANDTLACHBNKROUTNMB='081006162      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6204885007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='592083716        ',LOANDTLACHBNKROUTNMB='267084131      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7119264002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6009006124       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8958554001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6110031090       ',LOANDTLACHBNKROUTNMB='211274573      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6704665007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6142269          ',LOANDTLACHBNKROUTNMB='075905910      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5343185004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6209203242       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7005435003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6213742089       ',LOANDTLACHBNKROUTNMB='036076150      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2730416004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='646109173        ',LOANDTLACHBNKROUTNMB='122100024      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3108236003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6475906          ',LOANDTLACHBNKROUTNMB='091408446      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7414195004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='64922442         ',LOANDTLACHBNKROUTNMB='101100029      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4647705010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='65245375         ',LOANDTLACHBNKROUTNMB='322484113      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3181966006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='668450390        ',LOANDTLACHBNKROUTNMB='221172212      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9168814007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='669082559        ',LOANDTLACHBNKROUTNMB='111000614      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7937995007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='671818107        ',LOANDTLACHBNKROUTNMB='111000614      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7244225007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='700006643        ',LOANDTLACHBNKROUTNMB='091400554      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6456665007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='70142396         ',LOANDTLACHBNKROUTNMB='021311529      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6544355008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7026670716       ',LOANDTLACHBNKROUTNMB='063107513      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8466405002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='709828557        ',LOANDTLACHBNKROUTNMB='021000021      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2659136010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7241200648       ',LOANDTLACHBNKROUTNMB='221672851      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3195756003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='770738           ',LOANDTLACHBNKROUTNMB='011302742      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4456955001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='77101291         ',LOANDTLACHBNKROUTNMB='102201710      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9523185010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7751014742       ',LOANDTLACHBNKROUTNMB='063107513      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7867365010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='779709778        ',LOANDTLACHBNKROUTNMB='111000614      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7016955001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7815704745       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6237705006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7835343067       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3541886000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7852494343       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5597215000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7856783118       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5278215003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7879197283       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4792835010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7884827414       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5389855010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7888811524       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6548625003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7898134325       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9244615005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7902540225       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2511156009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7909592894       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6822035007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7935099577       ',LOANDTLACHBNKROUTNMB='111900659      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8702074006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7946808295       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6840245004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='800077551301     ',LOANDTLACHBNKROUTNMB='061107816      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4555165006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8092002578       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9659964000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='818457541        ',LOANDTLACHBNKROUTNMB='124001545      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4237965004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='836750286        ',LOANDTLACHBNKROUTNMB='072000326      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9354134008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9005159504       ',LOANDTLACHBNKROUTNMB='211174181      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6585635005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9145841017       ',LOANDTLACHBNKROUTNMB='266086554      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5540924004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='91944            ',LOANDTLACHBNKROUTNMB='221581641      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4061835006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9200168634       ',LOANDTLACHBNKROUTNMB='301171353      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7404425008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9200170291       ',LOANDTLACHBNKROUTNMB='301171353      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7087575002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9853258775       ',LOANDTLACHBNKROUTNMB='267090594      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7004855009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='9866385090       ',LOANDTLACHBNKROUTNMB='022000046      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7030875008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='989006838        ',LOANDTLACHBNKROUTNMB='111000614      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8032725007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='575001259        ',LOANDTLACHBNKROUTNMB='101101293      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2657656009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1056638          ',LOANDTLACHBNKROUTNMB='103902717      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7816015010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='011499923        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8891404007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='024144215        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5979454001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='054385407        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6521235004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='067119360        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9132375000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='79014923         ',LOANDTLACHBNKROUTNMB='124102509      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2813186002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='082419809        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5678245000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='082419809        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7230075009'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0109219396       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3821275003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0109219396       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3821285006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='0109219396       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6113894008'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='110006172        ',LOANDTLACHBNKROUTNMB='051404464      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240175004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='210849345        ',LOANDTLACHBNKROUTNMB='031100102      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3444465005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='213285559        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8304335002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='318350976        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3856694004'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='351235228        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6227664007'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='351235228        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8212985001'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='362246954        ',LOANDTLACHBNKROUTNMB='021502011      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7180765005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1030021750       ',LOANDTLACHBNKROUTNMB='121201814      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2433356003'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='2300298602       ',LOANDTLACHBNKROUTNMB='104913912      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3098226006'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;                                                                                                   
                                                                                                  
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='6300058567       ',LOANDTLACHBNKROUTNMB='221571473      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5744875000'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='7938601907       ',LOANDTLACHBNKROUTNMB='221571415      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3379066002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='8175728586       ',LOANDTLACHBNKROUTNMB='061000227      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2898166005'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='221000461213     ',LOANDTLACHBNKROUTNMB='021502804      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6445855010'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL 
set LOANDTLACHBNKACCT='1100000635454    ',LOANDTLACHBNKROUTNMB='263191387      ',                  
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1015146002'        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL;
                                                                                                    
commit;  

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\remove_ach_info_UAT.sql"
set echo on
set feedback on
select '*** Remove ACH information***' from dual;
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9532387103';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9534047404';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9534787207';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9571877100';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9572327809';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9584905005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9653915002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9716455004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9722965000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9769587810';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9788185005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9828847806';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9863967802';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9871127002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9906375005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1062867005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1102387009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1201527401';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1322087407';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1396307404';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1452808000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1498927109';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1514397400';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1533047406';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1540118005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1540587010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1575248009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1588537800';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1656427010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1660667102';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1732617000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1791817110';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1871687108';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1888867404';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='1917797407';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2010377809';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2012828007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2027427807';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2090837009';        
                                                                                                                                                                                                       
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2244297801';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2256597010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2328637009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2333387000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2333607005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2338687006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2396857105';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2405327004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2408947008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2489817005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2493127002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2522467010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2554987007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2561957007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2568087009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2583927010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2585337003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2591497001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2598937408';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2599167001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2599937007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2624407004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2626307002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2631707009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2639507002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2644057003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2647547003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2673367001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2687677001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2688667000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2688847006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2725947001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2736347004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2739427010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2794697002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2795787007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2797997004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2798027008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2801377010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2807757900';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2814197003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2822737010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2830907002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2835747905';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2839907009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2849017409';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2858207006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2861087008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2874207006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2875787007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2885477001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2913547410';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2913867009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2918177002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2918497004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2920407009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2923347010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2946957008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2952167003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2963497003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2982487006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='2991747907';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3005827007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3035107004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3037107008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3038877007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3041337010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3051207010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3051287001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3051327000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3051477010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3054897002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3060657007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3067107009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3067367006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3072667007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3075127004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3080887003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3100037004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3104707001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3115257003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3117857010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3140677209';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3140877210';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3142257205';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3159737007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3165187200';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3168277209';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3169237210';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3170457007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3171087201';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3187127009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3196837001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3198137105';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3223437104';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3227717004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3238297004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3255127001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3256067205';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3261157202';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3262897203';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3279297000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3298617006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3302977307';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3306797200';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3307157002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3313537201';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3319277207';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3324747004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3335237402';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3335917002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3348587009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3359977003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3362867202';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3375417010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3395897000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3396047005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3398977006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3405637008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3447027207';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3447417206';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3447637202';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3448187200';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3449157204';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3451887210';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3454527202';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3455747200';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3457277205';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3457307201';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3457487209';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3457777202';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3479077009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3485677204';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3487337004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3492217008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3492607410';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3499037005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3499347004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3501377002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3518047004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3518837005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3521537010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3524177004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3526617009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3526697000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3533727005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3536787007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3537477010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3541477002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3550207004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3559927004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3566317007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3566797000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3569917005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3570537006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3570607003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3571837004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3572677006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3573937003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3576787001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3578977003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3584357001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3585547001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3588907008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3592507401';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3593777000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3595967002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3600007405';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3602927000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3605027410';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3605277001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3605627010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3610037004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3610917008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3611607000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3619717406';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3621007001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3624267004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3628177009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3635567006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3637487010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3639167001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3652017007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3660997008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3661017009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3661037004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3661137010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3662817004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3672877004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3674767010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3675477008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3678287005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3679127005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3680307008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3680607004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3684247000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3687587003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3691247009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3692007009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3692677000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3707677004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3710107009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3710187000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3713857006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3714077010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3722977009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3724177009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3725187003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3726377003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3726647001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3729537009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3731707000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3732477005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3732867004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3733507003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3736197001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3736857009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3737637004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3738347002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3739207008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3740007004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3742197008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3746357005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3748117007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3755687001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3756677000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3757267008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3758267010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3762557006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3763527010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3764197009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3764597000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3766707000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3768627004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3768927000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3769557009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3770207006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3772037007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3773567004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3776357006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3778327001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3779907000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3780707007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3782827001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3797357005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3800237006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3801567002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3802247002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3804437004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3809387001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3810537006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3814527000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3816897010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3821707004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3825057007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3826157004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3828247000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3832917003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3836377004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3836457004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3837067007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3841957006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3847837006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3847897002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3848787006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3852437009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3853597002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3856367009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3857347005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3860777006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3861427008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3861467401';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3861577007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3861627009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3864497002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3866107005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3867157000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3867237000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3868247005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3868317002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3875387010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3876027009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3876197003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3876407005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3879557010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3884657010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3888447003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3889347010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3897647006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3898197004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3898337009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3899457001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3899547004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3900637005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3902117006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3904167003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3905897001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3906587004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3909127404';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3913237002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3913517003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3914657001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3916707007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3918157001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3919047005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3919617010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3920007001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3922187002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3922447008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3925897009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3926747001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3927877007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3930067001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3931677009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3933067007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3938887005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3940097003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3940327000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3950487006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3954587009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3955617301';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3955677003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3956877006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3957327203';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3957367008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3957757007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3961327010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3961787008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3962177004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3963437001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3963737008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3965927010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3967037007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3967957001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3970377005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3972877006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3973217009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3973497001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3980757005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3981057009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3986877007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3987667005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3990747006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3991657005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3992237010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3992857008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3993067009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3993777010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3994977002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3996627006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='3997237009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4003587008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4004657007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4006447007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4010187004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4012527003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4013377008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4013797005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4016227004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4020207001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4023367009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4024007008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4025037008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4027157002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4028247007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4028657001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4029667006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4030377010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4036447008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4038037007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4038157008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4039707009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4040117006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4042637002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4042817008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4043277003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4043687008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4044327007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4046547007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4047817007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4050007001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4050217005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4051547001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4055267008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4055697008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4059617003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4060217009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4069317000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4069637002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4070717010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4074027001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4074307002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4075977006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4077967007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4078237002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4081397005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4082267003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4084067006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4084247001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4084297005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4085647005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4087397006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4089107004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4090077005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4090097000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4091957000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4095547003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4095707003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4098057004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4100267008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4100647004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4100687005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4100777008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4101777010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4104727001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4105517010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4107597005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4107617009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4109407009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4110147000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4111427003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4111807010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4114247003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4114827000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4115857000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4119257005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4119277000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4119647004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4119817007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4120377003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4122787001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4123027009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4123067010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4123297009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4123617009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4124977003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4126527001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4126737005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4129527410';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4129747003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4131647007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4131877006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4131997007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4133577003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4134707007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4135247002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4135937008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4136137006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4136617008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4137387002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4137467002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4138637007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4138797009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4139197008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4139837010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4140627003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4140827004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4140907004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4141797010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4143237010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4143557001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4146017009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4146307002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4146707004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4146877009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4146897004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4150607001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4152137006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4152397003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4152727006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4153437004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4155407805';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4156707008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4158317002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4158407005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4158567007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4159477006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4160397003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4161967010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4164427007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4166407005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4168417001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4168787007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4169347006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4169707007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4169757000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4172087001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4172507007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4172547008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4173897010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4173947001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4174217007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4174987004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4175017008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4175137009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4176227003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4176237006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4176627005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4179027008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4179727006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4180007010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4180497006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4181197001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4181227008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4181757003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4181857009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4182067010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4182107009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4184057000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4185367001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4186257005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4186437000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4187687007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4187767007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4187807006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4188077003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4190477004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4191057009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4191147001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4191337010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4191367008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4192847001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4193287001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4193687003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4195557003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4195607005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4195727006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4195737009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4196517004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4197467004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4197917008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4198557009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4198687002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4198717009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4198737004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4199497006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4200277008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4200757010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4200827007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4200947008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201027005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201037008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201147006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201187007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201237009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201247001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4201627008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4202247003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4202667000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4203867003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4203977001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4204717006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4205407009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4205697004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4206197009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4206667008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4206787009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4206867009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4207377006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4207407002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4207437000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4208287005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4208617008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4208647006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4209437004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4209567008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4209717005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4209957007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4210567005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4211367006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4211417008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4211947003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4212307001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4212597007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4213277007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4213337001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4214247000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4214277009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4215997004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4216587001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4216727006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4216897000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4217837006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4218347003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219217001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219277008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219387006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219427005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219507005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219517008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219527000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219567001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219617003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219707006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4219747007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4220447008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4221237006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4221627005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4222317008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4222587008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4223257005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4223687005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4224347010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4224907001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4225017007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4225417009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4225827003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4226347003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4226817002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227247010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227377003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227387006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227467006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227477009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227497004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227717009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227837010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227927002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4227997001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4228907009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4229357001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4230827008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4231517000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4231727004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4231777008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4231787000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4231817007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4232427010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4233157003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4233397005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4233807008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4233917006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4234097000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4234987007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4235067004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4235687002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4235957000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4236427007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4236997003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4237087003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4238237002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4238377009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4238657010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4238777000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4238857000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4239247007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240117000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240257007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240317001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240597004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240877005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240927007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4240977000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4242937003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4242977004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4242997010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4243057001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4243087010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4243397009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4243417002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4243577004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4245257006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4246137007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4246317002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4247337010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4247787005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4250837008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4251317007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4252317009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4253077000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4253377007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4253697009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4254437003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4255847010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4256137000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4256767001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4256917009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4257557010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4258447003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4258967006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4259817009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4260307006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4260467008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4262717004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4262857000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4263137009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4263297000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4263537000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4264937004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4265207010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4265667008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4266777008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4267327004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4267617008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4267987003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4268837006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4268907003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4269447009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4269637007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4270687008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4271337010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4272597009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4273067005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4273457004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4274177005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4274907010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4275297008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4275567006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4275847007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4277487010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4277667005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4277897004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4278977006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4279697007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4280387005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4281597000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4282617006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4283537008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4284207005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4284277004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4284367007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4284587003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4285467004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4285517006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4285727010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4285977004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4289667002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4290627009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4291927007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4293477007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4293827005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4293927000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4296357001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4296697009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4298817001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4299007007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4299037005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4300547003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4300647009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4301157006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4301307003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4303287003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4303537006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4305417009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4306457001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4306937003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4308947010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4310087000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4310767003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4311047001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4311557001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4311587010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4312047003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4312147009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4312467000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4312927007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4313227000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4313587003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4314097000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4316047000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4316177004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4316917001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4316927004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4317477002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4317717002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4318407005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4318837005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4318917005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4319367008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4319897003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4321297010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4321347001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4321947004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4323387006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4323467006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4323827007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4324297005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4324337004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4324437010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4327217009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4328537002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4328557008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4329437009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4329727002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4330377001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4331707006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4331887003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4332567003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4332917001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4334407005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4335497001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4337937006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4338527003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4338627009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4339037000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4339617008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4356247006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4364907003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4391187007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4411317007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4458307001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4471317107';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4481317002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4509017804';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4673367000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4825667007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4923737803';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4939457001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='4962947102';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5134327800';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5231677105';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5297757108';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5436477903';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5665407004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5702877005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='5923427003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6033277107';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6227507407';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6234687402';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6260047402';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6280747800';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6282887408';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6294937407';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6297817902';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6352367107';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6369147107';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6408007104';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6569737101';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6584077004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6721657902';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6723357006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6831437406';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6847427800';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6849937107';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6869147103';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6876987302';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6909397103';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='6957317106';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7199907002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7352857008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7399357106';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7438467010';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7593175003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7621107201';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='7899697208';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8064775005';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8169577003';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8257447402';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8262647310';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8329687404';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8339377801';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8350687801';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8372467403';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8455287403';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8618195000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8623265002';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8631025000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8645537102';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8693205001';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8701227000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8781537009';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8816167300';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8842247903';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8873225004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8932157403';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='8993347307';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9052537101';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9066387007';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9089697303';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9139447101';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9142287004';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9160847402';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9193075008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9201607909';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9216615008';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9360155006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9371725006';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9414695000';        
                                                                                                    
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9442085002';        
                                                                                                                                                                                                        
update STGCSA.SOFVLND1TBL  set LOANDTLACHBNKACCT='  ',LOANDTLACHBNKROUTNMB='  ', 
LASTUPDTDT=SYSDATE, LASTUPDTUSERID=USER  where LOANNMB='9532085006';        
                                                                                                    
commit;  

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

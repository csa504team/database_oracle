define deploy_name=Sep_ACH_Debit
define package_name=check_diff_ach
define package_buildtime=20200823222047
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Sep_ACH_Debit_check_diff_ach created on Sun 08/23/2020 22:20:49.17 by Jasleen Gorowada
prompt deploy scripts for deploy Sep_ACH_Debit_check_diff_ach created on Sun 08/23/2020 22:20:49.17 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Sep_ACH_Debit_check_diff_ach: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script.sql"
set echo on
set feedback on
select '*** testing to see if loan exisits with ACH blank***' from dual;
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2128946007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2681936010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6355315008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8640654007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7354644009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3404086000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6916495006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7932495000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7008985010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3413085006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3570505005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2600196001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5110425003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8062885010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2590236006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2901616000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6890175005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4444725005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2968456005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7605035005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7818255005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7570124000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3745825010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5147565003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6195665008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8059995007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5839975009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6175075000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7278755000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9332734010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7258854002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8542534002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8168945002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8804255009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9128554007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6402445005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7808805000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5074374007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6605235001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2576306007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6547855000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2896356010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5202975001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7191625004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6670335003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8057235009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6163785004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2776976003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5993675000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4121395008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2511096004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3192276006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7435415008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4193855003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7125735004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7125545006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7797175005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8793154000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3161286006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7061005008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4655995005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8007425009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2306876003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9128994010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4599495002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5116475008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5479955009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3355935002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6049135004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3338856007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7599065006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3222566001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2670806000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9063005000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9394764003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5323685004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5499725007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6756845004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5528235002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9047535006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4724735002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5992625005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6829045002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3617474000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9092935007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6733535002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4149605001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3701885004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5906884000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8928225002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7689925008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4218545001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4529925005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5354915004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6889955006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5071475003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2588696005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5225035004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3578806002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5340305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5553435005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4741615003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4902095004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4256425001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4121935004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5458805000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4639635002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3737495007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8240965003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5291155003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1932777007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4381535009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6568465000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5100035000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3035776010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3637735010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6335995005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6350815006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6450205008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7943405001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7943475000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4778395002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2538636000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4998415008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2891466009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6362655003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4941044007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4968565006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3690796001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2899666004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5720015006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6425045006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6290125008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5165665002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7737874005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3292866003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9061225003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8470265000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2794436009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7189894009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9228145006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3104136000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7500454010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6735365003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6978515005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9042025007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7759034001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6370255001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6451005009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1519056003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1177897003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2524517005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9182865003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4947005003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2209346006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4772645004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4911565005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5165925008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4573775005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4506725001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2602956008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9345105004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6601915007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8182485002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7395275008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5671385004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4277265000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4574465008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4950715005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9209704006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7036355006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5195475005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6662415003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6196995004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6104094002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3180236008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7963215000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8964774004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7615985001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8302305000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7615105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3540235006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9093545010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6529545008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6145245002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3724905000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3424885005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2236926009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9046214006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4457185008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3912475010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5436225002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3820225008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8577485007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7149255007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3581396000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6051085001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8129765004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4605584006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3965005002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3415726008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4516575008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5004425010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5648555009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3346705001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3218086007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8170305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4831305008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9167275005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5939994005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8850185002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5363185001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2770096009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3716195001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3136866001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8333445010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1521617000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2395096006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6412965001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3269546003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4934964003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5745715000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6168395004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7457645008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5270595007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3182846007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6553405001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4248595006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6217045001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9657285000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6318415004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3131556003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2709166007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5584275008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8646265005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5222105006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6688695004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8829384002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8210335001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2767106009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6204885007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7119264002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8958554001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6704665007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5343185004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7005435003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2730416004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3108236003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7414195004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4647705010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3181966006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9168814007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7937995007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7244225007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6456665007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6544355008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8466405002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2659136010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3195756003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4456955001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9523185010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7867365010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7016955001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6237705006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3541886000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5597215000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5278215003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4792835010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5389855010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6548625003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9244615005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2511156009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6822035007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8702074006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6840245004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4555165006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9659964000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4237965004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9354134008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6585635005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5540924004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4061835006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7404425008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7087575002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7004855009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7030875008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8032725007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2657656009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2162596002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7816015010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8891404007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5979454001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6521235004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9132375000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2813186002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5678245000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7230075009'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3821275003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3821285006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6113894008'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4240175004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3444465005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8304335002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3856694004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6227664007'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8212985001'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7180765005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2433356003'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3098226006'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9446065004'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5744875000'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3379066002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2898166005'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6445855010'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1015146002'        
 and ltrim(LOANDTLACHBNKACCT) IS NULL  and ltrim(LOANDTLACHBNKROUTNMB) IS NULL;
                                                                                                    
select '*** testing to see if ACH already set as expected***' from dual;
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2128946007'        
 and rtrim(LOANDTLACHBNKACCT) ='0000007690'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113025723';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2681936010'        
 and rtrim(LOANDTLACHBNKACCT) ='0000031625'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211871691';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6355315008'        
 and rtrim(LOANDTLACHBNKACCT) ='0000036420'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171007';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8640654007'        
 and rtrim(LOANDTLACHBNKACCT) ='0000178013'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='107007139';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7354644009'        
 and rtrim(LOANDTLACHBNKACCT) ='0000281203146'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='061000104';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3404086000'        
 and rtrim(LOANDTLACHBNKACCT) ='0005201339873'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='053101121';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6916495006'        
 and rtrim(LOANDTLACHBNKACCT) ='0005793581314'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='113011258';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7932495000'        
 and rtrim(LOANDTLACHBNKACCT) ='00177949'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='303986096';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7008985010'        
 and rtrim(LOANDTLACHBNKACCT) ='0022434054'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='125108272';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3413085006'        
 and rtrim(LOANDTLACHBNKACCT) ='0023335306'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='066004367';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3570505005'        
 and rtrim(LOANDTLACHBNKACCT) ='003000788'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='241270851';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2600196001'        
 and rtrim(LOANDTLACHBNKACCT) ='00350001113439'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='041001039';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5110425003'        
 and rtrim(LOANDTLACHBNKACCT) ='003670292229'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='063000047';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8062885010'        
 and rtrim(LOANDTLACHBNKACCT) ='0073084305'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042000314';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2590236006'        
 and rtrim(LOANDTLACHBNKACCT) ='0077636996'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063104668';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2901616000'        
 and rtrim(LOANDTLACHBNKACCT) ='0084700'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='114911807';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6890175005'        
 and rtrim(LOANDTLACHBNKACCT) ='008982'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='101111704';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4444725005'        
 and rtrim(LOANDTLACHBNKACCT) ='01 9204997'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2968456005'        
 and rtrim(LOANDTLACHBNKACCT) ='010183516505'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='066011392';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7605035005'        
 and rtrim(LOANDTLACHBNKACCT) ='010212979505'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='066011392';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7818255005'        
 and rtrim(LOANDTLACHBNKACCT) ='011269359'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7570124000'        
 and rtrim(LOANDTLACHBNKACCT) ='011356901'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3745825010'        
 and rtrim(LOANDTLACHBNKACCT) ='011652659'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5147565003'        
 and rtrim(LOANDTLACHBNKACCT) ='0121616523'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063104668';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6195665008'        
 and rtrim(LOANDTLACHBNKACCT) ='0131839'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='061101197';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8059995007'        
 and rtrim(LOANDTLACHBNKACCT) ='015281132'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5839975009'        
 and rtrim(LOANDTLACHBNKACCT) ='0185595'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065200515';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6175075000'        
 and rtrim(LOANDTLACHBNKACCT) ='0186254'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065200515';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7278755000'        
 and rtrim(LOANDTLACHBNKACCT) ='0194654'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='011701314';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9332734010'        
 and rtrim(LOANDTLACHBNKACCT) ='027478769'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7258854002'        
 and rtrim(LOANDTLACHBNKACCT) ='029118627'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124000054';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8542534002'        
 and rtrim(LOANDTLACHBNKACCT) ='029711004903'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8168945002'        
 and rtrim(LOANDTLACHBNKACCT) ='030268117'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8804255009'        
 and rtrim(LOANDTLACHBNKACCT) ='030268117'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9128554007'        
 and rtrim(LOANDTLACHBNKACCT) ='030268117'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6402445005'        
 and rtrim(LOANDTLACHBNKACCT) ='0309204863'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7808805000'        
 and rtrim(LOANDTLACHBNKACCT) ='031061141'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5074374007'        
 and rtrim(LOANDTLACHBNKACCT) ='035368713'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6605235001'        
 and rtrim(LOANDTLACHBNKACCT) ='0361056065'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='304971932';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2576306007'        
 and rtrim(LOANDTLACHBNKACCT) ='0362182180'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='102300129';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6547855000'        
 and rtrim(LOANDTLACHBNKACCT) ='041466462'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2896356010'        
 and rtrim(LOANDTLACHBNKACCT) ='048 224839'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5202975001'        
 and rtrim(LOANDTLACHBNKACCT) ='0500001193'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063116177';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7191625004'        
 and rtrim(LOANDTLACHBNKACCT) ='0500001193'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063116177';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6670335003'        
 and rtrim(LOANDTLACHBNKACCT) ='0500139472'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063116177';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8057235009'        
 and rtrim(LOANDTLACHBNKACCT) ='0501664163'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='082902757';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6163785004'        
 and rtrim(LOANDTLACHBNKACCT) ='050252089'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2776976003'        
 and rtrim(LOANDTLACHBNKACCT) ='05096118'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='062202574';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5993675000'        
 and rtrim(LOANDTLACHBNKACCT) ='052025829'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4121395008'        
 and rtrim(LOANDTLACHBNKACCT) ='0551102809'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='103003632';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2511096004'        
 and rtrim(LOANDTLACHBNKACCT) ='05512307'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='123206338';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3192276006'        
 and rtrim(LOANDTLACHBNKACCT) ='0578340'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='061112364';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7435415008'        
 and rtrim(LOANDTLACHBNKACCT) ='0608009083'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='253271822';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4193855003'        
 and rtrim(LOANDTLACHBNKACCT) ='061377821'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7125735004'        
 and rtrim(LOANDTLACHBNKACCT) ='061488372'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7125545006'        
 and rtrim(LOANDTLACHBNKACCT) ='061488372'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7797175005'        
 and rtrim(LOANDTLACHBNKACCT) ='061531685'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8793154000'        
 and rtrim(LOANDTLACHBNKACCT) ='063005433'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021206249';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3161286006'        
 and rtrim(LOANDTLACHBNKACCT) ='068127561'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7061005008'        
 and rtrim(LOANDTLACHBNKACCT) ='0781106305'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='091302966';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4655995005'        
 and rtrim(LOANDTLACHBNKACCT) ='08029237'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='121301772';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8007425009'        
 and rtrim(LOANDTLACHBNKACCT) ='0816222078'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='271183701';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2306876003'        
 and rtrim(LOANDTLACHBNKACCT) ='0850053846'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='071925567';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9128994010'        
 and rtrim(LOANDTLACHBNKACCT) ='09986241'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='114902560';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4599495002'        
 and rtrim(LOANDTLACHBNKACCT) ='1000254555'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111916326';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5116475008'        
 and rtrim(LOANDTLACHBNKACCT) ='100086355'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400554';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5479955009'        
 and rtrim(LOANDTLACHBNKACCT) ='100086652'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400554';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3355935002'        
 and rtrim(LOANDTLACHBNKACCT) ='1002019595'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211885250';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6049135004'        
 and rtrim(LOANDTLACHBNKACCT) ='1002344016'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113025723';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3338856007'        
 and rtrim(LOANDTLACHBNKACCT) ='10031729'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='011601087';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7599065006'        
 and rtrim(LOANDTLACHBNKACCT) ='100646744'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='075911742';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3222566001'        
 and rtrim(LOANDTLACHBNKACCT) ='1010001076484'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='062203984';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2670806000'        
 and rtrim(LOANDTLACHBNKACCT) ='1010071149'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='011301798';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9063005000'        
 and rtrim(LOANDTLACHBNKACCT) ='1010190105'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='067011812';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9394764003'        
 and rtrim(LOANDTLACHBNKACCT) ='1010748270'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='074903719';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5323685004'        
 and rtrim(LOANDTLACHBNKACCT) ='10109696'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='041212983';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5499725007'        
 and rtrim(LOANDTLACHBNKACCT) ='1011529'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='111916180';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6756845004'        
 and rtrim(LOANDTLACHBNKACCT) ='1030047653'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5528235002'        
 and rtrim(LOANDTLACHBNKACCT) ='1033197987'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='124003116';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9047535006'        
 and rtrim(LOANDTLACHBNKACCT) ='105285112'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086300012';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4724735002'        
 and rtrim(LOANDTLACHBNKACCT) ='1059777'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='111922624';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5992625005'        
 and rtrim(LOANDTLACHBNKACCT) ='1060107140637'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='044102362';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6829045002'        
 and rtrim(LOANDTLACHBNKACCT) ='1070597131'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3617474000'        
 and rtrim(LOANDTLACHBNKACCT) ='11000000449362'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='321177586';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9092935007'        
 and rtrim(LOANDTLACHBNKACCT) ='11000000514283'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='321177586';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6733535002'        
 and rtrim(LOANDTLACHBNKACCT) ='1100035052'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121101985';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4149605001'        
 and rtrim(LOANDTLACHBNKACCT) ='1110027400'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='114903284';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3701885004'        
 and rtrim(LOANDTLACHBNKACCT) ='111042561'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121135045';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5906884000'        
 and rtrim(LOANDTLACHBNKACCT) ='111268389'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8928225002'        
 and rtrim(LOANDTLACHBNKACCT) ='111692822'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7689925008'        
 and rtrim(LOANDTLACHBNKACCT) ='112249280'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4218545001'        
 and rtrim(LOANDTLACHBNKACCT) ='1131346647'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043000096';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4529925005'        
 and rtrim(LOANDTLACHBNKACCT) ='115490019'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5354915004'        
 and rtrim(LOANDTLACHBNKACCT) ='1190915211'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6889955006'        
 and rtrim(LOANDTLACHBNKACCT) ='1210306506'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5071475003'        
 and rtrim(LOANDTLACHBNKACCT) ='1223380'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='111923607';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2588696005'        
 and rtrim(LOANDTLACHBNKACCT) ='1248218'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065204443';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5225035004'        
 and rtrim(LOANDTLACHBNKACCT) ='125076310'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='064209216';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3578806002'        
 and rtrim(LOANDTLACHBNKACCT) ='1251800538'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211170253';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5340305008'        
 and rtrim(LOANDTLACHBNKACCT) ='1289496'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='065204443';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5553435005'        
 and rtrim(LOANDTLACHBNKACCT) ='1300000297990'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='291479974';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4741615003'        
 and rtrim(LOANDTLACHBNKACCT) ='1310730381'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4902095004'        
 and rtrim(LOANDTLACHBNKACCT) ='1310756780'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4256425001'        
 and rtrim(LOANDTLACHBNKACCT) ='133029492'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4121935004'        
 and rtrim(LOANDTLACHBNKACCT) ='133103129'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5458805000'        
 and rtrim(LOANDTLACHBNKACCT) ='1340004427652'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='053101121';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4639635002'        
 and rtrim(LOANDTLACHBNKACCT) ='134253965'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3737495007'        
 and rtrim(LOANDTLACHBNKACCT) ='1343491'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='063115505';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8240965003'        
 and rtrim(LOANDTLACHBNKACCT) ='138233667'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5291155003'        
 and rtrim(LOANDTLACHBNKACCT) ='139560019'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='086518477';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1932777007'        
 and rtrim(LOANDTLACHBNKACCT) ='1400000174151'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='241279616';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4381535009'        
 and rtrim(LOANDTLACHBNKACCT) ='141233141'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6568465000'        
 and rtrim(LOANDTLACHBNKACCT) ='1431225'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='075917937';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5100035000'        
 and rtrim(LOANDTLACHBNKACCT) ='1440643992'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3035776010'        
 and rtrim(LOANDTLACHBNKACCT) ='149896'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100906';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3637735010'        
 and rtrim(LOANDTLACHBNKACCT) ='1509202079'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6335995005'        
 and rtrim(LOANDTLACHBNKACCT) ='152316024792'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='081000210';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6350815006'        
 and rtrim(LOANDTLACHBNKACCT) ='154006025'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6450205008'        
 and rtrim(LOANDTLACHBNKACCT) ='156377896'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7943405001'        
 and rtrim(LOANDTLACHBNKACCT) ='157384799'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7943475000'        
 and rtrim(LOANDTLACHBNKACCT) ='157384799'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4778395002'        
 and rtrim(LOANDTLACHBNKACCT) ='160029902'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2538636000'        
 and rtrim(LOANDTLACHBNKACCT) ='1615007052'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4998415008'        
 and rtrim(LOANDTLACHBNKACCT) ='1620011880'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='102103407';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2891466009'        
 and rtrim(LOANDTLACHBNKACCT) ='163070507815'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='091300023';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6362655003'        
 and rtrim(LOANDTLACHBNKACCT) ='170000638312'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='111301122';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4941044007'        
 and rtrim(LOANDTLACHBNKACCT) ='170100154'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4968565006'        
 and rtrim(LOANDTLACHBNKACCT) ='1771128343'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3690796001'        
 and rtrim(LOANDTLACHBNKACCT) ='1881268047'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000753';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2899666004'        
 and rtrim(LOANDTLACHBNKACCT) ='19114959'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='111310870';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5720015006'        
 and rtrim(LOANDTLACHBNKACCT) ='2000004065'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061120479';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6425045006'        
 and rtrim(LOANDTLACHBNKACCT) ='2000011615'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063116083';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6290125008'        
 and rtrim(LOANDTLACHBNKACCT) ='2000013223'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='067015313';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5165665002'        
 and rtrim(LOANDTLACHBNKACCT) ='2000030480669'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='111900659';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7737874005'        
 and rtrim(LOANDTLACHBNKACCT) ='20013331'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='113111983';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3292866003'        
 and rtrim(LOANDTLACHBNKACCT) ='20015982'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091901202';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9061225003'        
 and rtrim(LOANDTLACHBNKACCT) ='20051093'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='011300595';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8470265000'        
 and rtrim(LOANDTLACHBNKACCT) ='201756615'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='101015282';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2794436009'        
 and rtrim(LOANDTLACHBNKACCT) ='203296637'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7189894009'        
 and rtrim(LOANDTLACHBNKACCT) ='2090002237452'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9228145006'        
 and rtrim(LOANDTLACHBNKACCT) ='21099556'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='113111983';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3104136000'        
 and rtrim(LOANDTLACHBNKACCT) ='2115002295'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7500454010'        
 and rtrim(LOANDTLACHBNKACCT) ='237016578218'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='053000196';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6735365003'        
 and rtrim(LOANDTLACHBNKACCT) ='239270490'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6978515005'        
 and rtrim(LOANDTLACHBNKACCT) ='2409202675'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9042025007'        
 and rtrim(LOANDTLACHBNKACCT) ='2428384272'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274450';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7759034001'        
 and rtrim(LOANDTLACHBNKACCT) ='245003398'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='101101293';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6370255001'        
 and rtrim(LOANDTLACHBNKACCT) ='2489619'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='031309945';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6451005009'        
 and rtrim(LOANDTLACHBNKACCT) ='2500303535'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='074900356';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1519056003'        
 and rtrim(LOANDTLACHBNKACCT) ='250374633'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1177897003'        
 and rtrim(LOANDTLACHBNKACCT) ='2506021'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091915890';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2524517005'        
 and rtrim(LOANDTLACHBNKACCT) ='2506021'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091915890';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9182865003'        
 and rtrim(LOANDTLACHBNKACCT) ='2506021'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091915890';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4947005003'        
 and rtrim(LOANDTLACHBNKACCT) ='2509201981'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2209346006'        
 and rtrim(LOANDTLACHBNKACCT) ='2510320201'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081503704';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4772645004'        
 and rtrim(LOANDTLACHBNKACCT) ='2531226545'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113010547';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4911565005'        
 and rtrim(LOANDTLACHBNKACCT) ='25500011910071'                                                     
  and rtrim(LOANDTLACHBNKROUTNMB) ='111901056';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5165925008'        
 and rtrim(LOANDTLACHBNKACCT) ='256000883'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4573775005'        
 and rtrim(LOANDTLACHBNKACCT) ='256000883'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4506725001'        
 and rtrim(LOANDTLACHBNKACCT) ='266132'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='101101413';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2602956008'        
 and rtrim(LOANDTLACHBNKACCT) ='2677031136'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='324170085';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9345105004'        
 and rtrim(LOANDTLACHBNKACCT) ='2830001497'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171007';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6601915007'        
 and rtrim(LOANDTLACHBNKACCT) ='2964141838'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111900659';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8182485002'        
 and rtrim(LOANDTLACHBNKACCT) ='297000664939'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7395275008'        
 and rtrim(LOANDTLACHBNKACCT) ='297001750656'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5671385004'        
 and rtrim(LOANDTLACHBNKACCT) ='3000029433'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113103276';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4277265000'        
 and rtrim(LOANDTLACHBNKACCT) ='3000389377'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031315544';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4574465008'        
 and rtrim(LOANDTLACHBNKACCT) ='3000788'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='241270851';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4950715005'        
 and rtrim(LOANDTLACHBNKACCT) ='3003140996'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502341';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9209704006'        
 and rtrim(LOANDTLACHBNKACCT) ='300350328'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='211674775';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7036355006'        
 and rtrim(LOANDTLACHBNKACCT) ='3004522396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502341';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5195475005'        
 and rtrim(LOANDTLACHBNKACCT) ='3004554549'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502341';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6662415003'        
 and rtrim(LOANDTLACHBNKACCT) ='3004841033'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502341';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6196995004'        
 and rtrim(LOANDTLACHBNKACCT) ='3004893181'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502341';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6104094002'        
 and rtrim(LOANDTLACHBNKACCT) ='3005120912'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='055002406';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3180236008'        
 and rtrim(LOANDTLACHBNKACCT) ='3009001083'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7963215000'        
 and rtrim(LOANDTLACHBNKACCT) ='3009203010'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8964774004'        
 and rtrim(LOANDTLACHBNKACCT) ='301202197'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='113103276';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7615985001'        
 and rtrim(LOANDTLACHBNKACCT) ='305089668'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8302305000'        
 and rtrim(LOANDTLACHBNKACCT) ='31003733'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='071922777';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7615105006'        
 and rtrim(LOANDTLACHBNKACCT) ='31008110'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='311372744';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3540235006'        
 and rtrim(LOANDTLACHBNKACCT) ='3115103564'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='083000108';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9093545010'        
 and rtrim(LOANDTLACHBNKACCT) ='312018174'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111901519';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6529545008'        
 and rtrim(LOANDTLACHBNKACCT) ='316574'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091210074';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6145245002'        
 and rtrim(LOANDTLACHBNKACCT) ='319476'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091210074';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3724905000'        
 and rtrim(LOANDTLACHBNKACCT) ='3210251740'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3424885005'        
 and rtrim(LOANDTLACHBNKACCT) ='321130584'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2236926009'        
 and rtrim(LOANDTLACHBNKACCT) ='335077295'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9046214006'        
 and rtrim(LOANDTLACHBNKACCT) ='3509004884'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4457185008'        
 and rtrim(LOANDTLACHBNKACCT) ='3600009553'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111322994';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3912475010'        
 and rtrim(LOANDTLACHBNKACCT) ='3609201585'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5436225002'        
 and rtrim(LOANDTLACHBNKACCT) ='362074363'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3820225008'        
 and rtrim(LOANDTLACHBNKACCT) ='362097133'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8577485007'        
 and rtrim(LOANDTLACHBNKACCT) ='362247230'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7149255007'        
 and rtrim(LOANDTLACHBNKACCT) ='3628236894'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113024915';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3581396000'        
 and rtrim(LOANDTLACHBNKACCT) ='366020127'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6051085001'        
 and rtrim(LOANDTLACHBNKACCT) ='367013222'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8129765004'        
 and rtrim(LOANDTLACHBNKACCT) ='367067968'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4605584006'        
 and rtrim(LOANDTLACHBNKACCT) ='375005089557'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='072000805';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3965005002'        
 and rtrim(LOANDTLACHBNKACCT) ='3809000052'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3415726008'        
 and rtrim(LOANDTLACHBNKACCT) ='381954'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091209933';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4516575008'        
 and rtrim(LOANDTLACHBNKACCT) ='381954'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091209933';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5004425010'        
 and rtrim(LOANDTLACHBNKACCT) ='381954'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091209933';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5648555009'        
 and rtrim(LOANDTLACHBNKACCT) ='381954'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='091209933';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3346705001'        
 and rtrim(LOANDTLACHBNKACCT) ='395029966'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3218086007'        
 and rtrim(LOANDTLACHBNKACCT) ='3956231488'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121042882';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8170305008'        
 and rtrim(LOANDTLACHBNKACCT) ='3988215806'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111900659';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4831305008'        
 and rtrim(LOANDTLACHBNKACCT) ='4001770678'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042102694';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9167275005'        
 and rtrim(LOANDTLACHBNKACCT) ='4021842748'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274382';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5939994005'        
 and rtrim(LOANDTLACHBNKACCT) ='4045149137'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='103003632';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8850185002'        
 and rtrim(LOANDTLACHBNKACCT) ='4087823'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100016';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5363185001'        
 and rtrim(LOANDTLACHBNKACCT) ='409000310124'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2770096009'        
 and rtrim(LOANDTLACHBNKACCT) ='41998519'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='061207839';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3716195001'        
 and rtrim(LOANDTLACHBNKACCT) ='41998519'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='061207839';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3136866001'        
 and rtrim(LOANDTLACHBNKACCT) ='4234057251'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='041000124';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8333445010'        
 and rtrim(LOANDTLACHBNKACCT) ='4360527598'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='031201360';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1521617000'        
 and rtrim(LOANDTLACHBNKACCT) ='4402000428'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061202371';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2395096006'        
 and rtrim(LOANDTLACHBNKACCT) ='47134349'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='111909993';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6412965001'        
 and rtrim(LOANDTLACHBNKACCT) ='488038438839'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000025';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3269546003'        
 and rtrim(LOANDTLACHBNKACCT) ='4971682006'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000089';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4934964003'        
 and rtrim(LOANDTLACHBNKACCT) ='500017743'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='114903284';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5745715000'        
 and rtrim(LOANDTLACHBNKACCT) ='500020264'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='114903284';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6168395004'        
 and rtrim(LOANDTLACHBNKACCT) ='500022038'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='114903284';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7457645008'        
 and rtrim(LOANDTLACHBNKACCT) ='500164009'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='063116177';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5270595007'        
 and rtrim(LOANDTLACHBNKACCT) ='5006023642'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='043318500';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3182846007'        
 and rtrim(LOANDTLACHBNKACCT) ='5021104447'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='055002406';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6553405001'        
 and rtrim(LOANDTLACHBNKACCT) ='50230103'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='091300159';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4248595006'        
 and rtrim(LOANDTLACHBNKACCT) ='509975217'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='114000093';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6217045001'        
 and rtrim(LOANDTLACHBNKACCT) ='5109000435'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9657285000'        
 and rtrim(LOANDTLACHBNKACCT) ='524017613'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='121142287';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6318415004'        
 and rtrim(LOANDTLACHBNKACCT) ='530032409'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='114000093';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3131556003'        
 and rtrim(LOANDTLACHBNKACCT) ='5311028996'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='042200910';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2709166007'        
 and rtrim(LOANDTLACHBNKACCT) ='5325801263'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='054000030';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5584275008'        
 and rtrim(LOANDTLACHBNKACCT) ='542008006518'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8646265005'        
 and rtrim(LOANDTLACHBNKACCT) ='550087105'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091916941';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5222105006'        
 and rtrim(LOANDTLACHBNKACCT) ='5501122591'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='113024164';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6688695004'        
 and rtrim(LOANDTLACHBNKACCT) ='560019892306'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='067011760';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8829384002'        
 and rtrim(LOANDTLACHBNKACCT) ='5693446'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='031302971';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8210335001'        
 and rtrim(LOANDTLACHBNKACCT) ='5777770321'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2767106009'        
 and rtrim(LOANDTLACHBNKACCT) ='586022738971'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000025';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6204885007'        
 and rtrim(LOANDTLACHBNKACCT) ='5920006782'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='081006162';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7119264002'        
 and rtrim(LOANDTLACHBNKACCT) ='592083716'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='267084131';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8958554001'        
 and rtrim(LOANDTLACHBNKACCT) ='6009006124'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6704665007'        
 and rtrim(LOANDTLACHBNKACCT) ='6110031090'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274573';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5343185004'        
 and rtrim(LOANDTLACHBNKACCT) ='6142269'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='075905910';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7005435003'        
 and rtrim(LOANDTLACHBNKACCT) ='6209203242'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2730416004'        
 and rtrim(LOANDTLACHBNKACCT) ='6213742089'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='036076150';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3108236003'        
 and rtrim(LOANDTLACHBNKACCT) ='646109173'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='122100024';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7414195004'        
 and rtrim(LOANDTLACHBNKACCT) ='6475906'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='091408446';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4647705010'        
 and rtrim(LOANDTLACHBNKACCT) ='64922442'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='101100029';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3181966006'        
 and rtrim(LOANDTLACHBNKACCT) ='65245375'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='322484113';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9168814007'        
 and rtrim(LOANDTLACHBNKACCT) ='668450390'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='221172212';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7937995007'        
 and rtrim(LOANDTLACHBNKACCT) ='669082559'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7244225007'        
 and rtrim(LOANDTLACHBNKACCT) ='671818107'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6456665007'        
 and rtrim(LOANDTLACHBNKACCT) ='700006643'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='091400554';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6544355008'        
 and rtrim(LOANDTLACHBNKACCT) ='70142396'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='021311529';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8466405002'        
 and rtrim(LOANDTLACHBNKACCT) ='7026670716'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2659136010'        
 and rtrim(LOANDTLACHBNKACCT) ='709828557'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021000021';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3195756003'        
 and rtrim(LOANDTLACHBNKACCT) ='7241200648'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221672851';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4456955001'        
 and rtrim(LOANDTLACHBNKACCT) ='770738'                                                             
  and rtrim(LOANDTLACHBNKROUTNMB) ='011302742';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9523185010'        
 and rtrim(LOANDTLACHBNKACCT) ='77101291'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='102201710';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7867365010'        
 and rtrim(LOANDTLACHBNKACCT) ='7751014742'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='063107513';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7016955001'        
 and rtrim(LOANDTLACHBNKACCT) ='779709778'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6237705006'        
 and rtrim(LOANDTLACHBNKACCT) ='7815704745'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3541886000'        
 and rtrim(LOANDTLACHBNKACCT) ='7835343067'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5597215000'        
 and rtrim(LOANDTLACHBNKACCT) ='7852494343'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5278215003'        
 and rtrim(LOANDTLACHBNKACCT) ='7856783118'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4792835010'        
 and rtrim(LOANDTLACHBNKACCT) ='7879197283'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5389855010'        
 and rtrim(LOANDTLACHBNKACCT) ='7884827414'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6548625003'        
 and rtrim(LOANDTLACHBNKACCT) ='7888811524'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9244615005'        
 and rtrim(LOANDTLACHBNKACCT) ='7898134325'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2511156009'        
 and rtrim(LOANDTLACHBNKACCT) ='7902540225'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6822035007'        
 and rtrim(LOANDTLACHBNKACCT) ='7909592894'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8702074006'        
 and rtrim(LOANDTLACHBNKACCT) ='7935099577'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='111900659';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6840245004'        
 and rtrim(LOANDTLACHBNKACCT) ='7946808295'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4555165006'        
 and rtrim(LOANDTLACHBNKACCT) ='800077551301'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='061107816';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9659964000'        
 and rtrim(LOANDTLACHBNKACCT) ='8092002578'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4237965004'        
 and rtrim(LOANDTLACHBNKACCT) ='818457541'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='124001545';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9354134008'        
 and rtrim(LOANDTLACHBNKACCT) ='836750286'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='072000326';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6585635005'        
 and rtrim(LOANDTLACHBNKACCT) ='9005159504'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211174181';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5540924004'        
 and rtrim(LOANDTLACHBNKACCT) ='9145841017'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='266086554';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4061835006'        
 and rtrim(LOANDTLACHBNKACCT) ='91944'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='221581641';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7404425008'        
 and rtrim(LOANDTLACHBNKACCT) ='9200168634'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171353';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7087575002'        
 and rtrim(LOANDTLACHBNKACCT) ='9200170291'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='301171353';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7004855009'        
 and rtrim(LOANDTLACHBNKACCT) ='9853258775'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='267090594';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7030875008'        
 and rtrim(LOANDTLACHBNKACCT) ='9866385090'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='022000046';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8032725007'        
 and rtrim(LOANDTLACHBNKACCT) ='989006838'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='111000614';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2657656009'        
 and rtrim(LOANDTLACHBNKACCT) ='575001259'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='101101293';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2162596002'        
 and rtrim(LOANDTLACHBNKACCT) ='16248'                                                              
  and rtrim(LOANDTLACHBNKROUTNMB) ='104902392';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7816015010'        
 and rtrim(LOANDTLACHBNKACCT) ='1056638'                                                            
  and rtrim(LOANDTLACHBNKROUTNMB) ='103902717';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8891404007'        
 and rtrim(LOANDTLACHBNKACCT) ='011499923'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5979454001'        
 and rtrim(LOANDTLACHBNKACCT) ='024144215'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6521235004'        
 and rtrim(LOANDTLACHBNKACCT) ='054385407'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9132375000'        
 and rtrim(LOANDTLACHBNKACCT) ='067119360'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2813186002'        
 and rtrim(LOANDTLACHBNKACCT) ='79014923'                                                           
  and rtrim(LOANDTLACHBNKROUTNMB) ='124102509';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5678245000'        
 and rtrim(LOANDTLACHBNKACCT) ='082419809'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7230075009'        
 and rtrim(LOANDTLACHBNKACCT) ='082419809'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3821275003'        
 and rtrim(LOANDTLACHBNKACCT) ='0109219396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3821285006'        
 and rtrim(LOANDTLACHBNKACCT) ='0109219396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6113894008'        
 and rtrim(LOANDTLACHBNKACCT) ='0109219396'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='4240175004'        
 and rtrim(LOANDTLACHBNKACCT) ='110006172'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='051404464';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3444465005'        
 and rtrim(LOANDTLACHBNKACCT) ='210849345'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='031100102';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8304335002'        
 and rtrim(LOANDTLACHBNKACCT) ='213285559'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3856694004'        
 and rtrim(LOANDTLACHBNKACCT) ='318350976'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6227664007'        
 and rtrim(LOANDTLACHBNKACCT) ='351235228'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='8212985001'        
 and rtrim(LOANDTLACHBNKACCT) ='351235228'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='7180765005'        
 and rtrim(LOANDTLACHBNKACCT) ='362246954'                                                          
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502011';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2433356003'        
 and rtrim(LOANDTLACHBNKACCT) ='1030021750'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='121201814';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3098226006'        
 and rtrim(LOANDTLACHBNKACCT) ='2300298602'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='104913912';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='9446065004'        
 and rtrim(LOANDTLACHBNKACCT) ='6150000112'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='211274573';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='5744875000'        
 and rtrim(LOANDTLACHBNKACCT) ='6300058567'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571473';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='3379066002'        
 and rtrim(LOANDTLACHBNKACCT) ='7938601907'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='221571415';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='2898166005'        
 and rtrim(LOANDTLACHBNKACCT) ='8175728586'                                                         
  and rtrim(LOANDTLACHBNKROUTNMB) ='061000227';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='6445855010'        
 and rtrim(LOANDTLACHBNKACCT) ='221000461213'                                                       
  and rtrim(LOANDTLACHBNKROUTNMB) ='021502804';                           
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID  where LOANNMB='1015146002'        
 and rtrim(LOANDTLACHBNKACCT) ='1100000635454'                                                      
  and rtrim(LOANDTLACHBNKROUTNMB) ='263191387';                           
                                                                                                    
select '*** testing to see if ACH set to something unpexpected***' from dual;
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2128946007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0000007690'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113025723'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2681936010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0000031625'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211871691'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6355315008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0000036420'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'301171007'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8640654007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0000178013'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'107007139'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7354644009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0000281203146'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061000104'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3404086000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0005201339873'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'053101121'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6916495006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0005793581314'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113011258'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7932495000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'00177949'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'303986096'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7008985010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0022434054'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'125108272'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3413085006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0023335306'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'066004367'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3570505005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'003000788'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'241270851'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2600196001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'00350001113439'                                                  
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'041001039'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5110425003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'003670292229'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063000047'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8062885010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0073084305'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'042000314'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2590236006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0077636996'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063104668'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2901616000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0084700'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114911807'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6890175005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'008982'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101111704'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4444725005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'01 9204997'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2968456005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'010183516505'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'066011392'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7605035005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'010212979505'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'066011392'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7818255005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'011269359'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7570124000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'011356901'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3745825010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'011652659'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5147565003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0121616523'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063104668'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6195665008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0131839'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061101197'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8059995007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'015281132'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5839975009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0185595'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'065200515'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6175075000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0186254'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'065200515'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7278755000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0194654'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'011701314'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9332734010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'027478769'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7258854002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'029118627'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'124000054'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8542534002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'029711004903'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8168945002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'030268117'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8804255009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'030268117'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9128554007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'030268117'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6402445005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0309204863'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7808805000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'031061141'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5074374007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'035368713'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6605235001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0361056065'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'304971932'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2576306007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0362182180'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'102300129'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6547855000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'041466462'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2896356010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'048 224839'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5202975001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0500001193'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063116177'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7191625004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0500001193'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063116177'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6670335003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0500139472'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063116177'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8057235009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0501664163'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'082902757'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6163785004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'050252089'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2776976003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'05096118'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'062202574'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5993675000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'052025829'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4121395008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0551102809'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'103003632'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2511096004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'05512307'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'123206338'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3192276006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0578340'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061112364'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7435415008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0608009083'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'253271822'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4193855003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'061377821'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7125735004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'061488372'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7125545006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'061488372'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7797175005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'061531685'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8793154000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'063005433'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021206249'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3161286006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'068127561'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7061005008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0781106305'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091302966'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4655995005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'08029237'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121301772'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8007425009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0816222078'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'271183701'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2306876003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0850053846'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'071925567'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9128994010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'09986241'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114902560'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4599495002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1000254555'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111916326'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5116475008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'100086355'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091400554'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5479955009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'100086652'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091400554'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3355935002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1002019595'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211885250'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6049135004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1002344016'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113025723'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3338856007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'10031729'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'011601087'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7599065006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'100646744'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'075911742'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3222566001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1010001076484'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'062203984'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2670806000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1010071149'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'011301798'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9063005000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1010190105'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'067011812'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9394764003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1010748270'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'074903719'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5323685004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'10109696'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'041212983'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5499725007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1011529'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111916180'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6756845004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1030047653'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5528235002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1033197987'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'124003116'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9047535006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'105285112'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'086300012'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4724735002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1059777'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111922624'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5992625005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1060107140637'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'044102362'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6829045002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1070597131'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3617474000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'11000000449362'                                                  
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'321177586'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9092935007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'11000000514283'                                                  
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'321177586'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6733535002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1100035052'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121101985'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4149605001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1110027400'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114903284'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3701885004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'111042561'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121135045'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5906884000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'111268389'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8928225002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'111692822'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7689925008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'112249280'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4218545001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1131346647'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'043000096'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4529925005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'115490019'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5354915004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1190915211'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6889955006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1210306506'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5071475003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1223380'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111923607'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2588696005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1248218'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'065204443'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5225035004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'125076310'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'064209216'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3578806002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1251800538'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211170253'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5340305008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1289496'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'065204443'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5553435005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1300000297990'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'291479974'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4741615003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1310730381'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4902095004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1310756780'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4256425001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'133029492'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4121935004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'133103129'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5458805000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1340004427652'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'053101121'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4639635002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'134253965'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3737495007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1343491'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063115505'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8240965003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'138233667'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5291155003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'139560019'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'086518477'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='1932777007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1400000174151'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'241279616'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4381535009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'141233141'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6568465000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1431225'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'075917937'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5100035000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1440643992'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3035776010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'149896'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101100906'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3637735010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1509202079'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6335995005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'152316024792'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'081000210'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6350815006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'154006025'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6450205008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'156377896'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7943405001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'157384799'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7943475000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'157384799'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4778395002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'160029902'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2538636000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1615007052'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4998415008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1620011880'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'102103407'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2891466009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'163070507815'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091300023'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6362655003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'170000638312'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111301122'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4941044007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'170100154'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4968565006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1771128343'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063107513'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3690796001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1881268047'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000753'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2899666004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'19114959'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111310870'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5720015006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2000004065'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061120479'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6425045006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2000011615'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063116083'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6290125008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2000013223'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'067015313'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5165665002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2000030480669'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111900659'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7737874005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'20013331'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113111983'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3292866003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'20015982'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091901202'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9061225003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'20051093'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'011300595'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8470265000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'201756615'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101015282'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2794436009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'203296637'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7189894009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2090002237452'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063107513'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9228145006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'21099556'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113111983'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3104136000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2115002295'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7500454010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'237016578218'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'053000196'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6735365003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'239270490'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6978515005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2409202675'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9042025007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2428384272'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211274450'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7759034001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'245003398'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101101293'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6370255001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2489619'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'031309945'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6451005009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2500303535'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'074900356'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='1519056003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'250374633'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='1177897003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2506021'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091915890'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2524517005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2506021'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091915890'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9182865003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2506021'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091915890'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4947005003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2509201981'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2209346006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2510320201'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'081503704'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4772645004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2531226545'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113010547'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4911565005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'25500011910071'                                                  
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111901056'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5165925008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'256000883'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4573775005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'256000883'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4506725001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'266132'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101101413'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2602956008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2677031136'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'324170085'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9345105004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2830001497'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'301171007'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6601915007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2964141838'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111900659'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8182485002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'297000664939'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7395275008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'297001750656'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5671385004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3000029433'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113103276'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4277265000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3000389377'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'031315544'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4574465008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3000788'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'241270851'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4950715005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3003140996'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502341'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9209704006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'300350328'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211674775'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7036355006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3004522396'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502341'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5195475005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3004554549'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502341'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6662415003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3004841033'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502341'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6196995004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3004893181'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502341'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6104094002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3005120912'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'055002406'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3180236008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3009001083'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7963215000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3009203010'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8964774004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'301202197'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113103276'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7615985001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'305089668'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8302305000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'31003733'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'071922777'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7615105006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'31008110'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'311372744'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3540235006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3115103564'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'083000108'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9093545010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'312018174'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111901519'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6529545008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'316574'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091210074'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6145245002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'319476'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091210074'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3724905000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3210251740'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3424885005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'321130584'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2236926009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'335077295'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9046214006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3509004884'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4457185008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3600009553'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111322994'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3912475010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3609201585'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5436225002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'362074363'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3820225008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'362097133'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8577485007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'362247230'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7149255007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3628236894'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113024915'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3581396000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'366020127'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6051085001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'367013222'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8129765004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'367067968'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4605584006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'375005089557'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'072000805'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3965005002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3809000052'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3415726008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'381954'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091209933'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4516575008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'381954'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091209933'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5004425010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'381954'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091209933'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5648555009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'381954'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091209933'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3346705001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'395029966'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3218086007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3956231488'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121042882'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8170305008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'3988215806'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111900659'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4831305008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4001770678'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'042102694'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9167275005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4021842748'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211274382'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5939994005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4045149137'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'103003632'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8850185002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4087823'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101100016'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5363185001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'409000310124'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2770096009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'41998519'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061207839'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3716195001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'41998519'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061207839'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3136866001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4234057251'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'041000124'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8333445010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4360527598'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'031201360'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='1521617000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4402000428'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061202371'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2395096006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'47134349'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111909993'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6412965001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'488038438839'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000025'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3269546003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'4971682006'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021000089'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4934964003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'500017743'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114903284'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5745715000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'500020264'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114903284'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6168395004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'500022038'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114903284'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7457645008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'500164009'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063116177'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5270595007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5006023642'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'043318500'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3182846007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5021104447'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'055002406'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6553405001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'50230103'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091300159'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4248595006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'509975217'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114000093'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6217045001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5109000435'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9657285000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'524017613'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121142287'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6318415004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'530032409'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'114000093'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3131556003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5311028996'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'042200910'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2709166007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5325801263'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'054000030'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5584275008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'542008006518'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8646265005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'550087105'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091916941'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5222105006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5501122591'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'113024164'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6688695004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'560019892306'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'067011760'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8829384002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5693446'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'031302971'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8210335001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5777770321'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063107513'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2767106009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'586022738971'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000025'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6204885007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'5920006782'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'081006162'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7119264002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'592083716'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'267084131'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8958554001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6009006124'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6704665007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6110031090'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211274573'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5343185004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6142269'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'075905910'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7005435003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6209203242'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2730416004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6213742089'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'036076150'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3108236003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'646109173'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'122100024'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7414195004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6475906'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091408446'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4647705010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'64922442'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101100029'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3181966006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'65245375'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'322484113'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9168814007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'668450390'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221172212'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7937995007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'669082559'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000614'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7244225007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'671818107'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000614'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6456665007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'700006643'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'091400554'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6544355008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'70142396'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021311529'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8466405002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7026670716'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063107513'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2659136010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'709828557'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021000021'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3195756003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7241200648'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221672851'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4456955001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'770738'                                                          
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'011302742'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9523185010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'77101291'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'102201710'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7867365010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7751014742'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'063107513'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7016955001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'779709778'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000614'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6237705006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7815704745'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3541886000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7835343067'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5597215000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7852494343'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5278215003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7856783118'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4792835010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7879197283'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5389855010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7884827414'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6548625003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7888811524'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9244615005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7898134325'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2511156009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7902540225'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6822035007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7909592894'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8702074006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7935099577'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111900659'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6840245004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7946808295'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4555165006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'800077551301'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061107816'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9659964000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'8092002578'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4237965004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'818457541'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'124001545'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9354134008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'836750286'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'072000326'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6585635005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9005159504'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211174181'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5540924004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9145841017'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'266086554'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4061835006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'91944'                                                           
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221581641'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7404425008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9200168634'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'301171353'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7087575002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9200170291'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'301171353'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7004855009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9853258775'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'267090594'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7030875008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'9866385090'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'022000046'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8032725007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'989006838'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'111000614'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2657656009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'575001259'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'101101293'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2162596002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'16248'                                                           
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'104902392'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7816015010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1056638'                                                         
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'103902717'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8891404007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'011499923'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5979454001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'024144215'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6521235004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'054385407'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9132375000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'067119360'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2813186002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'79014923'                                                        
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'124102509'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5678245000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'082419809'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7230075009'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'082419809'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3821275003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0109219396'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3821285006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0109219396'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6113894008'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'0109219396'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='4240175004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'110006172'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'051404464'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3444465005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'210849345'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'031100102'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8304335002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'213285559'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3856694004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'318350976'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6227664007'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'351235228'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='8212985001'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'351235228'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='7180765005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'362246954'                                                       
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502011'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2433356003'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1030021750'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'121201814'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3098226006'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'2300298602'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'104913912'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='9446065004'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6150000112'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'211274573'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='5744875000'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'6300058567'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571473'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='3379066002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'7938601907'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'221571415'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='2898166005'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'8175728586'                                                      
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'061000227'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='6445855010'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'221000461213'                                                    
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'021502804'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    
update STGCSA.SOFVLND1TBL  set LASTUPDTUSERID=LASTUPDTUSERID 
where LOANNMB='1015146002'                    
 and ((rtrim(LOANDTLACHBNKACCT) <>'1100000635454'                                                   
 and ltrim(LOANDTLACHBNKACCT) IS NOT NULL )               
  or (rtrim(LOANDTLACHBNKROUTNMB) <>'263191387'                           
 and ltrim(LOANDTLACHBNKROUTNMB) IS NOT NULL));                               
                                                                                                    

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

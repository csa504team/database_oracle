create or replace PROCEDURE                  CDCONLINE.LOANSETTOFUNDDTSELCSP
(
p_Identifier number := null,
p_RPTDT date := SYSDATE,
p_SelCur out sys_refcursor
)
as
begin
	-- returns funding dashboard report dates based upon p_rptdt
    if p_Identifier = 0 then
    begin
        open p_SelCur for
			SELECT
				LPAD(EXTRACT(MONTH FROM l.ISSDT), 2, '0') AS FUNDINGDTMO
				, EXTRACT(YEAR FROM l.ISSDT) AS FUNDINGDTYR
			FROM
				cdconline.LoanTbl l 
			WHERE
				TRUNC(l.ISSDT) >= TRUNC(p_RPTDT) 
			GROUP BY 
				EXTRACT(MONTH FROM l.ISSDT), EXTRACT(YEAR FROM l.ISSDT)
			ORDER BY 
				FUNDINGDTYR || LPAD(FUNDINGDTMO, 2, '0') ASC;
    end;
    end if;
end;



GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDDTSELCSP TO CDCONLINECORPGOVPRTUPDATE;

GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDDTSELCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDDTSELCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDDTSELCSP TO CSAUPDTROLE;
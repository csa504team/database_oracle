create or replace PROCEDURE                  CDCONLINE.LOANSETTOFUNDSELCSP
(
p_Identifier number := null,
p_CDCREGNCD char := null,
p_CDCNMB char := null,
p_FUNDINGMO char := null,
p_FUNDINGYR char := null,
--p_RetVal out number,
p_SelCur out sys_refcursor
)
as
begin
	-- returns funding dashboard report information based upon a combination of cdc region, cdc, and the funding date (month-year)
    if p_Identifier = 0 then
		begin
			open p_SelCur for
			select
				l.CDCREGNCD
				, l.CDCNMB
				, l.LOANNMB
				, l.ISSDT
				, l.BORRNM
				, l.SMLLBUSCONS
				, l.ISSETTOFUNDCD
				, CASE l.ISSETTOFUNDCD WHEN 'N' THEN 'N' ELSE 'Y' END ISSETTOFUNDIND
			from
				LoanTbl l
			where
				TRIM(l.CDCNMB) = TRIM(p_CDCNMB)
			AND
				TRIM(l.CDCREGNCD) = TRIM(p_CDCREGNCD)
			AND
				EXTRACT(MONTH FROM l.ISSDT) = p_FUNDINGMO
			AND
				EXTRACT(YEAR FROM l.ISSDT) = p_FUNDINGYR
			ORDER BY
				l.CDCREGNCD ASC, l.CDCNMB ASC, l.LOANNMB ASC;
		end;
	-- returns funding dashboard report information for all cdcs; this to be used by csa analysts only
	elsif p_Identifier = 1 then
		begin
			open p_SelCur for
			select
				l.CDCREGNCD
				, l.CDCNMB
				, l.LOANNMB
				, l.ISSDT
				, l.BORRNM
				, l.SMLLBUSCONS
				, l.ISSETTOFUNDCD
				, CASE l.ISSETTOFUNDCD WHEN 'N' THEN 'N' ELSE 'Y' END ISSETTOFUNDIND
			from
				LoanTbl l
			where
				EXTRACT(MONTH FROM l.ISSDT) = p_FUNDINGMO
			AND
				EXTRACT(YEAR FROM l.ISSDT) = p_FUNDINGYR
			ORDER BY
				l.CDCREGNCD ASC, l.CDCNMB ASC, l.LOANNMB ASC;
		end;
	end if;
end;



--GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDSELCSP TO CDCONLINECORPGOVPRTUPDATE;

GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDSELCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDSELCSP TO CDCONLINEREADALLROLE;

--GRANT EXECUTE ON CDCONLINE.LOANSETTOFUNDSELCSP TO CSAUPDTROLE;
define deploy_name=Portfolio_LastPymt_Info
define package_name=UAT_0308
define package_buildtime=20210308154248
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Portfolio_LastPymt_Info_UAT_0308 created on Mon 03/08/2021 15:42:48.81 by Jasleen Gorowada
prompt deploy scripts for deploy Portfolio_LastPymt_Info_UAT_0308 created on Mon 03/08/2021 15:42:48.81 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Portfolio_LastPymt_Info_UAT_0308: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CaresActPortflLastPmtInfoTsp.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\CaresActPortflLastPmtInfoTsp.sql"
CREATE OR REPLACE PROCEDURE CDCONLINE.CaresActPortflLastPmtInfoTsp
(
  p_CDCREGNCD IN CHAR,
  p_CDCNMB IN CHAR,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		SELECT H.LoanNmb ,
               C.APPVDT,
               H.PymtTyp ,
               H.PostingDt ,
			   CASE
               WHEN H.PymtTyp = 'X' THEN NULL
               ELSE H.SBAFeeAmt + H.CSAFeeAmt + H.CDCFeeAmt + H.IntAmt + H.PrinAmt + H.UnallocAmt + H.LateFeeAmt + H.DueFromBorrAmt
                END total_pymt  ,
				L.DUEFROMBORRNEEDEDAMT,
				H.SBAFeeAmt ,
                H.CSAFeeAmt ,
                H.CDCFeeAmt ,
			    H.IntAmt ,
                H.PrinAmt ,
                H.LateFeeAmt ,
                H.UnallocAmt ,
                H.PrepayAmt ,
                H.BalAmt ,
				H.CreatUserId,
			    L.CURBALNEEDEDAMT 
			        FROM CDCONLINE.PymtHistryTbl H
					LEFT JOIN CDCONLINE.CnfrmCopyTbl C on C.Loannmb = H.Loannmb
					LEFT JOIN CDCONLINE.LoanTbl L on C.Loannmb = L.Loannmb
       WHERE C.CDCREGNCD = p_CDCREGNCD and
			  C.CDCNMB = p_CDCNMB
			  and H.PostingDt = (select max(PostingDt) from CDCONLINE.PymtHistryTbl where  Loannmb = H.Loannmb)
        ORDER BY H.Loannmb DESC ;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON CDCONLINE.CaresActPortflLastPmtInfoTsp TO CDCONLINEREADALLROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

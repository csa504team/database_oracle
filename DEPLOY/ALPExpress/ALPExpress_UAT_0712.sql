define deploy_name=ALPExpress
define package_name=UAT_0712
define package_buildtime=20210712125653
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy ALPExpress_UAT_0712 created on Mon 07/12/2021 12:56:54.41 by Jasleen Gorowada
prompt deploy scripts for deploy ALPExpress_UAT_0712 created on Mon 07/12/2021 12:56:54.41 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy ALPExpress_UAT_0712: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL_0712.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATEAPPCSP_MZ.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL_0712.sql"
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '1', 'For Application, The SBA Guaranty percentage ^3 multiplied by Requested Amount ^2 exceeds maximum ^1', user, sysdate, 
    user, sysdate, 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '2', 'For Application, The SBA Guaranty percentage ^3 multiplied by Requested Amount ^2 exceeds maximum ^1', user, sysdate, 
    user, sysdate, '');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4294', '1', 'For Application, ALP Express Loans may not be made for NAICS Code ^1', user, sysdate, 
    user, sysdate, 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4294', '2', 'For Application, ALP Express Loans may not be made for NAICS Code ^1', user, sysdate, 
    user, sysdate, '');
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATEAPPCSP_MZ.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATEAPPCSP_MZ (p_RetVal            OUT NUMBER,
                                                    p_LoanAppNmb            NUMBER,
                                                    p_ErrSeqNmb      IN OUT NUMBER,
                                                    p_TransInd              NUMBER,
                                                    p_LoanAppRecvDt         DATE
                                                   )
/*
 ********************************************************************************
 Created by:
 Created on:
 Purpose:
 Parameters:
 Modified by: APK - 08/02/2010 Issue:Is Payment Fully Amortizing default - Item 130
 Modified on: 08/02/2010
 Modified on: 08/16/2010 - APK - new validation in loanapp.ValidateApp stored procedure
 to make LoanAppDisasterCntrlNmb mandatory for PrcsMthdCd EQ 'DGB'
  APK --09/21/2010-- Modified for the error code 129, included the PrcsMthdCd and Exception logic.
  APK -- 09/27/2010-- modified parameter MatMoConsMax (checking) code to be identical as in Loan.
  09/28/2010-- APK-- included the FININSTRMNTTYPIND change for PrgrmValidTbl and parameter tables
  Modified By: 09/29/2010-- APK--Error 106 - This needs to be called for all loan
              programs.Remove the if logic for Program Cd = A both when checking the two
              parameters (LoanRqstAmtDltaMax and  LoanRqstAmtMax) and also setting the
              error code.
 Modified By: 09/29/2010 -- APK-- Error 118 - This needs to be called for all loan
              programs. There is already code not to call if the special purpose is not
              REVL or CONS, that is sufficient. Remove the if logic for Program Cd = A
              both when checking the two parameters(MatMoRevlMax and MatMoConsMax) and
              also setting the error code.
 Modified By: Error 109 - This needs to be called for all loan programs. Remove the if
              logic for Program Cd = A both when checking the two parameters
              (GntyPct7LrgAmtMax and  GntyPctLowAmtMax) and also setting the error code.
 Modified By: 09/29/2010 -- APK-- Error 110 - This needs to be called for all loan
              programs. Remove the if logic for Program Cd = A both when checking the
              parameter (GntyAmtMax) and also setting the error code.Rewrite the code
              for 110, take out all the references to LDP and PLP and remove the check
              against the parameter GntyAmt7NExpwExptConsRevlMax.
 Modified By: 10/05/2010 -- APK-- Modified the procedure to use the common function
              GET_IMPRMTRVAL to reduce code redundancy and also for proper formatting
Modified By: 11/23/2010 -- APK --Modified to get the logic for error code 101, to use adjustment factor into consideration, from validateprojcsp and added the same code to here. Also modified 106 to combine with 101 using adjustment factors.And 101 and 106 now apply for all programs.
Modified by: 02/03/2011-- Modified code for the (Eligibilty Indicator), as the Indicator locumn is removed from the LoanAppTbl.
 02/03/2011-- Modified code for the errors 234 and 247 to validate for all processing methods. previously they were validated for Only ARC's.
Modified: APK - 02/04/2011- modified to revert back the code for error codes 156 and 240, for Eligibility.
Modified: APK - 02/08/2011- modified to revert back the code for error codes 234 and 247, for Arc Reasons.
Modified: RGG - 02/14/2011- Commented the code for error codes 156 and 240, for Eligibility.
Modified: APK - 03/01/2011- modified condition for error 118 to be called for all processing methods not only for SMP.
  APK -- 01/03/2012-- modified code for error code 127,244,151 so to skip for PrgrmCd = H
 APK -- 01/31/2012 -- modified to add additional code block for handling 'SBX' group aggregate amount.
 APK -- 02/03/2012 -- modified to add two validations, 1027 and 1028, that apply only to Program Code = 'H'.Disaster Control Number is Required and Disaster Application Number is Required.
APK -- 02/06/2012 -- modified Validation 1028, to use LOANDISASTRAPPNMB, as the column LOANDISASTRAPPNMB was added to the loanapptbl in development which was present in test and prod but not in dev.
APK -- 02/06/2012 -- modified to add new validation 1029 to 1031, as part of Line of credit question.
�    If loan type doesn�t allow the REVL special purpose, LoanAppRevlInd must be null.
�    If loan type does allow the REVL special purpose:
o    If the loan application has the REVL special purpose, LoanAppRevlInd must be �Y�.
o    If the loan application doesn�t have the REVL special purpose, LoanAppRevlInd must be �N�.
APK -- 02/08/2012 -- modified to validations 1029 to 1031 to make some minor adjustments, to use LoanAppRevlInd in the conditions which was missing.
APK -- 05/25/2012 -- Modified Aggregate chages as per Ron's new requirement.
APK -- 07/10/2012 -- Modified to add new validation 1044 for LoanApp Payment amount not be greaterthan or equal to 1 million.
RGG -- 10/31/2012 -- Added new validation to check initial interest rate to be geater than or equal zero.
SP  -- 12/10/2012 -- modified code for error 174 as underwritingby depends not only on process method but also on agreement.
SP  -- 04/04/2013 -- Modified so as to get underwritinby from loanapptbl
RSURAPA -- 06/03/2013 -- Added a new validation to check if a current primary borrower or any of the new loan participants (co-borr/prin/guar)
                         are affiliates (all participant types guar/borr and prin) in an SLA (existing parameter = PrcsMthdReqLqdCr) loan that was created within 90 days (new parameter = LqdCrPrivLoanDays )
                         that is in Status (PS)� and the new loan is for a processing method that is in the parameter (new parameter = LqdCrPrivPrcsMthdBarred ).
SP  -- 11/21/2013 -- Modified to get interest values from LoanIntDtlTbl instead of LoanAppTbl
SP  -- 12/23/2013 -- Added validations 1.to check LoanAppIntDtlCd against code table and mandatory
SP  -- 01/14/2014 -- Moved validation checks for interest details to ValidateIntDtlCsp for errors:100,114,119,124,129,146,155,159,171,284,1049,238,243,120
SP  -- 02/03/2014 -- Modified  1053 to throw error if reconsideration indicator is not set to Y
SP  -- 03/05/2014 -- Modified  error 136 to remove check for :reconsideration indicator is null then the loan type cannot be `D�
SP  -- 04/09/2014 -- Modified to move validations 166,244,151,127 to VALIDATEAPPPROJCSP
PSP -- 08/04/2014 -- Modified v_FININSTRMNTTYPIND := E for prgrmcd = E
SB -- 09/22/2014 -- Added FININSTRMNTTYPIND to ADD_ERROR function call.
SB -- 10/06/2014 -- Added validation to Recondsideration Indicator and Underwriting By
PSP -- 10/15/2014 -- Modified to add prgmcd =R to prgmcd =A validations as ARC loans are actually 7a loans with a program code of "R".
RGG -- made a change to error 1029 to allow N for RevolvingInd
NK  --08/05/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd from
RY-- 12/12/16 OPSMDEV--1294 -- Added a new validation for Err Cd 3330 and added v_LOANGNTYMATSTIND,v_LOANGNTYNOTEDT
NK-- 02/23/2017 -- Added Additional Conditions validation for 504 loans(errcd 3330)
NK-- 03/16/2017 -- OPSMDEV 1402 Added Error Code 4039 and 4040
NK--03/28/2017 -- OPSMDEV 1385  Updated errcd 101
 RY--04/04/2017 --OPSMDEV 628 --Restricted errorcd 101 only for 504 loans
 SS--04/25/2017--OPSMDEV 1422--Added ERRCD 4048 All 504 loans maturity starts at first disbursement
 SS--04/27/2017--OPSMDEV 1422 --Added ERRCD 4049 All 504 loans require collateral
 SS--05/02/2017--OPSMDEV 1422-- Added Errcd 4050 For all 504 loans Balance to borow should not exceed $1000.
 NK--05/09/2017--CAFSOPER-- Updated Errcd 4039 and 4040
 SS--05/29/2017--OPSMDEV 1436 Added errcd 4071 for CDCServFeePct
 NK--08/21/2017--CAFSFOPER 805-- Updated error code 4050
 JP--09/13/2017--CAFSFOPER 805 added  v_LoanAppBalToBorrAmt as a parameter for ADD_ERROR function call 
                   when v_prgrmcd = 'E' AND v_LoanAppBalToBorrAmt > 1000
********************************************************************************
*/
AS
   v_MaxLoanExpo                 NUMBER;
   v_LoanSumAmt                  NUMBER;
   v_MaxLoanSumAmt               NUMBER;
   v_TaxId                       CHAR (10);
   v_MaxGntyPct                  NUMBER (6, 3);
   v_PrgrmCd                     CHAR (1);
   v_PrcsMthdCd                  CHAR (3);
   v_NAICSYrNmb                  NUMBER;
   v_NAICSCd                     NUMBER;                                                     --- sms 10/12/00
   v_LoanAppNm                   VARCHAR2 (80);                                                      ---PLPII
   v_LoanAppNewBusCd             CHAR (1);
   v_LoanAppRqstAmt              NUMBER;
   v_LoanAppSBAGntyPct           NUMBER (6, 3);
   v_LoanAppRqstMatMoQty         NUMBER;
   v_LoanAppFullAmortPymtInd     CHAR (1);
   v_LoanAppPymtAmt              NUMBER;
   v_LoanAppMoIntQty             NUMBER;
   --v_LoanAppInitlIntPct            number(6,3); --NN
   --v_LoanAppFixVarInd              char(1); --NN
   -- v_LoanAppInitlSprdOvrPrimePct   number(6,3) ; --NN
   --v_LoanAppAdjPrdCd               char(1); ---sms 10/20/00 --NN
   v_LoanAppLifInsurRqmtInd      CHAR (1);                                                    ---sms 10/20/00
   v_LoanAppInjctnInd            CHAR (1);
   v_LoanAppRcnsdrtnInd          CHAR (1);
   v_LoanAppEligEvalInd          CHAR (1);
   v_LoanAppEWCPSnglTransPostInd CHAR (1);
   v_LoanAppEWCPSnglTransInd     CHAR (1);
   v_SpcPurpsLoanCd              CHAR (4);
   v_DLTASpcPurpsInd             CHAR (1);
   --v_LoanAppBaseRt                 number(6,3);
   -- v_IMPrimeRtPct                  NUMBER (6, 3);
   v_LoanAppEPCInd               CHAR (1);
   v_LoanAppSBARcmndAmt          NUMBER;
   v_MaxLoanAppRqstAmt           NUMBER;
   v_GntyPctRqstAmtLrgLimit      NUMBER;
   v_GntyPct5Max                 NUMBER (6, 3);
   v_MatMoMax                    NUMBER;
   v_MatMoMin                    NUMBER;
   v_LoanCollatInd               CHAR (1);
   v_GntyPctMin                  NUMBER (6, 3);
   -- v_DirectLoanInd                 CHAR (1);
   v_PersonLoanInd               CHAR (1);
   --  v_BorrSeqNmb                    NUMBER;
   v_VetPctOwnrshpSum            NUMBER;
   v_PctOwnrshpVetSumMin         NUMBER;
   v_RecovInd                    CHAR (1);
   v_LoanTypInd                  CHAR (1);
   v_LoanAppMicroLendrId         CHAR (10);
   v_CrdScrThrshld               CHAR (3);
   v_CrdScrThrshldMax            CHAR (3);
   -- v_LqdCrTotScr                   CHAR (3);
   v_LoanAmtMin                  NUMBER;
   v_LoanAppCDCSeprtPrcsFeeInd   CHAR (1);
   v_LoanAppCDCNetDbentrAmt      NUMBER;
   v_LoanAppCDCGntyAmt           NUMBER;
   v_LoanAppCDCFundFeeAmt        NUMBER;
   v_LoanAppCDCPrcsFeeAmt        NUMBER;
   v_LoanAppCDCClsCostAmt        NUMBER;
   v_LoanAppCDCUndrwtrFeeAmt     NUMBER;
   --  v_LoanAppCDCJobRat              NUMBER (6, 3);
   v_LoanAppContribPct           NUMBER (5, 2);
   v_LoanAppContribAmt           NUMBER (15, 2);
   v_InjctnSum                   NUMBER (15, 2);
   v_ChkVal                      NUMBER;
   v_FY                          NUMBER (5, 0);
   v_LoanAppRecvDt               DATE;
   v_LoanAmt                     NUMBER;
   v_CohortCdCnt                 NUMBER;
   v_LOANAPPDISASTERCNTRLNMB     VARCHAR2 (20);
   v_LOANDISASTRAPPNMB           CHAR (30);
   --  v_SBAPrtcptnAmt                 NUMBER (19, 4);
   --  v_OrgntrPrtcptnAmt              NUMBER (19, 4);
   --  v_TtlOutstndngBal               NUMBER (19, 4);
   v_FININSTRMNTTYPIND           CHAR (1);
   v_IMPrmtrValChk               VARCHAR (255);
   v_IMPrmtrId                   VARCHAR2 (255);
   --v_EconDevObjctCd                CHAR (3);
   v_LoanAppJobRqmtMetInd        CHAR (1);
   --  v_LoanAppRqstAmtAdj             NUMBER (19, 4);
   --   v_LoanAppRqstAmtAdjDLTA         NUMBER (19, 4);
   v_ChkVal1                     NUMBER;
   --  v_COUNT                         NUMBER;
   v_AggregatePrcsGroup          CHAR (1);
   --  v_LOANPROCDAMT                  NUMBER (19, 4);
   --  v_LOAN7APYMTAMT                 NUMBER (19, 4);
   v_LoanAppRevlInd              CHAR (1);
   v_undrwritngby                CHAR (4);
   v_PrcsMthdReqLqdCr            VARCHAR2 (9);
   v_LqdCrPrivLoanDays           NUMBER;
   v_LqdCrPrivPrcsMthdBarred     VARCHAR2 (5);
   v_isSBAGntydLoan              VARCHAR2 (1);
   --   v_LOANINTLOANPARTPCT            NUMBER (6, 3);
   --   v_LOANINTLOANPARTMONMB          NUMBER (3, 0);
   --   v_LOANINTFIXVARIND              CHAR (1);
   --   v_LOANINTRT                     NUMBER (6, 3);
   v_IMRTTYPCD                   CHAR (3);
   --   v_LOANINTBASERT                 NUMBER (6, 3);
   --   v_LOANINTSPRDOVRPCT             NUMBER (6, 3);
   --  v_LOANINTADJPRDCD               CHAR (1);
   --  v_LOANINTADJPRDMONMB            NUMBER (3, 0);
   v_LOANAPPINTDTLCD             VARCHAR2 (2);
   v_LOANAMTLIMEXMPTIND          CHAR (1);
   v_LocId                       NUMBER;
   v_TotalPrevFyAppvAmt          NUMBER;
   v_TotalCurrFy5REAmt           NUMBER;
   v_LOANGNTYMATSTIND            CHAR (1);                                               ---added on 12/09/16
   v_LOANGNTYNOTEDT              DATE;                                                    --added on 12/12/16
   v_LOANPYMNINSTLMNTFREQCD      CHAR (1);
   v_BorrContAmt                 NUMBER (19, 4);
   v_SBAContAmt                  NUMBER (19, 4);
   v_IMPrmtrValTxt               VARCHAR (80);
   v_TotProjFinAmt               NUMBER;
   v_ThirdPartyLoanAmt           NUMBER;
   --   v_TotDebentAmt                  NUMBER (19, 4);
   --  v_LoanAppCDCSSubTotAmt          NUMBER (19, 4);
   --   v_CDCTotAdminCosts              NUMBER (19, 4);
   --   v_BalToBorrAmt                  NUMBER (19, 4);
   v_CDCGntyFeeMulti             NUMBER (7, 6);
   v_CDCFundFeeMulti             NUMBER (7, 6);
   v_CDCPrcsFeeMulti             NUMBER (7, 6);
   v_CDCServFeePctMax            NUMBER (6, 3);
   v_CDCServFeePctMin            NUMBER (6, 3);
   v_CDCServFeePct               NUMBER (6, 3);
   --   v_LoanAppCDCGrossDebentrAmt     NUMBER (19, 4);
   v_LoanAppBalToBorrAmt         NUMBER (19, 4);
BEGIN
   BEGIN
      SELECT PrgmCd,
             PrcsMthdCd                                                                      --- sms 10/12/00
                       ,
             LoanAppNm                                                                               ---PLPII
                      ,
             LoanAppNewBusCd,
             LoanAppRqstAmt,
             LoanAppSBAGntyPct,
             LoanAppRqstMatMoQty                                                  -- ,LoanAppInitlIntPct --NN
                                                                                   --  ,LoanAppFixVarInd --NN
             -- ,LoanAppInitlSprdOvrPrimePct --NN
             --  ,LoanAppAdjPrdCd ---sms 10/20/00 --NN
             ,
             LoanAppLifInsurRqmtInd                                                           ---sms 10/20/00
                                   ,
             LoanAppPymtAmt                                                                  --- sms 10/20/00
                           ,
             LoanAppMoIntQty                                                                 --- sms 10/20/00
                            ,
             LoanAppInjctnInd,
             LoanAppRcnsdrtnInd,
             LoanAppEligEvalInd,
             LoanAppEWCPSnglTransPostInd,
             LoanAppEWCPSnglTransInd                                   --  ,LoanAppBaseRt ---mz 05/22/01 --NN
                                    ,
             LoanAppFullAmortPymtInd                                                      --  ,IMRtTypCd --NN
                                    ,
             LoanAppEPCInd,
             LoanAppSBARcmndAmt,
             LoanCollatInd,
             RecovInd,
             NVL (
                  (SELECT LoanTypInd
                   FROM sbaref.PrcsMthdTbl z
                   WHERE z.PrcsMthdCd = a.PrcsMthdCd),
                  ' '
                 ),
             LoanAppMicroLendrId,
             LoanAppCDCGntyAmt,
             LoanAppCDCFundFeeAmt,
             LoanAppCDCPrcsFeeAmt,
             LoanAppCDCClsCostAmt,
             LoanAppCDCUndrwtrFeeAmt,
             LoanAppCDCNetDbentrAmt,
             LoanAppCDCSeprtPrcsFeeInd,
             LoanAppContribPct,
             LoanAppContribAmt,
             LOANAPPDISASTERCNTRLNMB,
             LOANDISASTRAPPNMB,
             LoanAppRevlInd,
             undrwritngby,
             LOANAPPINTDTLCD,
             LOANAMTLIMEXMPTIND,
             LOANGNTYMATSTIND,
             LOANGNTYNOTEDT,
             LOANPYMNINSTLMNTFREQCD,
             CDCServFeePct,
             LoanAppBalToBorrAmt
      INTO v_PrgrmCd,
           v_PrcsMthdCd,
           v_LoanAppNm                                                                               ---PLPII
                      ,
           v_LoanAppNewBusCd,
           v_LoanAppRqstAmt,
           v_LoanAppSBAGntyPct,
           v_LoanAppRqstMatMoQty                                               --  ,v_LoanAppInitlIntPct --NN
                                                                                 --  ,v_LoanAppFixVarInd --NN
           --  ,v_LoanAppInitlSprdOvrPrimePct --NN
                                                                                  --  ,v_LoanAppAdjPrdCd --NN
           ,
           v_LoanAppLifInsurRqmtInd,
           v_LoanAppPymtAmt,
           v_LoanAppMoIntQty,
           v_LoanAppInjctnInd,
           v_LoanAppRcnsdrtnInd,
           v_LoanAppEligEvalInd,
           v_LoanAppEWCPSnglTransPostInd,
           v_LoanAppEWCPSnglTransInd                                              --    ,v_LoanAppBaseRt --NN
                                    ,
           v_LoanAppFullAmortPymtInd                                                    --  ,v_IMRtTypCd --NN
                                    ,
           v_LoanAppEPCInd,
           v_LoanAppSBARcmndAmt,
           v_LoanCollatInd,
           v_RecovInd,
           v_LoanTypInd,
           v_LoanAppMicroLendrId,
           v_LoanAppCDCGntyAmt,
           v_LoanAppCDCFundFeeAmt,
           v_LoanAppCDCPrcsFeeAmt,
           v_LoanAppCDCClsCostAmt,
           v_LoanAppCDCUndrwtrFeeAmt,
           v_LoanAppCDCNetDbentrAmt,
           v_LoanAppCDCSeprtPrcsFeeInd,
           v_LoanAppContribPct,
           v_LoanAppContribAmt,
           v_LOANAPPDISASTERCNTRLNMB,
           v_LOANDISASTRAPPNMB,
           v_LoanAppRevlInd,
           v_undrwritngby,
           v_LOANAPPINTDTLCD,
           v_LOANAMTLIMEXMPTIND,
           v_LOANGNTYMATSTIND                                                             --added on 12/09/16
                             ,
           v_LOANGNTYNOTEDT,
           v_LOANPYMNINSTLMNTFREQCD,
           v_CDCServFeePct,
           v_LoanAppBalToBorrAmt
      FROM LoanAppTbl a
      WHERE LoanAppNmb = p_LoanAppNmb;



      BEGIN
         SELECT LOCID
         INTO v_LOCID
         FROM LoanAppPrtTbl
         WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;


      IF v_PrgrmCd = 'E'
      THEN
         v_FININSTRMNTTYPIND  := 'E';
      ELSE
         v_FININSTRMNTTYPIND  := 'L';
      END IF;

      --check if the loan is sba guarnteed or not based on the process method -- RSURAPA - 07/19/2013
      SELECT DECODE (loantypind, 'G', 'Y', 'N')
      INTO v_isSBAGntydLoan
      FROM sbaref.PrcsMthdtbl
      WHERE prcsmthdcd = v_prcsmthdcd;

      BEGIN
         SELECT LoanAppJobRqmtMetInd                                                    --    ,EconDevObjctCd
                                    , NAICSYrNmb, NAICSCd
         INTO v_LoanAppJobRqmtMetInd                                                      --,v_EconDevObjctCd
                                    , v_NAICSYrNmb, v_NAICSCd
         FROM LoanAppProjTbl
         WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_LoanAppJobRqmtMetInd  := NULL;
            --  v_EconDevObjctCd       := NULL;
            v_NAICSCd               := NULL;
      END;

      ---APK 05/01/2010 Modified for multiple primary borrowers so that query below returns --only onerow
      SELECT TaxId
      INTO v_TaxId
      FROM loanapp.LoanBorrTbl a
      WHERE     a.LoanAppNmb = p_LoanAppNmb
            AND a.LoanBusPrimBorrInd = 'Y'
            AND a.BORRSEQNMB = (SELECT MAX (z.BORRSEQNMB)
                                FROM loanapp.LoanBorrTbl z
                                WHERE z.LOANAPPNMB = a.LOANAPPNMB);

      SELECT COUNT (*)
      INTO v_ChkVal
      FROM IMPrmtrTbl a, IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'PrcsMthdDPHSet'
            AND IMPrmtrValTxt = v_PrcsMthdCd
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND (   IMPrmtrValEndDt IS NULL
                 OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

      v_PersonLoanInd      :=
         CASE
            WHEN v_ChkVal > 0 THEN 'Y'
            ELSE 'N'
         END;

      SELECT COUNT (*)
      INTO v_ChkVal
      FROM sbaref.IMRtTypTbl
      WHERE     IMRtTypCd = v_IMRtTypCd
            AND p_LoanAppRecvDt >= IMRtTypStrtDt
            AND (   p_LoanAppRecvDt < IMRtTypEndDt + 1
                 OR IMRtTypEndDt IS NULL);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;



   /*IF v_LoanAppAdjPrdCd = 'X' AND trim(v_LoanAppOthAdjPrdTxt) IS NULL
   THEN
       p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,169,p_TransInd);
   END IF;*/
   --v_LoanAppRqstAmtVar1 := 25000;
   --v_LoanAppRqstAmtVar2 := 50000;
   --Validates Loan Initial Interest Rate
   --Validates Spread Over Prime, default value
   --Validates Eligibilty Indicator
   /* 02-14-2011
   IF (v_LoanAppEligEvalInd IS NULL AND v_LoanTypInd != 'D') OR nvl(v_LoanAppEligEvalInd,'Y'
       ) NOT IN ('Y','N')
   THEN
       p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,156,p_TransInd,
                                 v_LoanAppEligEvalInd);
   END IF;
   IF (v_LoanAppEligEvalInd IS NULL AND v_LoanTypInd != 'D') OR nvl(v_LoanAppEligEvalInd,'Y'
       ) != 'Y'
   THEN
       p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,240,p_TransInd);
   END IF;*/
   --commenetd out below query as the validation is reverted back to original version.
   /*SELECT COUNT(*)
   INTO v_COUNT
   FROM LOANAPP.LOANAPPARCRSNTBL a
   WHERE a.LoanAppNmb = p_LoanAppNmb
   AND a.ARCLOANRSNCD IN (SELECT ARCLOANRSNCD
                          FROM SBAREF.ARCLOANRSNTBL
                          WHERE PRCSMTHDCD = NULL
                          );
   IF v_LoanTypInd != 'D' AND v_COUNT = 0
   THEN
       p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,240,p_TransInd);
   END IF;*/
   --Validates EWCP Single Transaction Indicator
   IF    (    v_PrcsMthdCd = 'PLW'
          AND v_LoanAppEWCPSnglTransInd IS NULL)
      OR (    v_LoanAppEWCPSnglTransInd IS NOT NULL
          AND v_LoanAppEWCPSnglTransInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    130,
                    p_TransInd,
                    v_LoanAppEWCPSnglTransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates EWCP Single Transaction Post Shipment Indicator
   IF     v_LoanAppEWCPSnglTransInd = 'Y'
      AND v_LoanAppEWCPSnglTransPostInd IS NULL
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    131,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
      INTO v_CDCGntyFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCGntyFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND (   IMPrmtrValEndDt IS NULL
                 OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

      SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
      INTO v_CDCFundFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCFundFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND (   IMPrmtrValEndDt IS NULL
                 OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

      SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
      INTO v_CDCPrcsFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCPrcsFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND (   IMPrmtrValEndDt IS NULL
                 OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

      SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
      INTO v_CDCServFeePctmax
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCServFeePctMax'
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND IMPrmtrValEndDt IS NULL;

      SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
      INTO v_CDCServFeePctmin
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCServFeePctMin'
            AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
            AND IMPrmtrValEndDt IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   BEGIN
      /* v_TotDebentAmt := v_LoanAppCDCNetDbentrAmt + v_LoanAppCDCUndrwtrFeeAmt + v_LoanAppCDCGntyAmt + v_LoanAppCDCFundFeeAmt + v_LoanAppCDCClsCostAmt;

      IF (NVL(v_LoanAppCDCSeprtPrcsFeeInd,'N') != 'Y') then
                      v_TotDebentAmt := v_TotDebentAmt + v_LoanAppCDCPrcsFeeAmt;
       END IF;
          v_BalToBorrAmt := v_LoanAppRqstAmt - v_TotDebentAmt;*/

      IF     v_prgrmcd = 'E'
         AND v_LoanAppBalToBorrAmt > 1000
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4050,
                       p_TransInd,
                       v_LoanAppBalToBorrAmt,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;
   END;

   IF     v_Prgrmcd = 'E'
      AND (   v_CDCServFeePct < v_CDCServFeePctmin
           OR v_CDCServFeePct > v_CDCServFeePctmax)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4071,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM sbaref.PrcsMthdTbl
   WHERE     PrcsMthdCd = v_PrcsMthdCd
         AND PrcsMthdExprsInd = 'Y';

   --Validates Injection Indicator
   IF    (    v_ChkVal = 0
          AND v_LoanAppInjctnInd IS NULL
          AND v_LoanTypInd != 'D')
      OR (    v_LoanAppInjctnInd IS NOT NULL
          AND v_LoanAppInjctnInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    149,
                    p_TransInd,
                    v_LoanAppInjctnInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates Life Insurance Required Indicator
   IF    (    v_PrcsMthdCd = 'LDP'
          AND v_LoanAppLifInsurRqmtInd IS NULL)
      OR (    v_LoanAppLifInsurRqmtInd IS NOT NULL
          AND v_LoanAppLifInsurRqmtInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    170,
                    p_TransInd,
                    v_LoanAppLifInsurRqmtInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates Months Interest Only Qty
   IF    v_LoanAppMoIntQty < 0
      OR (    v_LoanAppMoIntQty IS NULL
          AND v_PrgrmCd != 'E'
          AND v_LoanTypInd != 'D')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    168,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM sbaref.BusAgeCdTbl
   WHERE     BusAgeCd = v_LoanAppNewBusCd
         AND p_LoanAppRecvDt >= BusAgeStrtDt
         AND (   p_LoanAppRecvDt < BusAgeEndDt + 1
              OR BusAgeEndDt IS NULL);

   --Validates Business Age
   IF    (    v_PersonLoanInd = 'Y'
          AND v_LoanAppNewBusCd IS NOT NULL)
      OR (    v_LoanAppNewBusCd IS NOT NULL
          AND v_ChkVal = 0)
      OR (    v_LoanAppNewBusCd IS NULL
          AND v_PersonLoanInd = 'N'
          AND v_LoanTypInd != 'D')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    126,
                    p_TransInd,
                    v_LoanAppNewBusCd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validate Loan Name
   IF TRIM (v_LoanAppNm) IS NULL
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    163,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF    (    v_LoanTypInd != 'D'
          AND v_LoanCollatInd IS NULL)
      OR (    v_LoanCollatInd IS NOT NULL
          AND v_LoanCollatInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    250,
                    p_TransInd,
                    v_LoanCollatInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates Principal with Interest Indicator
   IF    (    v_LoanAppFullAmortPymtInd IS NOT NULL
          AND v_LoanAppFullAmortPymtInd NOT IN ('Y', 'N'))
      OR (    v_LoanAppFullAmortPymtInd IS NULL
          AND v_PrgrmCd != 'E'
          AND v_LoanTypInd != 'D')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    252,
                    p_TransInd,
                    v_LoanAppFullAmortPymtInd
                   );
   END IF;

   /*APK - 08/02/2010 Issue:Is Payment Fully Amortizing default - Item 130*/
   IF (    v_LoanAppFullAmortPymtInd IS NOT NULL
       AND v_LoanAppFullAmortPymtInd NOT IN ('Y', 'N')
       AND v_PrgrmCd IN ('A', 'R'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    252,
                    p_TransInd,
                    v_LoanAppFullAmortPymtInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   -- Validates Recondsideration Indicator and Underwriting By
   IF (    TRIM (v_undrwritngBy) = 'LNDR'
       AND v_LoanAppRcnsdrtnInd = 'Y')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    2074,
                    p_TransInd,
                    v_undrwritngBy,
                    v_LoanAppRcnsdrtnInd,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates Payment Amount
   --IF (v_LoanAppPymtAmt IS NULL OR v_LoanAppPymtAmt < 0) AND v_PrgrmCd NOT IN('X', 'E')
   IF     (   v_LoanAppPymtAmt IS NULL
           OR v_LoanAppPymtAmt < 0)
      AND v_PrgrmCd IN ('A', 'R')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    172,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates Recondsideration Indicator
   IF (    v_LoanAppRcnsdrtnInd IS NOT NULL
       AND v_LoanAppRcnsdrtnInd NOT IN ('Y', 'N'))
   /*OR (v_LoanAppRcnsdrtnInd IS NULL AND v_LoanTypInd != 'D') */
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    136,
                    p_TransInd,
                    v_LoanAppRcnsdrtnInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      v_IMPrmtrId   := 'LoanAmtMin';
      v_IMPrmtrValChk      :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );
      v_LoanAmtMin  := TO_NUMBER (v_IMPrmtrValChk);
   END;

   --Validates Request Amount
   IF    v_LoanAppRqstAmt IS NULL
      OR v_LoanAppRqstAmt < v_LoanAmtMin
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    105,
                    p_TransInd,
                    TO_CHAR (v_LoanAppRqstAmt),
                    TO_CHAR (v_LoanAmtMin),
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF     v_PrgrmCd IN ('A', 'R')
      AND (ROUND ( (v_LoanAppRqstAmt / 100), 0) * 100) != v_LoanAppRqstAmt
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    116,
                    p_TransInd,
                    TO_CHAR (v_LoanAppRqstAmt),
                    TO_CHAR (ROUND ( (v_LoanAppRqstAmt / 100), 0) * 100),
                    v_PrgrmCd,
                    '100',
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF     v_PrgrmCd = 'E'
      AND (ROUND ( (v_LoanAppRqstAmt / 1000), 0) * 1000) != v_LoanAppRqstAmt
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    116,
                    p_TransInd,
                    TO_CHAR (v_LoanAppRqstAmt),
                    TO_CHAR (ROUND ( (v_LoanAppRqstAmt / 1000), 0) * 1000),
                    v_PrgrmCd,
                    '1000',
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      FOR r1 IN (SELECT LOANECONDEVOBJCTCD
                 FROM LOANAPP.LOANECONDEVOBJCTCHLDTBL
                 WHERE LOANAPPNMB = p_LOANAPPNMB)
      LOOP
         SELECT COUNT (*)
         INTO v_ChkVal1
         FROM sbaref.EconDevObjctCdTbl
         WHERE     EconDevObjctGoalTyp = 'PPG'
               AND EconDevObjctCd = r1.LoanEconDevObjctCd;

         SELECT COUNT (*)
         INTO v_ChkVal
         FROM LoanSpcPurpsTbl
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND SpcPurpsLoanCd = 'DLTA';

         IF v_ChkVal > 0
         THEN
            v_DLTASpcPurpsInd  := 'Y';
         ELSE
            v_DLTASpcPurpsInd  := 'N';
         END IF;

         v_MaxLoanAppRqstAmt  := 0;

         SELECT COUNT (*)
         INTO v_ChkVal
         FROM IMPrmtrTbl a, IMPrmtrValTbl b
         WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
               AND a.IMPrmtrId = 'EconDevObjctEnergyCd'
               AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
               AND (   IMPrmtrValEndDt IS NULL
                    OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1)
               AND IMPrmtrValTxt = r1.LoanEconDevObjctCd;

         IF    v_ChkVal > 0
            OR (    v_LoanAppJobRqmtMetInd = 'Y'
                AND v_NAICSCd >= 300000
                AND v_NAICSCd < 400000)
         THEN
            BEGIN
               v_IMPrmtrId          := 'Loan5EconEngRqstAmtMax';
               v_IMPrmtrValChk      :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_MaxLoanAppRqstAmt  := TO_NUMBER (v_IMPrmtrValChk);
               --raise_application_error(-20001,'937 max : ' || to_char(v_MaxLoanAppRqstAmt));
            END;
         ELSIF v_ChkVal1 > 0                                                              --(check for 'PPG')
         THEN
            BEGIN
               v_IMPrmtrId          := 'Loan5EconRqstAmtMax';
               v_IMPrmtrValChk      :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_MaxLoanAppRqstAmt  := TO_NUMBER (v_IMPrmtrValChk);
               --raise_application_error(-20001,'951 max : ' || to_char(v_MaxLoanAppRqstAmt));
            END;
         ELSIF v_DLTASpcPurpsInd = 'Y'
         THEN
            BEGIN
               v_IMPrmtrId          := 'LoanRqstAmtDltaMax';
               v_IMPrmtrValChk      :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_MaxLoanAppRqstAmt  := TO_NUMBER (v_IMPrmtrValChk);
            END;
         ELSE
            BEGIN
               v_IMPrmtrId          := 'LoanRqstAmtMax';
               v_IMPrmtrValChk      :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_MaxLoanAppRqstAmt  := TO_NUMBER (v_IMPrmtrValChk);
            END;
         END IF;
raise_application_error(-20001,'max rqst amt : ' || to_char(v_MaxLoanAppRqstAmt));
         IF     v_PrgrmCd = 'E'
            AND v_LoanAppRqstAmt > v_MaxLoanAppRqstAmt
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          101,
                          p_TransInd,
                          TO_CHAR (v_LoanAppRqstAmt),
                          TO_CHAR (v_MaxLoanAppRqstAmt),
                          TO_CHAR (r1.LoanEconDevObjctCd),
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END LOOP;
   END;

   v_SpcPurpsLoanCd          := NULL;

   /*SELECT COUNT(*) INTO v_ChkVal FROM LoanSpcPurpsTbl WHERE LoanAppNmb = p_LoanAppNmb AND SpcPurpsLoanCd = 'REVL';
   IF v_ChkVal > 0 THEN
       v_SpcPurpsLoanCd := 'REVL';
   END IF;*/
   --Validates Maturity Term
   IF    v_LoanAppRqstMatMoQty IS NULL
      OR v_LoanAppRqstMatMoQty <= 0
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    115,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      v_IMPrmtrId  := 'MatMoMax';
      v_IMPrmtrValChk      :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );
      v_MatMoMax   := TO_NUMBER (v_IMPrmtrValChk);
   END;

   --changes by VK to make similar to loan.ValidateGntyFinCSP
   BEGIN
      SELECT SpcPurpsLoanCd
      INTO v_SpcPurpsLoanCd
      FROM LoanSpcPurpsTbl
      WHERE     LoanAppNmb = p_LoanAppNmb
            AND SpcPurpsLoanCd = 'REVL';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_SpcPurpsLoanCd  := NULL;
   END;

   IF v_SpcPurpsLoanCd = 'REVL'
   THEN
      BEGIN
         v_IMPrmtrId  := 'MatMoRevlMax';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_MatMoMax   := TO_NUMBER (v_IMPrmtrValChk);
      END;
   END IF;

   BEGIN
      SELECT SpcPurpsLoanCd
      INTO v_SpcPurpsLoanCd
      FROM LoanSpcPurpsTbl
      WHERE     LoanAppNmb = p_LoanAppNmb
            AND SpcPurpsLoanCd = 'CONS';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_SpcPurpsLoanCd  := NVL (v_SpcPurpsLoanCd, NULL);
   END;

   IF v_SpcPurpsLoanCd = 'CONS'
   THEN
      BEGIN
         v_IMPrmtrId  := 'MatMoConsMax';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_MatMoMax   := GREATEST (TO_NUMBER (v_IMPrmtrValChk), NVL (v_MatMoMax, 0));
      END;
   END IF;

   IF NVL (v_LoanAppRqstMatMoQty, 0) > NVL (v_MatMoMax, 0)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    118,
                    p_TransInd,
                    TO_CHAR (v_LoanAppRqstMatMoQty),
                    TO_CHAR (v_MatMoMax),
                    v_SpcPurpsLoanCd,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM IMPrmtrTbl a, IMPrmtrValTbl b
   WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
         AND a.IMPrmtrId IN ('MatMo5ShortTrmLimit', 'MatMo5LongTrmLimit')
         AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
         AND (   IMPrmtrValEndDt IS NULL
              OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1)
         AND IMPrmtrValTxt = TRIM (TO_CHAR (NVL (v_LoanAppRqstMatMoQty, 0)));

   IF     v_PrgrmCd = 'E'
      AND v_ChkVal = 0
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    157,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      v_IMPrmtrId   := 'GntyPctMin';
      v_IMPrmtrValChk      :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );
      v_GntyPctMin  := TO_NUMBER (v_IMPrmtrValChk);
   END;

   --Validates SBA Percentage Guaranteed
   IF    v_LoanAppSBAGntyPct IS NULL
      OR v_LoanAppSBAGntyPct < v_GntyPctMin
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    112,
                    p_TransInd,
                    TO_CHAR (v_GntyPctMin),
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   v_MaxGntyPct              := NULL;
   v_GntyPctRqstAmtLrgLimit  := NULL;

   BEGIN
      v_IMPrmtrId               := 'GntyPctRqstAmtLrgLimit';
      v_IMPrmtrValChk           :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );
      v_GntyPctRqstAmtLrgLimit  := TO_NUMBER (v_IMPrmtrValChk);
   END;

   BEGIN
      IF v_LoanAppRqstAmt > v_GntyPctRqstAmtLrgLimit
      THEN
         BEGIN
            v_IMPrmtrId   := 'GntyPct7LrgAmtMax';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_MaxGntyPct  := TO_NUMBER (v_IMPrmtrValChk);
         END;
      ELSE
         BEGIN
            v_IMPrmtrId   := 'GntyPctLowAmtMax';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_MaxGntyPct  := TO_NUMBER (v_IMPrmtrValChk);
         END;
      END IF;

      IF v_LoanAppSBAGntyPct > v_MaxGntyPct
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       109,
                       p_TransInd,
                       v_PrgrmCd,
                       TO_CHAR (v_LoanAppRqstAmt),
                       TO_CHAR (v_LoanAppSBAGntyPct),
                       TO_CHAR (v_MaxGntyPct),
                       v_PrcsMthdCd,
                       v_FININSTRMNTTYPIND
                      );
      END IF;
   END;

   IF    v_PrgrmCd = 'E'
      OR v_PrcsMthdCd = 'ARC'
   THEN
      BEGIN
         BEGIN
            v_IMPrmtrId    := 'GntyPct5Max';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_GntyPct5Max  := TO_NUMBER (v_IMPrmtrValChk);
         END;

         IF NVL (v_LoanAppSBAGntyPct, 0) != v_GntyPct5Max
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          111,
                          p_TransInd,
                          TO_CHAR (v_LoanAppSBAGntyPct),
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;
   END IF;

   v_MaxLoanExpo             := NULL;

   IF v_DLTASpcPurpsInd = 'Y'
   THEN
      BEGIN
         v_IMPrmtrId    := 'GntyAmtDELTAMax';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_MaxLoanExpo  := TO_NUMBER (v_IMPrmtrValChk);
      END;
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM LoanSpcPurpsTbl
   WHERE     SpcPurpsLoanCd = 'PREQ'
         AND LoanAppNmb = p_LoanAppNmb;

   IF v_ChkVal > 0
   THEN
      BEGIN
         v_IMPrmtrId    := 'GntyAmtPREQMax';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_MaxLoanExpo  := TO_NUMBER (v_IMPrmtrValChk);
      END;
   END IF;

   IF v_MaxLoanExpo IS NULL
   THEN
      BEGIN
         BEGIN
            v_IMPrmtrId    := 'GntyAmtMax';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_MaxLoanExpo  := TO_NUMBER (v_IMPrmtrValChk);
         END;
      END;
   END IF;

   IF v_LoanAppSBAGntyPct * v_LoanAppRqstAmt * .01 > v_MaxLoanExpo
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    110,
                    p_TransInd,
                    TO_CHAR (v_MaxLoanExpo),
                    TO_CHAR (v_LoanAppRqstAmt),
                    TO_CHAR (v_LoanAppSBAGntyPct),
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates EPC Indicator
   IF    (    v_LoanAppEPCInd IS NULL
          AND v_PersonLoanInd != 'Y'
          AND v_LoanTypInd != 'D')
      OR (    v_LoanAppEPCInd IS NOT NULL
          AND v_LoanAppEPCInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    276,
                    p_TransInd,
                    v_LoanAppEPCInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM sbaref.PrcsMthdTbl
   WHERE     PrcsMthdCd = v_PrcsMthdCd
         AND p_LoanAppRecvDt >= PrcsMthdStrtDt
         AND (   p_LoanAppRecvDt < PrcsMthdEndDt + 1
              OR PrcsMthdEndDt IS NULL);

   --Validates Processing Method
   IF v_ChkVal = 0
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    121,
                    p_TransInd,
                    v_PrcsMthdCd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM sbaref.PrgrmTbl
   WHERE     PrgrmCd = v_PrgrmCd
         AND p_LoanAppRecvDt >= PrgrmStrtDt
         AND (   p_LoanAppRecvDt < PrgrmEndDt + 1
              OR PrgrmEndDt IS NULL);

   --Validates Progrom Code
   IF v_ChkVal = 0
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    280,
                    p_TransInd,
                    v_PrgrmCd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT COUNT (*)
   INTO v_ChkVal
   FROM sbaref.PrgrmValidTbl
   WHERE     PrgrmCd = v_PrgrmCd
         AND PrcsMthdCd = v_PrcsMthdCd
         AND FININSTRMNTTYPIND = v_FININSTRMNTTYPIND
         AND p_LoanAppRecvDt >= PrgrmValidStrtDt
         AND (   p_LoanAppRecvDt < PrgrmValidEndDt + 1
              OR PrgrmValidEndDt IS NULL);

   IF v_ChkVal = 0
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    282,
                    p_TransInd,
                    v_PrgrmCd,
                    v_PrcsMthdCd,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validates SBA Recommend Loan Amount
   /* APK 04/15/2010 Modified below code for 174 so that 174 error cd is not checked for 'SMP'*/
   IF v_PrcsMthdCd = 'SMP'
   THEN
      BEGIN
         NULL;
      END;
   ELSE
      BEGIN
         /*PARTNER.CHECKUNDRWRITNGBYCSP(0,p_loanappnmb,v_PrcsMthdCd,sysdate,'LNDR',p_LoanAppRecvDt,v_ChkVal);

         /*SELECT COUNT(*)
         INTO v_ChkVal
         FROM sbaref.PrcsMthdTbl
         WHERE PrcsMthdCd         = v_PrcsMthdCd
         AND PrcsMthdUndrWritngBy = 'LNDR';*/

         IF TRIM (v_undrwritngBy) = 'LNDR'
         THEN
            v_ChkVal  := 1;
         ELSE
            v_ChkVal  := 0;
         END IF;


         IF     v_ChkVal > 0
            AND (   v_LoanAppSBARcmndAmt IS NULL
                 OR v_LoanAppSBARcmndAmt != v_LoanAppRqstAmt)
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          174,
                          p_TransInd,
                          TO_CHAR (v_LoanAppSBARcmndAmt),
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;
   END IF;

   /* APK 04/15/2010 Modified below code for 174 so that 174 error cd is not checked for 'SMP'*/
   IF v_PrcsMthdCd = 'SMP'
   THEN
      BEGIN
         NULL;
      END;
   ELSE
      BEGIN
         /*PARTNER.CHECKUNDRWRITNGBYCSP(0,p_loanappnmb,v_PrcsMthdCd,sysdate,'SBA',p_LoanAppRecvDt,v_ChkVal);
         SELECT COUNT(*)
         INTO v_ChkVal
         FROM sbaref.PrcsMthdTbl
         WHERE PrcsMthdUndrwritngBy = 'SBA'
         AND PrcsMthdCd             = v_PrcsMthdCd;*/

         IF TRIM (v_undrwritngBy) = 'SBA'
         THEN
            v_ChkVal  := 1;
         ELSE
            v_ChkVal  := 0;
         END IF;

         IF v_ChkVal > 0
         THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM LoanAppTbl
            WHERE     LoanAppNmb = p_LoanAppNmb
                  AND StatCd = 'AP';

            IF     v_ChkVal = 0
               AND (   v_LoanAppSBARcmndAmt IS NULL
                    OR v_LoanAppSBARcmndAmt <= 0
                    OR v_LoanAppSBARcmndAmt > v_LoanAppRqstAmt)
            THEN
               p_ErrSeqNmb      :=
                  ADD_ERROR (
                             p_LoanAppNmb,
                             p_ErrSeqNmb,
                             174,
                             p_TransInd,
                             TO_CHAR (v_LoanAppSBARcmndAmt),
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             v_FININSTRMNTTYPIND
                            );
            END IF;
         END IF;
      END;
   END IF;

   --------------------
   --rons changes start 1
   --------------------
   IF v_PrgrmCd = 'A'
   THEN
      BEGIN
         loan.LoadLoanEntCSP (p_LoanAppNmb);

         IF v_DLTASpcPurpsInd = 'Y'
         THEN
            BEGIN
               SELECT   v_LoanAppRqstAmt
                      + SUM (
                             CASE
                                WHEN     a.LoanStatCd > 1
                                     AND EXISTS
                                            (SELECT 1
                                             FROM loan.LoanGntySpcPurpsTbl z
                                             WHERE     SpcPurpsLoanCd = 'REVL'
                                                   AND z.LoanAppNmb = a.LoanAppNmb)
                                THEN
                                   a.LoanCurrAppvAmt
                                WHEN a.LoanStatCd > 1
                                THEN
                                   a.LoanOutBalAmt
                                ELSE
                                   LoanCurrAppvAmt
                             END
                            )
               INTO v_LoanSumAmt
               FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
               WHERE     a.LoanAppNmb = b.LoanAppNmb
                     AND EXISTS
                            (SELECT 1
                             FROM loan.LoanEntTbl z
                             WHERE     z.LoanAppNmb = p_LoanAppNmb
                                   AND z.TaxId = b.TaxId)
                     AND b.LoanBusPrimBorrInd = 'Y'
                     AND a.LoanStatCd < 4
                     AND a.PrgmCd = 'A'
                     AND EXISTS
                            (SELECT 1
                             FROM loan.LoanGntySpcPurpsTbl z
                             WHERE     z.LoanAppNmb = a.LoanAppNmb
                                   AND z.SpcPurpsLoanCd = 'DLTA');

               v_IMPrmtrId      := 'AggregateRqstAmtDltaMax';
               v_IMPrmtrValChk      :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_MaxLoanSumAmt  := TO_NUMBER (v_IMPrmtrValChk);
            END;
         ELSE
            BEGIN
               v_IMPrmtrId           := 'AggregatePrcsGroup';
               v_IMPrmtrValChk       :=
                  LOANAPP.GET_IMPRMTRVAL (
                                          v_IMPrmtrId,
                                          v_PrcsMthdCd,
                                          p_LoanAppRecvDt,
                                          v_RecovInd
                                         );
               v_AggregatePrcsGroup  := RTRIM (v_IMPrmtrValChk);

               IF v_AggregatePrcsGroup IS NOT NULL
               THEN
                  SELECT   v_LoanAppRqstAmt
                         + SUM (
                                CASE
                                   WHEN     a.LoanStatCd > 1
                                        AND EXISTS
                                               (SELECT 1
                                                FROM loan.LoanGntySpcPurpsTbl z
                                                WHERE     SpcPurpsLoanCd = 'REVL'
                                                      AND z.LoanAppNmb = a.LoanAppNmb)
                                   THEN
                                      a.LoanCurrAppvAmt
                                   WHEN a.LoanStatCd > 1
                                   THEN
                                      a.LoanOutBalAmt
                                   ELSE
                                      LoanCurrAppvAmt
                                END
                               )
                  INTO v_LoanSumAmt
                  FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                  WHERE     a.LoanAppNmb = b.LoanAppNmb
                        AND EXISTS
                               (SELECT 1
                                FROM loan.LoanEntTbl z
                                WHERE     z.LoanAppNmb = p_LoanAppNmb
                                      AND z.TaxId = b.TaxId)
                        AND b.LoanBusPrimBorrInd = 'Y'
                        AND a.LoanStatCd < 4
                        AND a.PrgmCd = 'A'
                        AND EXISTS
                               (SELECT 1
                                FROM loanapp.IMPrmtrTbl z, loanapp.IMPrmtrValTbl y
                                WHERE     z.IMPrmtrSeqNmb = y.IMPrmtrSeqNmb
                                      AND z.IMPrmtrId = v_IMPrmtrId
                                      AND y.IMPRMTRVALTXT = v_AggregatePrcsGroup
                                      AND y.PrcsMthdCd = a.PrcsMthdCd
                                      AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                                      AND (   IMPrmtrValEndDt IS NULL
                                           OR IMPrmtrValEndDt + 1 > p_LoanAppRecvDt));

                  v_IMPrmtrId      := 'AggregateRqstAmtGroupMax';
                  v_IMPrmtrValChk      :=
                     LOANAPP.GET_IMPRMTRVAL (
                                             v_IMPrmtrId,
                                             v_PrcsMthdCd,
                                             p_LoanAppRecvDt,
                                             v_AggregatePrcsGroup
                                            );
                  v_MaxLoanSumAmt  := TO_NUMBER (v_IMPrmtrValChk);
               ELSE
                  DELETE LOAN.TMPLOANGNTYAGGTBL
                  WHERE LoanMainAppNmb = p_LoanAppNmb;

                  INSERT INTO LOAN.TMPLOANGNTYAGGTBL (LOANMAINAPPNMB,
                                                      LOANAPPNMB,
                                                      LOANCURRAPPVAMT,
                                                      LOANAPPFUNDDT)
                     SELECT p_LoanAppNmb,
                            a.LoanAppNmb,
                            CASE
                               WHEN     a.LoanStatCd > 1
                                    AND EXISTS
                                           (SELECT 1
                                            FROM loan.LoanGntySpcPurpsTbl z
                                            WHERE     SpcPurpsLoanCd = 'REVL'
                                                  AND z.LoanAppNmb = a.LoanAppNmb)
                               THEN
                                  a.LoanCurrAppvAmt
                               WHEN a.LoanStatCd > 1
                               THEN
                                  a.LoanOutBalAmt
                               ELSE
                                  LoanCurrAppvAmt
                            END,
                            a.LOANAPPFUNDDT
                     FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                     WHERE     a.LoanAppNmb = b.LoanAppNmb
                           AND EXISTS
                                  (SELECT 1
                                   FROM loan.LoanEntTbl z
                                   WHERE     z.LoanAppNmb = p_LoanAppNmb
                                         AND z.TaxId = b.TaxId)
                           AND b.LoanBusPrimBorrInd = 'Y'
                           AND a.PrgmCd = 'A'
                           AND a.LoanStatCd < 4;

                  v_LoanSumAmt     := v_LoanAppRqstAmt + loan.GETGNTYAGGAMT (p_LoanAppNmb, p_LoanAppRecvDt);

                  DELETE LOAN.TMPLOANGNTYAGGTBL
                  WHERE LoanMainAppNmb = p_LoanAppNmb;

                  v_IMPrmtrId      := 'AggregateRqstAmtMax';
                  v_IMPrmtrValChk      :=
                     LOANAPP.GET_IMPRMTRVAL (
                                             v_IMPrmtrId,
                                             v_PrcsMthdCd,
                                             p_LoanAppRecvDt,
                                             v_RecovInd
                                            );
                  v_MaxLoanSumAmt  := TO_NUMBER (v_IMPrmtrValChk);
               END IF;
            END;
         END IF;

         IF NVL (v_LoanSumAmt, v_LoanAppRqstAmt) > v_MaxLoanSumAmt
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          176,
                          p_TransInd,
                          v_PrcsMthdCd,
                          TO_CHAR (v_LoanSumAmt),
                          TO_CHAR (v_MaxLoanSumAmt),
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;

         SELECT   v_LoanAppRqstAmt * v_LoanAppSBAGntyPct * 0.01
                + SUM (
                         (CASE
                             WHEN     a.LoanStatCd > 1
                                  AND EXISTS
                                         (SELECT 1
                                          FROM loan.LoanGntySpcPurpsTbl z
                                          WHERE     SpcPurpsLoanCd = 'REVL'
                                                AND z.LoanAppNmb = a.LoanAppNmb)
                             THEN
                                a.LoanCurrAppvAmt
                             WHEN a.LoanStatCd > 1
                             THEN
                                a.LoanOutBalAmt
                             ELSE
                                LoanCurrAppvAmt
                          END)
                       * LoanAppSBAGntyPct
                       * 0.01
                      )
         INTO v_LoanSumAmt
         FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
         WHERE     a.LoanAppNmb = b.LoanAppNmb
               AND EXISTS
                      (SELECT 1
                       FROM loan.LoanEntTbl z
                       WHERE     z.LoanAppNmb = p_LoanAppNmb
                             AND z.TaxId = b.TaxId)
               AND b.LoanBusPrimBorrInd = 'Y'
               AND a.PrgmCd = 'A'
               AND a.LoanStatCd < 4;

         --------------
         --rons changes end 1
         --------------
         BEGIN
            v_IMPrmtrId      := 'AggregateSBAShareAmtMax';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_MaxLoanSumAmt  := TO_NUMBER (v_IMPrmtrValChk);
         END;

         IF NVL (v_LoanSumAmt, v_LoanAppRqstAmt * v_LoanAppSBAGntyPct * 0.01) > v_MaxLoanSumAmt
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          177,
                          p_TransInd,
                          TO_CHAR (v_LoanSumAmt),
                          TO_CHAR (v_MaxLoanSumAmt),
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;

         --------------
         --rons changes start 2
         --------------
         --sbx apk 01/31/2012--
         BEGIN
            v_IMPrmtrId      := 'AggregatePerPrcsMthd';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_MaxLoanSumAmt  := TO_NUMBER (v_IMPrmtrValChk);
         END;

         IF NVL (v_MaxLoanSumAmt, 0) > 0
         THEN
            SELECT   v_LoanAppRqstAmt
                   + NVL (
                          SUM (
                               CASE
                                  WHEN     a.LoanStatCd > 1
                                       AND EXISTS
                                              (SELECT 1
                                               FROM loan.LoanGntySpcPurpsTbl z
                                               WHERE     SpcPurpsLoanCd = 'REVL'
                                                     AND z.LoanAppNmb = a.LoanAppNmb)
                                  THEN
                                     a.LoanCurrAppvAmt
                                  WHEN a.LoanStatCd > 1
                                  THEN
                                     a.LoanOutBalAmt
                                  ELSE
                                     LoanCurrAppvAmt
                               END
                              ),
                          0
                         )
            INTO v_LoanSumAmt
            FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
            WHERE     a.LoanAppNmb = b.LoanAppNmb
                  AND EXISTS
                         (SELECT 1
                          FROM loan.LoanEntTbl z
                          WHERE     z.LoanAppNmb = p_LoanAppNmb
                                AND z.TaxId = b.TaxId)
                  AND b.LoanBusPrimBorrInd = 'Y'
                  AND a.PrgmCd = 'A'
                  AND a.PrcsMthdCd = v_PrcsMthdCd
                  AND a.LoanStatCd < 4;

            IF v_LoanSumAmt > v_MaxLoanSumAmt
            THEN
               p_ErrSeqNmb      :=
                  ADD_ERROR (
                             p_LoanAppNmb,
                             p_ErrSeqNmb,
                             1100,
                             p_TransInd,
                             v_PrcsMthdCd,
                             TO_CHAR (v_LoanSumAmt),
                             TO_CHAR (v_MaxLoanSumAmt),
                             NULL,
                             NULL,
                             v_FININSTRMNTTYPIND
                            );
            END IF;
         END IF;

         ----sbx--

         IF v_PrcsMthdCd = 'ARC'
         THEN
            BEGIN
               SELECT COUNT (*)
               INTO v_ChkVal
               FROM loan.LoanGntyBorrTbl a, loan.LoanGntyTbl b
               WHERE     a.LoanAppNmb = b.LoanAppNmb
                     AND a.LoanBusPrimBorrInd = 'Y'
                     AND a.LoanBorrActvInactInd = 'A'
                     AND b.LoanStatCd != 4
                     AND b.PrcsMthdCd = 'ARC'
                     AND a.TaxId = v_TaxId;

               IF v_ChkVal > 0
               THEN
                  p_ErrSeqNmb      :=
                     ADD_ERROR (
                                p_LoanAppNmb,
                                p_ErrSeqNmb,
                                237,
                                p_TransInd,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                v_FININSTRMNTTYPIND
                               );
               END IF;

               IF v_LoanAppRqstMatMoQty NOT BETWEEN 72 AND 78
               THEN
                  p_ErrSeqNmb      :=
                     ADD_ERROR (
                                p_LoanAppNmb,
                                p_ErrSeqNmb,
                                248,
                                p_TransInd,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                v_FININSTRMNTTYPIND
                               );
               END IF;
            END;                                                                                     --IF ARC
         END IF;

         DELETE loan.LoanEntTbl
         WHERE LoanAppNmb = p_LoanAppNmb;
      END;
   END IF;

   IF     v_PrcsMthdCd = '5RE'
      AND NVL (v_LOANAMTLIMEXMPTIND, 'N') != 'Y'
      AND v_LocId IS NOT NULL
   THEN
      SELECT SUM (LoanCurrAppvamt)
      INTO v_TotalPrevFyAppvAmt
      FROM loan.loangntytbl
      WHERE     locid = v_LocId
            AND sbaref.fiscal_year (loanappfunddt) = sbaref.fiscal_year (SYSDATE) - 1
            AND prgmcd = 'E'
            AND (prcsmthdcd != '5RE' or prcsmthdcd != '5RX');

      SELECT v_LoanAppRqstAmt + NVL (SUM (LoanCurrAppvamt), 0)
      INTO v_TotalCurrFy5REAmt
      FROM loan.loangntytbl
      WHERE     locid = v_LocId
            AND sbaref.fiscal_year (loanappfunddt) = sbaref.fiscal_year (SYSDATE)
            AND prgmcd = 'E'
            AND prcsmthdcd = '5RE';

      v_IMPrmtrId  := 'Pct504RefinFySumMax';
      v_IMPrmtrValChk      :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );

      IF v_TotalCurrFy5REAmt > NVL (v_TotalPrevFyAppvAmt, 0) * TO_NUMBER (v_IMPrmtrValChk) / 100
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       1101,
                       p_TransInd,
                       TO_CHAR (v_TotalCurrFy5REAmt),
                       TO_CHAR (v_TotalPrevFyAppvAmt),
                       v_IMPrmtrValChk,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;
   END IF;

   IF v_PrcsMthdCd = 'PTX'
   THEN
      BEGIN
         SELECT SUM (T2.LoanBusPrinPctOwnrshpBus)
         INTO v_VetPctOwnrshpSum
         FROM LoanBorrTbl      T3,
              LoanPrinTbl      T1,
              LoanBusPrinTbl   T2,
              sbaref.VetTbl    v
         WHERE     T1.LoanAppNmb = p_LoanAppNmb
               AND T2.LoanAppNmb = p_LoanAppNmb
               AND T3.LoanAppNmb = p_LoanAppNmb
               AND T1.PrinSeqNmb = T2.PrinSeqNmb
               AND T2.BorrSeqNmb = T3.BorrSeqNmb
               AND T3.LoanBusPrimBorrInd = 'Y'
               AND T1.VetCd = v.VetCd
               AND v.VetGrpCd IN ('S', 'C');

         v_IMPrmtrId            := 'PctOwnrshpVetSumMin';
         v_IMPrmtrValChk        :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_PctOwnrshpVetSumMin  := TO_NUMBER (v_IMPrmtrValChk);

         IF NVL (v_VetPctOwnrshpSum, 0) < v_PctOwnrshpVetSumMin
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          180,
                          p_TransInd,
                          TO_CHAR (v_VetPctOwnrshpSum),
                          TO_CHAR (v_PctOwnrshpVetSumMin),
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;
   END IF;

   IF v_PrcsMthdCd = 'MLD'
   THEN
      BEGIN
         SELECT COUNT (*)
         INTO v_ChkVal
         FROM partner.PrtAliasTbl z
         WHERE     TRIM (z.PrtAliasNm) = TRIM (v_LoanAppMicroLendrId)
               AND z.ValidPrtAliasTyp = 'Micro';

         IF    v_LoanAppMicroLendrId IS NULL
            OR v_ChkVal = 0
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          200,
                          p_TransInd,
                          TO_CHAR (v_LoanAppMicroLendrId),
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;                                                                                           --IF MLD
   END IF;

   IF v_PrcsMthdCd = 'ARC'
   THEN
      BEGIN
         SELECT COUNT (*)
         INTO v_ChkVal
         FROM LoanAppARCRsnTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

         IF v_ChkVal = 0
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          234,
                          p_TransInd,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;

         SELECT COUNT (*)
         INTO v_ChkVal
         FROM LoanAppARCRsnTbl
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND ARCLoanRsnCd = 9
               AND ARCLoanRsnOthDescTxt IS NULL;

         IF v_ChkVal > 0
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          247,
                          p_TransInd,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;                                                                                           --IF ARC
   END IF;

   IF v_RecovInd = 'R'
   THEN
      BEGIN
         SELECT COUNT (*)
         INTO v_ChkVal
         FROM IMPrmtrTbl a, IMPrmtrValTbl b
         WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
               AND a.IMPrmtrId = 'NAICSCdNonARRA'
               AND TO_NUMBER (IMPrmtrValTxt) = v_NAICSCd
               AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
               AND (   IMPrmtrValEndDt IS NULL
                    OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

         IF v_ChkVal > 0
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          235,
                          p_TransInd,
                          TO_CHAR (v_NAICSCd),
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;

         SELECT COUNT (*)
         INTO v_ChkVal
         FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
         WHERE     a.LoanStatCd = 4
               AND a.LoanAppNmb = b.LoanAppNmb
               AND b.LoanBusPrimBorrInd = 'Y'
               AND b.TaxId = v_TaxId
               AND LoanInitlAppvAmt = v_LoanAppRqstAmt
               AND a.RecovInd IS NULL
               AND ( (SELECT MAX (LoanTransEffDt)
                      FROM loan.LoanTransTbl z
                      WHERE z.LoanAppNmb = a.LoanAppNmb) >= (SYSDATE - 90));

         IF v_ChkVal > 0
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          233,
                          p_TransInd,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END;                                                                                      --IF Recovery
   END IF;

   --Validates CDC Closing Cost Amount
   IF     v_PrgrmCd = 'E'
      AND (   v_LoanAppCDCClsCostAmt IS NULL
           OR v_LoanAppCDCClsCostAmt < 0)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    142,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validate CDC Net Debenture Amount
   IF     v_PrgrmCd = 'E'
      AND (   v_LoanAppCDCNetDbentrAmt IS NULL
           OR v_LoanAppCDCNetDbentrAmt <= 0)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    107,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF    (    v_PrgrmCd = 'E'
          AND v_LoanAppCDCSeprtPrcsFeeInd IS NULL)
      OR (    v_LoanAppCDCSeprtPrcsFeeInd IS NOT NULL
          AND v_LoanAppCDCSeprtPrcsFeeInd NOT IN ('Y', 'N'))
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    113,
                    p_TransInd,
                    v_LoanAppCDCSeprtPrcsFeeInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   SELECT NVL (SUM (LoanInjctnAmt), 0)
   INTO v_InjctnSum
   FROM LoanInjctnTbl
   WHERE LoanAppNmb = p_LoanAppNmb;

   --Validate Borrower Contribution Amount
   IF     v_PrgrmCd = 'E'
      AND (   v_LoanAppContribAmt IS NULL
           OR v_LoanAppContribAmt <> v_InjctnSum)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    147,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   --Validate Borrower Contribution Percent
   IF     v_PrgrmCd = 'E'
      AND (   v_LoanAppContribPct IS NULL
           OR v_LoanAppContribPct < 0)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    108,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF     v_PrgrmCd = 'E'
      AND v_LOANCOLLATIND = 'N'
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4049,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   BEGIN
      /* Get loan application information */
      SELECT l.PrcsMthdCd, LoanAppRecvDt, l.LoanAppSBARcmndAmt, l.RecovInd
      INTO v_PrcsMthdCd, v_LoanAppRecvDt, v_LoanAmt, v_RecovInd
      FROM loanapp.LoanAppTbl l
      WHERE l.LoanAppNmb = p_LoanAppNmb;

      v_FY  := TO_CHAR (SYSDATE, 'YYYY') + SIGN (SIGN (TO_CHAR (SYSDATE, 'MM') - 10) + 1);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   BEGIN
      /* Retrieve Cohort code */
      SELECT COUNT (*)
      INTO v_CohortCdCnt
      FROM sbaref.PrcsMthdCohortTbl pv, loanacct.FundsBalTbl fb
      WHERE     pv.PrcsMthdCd = v_PrcsMthdCd
            AND NVL (pv.RecovInd, ' ') = NVL (v_RecovInd, ' ')
            AND pv.CohortCd = fb.CohortCd
            AND fb.FundsFisclYr <= v_FY
            AND fb.FundsEndFisclYr >= v_FY
            --and fb.FUNDSORIGFROZENIND = 'N'
            --and fb.FundsBalAmt >= v_LoanAmt
            AND fb.FUNDSACTVINACTVIND = 'A'
            AND pv.COHORTSTRTDT <= SYSDATE
            AND (   pv.COHORTENDDT IS NULL
                 OR pv.COHORTENDDT + 1 > SYSDATE);
   --AND ROWNUM = 1
   --ORDER BY POS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_CohortCdCnt  := 0;
   END;

   IF (v_CohortCdCnt = 0)
   THEN
      BEGIN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       921,
                       p_TransInd,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END;
   END IF;

   IF v_PrcsMthdCd = 'SMP'
   THEN
      BEGIN
         v_IMPrmtrId  := 'MatMoMin';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_MatMoMin   := TO_NUMBER (v_IMPrmtrValChk);
      END;
   END IF;

   IF     v_PrcsMthdCd = 'SMP'
      AND v_LoanAppRqstMatMoQty < v_MatMoMin
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    922,
                    p_TransInd,
                    TO_CHAR (v_LoanAppRqstMatMoQty),
                    TO_CHAR (v_MatMoMin),
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   /*APK 08/16/2010- new validation in loanapp.ValidateLoanApp stored procedure to make LoanAppDisasterCntrlNmb mandatory for PrcsMthdCd EQ 'DGB' */
   IF     v_PrcsMthdCd = 'DGB'
      AND v_LOANAPPDISASTERCNTRLNMB IS NULL
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    925,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF v_PrgrmCd = 'H'
   THEN
      IF v_LOANAPPDISASTERCNTRLNMB IS NULL
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       1027,
                       p_TransInd,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;

      IF v_LOANDISASTRAPPNMB IS NULL
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       1028,
                       p_TransInd,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;
   END IF;

   BEGIN
      SELECT 1
      INTO v_ChkVal
      FROM DUAL
      WHERE EXISTS
               (SELECT *
                FROM SBAREF.PRGRMVALIDTBL v
                WHERE     v.PrcsMthdCd = v_PrcsMthdCd
                      AND v.FININSTRMNTTYPIND = v_FININSTRMNTTYPIND
                      AND p_LoanAppRecvDt >= PrgrmValidStrtDt
                      AND (   p_LoanAppRecvDt < PrgrmValidEndDt + 1
                           OR PrgrmValidEndDt IS NULL)
                      AND v.SPCPURPSLOANCD = 'REVL');
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_ChkVal  := 0;
   END;

   BEGIN
      SELECT SpcPurpsLoanCd
      INTO v_SpcPurpsLoanCd
      FROM LoanSpcPurpsTbl
      WHERE     LoanAppNmb = p_LoanAppNmb
            AND SpcPurpsLoanCd = 'REVL';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_SpcPurpsLoanCd  := NULL;
   END;

   IF (    v_ChkVal = 0
       AND v_LoanAppRevlInd IS NOT NULL
       AND v_LoanAppRevlInd != 'N')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1029,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF v_ChkVal = 1
   THEN
      IF (    v_SpcPurpsLoanCd = 'REVL'
          AND v_LoanAppRevlInd <> 'Y')
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       1030,
                       p_TransInd,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;

      IF (    v_SpcPurpsLoanCd <> 'REVL'
          AND v_LoanAppRevlInd <> 'N')
      THEN
         p_ErrSeqNmb      :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       1031,
                       p_TransInd,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       v_FININSTRMNTTYPIND
                      );
      END IF;
   END IF;

   -- validate LoanAppPymtAmt
   IF NVL (v_LoanAppPymtAmt, 0) >= 1000000
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1044,
                    p_TransInd,
                    TO_CHAR (p_LoanAppNmb),
                    TO_CHAR (v_LoanAppPymtAmt),
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF v_LOANAPPINTDTLCD IS NULL
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    2004,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   -- IF v_PrcsMthdCd = 'SMP'
   -- THEN
   -- BEGIN
   --     loan.PoolPrtpntInfoCsp(p_LoanAppNmb,v_SBAPrtcptnAmt,v_OrgntrPrtcptnAmt,
   --                            v_TtlOutstndngBal);
   -- END;
   -- END IF;
   -- IF v_PrcsMthdCd = 'SMP' and v_SBAPrtcptnAmt > (v_TtlOutstndngBal*0.8)
   -- THEN
   --     p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,926,p_TransInd);
   -- END IF;
   -- IF v_PrcsMthdCd = 'SMP' and v_OrgntrPrtcptnAmt < (v_TtlOutstndngBal*0.05)
   -- THEN
   --     p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,927,p_TransInd);
   -- END IF;
   /*
   Validation to check Current primary borrower or any of the new loan participants (co-borr/prin/guar)
   are affiliates (all participant types guar/borr and prin) in an
   SLA (existing parameter = PrcsMthdReqLqdCr) loan that was created within 90 days (new parameter)
   that is in Status (PS)� and the new loan is for a processing method that is in the �Processing Method Bared�
   RSURAPA -- 06/03/2013 new validation
   NK-08/10/2016-- OPSMDEV 1018 removed LoanCntlIntInd = 'Y' from LOANAPP.LOANGUARTBL and replaced LOANAFFILIND with LoanCntlIntInd
   */
   BEGIN
      v_IMPrmtrId                := 'LqdCrPrivPrcsMthdBarred';
      v_IMPrmtrValChk            :=
         LOANAPP.GET_IMPRMTRVAL (
                                 v_IMPrmtrId,
                                 v_PrcsMthdCd,
                                 p_LoanAppRecvDt,
                                 v_RecovInd
                                );
      v_LqdCrPrivPrcsMthdBarred  := v_IMPrmtrValChk;

      IF (v_LqdCrPrivPrcsMthdBarred IS NOT NULL)
      THEN
         v_IMPrmtrId          := 'PrcsMthdReqLqdCr';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_PrcsMthdReqLqdCr   := v_IMPrmtrValChk;

         v_IMPrmtrId          := 'LqdCrPrivLoanDays';
         v_IMPrmtrValChk      :=
            LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_RecovInd
                                   );
         v_LqdCrPrivLoanDays  := TO_NUMBER (v_IMPrmtrValChk);

         v_taxid              := NULL;

         IF     NVL (v_PrcsMthdReqLqdCr, '999999999') != '999999999'
            AND v_LoanAppRqstAmt > TO_NUMBER (v_PrcsMthdReqLqdCr)
            AND v_LoanAppRcnsdrtnInd <> 'Y'
         THEN
            BEGIN
               SELECT taxid
               INTO v_taxid
               FROM (SELECT taxid
                     FROM LOANAPP.LOANBORRTBL
                     WHERE     loanappnmb = p_LoanAppNmb
                           AND LoanCntlIntInd = 'Y'
                     UNION
                     SELECT taxid
                     FROM LOANAPP.LOANGUARTBL
                     WHERE loanappnmb = p_LoanAppNmb
                     -- AND LoanCntlIntInd = 'Y'
                     UNION
                     SELECT p.taxid
                     FROM loanapp.loanprintbl     p
                          JOIN LOANAPP.LOANBUSPRINTBL bp
                             ON (    p.loanappnmb = bp.loanappnmb
                                 AND p.prinseqnmb = bp.prinseqnmb)
                     WHERE     p.loanappnmb = p_LoanAppNmb
                           AND bp.LoanCntlIntInd = 'Y') t
               WHERE     taxid IN
                            ( (SELECT b.taxid
                               FROM LOANAPP.LOANAPPTBL    a
                                    JOIN LOANAPP.LOANBORRTBL b ON (a.loanappnmb = b.loanappnmb)
                               WHERE     a.statcd = 'PS'
                                     AND b.LoanCntlIntInd = 'Y'
                                     AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays)
                               --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                               UNION
                               SELECT g.taxid
                               FROM LOANAPP.LOANAPPTBL    a
                                    JOIN LOANAPP.LOANGUARTBL g ON (a.loanappnmb = g.loanappnmb)
                               WHERE     a.statcd = 'PS'
                                     --AND g.LoanCntlIntInd = 'Y'
                                     AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays)
                               --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                               UNION
                               SELECT p.taxid
                               FROM LOANAPP.LOANAPPTBL    a
                                    JOIN LOANAPP.LOANPRINTBL p ON (a.loanappnmb = p.loanappnmb)
                                    JOIN LOANAPP.LOANBUSPRINTBL bp
                                       ON (    p.loanappnmb = bp.loanappnmb
                                           AND p.prinseqnmb = bp.prinseqnmb)
                               WHERE     a.statcd = 'PS'
                                     AND bp.LoanCntlIntInd = 'Y'
                                     AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays) --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                                                                                           ))
                     AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_taxid  := NULL;
            END;
         END IF;

         IF (v_taxid IS NOT NULL)
         THEN
            v_IMPrmtrId         := 'CrdScrThrshld';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_CrdScrThrshld     := v_IMPrmtrValChk;

            v_IMPrmtrId         := 'CrdScrThrshldMax';
            v_IMPrmtrValChk      :=
               LOANAPP.GET_IMPRMTRVAL (
                                       v_IMPrmtrId,
                                       v_PrcsMthdCd,
                                       p_LoanAppRecvDt,
                                       v_RecovInd
                                      );
            v_CrdScrThrshldMax  := TO_NUMBER (v_IMPrmtrValChk);

            p_ErrSeqNmb         :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          1053,
                          p_TransInd,
                          TO_CHAR (v_taxid),
                          TO_CHAR (v_LqdCrPrivLoanDays),
                          TO_CHAR (v_CrdScrThrshld),
                          TO_CHAR (v_CrdScrThrshldMax),
                          NULL,
                          v_FININSTRMNTTYPIND
                         );
         END IF;
      END IF;
   END;

   ---Validation for mandatory Maturity Starts Indicator

   IF     (v_LOANGNTYMATSTIND IS NULL)
      AND (   (    v_PrgrmCd = 'A'
               AND v_undrwritngby = 'SBA')
           OR v_PrgrmCd = 'E')
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    3330,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   ----Validation for mandatory Note date
   IF     (v_PrgrmCd <> 'C')
      AND (v_LOANGNTYMATSTIND = 'N')
      AND (v_LOANGNTYNOTEDT IS NULL)
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1054,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF     v_PrgrmCd = 'E'
      AND v_LOANGNTYMATSTIND != 'F'
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4048,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;

   IF     v_PRCSMTHDCD = '504'
      AND v_PrgrmCd = 'E'
   THEN
      BEGIN
         SELECT SUM (LoanPartLendrAmt)
         INTO v_ThirdPartyLoanAmt
         FROM loanApp.LoanPartLendrTbl p
         WHERE p.loanappnmb = p_loanappnmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_ThirdPartyLoanAmt  := NULL;
      END;

      v_TotProjFinAmt      :=
         NVL (v_ThirdPartyLoanAmt, 0) + NVL (v_LoanAppCDCNetDbentrAmt, 0) + NVL (v_LoanAppContribAmt, 0);

      BEGIN
         SELECT IMPrmtrValTxt
         INTO v_IMPrmtrValTxt
         FROM LOANAPP.IMPRMTRVALTBL
         WHERE IMPrmtrSeqNmb = (SELECT IMPrmtrSeqNmb
                                FROM LOANAPP.IMPrmtrTbl
                                WHERE IMPrmtrId = 'SBAContrbPrcnt');

         --  v_BorrContAmt := (v_LOANINJCTNAMT/v_LOANAPPRQSTAMT)*100;
         --  v_BorrContAmt := (v_LoanAppCDCNetDbentrAmt/v_LOANAPPRQSTAMT)*100;

         v_SBAContAmt  := LOANAPP.loanamtprcnt (v_LoanAppCDCNetDbentrAmt, v_TotProjFinAmt);

         IF v_SBAContAmt > v_IMPrmtrValTxt
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          4040,
                          p_TransInd,
                          p_LoanAppNmb,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_fininstrmnttypind
                         );
         END IF;
      END;

      BEGIN
         SELECT IMPrmtrValTxt
         INTO v_IMPrmtrValTxt
         FROM LOANAPP.IMPRMTRVALTBL
         WHERE IMPrmtrSeqNmb = (SELECT IMPrmtrSeqNmb
                                FROM LOANAPP.IMPrmtrTbl
                                WHERE IMPrmtrId = 'BorrContrbPrcnt');

         v_BorrContAmt  := LOANAPP.loanamtprcnt (v_LoanAppContribAmt, v_TotProjFinAmt);

         IF v_BorrContAmt < v_IMPrmtrValTxt
         THEN
            p_ErrSeqNmb      :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          4039,
                          p_TransInd,
                          p_LoanAppNmb,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          v_fininstrmnttypind
                         );
         END IF;
      END;
   END IF;
   
   IF     v_PRCSMTHDCD = '5RX'
   AND v_PrgrmCd = 'E'
   THEN
         IF v_RecovInd = 'R'
       THEN
        
             SELECT COUNT (*)
             INTO v_ChkVal
             FROM IMPrmtrTbl a, IMPrmtrValTbl b
             WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
                   AND a.IMPrmtrId = 'NAICSCdNonARRA'
                   AND TO_NUMBER (IMPrmtrValTxt) = v_NAICSCd
                   AND (REGEXP_LIKE (to_char(v_NAICSCd),'^112|^311|^327|^335|^337|^443|^444|^451|^484|^485|^493|^622|^624|^711|^713|^721|^722'))
                   AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                   AND (   IMPrmtrValEndDt IS NULL
                        OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

             IF v_ChkVal > 0
             THEN
                p_ErrSeqNmb      :=
                   ADD_ERROR (
                              p_LoanAppNmb,
                              p_ErrSeqNmb,
                              4294,
                              p_TransInd,
                              TO_CHAR (v_NAICSCd),
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              v_FININSTRMNTTYPIND
                             );
             END IF;
       END IF;
       
          IF v_LoanAppSBAGntyPct * v_LoanAppRqstAmt * .01 > 500000
   THEN
      p_ErrSeqNmb      :=
         ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4293,
                    p_TransInd,
                    '500000',
                    TO_CHAR (v_LoanAppRqstAmt),
                    TO_CHAR (v_LoanAppSBAGntyPct),
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND
                   );
   END IF;
   END IF;

   /* BEGIN
       SELECT LoanAppInjctnInd  INTO v_LoanAppInjctnInd FROM LoanApp.LoanAppTbl
        WHERE LoanAppNmb = p_LoanAppNmb;

       IF v_LoanAppInjctnInd = 'N' THEN
                 p_ErrSeqNmb := ADD_ERROR(p_LoanAppNmb, p_ErrSeqNmb, 4046, p_TransInd,
                 p_LoanAppNmb,NULL,NULL,NULL,NULL,v_fininstrmnttypind);
       END IF;
    END; */

   p_ErrSeqNmb               := NVL (p_ErrSeqNmb, 0);
   p_RetVal                  := NVL (p_RetVal, 0);
END;
/

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

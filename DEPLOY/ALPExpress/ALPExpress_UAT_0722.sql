define deploy_name=ALPExpress
define package_name=UAT_0722
define package_buildtime=20210723135452
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy ALPExpress_UAT_0722 created on Fri 07/23/2021 13:54:53.02 by Jasleen Gorowada
prompt deploy scripts for deploy ALPExpress_UAT_0722 created on Fri 07/23/2021 13:54:53.02 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy ALPExpress_UAT_0722: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_LOANVALIDATIONERRTBL_0722.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_VALIDATIONEXCPTNTBL_0722.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATEAPPCSP.sql 
Jasleen Gorowada committed fe7611b on Fri May 28 16:16:27 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_LOANVALIDATIONERRTBL_0722.sql"
delete from LOANAPP.LOANVALIDATIONERRTBL where ERRCD=4293;
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '1', 'ALP Express delegated authority may not be used to process 504 loans previously screened out or declined by SLPC or decline by another CDC.', 'CNTRJGOROWADA', '07-JUL-21', 
    'CNTRJGOROWADA', '07-JUL-21', 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '2', 'ALP Express delegated authority may not be used to process 504 loans previously screened out or declined by SLPC or decline by another CDC.', 'CNTRJGOROWADA', '07-JUL-21', 
    'CNTRJGOROWADA', '07-JUL-21', '');
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\insert_VALIDATIONEXCPTNTBL_0722.sql"
Insert into LOANAPP.VALIDATIONEXCPTNTBL
   (EXCPTNSEQNMB, ERRCD, SYSTYPCD, PRCSMTHDCD, FININSTRMNTTYPIND, 
    ERRTYPIND, CREATUSERID, CREATDT, EXCPTNSTRTDT, EXCPTNENDDT, 
    LASTUPDTUSERID, LASTUPDTDT, OVERRIDECD)
 Values
   ('589', '4293', '', '5EX', '', 
    'W', 'CNTRJGOROWADA', '21-JUL-21', '21-JUL-21', '', 
    'CNTRJGOROWADA', '21-JUL-21', '');
COMMIT;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\VALIDATEAPPCSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATEAPPCSP (p_RetVal             OUT NUMBER,
                                                    p_LoanAppNmb             NUMBER,
                                                    p_ErrSeqNmb       IN OUT NUMBER,
                                                    p_TransInd               NUMBER,
                                                    p_LoanAppRecvDt          DATE) AS
    /* ********************************************************************************
        Created by:
        Created on:
        Purpose:
        Parameters:
         Modified by: APK - 08/02/2010 Issue:Is Payment Fully Amortizing default - Item 130
         Modified on: 08/16/2010 - APK - new validation in loanapp.ValidateApp stored procedure
                         to make LoanAppDisasterCntrlNmb mandatory for PrcsMthdCd EQ 'DGB'
                      APK --09/21/2010-- Modified for the error code 129, included the PrcsMthdCd and Exception logic.
                      APK -- 09/27/2010-- modified parameter MatMoConsMax (checking) code to be identical as in Loan.
                      09/28/2010-- APK-- included the FININSTRMNTTYPIND change for PrgrmValidTbl and parameter tables
         Modified By: 09/29/2010-- APK--Error 106 - This needs to be called for all loan
                                    programs.Remove the if logic for Program Cd = A both when checking the two
                                    parameters (LoanRqstAmtDltaMax and  LoanRqstAmtMax) and also setting the
                                    error code.
         Modified By: 09/29/2010 -- APK-- Error 118 - This needs to be called for all loan
                                     programs. There is already code not to call if the special purpose is not
                                     REVL or CONS, that is sufficient. Remove the if logic for Program Cd = A
                                     both when checking the two parameters(MatMoRevlMax and MatMoConsMax) and
                                     also setting the error code.
         Modified By: Error 109 - This needs to be called for all loan programs. Remove the if
                                    logic for Program Cd = A both when checking the two parameters
                                    (GntyPct7LrgAmtMax and  GntyPctLowAmtMax) and also setting the error code.
         Modified By: 09/29/2010 -- APK-- Error 110 - This needs to be called for all loan
                                    programs. Remove the if logic for Program Cd = A both when checking the
                                    Eparameter (GntyAmtMax) and also setting the error code.Rewrite the code
                                    for 110, take out all the references to LDP and PLP and remove the check
                                    against the parameter GntyAmt7NExpwExptConsRevlMax.
         Modified By: 10/05/2010 -- APK-- Modified the procedure to use the common function
                                    GET_IMPRMTRVAL to reduce code redundancy and also for proper formatting
         Modified By: 11/23/2010 -- APK --Modified to get the logic for error code 101, to use adjustment
              factor into consideration, from validateprojcsp and added the same code to here. Also modified 106 to
               combine with 101 using adjustment factors.And 101 and 106 now apply for all programs.
         Modified by: 02/03/2011-- Modified code for the (Eligibilty Indicator), as the Indicator locumn is removed from the LoanAppTbl.
                      02/03/2011-- Modified code for the errors 234 and 247 to validate for
                      all processing methods. previously they were validated for Only ARC's.
         Modified: APK - 02/04/2011- modified to revert back the code for error codes 156 and 240, for Eligibility.
         Modified: APK - 02/08/2011- modified to revert back the code for error codes 234 and 247, for Arc Reasons.
         Modified: RGG - 02/14/2011- Commented the code for error codes 156 and 240, for Eligibility.
         Modified: APK - 03/01/2011- modified condition for error 118 to be called for all processing methods not only for SMP.
                   APK -- 01/03/2012-- modified code for error code 127,244,151 so to skip for PrgrmCd = H
                   APK -- 01/31/2012 -- modified to add additional code block for handling 'SBX' group aggregate amount.
                   APK -- 02/03/2012 -- modified to add two validations, 1027 and 1028, that apply only to
                   Program Code = 'H'.Disaster Control Number is Required and Disaster Application Number is Required.
                   APK -- 02/06/2012 -- modified Validation 1028, to use LOANDISASTRAPPNMB, as the
                   column LOANDISASTRAPPNMB was added to the loanapptbl in development which was present in test and prod but not in dev.
                   APK -- 02/06/2012 -- modified to add new validation 1029 to 1031, as part of Line of credit question.
                              If loan type doesn�t allow the REVL special purpose, LoanAppRevlInd must be null.
                              If loan type does allow the REVL special purpose:
                              If the loan application has the REVL special purpose, LoanAppRevlInd must be �Y�.
                              If the loan application doesn�t have the REVL special purpose, LoanAppRevlInd must be �N�.
                   APK -- 02/08/2012 -- modified to validations 1029 to 1031 to make some minor adjustments,
                   to use LoanAppRevlInd in the conditions which was missing.
                   APK -- 05/25/2012 -- Modified Aggregate chages as per Ron's new requirement.
                   APK -- 07/10/2012 -- Modified to add new validation 1044 for LoanApp Payment amount not be greaterthan or equal to 1 million.
                   RGG -- 10/31/2012 -- Added new validation to check initial interest rate to be geater than or equal zero.
                   SP  -- 12/10/2012 -- modified code for error 174 as underwritingby depends not only on process method but also on agreement.
                   SP  -- 04/04/2013 -- Modified so as to get underwritinby from loanapptbl
                   RSURAPA -- 06/03/2013 -- Added a new validation to check if a current primary borrower or any of the new loan participants (co-borr/prin/guar)
                                are affiliates (all participant types guar/borr and prin) in
                                an SLA (existing parameter = PrcsMthdReqLqdCr) loan that was created within 90 days (new parameter = LqdCrPrivLoanDays )
                                that is in Status (PS)� and the new loan is for a processing method that is in the parameter (new parameter = LqdCrPrivPrcsMthdBarred ).
                   SP  -- 11/21/2013 -- Modified to get interest values from LoanIntDtlTbl instead of LoanAppTbl
                   SP  -- 12/23/2013 -- Added validations 1.to check LoanAppIntDtlCd against code table and mandatory
                   SP  -- 01/14/2014 -- Moved validation checks for interest details to ValidateIntDtlCsp for errors:100,114,119,124,129,146,155,159,171,284,1049,238,243,120
                   SP  -- 02/03/2014 -- Modified  1053 to throw error if reconsideration indicator is not set to Y
                   SP  -- 03/05/2014 -- Modified  error 136 to remove check for :reconsideration indicator is null then the loan type cannot be `D�
                   SP  -- 04/09/2014 -- Modified to move validations 166,244,151,127 to VALIDATEAPPPROJCSP
                   PSP -- 08/04/2014 -- Modified v_FININSTRMNTTYPIND := E for prgrmcd = E
                   SB -- 09/22/2014 -- Added FININSTRMNTTYPIND to ADD_ERROR function call.
                   SB -- 10/06/2014 -- Added validation to Recondsideration Indicator and Underwriting By
                   PSP -- 10/15/2014 -- Modified to add prgmcd =R to prgmcd =A validations as ARC loans are actually 7a loans with a program code of "R".
                   RGG -- made a change to error 1029 to allow N for RevolvingInd
                   NK  --08/05/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd from
                   RY-- 12/12/16 OPSMDEV--1294 -- Added a new validation for Err Cd 3330 and added v_LOANGNTYMATSTIND,v_LOANGNTYNOTEDT
                   NK-- 02/23/2017 -- Added Additional Conditions validation for 504 loans(errcd 3330)
                   NK-- 03/16/2017 -- OPSMDEV 1402 Added Error Code 4039 and 4040
                   NK--03/28/2017 -- OPSMDEV 1385  Updated errcd 101
                   RY--04/04/2017 --OPSMDEV 628 --Restricted errorcd 101 only for 504 loans
                   SS--04/25/2017--OPSMDEV 1422--Added ERRCD 4048 All 504 loans maturity starts at first disbursement
                   SS--04/27/2017--OPSMDEV 1422 --Added ERRCD 4049 All 504 loans require collateral
                   SS--05/02/2017--OPSMDEV 1422-- Added Errcd 4050 For all 504 loans Balance to borow should not exceed $1000.
                   NK--05/09/2017--CAFSOPER-- Updated Errcd 4039 and 4040
                   SS--05/29/2017--OPSMDEV 1436 Added errcd 4071 for CDCServFeePct
                   NK--08/21/2017--CAFSFOPER 805-- Updated error code 4050
                   JP--09/13/2017--CAFSFOPER 805 added  v_LoanAppBalToBorrAmt as a parameter for ADD_ERROR function call
                          when v_prgrmcd = 'E' AND v_LoanAppBalToBorrAmt > 1000
                   NK--09/26/2017--OPSMDEV 1523-- updtaed the errcd 157 to validate for 25year debenture along with 10 and 20 years.
                   SS--09/30/2017-- OPSMDEV 1528-- Changed the errcd 4039 and 4040  for new business and special purpose property
                   SS--10/18/2017--OPSMDEV 1548--Added errcd 4104 for prcsmthcd SBL
                   RY--01/05/2018--OPSMDEV-1584--Added new error codes for 4112,4113
                   JP -- 01/12/2018 (modified 101 error generation. No 101 error if E01 or E03 provided)
                   NK--01/17/2018 --OPSMDEV 1644 Added Error code 4126,4127,4128,4129
                   BR--07/26/2018--CAFSOPER1967--added exception on Error Code 2004  for Financial Instrument C (commitments) and D (drawdowns).
                   MZ--08/02/2018--Added new validation to block non-delegated loan from coming back as delegated loan
                   BR--09/18/2018-CAFSOPER2070- changed 'AO' to 'AR' for error code 4184 validation and added IF chkval = 1 to validation.
                   SS-10/30/2018--OPSMDEV 1965-- Added errcd 4185 for extraordinary servicing fee.
                   JP - 06/06/2019 - CAFSOPER-2773 commented out v_LoanAppJobRqmtMetInd = 'Y'
                                     on checking condition for v_IMPrmtrId = "Loan5EconEngRqstAmtMax"
                   JP - 06/28/2019 - CAFSOPER-2894  Error 2353 condition rewritten
                   MZ - 10/22/2019 - OPSMDEV-2299, validate microloan aggregate

                   MZ--11/6/2019   OPSMDEV 2298 Added err 249 75000 max amount for first year
                   MZ--11/6/2019 OPSMDEV 2306 25000max amount after the first year
                   SS--11/15/2019 OPSMDEV2305 Added 4203
                   MZ--03/15/2020 OPSMDEV 2416, validate naics code for export processing method
                   JP--04/02/2020 CARESACT_35, added error code 4210
                   MZ--04/23/2020 Remove PPP from aggregate
                   SS--5/21/2020 Added errcd4216 CARESACT 318
                   MZ--12/29/2020 Guaranty percent for direct loan is 100%
                   JP -- 1/7/2021 - SODSTORY-383 added error code 432
                   SL -- 1/7/2021 - SODSTORY-360/361. Added error code 4266 for LOANAPPUSRESIND
                   SL -- 1/7/2021 - SODSTORY-367 changed hard coded PPP to include 'PPP,PPS'
                   JP -- 1/19/2021 --  SODSTORY-383 modified validation for 4211
                   JP -- 09/02/2021 SODSTORY-475  included v_PrcsMthdCd for errroc code 432
                   SL -- 02/25/2021 SODSTORY-445  Added logic for error 101 for v_PrgrmCd A (used to only for ProgrmCd E)
                   SL -- 3/3/2021 - SODSTORY-571. Added error code 4275 and 4278 for LOANAPPSCHEDCIND
                                                  Added error code 4276 for LOANAPPSCHEDCYR
                                                  Added error code 4277 for LOANAPPGROSSINCOMEAMT
                                                  Modified error code 4210 and 4211 to support LOANAPPGROSSINCOMEAMT
                   SL -- 04/03/2021 - OPSMDEV-2564 Added new v_PrgrmCd D for v_FININSTRMNTTYPIND G for RRF loans
                   SL -- 04/09/2021 - OPSMDEV-2564 Added RRF loans to error 237
                   SL -- 04/13/2021 - OPSMDEV-2564 ADDED error 4284 for RRF
                   MZ -- 07/01/2021 - Check Hold Status for DCI loan
                   JG -- 07/20/2021 - NAICS Code Validation for 5EX
				   JG -- 07/21/2021 - Warning message for 5EX
       *********************************************************************************/
    v_MaxLoanExpo                   NUMBER;
    v_LoanSumAmt                    NUMBER;
    v_MaxLoanSumAmt                 NUMBER;
    v_TaxId                         CHAR (10);
    v_MaxGntyPct                    NUMBER (6, 3);
    v_PrgrmCd                       CHAR (1);
    v_PrcsMthdCd                    CHAR (3);
    v_NAICSYrNmb                    NUMBER;
    v_NAICSCd                       NUMBER;
    v_LoanAppNm                     VARCHAR2 (80);
    v_LoanAppNewBusCd               CHAR (1);
    v_LoanAppRqstAmt                NUMBER;
    v_LoanAppSBAGntyPct             NUMBER (6, 3);
    v_LoanAppRqstMatMoQty           NUMBER;
    v_LoanAppFullAmortPymtInd       CHAR (1);
    v_LoanAppPymtAmt                NUMBER;
    v_LoanAppMoIntQty               NUMBER;
    v_LoanAppLifInsurRqmtInd        CHAR (1);
    v_LoanAppInjctnInd              CHAR (1);
    v_LoanAppRcnsdrtnInd            CHAR (1);
    v_LoanAppEligEvalInd            CHAR (1);
    v_LoanAppEWCPSnglTransPostInd   CHAR (1);
    v_LoanAppEWCPSnglTransInd       CHAR (1);
    v_SpcPurpsLoanCd                CHAR (4);
    v_DLTASpcPurpsInd               CHAR (1);
    v_LoanAppEPCInd                 CHAR (1);
    v_LoanAppSBARcmndAmt            NUMBER;
    v_MaxLoanAppRqstAmt             NUMBER;
    v_GntyPctRqstAmtLrgLimit        NUMBER;
    v_GntyPct5Max                   NUMBER (6, 3);
    v_MatMoMax                      NUMBER;
    v_MatMoMin                      NUMBER;
    v_LoanCollatInd                 CHAR (1);
    v_GntyPctMin                    NUMBER (6, 3);
    v_PersonLoanInd                 CHAR (1);
    v_VetPctOwnrshpSum              NUMBER;
    v_PctOwnrshpVetSumMin           NUMBER;
    v_RecovInd                      CHAR (1);
    v_LoanTypInd                    CHAR (1);
    v_LoanAppMicroLendrId           CHAR (10);
    v_CrdScrThrshld                 CHAR (3);
    v_CrdScrThrshldMax              CHAR (3);
    v_LoanAmtMin                    NUMBER;
    v_LoanAppCDCSeprtPrcsFeeInd     CHAR (1);
    v_LoanAppCDCNetDbentrAmt        NUMBER;
    v_LoanAppCDCGntyAmt             NUMBER;
    v_LoanAppCDCFundFeeAmt          NUMBER;
    v_LoanAppCDCPrcsFeeAmt          NUMBER;
    v_LoanAppCDCClsCostAmt          NUMBER;
    v_LoanAppCDCUndrwtrFeeAmt       NUMBER;
    v_LoanAppContribPct             NUMBER (5, 2);
    v_LoanAppContribAmt             NUMBER (15, 2);
    v_InjctnSum                     NUMBER (15, 2);
    v_ChkVal                        NUMBER;
    v_FY                            NUMBER (5, 0);
    v_LoanAppRecvDt                 DATE;
    v_LoanAmt                       NUMBER;
    v_CohortCdCnt                   NUMBER;
    v_LOANAPPDISASTERCNTRLNMB       VARCHAR2 (20);
    v_LOANDISASTRAPPNMB             CHAR (30);
    v_FININSTRMNTTYPIND             CHAR (1);
    v_IMPrmtrValChk                 VARCHAR (255);
    v_IMPrmtrId                     VARCHAR2 (255);
    v_LoanAppJobRqmtMetInd          CHAR (1);
    v_ChkVal1                       NUMBER;
    v_AggregatePrcsGroup            CHAR (1);
    v_LoanAppRevlInd                CHAR (1);
    v_undrwritngby                  CHAR (4);
    v_PrcsMthdReqLqdCr              VARCHAR2 (9);
    v_LqdCrPrivLoanDays             NUMBER;
    v_LqdCrPrivPrcsMthdBarred       VARCHAR2 (5);
    v_isSBAGntydLoan                VARCHAR2 (1);
    v_IMRTTYPCD                     CHAR (3);
    v_LOANAPPINTDTLCD               VARCHAR2 (2);
    v_LOANAMTLIMEXMPTIND            CHAR (1);
    v_LocId                         NUMBER;
    v_PrtId                         NUMBER;
    v_TotalPrevFyAppvAmt            NUMBER;
    v_TotalCurrFy5REAmt             NUMBER;
    v_LOANGNTYMATSTIND              CHAR (1);                                                       ---added on 12/09/16
    v_LOANGNTYNOTEDT                DATE;                                                            --added on 12/12/16
    v_LOANPYMNINSTLMNTFREQCD        CHAR (1);
    v_IMPrmtrValTxt                 VARCHAR (80);
    v_TotProjFinAmt                 NUMBER;
    v_ThirdPartyLoanAmt             NUMBER;
    v_CDCGntyFeeMulti               NUMBER (7, 6);
    v_CDCFundFeeMulti               NUMBER (7, 6);
    v_CDCPrcsFeeMulti               NUMBER (7, 6);
    v_CDCServFeePctMax              NUMBER (6, 3);
    v_CDCServFeePctMin              NUMBER (6, 3);
    v_CDCServFeePct                 NUMBER (6, 3);
    v_LoanAppBalToBorrAmt           NUMBER (19, 4);
    v_publDt                        DATE;
    v_IsNewBus                      VARCHAR2 (1);
    v_IsSpcPurpNAICSCD              VARCHAR2 (1);
    v_Text1                         VARCHAR2 (80);
    v_BorrContPct                   NUMBER;
    v_ReqBorrContPct                NUMBER;
    v_ReqBorrContAmt                NUMBER;
    v_SBAContPct                    NUMBER;
    v_ReqSBAContPct                 NUMBER;
    v_ReqSBAContAmt                 NUMBER;
    v_MatMoRevlMax                  NUMBER;
    v_CrdScrInd                     CHAR (1);
    v_LQDCRTOTSCR                   NUMBER;
    v_projcost                      NUMBER;
    v_LoanAppRevlMoIntQty           NUMBER;
    v_LoanAppAmortMoIntQty          NUMBER;
    v_Imrttypdesctxt                VARCHAR2 (150);
    v_Text2                         VARCHAR2 (80);
    v_imrttypcd1                    CHAR (3);
    v_LoanPymtRepayInstlTypCd       CHAR (1);
    v_LoanAppExtraServFeeAMT        NUMBER;
    v_LoanAppExtraServFeeInd        CHAR (1);
    v_LoanEconDevObjctCd            VARCHAR2 (3);
    v_ChkEconDevGoalPPG             NUMBER;
    v_ChkEconDevEnergy              NUMBER;
    v_MLDFirstFundDt                DATE;
    v_LoanAppFundDt                 DATE;
    v_FyLoanAmtLmt                  NUMBER;
    v_FyLoanAmtSum                  NUMBER;
    v_LoanAppMonthPayroll           NUMBER;
    v_LoanRefinEIDLAmt              NUMBER;
    v_TaxId_Switch                  CHAR (10);
    v_PPPLoanNmb                    CHAR (10);
    v_LoanAppCurrEmpQty             NUMBER;
    v_LoanAmtMaxCurrEmp             NUMBER;
    v_LOANAPPUSRESIND               CHAR (1);
    v_LOANAPPSCHEDCIND              CHAR (1);
    v_LOANAPPSCHEDCYR               NUMBER;
    v_LOANAPPGROSSINCOMEAMT         NUMBER;
    v_DSPSTNSTATCD                  NUMBER;
    v_ErrText1                      VARCHAR2 (80);
    v_ErrText3                      VARCHAR2 (80);
    v_BUSTYPCD                      VARCHAR2 (5);
    
    -- List out NAICS code to not use
    CURSOR NAICSCDRNGTBLCUR
    IS
    SELECT * FROM SBAREF.NAICSCDRNGTBL;
    
    NAICSCDRNGTBLCUR_rec SBAREF.NAICSCDRNGTBL%rowtype;
    
BEGIN
    BEGIN
        SELECT PrgmCd,
               PrcsMthdCd,
               LoanAppNm,
               LoanAppNewBusCd,
               LoanAppRqstAmt,
               LoanAppSBAGntyPct,
               LoanAppRqstMatMoQty,
               LoanAppLifInsurRqmtInd,
               LoanAppPymtAmt,
               LoanAppMoIntQty,
               LoanAppInjctnInd,
               LoanAppRcnsdrtnInd,
               LoanAppEligEvalInd,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanAppFullAmortPymtInd,
               LoanAppEPCInd,
               LoanAppSBARcmndAmt,
               LoanCollatInd,
               RecovInd,
               NVL (
                   (SELECT LoanTypInd
                    FROM sbaref.PrcsMthdTbl z
                    WHERE z.PrcsMthdCd = a.PrcsMthdCd),
                   ' '),
               LoanAppMicroLendrId,
               LoanAppCDCGntyAmt,
               LoanAppCDCFundFeeAmt,
               LoanAppCDCPrcsFeeAmt,
               LoanAppCDCClsCostAmt,
               LoanAppCDCUndrwtrFeeAmt,
               LoanAppCDCNetDbentrAmt,
               LoanAppCDCSeprtPrcsFeeInd,
               LoanAppContribPct,
               LoanAppContribAmt,
               LOANAPPDISASTERCNTRLNMB,
               LOANDISASTRAPPNMB,
               LoanAppRevlInd,
               undrwritngby,
               LOANAPPINTDTLCD,
               LOANAMTLIMEXMPTIND,
               LOANGNTYMATSTIND,
               LOANGNTYNOTEDT,
               LOANPYMNINSTLMNTFREQCD,
               CDCServFeePct,
               LoanAppBalToBorrAmt,
               LoanAppRevlMoIntQty,
               LoanAppAmortMoIntQty,
               LoanPymtRepayInstlTypCd,
               LoanAppExtraServFeeAMT,
               LoanAppExtraServFeeInd,
               LoanAppMonthPayroll,
               LOANAPPUSRESIND,
               DSPSTNSTATCD,
               LOANAPPSCHEDCIND,
               LOANAPPSCHEDCYR,
               LOANAPPGROSSINCOMEAMT
        INTO v_PrgrmCd,
             v_PrcsMthdCd,
             v_LoanAppNm,
             v_LoanAppNewBusCd,
             v_LoanAppRqstAmt,
             v_LoanAppSBAGntyPct,
             v_LoanAppRqstMatMoQty,
             v_LoanAppLifInsurRqmtInd,
             v_LoanAppPymtAmt,
             v_LoanAppMoIntQty,
             v_LoanAppInjctnInd,
             v_LoanAppRcnsdrtnInd,
             v_LoanAppEligEvalInd,
             v_LoanAppEWCPSnglTransPostInd,
             v_LoanAppEWCPSnglTransInd,
             v_LoanAppFullAmortPymtInd,
             v_LoanAppEPCInd,
             v_LoanAppSBARcmndAmt,
             v_LoanCollatInd,
             v_RecovInd,
             v_LoanTypInd,
             v_LoanAppMicroLendrId,
             v_LoanAppCDCGntyAmt,
             v_LoanAppCDCFundFeeAmt,
             v_LoanAppCDCPrcsFeeAmt,
             v_LoanAppCDCClsCostAmt,
             v_LoanAppCDCUndrwtrFeeAmt,
             v_LoanAppCDCNetDbentrAmt,
             v_LoanAppCDCSeprtPrcsFeeInd,
             v_LoanAppContribPct,
             v_LoanAppContribAmt,
             v_LOANAPPDISASTERCNTRLNMB,
             v_LOANDISASTRAPPNMB,
             v_LoanAppRevlInd,
             v_undrwritngby,
             v_LOANAPPINTDTLCD,
             v_LOANAMTLIMEXMPTIND,
             v_LOANGNTYMATSTIND,
             v_LOANGNTYNOTEDT,
             v_LOANPYMNINSTLMNTFREQCD,
             v_CDCServFeePct,
             v_LoanAppBalToBorrAmt,
             v_LoanAppRevlMoIntQty,
             v_LoanAppAmortMoIntQty,
             v_LoanPymtRepayInstlTypCd,
             v_LoanAppExtraServFeeAMT,
             v_LoanAppExtraServFeeInd,
             v_LoanAppMonthPayroll,
             v_LOANAPPUSRESIND,
             v_DSPSTNSTATCD,
             v_LOANAPPSCHEDCIND,
             v_LOANAPPSCHEDCYR,
             v_LOANAPPGROSSINCOMEAMT
        FROM LoanAppTbl a
        WHERE LoanAppNmb = p_LoanAppNmb;

        BEGIN
            SELECT LOCID
            INTO v_LOCID
            FROM LoanAppPrtTbl
            WHERE LoanAppNmb = p_LoanAppNmb;
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;

        BEGIN
            SELECT BUSTYPCD
            INTO v_BUSTYPCD
            FROM loanApp.LoanBusTbl a
            inner join loanapp.loanborrtbl b on a.LoanAppNmb = b.LoanAppNmb
            WHERE a.LoanAppNmb = p_LoanAppNmb
            AND a.taxid = b.taxid
            AND b.LOANBUSPRIMBORRIND = 'Y';
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;

        BEGIN
            SELECT imrttypcd
            INTO v_imrttypcd1
            FROM loanapp.loanintdtltbl
            WHERE LoanAppNmb = p_LoanAppNmb;
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;

        SELECT CASE v_PrgrmCd
                   WHEN 'E' THEN 'E'
                   WHEN 'D' THEN 'G'
                   ELSE 'L'
               END
        INTO v_FININSTRMNTTYPIND
        FROM DUAL;


        --check if the loan is sba guarnteed or not based on the process method -- RSURAPA - 07/19/2013
        SELECT DECODE (loantypind, 'G', 'Y', 'N')
        INTO v_isSBAGntydLoan
        FROM sbaref.PrcsMthdtbl
        WHERE prcsmthdcd = v_prcsmthdcd;

        BEGIN
            SELECT LoanAppJobRqmtMetInd,
                   NAICSYrNmb,
                   NAICSCd,
                   LoanAppCurrEmpQty,
                   PPPLoanNmb
            INTO v_LoanAppJobRqmtMetInd,
                 v_NAICSYrNmb,
                 v_NAICSCd,
                 v_LoanAppCurrEmpQty,
                 v_PPPLoanNmb
            FROM LoanAppProjTbl
            WHERE LoanAppNmb = p_LoanAppNmb;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_LoanAppJobRqmtMetInd := NULL;
                v_NAICSCd := NULL;
        END;


        ---APK 05/01/2010 Modified for multiple primary borrowers so that query below returns --only onerow
        SELECT TaxId
        INTO v_TaxId
        FROM loanapp.LoanBorrTbl a
        WHERE     a.LoanAppNmb = p_LoanAppNmb
              AND a.BORRSEQNMB = (SELECT MAX (z.BORRSEQNMB)
                                  FROM loanapp.LoanBorrTbl z
                                  WHERE     z.LOANAPPNMB = a.LOANAPPNMB
                                        AND z.LoanBusPrimBorrInd = 'Y');

        SELECT COUNT (*)
        INTO v_ChkVal
        FROM IMPrmtrTbl a, IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'PrcsMthdDPHSet'
              AND IMPrmtrValTxt = v_PrcsMthdCd
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

        v_PersonLoanInd :=
            CASE
                WHEN v_ChkVal > 0 THEN 'Y'
                ELSE 'N'
            END;

        SELECT COUNT (*)
        INTO v_ChkVal
        FROM sbaref.IMRtTypTbl
        WHERE     IMRtTypCd = v_IMRtTypCd
              AND p_LoanAppRecvDt >= IMRtTypStrtDt
              AND (   p_LoanAppRecvDt < IMRtTypEndDt + 1
                   OR IMRtTypEndDt IS NULL);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;

	--validate potential fraud
	BEGIN
		SELECT 1
		INTO v_ChkVal
		FROM DUAL
		WHERE EXISTS(SELECT 1 FROM loanapp.loanborrtbl b, loan.BUSPERHLDSTATTBL h
			where b.loanappnmb = p_loanappnmb
			and b.taxid = h.taxid
			and h.ENDDT is null)
		or EXISTS(SELECT 1 FROM loanapp.loanprintbl b, loan.BUSPERHLDSTATTBL h
			where b.loanappnmb = p_loanappnmb
			and b.taxid = h.taxid
			and h.ENDDT IS NULL)
		or EXISTS(SELECT 1 FROM loanapp.loanguartbl b, loan.BUSPERHLDSTATTBL h
			where b.loanappnmb = p_loanappnmb
			and b.taxid = h.taxid
			and h.ENDDT is null);
			
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                435,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
	exception when others then
		null;
	end;

    /*select 1 into v_IMRtTypCd
     from sbaref.IMRtTypTbl
     WHERE
    IMRtTypCd = v_IMRtTypCd;
     EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
           NULL;
     END;*/


    --Validates EWCP Single Transaction Indicator

    IF    (    v_PrcsMthdCd = 'PLW'
           AND v_LoanAppEWCPSnglTransInd IS NULL)
       OR (    v_LoanAppEWCPSnglTransInd IS NOT NULL
           AND v_LoanAppEWCPSnglTransInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                130,
                p_TransInd,
                v_LoanAppEWCPSnglTransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;


    --Validates PrcsMthdCd PPP and PPS has a  v_LoanAppMonthPayroll value
    IF (    v_PrcsMthdCd IN ('PPP', 'PPS')
        AND NVL (v_LoanAppMonthPayroll, 0) <= 0) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb => p_LoanAppNmb,
                p_ErrSeqNmb => p_ErrSeqNmb,
                p_ErrCd => 4210,
                p_ErrTyp => p_TransInd,
                p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
    END IF;

    IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
        BEGIN
            IF (   v_LOANAPPUSRESIND IS NULL
                OR v_LOANAPPUSRESIND NOT IN ('N', 'Y')) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 4266,
                        p_ErrTyp => p_TransInd,
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            END IF;


            /*
            IF (v_BUSTYPCD IN (1,16,17) AND
                (v_LOANAPPSCHEDCIND IS NULL
                 OR v_LOANAPPSCHEDCIND NOT IN ('N', 'Y'))) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 4275,
                        p_ErrTyp => p_TransInd,
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            ELS */
            IF (    v_BUSTYPCD NOT IN (1, 16, 17,26,27)
                AND NVL (v_LOANAPPSCHEDCIND, 'N') = 'Y') THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 4278,
                        p_ErrTyp => p_TransInd,
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            END IF;

            DBMS_OUTPUT.put_line ('v_BUSTYPCD: ' || v_BUSTYPCD);
            DBMS_OUTPUT.put_line ('v_LOANAPPSCHEDCIND: ' || v_LOANAPPSCHEDCIND);

            IF (    v_LOANAPPSCHEDCIND = 'Y'
                AND (   v_LOANAPPSCHEDCYR IS NULL
                     OR v_LOANAPPSCHEDCYR NOT IN (2019, 2020))) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 4276,
                        p_ErrTyp => p_TransInd,
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            END IF;

            IF (    v_LOANAPPSCHEDCIND = 'Y'
                AND v_LOANAPPGROSSINCOMEAMT IS NULL) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 4277,
                        p_ErrTyp => p_TransInd,
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            END IF;
        END;
    END IF;

    --Validates if requested amount > No. of employees * v_LoanAmtMaxCurrEmp
    IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
        BEGIN
            FOR rec IN (SELECT IMPRMTRVALTXT     LoanAmtMaxCurrEmp
                        FROM LOANAPP.imprmtrvaltbl v, LOANAPP.imprmtrtbl i
                        WHERE     v.IMPRMTRSEQNMB = i.IMPRMTRSEQNMB
                              AND i.IMPRMTRID = 'LoanAmtMaxCurrEmp'
                              AND v.PrcsMthdCd = v_PrcsMthdCd
                              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                              AND (   IMPrmtrValEndDt IS NULL
                                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1))
            LOOP
                v_LoanAmtMaxCurrEmp := rec.LoanAmtMaxCurrEmp;
            END LOOP;

            IF NVL (v_LoanAppRqstAmt, 0) > NVL (v_LoanAmtMaxCurrEmp, 0) * NVL (v_LoanAppCurrEmpQty, 0) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb => p_LoanAppNmb,
                        p_ErrSeqNmb => p_ErrSeqNmb,
                        p_ErrCd => 432,
                        p_ErrTyp => p_TransInd,
                        p_Stuff1 => TO_CHAR (NVL (v_LoanAmtMaxCurrEmp, 0) * NVL (v_LoanAppCurrEmpQty, 0)),
                        p_Stuff2 => TO_CHAR (v_LoanAppCurrEmpQty),
                        p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
            END IF;
        END;
    END IF;

    /*
        --validate that
        -- The loan number exists in loan database as PPP loan and is not cancelled.
        ---The loan does not have any FLAGS associated with it.
        IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loan.loangntytbl g
            WHERE     g.LOANSTATCD != 4
                  AND PRCSMTHDCD = 'PPP'
                  AND LoanNmb = v_PPPLoanNmb
                  -- check for flags
                  AND NOT EXISTS
                          (SELECT 1
                           FROM loan.LOANHLDSTATTBL h
                           WHERE     h.loanappnmb = g.loanappnmb
                                 AND ENDDT IS NULL);

            IF v_ChkVal = 0 THEN
                NULL;
            --- ADD_ERROR ( error code replaces null)
            END IF;
        END IF;
    */
    --amount requested can not over 2.5 times of average payroll
    IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
        BEGIN
            SELECT LoanProcdAmt
            INTO v_LoanRefinEIDLAmt
            FROM LoanProcdTbl
            WHERE     LoanAppNmb = p_LoanAppNmb
                  AND LOANPROCDTYPCD = 27;
        EXCEPTION
            WHEN OTHERS THEN
                v_LoanRefinEIDLAmt := NULL;
        END;

        IF     v_NAICSCD >= 720000
           AND v_NAICSCD < 730000 THEN
            /*IF (v_LOANAPPSCHEDCIND = 'Y') THEN
                v_MaxLoanAppRqstAmt := CEIL (v_LOANAPPGROSSINCOMEAMT * 3.5 * 100) / 100 + NVL (v_LoanRefinEIDLAmt, 0);
            ELSE */
                v_MaxLoanAppRqstAmt := CEIL (v_LoanAppMonthPayroll * 3.5 * 100) / 100 + NVL (v_LoanRefinEIDLAmt, 0);
            --END IF;
        ELSE
            v_MaxLoanAppRqstAmt := CEIL (v_LoanAppMonthPayroll * 2.5 * 100) / 100 + NVL (v_LoanRefinEIDLAmt, 0);
        END IF;

        IF v_LoanAppRqstAmt > v_MaxLoanAppRqstAmt THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb => p_LoanAppNmb,
                    p_ErrSeqNmb => p_ErrSeqNmb,
                    p_ErrCd => 4211,
                    p_ErrTyp => p_TransInd,
                    p_Stuff1 => TO_CHAR (v_LoanAppRqstAmt),
                    p_Stuff2 => TO_CHAR (
                                   CASE
                                       WHEN v_NAICSCD BETWEEN 720000 AND 729999 THEN 3.5
                                       ELSE 2.5
                                   END),
                    p_Stuff3 =>
                        TO_CHAR ('Average Monthly Payroll $' || v_LoanAppMonthPayroll),
                    --v_LoanRefinEIDLAmt),
                    p_FININSTRMNTTYPIND => v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    --Validates EWCP Single Transaction Post Shipment Indicator
    IF     v_LoanAppEWCPSnglTransInd = 'Y'
       AND v_LoanAppEWCPSnglTransPostInd IS NULL THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                131,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
        INTO v_CDCGntyFeeMulti
        FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'CDCGntyFeeMulti'
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

        SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
        INTO v_CDCFundFeeMulti
        FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'CDCFundFeeMulti'
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

        SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
        INTO v_CDCPrcsFeeMulti
        FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'CDCPrcsFeeMulti'
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

        SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
        INTO v_CDCServFeePctmax
        FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'CDCServFeePctMax'
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND IMPrmtrValEndDt IS NULL;

        SELECT ROUND (TO_NUMBER (IMPrmtrValTxt), 6)
        INTO v_CDCServFeePctmin
        FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'CDCServFeePctMin'
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND IMPrmtrValEndDt IS NULL;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;


    BEGIN
        /* v_TotDebentAmt := v_LoanAppCDCNetDbentrAmt + v_LoanAppCDCUndrwtrFeeAmt + v_LoanAppCDCGntyAmt + v_LoanAppCDCFundFeeAmt + v_LoanAppCDCClsCostAmt;

        IF (NVL(v_LoanAppCDCSeprtPrcsFeeInd,'N') != 'Y') then
                        v_TotDebentAmt := v_TotDebentAmt + v_LoanAppCDCPrcsFeeAmt;
         END IF;
            v_BalToBorrAmt := v_LoanAppRqstAmt - v_TotDebentAmt;*/

        IF     v_prgrmcd = 'E'
           AND v_LoanAppBalToBorrAmt > 1000 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4050,
                    p_TransInd,
                    v_LoanAppBalToBorrAmt,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END;

    IF     v_Prgrmcd = 'E'
       AND (   v_CDCServFeePct < v_CDCServFeePctmin
            OR v_CDCServFeePct > v_CDCServFeePctmax) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4071,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM sbaref.PrcsMthdTbl
    WHERE     PrcsMthdCd = v_PrcsMthdCd
          AND PrcsMthdExprsInd = 'Y';

    --Validates Injection Indicator
    IF    (    v_ChkVal = 0
           AND v_LoanAppInjctnInd IS NULL
           AND v_LoanTypInd != 'D')
       OR (    v_LoanAppInjctnInd IS NOT NULL
           AND v_LoanAppInjctnInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                149,
                p_TransInd,
                v_LoanAppInjctnInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates Life Insurance Required Indicator
    IF    (    v_PrcsMthdCd = 'LDP'
           AND v_LoanAppLifInsurRqmtInd IS NULL)
       OR (    v_LoanAppLifInsurRqmtInd IS NOT NULL
           AND v_LoanAppLifInsurRqmtInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                170,
                p_TransInd,
                v_LoanAppLifInsurRqmtInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates Months Interest Only Qty
    IF    v_LoanAppMoIntQty < 0
       OR (    v_LoanAppMoIntQty IS NULL
           AND v_PrgrmCd != 'E'
           AND v_LoanTypInd != 'D') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                168,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM sbaref.BusAgeCdTbl
    WHERE     BusAgeCd = v_LoanAppNewBusCd
          AND p_LoanAppRecvDt >= BusAgeStrtDt
          AND (   p_LoanAppRecvDt < BusAgeEndDt + 1
               OR BusAgeEndDt IS NULL);

    --Validates Business Age
    IF    (    v_PersonLoanInd = 'Y'
           AND v_LoanAppNewBusCd IS NOT NULL)
       OR (    v_LoanAppNewBusCd IS NOT NULL
           AND v_ChkVal = 0)
       OR (    v_LoanAppNewBusCd IS NULL
           AND v_PersonLoanInd = 'N'
           AND v_LoanTypInd != 'D') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                126,
                p_TransInd,
                v_LoanAppNewBusCd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validate Loan Name
    IF TRIM (v_LoanAppNm) IS NULL THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                163,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF    (    v_LoanTypInd != 'D'
           AND v_LoanCollatInd IS NULL)
       OR (    v_LoanCollatInd IS NOT NULL
           AND v_LoanCollatInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                250,
                p_TransInd,
                v_LoanCollatInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates Principal with Interest Indicator
    IF    (    v_LoanAppFullAmortPymtInd IS NOT NULL
           AND v_LoanAppFullAmortPymtInd NOT IN ('Y', 'N'))
       OR (    v_LoanAppFullAmortPymtInd IS NULL
           AND v_PrgrmCd != 'E'
           AND v_LoanTypInd != 'D') THEN
        p_ErrSeqNmb := ADD_ERROR (p_LoanAppNmb, p_ErrSeqNmb, 252, p_TransInd, v_LoanAppFullAmortPymtInd);
    END IF;

    /*APK - 08/02/2010 Issue:Is Payment Fully Amortizing default - Item 130*/
    IF (    v_LoanAppFullAmortPymtInd IS NOT NULL
        AND v_LoanAppFullAmortPymtInd NOT IN ('Y', 'N')
        AND v_PrgrmCd IN ('A', 'R')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                252,
                p_TransInd,
                v_LoanAppFullAmortPymtInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    -- Validates Recondsideration Indicator and Underwriting By
    IF (    TRIM (v_undrwritngBy) = 'LNDR'
        AND v_LoanAppRcnsdrtnInd = 'Y') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                2074,
                p_TransInd,
                v_undrwritngBy,
                v_LoanAppRcnsdrtnInd,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates Payment Amount
    --IF (v_LoanAppPymtAmt IS NULL OR v_LoanAppPymtAmt < 0) AND v_PrgrmCd NOT IN('X', 'E')
    IF     (   v_LoanAppPymtAmt IS NULL
            OR v_LoanAppPymtAmt < 0)
       AND v_PrgrmCd IN ('A', 'R') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                172,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;


    --Validates Recondsideration Indicator
    IF (    v_LoanAppRcnsdrtnInd IS NOT NULL
        AND v_LoanAppRcnsdrtnInd NOT IN ('Y', 'N'))       /*OR (v_LoanAppRcnsdrtnInd IS NULL AND v_LoanTypInd != 'D') */
                                                    THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                136,
                p_TransInd,
                v_LoanAppRcnsdrtnInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        v_IMPrmtrId := 'LoanAmtMin';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_LoanAmtMin := TO_NUMBER (v_IMPrmtrValChk);
    END;

    --Validates Request Amount
    IF    v_LoanAppRqstAmt IS NULL
       OR v_LoanAppRqstAmt < v_LoanAmtMin THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                105,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstAmt),
                TO_CHAR (v_LoanAmtMin),
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_PrgrmCd IN ('A', 'R')
       AND (ROUND ((v_LoanAppRqstAmt / 100), 0) * 100) != v_LoanAppRqstAmt THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                116,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstAmt),
                TO_CHAR (ROUND ((v_LoanAppRqstAmt / 100), 0) * 100),
                v_PrgrmCd,
                '100',
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_PrgrmCd = 'E'
       AND (ROUND ((v_LoanAppRqstAmt / 1000), 0) * 1000) != v_LoanAppRqstAmt THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                116,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstAmt),
                TO_CHAR (ROUND ((v_LoanAppRqstAmt / 1000), 0) * 1000),
                v_PrgrmCd,
                '1000',
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        --        FOR r1 IN (SELECT LOANECONDEVOBJCTCD
        --                     FROM LOANAPP.LOANECONDEVOBJCTCHLDTBL
        --                   WHERE LOANAPPNMB = p_LOANAPPNMB)
        --        LOOP
        --            v_LoanEconDevObjctCd := r1.LoanEconDevObjctCd;

        SELECT COUNT (*)
        INTO v_ChkEconDevGoalPPG
        FROM LOANAPP.LOANECONDEVOBJCTCHLDTBL a, sbaref.EconDevObjctCdTbl r
        WHERE     a.LOANAPPNMB = p_LOANAPPNMB
              AND r.EconDevObjctGoalTyp = 'PPG'
              AND r.EconDevObjctCd = a.LoanEconDevObjctCd;

        SELECT COUNT (*)
        INTO v_ChkVal
        FROM LoanSpcPurpsTbl
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND SpcPurpsLoanCd = 'DLTA';

        IF v_ChkVal > 0 THEN
            v_DLTASpcPurpsInd := 'Y';
        ELSE
            v_DLTASpcPurpsInd := 'N';
        END IF;

        v_MaxLoanAppRqstAmt := 0;

        IF v_PrgrmCd IN('A', 'D') THEN
            BEGIN
                v_IMPrmtrId := 'LoanRqstAmtMax';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_MaxLoanAppRqstAmt := TO_NUMBER (v_IMPrmtrValChk);
            END;
        ELSIF v_PrgrmCd = 'E' THEN
            SELECT COUNT (*)
            INTO v_ChkEconDevEnergy
            FROM IMPrmtrTbl a, IMPrmtrValTbl b, LOANAPP.LOANECONDEVOBJCTCHLDTBL l
            WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
                  AND a.IMPrmtrId = 'EconDevObjctEnergyCd'
                  AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                  AND (   IMPrmtrValEndDt IS NULL
                       OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1)
                  AND IMPrmtrValTxt = l.LoanEconDevObjctCd
                  AND l.LoanAppNmb = p_LoanAppNmb;
    
            IF    v_ChkEconDevEnergy > 0
               OR v_NAICSCd BETWEEN 300000 AND 399999 THEN
                -- JP 06/06/2019 commented out  v_LoanAppJobRqmtMetInd = 'Y' based on CAFSOPER-2773
                BEGIN
                    v_IMPrmtrId := 'Loan5EconEngRqstAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanAppRqstAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;
            ELSIF v_ChkEconDevGoalPPG > 0 THEN                                                           --(check for 'PPG')
                BEGIN
                    v_IMPrmtrId := 'Loan5EconRqstAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanAppRqstAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;
            ELSIF v_DLTASpcPurpsInd = 'Y' THEN
                BEGIN
                    v_IMPrmtrId := 'LoanRqstAmtDltaMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanAppRqstAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;
            ELSE
                BEGIN
                    v_IMPrmtrId := 'LoanRqstAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanAppRqstAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;
            END IF;
        END IF;
        
        ---- JP added 01/12/2018 (no 101 error if E01 or E03 provided)
        IF     v_PrgrmCd IN ('A', 'E', 'D')
           AND v_LoanAppRqstAmt > v_MaxLoanAppRqstAmt THEN
            IF v_PrgrmCd = 'E' THEN
                v_ErrText1 := 'Total Debenture Amount ' || v_LoanAppRqstAmt;
                v_ErrText3 := ' for Economic Development Objective Code.';
            ELSE
                v_ErrText1 := 'Total Request Amount ' || v_LoanAppRqstAmt;
                v_ErrText3 := '.';
            END IF;

            --IF v_ChkVal = 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    101,
                    p_TransInd,
                    TO_CHAR (v_ErrText1),
                    TO_CHAR (v_MaxLoanAppRqstAmt),
                    TO_CHAR (v_ErrText3),                                                      --(v_LoanEconDevObjctCd),
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        --END IF;
        END IF;
    --        END LOOP;
    END;

    v_SpcPurpsLoanCd := NULL;

    /*SELECT COUNT(*) INTO v_ChkVal FROM LoanSpcPurpsTbl WHERE LoanAppNmb = p_LoanAppNmb AND SpcPurpsLoanCd = 'REVL';
    IF v_ChkVal > 0 THEN
        v_SpcPurpsLoanCd := 'REVL';
    END IF;*/
    --Validates Maturity Term
    IF    v_LoanAppRqstMatMoQty IS NULL
       OR v_LoanAppRqstMatMoQty <= 0 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                115,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        v_IMPrmtrId := 'MatMoMax';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_MatMoMax := TO_NUMBER (v_IMPrmtrValChk);
    END;

    --changes by VK to make similar to loan.ValidateGntyFinCSP
    BEGIN
        SELECT SpcPurpsLoanCd
        INTO v_SpcPurpsLoanCd
        FROM LoanSpcPurpsTbl
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND SpcPurpsLoanCd = 'REVL';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_SpcPurpsLoanCd := NULL;
    END;

    IF v_SpcPurpsLoanCd = 'REVL' THEN
        BEGIN
            v_IMPrmtrId := 'MatMoRevlMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MatMoMax := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;

    BEGIN
        SELECT SpcPurpsLoanCd
        INTO v_SpcPurpsLoanCd
        FROM LoanSpcPurpsTbl
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND SpcPurpsLoanCd = 'CONS';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_SpcPurpsLoanCd := NVL (v_SpcPurpsLoanCd, NULL);
    END;

    IF v_SpcPurpsLoanCd = 'CONS' THEN
        BEGIN
            v_IMPrmtrId := 'MatMoConsMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MatMoMax := GREATEST (TO_NUMBER (v_IMPrmtrValChk), NVL (v_MatMoMax, 0));
        END;
    END IF;

    IF NVL (v_LoanAppRqstMatMoQty, 0) > NVL (v_MatMoMax, 0) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                118,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstMatMoQty),
                TO_CHAR (v_MatMoMax),
                v_SpcPurpsLoanCd,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;



    IF v_PrcsMthdCd = 'SBL' THEN
        BEGIN
            v_IMPrmtrId := 'MatMoRevlMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MatMoRevlMax := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;


    IF v_LoanAppRqstMatMoQty > v_MatMoRevlMax THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4104,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstMatMoQty),
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --10,20 and 25 years debenture---
    BEGIN
        SELECT TRUNC (IMPrmtrValStrtDt)
        INTO v_PublDt
        FROM loanapp.IMPrmtrValTbl c
        WHERE c.IMPRMTRSEQNMB = (SELECT d.IMPRMTRSEQNMB
                                 FROM loanapp.IMPrmtrTbl d
                                 WHERE d.IMPRMTRID = '25YrDbentr');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_PublDt := NULL;
    END;

    IF     v_PrgrmCd = 'E'
       AND TRUNC (v_PublDt) <= TRUNC (p_LoanAppRecvDt) THEN
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM IMPrmtrTbl a, IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId IN ('10YrDbentr', '20YrDbentr', '25YrDbentr')
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1)
              AND IMPrmtrValTxt = TRIM (TO_CHAR (NVL (v_LoanAppRqstMatMoQty, 0)));



        IF v_ChkVal = 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    157,
                    p_TransInd,
                    120 || ', ' || 240 || ' Or ' || 300,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    ELSIF     v_PrgrmCd = 'E'
          AND TRUNC (v_PublDt) > TRUNC (p_LoanAppRecvDt) THEN
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM IMPrmtrTbl a, IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId IN ('10YrDbentr', '20YrDbentr')
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND (   IMPrmtrValEndDt IS NULL
                   OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1)
              AND IMPrmtrValTxt = TRIM (TO_CHAR (NVL (v_LoanAppRqstMatMoQty, 0)));



        IF v_ChkVal = 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    157,
                    p_TransInd,
                    120 || ' Or ' || 240,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    -- End;
    IF     v_Prgrmcd = 'E'
       AND v_Imrttypcd1 = 'NR1'
       AND v_LoanAppRqstMatMoQty != 120 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4162,
                p_TransInd,
                120,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_Prgrmcd = 'E'
       AND v_Imrttypcd1 = 'NR2'
       AND v_LoanAppRqstMatMoQty != 240 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4163,
                p_TransInd,
                240,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    -- END if;
    -- End;
    --  Begin
    IF     v_Prgrmcd = 'E'
       AND v_Imrttypcd1 = 'NR3'
       AND v_LoanAppRqstMatMoQty != 300 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4164,
                p_TransInd,
                300,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF v_PrgrmCd IN ('H', 'X') THEN
        v_GntyPctMin := 100;
    ELSE
        BEGIN
            v_IMPrmtrId := 'GntyPctMin';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_GntyPctMin := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;

    --Validates SBA Percentage Guaranteed

    IF    v_LoanAppSBAGntyPct IS NULL
       OR v_LoanAppSBAGntyPct < v_GntyPctMin THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                112,
                p_TransInd,
                TO_CHAR (v_GntyPctMin),
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    v_MaxGntyPct := NULL;
    v_GntyPctRqstAmtLrgLimit := NULL;

    IF v_PrgrmCd IN ('H',
                     'X',
                     'O',
                     'C') THEN
        v_MaxGntyPct := 100;
    ELSE
        BEGIN
            BEGIN
                v_IMPrmtrId := 'GntyPctRqstAmtLrgLimit';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_GntyPctRqstAmtLrgLimit := TO_NUMBER (v_IMPrmtrValChk);
            END;

            IF v_LoanAppRqstAmt > v_GntyPctRqstAmtLrgLimit THEN
                BEGIN
                    v_IMPrmtrId := 'GntyPct7LrgAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxGntyPct := TO_NUMBER (v_IMPrmtrValChk);
                END;
            ELSE
                BEGIN
                    v_IMPrmtrId := 'GntyPctLowAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxGntyPct := TO_NUMBER (v_IMPrmtrValChk);
                END;
            END IF;
        END;
    END IF;

    IF v_LoanAppSBAGntyPct > v_MaxGntyPct THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                109,
                p_TransInd,
                v_PrgrmCd,
                TO_CHAR (v_LoanAppRqstAmt),
                TO_CHAR (v_LoanAppSBAGntyPct),
                TO_CHAR (v_MaxGntyPct),
                v_PrcsMthdCd,
                v_FININSTRMNTTYPIND);
    END IF;

    IF    v_PrgrmCd = 'E'
       OR v_PrcsMthdCd = 'ARC' THEN
        BEGIN
            BEGIN
                v_IMPrmtrId := 'GntyPct5Max';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_GntyPct5Max := TO_NUMBER (v_IMPrmtrValChk);
            END;

            IF NVL (v_LoanAppSBAGntyPct, 0) != v_GntyPct5Max THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        111,
                        p_TransInd,
                        TO_CHAR (v_LoanAppSBAGntyPct),
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;
    END IF;
    
    IF v_PrcsMthdCd = 'RRF' AND NVL (v_LoanAppSBAGntyPct, 0) != 100 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4284,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    v_MaxLoanExpo := NULL;

    IF v_DLTASpcPurpsInd = 'Y' THEN
        BEGIN
            v_IMPrmtrId := 'GntyAmtDELTAMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MaxLoanExpo := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;


    SELECT COUNT (*)
    INTO v_ChkVal
    FROM LoanSpcPurpsTbl
    WHERE     SpcPurpsLoanCd = 'PREQ'
          AND LoanAppNmb = p_LoanAppNmb;

    IF v_ChkVal > 0 THEN
        BEGIN
            v_IMPrmtrId := 'GntyAmtPREQMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MaxLoanExpo := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;

    IF v_MaxLoanExpo IS NULL THEN
        BEGIN
            BEGIN
                v_IMPrmtrId := 'GntyAmtMax';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_MaxLoanExpo := TO_NUMBER (v_IMPrmtrValChk);
            END;
        END;
    END IF;

    IF v_LoanAppSBAGntyPct * v_LoanAppRqstAmt * .01 > v_MaxLoanExpo THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                110,
                p_TransInd,
                TO_CHAR (v_MaxLoanExpo),
                TO_CHAR (v_LoanAppRqstAmt),
                TO_CHAR (v_LoanAppSBAGntyPct),
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates EPC Indicator
    IF    (    v_LoanAppEPCInd IS NULL
           AND v_PersonLoanInd != 'Y'
           AND v_LoanTypInd != 'D')
       OR (    v_LoanAppEPCInd IS NOT NULL
           AND v_LoanAppEPCInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                276,
                p_TransInd,
                v_LoanAppEPCInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM sbaref.PrcsMthdTbl
    WHERE     PrcsMthdCd = v_PrcsMthdCd
          AND p_LoanAppRecvDt >= PrcsMthdStrtDt
          AND (   p_LoanAppRecvDt < PrcsMthdEndDt + 1
               OR PrcsMthdEndDt IS NULL);

    --Validates Processing Method
    IF v_ChkVal = 0 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                121,
                p_TransInd,
                v_PrcsMthdCd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM sbaref.PrgrmTbl
    WHERE     PrgrmCd = v_PrgrmCd
          AND p_LoanAppRecvDt >= PrgrmStrtDt
          AND (   p_LoanAppRecvDt < PrgrmEndDt + 1
               OR PrgrmEndDt IS NULL);

    --Validates Progrom Code
    IF v_ChkVal = 0 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                280,
                p_TransInd,
                v_PrgrmCd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM sbaref.PrgrmValidTbl
    WHERE     PrgrmCd = v_PrgrmCd
          AND PrcsMthdCd = v_PrcsMthdCd
          AND FININSTRMNTTYPIND = v_FININSTRMNTTYPIND
          AND p_LoanAppRecvDt >= PrgrmValidStrtDt
          AND (   p_LoanAppRecvDt < PrgrmValidEndDt + 1
               OR PrgrmValidEndDt IS NULL);

    IF v_ChkVal = 0 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                282,
                p_TransInd,
                v_PrgrmCd,
                v_PrcsMthdCd,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validates SBA Recommend Loan Amount
    /* APK 04/15/2010 Modified below code for 174 so that 174 error cd is not checked for 'SMP'*/
    IF v_PrcsMthdCd = 'SMP' THEN
        BEGIN
            NULL;
        END;
    ELSE
        BEGIN
            /*PARTNER.CHECKUNDRWRITNGBYCSP(0,p_loanappnmb,v_PrcsMthdCd,sysdate,'LNDR',p_LoanAppRecvDt,v_ChkVal);

            /*SELECT COUNT(*)
            INTO v_ChkVal
            FROM sbaref.PrcsMthdTbl
            WHERE PrcsMthdCd         = v_PrcsMthdCd
            AND PrcsMthdUndrWritngBy = 'LNDR';*/

            IF TRIM (v_undrwritngBy) = 'LNDR' THEN
                v_ChkVal := 1;
            ELSE
                v_ChkVal := 0;
            END IF;


            IF     v_ChkVal > 0
               AND (   v_LoanAppSBARcmndAmt IS NULL
                    OR v_LoanAppSBARcmndAmt != v_LoanAppRqstAmt) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        174,
                        p_TransInd,
                        TO_CHAR (v_LoanAppSBARcmndAmt),
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;
    END IF;

    /* APK 04/15/2010 Modified below code for 174 so that 174 error cd is not checked for 'SMP'*/
    IF v_PrcsMthdCd = 'SMP' THEN
        BEGIN
            NULL;
        END;
    ELSE
        BEGIN
            /*PARTNER.CHECKUNDRWRITNGBYCSP(0,p_loanappnmb,v_PrcsMthdCd,sysdate,'SBA',p_LoanAppRecvDt,v_ChkVal);
            SELECT COUNT(*)
            INTO v_ChkVal
            FROM sbaref.PrcsMthdTbl
            WHERE PrcsMthdUndrwritngBy = 'SBA'
            AND PrcsMthdCd             = v_PrcsMthdCd;*/

            IF TRIM (v_undrwritngBy) = 'SBA' THEN
                v_ChkVal := 1;
            ELSE
                v_ChkVal := 0;
            END IF;

            IF v_ChkVal > 0 THEN
                SELECT COUNT (*)
                INTO v_ChkVal
                FROM LoanAppTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND StatCd = 'AP';

                IF     v_ChkVal = 0
                   AND (   v_LoanAppSBARcmndAmt IS NULL
                        OR v_LoanAppSBARcmndAmt <= 0
                        OR v_LoanAppSBARcmndAmt > v_LoanAppRqstAmt) THEN
                    p_ErrSeqNmb :=
                        ADD_ERROR (
                            p_LoanAppNmb,
                            p_ErrSeqNmb,
                            174,
                            p_TransInd,
                            TO_CHAR (v_LoanAppSBARcmndAmt),
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            v_FININSTRMNTTYPIND);
                END IF;
            END IF;
        END;
    END IF;

    ----------------------
    --Validate microloan--
    ----------------------
    IF v_PrcsMthdCd = 'MLD' THEN
        SELECT   v_LoanAppRqstAmt
               + NVL (
                     SUM (
                         CASE
                             WHEN a.LoanStatCd > 1 THEN a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                             ELSE LoanCurrAppvAmt
                         END),
                     0)
        INTO v_LoanSumAmt
        FROM loan.LoanGntyTbl a
        WHERE     TRIM (LoanAppMicroLendrId) = TRIM (v_LoanAppMicroLendrId)
              AND a.LoanStatCd < 4
              AND FININSTRMNTTYPIND != 'N'
              AND a.PrcsMthdCd = 'MLD';

        v_IMPrmtrId := 'AggregateRqstAmtMax';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);

        IF NVL (v_LoanSumAmt, v_LoanAppRqstAmt) > v_MaxLoanSumAmt THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    176,
                    p_TransInd,
                    v_PrcsMthdCd,
                    TO_CHAR (v_LoanSumAmt),
                    TO_CHAR (v_MaxLoanSumAmt),
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;

        SELECT NVL (MIN (a.LoanAppFundDt), SYSDATE), SYSDATE
        INTO v_MLDFirstFundDt, v_LoanAppFundDt
        FROM loan.LoanGntyTbl a
        WHERE     TRIM (LoanAppMicroLendrId) = TRIM (v_LoanAppMicroLendrId)
              AND FININSTRMNTTYPIND != 'N'
              AND a.LoanStatCd != 4
              AND a.PrcsMthdCd = 'MLD';

        --Get the maximum fiscal year limit for MLD loan
        IF v_LoanAppFundDt <= ADD_MONTHS (v_MLDFirstFundDt, 12) THEN
            v_FyLoanAmtLmt :=
                TO_NUMBER (LOANAPP.GET_IMPRMTRVAL ('MLDFirstFYLoanAmtLmt', v_PrcsMthdCd, SYSDATE, v_RecovInd));


            SELECT   v_LoanAppRqstAmt
                   + NVL (
                         SUM (
                             CASE
                                 WHEN a.LoanStatCd > 1 THEN a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                 ELSE LoanCurrAppvAmt
                             END),
                         0)
            INTO v_FyLoanAmtSum
            FROM loan.LoanGntyTbl a
            WHERE     TRIM (LoanAppMicroLendrId) = TRIM (v_LoanAppMicroLendrId)
                  AND FININSTRMNTTYPIND != 'N'
                  AND a.LoanStatCd < 4
                  AND a.PrcsMthdCd = 'MLD'
                  AND a.LoanAppFundDt <= ADD_MONTHS (v_MLDFirstFundDt, 12);

            IF v_FyLoanAmtSum > v_FyLoanAmtLmt THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        249,
                        p_TransInd,
                        TO_CHAR (v_FyLoanAmtSum),
                        TO_CHAR (v_FyLoanAmtLmt),
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        ELSE
            v_FyLoanAmtLmt :=
                TO_NUMBER (LOANAPP.GET_IMPRMTRVAL ('MLDSecondFYLoanAmtLmt', v_PrcsMthdCd, SYSDATE, v_RecovInd));

            IF v_LoanAppRqstAmt > v_FyLoanAmtLmt THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        106,
                        p_TransInd,
                        TO_CHAR (v_LoanAppRqstAmt),
                        v_PrcsMthdCd,
                        TO_CHAR (v_FyLoanAmtLmt),
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;
    --Get the fiscal year sum based on the current loan
    END IF;

    --------------------
    --rons changes start 1
    --------------------
    IF v_PrgrmCd IN('A', 'D') THEN
        BEGIN
            loan.LoadLoanEntCSP (p_LoanAppNmb);

            IF v_DLTASpcPurpsInd = 'Y' THEN
                BEGIN
                    SELECT   v_LoanAppRqstAmt
                           + SUM (
                                 CASE
                                     WHEN     a.LoanStatCd > 1
                                          AND EXISTS
                                                  (SELECT 1
                                                   FROM loan.LoanGntySpcPurpsTbl z
                                                   WHERE     SpcPurpsLoanCd = 'REVL'
                                                         AND z.LoanAppNmb = a.LoanAppNmb) THEN
                                         a.LoanCurrAppvAmt
                                     WHEN a.LoanStatCd > 1 THEN
                                         a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                     ELSE
                                         LoanCurrAppvAmt
                                 END)
                    INTO v_LoanSumAmt
                    FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                    WHERE     a.LoanAppNmb = b.LoanAppNmb
                          AND EXISTS
                                  (SELECT 1
                                   FROM loan.LoanEntTbl z
                                   WHERE     z.LoanAppNmb = p_LoanAppNmb
                                         AND z.TaxId = b.TaxId)
                          AND b.LoanBusPrimBorrInd = 'Y'
                          AND a.LoanStatCd < 4
                          AND a.PrgmCd = 'A'
                          AND a.PrcsMthdCd NOT IN ('PPP', 'PPS')
                          AND EXISTS
                                  (SELECT 1
                                   FROM loan.LoanGntySpcPurpsTbl z
                                   WHERE     z.LoanAppNmb = a.LoanAppNmb
                                         AND z.SpcPurpsLoanCd = 'DLTA');

                    v_IMPrmtrId := 'AggregateRqstAmtDltaMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;
            ELSE
                BEGIN
                    v_IMPrmtrId := 'AggregatePrcsGroup';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_AggregatePrcsGroup := RTRIM (v_IMPrmtrValChk);

                    IF v_AggregatePrcsGroup IS NOT NULL THEN
                        SELECT   v_LoanAppRqstAmt
                               + SUM (
                                     CASE
                                         WHEN     a.LoanStatCd > 1
                                              AND EXISTS
                                                      (SELECT 1
                                                       FROM loan.LoanGntySpcPurpsTbl z
                                                       WHERE     SpcPurpsLoanCd = 'REVL'
                                                             AND z.LoanAppNmb = a.LoanAppNmb) THEN
                                             a.LoanCurrAppvAmt
                                         WHEN a.LoanStatCd > 1 THEN
                                             a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                         ELSE
                                             LoanCurrAppvAmt
                                     END)
                        INTO v_LoanSumAmt
                        FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                        WHERE     a.LoanAppNmb = b.LoanAppNmb
                              AND EXISTS
                                      (SELECT 1
                                       FROM loan.LoanEntTbl z
                                       WHERE     z.LoanAppNmb = p_LoanAppNmb
                                             AND z.TaxId = b.TaxId)
                              AND b.LoanBusPrimBorrInd = 'Y'
                              AND a.LoanStatCd < 4
                              AND a.PrgmCd = 'A'
                              AND a.PrcsMthdCd NOT IN ('PPP', 'PPS')
                              AND EXISTS
                                      (SELECT 1
                                       FROM loanapp.IMPrmtrTbl z, loanapp.IMPrmtrValTbl y
                                       WHERE     z.IMPrmtrSeqNmb = y.IMPrmtrSeqNmb
                                             AND z.IMPrmtrId = v_IMPrmtrId
                                             AND y.IMPRMTRVALTXT = v_AggregatePrcsGroup
                                             AND y.PrcsMthdCd = a.PrcsMthdCd
                                             AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                                             AND (   IMPrmtrValEndDt IS NULL
                                                  OR IMPrmtrValEndDt + 1 > p_LoanAppRecvDt));

                        v_IMPrmtrId := 'AggregateRqstAmtGroupMax';
                        v_IMPrmtrValChk :=
                            LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_AggregatePrcsGroup);
                        v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                    ELSE
                        IF v_PrcsMthdCd IN ('PPP', 'PPS', 'RRF') THEN
                            SELECT v_LoanAppRqstAmt + NVL(SUM (a.LoanCurrAppvAmt),0)
                            INTO v_LoanSumAmt
                            FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                            WHERE     a.LoanAppNmb = b.LoanAppNmb
                                  AND a.LoanAppNmb != p_LoanAppNmb
                                  AND b.LoanBusPrimBorrInd = 'Y'
                                  AND b.LOANBORRACTVINACTIND = 'A'
                                  AND a.LoanStatCd != 4
                                  --AND a.PrgmCd = 'A'
                                  AND a.PrcsMthdCd IN ('PPP', 'PPS', 'RRF')
                                  AND EXISTS
                                          (SELECT 1
                                           FROM loan.LoanEntTbl z
                                           WHERE     z.LoanAppNmb = p_LoanAppNmb
                                                 AND z.TaxId = b.TaxId);

                            v_IMPrmtrId := 'AggregateRqstAmtMax';
                            v_IMPrmtrValChk :=
                                LOANAPP.GET_IMPRMTRVAL (
                                    v_IMPrmtrId,
                                    v_PrcsMthdCd,
                                    p_LoanAppRecvDt,
                                    v_AggregatePrcsGroup);
                            v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                        ELSE
                            DELETE LOAN.TMPLOANGNTYAGGTBL
                            WHERE LoanMainAppNmb = p_LoanAppNmb;

                            INSERT INTO LOAN.TMPLOANGNTYAGGTBL (LOANMAINAPPNMB,
                                                                LOANAPPNMB,
                                                                LOANCURRAPPVAMT,
                                                                LOANAPPFUNDDT)
                                SELECT p_LoanAppNmb,
                                       a.LoanAppNmb,
                                       CASE
                                           WHEN     a.LoanStatCd > 1
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loan.LoanGntySpcPurpsTbl z
                                                         WHERE     SpcPurpsLoanCd = 'REVL'
                                                               AND z.LoanAppNmb = a.LoanAppNmb) THEN
                                               a.LoanCurrAppvAmt
                                           WHEN a.LoanStatCd > 1 THEN
                                               a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                           ELSE
                                               LoanCurrAppvAmt
                                       END,
                                       a.LOANAPPFUNDDT
                                FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                                WHERE     a.LoanAppNmb = b.LoanAppNmb
                                      AND EXISTS
                                              (SELECT 1
                                               FROM loan.LoanEntTbl z
                                               WHERE     z.LoanAppNmb = p_LoanAppNmb
                                                     AND z.TaxId = b.TaxId)
                                      AND b.LoanBusPrimBorrInd = 'Y'
                                      AND a.PrgmCd = 'A'
                                      AND a.PrcsMthdCd NOT IN ('PPP', 'PPS')
                                      AND a.LoanStatCd < 4;

                            v_LoanSumAmt := v_LoanAppRqstAmt + loan.GETGNTYAGGAMT (p_LoanAppNmb, p_LoanAppRecvDt);

                            DELETE LOAN.TMPLOANGNTYAGGTBL
                            WHERE LoanMainAppNmb = p_LoanAppNmb;

                            v_IMPrmtrId := 'AggregateRqstAmtMax';
                            v_IMPrmtrValChk :=
                                LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                            v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                        END IF;                                                                               --PPP loan
                    END IF;                                                                                  --group max
                END;
            END IF;

            IF NVL (v_LoanSumAmt, v_LoanAppRqstAmt) > v_MaxLoanSumAmt THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        176,
                        p_TransInd,
                        v_PrcsMthdCd,
                        TO_CHAR (v_LoanSumAmt),
                        TO_CHAR (v_MaxLoanSumAmt),
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            IF v_PrcsMthdCd NOT IN ('PPP', 'PPS') THEN
                SELECT   v_LoanAppRqstAmt * v_LoanAppSBAGntyPct * 0.01
                       + SUM (
                               (CASE
                                    WHEN     a.LoanStatCd > 1
                                         AND EXISTS
                                                 (SELECT 1
                                                  FROM loan.LoanGntySpcPurpsTbl z
                                                  WHERE     SpcPurpsLoanCd = 'REVL'
                                                        AND z.LoanAppNmb = a.LoanAppNmb) THEN
                                        a.LoanCurrAppvAmt
                                    WHEN a.LoanStatCd > 1 THEN
                                        a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                    ELSE
                                        LoanCurrAppvAmt
                                END)
                             * LoanAppSBAGntyPct
                             * 0.01)
                INTO v_LoanSumAmt
                FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                WHERE     a.LoanAppNmb = b.LoanAppNmb
                      AND EXISTS
                              (SELECT 1
                               FROM loan.LoanEntTbl z
                               WHERE     z.LoanAppNmb = p_LoanAppNmb
                                     AND z.TaxId = b.TaxId)
                      AND b.LoanBusPrimBorrInd = 'Y'
                      AND a.PrgmCd = 'A'
                      AND a.PrcsMthdCd NOT IN ('PPP', 'PPS')
                      AND a.LoanStatCd < 4;

                --------------
                --rons changes end 1
                --------------
                BEGIN
                    v_IMPrmtrId := 'AggregateSBAShareAmtMax';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;

                IF NVL (v_LoanSumAmt, v_LoanAppRqstAmt * v_LoanAppSBAGntyPct * 0.01) > v_MaxLoanSumAmt THEN
                    p_ErrSeqNmb :=
                        ADD_ERROR (
                            p_LoanAppNmb,
                            p_ErrSeqNmb,
                            177,
                            p_TransInd,
                            TO_CHAR (v_LoanSumAmt),
                            TO_CHAR (v_MaxLoanSumAmt),
                            NULL,
                            NULL,
                            NULL,
                            v_FININSTRMNTTYPIND);
                END IF;

                --------------
                --rons changes start 2
                --------------
                --sbx apk 01/31/2012--
                BEGIN
                    v_IMPrmtrId := 'AggregatePerPrcsMthd';
                    v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                    v_MaxLoanSumAmt := TO_NUMBER (v_IMPrmtrValChk);
                END;

                IF NVL (v_MaxLoanSumAmt, 0) > 0 THEN
                    SELECT   v_LoanAppRqstAmt
                           + NVL (
                                 SUM (
                                     CASE
                                         WHEN     a.LoanStatCd > 1
                                              AND EXISTS
                                                      (SELECT 1
                                                       FROM loan.LoanGntySpcPurpsTbl z
                                                       WHERE     SpcPurpsLoanCd = 'REVL'
                                                             AND z.LoanAppNmb = a.LoanAppNmb) THEN
                                             a.LoanCurrAppvAmt
                                         WHEN a.LoanStatCd > 1 THEN
                                             a.LoanOutBalAmt + NVL (a.LoanTotUndisbAmt, 0)
                                         ELSE
                                             LoanCurrAppvAmt
                                     END),
                                 0)
                    INTO v_LoanSumAmt
                    FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
                    WHERE     a.LoanAppNmb = b.LoanAppNmb
                          AND EXISTS
                                  (SELECT 1
                                   FROM loan.LoanEntTbl z
                                   WHERE     z.LoanAppNmb = p_LoanAppNmb
                                         AND z.TaxId = b.TaxId)
                          AND b.LoanBusPrimBorrInd = 'Y'
                          AND a.PrgmCd = 'A'
                          AND a.PrcsMthdCd = v_PrcsMthdCd
                          AND a.LoanStatCd < 4;

                    IF v_LoanSumAmt > v_MaxLoanSumAmt THEN
                        p_ErrSeqNmb :=
                            ADD_ERROR (
                                p_LoanAppNmb,
                                p_ErrSeqNmb,
                                1100,
                                p_TransInd,
                                v_PrcsMthdCd,
                                TO_CHAR (v_LoanSumAmt),
                                TO_CHAR (v_MaxLoanSumAmt),
                                NULL,
                                NULL,
                                v_FININSTRMNTTYPIND);
                    END IF;
                END IF;                                                                          --Per Processing Method
            END IF;                                                                                                --PPP

            IF v_PrcsMthdCd = 'ARC' THEN
                BEGIN
                    IF v_LoanAppRqstMatMoQty NOT BETWEEN 72 AND 78 THEN
                        p_ErrSeqNmb :=
                            ADD_ERROR (
                                p_LoanAppNmb,
                                p_ErrSeqNmb,
                                248,
                                p_TransInd,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                v_FININSTRMNTTYPIND);
                    END IF;
                END;                                                                                            --IF ARC
            END IF;

            DELETE loan.LoanEntTbl
            WHERE LoanAppNmb = p_LoanAppNmb;
        END;
    END IF;                                                                                                  --Program A

    SELECT COUNT(*) 
    INTO v_ChkVal
    FROM LoanAppHldStatTbl WHERE LoanAppNmb = p_LoanAppNmb AND ENDDT IS NULL;
    
        IF v_DSPSTNSTATCD = 8 OR v_ChkVal > 0 THEN
            --There are alerts on the loan
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4269,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
        
    IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
        IF NVL (TRIM (v_undrwritngBy), 'NONE') != 'SBA' THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4271,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;

        v_TaxId_Switch :=
            CASE
                WHEN SUBSTR (v_TaxId, 1, 1) = '0' THEN '1' || SUBSTR (v_TaxId, 2, 9)
                ELSE '0' || SUBSTR (v_TaxId, 2, 9)
            END;

        SELECT COUNT (*)
        INTO v_ChkVal
        FROM loan.LoanGntyBorrTbl a,
             loan.LoanGntyTbl b,
             loan.BusTbl mb,
             loan.LoanGntyBusTbl gb,
             loanapp.LoanBusTbl ab
        WHERE     a.LoanAppNmb = b.LoanAppNmb
              AND a.LoanBusPrimBorrInd = 'Y'
              AND a.LoanBorrActvInactInd = 'A'
              AND a.LoanAppNmb != p_LoanAppNmb
              AND b.LoanStatCd != 4
              AND b.PrcsMthdCd = v_PrcsMthdCd
              AND a.TaxId = v_TaxId_Switch
              AND a.LoanAppNmb = gb.LoanAppNmb
              AND a.TaxId = gb.TaxId
              AND a.TaxId = mb.TaxId
              AND ab.LoanAppNmb = p_LoanAppNmb
              AND ab.TaxId = v_TaxId
              AND UPPER (TRIM (ab.BUSNM)) = UPPER (TRIM (mb.BUSNM))
              AND ab.BUSPHYADDRSTCD = gb.BUSPHYADDRSTCD
              AND ROWNUM = 1;

        IF v_ChkVal = 0 THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loanapp.LoanAppTbl a,
                 loanapp.LoanBorrTbl b,
                 loanapp.LoanBusTbl gb,
                 loanapp.LoanBusTbl cb
            WHERE     a.LoanAppNmb = b.LoanAppNmb
                  AND b.LoanBusPrimBorrInd = 'Y'
                  AND a.PrcsMthdCd = v_PrcsMthdCd
                  AND a.LoanAppNmb != p_LoanAppNmb
                  AND a.StatCd IN ('AP',
                                   'R1',
                                   'R2',
                                   'PR',
                                   'FP')
                  AND a.LoanAppNmb != p_LoanAppNmb
                  AND b.TaxId = v_TaxId_Switch
                  AND b.LoanAppNmb = gb.LoanAppNmb
                  AND b.TaxId = gb.TaxId
                  AND cb.LoanAppNmb = p_LoanAppNmb
                  AND cb.TaxId = v_TaxId
                  AND UPPER (TRIM (gb.BUSNM)) = UPPER (TRIM (cb.BUSNM))
                  AND gb.BUSPHYADDRSTCD = cb.BUSPHYADDRSTCD
                  AND ROWNUM = 1;
        END IF;

        IF v_ChkVal > 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    258,
                    p_TransInd,
                    v_TaxId,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    IF v_PrcsMthdCd IN ('PPP',
                        'PPS',
                        'ARC',
                        'DCI') THEN
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM loan.LoanGntyBorrTbl a, loan.LoanGntyTbl b
        WHERE     a.LoanAppNmb = b.LoanAppNmb
              AND a.LoanBusPrimBorrInd = 'Y'
              AND a.LoanBorrActvInactInd = 'A'
              AND a.LoanAppNmb != p_LoanAppNmb
              AND b.LoanStatCd != 4
              AND b.PrcsMthdCd = v_PrcsMthdCd
              AND a.TaxId = v_TaxId;

        IF     v_ChkVal = 0
           AND v_PrcsMthdCd IN ('PPP', 'PPS') THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loanapp.LoanAppTbl a, loanapp.LoanBorrTbl b
            WHERE     a.LoanAppNmb = b.LoanAppNmb
                  AND b.LoanBusPrimBorrInd = 'Y'
                  AND a.PrcsMthdCd = v_PrcsMthdCd
                  AND a.StatCd IN ('AP',
                                   'R1',
                                   'PR',
                                   'FP')
                  AND a.LoanAppNmb != p_LoanAppNmb
                  AND b.TaxId = v_TaxId;
        END IF;

        IF v_ChkVal > 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    237,
                    p_TransInd,
                    CASE
                        WHEN v_PrcsMthdCd IN ('PPP', 'PPS') THEN 'Payroll Protection'
                        WHEN v_PrcsMthdCd = 'DCI' THEN 'COVID 19 Disaster'
                        ELSE v_PrcsMthdCd
                    END,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;

        --Can not have both PPP AND DCI, except DCI is funded before 04/01/2020
        v_ChkVal := 0;

        IF v_PrcsMthdCd IN ('PPP', 'PPS') THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loan.LoanGntyBorrTbl a, loan.LoanGntyTbl b
            WHERE     a.LoanAppNmb = b.LoanAppNmb
                  AND a.LoanBusPrimBorrInd = 'Y'
                  AND a.LoanBorrActvInactInd = 'A'
                  AND b.LoanStatCd != 4
                  AND b.PrcsMthdCd = 'DCI'
                  AND b.LoanAppFundDt >= '01 APR 2020'
                  AND a.TaxId = v_TaxId;


            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        253,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
			END IF;
                        
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loanapp.LoanAppTbl a, loanapp.LoanBorrTbl b
            WHERE a.LoanAppNmb = b.LoanAppNmb
			AND b.LoanBusPrimBorrInd = 'Y'
			AND a.PrcsMthdCd = 'RRF'
			AND a.StatCd != 'WD'
			AND b.TaxId = v_TaxId;

            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4286,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        ELSIF v_PrcsMthdCd = 'DCI' THEN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loan.LoanGntyBorrTbl a, loan.LoanGntyTbl b
            WHERE     a.LoanAppNmb = b.LoanAppNmb
                  AND a.LoanBusPrimBorrInd = 'Y'
                  AND a.LoanBorrActvInactInd = 'A'
                  AND b.LoanStatCd != 4
                  AND b.PrcsMthdCd IN ('PPP', 'PPS')
                  AND a.TaxId = v_TaxId;

            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        255,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;
    END IF;

    IF v_PrcsMthdCd = 'DCI' THEN
    	SELECT COUNT(*)
    	INTO v_ChkVal
    	FROM loan.LoanGntyTbl g, loan.LoanGntyBorrTbl b, loan.LoanHldStatTbl p
    	WHERE b.TaxId = v_TaxId
    	--AND g.PrcsMthdCd = 'PPP'
    	AND g.LoanStatcd != 4
    	AND g.LoanAppNmb = b.LoanAppNmb
    	AND g.LoanAppNmb = p.LoanAppNmb
    	AND b.LoanBusPrimBorrInd = 'Y'
    	AND b.LoanBorrActvInactInd = 'A'
    	AND p.LoanHldStatCd in(35,41,43,44,57,58,67,68,70,71,504,604)
    	AND p.EndDt IS NULL;
    	
    	IF v_ChkVal = 0 THEN
			SELECT COUNT(*)
			INTO v_ChkVal
			FROM loanapp.LoanBorrTbl b, loanapp.LoanAppHldStatTbl p
			WHERE b.TaxId = v_TaxId
			AND p.LoanAppNmb = b.LoanAppNmb
			AND b.LoanBusPrimBorrInd = 'Y'
			AND p.LoanHldStatCd in(35,41,43,44,57,58,67,68,70,71,504,604)
			AND p.EndDt IS NULL;
    	END IF;
    	
    	IF v_ChkVal > 0 THEN
			p_ErrSeqNmb :=
				ADD_ERROR (
					p_LoanAppNmb,
					p_ErrSeqNmb,
					4296,
					p_TransInd,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					v_FININSTRMNTTYPIND);
		END IF;
    END IF;
    
    IF v_PrcsMthdCd = 'RRF' THEN
    	/*SELECT COUNT (*)
    	INTO v_ChkVal
    	FROM loan.LoanGntyTbl g, loan.LoanGntyBorrTbl b, loan.LoanGntyProjTbl p
    	WHERE g.PrcsMthdCd = 'PPP'
    	AND g.LoanStatcd != 4
    	AND g.LoanAppNmb = b.LoanAppNmb
    	AND g.LoanAppNmb = p.LoanAppNmb
    	AND b.LoanBusPrimBorrInd = 'Y'
    	AND b.LoanBorrActvInactInd = 'A'
    	AND b.TaxId = v_TaxId
    	AND p.NAICSCD IS NOT NULL
    	AND p.NAICSCD NOT BETWEEN 722000 AND 723000;
    	
    	IF v_ChkVal > 0 THEN
			p_ErrSeqNmb :=
				ADD_ERROR (
					p_LoanAppNmb,
					p_ErrSeqNmb,
					4288,
					p_TransInd,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					v_FININSTRMNTTYPIND);
    	END IF;*/
    	
		SELECT COUNT (*)
		INTO v_ChkVal
		FROM loanapp.LoanAppTbl a, loanapp.LoanBorrTbl b
		WHERE a.LoanAppNmb = b.LoanAppNmb
		AND a.LoanAppNmb != p_LoanAppNmb
		AND b.LoanBusPrimBorrInd = 'Y'
		AND a.PrcsMthdCd = 'RRF'
		AND a.StatCd != 'WD'
		AND b.TaxId = v_TaxId;

		IF v_ChkVal > 0 THEN
			p_ErrSeqNmb :=
				ADD_ERROR (
					p_LoanAppNmb,
					p_ErrSeqNmb,
					237,
					p_TransInd,
					'Restaurant Revitalization',
					NULL,
					NULL,
					NULL,
					NULL,
					v_FININSTRMNTTYPIND);
		END IF;
		
		SELECT COUNT (*)
		INTO v_ChkVal
		FROM loanapp.SVOGHldTbl
		WHERE TaxId = v_TaxId;

		IF v_ChkVal > 0 THEN
			p_ErrSeqNmb :=
				ADD_ERROR (
					p_LoanAppNmb,
					p_ErrSeqNmb,
					4287,
					p_TransInd,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					v_FININSTRMNTTYPIND);
		END IF;
    END IF;
    
    --Can not have any loan before PPP paid off except RRF
    IF v_PrcsMthdCd NOT IN ('PPP', 'PPS') AND v_PrcsMthdCd != 'RRF' THEN
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM loan.LoanGntyBorrTbl a, loan.LoanGntyTbl b
        WHERE     a.LoanAppNmb = b.LoanAppNmb
              AND a.LoanBusPrimBorrInd = 'Y'
              AND a.LoanBorrActvInactInd = 'A'
              AND b.LoanStatCd NOT IN (4, 5)
              AND b.PrcsMthdCd IN ('PPP', 'PPS')
              AND a.TaxId = v_TaxId;

        IF v_ChkVal > 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    254,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    SELECT COUNT (*)
    INTO v_ChkVal
    FROM loanapp.loanborrtbl a, loanapp.loanprintbl b, loanapp.LOANBUSPRINTBL bp
    WHERE     a.loanappnmb = p_loanappnmb
          AND b.loanappnmb = p_loanappnmb
          AND a.borrseqnmb = bp.borrseqnmb
          AND b.prinseqnmb = bp.prinseqnmb
          AND a.LoanBusPrimBorrInd = 'Y'
          AND SUBSTR (a.taxid, 2, 9) = SUBSTR (b.taxid, 2, 9)
          AND SUBSTR (a.taxid, 1, 1) = '0'
          AND SUBSTR (b.taxid, 1, 1) = '1';

    IF v_ChkVal > 0 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4216,
                p_TransInd,
                v_TaxId,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;


    IF     v_PrcsMthdCd = '5RE'
       AND NVL (v_LOANAMTLIMEXMPTIND, 'N') != 'Y'
       AND v_LocId IS NOT NULL THEN
        SELECT SUM (LoanCurrAppvamt)
        INTO v_TotalPrevFyAppvAmt
        FROM loan.loangntytbl
        WHERE     locid = v_LocId
              AND sbaref.fiscal_year (loanappfunddt) = sbaref.fiscal_year (SYSDATE) - 1
              AND prgmcd = 'E'
              AND prcsmthdcd != '5RE';

        SELECT v_LoanAppRqstAmt + NVL (SUM (LoanCurrAppvamt), 0)
        INTO v_TotalCurrFy5REAmt
        FROM loan.loangntytbl
        WHERE     locid = v_LocId
              AND sbaref.fiscal_year (loanappfunddt) = sbaref.fiscal_year (SYSDATE)
              AND prgmcd = 'E'
              AND prcsmthdcd = '5RE';

        v_IMPrmtrId := 'Pct504RefinFySumMax';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);

        IF v_TotalCurrFy5REAmt > NVL (v_TotalPrevFyAppvAmt, 0) * TO_NUMBER (v_IMPrmtrValChk) / 100 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1101,
                    p_TransInd,
                    TO_CHAR (v_TotalCurrFy5REAmt),
                    TO_CHAR (v_TotalPrevFyAppvAmt),
                    v_IMPrmtrValChk,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    IF v_PrcsMthdCd = 'PTX' THEN
        BEGIN
            SELECT SUM (T2.LoanBusPrinPctOwnrshpBus)
            INTO v_VetPctOwnrshpSum
            FROM LoanBorrTbl T3,
                 LoanPrinTbl T1,
                 LoanBusPrinTbl T2,
                 sbaref.VetTbl v
            WHERE     T1.LoanAppNmb = p_LoanAppNmb
                  AND T2.LoanAppNmb = p_LoanAppNmb
                  AND T3.LoanAppNmb = p_LoanAppNmb
                  AND T1.PrinSeqNmb = T2.PrinSeqNmb
                  AND T2.BorrSeqNmb = T3.BorrSeqNmb
                  AND T3.LoanBusPrimBorrInd = 'Y'
                  AND T1.VetCd = v.VetCd
                  AND v.VetGrpCd IN ('S', 'C');

            v_IMPrmtrId := 'PctOwnrshpVetSumMin';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_PctOwnrshpVetSumMin := TO_NUMBER (v_IMPrmtrValChk);

            IF NVL (v_VetPctOwnrshpSum, 0) < v_PctOwnrshpVetSumMin THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        180,
                        p_TransInd,
                        TO_CHAR (v_VetPctOwnrshpSum),
                        TO_CHAR (v_PctOwnrshpVetSumMin),
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;
    END IF;

    IF v_PrcsMthdCd = 'MLD' THEN
        BEGIN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM partner.PrtAliasTbl z
            WHERE     TRIM (z.PrtAliasNm) = TRIM (v_LoanAppMicroLendrId)
                  AND z.ValidPrtAliasTyp = 'Micro';

            IF    v_LoanAppMicroLendrId IS NULL
               OR v_ChkVal = 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        200,
                        p_TransInd,
                        TO_CHAR (v_LoanAppMicroLendrId),
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;                                                                                                    --IF MLD
    END IF;

    IF v_PrcsMthdCd = 'ARC' THEN
        BEGIN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM LoanAppARCRsnTbl
            WHERE LoanAppNmb = p_LoanAppNmb;

            IF v_ChkVal = 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        234,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            SELECT COUNT (*)
            INTO v_ChkVal
            FROM LoanAppARCRsnTbl
            WHERE     LoanAppNmb = p_LoanAppNmb
                  AND ARCLoanRsnCd = 9
                  AND ARCLoanRsnOthDescTxt IS NULL;

            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        247,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;                                                                                                    --IF ARC
    END IF;

    IF v_RecovInd = 'R' THEN
        BEGIN
            SELECT COUNT (*)
            INTO v_ChkVal
            FROM IMPrmtrTbl a, IMPrmtrValTbl b
            WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
                  AND a.IMPrmtrId = 'NAICSCdNonARRA'
                  AND TO_NUMBER (IMPrmtrValTxt) = v_NAICSCd
                  AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
                  AND (   IMPrmtrValEndDt IS NULL
                       OR p_LoanAppRecvDt < IMPrmtrValEndDt + 1);

            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        235,
                        p_TransInd,
                        TO_CHAR (v_NAICSCd),
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            SELECT COUNT (*)
            INTO v_ChkVal
            FROM loan.LoanGntyTbl a, loan.LoanGntyBorrTbl b
            WHERE     a.LoanStatCd = 4
                  AND a.LoanAppNmb = b.LoanAppNmb
                  AND b.LoanBusPrimBorrInd = 'Y'
                  AND b.TaxId = v_TaxId
                  AND LoanInitlAppvAmt = v_LoanAppRqstAmt
                  AND a.RecovInd IS NULL
                  AND ((SELECT MAX (LoanTransEffDt)
                        FROM loan.LoanTransTbl z
                        WHERE z.LoanAppNmb = a.LoanAppNmb) >= (SYSDATE - 90));

            IF v_ChkVal > 0 THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        233,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END;                                                                                               --IF Recovery
    END IF;

    --Validates CDC Closing Cost Amount
    IF     v_PrgrmCd = 'E'
       AND (   v_LoanAppCDCClsCostAmt IS NULL
            OR v_LoanAppCDCClsCostAmt < 0) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                142,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validate CDC Net Debenture Amount
    IF     v_PrgrmCd = 'E'
       AND (   v_LoanAppCDCNetDbentrAmt IS NULL
            OR v_LoanAppCDCNetDbentrAmt <= 0) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                107,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF    (    v_PrgrmCd = 'E'
           AND v_LoanAppCDCSeprtPrcsFeeInd IS NULL)
       OR (    v_LoanAppCDCSeprtPrcsFeeInd IS NOT NULL
           AND v_LoanAppCDCSeprtPrcsFeeInd NOT IN ('Y', 'N')) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                113,
                p_TransInd,
                v_LoanAppCDCSeprtPrcsFeeInd,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        SELECT NVL (SUM (LoanInjctnAmt), 0)
        INTO v_InjctnSum
        FROM LoanInjctnTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_InjctnSum := NULL;
    END;

    --Validate Borrower Contribution Amount
    IF     v_PrgrmCd = 'E'
       AND (   v_LoanAppContribAmt IS NULL
            OR v_LoanAppContribAmt <> v_InjctnSum) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                147,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --Validate Borrower Contribution Percent
    IF     v_PrgrmCd = 'E'
       AND (   v_LoanAppContribPct IS NULL
            OR v_LoanAppContribPct < 0) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                108,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_PrgrmCd = 'E'
       AND v_LOANCOLLATIND = 'N' THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4049,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    BEGIN
        /* Get loan application information */
        SELECT l.PrcsMthdCd,
               LoanAppRecvDt,
               l.LoanAppSBARcmndAmt,
               l.RecovInd
        INTO v_PrcsMthdCd,
             v_LoanAppRecvDt,
             v_LoanAmt,
             v_RecovInd
        FROM loanapp.LoanAppTbl l
        WHERE l.LoanAppNmb = p_LoanAppNmb;

        v_FY := TO_CHAR (SYSDATE, 'YYYY') + SIGN (SIGN (TO_CHAR (SYSDATE, 'MM') - 10) + 1);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;

    BEGIN
        /* Retrieve Cohort code */
        SELECT COUNT (*)
        INTO v_CohortCdCnt
        FROM sbaref.PrcsMthdCohortTbl pv, loanacct.FundsBalTbl fb
        WHERE     pv.PrcsMthdCd = v_PrcsMthdCd
              AND NVL (pv.RecovInd, ' ') = NVL (v_RecovInd, ' ')
              AND pv.CohortCd = fb.CohortCd
              AND fb.FundsFisclYr <= v_FY
              AND fb.FundsEndFisclYr >= v_FY
              --and fb.FUNDSORIGFROZENIND = 'N'
              --and fb.FundsBalAmt >= v_LoanAmt
              AND fb.FUNDSACTVINACTVIND = 'A'
              AND pv.COHORTSTRTDT <= SYSDATE
              AND (   pv.COHORTENDDT IS NULL
                   OR pv.COHORTENDDT + 1 > SYSDATE);
    --AND ROWNUM = 1
    --ORDER BY POS;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_CohortCdCnt := 0;
    END;

    IF (v_CohortCdCnt = 0) THEN
        BEGIN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    921,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END;
    END IF;

    IF v_PrcsMthdCd = 'SMP' THEN
        BEGIN
            v_IMPrmtrId := 'MatMoMin';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_MatMoMin := TO_NUMBER (v_IMPrmtrValChk);
        END;
    END IF;

    IF     v_PrcsMthdCd = 'SMP'
       AND v_LoanAppRqstMatMoQty < v_MatMoMin THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                922,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstMatMoQty),
                TO_CHAR (v_MatMoMin),
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    /*APK 08/16/2010- new validation in loanapp.ValidateLoanApp stored procedure to make LoanAppDisasterCntrlNmb mandatory for PrcsMthdCd EQ 'DGB' */
    IF     v_PrcsMthdCd = 'DGB'
       AND v_LOANAPPDISASTERCNTRLNMB IS NULL THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                925,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF v_PrgrmCd = 'H' THEN
        IF v_LOANAPPDISASTERCNTRLNMB IS NULL THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1027,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;

        IF v_LOANDISASTRAPPNMB IS NULL THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1028,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    BEGIN
        SELECT 1
        INTO v_ChkVal
        FROM DUAL
        WHERE EXISTS
                  (SELECT *
                   FROM SBAREF.PRGRMVALIDTBL v
                   WHERE     v.PrcsMthdCd = v_PrcsMthdCd
                         AND v.FININSTRMNTTYPIND = v_FININSTRMNTTYPIND
                         AND p_LoanAppRecvDt >= PrgrmValidStrtDt
                         AND (   p_LoanAppRecvDt < PrgrmValidEndDt + 1
                              OR PrgrmValidEndDt IS NULL)
                         AND v.SPCPURPSLOANCD = 'REVL');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_ChkVal := 0;
    END;

    BEGIN
        SELECT SpcPurpsLoanCd
        INTO v_SpcPurpsLoanCd
        FROM LoanSpcPurpsTbl
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND SpcPurpsLoanCd = 'REVL';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_SpcPurpsLoanCd := NULL;
    END;

    IF (    v_ChkVal = 0
        AND v_LoanAppRevlInd IS NOT NULL
        AND v_LoanAppRevlInd != 'N') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                1029,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF v_ChkVal = 1 THEN
        IF (    v_SpcPurpsLoanCd = 'REVL'
            AND v_LoanAppRevlInd <> 'Y') THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1030,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;

        IF (    v_SpcPurpsLoanCd <> 'REVL'
            AND v_LoanAppRevlInd <> 'N') THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    1031,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    -- validate LoanAppPymtAmt
    IF NVL (v_LoanAppPymtAmt, 0) >= 1000000 THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                1044,
                p_TransInd,
                TO_CHAR (p_LoanAppNmb),
                TO_CHAR (v_LoanAppPymtAmt),
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    --  IF TRIM(UPPER(v_fininstrmnttypind)) IN ('C', 'D') THEN
    --  NULL;
    --  ELSE
    IF v_LOANAPPINTDTLCD IS NULL THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                2004,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    -- END IF;
    -- IF v_PrcsMthdCd = 'SMP'
    -- THEN
    -- BEGIN
    --     loan.PoolPrtpntInfoCsp(p_LoanAppNmb,v_SBAPrtcptnAmt,v_OrgntrPrtcptnAmt,
    --                            v_TtlOutstndngBal);
    -- END;
    -- END IF;
    -- IF v_PrcsMthdCd = 'SMP' and v_SBAPrtcptnAmt > (v_TtlOutstndngBal*0.8)
    -- THEN
    --     p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,926,p_TransInd);
    -- END IF;
    -- IF v_PrcsMthdCd = 'SMP' and v_OrgntrPrtcptnAmt < (v_TtlOutstndngBal*0.05)
    -- THEN
    --     p_ErrSeqNmb :=  ADD_ERROR(p_LoanAppNmb,p_ErrSeqNmb,927,p_TransInd);
    -- END IF;
    /*
    Validation to check Current primary borrower or any of the new loan participants (co-borr/prin/guar)
    are affiliates (all participant types guar/borr and prin) in an
    SLA (existing parameter = PrcsMthdReqLqdCr) loan that was created within 90 days (new parameter)
    that is in Status (PS)� and the new loan is for a processing method that is in the �Processing Method Bared�
    RSURAPA -- 06/03/2013 new validation
    NK-08/10/2016-- OPSMDEV 1018 removed LoanCntlIntInd = 'Y' from LOANAPP.LOANGUARTBL and replaced LOANAFFILIND with LoanCntlIntInd
    */
    BEGIN
        v_IMPrmtrId := 'LqdCrPrivPrcsMthdBarred';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_LqdCrPrivPrcsMthdBarred := v_IMPrmtrValChk;

        IF (v_LqdCrPrivPrcsMthdBarred IS NOT NULL) THEN
            v_IMPrmtrId := 'PrcsMthdReqLqdCr';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_PrcsMthdReqLqdCr := v_IMPrmtrValChk;

            v_IMPrmtrId := 'LqdCrPrivLoanDays';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
            v_LqdCrPrivLoanDays := TO_NUMBER (v_IMPrmtrValChk);

            v_taxid := NULL;

            IF     NVL (v_PrcsMthdReqLqdCr, '999999999') != '999999999'
               AND v_LoanAppRqstAmt > TO_NUMBER (v_PrcsMthdReqLqdCr)
               AND v_LoanAppRcnsdrtnInd <> 'Y' THEN
                BEGIN
                    SELECT taxid
                    INTO v_taxid
                    FROM (
                             SELECT taxid
                             FROM LOANAPP.LOANBORRTBL
                             WHERE     loanappnmb = p_LoanAppNmb
                                   AND LoanCntlIntInd = 'Y'
                             UNION
                             SELECT taxid
                             FROM LOANAPP.LOANGUARTBL
                             WHERE loanappnmb = p_LoanAppNmb
                             -- AND LoanCntlIntInd = 'Y'
                             UNION
                             SELECT p.taxid
                             FROM loanapp.loanprintbl p
                                  JOIN LOANAPP.LOANBUSPRINTBL bp
                                  ON (    p.loanappnmb = bp.loanappnmb
                                      AND p.prinseqnmb = bp.prinseqnmb)
                             WHERE     p.loanappnmb = p_LoanAppNmb
                                   AND bp.LoanCntlIntInd = 'Y'
                         ) t
                    WHERE     taxid IN ((SELECT b.taxid
                                         FROM LOANAPP.LOANAPPTBL a
                                              JOIN LOANAPP.LOANBORRTBL b
                                              ON (a.loanappnmb = b.loanappnmb)
                                         WHERE     a.statcd = 'PS'
                                               AND b.LoanCntlIntInd = 'Y'
                                               AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays)
                                         --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                                         UNION
                                         SELECT g.taxid
                                         FROM LOANAPP.LOANAPPTBL a
                                              JOIN LOANAPP.LOANGUARTBL g
                                              ON (a.loanappnmb = g.loanappnmb)
                                         WHERE     a.statcd = 'PS'                          --AND g.LoanCntlIntInd = 'Y'
                                               AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays)
                                         --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                                         UNION
                                         SELECT p.taxid
                                         FROM LOANAPP.LOANAPPTBL a
                                              JOIN LOANAPP.LOANPRINTBL p
                                              ON (a.loanappnmb = p.loanappnmb)
                                              JOIN LOANAPP.LOANBUSPRINTBL bp
                                              ON (    p.loanappnmb = bp.loanappnmb
                                                  AND p.prinseqnmb = bp.prinseqnmb)
                                         WHERE     a.statcd = 'PS'
                                               AND bp.LoanCntlIntInd = 'Y'
                                               AND a.loanappentrydt > (SYSDATE - v_LqdCrPrivLoanDays) --AND a.prcsmthdcd = v_PrcsMthdReqLqdCr
                                                                                                     ))
                          AND ROWNUM = 1;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_taxid := NULL;
                END;
            END IF;

            IF (v_taxid IS NOT NULL) THEN
                v_IMPrmtrId := 'CrdScrThrshld';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_CrdScrThrshld := v_IMPrmtrValChk;

                v_IMPrmtrId := 'CrdScrThrshldMax';
                v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
                v_CrdScrThrshldMax := TO_NUMBER (v_IMPrmtrValChk);



                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        1053,
                        p_TransInd,
                        TO_CHAR (v_taxid),
                        TO_CHAR (v_LqdCrPrivLoanDays),
                        TO_CHAR (v_CrdScrThrshld),
                        TO_CHAR (v_CrdScrThrshldMax),
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;
    END;

    ---Validation for mandatory Maturity Starts Indicator

    IF     (v_LOANGNTYMATSTIND IS NULL)
       AND (   (    v_PrgrmCd = 'A'
                AND v_undrwritngby = 'SBA')
            OR v_PrgrmCd = 'E') THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                3330,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    ----Validation for mandatory Note date
    IF     (v_PrgrmCd <> 'C')
       AND (v_LOANGNTYMATSTIND = 'N')
       AND (v_LOANGNTYNOTEDT IS NULL) THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                1054,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF v_PrcsMthdCd = 'MLD' THEN
        BEGIN
            v_IMPrmtrId := 'MatMoMax';
            v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, v_LOANGNTYNOTEDT, v_RecovInd);
            v_MatMoMax := GREATEST (TO_NUMBER (v_IMPrmtrValChk), NVL (v_MatMoMax, 0));
        END;
    END IF;

    IF     NVL (v_LoanAppRqstMatMoQty, 0) > NVL (v_MatMoMax, 0)
       AND v_LOANGNTYNOTEDT IS NOT NULL THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4203,
                p_TransInd,
                TO_CHAR (v_LoanAppRqstMatMoQty),
                TO_CHAR (v_MatMoMax),
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_PrgrmCd = 'E'
       AND NVL (v_LOANGNTYMATSTIND, 'N') != 'F' THEN
        p_ErrSeqNmb :=
            ADD_ERROR (
                p_LoanAppNmb,
                p_ErrSeqNmb,
                4048,
                p_TransInd,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                v_FININSTRMNTTYPIND);
    END IF;

    IF     v_PRCSMTHDCD = '504'
       AND v_PrgrmCd = 'E' THEN
        BEGIN
            SELECT SUM (LoanPartLendrAmt)
            INTO v_ThirdPartyLoanAmt
            FROM loanApp.LoanPartLendrTbl p
            WHERE p.loanappnmb = p_loanappnmb;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ThirdPartyLoanAmt := NULL;
        END;

        v_TotProjFinAmt :=
            NVL (v_ThirdPartyLoanAmt, 0) + NVL (v_LoanAppCDCNetDbentrAmt, 0) + NVL (v_LoanAppContribAmt, 0);


        BEGIN
            SELECT 'Y'
            INTO v_IsNewBus
            FROM LOANAPP.LOANAPPTBL
            WHERE     LoanAppNewBusCd IN (0, 1, 2)
                  AND LoanAppNmb = p_LoanAppNmb;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_IsNewBus := 'N';
        END;


        BEGIN
            SELECT 'Y'
            INTO v_ISSpcPurpNAICSCD
            FROM LOANAPP.LOANAPPPROJTBL
            WHERE     NAICSCd IN (SELECT NAICSCd
                                  FROM sbaref.cdcspcpurpsnaicstbl
                                  WHERE     NAICSCDSTRTDT <= SYSDATE
                                        AND (   NAICSCDENDDT IS NULL
                                             OR NAICSCDENDDT > SYSDATE))
                  AND loanappnmb = p_loanappnmb;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_IsSpcPurpNAICSCD := 'N';
        END;

        BEGIN
            IF (    v_IsNewBus = 'Y'
                AND v_IsSpcPurpNAICSCD = 'N') THEN
                v_ReqBorrContPct := 15;
                v_Text1 := 'if it is a new business.';
            ELSIF (    v_IsNewBus = 'N'
                   AND v_IsSpcPurpNAICSCD = 'Y') THEN
                v_ReqBorrContPct := 15;
                v_Text1 := 'if it is a special purpose property.';
            ELSIF (    v_IsNewBus = 'Y'
                   AND v_IsSpcPurpNAICSCD = 'Y') THEN
                v_ReqBorrContPct := 20;
                v_Text1 := 'if it is a special purpose property and also a new business.';
            ELSE
                v_ReqBorrContPct := 10;
                v_Text1 := '.';
            END IF;

            v_ReqBorrContAmt := (v_ReqBorrContPct * V_TotProjFinAmt) / 100;
        END;

        BEGIN
            SELECT IMPrmtrValTxt
            INTO v_IMPrmtrValTxt
            FROM LOANAPP.IMPRMTRVALTBL
            WHERE IMPrmtrSeqNmb = (SELECT IMPrmtrSeqNmb
                                   FROM LOANAPP.IMPrmtrTbl
                                   WHERE IMPrmtrId = 'BorrContrbPrcnt');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;

        v_BorrContPct := LOANAPP.loanamtprcnt (v_LoanAppContribAmt, v_TotProjFinAmt);


        IF v_BorrContPct < v_ReqBorrContPct THEN
            BEGIN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4039,
                        p_TransInd,
                        TRIM (v_ReqBorrContPct),                                        --NVL (v_PreGntyCurrAppvAmt, 0),
                        TRIM (v_Text1),                                                  -- NVL (v_LoanTotUndisbAmt, 0),
                        TRIM (TO_CHAR (v_ReqBorrContAmt, '$99,999,999.99')),                 --NVL (v_LoanOutBalAmt, 0),
                        TRIM (TO_CHAR (v_TotProjFinAmt, '$99,999,999.99')),
                        NULL,
                        v_fininstrmnttypind);
            END;
        END IF;

        BEGIN
            SELECT IMPrmtrValTxt
            INTO v_IMPrmtrValTxt
            FROM LOANAPP.IMPRMTRVALTBL
            WHERE IMPrmtrSeqNmb = (SELECT IMPrmtrSeqNmb
                                   FROM LOANAPP.IMPrmtrTbl
                                   WHERE IMPrmtrId = 'SBAContrbPrcnt');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;

        v_ReqSBAContPct := v_IMPrmtrValTxt;


        v_SBAContPct := LOANAPP.loanamtprcnt (v_LoanAppCDCNetDbentrAmt, v_TotProjFinAmt);

        v_ReqSBAContAmt := (v_ReqSBAContPct * V_TotProjFinAmt) / 100;


        IF v_SBAContPct > v_IMPrmtrValTxt THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    4040,
                    p_TransInd,
                    TRIM (v_SBAContPct),
                    TRIM (v_Text1),
                    TRIM (TO_CHAR (v_ReqSBAContAMT, '$99,999,999.99')),                      --NVL (v_LoanOutBalAmt, 0),
                    TRIM (TO_CHAR (v_TotProjFinAmt, '$99,999,999.99')),
                    v_fininstrmnttypind);
        END IF;
    --  END;
    END IF;

    --Validations for equity requirements
    BEGIN
        BEGIN
            SELECT LQDCRTOTSCR
            INTO v_LQDCRTOTSCR
            FROM LoanApp.LqdCrWSCTbl
            WHERE     loanappnmb = p_loanappnmb
                  AND ROWNUM = 1;
        EXCEPTION
            WHEN OTHERS THEN
                v_LQDCRTOTSCR := NULL;
        END;



        v_IMPrmtrId := 'CrdScrThrshld';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_CrdScrThrshld := TO_NUMBER (v_IMPrmtrValChk);

        v_IMPrmtrId := 'CrdScrThrshldMax';
        v_IMPrmtrValChk := LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId, v_PrcsMthdCd, p_LoanAppRecvDt, v_RecovInd);
        v_CrdScrThrshldMax := TO_NUMBER (v_IMPrmtrValChk);



        IF v_LQDCRTOTSCR < v_CrdScrThrshld THEN
            v_CrdScrInd := 'B';                                                                            --- BAD score
        ELSIF        v_LQDCRTOTSCR >= v_CrdScrThrshld
                 AND (v_LQDCRTOTSCR BETWEEN v_CrdScrThrshld AND v_CrdScrThrshldMax)
              OR (v_LQDCRTOTSCR <= v_CrdScrThrshldMax) THEN
            v_CrdScrInd := 'G';                                                                           --- GOOD Score
        ELSIF v_LQDCRTOTSCR IS NULL THEN
            v_CrdScrInd := 'N';                                                                             --- NO Score
        END IF;

        v_projcost := v_LoanAppRqstAmt + v_InjctnSum;

        IF     v_PrgrmCd = 'A'
           AND (   (    v_LoanAppRqstAmt > 350000
                    AND v_CrdScrInd = 'N')
                OR (    v_LoanAppRqstAmt <= 350000
                    AND v_CrdScrInd = 'B')) THEN
            IF     v_LoanAppNewBusCd = 0
               AND v_InjctnSum < .05 * v_projcost THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4112,
                        p_TransInd,
                        'Application',
                        '5%',
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            IF     v_LoanAppNewBusCd = 9
               AND v_InjctnSum < .1 * v_projcost THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4113,
                        p_TransInd,
                        'Application',
                        '10%',
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;

        IF     v_PRCSMTHDCD NOT IN ('SBX')
           AND v_LOANAPPSBARCMNDAMT <= 350000
           AND v_CrdScrInd = 'G' THEN
            IF     v_LoanAppNewBusCd = 0
               AND v_InjctnSum < .1 * v_projcost THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4112,
                        p_TransInd,
                        'Application',
                        '10%',
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            IF     v_LoanAppNewBusCd = 9
               AND v_InjctnSum < .1 * v_projcost THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4113,
                        p_TransInd,
                        'Application',
                        '10%',
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;
    END;

    /*
       BEGIN
         SELECT SpcPurpsLoanCd
           INTO v_SpcPurpsLoanCd
           FROM LoanSpcPurpsTbl
          WHERE LoanAppNmb = p_LoanAppNmb AND SpcPurpsLoanCd = 'REVL';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_SpcPurpsLoanCd := NULL;
      END;*/


    BEGIN
        IF     v_SpcPurpsLoanCd = 'REVL'
           AND v_PRCSMTHDCD = 'SBX'
           AND v_PrgrmCd = 'A' THEN
            IF v_LoanAppAmortMoIntQty < v_LoanAppRevlMoIntQty THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4126,
                        p_TransInd,
                        'Application',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;


            IF v_LoanAppRqstMatMoQty != v_LoanAppAmortMoIntQty + v_LoanAppRevlMoIntQty THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4129,
                        p_TransInd,
                        'Application',
                        v_LoanAppRqstMatMoQty,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            IF v_LoanAppRevlMoIntQty > (v_LoanAppRqstMatMoQty / 2) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4130,
                        p_TransInd,
                        'Application',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;

        IF v_SpcPurpsLoanCd = 'REVL' THEN
            IF v_LoanAppAmortMoIntQty IS NULL THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4128,
                        p_TransInd,
                        'Application',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;

            IF v_LoanAppRevlMoIntQty IS NULL THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4127,
                        p_TransInd,
                        'Application',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;


        IF     v_SpcPurpsLoanCd = 'REVL'
           AND v_PRCSMTHDCD NOT IN ('SBX')
           AND v_PrgrmCd = 'A' THEN
            IF (v_LoanAppRevlMoIntQty + v_LoanAppAmortMoIntQty) != v_LoanAppRqstMatMoQty THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4129,
                        p_TransInd,
                        'Application',
                        v_LoanAppRqstMatMoQty,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;

        IF     v_PrcsMthdCd IN ('CTR',
                                'SAB',
                                'SGC',
                                'SLC',
                                'STC',
                                '7EW',
                                'CLW',
                                'PLW')
           AND v_PrgrmCd = 'A' THEN
            IF     v_LoanAppExtraServFeeAMT > 0
               AND (   v_LoanAppExtraServFeeInd != 'Y'
                    OR v_LoanAppExtraServFeeInd IS NULL) THEN
                p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4185,
                        p_TransInd,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
            END IF;
        END IF;
    END;

    BEGIN
        IF     v_undrwritngby = 'LNDR'
           AND v_PrgrmCd = 'A' THEN
            BEGIN
                SELECT TAXID
                INTO v_TAXID
                FROM loanborrtbl
                WHERE     loanappnmb = p_loanappnmb
                      AND LoanBusPrimBorrInd = 'Y';

                SELECT PRTID
                INTO v_PRTID
                FROM loanappprttbl
                WHERE loanappnmb = p_loanappnmb;

                SELECT 1
                INTO v_ChkVal
                FROM DUAL
                WHERE EXISTS
                          (SELECT 1
                           FROM loanapptbl a, loanborrtbl b, loanappprttbl p
                           WHERE     statcd IN ('R1',
                                                'R2',
                                                'R3',
                                                'AR',
                                                'WD',
                                                'SO',
                                                'D',
                                                'R')
                                 AND a.loanappnmb = b.loanappnmb
                                 AND a.loanappnmb = p.loanappnmb
                                 AND a.PrgmCd = 'A'
                                 AND a.UNDRWRITNGBY = 'SBA '
                                 AND b.TaxId = v_TaxId
                                 AND b.LoanBusPrimBorrInd = 'Y'
                                 AND p.PrtId = v_PRTID
                                 AND a.LOANAPPUNDRWRITNGEXPRTDT IS NOT NULL
                                 AND v_LoanAppRqstAmt * 0.1 >= ABS (v_LoanAppRqstAmt - a.LoanAppRqstAmt));

                IF v_chkVal = 1 THEN
                    p_ErrSeqNmb :=
                        ADD_ERROR (
                            p_LoanAppNmb,
                            p_ErrSeqNmb,
                            4184,
                            p_TransInd,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            v_FININSTRMNTTYPIND);
                END IF;
            EXCEPTION
                WHEN OTHERS THEN
                    NULL;
            END;
        END IF;
    END;

    IF v_undrwritngby = 'SBA' THEN
        -- Following rewritten for CAFSOPER-2894
        -- if there are two records in sbaref.IMPrgrmAuthOthAgrmtTbl, one with UNDRWRITNGBY = 'LNDR'
        -- and other with UNDRWRITNGBY = 'SBA' for the same PRCSMTHDCD then it is not an error
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM sbaref.IMPrgrmAuthOthAgrmtTbl
        WHERE     PRCSMTHDCD = v_PRCSMTHDCD
              AND UNDRWRITNGBY = 'LNDR'
              AND p_LoanAppRecvDt BETWEEN IMPRGRMAUTHOTHSTRTDT AND NVL (IMPRGRMAUTHOTHENDDT, SYSDATE)
              AND NOT EXISTS
                      (SELECT PRCSMTHDCD
                       FROM sbaref.IMPrgrmAuthOthAgrmtTbl
                       WHERE     PRCSMTHDCD = v_PRCSMTHDCD
                             AND UNDRWRITNGBY = 'SBA'
                             AND p_LoanAppRecvDt BETWEEN IMPRGRMAUTHOTHSTRTDT AND NVL (IMPRGRMAUTHOTHENDDT, SYSDATE));

        IF v_ChkVal > 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    2353,
                    p_TransInd,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;

    IF    (    v_NAICSCD >= 721000
           AND v_NAICSCD < 722000)
       OR (    v_NAICSCD >= 447000
           AND v_NAICSCD < 448000) THEN
        SELECT COUNT (*)
        INTO v_ChkVal
        FROM IMPrmtrTbl a, IMPrmtrValTbl b
        WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
              AND a.IMPrmtrId = 'PrcsMthdNoGasStation'
              AND IMPrmtrValTxt = v_PrcsMthdCd
              AND IMPrmtrValStrtDt <= p_LoanAppRecvDt
              AND p_LoanAppRecvDt < NVL (IMPrmtrValEndDt, SYSDATE) + 1;

        IF v_ChkVal > 0 THEN
            p_ErrSeqNmb :=
                ADD_ERROR (
                    p_LoanAppNmb,
                    p_ErrSeqNmb,
                    664,
                    p_TransInd,
                    TO_CHAR (v_NAICSCD),
                    v_PrcsMthdCd,
                    NULL,
                    NULL,
                    NULL,
                    v_FININSTRMNTTYPIND);
        END IF;
    END IF;
    
    --NAICS code validation for 5EX
    OPEN NAICSCDRNGTBLCUR;
            LOOP
                FETCH NAICSCDRNGTBLCUR INTO NAICSCDRNGTBLCUR_rec;
                EXIT WHEN NAICSCDRNGTBLCUR%NOTFOUND;
                IF ((v_NAICSCd between NAICSCDRNGTBLCUR_rec.NAICSCDSTRTRNG and NAICSCDRNGTBLCUR_rec.NAICSCDENDRNG) and (v_PRCSMTHDCD = NAICSCDRNGTBLCUR_rec.PRCSMTHDCD)) THEN 
                    p_ErrSeqNmb :=
                    ADD_ERROR (
                        p_LoanAppNmb,
                        p_ErrSeqNmb,
                        4294,
                        p_TransInd,
                        TO_CHAR (v_NAICSCd),
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        v_FININSTRMNTTYPIND);
                END IF;
            END LOOP;
            CLOSE NAICSCDRNGTBLCUR;
			
	--Warning message for 5EX
	IF v_PRCSMTHDCD = '5EX' THEN
		p_ErrSeqNmb :=
						ADD_ERROR (
							p_LoanAppNmb,
							p_ErrSeqNmb,
							4293,
							p_TransInd,
							NULL,
							NULL,
							NULL,
							NULL,
							NULL,
							v_FININSTRMNTTYPIND);
	END IF;
	

    /* BEGIN
               SELECT LoanAppInjctnInd  INTO v_LoanAppInjctnInd FROM LoanApp.LoanAppTbl
                WHERE LoanAppNmb = p_LoanAppNmb;

               IF v_LoanAppInjctnInd = 'N' THEN
                         p_ErrSeqNmb := ADD_ERROR(p_LoanAppNmb, p_ErrSeqNmb, 4046, p_TransInd,
                         p_LoanAppNmb,NULL,NULL,NULL,NULL,v_fininstrmnttypind);
               END IF;
            END; */

    p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
    p_RetVal := NVL (p_RetVal, 0);
END;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=ALPExpress
define package_name=UAT_0706
define package_buildtime=20210706115631
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy ALPExpress_UAT_0706 created on Tue 07/06/2021 11:56:32.81 by Jasleen Gorowada
prompt deploy scripts for deploy ALPExpress_UAT_0706 created on Tue 07/06/2021 11:56:32.81 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy ALPExpress_UAT_0706: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\ALP_0706.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\ALP_0706.sql"
--2c
Insert into SBAREF.PRGRMVALIDTBL
   (PRGRMVALIDSEQNMB, PRGRMCD, PRCSMTHDCD, FININSTRMNTTYPIND, SPCPURPSLOANCD, 
    PROCDTYPCD, SUBPRGRMMFCD, PRGRMVALIDSTRTDT, LOANMFSPCPURPSCD, PRGRMVALIDENDDT, 
    LOANMFPRCSMTHDCD, LOANMFSTARIND, LOANMFDISASTRCD, DFLTSPCPURPSIND, PRGRMVALIDCREATUSERID, 
    PRGRMVALIDCREATDT)
 Values
   ((nvl((select max(PrgrmValidSeqNmb) from SBAREF.PRGRMVALIDTBL),0)+1), 'E', '5EX', 'E', 'CAIP', 
    'E', '4011', to_date('01-01-66','mm-dd-yy'), '', '', 
    'C', '', '', '', user, 
    sysdate);
--2f
update SBAREF.IMRTTYPTBL set IMRTTYPSTRTDT=sysdate where PRCSMTHDCD='5EX';
--select *  from SBAREF.IMRTTYPTBL where PRCSMTHDCD='5EX';

--2g
--select * from SBAREF.IMPRGRMAUTHOTHAGRMTTBL where PRCSMTHDCD='5EX';
update SBAREF.IMPRGRMAUTHOTHAGRMTTBL set UNDRWRITNGBY='LNDR' where PRCSMTHDCD='5EX';

--2h
--select * from LOANAPP.IMPRMTRVALTBL where PRCSMTHDCD='5EX' and IMPRMTRSEQNMB in (141,65)

update LOANAPP.IMPRMTRVALTBL set IMPRMTRVALTXT='500000' where IMPRMTRVALTXT='5000000' and PRCSMTHDCD='5EX' and IMPRMTRSEQNMB in (141,65);

commit;


set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

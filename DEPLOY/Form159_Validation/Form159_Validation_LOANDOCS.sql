define deploy_name=Form159_Validation
define package_name=LOANDOCS
define package_buildtime=20201229124123
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_LOANDOCS created on Tue 12/29/2020 12:41:23.81 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_LOANDOCS created on Tue 12/29/2020 12:41:23.81 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_LOANDOCS: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\update_for_defect.sql 
Jasleen Gorowada committed b8c9cb3 on Mon Dec 28 17:46:40 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Tables\LOANAGNTDOCTBL.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSDELTSP.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSINSTSP.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSSELTSP.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSUPDTSP.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Sequences\LOANAGNTDOCTBL_SEQ.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\update_for_defect.sql"
update SBAREF.DOCTYPBUSPRCSMTHDTBL  set DOCTYPCD=1268 where BUSPRCSTYPCD=2 and PRCSMTHDCD='7AG' and DOCTYPVALIDTYPCD='O' and ALLOWMULTIDOCIND='Y' and DOCTYPCD=1267;
commit;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Tables\LOANAGNTDOCTBL.sql"
CREATE TABLE LOANDOCS.LOANAGNTDOCTBL
(
  LOANAPPNMB      NUMBER(10)                    NOT NULL,
  LOANAGNTSEQNMB  NUMBER(10)                    NOT NULL,
  LOANAGNTID      NUMBER(10)                    NOT NULL,
  DOCID           NUMBER(10)                    NOT NULL,
  LOANAGNTDOCID   NUMBER(3)                     NOT NULL,
  CREATUSERID     VARCHAR2(32 BYTE)             DEFAULT user,
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             DEFAULT user,
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
TABLESPACE LOANDOCSDATA01TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;

/


GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO CNTRDANDU;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANDOCS.LOANAGNTDOCTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANDOCS.LOANAGNTDOCTBL TO LOANDOCSDEVROLE;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANORIGHQOVERRIDE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANDOCS.LOANAGNTDOCTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANPRTREAD;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANPRTUPDT;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANREAD;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANDOCS.LOANAGNTDOCTBL TO LOANUPDROLE;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO LOANUPDT;

GRANT SELECT ON LOANDOCS.LOANAGNTDOCTBL TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSDELTSP.sql"
CREATE OR REPLACE procedure LOANDOCS.LOANAGNTDOCSDELTSP
(
	p_IDENTIFIER	NUMBER :=0,
	p_RETVAL OUT NUMBER,
	p_LOANAPPNMB	NUMBER := 0,
	p_LOANAGNTSEQNMB	NUMBER := 0,
	p_LOANAGNTID	NUMBER :=0

)
AS
BEGIN
	SAVEPOINT LOANAGNTDOCSDELTSP;
	if p_IDENTIFIER = 0
	then
		begin
			DELETE FROM  LOANDOCS.LOANAGNTDOCTBL
			WHERE LOANAPPNMB = p_LOANAPPNMB AND
			LOANAGNTSEQNMB = p_LOANAGNTSEQNMB AND
			LOANAGNTID = p_LOANAGNTID;
			p_RETVAL := SQL%ROWCOUNT;
		end;
	end if;
EXCEPTION
	WHEN OTHERS
	THEN
		BEGIN
			RAISE;
			ROLLBACK TO LOANAGNTDOCSDELTSP;
		END;
END LOANAGNTDOCSDELTSP;
/


GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO POOLSECADMINROLE;


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSINSTSP.sql"
CREATE OR REPLACE procedure LOANDOCS.LOANAGNTDOCSINSTSP
(
	p_IDENTIFIER	NUMBER :=0,
	p_RETVAL OUT NUMBER,
	p_LOANAPPNMB	NUMBER := 0,
	p_LOANAGNTSEQNMB	NUMBER := 0,
	p_LOANAGNTID	NUMBER :=0,
	p_DOCID	NUMBER :=0

)
AS
BEGIN
	SAVEPOINT LOANAGNTDOCSINSTSP;
	if p_IDENTIFIER = 0
	then
		begin
			--select LOAN.LOANAGNTDOCTBL_SEQ.nextval into p_LOANAGNTDOCID from dual;
			--p_LOANAGNTDOCID := LOAN.LOANAGNTDOCTBL_SEQ.nextval;

			INSERT INTO LOANDOCS.LOANAGNTDOCTBL (
			LOANAPPNMB, LOANAGNTSEQNMB, LOANAGNTID,
			DOCID, LOANAGNTDOCID) values
			(p_LOANAPPNMB,
			 p_LOANAGNTSEQNMB,
			 p_LOANAGNTID,
			 p_DOCID,
			 (LOANDOCS.LOANAGNTDOCTBL_SEQ.nextval)
			);
			p_RETVAL := SQL%ROWCOUNT;
		end;
	end if;
EXCEPTION
	WHEN OTHERS
	THEN
		BEGIN
			RAISE;
			ROLLBACK TO LOANAGNTDOCSINSTSP;
		END;
END LOANAGNTDOCSINSTSP;
/


GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO POOLSECADMINROLE;


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSSELTSP.sql"
CREATE OR REPLACE procedure LOANDOCS.LOANAGNTDOCSSELTSP
(
	p_IDENTIFIER	NUMBER :=0,
	p_RETVAL OUT NUMBER,
	p_LOANAPPNMB	NUMBER := 0,
	p_LOANAGNTSEQNMB	NUMBER := 0,
	p_LOANAGNTID	NUMBER :=0,
	p_SelCur OUT sys_refcursor

)
AS
BEGIN
	SAVEPOINT LOANAGNTDOCSSELTSP;
	if p_IDENTIFIER = 0
	then
		begin
            open p_SelCur for
                select * from LOANDOCS.LOANAGNTDOCTBL
                WHERE LOANAPPNMB = p_LOANAPPNMB AND
                LOANAGNTSEQNMB = p_LOANAGNTSEQNMB AND
                LOANAGNTID = p_LOANAGNTID;
                p_RETVAL := SQL%ROWCOUNT;
		end;
	end if;
EXCEPTION
	WHEN OTHERS
	THEN
		BEGIN
			RAISE;
			ROLLBACK TO LOANAGNTDOCSSELTSP;
		END;
END LOANAGNTDOCSSELTSP;
/


GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO POOLSECADMINROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Procedures\LOANAGNTDOCSUPDTSP.sql"
CREATE OR REPLACE procedure LOANDOCS.LOANAGNTDOCSUPDTSP
(
	p_IDENTIFIER	NUMBER :=0,
	p_RETVAL OUT NUMBER,
	p_LOANAPPNMB	NUMBER := 0,
	p_LOANAGNTSEQNMB	NUMBER := 0,
	p_LOANAGNTID	NUMBER :=0,
	p_DOCID	NUMBER :=0

)
AS
BEGIN
	SAVEPOINT LOANAGNTDOCSUPDTSP;
	if p_IDENTIFIER = 0
	then
		begin
			UPDATE LOANDOCS.LOANAGNTDOCTBL
			SET LOANAGNTSEQNMB = p_LOANAGNTSEQNMB,
				LOANAGNTID = p_LOANAGNTID,
				DOCID	= p_DOCID
			WHERE LOANAPPNMB = p_LOANAPPNMB;
			p_RETVAL := SQL%ROWCOUNT;
		end;
	end if;
EXCEPTION
	WHEN OTHERS
	THEN
		BEGIN
			RAISE;
			ROLLBACK TO LOANAGNTDOCSUPDTSP;
		END;
END LOANAGNTDOCSUPDTSP;
/

GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANDOCS.DOCUPDTSP TO POOLSECADMINROLE;


-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\LOANDOCS\Sequences\LOANAGNTDOCTBL_SEQ.sql"
CREATE SEQUENCE LOANDOCS.LOANAGNTDOCTBL_SEQ
  START WITH 191
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER
  NOKEEP
  GLOBAL;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

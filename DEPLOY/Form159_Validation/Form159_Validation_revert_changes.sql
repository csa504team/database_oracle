define deploy_name=Form159_Validation
define package_name=revert_changes
define package_buildtime=20201124131605
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_revert_changes created on Tue 11/24/2020 13:16:07.50 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_revert_changes created on Tue 11/24/2020 13:16:07.50 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_revert_changes: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\drop_textfield_otheragent.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\delete_OTHER_LOANAGNTTYPCDTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\UAT_delete_records_LOANVALIDATIONERRTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_VALIDATEAGNTCSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTSELTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTINSTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTUPDTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\delete_record_VALIDATIONEXCPTNTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\DROP_LOANAGNTDOCTBL_SEQ.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\DROP_LOANAGNTDOCTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\DROP_PROCEDURES.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\drop_textfield_otheragent.sql"
ALTER TABLE LOAN.LOANAGNTTBL 
DROP COLUMN LOANAGNTOTHTYPTXT;
commit;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\delete_OTHER_LOANAGNTTYPCDTBL.sql"
DELETE from SBAREF.LOANAGNTTYPCDTBL where LOANAGNTTYPCD=7;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\UAT_delete_records_LOANVALIDATIONERRTBL.sql"
DELETE FROM LOANAPP.LOANVALIDATIONERRTBL where ERRCD in (4238,4239,4240,4241,4242,4243,4244,4245,4246,4247,4248,4249);
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_VALIDATEAGNTCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.VALIDATEAGNTCSP ( p_Identifier     IN     NUMBER := 0,
                                                    p_RetVal         OUT NUMBER,
                                                     p_LoanAppNmb  IN     NUMBER DEFAULT NULL,
                                                     p_ErrSeqNmb   IN OUT NUMBER,
                                                     p_TransInd    IN     NUMBER DEFAULT NULL,
                                                     p_LOANAGNTSeqNmb IN NUMBER DEFAULT NULL,
                                                     p_VALIDATIONTYP  IN     CHAR := NULL,
                                                     p_SelCur1           OUT SYS_REFCURSOR,
                                                     p_SelCur2           OUT SYS_REFCURSOR)

AS
--SS--07/27/2018 --OPSMDEV1861
--BR--10/26/2018--CAFSOPER2199- removed NOT from NULL analysis for both LOANAGNTAPPCNTPAIDAMT and LOANAGNTSBALENDRPAIDAMT because error should only fires if BOTH are null;
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns
--BR--12/12/2018--CAFSOPER2345--Added  'B' to if then else list for v_LOANAGNTBUSPERIND for error code 4173, 4174

   v_LoanAgntSeqNmb       NUMBER (10);
   v_LOANAGNTBUSPERIND    CHAR (1);
   v_LOANAGNTNM           VARCHAR2 (200);
   v_LOANAGNTCNTCTFIRSTNM VARCHAR2 (40);
   v_LOANAGNTCNTCTMIDNM   CHAR (1);
   v_LOANAGNTCNTCTLASTNM  VARCHAR2 (40);
   v_LOANAGNTCNTCTSFXNM   VARCHAR2 (4);
   v_LOANAGNTADDRSTR1NM   VARCHAR2 (80);
   v_LOANAGNTADDRSTR2NM   VARCHAR2 (80);
   v_LOANAGNTADDRCTYNM    VARCHAR2 (40);
   v_LOANAGNTADDRSTCD     CHAR (2);
   v_LOANAGNTADDRSTNM     VARCHAR2 (60);
   v_LOANAGNTADDRZIPCD    CHAR (5);
   v_LOANAGNTADDRZIP4CD   CHAR (4);
   v_LOANAGNTADDRPOSTCD   VARCHAR2 (20);
   v_LOANAGNTADDRCNTCD    CHAR (2);
   v_LOANAGNTTYPCD        NUMBER (3);
   v_LOANAGNTDOCUPLDIND   CHAR (1);
   v_LOANCDCTPLFEEIND     CHAR (1);
   v_LOANCDCTPLFEEAMT     NUMBER (15, 2);
   v_PRGMCD               CHAR (2);
   v_LOANAGNTSERVTYPCD   Number(3);
   v_ChkVal      Number;
   v_LOANAGNTAPPCNTPAIDAMT   Number(15,2);
   v_LOANAGNTSBALENDRPAIDAMT  Number(15,2);
   v_chkval1   Number;
   v_ErrCd                 NUMBER (10, 2);
   v_feedtlcnt NUMBER;

   CURSOR Agnt_Cur
   IS
      SELECT LoanAgntSeqNmb,
             LOANAGNTBUSPERIND,
             LOANAGNTNM,
             OcaDataOut(LOANAGNTCNTCTFIRSTNM),
             LOANAGNTCNTCTMIDNM,
             OcaDataOut(LOANAGNTCNTCTLASTNM),
             LOANAGNTCNTCTSFXNM,
             OcaDataOut(LOANAGNTADDRSTR1NM),
             OcaDataOut(LOANAGNTADDRSTR2NM),
             LOANAGNTADDRCTYNM,
             LOANAGNTADDRSTCD,
             LOANAGNTADDRSTNM,
             LOANAGNTADDRZIPCD,
             LOANAGNTADDRZIP4CD,
             LOANAGNTADDRPOSTCD,
             LOANAGNTADDRCNTCD,
             LOANAGNTTYPCD,
             LOANAGNTDOCUPLDIND,
             LOANCDCTPLFEEIND,
             LOANCDCTPLFEEAMT
      FROM   loanagnttbl
      WHERE  loanappnmb = p_loanappnmb
      and   LOANAGNTSeqNmb=  p_LOANAGNTSeqNmb;
        BEGIN
   p_ErrSeqNmb := 0;

   IF p_Identifier < 100 Then
      DELETE LoanGntyValidErrTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   END IF;    
--Begin

  BEGIN
      SELECT PRGMCD
      INTO   v_PRGMCD
      FROM   LoanGntytbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PRGMCD := NULL;
   END;
        

 
   OPEN Agnt_Cur;
   FETCH Agnt_Cur
      INTO v_LoanAgntSeqNmb,
           v_LOANAGNTBUSPERIND,
           v_LOANAGNTNM,
           v_LOANAGNTCNTCTFIRSTNM,
           v_LOANAGNTCNTCTMIDNM,
           v_LOANAGNTCNTCTLASTNM,
           v_LOANAGNTCNTCTSFXNM,
           v_LOANAGNTADDRSTR1NM,
           v_LOANAGNTADDRSTR2NM,
           v_LOANAGNTADDRCTYNM,
           v_LOANAGNTADDRSTCD,
           v_LOANAGNTADDRSTNM,
           v_LOANAGNTADDRZIPCD,
           v_LOANAGNTADDRZIP4CD,
           v_LOANAGNTADDRPOSTCD,
           v_LOANAGNTADDRCNTCD,
           v_LOANAGNTTYPCD,
           v_LOANAGNTDOCUPLDIND,
           v_LOANCDCTPLFEEIND,
           v_LOANCDCTPLFEEAMT;
          
   
     BEGIN
      SELECT COUNT (loanappnmb)
      INTO   v_chkval1
      FROM   loan.loangntytbl a
      WHERE      a.loanappnmb = p_loanappnmb
             AND a.LOANAGNTINVLVDIND = 'Y'
             
             AND (   v_PRGMCD = 'A'
                  OR v_PRGMCD = 'E')
             AND a.loanappnmb NOT IN (SELECT Loanappnmb FROM LOAN.LOANAGNTTBL);

      IF v_chkval1 > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4182;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
   END;
   
                  
   
   v_feedtlcnt := 0;
   
   FOR c2 IN (SELECT loanagntservtypcd, LOANAGNTSERVOTHTYPTXT,LOANAGNTAPPCNTPAIDAMT,LOANAGNTSBALENDRPAIDAMT
              FROM   LoanAgntFeeDtlTbl
              WHERE  Loanappnmb = p_Loanappnmb
               and   LOANAGNTSeqNmb = p_LOANAGNTSeqNmb 
               and (LOANAGNTAPPCNTPAIDAMT IS NOT NULL OR LOANAGNTSBALENDRPAIDAMT IS NOT NULL))
   LOOP
         v_feedtlcnt := v_feedtlcnt + 1;
         
         IF c2.LOANAGNTAPPCNTPAIDAMT IS NULL AND c2.LOANAGNTSBALENDRPAIDAMT IS NULL THEN
             NULL;
              BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4172;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;--add error for both amount is not null
         END IF;
         
      IF     c2.loanagntservtypcd = 5 and c2.LOANAGNTSERVOTHTYPTXT IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4181;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
   END LOOP;

 
   WHILE (Agnt_Cur%FOUND)
   LOOP
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND = 'B'
         AND v_LOANAGNTNM IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4170;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND  v_LOANAGNTTYPCD IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4171;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
     
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTFIRSTNM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4173;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;  
      
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTLASTNM   is null
      THEN
      BEgin
       
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4174;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTR1NM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4175;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRCTYNM   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4176;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTCD   is null
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4177;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIPCD   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4178;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                        
                                         );
               END;
      END IF;       
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIP4CD   is null
      THEN
        BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4179;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN  ('E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANCDCTPLFEEIND='Y' Then
            if  v_LOANCDCTPLFEEAMT is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4180;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      END IF;  
    
     
      
     FETCH Agnt_cur
         INTO v_LoanAgntSeqNmb,
              v_LOANAGNTBUSPERIND,
              v_LOANAGNTNM,
              v_LOANAGNTCNTCTFIRSTNM,
              v_LOANAGNTCNTCTMIDNM,
              v_LOANAGNTCNTCTLASTNM,
              v_LOANAGNTCNTCTSFXNM,
              v_LOANAGNTADDRSTR1NM,
              v_LOANAGNTADDRSTR2NM,
              v_LOANAGNTADDRCTYNM,
              v_LOANAGNTADDRSTCD,
              v_LOANAGNTADDRSTNM,
              v_LOANAGNTADDRZIPCD,
              v_LOANAGNTADDRZIP4CD,
              v_LOANAGNTADDRPOSTCD,
              v_LOANAGNTADDRCNTCD,
              v_LOANAGNTTYPCD,
              v_LOANAGNTDOCUPLDIND,
              v_LOANCDCTPLFEEIND,
              v_LOANCDCTPLFEEAMT;
   END LOOP;

   CLOSE Agnt_Cur;
 
    IF    (p_Identifier = 11)
      OR (p_Identifier = 111)
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'F'
            ORDER BY LoanGntyValidErrSeqNmb;

         OPEN p_SelCur2 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'W'
            ORDER BY LoanGntyValidErrSeqNmb;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
   END;
/

--GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEREADALLROLE;

--GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANAPPUPDAPPROLE;

--GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAGNTSELTSP (
   p_Identifier       IN     NUMBER := 0,
   p_LoanAppNmb       IN     NUMBER := 0,
   p_LoanAgntSeqNmb   IN     NUMBER := 0,
   p_SelCur              OUT SYS_REFCURSOR,
   p_RetVal              OUT NUMBER)
AS
--SS--OPSMDEV1862--07/16/2018--Collecting Agent data
--RY- 11/15/2018--OPSMDEV-2044:: Added OCADATAOUT for PII columns
BEGIN
   IF p_Identifier = 0
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE LOANAPPNMB = p_LOANAPPNMB;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 2
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT 1
              FROM LOAN.LoanAgntTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 11
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE     LOANAPPNMB = p_LOANAPPNMB
                   AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 12
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE     LOANAPPNMB = p_LOANAPPNMB
                   AND LoanAgntSeqNmb > NVL (p_LoanAgntSeqNmb, 0);

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_identifier = 13
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT a.LOANAPPNMB,
                   a.LOANAGNTTYPCD,
                   a.LOANAGNTBUSPERIND,
                   a.LOANAGNTNM,
                      OcaDataOut (a.LOANAGNTCNTCTFIRSTNM)
                   || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                   || ' '
                   || OcaDataOut (a.LOANAGNTCNTCTLastNm)
                   || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                      AS LoanContactName,
                      OcaDataOut (a.LOANAGNTADDRSTR1NM)
                   || RTRIM (' ' || OcaDataOut (a.LOANAGNTADDRSTR2NM))
                   || ' '
                   || a.LOANAGNTADDRCTYNM
                   || ' '
                   || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                   || ' '
                   || a.LOANAGNTADDRZIPCD
                      AS LoanContactaddress,
                   a.LOANCDCTPLFEEIND,
                   a.LOANCDCTPLFEEAMT,
                   b.LOANAGNTSERVTYPCD,
                   b.LOANAGNTSERVOTHTYPTXT,
                   b.LOANAGNTAPPCNTPAIDAMT,
                   b.LOANAGNTSBALENDRPAIDAMT,
                   c.Prgmcd,
                   c.LoanAppNm,
                   c.LoanNmb,
                   d.PrtlglNm,
                   e.loanpartlendrtypcd,
                   e.loanpartlendrnm,
                      e.loanpartlendrstr1nm
                   || RTRIM (' ' || e.loanpartlendrstr2nm)
                   || ' '
                   || e.loanpartlendrctynm
                   || ' '
                   || RTRIM (' ' || e.loanpartlendrstcd)
                   || ' '
                   || e.loanpartlendrcntrycd
                   || ' '
                   || RTRIM (' ' || e.loanpartlendrzip5cd)
                      AS LendrAddress,
                   e.LOCID
              FROM loanagnttbl a,
                   loanagntfeedtltbl b,
                   loangntytbl c,
                   Partner.PrtTbl d,
                   LoanGntyPartLendrtbl e
             WHERE     a.loanappnmb = b.loanappnmb
                   AND b.loanappnmb = c.loanappnmb
                   AND a.LOANAPPNMB = p_loanappnmb
                   AND c.PrtId = d.PrtId
                   AND e.loanappnmb(+) = a.loanappnmb
                   AND a.LoanAgntSeqNmb = p_LoanAgntSeqNmb
                   AND a.LoanAgntSeqNmb = b.LoanAgntSeqNmb;
      END;
   ELSIF p_Identifier = 38
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANAGNTID,
                   TO_CHAR (LOANCDCTPLFEEAMT, 'FM999999999999990.00')
                      LOANCDCTPLFEEAMT
              FROM LOAN.LoanAgntTbl
             WHERE LOANAPPNMB = p_LOANAPPNMB;


         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;
END;
/

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAgntINSTSP (
   p_identifier             IN     NUMBER := 0,
   p_retval                    OUT NUMBER,
   p_LOANAPPNMB                    NUMBER := NULL,
   p_LOANAGNTSEQNMB            OUT NUMBER,
   p_LOANAGNTBUSPERIND             CHAR := NULL,
   p_LOANAGNTNM                    VARCHAR2 := NULL,
   p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
   p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
   p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
   p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
   p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
   p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
   p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
   p_LOANAGNTADDRSTCD              CHAR := NULL,
   p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
   p_LOANAGNTADDRZIPCD             CHAR := NULL,
   p_LOANAGNTADDRZIP4CD            CHAR := NULL,
   p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
   p_LOANAGNTADDRCNTCD             CHAR := NULL,
   p_LOANAGNTTYPCD                 NUMBER := NULL,
   p_LOANAGNTDOCUPLDIND            CHAR := NULL,
   p_LOANCDCTPLFEEIND              CHAR := NULL,
   p_LOANCDCTPLFEEAMT              NUMBER := NULL,
   p_LOANAGNTID                    NUMBER := NULL,
   p_CREATUSERID                   VARCHAR2 := NULL,
   p_CREATDT                       DATE := NULL,
   p_LASTUPDTUSERID                VARCHAR2 := NULL,
   p_LASTUPDTDT                    DATE := NULL)
AS
/*SS--07/16/2018--OPSMDEV1862
--08/2/2018--OPSMDEV 1873 Added LoanAgntId
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAIN for PII columns
*/
BEGIN
   SAVEPOINT LoanAgntINS;

   IF p_Identifier = 0
   THEN
      BEGIN
         INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                       LoanAgntSeqNmb,
                                       LOANAGNTBUSPERIND,
                                       LOANAGNTNM,
                                       LOANAGNTCNTCTFIRSTNM,
                                       LOANAGNTCNTCTMIDNM,
                                       LOANAGNTCNTCTLASTNM,
                                       LOANAGNTCNTCTSFXNM,
                                       LOANAGNTADDRSTR1NM,
                                       LOANAGNTADDRSTR2NM,
                                       LOANAGNTADDRCTYNM,
                                       LOANAGNTADDRSTCD,
                                       LOANAGNTADDRSTNM,
                                       LOANAGNTADDRZIPCD,
                                       LOANAGNTADDRZIP4CD,
                                       LOANAGNTADDRPOSTCD,
                                       LOANAGNTADDRCNTCD,
                                       LOANAGNTTYPCD,
                                       LOANAGNTDOCUPLDIND,
                                       LOANCDCTPLFEEIND,
                                       LOANCDCTPLFEEAMT,
                                       LOANAGNTID,
                                       CreatUserId,
                                       CreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
                 VALUES (
                           p_LOANAPPNMB,
                           (SELECT NVL (MAX (LoanAgntSeqNmb), 0) + 1
                              FROM LOAN.LoanAgntTBL z
                             WHERE z.loanappnmb = p_loanappnmb),
                           p_LOANAGNTBUSPERIND,
                           p_LOANAGNTNM,
                           OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                           p_LOANAGNTCNTCTMIDNM,
                           OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                           p_LOANAGNTCNTCTSFXNM,
                           OcaDataIn (p_LOANAGNTADDRSTR1NM),
                           OcaDataIn (p_LOANAGNTADDRSTR2NM),
                           p_LOANAGNTADDRCTYNM,
                           p_LOANAGNTADDRSTCD,
                           p_LOANAGNTADDRSTNM,
                           p_LOANAGNTADDRZIPCD,
                           p_LOANAGNTADDRZIP4CD,
                           p_LOANAGNTADDRPOSTCD,
                           p_LOANAGNTADDRCNTCD,
                           p_LOANAGNTTYPCD,
                           p_LOANAGNTDOCUPLDIND,
                           p_LOANCDCTPLFEEIND,
                           p_LOANCDCTPLFEEAMT,
                           (SELECT NVL (MAX (LoanAgntID), 0) + 1
                              FROM LOAN.LoanAgntInfoCdTBL),
                           p_CreatUserId,
                           SYSDATE,
                           p_LastUpdtUserId,
                           SYSDATE);



         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 11
   THEN
      BEGIN
         INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                       LoanAgntSeqNmb,
                                       LOANAGNTBUSPERIND,
                                       LOANAGNTNM,
                                       LOANAGNTCNTCTFIRSTNM,
                                       LOANAGNTCNTCTMIDNM,
                                       LOANAGNTCNTCTLASTNM,
                                       LOANAGNTCNTCTSFXNM,
                                       LOANAGNTADDRSTR1NM,
                                       LOANAGNTADDRSTR2NM,
                                       LOANAGNTADDRCTYNM,
                                       LOANAGNTADDRSTCD,
                                       LOANAGNTADDRSTNM,
                                       LOANAGNTADDRZIPCD,
                                       LOANAGNTADDRZIP4CD,
                                       LOANAGNTADDRPOSTCD,
                                       LOANAGNTADDRCNTCD,
                                       LOANAGNTTYPCD,
                                       LOANAGNTDOCUPLDIND,
                                       LOANCDCTPLFEEIND,
                                       LOANCDCTPLFEEAMT,
                                       LOANAGNTID,
                                       CreatUserId,
                                       CreatDt,
                                       LastUpdtUserId,
                                       LastUpdtDt)
            SELECT p_LOANAPPNMB,
                   (  NVL ( (SELECT MAX (LoanAgntSeqNmb)
                               FROM loan.loanagnttbl z
                              WHERE z.loanappnmb = p_loanappnmb),
                           0)
                    + 1)
                      AS loanagntseqnmb,
                   p_LOANAGNTBUSPERIND,
                   p_LOANAGNTNM,
                   OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                   p_LOANAGNTCNTCTMIDNM,
                   OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                   p_LOANAGNTCNTCTSFXNM,
                   OcaDataIn (p_LOANAGNTADDRSTR1NM),
                   OcaDataIn (p_LOANAGNTADDRSTR2NM),
                   p_LOANAGNTADDRCTYNM,
                   p_LOANAGNTADDRSTCD,
                   p_LOANAGNTADDRSTNM,
                   p_LOANAGNTADDRZIPCD,
                   p_LOANAGNTADDRZIP4CD,
                   p_LOANAGNTADDRPOSTCD,
                   p_LOANAGNTADDRCNTCD,
                   p_LOANAGNTTYPCD,
                   p_LOANAGNTDOCUPLDIND,
                   p_LOANCDCTPLFEEIND,
                   p_LOANCDCTPLFEEAMT,
                   (SELECT NVL (MAX (LoanAgntID), 0) + 1
                      FROM LOAN.LoanAgntInfoCdTBL),
                   p_CreatUserId,
                   SYSDATE,
                   p_LastUpdtUserId,
                   SYSDATE
              FROM DUAL;

         p_retval := SQL%ROWCOUNT;



         SELECT MAX (LoanAgntSeqNmb)
           INTO p_retval
           FROM LoanAgntTBL
          WHERE loanappnmb = p_loanappnmb;
      END;
   END IF;

   p_retval := NVL (p_retval, 0);
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         RAISE;
         ROLLBACK TO LoanAgntINS;
      END;
END LoanAgntINSTSP;
/

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTINSTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\UAT_LOANAGNTUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAgntUPDTSP (
   p_identifier             IN     NUMBER := 0,
   p_retval                    OUT NUMBER,
   p_LOANAPPNMB                    NUMBER := NULL,
   p_LOANAGNTSEQNMB                NUMBER := NULL,
   p_LOANAGNTBUSPERIND             CHAR := NULL,
   p_LOANAGNTNM                    VARCHAR2 := NULL,
   p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
   p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
   p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
   p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
   p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
   p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
   p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
   p_LOANAGNTADDRSTCD              CHAR := NULL,
   p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
   p_LOANAGNTADDRZIPCD             CHAR := NULL,
   p_LOANAGNTADDRZIP4CD            CHAR := NULL,
   p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
   p_LOANAGNTADDRCNTCD             CHAR := NULL,
   p_LOANAGNTTYPCD                 NUMBER := NULL,
   p_LOANAGNTDOCUPLDIND            CHAR := NULL,
   p_LOANCDCTPLFEEIND              CHAR := NULL,
   p_LOANCDCTPLFEEAMT              NUMBER := NULL,
   p_LOANAGNTID                    NUMBER := NULL,
   p_LastupdtuserId                VARCHAR2 := NULL)
AS
--SS--07/18/2018--OPSMDEV 1861
--RY- 11/15/2018--OPSMDEV:: Added OCADATAIN for PII columns
BEGIN
   SAVEPOINT LoanAgntUPDTSP;

   IF p_identifier = 0
   THEN
      BEGIN
         UPDATE LOAN.LoanAgntTBL
            SET LOANAGNTBUSPERIND = p_LOANAGNTBUSPERIND,
                LOANAGNTNM = p_LOANAGNTNM,
                LOANAGNTCNTCTFIRSTNM = OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                LOANAGNTCNTCTMIDNM = p_LOANAGNTCNTCTMIDNM,
                LOANAGNTCNTCTLASTNM = OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                LOANAGNTCNTCTSFXNM = p_LOANAGNTCNTCTSFXNM,
                LOANAGNTADDRSTR1NM = OcaDataIn (p_LOANAGNTADDRSTR1NM),
                LOANAGNTADDRSTR2NM = OcaDataIn (p_LOANAGNTADDRSTR2NM),
                LOANAGNTADDRCTYNM = p_LOANAGNTADDRCTYNM,
                LOANAGNTADDRSTCD = p_LOANAGNTADDRSTCD,
                LOANAGNTADDRSTNM = p_LOANAGNTADDRSTNM,
                LOANAGNTADDRZIPCD = p_LOANAGNTADDRZIPCD,
                LOANAGNTADDRZIP4CD = p_LOANAGNTADDRZIP4CD,
                LOANAGNTADDRPOSTCD = p_LOANAGNTADDRPOSTCD,
                LOANAGNTADDRCNTCD = p_LOANAGNTADDRCNTCD,
                LOANAGNTTYPCD = p_LOANAGNTTYPCD,
                LOANAGNTDOCUPLDIND = p_LOANAGNTDOCUPLDIND,
                LOANCDCTPLFEEIND = p_LOANCDCTPLFEEIND,
                LOANCDCTPLFEEAMT = p_LOANCDCTPLFEEAMT,
                LOANAGNTID = p_LOANAGNTID,
                Lastupdtuserid = p_lastupdtuserId,
                LastupdtDt = SYSDATE
          WHERE     LOANAPPNMB = p_LOANAPPNMB
                AND LOANAGNTSEQNMB = p_LOANAGNTSEQNMB;


         p_retval := SQL%ROWCOUNT;
      END;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK TO LoanAgntUPDTSP;
      RAISE;
END;
/

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTUPDTSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\delete_record_VALIDATIONEXCPTNTBL.sql"
delete from LOANAPP.VALIDATIONEXCPTNTBL where ERRCD=4244;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Sequences\DROP_LOANAGNTDOCTBL_SEQ.sql"
DROP SEQUENCE LOAN.LOANAGNTDOCTBL_SEQ;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Tables\DROP_LOANAGNTDOCTBL.sql"
DROP TABLE LOAN.LOANAGNTDOCTBL;
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\DROP_PROCEDURES.sql"
DROP PROCEDURE LOAN.LOANAGNTDOCSSELTSP;
DROP PROCEDURE LOAN.LOANAGNTDOCSINSTSP;
DROP PROCEDURE LOAN.LOANAGNTDOCSUPDTSP;
DROP PROCEDURE LOAN.LOANAGNTDOCSDELTSP;
DROP PROCEDURE LOAN.LOANAGNTSELCSP;
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

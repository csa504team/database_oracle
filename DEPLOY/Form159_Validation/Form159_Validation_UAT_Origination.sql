define deploy_name=Form159_Validation
define package_name=UAT_Origination
define package_buildtime=20210112171816
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_UAT_Origination created on Tue 01/12/2021 17:18:17.11 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_UAT_Origination created on Tue 01/12/2021 17:18:17.11 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_UAT_Origination: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_textfield_otheragent.sql 
Jasleen Gorowada committed 6849f65 on Mon Dec 14 09:23:49 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTSELTSP.sql 
Jasleen Gorowada committed 3ea0b16 on Thu Oct 22 14:14:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTINSTSP.sql 
Jasleen Gorowada committed 3ea0b16 on Thu Oct 22 14:14:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTUPDTSP.sql 
Jasleen Gorowada committed 3ea0b16 on Thu Oct 22 14:14:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTSELCSP.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL_ORIG.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\update_DOCTYPBUSPRCSMTHDTBL_ORIG.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_textfield_otheragent.sql"
ALTER TABLE LOANAPP.LOANAGNTTBL 
ADD LOANAGNTOTHTYPTXT varchar2(80);
commit;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LoanAGNTSELTSP (p_Identifier       IN     NUMBER := 0,
                                                    p_LoanAppNmb       IN     NUMBER := 0,
                                                    p_LoanAgntSeqNmb   IN     NUMBER := 0,
                                                    p_SelCur              OUT SYS_REFCURSOR,
                                                    p_RetVal              OUT NUMBER)
AS
/*SS--OPSMDEV1862--07/16/2018--Collecting Agent data
SS--OPSMDEV 1874 added identifier 13
SS--11/15/2018 added function for encryption

*/

BEGIN
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 2
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT 1
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LoanAppNmb = p_LoanAppNmb AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt                                     
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 12
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt
                                    
                                FROM LOANAPP.LoanAgntTbl 
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb > NVL (p_LoanAgntSeqNmb, 0);
                              

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_identifier = 13
    THEN
        BEGIN
            OPEN p_SelCur FOR
                SELECT a.LOANAPPNMB,
                       a.LOANAGNTTYPCD,
                       a.LOANAGNTBUSPERIND,
                       a.LOANAGNTNM,
                          ocadataout (a.LOANAGNTCNTCTFIRSTNM)
                       || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                       || ' '
                       || ocadataout (a.LOANAGNTCNTCTLastNm)
                       || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                           AS LoanContactName,
                          ocadataout (a.LOANAGNTADDRSTR1NM)
                       || RTRIM (' ' || ocadataout (a.LOANAGNTADDRSTR2NM))
                       || ' '
                       || a.LOANAGNTADDRCTYNM
                       || ' '
                       || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                       || ' '
                       || a.LOANAGNTADDRZIPCD
                           AS LoanContactaddress,
                       a.LOANCDCTPLFEEIND,
                       a.LOANCDCTPLFEEAMT,
                       a.LOANAGNTOTHTYPTXT,
                       b.LOANAGNTSERVTYPCD,
                       b.LOANAGNTSERVOTHTYPTXT,
                       b.LOANAGNTAPPCNTPAIDAMT,
                       b.LOANAGNTSBALENDRPAIDAMT,
                       c.Prgmcd,
                       c.LoanAppNm,
                       c.LoanNmb,
                       d.LoanAppPrtNm,
                       e.loanpartlendrtypcd,
                       e.loanpartlendrnm,
                          e.loanpartlendrstr1nm
                       || RTRIM (' ' || e.loanpartlendrstr2nm)
                       || ' '
                       || e.loanpartlendrctynm
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrstcd)
                       || ' '
                       || e.loanpartlendrcntrycd
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrzip5cd)
                           AS LendrAddress,
                       e.LOCID
                  FROM loanagnttbl        a,
                       loanagntfeedtltbl  b,
                       loanapptbl         c,
                       LoanAppPrtTbl      d,
                       loanpartlendrtbl   e
                 WHERE     a.loanappnmb = b.loanappnmb
                       AND b.loanappnmb = c.loanappnmb
                       AND a.LOANAPPNMB = p_LOANAPPNMB
                       AND a.loanappnmb = d.loanappnmb
                       AND e.LOANAPPNMB(+) = a.LOANAPPNMB
                       AND a.LoanAgntSeqNmb = p_LoanAgntSeqNmb
                       AND a.LoanAgntSeqNmb = b.LoanAgntSeqNmb;
        END;
    ELSIF p_Identifier = 38
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)                      LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)                       LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)                        LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)                        LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     TO_CHAR (LOANCDCTPLFEEAMT, 'FM999999999999990.00')     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;
END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LoanAgntINSTSP (p_identifier             IN     NUMBER := 0,
                                                    p_retval                    OUT NUMBER,
                                                    p_LOANAPPNMB                    NUMBER := NULL,
                                                    p_LOANAGNTSEQNMB            OUT NUMBER,
                                                    p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                    p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                    p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                    p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                    p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                    p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                    p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                    p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                    p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                    p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                    p_LOANAGENTTYPEOTHER            VARCHAR2 := NULL,
                                                    p_LOANAGNTID                    NUMBER := NULL,
                                                    p_CREATUSERID                   VARCHAR2 := NULL,
                                                    p_CREATDT                       DATE := NULL,
                                                    p_LASTUPDTUSERID                VARCHAR2 := NULL,
                                                    p_LASTUPDTDT                    DATE := NULL)
AS
--SS--07/16/2018--OPSMDEV1862
--SS--11/15/2018--OPSMDEV   Added function for encryption
BEGIN
    SAVEPOINT LoanAgntINS;

    IF p_Identifier = 0
    THEN
        BEGIN
            INSERT INTO LOANAPP.LoanAgntTBL (LOANAPPNMB,
                                             LoanAgntSeqNmb,
                                             LOANAGNTBUSPERIND,
                                             LOANAGNTNM,
                                             LOANAGNTCNTCTFIRSTNM,
                                             LOANAGNTCNTCTMIDNM,
                                             LOANAGNTCNTCTLASTNM,
                                             LOANAGNTCNTCTSFXNM,
                                             LOANAGNTADDRSTR1NM,
                                             LOANAGNTADDRSTR2NM,
                                             LOANAGNTADDRCTYNM,
                                             LOANAGNTADDRSTCD,
                                             LOANAGNTADDRSTNM,
                                             LOANAGNTADDRZIPCD,
                                             LOANAGNTADDRZIP4CD,
                                             LOANAGNTADDRPOSTCD,
                                             LOANAGNTADDRCNTCD,
                                             LOANAGNTTYPCD,
                                             LOANAGNTDOCUPLDIND,
                                             LOANCDCTPLFEEIND,
                                             LOANCDCTPLFEEAMT,
                                             LOANAGNTID,
                                             LOANAGNTOTHTYPTXT,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
                 VALUES (p_LOANAPPNMB,
                         (SELECT NVL (MAX (LoanAgntSeqNmb), 0) + 1
                            FROM LOANAPP.LoanAgntTBL z
                           WHERE z.loanappnmb = p_loanappnmb),
                         p_LOANAGNTBUSPERIND,
                         p_LOANAGNTNM,
                         ocadatain (p_LOANAGNTCNTCTFIRSTNM),
                         p_LOANAGNTCNTCTMIDNM,
                         ocadatain (p_LOANAGNTCNTCTLASTNM),
                         p_LOANAGNTCNTCTSFXNM,
                         ocadatain (p_LOANAGNTADDRSTR1NM),
                         ocadatain (p_LOANAGNTADDRSTR2NM),
                         p_LOANAGNTADDRCTYNM,
                         p_LOANAGNTADDRSTCD,
                         p_LOANAGNTADDRSTNM,
                         p_LOANAGNTADDRZIPCD,
                         p_LOANAGNTADDRZIP4CD,
                         p_LOANAGNTADDRPOSTCD,
                         p_LOANAGNTADDRCNTCD,
                         p_LOANAGNTTYPCD,
                         p_LOANAGNTDOCUPLDIND,
                         p_LOANCDCTPLFEEIND,
                         p_LOANCDCTPLFEEAMT,
                         (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                         p_LOANAGENTTYPEOTHER,
                         p_CreatUserId,
                         SYSDATE,
                         p_LastUpdtUserId,
                         SYSDATE);

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            INSERT INTO LOANAPP.LoanAgntTBL (LOANAPPNMB,
                                             LoanAgntSeqNmb,
                                             LOANAGNTBUSPERIND,
                                             LOANAGNTNM,
                                             LOANAGNTCNTCTFIRSTNM,
                                             LOANAGNTCNTCTMIDNM,
                                             LOANAGNTCNTCTLASTNM,
                                             LOANAGNTCNTCTSFXNM,
                                             LOANAGNTADDRSTR1NM,
                                             LOANAGNTADDRSTR2NM,
                                             LOANAGNTADDRCTYNM,
                                             LOANAGNTADDRSTCD,
                                             LOANAGNTADDRSTNM,
                                             LOANAGNTADDRZIPCD,
                                             LOANAGNTADDRZIP4CD,
                                             LOANAGNTADDRPOSTCD,
                                             LOANAGNTADDRCNTCD,
                                             LOANAGNTTYPCD,
                                             LOANAGNTDOCUPLDIND,
                                             LOANCDCTPLFEEIND,
                                             LOANCDCTPLFEEAMT,
                                             LOANAGNTID,
                                             LOANAGNTOTHTYPTXT,
                                             CreatUserId,
                                             CreatDt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
                SELECT p_LOANAPPNMB,
                       (  NVL ((SELECT MAX (LoanAgntSeqNmb)
                                  FROM loanapp.loanagnttbl z
                                 WHERE z.loanappnmb = p_loanappnmb),
                               0)
                        + 1)
                           AS loanagntseqnmb,
                       p_LOANAGNTBUSPERIND,
                       p_LOANAGNTNM,
                       ocadatain (p_LOANAGNTCNTCTFIRSTNM),
                       p_LOANAGNTCNTCTMIDNM,
                       ocadatain (p_LOANAGNTCNTCTLASTNM),
                       p_LOANAGNTCNTCTSFXNM,
                       ocadatain (p_LOANAGNTADDRSTR1NM),
                       ocadatain (p_LOANAGNTADDRSTR2NM),
                       p_LOANAGNTADDRCTYNM,
                       p_LOANAGNTADDRSTCD,
                       p_LOANAGNTADDRSTNM,
                       p_LOANAGNTADDRZIPCD,
                       p_LOANAGNTADDRZIP4CD,
                       p_LOANAGNTADDRPOSTCD,
                       p_LOANAGNTADDRCNTCD,
                       p_LOANAGNTTYPCD,
                       p_LOANAGNTDOCUPLDIND,
                       p_LOANCDCTPLFEEIND,
                       p_LOANCDCTPLFEEAMT,
                       (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                       p_LOANAGENTTYPEOTHER,
                       p_CreatUserId,
                       SYSDATE,
                       p_LastUpdtUserId,
                       SYSDATE
                  FROM DUAL;

            p_retval := SQL%ROWCOUNT;



            SELECT MAX (LoanAgntSeqNmb)
              INTO p_retval
              FROM LoanAgntTBL
             WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    p_retval := NVL (p_retval, 0);
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO LoanAgntINS;
        END;
END LoanAgntINSTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LoanAgntUPDTSP (p_identifier             IN     NUMBER := 0,
                                                    p_retval                    OUT NUMBER,
                                                    p_LOANAPPNMB                    NUMBER := NULL,
                                                    p_LOANAGNTSEQNMB                NUMBER := NULL,
                                                    p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                    p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                    p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                    p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                    p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                    p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                    p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                    p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                    p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                    p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                    p_LOANAGNTID                    NUMBER := NULL,
                                                    p_LOANAGENTTYPEOTHER            VARCHAR2 := NULL,
                                                    p_Lastupdtuserid                VARCHAR2 := NULL)
AS
--SS--7/16/2018--OPSMDEV 1862
--SS--11/15/2018 Added function for encryption
BEGIN
    SAVEPOINT LoanAgntUPDTSP;

    IF p_identifier = 0
    THEN
        BEGIN
            UPDATE LOANAPP.LoanAgntTBL
               SET LOANAGNTBUSPERIND = p_LOANAGNTBUSPERIND,
                   LOANAGNTNM = p_LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM = ocadatain (p_LOANAGNTCNTCTFIRSTNM),
                   LOANAGNTCNTCTMIDNM = p_LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM = ocadatain (p_LOANAGNTCNTCTLASTNM),
                   LOANAGNTCNTCTSFXNM = p_LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM = ocadatain (p_LOANAGNTADDRSTR1NM),
                   LOANAGNTADDRSTR2NM = ocadatain (p_LOANAGNTADDRSTR2NM),
                   LOANAGNTADDRCTYNM = p_LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD = p_LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM = p_LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD = p_LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD = p_LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD = p_LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD = p_LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD = p_LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND = p_LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND = p_LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT = p_LOANCDCTPLFEEAMT,
                   LOANAGNTID = p_LOANAGNTID,
                   LOANAGNTOTHTYPTXT = p_LOANAGENTTYPEOTHER,
                   Lastupdtuserid = p_Lastupdtuserid,
                   LastupdtDt = SYSDATE
             WHERE LOANAPPNMB = p_LOANAPPNMB AND LOANAGNTSEQNMB = p_LOANAGNTSEQNMB;


            p_retval := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        ROLLBACK TO LoanAgntUPDTSP;
        RAISE;
END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Procedures\LOANAGNTSELCSP.sql"
CREATE OR REPLACE procedure LOANAPP.LOANAGNTSELCSP
(p_Identifier 	IN NUMBER  := 0,
p_LocId in NUMBER :=0,
p_StartDt	IN DATE := null,
p_EndDt	IN DATE := null,
p_LoanNmb IN CHAR := null,
p_AgntNm IN VARCHAR2 := null,
p_RetVal  	OUT NUMBER,
p_SelCur 	OUT SYS_REFCURSOR)
as
begin
  if(p_Identifier =0) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM)) as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission


       FROM
       LOANAPP.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOANAPP.LOANBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOANAPP.LOANBUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B' and  a.LOANAPPNMB = bb.LOANAPPNMB
       left join LOANAPP.LOANPERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P' and  a.LOANAPPNMB = bp.LOANAPPNMB
       left join LOANAPP.LOANAPPTBL l on l.LOANAPPNMB = a.LOANAPPNMB 
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       (lp.LOCID =  p_LocId or p_LocId is null) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null) and
        ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null))
       and rownum<=10000
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    elsif (p_Identifier=1) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM ))as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOANAPP.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOANAPP.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission

       FROM
       LOANAPP.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOANAPP.LOANBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOANAPP.LOANBUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B' and  a.LOANAPPNMB = bb.LOANAPPNMB
       left join LOANAPP.LOANPERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P' and  a.LOANAPPNMB = bp.LOANAPPNMB
       left join LOANAPP.LOANAPPTBL  l on l.LOANAPPNMB = a.LOANAPPNMB
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       lp.LOCID=p_LocId and
        ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null)) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null)
       and rownum<=10000
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    end if;
end;
/


GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO CNTRACHANNURI;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO CNTRDFREDERICKS;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANLOOKUPUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANPOSTSERVRECVUPDT;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANPOSTSERVUPDT;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO READLOANROLE;

GRANT EXECUTE ON LOANAPP.LOANAGNTSELCSP TO UPDLOANROLE;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL_ORIG.sql"
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 1,
 '7AG',
 sysdate,
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\update_DOCTYPBUSPRCSMTHDTBL_ORIG.sql"
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null where DOCTYPCD=927 and   PRCSMTHDCD='7AG' and BUSPRCSTYPCD=1;
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

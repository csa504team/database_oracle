define deploy_name=Form159_Validation
define package_name=PROD_CAOB_Origination
define package_buildtime=20210226112045
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_PROD_CAOB_Origination created on Fri 02/26/2021 11:20:46.76 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_PROD_CAOB_Origination created on Fri 02/26/2021 11:20:46.77 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_PROD_CAOB_Origination: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL_ORIG.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\update_DOCTYPBUSPRCSMTHDTBL_ORIG.sql 
Jasleen Gorowada committed 9fae0a2 on Wed Jan 20 16:04:46 2021 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL_ORIG.sql"
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 1,
 '7AG',
 sysdate,
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\update_DOCTYPBUSPRCSMTHDTBL_ORIG.sql"
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null where DOCTYPCD=927 and   PRCSMTHDCD='7AG' and BUSPRCSTYPCD=1;
commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

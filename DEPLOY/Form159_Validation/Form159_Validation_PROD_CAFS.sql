define deploy_name=Form159_Validation
define package_name=PROD_CAFS
define package_buildtime=20210226111701
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_PROD_CAFS created on Fri 02/26/2021 11:17:02.94 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_PROD_CAFS created on Fri 02/26/2021 11:17:02.94 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_PROD_CAFS: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_textfield_otheragent.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_OTHER_LOANAGNTTYPCDTBL.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTSELTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTINSTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTUPDTSP.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\UAT_add_records_LOANVALIDATIONERRTBL.sql 
Jasleen Gorowada committed bdefba8 on Thu Oct 29 16:10:18 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL_1.sql 
Jasleen Gorowada committed e51738e on Mon Dec 21 15:30:56 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPTBL.sql 
Jasleen Gorowada committed fe2a37e on Tue Nov 3 19:44:53 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL.sql 
Jasleen Gorowada committed b36dada on Tue Nov 10 18:00:24 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATEAGNTCSP.sql 
Jasleen Gorowada committed 571eae9 on Thu Dec 24 14:55:34 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTSELCSP.sql 
Jasleen Gorowada committed 97fce0f on Wed Dec 30 09:29:46 2020 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_textfield_otheragent.sql"
ALTER TABLE LOAN.LOANAGNTTBL 
ADD LOANAGNTOTHTYPTXT varchar2(80);
commit;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_OTHER_LOANAGNTTYPCDTBL.sql"
INSERT INTO SBAREF.LOANAGNTTYPCDTBL (
   LOANAGNTTYPCD, LOANAGNTTYPDESCTXT, LOANAGNTTYPSTRTDT, 
   LOANAGNTTYPENDDT, CREATUSERID, CREATDT, 
   LASTUPDTUSERID, LASTUPDTDT) 
VALUES ( 7,
 'Other',
 trunc(sysdate),
 null,
 user,
 sysdate,
 user,
 sysdate );
commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAGNTSELTSP (p_Identifier       IN     NUMBER := 0,
                                                 p_LoanAppNmb       IN     NUMBER := 0,
                                                 p_LoanAgntSeqNmb   IN     NUMBER := 0,
                                                 p_SelCur              OUT SYS_REFCURSOR,
                                                 p_RetVal              OUT NUMBER)
AS
--SS--OPSMDEV1862--07/16/2018--Collecting Agent data
--RY- 11/15/2018--OPSMDEV-2044:: Added OCADATAOUT for PII columns
BEGIN
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     OcaDataOut (LOANAGNTCNTCTFIRSTNM)     AS LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     OcaDataOut (LOANAGNTCNTCTLASTNM)      AS LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     OcaDataOut (LOANAGNTADDRSTR1NM)       AS LOANAGNTADDRSTR1NM,
                                     OcaDataOut (LOANAGNTADDRSTR2NM)       AS LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTID,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt,
                                     LOANAGNTOTHTYPTXT
                                FROM LOAN.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 2
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT 1
                                FROM LOAN.LoanAgntTbl
                               WHERE LoanAppNmb = p_LoanAppNmb AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     OcaDataOut (LOANAGNTCNTCTFIRSTNM)     AS LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     OcaDataOut (LOANAGNTCNTCTLASTNM)      AS LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     OcaDataOut (LOANAGNTADDRSTR1NM)       AS LOANAGNTADDRSTR1NM,
                                     OcaDataOut (LOANAGNTADDRSTR2NM)       AS LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTID,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt,
                                     LOANAGNTOTHTYPTXT
                                FROM LOAN.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 12
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     OcaDataOut (LOANAGNTCNTCTFIRSTNM)     AS LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     OcaDataOut (LOANAGNTCNTCTLASTNM)      AS LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     OcaDataOut (LOANAGNTADDRSTR1NM)       AS LOANAGNTADDRSTR1NM,
                                     OcaDataOut (LOANAGNTADDRSTR2NM)       AS LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTID,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt,
                                     LOANAGNTOTHTYPTXT
                                FROM LOAN.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb > NVL (p_LoanAgntSeqNmb, 0);

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_identifier = 13
    THEN
        BEGIN
            OPEN p_SelCur FOR
                SELECT a.LOANAPPNMB,
                       a.LOANAGNTTYPCD,
                       a.LOANAGNTBUSPERIND,
                       a.LOANAGNTNM,
                          OcaDataOut (a.LOANAGNTCNTCTFIRSTNM)
                       || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                       || ' '
                       || OcaDataOut (a.LOANAGNTCNTCTLastNm)
                       || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                           AS LoanContactName,
                          OcaDataOut (a.LOANAGNTADDRSTR1NM)
                       || RTRIM (' ' || OcaDataOut (a.LOANAGNTADDRSTR2NM))
                       || ' '
                       || a.LOANAGNTADDRCTYNM
                       || ' '
                       || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                       || ' '
                       || a.LOANAGNTADDRZIPCD
                           AS LoanContactaddress,
                       a.LOANCDCTPLFEEIND,
                       a.LOANCDCTPLFEEAMT,
                       a.LOANAGNTOTHTYPTXT,
                       b.LOANAGNTSERVTYPCD,
                       b.LOANAGNTSERVOTHTYPTXT,
                       b.LOANAGNTAPPCNTPAIDAMT,
                       b.LOANAGNTSBALENDRPAIDAMT,
                       c.Prgmcd,
                       c.LoanAppNm,
                       c.LoanNmb,
                       d.PrtlglNm,
                       e.loanpartlendrtypcd,
                       e.loanpartlendrnm,
                          e.loanpartlendrstr1nm
                       || RTRIM (' ' || e.loanpartlendrstr2nm)
                       || ' '
                       || e.loanpartlendrctynm
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrstcd)
                       || ' '
                       || e.loanpartlendrcntrycd
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrzip5cd)
                           AS LendrAddress,
                       e.LOCID
                  FROM loanagnttbl           a,
                       loanagntfeedtltbl     b,
                       loangntytbl           c,
                       Partner.PrtTbl        d,
                       LoanGntyPartLendrtbl  e
                 WHERE     a.loanappnmb = b.loanappnmb
                       AND b.loanappnmb = c.loanappnmb
                       AND a.LOANAPPNMB = p_loanappnmb
                       AND c.PrtId = d.PrtId
                       AND e.loanappnmb(+) = a.loanappnmb
                       AND a.LoanAgntSeqNmb = p_LoanAgntSeqNmb
                       AND a.LoanAgntSeqNmb = b.LoanAgntSeqNmb;
        END;
    ELSIF p_Identifier = 38
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     OcaDataOut (LOANAGNTCNTCTFIRSTNM)                      AS LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     OcaDataOut (LOANAGNTCNTCTLASTNM)                       AS LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     OcaDataOut (LOANAGNTADDRSTR1NM)                        AS LOANAGNTADDRSTR1NM,
                                     OcaDataOut (LOANAGNTADDRSTR2NM)                        AS LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANAGNTID,
                                     TO_CHAR (LOANCDCTPLFEEAMT, 'FM999999999999990.00')     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT
                                FROM LOAN.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;


            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;
END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAgntINSTSP (p_identifier             IN     NUMBER := 0,
                                                 p_retval                    OUT NUMBER,
                                                 p_LOANAPPNMB                    NUMBER := NULL,
                                                 p_LOANAGNTSEQNMB            OUT NUMBER,
                                                 p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                 p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                 p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                 p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                 p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                 p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                 p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                 p_LOANAGENTTYPEOTHER             VARCHAR2 := NULL,
                                                 p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                 p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                 p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                 p_LOANAGNTID                    NUMBER := NULL,
                                                 p_CREATUSERID                   VARCHAR2 := NULL,
                                                 p_CREATDT                       DATE := NULL,
                                                 p_LASTUPDTUSERID                VARCHAR2 := NULL,
                                                 p_LASTUPDTDT                    DATE := NULL)
AS
/*SS--07/16/2018--OPSMDEV1862
--08/2/2018--OPSMDEV 1873 Added LoanAgntId
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAIN for PII columns
*/
BEGIN
    SAVEPOINT LoanAgntINS;

    IF p_Identifier = 0
    THEN
        BEGIN
            INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                          LoanAgntSeqNmb,
                                          LOANAGNTBUSPERIND,
                                          LOANAGNTNM,
                                          LOANAGNTCNTCTFIRSTNM,
                                          LOANAGNTCNTCTMIDNM,
                                          LOANAGNTCNTCTLASTNM,
                                          LOANAGNTCNTCTSFXNM,
                                          LOANAGNTADDRSTR1NM,
                                          LOANAGNTADDRSTR2NM,
                                          LOANAGNTADDRCTYNM,
                                          LOANAGNTADDRSTCD,
                                          LOANAGNTADDRSTNM,
                                          LOANAGNTADDRZIPCD,
                                          LOANAGNTADDRZIP4CD,
                                          LOANAGNTADDRPOSTCD,
                                          LOANAGNTADDRCNTCD,
                                          LOANAGNTTYPCD,
                                          LOANAGNTDOCUPLDIND,
                                          LOANCDCTPLFEEIND,
                                          LOANCDCTPLFEEAMT,
                                          LOANAGNTID,
                                          CreatUserId,
                                          CreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LOANAGNTOTHTYPTXT)
                 VALUES (p_LOANAPPNMB,
                         (SELECT NVL (MAX (LoanAgntSeqNmb), 0) + 1
                            FROM LOAN.LoanAgntTBL z
                           WHERE z.loanappnmb = p_loanappnmb),
                         p_LOANAGNTBUSPERIND,
                         p_LOANAGNTNM,
                         OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                         p_LOANAGNTCNTCTMIDNM,
                         OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                         p_LOANAGNTCNTCTSFXNM,
                         OcaDataIn (p_LOANAGNTADDRSTR1NM),
                         OcaDataIn (p_LOANAGNTADDRSTR2NM),
                         p_LOANAGNTADDRCTYNM,
                         p_LOANAGNTADDRSTCD,
                         p_LOANAGNTADDRSTNM,
                         p_LOANAGNTADDRZIPCD,
                         p_LOANAGNTADDRZIP4CD,
                         p_LOANAGNTADDRPOSTCD,
                         p_LOANAGNTADDRCNTCD,
                         p_LOANAGNTTYPCD,
                         p_LOANAGNTDOCUPLDIND,
                         p_LOANCDCTPLFEEIND,
                         p_LOANCDCTPLFEEAMT,
                         (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                         p_CreatUserId,
                         SYSDATE,
                         p_LastUpdtUserId,
                         SYSDATE,
                         p_LOANAGENTTYPEOTHER);

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                          LoanAgntSeqNmb,
                                          LOANAGNTBUSPERIND,
                                          LOANAGNTNM,
                                          LOANAGNTCNTCTFIRSTNM,
                                          LOANAGNTCNTCTMIDNM,
                                          LOANAGNTCNTCTLASTNM,
                                          LOANAGNTCNTCTSFXNM,
                                          LOANAGNTADDRSTR1NM,
                                          LOANAGNTADDRSTR2NM,
                                          LOANAGNTADDRCTYNM,
                                          LOANAGNTADDRSTCD,
                                          LOANAGNTADDRSTNM,
                                          LOANAGNTADDRZIPCD,
                                          LOANAGNTADDRZIP4CD,
                                          LOANAGNTADDRPOSTCD,
                                          LOANAGNTADDRCNTCD,
                                          LOANAGNTTYPCD,
                                          LOANAGNTDOCUPLDIND,
                                          LOANCDCTPLFEEIND,
                                          LOANCDCTPLFEEAMT,
                                          LOANAGNTID,
                                          CreatUserId,
                                          CreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LOANAGNTOTHTYPTXT)
                SELECT p_LOANAPPNMB,
                       (  NVL ((SELECT MAX (LoanAgntSeqNmb)
                                  FROM loan.loanagnttbl z
                                 WHERE z.loanappnmb = p_loanappnmb),
                               0)
                        + 1)
                           AS loanagntseqnmb,
                       p_LOANAGNTBUSPERIND,
                       p_LOANAGNTNM,
                       OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                       p_LOANAGNTCNTCTMIDNM,
                       OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                       p_LOANAGNTCNTCTSFXNM,
                       OcaDataIn (p_LOANAGNTADDRSTR1NM),
                       OcaDataIn (p_LOANAGNTADDRSTR2NM),
                       p_LOANAGNTADDRCTYNM,
                       p_LOANAGNTADDRSTCD,
                       p_LOANAGNTADDRSTNM,
                       p_LOANAGNTADDRZIPCD,
                       p_LOANAGNTADDRZIP4CD,
                       p_LOANAGNTADDRPOSTCD,
                       p_LOANAGNTADDRCNTCD,
                       p_LOANAGNTTYPCD,
                       p_LOANAGNTDOCUPLDIND,
                       p_LOANCDCTPLFEEIND,
                       p_LOANCDCTPLFEEAMT,
                       (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                       p_CreatUserId,
                       SYSDATE,
                       p_LastUpdtUserId,
                       SYSDATE,
                       p_LOANAGENTTYPEOTHER
                  FROM DUAL;

            p_retval := SQL%ROWCOUNT;

            SELECT MAX (LoanAgntSeqNmb)
              INTO p_retval
              FROM LoanAgntTBL
             WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    p_retval := NVL (p_retval, 0);
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO LoanAgntINS;
        END;
END LoanAgntINSTSP;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.LoanAgntUPDTSP (p_identifier             IN     NUMBER := 0,
                                                 p_retval                    OUT NUMBER,
                                                 p_LOANAPPNMB                    NUMBER := NULL,
                                                 p_LOANAGNTSEQNMB                NUMBER := NULL,
                                                 p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                 p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                 p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                 p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                 p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                 p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                 p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                 p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                 p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                 p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                 p_LOANAGNTID                    NUMBER := NULL,
                                                 p_LastupdtuserId                VARCHAR2 := NULL,
                                                 p_LOANAGENTTYPEOTHER             VARCHAR2 :=NULL)
AS
--SS--07/18/2018--OPSMDEV 1861
--RY- 11/15/2018--OPSMDEV:: Added OCADATAIN for PII columns
BEGIN
    SAVEPOINT LoanAgntUPDTSP;

    IF p_identifier = 0
    THEN
        BEGIN
            UPDATE LOAN.LoanAgntTBL
               SET LOANAGNTBUSPERIND = p_LOANAGNTBUSPERIND,
                   LOANAGNTNM = p_LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM = OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                   LOANAGNTCNTCTMIDNM = p_LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM = OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                   LOANAGNTCNTCTSFXNM = p_LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM = OcaDataIn (p_LOANAGNTADDRSTR1NM),
                   LOANAGNTADDRSTR2NM = OcaDataIn (p_LOANAGNTADDRSTR2NM),
                   LOANAGNTADDRCTYNM = p_LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD = p_LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM = p_LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD = p_LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD = p_LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD = p_LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD = p_LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD = p_LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND = p_LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND = p_LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT = p_LOANCDCTPLFEEAMT,
                   LOANAGNTID = p_LOANAGNTID,
                   Lastupdtuserid = p_lastupdtuserId,
                   LastupdtDt = SYSDATE,
                   LOANAGNTOTHTYPTXT = p_LOANAGENTTYPEOTHER
             WHERE LOANAPPNMB = p_LOANAPPNMB AND LOANAGNTSEQNMB = p_LOANAGNTSEQNMB;


            p_retval := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        ROLLBACK TO LoanAgntUPDTSP;
        RAISE;
END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\UAT_add_records_LOANVALIDATIONERRTBL.sql"
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4238,
 1,
 'For Agent^1, Loan packaging has an amount paid by Applicant and SBA Lender. The loan packaging amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4239,
 1,
 'For Agent^1, Financial statement preparation has an amount paid by Applicant and SBA Lender. The Financial statement preparation amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4240,
 1,
 'For Agent^1, Broker or Referral services has an amount paid by Applicant and SBA Lender. The Broker or Referral services amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
  INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4241,
 1,
 'For Agent^1, Consultant services has an amount paid by Applicant and SBA Lender. The Consultant services amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4242,
 1,
 'For Agent^1, Other has an amount paid by Applicant and SBA Lender. The Other amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4243,
 1,
 'For  Agent^1,  Description is mandatory for other type of agent.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4245,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Loan Packaging service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4246,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Financial Statement service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4247,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Broker or Referal services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );

INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4248,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Consultant services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4249,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Other services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL_1.sql  "
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4257,
 1,
 'For Agent^1, Amount paid for services should not be blank or non-numeric.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4258,
 1,
 'For Agent^1, Agent Zipcode must contain exactly 5 characters.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPTBL.sql "
INSERT INTO SBAREF.DOCTYPTBL (
   DOCTYPCD, DOCTYPDESCTXT, DOCTYPSTRTDT, 
   DOCTYPENDDT, CREATUSERID, CREATDT, 
   TAB, DOCTYPPREAPPIND) 
VALUES ( 1258,
 'T10 - SBA Form 159 - Itemization and Supporting Documentation',
 trunc(sysdate),
 null,
 user,
 trunc(sysdate),
 'T10',
 null );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL.sql  "
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 2,
 '7AG',
 sysdate,
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATEAGNTCSP.sql"
CREATE OR REPLACE PROCEDURE LOAN.VALIDATEAGNTCSP ( p_Identifier     IN     NUMBER := 0,
                                                    p_RetVal         OUT NUMBER,
                                                     p_LoanAppNmb  IN     NUMBER DEFAULT NULL,
                                                     p_ErrSeqNmb   IN OUT NUMBER,
                                                     p_TransInd    IN     NUMBER DEFAULT NULL,
                                                     p_LOANAGNTSeqNmb IN NUMBER DEFAULT NULL,
                                                     p_VALIDATIONTYP  IN     CHAR := NULL,
                                                     p_SelCur1           OUT SYS_REFCURSOR,
                                                     p_SelCur2           OUT SYS_REFCURSOR)

AS
--SS--07/27/2018 --OPSMDEV1861
--BR--10/26/2018--CAFSOPER2199- removed NOT from NULL analysis for both LOANAGNTAPPCNTPAIDAMT and LOANAGNTSBALENDRPAIDAMT because error should only fires if BOTH are null;
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns
--BR--12/12/2018--CAFSOPER2345--Added  'B' to if then else list for v_LOANAGNTBUSPERIND for error code 4173, 4174

   v_LoanAgntSeqNmb       NUMBER (10);
   v_LOANAGNTBUSPERIND    CHAR (1);
   v_LOANAGNTNM           VARCHAR2 (200);
   v_LOANAGNTCNTCTFIRSTNM VARCHAR2 (40);
   v_LOANAGNTCNTCTMIDNM   CHAR (1);
   v_LOANAGNTCNTCTLASTNM  VARCHAR2 (40);
   v_LOANAGNTCNTCTSFXNM   VARCHAR2 (4);
   v_LOANAGNTADDRSTR1NM   VARCHAR2 (80);
   v_LOANAGNTADDRSTR2NM   VARCHAR2 (80);
   v_LOANAGNTADDRCTYNM    VARCHAR2 (40);
   v_LOANAGNTADDRSTCD     CHAR (2);
   v_LOANAGNTADDRSTNM     VARCHAR2 (60);
   v_LOANAGNTADDRZIPCD    CHAR (5);
   v_LOANAGNTADDRZIP4CD   CHAR (4);
   v_LOANAGNTADDRPOSTCD   VARCHAR2 (20);
   v_LOANAGNTADDRCNTCD    CHAR (2);
   v_LOANAGNTTYPCD        NUMBER (3);
   v_LOANAGNTDOCUPLDIND   CHAR (1);
   v_LOANCDCTPLFEEIND     CHAR (1);
   v_LOANCDCTPLFEEAMT     NUMBER (15, 2);
   v_LOANAGENTTYPEOTHER VARCHAR2(80);
   v_PRGMCD               CHAR (2);
   v_LOANAGNTSERVTYPCD   Number(3);
   v_ChkVal      Number;
   v_LOANAGNTAPPCNTPAIDAMT   Number(15,2);
   v_LOANAGNTSBALENDRPAIDAMT  Number(15,2);
   v_chkval1   Number;
   v_ErrCd                 NUMBER (10, 2);
   v_feedtlcnt NUMBER;
   v_total_appcnt_paid_amt NUMBER;
   v_total_lndr_paid_amt NUMBER;
   v_total_docs NUMBER;


   CURSOR Agnt_Cur
   IS
      SELECT LoanAgntSeqNmb,
             LOANAGNTBUSPERIND,
             LOANAGNTNM,
             OcaDataOut(LOANAGNTCNTCTFIRSTNM) LOANAGNTCNTCTFIRSTNM,
             LOANAGNTCNTCTMIDNM,
             OcaDataOut(LOANAGNTCNTCTLASTNM) LOANAGNTCNTCTLASTNM,
             LOANAGNTCNTCTSFXNM,
             OcaDataOut(LOANAGNTADDRSTR1NM) LOANAGNTADDRSTR1NM,
             OcaDataOut(LOANAGNTADDRSTR2NM) LOANAGNTADDRSTR2NM,
             LOANAGNTADDRCTYNM,
             LOANAGNTADDRSTCD,
             LOANAGNTADDRSTNM,
             LOANAGNTADDRZIPCD,
             LOANAGNTADDRZIP4CD,
             LOANAGNTADDRPOSTCD,
             LOANAGNTADDRCNTCD,
             LOANAGNTTYPCD,
             LOANAGNTDOCUPLDIND,
             LOANCDCTPLFEEIND,
             LOANCDCTPLFEEAMT,
			 LOANAGNTOTHTYPTXT
      FROM   loanagnttbl
      WHERE  loanappnmb = p_loanappnmb
      and   LOANAGNTSeqNmb=  p_LOANAGNTSeqNmb;

	cursor sel_loanangtfeedtl_appcnt
    is
        select sum(LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL where LOANAPPNMB=p_LoanAppNmb and LOANAGNTSEQNMB=p_LOANAGNTSeqNmb
        group by LOANAPPNMB;

	cursor sel_loanangtfeedtl_lndr
    is
        select sum(LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL where LOANAPPNMB=p_LoanAppNmb and LOANAGNTSEQNMB=p_LOANAGNTSeqNmb
        group by LOANAPPNMB;

    cursor count_Docs
    is
        select count(LOANAGNTDOCID) from LOANDOCS.LOANAGNTDOCTBL@caob1 d, LOAN.LOANAGNTTBL a where a.LOANAPPNMB=p_LoanAppNmb and a.LOANAGNTSEQNMB=p_LOANAGNTSeqNmb and a.LOANAGNTID=d.LOANAGNTID;


        BEGIN
   p_ErrSeqNmb := 0;

   IF p_Identifier < 100 Then
      DELETE LoanGntyValidErrTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   END IF;
--Begin

  BEGIN
      SELECT PRGMCD
      INTO   v_PRGMCD
      FROM   LoanGntytbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PRGMCD := NULL;
   END;



   OPEN Agnt_Cur;
   FETCH Agnt_Cur
      INTO v_LoanAgntSeqNmb,
           v_LOANAGNTBUSPERIND,
           v_LOANAGNTNM,
           v_LOANAGNTCNTCTFIRSTNM,
           v_LOANAGNTCNTCTMIDNM,
           v_LOANAGNTCNTCTLASTNM,
           v_LOANAGNTCNTCTSFXNM,
           v_LOANAGNTADDRSTR1NM,
           v_LOANAGNTADDRSTR2NM,
           v_LOANAGNTADDRCTYNM,
           v_LOANAGNTADDRSTCD,
           v_LOANAGNTADDRSTNM,
           v_LOANAGNTADDRZIPCD,
           v_LOANAGNTADDRZIP4CD,
           v_LOANAGNTADDRPOSTCD,
           v_LOANAGNTADDRCNTCD,
           v_LOANAGNTTYPCD,
           v_LOANAGNTDOCUPLDIND,
           v_LOANCDCTPLFEEIND,
           v_LOANCDCTPLFEEAMT,
		   v_LOANAGENTTYPEOTHER;


	open sel_loanangtfeedtl_appcnt;
    fetch sel_loanangtfeedtl_appcnt
    into  v_total_appcnt_paid_amt;


	open sel_loanangtfeedtl_lndr;
    fetch sel_loanangtfeedtl_lndr
    into  v_total_lndr_paid_amt;

    open count_Docs;
    fetch count_Docs
    into v_total_docs;

     BEGIN
      SELECT COUNT (loanappnmb)
      INTO   v_chkval1
      FROM   loan.loangntytbl a
      WHERE      a.loanappnmb = p_loanappnmb
             AND a.LOANAGNTINVLVDIND = 'Y'

             AND (   v_PRGMCD = 'A'
                  OR v_PRGMCD = 'E')
             AND a.loanappnmb NOT IN (SELECT Loanappnmb FROM LOAN.LOANAGNTTBL);

      IF v_chkval1 > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4182;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
   END;



   v_feedtlcnt := 0;

   FOR c2 IN (SELECT loanagntservtypcd, LOANAGNTSERVOTHTYPTXT,LOANAGNTAPPCNTPAIDAMT,LOANAGNTSBALENDRPAIDAMT
              FROM   LoanAgntFeeDtlTbl
              WHERE  Loanappnmb = p_Loanappnmb
               and   LOANAGNTSeqNmb = p_LOANAGNTSeqNmb
               and (LOANAGNTAPPCNTPAIDAMT IS NOT NULL OR LOANAGNTSBALENDRPAIDAMT IS NOT NULL))
   LOOP
   	  v_feedtlcnt := v_feedtlcnt + 1;

   	  IF c2.LOANAGNTAPPCNTPAIDAMT IS NULL AND c2.LOANAGNTSBALENDRPAIDAMT IS NULL THEN
   	  	NULL;
   	  	 BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4172;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;--add error for both amount is not null
   	  END IF;

      IF     c2.loanagntservtypcd = 5 and (nvl(c2.LOANAGNTAPPCNTPAIDAMT,0)>0 or nvl(c2.LOANAGNTSBALENDRPAIDAMT,0)>0) and c2.LOANAGNTSERVOTHTYPTXT IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4181;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 1 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4238;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;

	  IF     c2.loanagntservtypcd = 2 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4239 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 3 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4240 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 4 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4241 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 5 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4242 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
    	  -- Service amount exceeds 5 figures
	  	  IF     c2.loanagntservtypcd = 1 and (c2.LOANAGNTAPPCNTPAIDAMT > 99999.99 OR c2.LOANAGNTSBALENDRPAIDAMT > 99999.99)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4245 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 2 and (c2.LOANAGNTAPPCNTPAIDAMT > 99999.99 OR c2.LOANAGNTSBALENDRPAIDAMT > 99999.99)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4246 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 3 and (c2.LOANAGNTAPPCNTPAIDAMT > 99999.99 OR c2.LOANAGNTSBALENDRPAIDAMT > 99999.99)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4247 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 4 and (c2.LOANAGNTAPPCNTPAIDAMT > 99999.99 OR c2.LOANAGNTSBALENDRPAIDAMT > 99999.99)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4248 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 5 and (c2.LOANAGNTAPPCNTPAIDAMT > 99999.99 OR c2.LOANAGNTSBALENDRPAIDAMT > 99999.99)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4249 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
    --Amount cannot be non-numeric
        IF    REGEXP_LIKE(c2.LOANAGNTAPPCNTPAIDAMT, '[A-Za-z]')
        --not REGEXP_LIKE(c2.LOANAGNTAPPCNTPAIDAMT, '[^A-Za-z]') /* (c2.loanagntservtypcd = 1 or c2.loanagntservtypcd = 2 or c2.loanagntservtypcd = 3 or c2.loanagntservtypcd = 4 or c2.loanagntservtypcd = 5*/

      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4259 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;


   END LOOP;


	  -- Total compensation paid by Agent exceeds 2500
		/*IF v_total_appcnt_paid_amt >2500 and v_total_docs=0 then
		 BEGIN
		  p_ErrSeqNmb := p_ErrSeqNmb + 1;
		  v_ErrCd :=  4244 ;

		  LoanGntyValidErrInsCSP (
								  p_LoanAppNmb,
								  v_ErrCd,
								  p_TransInd,
								  p_VALIDATIONTYP,
								  To_char(v_Loanagntseqnmb),
								  NULL,
								  NULL,
								  NULL,
								  NULL

								 );
	   END;
      END IF;

	 -- Total compensation paid by Lender exceeds 2500
		IF v_total_lndr_paid_amt >2500 and v_total_docs=0 then
		 BEGIN
		  p_ErrSeqNmb := p_ErrSeqNmb + 1;
		  v_ErrCd :=  4244 ;

		  LoanGntyValidErrInsCSP (
								  p_LoanAppNmb,
								  v_ErrCd,
								  p_TransInd,
								  p_VALIDATIONTYP,
								  To_char(v_Loanagntseqnmb),
								  NULL,
								  NULL,
								  NULL,
								  NULL

								 );
	   END;
      END IF; */
      	 -- Total compensation paid by Lender or Applicant cannot be 0
		IF nvl(v_total_lndr_paid_amt,0) = 0 and nvl(v_total_appcnt_paid_amt,0)= 0 then
		 BEGIN
		  p_ErrSeqNmb := p_ErrSeqNmb + 1;
		  v_ErrCd :=  4257 ;

		  LoanGntyValidErrInsCSP (
								  p_LoanAppNmb,
								  v_ErrCd,
								  p_TransInd,
								  p_VALIDATIONTYP,
								  To_char(v_Loanagntseqnmb),
								  NULL,
								  NULL,
								  NULL,
								  NULL

								 );
	   END;
      END IF;

      --Other type of service needs description
	  IF     v_LOANAGNTTYPCD = 7 and v_LOANAGENTTYPEOTHER is NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4243 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;


   WHILE (Agnt_Cur%FOUND)
   LOOP
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND = 'B'
         AND v_LOANAGNTNM IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4170;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND  v_LOANAGNTTYPCD IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4171;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;

     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTFIRSTNM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4173;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTLASTNM   is null
      THEN
      BEgin

                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4174;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;

     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTR1NM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4175;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRCTYNM   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4176;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTCD   is null
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4177;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIPCD   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4178;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND length(trim(v_LOANAGNTADDRZIPCD))<>5
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4258;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIP4CD   is null
      THEN
        BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4179;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      IF     v_PRGMCD IN  ('E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANCDCTPLFEEIND='Y' Then
            if  v_LOANCDCTPLFEEAMT is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4180;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;
      END IF;
      END IF;



     FETCH Agnt_cur
         INTO v_LoanAgntSeqNmb,
              v_LOANAGNTBUSPERIND,
              v_LOANAGNTNM,
              v_LOANAGNTCNTCTFIRSTNM,
              v_LOANAGNTCNTCTMIDNM,
              v_LOANAGNTCNTCTLASTNM,
              v_LOANAGNTCNTCTSFXNM,
              v_LOANAGNTADDRSTR1NM,
              v_LOANAGNTADDRSTR2NM,
              v_LOANAGNTADDRCTYNM,
              v_LOANAGNTADDRSTCD,
              v_LOANAGNTADDRSTNM,
              v_LOANAGNTADDRZIPCD,
              v_LOANAGNTADDRZIP4CD,
              v_LOANAGNTADDRPOSTCD,
              v_LOANAGNTADDRCNTCD,
              v_LOANAGNTTYPCD,
              v_LOANAGNTDOCUPLDIND,
              v_LOANCDCTPLFEEIND,
              v_LOANCDCTPLFEEAMT,
			  v_LOANAGENTTYPEOTHER;
   END LOOP;

   CLOSE Agnt_Cur;

    IF    (p_Identifier = 11)
      OR (p_Identifier = 111)
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'F'
            ORDER BY LoanGntyValidErrSeqNmb;

         OPEN p_SelCur2 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'W'
            ORDER BY LoanGntyValidErrSeqNmb;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
   END;
/

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\LOANAGNTSELCSP.sql"
CREATE OR REPLACE procedure LOAN.LOANAGNTSELCSP
(p_Identifier 	IN NUMBER  := 0,
p_LocId in NUMBER :=0,
p_StartDt	IN DATE := null,
p_EndDt	IN DATE := null,
p_LoanNmb IN CHAR := null,
p_AgntNm IN VARCHAR2 := null,
p_RetVal  	OUT NUMBER,
p_SelCur 	OUT SYS_REFCURSOR)
as
begin
  if(p_Identifier =0) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM)) as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission


       FROM
       LOAN.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOAN.LOANGNTYBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOAN.BUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B'
       left join LOAN.PERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P'
       left join LOAN.LOANGNTYTBL l on l.LOANAPPNMB = a.LOANAPPNMB
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       (l.LOCID =  p_LocId or p_LocId is null) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null) and
        ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null))
       and rownum<=10001
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    elsif (p_Identifier=1) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM ))as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission

       FROM
       LOAN.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOAN.LOANGNTYBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOAN.BUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B'
       left join LOAN.PERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P'
       left join LOAN.LOANGNTYTBL  l on l.LOANAPPNMB = a.LOANAPPNMB
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       l.LOCID=p_LocId and
       ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null)) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null)
       and rownum<=10001
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    end if;
end;
/





GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO UPDLOANROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

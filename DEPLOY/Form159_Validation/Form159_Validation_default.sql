define deploy_name=Form159_Validation
define package_name=default
define package_buildtime=20201026115603
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_default created on Mon 10/26/2020 11:56:04.77 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_default created on Mon 10/26/2020 11:56:04.77 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL.sql 
Jasleen Gorowada committed ee8bd8d on Tue Oct 20 17:25:31 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATEAGNTCSP.sql 
Jasleen Gorowada committed 061843a on Thu Oct 22 16:52:02 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOANAPP\Misc\add_records_LOANVALIDATIONERRTBL.sql"
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4243,
 1,
 'For  Agent^1,  Description is mandatory for other type of agent.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4244,
 1,
 'For Agent^1, the total compensation amount exceeds $2,500, itemization and supporting documentation is required.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4245,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Loan Packaging service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4246,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Financial Statement service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4247,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Broker or Referal services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );

INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4248,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Consultant services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4249,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Other services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Procedures\VALIDATEAGNTCSP.sql"
DROP PROCEDURE LOAN.VALIDATEAGNTCSP;

CREATE OR REPLACE PROCEDURE LOAN.VALIDATEAGNTCSP ( p_Identifier     IN     NUMBER := 0,
                                                    p_RetVal         OUT NUMBER,
                                                     p_LoanAppNmb  IN     NUMBER DEFAULT NULL,
                                                     p_ErrSeqNmb   IN OUT NUMBER,
                                                     p_TransInd    IN     NUMBER DEFAULT NULL,
                                                     p_LOANAGNTSeqNmb IN NUMBER DEFAULT NULL,
                                                     p_VALIDATIONTYP  IN     CHAR := NULL,
                                                     p_SelCur1           OUT SYS_REFCURSOR,
                                                     p_SelCur2           OUT SYS_REFCURSOR)

AS
--SS--07/27/2018 --OPSMDEV1861
--BR--10/26/2018--CAFSOPER2199- removed NOT from NULL analysis for both LOANAGNTAPPCNTPAIDAMT and LOANAGNTSBALENDRPAIDAMT because error should only fires if BOTH are null;
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns
--BR--12/12/2018--CAFSOPER2345--Added  'B' to if then else list for v_LOANAGNTBUSPERIND for error code 4173, 4174

   v_LoanAgntSeqNmb       NUMBER (10);
   v_LOANAGNTBUSPERIND    CHAR (1);
   v_LOANAGNTNM           VARCHAR2 (200);
   v_LOANAGNTCNTCTFIRSTNM VARCHAR2 (40);
   v_LOANAGNTCNTCTMIDNM   CHAR (1);
   v_LOANAGNTCNTCTLASTNM  VARCHAR2 (40);
   v_LOANAGNTCNTCTSFXNM   VARCHAR2 (4);
   v_LOANAGNTADDRSTR1NM   VARCHAR2 (80);
   v_LOANAGNTADDRSTR2NM   VARCHAR2 (80);
   v_LOANAGNTADDRCTYNM    VARCHAR2 (40);
   v_LOANAGNTADDRSTCD     CHAR (2);
   v_LOANAGNTADDRSTNM     VARCHAR2 (60);
   v_LOANAGNTADDRZIPCD    CHAR (5);
   v_LOANAGNTADDRZIP4CD   CHAR (4);
   v_LOANAGNTADDRPOSTCD   VARCHAR2 (20);
   v_LOANAGNTADDRCNTCD    CHAR (2);
   v_LOANAGNTTYPCD        NUMBER (3);
   v_LOANAGNTDOCUPLDIND   CHAR (1);
   v_LOANCDCTPLFEEIND     CHAR (1);
   v_LOANCDCTPLFEEAMT     NUMBER (15, 2);
   v_LOANAGNTOTHTYPTXT VARCHAR2(80);
   v_PRGMCD               CHAR (2);
   v_LOANAGNTSERVTYPCD   Number(3);
   v_ChkVal      Number;
   v_LOANAGNTAPPCNTPAIDAMT   Number(15,2);
   v_LOANAGNTSBALENDRPAIDAMT  Number(15,2);
   v_chkval1   Number;
   v_ErrCd                 NUMBER (10, 2);
   v_feedtlcnt NUMBER;
   v_total_appcnt_paid_amt NUMBER;
   v_total_lndr_paid_amt NUMBER;
   

   CURSOR Agnt_Cur
   IS
      SELECT LoanAgntSeqNmb,
             LOANAGNTBUSPERIND,
             LOANAGNTNM,
             OcaDataOut(LOANAGNTCNTCTFIRSTNM) LOANAGNTCNTCTFIRSTNM,
             LOANAGNTCNTCTMIDNM,
             OcaDataOut(LOANAGNTCNTCTLASTNM) LOANAGNTCNTCTLASTNM,
             LOANAGNTCNTCTSFXNM,
             OcaDataOut(LOANAGNTADDRSTR1NM) LOANAGNTADDRSTR1NM,
             OcaDataOut(LOANAGNTADDRSTR2NM) LOANAGNTADDRSTR2NM,
             LOANAGNTADDRCTYNM,
             LOANAGNTADDRSTCD,
             LOANAGNTADDRSTNM,
             LOANAGNTADDRZIPCD,
             LOANAGNTADDRZIP4CD,
             LOANAGNTADDRPOSTCD,
             LOANAGNTADDRCNTCD,
             LOANAGNTTYPCD,
             LOANAGNTDOCUPLDIND,
             LOANCDCTPLFEEIND,
             LOANCDCTPLFEEAMT,
			 LOANAGNTOTHTYPTXT
      FROM   loanagnttbl
      WHERE  loanappnmb = p_loanappnmb
      and   LOANAGNTSeqNmb=  p_LOANAGNTSeqNmb;
	  
	cursor sel_loanangtfeedtl_appcnt 
    is
        select sum(LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL where LOANAPPNMB=p_LoanAppNmb and LOANAGNTSEQNMB=p_LOANAGNTSeqNmb 
        group by LOANAPPNMB; 
		
	cursor sel_loanangtfeedtl_lndr 
    is
        select sum(LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL where LOANAPPNMB=p_LoanAppNmb and LOANAGNTSEQNMB=p_LOANAGNTSeqNmb 
        group by LOANAPPNMB;
	  
        BEGIN
   p_ErrSeqNmb := 0;

   IF p_Identifier < 100 Then
      DELETE LoanGntyValidErrTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   END IF;    
--Begin

  BEGIN
      SELECT PRGMCD
      INTO   v_PRGMCD
      FROM   LoanGntytbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PRGMCD := NULL;
   END;
        

 
   OPEN Agnt_Cur;
   FETCH Agnt_Cur
      INTO v_LoanAgntSeqNmb,
           v_LOANAGNTBUSPERIND,
           v_LOANAGNTNM,
           v_LOANAGNTCNTCTFIRSTNM,
           v_LOANAGNTCNTCTMIDNM,
           v_LOANAGNTCNTCTLASTNM,
           v_LOANAGNTCNTCTSFXNM,
           v_LOANAGNTADDRSTR1NM,
           v_LOANAGNTADDRSTR2NM,
           v_LOANAGNTADDRCTYNM,
           v_LOANAGNTADDRSTCD,
           v_LOANAGNTADDRSTNM,
           v_LOANAGNTADDRZIPCD,
           v_LOANAGNTADDRZIP4CD,
           v_LOANAGNTADDRPOSTCD,
           v_LOANAGNTADDRCNTCD,
           v_LOANAGNTTYPCD,
           v_LOANAGNTDOCUPLDIND,
           v_LOANCDCTPLFEEIND,
           v_LOANCDCTPLFEEAMT,
		   v_LOANAGNTOTHTYPTXT;
		   
		   
	open sel_loanangtfeedtl_appcnt;
    fetch sel_loanangtfeedtl_appcnt 
    into  v_total_appcnt_paid_amt;
    
	
	open sel_loanangtfeedtl_lndr;
    fetch sel_loanangtfeedtl_lndr 
    into  v_total_lndr_paid_amt;
   
     BEGIN
      SELECT COUNT (loanappnmb)
      INTO   v_chkval1
      FROM   loan.loangntytbl a
      WHERE      a.loanappnmb = p_loanappnmb
             AND a.LOANAGNTINVLVDIND = 'Y'
             
             AND (   v_PRGMCD = 'A'
                  OR v_PRGMCD = 'E')
             AND a.loanappnmb NOT IN (SELECT Loanappnmb FROM LOAN.LOANAGNTTBL);

      IF v_chkval1 > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4182;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
   END;
   
                  
   
   v_feedtlcnt := 0;
   
   FOR c2 IN (SELECT loanagntservtypcd, LOANAGNTSERVOTHTYPTXT,LOANAGNTAPPCNTPAIDAMT,LOANAGNTSBALENDRPAIDAMT
              FROM   LoanAgntFeeDtlTbl
              WHERE  Loanappnmb = p_Loanappnmb
               and   LOANAGNTSeqNmb = p_LOANAGNTSeqNmb 
               and (LOANAGNTAPPCNTPAIDAMT IS NOT NULL OR LOANAGNTSBALENDRPAIDAMT IS NOT NULL))
   LOOP
   	  v_feedtlcnt := v_feedtlcnt + 1;
   	  
   	  IF c2.LOANAGNTAPPCNTPAIDAMT IS NULL AND c2.LOANAGNTSBALENDRPAIDAMT IS NULL THEN
   	  	NULL;
   	  	 BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4172;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;--add error for both amount is not null
   	  END IF;
   	  
      IF     c2.loanagntservtypcd = 5 and c2.LOANAGNTSERVOTHTYPTXT IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4181;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 1 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4238;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
	  
	  IF     c2.loanagntservtypcd = 2 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4239 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 3 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4240 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 4 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4241 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  IF     c2.loanagntservtypcd = 5 and c2.LOANAGNTAPPCNTPAIDAMT > 0 AND c2.LOANAGNTSBALENDRPAIDAMT > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4242 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	 --Other type of service needs description
	  IF     v_LOANAGNTTYPCD = 7 and v_LOANAGNTOTHTYPTXT is NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4243 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
      
    	  -- Service amount exceeds 5 figures
	  	  IF     c2.loanagntservtypcd = 1 and (length(c2.LOANAGNTAPPCNTPAIDAMT) > 5 OR length(c2.LOANAGNTSBALENDRPAIDAMT) > 5)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4245 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 2 and (length(c2.LOANAGNTAPPCNTPAIDAMT) > 5 OR length(c2.LOANAGNTSBALENDRPAIDAMT) > 5)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4246 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 3 and (length(c2.LOANAGNTAPPCNTPAIDAMT) > 5 OR length(c2.LOANAGNTSBALENDRPAIDAMT) > 5)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4247 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 4 and (length(c2.LOANAGNTAPPCNTPAIDAMT) > 5 OR length(c2.LOANAGNTSBALENDRPAIDAMT) > 5)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4248 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
	  	  IF     c2.loanagntservtypcd = 5 and (length(c2.LOANAGNTAPPCNTPAIDAMT) > 5 OR length(c2.LOANAGNTSBALENDRPAIDAMT) > 5)
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd :=  4249 ;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
  
    
   END LOOP;
	  
	  
	  -- Total compensation paid by Agent exceeds 2500
		IF v_total_appcnt_paid_amt >2500 then
		 BEGIN
		  p_ErrSeqNmb := p_ErrSeqNmb + 1;
		  v_ErrCd :=  4244 ;

		  LoanGntyValidErrInsCSP (
								  p_LoanAppNmb,
								  v_ErrCd,
								  p_TransInd,
								  p_VALIDATIONTYP,
								  To_char(v_Loanagntseqnmb),
								  NULL,
								  NULL,
								  NULL,
								  NULL
								 
								 );
	   END;
      END IF;
	  
	 -- Total compensation paid by Lender exceeds 2500
		IF v_total_lndr_paid_amt >2500 then
		 BEGIN
		  p_ErrSeqNmb := p_ErrSeqNmb + 1;
		  v_ErrCd :=  4244 ;

		  LoanGntyValidErrInsCSP (
								  p_LoanAppNmb,
								  v_ErrCd,
								  p_TransInd,
								  p_VALIDATIONTYP,
								  To_char(v_Loanagntseqnmb),
								  NULL,
								  NULL,
								  NULL,
								  NULL
								 
								 );
	   END;
      END IF;
	  	  

   WHILE (Agnt_Cur%FOUND)
   LOOP
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND = 'B'
         AND v_LOANAGNTNM IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4170;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND  v_LOANAGNTTYPCD IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4171;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
     
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTFIRSTNM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4173;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;  
      
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTLASTNM   is null
      THEN
      BEgin
       
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4174;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTR1NM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4175;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRCTYNM   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4176;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTCD   is null
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4177;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIPCD   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4178;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                        
                                         );
               END;
      END IF;       
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIP4CD   is null
      THEN
        BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4179;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN  ('E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANCDCTPLFEEIND='Y' Then
            if  v_LOANCDCTPLFEEAMT is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4180;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      END IF;  
    
     
      
     FETCH Agnt_cur
         INTO v_LoanAgntSeqNmb,
              v_LOANAGNTBUSPERIND,
              v_LOANAGNTNM,
              v_LOANAGNTCNTCTFIRSTNM,
              v_LOANAGNTCNTCTMIDNM,
              v_LOANAGNTCNTCTLASTNM,
              v_LOANAGNTCNTCTSFXNM,
              v_LOANAGNTADDRSTR1NM,
              v_LOANAGNTADDRSTR2NM,
              v_LOANAGNTADDRCTYNM,
              v_LOANAGNTADDRSTCD,
              v_LOANAGNTADDRSTNM,
              v_LOANAGNTADDRZIPCD,
              v_LOANAGNTADDRZIP4CD,
              v_LOANAGNTADDRPOSTCD,
              v_LOANAGNTADDRCNTCD,
              v_LOANAGNTTYPCD,
              v_LOANAGNTDOCUPLDIND,
              v_LOANCDCTPLFEEIND,
              v_LOANCDCTPLFEEAMT,
			  v_LOANAGNTOTHTYPTXT;
   END LOOP;

   CLOSE Agnt_Cur;
 
    IF    (p_Identifier = 11)
      OR (p_Identifier = 111)
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'F'
            ORDER BY LoanGntyValidErrSeqNmb;

         OPEN p_SelCur2 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'W'
            ORDER BY LoanGntyValidErrSeqNmb;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
   END;
/


GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO UPDLOANROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

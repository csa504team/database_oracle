define deploy_name=Form159_Validation
define package_name=CATWEB2
define package_buildtime=20210430133814
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_CATWEB2 created on Fri 04/30/2021 13:38:14.82 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_CATWEB2 created on Fri 04/30/2021 13:38:14.82 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_CATWEB2: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\MISC\Script_for_CATWEB2.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\MISC\Script_for_CATWEB2.sql"

--Insert 1268 into DOCTYPBUSPRCSMTHDTBL
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, '7AG', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
COMMIT;
----** First script is to fix the spelling error of Document name and second script is to take off the end date and make document optional in Origination.

update SBAREF.DOCTYPTBL set DOCTYPDESCTXT='T10 - SBA Form 159 - Fee Disclosure and Compensation Form' where DOCTYPCD=927;
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY') where DOCTYPCD=927 and   PRCSMTHDCD='7AG' and BUSPRCSTYPCD=1 ;
commit;

----** Add DOCTYPCD 1268 for remaining 13 7A loans

Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'ITR', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, '7EW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CLP', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CLW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CTR', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'PLP', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'PLW', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SAB', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SGC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'SLC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'STC', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CAI', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT,
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1268, 1, 'CAT', TO_DATE('11/23/2016 2:42:57 PM', 'MM/DD/YYYY HH:MI:SS AM'), TO_DATE('6/25/2017', 'MM/DD/YYYY'),
    user, trunc(sysdate), 'R', 'Y');
commit;

----**This will add code 1258 for all other 7A loans for both Servicing and Origination and also update DOCTYPCD 927 to be optional and have an end date of null since all legacy --documents have been moved over to 1268.

Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'ITR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, '7EW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CTR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'PLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'PLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SAB',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SGC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'SLC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'STC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 1, 'CAI',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'ITR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, '7EW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CTR',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'PLP',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'PLW',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SAB',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SGC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'SLC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'STC',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                                                                                                      
                                                                                                                                                      
Insert into SBAREF.DOCTYPBUSPRCSMTHDTBL
   (DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, 
    CREATUSERID, CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND)
Values
   (1258, 2, 'CAI',sysdate, null, 
    user, trunc(sysdate), 'O', 'Y');                                                                                                                 
                                                                            commit;

update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'ITR' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = '7EW' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CLP' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CLW' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CTR' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'PLP' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'PLW' and BUSPRCSTYPCD=1 ;                                                                                                              
commit;                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SAB' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SGC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'SLC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'STC' and BUSPRCSTYPCD=1 ;                                                                                                              
                                                                                                                                                      
                                                                                                                                                      
update SBAREF.DOCTYPBUSPRCSMTHDTBL set DOCTYPBUSPRCSMTHDENDDT=null , DOCTYPVALIDTYPCD='O',DOCTYPBUSPRCSMTHDSTRTDT = to_date('04/01/2021','MM/DD/YYYY')
 where DOCTYPCD=927 and  
PRCSMTHDCD = 'CAI' and BUSPRCSTYPCD=1 ;


commit;    
                                                                                                                                       
----**Add code for SBX
-- 1258
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 1,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );

 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 1258,
 2,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
 
 -- 927
 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 927,
 1,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );

 INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( 927,
 2,
 'SBX',
 to_date('04/05/2021','mm/dd/yyyy'),
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=Form159_Validation
define package_name=UAT_defects_CAOB
define package_buildtime=20201220212634
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Form159_Validation_UAT_defects_CAOB created on Sun 12/20/2020 21:26:34.91 by Jasleen Gorowada
prompt deploy scripts for deploy Form159_Validation_UAT_defects_CAOB created on Sun 12/20/2020 21:26:34.91 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Form159_Validation_UAT_defects_CAOB: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPTBL.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPBUSPRCSMTHDTBL.sql"
INSERT INTO SBAREF.DOCTYPBUSPRCSMTHDTBL (
   DOCTYPCD, BUSPRCSTYPCD, PRCSMTHDCD, 
   DOCTYPBUSPRCSMTHDSTRTDT, DOCTYPBUSPRCSMTHDENDDT, CREATUSERID, 
   CREATDT, DOCTYPVALIDTYPCD, ALLOWMULTIDOCIND) 
VALUES ( (select max(DOCTYPCD) from SBAREF.DOCTYPTBL),
 2,
 '7AG',
 sysdate,
 null,
 user,
 trunc(sysdate),
 'O',
 'Y' );
 commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CAOB\SBAREF\Misc\add_DOCTYPE_DOCTYPTBL.sql"
INSERT INTO SBAREF.DOCTYPTBL (
   DOCTYPCD, DOCTYPDESCTXT, DOCTYPSTRTDT, 
   DOCTYPENDDT, CREATUSERID, CREATDT, 
   TAB, DOCTYPPREAPPIND) 
VALUES ( (select max(DOCTYPCD) from SBAREF.DOCTYPTBL)+1,
 'T10 - SBA Form 159 - Itemization and Supporting Documentation',
 trunc(sysdate),
 null,
 user,
 trunc(sysdate),
 'T10',
 null );
 commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

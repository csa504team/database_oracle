define deploy_name=Excluding_PYMT110RPTARCHVTBL_fromcrossover
define package_name=CSA
define package_buildtime=20200622122715
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Excluding_PYMT110RPTARCHVTBL_fromcrossover_CSA created on Mon 06/22/2020 12:27:16.92 by Jasleen Gorowada
prompt deploy scripts for deploy Excluding_PYMT110RPTARCHVTBL_fromcrossover_CSA created on Mon 06/22/2020 12:27:16.94 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Excluding_PYMT110RPTARCHVTBL_fromcrossover_CSA: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CSA\Misc\alter_csa_xover_status.sql 
Jasleen0605 committed 0316399 on Fri Feb 7 17:00:19 2020 -0500

-- D:\dba_oracle_Bitbucket\database_oracle\CSA\Views\xover_status_csa_v.sql 
Jasleen0605 committed 0316399 on Fri Feb 7 17:00:19 2020 -0500

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CSA\Misc\alter_csa_xover_status.sql"
alter table CSA.XOVER_STATUS drop (STGCNT_PYMT110RPTARCHVTBL,
  CSACNT_PYMT110RPTARCHVTBL);
  commit;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CSA\Views\xover_status_csa_v.sql"
CREATE OR REPLACE VIEW CSA.XOVER_STATUS_CSA_V
BEQUEATH DEFINER
AS
    SELECT 'CX'                      xover_last_event,
           csa_init_dt,
           CSA_MERGE_end_dt,
           SYSDATE                   CSA_EXPORT_START_DT,
           stgcsa_init_dt,
           STGCSA_EXPORT_START_DT,
           STGCSA_MERGE_END_DT,
           stgcnt_SOFVBGW9TBL,
           stgcnt_SOFVCDCMSTRTBL,
           stgcnt_SOFVCTCHTBL,
           stgcnt_SOFVDFPYTBL,
           stgcnt_SOFVDUEBTBL,
           stgcnt_SOFVFTPOTBL,
           stgcnt_SOFVGNTYTBL,
           stgcnt_SOFVGRGCTBL,
           stgcnt_SOFVLND1TBL,
           stgcnt_SOFVLND2TBL,
           stgcnt_SOFVOTRNTBL,
           stgcnt_SOFVPYMTTBL,
           stgcnt_SOFVRTSCTBL,
           stgcnt_SOFVAUDTTBL,
           stgcnt_SOFVBFFATBL,
           stgcnt_SOFVBGDFTBL,
           stgcnt_SOFVDRTBL,
           cnt_SOFVBGW9TBL           csacnt_SOFVBGW9TBL,
           cnt_SOFVCDCMSTRTBL        csacnt_SOFVCDCMSTRTBL,
           cnt_SOFVCTCHTBL           csacnt_SOFVCTCHTBL,
           cnt_SOFVDFPYTBL           csacnt_SOFVDFPYTBL,
           cnt_SOFVDUEBTBL           csacnt_SOFVDUEBTBL,
           cnt_SOFVFTPOTBL           csacnt_SOFVFTPOTBL,
           cnt_SOFVGNTYTBL           csacnt_SOFVGNTYTBL,
           cnt_SOFVGRGCTBL           csacnt_SOFVGRGCTBL,
           cnt_SOFVLND1TBL           csacnt_SOFVLND1TBL,
           0                         csacnt_SOFVLND2TBL,
           cnt_SOFVOTRNTBL           csacnt_SOFVOTRNTBL,
           cnt_SOFVPYMTTBL           csacnt_SOFVPYMTTBL,
           cnt_SOFVRTSCTBL           csacnt_SOFVRTSCTBL,
           cnt_SOFVAUDTTBL           csacnt_SOFVAUDTTBL,
           cnt_SOFVBFFATBL           csacnt_SOFVBFFATBL,
           cnt_SOFVBGDFTBL           csacnt_SOFVBGDFTBL,
           cnt_SOFVDRTBL             csacnt_SOFVDRTBL,
           err_step,
           err_dt,
           err_msg,
           stgcnt_sofvwirekeytottbl,
           cnt_sofvwirekeytottbl     csacnt_sofvwirekeytottbl
        FROM csa.XOVER_STATUS,
           (SELECT COUNT (*) cnt_SOFVBGW9TBL FROM csa.SOFVBGW9TBL),
           (SELECT COUNT (*) cnt_SOFVCDCMSTRTBL FROM csa.SOFVCDCMSTRTBL),
           (SELECT COUNT (*) cnt_SOFVCTCHTBL FROM csa.SOFVCTCHTBL),
           (SELECT COUNT (*) cnt_SOFVDFPYTBL FROM csa.SOFVDFPYTBL),
           (SELECT COUNT (*) cnt_SOFVDUEBTBL FROM csa.SOFVDUEBTBL),
           (SELECT COUNT (*) cnt_SOFVFTPOTBL FROM csa.SOFVFTPOTBL),
           (SELECT COUNT (*) cnt_SOFVGNTYTBL FROM csa.SOFVGNTYTBL),
           (SELECT COUNT (*) cnt_SOFVGRGCTBL FROM csa.SOFVGRGCTBL),
           (SELECT COUNT (*) cnt_SOFVLND1TBL FROM csa.SOFVLND1TBL),
           (SELECT COUNT (*) cnt_SOFVOTRNTBL FROM csa.SOFVOTRNTBL),
           (SELECT COUNT (*) cnt_SOFVPYMTTBL FROM csa.SOFVPYMTTBL),
           (SELECT COUNT (*) cnt_SOFVRTSCTBL FROM csa.SOFVRTSCTBL),
           (SELECT COUNT (*) cnt_SOFVAUDTTBL FROM csa.SOFVAUDTTBL),
           (SELECT COUNT (*) cnt_SOFVBFFATBL FROM csa.SOFVBFFATBL),
           (SELECT COUNT (*) cnt_SOFVBGDFTBL FROM csa.SOFVBGDFTBL),
           (SELECT COUNT (*) cnt_sofvwirekeytottbl FROM csa.sofvwirekeytottbl),
           (SELECT COUNT (*) cnt_SOFVDRTBL FROM csa.SOFVDRTBL);


GRANT SELECT ON CSA.XOVER_STATUS_CSA_V TO CSAREADALLROLE;


-- KU$VAT tables are Oracle generated and used for exporting views as tables
--   if the view structure changes, using the old KU table(s) is bad
-- In this case, col STGCNT_SOFVLND1TBL is only part of XOVER_STATUS 
begin
  for drop_ku_table in
    (select distinct 'drop table '||owner||'.'||table_name drop_stmt
       from dba_tab_columns where column_name='STGCNT_SOFVLND1TBL'
         and table_name like 'KU$VAT%')
  loop
    execute immediate drop_ku_table.drop_stmt;
  end loop;
end;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat


CREATE OR replace PROCEDURE cdconline.ppwirerptseltsp ( 
p_identifier in NUMBER,  
p_cdcregncd in CHAR, 
p_cdcnmb in CHAR,
p_prepaydt in DATE,
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns ALL wire report data for agents
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
				SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns wire report data for a specific month
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				WHERE
					TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns wire report data for a specific month, cdc
	ELSIF p_Identifier = 2 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
					loannmb
					, prepaydt
					, borrnm
					, cdcregncd
					, cdcnmb
					, duetopploanamt
					, wireamt
					, diffamt
					, isarchived
					, creatuserid
					, creatdt
					, lastupdtuserid
					, lastupdtdt
					, semiandt
					, wirerecvdt
				FROM 
					cdconline.ppwirerpttbl
				WHERE
					TRIM(cdcnmb) = TRIM(p_cdcnmb)
				AND
					TRIM(cdcregncd) = TRIM(p_cdcregncd)
				AND
					TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
				ORDER BY
					cdcregncd ASC, cdcnmb ASC, loannmb ASC, semiandt ASC;
		END;
	-- returns all valid prepayment dates
	ELSIF p_Identifier = 3 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				DISTINCT(prepaydt)
			FROM 
				cdconline.ppwirerpttbl
			ORDER BY
				prepaydt DESC;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.ppwirerptseltsp TO cdconlinereadallrole,cdconlineprtread,cdconlinegovread,CDCONLINEDEVROLE;
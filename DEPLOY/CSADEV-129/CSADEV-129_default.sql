define deploy_name=CSADEV-129
define package_name=default
define package_buildtime=20190916131238
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-129_default created on Mon 09/16/2019 13:12:38.61 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-129_default created on Mon 09/16/2019 13:12:38.63 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-129_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\rptschdlpodollrtbl.sql 
zeeshan9000 committed 06fe40f on Fri Aug 2 12:36:44 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\rptschdlpodollrselcsp.sql 
zeeshan9000 committed 54574ab on Thu Aug 22 11:43:34 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_RPTSCHDLPODOLLRTBL.sql 
John low committed 44fc9fb on Fri Jul 12 09:06:13 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_LTRPPWORKUPAUDTTBL.sql 
John low committed 44fc9fb on Fri Jul 12 09:06:13 2019 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_PPWIRERPTTBL.sql 
John low committed 44fc9fb on Fri Jul 12 09:06:13 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\rptschdlpodollrtbl.sql"
CREATE TABLE cdconline.rptschdlpodollrtbl
(
	loannmb CHAR(10 CHAR) NOT NULL
	, prepaydt DATE NOT NULL
	, borrnm VARCHAR2(80 BYTE)
	, cdcregncd CHAR(2 BYTE) NOT NULL 
	, cdcnmb CHAR(4 BYTE) NOT NULL 
	, wkprepayamt NUMBER(19, 4)
	, isarchived CHAR(1 CHAR) DEFAULT 'N'
	, creatuserid VARCHAR2(32 BYTE) NOT NULL 
	, creatdt DATE DEFAULT SYSDATE NOT NULL 
	, lastupdtuserid VARCHAR2(32 BYTE) NOT NULL 
	, lastupdtdt DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinereadallrole;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlineprtread;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinegovread;
GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.rptschdlpodollrtbl TO CDCONLINEDEVROLE;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\rptschdlpodollrselcsp.sql"
CREATE OR replace PROCEDURE cdconline.rptschdlpodollrselcsp ( 
p_identifier  NUMBER := NULL,  
p_prepaydt    DATE := SYSDATE, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns report data irrespective of cdc number and cdc region for current data
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	CASE WHEN ls.curbalneededamt > 0 THEN
						(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4 ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY)
					ELSE
						ls.wkprepayamt - (SELECT NVL(SUM(UnallocAmt), 0) FROM cdconline.PymtHistryTbl WHERE  LoanNmb = rq.loannmb) 
					END wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- returns report data filtered by cdc number, and cdc region for the current month
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	CASE WHEN ls.curbalneededamt > 0 THEN
						(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4 ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY)
					ELSE
						ls.wkprepayamt - (SELECT NVL(SUM(UnallocAmt), 0) FROM cdconline.PymtHistryTbl WHERE  LoanNmb = rq.loannmb) 
					END wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			AND TRIM(ls.cdcnmb) = TRIM(p_cdcnmb)
			AND TRIM(ls.cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- archives data at the end of the month for given prepay date; this should always be the current date unless testing for previous versions
	ELSIF p_Identifier = 2 THEN
		BEGIN
			DELETE FROM cdconline.rptschdlpodollrtbl WHERE TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt));
			
			INSERT INTO cdconline.rptschdlpodollrtbl(loannmb, prepaydt, borrnm, cdcregncd, cdcnmb, wkprepayamt, isarchived, creatuserid, creatdt, lastupdtuserid, lastupdtdt)
			SELECT 
				rq.loannmb
				, rq.prepaydt
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, ls.cdcregncd
				, ls.cdcnmb
				,	CASE WHEN ls.curbalneededamt > 0 THEN
						(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND TRUNC(LAST_DAY(audt.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt)) AND audt.rqstcd = 4 ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY)
					ELSE
						ls.wkprepayamt - (SELECT NVL(SUM(UnallocAmt), 0) FROM cdconline.PymtHistryTbl WHERE  LoanNmb = rq.loannmb) 
					END wkprepayamt
				, 'Y' AS isarchived
				, USER
				, SYSDATE
				, USER
				, SYSDATE
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 );
			
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	CASE WHEN ls.curbalneededamt > 0 THEN
						(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND TRUNC(LAST_DAY(audt.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt)) AND audt.rqstcd = 4 ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY)
					ELSE
						ls.wkprepayamt - (SELECT NVL(SUM(UnallocAmt), 0) FROM cdconline.PymtHistryTbl WHERE  LoanNmb = rq.loannmb) 
					END wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 4 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 5 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	END IF;
END;
/

GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinedevrole;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_RPTSCHDLPODOLLRTBL.sql"
create or replace view CDCONLINE.SC_RPTSCHDLPODOLLRTBL 
  (
LOANNMB
,PREPAYDT
,BORRNM
,CDCREGNCD
,CDCNMB
,WKPREPAYAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-07-11 16:10:54
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,PREPAYDT
/* BORRNM */ ,'BWR: '||loannmb
,CDCREGNCD
,CDCNMB
,WKPREPAYAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.RPTSCHDLPODOLLRTBL ;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_LTRPPWORKUPAUDTTBL.sql"
create or replace view CDCONLINE.SC_LTRPPWORKUPAUDTTBL 
  (
RQSTREF
,RQSTDT
,LOANNMB
,RQSTCD
,PREPAYDT
,CDCREGNCD
,CDCNMB
,PPWORKUPLTR
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
,DUETOPPLOANAMT
) as select
  -- Scrubbing View - Definition generated 2019-07-11 15:47:32
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
RQSTREF
,RQSTDT
,LOANNMB
,RQSTCD
,PREPAYDT
,CDCREGNCD
,CDCNMB
/* PPWORKUPLTR */ ,'Value Replaced by Scrubbing'
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
,DUETOPPLOANAMT
from CDCONLINE.LTRPPWORKUPAUDTTBL ;
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Views\SC_PPWIRERPTTBL.sql"
create or replace view CDCONLINE.SC_PPWIRERPTTBL 
  (
LOANNMB
,PREPAYDT
,BORRNM
,CDCREGNCD
,CDCNMB
,DUETOPPLOANAMT
,WIREAMT
,DIFFAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
,SEMIANDT
,WIRERECVDT
) as select
  -- Scrubbing View - Definition generated 2019-07-11 15:47:33
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,PREPAYDT
/* BORRNM */ ,decode(ltrim(BORRNM),null,BORRNM,'BWR: '||loannmb)
,CDCREGNCD
,CDCNMB
,DUETOPPLOANAMT
,WIREAMT
,DIFFAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
,SEMIANDT
,WIRERECVDT
from CDCONLINE.PPWIRERPTTBL ;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

CREATE OR replace PROCEDURE cdconline.prwireselcsp ( 
p_identifier  NUMBER := NULL,  
p_prepaydt    DATE := SYSDATE, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- returns payments from payment review by the prepay date (month-year) irrespective of cdc number and cdc region
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				ct.transid,
				ct.loannmb AS loannmb,
				ct.transind1,
				ct.creatdt,
				ct.statidcur,
				mcta.maxoftransattrid,
				to_number(mcta.attrval) AS amount,
				mcta2.attrval AS imptdt,
				mcta3.attrval AS postdate,
				mcta1.attrval AS "CURRENT",
				rts.statnm AS status,
				mcta22.attrval AS batchno,
				mcta19.attrval AS pymttypeid,
				lt.cdcregncd,
				lt.cdcnmb 
			FROM
				stgcsa.coretranstbl ct 
				LEFT JOIN
					cdconline.loantbl lt 
					ON ct.loannmb = lt.loannmb 
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 20 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta 
					ON ct.transid = mcta.transid 		--PymtAmt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 24 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta1 
					ON ct.transid = mcta1.transid 		--Current
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 34 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta2 
					ON ct.transid = mcta2.transid 		--ImpDt
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 27 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta3 
					ON ct.transid = mcta3.transid 		--PostDt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 19 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta19 
					ON ct.transid = mcta19.transid 		--PymtTyp
				LEFT OUTER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 22 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta22 
					ON ct.transid = mcta22.transid 		--BatchNo
				INNER JOIN
					stgcsa.reftransstattbl rts 
					ON rts.statid = ct.statidcur 
			WHERE
				transtypid = 3 
				AND ct.statidcur IN 
				(
					7,
					18,
					33
				)
				AND LAST_DAY(TRUNC(mcta2.attrval)) = LAST_DAY(TRUNC(p_prepaydt))
			;
		END;
	-- returns payments from payment review by the prepay date (month-year), cdc number, and cdc region
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				ct.transid,
				ct.loannmb AS loannmb,
				ct.transind1,
				ct.creatdt,
				ct.statidcur,
				mcta.maxoftransattrid,
				to_number(mcta.attrval) AS amount,
				mcta2.attrval AS imptdt,
				mcta3.attrval AS postdate,
				mcta1.attrval AS "CURRENT",
				rts.statnm AS status,
				mcta22.attrval AS batchno,
				mcta19.attrval AS pymttypeid,
				lt.cdcregncd,
				lt.cdcnmb 
			FROM
				stgcsa.coretranstbl ct 
				LEFT JOIN
					cdconline.loantbl lt 
					ON ct.loannmb = lt.loannmb 
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 20 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta 
					ON ct.transid = mcta.transid 		--PymtAmt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 24 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta1 
					ON ct.transid = mcta1.transid 		--Current
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 34 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta2 
					ON ct.transid = mcta2.transid 		--ImpDt
				INNER JOIN
					(
						SELECT
							transid,
							CASE
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d)+$') 
								THEN
									to_date ('12/31/1899', 'MM/DD/YYYY') + xxattrval 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){4}/(\d){1,2}/(\d){1,2}') 
								THEN
									to_date (xxattrval, 'YYYY/MM/DD') 
								WHEN
									regexp_like(TRIM(xxattrval), '^(\d){1,2}/(\d){1,2}/(\d){4}') 
								THEN
									to_date (xxattrval, 'MM/DD/YYYY') 
								WHEN
									instr(xxattrval, '-') > 0 
								THEN
									to_date (xxattrval, 'YYYY-MM-DD HH24:MI:SS') 
								ELSE
									NULL 
							END
							AS attrval 
						FROM
							(
								SELECT
									MAX(transattrid) AS maxoftransattrid,
									transid,
									attrval AS xxattrval 
								FROM
									stgcsa.coretransattrtbl 
								WHERE
									attrid = 27 
								GROUP BY
									transid,
									attrval 
								ORDER BY
									transid DESC 
							)
							xxx 
					)
					mcta3 
					ON ct.transid = mcta3.transid 		--PostDt
				INNER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 19 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta19 
					ON ct.transid = mcta19.transid 		--PymtTyp
				LEFT OUTER JOIN
					(
						SELECT
							MAX(transattrid) AS maxoftransattrid,
							transid,
							attrval 
						FROM
							stgcsa.coretransattrtbl 
						WHERE
							attrid = 22 
						GROUP BY
							transid,
							attrval 
						ORDER BY
							transid DESC
					)
					mcta22 
					ON ct.transid = mcta22.transid 		--BatchNo
				INNER JOIN
					stgcsa.reftransstattbl rts 
					ON rts.statid = ct.statidcur 
			WHERE
				transtypid = 3 
				AND ct.statidcur IN 
				(
					7,
					18,
					33
				)
				AND LAST_DAY(TRUNC(mcta2.attrval)) = LAST_DAY(TRUNC(p_prepaydt))
				AND TRIM(lt.cdcregncd) = TRIM(p_cdcregncd)
				AND TRIM(lt.cdcnmb) = TRIM(p_cdcnmb)
			;
		END;
	END IF;
END;

GRANT EXECUTE ON cdconline.prwireselcsp TO cdconlinereadallrole,cdconlineprtread,cdconlinegovread,CDCONLINEDEVROLE;
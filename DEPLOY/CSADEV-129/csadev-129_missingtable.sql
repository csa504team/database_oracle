-- deploy scripts for deploy csadev-129_missingtable created on Fri 08/02/2019 12:56:08.47 by johnlow
define deploy_name=csadev-129
define package_name=missingtable
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off
/* execute GIT Pull to ensure all files are current 
Updating cdecf84..6a5862c
Fast-forward
 CDCONLINE/Tables/rptschdlpodollrtbl.sql     |  2 ++
 DEPLOY/CSADEV-109/ad_hoc_grant_fix.sql      | 35 +++++++++++++++++++++++++++++
 DEPLOY/CSADEV-129/missingtable_filelist.txt |  1 +
 3 files changed, 38 insertions(+)
 create mode 100644 DEPLOY/CSADEV-109/ad_hoc_grant_fix.sql
 create mode 100644 DEPLOY/CSADEV-129/missingtable_filelist.txt
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Tables\rptschdlpodollrtbl.sql 
zeeshan9000 committed 06fe40f on Fri Aug 2 12:36:44 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Tables\rptschdlpodollrtbl.sql"
CREATE TABLE cdconline.rptschdlpodollrtbl
(
	loannmb CHAR(10 CHAR) NOT NULL
	, prepaydt DATE NOT NULL
	, borrnm VARCHAR2(80 BYTE)
	, cdcregncd CHAR(2 BYTE) NOT NULL 
	, cdcnmb CHAR(4 BYTE) NOT NULL 
	, wkprepayamt NUMBER(19, 4)
	, isarchived CHAR(1 CHAR) DEFAULT 'N'
	, creatuserid VARCHAR2(32 BYTE) NOT NULL 
	, creatdt DATE DEFAULT SYSDATE NOT NULL 
	, lastupdtuserid VARCHAR2(32 BYTE) NOT NULL 
	, lastupdtdt DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinereadallrole;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlineprtread;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinegovread;
GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.rptschdlpodollrtbl TO CDCONLINEDEVROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

CREATE TABLE cdconline.ppwirerpttbl
(
	loannmb CHAR(10 CHAR) NOT NULL
	, prepaydt DATE NOT NULL
	, borrnm VARCHAR2(80 BYTE)
	, cdcregncd CHAR(2 BYTE) NOT NULL 
	, cdcnmb CHAR(4 BYTE) NOT NULL 
	, duetopploanamt NUMBER(19, 4)
	, wireamt NUMBER(19, 4)
	, diffamt NUMBER(19, 4)
	, isarchived CHAR(1 CHAR) DEFAULT 'N'
	, creatuserid VARCHAR2(32 BYTE) NOT NULL 
	, creatdt DATE DEFAULT SYSDATE NOT NULL 
	, lastupdtuserid VARCHAR2(32 BYTE) NOT NULL 
	, lastupdtdt DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON cdconline.ppwirerpttbl TO cdconlinereadallrole,CDCONLINEDEVROLE;
CREATE OR replace PROCEDURE cdconline.ltrppworkupaudtinstsp ( 
p_identifier  NUMBER := NULL, 
p_rqstref     NUMBER := NULL, 
p_rqstdt      DATE := NULL, 
p_loannmb     VARCHAR2 := NULL, 
p_rqstcd      NUMBER := NULL, 
p_prepaydt    DATE := NULL, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_ppworkupltr CLOB := NULL, 
p_creatuserid VARCHAR2 := NULL, 
p_creatdt     DATE := NULL,
p_duetopploanamt NUMBER := NULL) 
AS 
BEGIN 
    SAVEPOINT ltrppworkupaudttsp; 

    IF p_identifier = 0 THEN 
      BEGIN 
          INSERT INTO cdconline.ltrppworkupaudttbl 
                      (rqstref, 
                       rqstdt, 
                       loannmb, 
                       rqstcd, 
                       prepaydt, 
                       cdcregncd, 
                       cdcnmb, 
                       ppworkupltr, 
                       creatuserid, 
                       creatdt, 
                       lastupdtuserid, 
                       lastupdtdt,
					   duetopploanamt) 
          VALUES      ( p_rqstref, 
                       p_rqstdt, 
                       p_loannmb, 
                       p_rqstcd, 
                       p_prepaydt, 
                       p_cdcregncd, 
                       p_cdcnmb, 
                       p_ppworkupltr, 
                       p_creatuserid, 
                       SYSDATE, 
                       p_creatuserid, 
                       SYSDATE,
					   p_duetopploanamt); 
      END; 
    END IF; 
EXCEPTION 
  WHEN OTHERS THEN 
             BEGIN 
                 ROLLBACK TO ltrppworkupaudttsp; 

                 RAISE; 
             END; 
END; 
define deploy_name=CSADEV-129
define package_name=scheduled
define package_buildtime=20191010114132
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-129_scheduled created on Thu 10/10/2019 11:41:34.79 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-129_scheduled created on Thu 10/10/2019 11:41:34.80 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-129_scheduled: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\rptschdlpodollrselcsp.sql 
zeeshan9000 committed 7ebb532 on Thu Oct 10 11:16:46 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\rptschdlpodollrselcsp.sql"
CREATE OR replace PROCEDURE cdconline.rptschdlpodollrselcsp ( 
p_identifier  NUMBER := NULL,  
p_prepaydt    DATE := SYSDATE, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- TO_NUMBER(REPLACE(SUBSTR(PPWORKUPLTR, INSTR(PPWORKUPLTR, 'Less Net Amount Of Unallocated Funds')+122, INSTR(SUBSTR(PPWORKUPLTR, INSTR(PPWORKUPLTR, 'Less Net Amount Of Unallocated Funds')+122, 20), '<')-1), ',', '')) is used to extract the unallocated funds from the archived workup cover letter
	
	-- PER Vincent's direction on 10/10/2019, the wkprepayamt should always be from the archived workup cover letter
	-- returns report data irrespective of cdc number and cdc region for current data
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- returns report data filtered by cdc number, and cdc region for the current month
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			AND TRIM(ls.cdcnmb) = TRIM(p_cdcnmb)
			AND TRIM(ls.cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- archives data at the end of the month for given prepay date; this should always be the current date unless testing for previous versions
	ELSIF p_Identifier = 2 THEN
		BEGIN
			DELETE FROM cdconline.rptschdlpodollrtbl WHERE TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt));
			
			INSERT INTO cdconline.rptschdlpodollrtbl(loannmb, prepaydt, borrnm, cdcregncd, cdcnmb, wkprepayamt, isarchived, creatuserid, creatdt, lastupdtuserid, lastupdtdt)
			SELECT 
				rq.loannmb
				, rq.prepaydt
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, ls.cdcregncd
				, ls.cdcnmb
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
				, 'Y' AS isarchived
				, USER
				, SYSDATE
				, USER
				, SYSDATE
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 );
			
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 4 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 5 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	END IF;
END;
/

GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlineprtread;
--GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinedevrole;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

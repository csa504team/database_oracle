-- deploy scripts for deploy accelerations_create_issue_default created on Fri 07/12/2019 14:51:58.89 by johnlow
define deploy_name=accelerations_create_issue
define package_name=default
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\\CDCONLINE\Misc\insert_servicing_record.sql 
zeeshan9000 committed b7abfab on Fri Jul 12 14:45:18 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\\CDCONLINE\Misc\insert_servicing_record.sql"
INSERT INTO cdconline.servcntrtbl
	(servcntrcdcid
	 , servcntrid
	 , cdcregncd
	 , cdcnmb
	 , creatuserid
	 , creatdt
	 , lastupdtuserid
	 , lastupdtdt)
VALUES      ( (SELECT MAX(servcntrcdcid) + 1
	  FROM   cdconline.servcntrtbl)
	 , 'sc01'
	 , '06'
	 , '717'
	 , USER
	 , SYSDATE
	 , USER
	 , SYSDATE );

COMMIT; 
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

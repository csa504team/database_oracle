define deploy_name=FEB_2021_Ach_Debit
define package_name=check_deletion_PROD_missing_records
define package_buildtime=20210121141135
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FEB_2021_Ach_Debit_check_deletion_PROD_missing_records created on Thu 01/21/2021 14:11:35.98 by Jasleen Gorowada
prompt deploy scripts for deploy FEB_2021_Ach_Debit_check_deletion_PROD_missing_records created on Thu 01/21/2021 14:11:35.98 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FEB_2021_Ach_Debit_check_deletion_PROD_missing_records: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql 
John low committed 7981ea7 on Mon Apr 27 09:13:15 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script_for_deletion_missing_records_PROD.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\verify_onlinedb_isup.sql"
WHENEVER SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;
WHENEVER SQLERROR CONTINUE

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Misc\check_script_for_deletion_missing_records_PROD.sql"
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4149497008';   
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4586335001';    
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4702275005';  
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='5525195008';      
  
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='2881556008';                           

select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3100037004';    
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3658915004';  

select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='3847787004'; 
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4042007001';

select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='4139177002';  
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8244325007';   
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8344445005'; 
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='8478355008'; 
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='6514615002'; 
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7056865003'; 
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7458095000';  
  
select  LOANNMB, LOANDTLACHBNKACCT, LOANDTLACHBNKROUTNMB from STGCSA.SOFVLND1TBL
where LOANNMB='7638225010';    
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

grant execute on 
  cdconline.LatePymt45SelCSP
to CDCONLINEREADALLROLE,CDCONLINEPRTREAD,CDCONLINEDEVROLE;                                                              
grant execute on 
  cdconline.LatePymt65SelCSP
to CDCONLINEREADALLROLE,CDCONLINEPRTREAD,CDCONLINEDEVROLE;                                                              
grant execute on 
  cdconline.StatNonCurrentSelTSP
to CDCONLINEREADALLROLE,CDCONLINEPRTREAD,CDCONLINEDEVROLE;                                                              
grant execute on 
  cdconline.StatPortflDelCSP
to CDCONLINEREADALLROLE,CDCONLINEPRTREAD,CDCONLINEDEVROLE;                                                              

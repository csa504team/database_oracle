alter session set nls_date_format='Day DD Mon yyyy HH24:MI:SS';
set headings on
set pagesize 100
column XOVER_TIME format 99.99
column outbound_time format 99.99
column BATCH_time format 99.99
column inbound_time format 99.99
with 
mergetask as
  (select csa_sid mt_csa_sid, creatdt mt_sm, logentryid mt_logentryid       
    from stgcsa.coreactvtylogtbl where creatdt>to_date('20190227','yyyymmdd')
      and msg_id in (221) order by logentryid),
mt_xtimes0 as 
  (select mt_csa_sid, MT_SM , creatdt M,  --  Time of merge (start) from this start log rec
    to_date(substr(actvtydtls,instr(actvtydtls,'SI:')+3,14),'mm-dd hh24:mi:ss') SI,
    to_date(substr(actvtydtls,instr(actvtydtls,'SX:')+3,14),'mm-dd hh24:mi:ss') SX,
    to_date(substr(actvtydtls,instr(actvtydtls,'SM:')+3,14),'mm-dd hh24:mi:ss') SM,
    to_date(substr(actvtydtls,instr(actvtydtls,'CI:')+3,14),'mm-dd hh24:mi:ss') CI,
    to_date(substr(actvtydtls,instr(actvtydtls,'CX:')+3,14),'mm-dd hh24:mi:ss') CX,
    to_date(substr(actvtydtls,instr(actvtydtls,'CM:')+3,14),'mm-dd hh24:mi:ss') CM
    from stgcsa.coreactvtylogtbl, mergetask 
    where csa_sid=mt_csa_sid and msg_id=228),
mt_xtimes as 
  (select mt_csa_sid, si, (mt_sm-si)*1440 x_dur, sx, (sx-si)*1440 sx_dur,
    cm, (cm-sx)*1440 cm_dur,
    (cm-si)*1440 out_dur,
    ci, (ci-cm)*1440 csa_dur,
    cx, (cx-ci)*1440 cx_dur,
    M, (m-cx)*1440 imp_dur,       
    MT_SM SM, (MT_SM-M)*1440 SM_DUR,
    (MT_SM-CI)*1440 In_dur
    from stgcsa.coreactvtylogtbl, mt_xtimes0 
    where csa_sid=mt_csa_sid and msg_id=228),      
mt_errs as
  (select mt.mt_csa_sid,err.*
    from stgcsa.coreactvtylogtbl err, mergetask mt, mt_xtimes xt
      where  mt.mt_csa_sid=xt.mt_csa_sid 
        and msg_severity=3 
        and creatdt>=si and creatdt<=sm
        ) 
select SI XOVER_START, X_DUR XOVER_TIME  , OUT_DUR OUTBOUND_TIME, 
  csa_dur Batch_TIME, in_dur INBOUND_TIME 
  from mt_xtimes where x_dur<1000 order by 1;

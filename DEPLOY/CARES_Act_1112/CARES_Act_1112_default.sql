define deploy_name=CARES_Act_1112
define package_name=default
define package_buildtime=20200420123524
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CARES_Act_1112_default created on Mon 04/20/2020 12:35:25.53 by Jasleen Gorowada
prompt deploy scripts for deploy CARES_Act_1112_default created on Mon 04/20/2020 12:35:25.56 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CARES_Act_1112_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\LoanPymtHistSelTSP.sql 
Jasleen0605 committed 863b04e on Mon Apr 20 12:35:11 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\LoanPymtHistSelTSP.sql"
create or replace PROCEDURE cdconline.LoanPymtHistSelTSP 
--DROP   PROCEDURE [dbo].[CDC_LoanPaymtHistory_R]

(
  p_GPNum IN CHAR,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN

   OPEN  p_SelCur1 FOR
      SELECT LoanNmb ,
             PymtTyp ,
             PostingDt ,
             CASE 
                  WHEN PymtTyp = 'X' THEN NULL
             ELSE SBAFeeAmt + CSAFeeAmt + CDCFeeAmt + IntAmt + PrinAmt + UnallocAmt + LateFeeAmt + DueFromBorrAmt
                END total_pymt  ,
             SBAFeeAmt ,
             CSAFeeAmt ,
             CDCFeeAmt ,
             IntAmt ,
             PrinAmt ,
             LateFeeAmt ,
             UnallocAmt ,
             PrepayAmt ,
             BalAmt ,
             DueFromBorrAmt ,
             CreatUserId
        FROM PymtHistryTbl 
       WHERE  LoanNmb = p_GPNum
        ORDER BY PostingDt DESC ;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

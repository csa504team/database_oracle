/*
	Updates note balance and principal for loan 8639994007
*/

UPDATE
	stgcsa.prploanrqsttbl
SET
	notebalcalcamt = 201869.69
	, prinprojamt = 2151.48
	, lastupdtdt = SYSDATE
WHERE
	loannmb = 8639994007;
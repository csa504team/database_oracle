with list_of_tbls as
  (select 'SOFVCTCHTBL' oname from dual                   
    union select 'SOFVLND2TBL' oname from dual                   
    union select 'SOFVGNTYTBL' oname from dual                   
    union select 'SOFVOTRNTBL' oname from dual                   
    union select 'SOFVBGDFTBL' oname from dual                   
    union select 'SOFVDFPYTBL' oname from dual                   
    union select 'SOFVFTPOTBL' oname from dual                   
    union select 'SOFVAUDTTBL' oname from dual                   
    union select 'SOFVDUEBTBL' oname from dual                   
    union select 'SOFVRTSCTBL' oname from dual                   
    union select 'SOFVGRGCTBL' oname from dual                   
    union select 'SOFVLND1TBL' oname from dual                   
    union select 'SOFVBFFATBL' oname from dual                   
    union select 'SOFVBGW9TBL' oname from dual                   
    union select 'SOFVCDCMSTRTBL' oname from dual                
    union select 'SOFVPYMTTBL' oname from dual) 
select oname, 
  nvl(sum(decode(upd_action,'I',1,0)),0) ins_cnt,     
  nvl(sum(decode(upd_action,'U',1,0)),0) upd_cnt,     
  nvl(sum(decode(upd_action,'D',1,0)),0) del_cnt
from stgcsa.stgupdtlogtbl log, list_of_tbls tbls
where stgcsa.runtime.mycsasid=csa_sid(+) and tbls.oname=log.tablenm(+)
group by oname       
  
  
  
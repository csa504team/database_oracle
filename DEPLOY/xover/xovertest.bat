echo -- %date% %time% starting crossover test %1% - full tables
echo -- %date% %time% starting export from cafs
set ctlfiles=c:\csa\csa504data_svn\deploy\xover
call stglog "%1% starting xover for %username% from base %ctlfiles%"
call stglog "%1% starting cafs export"
expdp '/@orcl as sysdba' parfile=%ctlfiles%\xover_cafs_exp.ctl
call stglog "%1% done cafs export, starting cala import"
echo -- %date% %time% done cafs export starting cala import
impdp '/@orcl as sysdba' parfile=%ctlfiles%\xover_cala_imp.ctl
echo -- %date% %time% done cala import starting csa workload 
call stglog "%1% done cala import starting csa workload"
sqlplus "/@orcl as sysdba" @csa_workload
echo -- %date% %time% done csa workload starting cala export 
call stglog "%1% done csa workload, starting cala export"
expdp '/@orcl as sysdba' parfile=%ctlfiles%\xover_cala_exp.ctl
call stglog "%1% done cala export, starting cafs import"
echo -- %date% %time% done cals export starting cafs import
impdp '/@orcl as sysdba' parfile=%ctlfiles%\xover_cafs_imp.ctl
echo -- %date% %time% done cafs import starting merge
call stglog "%1% done cafs cafs import starting merge"
sqlplus "/@orcl as sysdba" @xover_run_mergeback
echo -- %date% %time% done mergeback

call stglog " %1% done"

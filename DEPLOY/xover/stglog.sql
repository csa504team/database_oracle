set feedback off
set verify off
declare
  logentryid number;
begin  
  stgcsa.runtime.logger(logentryid,'STDTST',999,'&&1',user,nvl(&&2,0),
    program_name=>'STGLOG');
  commit;
end;  
/
exit;
-- views for crossover
CREATE OR REPLACE FORCE VIEW STGCSA.XOVER_CURRENT_TIME_VIEW
(
  DT,
  CNT_SOFVBGW9TBL,
  CNT_SOFVCDCMSTRTBL,
  CNT_SOFVCTCHTBL,
  CNT_SOFVDFPYTBL,
  CNT_SOFVDUEBTBL,
  CNT_SOFVFTPOTBL,
  CNT_SOFVGNTYTBL,
  CNT_SOFVGRGCTBL,
  CNT_SOFVLND1TBL,
  CNT_SOFVLND2TBL,
  CNT_SOFVOTRNTBL,
  CNT_SOFVPYMTTBL,
  CNT_SOFVRTSCTBL,
  CNT_SOFVAUDTTBL,
  CNT_SOFVBFFATBL,
  CNT_SOFVBGDFTBL
)
BEQUEATH DEFINER
AS
  SELECT dt,
         cnt_SOFVBGW9TBL,
         cnt_SOFVCDCMSTRTBL,
         cnt_SOFVCTCHTBL,
         cnt_SOFVDFPYTBL,
         cnt_SOFVDUEBTBL,
         cnt_SOFVFTPOTBL,
         cnt_SOFVGNTYTBL,
         cnt_SOFVGRGCTBL,
         cnt_SOFVLND1TBL,
         cnt_SOFVLND2TBL,
         cnt_SOFVOTRNTBL,
         cnt_SOFVPYMTTBL,
         cnt_SOFVRTSCTBL,
         cnt_SOFVAUDTTBL,
         cnt_SOFVBFFATBL,
         cnt_SOFVBGDFTBL
    FROM (SELECT SYSDATE dt, COUNT (*) cnt_SOFVBGW9TBL
            FROM stgcsa.xover_SOFVBGW9TBL_view),
         (SELECT COUNT (*)   cnt_SOFVCDCMSTRTBL
            FROM stgcsa.xover_SOFVCDCMSTRTBL_view),
         (SELECT COUNT (*) cnt_SOFVCTCHTBL FROM stgcsa.xover_SOFVCTCHTBL_view),
         (SELECT COUNT (*) cnt_SOFVDFPYTBL FROM stgcsa.xover_SOFVDFPYTBL_view),
         (SELECT COUNT (*) cnt_SOFVDUEBTBL FROM stgcsa.xover_SOFVDUEBTBL_view),
         (SELECT COUNT (*) cnt_SOFVFTPOTBL FROM stgcsa.xover_SOFVFTPOTBL_view),
         (SELECT COUNT (*) cnt_SOFVGNTYTBL FROM stgcsa.xover_SOFVGNTYTBL_view),
         (SELECT COUNT (*) cnt_SOFVGRGCTBL FROM stgcsa.xover_SOFVGRGCTBL_view),
         (SELECT COUNT (*) cnt_SOFVLND1TBL FROM stgcsa.xover_SOFVLND1TBL_view),
         (SELECT COUNT (*) cnt_SOFVLND2TBL FROM stgcsa.xover_SOFVLND2TBL_view),
         (SELECT COUNT (*) cnt_SOFVOTRNTBL FROM stgcsa.xover_SOFVOTRNTBL_view),
         (SELECT COUNT (*) cnt_SOFVPYMTTBL FROM stgcsa.xover_SOFVPYMTTBL_view),
         (SELECT COUNT (*) cnt_SOFVRTSCTBL FROM stgcsa.xover_SOFVRTSCTBL_view),
         (SELECT COUNT (*) cnt_SOFVAUDTTBL FROM stgcsa.xover_SOFVAUDTTBL_view),
         (SELECT COUNT (*) cnt_SOFVBFFATBL FROM stgcsa.xover_SOFVBFFATBL_view),
         (SELECT COUNT (*) cnt_SOFVBGDFTBL FROM stgcsa.xover_SOFVBGDFTBL_view);
create or replace view stgcsa.XOVER_SOFVPYMTTBL_view as                   
  select * from stgcsa.SOFVPYMTTBL
    WHERE pymtpostpymtdt > SYSDATE - 400;  
grant select on stgcsa.XOVER_SOFVPYMTTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVCDCMSTRTBL_view as                
  select * from stgcsa.SOFVCDCMSTRTBL;                
grant select on stgcsa.XOVER_SOFVCDCMSTRTBL_view to stgcsadevrole;              
                                                                                
create or replace view stgcsa.XOVER_SOFVAUDTTBL_view as                   
  select * from stgcsa.SOFVAUDTTBL;                   
grant select on stgcsa.XOVER_SOFVAUDTTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVLND2TBL_view as                   
  select * from stgcsa.SOFVLND2TBL;                   
grant select on stgcsa.XOVER_SOFVLND2TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVRTSCTBL_view as                   
  select * from stgcsa.SOFVRTSCTBL;                   
grant select on stgcsa.XOVER_SOFVRTSCTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVCTCHTBL_view as                   
  select * from stgcsa.SOFVCTCHTBL;                   
grant select on stgcsa.XOVER_SOFVCTCHTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVFTPOTBL_view as                   
  select * from stgcsa.SOFVFTPOTBL;                   
grant select on stgcsa.XOVER_SOFVFTPOTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVGNTYTBL_view as                   
  select * from stgcsa.SOFVGNTYTBL;                   
grant select on stgcsa.XOVER_SOFVGNTYTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVGRGCTBL_view as                   
  select * from stgcsa.SOFVGRGCTBL;                   
grant select on stgcsa.XOVER_SOFVGRGCTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVLND1TBL_view as                   
  select * from stgcsa.SOFVLND1TBL;                   
grant select on stgcsa.XOVER_SOFVLND1TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBGDFTBL_view as                   
  select * from stgcsa.SOFVBGDFTBL;                   
grant select on stgcsa.XOVER_SOFVBGDFTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVDUEBTBL_view as                   
  select * from stgcsa.SOFVDUEBTBL;                   
grant select on stgcsa.XOVER_SOFVDUEBTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVDFPYTBL_view as                   
  select * from stgcsa.SOFVDFPYTBL;                   
grant select on stgcsa.XOVER_SOFVDFPYTBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBFFATBL_view as                   
  select * from stgcsa.SOFVBFFATBL;                   
grant select on stgcsa.XOVER_SOFVBFFATBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVBGW9TBL_view as                   
  select * from stgcsa.SOFVBGW9TBL;                   
grant select on stgcsa.XOVER_SOFVBGW9TBL_view to stgcsadevrole;                 
                                                                                
create or replace view stgcsa.XOVER_SOFVOTRNTBL_view as                   
  select * from stgcsa.SOFVOTRNTBL;                   
grant select on stgcsa.XOVER_SOFVOTRNTBL_view to stgcsadevrole;  
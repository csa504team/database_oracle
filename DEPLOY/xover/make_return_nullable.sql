begin
  for r in 
    (Select 'alter table stgcsa.'||table_name
      ||' modify ('||column_name||' null)' sqltxt
      from dba_tab_columns where owner='STGCSA' 
      and table_name like 'XOVER_RETURN%' and nullable='N'
      order by 1) 
  loop
    execute immediate r.sqltxt;
  end loop;
end;         
/
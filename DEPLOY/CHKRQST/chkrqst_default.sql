-- deploy scripts for deploy chkrqst_default created on Wed 07/31/2019 13:21:00.96 by johnlow
define deploy_name=chkrqst
define package_name=default
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off
/* execute GIT Pull to ensure all files are current 
From https://bitbucket.org/csa504team/database_oracle
   32b7b33..e0f9411  master     -> origin/master
Updating 32b7b33..e0f9411
Fast-forward
 CDCONLINE/Procedures/chkrqstseltsp.sql | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\CDCONLINE\Tables\chkrqsttbl.sql 
John low committed b156fca on Tue Jul 30 16:24:09 2019 -0400

-- C:\CSA\database_oracle\CDCONLINE\Procedures\chkrqstseltsp.sql 
Harsha Chunduru committed e0f9411 on Wed Jul 31 17:15:10 2019 +0000

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Tables\chkrqsttbl.sql"
CREATE TABLE CDCONLINE.CHKRQSTTBL
(
  CHKNMB          VARCHAR2(255 BYTE),
  ISSDT           DATE,
  CHKAMT          NUMBER(15,2),
  LOANNMB         VARCHAR2(255 BYTE),
  RCPNTNM         VARCHAR2(80 BYTE),
  MAILADRSTRNM    VARCHAR2(80 BYTE),
  MAILADRCTYNM    VARCHAR2(80 BYTE),
  MAILADRSTCD     VARCHAR2(80 BYTE),
  MAILADRZIPCD    VARCHAR2(80 BYTE),
  ESCHTMNTDT      DATE,
  CHKREASON       VARCHAR2(255 BYTE),
  LASTARPUPDDT    DATE,
  CDCREGNCD       CHAR(2 BYTE),
  CDCNMB          CHAR(4 BYTE),
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE
)
TABLESPACE CDCONLINEDATATBS;


GRANT SELECT ON CDCONLINE.CHKRQSTTBL TO CDCONLINEREADALLROLE;

-- Deploy component file "C:\CSA\database_oracle\CDCONLINE\Procedures\chkrqstseltsp.sql"
create or replace PROCEDURE           cdconline.ChkRqstSelTSP
(
	p_Identifier IN number := 0,
	p_CDCRgnCD IN char default null,
	p_CDCNmb IN char default null,
	p_RetVal OUT number,
	p_ErrVal OUT number,
	p_ErrMsg OUT varchar2,
	p_SelCur OUT SYS_REFCURSOR
)AS
 -- variable declaration
 BEGIN
	 SAVEPOINT ChkRqstSelTSP;
	 
-- CDC's will see all records
	IF p_Identifier = 0
		THEN
		/* select from CHKRQSTTBL Table */
	 BEGIN
		 OPEN p_SelCur FOR
			SELECT
				CHKNMB,
				ISSDT,
				CHKAMT,
				LOANNMB,
				RCPNTNM,
				MAILADRSTRNM,
				MAILADRCTYNM,
				MAILADRSTCD,
				MAILADRZIPCD,
				ESCHTMNTDT,
				LASTARPUPDDT
			FROM CHKRQSTTBL;
		 
		 p_RetVal := SQL%ROWCOUNT;
		 p_ErrVal := SQLCODE;
		 p_ErrMsg := SQLERRM;
	 END;
	 
-- CDC's will see their own records
	 ELSIF p_Identifier = 1
		THEN
		/* select from CHKRQSTTBL Table */
	BEGIN
		 OPEN p_SelCur FOR
			SELECT
				CHKNMB,
				ISSDT,
				CHKAMT,
				LOANNMB,
				RCPNTNM,
				MAILADRSTRNM,
				MAILADRCTYNM,
				MAILADRSTCD,
				MAILADRZIPCD,
				ESCHTMNTDT,
				LASTARPUPDDT
			FROM CHKRQSTTBL
			WHERE TRIM(CDCREGNCD) = TRIM(p_CDCRgnCD)
			AND TRIM(CDCNMB) = TRIM(p_CDCNmb)
			;
		 
		 p_RetVal := SQL%ROWCOUNT;
		 p_ErrVal := SQLCODE;
		 p_ErrMsg := SQLERRM;
	 END;
	 
	 END IF;
	 EXCEPTION
		 WHEN OTHERS THEN
		 BEGIN
			 p_RetVal := 0;
			 p_ErrVal := SQLCODE;
			 p_ErrMsg := SQLERRM;
		 
		 ROLLBACK TO ChkRqstSelTSP;
		 RAISE;
	 END;
 END ChkRqstSelTSP;
 /
 
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO CDCONLINEREADALLROLE;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO cdconlineprtread;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO cdconlinegovread;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO CDCONLINEDEVROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

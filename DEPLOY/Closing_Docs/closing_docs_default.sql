-- deploy scripts for deploy closing_docs_default created on Thu 08/01/2019 15:45:56.67 by johnlow
define deploy_name=closing_docs
define package_name=default
set echo off
set verify off
column tsp new_value tsp 
column gn new_value gn
column usrnm new_value usrnm
column datetime new_value datetime
set feedback off
set heading off
set termout off
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm,
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..lst' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
set feedback on
set heading on
set echo on
set termout off
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\PARTNER\Tables\CDCBNKAGRMTTBL.sql 
John low committed c286527 on Thu Aug 1 15:32:44 2019 -0400

-- C:\CSA\database_oracle\LOANAPP\Tables\LOANCDCCNTCTTBL.sql 
John low committed f49bc87 on Thu Aug 1 14:25:17 2019 -0400

-- C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTDELTSP.sql 
John low committed d7683cd on Thu Aug 1 15:45:25 2019 -0400

-- C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTINSTSP.sql 
John low committed d7683cd on Thu Aug 1 15:45:25 2019 -0400

-- C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTSELTSP.sql 
John low committed d7683cd on Thu Aug 1 15:45:25 2019 -0400

-- C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTUPDTSP.sql 
John low committed d7683cd on Thu Aug 1 15:45:25 2019 -0400

-- C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTINSTSP.sql 
John low committed f49bc87 on Thu Aug 1 14:25:17 2019 -0400

-- C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTSELTSP.sql 
John low committed f49bc87 on Thu Aug 1 14:25:17 2019 -0400

-- C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTUPDTSP.sql 
John low committed f49bc87 on Thu Aug 1 14:25:17 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\PARTNER\Tables\CDCBNKAGRMTTBL.sql"
CREATE TABLE PARTNER.CDCBNKAGRMTTBL
(
  CDCPRTID           NUMBER(7)                  NOT NULL,
  CDCBNKAGRMTSEQNMB  NUMBER(10)                 NOT NULL,
  BNKHQLOCID         NUMBER(7)                  NOT NULL,
  CDCBNKAGRMTSTRTDT  DATE                       NOT NULL,
  CDCBNKAGRMTENDDT   DATE,
  CREATUSERID        VARCHAR2(32 BYTE)          NOT NULL,
  CREATDT            DATE                       NOT NULL,
  LASTUPDTUSERID     VARCHAR2(32 BYTE)          NOT NULL,
  LASTUPDTDT         DATE                       NOT NULL
)
TABLESPACE PARTNERDATA1TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX PARTNER.XPKCDCBNKAGRMTTBL ON PARTNER.CDCBNKAGRMTTBL
(CDCPRTID, CDCBNKAGRMTSEQNMB)
LOGGING
TABLESPACE PARTNERDATA1TBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE PARTNER.CDCBNKAGRMTTBL ADD (
  CONSTRAINT XPKCDCBNKAGRMTTBL
  PRIMARY KEY
  (CDCPRTID, CDCBNKAGRMTSEQNMB)
  USING INDEX PARTNER.XPKCDCBNKAGRMTTBL
  ENABLE VALIDATE);

ALTER TABLE PARTNER.CDCBNKAGRMTTBL ADD (
  CONSTRAINT CDCPRTID 
  FOREIGN KEY (CDCPRTID) 
  REFERENCES PARTNER.PRTTBL (PRTID)
  ENABLE VALIDATE);

GRANT SELECT, UPDATE ON PARTNER.CDCBNKAGRMTTBL TO CLS2FA;

GRANT SELECT, UPDATE ON PARTNER.CDCBNKAGRMTTBL TO CLSAUTH;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO LOAN;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO LOANAPP;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO LOANPOSTSERVSU;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO PARTNERREADALLROLE;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO PARTNERUPDCDROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON PARTNER.CDCBNKAGRMTTBL TO PIMSREP;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO POOLPIMSREADROLE;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO POOLPIMSUPDROLE;

GRANT SELECT, UPDATE ON PARTNER.CDCBNKAGRMTTBL TO SECURITY;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO CDCOnlinePrtRead;

GRANT SELECT ON PARTNER.CDCBNKAGRMTTBL TO CDCOnlineCorpGovPrtUpdate;


-- Deploy component file "C:\CSA\database_oracle\LOANAPP\Tables\LOANCDCCNTCTTBL.sql"
CREATE TABLE LOANAPP.LOANCDCCNTCTTBL
(
  LOANAPPNMB          NUMBER(10),
  CDCCNTCTFULLNM      VARCHAR2(160 BYTE),
  CDCCNTCTPHONE       VARCHAR2(20 BYTE),
  CDCCNTCTEMAILADR    VARCHAR2(255 BYTE),
  CDCCNTCTFAX         VARCHAR2(20 BYTE),
  CLSATTYFULLNM       VARCHAR2(160 BYTE),
  CLSATTYPHONE        VARCHAR2(20 BYTE),
  CLSATTYEMAILADR     VARCHAR2(255 BYTE),
  CLSATTYFAX          VARCHAR2(20 BYTE),
  CLSATTYSBADESIGIND  VARCHAR2(1 BYTE),
  DEBAUTHDT           DATE,
  FILEORDERDT         DATE,
  CREATUSERID         VARCHAR2(32 BYTE)         DEFAULT USER                  NOT NULL,
  CREATDT             DATE                      DEFAULT SYSDATE               NOT NULL,
  LASTUPDTUSERID      VARCHAR2(32 BYTE),
  LASTUPDTDT          DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOANAPP.XPKLOANCDCCNTCTTBL ON LOANAPP.LOANCDCCNTCTTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOANAPP.LOANCDCCNTCTTBL ADD (
  CONSTRAINT XPKLOANCDCCNTCTTBL
  PRIMARY KEY
  (LOANAPPNMB)
  USING INDEX LOANAPP.XPKLOANCDCCNTCTTBL
  ENABLE VALIDATE);
  
/*  

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO CDCONLINEDEVROLE;
GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAPP.LOANCDCCNTCTTBL TO LOANAPPDEVROLE;
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, 
  ON COMMIT REFRESH, QUERY REWRITE, READ, 
  DEBUG, FLASHBACK ON LOANAPP.LOANCDCCNTCTTBL TO CNTRJSOLANKI;


*/

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO CDCONLINEREADALLROLE;



GRANT SELECT, UPDATE ON LOANAPP.LOANCDCCNTCTTBL TO LOAN;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANACCT;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANACCTREADALLROLE;


GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAPP.LOANCDCCNTCTTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANORIGHQOVERRIDE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAPP.LOANCDCCNTCTTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANPRTREAD;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANPRTUPDT;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANREAD;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANSERVSBICGOV;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAPP.LOANCDCCNTCTTBL TO LOANUPDROLE;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO LOANUPDT;

GRANT SELECT ON LOANAPP.LOANCDCCNTCTTBL TO POOLSECADMINROLE;

-- Deploy component file "C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTDELTSP.sql"
CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTDELTSP(
p_IDENTIFIER             NUMBER  := 0,
p_RETVAL             OUT NUMBER,
p_CDCPRTID               NUMBER  :=NULL,
p_BNKHQLOCID             NUMBER  :=NULL,
p_CDCBNKAGRMTSEQNMB      NUMBER:=NULL,
p_CDCBNKAGRMTSTRTDT      DATE    :=NULL,
p_CDCBNKAGRMTENDDT       DATE    :=NULL,
p_CREATUSERID            VARCHAR2:=NULL
)
AS
/*
RY :07/29/2019: Created to delete the records from agreement table for the given prtid and locid.
*/
BEGIN
    
    SAVEPOINT CDCBNKAGRMTDEL;
        
        IF  p_Identifier = 0 THEN
       BEGIN 
       DELETE from PARTNER.CDCBNKAGRMTTBL
				where CDCPRTID=p_CDCPRTID  
                 AND  BNKHQLOCID=p_BNKHQLOCID
                 AND  CDCBNKAGRMTSEQNMB=P_CDCBNKAGRMTSEQNMB ;   

        p_RETVAL :=SQL%ROWCOUNT;
        
        END;
        
       END IF;
                
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK TO CDCBNKAGRMTDEL;
RAISE;
        
END;
/
grant execute on PARTNER.CDCBNKAGRMTDELTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTDELTSP to 'CDCOnlineCorpGovPrtUpdate; 

-- Deploy component file "C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTINSTSP.sql"
CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTINSTSP(
p_IDENTIFIER             NUMBER  := 0,
p_RETVAL             OUT NUMBER,
p_CDCBNKAGRMTSEQNMB  OUT NUMBER,
p_CDCPRTID               NUMBER  :=NULL,
p_BNKHQLOCID             NUMBER  :=NULL,
p_CDCBNKAGRMTSTRTDT      DATE    :=NULL,
p_CDCBNKAGRMTENDDT       DATE    :=NULL,
p_CREATUSERID            VARCHAR2:=NULL
)
AS
/*RY :05/07/2019: Created to capture agreements between Certified Development Company and banks.
*/
BEGIN
    
    SAVEPOINT CDCBNKAGRMTINS;
        
        IF  p_Identifier = 0 THEN
        
        BEGIN
        
        BEGIN
        SELECT NVL(MAX(CDCBNKAGRMTSEQNMB),0)+1 INTO p_CDCBNKAGRMTSEQNMB
        FROM PARTNER.CDCBNKAGRMTTBL
        WHERE CDCPRTID = p_CDCPRTID;
        EXCEPTION WHEN OTHERS THEN
        p_CDCBNKAGRMTSEQNMB := 1;
        END;
           
       INSERT INTO PARTNER.CDCBNKAGRMTTBL(CDCPRTID,
                                          CDCBNKAGRMTSEQNMB,
                                          BNKHQLOCID ,
                                          CDCBNKAGRMTSTRTDT,
                                          CDCBNKAGRMTENDDT,
                                          CREATUSERID,
                                          CREATDT,
                                          LASTUPDTUSERID,
                                          LASTUPDTDT)
                                    VALUES(p_CDCPRTID,
                                          p_CDCBNKAGRMTSEQNMB,
                                          p_BNKHQLOCID ,
                                          p_CDCBNKAGRMTSTRTDT,
                                          p_CDCBNKAGRMTENDDT,
                                          p_CREATUSERID,
                                          SYSDATE,
                                          p_CREATUSERID,
                                          SYSDATE);

        p_RETVAL :=SQL%ROWCOUNT;
        
        END;
        
       END IF;
                
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK TO CDCBNKAGRMTINS;
RAISE;
        
END;
/

grant execute on PARTNER.CDCBNKAGRMTinsTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTinsTSP to 'CDCOnlineCorpGovPrtUpdate; 


-- Deploy component file "C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTSELTSP.sql"
CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTSELTSP(
p_IDENTIFIER        NUMBER  := 0,
p_RETVAL        OUT NUMBER,
p_CDCPRTID          NUMBER  := 0,
p_CDCBNKAGRMTSEQNMB NUMBER  := 0,
p_SELCUR        OUT  SYS_REFCURSOR
)
AS
/*
RY :05/07/2019: Created to capture agreements between Certified Development Company and banks
*/
BEGIN

        
    IF  p_Identifier = 0 THEN
    /* Select Row On the basis of Key*/
    BEGIN
        OPEN p_SelCur FOR
        SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL 
        WHERE CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSEQNMB =p_CDCBNKAGRMTSEQNMB;        
        p_RETVAL := SQL%ROWCOUNT;            
    END;
    ELSIF  p_Identifier = 2 THEN
    /*Check The Existance Of Row*/
    BEGIN
        OPEN p_SelCur FOR
        SELECT 1 FROM PARTNER.CDCBNKAGRMTTBL  
        WHERE CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSEQNMB =p_CDCBNKAGRMTSEQNMB; 
        p_RETVAL := SQL%ROWCOUNT;                            
    END;
    ELSIF  p_Identifier = 11 THEN
    /* GETS ALL ACTIVE AGREEMENTS BASED ON PRTID*/
    BEGIN
        
        OPEN p_SelCur FOR
         SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              p.prtlocnm as BNKNM,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL l,partner.prtloctbl p 
        WHERE l.BNKHQLOCID = p.locid(+)
        and CDCPRTID =p_CDCPRTID 
        AND CDCBNKAGRMTSTRTDT<=SYSDATE 
        AND (CDCBNKAGRMTENDDT >=SYSDATE OR CDCBNKAGRMTENDDT IS NULL);

        p_RETVAL :=SQL%ROWCOUNT;
    END;
    ELSIF  p_Identifier = 12 THEN
    /* GETS ALL AGREEMENTS BASED ON PRTID*/
    BEGIN
        
        OPEN p_SelCur FOR
         SELECT CDCPRTID,
              CDCBNKAGRMTSEQNMB,
              BNKHQLOCID,
              p.prtlocnm as BNKNM,
              CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT
        FROM PARTNER.CDCBNKAGRMTTBL l,partner.prtloctbl p
        WHERE l.BNKHQLOCID = p.locid(+)
        and CDCPRTID =p_CDCPRTID;

        p_RETVAL :=SQL%ROWCOUNT;
    END;
  
    END IF;
                
EXCEPTION
WHEN OTHERS THEN
RAISE;
                            
END CDCBNKAGRMTSELTSP;
/

grant execute on PARTNER.CDCBNKAGRMTselTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTsELTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTselTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTselTSP to 'CDCOnlineCorpGovPrtUpdate; 


-- Deploy component file "C:\CSA\database_oracle\PARTNER\Procedures\CDCBNKAGRMTUPDTSP.sql"
CREATE OR REPLACE PROCEDURE PARTNER.CDCBNKAGRMTUPDTSP(
p_IDENTIFIER         NUMBER  := 0,
p_RETVAL         OUT NUMBER,
p_CDCPRTID           NUMBER  :=NULL,
p_CDCBNKAGRMTSEQNMB  NUMBER  :=NULL,
p_BNKHQLOCID         NUMBER  :=NULL,
p_CDCBNKAGRMTSTRTDT  DATE    :=NULL,
p_CDCBNKAGRMTENDDT   DATE    :=NULL,
p_LASTUPDTUSERID     VARCHAR2:=NULL
)
AS
/*
RY :05/07/2019: Created to capture agreements between Certified Development Company and banks
*/
BEGIN
    SAVEPOINT CDCBNKAGRMTUPD;
        
    IF  p_Identifier = 0 THEN

    BEGIN
  /* Updates based on key*/
      
        UPDATE PARTNER.CDCBNKAGRMTTBL
        SET   BNKHQLOCID= p_BNKHQLOCID,
              CDCBNKAGRMTSTRTDT= p_CDCBNKAGRMTSTRTDT,
              CDCBNKAGRMTENDDT= p_CDCBNKAGRMTENDDT,
              LASTUPDTUSERID  = p_LASTUPDTUSERID,
              LASTUPDTDT  = SYSDATE                                                
        WHERE CDCPRTID = p_CDCPRTID
        AND CDCBNKAGRMTSEQNMB= p_CDCBNKAGRMTSEQNMB;
        p_RetVal := SQL%ROWCOUNT;
    END;
    ELSIF  p_Identifier = 11 THEN
  
    BEGIN
        UPDATE PARTNER.CDCBNKAGRMTTBL
        SET  CDCBNKAGRMTENDDT= p_CDCBNKAGRMTENDDT,
              LASTUPDTUSERID  = p_LASTUPDTUSERID,
              LASTUPDTDT  = SYSDATE                                                
        WHERE CDCPRTID = p_CDCPRTID
        AND BNKHQLOCID= p_BNKHQLOCID;
        p_RetVal := SQL%ROWCOUNT; 
       
    END;
    END IF; 
                
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK TO CDCBNKAGRMTUPD;
RAISE;
END CDCBNKAGRMTUPDTSP;
/

grant execute on PARTNER.CDCBNKAGRMTupdTSP to PARTNERUPDCDROLE; 
grant execute on PARTNER.CDCBNKAGRMTupdTSP to POOLPIMSUPDROLE; 
grant execute on PARTNER.CDCBNKAGRMTUPDTSP to CDCOnlinePrtRead; 
grant execute on PARTNER.CDCBNKAGRMTUPDTSP to 'CDCOnlineCorpGovPrtUpdate; 


-- Deploy component file "C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTINSTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTINSTSP (p_Identifier           IN     NUMBER DEFAULT 0,
                                                        P_Loanappnmb           IN     NUMBER DEFAULT NULL,
                                                        P_CDCCntctFullNm       IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctphone        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctemailadr     IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctfax          IN     VARCHAR2 DEFAULT NULL,
                                                        P_CLSAttyFullNm        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyphone         IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyemailadr      IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyfax           IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattysbadesigind   IN     VARCHAR2 DEFAULT NULL,
                                                        P_Debauthdt            IN     DATE DEFAULT NULL,
                                                        P_Fileorderdt          IN     DATE DEFAULT NULL,
                                                        P_Creatuserid          IN     VARCHAR2 DEFAULT USER,
                                                        P_Creatdt              IN     DATE DEFAULT SYSDATE,
                                                        p_RetVal                  OUT NUMBER)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    SAVEPOINT LOANCDCCNTCTINSTSP;

    /* Insert into LOANCDCCNTCTTBL */
    IF p_Identifier = 0 THEN
        BEGIN
            INSERT INTO LOANAPP.LOANCDCCNTCTTBL (LoanAppNmb,
                                                 CDCCntctFullNm,
                                                 CdccntctPhone,
                                                 CdccntctEmailadr,
                                                 CdccntctFax,
                                                 CLSAttyFullNm,
                                                 ClsattyPhone,
                                                 ClsattyEmailadr,
                                                 ClsattyFax,
                                                 ClsattySBAdesigind,
                                                 DebAuthdt,
                                                 FileOrderdt,
                                                 CreatUserid,
                                                 CreatDt                                                   --,
                                                        -- Lastupdtuserid,
                                                        --  Lastupdtdt
                                                        )
                 VALUES (P_Loanappnmb,
                         P_CDCCntctFullNm,
                         P_Cdccntctphone,
                         P_Cdccntctemailadr,
                         P_Cdccntctfax,
                         P_CLSAttyFullNm,
                         P_Clsattyphone,
                         P_Clsattyemailadr,
                         P_Clsattyfax,
                         P_Clsattysbadesigind,
                         P_Debauthdt,
                         P_Fileorderdt,
                         P_Creatuserid,
                         P_Creatdt);
        END;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
        ROLLBACK TO LOANCDCCNTCTINSTSP;
END LOANCDCCNTCTINSTSP;
/
-- Deploy component file "C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTSELTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTSELTSP (p_Identifier   IN     NUMBER DEFAULT 0,
                                                        p_LoanAppNmb   IN     NUMBER DEFAULT NULL,
                                                        p_RetVal          OUT NUMBER,
                                                        p_SelCur          OUT SYS_REFCURSOR)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    IF p_Identifier = 0 THEN
        /* Select Row On the basis of Key*/
        OPEN p_SelCur FOR SELECT LoanAppNmb,
                                 CDCCntctFullNm,
                                 CdccntctPhone,
                                 CdccntctEmailadr,
                                 CdccntctFax,
                                 CLSAttyFullNm,
                                 ClsattyPhone,
                                 ClsattyEmailadr,
                                 ClsattyFax,
                                 ClsattySBAdesigind,
                                 DebAuthdt,
                                 FileOrderdt,
                                 CreatUserid,
                                 CreatDt                                                                   --,
                            -- Lastupdtuserid,
                            --  Lastupdtdt
                            FROM LOANAPP.LOANCDCCNTCTTBL
                           WHERE LoanAppNmb = p_LoanAppNmb;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
END LOANCDCCNTCTSELTSP;
/
-- Deploy component file "C:\CSA\database_oracle\LOANAPP\Procedures\LOANCDCCNTCTUPDTSP.sql"
CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTUPDTSP (p_Identifier           IN     NUMBER DEFAULT 0,
                                                        P_Loanappnmb           IN     NUMBER DEFAULT NULL,
                                                        P_CDCCntctFullNm       IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctphone        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctemailadr     IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctfax          IN     VARCHAR2 DEFAULT NULL,
                                                        P_CLSAttyFullNm        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyphone         IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyemailadr      IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyfax           IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattysbadesigind   IN     VARCHAR2 DEFAULT NULL,
                                                        P_Debauthdt            IN     DATE DEFAULT   SYSDATE
                                                                                                   - 1000,
                                                        P_Fileorderdt          IN     DATE DEFAULT   SYSDATE
                                                                                                   - 1000,
                                                        P_Lastupdtuserid       IN     VARCHAR2 DEFAULT USER,
                                                        P_Lastupdtdt           IN     DATE DEFAULT SYSDATE,
                                                        p_RetVal                  OUT NUMBER)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    SAVEPOINT LOANCDCCNTCTUPDTSP;

    /* To update LOANCDCCNTCTTBL */
    IF p_Identifier = 0 THEN
        UPDATE LOANAPP.LOANCDCCNTCTTBL
           SET CDCCntctFullNm = NVL (P_CDCCntctFullNm, CDCCntctFullNm),
               CdccntctPhone = NVL (P_Cdccntctphone, Cdccntctphone),
               CdccntctEmailadr = NVL (P_Cdccntctemailadr, Cdccntctemailadr),
               CdccntctFax = NVL (P_Cdccntctfax, Cdccntctfax),
               CLSAttyFullNm = NVL (P_CLSAttyFullNm, CLSAttyFullNm),
               ClsattyPhone = NVL (P_Clsattyphone, Clsattyphone),
               ClsattyEmailadr = NVL (P_Clsattyemailadr, Clsattyemailadr),
               ClsattyFax = NVL (P_Clsattyfax, Clsattyfax),
               ClsattySBAdesigind = NVL (P_Clsattysbadesigind, Clsattysbadesigind),
               DebAuthdt = NVL (P_Debauthdt, DebAuthdt),
               FileOrderdt = NVL (P_Fileorderdt, P_Fileorderdt),
               Lastupdtuserid = P_Lastupdtuserid,
               Lastupdtdt = P_Lastupdtdt
         WHERE LoanAppNmb = P_LoanAppNmb;
    ELSIF p_Identifier = 1 THEN
        UPDATE LOANAPP.LOANCDCCNTCTTBL
           SET CDCCntctFullNm = P_CDCCntctFullNm,
               CdccntctPhone = P_Cdccntctphone,
               CdccntctEmailadr = P_Cdccntctemailadr,
               CdccntctFax = P_Cdccntctfax,
               CLSAttyFullNm = P_CLSAttyFullNm,
               ClsattyPhone = P_Clsattyphone,
               ClsattyEmailadr = P_Clsattyemailadr,
               ClsattyFax = P_Clsattyfax,
               ClsattySBAdesigind = P_Clsattysbadesigind,
               DebAuthdt = P_Debauthdt,
               FileOrderdt = P_Fileorderdt,
               Lastupdtuserid = P_Lastupdtuserid,
               Lastupdtdt = P_Lastupdtdt
         WHERE LoanAppNmb = P_LoanAppNmb;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
        ROLLBACK TO LOANCDCCNTCTUPDTSP;
END LOANCDCCNTCTUPDTSP;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..LST

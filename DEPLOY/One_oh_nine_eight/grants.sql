/*
select distinct 'grant '||privilege||' on cdconline.cdc1098tbl to '
  ||grantee||';' from 
  (select table_name, grantee, privilege from dba_tab_privs 
  where owner='CDCONLINE' and table_name = 'LOANTBL'
  order by 1,2,3);  
*/
grant DELETE on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant SELECT on cdconline.cdc1098tbl to STGCSA;                                 
grant INSERT on cdconline.cdc1098tbl to CDCONLINEDEVROLE;                       
grant INSERT on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant UPDATE on cdconline.cdc1098tbl to CDCONLINEDEVROLE;                       
grant UPDATE on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant SELECT on cdconline.cdc1098tbl to CDCONLINEREADALLROLE;                   
grant SELECT on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant UPDATE on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant SELECT on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant INSERT on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant SELECT on cdconline.cdc1098tbl to CDCONLINEDEVROLE;                       
grant DELETE on cdconline.cdc1098tbl to CDCONLINEDEVROLE;                       
grant DELETE on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              

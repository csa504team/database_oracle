--------------------------------------------------------
--  DDL for Table CDC1098TBL
--------------------------------------------------------
-- alter table CDCONLINE.CDC1098TBL drop constraint pk_cdc_1098;
-- drop index CDCONLINE.PK_CDC_1098;
-- drop table ;
  CREATE TABLE CDCONLINE.CDC1098TBL 
   (            TAXYR CHAR(4 BYTE),                 
   -- Note:  This value will be 2018, 2019, etc., which should be the 
   --  previous year.
                LOANNMB CHAR(10 BYTE),                     
   -- For example, when we run the 1098 job in 2019, it will be for 
   --    tax year 2018.
                PAYERID CHAR(14 BYTE),
                CDCNMB CHAR(6 BYTE),
                TAXID CHAR(10 BYTE), 
                ASSUMPIND CHAR(1 BYTE),
                COSTCNTR VARCHAR2(80 BYTE),
                TRANSID CHAR(18 BYTE),
                INTAPPLAMT NUMBER(15,2),
                MTGPRINAMT NUMBER(15,2),
                MTGORGLDT DATE,
                RFNDOVRPDINTAMT NUMBER(15,2),
                MTGINSPREMSAMT NUMBER(15,2),
                PTSPAID CHAR(15 BYTE),
                LOANSECURBYREALPROPIND CHAR(1 BYTE),              
                PROPMAILADRSTR1NM VARCHAR2(80 BYTE), 
                PROPMAILADRSTR2NM VARCHAR2(80 BYTE), 
                PROPMAILADRCTYNM VARCHAR2(80 BYTE), 
                PROPMAILADRSTCD CHAR(2 BYTE), 
                PROPMAILADRZIPCD CHAR(5 BYTE), 
                PROPMAILADRZIP4CD CHAR(4 BYTE), 
                PROPDESCCNT VARCHAR2(80 BYTE),
                PROPDESCSTCD CHAR(2 BYTE),
                NMBMTGPROPS CHAR(1 BYTE),
                PROPDESCAPN VARCHAR2(80 BYTE),
                OTHER VARCHAR2(80 BYTE),
                CREATUSERID VARCHAR2(32 BYTE), 
                CREATDT DATE DEFAULT SYSDATE, 
                LASTUPDTUSERID VARCHAR2(32 BYTE), 
                LASTUPDTDT DATE DEFAULT SYSDATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE CDCONLINEDATATBS   NO INMEMORY ;
--------------------------------------------------------
--  DDL for Index PK_CDC_1098
--------------------------------------------------------

/*  
  CREATE UNIQUE INDEX CDCONLINE.PK_CDC_1098 
    ON CDCONLINE.CDC1098TBL (TAXYR,LOANNMB,ASSUMPIND) 
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
    BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
    TABLESPACE CDCONLINEINDTBS ;
 
  alter table CDCONLINE.CDC1098TBL 
    add constraint pk_cdc_1098 primary key (taxyr, loannmb,ASSUMPIND)
    using index CDCONLINE.PK_CDC_1098; 
*/

  CREATE INDEX CDCONLINE.CDC_1098_IX 
    ON CDCONLINE.CDC1098TBL (TAXYR,LOANNMB,ASSUMPIND) 
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
    BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
    TABLESPACE CDCONLINEINDTBS ;

grant DELETE on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant SELECT on cdconline.cdc1098tbl to STGCSA;                                 
grant INSERT on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant UPDATE on cdconline.cdc1098tbl to CDCONLINEDEVROLE;                       
grant UPDATE on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant SELECT on cdconline.cdc1098tbl to CDCONLINEREADALLROLE;                   
grant SELECT on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;              
grant UPDATE on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant SELECT on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant INSERT on cdconline.cdc1098tbl to CSAUPDTROLE;                            
grant DELETE on cdconline.cdc1098tbl to CDCONLINECORPGOVPRTUPDATE;      
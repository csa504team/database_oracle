define deploy_name=CSADEV-160
define package_name=default
define package_buildtime=20200629145536
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-160_default created on Mon 06/29/2020 14:55:39.37 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-160_default created on Mon 06/29/2020 14:55:39.37 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-160_default: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\PayoutStmtDataSelTSP.sql 
Jasleenkaur Gorowada committed 8d04db5 on Mon Jun 29 14:53:42 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\AppFundsDelCSP.sql 
Jasleenkaur Gorowada committed 8d04db5 on Mon Jun 29 14:53:42 2020 -0400

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\APPFUNDSEMAILSELCSP.sql 
Jasleenkaur Gorowada committed 8d04db5 on Mon Jun 29 14:53:42 2020 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\PayoutStmtDataSelTSP.sql"
create or replace 
PROCEDURE           CDCONLINE.PayoutStmtDataSelTSP 
(
  p_region_num IN CHAR DEFAULT NULL ,
  p_cdc_num IN CHAR DEFAULT NULL ,
  p_statement_date IN DATE DEFAULT NULL ,
  p_SelCur1 OUT SYS_REFCURSOR,
  p_SelCur2 OUT SYS_REFCURSOR
)
AS

BEGIN

   OPEN  p_SelCur1 FOR
      SELECT CDCRegnCd CDCRegnCd  ,
             CDCNmb CDCNmb  ,
             UPPER(StmtDt) StmtDt  ,
             LoanNmb LoanNmb  ,
             PrgrmNmb PrgrmNmb  ,
             UPPER(BorrNm) BorrNm  ,
             DbentrAmt DbentrAmt  ,
             CDCFeeAmt CDCFeeAmt  ,
             MultiCDCFeeInd MultiCDCFeeInd  ,
             WithheldAmt WithheldAmt  ,
             ACHInd ACHInd  ,
             ChkACHAmt ChkACHAmt  
        FROM PayoutStmtTbl 
       WHERE  CDCRegnCd = p_region_num
                AND CDCNmb = p_cdc_num
                AND StmtDt = p_statement_date
        ORDER BY UPPER(BorrNm) ;
   --- calculate the statment check/ach Amt
   OPEN  p_SelCur2 FOR
      SELECT SUM(ChkACHAmt)  total_check_ach_amount  
        FROM PayoutStmtTbl 
       WHERE  CDCRegnCd = p_region_num
                AND CDCNmb = p_cdc_num
                AND StmtDt = p_statement_date ;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\AppFundsDelCSP.sql"
create or replace 
PROCEDURE            CDCONLINE.AppFundsDelCSP
(
  p_SOD IN CHAR DEFAULT NULL ,
  p_RegionNum IN CHAR DEFAULT NULL ,
  p_CDC_Cert IN CHAR DEFAULT NULL ,
  p_SBA IN NUMBER DEFAULT 0 ,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN

   DELETE FROM CDCONLINE.TTTempAppFundsTbl;
   -- CDC_App_Funds_R null, '10','468 ',0
   IF p_SBA = 1 THEN
    INSERT INTO CDCONLINE.TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
     SELECT sp.UserLevelRoleID UserLevelRoleID  ,
            UPPER(sp.DistNm) DistNm  ,
            sp.CDCRegnCd CDCRegnCd  ,
            sp.CDCNmb CDCNmb  ,
            UPPER(sp.CDCNm) CDCNm  ,
            UPPER(sp.BorrNm) BorrNm  ,
            UPPER(sp.StmtNm) StmtNm  ,
            ap.LoanNmb LoanNmb  ,
            ap.PymtRecvDDtx PymtRecvDDtx  ,
            ap.SBAFeeAmt SBAFeeAmt  ,
            ap.CSAFeeAmt CSAFeeAmt  ,
            ap.CDCFeeAmt CDCFeeAmt  ,
            ap.LateFeeAmt LateFeeAmt  ,
            ap.IntApplAmt IntApplAmt  ,
            ap.PrinApplAmt PrinApplAmt  ,
            ap.DiffToEscrowAmt DiffToEscrowAmt  ,
            NULL RefiLoan  ,
			USER ,
			USER
       FROM PortflTbl sp
              JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
      WHERE  sp.CDCRegnCd = p_RegionNum
               AND sp.CDCNmb = p_CDC_Cert
       ORDER BY UPPER(sp.StmtNm);

   --select * from CDCPortflTbl
   ELSE
      IF LTRIM(RTRIM(p_SOD)) > 0 THEN
       INSERT INTO TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
        SELECT sp.UserLevelRoleID UserLevelRoleID  ,
               UPPER(sp.DistNm) DistNm  ,
               sp.CDCRegnCd CDCRegnCd  ,
               sp.CDCNmb CDCNmb  ,
               UPPER(sp.CDCNm) CDCNm  ,
               UPPER(sp.BorrNm) BorrNm  ,
               UPPER(sp.StmtNm) StmtNm  ,
               ap.LoanNmb LoanNmb  ,
               ap.PymtRecvDDtx PymtRecvDDtx  ,
               ap.SBAFeeAmt SBAFeeAmt  ,
               ap.CSAFeeAmt CSAFeeAmt  ,
               ap.CDCFeeAmt CDCFeeAmt  ,
               ap.LateFeeAmt LateFeeAmt  ,
               ap.IntApplAmt IntApplAmt  ,
               ap.PrinApplAmt PrinApplAmt  ,
               ap.DiffToEscrowAmt DiffToEscrowAmt  ,
               NULL RefiLoan  ,
			   USER ,
			   USER
          FROM PortflTbl sp
                 JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb

        --WHERE		UserLevelRoleID = @SOD
        WHERE  sp.CDCRegnCd = p_RegionNum
                 AND sp.CDCNmb = p_CDC_Cert
                 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
          ORDER BY UPPER(sp.StmtNm);
      ELSE
         INSERT INTO CDCONLINE.TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
           SELECT sp.UserLevelRoleID UserLevelRoleID  ,
                  UPPER(sp.DistNm) DistNm  ,
                  sp.CDCRegnCd CDCRegnCd  ,
                  sp.CDCNmb CDCNmb  ,
                  UPPER(sp.CDCNm) CDCNm  ,
                  UPPER(sp.BorrNm) BorrNm  ,
                  UPPER(sp.StmtNm) StmtNm  ,
                  ap.LoanNmb LoanNmb  ,
                  ap.PymtRecvDDtx PymtRecvDDtx  ,
                  ap.SBAFeeAmt SBAFeeAmt  ,
                  ap.CSAFeeAmt CSAFeeAmt  ,
                  ap.CDCFeeAmt CDCFeeAmt  ,
                  ap.LateFeeAmt LateFeeAmt  ,
                  ap.IntApplAmt IntApplAmt  ,
                  ap.PrinApplAmt PrinApplAmt  ,
                  ap.DiffToEscrowAmt DiffToEscrowAmt  ,
                  NULL RefiLoan  ,
				  USER ,
				  USER
             FROM CDCONLINE.PortflTbl sp
                    JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
            WHERE  sp.CDCRegnCd = p_RegionNum
                     AND sp.CDCNmb = p_CDC_Cert
             ORDER BY UPPER(sp.StmtNm);
      END IF;
   END IF;
   MERGE INTO CDCONLINE.TTTempAppFundsTbl p
   USING (SELECT p.ROWID row_id, ln.RefiLoan
   FROM CDCONLINE.TTTempAppFundsTbl p
          JOIN LoanTbl ln   ON p.LoanNmb = ln.LoanNmb ) src
   ON ( p.ROWID = src.row_id )
   WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
   OPEN  p_SelCur1 FOR
      SELECT UserLevelRoleID UserLevelRoleID  ,
             UPPER(DistNm) DistNm  ,
             CDCRegnCd CDCRegnCd  ,
             CDCNmb CDCNmb  ,
             UPPER(CDCNm) CDCNm  ,
             UPPER(BorrNm) BorrNm  ,
             UPPER(StmtNm) StmtNm  ,
             LoanNmb LoanNmb  ,
             PymtRecvDt PymtRecvDt  ,
             SBAFeeAmt SBAFeeAmt  ,
             CSAFeeAmt CSAFeeAmt  ,
             CDCFeeAmt CDCFeeAmt  ,
             LateFeeAmt LateFeeAmt  ,
             IntApplAmt IntApplAmt  ,
             PrinApplAmt PrinApplAmt  ,
             DiffToEscrowAmt DiffToEscrowAmt  ,
             RefiLoan RefiLoan
        FROM CDCONLINE.TTTempAppFundsTbl  
		ORDER BY UPPER(StmtNm);
EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Procedures\APPFUNDSEMAILSELCSP.sql"
create or replace 
PROCEDURE           CDCONLINE.AppFundsEmailSelCSP 
(
  p_SOD IN CHAR DEFAULT NULL ,
  p_RegionNum IN CHAR DEFAULT NULL ,
  p_CDC_Cert IN CHAR DEFAULT NULL ,
  p_SBA IN NUMBER DEFAULT 0 ,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN

   IF p_SBA = 1 THEN
    OPEN  p_SelCur1 FOR
      SELECT sp.UserLevelRoleID ,
             UPPER(sp.DistNm) DistNm ,
             sp.CDCRegnCd ,
             sp.CDCNmb ,
             UPPER(sp.CDCNm) CDCNm,
             UPPER(sp.BorrNm) BorrNm,
             --sp.StmtNm,
             ap.LoanNmb ,
             ap.PymtRecvDDtx ,
             ap.SBAFeeAmt ,
             ap.CSAFeeAmt ,
             ap.CDCFeeAmt ,
             ap.IntApplAmt ,
             ap.PrinApplAmt ,
             ap.DiffToEscrowAmt 
        FROM PortflTbl sp
               JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
       WHERE  sp.CDCRegnCd = p_RegionNum
                AND sp.CDCNmb = p_CDC_Cert
        ORDER BY UPPER(sp.BorrNm) ;
   ELSE
      IF LTRIM(RTRIM(p_SOD)) > 0 THEN
       OPEN  p_SelCur1 FOR
         SELECT sp.UserLevelRoleID ,
                UPPER(sp.DistNm) DistNm,
                sp.CDCRegnCd ,
                sp.CDCNmb ,
                UPPER(sp.CDCNm) CDCNm,
                UPPER(sp.BorrNm) BorrNm,
                --sp.StmtNm,
                ap.LoanNmb ,
                ap.PymtRecvDDtx ,
                ap.SBAFeeAmt ,
                ap.CSAFeeAmt ,
                ap.CDCFeeAmt ,
                ap.IntApplAmt ,
                ap.PrinApplAmt ,
                ap.DiffToEscrowAmt 
           FROM PortflTbl sp
                  JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
          
         --WHERE		SODNmb = @SOD
         WHERE  sp.CDCRegnCd = p_RegionNum
                  AND sp.CDCNmb = p_CDC_Cert
                  AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
           ORDER BY UPPER(sp.BorrNm) ;
      ELSE
         OPEN  p_SelCur1 FOR
            SELECT sp.UserLevelRoleID ,
                   UPPER(sp.DistNm) DistNm,
                   sp.CDCRegnCd ,
                   sp.CDCNmb ,
                   UPPER(sp.CDCNm) CDCNm,
                   --sp.BorrNm,
                   UPPER(sp.StmtNm) StmtNm,
                   ap.LoanNmb ,
                   NVL(ap.PymtRecvDDtx, ' ') PymtRecvDDtx  ,
                   NVL(ap.SBAFeeAmt, 0.00) SBAFee  ,
                   NVL(ap.CSAFeeAmt, 0.00) csc_fee  ,
                   NVL(ap.CDCFeeAmt, 0.00) CDCFeeAmt  ,
                   NVL(ap.IntApplAmt, 0.00) IntAppl  ,
                   NVL(ap.PrinApplAmt, 0.00) PrinAppl  ,
                   NVL(ap.DiffToEscrowAmt, 0.00) DiffToEscrowAmt  
              FROM PortflTbl sp
                     JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
             WHERE  sp.CDCRegnCd = p_RegionNum
                      AND sp.CDCNmb = p_CDC_Cert
              ORDER BY UPPER(sp.StmtNm) ;
      END IF;
   END IF;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

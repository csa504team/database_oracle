define deploy_name=Holidays_2021
define package_name=Holidays_2021
define package_buildtime=20201214 93115
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy Holidays_2021_Holidays_2021 created on Mon 12/14/2020  9:31:16.55 by Jasleen Gorowada
prompt deploy scripts for deploy Holidays_2021_Holidays_2021 created on Mon 12/14/2020  9:31:16.55 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy Holidays_2021_Holidays_2021: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Misc\2021_insert_holidays.sql 
Zeeshan Muhammad committed bbed1ad on Mon Dec 14 02:33:39 2020 +0000

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Misc\2021_insert_holidays.sql"
DELETE FROM cdconline.holidaystbl WHERE holidaydt >= TO_DATE('01/01/2021' ,'mm/dd/yyyy');

INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('01/01/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('01/18/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('02/15/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('05/31/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('07/05/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('09/06/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('10/11/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('11/11/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('11/25/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);
INSERT INTO CDCONLINE.HOLIDAYSTBL(HOLIDAYDT,CREATUSERID,CREATDT,LASTUPDTUSERID,LASTUPDTDT) VALUES (TO_DATE('12/25/2021','mm/dd/yyyy'), USER, SYSDATE, USER, SYSDATE);

COMMIT;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

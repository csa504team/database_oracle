-- dummy data insert into new table STGCSA.SOFVWIREKEYTOTTBL
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/07', 'yyyy/mm/dd')), '50450B', 327222.65, 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/07', 'yyyy/mm/dd')), '504EDFLF', 1225.08, 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/07', 'yyyy/mm/dd')), '504GR', 235153.07, 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/07', 'yyyy/mm/dd')), 'DailySBA  ', 29375.94, 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/07', 'yyyy/mm/dd')), 'DefunctCDC', 122.95, 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/08', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/08', 'yyyy/mm/dd')), '50450B', 15694125.40, 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/08', 'yyyy/mm/dd')), '504EDFLF', 20.93, 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/08', 'yyyy/mm/dd')), '504GR', 543212.17, 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'DailySBA  ', 20026.02, 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')));
INSERT INTO STGCSA.SOFVWIREKEYTOTTBL
(WireKeyDt, WireKeyFileNm, WireKeyTotAmt, CreatUserId, CreatDt, LastUpdtUserId, LastUpdtDt)
VALUES
((TO_DATE('2019/08/08', 'yyyy/mm/dd')), 'DefunctCDC', 264153.88, 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')), 'batchCSA', (TO_DATE('2019/08/09', 'yyyy/mm/dd')));

define deploy_name=csadev-139
define package_name=newsp
define package_buildtime=20191118151659
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy csadev-139_newsp created on Mon 11/18/2019 15:17:04.09 by johnlow
prompt deploy scripts for deploy csadev-139_newsp created on Mon 11/18/2019 15:17:04.09 by johnlow
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy csadev-139_newsp: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy C:\CSA\database_oracle\DEPLOY_WORK\file_list.txt
-- C:\CSA\database_oracle\STGCSA\Procedures\pymt110rptarchvtblcsp.sql 
Harsha Chunduru committed 150f26f on Mon Nov 18 20:08:51 2019 +0000

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "C:\CSA\database_oracle\STGCSA\Procedures\pymt110rptarchvtblcsp.sql"
create or replace PROCEDURE STGCSA.PYMT110RPTARCHVTBLCSP(
P_IDENTIFIER	NUMBER := NULL
, P_POSTDATE	DATE:=SYSDATE
, P_SELCUR 		OUT SYS_REFCURSOR)
AS
	v_prcsind		CHAR;
BEGIN
	--
	IF P_IDENTIFIER = 0 THEN

		SELECT prcsind INTO v_prcsind FROM stgcsa.pymt110rptarchvtbl WHERE TRUNC(postdt) = TRUNC(p_postdate) ORDER BY postdt DESC FETCH FIRST 1 ROW ONLY;

		IF v_prcsind = 'Y' THEN
			BEGIN
				OPEN P_SELCUR FOR
				SELECT
					PRININTTOTAMT,
					UNALLOCAMT,
					REPDAMT,
					FEEPDAMT,
					LATEFEEAMT,
					LENDRFEEAMT,
					PREPAYAMT,
					PRCSIND,
					CREATUSERID,
					CREATDT,
					LASTUPDTUSERID,
					LASTUPDTDT,
					POSTDT
				FROM
					STGCSA.PYMT110RPTARCHVTBL
				WHERE
					TRUNC(POSTDT) = TRUNC(P_POSTDATE)
				ORDER BY
					POSTDT DESC FETCH FIRST 1 ROW ONLY;
			END;
		ELSIF v_prcsind = 'N' THEN
			BEGIN
				UPDATE STGCSA.PYMT110RPTARCHVTBL
					SET PREPAYAMT = ( SELECT SUM(amount) AS PREPAYAMT
									  FROM (SELECT CT.TransID,
												CT.LoanNmb AS LoanNmb,
												CT.TransInd1,
												CT.CreatDt,
												CT.StatIDCur,
												MCTA.MaxOftransAttrID,
												TO_NUMBER(MCTA.AttrVal) AS Amount,
												MCTA2.AttrVal AS ImptDt,
												MCTA3.AttrVal AS PostDate,
												MCTA1.AttrVal AS "Current",
												--RTS.StatNm AS Status,
												MCTA22.AttrVal AS BatchNo,
												MCTA19.AttrVal AS PymtTypeID
											  FROM stgCSA.CoreTransTbl CT
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=20 GROUP BY TransID, AttrVal order by TransID DESC) MCTA ON CT.TransID = MCTA.TransID --PymtAmt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=24 GROUP BY TransID, AttrVal order by TransID DESC) MCTA1 ON CT.TransID = MCTA1.TransID --Current
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=34 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA2 ON CT.TransID = MCTA2.TransID --ImpDt
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=27 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA3 ON CT.TransID = MCTA3.TransID --PostDt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=19 GROUP BY TransID, AttrVal order by TransID DESC) MCTA19 ON CT.TransID = MCTA19.TransID --PymtTyp
											  LEFT OUTER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=22 GROUP BY TransID, AttrVal order by TransID DESC) MCTA22 ON CT.TransID = MCTA22.TransID --BatchNo
											  WHERE CT.StatIDCur IN (6, 7, 18, 33, 41)
											  --INNER JOIN stgCSA.RefTransStatTbl RTS ON RTS.StatID = CT.StatIDCur WHERE 0=0 AND transTypID = 3
											  --AND TRUNC(MCTA3.AttrVal) = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.cashReconDate#">
											  --AND TRIM(RTS.StatNm) IN ('Pending Prepay', 'Prepayment', 'Prepay Reject', 'Prepay NSF', 'Pending Mgmt Approval')
											  AND TRUNC(MCTA3.AttrVal) > add_months(sysdate, -12)
											  ORDER BY ImptDt DESC, Amount DESC)
									),
						LENDRFEEAMT =( SELECT SUM(amount) AS LENDRFEEAMT
									   FROM (SELECT
												CT.TransID,
												CT.LoanNmb AS LoanNmb,
												CT.TransInd1,
												CT.CreatDt,
												CT.StatIDCur,
												MCTA.MaxOftransAttrID,
												TO_NUMBER(MCTA.AttrVal) AS Amount,
												MCTA2.AttrVal AS ImptDt,
												MCTA3.AttrVal AS PostDate,
												MCTA1.AttrVal AS "Current",
												--RTS.StatNm AS Status,
												MCTA22.AttrVal AS BatchNo,
												MCTA19.AttrVal AS PymtTypeID
											  FROM stgCSA.CoreTransTbl CT
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=20 GROUP BY TransID, AttrVal order by TransID DESC) MCTA ON CT.TransID = MCTA.TransID --PymtAmt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=24 GROUP BY TransID, AttrVal order by TransID DESC) MCTA1 ON CT.TransID = MCTA1.TransID --Current
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=34 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA2 ON CT.TransID = MCTA2.TransID --ImpDt
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=27 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA3 ON CT.TransID = MCTA3.TransID --PostDt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=19 GROUP BY TransID, AttrVal order by TransID DESC) MCTA19 ON CT.TransID = MCTA19.TransID --PymtTyp
											  LEFT OUTER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=22 GROUP BY TransID, AttrVal order by TransID DESC) MCTA22 ON CT.TransID = MCTA22.TransID --BatchNo
											  WHERE CT.StatIDCur = 24
											  --INNER JOIN stgCSA.RefTransStatTbl RTS ON RTS.StatID = CT.StatIDCur WHERE 0=0 AND transTypID = 3
											  --AND TRUNC(MCTA3.AttrVal) = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.cashReconDate#">
											  --AND TRIM(RTS.StatNm) IN ('Pending Prepay', 'Prepayment', 'Prepay Reject', 'Prepay NSF', 'Pending Mgmt Approval')
											  AND TRUNC(MCTA3.AttrVal) > add_months(sysdate, -12)
											  ORDER BY ImptDt DESC, Amount DESC
												)
									),
						PRCSIND = 'Y',
						LASTUPDTDT = SYSDATE,
						LASTUPDTUSERID = USER
						WHERE TRUNC(POSTDT) = TRUNC(P_POSTDATE);
						-- the above query will update all records for that post date
			END;

			BEGIN
				OPEN P_SELCUR FOR
				SELECT
					PRININTTOTAMT,
					UNALLOCAMT,
					REPDAMT,
					FEEPDAMT,
					LATEFEEAMT,
					LENDRFEEAMT,
					PREPAYAMT,
					PRCSIND,
					CREATUSERID,
					CREATDT,
					LASTUPDTUSERID,
					LASTUPDTDT,
					POSTDT
				FROM
					STGCSA.PYMT110RPTARCHVTBL
				WHERE
					TRUNC(POSTDT) = TRUNC(P_POSTDATE)
				AND
					PRCSIND = 'Y'
				ORDER BY
					POSTDT DESC FETCH FIRST 1 ROW ONLY;
			END;
		END IF;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END;
END;

/

GRANT EXECUTE ON STGCSA.PYMT110RPTARCHVTBLCSP TO STGCSADEVROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

define deploy_name=CSADEV-139
define package_name=newtable
define package_buildtime=20191017143141
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-139_newtable created on Thu 10/17/2019 14:31:50.95 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-139_newtable created on Thu 10/17/2019 14:31:50.96 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-139_newtable: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
From https://bitbucket.org/csa504team/database_oracle
   1c491e9..edc0278  master     -> origin/master
Updating 1c491e9..edc0278
Fast-forward
 DEPLOY/CSADEV-143/default_filelist.txt | 2 ++
 1 file changed, 2 insertions(+)
 create mode 100644 DEPLOY/CSADEV-143/default_filelist.txt
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Tables\PYMT110RPTARCHTBL.sql 
Jasleen0605 committed 1c491e9 on Wed Oct 16 14:55:05 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Tables\PYMT110RPTARCHTBL.sql"
CREATE TABLE STGCSA.PYMT110RPTARCHTBL
(
  PITRANSFER      NUMBER(15,2),
  UNALLOCAMT      NUMBER(15,2),
  REPAIDAMT       NUMBER(15,2),
  FEEPAIDAMT      NUMBER(15,2),
  LATEFEEAMT      NUMBER(15,2),
  LENDERFEE       NUMBER(15,2),
  PREPAYMENTS     NUMBER(15,2),
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
TABLESPACE STGCSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


--GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO CSADEVROLE;

--GRANT SELECT ON STGCSA.PYMT110RPTARCHTBL TO CSAREADALLROLE;

--GRANT INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON STGCSA.PYMT110RPTARCHTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO LOANCSAREVIEWERROLE;

--GRANT DELETE, INSERT, SELECT, UPDATE ON STGCSA.PYMT110RPTARCHTBL TO STGCSADEVROLE;

GRANT SELECT ON STGCSA.PYMT110RPTARCHTBL TO STGCSAREADALLROLE;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

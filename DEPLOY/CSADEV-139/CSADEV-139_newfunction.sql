define deploy_name=CSADEV-139
define package_name=newfunction
define package_buildtime=20190926124246
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy CSADEV-139_newfunction created on Thu 09/26/2019 12:42:47.80 by Jasleen Gorowada
prompt deploy scripts for deploy CSADEV-139_newfunction created on Thu 09/26/2019 12:42:47.82 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy CSADEV-139_newfunction: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Functions\fn_bizdateadd.sql 
hchunduru committed e12bb54 on Thu Sep 26 11:57:43 2019 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\STGCSA\Functions\fn_bizdateadd.sql"
create or replace FUNCTION           "FN_BIZDATEADD" 
--drop function        fn_BizDateAdd


(
  v_in_date IN DATE,
  v_biz_day IN NUMBER
)
RETURN DATE
AS
   v_out_date DATE;
   v_weekday NUMBER(3,0);
   v_interim_date DATE;
   v_interval NUMBER(5,0);
   v_days_added NUMBER(10,0);
/*
        select dbo.fn_BizDateAdd('2/1/2008', -7)
        select dbo.fn_BizDateAdd('2/1/2008', 0)
        select dbo.fn_BizDateAdd('2/1/2008', 1)
*/


BEGIN
   v_days_added := 0 ;
   IF v_biz_day > 0 THEN
    v_interval := 1 ;
   ELSE
      IF v_biz_day < 0 THEN
       v_interval := -1 ;
      ELSE
         v_interval := 0 ;
      END IF;
   END IF;
   v_interim_date := v_in_date ;
   WHILE v_days_added < ABS(v_biz_day) 
   LOOP 
      DECLARE
         v_temp NUMBER(1, 0) := 0;
      
      BEGIN
         v_interim_date := v_interim_date + v_interval;
         v_weekday := TO_CHAR(v_interim_date, 'D');
         -- if not a businiess day, keep searching for the next business day
         BEGIN
            SELECT 1 INTO v_temp
              FROM DUAL
             WHERE NOT ( v_weekday IN ( 1,7 )


           OR v_interim_date IN ( SELECT HOLIDAYDT 
                                  FROM STGCSA.REFCALNDRHOLIDAYTBL  )
          );
         EXCEPTION
            WHEN OTHERS THEN
               NULL;
         END;
            
         IF v_temp = 1 THEN
          v_days_added := v_days_added + 1 ;
         END IF;
      
      END;
   END LOOP;
   v_out_date := v_interim_date ;
   RETURN (v_out_date);


EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON STGCSA.FN_BIZDATEADD TO STGCSADEVROLE;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

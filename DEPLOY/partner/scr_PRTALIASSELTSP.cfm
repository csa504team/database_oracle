<!--- Saved 01/25/2018 10:37:55. --->
PROCEDURE PRTALIASSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_LocId NUMBER := NULL,
 p_VALIDPRTSUBCATTYP VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd 
 */
 StoO_rowcnt NUMBER:=0;
 BEGIN
 SAVEPOINT PrtAliasSel;
 
 IF p_Identifier = 0 THEN
 BEGIN
 OPEN p_SelCur FOR 
 SELECT PrtId,
 ValidPrtAliasTyp,
 PrtAliasNm
 FROM PrtAliasTbl 
 WHERE PrtId = p_PrtId 
 and ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtAliasTbl 
 WHERE PrtId = p_PrtId 
 and ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier = 11 THEN
 /* Select alias name for a given location id and alias type*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 ValidPrtAliasTyp,
 PrtAliasNm
 FROM PrtAliasTbl a, PrtLocTbl b 
 WHERE LocId = p_LocId 
 and a.PrtId = b.PrtId 
 and a.ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 12 THEN
 /* Check if Alias already exists */
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtAliasTbl pa, PrtTbl p 
 WHERE pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp 
 AND pa.PrtAliasNm = p_PrtAliasNm 
 AND pa.PrtId = p.PrtId 
 AND p.PrtCurStatCd = 'O'; 
 p_RetVal :=SQL%ROWCOUNT;
 end;
 elsif p_identifier = 13 then
 begin
 OPEN p_SelCur FOR
 SELECT p.PrtId, p.PrtLglNm, p.PhyAddrStr1Txt,p.PhyAddrCtyNm,p.PhyAddrStCd,p.PhyAddrPostCd, pa.PrtAliasNm 
 FROM PrtSrchTbl p, PrtAliasTbl pa 
 WHERE p.PrtId = pa.PrtId 
 AND p.PrtCurStatCd = 'O' 
 AND p.ValidLocTyp='HO' 
 AND pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 AND UPPER(pa.PrtAliasNm) LIKE upper(p_PrtAliasNm)+'%';
 p_RetVal :=SQL%ROWCOUNT;
 end; 
 elsif p_identifier = 14 then
 begin
 OPEN p_SelCur FOR
 SELECT p.PrtId ,p.PrtLglNm, p.ValidPrtPrimCatTyp, l.PrtLocNm, l.ValidLocTyp,
 p.PhyAddrStr1Txt, p.PhyAddrCtyNm, p.PhyAddrStCd , p.PhyAddrPostCd ,l.LocId ,
 l.PrtLocFIRSNmb
 FROM partner.PrtSrchTbl p, partner.LocSrchTbl l, partner.PrtAliasTbl pa
 WHERE pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 AND pa.PrtAliasNm = p_PrtAliasNm
 AND p.PrtId = pa.PrtId 
 AND l.PrtId = p.PrtId
 AND (p.PrtCurStatCd = 'O' OR p.PrtCurStatCd = 'I' OR p.PrtCurStatCd = 'C');
 p_RetVal :=SQL%ROWCOUNT;
 end;
 elsif p_identifier = 15 then
 begin
 OPEN p_SelCur FOR
 SELECT p.PrtLglNm,p.PhyAddrStr1Txt, p.PhyAddrCtyNm, p.PhyAddrStCd , p.PhyAddrPostCd 
 FROM partner.PrtSrchTbl p, partner.PrtAliasTbl pa
 where pa.PrtId = p.PrtId
 and p.VALIDPRTSUBCATTYP = p_VALIDPRTSUBCATTYP
 and pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 and pa.PrtALiasNm = p_PrtAliasNm;
 p_RetVal :=SQL%ROWCOUNT;
 end;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAliasSel;
 RAISE;
 
 END PRTALIASSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


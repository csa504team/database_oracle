<!--- Saved 05/05/2015 13:16:33. --->
PROCEDURE OFCBYSTSELCSP(
 p_userName VARCHAR2 := NULL,
 p_stateCd CHAR := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 SAVEPOINT OFCBYSTSELCSP;
 
 
 
 OPEN p_SelCur FOR
 SELECT DISTINCT o.SBAOfcCd
 , o.SBAOfcTypCd
 , o.SBAOfc1Nm
 FROM SBAREF.SBAOfcTbl o, SBAREF.CntyTbl c 
 WHERE o.SBAOfcCd = c.OrignOfcCd 
 AND 
 (o.SBAOfcEndDt > SYSDATE 
 or o.SBAOfcEndDt IS NULL) 
 AND 
 (c.CntyEndDt > SYSDATE 
 or c.CntyEndDt IS NULL) 
 AND c.StCd = p_stateCd 
 ORDER BY o.SBAOfc1Nm ;
 p_RetVal :=SQL%ROWCOUNT;
 
 
 EXCEPTION
 WHEN OTHERS THEN 
 RAISE;
 ROLLBACK TO SAVEPOINT OFCBYSTSELCSP;
 
 END OFCBYSTSELCSP;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 04/24/2017 15:24:11. --->
PROCEDURE GETAREAOFOPRDTLSELTSP (
 p_prtId NUMBER := NULL,
 p_prtAgrmtSeqNmb NUMBER := NULL,
 p_parentPrgrmSeqNmb NUMBER := NULL,
 p_statuscode CHAR := NULL,
 p_srchby CHAR := NULL,
 p_stateCd CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_officeCd CHAR := NULL)
 AS
 /* SB - 04/24/2017 -- Modified as per Ian's email */
 
 BEGIN
 OPEN p_selcur FOR
 SELECT c.StCd,
 c.CntyCd,
 CASE
 WHEN pa.PRTAGRMTEFFDT < SYSDATE -- Active, Inactive or Terminated
 THEN
 CASE
 WHEN ( PrtAreaOfOperCntyEffDt <= SYSDATE
 AND ( PrtAreaOfOperCntyEndDt > SYSDATE
 OR PrtAreaOfOperCntyEndDt IS NULL))
 THEN
 'Y'
 ELSE
 'N'
 END
 ELSE --- Pending Future agreement
 CASE
 WHEN ( PrtAreaOfOperCntyEffDt > SYSDATE
 AND ( PrtAreaOfOperCntyEndDt > SYSDATE
 OR PrtAreaOfOperCntyEndDt IS NULL))
 THEN
 'Y'
 ELSE
 'N'
 END
 END
 AS Selected,
 c.OrignOfcCd,
 c.CntyNm,
 s.SBAOfc1Nm,
 a.PrtAreaOfOperCntyEffDt,
 a.PrtAreaOfOperSeqNmb,
 pa.PRTAGRMTEFFDT,
 PrtAreaOfOperCntyEndDt
 FROM sbaref.CntyTbl c
 LEFT OUTER JOIN sbaref.SBAOfcTbl s
 ON ( c.OrignOfcCd = s.SBAOfcCd
 AND (s.SBAOfcEndDt > SYSDATE OR s.SBAOfcEndDt IS NULL))
 LEFT OUTER JOIN partner.PrtAreaOfOperTbl a
 ON ( c.StCd = a.StCd
 AND c.CntyCd = a.CntyCd
 AND a.PrtAgrmtSeqNmb = p_prtAgrmtSeqNmb
 AND a.PrtId = p_prtId
 AND PrtAreaOfOperCntyEndDt IS NULL)
 LEFT OUTER JOIN PARTNER.PRTAGRMTTBL pa
 ON pa.prtid = a.Prtid
 AND pa.PrtAgrmtSeqNmb = a.PrtAgrmtSeqNmb
 WHERE (c.CntyEndDt > SYSDATE OR c.CntyEndDt IS NULL)
 AND c.StCd IN (SELECT DISTINCT StCd
 FROM partner.PrtAreaOfOperTbl
 WHERE PrtAgrmtSeqNmb = p_prtAgrmtSeqNmb
 AND PrtId = p_prtId
 AND PrtAreaOfOperCntyEndDt IS NULL)
 ORDER BY StCd, SBAOfc1Nm, CntyNm;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


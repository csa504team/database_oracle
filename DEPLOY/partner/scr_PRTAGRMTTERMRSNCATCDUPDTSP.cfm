<!--- Saved 05/05/2015 13:16:59. --->
PROCEDURE PRTAGRMTTERMRSNCATCDUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCatCd NUMBER := 0,
 p_PrtAgrmtTrmRsnCatDscTxt VARCHAR2 := NULL,
 p_PrtAgrmtTrmRsnCatCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCatCdUpd;
 
 IF p_Identifier = 0 THEN
 /* Update into PrtAgrmtTermRsnCatCd Table */
 BEGIN
 UPDATE PARTNER.PrtAgrmtTermRsnCatCdTbl
 SET PrtAgrmtTermRsnCatCd = p_PrtAgrmtTermRsnCatCd, 
 PrtAgrmtTermRsnCatDescTxt =p_PrtAgrmtTrmRsnCatDscTxt, 
 PrtAgrmtTermRsnCatCreatUserId = p_PrtAgrmtTrmRsnCatCreatUserId,
 PrtAgrmtTermRsnCatCreatDt = SYSDATE
 WHERE PrtAgrmtTermRsnCatCd = p_PrtAgrmtTermRsnCatCd;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtTermRsnCatCdUpd;
 RAISE;
 END PRTAGRMTTERMRSNCATCDUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:10. --->
PROCEDURE PRTAREAOFOPERDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrtAreaOfOperSeqNmb NUMBER := 0,
 p_StCd CHAR := NULL,
 p_CntyCd CHAR := NULL,
 p_OfcCd CHAR := NULL)
 AS
 BEGIN
 SAVEPOINT PrtAreaOfOperDel;
 
 IF p_Identifier = 0 THEN
 /* delete from PrtAreaOfOperTbl Table */
 BEGIN
 DELETE PrtAreaOfOperTbl 
 WHERE PrtId =p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb =p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 ELSIF p_Identifier = 11 THEN
 /* delete all the area of operation for Pending Agreement based on State */
 BEGIN
 DELETE PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND StCd = p_StCd;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 ELSIF p_Identifier = 12 THEN
 /* delete all the area of operation for Pending Agreement based on State and Office*/
 BEGIN
 DELETE PrtAreaOfOperTbl a 
 where exists (select * from sbaref.CntyTbl b 
 where a.CntyCd = b.CntyCd 
 AND b.CntyStrtDt <= SYSDATE 
 AND (b.CntyEndDt > SYSDATE 
 or CntyEndDt IS NULL
 )
 AND b.OrignOfcCd = p_OfcCd
 AND b.StCd = p_StCd)
 AND a.PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND a.StCd = p_StCd;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 ELSIF p_Identifier = 13 THEN
 /* delete all the area of operation for Pending Agreement based on County */
 BEGIN
 DELETE PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND StCd = p_StCd 
 AND CntyCd = p_CntyCd;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAreaOfOperDel;
 RAISE; 
 END PRTAREAOFOPERDELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


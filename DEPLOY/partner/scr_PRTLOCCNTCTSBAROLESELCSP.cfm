<!--- Saved 01/25/2018 10:37:57. --->
procedure PRTLOCCNTCTSBAROLESELCSP
 (
 p_LocId NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR
 )
 as 
 /*
 RY--01/24/2018--OPSMDEV-1335:: Removed the references of PrtCntctDataSourcId
 */
 begin
 savepoint PRTLOCCNTCTSBAROLESELCSP;
 
 open p_SelCur for
 SELECT PrtCntctSBARoleTypCd,
 a.LocId,
 a.PrtCntctNmb,
 PrtCntctFirstNm,
 PrtCntctJobTitlNm,
 PrtCntctLastNm,
 PrtCntctInitialNm,
 PrtCntctPrefixNm,
 PrtCntctSfxNm,
 PrtLocCntctMailStopNm,
 PERSON_ID,
 INST_ID,
 LOC_ID
 FROM PrtLocCntctSBARoleTbl a,PrtLocCntctTbl b
 WHERE a.LocId = b.LocId
 and a.PrtCntctNmb = b.PrtCntctNmb
 and a.LocId = p_LocId 
 and a.PrtCntctNmb = nvl(p_PrtCntctNmb,a.PrtCntctNmb)
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctSBARoleSelCSP;
 RAISE;
 
 END PRTLOCCNTCTSBAROLESELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


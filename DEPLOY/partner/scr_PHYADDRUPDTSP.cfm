<!--- Saved 01/23/2018 15:31:59. --->
PROCEDURE PHYADDRUPDTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PhyAddrSeqNmb NUMBER := 0,
 p_ValidAddrTyp VARCHAR2 := NULL,
 p_PrtCntctNmb NUMBER := 0,
 p_PhyAddrCtyNm VARCHAR2 := NULL,
 p_PhyAddrCntCd VARCHAR2 := NULL,
 p_PhyAddrCntyNm VARCHAR2 := NULL,
 p_PhyAddrFIPSCntyCd CHAR := NULL,
 p_PhyAddrPostCd VARCHAR2 := NULL,
 p_PhyAddrStCd VARCHAR2 := NULL,
 p_PhyAddrStr1Txt VARCHAR2 := NULL,
 p_PhyAddrStr2Txt VARCHAR2 := NULL,
 p_PhyAddrUSDpndcyInd CHAR := NULL,
 p_LOC_ID NUMBER := 0,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_ADDRESS_TYPE_CODE VARCHAR2 := NULL,
 p_INST_ID NUMBER := 0,
 p_PhyAddrCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PhyAddrDataSourcId from ID 0
 */
 -- Trigger variable
 OldDataRec PhyAddrTbl%ROWTYPE;
 NewDataRec PhyAddrTbl%ROWTYPE;
 BEGIN
 IF p_Identifier = 0
 THEN
 BEGIN
 -- get old snapshot
 SELECT *
 INTO OldDataRec
 FROM PhyAddrTbl
 WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 UPDATE PARTNER.PhyAddrTbl
 SET LocId = p_LocId,
 PhyAddrSeqNmb = p_PhyAddrSeqNmb,
 ValidAddrTyp = p_ValidAddrTyp,
 PrtCntctNmb = p_PrtCntctNmb,
 PhyAddrCtyNm = p_PhyAddrCtyNm,
 PhyAddrCntCd = p_PhyAddrCntCd,
 PhyAddrCntyNm = p_PhyAddrCntyNm,
 PhyAddrFIPSCntyCd = p_PhyAddrFIPSCntyCd,
 PhyAddrPostCd = p_PhyAddrPostCd,
 PhyAddrStCd = p_PhyAddrStCd,
 PhyAddrStr1Txt = p_PhyAddrStr1Txt,
 PhyAddrStr2Txt = p_PhyAddrStr2Txt,
 PhyAddrUSDpndcyInd = p_PhyAddrUSDpndcyInd,
 LOC_ID = p_LOC_ID,
 DEPARTMENT = p_DEPARTMENT,
 ADDRESS_TYPE_CODE = p_ADDRESS_TYPE_CODE,
 INST_ID = p_INST_ID,
 lastupdtuserid = p_PhyAddrCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 SELECT *
 INTO NewDataRec
 FROM PhyAddrTbl
 WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 PhyAddrInsUpdTrigCSP (OldDataRec, NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK TO SAVEPOINT PhyAddrUpd;
 END PHYADDRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


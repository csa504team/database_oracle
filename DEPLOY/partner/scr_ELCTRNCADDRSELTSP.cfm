<!--- Saved 01/23/2018 15:31:56. --->
PROCEDURE ELCTRNCADDRSELTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ElctrncAddrNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_ValidElctrncAddrTyp VARCHAR2 := NULL,
 p_PrtCntctNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 * RGG 04/15/2015 -- Added Identifier 14 for returning Contact Information and SBA Role information
 RY--01/23/18--OPSMDEV1335:: Removed the references of ElctrncAddrDataSourcId
 */
 BEGIN
 --SAVEPOINT ELCTRNCADDRSELTSP;
 IF p_Identifier = 0
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 ElctrncAddrNmb,
 ValidElctrncAddrTyp,
 PrtCntctNmb,
 ElctrncAddrTxt,
 LOC_ID,
 DEPARTMENT,
 PERSON_ID,
 TYPE_CODE,
 RANK,
 INST_ID
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId AND ElctrncAddrNmb = p_ElctrncAddrNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId AND ElctrncAddrNmb = p_ElctrncAddrNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.LocId,
 e.ElctrncAddrNmb,
 e.ValidElctrncAddrTyp,
 e.PrtCntctNmb,
 e.ElctrncAddrTxt
 FROM ElctrncAddrTbl e, PrtLocTbl l
 WHERE e.LocId = l.LocId
 AND l.LocId = p_LocId
 AND e.PrtCntctNmb IS NULL
 ORDER BY e.ValidElctrncAddrTyp, e.ElctrncAddrNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 12
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.LocId,
 e.ValidElctrncAddrTyp,
 e.ElctrncAddrNmb,
 e.PrtCntctNmb,
 e.ElctrncAddrTxt
 FROM ElctrncAddrTbl e
 WHERE e.LocId = p_LocId AND e.PrtCntctNmb = p_PrtCntctNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 13
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.LocId,
 e.ValidElctrncAddrTyp,
 e.ElctrncAddrNmb,
 e.PrtCntctNmb,
 e.ElctrncAddrTxt
 FROM partner.ElctrncAddrTbl e, partner.PRTLOCCNTCTSBAROLETBL s
 WHERE e.LocId = s.LocId
 AND e.PrtCntctNmb = s.PrtCntctNmb
 AND s.PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd
 AND e.LocId = p_LocId
 AND e.ValidElctrncAddrTyp = p_ValidElctrncAddrTyp;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 14
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.LocId,
 C.PrtCntctFirstNm,
 C.PrtCntctLastNm,
 C.PrtCntctJobTitlNm,
 e.PrtCntctNmb,
 S.PrtCntctSBARoleTypCd,
 R.PRTCNTCTSBAROLETYPTXT,
 e.ValidElctrncAddrTyp,
 e.ElctrncAddrNmb,
 e.ElctrncAddrTxt
 FROM partner.ElctrncAddrTbl e,
 partner.PRTLOCCNTCTSBAROLETBL S,
 PARTNER.PRTCNTCTSBAROLETYPTBL R,
 partner.PrtLocCntctTbl C
 WHERE e.LocId = p_LocId
 AND e.LocId = s.LocId
 AND e.LocId = C.LocId
 AND e.PrtCntctNmb = S.PrtCntctNmb
 AND e.PrtCntctNmb = C.PrtCntctNmb
 AND S.PrtCntctSBARoleTypCd = R.PRTCNTCTSBAROLETYPCD
 AND R.PRTCNTCTSBAROLETYPCD = 18;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 -- ROLLBACK TO ELCTRNCADDRSELTSP;
 RAISE;
 ROLLBACK;
 END ELCTRNCADDRSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


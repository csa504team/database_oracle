<!--- Saved 05/05/2015 13:18:06. --->
PROCEDURE PRTLOCCNTCTSBAROLEDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0)
 AS
 /*PSP : 11/14/2014 : Modified for: If we delete a LINC Contact role we need to call the stored procedure (PRTGEOLOCCNTRCTDELTSP) that deletes the areas of operation
 */
 v_RetVal NUMBER(5);
 BEGIN
 SAVEPOINT PrtLocCntctSBARoleDel;
 IF p_Identifier = 0 THEN
 /* Delete Row On the basis of Key*/
 BEGIN
 
 IF p_PrtCntctSBARoleTypCd = 17
 THEN
 PARTNER.PRTGEOLOCCNTRCTDELTSP(p_Identifier => 0, 
 p_RetVal=>v_RetVal,
 p_LocId=>p_LocId,
 p_PrtCntctNmb=>p_PrtCntctNmb);
 END IF;
 
 DELETE FROM PrtLocCntctSBARoleTbl 
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctSBARoleDell;
 RAISE;
 END PRTLOCCNTCTSBAROLEDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


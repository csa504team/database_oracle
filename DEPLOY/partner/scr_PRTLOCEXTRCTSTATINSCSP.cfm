<!--- Saved 05/05/2015 13:18:22. --->
PROCEDURE PRTLOCEXTRCTSTATINSCSP(
 p_PrtId number := NULL,
 p_CreatUserId VARCHAR2 := NULL) --PrtId of location to be added.
 AS
 v_CurrentTime DATE;
 v_ErrorMessageTxt VARCHAR2(255);
 v_ErrorNmb NUMBER(5);
 v_LocId NUMBER(7);
 v_PrimaryTyp VARCHAR2(5);
 v_SubcategoryTyp VARCHAR(5);
 v_temp number(3,0) := 0;
 BEGIN
 -- SAVEPOINT PrtLocExtrctStatInsCSP;
 v_CurrentTime := sysdate;
 v_ErrorNmb := 0;
 -- Error Checking...
 IF p_PrtId IS NULL THEN
 BEGIN
 v_ErrorNmb := 99999;
 END;
 END IF;
 --Determine location eligibility for FIRS processing.
 --Skip processing of Microlender locations.
 begin
 SELECT p.ValidPrtPrimCatTyp,
 p.ValidPrtSubCatTyp
 into v_PrimaryTyp,
 v_SubcategoryTyp
 FROM partner.PrtTbl p
 WHERE p.PrtId = p_PrtId;
 EXCEPTION 
 WHEN NO_DATA_FOUND THEN 
 v_PrimaryTyp := NULL; 
 v_SubcategoryTyp := NULL;
 end;
 --r IF (v_PrimaryTyp = "Lendr"
 
 select count(*) into v_temp 
 from ValidPrtSubCatTbl 
 where ValidPrtSubCatFIRSInd = 'N'
 and ValidPrtSubCatTyp = v_SubcategoryTyp;
 
 IF v_temp > 0 then
 BEGIN 
 RETURN;
 END; --Microlender exclusion.
 END IF;
 --Get Head Office location.
 begin
 SELECT MAX(l.LocId) into v_LocId --max just in case.
 FROM partner.PrtLocTbl l
 WHERE l.PrtId = p_PrtId
 AND l.PrtLocFIRSNmb IS NOT NULL
 AND l.ValidLocTyp = 'HO';
 Exception when NO_DATA_FOUND then
 v_LocId := null;
 end;
 -- Add queue entry, either inserting or updating.
 IF v_ErrorNmb = 0 AND v_LocId IS NOT NULL THEN
 BEGIN
 SELECT count(*) into v_temp
 FROM partner.LocExtrctStatTbl s
 WHERE s.LocId = v_LocId;
 IF v_temp > 0 then
 BEGIN
 UPDATE partner.LocExtrctStatTbl
 SET LocExtrctStatLastQueueTime = v_CurrentTime,
 LocExtrctStatExtrctInd = 'N', --Reset status.
 lastupdtuserid = p_CreatUserId,
 lastupdtdt = sysdate
 WHERE LocId = v_LocId;
 EXCEPTION WHEN OTHERS THEN
 v_ErrorNmb := SQLCODE;
 --SELECT v_ErrorNmb = @@ERROR
 END;
 ELSE 
 BEGIN
 INSERT INTO partner.LocExtrctStatTbl
 (LocId,
 LocExtrctStatFirstQueueTime,
 LocExtrctStatLastQueueTime)
 VALUES 
 (v_LocId,
 v_CurrentTime,
 v_CurrentTime); --ExtractedInd defaults to "N"
 EXCEPTION WHEN OTHERS THEN
 v_ErrorNmb := SQLCODE;
 --SELECT v_ErrorNmb = @@ERROR
 END;
 end if;
 END;
 END IF;
 IF v_ErrorNmb != 0 THEN
 BEGIN
 v_ErrorMessageTxt := 'Failed to add ''HO'' location for Partner ''' || RPAD( p_PrtId, 7, ' ') 
 || ''' to the extract queue.';
 RAISE_APPLICATION_ERROR(-20000,v_ErrorMessageTxt);
 ROLLBACK TO PrtLocExtrctStatInsCSP; 
 END;
 END IF;
 
 END PrtLocExtrctStatInsCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


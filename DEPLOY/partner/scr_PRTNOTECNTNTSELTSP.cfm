<!--- Saved 05/05/2015 13:18:48. --->
PROCEDURE PRTNOTECNTNTSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtNoteSeqNmb NUMBER := 0,
 p_PrtNoteCntntSeqNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtNoteCntntSel;
 IF p_Identifier = 0 THEN
 /* Select from PrtNoteCntnt Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtNoteSeqNmb,
 PrtNoteCntntSeqNmb,
 PrtNoteCntntTxt
 FROM PrtNoteCntntTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb 
 and PrtNoteCntntSeqNmb = p_PrtNoteCntntSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtNoteCntntTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb 
 and PrtNoteCntntSeqNmb = p_PrtNoteCntntSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Select from PrtNoteCntnt Table based on LocId and PrtNoteSeqNmb */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtNoteSeqNmb,
 PrtNoteCntntSeqNmb,
 PrtNoteCntntTxt
 FROM PrtNoteCntntTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSel;
 RAISE;
 END PRTNOTECNTNTSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


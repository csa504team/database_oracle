<!--- Saved 05/05/2015 13:17:50. --->
procedure PRTLOCACHACCTINSTSP
 (p_Identifier number := 0, 
 p_RetVal out number,
 p_LocId number := 0,
 p_ValidACHAcctTyp varchar2 := NULL,
 p_ACHRoutingNmb varchar2 := NULL,
 p_ACHAcctNmb varchar2 := NULL,
 p_ACHAcctDesc varchar2 := NULL,
 p_PrtLocACHAcctCreatUserId VARCHAR2 := NULL
 )
 as
 begin
 savepoint PrtLocACHAcctIns;
 if p_Identifier = 0 then
 /* Insert into PrtLocACHAcctTbl Table */
 begin
 INSERT INTO PrtLocACHAcctTbl
 (
 LocId,
 ValidACHAcctTyp,
 ACHRoutingNmb,
 ACHAcctNmb,
 ACHAcctDesc,
 PrtLocACHAcctCreatUserId,
 PrtLocACHAcctCreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT
 )
 VALUES
 (
 p_LocId,
 p_ValidACHAcctTyp,
 p_ACHRoutingNmb,
 p_ACHAcctNmb,
 p_ACHAcctDesc,
 p_PrtLocACHAcctCreatUserId,
 sysdate,
 p_PrtLocACHAcctCreatUserId,
 sysdate
 );
 p_RetVal := SQL%rowcount;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocACHAcctIns;
 RAISE;
 end PrtLocACHAcctInsTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


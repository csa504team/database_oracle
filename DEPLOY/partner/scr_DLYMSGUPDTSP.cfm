<!--- Saved 05/05/2015 13:16:02. --->
PROCEDURE DLYMSGUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_DlyMsgId NUMBER := 0,
 p_DlyMsgTxt VARCHAR2 := NULL,
 p_DlyMsgDt DATE :=NULL,
 p_DlyMsgCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT DlyMsgUpd;
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.DLYMSGTBL
 SET DlyMsgDt = p_DlyMsgDt,
 DlyMsgTxt = p_DlyMsgTxt,
 lastupdtuserid = p_DlyMsgCreatUserId,
 lastupdtdt = SYSDATE
 WHERE DlyMsgId = p_DlyMsgId;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO DlyMsgUpd;
 
 END DLYMSGUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


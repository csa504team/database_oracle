<!--- Saved 05/10/2017 15:50:36. --->
PROCEDURE PRTBYPRTIDSRCHSELCSP(
 p_userName VARCHAR2 DEFAULT NULL,
 p_pSwitch NUMBER DEFAULT NULL,
 p_Column CHAR DEFAULT NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 --SS 04/20/2017 CAFSOPER 715 Added pSwitch 8 
 BEGIN
 --SAVEPOINT PRTBYPRTIDSRCHSEL;
 IF p_pSwitch = 1 THEN -- Firs Number
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm,
 p.PhyAddrStCd,
 p.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 p.PrtLglSrchNm 
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE l.PrtLocFIRSNmb = p_Column
 AND l.PrtId = p.PrtId 
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 2 THEN -- LocId
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm,
 p.PhyAddrStCd,
 p.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 PrtLglSrchNm 
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE l.LocId = ROUND( TO_NUMBER(p_Column),0) 
 AND l.PrtId = p.PrtId 
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 3 THEN -- PrtId
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 p.PrtLocNm,
 p.ValidLocTyp,
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm,
 p.PhyAddrStCd,
 p.PhyAddrPostCd,
 l.LocId,
 p.PrtLocFIRSNmb,
 PrtLglSrchNm 
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE p.PrtId = ROUND( TO_NUMBER(p_Column),0)
 AND p.PrtId = l.PrtId
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 4 THEN -- LocId and Location address
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrStr2Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd,
 l.LocId,
 p.PrtLocFIRSNmb,
 PrtLglSrchNm 
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE l.LocId = ROUND( TO_NUMBER(p_Column),0) 
 AND l.PrtId = p.PrtId 
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 5 then -- LocId
 BEGIN
 open p_SelCur for
 SELECT p.PrtId ,p.PrtLglNm, p.ValidPrtPrimCatTyp, l.PrtLocNm, l.ValidLocTyp,
 p.PhyAddrStr1Txt, p.PhyAddrCtyNm, p.PhyAddrStCd , p.PhyAddrPostCd ,l.LocId ,
 l.PrtLocFIRSNmb,PrtLglSrchNm -- Changed from: p.PrtLocFIRSNmb
 FROM PrtSrchTbl p, LocSrchTbl l
 WHERE l.LocId = round(to_number(p_Column),0)
 AND l.PrtId = p.PrtId
 AND (p.PrtCurStatCd = 'O' OR p.PrtCurStatCd = 'I' OR p.PrtCurStatCd = 'C');
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 6 then-- PrtId
 BEGIN
 open p_SelCur for
 SELECT p.PrtId ,p.PrtLglNm, p.ValidPrtPrimCatTyp, p.PrtLocNm, p.ValidLocTyp,
 p.PhyAddrStr1Txt, p.PhyAddrCtyNm, p.PhyAddrStCd , p.PhyAddrPostCd ,l.LocId ,
 p.PrtLocFIRSNmb,PrtLglSrchNm 
 FROM PrtSrchTbl p ,LocSrchTbl l
 WHERE p.PrtId = round(to_number(p_Column),0)
 AND l.PrtId = p.PrtId
 AND (p.PrtCurStatCd = 'O' OR p.PrtCurStatCd = 'I' OR p.PrtCurStatCd = 'C');
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 7 then-- Firs Number with location address
 BEGIN
 open p_SelCur for
 SELECT p.PrtId ,p.PrtLglNm, p.ValidPrtPrimCatTyp, l.PrtLocNm, l.ValidLocTyp,
 l.PhyAddrStr1Txt,l.PhyAddrStr2Txt, l.PhyAddrCtyNm, l.PhyAddrStCd , l.PhyAddrPostCd ,l.LocId ,
 l.PrtLocFIRSNmb,PrtLglSrchNm -- Changed from: p.PrtLocFIRSNmb
 FROM PrtSrchTbl p, LocSrchTbl l
 WHERE l.PrtLocFIRSNmb = p_Column
 AND l.PrtId = p.PrtId
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_pSwitch = 8 THEN 
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm,
 p.PhyAddrStCd,
 p.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 PrtLglSrchNm 
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE l.LocId = ROUND( TO_NUMBER(p_Column),0) 
 AND l.PrtId = p.PrtId ; 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE; 
 --ROLLBACK TO PRTBYPRTIDSRCHSEL;
 
 
 END PRTBYPRTIDSRCHSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


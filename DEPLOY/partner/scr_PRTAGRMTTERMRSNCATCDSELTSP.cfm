<!--- Saved 05/05/2015 13:16:58. --->
PROCEDURE PRTAGRMTTERMRSNCATCDSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCatCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCatCdSel;
 
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT PrtAgrmtTermRsnCatCd,
 PrtAgrmtTermRsnCatDescTxt
 FROM PrtAgrmtTermRsnCatCdTbl 
 WHERE PrtAgrmtTermRsnCatCd = p_PrtAgrmtTermRsnCatCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT 1 FROM PrtAgrmtTermRsnCatCdTbl 
 WHERE PrtAgrmtTermRsnCatCd = p_PrtAgrmtTermRsnCatCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 4 THEN
 /*Check The Rows*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT PrtAgrmtTermRsnCatCd,
 PrtAgrmtTermRsnCatDescTxt 
 FROM PrtAgrmtTermRsnCatCdTbl; 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtTermRsnCatCdSel;
 RAISE;
 END PRTAGRMTTERMRSNCATCDSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


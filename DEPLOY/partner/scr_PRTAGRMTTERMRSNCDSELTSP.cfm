<!--- Saved 05/05/2015 13:17:00. --->
PROCEDURE PRTAGRMTTERMRSNCDSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCdSel;
 
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT PrtAgrmtTermRsnCd,
 PrtAgrmtTermRsnDispOrdNmb,
 PrtAgrmtTermRsnDescTxt,
 PrtAgrmtTermRsnCatCd
 FROM PrtAgrmtTermRsnCdTbl 
 WHERE PrtAgrmtTermRsnCd =p_PrtAgrmtTermRsnCd;
 
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT 1 FROM PrtAgrmtTermRsnCdTbl 
 WHERE PrtAgrmtTermRsnCd = p_PrtAgrmtTermRsnCd; 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 /* Select all the reason codes and the category description text ordered by category*/
 BEGIN
 OPEN p_SelCur FOR 
 SELECT r.PrtAgrmtTermRsnCd,
 r.PrtAgrmtTermRsnDispOrdNmb,
 r.PrtAgrmtTermRsnDescTxt,
 r.PrtAgrmtTermRsnCatCd,
 rc.PrtAgrmtTermRsnCatDescTxt
 FROM PrtAgrmtTermRsnCdTbl r, PrtAgrmtTermRsnCatCdTbl rc 
 WHERE r.PrtAgrmtTermRsnCatCd = rc.PrtAgrmtTermRsnCatCd 
 ORDER BY rc.PrtAgrmtTermRsnCatCd ; 
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtTermRsnCdSel;
 RAISE;
 END PRTAGRMTTERMRSNCDSELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


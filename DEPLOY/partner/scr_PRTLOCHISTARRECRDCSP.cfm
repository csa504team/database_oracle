<!--- Saved 05/05/2015 13:18:27. --->
PROCEDURE PRTLOCHISTARRECRDCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 StoO_rowcnt number;
 BEGIN
 SAVEPOINT PrtLocHistARRecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryEffDt >= p_ReportBegDateTime 
 AND a.LocHistryEffDt <= p_ReportEndDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryEffDt >= p_ReportBegDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryEffDt <= p_ReportEndDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(84)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' );
 /* BEGIN
 SELECT 1 INTO StoO_rowcnt FROM DUAL
 WHERE EXISTS (
 SELECT a.LocHistryAffctPrtId, a.LocId, a.LocHistryLocNm 
 AffectedTitle, r.LocHistryLocNm ResultingTitle, RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 
 10, ' ') EffectiveDate, a.LocHistryCreatDt DateAddedToPIMS 
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND 
 (r.LocHistryPartyTyp = 'R' 
 AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE, 
 1, 11) || '%' ) );
 END;*/
 END;
 end if; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocHistARRecrd;
 RAISE;
 END PRTLOCHISTARRECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


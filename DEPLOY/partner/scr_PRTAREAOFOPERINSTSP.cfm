<!--- Saved 05/05/2015 13:17:11. --->
PROCEDURE PRTAREAOFOPERINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrtAreaOfOperSeqNmb NUMBER := 0,
 p_StCd CHAR := NULL,
 p_CntyCd CHAR := NULL,
 p_PrtAreaOfOperCntyEffDt DATE := NULL,
 p_PrtAreaOfOperCntyEndDt DATE := NULL,
 p_PrtAreaOfOperCreatUserId VARCHAR2 := NULL,
 p_OfcCd CHAR := NULL
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 NewDataRec PrtAreaOfOperTbl%ROWTYPE;
 v_cnt number(10);
 v_CntyCd char(3);
 BEGIN
 SAVEPOINT PrtAreaOfOperIns;
 IF p_Identifier = 0 THEN
 BEGIN
 INSERT INTO PARTNER.PrtAreaOfOperTbl(
 PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,
 lastupdtuserid,
 lastupdtdt)
 VALUES(
 p_PrtId,
 p_PrtAgrmtSeqNmb,
 p_PrtAreaOfOperSeqNmb,
 p_StCd,
 p_CntyCd,
 p_PrtAreaOfOperCntyEffDt,
 p_PrtAreaOfOperCntyEndDt,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE);
 -- new snapshot
 begin
 select * into NewDataRec from PrtAreaOfOperTbl
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb and PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 BEGIN
 INSERT INTO PARTNER.PrtAreaOfOperTbl(
 PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,
 lastupdtuserid,
 lastupdtdt)
 SELECT 
 p_PrtId,
 p_PrtAgrmtSeqNmb,
 NVL(MAX(PrtAreaOfOperSeqNmb),0) + 1,
 p_StCd,
 p_CntyCd,
 p_PrtAreaOfOperCntyEffDt,
 p_PrtAreaOfOperCntyEndDt,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE 
 FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 -- new snapshot
 begin
 select * into NewDataRec from PrtAreaOfOperTbl o
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 and PrtAreaOfOperSeqNmb = (select max(PrtAreaOfOperSeqNmb) from PrtAreaOfOperTbl z
 WHERE o.PrtId = z.PrtId and o.PrtAgrmtSeqNmb = z.PrtAgrmtSeqNmb);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; 
 END;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 elsif p_Identifier = 12 then
 /* add all the area of operation by state */
 begin
 v_CntyCd := p_CntyCd;
 v_CntyCd := NULL;
 
 select count(*) into v_cnt from sbaref.CntyTbl
 where StCd = p_StCd and CntyCd > NVL(v_CntyCd,'0')
 AND CntyStrtDt <= SYSDATE AND (CntyEndDt > SYSDATE or CntyEndDt is null);
 
 while v_cnt > 0
 loop
 select min(CntyCd) into v_CntyCd from sbaref.CntyTbl where StCd = p_StCd and CntyCd > NVL(v_CntyCd,'0')
 AND CntyStrtDt <= SYSDATE AND (CntyEndDt > SYSDATE or CntyEndDt is null);
 Insert into PrtAreaOfOperTbl(
 PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,LASTUPDTUSERID,LASTUPDTDT )
 select
 p_PrtId,
 p_PrtAgrmtSeqNmb,
 NVL(max(PrtAreaOfOperSeqNmb),0)+1,
 p_StCd,
 v_CntyCd,
 p_PrtAreaOfOperCntyEffDt,
 p_PrtAreaOfOperCntyEndDt,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE
 from PrtAreaOfOperTbl
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 --p_error := SQLCODE;
 -- new snapshot
 begin
 select * into NewDataRec from PrtAreaOfOperTbl o
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 and PrtAreaOfOperSeqNmb = (select max(PrtAreaOfOperSeqNmb) from PrtAreaOfOperTbl z
 WHERE o.PrtId = z.PrtId and o.PrtAgrmtSeqNmb = z.PrtAgrmtSeqNmb);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAreaOfOperInsUpdTrigCSP (NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 select count(*) into v_cnt from sbaref.CntyTbl
 where StCd = p_StCd and CntyCd > NVL(v_CntyCd,'0')
 AND CntyStrtDt <= SYSDATE AND (CntyEndDt > SYSDATE or CntyEndDt is null);
 end loop;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 13 THEN
 /* add all the area of operation by state and office */
 BEGIN
 v_CntyCd := p_CntyCd;
 v_CntyCd := NULL;
 select count(*) into v_cnt from sbaref.CntyTbl 
 where StCd = p_StCd and OrignOfcCd = p_OfcCd and CntyCd > NVL(v_CntyCd,'0')
 AND CntyStrtDt <= sysdate AND (CntyEndDt > sysdate or CntyEndDt is null);
 while v_cnt > 0 
 loop
 SELECT MIN(CntyCd) INTO v_CntyCd FROM SBAREF.CntyTbl 
 WHERE StCd = p_StCd and OrignOfcCd = p_OfcCd 
 and CntyCd > NVL(v_CntyCd,'0') AND CntyStrtDt <= SYSDATE 
 AND (CntyEndDt > SYSDATE or CntyEndDt IS NULL); 
 
 INSERT INTO PARTNER.PrtAreaOfOperTbl(
 PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,LASTUPDTUSERID,LASTUPDTDT)
 SELECT 
 p_PrtId,
 p_PrtAgrmtSeqNmb,
 NVL(MAX(PrtAreaOfOperSeqNmb),0) + 1,
 p_StCd,
 v_CntyCd,
 p_PrtAreaOfOperCntyEffDt,
 p_PrtAreaOfOperCntyEndDt,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE ,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE 
 FROM PARTNER.PrtAreaOfOperTbl
 WHERE PrtId = p_PrtId 
 and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 -- new snapshot
 begin
 select * into NewDataRec from PrtAreaOfOperTbl o
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 and PrtAreaOfOperSeqNmb = (select max(PrtAreaOfOperSeqNmb) from PrtAreaOfOperTbl z
 WHERE o.PrtId = z.PrtId and o.PrtAgrmtSeqNmb = z.PrtAgrmtSeqNmb);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; 
 END;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAreaOfOperInsUpdTrigCSP (NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 select count(*) into v_cnt from sbaref.CntyTbl 
 where StCd = p_StCd and OrignOfcCd = p_OfcCd and CntyCd > NVL(v_CntyCd,'0')
 AND CntyStrtDt <= sysdate AND (CntyEndDt > sysdate or CntyEndDt is null);
 END LOOP;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 14 THEN
 BEGIN
 DELETE PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 INSERT INTO PARTNER.PrtAreaOfOperTbl(
 PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,
 LastUpdtUserId,
 LastUpdtDt)
 SELECT 
 o.PrtId,
 p_PrtAgrmtSeqNmb,
 o.PrtAreaOfOperSeqNmb,
 o.StCd,
 o.CntyCd,
 p_PrtAreaOfOperCntyEffDt,
 NULL,
 p_PrtAreaOfOperCreatUserId,
 SYSDATE,
 p_PrtAreaOfOperCreatUserId,
 sysdate 
 FROM PrtAreaOfOperTbl o, PrtAgrmtTbl ca, PrtAgrmtTbl pa, PrgrmTbl p 
 WHERE ca.PrtId = p_PrtId 
 AND ca.PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND ca.PrgrmId = p.PrgrmId 
 AND p.PrgrmParntPrgrmId IS NOT NULL 
 AND ca.PrtId = pa.PrtId 
 AND p.PrgrmParntPrgrmId = pa.PrgrmId 
 AND pa.PrtId = o.PrtId 
 AND pa.PrtAgrmtSeqNmb = o.PrtAgrmtSeqNmb 
 AND o.PrtAreaOfOperCntyEffDt <= SYSDATE 
 and (o.PrtAreaOfOperCntyEndDt > SYSDATE 
 or o.PrtAreaOfOperCntyEndDt IS NULL);
 
 -- new snapshot
 begin
 SELECT o.* into NewDataRec FROM PrtAreaOfOperTbl o, PrtAgrmtTbl ca, PrtAgrmtTbl pa, PrgrmTbl p 
 WHERE ca.PrtId = p_PrtId AND ca.PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb AND ca.PrgrmId = p.PrgrmId
 AND p.PrgrmParntPrgrmId IS NOT NULL AND ca.PrtId = pa.PrtId AND p.PrgrmParntPrgrmId = pa.PrgrmId
 AND pa.PrtId = o.PrtId AND pa.PrtAgrmtSeqNmb = o.PrtAgrmtSeqNmb AND o.PrtAreaOfOperCntyEffDt <= SYSDATE
 and (o.PrtAreaOfOperCntyEndDt > SYSDATE or o.PrtAreaOfOperCntyEndDt IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; 
 END;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 -- call trigger
 IF SQLCODE = 0 and p_Identifier <> 12
 THEN
 BEGIN
 PrtAreaOfOperInsUpdTrigCSP (NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAreaOfOperIns;
 RAISE; 
 END PRTAREAOFOPERINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:16. --->
PROCEDURE IMSeqNmbSelTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_IMSeqId IN VARCHAR2 := NULL ,
 p_RetVal OUT NUMBER ,
 p_SelCur OUT SYS_REFCURSOR
 )
 
 AS
 BEGIN
 
 SAVEPOINT IMSeqNmbSel;
 
 
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 SELECT IMSeqId,
 IMSeqNmb
 FROM IMSeqNmbTbl
 WHERE IMSeqId = p_IMSeqId;
 
 
 END;
 ELSIF p_Identifier = 2 
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM IMSeqNmbTbl
 WHERE IMSeqId = p_IMSeqId;
 END;
 
 END IF;
 
 
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


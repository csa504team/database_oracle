<!--- Saved 05/05/2015 13:19:21. --->
PROCEDURE PRTWBCLOCUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ValidWBCClassCd NUMBER := 0,
 p_ValidWBCStatCd NUMBER := 0,
 p_PrtWBCLocFirstFundYrNmb NUMBER := 0,
 p_PrtWBCLocPrjctdFnlFundYrNmb NUMBER := 0,
 p_PrtWBCLocCreatUserId VARCHAR2 := NULL) 
 AS
 BEGIN
 SAVEPOINT PrtWBCLocUpd;
 IF p_Identifier = 0 THEN
 /* Updert into PrtWBCLocTbl Table */
 BEGIN
 UPDATE PARTNER.PrtWBCLocTbl
 SET LocId =p_LocId, 
 ValidWBCClassCd =p_ValidWBCClassCd, 
 ValidWBCStatCd =p_ValidWBCStatCd, 
 PrtWBCLocFirstFundYrNmb =p_PrtWBCLocFirstFundYrNmb, 
 PrtWBCLocPrjctdFnlFundYrNmb =p_PrtWBCLocPrjctdFnlFundYrNmb, 
 LASTUPDTUSERID =p_PrtWBCLocCreatUserId, 
 LASTUPDTDT =SYSDATE
 WHERE LocId =p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PrtWBCLocUpd;
 END;
 
 END PRTWBCLOCUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


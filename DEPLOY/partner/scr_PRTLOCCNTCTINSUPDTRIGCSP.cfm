<!--- Saved 05/05/2015 13:18:01. --->
PROCEDURE PrtLocCntctInsUpdTrigCSP (
 NewDataRec PrtLocCntctTbl%ROWTYPE
 )
 AS
 --
 -- Adds the corresponding location to the extract queue.
 --
 -- If a Loan Manager contact is affected, then ALL the
 -- locations for the Partner are queued.
 --
 -- Revision History -----------------------------------------
 -- 04/15/2002 Mike Zheng New database structure
 -- ----------------------------------------------------------
 /*
 Object: Trigger
 Name: Priya Varigala
 Date Written: May 23, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 v_ErrorNmb number(5);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_PartnerId number(7);
 v_PartnerRoleTyp number(11);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 -- Temporary variables
 v_cnt number(11);
 
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_LocationId := NewDataRec.LocId;
 BEGIN
 SELECT pl.PrtLocFIRSRecrdTyp INTO v_FIRSRecordTyp from PrtLocTbl pl WHERE pl.LocId = NewDataRec.LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FIRSRecordTyp := NULL; END;
 BEGIN
 SELECT pl.PrtId INTO v_PartnerId from PrtLocTbl pl WHERE pl.LocId = NewDataRec.LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PartnerId := NULL; END;
 
 BEGIN
 SELECT PrtCntctPosTypCd INTO v_PartnerRoleTyp FROM PrtLocCntctPosTbl b
 WHERE b.LocId = NewDataRec.LocId
 AND b.PrtCntctNmb = NewDataRec.PrtCntctNmb
 AND PrtCntctPosTypCd = 3;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PartnerRoleTyp := NULL; END;
 
 IF v_PartnerRoleTyp = 3 AND v_FIRSRecordTyp = 'HO'
 THEN
 BEGIN
 -- Add all locations for Partner to extract queue.
 LocExtrctStatUpdCSP (v_PartnerId, NewDataRec.LastUpdtUserId);
 v_ErrorNmb := SQLCODE;
 END;
 ELSE
 BEGIN
 -- Add location to extract queue.
 LocExtrctStatInsCSP (v_LocationId, NewDataRec.PrtCntctCreatUserId);
 v_ErrorNmb := SQLCODE;
 END;
 END IF;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31002;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocCntctInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:35. --->
PROCEDURE PRTDESCTYPCDSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtDescTypCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtDescTypCdSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtDescTypCd,
 PrtDescTypTxt
 FROM PrtDescTypCdTbl 
 WHERE PrtDescTypCd = PrtDescTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtDescTypCdTbl 
 WHERE PrtDescTypCd = PrtDescTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 4 THEN
 /*Check The Rows*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtDescTypCd
 PrtDescTypTxt 
 FROM PrtDescTypCdTbl;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtDescTypCdSel;
 RAISE;
 
 END PRTDESCTYPCDSELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


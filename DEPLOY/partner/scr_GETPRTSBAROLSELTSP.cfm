<!--- Saved 03/24/2016 11:46:46. --->
PROCEDURE GETPRTSBAROLSELTSP(
 
 p_locId number :=null,
 p_PrtCntctNmb number :=null,
 p_queryType CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN 
 
 if p_queryType='all' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT csr.PrtCntctSBARoleTypTxt, csr.PrtCntctSBARoleTypCd 
 FROM partner.PrtCntctSBARoleTypTbl csr ;
 END;
 Else if p_queryType='selected' then 
 BEGIN
 OPEN p_SelCur FOR 
 SELECT csr.PrtCntctSBARoleTypTxt,
 case WHEN cs.PrtCntctSBARoleTypCd IS null then 'N' 
 else 'Y' 
 end 
 FROM partner.PrtCntctSBARoleTypTbl csr, partner.PrtLocCntctSBARoleTbl cs 
 WHERE csr.PrtCntctSBARoleTypCd = cs.PrtCntctSBARoleTypCd(+) 
 AND p_locId=cs.LocId 
 AND p_PrtCntctNmb=cs.PrtCntctNmb; 
 END;
 Else
 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT csr.PrtCntctSBARoleTypTxt, csr.PrtCntctSBARoleTypCd 
 FROM partner.PrtLocCntctTbl c, partner.PrtLocCntctSBARoleTbl cs ,partner.PrtCntctSBARoleTypTbl csr 
 WHERE c.LocId = cs.LocId 
 AND c.PrtCntctNmb = cs.PrtCntctNmb 
 AND cs.PrtCntctSBARoleTypCd = csr.PrtCntctSBARoleTypCd(+)
 AND c.LocId = p_locId 
 AND p_PrtCntctNmb=c.PrtCntctNmb;
 END;
 END if;
 
 
 END if;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


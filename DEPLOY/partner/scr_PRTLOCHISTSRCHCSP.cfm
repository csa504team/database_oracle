<!--- Saved 05/05/2015 13:18:30. --->
PROCEDURE PRTLOCHISTSRCHCSP(
 p_LocHistryLocNm VARCHAR2 := NULL,
 p_LocHistryCtyNm VARCHAR2 := NULL,
 p_LocHistryStCd VARCHAR2 := NULL,
 p_LocHistryStr1Txt VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*[SPCONV-ERR(48)]:(LIKE) if using '[' Manual conversion required*/
 BEGIN
 SAVEPOINT PRTLOCHISTSRCH;
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryPartyTyp,
 h.LocHistryPartyTyp,
 a.LocHistryLocNm SearchedForLocationName,
 h.LocHistryLocNm LocationName,
 a.ValidChngCd,
 a.LocHistryLocTyp SearchedLocTyp,
 h.LocHistryLocTyp,
 a.LocHistryStr1Txt SearchedStreet,
 a.LocHistryCtyNm SearchedCty,
 a.LocHistryStCd SearchedSt,
 a.LocHistryPostCd SearchedPostCd,
 h.LocHistryStr1Txt Street,
 h.LocHistryCtyNm Cty,
 h.LocHistryStCd St,
 h.LocHistryPostCd PostCd,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl h 
 WHERE a.LocHistryLocNm LIKE p_LocHistryLocNm 
 AND (a.LocHistryPartyTyp <> h.LocHistryPartyTyp 
 AND a.LocHistryEffDt = h.LocHistryEffDt 
 AND a.LocHistryAffctPrtId = h.LocHistryAffctPrtId 
 AND a.LocId = h.LocId
 );
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescTypSel;
 RAISE;
 
 END PRTLOCHISTSRCHCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


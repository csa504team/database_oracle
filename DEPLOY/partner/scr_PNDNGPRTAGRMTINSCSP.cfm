<!--- Saved 05/05/2015 13:16:41. --->
PROCEDURE PNDNGPRTAGRMTINSCSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtEffDt DATE := NULL,
 p_PrtAgrmtExprDt DATE := NULL,
 p_PrtAgrmtCreatUserId VARCHAR2 := NULL
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 v_PrtAgrmtSeqNmb NUMBER(11);
 v_MaxPrtAgrmtSeqNmb NUMBER(11);
 v_PrtAgrmtDocLocOfcCd CHAR(4);
 v_PrtAgrmtAreaOfOperTxt VARCHAR2(1000);
 v_PrtAgrmtDocLocTxt VARCHAR2(80);
 v_PrtAgrmtWvrInd CHAR(1);
 v_PrgrmAreaOfOperReqdInd char(1);
 v_cnt number(11);
 -- Trigger variable
 PrtAgrmtNewDataRec PrtAgrmtTbl%ROWTYPE;
 PrtAreaOfOperNewDataRec PrtAreaOfOperTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PndngPrtAgrmtIns;
 
 IF p_Identifier = 0
 THEN
 BEGIN
 select count(*) into v_cnt from PrtAgrmtTbl where PrtId = p_PrtId and PrgrmId = p_PrgrmId;
 IF v_cnt > 0
 THEN
 BEGIN /* begin If agreement exists */
 select max(PrtAgrmtSeqNmb) into v_PrtAgrmtSeqNmb from PrtAgrmtTbl
 where PrtId = p_PrtId and PrgrmId = p_PrgrmId;
 
 BEGIN
 select PrtAgrmtDocLocOfcCd into v_PrtAgrmtDocLocOfcCd
 from PrtAgrmtTbl where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtDocLocOfcCd := NULL; END;
 BEGIN
 select PrtAgrmtAreaOfOperTxt into v_PrtAgrmtAreaOfOperTxt
 from PrtAgrmtTbl where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtAreaOfOperTxt := NULL; END;
 BEGIN
 select PrtAgrmtDocLocTxt into v_PrtAgrmtDocLocTxt
 from PrtAgrmtTbl where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtDocLocTxt := NULL; END;
 BEGIN
 select PrtAgrmtWvrInd into v_PrtAgrmtWvrInd
 from PrtAgrmtTbl where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtWvrInd := NULL; END;
 PrtAgrmtInsTSP (11, p_RetVal, p_PrtId, NULL, p_PrgrmId, v_PrtAgrmtDocLocOfcCd, v_PrtAgrmtAreaOfOperTxt,
 v_PrtAgrmtDocLocTxt, p_PrtAgrmtEffDt, p_PrtAgrmtExprDt,
 NULL, NULL, v_PrtAgrmtWvrInd, p_PrtAgrmtCreatUserId);
 
 IF SQLCODE != 0
 THEN
 BEGIN
 rollback to PndngPrtAgrmtIns;
 return;
 END;
 /*ELSE -- call trigger
 BEGIN
 -- get new snapshot
 SELECT * INTO PrtAgrmtNewDataRec FROM PrtAgrmtTbl a
 WHERE PrtId = p_PrtId and PrtAgrmtSeqNmb = (select max(PrtAgrmtSeqNmb) from PrtAgrmtTbl z
 where a.PrtId = z.PrtId);
 PrtAgrmtInsTrigCSP (PrtAgrmtNewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;*/ -- end trigger
 -- Tigger not needed it is called in PrtAgrmtInsTSP
 END IF;
 BEGIN
 select PrgrmAreaOfOperReqdInd into v_PrgrmAreaOfOperReqdInd from PrgrmTbl
 where PrgrmId = p_PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrgrmAreaOfOperReqdInd := NULL; END;
 
 select count(*) into v_cnt from PrtAreaOfOperTbl where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb
 and v_PrgrmAreaOfOperReqdInd = 'Y';
 IF v_cnt > 0
 THEN
 BEGIN /* begin If area of operation exists */
 select max(PrtAgrmtSeqNmb) into v_MaxPrtAgrmtSeqNmb from PrtAgrmtTbl where PrtId = p_PrtId;
 Insert into PrtAreaOfOperTbl (PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt,
 lastupdtuserid,
 lastupdtdt)
 select p_PrtId,
 v_MaxPrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 p_PrtAgrmtEffDt,
 NULL,
 p_PrtAgrmtCreatUserId,
 SYSDATE,
 p_PrtAgrmtCreatUserId,
 SYSDATE
 from PrtAreaOfOperTbl
 where PrtId = p_PrtId
 and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb
 and PrtAreaOfOperCntyEndDt IS NULL;
 
 -- No need for trigger, logic is implemented in front-end
 /*IF SQLCODE != 0
 THEN
 BEGIN
 rollback to PndngPrtAgrmtIns;
 return;
 END;
 ELSE -- call trigger
 BEGIN
 -- get new snapshot
 select p_PrtId, v_MaxPrtAgrmtSeqNmb, PrtAreaOfOperSeqNmb, StCd, CntyCd, p_PrtAgrmtEffDt,
 NULL, p_PrtAgrmtCreatUserId, SYSDATE,p_PrtAgrmtCreatUserId, SYSDATE into PrtAreaOfOperNewDataRec
 from PrtAreaOfOperTbl
 where PrtId = p_PrtId and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb and PrtAreaOfOperCntyEndDt IS NULL;
 PrtAreaOfOperInsUpdTrigCSP (PrtAreaOfOperNewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF;*/
 
 END;/* End If area of operation exists */
 END IF;
 END; /* End If agreement exists */
 ELSE
 RETURN; -- 999
 END IF;
 END; -- end identifier 0
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


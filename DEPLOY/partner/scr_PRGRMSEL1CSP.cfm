<!--- Saved 05/05/2015 13:16:44. --->
PROCEDURE PRGRMSEL1CSP(
 userName IN VARCHAR2 DEFAULT NULL,
 PrgrmId IN VARCHAR2 DEFAULT NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 
 BEGIN
 OPEN p_SelCur FOR
 
 SELECT PrgrmId
 , PrgrmDesc
 , PrgrmDocReqdInd
 , PrgrmEffDt
 , PrgrmFormId
 , ValidPrgrmTyp
 FROM PrgrmTbl 
 WHERE PrgrmId = PrgrmId;
 
 p_RetVal := SQL%ROWCOUNT;
 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO ELCTRNCADDRSELTSP;
 RAISE;
 END PRGRMSEL1CSP;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 03/21/2017 11:43:10. --->
PROCEDURE VALIDATESBAAGNTAGRMTCSP
 (
 p_SBAAGNTPRTID NUMBER :=NULL,
 
 p_SBAAGNTHQLOCID NUMBER :=NULL,
 p_PRGRMCD CHAR:=NULL,
 p_CreatUserId VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 --SS--03/15/2016 OPSMdev1391 find active agreements for given period
 v_count NUMBER(3);
 
 BEGIN
 savepoint VALIDATESBAAGNTAGRMTCSP;
 
 
 begin
 open p_selcur for 
 select PRgrmCd from PARTNER.SBAAGNTAGRMTTBL 
 where
 
 SBAAGNTPRTID = p_SBAAGNTPRTID
 and SBAAGNTHQLOCID = p_SBAAGNTHQLOCID
 and PrgrmCd=p_PrgrmCd
 and (SBAAGNTAGRMTSTRTDT <= Sysdate and (SBAAGNTAGRMTENDDT >=Sysdate or SBAAGNTAGRMTENDDT is NULL))
 
 ;
 END ;
 
 
 Exception When Others Then
 Rollback to VALIDATESBAAGNTAGRMTCSP;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


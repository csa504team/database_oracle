<!--- Saved 01/25/2018 10:37:59. --->
PROCEDURE PRTLOCSELTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtId NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /* SB : 06/05/2014 : Added Identifier 16 to return all of the active PIMS agreements for an given Location Id
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd,PhyAddrDataSourcId
 */
 BEGIN
 SAVEPOINT PrtLocSel;
 
 IF p_Identifier = 0
 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp,
 PrtId,
 PrtLocInactDt,
 PrtLocNm,
 PrtLocOpndDt,
 PrtLocCurStatCd,
 ValidLocTyp,
 LOC_ID,
 INST_ID
 FROM PrtLocTbl
 WHERE LocId = p_LocId;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2
 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtLocTbl
 WHERE LocId = p_LocId;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11
 THEN
 /* Select list of sublocations for a given main location id */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocId,
 a.PrtLocNm,
 a.PrtLocFIRSNmb,
 a.PrtLocCurStatCd
 FROM PrtLocTbl a
 WHERE a.PrtId = (SELECT z.PrtId
 FROM PrtLocTbl z
 WHERE z.LocId = p_LocId);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 12
 THEN
 /* All Physical Addresses of Partner's Location */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocId,
 l.prtlocnm,
 a.PhyAddrSeqNmb,
 a.ValidAddrTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd,
 a.PhyAddrFIPSCntyCd,
 a.PhyAddrCntCd,
 a.PhyAddrUSDpndcyInd,
 va.ValidAddrTypDesc
 FROM PhyAddrTbl a
 LEFT OUTER JOIN ValidAddrTypTbl va
 ON a.ValidAddrTyp = va.ValidAddrTyp,
 PrtLocTbl l
 WHERE a.LocId = l.LocId
 AND l.LocId = p_LocId
 AND a.PrtCntctNmb IS NULL;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 13
 THEN
 /* Based on Location Id, Program select the primary address and agreement information for the
 Main Location i.e HO */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocId,
 a.PrtId,
 b.PrtLglNm,
 c.PhyAddrStr1Txt,
 c.PhyAddrStr2Txt,
 c.PhyAddrCtyNm,
 c.PhyAddrStCd,
 c.PhyAddrPostCd,
 c.PhyAddrFIPSCntyCd,
 (SELECT z.ElctrncAddrTxt
 FROM ElctrncAddrTbl z
 WHERE z.LocId = p_LocId
 AND z.ValidElctrncAddrTyp = 'Phone'
 AND z.ElctrncAddrNmb =
 (SELECT MIN (ElctrncAddrNmb)
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ValidElctrncAddrTyp = 'Phone'
 AND PrtCntctNmb IS NULL))
 Phone,
 (SELECT y.ElctrncAddrTxt
 FROM ElctrncAddrTbl y
 WHERE y.LocId = p_LocId
 AND y.ValidElctrncAddrTyp = 'Fax'
 AND y.ElctrncAddrNmb =
 (SELECT MIN (ElctrncAddrNmb)
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ValidElctrncAddrTyp = 'Fax'
 AND PrtCntctNmb IS NULL))
 Fax,
 d.PrtAgrmtEffDt,
 d.PrtAgrmtExprDt,
 PrtAgrmtTermDt
 FROM PrtLocTbl a,
 PrtTbl b,
 PhyAddrTbl c,
 PrtAgrmtTbl d
 WHERE a.LocId = p_LocId
 AND a.ValidLocTyp = 'HO'
 AND a.PrtId = b.PrtId
 AND a.LocId = c.LocId
 AND c.ValidAddrTyp = 'Phys'
 AND a.PrtId = d.PrtId
 AND d.PrgrmId = p_PrgrmId
 AND d.PrtAgrmtSeqNmb = (SELECT MAX (z.PrtAgrmtSeqNmb)
 FROM PrtAgrmtTbl z
 WHERE z.PrtId = a.PrtId);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 14
 THEN
 /* Based on Partner Id, select all OPEN locations */
 BEGIN
 OPEN p_SelCur FOR
 SELECT b.LocId,
 b.PrtLocNm,
 b.PrtId,
 b.ValidLocTyp,
 c.PhyAddrStr1Txt,
 c.PhyAddrStr2Txt,
 c.PhyAddrCtyNm,
 c.PhyAddrStCd,
 c.PhyAddrPostCd
 FROM PrtTbl a, PrtLocTbl b, PhyAddrTbl c
 WHERE a.PrtId = p_PrtId
 AND a.PrtCurStatCd = 'O'
 AND a.PrtId = b.PrtId
 AND b.PrtLocCurStatCd = 'O'
 AND b.LocId = c.LocId
 AND c.PrtCntctNmb IS NULL;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 15
 THEN
 /* Based on Location Id, Program select the primary address and agreement information for the
 Main Location i.e HO */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocId,
 a.PrtId,
 b.PrtLglNm,
 c.PhyAddrStr1Txt,
 c.PhyAddrStr2Txt,
 c.PhyAddrCtyNm,
 c.PhyAddrStCd,
 c.PhyAddrPostCd,
 c.PhyAddrFIPSCntyCd,
 (SELECT z.ElctrncAddrTxt
 FROM partner.ElctrncAddrTbl z
 WHERE z.LocId = p_LocId
 AND z.ValidElctrncAddrTyp = 'Phone'
 AND z.ElctrncAddrNmb =
 (SELECT MIN (ElctrncAddrNmb)
 FROM partner.ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ValidElctrncAddrTyp = 'Phone'
 AND PrtCntctNmb IS NULL))
 Phone,
 (SELECT y.ElctrncAddrTxt
 FROM partner.ElctrncAddrTbl y
 WHERE y.LocId = p_LocId
 AND y.ValidElctrncAddrTyp = 'Fax'
 AND y.ElctrncAddrNmb =
 (SELECT MIN (ElctrncAddrNmb)
 FROM partner.ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ValidElctrncAddrTyp = 'Fax'
 AND PrtCntctNmb IS NULL))
 Fax,
 (SELECT z.ElctrncAddrTxt
 FROM partner.ElctrncAddrTbl z
 WHERE z.LocId = p_LocId
 AND z.ValidElctrncAddrTyp = 'Email'
 AND z.ElctrncAddrNmb =
 (SELECT MIN (ElctrncAddrNmb)
 FROM partner.ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ValidElctrncAddrTyp = 'Email'
 AND PrtCntctNmb IS NULL))
 Email,
 d.PrtAgrmtEffDt,
 d.PrtAgrmtExprDt,
 PrtAgrmtTermDt
 FROM partner.PrtLocTbl a,
 partner.prtTbl b,
 partner.PhyAddrTbl c,
 partner.PrtAgrmtTbl d
 WHERE a.LocId = p_LocId
 AND a.ValidLocTyp = 'HO'
 AND a.PrtId = b.PrtId
 AND a.LocId = c.LocId
 AND c.ValidAddrTyp = 'Phys'
 AND a.PrtId = d.PrtId
 AND d.PrgrmId = p_PrgrmId
 AND d.PrtAgrmtSeqNmb =
 (SELECT MAX (z.PrtAgrmtSeqNmb)
 FROM partner.PrtAgrmtTbl z
 WHERE z.PrtId = a.PrtId AND d.PrgrmId = z.PrgrmId)
 AND c.PrtCntctNmb IS NULL;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 16
 THEN
 /* All PIMS agreements for a given Location Id */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocId,
 a.PrtLocNm,
 a.PrtId,
 d.PRGRMID,
 c.PRGRMDESC,
 d.PRTAGRMTDOCLOCTXT,
 d.PRTAGRMTEFFDT,
 d.PRTAGRMTEXPRDT,
 d.PRTAGRMTTERMDT,
 d.PRTAGRMTTERMRSNCD,
 d.PRTAGRMTWVRIND,
 d.PRTAGRMTLOANLIMTIND,
 d.PRTAGRMTMAXMOLOANQTY,
 d.PRTAGRMTMAXFYLOANQTY,
 d.PRTAGRMTMAXWKLOANQTY,
 d.PRTAGRMTCREATUSERID,
 d.PRTAGRMTCREATDT
 FROM PrtLocTbl a,
 PrtTbl b,
 PrgrmTbl c,
 PrtAgrmtTbl d
 WHERE a.LocId = p_locid
 -- AND a.ValidLocTyp = 'HO'
 AND a.PrtId = b.PrtId
 AND a.PrtId = d.PrtId
 AND d.prgrmid = c.prgrmid
 AND d.PrgrmId = p_PrgrmId
 AND b.PrtCurStatCd = 'O'
 AND a.PrtLocCurStatCd = 'O'
 AND (d.PrtAgrmtEffDt <= SYSDATE)
 AND ( d.PrtAgrmtExprDt IS NULL
 OR d.PrtAgrmtExprDt >= SYSDATE)
 AND ( d.PrtAgrmtTermDt IS NULL
 OR d.PrtAgrmtTermDt > SYSDATE);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtLocSel;
 RAISE;
 END PRTLOCSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


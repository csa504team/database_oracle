<!--- Saved 03/18/2016 14:13:32. --->
PROCEDURE PRTLOCDESCTYPINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtDescTypCd NUMBER := 0,
 p_PrtLocDescTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocDescTypIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtLocDescTyp Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocDescTypTbl 
 (LocId, PrtLocDescTypSeqNmb, PrtDescTypCd, PrtLocDescTypCreatUserId, PrtLocDescTypCreatDt,lastupdtuserid,lastupdtdt)
 SELECT p_LocId, NVL(MAX(PrtLocDescTypSeqNmb), 0) + 1, p_PrtDescTypCd,
 p_PrtLocDescTypCreatUserId, SYSDATE,p_PrtLocDescTypCreatUserId, SYSDATE 
 FROM PrtLocDescTypTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescTypIns;
 RAISE;
 
 END PRTLOCDESCTYPINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


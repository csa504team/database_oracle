<!--- Saved 05/05/2015 13:17:00. --->
PROCEDURE PRTAGRMTTERMRSNCDINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtTermRsnDispOrdNmb NUMBER := 0,
 p_PrtAgrmtTermRsnDescTxt VARCHAR2 := NULL,
 p_PrtAgrmtTermRsnCatCd NUMBER := 0,
 p_PrtAgrmtTermRsnCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCdIns;
 
 IF p_Identifier = 0 THEN
 /* Insert into PrtAgrmtTermRsnCdTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtAgrmtTermRsnCdTbl 
 (PrtAgrmtTermRsnCd, PrtAgrmtTermRsnDispOrdNmb, 
 PrtAgrmtTermRsnDescTxt,PrtAgrmtTermRsnCatCd,
 PrtAgrmtTermRsnCreatUserId, PrtAgrmtTermRsnCreatDt
 )
 VALUES (p_PrtAgrmtTermRsnCd,p_PrtAgrmtTermRsnDispOrdNmb,p_PrtAgrmtTermRsnDescTxt,
 p_PrtAgrmtTermRsnCatCd, p_PrtAgrmtTermRsnCreatUserId,SYSDATE
 );
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtTermRsnCdIns;
 RAISE;
 END PRTAGRMTTERMRSNCDINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:32:03. --->
PROCEDURE PRTOWNRSHPSELTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtOwnrshpRank NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PrtOwnrshpSel;
 
 IF p_Identifier = 0
 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtOwnrshpRank,
 OwningPrtId,
 ValidOwnrPrtTyp,
 PrtOwnrshpPct,
 ValidOwndPrtTyp,
 PrtOwnrshpCreatUserId,
 PrtOwnrshpCreatDt
 FROM PrtOwnrshpTbl
 WHERE PrtId = p_PrtId AND PrtOwnrshpRank = p_PrtOwnrshpRank;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2
 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtOwnrshpTbl
 WHERE PrtId = p_PrtId AND PrtOwnrshpRank = p_PrtOwnrshpRank;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 BEGIN
 RAISE;
 ROLLBACK TO PrtOwnrshpSel;
 END;
 END PRTOWNRSHPSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:56. --->
PROCEDURE PRTOWNRSHPDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtOwnrshpRank NUMBER := 0)
 AS
 -- trigger varitable
 OldDatarec PrtOwnrshpTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtOwnrshpDel;
 IF p_Identifier = 0 THEN
 /* Delect Row On the basis of Key*/
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtOwnrshpTbl WHERE PrtId = p_PrtId and PrtOwnrshpRank = p_PrtOwnrshpRank;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL;
 end;
 DELETE FROM PrtOwnrshpTbl 
 WHERE PrtId = p_PrtId 
 and PrtOwnrshpRank = p_PrtOwnrshpRank;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtOwnrshpDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtOwnrshpDel;
 RAISE;
 END PRTOWNRSHPDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


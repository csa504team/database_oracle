<!--- Saved 05/05/2015 13:18:26. --->
PROCEDURE PRTLOCHISTARECRDCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 StoO_rowcnt NUMBER;
 BEGIN
 SAVEPOINT PrtLocHistARecrd;
 IF( p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm,
 a.LocHistryStCd,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryCreatDt > p_ReportBegDateTime 
 AND a.LocHistryCreatDt < p_ReportEndDateTime;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm,
 a.LocHistryStCd,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryCreatDt >= p_ReportBegDateTime;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm,
 a.LocHistryStCd,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryCreatDt <= p_ReportEndDateTime;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(74)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm,
 a.LocHistryStCd,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' );
 /*BEGIN
 SELECT 1 INTO StoO_rowcnt FROM DUAL
 WHERE EXISTS (
 SELECT a.LocHistryLocNm AffectedTitle, a.LocHistryStr1Txt, 
 a.LocHistryStr2Txt, a.LocHistryCtyNm, a.LocHistryStCd, 
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate, 
 a.LocHistryCreatDt DateAddedToPIMS 
 FROM LocHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE, 
 1, 11) || '%' ) );
 END;*/
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocHistARecrd;
 RAISE;
 END PRTLOCHISTARECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


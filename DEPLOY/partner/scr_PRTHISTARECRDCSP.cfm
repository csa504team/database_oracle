<!--- Saved 05/05/2015 13:17:42. --->
PROCEDURE PRTHISTARECRDCSP(
 p_ReportBegDateTime IN DATE := NULL,
 p_ReportEndDateTime IN DATE := NULL,
 p_ValidChngCd IN VARCHAR2 := NULL,
 p_PrtId IN NUMBER := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtHistARecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt > p_ReportBegDateTime 
 AND a.PrtHistryCreatDt < p_ReportEndDateTime;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt > p_ReportBegDateTime 
 AND a.PrtHistryCreatDt < p_ReportEndDateTime 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt > p_ReportBegDateTime;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt > p_ReportBegDateTime 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt < p_ReportEndDateTime;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt < p_ReportEndDateTime 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(106)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' );
 END;
 ELSE
 BEGIN
 /*[SPCONV-ERR(116)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' ) 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO PrtHistARecrd;
 RAISE;
 END;
 END PRTHISTARECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


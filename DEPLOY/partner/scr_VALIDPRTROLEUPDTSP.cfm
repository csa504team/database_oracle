<!--- Saved 05/05/2015 13:20:09. --->
PROCEDURE VALIDPRTROLEUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtRoleTyp VARCHAR2:=NULL,
 p_ValidPrtRoleDispOrdNmb NUMBER:=0,
 p_ValidPrtRoleDesc VARCHAR2:=NULL,
 p_ValidPrtRoleCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtRoleUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE ValidPrtRoleTbl
 SET 
 ValidPrtRoleDispOrdNmb =p_ValidPrtRoleDispOrdNmb , 
 ValidPrtRoleDesc =p_ValidPrtRoleDesc, 
 ValidPrtRoleCreatUserId =p_ValidPrtRoleCreatUserId, 
 ValidPrtRoleCreatDt =SYSDATE
 WHERE ValidPrtRoleTyp = p_ValidPrtRoleTyp;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF ;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTROLEUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


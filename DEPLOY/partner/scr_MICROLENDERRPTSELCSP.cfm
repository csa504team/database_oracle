<!--- Saved 05/05/2015 13:16:30. --->
procedure MicroLenderRptSelCSP
 (
 p_SelCur59 OUT SYS_REFCURSOR,
 p_SelCurSt OUT SYS_REFCURSOR
 )
 as
 v_o_prtid number;
 v_o_locid number;
 v_o_stcd char(2);
 v_o_stnm varchar2(60);
 v_servicearea varchar2(4000);
 begin
 delete partner.t_microlendertbl;
 v_o_prtid := 0;
 
 for c1 in(select o.PrtId, o.StCd, o.CntyCd, s.StNm, c.CntyNm, ps.locid
 from partner.PrtAreaOfOperTbl o, partner.PrtAgrmtTbl g, sbaref.StTbl s, sbaref.CntyTbl c, partner.PrtSrchTbl ps
 where o.PrtId = g.PrtId 
 and o.PRTAGRMTSEQNMB = g.PRTAGRMTSEQNMB
 and o.prtid = ps.prtid
 and ps.ValidPrtSubCatTyp = 'Micro'
 AND ps.PrtCurStatCd = 'O'
 and trim(g.PrgrmId) = 'MicroLoans'
 and o.StCd = s.StCd
 and o.StCd = c.StCd
 and o.CntyCd = c.CntyCd
 and g.PrtAgrmtEffDt <= SYSDATE 
 and (g.PrtAgrmtExprDt + 1 > SYSDATE or g.PrtAgrmtExprDt IS NULL)
 and (g.PrtAgrmtTermDt + 1 > SYSDATE or g.PrtAgrmtTermDt IS NULL)
 and (o.PRTAREAOFOPERCNTYEFFDT <= SYSDATE or o.PRTAREAOFOPERCNTYEFFDT IS NULL)
 and (o.PRTAREAOFOPERCNTYENDDT +1 > SYSDATE or o.PRTAREAOFOPERCNTYENDDT IS NULL)
 order by 1,2,3
 )
 loop
 if v_o_PrtId != 0 and (v_o_PrtId != c1.PrtId or v_o_StCd != c1.StCd) then
 insert into partner.t_microlendertbl
 (
 PrtId,
 HoLocId,
 StLocId,
 StCd,
 StNm,
 ServiceArea
 )
 select
 v_o_prtid,
 v_o_locid,
 l.locid,
 v_o_stcd,
 v_o_stnm,
 v_servicearea
 from prtloctbl l, partner.PhyAddrTbl a
 where l.locid = a.locid
 and a.ValidAddrTyp='Phys'
 and a.PrtCntctNmb is NULL
 and l.PRTLOCCURSTATCD = 'O'
 and a.PhyAddrStCd = v_o_stcd
 and l.prtid = v_o_prtid;
 
 if SQL%ROWCOUNT = 0 then
 insert into partner.t_microlendertbl
 (
 PrtId,
 HOLocId,
 StLocId,
 StCd,
 StNm,
 ServiceArea
 )
 values
 (
 v_o_prtid,
 v_o_locid,
 v_o_locid,
 v_o_stcd,
 v_o_stnm,
 v_servicearea
 );
 end if;
 
 v_servicearea := null;
 end if;
 
 v_o_prtid := c1.prtid;
 v_o_locid := c1.locid;
 v_o_stcd := c1.stcd;
 v_o_stnm := c1.stnm;
 v_servicearea := case when v_servicearea is null then c1.CntyNm else v_servicearea || ', ' || c1.CntyNm end;
 end loop;
 
 if v_o_PrtId != 0 then
 insert into partner.t_microlendertbl
 (
 PrtId,
 HOLocId,
 stLocId,
 StCd,
 StNm,
 ServiceArea
 )
 select
 v_o_prtid,
 v_o_locid,
 l.locid,
 v_o_stcd,
 v_o_stnm,
 v_servicearea
 from prtloctbl l, partner.PhyAddrTbl a
 where l.locid = a.locid
 and a.ValidAddrTyp='Phys'
 and a.PrtCntctNmb is NULL
 and l.PRTLOCCURSTATCD = 'O'
 and a.PhyAddrStCd = v_o_stcd
 and l.prtid = v_o_prtid;
 
 if SQL%ROWCOUNT = 0 then
 insert into partner.t_microlendertbl
 (
 PrtId,
 HOLocId,
 StLocId,
 StCd,
 StNm,
 ServiceArea
 )
 values
 (
 v_o_prtid,
 v_o_locid,
 v_o_locid,
 v_o_stcd,
 v_o_stnm,
 v_servicearea
 );
 end if;
 end if;
 
 delete partner.t_microlendertbl_59;
 insert into partner.t_microlendertbl_59
 (
 PrtId,
 LocId
 )
 select
 prtid,
 holocid
 from partner.t_microlendertbl
 group by prtid,holocid
 having count(distinct StCd) >= 59;
 
 delete partner.t_microlendertbl a where exists(select 1 from partner.t_microlendertbl_59 z where z.prtid = a.prtid);
 
 open p_selcur59 for
 select 'Nationwide' as StNm, 
 (select PrtLglNm from partner.prttbl z where z.prtid = p.prtid) as PrtLglNm,
 c.PrtCntctFirstNm || case when trim(PRTCNTCTINITIALNM) is null then '' else ' ' || trim(PRTCNTCTINITIALNM) || '.' end || rtrim(' ' || c.PrtCntctLastNm) as PrtCntctNm, 
 p.PrtId, 
 p.LocId, 
 a.PhyAddrStr1Txt, 
 a.PhyAddrStr2Txt, 
 a.PhyAddrCtyNm, 
 a.PhyAddrStCd, 
 a.PhyAddrPostCd,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.LocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Phone'
 and rownum = 1) as Phone,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.LocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Fax'
 and rownum = 1) as Fax,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.LocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Email'
 and rownum = 1) as Email,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.LocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Web'
 and rownum = 1) as Web,
 c.PrtCntctNmb, 
 c.PRTCNTCTJOBTITLNM as ContactRole,
 null as ServiceArea
 from partner.t_microlendertbl_59 p, partner.PhyAddrTbl a, partner.PrtLocCntctTbl c, partner.PrtLocCntctSBARoleTbl r
 where p.LocId = a.LocId 
 and a.ValidAddrTyp='Phys'
 and a.PrtCntctNmb is NULL
 and c.locid = p.locid
 and c.locid = r.locid
 and c.PrtCntctNmb = r.PrtCntctNmb
 and r.PrtCntctSBARoleTypCd = 1
 order by 2,3;
 
 open p_selcurSt for
 select StNm, 
 (select PrtLglNm from partner.prttbl z where z.prtid = p.prtid) as PrtLglNm,
 p.StLocId as LocId, 
 0 as Seq,
 c.PrtCntctFirstNm || case when trim(PRTCNTCTINITIALNM) is null then '' else ' ' || trim(PRTCNTCTINITIALNM) || '.' end || rtrim(' ' || c.PrtCntctLastNm) as PrtCntctNm, 
 p.PrtId, 
 a.PhyAddrStr1Txt, 
 a.PhyAddrStr2Txt, 
 a.PhyAddrCtyNm, 
 a.PhyAddrStCd, 
 a.PhyAddrPostCd,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Phone'
 and rownum = 1) as Phone,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Fax'
 and rownum = 1) as Fax,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Email'
 and rownum = 1) as Email,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Web'
 and rownum = 1) as Web,
 --c.PrtCntctNmb, 
 'Executive Director' as ContactRole,
 ServiceArea
 from partner.t_microlendertbl p, partner.PhyAddrTbl a, partner.PrtLocCntctTbl c, partner.PrtLocCntctPosTbl r
 where p.StLocId = a.LocId 
 and a.ValidAddrTyp='Phys'
 and a.PrtCntctNmb is NULL
 and c.locid = p.Stlocid
 and c.locid = r.locid
 and c.PrtCntctNmb = r.PrtCntctNmb
 and r.PrtCntctPosTypCd = 4
 union
 select StNm, 
 (select PrtLglNm from partner.prttbl z where z.prtid = p.prtid) as PrtLglNm,
 p.StLocId as LocId, 
 1 as Seq,
 c.PrtCntctFirstNm || case when trim(PRTCNTCTINITIALNM) is null then '' else ' ' || trim(PRTCNTCTINITIALNM) || '.' end || rtrim(' ' || c.PrtCntctLastNm) as PrtCntctNm, 
 p.PrtId, 
 a.PhyAddrStr1Txt, 
 a.PhyAddrStr2Txt, 
 a.PhyAddrCtyNm, 
 a.PhyAddrStCd, 
 a.PhyAddrPostCd,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Phone'
 and rownum = 1) as Phone,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Fax'
 and rownum = 1) as Fax,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Email'
 and rownum = 1) as Email,
 (select ELCTRNCADDRTXT from partner.ElctrncAddrTbl z
 where z.LocId = p.stLocId and z.PrtCntctNmb IS NULL
 and z.ValidElctrncAddrTyp='Web'
 and rownum = 1) as Web,
 --c.PrtCntctNmb, 
 'Microlending' as ContactRole,
 ServiceArea
 from partner.t_microlendertbl p, partner.PhyAddrTbl a, partner.PrtLocCntctTbl c, partner.PrtLocCntctSBARoleTbl r
 where p.StLocId = a.LocId 
 and a.ValidAddrTyp='Phys'
 and a.PrtCntctNmb is NULL
 and c.locid = p.Stlocid
 and c.locid = r.locid
 and c.PrtCntctNmb = r.PrtCntctNmb
 and r.PrtCntctSBARoleTypCd = 1
 order by 1,2,3,4,5;
 end; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


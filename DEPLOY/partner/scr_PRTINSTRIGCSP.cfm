<!--- Saved 05/05/2015 13:17:47. --->
PROCEDURE PRTINSTRIGCSP (
 NewDataRec PrtTbl%ROWTYPE
 )
 AS
 -- Object : Trigger
 -- Date : August 29, 2000
 -- Programmer : Rajeswari DSP
 -- Company : AQUAS, Inc.
 --This trigger loads the into PrtSrchTbl table.
 -- 04/05/2001 Rajeswari DSP Modified code to include RolePrivilege
 -- in search tables.
 v_ErrorNmb number(5);
 v_PartnerId number(7);
 v_EstbDt date;
 v_PartnerLglNm varchar2(200);
 v_PrtLglSrchNm varchar2(200);
 v_PrimCatTyp varchar2(5);
 v_SubCatTyp varchar2(5);
 v_RolePrivilege varchar2(20);
 v_PrtCurStatCd CHAR(1); -- new
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 v_UserId varchar2(32);
 v_RetVal number(5);
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_ErrorNmb := 0;
 v_INST_ID := 0;
 v_PartnerId := NewDataRec.PrtId;
 v_EstbDt := NewDataRec.PrtEstbDt;
 v_PartnerLglNm := NewDataRec.PrtLglNm;
 v_PrimCatTyp := NewDataRec.ValidPrtPrimCatTyp;
 v_SubCatTyp := NewDataRec.ValidPrtSubCatTyp ;
 v_PrtCurStatCd := NewDataRec.PrtCurStatCd;
 v_UserId := NewDataRec.PrtCreatUserId;
 BEGIN
 SELECT RolePrivilegePrgrmId into v_RolePrivilege FROM partner.ValidPrtSubCatTbl WHERE ValidPrtSubCatTyp = v_SubCatTyp;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_RolePrivilege := NULL; END;
 
 -- IAC 05/22/2001 Added following code to move the 'THE' to the end of the legal name:
 IF substr(v_PartnerLglNm,1,4) = 'The '
 THEN
 BEGIN
 v_PrtLglSrchNm := Ltrim(Upper(substr(v_PartnerLglNm,4,length(v_PartnerLglNm)-3) ||' ,'|| substr(v_PartnerLglNm,1,4)));
 EXCEPTION WHEN OTHERS THEN v_PrtLglSrchNm := NULL; END;
 ELSE
 BEGIN
 v_PrtLglSrchNm := Ltrim(Upper(v_PartnerLglNm));
 EXCEPTION WHEN OTHERS THEN v_PrtLglSrchNm := NULL; END;
 END IF;
 
 INSERT INTO partner.PrtSrchTbl (
 PrtId,
 PrtLglNm,
 PrtLglSrchNm,
 ValidPrtPrimCatTyp,
 ValidPrtSubCatTyp,
 RolePrivilegePrgrmId,
 PrtCurStatCd,
 PrtSrchCreatUserId,
 PrtSrchCreatDt,
 LastUpdtUserId,
 LastUpdtDt)
 VALUES (
 v_PartnerId,
 v_PartnerLglNm,
 v_PrtLglSrchNm,
 v_PrimCatTyp,
 v_SubCatTyp,
 v_RolePrivilege,
 v_PrtCurStatCd,
 NewDataRec.PrtCreatUserId,
 NewDataRec.PrtCreatDt,
 NewDataRec.LastUpdtUserId,
 NewDataRec.LastUpdtDt);
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 30006;
 v_ReplicationErrorMessageTxt := ' Partner not inserted in PrtSrchTbl.' || 'PrtId was ' || to_char(v_PartnerId) || '.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END;
 END IF;
 
 IF v_SubCatTyp = 'CDC'
 THEN
 BEGIN
 IF v_EstbDt is null
 THEN
 BEGIN
 v_EstbDt := trunc(SYSDATE);
 UPDATE partner.PrtTbl
 SET PrtEstbDt = trunc(SYSDATE)
 where PrtId = v_PartnerId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 30008;
 v_ReplicationErrorMessageTxt := ' Partner not updated in PrtTbl.' || 'PrtId was ' || to_char(v_PartnerId) || '.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END;
 END IF;
 END;
 END IF;
 
 /* Insert the CDC agreement into Agreement Table */
 PrtAgrmtInsTSP (11, v_RetVal, v_PartnerId, NULL, 'CDC', NULL, NULL, NULL, v_EstbDt, NULL, NULL, NULL, 'N', v_UserId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 30009;
 v_ReplicationErrorMessageTxt := ' Agreement not inserted in PrtAgrmtTbl.' || 'PrtId was ' || to_char(v_PartnerId) || '.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtInsTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END;
 END IF;
 END;
 END IF;
 RETURN;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


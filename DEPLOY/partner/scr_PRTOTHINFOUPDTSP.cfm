<!--- Saved 05/05/2015 13:18:54. --->
PROCEDURE PRTOTHINFOUPDTSP(
 p_Identifier NUMBER := 0,
 p_PrtId NUMBER := null,
 p_PrtSBICLicnsNmb CHAR := null,
 p_PrtCDCNmb CHAR := null,
 p_PrtFmrNm varchar2 := null,
 p_PrtMicroLendrNmb char := null,
 p_PrtTaxId char := null,
 p_PrtTrdNm varchar2 := null,
 p_PrtFRSNmb number := null,
 p_PrtNCUANmb number := null,
 p_PrtFDICNmb number := null,
 p_PrtSCORENmb char := null,
 p_PrtDUNSNmb char := null,
 p_LastUpdtUserId varchar2 := null
 )
 AS
 BEGIN
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.PrtOthInfoTbl SET
 PrtSBICLicnsNmb = p_PrtSBICLicnsNmb, 
 PrtCDCNmb = p_PrtCDCNmb, 
 PrtFmrNm = p_PrtFmrNm, 
 PrtMicroLendrNmb = p_PrtMicroLendrNmb, 
 PrtTaxId = p_PrtTaxId, 
 PrtTrdNm = p_PrtTrdNm, 
 PrtFRSNmb = p_PrtFRSNmb, 
 PrtNCUANmb = p_PrtNCUANmb,
 PrtFDICNmb = p_PrtFDICNmb,
 PrtSCORENmb = p_PrtSCORENmb,
 PrtDUNSNmb = p_PrtDUNSNmb,
 LASTUPDTUSERID = p_LASTUPDTUSERID,
 LASTUPDTDT = SYSDATE
 WHERE PrtId = p_PrtId; 
 
 PARTNER.LOCEXTRCTSTATUPDCSP(p_PrtId,p_LASTUPDTUSERID);
 END;
 END IF;
 END PrtOthInfoUPDTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


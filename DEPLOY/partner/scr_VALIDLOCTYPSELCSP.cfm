<!--- Saved 05/05/2015 13:19:47. --->
PROCEDURE VALIDLOCTYPSELCSP
 (
 p_userName VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER
 
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidLocTyp, 
 ValidLocTypDesc
 FROM ValidLocTypTbl 
 WHERE ValidLocTypDispOrdNmb != 0 
 ORDER BY ValidLocTypDispOrdNmb ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 RAISE;
 ROLLBACK;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


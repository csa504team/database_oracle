<!--- Saved 05/05/2015 13:19:34. --->
PROCEDURE VALIDACHACCTTYPSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidACHAcctTyp VARCHAR2 :=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidACHAcctTypSel;
 IF p_Identifier= 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT ValidACHAcctTyp, 
 ValidACHAcctTypDesc, 
 ValidACHAcctTypDispOrdNmb
 FROM ValidACHAcctTypTbl 
 WHERE ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := SQL%ROWCOUNT;
 end;
 ELSIF p_Identifier= 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM ValidACHAcctTypTbl 
 WHERE ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDACHACCTTYPSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 03/18/2016 23:15:16. --->
PROCEDURE PRTLOCDESCINSTSP(
 Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtLocDescSeqNmb NUMBER := 0,
 p_PrtLocDescTxt VARCHAR2 := NULL,
 p_PrtLocDescCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocDescIns;
 IF Identifier = 0 THEN
 /* Insert into PrtLocDesc Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocDescTbl 
 (LocId,PrtLocDescTypSeqNmb,PrtLocDescSeqNmb,PrtLocDescTxt,PrtLocDescCreatUserId,
 PrtLocDescCreatDt,lastupdtuserid,lastupdtdt)
 SELECT p_LocId,p_PrtLocDescTypSeqNmb, NVL(MAX(PrtLocDescSeqNmb), 0) + 1, p_PrtLocDescTxt,
 p_PrtLocDescCreatUserId, SYSDATE,p_PrtLocDescCreatUserId, SYSDATE 
 FROM PrtLocDescTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescIns;
 RAISE;
 END PRTLOCDESCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


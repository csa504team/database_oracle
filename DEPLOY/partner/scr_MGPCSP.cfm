<!--- Saved 08/11/2014 10:11:43. --->
PROCEDURE MGPCSP(
 p_Identifier number := 0,
 p_RetVal out number,
 p_PrtLglNm VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_LocId NUMBER := 0,
 p_PhyAddrStCd VARCHAR2 := NULL,
 p_PrtCurStatCd CHAR := NULL,
 p_PrtLglNmSrchTyp NUMBER := 0,
 p_SelCur out SYS_REFCURSOR
 )
 AS
 BEGIN
 INSERT 
 INTO partner.TT_tmp1 
 SELECT DISTINCT pa.PrtAliasNm,
 l.PrtLocNm,
 p.PrtLglNm,
 l.LocId,
 p.ValidPrtSubCatTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd 
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p, partner.PrtLocTbl l, 
 partner.PhyAddrTbl a left outer join partner.ElctrncAddrTbl c on a.LocId = c.LocId
 WHERE p.PrtId = pa.PrtId AND l.PrtId = pa.PrtId AND a.LocId = l.LocId 
 AND pa.ValidPrtAliasTyp = 'TIN'
 AND p.ValidPrtSubCatTyp IN ( 'Srty', 'BNDAG') 
 /*AND UPPER(p.PrtLglNm) LIKE (CASE 
 WHEN p_PrtLglNmSrchTyp = 1 THEN nvl(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm)) || '%' 
 WHEN p_PrtLglNmSrchTyp = 2 THEN '%' || nvl(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm)) || '%' 
 WHEN p_PrtLglNmSrchTyp = 3 THEN nvl(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm)) 
 ELSE UPPER(p.PrtLglNm) END) 
 AND pa.PrtAliasNm LIKE nvl(p_PrtAliasNm, pa.PrtAliasNm) 
 AND l.LocId = nvl(p_LocId, l.LocId) 
 AND a.PhyAddrStCd = nvl(p_PhyAddrStCd, a.PhyAddrStCd)
 AND l.PrtLocCurStatCd = 'O' 
 AND p.PrtCurStatCd IN (CASE WHEN p_PrtCurStatCd = 'O' 
 THEN 'O'
 WHEN p_PrtCurStatCd = 'C'
 THEN 'C'
 WHEN p_PrtCurStatCd = 'D'
 THEN 'D'
 WHEN p_PrtCurStatCd = 'A'
 THEN p.PrtCurStatCd
 ELSE p.PrtCurStatCd
 end)*/;
 
 
 open p_SelCur for select * from partner.TT_tmp1 a ;
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


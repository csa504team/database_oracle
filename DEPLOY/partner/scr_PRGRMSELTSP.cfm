<!--- Saved 05/05/2015 13:16:46. --->
PROCEDURE PRGRMSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrgrmId VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 SAVEPOINT PrgrmSel;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 OPEN p_SelCur FOR SELECT PrgrmId
 , PrgrmAreaOfOperReqdInd
 , PrgrmDispOrdNmb
 , PrgrmDocReqdInd
 , PrgrmEffDt
 , PrgrmFormId
 , PrgrmMaxTrmMoQty
 , PrgrmDesc
 , ValidPrgrmTyp
 , PrgrmParntPrgrmId
 FROM PrgrmTbl 
 WHERE PrgrmId = p_PrgrmId;
 
 END;
 
 ELSIF p_Identifier = 11 
 THEN
 BEGIN
 
 OPEN p_SelCur FOR SELECT PrgrmAreaOfOperReqdInd
 , PrgrmDocReqdInd
 , PrgrmMaxTrmMoQty
 FROM PrgrmTbl 
 WHERE PrgrmId = p_PrgrmId;
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE; 
 ROLLBACK TO SAVEPOINT PhyAddrSel;
 
 END PRGRMSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


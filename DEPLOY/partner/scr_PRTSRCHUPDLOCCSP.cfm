<!--- Saved 05/05/2015 13:19:15. --->
PROCEDURE PRTSRCHUPDLOCCSP(
 p_PrtId IN NUMBER DEFAULT NULL)
 AS
 v_LocId NUMBER(7,0);
 v_PrtLocFIRSNmb CHAR(7);
 v_PrtLocNm VARCHAR2(200);
 v_ValidLocTyp VARCHAR2(5);
 v_PrtLocCreatUserId VARCHAR2(32);
 v_PrtLocCreatDt DATE;
 v_INST_ID NUMBER(8,0);
 v_ErrorNmb NUMBER(5,0);
 v_ReplicationErrorMessageTxt VARCHAR2(255);
 BEGIN
 
 v_INST_ID := 0;
 
 FOR rec IN ( SELECT l.LocId, l.PrtLocFIRSNmb, l.PrtLocNm, l.ValidLocTyp, l.PrtLocCreatUserId,
 l.PrtLocCreatDt FROM PrtLocTbl l 
 WHERE l.PrtId = p_PrtId 
 and l.ValidLocTyp = 'HO')
 LOOP
 v_LocId := rec.LocId ; 
 v_PrtLocFIRSNmb := rec.PrtLocFIRSNmb ; 
 v_PrtLocNm := rec.PrtLocNm ; 
 v_ValidLocTyp := rec.ValidLocTyp ; 
 v_PrtLocCreatUserId := rec.PrtLocCreatUserId ; 
 v_PrtLocCreatDt := rec.PrtLocCreatDt ; 
 END LOOP;
 UPDATE PARTNER.PrtSrchTbl
 SET LocId = v_LocId, 
 PrtLocFIRSNmb = v_PrtLocFIRSNmb, 
 PrtLocNm = v_PrtLocNm, 
 ValidLocTyp = v_ValidLocTyp, 
 LastUpdtUserId = v_PrtLocCreatUserId, 
 LastUpdtDt = NVL(v_PrtLocCreatDt, SYSDATE)
 WHERE PrtId = p_PrtId;
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,'PrtSrchUpdLocCSP',v_ErrorNmb,v_ReplicationErrorMessageTxt
 ,null);
 v_ErrorNmb := 50201;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl.' 
 || ' PrtId=' || RPAD(p_PrtId, 7, ' ');
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,
 'PrtSrchUpdLocCSP',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,null);
 
 
 END;
 END IF;
 UPDATE PARTNER.PrtSrchTbl
 SET LocQty = ( 
 SELECT COUNT(l.LocId) 
 FROM PrtLocTbl l 
 WHERE l.PrtId = p_PrtId 
 AND l.PrtLocCurStatCd = 'O' )
 WHERE PrtId = p_PrtId;
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,
 'PrtSrchUpdLocCSP',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,null);
 v_ErrorNmb := 50202;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl.' 
 || ' PrtId=' || RPAD(p_PrtId, 7, ' ');
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,
 'PrtSrchUpdLocCSP',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,null);
 
 END;
 END IF;
 
 
 END PRTSRCHUPDLOCCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:41. --->
PROCEDURE PNDNGPRTAGRMTDELCSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := NULL,
 p_PrtAgrmtSeqNmb NUMBER := NULL)
 AS
 OldDataRec PrtAgrmtTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PndngPrtAgrmtDel;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 DELETE FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 --get old snapshot
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 DELETE FROM PrtAgrmtTbl 
 WHERE PrtId = p_PrtId 
 and PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAgrmtDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal :=SQL%ROWCOUNT;
 
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO SAVEPOINT PndngPrtAgrmtDe;
 
 END;
 
 END IF;
 
 END PNDNGPRTAGRMTDELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:30. --->
PROCEDURE PRTCNTCTSBAROLETYPSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTCNTCTSBAROLETYPSEL;
 IF p_Identifier= 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtCntctSBARoleTypCd,
 PrtCntctSBARoleTypTxt
 FROM PrtCntctSBARoleTypTbl 
 WHERE PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier= 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 
 FROM PrtCntctSBARoleTypTbl 
 WHERE PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier= 4 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtCntctSBARoleTypCd
 PrtCntctSBARoleTypTxt 
 FROM PrtCntctSBARoleTypTbl;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTCNTCTSBAROLETYPSEL;
 RAISE;
 
 END PRTCNTCTSBAROLETYPSELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


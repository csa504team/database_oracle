<!--- Saved 01/23/2018 15:31:58. --->
PROCEDURE LOCHISTRYSELTSP(
 p_Identifier NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER,
 p_LocHistryAffctPrtId NUMBER := 0,
 p_LocId NUMBER := 0,
 p_LocHistryEffDt DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_LocHistryPartyTyp CHAR := NULL,
 p_LocHistryInitPrtId NUMBER := 0)
 AS
 /* RY- 03/21/2016----Added a new identitifier 3
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd */
 BEGIN
 BEGIN
 SAVEPOINT LocHistrySel;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 OPEN p_SelCur FOR SELECT LocHistryAffctPrtId
 , LocId
 , LocHistryEffDt
 , ValidChngCd
 , LocHistryPartyTyp
 , LocHistryInitPrtId
 , LocHistryResultingPrtId
 , LocHistryChngDesc
 , LocHistryLocNm
 , LocHistryLocTyp
 , LocHistryCtyNm
 , LocHistryCntCd
 , LocHistryFIPSCntyCd
 , LocHistryPostCd
 , LocHistryStCd
 , LocHistryStr1Txt
 , LocHistryStr2Txt
 , LocHistryUSDpndyInd
 , LocHistryTFPHistryDt
 , AFFECTED_INST_ID
 , LOC_ID
 , EFFECTIVE_DATE
 , PARTY_TYPE_INDICATOR
 , CHANGE_CODE
 , INITIATOR_INST_ID
 , STATUS
 FROM LocHistryTbl 
 WHERE LocHistryAffctPrtId =p_LocHistryAffctPrtId 
 AND LocId = p_LocId 
 AND LocHistryEffDt = p_LocHistryEffDt 
 AND ValidChngCd = p_ValidChngCd 
 AND LocHistryPartyTyp = p_LocHistryPartyTyp 
 AND LocHistryInitPrtId = p_LocHistryInitPrtId;
 
 END;
 ELSIF p_Identifier = 2 
 THEN
 BEGIN
 OPEN p_SelCur FOR SELECT 1
 FROM LocHistryTbl WHERE LocHistryAffctPrtId = LocHistryAffctPrtId 
 AND LocId = LocId 
 AND LocHistryEffDt = LocHistryEffDt 
 AND ValidChngCd = ValidChngCd 
 AND LocHistryPartyTyp = LocHistryPartyTyp 
 AND LocHistryInitPrtId = LocHistryInitPrtId;
 
 p_RetVal :=SQL%ROWCOUNT;
 
 
 
 
 
 
 END;
 
 
 
 
 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO SAVEPOINT IMSEQNMBSELTSP;
 RAISE;
 
 
 END;
 END LOCHISTRYSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/25/2018 10:37:54. --->
PROCEDURE PrtAliasInsTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_LocId NUMBER := 0,
 p_PrtAliasCreatUserId VARCHAR2 := NULL)
 AS
 /* SB -- 10/15/2014 -- Added New Validation
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 v_PrtId NUMBER (11);
 v_SelCur SYS_REFCURSOR;
 v_ErrSeqNmb NUMBER;
 BEGIN
 SAVEPOINT PrtAliasIns;
 
 IF p_ValidPrtAliasTyp = 'TIN'
 THEN
 BEGIN
 PARTNER.VALIDATEPRTALIASCSP (p_PrtId,
 p_ValidPrtAliasTyp,
 p_PrtAliasNm,
 p_PRTALIASCREATUSERID,
 v_ErrSeqNmb,
 v_SelCur);
 
 IF v_ErrSeqNmb > 0
 THEN
 raise_application_error (-20001, 'Invalid TIN:' || p_PrtAliasNm);
 RETURN;
 END IF;
 END;
 END IF;
 
 IF p_Identifier = 0
 THEN
 /* Insert into PrtAliasTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtAliasTbl (PrtId,
 ValidPrtAliasTyp,
 PrtAliasNm,
 PrtAliasCreatUserId,
 LASTUPDTUSERID,
 LASTUPDTDT)
 VALUES (p_PrtId,
 p_ValidPrtAliasTyp,
 p_PrtAliasNm,
 p_PrtAliasCreatUserId,
 p_PrtAliasCreatUserId,
 SYSDATE);
 
 --call trigger
 IF SQLCODE != 0
 THEN
 BEGIN
 PrtAliasInsUpdTrigCSP (p_PrtId, p_ValidPrtAliasTyp);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 IF p_Identifier = 11
 THEN
 /* Insert into PrtAliasTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtAliasTbl (PrtId,
 ValidPrtAliasTyp,
 PrtAliasNm,
 PrtAliasCreatUserId,
 LASTUPDTUSERID,
 LASTUPDTDT)
 VALUES ( (SELECT PrtId
 FROM PARTNER.PrtLocTbl
 WHERE LocId = p_LocId),
 p_ValidPrtAliasTyp,
 p_PrtAliasNm,
 p_PrtAliasCreatUserId,
 p_PrtAliasCreatUserId,
 SYSDATE);
 
 --call trigger
 IF SQLCODE != 0
 THEN
 BEGIN
 BEGIN
 SELECT PrtId
 INTO v_PrtId
 FROM PrtLocTbl
 WHERE LocId = p_LocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtId := NULL;
 END;
 
 PrtAliasInsUpdTrigCSP (v_PrtId, p_ValidPrtAliasTyp);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtAliasIns;
 RAISE;
 END PrtAliasInsTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


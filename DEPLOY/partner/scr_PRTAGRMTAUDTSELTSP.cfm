<!--- Saved 05/05/2015 13:16:49. --->
PROCEDURE PRTAGRMTAUDTSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtAudtNmb IN NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 SAVEPOINT PrtAgrmtAudtSel;
 
 IF p_Identifier = 0 THEN
 BEGIN
 OPEN p_SelCur FOR SELECT PrtAgrmtAudtNmb
 , PrtId
 , PrtAgrmtSeqNmb
 , PrgrmId
 , PrtAgrmtAudtDocLocOfcCd
 , PrtAgrmtAudtAreaOfOperTxt
 , PrtAgrmtAudtDocLocTxt
 , PrtAgrmtAudtEffDt
 , PrtAgrmtAudtExprDt
 , PrtAgrmtAudtTermDt
 , PrtAgrmtTermRsnCd
 , PrtAgrmtAudtWvrInd
 FROM PrtAgrmtAudtTbl 
 WHERE PrtAgrmtAudtNmb = p_PrtAgrmtAudtNmb;
 END;
 ELSIF p_Identifier = 2 THEN
 BEGIN
 OPEN p_SelCur FOR SELECT 1 FROM PrtAgrmtAudtTbl WHERE PrtAgrmtAudtNmb = PrtAgrmtAudtNmb;
 
 
 END;
 p_RetVal :=SQL%ROWCOUNT;
 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtAudtSel;
 RAISE;
 END PRTAGRMTAUDTSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:15:56. --->
PROCEDURE CHECKUNDRWRITNGBYCSP
 ( p_Identifier number :=null,
 p_loanappnmb NUMBER := NULL ,
 p_PrcsMthdCd CHAR :=NULL,
 p_PrtAgrmtAsOnDt DATE:=NULL,
 p_UndrwritngBy char:=null,
 p_LoanAppRecvDt DATE:=null,
 p_RetVal OUT NUMBER 
 )
 AS
 /*Step 1: For partner id and processing method, for all the mapped active agreements, if at least one agreement level (PIMS) override exists, set the value and exit. In case of multi-rows, UW by LNDR has precedence.
 Step 2: For partner id and processing method, for all the mapped active agreements, use UW By value for given PIMS program Id (SBAREF). In case of multi-rows, preffered order number is considered
 */
 v_Prtid number(7,0);
 v_count1 number:=NULL;
 v_UndrwritngBy CHAR(4):=NULL;
 v_AGRMTPREFORDNMB number(3,0):= NULL;
 BEGIN
 SAVEPOINT CHECKUNDRWRITNGBYCSP;
 if p_identifier=0
 then 
 BEGIN
 
 SELECT Prtid
 INTO v_Prtid 
 FROM LOANAPP.LOANAPPPRTTBL 
 WHERE LoanAppNmb = p_LoanAppNmb;
 EXCEPTION
 WHEN no_data_found 
 THEN
 v_Prtid := NULL;
 
 END;
 
 ELSif p_identifier=1
 then 
 BEGIN
 
 SELECT Prtid
 INTO v_Prtid 
 FROM LOAN.LOANGNTYTBL 
 WHERE LoanAppNmb = p_LoanAppNmb;
 EXCEPTION
 WHEN no_data_found 
 THEN
 v_Prtid := NULL;
 
 END;
 end if;
 
 select count(*) into v_count1
 from SBAREF.PRCSMTHDTBL p
 where p.prcsmthdcd=p_prcsmthdcd
 and trunc(p.PRCSMTHDSTRTDT)<= trunc(nvl(p_LoanAppRecvDt,sysdate))
 and (p.PRCSMTHDENDDT is null
 or trunc(p.PRCSMTHDENDDT) >= trunc(nvl(p_LoanAppRecvDt,sysdate)))
 and p.LOANTYPIND = 'D';-- for direct loans,need not check partner.PrtAgrmtTbl for UndrwritngBy as it is always SBA.
 
 if v_count1 > 0
 then 
 begin
 v_UndrwritngBy := 'SBA';
 end;
 
 else 
 
 begin 
 begin
 select case when MAX(DECODE(x.LendrDirectAuthInd,'Y',2,'N',1,0))=2
 then 'LNDR' --step 1
 when MAX(DECODE (x.LendrDirectAuthInd,'Y',2,'N',1,0))=1
 then 'SBA' -- step 1
 when MAX(DECODE (x.LendrDirectAuthInd,'Y',2,'N',1,0))=0 
 then NULL -- step 2
 end,
 min(x.AGRMTPREFORDNMB) into v_UndrwritngBy,v_AGRMTPREFORDNMB
 FROM (select distinct LendrDirectAuthInd,UndrwritngBy,AGRMTPREFORDNMB
 from 
 partner.PrtAgrmtTbl a,
 partner.PrgrmTbl p,
 sbaref.IMPrgrmAuthOthAgrmtTbl u 
 left outer join sbaref.PrgrmValidTbl b on u.PrcsMthdCd = b.PrcsMthdCd
 where a.PrtId = v_Prtid
 AND trunc(a.PrtAgrmtEffDt) <= trunc(nvl(p_PrtAgrmtAsOnDt,sysdate))
 and ( a.PrtAgrmtExprDt is null
 or trunc(a.PrtAgrmtExprDt) >= trunc(nvl(p_PrtAgrmtAsOnDt,sysdate)))
 and ( a.PrtAgrmtTermDt is null
 or trunc(a.PrtAgrmtTermDt) > trunc(nvl(p_PrtAgrmtAsOnDt,sysdate)))
 and a.PrgrmId = p.PrgrmId
 and p.ValidPrgrmTyp = u.PIMSPrgrmId
 and trunc(u.IMPrgrmAuthOthStrtDt) <= trunc(nvl(p_LoanAppRecvDt,sysdate))
 and ( u.IMPrgrmAuthOthEndDt is null
 or trunc(u.IMPrgrmAuthOthEndDt) >= trunc(nvl(p_LoanAppRecvDt,sysdate)))
 and ( b.PrgrmValidStrtDt is null 
 or trunc(b.PrgrmValidStrtDt) <= trunc(nvl(p_LoanAppRecvDt,sysdate)))
 and ( b.PrgrmValidEndDt is null
 or trunc(b.PrgrmValidEndDt) >= trunc(nvl(p_LoanAppRecvDt,sysdate)))
 and u.PrcsMthdCd = p_prcsmthdcd)x; 
 EXCEPTION
 WHEN no_data_found 
 THEN
 
 v_UndrwritngBy := NULL;
 v_AGRMTPREFORDNMB := NULL;
 end; 
 
 if v_UndrwritngBy IS NULL and v_AGRMTPREFORDNMB is not null --step 2
 then 
 begin
 select UndrwritngBy into v_UndrwritngBy
 FROM sbaref.IMPrgrmAuthOthAgrmtTbl s
 where prcsmthdcd = p_prcsmthdcd
 and AGRMTPREFORDNMB = v_AGRMTPREFORDNMB ;
 EXCEPTION
 WHEN no_data_found 
 THEN 
 v_UndrwritngBy := NULL;
 end;
 end if;
 end; 
 end if; 
 
 
 if trim(v_UndrwritngBy)=trim(p_UndrwritngBy) --checking with input SBA or LNDR
 then 
 
 p_RetVal:=1;
 
 else
 
 p_RetVal:=0;
 
 end if; 
 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO CHECKUNDRWRITNGBYCSP;
 RAISE;
 END; 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


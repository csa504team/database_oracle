<!--- Saved 05/05/2015 13:16:21. --->
PROCEDURE LOCEXTRCTSTATUPDCSP
 (
 p_PrtId IN NUMBER := NULL, --PrtId of locations to be added.
 p_CreatUserID VARCHAR2 := NULL
 )
 AS
 -- Adds a queue entry (location) into the
 -- LocExtrctStatTbl table for each of the
 -- locations of the given PrtId.
 -- Return status of 0 indicates successful completion.
 -- Return status of -100 indicates an error condition.
 -- Revision History -----------------------------------------
 -- 04/18/2001 Rajeswari DSP Primary Typ of Microlender has been
 -- changed from 'Lendr' to 'NBLDR'
 -- 07/12/2000 C.Woodard Removed RAISERROR and ROLLBACK.
 -- 07/06/2000 C.Woodard Added code to exclude Microlenders.
 -- 04/19/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 v_CurrentTime DATE;
 v_ErrorMessageTxt VARCHAR2(255);
 v_InsertErrorNmb NUMBER(5);
 v_UpdateErrorNmb NUMBER(5);
 v_LocationId NUMBER(7);
 v_PrimaryTyp VARCHAR2(5);
 v_SubcategoryTyp VARCHAR2(5);
 v_temp NUMBER(1, 0) := 0;
 BEGIN
 
 v_CurrentTime := SYSDATE ;
 v_InsertErrorNmb := 0 ;
 v_UpdateErrorNmb := 0 ;
 
 IF p_PrtId IS NULL 
 THEN
 BEGIN
 v_InsertErrorNmb := 99999 ;
 END;
 END IF;
 --Determine location eligibility for FIRS processing.
 --Skip processing of Microlender locations.
 SELECT p.ValidPrtPrimCatTyp, p.ValidPrtSubCatTyp
 INTO v_PrimaryTyp, v_SubcategoryTyp
 FROM PrtTbl p
 WHERE p.PrtId = p_PrtId;
 --r IF (@PrimaryTyp = "Lendr"
 
 BEGIN
 SELECT 1 into v_temp
 FROM ValidPrtSubCatTbl 
 WHERE ValidPrtSubCatFIRSInd = 'N'
 AND ValidPrtSubCatTyp = v_SubcategoryTyp;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 NULL;
 END;
 
 
 --Microlender exclusion.
 -- Update or insert queue entry.
 IF v_temp=1
 THEN
 
 BEGIN
 UPDATE LocExtrctStatTbl S
 SET LocExtrctStatLastQueueTime = v_CurrentTime,
 LocExtrctStatExtrctInd = 'N',--Reset status. 
 LastUpdtUserId = p_CreatUserID,
 LastUpdtDt = SYSDATE
 WHERE S.LocId IN ( SELECT pl.LocId
 FROM PrtLocTbl pl
 WHERE pl.PrtId = p_PrtId );
 
 END;
 ELSE
 BEGIN
 INSERT INTO LocExtrctStatTbl
 ( LocId, LocExtrctStatFirstQueueTime, LocExtrctStatLastQueueTime,LocExtrctStatCreatUserId,
 LocExtrctStatCreatDt, LastUpdtUserId, LastUpdtDt )
 ( SELECT pl.LocId,
 v_CurrentTime,
 v_CurrentTime, --ExtractedInd defaults to "N"
 p_CreatUserID,
 SYSDATE,
 p_CreatUserID,
 SYSDATE
 FROM PrtLocTbl pl
 WHERE pl.PrtId = p_PrtId
 AND pl.PrtLocFIRSNmb IS NOT NULL
 AND pl.LocId NOT IN ( SELECT S.LocId
 FROM LocExtrctStatTbl S ) );
 
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:42. --->
PROCEDURE PRTHISTARRECRDCSP(
 p_ReportBegDateTime IN DATE DEFAULT NULL,
 p_ReportEndDateTime IN DATE DEFAULT NULL,
 p_ValidChngCd IN VARCHAR2 DEFAULT NULL,
 p_PrtId IN NUMBER DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtHistARRecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd);
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(136)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' );
 END;
 ELSE
 BEGIN
 /*[SPCONV-ERR(150)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 r.PrtHistryLglNm ResultingTitle,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = r.PrtId 
 AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' ) 
 AND a.PrtId = p_PrtId;
 END;
 END IF;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO PrtHistARRecrd;
 RAISE;
 END;
 END PRTHISTARRECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


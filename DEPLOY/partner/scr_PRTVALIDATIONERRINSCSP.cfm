<!--- Saved 05/05/2015 13:19:18. --->
PROCEDURE PRTVALIDATIONERRINSCSP(
 p_PrtId IN NUMBER :=0,
 p_ErrCd IN NUMBER :=0,
 p_CreatUserId IN VARCHAR2 := NULL,
 p_Parm1 IN VARCHAR2 := NULL,
 p_Parm2 IN VARCHAR2 := NULL,
 p_Parm3 IN VARCHAR2 := NULL,
 p_Parm4 IN VARCHAR2 := NULL,
 p_Parm5 IN VARCHAR2 := NULL
 )
 AS
 v_ErrTxt VARCHAR2(255);
 v_ErrSeqNmb NUMBER(10);
 v_RetVal NUMBER(10);
 v_temp number := 0;
 BEGIN
 SELECT ErrTxt
 INTO v_ErrTxt
 FROM partner.PrtErrCdTbl
 WHERE ErrCd = p_ErrCd;
 
 IF INSTR(v_ErrTxt,'^1') > 0 THEN
 v_ErrTxt := stuff(v_ErrTxt, INSTR(v_ErrTxt, '^1'), 2, p_Parm1); 
 END IF;
 IF INSTR(v_ErrTxt,'^2') > 0 THEN
 v_ErrTxt := stuff(v_ErrTxt, INSTR(v_ErrTxt, '^2'), 2, p_Parm2);
 END IF;
 IF INSTR(v_ErrTxt,'^3') > 0 THEN
 v_ErrTxt := stuff(v_ErrTxt, INSTR(v_ErrTxt, '^3'), 2, p_Parm3);
 END IF;
 IF INSTR(v_ErrTxt,'^4') > 0 THEN
 v_ErrTxt := stuff(v_ErrTxt, INSTR(v_ErrTxt, '^4'), 2, p_Parm4);
 END IF;
 IF INSTR(v_ErrTxt,'^5') > 0 THEN
 v_ErrTxt := stuff(v_ErrTxt, INSTR(v_ErrTxt, '^5'), 2, p_Parm5);
 END IF;
 
 insert into partner.prtvalidationerrtbl(PRTID, PRTERRSEQNMB, PRTERRCD, PRTERRTXT, CREATUSERID, CREATDT)
 select p_PrtId, nvl((select max(PrtErrSeqNmb) from partner.PrtValidationErrTbl where prtid = p_prtid),0) + 1, p_errcd, v_errtxt, p_creatUserId, sysdate
 from dual;
 
 END PRTVALIDATIONERRINSCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


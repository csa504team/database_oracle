<!--- Saved 05/20/2016 14:04:10. --->
PROCEDURE GETCNTCTSBAROLSELTSP(
 p_locId number :=null,
 p_PrtCntctNmb number :=null,
 p_queryType CHAR,
 p_SelCur OUT SYS_REFCURSOR )
 AS
 BEGIN 
 
 if p_queryType='all' THEN
 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT csr.PrtCntctSBARoleTypTxt, csr.PrtCntctSBARoleTypCd 
 FROM partner.PrtCntctSBARoleTypTbl csr ;
 END;
 Else if p_queryType='selected' then 
 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT csr.PrtCntctSBARoleTypTxt,
 case when csr.PrtCntctSBARoleTypCd is NULL then 'N' 
 else 'Y' 
 end
 
 FROM partner.PrtCntctSBARoleTypTbl csr, partner.PrtLocCntctSBARoleTbl cs 
 WHERE csr.PrtCntctSBARoleTypCd = cs.PrtCntctSBARoleTypCd(+) 
 AND cs.LocId=p_locId 
 AND cs.PrtCntctNmb=NVL(p_PrtCntctNmb,cs.PrtCntctNmb); 
 END;
 Else 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT csr.PrtCntctSBARoleTypTxt, csr.PrtCntctSBARoleTypCd 
 FROM partner.PrtLocCntctTbl c, partner.PrtLocCntctSBARoleTbl cs ,partner.PrtCntctSBARoleTypTbl csr 
 WHERE c.LocId = cs.LocId 
 AND c.PrtCntctNmb = cs.PrtCntctNmb 
 AND cs.PrtCntctSBARoleTypCd = csr.PrtCntctSBARoleTypCd(+)
 AND c.LocId=p_locId 
 AND c.PrtCntctNmb=NVL(p_PrtCntctNmb,c.PrtCntctNmb);
 END;
 
 END IF;
 
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


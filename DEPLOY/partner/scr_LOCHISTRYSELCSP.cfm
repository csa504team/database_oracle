<!--- Saved 04/12/2016 14:52:17. --->
PROCEDURE LOCHISTRYSELCSP(
 p_RetVal OUT NUMBER,
 p_ValidChngCd VARCHAR2 := NULL,
 p_LocHistryStrtDt DATE := NULL,
 p_LocHistryEndDt DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 BEGIN
 SAVEPOINT LocHistrySelCSP;
 
 
 BEGIN EXECUTE IMMEDIATE 'truncate table partner.TEMPLocHistTbl'; END;
 INSERT INTO partner.TEMPLocHistTbl
 SELECT DISTINCT LocHistryAffctPrtId,LocId,LocHistryEffDt,
 ValidChngCd,LocHistryInitPrtId
 FROM partner.LocHistryTbl 
 WHERE ValidChngCd = p_ValidChngCd
 AND trunc(LocHistryEffDt) >= p_LocHistryStrtDt
 AND trunc(LocHistryEffDt) <= p_LocHistryEndDt;
 
 OPEN p_SelCur FOR 
 SELECT t.LocHistryAffctPrtId,i.LocId As IntitatorLocId,
 i.LocHistryLocNm as InitiatorTitle, i.LocHistryCtyNm As InitiatorCity,
 i.LocHistryStr1Txt As InitiatorStreet1,i.LocHistryStr2Txt As InitiatorStreet2,
 i.LocHistryStCd As InitiatorStCd, i.LocHistryPostCd As InitiatorPostalCd,
 i.LocHistryFIPSCntyCd As InitiatorFIPSCntyCd,i.LocHistryLocTyp As InitiatorLocType,
 i.LocHistryInitPrtId As InitiatorInitPrtId,i.LocHistryResultingPrtId AS InitiatorResultingPrtId,
 v.ValidChngCdDesc,a.LocHistryLocNm as AffectedTitle,
 a.LocHistryCtyNm As AffectedCity,a.LocHistryStCd As AffectedStCd,
 a.LocHistryStr1Txt As AffectedStreet1,a.LocHistryStr2Txt As AffectedStreet2,
 a.LocHistryPostCd As AffectedPostalCd,a.LocHistryFIPSCntyCd As AffectedFIPSCntyCd,
 a.LocHistryLocTyp As AffectedLocType,a.LocHistryInitPrtId As AffectedInitPrtId,
 a.LocHistryResultingPrtId AS AffectedResultingPrtId,a.LocId As AffectedLocId,
 a.LocHistryChngDesc,r.LocHistryLocNm as ResultingTitle,
 r.LocHistryCtyNm As ResultingCity,r.LocHistryStCd As ResultingStCd,
 r.LocHistryStr1Txt As ResultingStreet1,r.LocHistryStr2Txt As ResultingStreet2,
 r.LocHistryPostCd As ResultingPostalCd,r.LocHistryFIPSCntyCd As ResultingFIPSCntyCd,
 r.LocHistryLocTyp As ResultingLocType, r.LocHistryInitPrtId As ResultingInitPrtId,
 r.LocHistryResultingPrtId AS ResultingResultingPrtId,r.LocId As ResultingLocId, 
 To_char(t.LocHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 t.LocId,t.ValidChngCd,v.ValidChngCdDispOrdNmb, a.LocHistryCreatDt as DateAddedToPIMS,
 case when i.LocHistryPartyTyp = 'I' then 'I' when a.LocHistryPartyTyp = 'A' then 'A' when r.LocHistryPartyTyp = 'R' then 'R' end as PartyTyp,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = i.LocHistryLocTyp) as InitiatorLocTypeDesc,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = a.LocHistryLocTyp) as AffectedLocTypeDesc,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = r.LocHistryLocTyp) as ResultingLocTypeDesc
 FROM partner.TEMPLocHistTbl t left outer join partner.LocHistryTbl a on t.LocHistryAffctPrtId = a.LocHistryAffctPrtId
 AND t.LocId = a.LocId AND t.LocHistryEffDt = a.LocHistryEffDt
 AND a.LocHistryPartyTyp = 'A' AND t.ValidChngCd = a.ValidChngCd
 left outer join partner.LocHistryTbl i on t.LocHistryAffctPrtId = i.LocHistryAffctPrtId
 AND t.LocId = i.LocId AND t.LocHistryEffDt = i.LocHistryEffDt
 AND i.LocHistryPartyTyp = 'I' AND t.ValidChngCd = i.ValidChngCd
 left outer join partner.LocHistryTbl r on t.LocHistryAffctPrtId = r.LocHistryAffctPrtId
 AND t.LocId = r.LocId AND t.LocHistryEffDt=r.LocHistryEffDt
 AND r.LocHistryPartyTyp = 'R' AND t.ValidChngCd = r.ValidChngCd
 left outer join partner.ValidChngCdTbl v on t.ValidChngCd = v.ValidChngCd
 ORDER BY t.LocHistryEffDt DESC,t.ValidChngCd, CASE WHEN a.LocHistryLocTyp = 'HO' then 0 else 1 end;
 EXCEPTION 
 WHEN OTHERS THEN
 ROLLBACK TO SAVEPOINT LocHistrySelCSP;
 RAISE;
 END;
 END LocHistrySelCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


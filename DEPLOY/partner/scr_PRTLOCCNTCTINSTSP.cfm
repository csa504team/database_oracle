<!--- Saved 01/23/2018 15:32:01. --->
PROCEDURE PRTLOCCNTCTINSTSP (
 p_Identifier IN NUMBER := 0,
 p_RetVal IN OUT NUMBER,
 p_LocId IN NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctFirstNm VARCHAR2 := NULL,
 p_PrtCntctJobTitlNm VARCHAR2 := NULL,
 p_PrtCntctLastNm VARCHAR2 := NULL,
 p_PrtCntctInitialNm VARCHAR2 := NULL,
 p_PrtCntctPrefixNm VARCHAR2 := NULL,
 p_PrtCntctSfxNm VARCHAR2 := NULL,
 p_PrtLocCntctMailStopNm VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := 0,
 p_INST_ID NUMBER := 0,
 p_LOC_ID NUMBER := 0,
 p_PrtCntctCreatUserId VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtCntctDataSourcId 
 */
 error NUMBER (10, 0);
 NewDataRec PrtLocCntctTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtLocCntctIns;
 
 IF p_Identifier = 0
 THEN
 /* Insert into PrtLocCntctTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocCntctTbl (LocId,
 PrtCntctNmb,
 PrtCntctFirstNm,
 PrtCntctJobTitlNm,
 PrtCntctLastNm,
 PrtCntctInitialNm,
 PrtCntctPrefixNm,
 PrtCntctSfxNm,
 PrtLocCntctMailStopNm,
 PERSON_ID,
 INST_ID,
 LOC_ID,
 PrtCntctCreatUserId,
 PRTCNTCTCREATDT,
 LASTUPDTUSERID,
 LASTUPDTDT)
 VALUES (p_LocId,
 p_PrtCntctNmb,
 p_PrtCntctFirstNm,
 p_PrtCntctJobTitlNm,
 p_PrtCntctLastNm,
 p_PrtCntctInitialNm,
 p_PrtCntctPrefixNm,
 p_PrtCntctSfxNm,
 p_PrtLocCntctMailStopNm,
 p_PERSON_ID,
 p_INST_ID,
 p_LOC_ID,
 p_PrtCntctCreatUserId,
 SYSDATE,
 p_PrtCntctCreatUserId,
 SYSDATE);
 
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtLocCntctTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11
 THEN
 BEGIN
 INSERT INTO PARTNER.PrtLocCntctTbl (LocId,
 PrtCntctNmb,
 PrtCntctFirstNm,
 PrtCntctJobTitlNm,
 PrtCntctLastNm,
 PrtCntctInitialNm,
 PrtCntctPrefixNm,
 PrtCntctSfxNm,
 PrtLocCntctMailStopNm,
 PERSON_ID,
 INST_ID,
 LOC_ID,
 PrtCntctCreatUserId,
 PRTCNTCTCREATDT,
 LASTUPDTUSERID,
 LASTUPDTDT)
 SELECT p_LocId,
 NVL (MAX (PrtCntctNmb), 0) + 1,
 p_PrtCntctFirstNm,
 p_PrtCntctJobTitlNm,
 p_PrtCntctLastNm,
 p_PrtCntctInitialNm,
 p_PrtCntctPrefixNm,
 p_PrtCntctSfxNm,
 p_PrtLocCntctMailStopNm,
 p_PERSON_ID,
 p_INST_ID,
 p_LOC_ID,
 p_PrtCntctCreatUserId,
 SYSDATE,
 p_PrtCntctCreatUserId,
 SYSDATE
 FROM PrtLocCntctTbl
 WHERE LocId = p_LocId;
 
 OPEN p_SelCur FOR
 SELECT MAX (PrtCntctNmb) AS PrtCntctNmb
 FROM PrtLocCntctTbl
 WHERE LocId = p_LocId;
 
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtLocCntctTbl c
 WHERE LocId = p_LocId
 AND PrtCntctNmb = (SELECT MAX (PrtCntctNmb)
 FROM PrtLocCntctTbl z
 WHERE c.LocId = z.LocId);
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctInsUpdTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtLocCntctIns;
 RAISE;
 END PRTLOCCNTCTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:39. --->
PROCEDURE VALIDCHNGCDSELTSP
 (
 p_Identifier NUMBER:=0,
 p_ValidChngCd VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidChngCdSel;
 IF p_Identifier= 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidChngCd, 
 ValidChngCdDesc, 
 ValidChngCdTyp, 
 ValidChngCdDispOrdNmb
 FROM ValidChngCdTbl 
 WHERE ValidChngCd = p_ValidChngCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier= 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM ValidChngCdTbl 
 WHERE ValidChngCd = p_ValidChngCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 END IF ; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDCHNGCDSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:15:53. --->
PROCEDURE BNDAGNTLICENSNGDELTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_RetVal OUT NUMBER ,
 p_IMUserNm IN VARCHAR2 := NULL 
 )
 AS
 
 
 v_LocId NUMBER(7,0);
 BEGIN
 
 IF p_Identifier = 0 
 THEN
 
 
 /* Insert intodbo.BndAgntLicensngTbl Table */
 BEGIN
 SELECT LocId INTO v_LocId
 FROM security.IMUserTbl 
 WHERE IMUserNm = p_IMUserNm;
 
 DELETE BndAgntLicensngTbl
 WHERE LocId = v_LocId;
 
 p_RetVal := SQL%ROWCOUNT ;
 END;
 
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


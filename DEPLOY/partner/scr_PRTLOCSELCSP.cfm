<!--- Saved 05/05/2015 13:18:40. --->
PROCEDURE PRTLOCSELCSP(
 p_userName VARCHAR2 := NULL,
 p_PrtLocFIRSNmb VARCHAR2 := NULL,
 p_Status CHAR := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCSELCSPl;
 IF p_Status is NULL 
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.PrtId,
 l.PrtLocNm,
 NULL,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 l.PrtLocCurStatCd,
 l.PrtLocOpndDt,
 l.PrtLocInactDt,
 l.PrtLocFIRSRecrdTyp,
 l.PhyAddrStr2Txt
 FROM LocSrchTbl l, PrtSrchTbl p 
 WHERE l.PrtLocFIRSNmb = p_PrtLocFIRSNmb 
 AND l.PrtId = p.PrtId 
 AND l.PrtLocCurStatCd = 'O' 
 AND p.PrtCurStatCd = 'O';
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.PrtId,
 l.PrtLocNm,
 NULL,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 l.PrtLocCurStatCd,
 l.PrtLocOpndDt,
 l.PrtLocInactDt,
 l.PrtLocFIRSRecrdTyp,
 l.PhyAddrStr2Txt
 FROM LocSrchTbl l 
 WHERE l.PrtLocFIRSNmb = p_PrtLocFIRSNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCSELCSP;
 RAISE;
 END PRTLOCSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


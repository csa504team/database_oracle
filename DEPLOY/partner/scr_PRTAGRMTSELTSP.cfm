<!--- Saved 03/11/2016 10:39:25. --->
PROCEDURE PRTAGRMTSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrcsMthdCd char := NULL,
 p_PrtAgrmtAsOnDt date := null,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 /*
 * RGG 07/26/2012: Added the new column LendrDirectAuthInd to Identifier 13 for CAI changes.
 RGG 07/30/2012: Added the new column LendrDirectAuthInd to Identifier 12,14 for CAI changes.
 sp 12/5/2012 :modified id=13 for all active agreements with appropriate UndrwritngBy.
 SP 1/3/2012 : Modified id=13 to remove logic for undrwritingby, and added id=15 for undrwritingby logic
 sp 02/20/2012 : Modified id=13 to remove preferred order logic and added to id=15 for underwriting
 sp 02/28/2012 : Modified id=15 to make it dynamic instead of hard coding for direct loans
 SB -- 07/07/2014 Added Identifier 38 for Extract XML 
 PSP --01/26/2015 : Modified id = 13 to remove check for area of operations as Lender wants to see all of the processing methods that they have agreement in PIMS regardless of the area of operations because they are not restricted by area of operations.
 */
 v_today date :=NULL;
 v_UndrwritngBy char(4) :=NULL;
 v_AGRMTPREFORDNMB number(3,0):= NULL;
 v_count1 number(3,0):= NULL;
 BEGIN
 SAVEPOINT PrtAgrmtSel;
 
 
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 
 
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtAgrmtSeqNmb,
 PrgrmId,
 PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,
 PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,
 PrtAgrmtExprDt,
 PrtAgrmtTermDt,
 PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,
 PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty
 FROM PrtAgrmtTbl 
 WHERE PrtId =p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb; 
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtAgrmtTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Agreement.Fetch() based on PrtId */
 BEGIN
 v_today := trunc(sysdate);
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 PrtAgrmtSeqNmb,
 a.PrgrmId,
 CASE 
 WHEN PrtAgrmtEffDt<=SYSDATE AND (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt is NULL) and 
 (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt is NULL) THEN 'A' 
 WHEN PrtAgrmtExprDt< v_today AND PrtAgrmtTermDt IS null THEN 'I' 
 WHEN PrtAgrmtTermDt<=SYSDATE THEN 'T' 
 WHEN PrtAgrmtEffDt>SYSDATE THEN 'P' 
 END PrtAgrmtCurStatCd,
 PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,
 PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,
 PrtAgrmtExprDt,
 PrtAgrmtTermDt,
 PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,
 /* CASE 
 WHEN prt.ValidPrtPrimCatTyp='NBLDR' THEN 'Y' 
 ELSE p.PrgrmAreaOfOperReqdInd 
 END*/p.PrgrmAreaOfOperReqdInd PrgrmAreaOfOperReqdInd,
 CASE 
 WHEN (p.PrgrmAreaOfOperReqdInd='Y' /*OR prt.ValidPrtPrimCatTyp='NBLDR'*/) 
 THEN (SELECT NVL(COUNT(o.PrtAreaOfOperSeqNmb), 0) FROM PrtAreaOfOperTbl o 
 WHERE o.PrtId = a.PrtId 
 and o.PrtAgrmtSeqNmb = a.PrtAgrmtSeqNmb 
 and (o.PrtAreaOfOperCntyEndDt > SYSDATE 
 or o.PrtAreaOfOperCntyEndDt IS NULL)
 ) 
 ELSE 0 
 END AreasOfOperNmb,
 p.PrgrmDesc 
 FROM PrtAgrmtTbl a, PrgrmTbl p, PrtTbl prt 
 WHERE a.PrtId = p_PrtId 
 AND a.PrtId = prt.PrtId 
 AND a.PrgrmId = p.PrgrmId 
 ORDER BY a.PrgrmId , a.PrtAgrmtSeqNmb DESC;
 p_RetVal := SQL%ROWCOUNT;
 
 END; 
 ELSIF p_Identifier = 12 THEN
 /* Agreement.Fetch() based on PrtId and PrgrmId */
 BEGIN
 v_today := trunc(sysdate);
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 PrtAgrmtSeqNmb,
 a.PrgrmId,
 CASE 
 WHEN PrtAgrmtEffDt<=SYSDATE AND (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt is NULL) and 
 (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt is NULL) THEN 'A' 
 WHEN PrtAgrmtExprDt < v_today AND PrtAgrmtTermDt IS null THEN 'I' 
 WHEN PrtAgrmtTermDt <= SYSDATE THEN 'T' 
 WHEN PrtAgrmtEffDt > SYSDATE THEN 'P' 
 END PrtAgrmtCurStatCd,
 PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,
 PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,
 PrtAgrmtExprDt,
 PrtAgrmtTermDt,
 PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,
 PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty,
 p.PrgrmDesc,
 a.LendrDirectAuthInd -- RGG 07/30/2012 
 FROM PrtAgrmtTbl a, PrgrmTbl p 
 WHERE a.PrtId = p_PrtId 
 AND a.PrgrmId = p_PrgrmId 
 AND a.PrgrmId = p.PrgrmId
 ORDER BY PrtAgrmtCurStatCd DESC;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 13 THEN
 /* select all the processing method of active agreement */
 BEGIN
 v_today := trunc(sysdate);
 open p_SelCur for
 select distinct a.PrtId,
 u.PrcsMthdCd,
 a.PrtAgrmtLoanLimtInd,
 a.PrtAgrmtMaxMoLoanQty,
 a.PrtAgrmtMaxFyLoanQty,
 a.PrtAgrmtMaxWkLoanQty,
 a.PrtAgrmtEffDt,
 p.PrgrmDesc
 from partner.PrtAgrmtTbl a,
 partner.PrgrmTbl p,
 partner.PrtTbl prt,
 SBAREF.PRCSMTHDTBL m,
 sbaref.IMPrgrmAuthOthAgrmtTbl u 
 left outer join sbaref.PrgrmValidTbl b on (u.PrcsMthdCd = b.PrcsMthdCd)
 where (a.PrtId = p_PrtId)
 and (a.PrtId = prt.PrtId)
 and (a.PrtAgrmtEffDt <= nvl(p_PrtAgrmtAsOnDt,sysdate))
 and ( a.PrtAgrmtExprDt is null
 or a.PrtAgrmtExprDt >= nvl(p_PrtAgrmtAsOnDt,v_today))
 and ( a.PrtAgrmtTermDt is null
 or a.PrtAgrmtTermDt > nvl(p_PrtAgrmtAsOnDt,sysdate))
 and (a.PrgrmId = p.PrgrmId)
 and (p.ValidPrgrmTyp = u.PIMSPrgrmId)
 and (TRUNC(u.IMPrgrmAuthOthStrtDt) <= v_today)
 and ( u.IMPrgrmAuthOthEndDt is null
 or TRUNC(u.IMPrgrmAuthOthEndDt) >= v_today) 
 and ( b.PrgrmValidStrtDt is null -- outer join, could be null
 or b.PrgrmValidStrtDt <= sysdate)
 and ( b.PrgrmValidEndDt is null
 or b.PrgrmValidEndDt >= v_today)
 /*and (case
 when ( p.PrgrmAreaOfOperReqdInd = 'Y' )
 -- or prt.ValidPrtPrimCatTyp = 'NBLDR')
 then (select nvl(count(o.PrtAreaOfOperSeqNmb),0)
 from partner.PrtAreaOfOperTbl o
 where (o.PrtId = a.PrtId)
 and (o.PrtAgrmtSeqNmb = a.PrtAgrmtSeqNmb)
 and (o.PrtAreaOfOperCntyEffDt <= v_today)
 and ( o.PrtAreaOfOperCntyEndDt is null
 or o.PrtAreaOfOperCntyEndDt >= v_today)
 )
 else 1
 end >0
 )*/
 and NVL(u.AppvAuthind,'Y')='Y'
 and (m.PrcsMthdCd = u.PrcsMthdCd)
 and (m.PrcsMthdStrtDt <= sysdate)
 and (m.PrcsMthdEndDt is null or m.PrcsMthdEndDt >= sysdate) 
 order by u.PrcsMthdCd;
 p_RetVal := SQL%ROWCOUNT;
 
 END; 
 ELSIF p_Identifier = 14 THEN
 /* Select pending agreement plus the most current other agreement Inactive, Active, Terminated */
 BEGIN
 v_today := trunc(sysdate);
 OPEN p_SelCur FOR 
 SELECT a.PrtId,
 a.PrtAgrmtSeqNmb,
 a.PrgrmId,
 CASE WHEN PrtAgrmtEffDt <= SYSDATE AND (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt is NULL) 
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt is NULL) THEN 'A' 
 WHEN PrtAgrmtExprDt < v_today AND PrtAgrmtTermDt IS null THEN 'I' 
 WHEN PrtAgrmtTermDt<=SYSDATE THEN 'T' 
 WHEN PrtAgrmtEffDt>SYSDATE THEN 'P' 
 END PrtAgrmtCurStatCd,
 a.PrtAgrmtDocLocOfcCd,
 a.PrtAgrmtAreaOfOperTxt,
 a.PrtAgrmtDocLocTxt,
 a.PrtAgrmtEffDt,
 a.PrtAgrmtExprDt,
 a.PrtAgrmtTermDt,
 a.PrtAgrmtTermRsnCd,
 a.PrtAgrmtWvrInd,
 p.ValidPrgrmTyp,
 /*CASE WHEN prt.ValidPrtPrimCatTyp='NBLDR' THEN 'Y' 
 ELSE p.PrgrmAreaOfOperReqdInd 
 END*/p.PrgrmAreaOfOperReqdInd PrgrmAreaOfOperReqdInd,
 CASE WHEN (p.PrgrmAreaOfOperReqdInd='Y' /*OR prt.ValidPrtPrimCatTyp='NBLDR'*/) 
 THEN (SELECT NVL(COUNT(o.PrtAreaOfOperSeqNmb), 0) 
 FROM PrtAreaOfOperTbl o 
 WHERE o.PrtId = a.PrtId 
 and o.PrtAgrmtSeqNmb = a.PrtAgrmtSeqNmb 
 and (o.PrtAreaOfOperCntyEndDt > SYSDATE 
 or o.PrtAreaOfOperCntyEndDt IS NULL) 
 ) 
 ELSE 0 
 End AreasOfOperNmb,
 p.PrgrmParntPrgrmId, -- Added IAC 03/06/02
 PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty,
 p.PrgrmDocReqdInd,
 p.PrgrmMaxTrmMoQty,
 p.PrgrmDesc,
 a.LendrDirectAuthInd -- RGG 07/30/2012 
 FROM PrtAgrmtTbl a , PrgrmTbl p, PrtTbl prt
 WHERE a.PrtId = p_PrtId
 AND a.PrtId = prt.PrtId
 AND a.PrgrmId = p.PrgrmId
 AND (a.PrtAgrmtEffDt > SYSDATE OR
 a.PrtAgrmtSeqNmb = (Select Max(s.PrtAgrmtSeqNmb)
 FROM PrtAgrmtTbl s
 WHERE a.PrtId =s.PrtId
 AND a.PrgrmId = s.PrgrmId
 AND s.PrtAgrmtEffDt <= SYSDATE
 )
 )
 order by a.PrgrmId, a.PrtAgrmtSeqNmb desc ;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 15 THEN
 /* TO GET UNDRWRITINGBY */
 /* Step 1:For partner id and processing method, for all the mapped active agreements, if at least one agreement level in partner.PrtAgrmtTbl override exists, set the value and exit. In case of multi-rows, UW by LNDR has precedence.
 Step2: If Step 1 returns null for all the mapped active agreements, use UW By value for given PIMS program Id (sbaref.IMPrgrmAuthOthAgrmtTbl). In case of multi-rows, use preferred order number */
 Begin
 select distinct UndrwritngBy into v_UndrwritngBy
 from sbaref.IMPrgrmAuthOthAgrmtTbl u
 where u.prcsmthdcd=p_prcsmthdcd
 and trunc(u.IMPrgrmAuthOthStrtDt) <= trunc(sysdate)
 and ( u.IMPrgrmAuthOthEndDt is null
 or trunc(u.IMPrgrmAuthOthEndDt) >= trunc(sysdate))
 and u.PIMSPrgrmId is null; 
 EXCEPTION
 WHEN others 
 THEN
 v_UndrwritngBy := NULL;-- for direct loans,PIMSPrgrmId is null,need not check partner.PrtAgrmtTbl for UndrwritngBy
 End; 
 
 if v_UndrwritngBy is null then
 Begin
 select case when MAX(DECODE(x.LendrDirectAuthInd,'Y',2,'N',1,0))=2
 then 'LNDR' --step 1
 when MAX(DECODE (x.LendrDirectAuthInd,'Y',2,'N',1,0))=1
 then 'SBA' -- step 1
 when MAX(DECODE (x.LendrDirectAuthInd,'Y',2,'N',1,0))=0 
 then NULL -- step 2
 end,
 min(x.AGRMTPREFORDNMB) into v_UndrwritngBy,v_AGRMTPREFORDNMB
 FROM (select distinct LendrDirectAuthInd,UndrwritngBy,AGRMTPREFORDNMB
 from 
 partner.PrtAgrmtTbl a,
 partner.PrgrmTbl p,
 sbaref.IMPrgrmAuthOthAgrmtTbl u 
 left outer join sbaref.PrgrmValidTbl b on u.PrcsMthdCd = b.PrcsMthdCd
 where a.PrtId = p_Prtid
 AND trunc(a.PrtAgrmtEffDt) <= trunc(nvl(p_PrtAgrmtAsOnDt,sysdate))
 and ( a.PrtAgrmtExprDt is null
 or trunc(a.PrtAgrmtExprDt) >= trunc(nvl(p_PrtAgrmtAsOnDt,sysdate)))
 and ( a.PrtAgrmtTermDt is null
 or trunc(a.PrtAgrmtTermDt) > trunc(nvl(p_PrtAgrmtAsOnDt,sysdate)))
 and a.PrgrmId = p.PrgrmId
 and p.ValidPrgrmTyp = u.PIMSPrgrmId
 and trunc(u.IMPrgrmAuthOthStrtDt) <= trunc(sysdate)
 and ( u.IMPrgrmAuthOthEndDt is null
 or trunc(u.IMPrgrmAuthOthEndDt) >= trunc(sysdate))
 and ( b.PrgrmValidStrtDt is null 
 or trunc(b.PrgrmValidStrtDt) <= trunc(sysdate))
 and ( b.PrgrmValidEndDt is null
 or trunc(b.PrgrmValidEndDt) >= trunc(sysdate))
 and u.PrcsMthdCd = p_prcsmthdcd)x; 
 EXCEPTION
 WHEN no_data_found 
 THEN 
 v_UndrwritngBy := NULL;
 v_AGRMTPREFORDNMB := NULL;
 end; 
 end if;
 if v_UndrwritngBy IS NULL and v_AGRMTPREFORDNMB is not null --step 2
 then 
 begin
 select UndrwritngBy into v_UndrwritngBy
 FROM sbaref.IMPrgrmAuthOthAgrmtTbl s
 where prcsmthdcd = p_prcsmthdcd
 and AGRMTPREFORDNMB = v_AGRMTPREFORDNMB ;
 EXCEPTION
 WHEN no_data_found 
 THEN 
 v_UndrwritngBy := NULL;
 end;
 end if;
 
 begin
 open p_SelCur for
 
 select v_UndrwritngBy as UndrwritngBy from dual;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 NULL;
 end; 
 p_RetVal := SQL%ROWCOUNT; 
 ELSIF p_Identifier = 38 THEN
 BEGIN
 OPEN p_SelCur FOR 
 /* Retrieve just the PrgrmIds of all active agreements. */
 select PrgrmId
 from partner.PrtAgrmtTbl 
 where (PrtId = p_PrtId)
 and (PrtAgrmtEffDt <= sysdate)
 and (PrtAgrmtExprDt >= trunc(sysdate) or PrtAgrmtExprDt is null)
 and (PrtAgrmtTermDt > sysdate or PrtAgrmtTermDt is null)
 order by PrgrmId;
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtSel;
 RAISE;
 
 END PRTAGRMTSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


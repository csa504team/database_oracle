<!--- Saved 05/05/2015 13:20:08. --->
PROCEDURE VALIDPRTROLESELCSP
 (
 p_userName VARCHAR2:=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrtRoleTyp, 
 ValidPrtRoleDesc
 FROM ValidPrtRoleTbl 
 WHERE ValidPrtRoleDispOrdNmb != 0 
 ORDER BY ValidPrtRoleDispOrdNmb ; 
 p_RetVal := SQL%ROWCOUNT; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:55. --->
PROCEDURE PRTAGRMTINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtDocLocOfcCd CHAR := NULL,
 p_PrtAgrmtAreaOfOperTxt VARCHAR2 := NULL,
 p_PrtAgrmtDocLocTxt VARCHAR2 := NULL,
 p_PrtAgrmtEffDt DATE := NULL,
 p_PrtAgrmtExprDt DATE := NULL,
 p_PrtAgrmtTermDt DATE := NULL,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtWvrInd CHAR := NULL,
 p_PrtAgrmtCreatUserId VARCHAR2 := NULL,
 p_LendrDirectAuthInd char := NULL
 )
 AS
 NewDataRec PrtAgrmtTbl%ROWTYPE;
 BEGIN
 
 SAVEPOINT PrtAgrmtIns;
 
 IF p_Identifier = 0 THEN
 BEGIN
 if p_PrgrmId = 'Community Express' then
 begin
 INSERT INTO PARTNER.PrtAgrmtTbl (PrtId, PrtAgrmtSeqNmb, PrgrmId, PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt, PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt, PrtAgrmtExprDt,PrtAgrmtTermDt, PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,PrtAgrmtLoanLimtInd, PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,PrtAgrmtMaxWkLoanQty,PrtAgrmtCreatUserId,
 PrtAgrmtCreatDt,lastupdtuserid,lastupdtdt)
 VALUES (p_PrtId,p_PrtAgrmtSeqNmb,p_PrgrmId,p_PrtAgrmtDocLocOfcCd,
 p_PrtAgrmtAreaOfOperTxt,
 p_PrtAgrmtDocLocTxt,p_PrtAgrmtEffDt,p_PrtAgrmtExprDt,
 p_PrtAgrmtTermDt,p_PrtAgrmtTermRsnCd,p_PrtAgrmtWvrInd,'Y',5,999,
 10,p_PrtAgrmtCreatUserId,SYSDATE,p_PrtAgrmtCreatUserId,SYSDATE);
 begin
 SELECT * INTO NewDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 p_RetVal :=SQL%ROWCOUNT;
 end;
 else
 begin
 INSERT INTO PARTNER.PrtAgrmtTbl (PrtId,PrtAgrmtSeqNmb,PrgrmId,PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,PrtAgrmtExprDt,PrtAgrmtTermDt,PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,PrtAgrmtLoanLimtInd,PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,PrtAgrmtMaxWkLoanQty,PrtAgrmtCreatUserId,
 PrtAgrmtCreatDt,lastupdtuserid,lastupdtdt)
 VALUES (p_PrtId,p_PrtAgrmtSeqNmb,p_PrgrmId,p_PrtAgrmtDocLocOfcCd,
 p_PrtAgrmtAreaOfOperTxt,
 p_PrtAgrmtDocLocTxt,p_PrtAgrmtEffDt,p_PrtAgrmtExprDt,
 p_PrtAgrmtTermDt,p_PrtAgrmtTermRsnCd,p_PrtAgrmtWvrInd,'N',NULL,
 NULL,NULL,p_PrtAgrmtCreatUserId,SYSDATE,p_PrtAgrmtCreatUserId,SYSDATE);
 begin
 SELECT * INTO NewDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 p_RetVal :=SQL%ROWCOUNT;
 end;
 end if; 
 END;
 ELSIF p_Identifier = 11 THEN
 BEGIN
 
 if p_PrgrmId = 'Community Express' then
 begin
 INSERT INTO PARTNER.PrtAgrmtTbl (PrtId,PrtAgrmtSeqNmb,PrgrmId,PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,PrtAgrmtExprDt,PrtAgrmtTermDt,PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,PrtAgrmtLoanLimtInd,PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,PrtAgrmtMaxWkLoanQty,PrtAgrmtCreatUserId,
 PrtAgrmtCreatDt,lastupdtuserid,lastupdtdt,LendrDirectAuthInd)
 SELECT p_PrtId,NVL(MAX(PrtAgrmtSeqNmb), 0) + 1,p_PrgrmId,
 p_PrtAgrmtDocLocOfcCd,p_PrtAgrmtAreaOfOperTxt,
 p_PrtAgrmtDocLocTxt,p_PrtAgrmtEffDt,
 p_PrtAgrmtExprDt,p_PrtAgrmtTermDt,p_PrtAgrmtTermRsnCd,
 p_PrtAgrmtWvrInd,'Y',5,999,10,p_PrtAgrmtCreatUserId,
 SYSDATE,p_PrtAgrmtCreatUserId,SYSDATE,p_LendrDirectAuthInd
 FROM PrtAgrmtTbl 
 WHERE PrtId =p_PrtId; 
 begin
 SELECT * INTO NewDataRec FROM PrtAgrmtTbl a WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = (select max(PrtAgrmtSeqNmb) from PrtAgrmtTbl z where a.PrtId = z.PrtId);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 p_RetVal :=SQL%ROWCOUNT;
 end;
 else
 begin
 INSERT INTO PARTNER.PrtAgrmtTbl (PrtId,PrtAgrmtSeqNmb,PrgrmId,PrtAgrmtDocLocOfcCd,
 PrtAgrmtAreaOfOperTxt,PrtAgrmtDocLocTxt,
 PrtAgrmtEffDt,PrtAgrmtExprDt,PrtAgrmtTermDt,PrtAgrmtTermRsnCd,
 PrtAgrmtWvrInd,PrtAgrmtLoanLimtInd,PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty,PrtAgrmtMaxWkLoanQty,PrtAgrmtCreatUserId,
 PrtAgrmtCreatDt,lastupdtuserid,lastupdtdt,LendrDirectAuthInd)
 SELECT p_PrtId,NVL(MAX(PrtAgrmtSeqNmb), 0) + 1,p_PrgrmId,
 p_PrtAgrmtDocLocOfcCd,p_PrtAgrmtAreaOfOperTxt,
 p_PrtAgrmtDocLocTxt, p_PrtAgrmtEffDt,
 p_PrtAgrmtExprDt,p_PrtAgrmtTermDt,p_PrtAgrmtTermRsnCd,
 p_PrtAgrmtWvrInd,'N',NULL,NULL,NULL,
 p_PrtAgrmtCreatUserId, SYSDATE,p_PrtAgrmtCreatUserId,SYSDATE,p_LendrDirectAuthInd
 FROM PrtAgrmtTbl 
 WHERE PrtId =p_PrtId;
 begin
 SELECT * INTO NewDataRec FROM PrtAgrmtTbl a WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = (select max(PrtAgrmtSeqNmb) from PrtAgrmtTbl z where a.PrtId = z.PrtId);
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 p_RetVal :=SQL%ROWCOUNT;
 end;
 end if;
 END;
 END IF;
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAgrmtInsTrigCSP (NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtIns;
 RAISE;
 
 END PRTAGRMTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


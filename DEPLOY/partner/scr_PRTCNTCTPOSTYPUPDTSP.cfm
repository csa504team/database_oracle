<!--- Saved 05/05/2015 13:17:27. --->
PROCEDURE PRTCNTCTPOSTYPUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctPosTypCd NUMBER := 0,
 p_PrtCntctPosTypTxt VARCHAR2 := NULL,
 p_PrtCntctPosTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtCntctPosTypUpd;
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.PrtCntctPosTypTbl
 SET PrtCntctPosTypTxt = p_PrtCntctPosTypTxt, 
 LASTUPDTUSERID = p_PrtCntctPosTypCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtCntctPosTypCd = p_PrtCntctPosTypCd ;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtCntctPosTypUpd;
 RAISE;
 
 END PRTCNTCTPOSTYPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/25/2018 10:37:59. --->
PROCEDURE PRTLOCUPDCSP (
 p_PrtLocCreatUserId VARCHAR2 := NULL,
 p_LocId NUMBER := NULL,
 p_PrtLocFIRSNmb CHAR := NULL,
 p_PrtLocFIRSRecrdTyp CHAR := NULL,
 p_PrtLocNm VARCHAR2 := NULL,
 p_ValidLocTyp VARCHAR2 := NULL,
 p_PrtLocOpndDt DATE := NULL,
 p_PrtLocInactDt DATE := NULL,
 p_PrtLocCurStatCd CHAR := NULL,
 p_PrtLocOperHrsTxt VARCHAR2 := NULL,
 p_RetVal OUT NUMBER)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd 
 */
 -- Trigger variables
 OldDataRec partner.PrtLocTbl%ROWTYPE;
 NewDataRec partner.PrtLocTbl%ROWTYPE;
 BEGIN
 SAVEPOINT location_update;
 
 -- get old snapshot
 BEGIN
 SELECT *
 INTO OldDataRec
 FROM PrtLocTbl
 WHERE LocId = p_LocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 OldDataRec := NULL;
 END;
 
 UPDATE PARTNER.PrtLocTbl
 SET PrtLocFIRSNmb = p_PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp = p_PrtLocFIRSRecrdTyp,
 PrtLocNm = p_PrtLocNm,
 ValidLocTyp = p_ValidLocTyp,
 PrtLocOpndDt = p_PrtLocOpndDt,
 PrtLocInactDt = p_PrtLocInactDt,
 PrtLocCurStatCd = p_PrtLocCurStatCd,
 PrtLocOperHrsTxt = p_PrtLocOperHrsTxt,
 LASTUPDTUSERID = RTRIM (p_PrtLocCreatUserId),
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId;
 
 p_RetVal := SQL%ROWCOUNT;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtLocTbl
 WHERE LocId = p_LocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtLocUpdTrigCSP (OldDataRec, NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO location_update;
 RAISE;
 END PRTLOCUPDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:48. --->
PROCEDURE VALIDLOCTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidLocTyp VARCHAR2 :=NULL,
 p_ValidLocTypDispOrdNmb NUMBER:=0,
 p_ValidLocTypDesc VARCHAR2 :=NULL,
 p_ValidLocTypCreatUserId VARCHAR2 :=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidLocTypUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 UPDATE ValidLocTypTbl
 SET 
 ValidLocTypDispOrdNmb =p_ValidLocTypDispOrdNmb, 
 ValidLocTypDesc =p_ValidLocTypDesc, 
 ValidLocTypCreatUserId =p_ValidLocTypCreatUserId, 
 ValidLocTypCreatDt =SYSDATE
 WHERE ValidLocTyp =p_ValidLocTyp;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDLOCTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


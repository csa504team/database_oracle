<!--- Saved 08/11/2014 10:12:12. --->
PROCEDURE PrtAliasDelTrigCSP (
 p_PartnerId number,
 p_AliasTyp varchar2
 )
 AS
 --
 -- If a PartnerAlias of type FDIC or FRS or NCUA or TIN is
 -- affected, then ALL the locations for the Partner are
 -- added to the extract queue.
 --
 -- Revision History -----------------------------------------
 -- 04/20/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 BEGIN
 v_INST_ID := 0;
 IF p_AliasTyp IN ('FDIC', 'FRS', 'NCUA', 'TIN')
 THEN
 BEGIN
 -- Add all locations for Partner to extract queue.
 LocExtrctStatUpdCSP (p_PartnerId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 30882;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtAliasDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 RETURN;
 END;
 END IF;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:32:04. --->
PROCEDURE PRTUPDCSP (
 p_userName VARCHAR2 := NULL,
 p_PrtId NUMBER := NULL,
 p_ofcCd CHAR := NULL,
 p_PrtChrtrTyp VARCHAR2 := NULL,
 p_PrtEstbDt DATE := NULL,
 p_PrtFisclYrEndDayNmb NUMBER := NULL,
 p_PrtFisclYrEndMoNmb NUMBER := NULL,
 p_PrtInactDt DATE := NULL,
 p_PrtLglNm VARCHAR2 := NULL,
 p_ValidPrtLglTyp VARCHAR2 := NULL,
 p_ValidPrtPrimCatTyp VARCHAR2 := NULL,
 p_PrtCurStatCd CHAR := NULL,
 p_ValidPrtSubCatTyp VARCHAR2 := NULL)
 AS
 /* RY--01/19/18--OPSMDEV-1335:: Commented out code from "IF t_PrtDataSourcId = 'TFP-Replication' block.
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtDataSourcId
 */
 v_RetVal NUMBER (10, 0);
 -- trigger variables
 OldDataRec PrtTbl%ROWTYPE;
 NewDataRec PrtTbl%ROWTYPE;
 BEGIN
 SAVEPOINT partner_update;
 
 
 
 SELECT COUNT (*)
 INTO v_RetVal
 FROM PrtTbl
 WHERE PrtId = p_PrtId;
 
 IF v_RetVal <> 1
 THEN
 BEGIN
 raise_application_error (-20001,
 'Incorrect number of rows for update');
 RETURN;
 END;
 END IF;
 
 
 -- IF t_PrtDataSourcId = 'TFP-Replication'
 -- THEN
 -- BEGIN
 -- ROLLBACK TO SAVEPOINT partner_update;
 -- raise_application_error (-20999,
 -- 'Update of replicated data not allowed');
 -- RETURN /*-100*/
 -- ;
 -- END;
 -- END IF; --RY 01/19/18
 
 -- get old snapshot
 BEGIN
 SELECT *
 INTO OldDataRec
 FROM PrtTbl
 WHERE PrtId = p_PrtId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 OldDataRec := NULL;
 END;
 
 UPDATE PARTNER.PrtTbl
 SET OfcCd = p_ofcCd,
 PrtChrtrTyp = p_PrtChrtrTyp,
 PrtEstbDt = p_PrtEstbDt,
 PrtFisclYrEndDayNmb = p_PrtFisclYrEndDayNmb,
 PrtFisclYrEndMoNmb = p_PrtFisclYrEndMoNmb,
 PrtInactDt = p_PrtInactDt,
 PrtLglNm = p_PrtLglNm,
 ValidPrtLglTyp = p_ValidPrtLglTyp,
 ValidPrtPrimCatTyp = p_ValidPrtPrimCatTyp,
 PrtCurStatCd = p_PrtCurStatCd,
 ValidPrtSubCatTyp = p_ValidPrtSubCatTyp,
 lastupdtuserid = RTRIM (p_userName),
 lastupdtdt = SYSDATE
 WHERE PrtId = p_PrtId;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtTbl
 WHERE PrtId = p_PrtId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtUpdTrigCSP (OldDataRec, NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END PRTUPDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


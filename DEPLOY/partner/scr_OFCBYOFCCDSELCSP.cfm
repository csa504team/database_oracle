<!--- Saved 05/05/2015 13:16:32. --->
PROCEDURE OFCBYOFCCDSELCSP(
 p_userName IN VARCHAR2 := NULL,
 p_SBAOfcCd IN CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 IF ( p_SBAOfcCd is NULL) 
 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT DISTINCT o.SBAOfcCd
 , o.SBAOfcTypCd
 , o.SBAOfc1Nm
 , o.StCd
 FROM SBAREF.SBAOfcTbl o 
 WHERE o.SBAOfcTypCd IN (3, 4, 7) 
 AND 
 (o.SBAOfcEndDt > SYSDATE 
 or o.SBAOfcEndDt IS NULL) 
 ORDER BY o.SBAOfc1Nm ;
 END;
 ELSE
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT DISTINCT o.SBAOfcCd
 , o.SBAOfcTypCd
 , o.SBAOfc1Nm
 , o.StCd
 FROM SBAREF.SBAOfcTbl o 
 WHERE o.SBAOfcCd = SBAOfcCd 
 AND (o.SBAOfcEndDt > SYSDATE 
 or o.SBAOfcEndDt IS NULL) 
 ORDER BY o.SBAOfc1Nm ;
 END;
 END IF;
 END OFCBYOFCCDSELCSP;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


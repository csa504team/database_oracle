<!--- Saved 05/05/2015 13:18:46. --->
PROCEDURE PRTNOTECNTNTDELTSP(
 p_Identifier IN NUMBER := 0,
 p_RetVal IN OUT NUMBER,
 p_LocId IN NUMBER := 0,
 p_PrtNoteSeqNmb IN NUMBER := 0)
 AS
 BEGIN
 SAVEPOINT PrtNoteCntntDel;
 IF p_Identifier = 0 THEN
 /* Delate of PrtNoteCntnt Table */
 BEGIN
 DELETE PrtNoteCntntTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteCntntDel;
 RAISE;
 
 END PRTNOTECNTNTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


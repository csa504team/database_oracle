<!--- Saved 11/16/2010 10:11:09. --->
PROCEDURE VALIDPRGRMTYPSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrgrmTyp VARCHAR2 :=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 SAVEPOINT ValidPrgrmTypSel;
 
 IF p_Identifier= 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrgrmTyp, 
 ValidPrgrmTypDispOrdNmb, 
 ValidPrgrmTypDesc
 FROM ValidPrgrmTypTbl 
 WHERE ValidPrgrmTyp = p_ValidPrgrmTyp;
 end;
 
 ELSIF p_Identifier= 2 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM ValidPrgrmTypTbl 
 WHERE ValidPrgrmTyp = ValidPrgrmTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRGRMTYPSEL;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


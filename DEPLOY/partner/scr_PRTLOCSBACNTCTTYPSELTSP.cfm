<!--- Saved 05/05/2015 13:18:36. --->
PROCEDURE PRTLOCSBACNTCTTYPSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctTypSel;
 IF p_Identifier = 0 THEN
 /* Select from PrtLocSBACntctTyp Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtLocSBACntctTypCd,
 PrtLocSBACntctDescTxt
 FROM PrtLocSBACntctTypTbl 
 WHERE PrtLocSBACntctTypCd = p_PrtLocSBACntctTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocSBACntctTypTbl 
 WHERE PrtLocSBACntctTypCd = p_PrtLocSBACntctTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 4 THEN
 /*Select all the rows*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtLocSBACntctTypCd,
 PrtLocSBACntctDescTxt 
 FROM PrtLocSBACntctTypTbl;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctTypSel;
 RAISE;
 
 END PRTLOCSBACNTCTTYPSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


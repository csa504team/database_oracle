<!--- Saved 05/05/2015 13:19:26. --->
PROCEDURE SBAOFCDCNTYSELCSP(
 p_userName VARCHAR2 := NULL,
 p_retVal OUT NUMBER ,
 p_OfficeCodes VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT SBAOFCDCNTYSEL; 
 OPEN p_SelCur FOR
 SELECT c.StCd,
 c.CntyCd,
 c.OrignOfcCd,
 c.CntyNm,
 s.SBAOfc1Nm
 FROM SBAREF.CntyTbl c, SBAREF.SBAOfcTbl s 
 WHERE INSTR(p_OfficeCodes,c.OrignOfcCd) > 0 
 AND c.OrignOfcCd = s.SBAOfcCd 
 AND (s.SBAOfcEndDt > SYSDATE or s.SBAOfcEndDt IS NULL) 
 AND (c.CntyEndDt > SYSDATE or c.CntyEndDt IS NULL) 
 ORDER BY c.StCd , c.OrignOfcCd , c.CntyNm ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO SBAOFCDCNTYSEL;
 END;
 END SBAOFCDCNTYSELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


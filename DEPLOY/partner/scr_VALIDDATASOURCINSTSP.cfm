<!--- Saved 01/23/2018 15:32:05. --->
PROCEDURE VALIDDATASOURCINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidDataSourcCd NUMBER:=0,
 p_ValidDataSourcDescTxt VARCHAR2 :=NULL,
 p_ValidDataSourcCreatUserId VARCHAR2 :=NULL
 )
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT ValidDataSourcIns;
 IF p_Identifier= 0 THEN
 /* Insert into ValidDataSourcTbl Table */
 BEGIN
 INSERT INTO ValidDataSourcTbl 
 (
 ValidDataSourcDescTxt, 
 ValidDataSourcCreatUserId
 )
 VALUES (
 p_ValidDataSourcDescTxt, 
 p_ValidDataSourcCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDDATASOURCINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


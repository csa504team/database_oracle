<!--- Saved 01/23/2018 15:32:00. --->
PROCEDURE PRTINSTSP (
 --p_userName VARCHAR2 := NULL,
 p_CreatUserId VARCHAR2 := NULL,
 p_OfcCd CHAR := NULL,
 p_PrtChrtrTyp VARCHAR2 := NULL,
 p_PrtEstbDt DATE := NULL,
 p_PrtFisclYeEndDayNmb NUMBER := NULL,
 p_PrtFisclYrEndMoNmb NUMBER := NULL,
 p_PrtInactDt DATE := NULL,
 p_PrtLglNm VARCHAR2 := NULL,
 p_ValidPrtLglTyp VARCHAR2 := NULL,
 p_RetVal OUT NUMBER,
 p_ValidPrtPrimCatTyp VARCHAR2 := NULL,
 p_PrtCurStatCd CHAR := NULL,
 p_ValidPrtSubCatTyp VARCHAR2 := NULL,
 p_assigned_id OUT NUMBER)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtDataSourcId
 */
 -- trigger variables
 NewDataRec PrtTbl%ROWTYPE;
 v_PrtId NUMBER (7);
 BEGIN
 SAVEPOINT PRTINSTSP;
 
 BEGIN
 v_PrtId := partner.PrtIdSeq.NEXTVAL;
 
 INSERT INTO PARTNER.PrtTbl (PrtId,
 OfcCd,
 PrtChrtrTyp,
 PrtEstbDt,
 PrtFisclYrEndDayNmb,
 PrtFisclYrEndMoNmb,
 PrtInactDt,
 PrtLglNm,
 ValidPrtLglTyp,
 ValidPrtPrimCatTyp,
 PrtCurStatCd,
 ValidPrtSubCatTyp,
 PrtCreatUserId,
 PrtCreatDt,
 lastupdtuserid,
 lastupdtdt)
 VALUES (v_PrtId,
 p_OfcCd,
 p_PrtChrtrTyp,
 p_PrtEstbDt,
 p_PrtFisclYeEndDayNmb,
 p_PrtFisclYrEndMoNmb,
 p_PrtInactDt,
 p_PrtLglNm,
 TRIM (p_ValidPrtLglTyp),
 p_ValidPrtPrimCatTyp,
 p_PrtCurStatCd,
 p_ValidPrtSubCatTyp,
 RTRIM (p_CreatUserId),
 SYSDATE,
 RTRIM (p_CreatUserId),
 SYSDATE);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 p_assigned_id := v_PrtId;
 
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtTbl
 WHERE PrtId = v_PrtId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtInsTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PRTINSTSP;
 RAISE;
 END PRTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


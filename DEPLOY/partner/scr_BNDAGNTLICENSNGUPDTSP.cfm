<!--- Saved 05/05/2015 13:15:55. --->
PROCEDURE BNDAGNTLICENSNGUPDTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_RetVal OUT NUMBER ,
 p_IMUserNm IN VARCHAR2 := NULL ,
 p_LicensingStCd IN CHAR := NULL 
 )
 AS
 
 v_LocId NUMBER(7,0);
 BEGIN
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 /* Insert intodbo.BndAgntLicensngTbl Table */
 
 SELECT LocId INTO v_LocId
 FROM security.IMUserTbl 
 WHERE IMUserNm = p_IMUserNm;
 
 -- delete sbg..BndAgntLicensngTbl where LocId = @LocId
 
 
 INSERT INTO BndAgntLicensngTbl
 ( LocId, LicensingStCd, LASTUPDTUSERID,LASTUPDTDT )
 VALUES ( v_LocId, p_LicensingStCd, p_IMUserNm, SYSDATE );
 
 p_RetVal := SQL%ROWCOUNT ;
 
 END;
 END IF;
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


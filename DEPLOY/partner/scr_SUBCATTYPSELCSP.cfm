<!--- Saved 05/05/2015 13:19:31. --->
PROCEDURE SUBCATTYPSELCSP
 (
 p_userName VARCHAR2 :=NULL,
 p_Purpose VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 IF p_Purpose = 'search' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT 
 v.ValidSBAPrtPrimCatTyp , 
 c.ValidPrtPrimCatTypDesc, 
 v.ValidPrtSubCatTyp, 
 v.ValidPrtSubCatDesc
 FROM ValidPrtSubCatTbl v, ValidPrtPrimCatTbl c 
 WHERE v.ValidSBAPrtPrimCatTyp = c.ValidPrtPrimCatTyp 
 ORDER BY c.ValidPrtPrimCatTypDesc , v.ValidPrtSubCatDesc ;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT 
 v.ValidSBAPrtPrimCatTyp, 
 c.ValidPrtPrimCatTypDesc, 
 v.ValidPrtSubCatTyp, 
 v.ValidPrtSubCatDesc
 FROM SECURITY.IMUserTbl u, SECURITY.IMUserRoleTbl r, SECURITY.IMRoleObjctPrvlgTbl p, 
 ValidPrtSubCatTbl v, ValidPrtPrimCatTbl c 
 WHERE u.IMUserNm = p_userName 
 AND u.IMUserId = r.IMUserId 
 AND p.IMAppRoleId = r.IMAppRoleId 
 AND (v.RolePrivilegePrgrmId = p.IMPIMSPrgrmId OR p.IMPIMSPrgrmId = '%') 
 AND v.ValidSBAPrtPrimCatTyp = c.ValidPrtPrimCatTyp 
 ORDER BY c.ValidPrtPrimCatTypDesc , v.ValidPrtSubCatDesc ;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO SUBCATTYPSELCSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


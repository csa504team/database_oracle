<!--- Saved 05/05/2015 13:15:57. --->
PROCEDURE CNTYSELCSP(
 p_userName VARCHAR2 :=NULL,
 p_stateCd IN CHAR :=NULL,
 p_RetVal OUT number,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 OPEN p_SelCur FOR SELECT c.StCd
 , c.CntyCd
 , c.OrignOfcCd
 , c.CntyNm
 , s.SBAOfc1Nm
 FROM SBAREF.CntyTbl c, SBAREF.SBAOfcTbl s
 WHERE c.StCd = p_stateCd
 AND c.OrignOfcCd = s.SBAOfcCd
 AND
 (s.SBAOfcEndDt > SYSDATE
 or s.SBAOfcEndDt IS NULL)
 AND
 (c.CntyEndDt > SYSDATE
 or c.CntyEndDt IS NULL)
 ORDER BY c.OrignOfcCd , c.CntyNm ;
 p_RetVal := SQL%ROWCOUNT;
 
 
 EXCEPTION
 WHEN OTHERS THEN 
 RAISE;
 ROLLBACK TO CNTYSELCSP;
 
 END ;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


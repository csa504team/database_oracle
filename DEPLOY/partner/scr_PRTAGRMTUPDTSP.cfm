<!--- Saved 02/01/2017 14:43:53. --->
PROCEDURE PRTAGRMTUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtDocLocOfcCd CHAR := NULL,
 p_PrtAgrmtAreaOfOperTxt VARCHAR2 := NULL,
 p_PrtAgrmtDocLocTxt VARCHAR2 := NULL,
 p_PrtAgrmtEffDt DATE := NULL,
 p_PrtAgrmtExprDt DATE := NULL,
 p_PrtAgrmtTermDt DATE := NULL,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtWvrInd CHAR := NULL,
 p_PrtAgrmtLoanLimtInd char := Null,
 p_PrtAgrmtMaxMoLoanQty number := 0,
 p_PrtAgrmtMaxFyLoanQty number := 0,
 p_PrtAgrmtMaxWkLoanQty number := 0,
 p_PrtAgrmtCreatUserId VARCHAR2 := NULL,
 p_LendrDirectAuthInd char := NULL)
 AS
 
 OldDataRec PrtAgrmtTbl%ROWTYPE;
 NewDataRec PrtAgrmtTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtAgrmtUpd; 
 
 
 
 IF p_Identifier = 0 THEN
 /* Updert into PrtAgrmt Table */
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 UPDATE PARTNER.PrtAgrmtTbl
 
 
 SET PrtId =p_prtid, 
 PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb, 
 PrgrmId =p_PrgrmId, 
 PrtAgrmtDocLocOfcCd =p_PrtAgrmtDocLocOfcCd, 
 PrtAgrmtAreaOfOperTxt =p_PrtAgrmtAreaOfOperTxt, 
 PrtAgrmtDocLocTxt =p_PrtAgrmtDocLocTxt, 
 PrtAgrmtEffDt =p_PrtAgrmtEffDt, 
 PrtAgrmtExprDt =p_PrtAgrmtExprDt, 
 PrtAgrmtTermDt =p_PrtAgrmtTermDt, 
 PrtAgrmtTermRsnCd =p_PrtAgrmtTermRsnCd, 
 PrtAgrmtWvrInd =p_PrtAgrmtWvrInd, 
 PrtAgrmtLoanLimtInd=p_PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty=p_PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty=p_PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty=p_PrtAgrmtMaxWkLoanQty, 
 lastupdtuserid =p_PrtAgrmtCreatUserId, 
 lastupdtdt =SYSDATE 
 WHERE PrtId =p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 /* Agreement.Update() for active one */
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 UPDATE PARTNER.PrtAgrmtTbl
 SET PrtAgrmtDocLocOfcCd =p_PrtAgrmtDocLocOfcCd, 
 PrtAgrmtAreaOfOperTxt =p_PrtAgrmtAreaOfOperTxt, 
 PrtAgrmtDocLocTxt =p_PrtAgrmtDocLocTxt, 
 PrtAgrmtTermDt =p_PrtAgrmtTermDt, 
 PrtAgrmtTermRsnCd =p_PrtAgrmtTermRsnCd, 
 PrtAgrmtWvrInd =p_PrtAgrmtWvrInd, 
 PrtAgrmtLoanLimtInd=p_PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty=p_PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty=p_PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty=p_PrtAgrmtMaxWkLoanQty,
 lastupdtuserid =p_PrtAgrmtCreatUserId, 
 lastupdtdt =SYSDATE,
 LendrDirectAuthInd = p_LendrDirectAuthInd 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 12 THEN
 /* Agreement.Update() for pending one */
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 UPDATE PARTNER.PrtAgrmtTbl
 SET PrtAgrmtDocLocOfcCd =p_PrtAgrmtDocLocOfcCd, 
 PrtAgrmtAreaOfOperTxt =p_PrtAgrmtAreaOfOperTxt, 
 PrtAgrmtDocLocTxt =p_PrtAgrmtDocLocTxt, 
 PrtAgrmtEffDt =p_PrtAgrmtEffDt, 
 PrtAgrmtExprDt =p_PrtAgrmtExprDt, 
 PrtAgrmtWvrInd =p_PrtAgrmtWvrInd, 
 PrtAgrmtLoanLimtInd=p_PrtAgrmtLoanLimtInd,
 PrtAgrmtMaxMoLoanQty=p_PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty=p_PrtAgrmtMaxFyLoanQty,
 PrtAgrmtMaxWkLoanQty=p_PrtAgrmtMaxWkLoanQty, 
 lastupdtuserid =p_PrtAgrmtCreatUserId, 
 lastupdtdt =SYSDATE,
 LendrDirectAuthInd = p_LendrDirectAuthInd 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 PrtAgrmtUpdTrigCSP (OldDataRec, NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 COMMIT; 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtUpd;
 RAISE;
 END PRTAGRMTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:19. --->
PROCEDURE PRTWBCLOCINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER:=0,
 p_ValidWBCClassCd NUMBER:=0,
 p_ValidWBCStatCd NUMBER:=0,
 p_PrtWBCLocFirstFundYrNmb NUMBER:=0,
 p_PrtWBCLocPrjctdFnlFundYrNmb NUMBER:=0,
 p_PrtWBCLocCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT PrtWBCLocIns;
 IF p_Identifier= 0 THEN
 /* Insert into PrtWBCLocTbl Table */
 BEGIN
 INSERT INTO PrtWBCLocTbl
 (
 LocId, 
 ValidWBCClassCd, 
 ValidWBCStatCd, 
 PrtWBCLocFirstFundYrNmb, 
 PrtWBCLocPrjctdFnlFundYrNmb, 
 PrtWBCLocCreatUserId, 
 PrtWBCLocCreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT
 )
 VALUES (
 p_LocId, 
 p_ValidWBCClassCd, 
 p_ValidWBCStatCd, 
 p_PrtWBCLocFirstFundYrNmb, 
 p_PrtWBCLocPrjctdFnlFundYrNmb,
 p_PrtWBCLocCreatUserId,
 SYSDATE,
 p_PrtWBCLocCreatUserId,
 SYSDATE
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTWBCLOCINSTSP;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:15. --->
PROCEDURE GENERATEFIRSNMBCSP(
 p_FIRSNmb IN OUT CHAR,
 p_CreatUserId varCHAR2 := NULL)
 AS
 /*
 Created By:
 Purpose:
 Parameters:
 Modified By: APK -- 08/02/2010-- Modified to include proper exception handlers.
 RGG -- 09/27/2011-- Modified to accept p_creatuserid as input and update accordingly.
 */
 p_ErrorNmb NUMBER(5,0);
 p_HaveFIRSNmbInd CHAR(1);
 p_INST_ID NUMBER(8,0);
 p_LastFIRSNmb CHAR(7);
 p_LOC_ID NUMBER(8,0);
 p_NewFIRSNmb CHAR(7);
 p_ReplicationErrorMessageTxt VARCHAR2(255);
 tempVar1 VARCHAR2(255);
 StoO_error NUMBER(4);
 StoO_rowcnt NUMBER(4);
 StoO_selcnt NUMBER(4);
 BEGIN
 p_ErrorNmb := 0;
 p_INST_ID := 0;
 p_LOC_ID := 0;
 begin 
 SELECT LastFIRSNmb
 INTO tempVar1
 FROM LastFIRSNmbTbl;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 tempVar1 := NULL;
 end;
 p_LastFIRSNmb := tempVar1;
 IF StoO_error != 0 OR StoO_rowcnt != 1 THEN
 BEGIN
 p_ErrorNmb := StoO_error;
 p_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 p_ErrorNmb := 20001;
 p_ReplicationErrorMessageTxt := 'FIRS Number not generated.' 
 || ' Unable to retrieve last FIRS Number.' || ' The value ''MISSING'' has been substituted.';
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 END;
 END IF;
 IF p_ErrorNmb = 0 THEN
 BEGIN
 p_NewFIRSNmb := RPAD( ROUND( TO_NUMBER(p_LastFIRSNmb),0) + 1, 7, 
 ' ');
 END;
 END IF;
 IF StoO_error != 0 THEN
 BEGIN
 p_ErrorNmb := StoO_error;
 p_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 p_ErrorNmb := 20001;
 p_ReplicationErrorMessageTxt := 'FIRS Number not generated.' 
 || ' Unable to increment the last FIRS Number.' || ' The value ''MISSING'' has been substituted.';
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 END;
 END IF;
 p_HaveFIRSNmbInd := 'N';
 <<i_loop1>>
 WHILE ( p_ErrorNmb = 0) AND ( p_HaveFIRSNmbInd = 'N') LOOP
 BEGIN
 IF p_NewFIRSNmb > '7999999' THEN
 BEGIN
 p_ErrorNmb := 20001;
 p_ReplicationErrorMessageTxt := 'FIRS Number not generated.' 
 || ' Value would exceed ''7999999''.' || 'Value would have been ' 
 || p_NewFIRSNmb || ' The value ''MISSING'' has been substituted.';
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 END;
 END IF;
 IF p_ErrorNmb = 0 THEN
 BEGIN
 BEGIN
 BEGIN
 StoO_selcnt := 0;
 SELECT 1 INTO StoO_selcnt
 FROM DUAL
 WHERE NOT EXISTS ( 
 SELECT 1 FROM PrtLocTbl l 
 WHERE l.PrtLocFIRSNmb = p_NewFIRSNmb );
 
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 StoO_selcnt := NULL;
 END;
 IF StoO_selcnt != 0 THEN
 BEGIN
 p_HaveFIRSNmbInd := 'Y';
 UPDATE PARTNER.LastFIRSNmbTbl
 SET LastFIRSNmb = p_NewFIRSNmb, 
 LastFIRSNmbCreatUserId = p_CreatUserId, 
 LastFIRSNmbCreatDt = SYSDATE
 WHERE LastFIRSNmb = p_LastFIRSNmb;
 END;
 ELSE
 BEGIN
 p_NewFIRSNmb := RPAD( ROUND( TO_NUMBER(p_NewFIRSNmb),0) + 1,7, ' ');
 END;
 END IF;
 END;
 END;
 END IF;
 IF StoO_error != 0 THEN
 BEGIN
 p_ErrorNmb := StoO_error;
 p_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 p_ErrorNmb := 20001;
 p_ReplicationErrorMessageTxt := 'FIRS Number not generated.' 
 || ' Unable to update last FIRS Number.' || ' The value ''MISSING'' has been substituted.';
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 END;
 END IF;
 END;
 END LOOP;
 IF p_ErrorNmb = 0 THEN
 p_FIRSNmb := trim(p_NewFIRSNmb);
 ELSE
 p_FIRSNmb := 'MISSING';
 END IF;
 IF p_LastFIRSNmb > '7990000' THEN
 BEGIN
 p_ErrorNmb := 20001;
 p_ReplicationErrorMessageTxt := 'WARNING - FIRS Number values are approaching' 
 || ' their designed upper limit of ''7999999''.' || 'Last number assigned was ' 
 || p_NewFIRSNmb;
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID,p_LOC_ID,'GenerateFIRSNmbCSP',p_ErrorNmb,p_ReplicationErrorMessageTxt,NULL);
 END;
 END IF;
 
 END GENERATEFIRSNMBCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


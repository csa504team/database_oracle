<!--- Saved 05/05/2015 13:20:03. --->
PROCEDURE VALIDPRTNOTESUBJTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtNoteSubjTypCd NUMBER:=0,
 p_ValidPrtNoteSubjTypDescTxt VARCHAR2 :=NULL,
 p_ValidPrtNoteSubjCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtNoteSubjTypIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtNoteSubjTypTbl 
 (
 ValidPrtNoteSubjTypCd, 
 ValidPrtNoteSubjTypDescTxt, 
 ValidPrtNoteSubjCreatUserId)
 VALUES (
 p_ValidPrtNoteSubjTypCd, 
 p_ValidPrtNoteSubjTypDescTxt, 
 p_ValidPrtNoteSubjCreatUserId
 );
 
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 
 WHEN OTHERS THEN
 ROLLBACK TO VALIDPRTNOTESUBJTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


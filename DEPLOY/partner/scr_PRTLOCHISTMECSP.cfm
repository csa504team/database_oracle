<!--- Saved 05/05/2015 13:18:29. --->
PROCEDURE PRTLOCHISTMECSP(
 p_ReportBegDateTime IN DATE := NULL,
 p_ReportEndDateTime IN DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCHISTME; 
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate
 FROM LocHistryTbl a, LocHistryTbl r, LocHistryTbl i 
 WHERE a.ValidChngCd = 'ME' 
 AND a.LocHistryEffDt >= p_ReportBegDateTime 
 AND a.LocHistryEffDt <= p_ReportEndDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND (a.LocHistryAffctPrtId = r.LocHistryAffctPrtId AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = 'ME')
 ) 
 AND (a.LocHistryAffctPrtId = i.LocHistryAffctPrtId AND a.LocId = i.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = 'ME')
 ); 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCHISTME;
 RAISE;
 END PRTLOCHISTMECSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


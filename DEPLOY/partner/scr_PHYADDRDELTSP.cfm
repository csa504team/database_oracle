<!--- Saved 05/05/2015 13:16:36. --->
PROCEDURE PHYADDRDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PhyAddrSeqNmb NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0)
 AS
 OldDataRec PhyAddrTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PhyAddrDel;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 -- old snapshot
 SELECT * INTO OldDataRec FROM PhyAddrTbl WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 DELETE FROM PhyAddrTbl
 WHERE LocId = p_LocId 
 AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 
 THEN
 BEGIN
 -- old snapshot
 SELECT * INTO OldDataRec FROM PhyAddrTbl WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb AND PrtCntctNmb IS NOT NULL;
 DELETE FROM PhyAddrTbl 
 WHERE LocId = p_LocId 
 AND PrtCntctNmb = p_PrtCntctNmb 
 AND PrtCntctNmb IS NOT NULL;
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK TO SAVEPOINT IMSEQNMBSELTSP;
 END PHYADDRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


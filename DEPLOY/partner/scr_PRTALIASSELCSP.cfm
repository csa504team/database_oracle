<!--- Saved 05/05/2015 13:17:06. --->
PROCEDURE PRTALIASSELCSP(
 p_userName VARCHAR2 := NULL,
 p_PrtId NUMBER := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 OPEN p_SelCur FOR 
 SELECT p.PrtId,
 pa.ValidPrtAliasTyp,
 pa.PrtAliasNm
 FROM PrtTbl p, PrtAliasTbl pa 
 WHERE p.PrtId = pa.PrtId 
 AND p.PrtId = p_PrtId;
 END PRTALIASSELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:13. --->
PROCEDURE VALIDPRTTRANSTYPSELTSP
 (
 p_Identifier NUMBER:=0,
 P_RetVal OUT NUMBER,
 P_ValidPrtTransTyp VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 
 SAVEPOINT ValidPrtTransTypSel;
 
 IF p_Identifier= 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 
 SELECT 
 ValidPrtTransTyp, 
 ValidPrtTransTypDispOrdNmb, 
 ValidPrtTransTypDesc
 FROM ValidPrtTransTypTbl 
 WHERE ValidPrtTransTyp = p_ValidPrtTransTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier= 2 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 1
 FROM ValidPrtTransTypTbl 
 WHERE ValidPrtTransTyp = p_ValidPrtTransTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTTRANSTYPSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


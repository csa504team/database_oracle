<!--- Saved 05/05/2015 13:18:28. --->
PROCEDURE PRTLOCHISTMCCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCHISTMC; 
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm,
 r.LocHistryLocNm,
 a.LocHistryEffDt
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = 'MC' 
 AND a.LocHistryEffDt >= p_ReportBegDateTime 
 AND a.LocHistryEffDt <= p_ReportEndDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND a.LocHistryEffDt = r.LocHistryEffDt 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = 'MC');
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCHISTMC;
 RAISE;
 END PRTLOCHISTMCCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


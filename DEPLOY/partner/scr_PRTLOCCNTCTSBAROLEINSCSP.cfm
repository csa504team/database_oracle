<!--- Saved 01/25/2018 10:37:57. --->
procedure PRTLOCCNTCTSBAROLEINSCSP
 (
 p_LocId NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctSBARoleCreatUserId CHAR := NULL,
 p_PrtCntctFirstNm VARCHAR2 := NULL,
 p_PrtCntctJobTitlNm VARCHAR2 := NULL,
 p_PrtCntctLastNm VARCHAR2 := NULL,
 p_PrtCntctInitialNm VARCHAR2 := NULL,
 p_PrtCntctPrefixNm VARCHAR2 := NULL,
 p_PrtCntctSfxNm VARCHAR2 := NULL,
 p_PrtLocCntctMailStopNm VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := 0,
 p_INST_ID NUMBER := 0,
 p_LOC_ID NUMBER := 0,
 p_PrtCntctCreatUserId VARCHAR2 := NULL,
 p_PrtCntctNmb OUT NUMBER
 )
 as
 /*
 RY--01/24/2018--OPSMDEV-1335:: Removed the references of PrtCntctDataSourcId
 */
 v_temp number := 0;
 begin
 savepoint PRTLOCCNTCTSBAROLEINS;
 select count(*) into v_temp
 from PRTLOCCNTCTSBAROLETBL 
 where LocId = p_LocId
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 
 if v_temp > 0 then
 begin
 UPDATE PrtLocCntctTbl
 SET PrtCntctFirstNm=p_PrtCntctFirstNm,
 PrtCntctJobTitlNm=p_PrtCntctJobTitlNm,
 PrtCntctLastNm=p_PrtCntctLastNm,
 PrtCntctInitialNm=p_PrtCntctInitialNm,
 PrtCntctPrefixNm=p_PrtCntctPrefixNm,
 PrtCntctSfxNm=p_PrtCntctSfxNm,
 PrtLocCntctMailStopNm=p_PrtLocCntctMailStopNm,
 PERSON_ID=p_PERSON_ID,
 INST_ID=p_INST_ID,
 LOC_ID=p_LOC_ID,
 LASTUPDTUSERID= p_PrtCntctCreatUserId,
 LASTUPDTDT = sysdate
 WHERE LocId=p_LocId
 and PrtCntctNmb=(select max(PrtCntctNmb)
 from PRTLOCCNTCTSBAROLETBL 
 where LocId = p_LocId
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd);
 end;
 else
 begin
 INSERT INTO PARTNER.PrtLocCntctTbl 
 (LocId, PrtCntctNmb, PrtCntctFirstNm,PrtCntctJobTitlNm, PrtCntctLastNm,
 PrtCntctInitialNm, PrtCntctPrefixNm, PrtCntctSfxNm,PrtLocCntctMailStopNm, PERSON_ID,INST_ID,
 LOC_ID, PrtCntctCreatUserId,LASTUPDTUSERID,LASTUPDTDT)
 select p_LocId,nvl(max(PrtCntctNmb),0)+1, p_PrtCntctFirstNm,p_PrtCntctJobTitlNm,
 p_PrtCntctLastNm,p_PrtCntctInitialNm,p_PrtCntctPrefixNm,p_PrtCntctSfxNm,
 p_PrtLocCntctMailStopNm,p_PERSON_ID,p_INST_ID,p_LOC_ID,p_PrtCntctCreatUserId,
 p_PrtCntctCreatUserId,SYSDATE
 from partner.PrtLocCntctTbl
 where LocId = p_LocId;
 
 INSERT INTO PARTNER.PrtLocCntctSBARoleTbl(LocId,PrtCntctNmb,PrtCntctSBARoleTypCd,PrtCntctSBARoleCreatUserId,LASTUPDTUSERID,LASTUPDTDT)
 select p_LocId,max(PrtCntctNmb),p_PrtCntctSBARoleTypCd,p_PrtCntctSBARoleCreatUserId,p_PrtCntctSBARoleCreatUserId,SYSDATE
 from PrtLocCntctTbl
 where LocId = p_LocId;
 
 select max(PrtCntctNmb) INTO p_PrtCntctNmb
 from PrtLocCntctTbl
 where LocId = p_LocId;
 
 end;
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCCNTCTSBAROLEINS;
 RAISE;
 END PRTLOCCNTCTSBAROLEINSCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


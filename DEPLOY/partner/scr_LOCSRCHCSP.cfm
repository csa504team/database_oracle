<!--- Saved 05/05/2016 11:04:32. --->
PROCEDURE locsrchcsp
 ( 
 
 p_LocType varchar2 := NULL,
 p_subcategoryTyp varchar2 := null, 
 p_cityNm varchar2 := null, 
 p_citySrchInd char := null,
 p_stateCd varchar2 := null, 
 p_zipCd varchar2 := null,
 p_zipSrchInd char := null,
 p_locNmsrchind char := null,
 p_curStatCd varchar2 := null,
 p_prtlocNm varchar2 := null, 
 p_officeCd varchar2 := null, 
 p_regionCd varchar2 := null, 
 p_cntyCd varchar2 := null, 
 p_congDistCd varchar2 := null, 
 p_programId varchar2 := null, 
 p_prgrmStatCd varchar2 := null,
 p_queryTyp varchar2 := null,
 p_rowCount number := 50, 
 p_stateCity varchar2 := null,
 p_selcur out sys_refcursor,
 p_retval out number
 
 )
 AS
 v_identifier number(2);
 v_locNmSrchInd char(1);
 v_cityNm varchar2(80);
 v_zipCd varchar2(10);
 v_prtlocNm varchar(80);
 
 BEGIN
 
 if p_programId is null then
 if lower(p_queryTyp) = 'next' then
 v_identifier := 12;
 else
 v_identifier := 0;
 end if;
 else 
 v_identifier := 11;
 end if;
 
 v_locNmSrchInd := p_locNmSrchInd ;
 
 if upper(substr(p_PrtLocNm,1,3)) = 'THE' then
 v_PrtLocNm := upper(substr(p_PrtLocNm,1,4)); 
 v_locNmSrchInd := 'C'; 
 else
 v_PrtLocNm := p_PrtLocNm; 
 end if; 
 
 if v_locNmSrchInd = 'C' then
 v_PrtLocNm := '%'|| upper(v_PrtLocNm) ||'%';
 elsif v_locNmSrchInd = 'S' then
 v_PrtLocNm := upper(v_PrtLocNm) ||'%';
 elsif v_locNmSrchInd = 'E' then
 v_PrtLocNm := upper(v_PrtLocNm);
 else
 v_PrtLocNm := upper(v_PrtLocNm);
 end if; 
 
 v_cityNm := p_cityNm;
 
 if p_citySrchInd = 'C' then
 v_cityNm := '%'|| upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'S' then
 v_cityNm := upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'E' then
 v_cityNm := upper(v_cityNm);
 else
 v_cityNm := upper(v_cityNm);
 end if; 
 
 v_zipCd := p_zipCd;
 
 
 if p_zipSrchInd = 'C' then
 v_zipCd := '%'|| v_zipCd ||'%';
 elsif p_zipSrchInd = 'S' then
 v_zipCd := v_zipCd ||'%';
 elsif p_zipSrchInd = 'E' then
 v_zipCd := v_zipCd;
 else
 v_zipCd := p_zipCd;
 end if;
 
 if v_identifier = 0 then
 
 BEGIN
 open p_selcur for
 SELECT rownum,
 l.PrtLocNm ,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt, 
 l.PhyAddrCtyNm, 
 l.PhyAddrStCd , 
 l.PhyAddrPostCd, 
 l.ValidPrtSubCatTyp, 
 l.ValidLocTypDispOrdNmb,
 l.LocId, 
 l.PrtLocSrchNm ,
 l.PrtId,
 l.PhyAddrCtySrchNm,
 CASE WHEN l.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ'
 ELSE l.PhyAddrStCd|| l.PhyAddrCtySrchNm||Upper(l.PhyAddrStr1Txt)||to_char(l.LocId) END AS StateCity 
 FROM partner.LocSrchTbl l
 WHERE l.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,l.ValidPrtSubCatTyp)
 AND l.ValidLocTyp like nvl ( p_LocType,l.ValidLocTyp)
 AND l.PrtLocSrchNm like nvl(v_PrtLocNm,l.PrtLocSrchNm)
 AND l.PhyAddrCtySrchNm like nvl(v_cityNm,l.PhyAddrCtySrchNm)
 AND l.PhyAddrStCd IN nvl(p_stateCd,l.PhyAddrStCd)
 AND l.PhyAddrPostCd like nvl(v_zipCd,l.PhyAddrPostCd)
 AND l.OrignOfcCd like NVL(p_officeCd ,l.OrignOfcCd)
 AND l.OrignOfcCd like nvl(p_regionCd||'%',l.OrignOfcCd)
 AND CASE WHEN length(p_cntyCd) = 1 AND p_cntyCd in ('0','1','2','3','4','5','6','7','8','9') AND l.CntyCd = '00'||p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) between 1 and 3 AND l.CntyCd = p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) > 3 AND l.CNTYNM LIKE p_CntyCd||'%'
 THEN 1
 WHEN p_CntyCd is null and l.CntyCd = l.CntyCd
 THEN 1
 ELSE 0
 END = 1
 AND l.CongrsnlDistNmb = NVL(P_congDistCd, l.CongrsnlDistNmb)
 AND l.PrtLocCurStatCd = NVL( P_curStatCd , l.PrtLocCurStatCd)
 AND ROWNUM <= p_RowCount
 ORDER BY l.PrtLocSrchNm , StateCity;
 
 end; 
 
 elsif v_identifier = 11 then 
 
 begin 
 open p_selcur for
 SELECT rownum,l.PrtLocNm ,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt, 
 l.PhyAddrCtyNm, 
 l.PhyAddrStCd , 
 l.PhyAddrPostCd, 
 l.ValidPrtSubCatTyp, 
 l.ValidLocTypDispOrdNmb,
 l.LocId,
 l.PrtLocSrchNm ,
 l.PrtId,
 l.PhyAddrCtySrchNm,
 
 CASE WHEN l.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ'
 ELSE l.PhyAddrStCd|| l.PhyAddrCtySrchNm||Upper(l.PhyAddrStr1Txt)||to_char(l.LocId) END AS StateCity 
 FROM partner.LocSrchTbl l,PARTNER.PrtAgrmtTbl a
 WHERE l.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,l.ValidPrtSubCatTyp)
 AND l.ValidLocTyp like nvl ( p_LocType,l.ValidLocTyp)
 AND l.PrtLocSrchNm like nvl(v_PrtLocNm,l.PrtLocSrchNm)
 AND l.PhyAddrCtySrchNm like nvl(v_cityNm,l.PhyAddrCtySrchNm)
 AND l.PhyAddrStCd IN nvl(p_stateCd,l.PhyAddrStCd)
 AND l.PhyAddrPostCd like nvl(v_zipCd,l.PhyAddrPostCd)
 AND l.OrignOfcCd like NVL(p_officeCd ,l.OrignOfcCd)
 AND l.OrignOfcCd like nvl(p_regionCd||'%',l.OrignOfcCd)
 AND CASE WHEN length(p_cntyCd) = 1 AND p_cntyCd in ('0','1','2','3','4','5','6','7','8','9') AND l.CntyCd = '00'||p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) between 1 and 3 AND l.CntyCd = p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) > 3 AND l.CNTYNM LIKE p_CntyCd||'%'
 THEN 1
 WHEN p_CntyCd is null and l.CntyCd = l.CntyCd
 THEN 1
 ELSE 0
 END = 1
 AND l.CongrsnlDistNmb = NVL(p_congDistCd, l.CongrsnlDistNmb)
 AND l.PrtLocCurStatCd = NVL( p_curStatCd , l.PrtLocCurStatCd)
 AND l.PrtId = a.PrtId
 AND CASE WHEN p_prgrmStatCd = 'A' AND a.PrgrmId IN (p_programId)
 AND a.PrtAgrmtEffDt<=sysdate AND ((a.PrtAgrmtExprDt >= sysdate OR a.PrtAgrmtExprDt is NULL)
 AND (a.PrtAgrmtTermDt >sysdate OR a.PrtAgrmtTermDt is NULL)) 
 THEN 1
 WHEN p_prgrmStatCd = 'I' AND a.PrgrmId IN (p_programId)
 AND (a.PrtAgrmtExprDt < sysdate 
 AND a.PrtAgrmtTermDt is NULL 
 AND a.PrtAgrmtSeqNmb=(Select Max(b.PrtAgrmtSeqNmb) from partner.PrtAgrmtTbl b WHERE a.PrtId = b.PrtId AND b.PrgrmId IN (p_programId))
 )
 THEN 1
 WHEN p_prgrmStatCd = 'T' AND a.PrgrmId IN (p_programId) 
 AND a.PrtAgrmtTermDt <=sysdate
 THEN 1
 WHEN p_prgrmStatCd = 'P' AND a.PrgrmId IN (p_programId) 
 AND a.PrtAgrmtEffDt > sysdate
 THEN 1 
 WHEN (p_prgrmStatCd IS NULL OR p_prgrmStatCd NOT IN('A','I','T','P')) AND a.PrgrmId IN(p_programId) 
 THEN 1
 ELSE 0
 END = 1
 
 AND ROWNUM <= p_RowCount
 ORDER BY l.PrtLocSrchNm , StateCity;
 end;
 
 
 elsif v_identifier = 12 then 
 
 begin 
 open p_selcur for
 SELECT rownum,l.PrtLocNm ,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt, 
 l.PhyAddrCtyNm, 
 l.PhyAddrStCd , 
 l.PhyAddrPostCd, 
 l.ValidPrtSubCatTyp, 
 l.ValidLocTypDispOrdNmb,
 l.LocId,
 l.PrtLocSrchNm ,
 l.PrtId,
 l.PhyAddrCtySrchNm,
 CASE WHEN l.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ'
 ELSE l.PhyAddrStCd|| l.PhyAddrCtySrchNm||Upper(l.PhyAddrStr1Txt)||to_char(l.LocId) END AS StateCity 
 FROM partner.LocSrchTbl l
 WHERE l.PrtLocSrchNm >= upper(p_PrtLocNm)
 AND (CASE WHEN l.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ' 
 ELSE l.PhyAddrStCd||l.PhyAddrCtySrchNm||Upper(l.PhyAddrStr1Txt)||to_char(l.LocId)
 END ) >= upper(p_stateCity)
 ORDER BY l.PrtLocSrchNm,StateCity;
 end;
 end if;
 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


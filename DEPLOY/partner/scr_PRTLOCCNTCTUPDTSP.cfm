<!--- Saved 01/23/2018 15:32:02. --->
PROCEDURE PRTLOCCNTCTUPDTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctFirstNm VARCHAR2 := NULL,
 p_PrtCntctJobTitlNm VARCHAR2 := NULL,
 p_PrtCntctLastNm VARCHAR2 := NULL,
 p_PrtCntctInitialNm VARCHAR2 := NULL,
 p_PrtCntctPrefixNm VARCHAR2 := NULL,
 p_PrtCntctSfxNm VARCHAR2 := NULL,
 p_PrtLocCntctMailStopNm VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := 0,
 p_INST_ID NUMBER := 0,
 p_LOC_ID NUMBER := 0,
 p_PrtCntctCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtCntctDataSourcId
 */
 p_error NUMBER (10, 0);
 p_tran_flag NUMBER (10, 0);
 NewDataRec PrtLocCntctTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtLocCntctUpd;
 
 IF p_Identifier = 0
 THEN
 /* Update PrtLocCntctTbl Table */
 BEGIN
 UPDATE PrtLocCntctTbl
 SET PrtCntctFirstNm = p_PrtCntctFirstNm,
 PrtCntctJobTitlNm = p_PrtCntctJobTitlNm,
 PrtCntctLastNm = p_PrtCntctLastNm,
 PrtCntctInitialNm = p_PrtCntctInitialNm,
 PrtCntctPrefixNm = p_PrtCntctPrefixNm,
 PrtCntctSfxNm = p_PrtCntctSfxNm,
 PrtLocCntctMailStopNm = p_PrtLocCntctMailStopNm,
 PERSON_ID = p_PERSON_ID,
 INST_ID = p_INST_ID,
 LOC_ID = p_LOC_ID,
 LASTUPDTUSERID = p_PrtCntctCreatUserId,
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtLocCntctTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 END;
 END IF;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctInsUpdTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtLocCntctUpd;
 RAISE;
 END PRTLOCCNTCTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


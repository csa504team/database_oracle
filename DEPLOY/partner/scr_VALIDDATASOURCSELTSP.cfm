<!--- Saved 01/23/2018 15:32:05. --->
PROCEDURE VALIDDATASOURCSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT ValidDataSourcSel;
 IF p_Identifier= 4 THEN
 /* Select Code and Description Text*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT ValidDataSourcDescTxt
 FROM ValidDataSourcTbl;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDDATASOURCSELTSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:12. --->
PROCEDURE VALIDPRTSUBCATUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtSubCatTyp VARCHAR2:=NULL,
 p_ValidPrtSubCatDispOrdNmb NUMBER:=0,
 p_ValidPrtSubCatDesc VARCHAR2:=NULL,
 p_ValidSBAPrtPrimCatTyp VARCHAR2:=NULL,
 p_RolePrivilegePrgrmId VARCHAR2:=NULL,
 p_ValidPrtSubCatFIRSInd CHAR:=NULL,
 p_ValidPrtSubCatCreatUserId VARCHAR2:=NULL
 --,
 -- p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtSubCatUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 UPDATE ValidPrtSubCatTbl
 SET 
 ValidPrtSubCatDispOrdNmb =p_ValidPrtSubCatDispOrdNmb, 
 ValidPrtSubCatDesc =p_ValidPrtSubCatDesc, 
 ValidSBAPrtPrimCatTyp =p_ValidSBAPrtPrimCatTyp, 
 RolePrivilegePrgrmId =p_RolePrivilegePrgrmId, 
 ValidPrtSubCatFIRSInd =p_ValidPrtSubCatFIRSInd, 
 ValidPrtSubCatCreatUserId =p_ValidPrtSubCatCreatUserId,
 ValidPrtSubCatCreatDt =SYSDATE
 WHERE ValidPrtSubCatTyp = p_ValidPrtSubCatTyp;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF ;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTSUBCATUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


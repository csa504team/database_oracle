<!--- Saved 01/23/2018 15:31:57. --->
PROCEDURE GETPRTHISTRYCSP 
 (
 p_PrtId Number := NULL,
 p_StrtDt Date := NULL,
 p_EndDt Date := NULL,
 p_ChngCd VARCHAR2 :=Null,
 p_SrchTyp VARCHAR2 :=Null,
 p_ChngOrdNmb Number :=Null,
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS 
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 
 IF lower(p_SrchTyp) = 'bydate' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT p.ValidChngCd, v.ValidChngCdDispOrdNmb , v.ValidChngCdDesc,p.PrtId as InitiatorPrtId,
 count( distinct p.PrtId) as PrtNum 
 FROM partner.PrtHistryTbl p , partner.ValidChngCdTbl v
 WHERE trunc(p.PrtHistryCreatDt) like trunc(sysdate) --(substr(sysdate,1,11)||'%') 
 AND v.ValidChngCd = p.ValidChngCd
 GROUP BY p.ValidChngCd ,v.ValidChngCdDesc,p.PrtId ,v.ValidChngCdDispOrdNmb
 ORDER BY p.ValidChngCd;
 END; 
 END IF;
 
 
 IF lower(p_SrchTyp) = 'bychngcd' THEN
 IF p_chngOrdNmb != 0 THEN
 IF P_ChngOrdNmb in (1,2,3,4) THEN
 
 partner.PrtHistAIRRecrdCSP(p_ReportBegDateTime,p_ReportEndDateTime,p_ValidChngCd,p_PrtId,p_SelCur);
 
 END IF;
 END IF;
 IF p_ChngOrdNmb = 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtHistryLglNm as AffectedTitle, 
 p.PrtHistryEffDt as EffectiveDate,
 p.PrtHistryCreatDt as DateAddedToPIMS ,
 p.ValidChngCd 
 FROM partner.PrtHistryTbl p 
 WHERE TRUNC(p.PrtHistryCreatDt) like TRUNC(sysdate);--(substr(sysdate,1,11)||'%');
 END;
 END IF;
 END IF;
 IF lower(p_srchTyp) ='byprtid' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT P.ValidChngCd , v.ValidChngCdDesc,p.PrtId as InitiatorPrtId,
 v.ValidChngCdDesc,
 p.PrtHistryLglNm,
 p.PrtHistryPartyTyp, 
 p.PrtHistryEffDt as EffectiveDate,
 p.PrtHistryInitPrtId,
 p.PrtHistryResultingPrtId 
 FROM partner.PrtHistryTbl p , partner.ValidChngCdTbl v 
 WHERE p.PrtId = p_PrtId 
 AND v.ValidChngCd = p.ValidChngCd
 AND p.PrtHistryCreatDt >= p_StrtDt 
 AND p.PrtHistryCreatDt <= p_EndDt 
 ORDER BY p.PrtHistryEffDt;
 END;
 END IF;
 
 IF lower(p_SrchTyp) = 'forpartnerhistory' THEN
 
 BEGIN
 OPEN p_SelCur FOR
 
 SELECT i.PrtId as InitiatorPrtId,
 i.PrtHistryLglNm as InitiatorTitle,
 v.ValidChngCdDesc,
 a.PrtId as AffectedPrtId,
 a.PrtHistryLglNm as AffectedTitle,
 r.PrtId as ResultingPrtId,
 r.PrtHistryLglNm as ResultingTitle,
 a.PrtHistryEffDt as EffectiveDate,--To_char(a.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 a.ValidChngCd,
 v.ValidChngCdDispOrdNmb,
 a.PrtHistryCreatDt as DateAddedToPIMS, 
 a.PrtHistryChrtrTyp as AffectedCharter,
 r.PrtHistryChrtrTyp as ResultingCharter,
 a.PrtHistryReglAgencyNm as AffectedRegulator ,
 i.PrtHistryReglAgencyNm as InitiatorRegulator,
 r.PrtHistryReglAgencyNm as ResultingRegulator 
 FROM partner.PrtHistryTbl a 
 LEFT OUTER JOIN partner.PrtHistryTbl i ON a.PrtId = i.PrtId 
 AND a.PrtHistryEffDt=i.PrtHistryEffDt 
 AND i.PrtHistryPartyTyp = 'I' 
 AND a.ValidChngCd = i.ValidChngCd 
 LEFT OUTER JOIN partner.PrtHistryTbl r ON a.PrtId = r.PrtId 
 AND a.PrtHistryEffDt=r.PrtHistryEffDt 
 AND r.PrtHistryPartyTyp = 'R' 
 AND a.ValidChngCd = r.ValidChngCd
 and a.ValidChngCd= NVL(p_ChngCd,a.ValidChngCd) 
 LEFT OUTER JOIN partner.ValidChngCdTbl v ON a.ValidChngCd =v.ValidChngCd
 WHERE a.PrtHistryPartyTyp = 'A'
 and a.prtId = NVL(p_prtId,a.prtId)
 AND TRUNC(a.PrtHistryEffDt ) >=TRUNC( NVL(p_StrtDt,a.PrtHistryEffDt))
 AND TRUNC(a.PrtHistryEffDt ) <=TRUNC(NVL(p_EndDt,a.PrtHistryEffDt))
 
 ORDER BY a.PrtHistryEffDt DESC;
 
 END;
 
 END IF;
 
 
 
 
 IF lower(p_SrchTyp) = 'editpartnerhistory' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.ValidChngCd , 
 v.ValidChngCdDesc,
 p.PrtId,
 p.PrtHistryEffDt,
 p.ValidChngCd,
 p.PrtHistryPartyTyp,
 p.PrtHistryInitPrtId,
 p.PrtHistryResultingPrtId, 
 p.PrtHistryChrtrTyp,
 p.PrtHistryEstbDt,
 p.PrtHistryLglNm,
 p.PrtHistryPrimTyp,
 p.PrtHistrySubCatTyp,
 p.PrtHistryReglAgencyNm
 FROM partner.PrtHistryTbl p , partner.ValidChngCdTbl v
 WHERE v.ValidChngCd = p.ValidChngCd 
 and p.PrtId = p_prtId
 AND p.PrtHistryEffDt = p_StrtDt
 AND p.ValidChngCd = p_ChngCd;
 END;
 END IF;
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:32:06. --->
PROCEDURE VALIDDATASOURCUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidDataSourcDescTxt VARCHAR2 :=NULL,
 p_ValidDataSourcCreatUserId VARCHAR2 :=NULL
 )
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT ValidDataSourcUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 
 UPDATE ValidDataSourcTbl
 SET 
 ValidDataSourcDescTxt = p_ValidDataSourcDescTxt, 
 ValidDataSourcCreatUserId = p_ValidDataSourcCreatUserId, 
 ValidDataSourcCreatDt = SYSDATE;
 -- Where ValidDataSourcCd = p_ValidDataSourcCd;
 
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDDATASOURCUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:29. --->
PROCEDURE PRTCNTCTSBAROLETYPINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctSBARoleTypTxt VARCHAR2 := NULL,
 p_PrtSBARoleTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PRTCNTCTSBAROLETYPINS;
 IF p_Identifier = 0 THEN
 /* Insert into PrtCntctSBARoleTyp Table */
 BEGIN
 INSERT INTO PARTNER.PrtCntctSBARoleTypTbl 
 (PrtCntctSBARoleTypCd,PrtCntctSBARoleTypTxt,PrtSBARoleTypCreatUserId,PrtSBARoleTypCreatDt)
 VALUES (p_PrtCntctSBARoleTypCd,p_PrtCntctSBARoleTypTxt,p_PrtSBARoleTypCreatUserId,SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTCNTCTSBAROLETYPINS;
 RAISE;
 
 END PRTCNTCTSBAROLETYPINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:16. --->
PROCEDURE VALIDWBCCLASSUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidWBCClassCd NUMBER:=0,
 p_ValidWBCClassDescTxt VARCHAR2:=NULL,
 p_ValidWBCClassCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 SAVEPOINT ValidWBCClassUpd;
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE ValidWBCClassTbl
 SET 
 ValidWBCClassCd = p_ValidWBCClassCd, 
 ValidWBCClassDescTxt = p_ValidWBCClassDescTxt, 
 ValidWBCClassCreatUserId = p_ValidWBCClassCreatUserId,
 ValidWBCClassCreatDt = SYSDATE
 where ValidWBCClassCd = p_ValidWBCClassCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 END IF ;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDWBCCLASSUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


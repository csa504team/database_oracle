<!--- Saved 01/25/2018 10:37:55. --->
PROCEDURE PRTALIASUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_LocId NUMBER := 0,
 p_PrtAliasCreatUserId IN VARCHAR2 := NULL)
 AS
 /*SB -- 10/17/2014 Modified code for TIN's Validation
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 v_PrtId number(11);
 v_SelCur SYS_REFCURSOR;
 v_ErrSeqNmb NUMBER;
 BEGIN
 SAVEPOINT PrtAliasUpd;
 
 IF p_ValidPrtAliasTyp = 'TIN'
 THEN
 BEGIN
 PARTNER.VALIDATEPRTALIASCSP (p_PrtId, p_ValidPrtAliasTyp, p_PrtAliasNm, p_PRTALIASCREATUSERID, v_ErrSeqNmb, v_SelCur);
 IF v_ErrSeqNmb > 0
 THEN
 raise_application_error(-20001,'Invalid TIN:'||p_PrtAliasNm);
 RETURN;
 END IF;
 END;
 END IF;
 
 IF p_Identifier = 0 THEN
 /* Update PhyAddrTbl Table */
 BEGIN
 UPDATE PrtAliasTbl
 SET PrtId = p_PrtId, 
 ValidPrtAliasTyp = p_ValidPrtAliasTyp, 
 PrtAliasNm = p_PrtAliasNm, 
 LASTUPDTUSERID = p_PrtAliasCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = p_PrtId
 AND ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 --call trigger
 IF SQLCODE != 0
 THEN
 BEGIN
 PrtAliasInsUpdTrigCSP (p_PrtId, p_ValidPrtAliasTyp);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 IF p_Identifier = 11 THEN
 /* Update PhyAddrTbl Table */
 BEGIN
 UPDATE PrtAliasTbl
 SET ValidPrtAliasTyp = p_ValidPrtAliasTyp, 
 PrtAliasNm = p_PrtAliasNm, 
 LASTUPDTUSERID = p_PrtAliasCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = (select PrtId from PARTNER.PrtLocTbl where LocId = p_LocId)
 AND ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 --call trigger
 IF SQLCODE != 0
 THEN
 BEGIN
 begin
 select PrtId into v_PrtId from PARTNER.PrtLocTbl where LocId = p_LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtId := NULL; END;
 PrtAliasInsUpdTrigCSP (v_PrtId, p_ValidPrtAliasTyp);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAliasUpd;
 RAISE;
 
 END PRTALIASUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:40. --->
PROCEDURE PIMSLENDERDATACSP
 (
 v_PrtLocFIRSNmb IN CHAR DEFAULT NULL ,
 cv_1 IN OUT SYS_REFCURSOR,
 cv_2 IN OUT SYS_REFCURSOR
 )
 AS
 v_MainLocId NUMBER(7);
 v_Owr3LocId NUMBER(7);
 v_Owr4LocId NUMBER(7);
 v_Owr5LocId NUMBER(7);
 v_PrtId NUMBER(7);
 v_LocId NUMBER(7);
 v_MainPrtId NUMBER(7);
 v_OwningPrtId NUMBER(7);
 v_ValidLocTyp VARCHAR2(5);
 BEGIN
 SELECT ValidLocTyp,
 PrtId,
 LocId,
 PrtId
 INTO v_ValidLocTyp,
 v_PrtId,
 v_LocId,
 v_MainPrtId
 FROM PrtLocTbl 
 WHERE PrtLocFIRSNmb = v_PrtLocFIRSNmb;
 IF v_ValidLocTyp NOT IN ( 'HO','TRHO','RHO','SAVHO','INTHO' ) THEN
 SELECT LocId
 INTO v_MainLocId
 FROM PrtLocTbl 
 WHERE PrtId = v_PrtId
 AND ValidLocTyp IN ( 'HO','TRHO','RHO','SAVHO','INTHO' );
 ELSE
 v_MainLocId := v_LocId ;
 END IF;
 v_OwningPrtId := NULL ;
 SELECT OwningPrtId
 INTO v_OwningPrtId
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId
 AND NVL(PrtOwnrshpPct, 100) = ( SELECT NVL(MAX(PrtOwnrshpPct), 100)
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId );
 IF v_OwningPrtId IS NULL THEN
 v_Owr3LocId := v_MainLocId ;
 ELSE
 SELECT LocId
 INTO v_Owr3LocId
 FROM PrtLocTbl 
 WHERE PrtId = v_OwningPrtId
 AND ValidLocTyp IN ( 'HO','TRHO','RHO','SAVHO','INTHO' );
 END IF;
 IF v_OwningPrtId IS NOT NULL THEN
 
 BEGIN
 v_PrtId := v_OwningPrtId ;
 v_OwningPrtId := NULL ;
 SELECT OwningPrtId
 INTO v_OwningPrtId
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId
 AND NVL(PrtOwnrshpPct, 100) = ( SELECT NVL(MAX(PrtOwnrshpPct), 100)
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId );
 END;
 END IF;
 IF v_OwningPrtId IS NULL THEN
 v_Owr4LocId := v_Owr3LocId ;
 ELSE
 SELECT LocId
 INTO v_Owr4LocId
 FROM PrtLocTbl 
 WHERE PrtId = v_OwningPrtId
 AND ValidLocTyp IN ( 'HO','TRHO','RHO','SAVHO','INTHO' );
 END IF;
 IF v_OwningPrtId IS NOT NULL THEN
 
 BEGIN
 v_PrtId := v_OwningPrtId ;
 v_OwningPrtId := NULL ;
 SELECT OwningPrtId
 INTO v_OwningPrtId
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId
 AND NVL(PrtOwnrshpPct, 100) = ( SELECT NVL(MAX(PrtOwnrshpPct), 100)
 FROM PrtOwnrshpTbl 
 WHERE PrtId = v_PrtId );
 END;
 END IF;
 IF v_OwningPrtId IS NULL THEN
 v_Owr5LocId := v_Owr4LocId ;
 ELSE
 SELECT LocId
 INTO v_Owr5LocId
 FROM PrtLocTbl 
 WHERE PrtId = v_OwningPrtId
 AND ValidLocTyp IN ( 'HO','TRHO','RHO','SAVHO','INTHO' );
 END IF;
 OPEN cv_1 FOR
 SELECT t1.PrtLocFIRSNmb "L1FIRS",
 SUBSTR('0000000' || CAST(t1.LocId AS VARCHAR2(7)), -1, 7) "L1PIMS",
 SUBSTR(t1.PrtLocNm, 1, 30) "L1NAME",
 SUBSTR(p1.PhyAddrCtyNm, 1, 30) "L1CITY",
 SUBSTR(p1.PhyAddrStCd, 1, 2) "L1STATE",
 t2.PrtLocFIRSNmb "L2FIRS",
 SUBSTR('0000000' || CAST(t2.LocId AS VARCHAR2(7)), -1, 7) "L2PIMS",
 SUBSTR(t2.PrtLocNm, 1, 30) "L2NAME",
 SUBSTR(p2.PhyAddrStr1Txt, 1, 30) "L2STREET",
 SUBSTR(p2.PhyAddrCtyNm, 1, 30) "L2CITY",
 SUBSTR(p2.PhyAddrStCd, 1, 2) "L2STATE",
 SUBSTR(p2.PhyAddrPostCd, 1, 5) "L2ZIP",
 SUBSTR(p2.PhyAddrFIPSCntyCd, 3, 3) "L2COUNTY",
 OfcCd "L2DISTOFF",
 ( SELECT SUBSTR(PrtAliasNm, 1, 20)
 FROM PrtAliasTbl 
 WHERE PrtId = v_MainPrtId
 AND ValidPrtAliasTyp = 'FDIC' ) "L2FDIC",
 ( SELECT SUBSTR(PrtAliasNm, 1, 20)
 FROM PrtAliasTbl 
 WHERE PrtId = v_MainPrtId
 AND ValidPrtAliasTyp = 'NCUA' ) "L2NCUAN",
 SUBSTR('00000000' || CAST(t2.INST_ID AS VARCHAR2(8)), -1, 8) || SUBSTR('00000000' || CAST(t2.LOC_ID AS VARCHAR2(8)), -1, 8) "L2TOMP",
 ValidPrtPrimCatTyp "L2CAT",
 ValidPrtSubCatTyp "L2SCAT",
 PrtCurStatCd "L2CPST",
 'UNNWN' "L2PGMID"
 FROM PrtLocTbl t1,
 PhyAddrTbl p1,
 PrtLocTbl t2,
 PhyAddrTbl p2,
 PrtTbl a
 WHERE t1.LocId = v_LocId
 AND p1.LocId = v_LocId
 AND p1.ValidAddrTyp = 'Phys'
 AND t2.LocId = v_MainLocId
 AND p2.LocId = v_MainLocId
 AND p2.ValidAddrTyp = 'Phys'
 AND a.PrtId = v_MainPrtId;
 OPEN cv_2 FOR
 SELECT t3.PrtLocFIRSNmb "L3FIRS",
 SUBSTR('0000000' || CAST(t3.LocId AS VARCHAR2(7)), -1, 7) "L3PIMS",
 SUBSTR(t3.PrtLocNm, 1, 30) "L3NAME",
 SUBSTR(p3.PhyAddrCtyNm, 1, 30) "L3CITY",
 SUBSTR(p3.PhyAddrStCd, 1, 2) "L3STATE",
 t4.PrtLocFIRSNmb "L4FIRS",
 SUBSTR('0000000' || CAST(t4.LocId AS VARCHAR2(7)), -1, 7) "L4PIMS",
 SUBSTR(t4.PrtLocNm, 1, 30) "L4NAME",
 SUBSTR(p4.PhyAddrCtyNm, 1, 30) "L4CITY",
 SUBSTR(p4.PhyAddrStCd, 1, 2) "L4STATE",
 t5.PrtLocFIRSNmb "L5FIRS",
 SUBSTR('0000000' || CAST(t5.LocId AS VARCHAR2(7)), -1, 7) "L5PIMS",
 SUBSTR(t5.PrtLocNm, 1, 30) "L5NAME",
 SUBSTR(p5.PhyAddrCtyNm, 1, 30) "L5CITY",
 SUBSTR(p5.PhyAddrStCd, 1, 2) "L5STATE"
 FROM PrtLocTbl t3,
 PhyAddrTbl p3,
 PrtLocTbl t4,
 PhyAddrTbl p4,
 PrtLocTbl t5,
 PhyAddrTbl p5
 WHERE t3.LocId = v_Owr3LocId
 AND p3.LocId = v_Owr3LocId
 AND p3.ValidAddrTyp = 'Phys'
 AND t4.LocId = v_Owr4LocId
 AND p4.LocId = v_Owr4LocId
 AND p4.ValidAddrTyp = 'Phys'
 AND t5.LocId = v_Owr5LocId
 AND p5.LocId = v_Owr5LocId
 AND p5.ValidAddrTyp = 'Phys';
 End; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


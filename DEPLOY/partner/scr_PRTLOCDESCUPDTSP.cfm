<!--- Saved 05/05/2015 13:18:21. --->
PROCEDURE PRTLOCDESCUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtLocDescSeqNmb NUMBER := 0,
 p_PrtLocDescTxt VARCHAR2 := NULL,
 p_PrtLocDescCreatUserId VARCHAR2 := NULL)
 AS
 TranFlag NUMBER(10,0);
 error NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtLocDescUpd;
 IF p_Identifier = 0 THEN
 /* Update of PrtLocDesc Table */
 BEGIN
 UPDATE PARTNER.PrtLocDescTbl
 SET PrtLocDescTxt = p_PrtLocDescTxt, 
 lastupdtuserid = p_PrtLocDescCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb 
 and PrtLocDescSeqNmb = p_PrtLocDescSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescUpd;
 RAISE; 
 END PRTLOCDESCUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


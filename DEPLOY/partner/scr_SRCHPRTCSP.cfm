<!--- Saved 08/10/2016 15:55:46. --->
PROCEDURE SrchPrtCSP
 ( 
 --p_Identifier number := 0,
 p_PrtLglNm varchar2 :=Null,
 p_subcategoryTyp varchar2 := null, 
 p_cityNm varchar2 := null,
 p_citySrchInd char := null, 
 p_StCd varchar2 := null, 
 p_zipCd varchar2 := null,
 p_zipSrchInd char := null,
 p_lglNmsrchind char := null,
 p_RowCount number := 50,
 p_selcur out sys_refcursor,
 p_retval out number,
 p_queryTyp varchar2 := null,
 p_lglSrchNm varchar2 := null,
 p_PrtCurStatCd varchar2 := null
 )
 --NK--08/09/2016--OPSMDEV-1059-- Replacing JBOSS methods SearchPartner and SearchAllPartner
 As
 v_lglNmSrchInd char(1);
 v_cityNm varchar2(80);
 v_zipCd varchar2(10);
 v_PrtLglNm varchar2(80);
 v_identifier number(2);
 v_lglSrchNm Varchar2(80);
 BEGIN
 if lower(p_queryTyp) = 'next' then
 v_identifier := 11;
 else
 v_identifier := 0;
 end if;
 
 v_PrtLglNm := UPPER(p_PrtLglNm );
 v_LglNmSrchInd := p_LglNmSrchInd;
 v_lglSrchNm:=upper(p_lglSrchNm);
 
 if v_LglNmSrchInd = 'C' then
 v_PrtLglNm := '%'|| upper(v_PrtLglNm ) ||'%';
 elsif v_LglNmSrchInd = 'S' then
 v_PrtLglNm := upper(v_PrtLglNm ) ||'%';
 elsif v_LglNmSrchInd = 'E' then
 v_PrtLglNm := upper(v_PrtLglNm );
 else
 v_PrtLglNm := upper(v_PrtLglNm );
 end if ; 
 
 v_cityNm := p_cityNm;
 
 if p_citySrchInd = 'C' then
 v_cityNm := '%'|| upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'S' then
 v_cityNm := upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'E' then
 v_cityNm := upper(v_cityNm);
 else
 v_cityNm := upper(v_cityNm);
 end if; 
 
 v_zipCd := p_zipCd;
 
 
 if p_zipSrchInd = 'C' then
 v_zipCd := '%'|| v_zipCd ||'%';
 elsif p_zipSrchInd = 'S' then
 v_zipCd := v_zipCd ||'%';
 elsif p_zipSrchInd = 'E' then
 v_zipCd := v_zipCd;
 else
 v_zipCd := p_zipCd;
 end if;
 
 if v_identifier = 0 then
 
 BEGIN
 open p_selcur for 
 SELECT ROWNUM, 
 p.PrtId, 
 p.PrtLglNm, 
 p.ValidPrtPrimCatTyp, 
 p.PrtLglNm , 
 p.ValidLocTyp, 
 p.PhyAddrStr1Txt, 
 p.PhyAddrCtyNm, 
 p.PhyAddrStCd , 
 p.PhyAddrPostCd , 
 p.LocId ,
 p.PrtLocFIRSNmb ,
 p.LocQty , 
 p.PrtLglSrchNm 
 FROM partner.PrtSrchTbl p 
 WHERE p.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,p.ValidPrtSubCatTyp)
 AND p.PrtlglSrchNm like nvl(v_PrtLglNm ,p.PrtlglSrchNm)
 AND ( (v_cityNm is NULL) or (upper(p.PhyAddrCtySrchNm) like v_cityNm) )
 AND ( (p_StCd is NULL) or ( upper(p.PhyAddrStCd) like upper(p_StCd)||'%') )
 AND ( (v_zipCd is NULL) or (upper(p.PhyAddrPostCd) like v_zipCd) )
 /* AND case when p.PrtCurStatCd IN NVL(p_PrtCurStatCd,p.PrtCurStatCd) then 1--searchallprt
 when p.PrtCurStatCd = 'O' then 2
 else 0
 end */
 AND p.PrtCurStatCd = 'O'
 AND ROWNUM <= p_RowCount;
 
 
 end; 
 
 END IF; 
 
 if v_identifier = 11 then
 
 BEGIN
 open p_selcur for 
 SELECT ROWNUM, 
 p.PrtId, 
 p.PrtLglNm, 
 p.ValidPrtPrimCatTyp, 
 p.PrtLglNm , 
 p.ValidLocTyp, 
 p.PhyAddrStr1Txt, 
 p.PhyAddrCtyNm, 
 p.PhyAddrStCd , 
 p.PhyAddrPostCd , 
 p.LocId ,
 p.PrtLocFIRSNmb ,
 p.LocQty , 
 p.PrtLglSrchNm 
 FROM partner.PrtSrchTbl p 
 WHERE p.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,p.ValidPrtSubCatTyp)
 AND p.PrtlglSrchNm like nvl(v_PrtLglNm ,p.PrtlglSrchNm)
 AND ( (v_cityNm is NULL) or (upper(p.PhyAddrCtySrchNm) like v_cityNm) )
 AND ( (p_StCd is NULL) or ( upper(p.PhyAddrStCd) like upper(p_StCd)||'%') )
 AND ( (v_zipCd is NULL) or (upper(p.PhyAddrPostCd) like v_zipCd) )
 /* AND case when p.PrtCurStatCd IN NVL(p_PrtCurStatCd,p.PrtCurStatCd) then 1--searchallprt
 when p.PrtCurStatCd = 'O' then 1
 else 0
 end = 1*/
 AND p.PrtCurStatCd = 'O'
 AND p.PrtLglSrchNm > v_PrtLglNm
 AND ROWNUM <= p_RowCount;
 
 
 end; 
 
 END IF; 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


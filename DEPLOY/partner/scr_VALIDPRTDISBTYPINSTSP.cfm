<!--- Saved 05/05/2015 13:19:57. --->
PROCEDURE VALIDPRTDISBTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtDisbTyp VARCHAR2 :=NULL,
 p_ValidPrtDisbTypDesc VARCHAR2 :=NULL,
 p_ValidPrtDisbTypDispOrdNmb NUMBER:=0,
 p_ValidPrtDisbTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtDisbTypIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtDisbTypTbl 
 (
 ValidPrtDisbTyp, 
 ValidPrtDisbTypDesc, 
 ValidPrtDisbTypDispOrdNmb, 
 ValidPrtDisbTypCreatUserId
 )
 VALUES (
 p_ValidPrtDisbTyp, 
 p_ValidPrtDisbTypDesc, 
 p_ValidPrtDisbTypDispOrdNmb, 
 p_ValidPrtDisbTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDPRTDISBTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


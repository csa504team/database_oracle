<!--- Saved 05/05/2015 13:19:24. --->
PROCEDURE PUTROOTPARTNERINTOTREECSP(
 p_RootId NUMBER := NULL,
 p_ErrorNmb OUT NUMBER,
 p_SelCur out sys_refcursor )
 AS
 p_INST_ID NUMBER(8,0);
 p_LOC_ID NUMBER(8,0);
 p_ReplicationErrorMessageTxt VARCHAR2(255);
 BEGIN
 
 SAVEPOINT PUTROOTPARTNERINTOTRE;
 BEGIN
 
 p_INST_ID := 0;
 p_LOC_ID := 0;
 IF p_RootId IS NULL 
 THEN
 
 COMMIT;
 
 END IF;
 
 INSERT INTO PARTNER.PrtOwnrshpTreeTbl (Root, LevelNmb, PrtLglNm, PrtId, OwningPrtId, PrtOwnrshpPct)
 SELECT p_RootId, 0, p.PrtLglNm, p_RootId, NULL, NULL 
 FROM PrtTbl p WHERE p.PrtId = p_RootId;
 PARTNER.PutOwnedPartnersIntoTreeCSP(p_RootId,p_RootId, 0);
 IF p_ErrorNmb <> 0 
 THEN
 BEGIN
 p_ErrorNmb := 99999;
 p_ReplicationErrorMessageTxt := 'Error calling PutOwnedPartnersIntoTreeCSP.';
 p_INST_ID := 0;
 p_LOC_ID := 0;
 PARTNER.PrtReplicationErrInsCSP(p_INST_ID, p_LOC_ID,'PutRootPartnerIntoTreeCSP', p_ErrorNmb,p_ReplicationErrorMessageTxt, null);
 END;
 END IF; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PUTROOTPARTNERINTOTRE;
 END;
 
 END; 
 END PUTROOTPARTNERINTOTREECSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:31:57. --->
PROCEDURE ELCTRNCADDRUPDCSP (
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ElctrncAddrNmb NUMBER := 0,
 p_ValidElctrncAddrTyp VARCHAR2 := NULL,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_ElctrncAddrTxt VARCHAR2 := NULL,
 p_LOC_ID NUMBER := 0,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := 0,
 p_TYPE_CODE VARCHAR2 := NULL,
 p_RANK NUMBER := 0,
 p_INST_ID NUMBER := 0,
 p_ElctrncAddrCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ElctrncAddrDataSourcId 
 */
 v_temp NUMBER (10, 0) := 0;
 BEGIN
 --SAVEPOINT ELCTRNCADDRUPDCSP;
 SELECT COUNT (*)
 INTO v_temp
 FROM partner.ElctrncAddrTbl e, partner.PRTLOCCNTCTSBAROLETBL s
 WHERE e.LocId = s.LocId
 AND e.PrtCntctNmb = s.PrtCntctNmb
 AND s.PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd
 AND e.LocId = p_LocId
 AND e.ValidElctrncAddrTyp = p_ValidElctrncAddrTyp;
 
 IF v_temp > 0
 THEN
 BEGIN
 UPDATE PARTNER.ElctrncAddrTbl
 SET ElctrncAddrTxt = p_ElctrncAddrTxt,
 LASTUPDTUSERID = p_ElctrncAddrCreatUserId,
 LASTUPDTDT = SYSDATE
 WHERE LOCID = p_LocId
 AND ValidElctrncAddrTyp = p_ValidElctrncAddrTyp;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrInsUpdTrigCSP (p_LocId, p_ElctrncAddrCreatUserId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 INSERT INTO PARTNER.ElctrncAddrTbl (LocId,
 ElctrncAddrNmb,
 ValidElctrncAddrTyp,
 PrtCntctNmb,
 ElctrncAddrTxt,
 LOC_ID,
 DEPARTMENT,
 PERSON_ID,
 TYPE_CODE,
 RANK,
 INST_ID,
 lastupdtuserid,
 lastupdtdt)
 VALUES (
 p_LocId,
 NVL ( (SELECT MAX (ElctrncAddrNmb)
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId),
 0)
 + 1,
 p_ValidElctrncAddrTyp,
 (SELECT MAX (PrtCntctNmb)
 FROM PrtLocCntctSBARoleTbl
 WHERE LocId = p_LocId
 AND PrtCntctSbaRoleTypCd =
 p_PrtCntctSbaRoleTypCd),
 p_ElctrncAddrTxt,
 p_LOC_ID,
 p_DEPARTMENT,
 p_PERSON_ID,
 p_TYPE_CODE,
 p_RANK,
 p_INST_ID,
 p_ElctrncAddrCreatUserId,
 SYSDATE);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrInsUpdTrigCSP (p_LocId, p_ElctrncAddrCreatUserId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK;
 END ELCTRNCADDRUPDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


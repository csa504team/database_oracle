<!--- Saved 05/05/2015 13:18:54. --->
PROCEDURE PRTOTHINFOINSTSP(
 p_Identifier NUMBER := 0,
 p_PrtId NUMBER := null,
 p_PrtSBICLicnsNmb CHAR := null,
 p_PrtCDCNmb CHAR := null,
 p_PrtFmrNm varchar2 := null,
 p_PrtMicroLendrNmb char := null,
 p_PrtTaxId char := null,
 p_PrtTrdNm varchar2 := null,
 p_PrtFRSNmb number := null,
 p_PrtNCUANmb number := null,
 p_PrtFDICNmb number := null,
 p_PrtSCORENmb char := null,
 p_PrtDUNSNmb char := null,
 p_CreatUserId varchar2 := null
 )
 AS
 BEGIN
 IF p_Identifier = 0 THEN
 /* Insert into PrtOthInfoTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtOthInfoTbl 
 (
 PrtId, 
 PrtSBICLicnsNmb, 
 PrtCDCNmb, 
 PrtFmrNm, 
 PrtMicroLendrNmb, 
 PrtTaxId, 
 PrtTrdNm, 
 PrtFRSNmb, 
 PrtNCUANmb,
 PrtFDICNmb,
 PrtSCORENmb,
 PrtDUNSNmb,
 CreatUserId,
 CreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT
 )
 VALUES 
 (
 p_PrtId, 
 p_PrtSBICLicnsNmb,
 p_PrtCDCNmb,
 p_PrtFmrNm,
 p_PrtMicroLendrNmb, 
 p_PrtTaxId, 
 p_PrtTrdNm, 
 p_PrtFRSNmb, 
 p_PrtNCUANmb,
 p_PrtFDICNmb,
 p_PrtSCORENmb,
 p_PrtDUNSNmb,
 p_CreatUserId,
 SYSDATE,
 p_CreatUserId,
 SYSDATE
 );
 
 PARTNER.LOCEXTRCTSTATUPDCSP(p_PrtId,p_CreatUserId);
 END;
 END IF;
 END PrtOthInfoINSTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:46. --->
PROCEDURE VALIDELCTRNCADDRTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidElctrncAddrTyp VARCHAR2 :=NULL,
 pValidElctrncAddrTypDispOrdNmb NUMBER:=0,
 p_ValidElctrncAddrTypDesc VARCHAR2 :=NULL,
 p_ValidElctrncAddrCreatUserId VARCHAR2 :=NULL
 
 )
 AS
 BEGIN
 SAVEPOINT ValidElctrncAddrTypUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 UPDATE ValidElctrncAddrTypTbl
 SET 
 ValidElctrncAddrTypDispOrdNmb = pValidElctrncAddrTypDispOrdNmb,
 ValidElctrncAddrTypDesc = p_ValidElctrncAddrTypDesc, 
 ValidElctrncAddrCreatUserId =p_ValidElctrncAddrCreatUserId, 
 ValidElctrncAddrTypCreatDt = SYSDATE
 WHERE ValidElctrncAddrTyp =p_ValidElctrncAddrTyp;
 
 p_RetVal := SQL%ROWCOUNT;
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDELCTRNCADDRTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


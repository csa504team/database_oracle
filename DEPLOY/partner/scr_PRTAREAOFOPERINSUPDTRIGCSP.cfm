<!--- Saved 05/05/2015 13:17:12. --->
PROCEDURE PRTAREAOFOPERINSUPDTRIGCSP (
 NewDataRec PrtAreaOfOperTbl%ROWTYPE
 )
 AS
 v_error number(11);
 v_RowCount number(11);
 v_ErrorMessageTxt varchar2(255);
 v_ErrorNmb number(11);
 -- Temporary variables
 v_cnt number(11);
 BEGIN
 v_RowCount := SQL%ROWCOUNT;
 
 /* Check Area of Operation dates to be between agreement dates */
 select count(*) into v_cnt from PrtAgrmtTbl a
 where a.PrtId = NewDataRec.PrtId
 and a.PrtAgrmtSeqNmb = NewDataRec.PrtAgrmtSeqNmb
 and NewDataRec.PrtAreaOfOperCntyEffDt < a.PrtAgrmtEffDt;
 
 IF v_cnt > 0 
 THEN
 BEGIN
 v_ErrorNmb := 31603;
 v_ErrorMessageTxt := 'Area of Operation Effective date should be greater than or equal to agreement date.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 
 /* Test for existing active area of operation */
 IF NewDataRec.PrtAreaOfOperCntyEffDt <= SYSDATE
 THEN
 BEGIN
 select count(*) into v_cnt from PrtAreaOfOperTbl a where a.PrtId = NewDataRec.PrtId
 and a.PrtAgrmtSeqNmb = NewDataRec.PrtAgrmtSeqNmb
 and a.StCd = NewDataRec.StCd
 and a.CntyCd = NewDataRec.CntyCd
 and a.PrtAreaOfOperCntyEffDt <= SYSDATE
 and a.PrtAreaOfOperCntyEndDt IS NULL;
 
 IF v_cnt > v_RowCount
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Active Area Of Operation already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* End if active area of operation */
 END IF;
 
 /* Test for existing pending area of operation */
 IF NewDataRec.PrtAreaOfOperCntyEffDt > SYSDATE
 THEN
 BEGIN
 select count(*) into v_cnt from PrtAreaOfOperTbl a where a.PrtId = NewDataRec.PrtId
 and a.PrtAgrmtSeqNmb = NewDataRec.PrtAgrmtSeqNmb
 and a.StCd = NewDataRec.StCd 
 and a.CntyCd = NewDataRec.CntyCd
 and a.PrtAreaOfOperCntyEffDt > SYSDATE;
 
 IF v_cnt > v_RowCount
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Pending Area Of Operation already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* End if active area of operation */
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


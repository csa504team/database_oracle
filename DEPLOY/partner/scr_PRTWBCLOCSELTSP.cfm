<!--- Saved 05/05/2015 13:19:20. --->
PROCEDURE PRTWBCLOCSELTSP(
 p_Identifier IN NUMBER := 0,
 p_RetVal IN OUT NUMBER,
 p_LocId IN NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtWBCLocSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 ValidWBCClassCd,
 ValidWBCStatCd,
 PrtWBCLocFirstFundYrNmb,
 PrtWBCLocPrjctdFnlFundYrNmb,
 PrtWBCLocCreatUserId,
 PrtWBCLocCreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT
 FROM PrtWBCLocTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtWBCLocTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PrtNoteUpd;
 END;
 END PRTWBCLOCSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


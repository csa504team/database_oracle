<!--- Saved 03/24/2016 11:46:46. --->
procedure PRTLOCACHACCTSELTSP
 (p_Identifier number := 0, 
 p_RetVal out number,
 p_LocId number := 0,
 p_ValidACHAcctTyp varchar2 := NULL,
 p_SelCur out SYS_REFCURSOR
 )
 --NK 03/24/2016 added identifier 11 
 as
 begin
 savepoint PrtLocACHAcctSel;
 if p_Identifier = 0 then
 begin
 open p_SelCur for
 SELECT
 LocId,
 ValidACHAcctTyp,
 ACHRoutingNmb,
 ACHAcctNmb,
 ACHAcctDesc
 FROM PrtLocACHAcctTbl
 WHERE LocId = p_LocId
 AND ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := sql%rowcount;
 end;
 elsif p_Identifier = 2 then
 begin
 open p_SelCur for
 SELECT 1
 FROM PrtLocACHAcctTbl
 WHERE LocId = p_LocId
 AND ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := sql%rowcount;
 end;
 end if;
 if p_Identifier = 11 then
 begin
 open p_SelCur for
 SELECT
 LocId,
 ValidACHAcctTyp,
 ACHRoutingNmb,
 ACHAcctNmb,
 ACHAcctDesc
 FROM PrtLocACHAcctTbl 
 WHERE LocId= nvl(p_LocId,LocId) 
 AND ValidACHAcctTyp=nvl(p_ValidACHAcctTyp,ValidACHAcctTyp); 
 end;
 end if; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocACHAcctSel;
 RAISE;
 end PrtLocACHAcctSelTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


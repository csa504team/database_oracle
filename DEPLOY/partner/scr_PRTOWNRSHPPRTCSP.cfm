<!--- Saved 01/25/2018 10:37:59. --->
PROCEDURE PRTOWNRSHPPRTCSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_OwningPrtId IN NUMBER DEFAULT NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY --01/23/2018-- OPSMDEV-1335 :: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PrtNoteUpd; 
 OPEN p_SelCur FOR
 SELECT po.PrtOwnrshpPct,
 po.ValidOwndPrtTyp,
 po.ValidOwnrPrtTyp,
 po.PrtOwnrshpRank,
 p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd
 FROM PrtOwnrshpTbl po, PrtTbl p, LocSrchTbl l 
 WHERE po.OwningPrtId = p_OwningPrtId 
 AND po.PrtId = p.PrtId 
 AND po.PrtId = l.PrtId 
 AND l.ValidLocTyp = 'HO' 
 ORDER BY p.PrtLglNm ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PrtNoteUpd;
 END;
 END PRTOWNRSHPPRTCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


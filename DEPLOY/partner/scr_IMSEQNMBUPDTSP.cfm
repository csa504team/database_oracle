<!--- Saved 05/05/2015 13:16:18. --->
PROCEDURE IMSEQNMBUPDTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_IMSeqId IN VARCHAR2 := NULL ,
 p_IMSeqNmb IN NUMBER := 0 ,
 p_CreatUserId IN VARCHAR2 := NULL
 )
 AS
 
 BEGIN
 
 -- SAVEPOINT IMSeqNmbUpd;
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 UPDATE IMSeqNmbTbl
 SET IMSeqId = p_IMSeqId,
 IMSeqNmb = p_IMSeqNmb,
 CreatUserId = p_CreatUserId,
 CreatDt = SYSDATE
 WHERE IMSeqId = p_IMSeqId;
 
 END;
 ELSIF p_Identifier = 11 
 THEN
 /* Increment Sequence Number */
 
 BEGIN
 UPDATE IMSeqNmbTbl
 SET IMSeqNmb = IMSeqNmb + 1,
 CreatUserId = p_CreatUserId,
 CreatDt = SYSDATE
 WHERE IMSeqId = p_IMSeqId;
 
 END;
 
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


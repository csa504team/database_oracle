<!--- Saved 05/05/2015 13:20:17. --->
PROCEDURE ValidWBCStatSelTSP
 (p_Identifier number := 0,
 p_RetVal OUT number,
 p_ValidWBCStatCd number :=0,
 p_SelCur OUT SYS_REFCURSOR)
 as
 begin
 
 
 IF p_Identifier = 0
 THEN
 /* Select Row On the basis of Key*/
 begin
 OPEN p_SelCur for
 SELECT ValidWBCStatCd ,
 ValidWBCStatDescTxt
 FROM partner.ValidWBCStatTbl
 WHERE ValidWBCStatCd =p_ValidWBCStatCd;
 
 p_RetVal := SQL%ROWCOUNT ;
 END;
 
 ELSIF p_Identifier = 2
 THEN
 /*Check The Existance Of Row*/
 begin
 OPEN p_SelCur for 
 SELECT 1
 FROM partner.ValidWBCStatTbl
 WHERE ValidWBCStatCd =p_ValidWBCStatCd;
 
 p_RetVal := SQL%ROWCOUNT ;
 END;
 ELSIF p_Identifier = 4
 THEN
 /* Select Code and Description Text*/
 begin
 OPEN p_SelCur for
 SELECT ValidWBCStatCd ,ValidWBCStatDescTxt
 FROM partner.ValidWBCStatTbl;
 
 p_RetVal := SQL%ROWCOUNT ;
 END;
 END IF; 
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


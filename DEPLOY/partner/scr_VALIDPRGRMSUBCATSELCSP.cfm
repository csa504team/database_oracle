<!--- Saved 10/25/2017 15:14:38. --->
PROCEDURE VALIDPRGRMSUBCATSELCSP(
 p_Identifier NUMBER := 0,
 p_userName VARCHAR2 :=NULL,
 P_prtId NUMBER:=NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT v.PrgrmId
 , v.ValidPrtSubCatTyp
 , pr.ValidPrgrmTyp
 , pr.PrgrmParntPrgrmId
 , pr.PrgrmDesc
 , pr.PRGRMMAXTRMMOQTY
 FROM PARTNER.ValidPrgrmSubCatTbl v, PARTNER.PrtTbl p, PARTNER.PrgrmTbl pr 
 WHERE v.ValidPrtSubCatTyp = p.ValidPrtSubCatTyp 
 AND p.PrtId = P_prtId 
 AND v.PrgrmId = pr.PrgrmId;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 RAISE;
 ROLLBACK;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


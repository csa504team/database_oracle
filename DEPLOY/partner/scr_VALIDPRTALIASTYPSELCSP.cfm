<!--- Saved 05/05/2015 13:19:55. --->
PROCEDURE VALIDPRTALIASTYPSELCSP
 (
 p_userName VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrtAliasTyp,
 ValidPrtAliasTypDesc 
 FROM ValidPrtAliasTypTbl 
 WHERE ValidPrtAliasTypDispOrdNmb != 0 
 ORDER BY ValidPrtAliasTypDispOrdNmb ;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 RAISE;
 ROLLBACK;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:25. --->
PROCEDURE PRTCNTCTPOSTYPINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctPosTypCd NUMBER := 0,
 p_PrtCntctPosTypTxt VARCHAR2 := NULL,
 p_PrtCntctPosTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtCntctPosTypIns;
 
 IF p_Identifier = 0 THEN
 /* Insert into PrtCntctPosTypTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtCntctPosTypTbl (PrtCntctPosTypCd, PrtCntctPosTypTxt, PrtCntctPosTypCreatUserId,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_PrtCntctPosTypCd,p_PrtCntctPosTypTxt,p_PrtCntctPosTypCreatUserId,p_PrtCntctPosTypCreatUserId,SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtCntctPosTypIns;
 RAISE;
 
 END PRTCNTCTPOSTYPINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


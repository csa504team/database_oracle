<!--- Saved 08/09/2017 09:58:20. --->
PROCEDURE VALIDATESBICCSP (p_Identifier IN NUMBER := 0,
 p_PRTID IN NUMBER := NULL,
 p_LOCID IN NUMBER := NULL,
 p_PRTSBICAPPNMB IN NUMBER := NULL,
 p_TransInd IN NUMBER := NULL,
 p_CreatUserId IN VARCHAR2 := NULL,
 p_RetVal OUT NUMBER,
 p_ErrSeqNmb OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 /*
 *created by:RGG 05-21-2014
 * Purpose: Validations for SBIC system
 * Modified by:
 * */
 --v_tranflag NUMBER (12);
 --v_ErrTyp NUMBER (5);
 --v_ErrCd NUMBER (12);
 --v_ErrTxt VARCHAR2 (255);
 --v_temp NUMBER (1, 0) := 0;
 v_indcd VARCHAR2 (1);
 v_PRTSBICLICNSNMB VARCHAR2 (8);
 v_CntryValidInd CHAR (1);
 v_StValidInd CHAR (1);
 v_ZipValidInd CHAR (1);
 v_Zip4ValidInd CHAR (1);
 v_StZipValidInd CHAR (1);
 v_count NUMBER (10);
 v_StCntyValidInd CHAR (1); -- NK-- 06/13/2016
 v_ZipCntyValidInd CHAR (1); -- NK-- 06/13/2016
 v_FullZipValidInd CHAR (1); -- JP - 08/09/2017
 BEGIN
 SAVEPOINT VALIDATESBICCSP;
 
 p_ErrSeqNmb := 0;
 
 BEGIN
 SELECT PRTSBICINDCD, PRTSBICLICNSNMB
 INTO v_indcd, v_PRTSBICLICNSNMB
 FROM PARTNER.PRTSBICADDTNLINFOTBL
 WHERE PRTSBICAPPNMB = p_PRTSBICAPPNMB;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_indcd := NULL;
 v_PRTSBICLICNSNMB := NULL;
 END;
 
 
 
 IF ( v_indcd NOT IN ('A', '2')
 AND v_PRTSBICLICNSNMB IS NULL)
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2026,
 p_TransInd,
 p_CreatUserId,
 NULL,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 /*License Numbers must be unique */
 
 SELECT COUNT (*)
 INTO v_count
 FROM PARTNER.PRTSBICADDTNLINFOTBL
 WHERE PRTSBICLICNSNMB = v_PRTSBICLICNSNMB;
 
 IF v_count > 1
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2043,
 p_TransInd,
 p_CreatUserId,
 NULL,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 /*If there is a approved commitment, then you cannot change the status to new applicant*/
 
 SELECT COUNT (*)
 INTO v_count
 FROM loan.loangntytbl
 WHERE SBICLICNSNMB = v_PRTSBICLICNSNMB
 AND FININSTRMNTTYPIND = 'C';
 
 IF ( v_count > 0
 AND v_indcd = 'A')
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2046,
 p_TransInd,
 p_CreatUserId,
 v_PRTSBICLICNSNMB,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 /*If there is at least one draw down with status 2, then only the license status can go into Liquidation*/
 
 SELECT COUNT (*)
 INTO v_count
 FROM loan.loangntytbl
 WHERE SBICLICNSNMB = v_PRTSBICLICNSNMB
 AND FININSTRMNTTYPIND = 'D'
 AND loanstatcd = 2;
 
 IF ( v_count = 0
 AND v_indcd = 'L')
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2050,
 p_TransInd,
 p_CreatUserId,
 v_PRTSBICLICNSNMB,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 
 /*Validating Address Fields*/
 
 FOR c_addr
 IN (SELECT PhyAddrStr1Txt,
 PhyAddrCtyNm,
 PhyAddrStCd,
 SUBSTR (TRIM (PhyAddrPostCd), 1, 5) AS PhyAddrPostCd_5,
 PhyAddrCntCd
 FROM partner.PhyAddrTbl
 WHERE locid = p_locid
 AND ValidAddrTyp = 'Phys'
 AND PrtCntctNmb IS NULL
 AND PHYADDRSEQNMB = (SELECT MAX (PHYADDRSEQNMB)
 FROM partner.PhyAddrTbl
 WHERE locid = p_locid
 AND ValidAddrTyp = 'Phys'
 AND PrtCntctNmb IS NULL))
 LOOP
 -- Invalid Street Address
 IF LTRIM (c_addr.PhyAddrStr1Txt) IS NULL
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2027,
 p_TransInd,
 p_CreatUserId,
 NULL,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 -- Invalid City Name
 IF LTRIM (c_addr.PhyAddrCtyNm) IS NULL
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2028,
 p_TransInd,
 p_CreatUserId,
 NULL,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 --Invalid Country Code
 IF (c_addr.PhyAddrCntCd IS NULL)
 OR (UPPER (c_addr.PhyAddrCntCd) <> 'US')
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2029,
 p_TransInd,
 p_CreatUserId,
 c_addr.PhyAddrCntCd,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 IF (c_addr.PhyAddrCntCd IS NOT NULL)
 OR (c_addr.PhyAddrCntCd = 'US')
 THEN
 BEGIN
 sbaref.ValidateStZipCSP (
 3,
 c_addr.PhyAddrCntCd,
 c_addr.PhyAddrStCd,
 c_addr.PhyAddrPostCd_5,
 NULL,
 NULL,
 NULL,
 v_CntryValidInd,
 v_StValidInd,
 v_ZipValidInd,
 v_Zip4ValidInd,
 v_StZipValidInd,
 v_StCntyValidInd,
 v_ZipCntyValidInd,
 v_FullZipValidInd
 );
 
 --Invalid State code
 IF v_StValidInd = 'N'
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2030,
 p_TransInd,
 p_CreatUserId,
 NULL,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 --Invalid ZipCode
 IF v_ZipValidInd = 'N'
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2031,
 p_TransInd,
 p_CreatUserId,
 c_addr.PhyAddrPostCd_5,
 NULL,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 
 --Invalid State code for the given zipcode
 IF v_StZipValidInd = 'N'
 THEN
 BEGIN
 p_ErrSeqNmb :=
 ADD_ERROR (
 p_PRTSBICAPPNMB,
 p_LocId,
 p_PrtId,
 p_ErrSeqNmb,
 2032,
 p_TransInd,
 p_CreatUserId,
 c_addr.PhyAddrStCd,
 c_addr.PhyAddrPostCd_5,
 NULL,
 NULL,
 NULL
 );
 END;
 END IF;
 END;
 END IF;
 END LOOP;
 
 
 IF p_Identifier = 11
 THEN
 /* Retreive all the validation error for a given loan */
 BEGIN
 OPEN p_SelCur FOR
 SELECT AppNmb, ErrSeqNmb, ErrCd, ErrTxt
 FROM ValidationErrTbl
 WHERE AppNmb = p_PRTSBICAPPNMB
 ORDER BY ErrSeqNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO VALIDATESBICCSP;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:56. --->
PROCEDURE PRTLOCCLSFINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocClsfSeqNmb NUMBER := 0,
 p_ValidPrtLocClsfCd NUMBER := 0,
 p_PrtLocClsfCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocClsfIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtLocClsfTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocClsfTbl 
 (LocId, PrtLocClsfSeqNmb, ValidPrtLocClsfCd, PrtLocClsfCreatUserId, PrtLocClsfCreatDt,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_LocId,p_PrtLocClsfSeqNmb,p_ValidPrtLocClsfCd, p_PrtLocClsfCreatUserId, SYSDATE,p_PrtLocClsfCreatUserId, SYSDATE);
 p_RetVal := SQl%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Insert into PrtLocClsfTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocClsfTbl 
 (LocId, PrtLocClsfSeqNmb, ValidPrtLocClsfCd, PrtLocClsfCreatUserId, PrtLocClsfCreatDt,LASTUPDTUSERID,LASTUPDTDT)
 SELECT p_LocId, NVL(MAX(PrtLocClsfSeqNmb), 0) + 1, p_ValidPrtLocClsfCd,
 p_PrtLocClsfCreatUserId, SYSDATE,p_PrtLocClsfCreatUserId, SYSDATE 
 FROM PrtLocClsfTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocClsfIns;
 RAISE;
 
 END PRTLOCCLSFINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


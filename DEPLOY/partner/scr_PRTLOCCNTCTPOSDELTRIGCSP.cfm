<!--- Saved 05/05/2015 13:18:02. --->
PROCEDURE PrtLocCntctPosDelTrigCSP (
 OldDataRec PrtLocCntctPosTbl%ROWTYPE
 )
 AS
 /*--
 -- Adds the corresponding location to the extract queue.
 --
 -- If a Loan Manager contact is affected, then ALL the
 -- locations for the Partner are queued.
 --
 -- Revision History -----------------------------------------
 -- 04/15/2002 Mike Zheng New database structure
 -- ----------------------------------------------------------
 */
 v_ErrorNmb number(5);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_PartnerId number(7);
 v_PartnerRoleTyp number(11);
 v_ContactNmb number(8);
 v_PhyAddrPrtRoleTyp varchar2(5);
 v_PrtLocCntctCount number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 -- Temporary variables
 v_cnt number(11);
 
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_LocationId := OldDataRec.LocId;
 v_PartnerRoleTyp := OldDataRec.PrtCntctPosTypCd;
 v_ContactNmb := OldDataRec.PrtCntctNmb;
 BEGIN
 SELECT pl.PrtLocFIRSRecrdTyp INTO v_FIRSRecordTyp FROM partner.PrtLocTbl pl WHERE pl.LocId = OldDataRec.LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FIRSRecordTyp := NULL; END;
 BEGIN
 SELECT pl.PrtId INTO v_PartnerId FROM partner.PrtLocTbl pl WHERE pl.LocId = OldDataRec.LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PartnerId := NULL; END;
 
 IF v_PartnerRoleTyp = 3 AND v_FIRSRecordTyp = 'HO'
 THEN
 BEGIN
 -- Add all locations for Partner to extract queue.
 partner.LocExtrctStatUpdCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 END;
 ELSE
 BEGIN
 -- Add location to extract queue.
 partner.LocExtrctStatInsCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 END;
 END IF;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31082;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocCntctDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


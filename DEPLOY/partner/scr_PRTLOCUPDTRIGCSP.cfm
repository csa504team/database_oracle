<!--- Saved 05/05/2015 13:18:46. --->
PROCEDURE PRTLOCUPDTRIGCSP
 (
 OldDataRec partner.PrtLocTbl%ROWTYPE,
 NewDataRec partner.PrtLocTbl%ROWTYPE
 )
 -- Revision History -----------------------------------------
 -- 04/18/2001 Rajeswari DSP Primary Typ of Microlender has been
 -- changed from 'Lendr' to 'NBLDR' and
 -- including all lenders and NBLDR expect
 -- Microlender for FIRS extraction.
 -- 08/29/2000 C.Woodard Added code to refresh location data
 -- in partner search table and
 -- location search table.
 -- 04/19/2000 C.Woodard Removed cursor processing.
 -- C.Woodard 02/27/2000 Corrected logic to enforce business
 -- rule: Allow only one "HO" type
 -- location for a given Partner.
 -- C.Woodard 01/31/2000 Added logic to enforce business rule:
 -- Allow only one "HO" type location for
 -- a given Partner.
 -- C.Woodard 01/17/2000 Modified to only set FIRS#, FIRSRecordTyp
 -- IF PrimaryTyp = "HC"
 -- OR (PrimaryTyp = "Lendr"
 -- AND SubcategoryTyp != "Micro")
 -- ----------------------------------------------------------
 AS
 v_ErrorNmb number(5);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_LocationTyp varchar2(5);
 v_NewHeadLocationId number(7);
 v_PartnerId number(7);
 v_PreviousPartnerId number(7);
 v_PrimaryTyp varchar2(5);
 v_OutFIRSRecordTyp CHAR(2);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 v_SubcategoryTyp varchar2(5);
 -- Temporary variables
 v_cnt number(11);
 BEGIN
 v_INST_ID := 0;
 v_LocationId := NewDataRec.LocId;
 v_PartnerId := NewDataRec.PrtId;
 v_LocationTyp := NewDataRec.ValidLocTyp;
 v_LOC_ID := NewDataRec.LOC_ID;
 v_INST_ID := NVL(NewDataRec.INST_ID, 0);
 BEGIN
 SELECT p.ValidPrtPrimCatTyp into v_PrimaryTyp FROM partner.PrtTbl p WHERE p.PrtId = NewDataRec.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrimaryTyp := NULL; END;
 BEGIN
 SELECT p.ValidPrtSubCatTyp into v_SubcategoryTyp FROM partner.PrtTbl p WHERE p.PrtId = NewDataRec.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_SubcategoryTyp := NULL; END;
 
 v_ErrorNmb := 0;
 --Check if primary key has not been modified.
 IF NewDataRec.LocId <> OldDataRec.LocId
 THEN
 BEGIN
 v_ErrorNmb := 31265;
 v_ReplicationErrorMessageTxt := 'WARNING: Primary key (LocId)' || ' may have been modified.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 0;
 END;
 END IF;
 
 --Ensure only one head office location per Partner.
 SELECT COUNT(*) into v_cnt FROM partner.PrtLocTbl l WHERE l.ValidLocTyp = 'HO' AND l.PrtId = v_PartnerId;
 IF v_cnt > 1
 THEN
 BEGIN
 --Reset columns to values prior to update.
 UPDATE partner.PrtLocTbl l
 SET l.PrtLocFIRSNmb = OldDataRec.PrtLocFIRSNmb,
 l.PrtLocFIRSRecrdTyp = OldDataRec.PrtLocFIRSRecrdTyp,
 l.PrtId = OldDataRec.PrtId,
 l.PrtLocInactDt = OldDataRec.PrtLocInactDt,
 l.PrtLocNm = OldDataRec.PrtLocNm,
 l.PrtLocOpndDt = OldDataRec.PrtLocOpndDt,
 l.PrtLocCurStatCd = OldDataRec.PrtLocCurStatCd,
 l.ValidLocTyp = OldDataRec.ValidLocTyp,
 l.LOC_ID = OldDataRec.LOC_ID,
 l.INST_ID = OldDataRec.INST_ID,
 l.LastUpdtUserId = NewDataRec.LastUpdtUserId,
 l.LastUpdtDt = SYSDATE
 WHERE l.LocId = v_LocationId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; --Use system message
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --SQL Error
 END IF;
 v_ErrorNmb := 31263;
 v_ReplicationErrorMessageTxt := 'Multiple head office locations not allowed.' || ' PartnerLocation update undone.' ||
 ' LocationId was ' || to_char(v_LocationId) || '.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Multiple head office error.
 ELSE
 BEGIN --No multiple head office error.
 v_OutFIRSRecordTyp := NULL;
 --Determine location eligibility for FIRS processing.
 -- Set value of FIRSRecordTyp.
 select count(*) into v_cnt from ValidPrtSubCatTbl 
 where ValidPrtSubCatFIRSInd = 'Y' and ValidPrtSubCatTyp = v_SubcategoryTyp;
 
 IF v_cnt > 0
 THEN
 BEGIN
 partner.PrtLocFIRSRecrdSelCSP (v_LocationId, v_OutFIRSRecordTyp);
 v_ErrorNmb := SQLCODE;
 END; --Cases for FIRS processing
 END IF;
 
 -- Set value of FIRSRecordTyp.
 IF v_ErrorNmb = 0
 THEN
 BEGIN
 UPDATE partner.PrtLocTbl l
 SET PrtLocFIRSRecrdTyp = v_OutFIRSRecordTyp
 WHERE l.LocId = v_LocationId;
 IF SQLCODE != 0
 THEN
 BEGIN
 v_ErrorNmb := SQLCODE;
 v_ReplicationErrorMessageTxt := NULL; -- Use system message
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error
 END IF;
 END; -- Update
 END IF;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31264;
 v_ReplicationErrorMessageTxt := 'Unable to set FIRSRecordTyp.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error
 END IF;
 
 -- Add location to extract queue.
 partner.LocExtrctStatInsCSP (v_LocationId,NewDataRec.LastUpdtUserId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31266;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --Refresh location data in partner search table for new partner.
 partner.PrtSrchUpdLocCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31267;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' PrtId=' || to_char(v_PartnerId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh address data in partner search table for new partner.
 partner.PrtSrchUpdAddrCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31268;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh partner data in location search table.
 partner.LocSrchUpdPrtCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31269;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing LocSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh location data in location search table.
 partner.LocSrchUpdLocCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31270;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing LocSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Check for change in PrtId.
 v_PreviousPartnerId := OldDataRec.PrtId;
 IF v_PreviousPartnerId IS NOT NULL AND v_PreviousPartnerId <> v_PartnerId
 THEN
 BEGIN
 --Refresh location data in partner search table for previous partner.
 partner.PrtSrchUpdLocCSP (v_PreviousPartnerId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31271;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' PrtId=' || to_char(v_PreviousPartnerId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh address data in partner search table for previous partner using any new HO location
 -- data for that partner, or null values otherwise.
 BEGIN
 SELECT L.LocId into v_NewHeadLocationId FROM partner.PrtLocTbl L
 WHERE L.PrtId = v_PreviousPartnerId
 AND L.ValidLocTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_NewHeadLocationId := NULL; END;
 
 IF v_NewHeadLocationId IS NOT NULL
 THEN
 BEGIN
 partner.PrtSrchUpdAddrCSP (v_NewHeadLocationId);
 v_ErrorNmb := SQLCODE;
 END;
 ELSE
 BEGIN
 UPDATE partner.PrtSrchTbl
 SET PhyAddrCntCd = NULL,
 PhyAddrStr1Txt = NULL,
 PhyAddrCtyNm = NULL,
 PhyAddrStCd = NULL,
 PhyAddrPostCd = NULL,
 PhyAddrFIPSCntyCd = NULL,
 LastUpdtUserId = NewDataRec.LastUpdtUserId,
 LastUpdtDt = SYSDATE
 WHERE PrtId = v_PreviousPartnerId;
 v_ErrorNmb := SQLCODE;
 END;
 END IF;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31272;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' LocId=' || to_char(v_NewHeadLocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 END; --Refresh previous partner.
 END IF;
 END; --No multiple head office error.
 END IF;
 RETURN;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


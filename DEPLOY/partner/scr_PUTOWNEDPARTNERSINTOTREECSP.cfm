<!--- Saved 05/05/2015 13:19:23. --->
PROCEDURE PutOwnedPartnersIntoTreeCSP (
 p_RootId number,
 p_ParentId number,
 p_ParentLevelNmb number
 )
 AS
 --
 -- Inserts any partners owned directly or indirectly
 -- by the specified partner (p_ParentId) into the
 -- partner ownership tree.
 --
 /*
 Name Date Modification
 Hitarshi Buch 12/15/2003 Changed WHERE clause to use LIKE
 Hitarshi Buch 10/09/2003 If @@nestlevel > 12 return 0 (instead of -100),
 so that rest of ownership information gets processed.
 Hitarshi Buch 10/01/2003 Made changes to the modification made on
 05/20/2003 to account for the PrtId length in the
 query.
 Hitarshi Buch 05/20/2003 Error Number 99998 will not be written to the
 PrtReplicationErrTbl if one or more records already
 exist in the error table for a particular partner id.
 */
 v_DuplicateInd char(1);
 v_ErrorNodeId number(7,0);
 v_MaxLevelNmb number(2);
 v_NextLevelNmb number(2);
 v_PrtId number(7);
 v_PrtOwnrshpPct number(5,2);
 --v_PrtOwnrshpPct number;
 v_PrtLglNm varchar2(200);
 -- Error Handling variables
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_cnt number(10);
 
 CURSOR OwnedPartnerCsr IS SELECT o.PrtId, NVL(o.PrtOwnrshpPct,0) as PrtOwnrshpPct, p.PrtLglNm FROM PrtOwnrshpTbl o, PrtTbl p
 WHERE o.OwningPrtId = p_ParentId AND p.PrtId = o.PrtId ORDER BY p.PrtLglNm;
 BEGIN
 v_MaxLevelNmb := 10;
 v_INST_ID := 0;
 v_LOC_ID := 0;
 
 --Error checking
 /*DEBUG*/
 /*SELECT "ParentLevel=", CONVERT(CHAR(3), p_ParentLevelNmb),
 "v_v_NESTLEVEL=", CONVERT(CHAR(3), v_v_nestlevel)*/
 IF p_ParentId IS NULL OR p_ParentLevelNmb IS NULL OR p_ParentLevelNmb < 0
 THEN
 RETURN;
 END IF;
 
 IF p_ParentLevelNmb > v_MaxLevelNmb
 THEN
 RETURN;
 END IF;
 
 
 --Insert Partners which this Partner itself owns.
 OPEN OwnedPartnerCsr;
 LOOP
 FETCH OwnedPartnerCsr INTO v_PrtId, v_PrtOwnrshpPct, v_PrtLglNm;
 EXIT WHEN OwnedPartnerCsr%NOTFOUND;
 
 partner.CHECKDUPLICATENODECSP (p_ParentLevelNmb, v_PrtId, v_DuplicateInd);
 
 IF v_DuplicateInd = 'N' 
 THEN
 BEGIN --Normal processing.
 v_NextLevelNmb := p_ParentLevelNmb + 1;
 
 INSERT INTO partner.PrtOwnrshpTreeTbl (NodeId, Root,LevelNmb,PrtLglNm,PrtId,OwningPrtId,PrtOwnrshpPct)
 VALUES ((select NVL(max(NodeId),0)+1 from PrtOwnrshpTreeTbl), p_RootId, v_NextLevelNmb, v_PrtLglNm, v_PrtId, p_ParentId, v_PrtOwnrshpPct);
 
 --Insert Partners which this Partner itself owns.
 PutOwnedPartnersIntoTreeCSP (p_RootId, v_PrtId, v_NextLevelNmb);
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 99999;
 v_ReplicationErrorMessageTxt := 'Error calling PutOwnedPartnersIntoTreeCSP' || ' recursively.' ||
 ' p_RootId ' || to_char(p_RootId) || ' p_ParentId ' ||
 to_char(v_PrtId) || ' p_ParentLevelNmb ' || to_char(v_NextLevelNmb);
 v_INST_ID := 0;
 v_LOC_ID := 0;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PutOwnedPartnersIntoTreeCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; --Error.
 END IF;
 END; --Normal processing.
 ELSE
 BEGIN
 v_ErrorNmb := 99998;
 SELECT MAX(NodeId) + 1 into v_ErrorNodeId FROM partner.PrtOwnrshpTreeTbl;
 v_ReplicationErrorMessageTxt := 'Cycle error. Attempt to add partner node' || ' which would duplicate a partner' ||
 ' already in this branch of the tree.' || ' PrtId=' || to_char(v_PrtId) ||
 ' Node ' || to_char(v_ErrorNodeId) || ' not added at level ' || to_char(p_ParentLevelNmb + 1);
 v_INST_ID := 0;
 v_LOC_ID := 0;
 
 -- 05/20/2003: If error already exists for a partner, then no need to write it to the Error table
 -- 10/01/2003: Replaced '7' with 'char_length(convert(varchar2(7),v_PrtId))'
 -- 12/15/2003: Changed WHERE clause to use LIKE
 SELECT count(*) INTO v_cnt FROM partner.PrtReplicationErrTbl WHERE PrtReplicationErrNmb = 99998 
 AND PrtReplicationErrMsgTxt LIKE '%PrtId=' || to_char(v_PrtId) || '%';
 
 IF v_cnt < 1
 THEN
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PutOwnedPartnersIntoTreeCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END IF;
 END; --Cycle error.
 END IF;
 END LOOP;
 /*EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 raise_application_error(-20001,v_PrtId ||'~'|| v_PrtOwnrshpPct);
 -- RAISE;
 END;*/
 END;
 --Processing Owned Partners. 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


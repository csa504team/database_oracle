<!--- Saved 05/05/2015 13:17:56. --->
PROCEDURE PRTLOCCLSFDELTSP(
 p_Identifier NUMBER := 0,
 P_RetVal OUT NUMBER,
 P_LocId NUMBER := 0,
 P_PrtLocClsfSeqNmb IN NUMBER := 0)
 AS
 BEGIN
 SAVEPOINT PrtLocClsfDel;
 IF p_Identifier = 0 THEN
 /* Delect Row On the basis of Key*/
 BEGIN
 DELETE FROM PrtLocClsfTbl 
 WHERE LocId = p_LocId 
 AND PrtLocClsfSeqNmb = p_PrtLocClsfSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 ELSIF p_Identifier = 11 THEN
 /* Delect All Rows based on a Location*/
 BEGIN
 DELETE FROM PrtLocClsfTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocClsfDelP;
 RAISE;
 
 END PRTLOCCLSFDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


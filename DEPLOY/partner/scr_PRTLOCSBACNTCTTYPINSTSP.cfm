<!--- Saved 05/05/2015 13:18:36. --->
PROCEDURE PRTLOCSBACNTCTTYPINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_PrtLocSBACntctDescTxt VARCHAR2 := NULL,
 p_PrtLocSBACntctTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PRTLOCSBACNTCTTYPINS;
 IF p_Identifier = 0 THEN
 /* Insert into PrtLocSBACntctTyp Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocSBACntctTypTbl 
 (PrtLocSBACntctTypCd,PrtLocSBACntctDescTxt,PrtLocSBACntctTypCreatUserId,
 PrtLocSBACntctTypCreatDt,LastUpdtUserId,LastUpdtDt)
 VALUES (p_PrtLocSBACntctTypCd, p_PrtLocSBACntctDescTxt,p_PrtLocSBACntctTypCreatUserId,
 SYSDATE,p_PrtLocSBACntctTypCreatUserId,sysdate);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCSBACNTCTTYPINS;
 RAISE;
 
 END PRTLOCSBACNTCTTYPINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


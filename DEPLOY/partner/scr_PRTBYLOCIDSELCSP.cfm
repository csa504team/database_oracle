<!--- Saved 05/05/2015 13:17:18. --->
PROCEDURE PRTBYLOCIDSELCSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_LocId IN NUMBER DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /* SB -- 09/15/2014 -- Changed the Select Logic as per Ian's email */
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb,
 V.VALIDLOCTYPDESC
 FROM partner.PrtSrchTbl p, partner.LocSrchTbl l, partner.ValidLocTypTbl v 
 WHERE l.LocId = p_LocId 
 AND l.PrtLocCurStatCd IN ('O','P') 
 AND p.PrtCurStatCd IN ('O','P') 
 AND l.PrtId = p.PrtId
 AND L.VALIDLOCTYP=V.VALIDLOCTYP;
 
 /*SELECT p.PrtId,
 p.PrtLglNm,
 p.ValidPrtPrimCatTyp,
 l.PrtLocNm,
 l.ValidLocTyp,
 l.PhyAddrStr1Txt,
 l.PhyAddrCtyNm,
 l.PhyAddrStCd,
 l.PhyAddrPostCd,
 l.LocId,
 l.PrtLocFIRSNmb
 FROM PrtSrchTbl p, LocSrchTbl l 
 WHERE l.LocId = p_LocId 
 AND l.PrtLocCurStatCd = 'O' 
 AND p.PrtCurStatCd = 'O' 
 AND l.PrtId = p.PrtId;*/
 
 END PRTBYLOCIDSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by prodigy09. 
DATE:				05/03/2016.
DESCRIPTION:		Standardized call to SRCHNOTECSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	05/03/2016, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 42)
	and	(Left(CGI.Script_Name,    42) is "/cfincludes/oracle/partner/spc_SRCHNOTECSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"			default="">
<cfparam name="Variables.ErrMsg"				default="">
<cfparam name="Variables.SkippedSpcs"			default="">
<cfparam name="Variables.TxnErr"				default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs				= ListAppend(Variables.SkippedSpcs, "SRCHNOTECSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"			default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"			default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"			default="call SRCHNOTECSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName				= "SRCHNOTECSP">
	<cfparam name="Variables.NoteSubjTxt"		default="">
	<cfparam name="Variables.LocId"				default="">
	<cfparam name="Variables.NoteEffDate"		default="">
	<cfparam name="Variables.StartDate"			default="">
	<cfparam name="Variables.EndDate"			default="">
	<cfparam name="Variables.PrtNoteEffDt"		default="">
	<cfparam name="Variables.NoteSubjTyp"		default="">
	<cfparam name="Variables.AuthorFirstNm"		default="">
	<cfparam name="Variables.AuthorLastNm"		default="">
	<cfparam name="Variables.SbaOfcCd"			default="">
	<cfparam name="Variables.NoteSubjSrchInd"	default="">
	<cftry>
		<cfstoredproc procedure="PARTNER.SRCHNOTECSP" datasource="#Variables.db#"
											username="#Variables.username#" password="#Variables.password#">
		<cfif Len(Variables.NoteSubjTxt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_NoteSubjTxt"
											value="#Variables.NoteSubjTxt#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_NoteSubjTxt"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.LocId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_LocId"
											value="#Variables.LocId#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_LocId"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.NoteEffDate) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_NoteEffDate"
											value="#Variables.NoteEffDate#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_NoteEffDate"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.StartDate) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_StartDate"
											value="#Variables.StartDate#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_StartDate"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.EndDate) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_EndDate"
											value="#Variables.EndDate#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_EndDate"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.PrtNoteEffDt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtNoteEffDt"
											value="#Variables.PrtNoteEffDt#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtNoteEffDt"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.NoteSubjTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_NoteSubjTyp"
											value="#Variables.NoteSubjTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_NoteSubjTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.AuthorFirstNm) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_AuthorFirstNm"
											value="#Variables.AuthorFirstNm#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_AuthorFirstNm"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.AuthorLastNm) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_AuthorLastNm"
											value="#Variables.AuthorLastNm#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_AuthorLastNm"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.SbaOfcCd) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_SbaOfcCd"
											value="#Variables.SbaOfcCd#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_SbaOfcCd"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.NoteSubjSrchInd) GT 0>
			<cfprocparam	type="In"		dbvarname=":P_NoteSubjSrchInd"
											value="#Variables.NoteSubjSrchInd#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":P_NoteSubjSrchInd"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


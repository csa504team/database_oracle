<!--- Saved 05/05/2015 13:16:28. --->
PROCEDURE LOCSRCHUPDADDRCSP(
 p_LocId NUMBER := NULL)
 AS
 CurDataRec PhyAddrTbl%ROWTYPE;
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_CntyCd char(3);
 v_PhyAddrStCd varchar2(75);
 v_PhyAddrPostCd varchar2(20);
 BEGIN
 v_INST_ID := 0;
 v_LOC_ID := 0;
 --Error checking
 IF p_LocId IS NULL
 THEN
 RETURN;
 END IF;
 
 BEGIN
 SELECT * INTO CurDataRec FROM PhyAddrTbl a WHERE LocId = p_LocId AND PrtCntctNmb is null AND ValidAddrTyp = 'Phys'
 AND PhyAddrSeqNmb = (SELECT MAX(PhyAddrSeqNmb) FROM PhyAddrTbl z WHERE a.LocId = z.LocId AND z.PrtCntctNmb is null
 AND a.ValidAddrTyp = z.ValidAddrTyp);
 EXCEPTION WHEN NO_DATA_FOUND THEN CurDataRec := NULL; END;
 
 /* IAC 1/26/2001 Only do the Update if there is address data returned
 There can only be one Main Phys address per Partner... */
 UPDATE LocSrchTbl
 SET ValidAddrTyp = CurDataRec.ValidAddrTyp,
 PhyAddrStr1Txt = CurDataRec.PhyAddrStr1Txt,
 PhyAddrStr2Txt = CurDataRec.PhyAddrStr2Txt,
 PhyAddrCtyNm = CurDataRec.PhyAddrCtyNm,
 PhyAddrStCd = CurDataRec.PhyAddrStCd,
 PhyAddrPostCd = CurDataRec.PhyAddrPostCd,
 PhyAddrFIPSCntyCd = CurDataRec.PhyAddrFIPSCntyCd,
 PhyAddrCntCd = CurDataRec.PhyAddrCntCd,
 PhyAddrUSDpndcyInd = CurDataRec.PhyAddrUSDpndcyInd,
 PhyAddrDataSourcId = CurDataRec.PhyAddrDataSourcId,
 PhyAddrCtySrchNm = UPPER(CurDataRec.PhyAddrCtyNm),
 CntyCd = substr(CurDataRec.PhyAddrFIPSCntyCd,3,3),
 LastUpdtUserId = CurDataRec.PhyAddrCreatUserId,
 LastUpdtDt = NVL(CurDataRec.PhyAddrCreatDt,SYSDATE)
 WHERE LocId = p_LocId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; /* Use system message*/
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 51201;
 v_ReplicationErrorMessageTxt := 'Error updating LocSrchTbl. LocId=' || to_char(p_LocId);
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; -- Update Error
 END IF;
 
 --update LocSrchTbl with CntyNm and OrignOfcCd for each location
 IF CurDataRec.PhyAddrFIPSCntyCd IS NOT NULL
 THEN
 v_CntyCd := substr(CurDataRec.PhyAddrFIPSCntyCd,3,3);
 END IF;
 v_PhyAddrStCd := CurDataRec.PhyAddrStCd;
 IF v_CntyCd IS NOT NULL AND v_PhyAddrStCd IS NOT NULL
 THEN
 UPDATE partner.LocSrchTbl l
 SET (l.CntyNm, l.OrignOfcCd) = (select c.CntyNm, c.OrignOfcCd FROM sbaref.CntyTbl c
 WHERE c.CntyCd = v_CntyCd and c.StCd = trim(v_PhyAddrStCd) 
 and (c.CntyEndDt > SYSDATE or c.CntyEndDt is Null))
 WHERE trim(PhyAddrCntCd) = 'US'
 and LocId = p_LocId;
 END IF;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; /* Use system message*/
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 51202;
 v_ReplicationErrorMessageTxt := 'Error updating LocSrchTbl. LocId=' || to_char(p_LocId);
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; --Error.
 END IF;
 
 --update CongrsnlDistNmb for each Location
 v_PhyAddrPostCd := CurDataRec.PhyAddrPostCd;
 IF v_PhyAddrPostCd IS NOT NULL
 THEN
 UPDATE partner.LocSrchTbl l
 SET l.CongrsnlDistNmb = (select c.IMCongrsnlDistCd from sbaref.Zip5CdTbl c where substr(v_PhyAddrPostCd,1,5) = c.Zip5Cd)
 WHERE ltrim(PhyAddrCntCd) = 'US' and LocId = p_LocId;
 END IF;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; /* Use system message*/
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 51203;
 v_ReplicationErrorMessageTxt := 'Error updating LocSrchTbl. LocId=' || to_char(p_LocId);
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; --Error.
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


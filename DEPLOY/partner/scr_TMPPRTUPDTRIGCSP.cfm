<!--- Saved 01/23/2018 15:32:05. --->
PROCEDURE TmpPRTUPDTRIGCSP (
 OldDataRec PrtTbl%ROWTYPE,
 NewDataRec PrtTbl%ROWTYPE)
 AS
 PrtLocOldDataRec PrtLocTbl%ROWTYPE;
 PrtLocNewDataRec PrtLocTbl%ROWTYPE;
 PrtAgrmtOldDataRec PrtAgrmtTbl%ROWTYPE;
 PrtAgrmtNewDataRec PrtAgrmtTbl%ROWTYPE;
 
 --
 -- Adds all locations of Partner to extract queue
 -- when Partner is updated.
 --
 /* Revision History -----------------------------------------
 -- 09/25/2001 Rajeswari DSP Modified to close the locations and
 -- agreements if the partner is closed
 -- for non tfp-replication records
 -- 04/05/2001 Rajeswari DSP Modified code to include RolePrivilege
 -- in search tables.
 -- 08/29/2000 Rajeswari DSP Added code to update partner data
 -- in PrtSrchTbl and LocSrchTbl.
 -- 08/25/2000 C.Woodard Modified to add ALL locations to
 -- extract queue. Previous version
 -- added only HO location.
 -- 03/29/2000 C.Woodard Removed cursor processing.
 --
 -- ----------------------------------------------------------
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtDataSourcId
 */
 v_ErrorNmb NUMBER (5);
 v_INST_ID NUMBER (8);
 v_LOC_ID NUMBER (8);
 v_PartnerId NUMBER (7);
 v_LocationId NUMBER (7);
 v_Status CHAR (1);
 v_DataSource VARCHAR2 (20);
 v_RolePrivilege VARCHAR2 (20);
 v_PrtReplicationErrMsgTxt VARCHAR2 (255);
 v_ReplicationErrorMessageTxt VARCHAR2 (255);
 v_Rowcount NUMBER (12);
 v_PrtAgrmtSeqNmb NUMBER (3);
 v_PrtLocId NUMBER (7);
 v_OldUpdDt DATE;
 v_OldStatus CHAR (1);
 v_RetVal NUMBER (5);
 BEGIN
 v_PrtLocId := NULL;
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_LOC_ID := NULL;
 v_PartnerId := NewDataRec.PrtId;
 v_Status := NewDataRec.PrtCurStatCd;
 v_INST_ID := NewDataRec.INST_ID;
 
 --Check if primary key has not been modified.
 IF (OldDataRec.PrtId <> NewDataRec.PrtId)
 THEN
 BEGIN
 v_ErrorNmb := 30501;
 v_PrtReplicationErrMsgTxt :=
 'WARNING: Primary key (PartnerId) may have been modified.'
 || ' New PartnerId is '
 || TO_CHAR (v_PartnerId)
 || '.';
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_PrtReplicationErrMsgTxt,
 NULL);
 END;
 END IF;
 
 -- Add head office location to extract queue.
 partner.LocExtrctStatUpdCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 30502;
 v_PrtReplicationErrMsgTxt :=
 'Unable to add HO location to extract queue.';
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_PrtReplicationErrMsgTxt,
 NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --Update partner data in partner search table.
 PrtSrchUpdPrtCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30503;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt :=
 'Error refreshing PrtSrchTbl.'
 || ' PrtId='
 || TO_CHAR (v_PartnerId);
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 -- Get RolePrivilegePrgrmID
 BEGIN
 SELECT RolePrivilegePrgrmId
 INTO v_RolePrivilege
 FROM ValidPrtSubCatTbl v, PrtTbl p
 WHERE p.PrtId = v_PartnerId
 AND v.ValidPrtSubCatTyp = p.ValidPrtSubCatTyp;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_RolePrivilege := NULL;
 END;
 
 --Update partner data in location search table.
 UPDATE partner.LocSrchTbl s
 SET (PrtId,
 ValidPrtSubCatTyp,
 RolePrivilegePrgrmId,
 LastUpdtUserId,
 LastUpdtDt) =
 (SELECT p.PrtId,
 p.ValidPrtSubCatTyp,
 v_RolePrivilege,
 USER,
 SYSDATE
 FROM partner.PrtLocTbl l, partner.PrtTbl p
 WHERE s.LocId = l.LocId
 AND l.PrtId = p.PrtId
 AND l.PrtId = v_PartnerId)
 WHERE EXISTS
 (SELECT 1
 FROM partner.PrtLocTbl l, partner.PrtTbl p
 WHERE s.LocId = l.LocId
 AND l.PrtId = p.PrtId
 AND l.PrtId = v_PartnerId);
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30504;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt :=
 'Error refreshing LocSrchTbl.'
 || ' LocId='
 || TO_CHAR (v_LocationId);
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 -- update location records and agreement records when a partner is closed.
 IF v_DataSource != 'TFP-Replication' AND v_Status = 'C'
 THEN
 BEGIN
 BEGIN
 SELECT MIN (LocId)
 INTO v_PrtLocId
 FROM PrtLocTbl
 WHERE PrtLocCurStatCd = 'O' AND PrtId = v_PartnerId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtLocId := NULL;
 END;
 
 WHILE (v_PrtLocId IS NOT NULL)
 LOOP
 BEGIN
 -- get old snapsshot
 BEGIN
 SELECT *
 INTO PrtLocOldDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocOldDataRec := NULL;
 END;
 
 UPDATE partner.PrtLocTbl
 SET PrtLocCurStatCd = 'C',
 LastUpdtUserId = USER,
 LastUpdtDt = SYSDATE
 WHERE LocId = v_PrtLocId
 AND PrtLocCurStatCd = 'O'
 AND PrtId = v_PartnerId;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30505;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt :=
 'Error updating PrtLocTbl.'
 || ' PrtId='
 || TO_CHAR (v_PartnerId)
 || ', PrtLocId='
 || TO_CHAR (v_PrtLocId);
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 ELSE -- call trigger
 BEGIN
 -- get new snapsshot
 BEGIN
 SELECT *
 INTO PrtLocNewDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocNewDataRec := NULL;
 END;
 
 PrtLocUpdTrigCSP (PrtLocOldDataRec, PrtLocNewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF;
 
 BEGIN
 SELECT MIN (LocId)
 INTO v_PrtLocId
 FROM PrtLocTbl
 WHERE PrtLocCurStatCd = 'O' AND PrtId = v_PartnerId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtLocId := NULL;
 END;
 END;
 END LOOP; /* end WHILE */
 END; /* end IF v_DataSource != 'TFP-Replication' and v_Status = 'C' */
 END IF;
 
 IF (v_Status = 'C')
 THEN
 BEGIN
 /* Select the active agreements and terminate them */
 BEGIN
 SELECT MIN (PrtAgrmtSeqNmb)
 INTO v_PrtAgrmtSeqNmb
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId
 AND ( PrtAgrmtEffDt <= SYSDATE
 AND ( PrtAgrmtExprDt >= SYSDATE
 OR PrtAgrmtExprDt IS NULL)
 AND ( PrtAgrmtTermDt > SYSDATE
 OR PrtAgrmtTermDt IS NULL));
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtAgrmtSeqNmb := NULL;
 END;
 
 WHILE (v_PrtAgrmtSeqNmb IS NOT NULL)
 LOOP
 BEGIN
 BEGIN
 -- get old snapshot
 SELECT *
 INTO PrtAgrmtOldDataRec
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId
 AND PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtAgrmtOldDataRec := NULL;
 END;
 
 UPDATE PrtAgrmtTbl
 SET PrtAgrmtTermDt = SYSDATE,
 PrtAgrmtTermRsnCd = 4,
 LastUpdtUserId = USER,
 LastUpdtDt = SYSDATE
 WHERE PrtId = v_PartnerId
 AND PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30506;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt :=
 'Error updating PrtAgrmtTbl.'
 || ' PrtId='
 || TO_CHAR (v_PartnerId);
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 ELSE -- call trigger
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO PrtAgrmtNewDataRec
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId
 AND PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtAgrmtNewDataRec := NULL;
 END;
 
 PrtAgrmtUpdTrigCSP (PrtAgrmtOldDataRec,
 PrtAgrmtNewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF;
 
 BEGIN
 SELECT MIN (PrtAgrmtSeqNmb)
 INTO v_PrtAgrmtSeqNmb
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId
 AND ( PrtAgrmtEffDt <= SYSDATE
 AND ( PrtAgrmtExprDt >= SYSDATE
 OR PrtAgrmtExprDt IS NULL)
 AND ( PrtAgrmtTermDt > SYSDATE
 OR PrtAgrmtTermDt IS NULL))
 AND PrtAgrmtSeqNmb > v_PrtAgrmtSeqNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtAgrmtSeqNmb := NULL;
 END;
 END; /* End while (v_PrtAgrmtSeqNmb != NULL) for active */
 END LOOP;
 
 /* select the pending agreements and delete them */
 BEGIN
 SELECT MIN (PrtAgrmtSeqNmb)
 INTO v_PrtAgrmtSeqNmb
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId AND PrtAgrmtEffDt > SYSDATE;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtAgrmtSeqNmb := NULL;
 END;
 
 WHILE (v_PrtAgrmtSeqNmb IS NOT NULL)
 LOOP
 BEGIN
 PndngPrtAgrmtDelCSP (0,
 v_RetVal,
 v_PartnerId,
 v_PrtAgrmtSeqNmb);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30506;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt :=
 'Error deleting pending agreement from PrtAgrmtTbl.'
 || ' PrtId='
 || TO_CHAR (v_PartnerId);
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtUpdTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 BEGIN
 SELECT MIN (PrtAgrmtSeqNmb)
 INTO v_PrtAgrmtSeqNmb
 FROM PrtAgrmtTbl
 WHERE PrtId = v_PartnerId
 AND PrtAgrmtEffDt > SYSDATE
 AND PrtAgrmtSeqNmb > v_PrtAgrmtSeqNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtAgrmtSeqNmb := NULL;
 END;
 END; /* end while (v_PrtAgrmtSeqNmb != NULL) for pending */
 END LOOP;
 END; /* end if (v_Status = 'C') */
 END IF;
 
 -- Added by Prasad Mente on 01/23/2008
 -- get old updated date & old status
 v_OldUpdDt := OldDataRec.LastUpdtDt;
 v_OldStatus := OldDataRec.PrtCurStatCd;
 
 IF v_DataSource != 'TFP-Replication'
 AND v_Status = 'O'
 AND v_OldStatus = 'C'
 THEN
 BEGIN -- Begin of updating Locations to Open
 -- get HO LocId
 BEGIN
 SELECT LocId
 INTO v_PrtLocId
 FROM PrtLocTbl
 WHERE PrtId = v_PartnerId AND ValidLocTyp = 'HO';
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtLocId := NULL;
 END;
 
 -- update HO location status to O - open
 -- get the old snapshot
 BEGIN
 SELECT *
 INTO PrtLocOldDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocOldDataRec := NULL;
 END;
 
 UPDATE PrtLocTbl
 SET PrtLocCurStatCd = 'O',
 LastUpdtUserId = USER,
 LastUpdtDt = SYSDATE
 WHERE LocId = v_PrtLocId
 AND PrtId = v_PartnerId
 AND PrtLocCurStatCd = 'C'
 AND ValidLocTyp = 'HO';
 
 IF SQLCODE = 0
 THEN
 BEGIN -- call trigger
 -- get new snapsshot
 BEGIN
 SELECT *
 INTO PrtLocNewDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocNewDataRec := NULL;
 END;
 
 PrtLocUpdTrigCSP (PrtLocOldDataRec, PrtLocNewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF;
 
 -- update locations status to O which were closed on same date as partner
 BEGIN
 SELECT MIN (LocId)
 INTO v_PrtLocId
 FROM PrtLocTbl
 WHERE PrtLocCurStatCd = 'C' AND PrtId = v_PartnerId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtLocId := NULL;
 END;
 
 WHILE (v_PrtLocId IS NOT NULL)
 LOOP
 BEGIN
 -- get the old snapshot
 BEGIN
 SELECT *
 INTO PrtLocOldDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocOldDataRec := NULL;
 END;
 
 UPDATE PrtLocTbl
 SET PrtLocCurStatCd = 'O',
 LastUpdtUserId = USER,
 LastUpdtDt = SYSDATE
 WHERE LocId = v_PrtLocId
 AND PrtLocCurStatCd = 'C'
 AND PrtId = v_PartnerId
 AND TRUNC (PrtLocCreatDt) = TRUNC (v_OldUpdDt);
 
 IF SQLCODE = 0
 THEN
 BEGIN -- call trigger
 -- get new snapsshot
 BEGIN
 SELECT *
 INTO PrtLocNewDataRec
 FROM PrtLocTbl
 WHERE LocId = v_PrtLocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocNewDataRec := NULL;
 END;
 
 PrtLocUpdTrigCSP (PrtLocOldDataRec, PrtLocNewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF;
 
 BEGIN
 SELECT MIN (LocId)
 INTO v_PrtLocId
 FROM PrtLocTbl
 WHERE PrtLocCurStatCd = 'C'
 AND PrtId = v_PartnerId
 AND TRUNC (PrtLocCreatDt) = TRUNC (v_OldUpdDt);
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrtLocId := NULL;
 END;
 END; /* end WHILE */
 END LOOP;
 END; -- End of updating Locations to Open
 END IF;
 
 -- End of modifications by Prasad Mente on 01/23/2008
 RETURN;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:31:57. --->
PROCEDURE ELCTRNCADDRUPDTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ElctrncAddrNmb NUMBER := 0,
 p_ValidElctrncAddrTyp VARCHAR2 := NULL,
 p_PrtCntctNmb NUMBER := 0,
 p_ElctrncAddrTxt VARCHAR2 := NULL,
 p_LOC_ID NUMBER := 0,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := 0,
 p_TYPE_CODE VARCHAR2 := NULL,
 p_RANK NUMBER := 0,
 p_INST_ID NUMBER := 0,
 p_ElctrncAddrCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ElctrncAddrDataSourcId 
 */
 BEGIN
 --SAVEPOINT ELCTRNCADDRUPDTSP;
 IF p_Identifier = 0
 THEN
 BEGIN
 UPDATE PARTNER.ElctrncAddrTbl
 SET ValidElctrncAddrTyp = p_ValidElctrncAddrTyp,
 PrtCntctNmb = p_PrtCntctNmb,
 ElctrncAddrTxt = p_ElctrncAddrTxt,
 LOC_ID = p_LOC_ID,
 DEPARTMENT = p_DEPARTMENT,
 PERSON_ID = p_PERSON_ID,
 TYPE_CODE = p_TYPE_CODE,
 RANK = p_RANK,
 INST_ID = p_INST_ID,
 lastupdtuserid = p_ElctrncAddrCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LOCID = p_LocId AND ELCTRNCADDRNMB = p_ElctrncAddrNmb;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrInsUpdTrigCSP (p_LocId, p_ElctrncAddrCreatUserId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK;
 END ELCTRNCADDRUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:06. --->
PROCEDURE PRTREPLICATIONERRINSCSP(
 p_INST_ID NUMBER := NULL,
 p_LOC_ID NUMBER := NULL,
 p_PrtReplicationErrProcNm VARCHAR2 := NULL,
 p_PrtReplicationErrNmb NUMBER := 0,
 p_PrtReplicationErrMsgTxt VARCHAR2 := NULL,
 p_PrtReplicationErrRemrksTxt VARCHAR2 := NULL)
 AS
 i_PrtReplicationErrProcNm VARCHAR2(50); 
 i_PrtReplicationErrNmb NUMBER ; 
 BEGIN
 
 IF p_PrtReplicationErrProcNm IS NULL THEN
 i_PrtReplicationErrProcNm :='None specified.';
 END IF;
 IF p_PrtReplicationErrNmb = 0 THEN
 i_PrtReplicationErrNmb :=20001;
 END IF;
 /* IF p_PrtReplicationErrNmb BETWEEN 17000 AND 19999 THEN
 BEGIN
 sp_getmessage(p_PrtReplicationErrNmb,
 p_PrtReplicationErrMsgTxt);
 END;
 END IF; */
 INSERT INTO PARTNER.PrtReplicationErrTbl (PrtReplicationErrId, PrtReplicationErrTime, INST_ID, LOC_ID, PrtReplicationErrProcNm, PrtReplicationErrNmb,
 PrtReplicationErrMsgTxt, PrtReplicationErrRemrksTxt,PRTREPLICATIONERRCREATUSERID,PRTREPLICATIONERRCREATDT,LastUpdtUserId,LastUpdtDt)
 VALUES (PRTRepErrIDSEQ.nextval, SYSDATE,p_INST_ID, p_LOC_ID,NVL(i_PrtReplicationErrProcNm,p_PrtReplicationErrProcNm),
 NVL(i_PrtReplicationErrNmb,p_PrtReplicationErrNmb), p_PrtReplicationErrMsgTxt,p_PrtReplicationErrRemrksTxt,user,sysdate,user,sysdate);
 
 
 END PRTREPLICATIONERRINSCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:32:00. --->
PROCEDURE PRTHISTRYSELTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtHistryEffDt DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_PrtHistryPartyTyp CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PrtHistrySel;
 
 IF p_Identifier = 0
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtHistryEffDt,
 ValidChngCd,
 PrtHistryPartyTyp,
 PrtHistryInitPrtId,
 PrtHistryResultingPrtId,
 PrtHistryChrtrTyp,
 PrtHistryEstbDt,
 PrtHistryLglNm,
 PrtHistryPrimTyp,
 PrtHistrySubCatTyp,
 PrtHistryReglAgencyNm,
 PrtHistryTFPHistryDt,
 AFFECTED_INST_ID,
 EFFECTIVE_DATE,
 PARTY_TYPE_INDICATOR,
 CHANGE_CODE
 FROM PrtHistryTbl
 WHERE PrtId = p_PrtId
 AND PrtHistryEffDt = p_PrtHistryEffDt
 AND ValidChngCd = p_ValidChngCd
 AND PrtHistryPartyTyp = p_PrtHistryPartyTyp;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtHistryTbl
 WHERE PrtId = p_PrtId
 AND PrtHistryEffDt = p_PrtHistryEffDt
 AND ValidChngCd = p_ValidChngCd
 AND PrtHistryPartyTyp = p_PrtHistryPartyTyp;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtHistrySel;
 RAISE;
 END PRTHISTRYSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


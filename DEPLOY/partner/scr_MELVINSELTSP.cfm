<!--- Saved 11/17/2010 11:48:52. --->
PROCEDURE MelvinSelTsp(
 p_Identifier number := 0,
 p_RetVal out number,
 p_PrtLglNm VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_LocId NUMBER := 0,
 p_PhyAddrStCd VARCHAR2 := NULL,
 p_PrtCurStatCd CHAR := NULL,
 p_PrtLglNmSrchTyp NUMBER := 0,
 p_SelCur out SYS_REFCURSOR
 )
 AS
 v_x CHAR;
 v_y CHAR;
 v_z CHAR;
 v_cnt number(5,0);
 v_LicensingStCd CHAR(2);
 v_LocId1 NUMBER(7, 0);
 v_LocId NUMBER(7,0);
 v_LicensingStCd1 VARCHAR2(255);
 CURSOR procur IS 
 SELECT DISTINCT LocId,
 LicensingStCd 
 FROM partner.BndAgntLicensngTbl 
 WHERE LocId IN (
 SELECT DISTINCT LocId 
 FROM partner.TT_tmp1) 
 ORDER BY LocId, LicensingStCd;
 BEGIN
 delete partner.TT_tmp1;
 delete partner.TT_tmp2;
 INSERT 
 INTO partner.TT_tmp1 
 SELECT DISTINCT pa.PrtAliasNm,
 l.PrtLocNm,
 p.PrtLglNm,
 l.LocId,
 p.ValidPrtSubCatTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd 
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p, partner.PrtLocTbl
 l, partner.PhyAddrTbl a left outer join partner.ElctrncAddrTbl c on a.LocId = c.LocId
 WHERE p.PrtId = pa.PrtId AND l.PrtId = pa.PrtId AND a.LocId = l.LocId 
 AND pa.ValidPrtAliasTyp = 'TIN'
 AND p.ValidPrtSubCatTyp IN ( 'Srty', 'BNDAG'
 ) 
 --AND l.ValidLocTyp = 'HO'
 --AND p.PrtCurStatCd = 'O'
 AND UPPER(p.PrtLglNm) LIKE (CASE 
 WHEN p_PrtLglNmSrchTyp = 1 THEN nvl(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm
 )) || '%' 
 WHEN p_PrtLglNmSrchTyp = 2 THEN '%' || nvl(UPPER(p_PrtLglNm),
 UPPER(p.PrtLglNm)) || '%' 
 WHEN p_PrtLglNmSrchTyp = 3 THEN nvl(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm
 )) 
 ELSE
 UPPER(p.PrtLglNm) 
 END) 
 AND pa.PrtAliasNm LIKE nvl(p_PrtAliasNm, pa.PrtAliasNm) 
 AND l.LocId = nvl(p_LocId, l.LocId) 
 AND a.PhyAddrStCd = nvl(p_PhyAddrStCd, a.PhyAddrStCd)
 AND l.PrtLocCurStatCd = 'O' 
 AND p.PrtCurStatCd IN (CASE WHEN p_PrtCurStatCd = 'O' 
 THEN 'O'
 WHEN p_PrtCurStatCd = 'C'
 THEN 'C'
 WHEN p_PrtCurStatCd = 'D'
 THEN 'D'
 WHEN p_PrtCurStatCd = 'A'
 THEN p.PrtCurStatCd
 ELSE p.PrtCurStatCd
 end);
 
 
 SELECT COUNT(DISTINCT to_char(LocId) || LicensingStCd) into v_cnt
 FROM partner.BndAgntLicensngTbl 
 WHERE LocId IN (
 SELECT DISTINCT LocId 
 FROM partner.TT_tmp1); 
 
 OPEN procur; 
 FETCH procur INTO v_LocId, v_LicensingStCd; 
 v_LocId1 := v_LocId;
 v_LicensingStCd1 := v_LicensingStCd;
 WHILE v_cnt >= 2 
 loop
 FETCH procur 
 INTO v_LocId, v_LicensingStCd; 
 IF v_LocId = v_LocId1 then
 v_LicensingStCd1 := v_LicensingStCd1 || ',' || v_LicensingStCd ;
 ELSE
 BEGIN
 INSERT 
 INTO partner.TT_tmp2 VALUES (v_LocId1, v_LicensingStCd1);
 v_LocId1 := v_LocId;
 v_LicensingStCd1 := v_LicensingStCd;
 END;
 end if;
 v_cnt := v_cnt - 1 ;
 END loop;
 close procur;
 INSERT 
 INTO partner.TT_tmp2 VALUES (v_LocId1, v_LicensingStCd1);/* Adaptive Server has expanded all '*' elements in the following statement */ 
 ---
 open p_SelCur for
 SELECT a.PrtAliasNm,a.PrtLocNm, upper(a.PrtLglNm)as PrtLglNm, a.LocId, a.ValidPrtSubCatTyp, a.PhyAddrStr1Txt, a.PhyAddrStr2Txt, a.PhyAddrCtyNm, a.PhyAddrStCd, a.PhyAddrPostCd ,
 b.LicensingStCd 
 FROM partner.TT_tmp1 a left outer join partner.TT_tmp2 b on a.LocId = b.LocId 
 ORDER BY upper(a.PrtLglNm) ASC;
 p_RetVal := SQL%rowcount;
 
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:47. --->
PROCEDURE VALIDLOCTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidLocTyp VARCHAR2 :=NULL,
 p_ValidLocTypDispOrdNmb NUMBER:=0,
 p_ValidLocTypDesc VARCHAR2 :=NULL,
 p_ValidLocTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidLocTypIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidLocTypTbl 
 (
 ValidLocTyp, 
 ValidLocTypDispOrdNmb, 
 ValidLocTypDesc, 
 ValidLocTypCreatUserId
 )
 VALUES (
 p_ValidLocTyp, 
 p_ValidLocTypDispOrdNmb, 
 p_ValidLocTypDesc, 
 p_ValidLocTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDLOCTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:32. --->
PROCEDURE PRTCNTCTSBAROLETYPUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctSBARoleTypTxt VARCHAR2 := NULL,
 p_PrtSBARoleTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtCntctSBARoleTypUpd;
 IF p_Identifier = 0 THEN
 /* Updert into PrtCntctSBARoleTyp Table */
 BEGIN
 UPDATE PARTNER.PrtCntctSBARoleTypTbl
 SET PrtCntctSBARoleTypTxt = p_PrtCntctSBARoleTypTxt, 
 PrtSBARoleTypCreatUserId = p_PrtSBARoleTypCreatUserId,
 PrtSBARoleTypCreatDT =SYSDATE
 WHERE PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtCntctSBARoleTypUpd;
 RAISE;
 
 END PRTCNTCTSBAROLETYPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


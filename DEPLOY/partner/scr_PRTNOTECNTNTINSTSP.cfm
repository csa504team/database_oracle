<!--- Saved 05/05/2015 13:18:47. --->
PROCEDURE PRTNOTECNTNTINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtNoteSeqNmb NUMBER := 0,
 p_PrtNoteCntntSeqNmb NUMBER := 0,
 p_PrtNoteCntntTxt VARCHAR2 := NULL,
 p_PrtNoteCntntCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtNoteCntntIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtNoteCntnt Table */
 BEGIN
 INSERT INTO PARTNER.PrtNoteCntntTbl 
 (LocId, PrtNoteSeqNmb, PrtNoteCntntSeqNmb, PrtNoteCntntTxt, PrtNoteCntntCreatUserId,
 PrtNoteCntntCreatDt,lastupdtuserid,lastupdtdt)
 SELECT p_LocId, p_PrtNoteSeqNmb, NVL(MAX(PrtNoteCntntSeqNmb),0) + 1, p_PrtNoteCntntTxt,
 p_PrtNoteCntntCreatUserId, SYSDATE,p_PrtNoteCntntCreatUserId, SYSDATE 
 FROM PrtNoteCntntTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteCntntIns;
 RAISE;
 
 END PRTNOTECNTNTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


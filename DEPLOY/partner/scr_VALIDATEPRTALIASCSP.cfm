<!--- Saved 05/05/2015 13:19:38. --->
PROCEDURE VALIDATEPRTALIASCSP(
 p_PrtId NUMBER := 0,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_PRTALIASCREATUSERID VARCHAR2 := NULL,
 p_ErrSeqNmb OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 p_CheckStatusCd char(1);
 BEGIN
 p_ErrSeqNmb := 0;
 
 IF p_ValidPrtAliasTyp = 'TIN' THEN
 BEGIN
 SBAREF.CHECKTAXIDCSP(p_PrtAliasNm,p_CheckStatusCd,null);
 
 IF p_CheckStatusCd != 'Y' THEN
 p_ErrSeqNmb := p_ErrSeqNmb + 1;
 PRTVALIDATIONERRINSCSP(p_PrtId,109,p_PRTALIASCREATUSERID,p_PrtAliasNm);
 END IF;
 END;
 END IF;
 
 OPEN p_SelCur for select * from partner.PRTVALIDATIONERRTBL WHERE PrtId = p_PrtId; 
 END VALIDATEPRTALIASCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


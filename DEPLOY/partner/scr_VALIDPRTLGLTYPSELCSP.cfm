<!--- Saved 05/05/2015 13:19:59. --->
PROCEDURE VALIDPRTLGLTYPSELCSP
 (
 p_userName VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrtLglTyp, 
 ValidPrtLglTypDesc
 FROM PARTNER.ValidPrtLglTypTbl 
 WHERE ValidPrtLglTypDispOrdNmb != 0 
 ORDER BY ValidPrtLglTypDispOrdNmb ;
 p_RetVal := SQL%ROWCOUNT; 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 raise_application_error(-20001, 'Check error');
 -- ROLLBACK TO VALIDPRTLGLTYPSELCSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


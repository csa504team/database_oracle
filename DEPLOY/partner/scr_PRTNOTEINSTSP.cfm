<!--- Saved 05/05/2015 13:18:51. --->
PROCEDURE PRTNOTEINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtNoteSeqNmb NUMBER := 0,
 p_ValidPrtNoteSubjTypCd NUMBER := 0,
 p_PrtNoteSubjTxt VARCHAR2 := NULL,
 p_IMUserNm CHAR := NULL,
 p_PrtNoteEffDt DATE := NULL,
 p_PrtNoteCreatUserId VARCHAR2 := NULL)
 AS
 
 /* SB -- Changed from Employeeid to IMUSER as per Java Req */
 BEGIN
 SAVEPOINT PrtNoteIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtNote Table */
 BEGIN
 INSERT INTO PARTNER.PrtNoteTbl 
 (LocId, PrtNoteSeqNmb, ValidPrtNoteSubjTypCd, PrtNoteSubjTxt,IMUserNm, PrtNoteEffDt,
 PrtNoteCreatUserId, PrtNoteCreatDt,lastupdtuserid,lastupdtdt)
 SELECT p_LocId, NVL(MAX(PrtNoteSeqNmb), 0) + 1, p_ValidPrtNoteSubjTypCd,p_PrtNoteSubjTxt,p_IMUserNm,
 p_PrtNoteEffDt,p_PrtNoteCreatUserId,SYSDATE,p_PrtNoteCreatUserId,SYSDATE 
 FROM PrtNoteTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteIns;
 RAISE;
 
 END PRTNOTEINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:31:59. --->
PROCEDURE PRTHISTRYINSTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtHistryEffDt DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_PrtHistryPartyTyp CHAR := NULL,
 p_PrtHistryInitPrtId NUMBER := 0,
 p_PrtHistryResultingPrtId NUMBER := 0,
 p_PrtHistryChrtrTyp VARCHAR2 := NULL,
 p_PrtHistryEstbDt DATE := NULL,
 p_PrtHistryLglNm VARCHAR2 := NULL,
 p_PrtHistryPrimTyp VARCHAR2 := NULL,
 p_PrtHistrySubCatTyp VARCHAR2 := NULL,
 p_PrtHistryReglAgencyNm VARCHAR2 := NULL,
 p_PrtHistryTFPHistryDt DATE := NULL,
 p_AFFECTED_INST_ID NUMBER := 0,
 p_EFFECTIVE_DATE DATE := NULL,
 p_PARTY_TYPE_INDICATOR CHAR := NULL,
 p_CHANGE_CODE VARCHAR2 := NULL,
 p_PrtHistryCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PrtHistryIns;
 
 IF p_Identifier = 0
 THEN
 /* Insert into PrtHistryTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtHistryTbl (PrtId,
 PrtHistryEffDt,
 ValidChngCd,
 PrtHistryPartyTyp,
 PrtHistryInitPrtId,
 PrtHistryResultingPrtId,
 PrtHistryChrtrTyp,
 PrtHistryEstbDt,
 PrtHistryLglNm,
 PrtHistryPrimTyp,
 PrtHistrySubCatTyp,
 PrtHistryReglAgencyNm,
 PrtHistryTFPHistryDt,
 AFFECTED_INST_ID,
 EFFECTIVE_DATE,
 PARTY_TYPE_INDICATOR,
 CHANGE_CODE,
 PrtHistryCreatUserId,
 PrtHistryCreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT)
 VALUES (p_PrtId,
 p_PrtHistryEffDt,
 p_ValidChngCd,
 p_PrtHistryPartyTyp,
 p_PrtHistryInitPrtId,
 p_PrtHistryResultingPrtId,
 p_PrtHistryChrtrTyp,
 p_PrtHistryEstbDt,
 p_PrtHistryLglNm,
 p_PrtHistryPrimTyp,
 p_PrtHistrySubCatTyp,
 p_PrtHistryReglAgencyNm,
 p_PrtHistryTFPHistryDt,
 p_AFFECTED_INST_ID,
 p_EFFECTIVE_DATE,
 p_PARTY_TYPE_INDICATOR,
 p_CHANGE_CODE,
 p_PrtHistryCreatUserId,
 SYSDATE,
 p_PrtHistryCreatUserId,
 SYSDATE);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtHistryIns;
 RAISE;
 END PRTHISTRYINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


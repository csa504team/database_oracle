<!--- Saved 05/05/2015 13:18:24. --->
PROCEDURE PRTLOCHISTAIRECRDCSP(
 p_ReportBegDateTime IN DATE DEFAULT NULL,
 p_ReportEndDateTime IN DATE DEFAULT NULL,
 p_ValidChngCd IN VARCHAR2 DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 StoO_rowcnt NUMBER;
 BEGIN
 SAVEPOINT PrtLocHistAIRecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd 
 THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt >= p_ReportBegDateTime 
 AND a.LocHistryCreatDt <= p_ReportEndDateTime;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE 
 WHEN a.LocHistryCntCd!=i.LocHistryCntCd THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt >= p_ReportBegDateTime;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt <= p_ReportEndDateTime;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(132)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 a.LocHistryStr1Txt,
 a.LocHistryStr2Txt,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd 
 THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' );
 /*BEGIN
 SELECT 1 INTO StoO_rowcnt FROM DUAL
 WHERE EXISTS (
 SELECT a.LocHistryAffctPrtId, a.LocId, a.LocHistryLocNm 
 AffectedTitle, a.LocHistryStr1Txt, a.LocHistryStr2Txt, 
 a.LocHistryCtyNm AffectedCity, a.LocHistryStCd AffectedState, 
 i.LocHistryLocNm InitiatorTitle, i.LocHistryCtyNm InitiatorCity, 
 i.LocHistryStCd InitiatorState, CASE 
 WHEN a.LocHistryCntCd!=i.LocHistryCntCd THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry, RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 
 10, ' ') EffectiveDate, a.LocHistryCreatDt DateAddedToPIMS 
 FROM LocHistryTbl a, LocHistryTbl i 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND 
 (i.LocHistryPartyTyp = 'I' 
 AND i.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' ) );
 END;*/
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescTypSel;
 RAISE;
 
 END PRTLOCHISTAIRECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


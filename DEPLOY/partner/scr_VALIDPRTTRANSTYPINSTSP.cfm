<!--- Saved 05/05/2015 13:20:12. --->
PROCEDURE VALIDPRTTRANSTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 ValidPrtTransTyp VARCHAR2 :=NULL,
 ValidPrtTransTypDispOrdNmb NUMBER:=0,
 ValidPrtTransTypDesc VARCHAR2 :=NULL,
 ValidPrtTransTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtTransTypIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtTransTypTbl 
 (
 ValidPrtTransTyp, 
 ValidPrtTransTypDispOrdNmb, 
 ValidPrtTransTypDesc, 
 ValidPrtTransTypCreatUserId
 )
 VALUES (
 ValidPrtTransTyp, 
 ValidPrtTransTypDispOrdNmb, 
 
 ValidPrtTransTypDesc, 
 ValidPrtTransTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTTRANSTYPINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


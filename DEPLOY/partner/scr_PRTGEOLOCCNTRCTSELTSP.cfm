<!--- Saved 05/05/2015 13:17:38. --->
PROCEDURE PRTGEOLOCCNTRCTSELTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_LocId IN NUMBER := 0,
 p_PrtCntCtnmb IN NUMBER := 0,
 p_RetVal OUT NUMBER ,
 p_SelCur OUT SYS_REFCURSOR
 )
 
 AS
 /* SB -- 10/28/2014 -- Created for LINC Initial Lender Email
 PSP -- 12/16/2014 -- Added id= 14,15,16 */
 BEGIN
 SAVEPOINT PRTGEOLOCCNTRCTSELTSP;
 IF p_Identifier = 0 
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT LOCID,PRTCNTCTNMB,STCD,
 CNTYCD,CREATUSERID,CREATDT
 FROM PARTNER.PRTGEOLOCCNTRCTTBL
 WHERE Locid = p_LocId;
 p_RetVal := SQL%ROWCOUNT ;
 END;
 ELSIF p_Identifier = 2 
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PARTNER.PRTGEOLOCCNTRCTTBL
 WHERE Locid = p_LocId;
 p_RetVal := SQL%ROWCOUNT ;
 END;
 ELSIF p_Identifier = 11 /*for Linc Areas of operation:*/ 
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 Select STCD,COUNT(*) AS CNT
 from PARTNER.PRTGEOLOCCNTRCTTBL
 WHERE LocId = p_LocId AND PrtCntCtnmb=p_PrtCntCtnmb
 GROUP BY STCD
 ORDER BY STCD;
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 ELSIF p_Identifier = 12 /*To return all counties and a flag for a given location contact and will be passed to Flash using web service */ 
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 Select c.STCD,
 c.CNTYCD,
 c.CNTYNM,
 NVL((Select 'true' FROM PARTNER.PRTGEOLOCCNTRCTTBL g
 WHERE g.LocId = p_LocId 
 AND PRTCNTCTNMB= p_PrtCntCtnmb
 AND g.STCD=c.STCD AND g.CNTYCD=c.CNTYCD),'false') AS Selected
 from sbaref.cntytbl c WHERE c.CNTYENDDT Is Null
 ORDER BY c.STCD,c.CNTYCD;
 
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 ELSIF p_Identifier = 13
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 
 Select s.STCD,
 s.STNM,
 (Select count(*) FROM sbaref.cntytbl c where c.STCD=s.STCD AND CNTYENDDT is null) AS CNTYS,
 (Select COUNT(*) FROM PARTNER.PRTGEOLOCCNTRCTTBL g
 WHERE g.LocId = p_LocId 
 AND PRTCNTCTNMB= p_PrtCntCtnmb
 AND g.STCD=s.STCD ) AS Selected
 from sbaref.sttbl s
 GROUP BY s.STNM, s.STCD
 ORDER BY s.STNM, s.STCD;
 
 
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 ELSIF p_Identifier = 14
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 
 Select c.STCD,s.STNM,COUNT(*) AS CNT
 from PARTNER.PRTGEOLOCCNTRCTTBL c,sbaref.StTbl s
 WHERE c.LocId = p_LocId AND c.PrtCntCtnmb=p_PrtCntCtnmb
 AND c.StCd = s.STCd
 GROUP BY c.STCD,s.STNM
 ORDER BY c.STCD,s.STNM;
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 ELSIF p_Identifier = 15
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 
 Select STCD, CNTYCD
 from PARTNER.PRTGEOLOCCNTRCTTBL
 WHERE LocId = p_LocId AND PrtCntCtnmb=p_PrtCntCtnmb;
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 
 ELSIF p_Identifier = 16
 THEN
 
 BEGIN
 OPEN p_SelCur FOR
 
 Select G.locid,c.PrtCntctFirstNm,c.PrtCntctLastNm,G.PrtCntCtnmb,G.STCD, CNTYCD
 from PARTNER.PRTGEOLOCCNTRCTTBL G, PARTNER.PrtLocCntctTbl c
 WHERE g.LocId = p_LocId
 and G.locid=c.locid 
 and G.PRTCNTCTNMB=C.PRTCNTCTNMB
 ORDER BY G.locid,c.PrtCntctFirstNm,c.PrtCntctLastNm,G.PrtCntCtnmb,STCD,CNTYCD;
 
 
 p_RetVal := SQL%ROWCOUNT ;
 END; 
 
 
 END IF;
 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


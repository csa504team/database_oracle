<!--- Saved 05/02/2016 12:49:58. --->
PROCEDURE LOCHISTRYBYPRTSELCSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_ValidChngCd VARCHAR2 := NULL,
 p_LocHistryEffDt DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT LocHistryByPrtSelCSP;
 
 BEGIN EXECUTE IMMEDIATE 'truncate table partner.TEMPLOCHISTBYPRTTBL'; END;
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 INSERT INTO partner.TEMPLOCHISTBYPRTTBL
 SELECT DISTINCT LocHistryAffctPrtId,LocId,LocHistryEffDt, ValidChngCd,LocHistryInitPrtId
 FROM partner.LocHistryTbl 
 WHERE LocHistryAffctPrtId= p_PrtId
 AND ValidChngCd = p_ValidChngCd
 AND trunc(LocHistryEffDt)= p_LocHistryEffDt;
 
 END;
 else
 BEGIN
 INSERT INTO partner.TEMPLOCHISTBYPRTTBL
 SELECT DISTINCT LocHistryAffctPrtId,LocId,LocHistryEffDt, ValidChngCd,LocHistryInitPrtId
 FROM partner.LocHistryTbl 
 WHERE LocHistryAffctPrtId= p_PrtId;
 END;
 END IF;
 
 OPEN p_SelCur FOR 
 SELECT t.LocHistryAffctPrtId,i.LocId As IntitatorLocId,
 i.LocHistryLocNm as InitiatorTitle, i.LocHistryCtyNm As InitiatorCity,
 i.LocHistryStr1Txt As InitiatorStreet1,i.LocHistryStr2Txt As InitiatorStreet2,
 i.LocHistryStCd As InitiatorStCd, i.LocHistryPostCd As InitiatorPostalCd,
 i.LocHistryFIPSCntyCd As InitiatorFIPSCntyCd,i.LocHistryLocTyp As InitiatorLocType,
 i.LocHistryInitPrtId As InitiatorInitPrtId,i.LocHistryResultingPrtId AS InitiatorResultingPrtId,
 v.ValidChngCdDesc,a.LocHistryLocNm as AffectedTitle,
 a.LocHistryCtyNm As AffectedCity,a.LocHistryStCd As AffectedStCd,
 a.LocHistryStr1Txt As AffectedStreet1,a.LocHistryStr2Txt As AffectedStreet2,
 a.LocHistryPostCd As AffectedPostalCd,a.LocHistryFIPSCntyCd As AffectedFIPSCntyCd,
 a.LocHistryLocTyp As AffectedLocType,a.LocHistryInitPrtId As AffectedInitPrtId,
 a.LocHistryResultingPrtId AS AffectedResultingPrtId,a.LocId As AffectedLocId,
 a.LocHistryChngDesc,r.LocHistryLocNm as ResultingTitle,
 r.LocHistryCtyNm As ResultingCity,r.LocHistryStCd As ResultingStCd,
 r.LocHistryStr1Txt As ResultingStreet1,r.LocHistryStr2Txt As ResultingStreet2,
 r.LocHistryPostCd As ResultingPostalCd,r.LocHistryFIPSCntyCd As ResultingFIPSCntyCd,
 r.LocHistryLocTyp As ResultingLocType, r.LocHistryInitPrtId As ResultingInitPrtId,
 r.LocHistryResultingPrtId AS ResultingResultingPrtId,r.LocId As ResultingLocId, 
 To_char(t.LocHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 t.LocId,t.ValidChngCd,v.ValidChngCdDispOrdNmb, a.LocHistryCreatDt as DateAddedToPIMS,
 case when i.LocHistryPartyTyp = 'I' then 'I' when a.LocHistryPartyTyp = 'A' then 'A' when r.LocHistryPartyTyp = 'R' then 'R' end as PartyTyp,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = i.LocHistryLocTyp) as InitiatorLocTypeDesc,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = a.LocHistryLocTyp) as AffectedLocTypeDesc,
 (select ValidLocTypDesc from partner.ValidLocTypTbl where ValidLocTyp = r.LocHistryLocTyp) as ResultingLocTypeDesc
 FROM partner.TEMPLOCHISTBYPRTTBL t, partner.LocHistryTbl a,partner.LocHistryTbl i,partner.LocHistryTbl r,partner.ValidChngCdTbl v
 WHERE ( t.LocHistryAffctPrtId = a.LocHistryAffctPrtId(+) AND t.LocId = a.LocId(+) 
 AND t.LocHistryEffDt = a.LocHistryEffDt(+) AND a.LocHistryPartyTyp = 'A'
 AND t.ValidChngCd = a.ValidChngCd(+)) AND ( t.LocHistryAffctPrtId = i.LocHistryAffctPrtId(+)
 AND t.LocId = i.LocId(+) AND t.LocHistryEffDt = i.LocHistryEffDt(+) AND i.LocHistryPartyTyp = 'I'
 AND t.ValidChngCd = i.ValidChngCd(+)) AND ( t.LocHistryAffctPrtId = r.LocHistryAffctPrtId(+)
 AND t.LocId = r.LocId(+) AND t.LocHistryEffDt = r.LocHistryEffDt(+) AND r.LocHistryPartyTyp = 'R'
 AND t.ValidChngCd = r.ValidChngCd(+)) AND t.ValidChngCd = v.ValidChngCd(+)
 ORDER BY t.LocHistryEffDt DESC,t.ValidChngCd,
 CASE WHEN a.LocHistryLocTyp = 'HO' then 0 else 1 end; 
 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO SAVEPOINT LocHistryByPrtSelCSP;
 RAISE;
 
 
 END LocHistryByPrtSelCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


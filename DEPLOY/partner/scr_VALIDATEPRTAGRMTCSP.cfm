<!--- Saved 05/05/2015 13:19:37. --->
PROCEDURE VALIDATEPRTAGRMTCSP(
 p_PrtId NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtDocLocOfcCd CHAR := NULL,
 p_PrtAgrmtAreaOfOperTxt VARCHAR2 := NULL,
 p_PrtAgrmtDocLocTxt VARCHAR2 := NULL,
 p_PrtAgrmtEffDt DATE := NULL,
 p_PrtAgrmtExprDt DATE := NULL,
 p_PrtAgrmtTermDt DATE := NULL,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtWvrInd CHAR := NULL,
 p_PrtAgrmtCreatUserId VARCHAR2 := NULL,
 p_LendrDirectAuthInd char := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 v_ValidPrtSubCatTyp varchar2(5);
 v_MaxTermMonthQty number(3);
 v_cnt number(11);
 v_today DATE;
 BEGIN
 v_today := trunc(SYSDATE);
 
 DELETE partner.PRTVALIDATIONERRTBL WHERE PrtId = p_PrtId;
 
 BEGIN
 SELECT p.ValidPrtSubCatTyp INTO v_ValidPrtSubCatTyp FROM PrtTbl p WHERE p.PrtId = p_PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,101,p_PrtAgrmtCreatUserId);
 END;
 
 BEGIN
 SELECT prog.PrgrmMaxTrmMoQty INTO v_MaxTermMonthQty FROM PrgrmTbl prog WHERE prog.PrgrmId = p_PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,102,p_PrtAgrmtCreatUserId, p_PrgrmId);
 END;
 
 --Check compatibility of Subcat and Program.
 SELECT count(*) into v_cnt FROM ValidPrgrmSubCatTbl v
 WHERE v.PrgrmId = p_PrgrmId
 AND v.ValidPrtSubCatTyp = v_ValidPrtSubCatTyp;
 
 IF v_cnt = 0
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,103,p_PrtAgrmtCreatUserId, p_PrgrmId, v_ValidPrtSubCatTyp);
 END IF;
 
 /* Check to see that the Expiration Date doesn't exceed the maximumm term limit */
 IF p_PrtAgrmtExprDt IS NOT NULL
 THEN
 --Validate dates.
 IF p_PrtAgrmtEffDt >= p_PrtAgrmtExprDt
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,104,p_PrtAgrmtCreatUserId, to_char(p_PrtAgrmtExprDt,'mm/dd/yyyy'), to_char(p_PrtAgrmtEffDt,'mm/dd/yyyy'));
 END IF;
 
 IF v_MaxTermMonthQty IS NOT NULL AND trunc(MONTHS_BETWEEN(p_PrtAgrmtExprDt,p_PrtAgrmtEffDt)) > v_MaxTermMonthQty
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,105,p_PrtAgrmtCreatUserId, to_char(trunc(MONTHS_BETWEEN(p_PrtAgrmtExprDt,p_PrtAgrmtEffDt))), to_char(v_MaxTermMonthQty));
 END IF; /* End DATEDIFF(mm, p_PrtAgrmtEffDt, p_PrtAgrmtExprDt) > v_MaxTermMonthQty */
 END IF;
 
 /* Check to see if this is Active agreement no other active agreement exists */
 IF (p_PrtAgrmtEffDt <= SYSDATE and (p_PrtAgrmtExprDt >= v_today or p_PrtAgrmtExprDt IS NULL) and
 (p_PrtAgrmtTermDt > SYSDATE or p_PrtAgrmtTermDt IS NULL))
 THEN
 select count(*) into v_cnt from PrtAgrmtTbl
 where PrtId = p_PrtId
 and PrgrmId = p_PrgrmId
 and (PrtAgrmtEffDt <= SYSDATE and (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt IS NULL) 
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL));
 IF v_cnt > 0
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,106,p_PrtAgrmtCreatUserId, to_char(p_PrtAgrmtEffDt,'mm/dd/yyyy'));
 END IF;
 END IF;
 
 /* Check to see if this is Pending agreement no other pending agreement exists */
 IF (p_PrtAgrmtEffDt > SYSDATE)
 THEN
 select count(*) into v_cnt from PrtAgrmtTbl
 where PrtId = p_PrtId
 and PrgrmId = p_PrgrmId
 and PrtAgrmtEffDt > SYSDATE;
 
 IF v_cnt > 0
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,107,p_PrtAgrmtCreatUserId, to_char(p_PrtAgrmtEffDt,'mm/dd/yyyy'));
 END IF;
 
 /* Check to see this pending agreement period doesn't overlap with the active agreement */
 select count(*) into v_cnt from PrtAgrmtTbl where PrtId = p_PrtId and PrgrmId = p_PrgrmId
 and (PrtAgrmtExprDt >= p_PrtAgrmtEffDt or PrtAgrmtExprDt IS NULL)
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL);
 
 IF v_cnt > 0
 THEN
 PRTVALIDATIONERRINSCSP(p_PrtId,108,p_PrtAgrmtCreatUserId, to_char(p_PrtAgrmtEffDt,'mm/dd/yyyy'));
 END IF;
 END IF;
 
 OPEN p_SelCur for select * from partner.PRTVALIDATIONERRTBL WHERE PrtId = p_PrtId;
 END VALIDATEPRTAGRMTCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


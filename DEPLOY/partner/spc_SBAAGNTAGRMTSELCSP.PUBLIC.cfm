<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by sherryLiu. 
DATE:				09/15/2017.
DESCRIPTION:		Standardized call to SBAAGNTAGRMTSELCSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	09/15/2017, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 49)
	and	(Left(CGI.Script_Name,    49) is "/cfincludes/oracle/partner/spc_SBAAGNTAGRMTSELCSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"			default="">
<cfparam name="Variables.ErrMsg"				default="">
<cfparam name="Variables.SkippedSpcs"			default="">
<cfparam name="Variables.TxnErr"				default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs				= ListAppend(Variables.SkippedSpcs, "SBAAGNTAGRMTSELCSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"			default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"			default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"			default="call SBAAGNTAGRMTSELCSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName				= "SBAAGNTAGRMTSELCSP">
	<cfparam name="Variables.IDENTIFIER"		default="0">
	<cfparam name="Variables.RETVAL"			default="">
	<cfparam name="Variables.SbaAgntHQlocid"	default="">
	<cftry>
		<cfstoredproc procedure="PARTNER.SBAAGNTAGRMTSELCSP" datasource="#Variables.db#">
		<cfif Len(Variables.IDENTIFIER) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_IDENTIFIER"
											value="#Variables.IDENTIFIER#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_IDENTIFIER"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfprocparam		type="Out"		dbvarname=":p_RETVAL"
											variable="Variables.RETVAL"
											cfsqltype="CF_SQL_NUMBER">
		<cfif Len(Variables.SbaAgntHQlocid) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_SbaAgntHQlocid"
											value="#Variables.SbaAgntHQlocid#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_SbaAgntHQlocid"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


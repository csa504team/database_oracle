<!--- Saved 05/05/2015 13:16:57. --->
PROCEDURE PRTAGRMTTERMRSNCATCDINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCatCd NUMBER := 0,
 p_PrtAgrmtTermRsnCatDescTxt VARCHAR2 := NULL,
 p_PrtAgrmtTrmRsnCatCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCatCdIns;
 
 IF p_Identifier = 0 THEN
 /* Insert into PrtAgrmtTermRsnCatCdTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtAgrmtTermRsnCatCdTbl 
 (PrtAgrmtTermRsnCatCd, PrtAgrmtTermRsnCatDescTxt, 
 PrtAgrmtTermRsnCatCreatUserId, PrtAgrmtTermRsnCatCreatDt
 )
 VALUES (p_PrtAgrmtTermRsnCatCd,p_PrtAgrmtTermRsnCatDescTxt,p_PrtAgrmtTrmRsnCatCreatUserId,SYSDATE);
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PhyAddrIns;
 RAISE;
 
 END PRTAGRMTTERMRSNCATCDINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


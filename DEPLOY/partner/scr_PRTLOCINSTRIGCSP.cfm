<!--- Saved 01/25/2018 10:37:58. --->
PROCEDURE PRTLOCINSTRIGCSP (
 NewDataRec PrtLocTbl%ROWTYPE)
 AS
 /*
 Revision History -----------------------------------------
 08/13/2001 Ian Clark Commented out the code limiting FIRS number generation to SBA.
 FIRS Numbers will now be generated for ALL Inserts.
 04/30/2001 Ian Clark Uncommented the code so we don't generate for Thomson data.
 04/25/2001 Rajeswari DSP Generating FIRS No.s for all records
 even for thomson records
 04/18/2001 Rajeswari DSP Primary Typ of Microlender has been
 changed from 'Lendr' to 'NBLDR' and
 including all lenders and NBLDR expect
 Microlender for FIRS extraction.
 04/05/2001 Rajeswari DSP Modified code to include RolePrivilege
 in search tables.
 01/18/2001 Rajeswari DSP Removed createuser and createdate
 columns while generating FIRS No.
 01/16/2001 Rajeswari DSP Added code to generate FIRS No. for
 SBA specific records.
 08/28/2000 C.Woodard Added code to insert LocSrchTbl.
 04/25/2000 C.Woodard Initialized @ErrorNmb. Removed
 sections of logic that were
 being performed redundantly in
 the U_PartnerLocation trigger
 which is fired (nested) by this
 trigger.
 04/18/2000 C.Woodard Removed cursor processing.
 02/26/2000 C.Woodard Modified logic to enforce business rule:
 Allow only one 'HO' type location for
 a given Partner.
 01/17/2000 C.Woodard Modified to only set FIRS#, FIRSRecordTyp
 IF PrimaryTyp = 'HC'
 OR (PrimaryTyp = 'Lendr'
 AND SubcategoryTyp != 'Micro')
 Modified By: APK -- 08/02/2010-- Modified to include TRIM for the returned FIRSNmb as the returned variable holds spaces in Oracle if returned as CHAR.
 
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtDataSourcId,PhyAddrDataSourcId
 
 */
 -- ----------------------------------------------------------
 v_ErrorNmb NUMBER (5);
 v_FIRSNmb CHAR (7);
 v_LastUpdtUserId VARCHAR2 (32);
 v_FIRSRecordTyp CHAR (2);
 v_INST_ID NUMBER (8);
 v_LOC_ID NUMBER (8);
 v_LocationId NUMBER (7);
 v_LocationTyp VARCHAR2 (5);
 v_PartnerId NUMBER (7);
 v_PrimaryTyp VARCHAR2 (5);
 v_ProcessingMode CHAR (1); -- 'N'-Normal or 'I'-Initial Load
 v_OutFIRSRecordTyp CHAR (2);
 v_ReplicationErrorMessageTxt VARCHAR2 (255);
 v_Rowcount NUMBER (12);
 v_SubcategoryTyp VARCHAR2 (5);
 v_RolePrivilege VARCHAR2 (20);
 v_DataSource VARCHAR2 (20);
 v_PrtLocNm VARCHAR2 (200);
 v_PrtLocSrchNm VARCHAR2 (200);
 -- Temporary variables
 v_cnt NUMBER (11);
 OldLocDataRec partner.PrtLocTbl%ROWTYPE;
 NewLocDataRec partner.PrtLocTbl%ROWTYPE;
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 /**** SET MODE TO 'I' FOR INITIAL LOAD PROCESSING !! ***/
 /**** SET MODE TO 'N' AFTER INITIAL LOAD IS COMPLETE!! ***/
 -- SELECT v_ProcessingMode = 'I' -- 'N'-Normal or 'I'-Initial Load
 v_ProcessingMode := 'N'; -- 'N'-Normal or 'I'-Initial Load
 v_ErrorNmb := 0;
 v_INST_ID := 0;
 v_LocationId := NewDataRec.LocId;
 v_PrtLocNm := NewDataRec.PrtLocNm;
 v_PrtLocSrchNm := UPPER (NewDataRec.PrtLocNm);
 v_FIRSNmb := NewDataRec.PrtLocFIRSNmb;
 v_LastUpdtUserId := NewDataRec.LastUpdtUserId;
 v_PartnerId := NewDataRec.PrtId;
 v_LocationTyp := NewDataRec.ValidLocTyp;
 v_LOC_ID := NewDataRec.LOC_ID;
 v_INST_ID := NVL (NewDataRec.INST_ID, 0);
 
 --Ensure only one head office location per Partner.
 SELECT COUNT (*)
 INTO v_cnt
 FROM partner.PrtLocTbl l
 WHERE l.ValidLocTyp = 'HO' AND l.PrtId = v_PartnerId;
 
 IF v_cnt > 1
 THEN
 BEGIN
 -- get old snapshot
 SELECT *
 INTO OldLocDataRec
 FROM partner.PrtLocTbl
 WHERE LocId = v_LocationId;
 
 DELETE FROM partner.PrtLocTbl l
 WHERE l.LocId = v_LocationId;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; --Use system message
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END;
 ELSE -- call del trigger
 BEGIN
 PrtLocDelTrigCSP (OldLocDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END; -- end trigger
 END IF; --SQL Error
 
 v_ErrorNmb := 31203;
 v_ReplicationErrorMessageTxt :=
 'Multiple head office locations not allowed.'
 || ' PartnerLocation not inserted.'
 || 'LocId was '
 || TO_CHAR (v_LocationId)
 || '.';
 partner.PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Multiple head office error.
 ELSE
 BEGIN
 --No multiple head office error.
 --Determine location eligibility for FIRS processing.
 BEGIN
 SELECT p.ValidPrtPrimCatTyp
 INTO v_PrimaryTyp
 FROM partner.PrtTbl p
 WHERE p.PrtId = v_PartnerId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_PrimaryTyp := NULL;
 END;
 
 BEGIN
 SELECT p.ValidPrtSubCatTyp
 INTO v_SubcategoryTyp
 FROM partner.PrtTbl p
 WHERE p.PrtId = v_PartnerId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_SubcategoryTyp := NULL;
 END;
 
 
 
 SELECT COUNT (*)
 INTO v_cnt
 FROM ValidPrtSubCatTbl
 WHERE ValidPrtSubCatFIRSInd = 'Y'
 AND ValidPrtSubCatTyp = v_SubcategoryTyp;
 
 IF v_cnt > 0
 THEN
 BEGIN
 v_FIRSNmb := NULL;
 v_OutFIRSRecordTyp := NULL;
 
 IF v_ProcessingMode = 'N'
 THEN
 BEGIN --Normal mode
 -- Get new FIRS number.
 partner.GenerateFIRSNmbCSP (v_FIRSNmb, v_LastUpdtUserId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt :=
 'FIRS Number not generated properly.';
 PrtReplicationErrInsCSP (
 v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 31201,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; -- Error.
 END IF;
 END; -- Get new FIRS number.
 END IF;
 
 -- Set values of FIRS Number and FIRSRecordTyp.
 -- (FIRSRecordTyp will be set in the
 -- U_PartnerLocation trigger, which will also
 -- add the location to extract queue.)
 IF v_ErrorNmb = 0
 THEN
 BEGIN
 --get old snapshot
 SELECT *
 INTO OldLocDataRec
 FROM partner.PrtLocTbl
 WHERE LocId = v_LocationId;
 
 UPDATE partner.PrtLocTbl l
 SET PrtLocFIRSNmb = TRIM (v_FIRSNmb)
 WHERE l.LocId = v_LocationId;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := SQLCODE;
 v_ReplicationErrorMessageTxt := NULL; --Use system message
 PrtReplicationErrInsCSP (
 v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; -- Error
 ELSE -- call upd trigger
 BEGIN
 -- get new snapshot
 SELECT *
 INTO NewLocDataRec
 FROM partner.PrtLocTbl
 WHERE LocId = v_LocationId;
 
 PrtLocUpdTrigCSP (OldLocDataRec, NewLocDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end upd trigger
 END; -- Update
 END IF;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31204;
 v_ReplicationErrorMessageTxt :=
 'Unable to set FIRS Number and/or FIRSRecordTyp.';
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; -- Error
 END IF;
 END; --Cases for FIRS processing
 END IF;
 END; --No multiple head office error.
 END IF;
 
 IF SUBSTR (v_PrtLocNm, 1, 4) = 'The '
 THEN
 v_PrtLocSrchNm :=
 LTRIM (
 SUBSTR (v_PrtLocSrchNm, 4, LENGTH (v_PrtLocSrchNm) - 3)
 || ' ,'
 || SUBSTR (v_PrtLocSrchNm, 1, 4));
 END IF;
 
 --Add location data to location search table.
 INSERT INTO LocSrchTbl (LocId,
 PrtId,
 ValidPrtSubCatTyp,
 PrtLocNm,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp,
 PrtLocOpndDt,
 PrtLocInactDt,
 PrtLocCurStatCd,
 ValidLocTyp,
 ValidLocTypDispOrdNmb,
 -- ValidPrtRoleTyp,
 ValidAddrTyp,
 PhyAddrStr1Txt,
 PhyAddrStr2Txt,
 PhyAddrCtyNm,
 PhyAddrStCd,
 PhyAddrPostCd,
 PhyAddrFIPSCntyCd,
 PhyAddrCntCd,
 PhyAddrUSDpndcyInd,
 PrtLocSrchNm,
 PhyAddrCtySrchNm,
 LocSrchCreatUserId,
 LocSrchCreatDt,
 LastUpdtUserId,
 LastUpdtDt)
 SELECT l.LocId,
 p.PrtId,
 p.ValidPrtSubCatTyp,
 l.PrtLocNm,
 l.PrtLocFIRSNmb,
 l.PrtLocFIRSRecrdTyp,
 l.PrtLocInactDt,
 l.PrtLocOpndDt,
 l.PrtLocCurStatCd,
 l.ValidLocTyp,
 v.ValidLocTypDispOrdNmb,
 -- a.ValidPrtRoleTyp,
 a.ValidAddrTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd,
 a.PhyAddrFIPSCntyCd,
 a.PhyAddrCntCd,
 a.PhyAddrUSDpndcyInd,
 v_PrtLocSrchNm,
 UPPER (a.PhyAddrCtyNm),
 l.PrtLocCreatUserId,
 l.PrtLocCreatDt,
 l.LastUpdtUserId,
 l.LastUpdtDt
 FROM partner.PrtLocTbl l
 LEFT OUTER JOIN PrtTbl p ON l.PrtId = p.PrtId
 LEFT OUTER JOIN PhyAddrTbl a
 ON l.LocId = a.LocId AND a.ValidAddrTyp = 'Phys'
 LEFT OUTER JOIN ValidLocTypTbl v
 ON l.ValidLocTyp = v.ValidLocTyp
 WHERE l.LocId = v_LocationId;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 PrtReplicationErrInsCSP (v_INST_ID,
 v_ErrorNmb := 31205;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Loading LocSrchTbl.';
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 --Refresh location data in partner search table.
 partner.PrtSrchUpdLocCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31206;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 -- update RolePrivilegePrgrmID
 BEGIN
 SELECT RolePrivilegePrgrmId
 INTO v_RolePrivilege
 EXCEPTION
 FROM partner.ValidPrtSubCatTbl v, partner.PrtTbl p
 WHERE p.PrtId = v_PartnerId
 AND v.ValidPrtSubCatTyp = p.ValidPrtSubCatTyp;
 WHEN NO_DATA_FOUND
 THEN
 v_RolePrivilege := NULL;
 END;
 
 UPDATE partner.LocSrchTbl
 SET RolePrivilegePrgrmId = v_RolePrivilege
 WHERE LocId = v_LocationId;
 
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31207;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Updating LocSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID,
 v_LOC_ID,
 'PrtLocInsTrig',
 v_ErrorNmb,
 v_ReplicationErrorMessageTxt,
 NULL);
 END; --Error.
 END IF;
 
 RETURN;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


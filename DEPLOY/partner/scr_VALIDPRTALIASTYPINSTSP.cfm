<!--- Saved 05/05/2015 13:19:55. --->
PROCEDURE VALIDPRTALIASTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtAliasTyp VARCHAR2 :=NULL,
 p_ValidPrtAliasTypDesc VARCHAR2 :=NULL,
 p_ValidPrtAliasTypDispOrdNmb NUMBER:=0,
 p_ValidPrtAliasTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtAliasTypIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtAliasTypTbl 
 (
 ValidPrtAliasTyp, 
 ValidPrtAliasTypDesc, 
 ValidPrtAliasTypDispOrdNmb, 
 ValidPrtAliasTypCreatUserId
 )
 VALUES (
 p_ValidPrtAliasTyp, 
 p_ValidPrtAliasTypDesc, 
 p_ValidPrtAliasTypDispOrdNmb, 
 p_ValidPrtAliasTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO ValidPrtAliasTypIns;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


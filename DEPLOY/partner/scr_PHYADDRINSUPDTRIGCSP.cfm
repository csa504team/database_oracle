<!--- Saved 05/05/2015 13:16:37. --->
PROCEDURE PHYADDRINSUPDTRIGCSP (
 OldDataRec PhyAddrTbl%ROWTYPE,
 NewDataRec PhyAddrTbl%ROWTYPE
 )
 /*-
 -- Adds the corresponding location to the extract queue.
 --
 -- Revision History -----------------------------------------
 -- 07/17/2003 H. Buch Replaced the FIPS retreival code by getting it from ZipCdTbl
 and Zip5CdTbl in sbaref database
 -- 10/24/2002 Ian Clark Added code to try to get the FIPS (State & County Codes)
 from the CtyCdTbl and CntyTbl based on the State & City
 if more than one county code take the largest number.
 If not able to find a match write an error for research.
 -- 08/29/2000 C.Woodard Added code to refresh location data
 -- in partner search table and
 -- location search table.
 -- 04/18/2000 C.Woodard Created.
 -- ----------------------------------------------------------*/
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 AS
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_PhyAddrSeqNmb number(5);
 v_ValidAddrTyp varchar2(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_PrtReplicationErrMsgTxt varchar2(255);
 v_PhyAddrCtyNm varchar2(75);
 v_PhyAddrFIPSCntyCd char(5);
 v_PhyAddrCntyNm varchar2(50);
 v_PhyAddrStCd varchar2(75);
 v_PhyAddrCntCd varchar2(5);
 v_PhyAddrPostCd varchar2(20);
 v_Rowcount number(12);
 v_LengthOfZip number(4);
 BEGIN
 v_INST_ID := 0;
 v_LocationId := NewDataRec.LocId;
 v_PhyAddrSeqNmb := NewDataRec.PhyAddrSeqNmb;
 v_ValidAddrTyp := NewDataRec.ValidAddrTyp;
 v_PhyAddrCtyNm := UPPER(NewDataRec.PhyAddrCtyNm);
 v_PhyAddrFIPSCntyCd := NewDataRec.PhyAddrFIPSCntyCd;
 v_PhyAddrCntyNm := NewDataRec.PhyAddrCntyNm;
 v_PhyAddrStCd := NewDataRec.PhyAddrStCd;
 v_PhyAddrCntCd := NewDataRec.PhyAddrCntCd;
 v_PhyAddrPostCd := NewDataRec.PhyAddrPostCd;
 v_LengthOfZip := length(NewDataRec.PhyAddrPostCd);
 
 /*-- 10/24/2002 ADDED CODE HERE TO CHECK FOR AND GET FIPS CODES only if it is missing
 we will not be validating values provided by Thomson
 07/17/2003 ADDED CODE TO GET THE FIPS COUNTY CODE FROM THE ZipCdTbl and Zip5CdTbl
 IN sbaref database.
 */
 IF v_PhyAddrFIPSCntyCd IS NULL AND v_PhyAddrCntCd = 'US'
 THEN
 BEGIN -- FIPS CODE MISSING FOR A US Address
 IF (v_LengthOfZip = 10) -- If 9 digit zip check ZipCdTbl
 THEN
 BEGIN
 SELECT s.NumrcStCd || z.CntyCd, cn.CntyNm into v_PhyAddrFIPSCntyCd, v_PhyAddrCntyNm
 FROM sbaref.StTbl s, sbaref.ZipCdTbl z, sbaref.CntyTbl cn
 WHERE s.StCd = v_PhyAddrStCd
 AND z.ZipCd5 = substr(v_PhyAddrPostCd,1,5)
 AND z.ZipCd4 = substr(v_PhyAddrPostCd,7,4)
 AND s.StCd = cn.StCd
 AND z.CntyCd = cn.CntyCd;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PhyAddrFIPSCntyCd := NULL; v_PhyAddrCntyNm := NULL; END;
 END IF;
 
 IF (v_PhyAddrFIPSCntyCd IS NULL)
 THEN
 BEGIN -- if no match found in ZipCdTbl, check Zip5CdTbl
 SELECT s.NumrcStCd || z.CntyCd, cn.CntyNm into v_PhyAddrFIPSCntyCd, v_PhyAddrCntyNm
 FROM sbaref.StTbl s, sbaref.Zip5CdTbl z, sbaref.CntyTbl cn
 WHERE s.StCd = v_PhyAddrStCd
 AND z.Zip5Cd = substr(v_PhyAddrPostCd,1,5)
 AND s.StCd = cn.StCd
 AND z.CntyCd =cn.CntyCd
 AND cn.CntyCd = (select max(CntyCd) from sbaref.CntyTbl z where cn.StCd = z.StCd);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PhyAddrFIPSCntyCd :=NULL; v_PhyAddrCntyNm := NULL; END;
 END IF;
 
 IF v_PhyAddrFIPSCntyCd IS NOT NULL
 THEN
 BEGIN -- We were able to find a FIPS Code. Now Update the Address Table.
 UPDATE PhyAddrTbl
 SET PhyAddrFIPSCntyCd = v_PhyAddrFIPSCntyCd,
 PhyAddrCntyNm = v_PhyAddrCntyNm, 
 LASTUPDTUSERID = NewDataRec.LASTUPDTUSERID,
 LASTUPDTDT =SYSDATE
 WHERE LocId = v_LocationId
 AND PhyAddrSeqNmb = v_PhyAddrSeqNmb
 AND ValidAddrTyp=v_ValidAddrTyp;
 
 -- ERROR PROCESSING IF UPDATE FAILS
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_PrtReplicationErrMsgTxt := NULL; --Use system message.
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_PrtReplicationErrMsgTxt, NULL);
 v_ErrorNmb := 31869;
 v_PrtReplicationErrMsgTxt := 'Sybase Error. Unable to Get FIPS Code For' || ' State = ' ||
 v_PhyAddrStCd || ' Zip = ' || v_PhyAddrPostCd || ', LocId= ' || to_char(v_LocationId) ||
 ', Seq=' || to_char(v_PhyAddrSeqNmb) || ', ValidAddrTyp=' || v_ValidAddrTyp;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_PrtReplicationErrMsgTxt, NULL);
 END; --Error on Update
 END IF;
 END; -- UDATE Physical Address row.
 END IF;
 END;
 ELSE
 BEGIN -- Could not Find the City Write an Error for Research..
 v_ErrorNmb := 31870;
 v_PrtReplicationErrMsgTxt := 'Unable to Get FIPS Code For' || ' State = ' || v_PhyAddrStCd || ' Zip = ' ||
 v_PhyAddrPostCd || ', LocId= ' || to_char(v_LocationId) || ', Seq=' ||
 to_char(v_PhyAddrSeqNmb) || ', ValidAddrTyp=' || v_ValidAddrTyp;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_PrtReplicationErrMsgTxt, NULL);
 END;
 END IF; -- END ADDING CODE TO CHECK FOR AND GET FIPS CODES
 
 -- Add location to extract queue.
 partner.LocExtrctStatInsCSP (v_LocationId,NewDataRec.LASTUPDTUSERID);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31802;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --Refresh address data in partner search table.
 partner.PrtSrchUpdAddrCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31867;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh address data in location search table.
 partner.LocSrchUpdAddrCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31868;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing LocSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 RETURN;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


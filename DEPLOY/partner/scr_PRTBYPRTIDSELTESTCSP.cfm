<!--- Saved 05/05/2015 13:17:23. --->
PROCEDURE PRTBYPRTIDSELTESTCSP(
 p_userName VARCHAR2 := NULL,
 p_PrtId NUMBER := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.PrtId
 , p.PrtLglNm
 , p.ValidPrtPrimCatTyp
 , l.PrtLocNm
 , l.ValidLocTyp
 , l.PhyAddrStr1Txt
 , l.PhyAddrCtyNm
 , l.PhyAddrStCd
 , l.PhyAddrPostCd
 , l.LocId
 , l.PrtLocFIRSNmb
 FROM LocSrchTbl l, PrtSrchTbl p 
 WHERE p.PrtId = p_PrtId 
 AND p.PrtCurStatCd = 'O' 
 AND p.PrtId = l.PrtId 
 ORDER BY l.LocId ;
 
 END PRTBYPRTIDSELTESTCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


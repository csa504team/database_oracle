<!--- Saved 01/23/2018 15:32:04. --->
PROCEDURE PRTSELTSP
 (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtLglNm varchar2 := NULL,
 p_ValidPrtAliasTyp varchar2 := 'TIN',
 p_PrtAliasNm varchar2 := NULL,
 p_ValidPrtSubCatTyp varchar2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtDataSourcId from ID-0
 */
 BEGIN
 SAVEPOINT PrtSelTSP;
 
 IF p_Identifier = 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT
 PrtId,
 OfcCd,
 PrtChrtrTyp,
 PrtEstbDt,
 PrtFisclYrEndDayNmb,
 PrtFisclYrEndMoNmb,
 PrtInactDt,
 PrtLglNm,
 ValidPrtLglTyp,
 ValidPrtPrimCatTyp,
 PrtCurStatCd,
 ValidPrtSubCatTyp,
 INST_ID
 FROM partner.PrtTbl
 WHERE PrtId = p_PrtId;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 2 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM partner.PrtTbl
 WHERE PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 11 THEN
 /* Select Partner Information On the basis partial Partner Legal Name and ValidPrtAliasTyp */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 PrtAliasNm,
 c.LocId,
 b.PrtLglNm
 FROM PrtAliasTbl a, PrtTbl b, PrtLocTbl c
 where upper(b.PrtLglNm) like upper(p_PrtLglNm) || '%'
 and a.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 and a.PrtId = b.PrtId
 and a.PrtId = c.PrtId
 and c.ValidLocTyp = 'HO'
 order by upper(b.PrtLglNm);
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 12 THEN
 /* Select Partner Information On the basis Alias Type and Alias Name */
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 PrtAliasNm,
 c.LocId,
 b.PrtLglNm
 FROM PrtAliasTbl a, PrtTbl b, PrtLocTbl c
 where ValidPrtAliasTyp = p_ValidPrtAliasTyp
 and PrtAliasNm = p_PrtAliasNm
 and a.PrtId = b.PrtId
 and a.PrtId = c.PrtId
 and c.ValidLocTyp = 'HO';
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 13 THEN
 /* Select Partner Information based on Partner Subcategory Type and Alias Type */
 BEGIN
 OPEN p_SelCur FOR
 select a.PrtId,
 b.PrtAliasNm,
 a.PrtLglNm
 from PrtTbl a, PrtAliasTbl b
 where a.PrtId = b.PrtId
 and a.ValidPrtSubCatTyp = p_ValidPrtSubCatTyp
 and b.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 order by upper(a.PrtLglNm);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 14 THEN
 /* client..PrtSelTSP - Identifier 0, 12, 13 */
 BEGIN
 OPEN p_SelCur FOR
 SELECT substr(PrtAliasNm,1,1) as EinSsn,
 substr(PrtAliasNm,2,9) as PrtId,
 ValidPrtSubCatTyp,
 ValidPrtLglTyp,
 PrtLglNm as PrtNm,
 PrtLocNm as PrtTrdNm,
 (select max(ElctrncAddrTxt) from partner.ElctrncAddrTbl
 where LocId = l.LocId and ValidElctrncAddrTyp = 'Phone') as PrtVcePhnNmb,
 (select max(ElctrncAddrTxt) from partner.ElctrncAddrTbl
 where LocId = l.LocId and ValidElctrncAddrTyp = 'Fax') as PrtFaxPhnNmb,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm as PrtCtyNm,
 substr(a.PhyAddrFIPSCntyCd,3,3) as CntyCd,
 a.PhyAddrStCd as StCd,
 CASE
 WHEN LENGTH(a.PhyAddrPostCd) > 5
 THEN substr(a.PhyAddrPostCd,1,5)
 ELSE a.PhyAddrPostCd
 END as ZipCd,
 CASE
 WHEN LENGTH(a.PhyAddrPostCd) > 9
 THEN substr(a.PhyAddrPostCd,7,10)
 ELSE null
 END as Zip4Cd
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p, partner.PrtLocTbl l,
 partner.PhyAddrTbl a
 WHERE pa.PrtAliasNm = NVL(p_PrtAliasNm,pa.PrtAliasNm)
 AND pa.PrtId = p.PrtId
 AND p.ValidPrtSubCatTyp = NVL(p_ValidPrtSubCatTyp,p.ValidPrtSubCatTyp)
 AND p.PrtCurStatCd = 'O'
 AND p.PrtId = l.PrtId
 AND l.ValidLocTyp = 'HO'
 AND l.LocId = a.LocId
 AND a.ValidAddrTyp = 'Phys'
 AND a.PrtCntctNmb is null;
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 15 THEN
 /* client..PrtSelTSP - Identifier 2 */
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p
 WHERE pa.PrtAliasNm = p_PrtAliasNm
 AND pa.PrtId = p.PrtId
 AND p.PrtCurStatCd = 'O';
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 16 THEN
 /* client..PrtSelTSP - Identifier 3 */
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtLglNm as PrtNm
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p
 WHERE pa.PrtAliasNm = p_PrtAliasNm
 AND pa.PrtId = p.PrtId
 AND p.PrtCurStatCd = 'O';
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 ELSIF p_Identifier = 17 THEN
 /* client..PrtSelTSP - Identifier 11 */
 BEGIN
 OPEN p_SelCur FOR
 SELECT pa.PrtAliasNm as PrtId,
 p.PrtLglNm as PrtNm,
 l.PrtLocNm as PrtTrdNm
 FROM partner.PrtTbl p, partner.PrtAliasTbl pa, partner.PrtLocTbl l
 WHERE p.ValidPrtSubCatTyp = p_ValidPrtSubCatTyp
 AND p.PrtCurStatCd = 'O'
 AND p.PrtId = pa.PrtId
 AND pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 AND p.PrtId = l.PrtId
 AND l.ValidLocTyp = 'HO';
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 18 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT ps.PrtId, ps.PrtLglNm, ps.PhyAddrStr1Txt, 
 (select PhyAddrStr2Txt from partner.PhyAddrTbl a 
 WHERE a.LocId = ps.LocId AND PrtCntctNmb is null AND ValidAddrTyp = 'Phys'
 AND PhyAddrSeqNmb = (SELECT MAX(PhyAddrSeqNmb) FROM partner.PhyAddrTbl z 
 WHERE a.LocId = z.LocId AND z.PrtCntctNmb is null
 AND z.ValidAddrTyp = a.ValidAddrTyp)) as PhyAddrStr2Txt,
 ps.PhyAddrCtyNm, ps.PhyAddrCtySrchNm, ps.PhyAddrStCd, ps.PhyAddrPostCd , s.StNm, ao.StCd
 FROM partner.PrtSrchTbl ps ,partner.PrtAgrmtTbl pa, partner.PrtAreaOfOperTbl ao,sbaref.StTbl s
 WHERE ps.ValidPrtSubCatTyp = 'Micro' AND PrtCurStatCd = 'O'
 AND ps.PrtId = pa.PrtId AND pa.PrtAgrmtEffDt <= sysdate
 AND ((pa.PrtAgrmtExprDt >= sysdate OR pa.PrtAgrmtExprDt is NULL)
 AND (pa.PrtAgrmtTermDt >sysdate OR pa.PrtAgrmtTermDt is NULL))
 AND pa.PrtId = ao.PrtId AND pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
 AND ao.StCd = s.StCd
 AND ao.PrtAreaOfOperCntyEndDt IS NULL
 ORDER BY s.StNm, ps.PrtLglNm;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 19 THEN
 BEGIN
 OPEN p_SelCur FOR
 select a.PrtId,
 b.PrtAliasNm,
 a.PrtLglNm
 from PrtTbl a, PrtAliasTbl b
 where a.PrtId = b.PrtId
 and a.PrtCurStatCd = 'O'
 and b.ValidPrtAliasTyp = p_ValidPrtAliasTyp
 order by upper(a.PrtLglNm);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 
 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO PrtSelTSP;
 RAISE;
 END ;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


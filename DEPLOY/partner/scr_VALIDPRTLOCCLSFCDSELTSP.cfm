<!--- Saved 05/05/2015 13:20:02. --->
PROCEDURE VALIDPRTLOCCLSFCDSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtLocClsfCd NUMBER:=0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtLocClsfCdSel;
 IF p_Identifier= 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrtLocClsfCd, 
 ValidPrtLocClsfDescTxt, 
 ValidPrtLocClsfCreatUserId, 
 ValidPrtLocClsfCreatDt
 FROM ValidPrtLocClsfCdTbl 
 WHERE ValidPrtLocClsfCd = p_ValidPrtLocClsfCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier= 2 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM ValidPrtLocClsfCdTbl 
 WHERE ValidPrtLocClsfCd = p_ValidPrtLocClsfCd;
 p_RetVal := SQL%ROWCOUNT; 
 end;
 end if;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTLOCCLSFCDSEL;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


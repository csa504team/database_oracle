<!--- Saved 05/05/2015 13:18:03. --->
PROCEDURE PRTLOCCNTCTPOSDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctPosTypCd NUMBER := 0)
 AS
 OldDataRec PrtLocCntctPosTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtLocCntctPosDel;
 IF p_Identifier = 0 THEN
 /* Delete Row On the basis of Key*/
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtLocCntctPosTbl WHERE LocId = p_LocId and PrtCntctNmb = p_PrtCntctNmb
 and PrtCntctPosTypCd = p_PrtCntctPosTypCd;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 DELETE FROM PrtLocCntctPosTbl 
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctPosTypCd = p_PrtCntctPosTypCd;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctPosDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctPosDel;
 RAISE;
 
 END PRTLOCCNTCTPOSDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


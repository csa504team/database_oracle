<!--- Saved 05/05/2015 13:18:28. --->
PROCEDURE PRTLOCHISTCCCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCHISTCC; 
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm,
 r.LocHistryLocNm,
 a.LocHistryEffDt
 FROM LocHistryTbl a, LocHistryTbl r 
 WHERE a.ValidChngCd = 'CC' 
 AND a.LocHistryEffDt >= p_ReportBegDateTime 
 AND a.LocHistryEffDt <= p_ReportEndDateTime 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = r.LocId 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = 'CC');
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCHISTCC;
 RAISE;
 END PRTLOCHISTCCCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


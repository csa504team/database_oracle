<!--- Saved 05/05/2015 13:20:07. --->
PROCEDURE VALIDPRTPRIMCATUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtPrimCatTyp VARCHAR2:=NULL,
 p_ValidPrtPrimCatDispOrdNmb NUMBER:=0,
 p_ValidPrtPrimCatTypDesc VARCHAR2:=NULL,
 p_ValidPrtPrimCatCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtPrimCatUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 UPDATE ValidPrtPrimCatTbl
 SET 
 ValidPrtPrimCatDispOrdNmb =p_ValidPrtPrimCatDispOrdNmb, 
 ValidPrtPrimCatTypDesc =p_ValidPrtPrimCatTypDesc , 
 ValidPrtPrimCatCreatUserId=p_ValidPrtPrimCatCreatUserId, 
 ValidPrtPrimCatCreatDt =SYSDATE
 WHERE ValidPrtPrimCatTyp = p_ValidPrtPrimCatTyp;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF ;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTPRIMCATUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


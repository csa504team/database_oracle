<!--- Saved 05/05/2015 13:19:22. --->
PROCEDURE PUTALLPARTNERSINTOTREECSP
 AS
 CURSOR RootPartnerCsr
 IS SELECT DISTINCT po.OwningPrtId
 FROM PrtOwnrshpTbl po, PrtTbl p
 WHERE po.OwningPrtId = p.PrtId
 AND po.OwningPrtId NOT IN ( SELECT PrtId
 FROM PrtOwnrshpTbl )
 ORDER BY p.PrtLglNm;
 --
 -- Inserts each root partner by executing
 -- PutRootPartnerIntoTreeCSP, which in turn puts
 -- any partners owned directly or indirectly
 -- by that root partner into the partner
 -- ownership tree.
 --
 v_RootId NUMBER(7);
 -- Error Handling variables
 v_INST_ID NUMBER(8);
 v_LOC_ID NUMBER(8);
 v_ErrorNmb NUMBER(5);
 v_SelCursor SYS_REFCURSOR;
 v_ReplicationErrorMessageTxt VARCHAR2(255);
 BEGIN
 
 v_INST_ID := 0 ;
 v_LOC_ID := 0 ;
 OPEN RootPartnerCsr;
 FETCH RootPartnerCsr INTO v_RootId;
 WHILE ( RootPartnerCsr%FOUND ) 
 LOOP 
 
 BEGIN
 --Insert the Root Partner into the Tree.
 PutRootPartnerIntoTreeCSP(v_RootId , v_ErrorNmb, v_SelCursor);
 IF v_ErrorNmb <> 0 THEN
 
 BEGIN
 v_ErrorNmb := 99999 ;
 v_ReplicationErrorMessageTxt := 'Error calling PutRootPartnerIntoTreeCSP.' || ' Root PrtId=' || to_char(v_RootId) ;
 v_INST_ID := 0 ;
 v_LOC_ID := 0 ;
 PrtReplicationErrInsCSP(p_INST_ID => v_INST_ID,
 p_LOC_ID => v_LOC_ID,
 p_PrtReplicationErrProcNm => 'PutAllPartnersIntoTreeCSP',
 p_PrtReplicationErrNmb => v_ErrorNmb,
 p_PrtReplicationErrMsgTxt => v_ReplicationErrorMessageTxt);
 
 -- RETURN -100;
 END;
 END IF;
 --Error.
 FETCH RootPartnerCsr INTO v_RootId;
 END;
 END LOOP;
 --Processing Root Partners.
 CLOSE RootPartnerCsr;
 End;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


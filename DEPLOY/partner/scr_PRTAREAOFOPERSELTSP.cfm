<!--- Saved 05/05/2015 13:17:13. --->
PROCEDURE PRTAREAOFOPERSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrtAreaOfOperSeqNmb NUMBER := 0,
 p_StCd CHAR := NULL,
 p_CntyCd CHAR := NULL,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_ValidPrtSubCatTyp VARCHAR2 := NULL,
 p_OfcCd CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT PrtAreaOfOperSel;
 
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt
 FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 /* Select Current Area of Operation for Active Agreement */
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt
 FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperCntyEffDt <= SYSDATE 
 and (PrtAreaOfOperCntyEndDt > SYSDATE or PrtAreaOfOperCntyEndDt IS NULL);
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 12 THEN
 /* Select All the Area of Operation for Pending Agreement */
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt
 FROM PrtAreaOfOperTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 13 THEN
 /* Given a Partner, State and county return the result set for the current agreement */
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt
 FROM PrtAreaOfOperTbl a 
 WHERE PrtId = p_PrtId 
 AND StCd = p_StCd 
 and CntyCd = p_CntyCd 
 and PrtAgrmtSeqNmb = (SELECT MAX(z.PrtAgrmtSeqNmb) 
 FROM PrtAreaOfOperTbl z 
 WHERE z.PrtId = a.PrtId) 
 and PrtAreaOfOperCntyEffDt <= SYSDATE;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 14 THEN
 /* List of list of partners for a given partner type in a state */
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT a.PrtId,
 b.PrtAliasNm,
 a.PrtLglNm
 FROM PrtTbl a, PrtAliasTbl b, PrtAreaOfOperTbl c 
 WHERE a.PrtId = b.PrtId 
 and a.ValidPrtSubCatTyp = p_ValidPrtSubCatTyp 
 and b.ValidPrtAliasTyp = p_ValidPrtAliasTyp 
 and a.PrtId = c.PrtId 
 --and c.StCd = p_StCd 
 ORDER BY UPPER(a.PrtLglNm);
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 15 THEN
 /* List of list of partners for a given partner type and a district office */
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT a.PrtId,
 b.PrtAliasNm,
 a.PrtLglNm
 FROM PrtTbl a, PrtAliasTbl b, PrtAreaOfOperTbl c, SBAREF.CntyTbl d 
 WHERE a.PrtId = b.PrtId 
 and a.ValidPrtSubCatTyp = P_ValidPrtSubCatTyp 
 and b.ValidPrtAliasTyp = P_ValidPrtAliasTyp 
 and a.PrtId = c.PrtId 
 and d.OrignOfcCd = p_OfcCd 
 and d.StCd = c.StCd 
 and d.CntyCd = c.CntyCd 
 ORDER BY UPPER(a.PrtLglNm);
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAreaOfOperSel;
 RAISE;
 
 END PRTAREAOFOPERSELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


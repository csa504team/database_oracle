<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by sherryLiu. 
DATE:				01/23/2018.
DESCRIPTION:		Standardized call to PRTUPDCSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	01/23/2018, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 40)
	and	(Left(CGI.Script_Name,    40) is "/cfincludes/oracle/partner/spc_PRTUPDCSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"				default="">
<cfparam name="Variables.ErrMsg"					default="">
<cfparam name="Variables.SkippedSpcs"				default="">
<cfparam name="Variables.TxnErr"					default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs					= ListAppend(Variables.SkippedSpcs, "PRTUPDCSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"				default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"				default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"				default="call PRTUPDCSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName					= "PRTUPDCSP">
	<cfparam name="Variables.userName"				default="">
	<cfparam name="Variables.PrtId"					default="">
	<cfparam name="Variables.ofcCd"					default="">
	<cfparam name="Variables.PrtChrtrTyp"			default="">
	<cfparam name="Variables.PrtEstbDt"				default="">
	<cfparam name="Variables.PrtFisclYrEndDayNmb"	default="">
	<cfparam name="Variables.PrtFisclYrEndMoNmb"	default="">
	<cfparam name="Variables.PrtInactDt"			default="">
	<cfparam name="Variables.PrtLglNm"				default="">
	<cfparam name="Variables.ValidPrtLglTyp"		default="">
	<cfparam name="Variables.ValidPrtPrimCatTyp"	default="">
	<cfparam name="Variables.PrtCurStatCd"			default="">
	<cfparam name="Variables.ValidPrtSubCatTyp"		default="">
	<cftry>
		<cfstoredproc procedure="PARTNER.PRTUPDCSP" datasource="#Variables.db#"
											username="#Variables.username#" password="#Variables.password#">
		<cfif Len(Variables.userName) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_userName"
											value="#Variables.userName#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_userName"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtId"
											value="#Variables.PrtId#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtId"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.ofcCd) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ofcCd"
											value="#Variables.ofcCd#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ofcCd"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.PrtChrtrTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtChrtrTyp"
											value="#Variables.PrtChrtrTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtChrtrTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtEstbDt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtEstbDt"
											value="#Variables.PrtEstbDt#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtEstbDt"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.PrtFisclYrEndDayNmb) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtFisclYrEndDayNmb"
											value="#Variables.PrtFisclYrEndDayNmb#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtFisclYrEndDayNmb"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.PrtFisclYrEndMoNmb) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtFisclYrEndMoNmb"
											value="#Variables.PrtFisclYrEndMoNmb#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtFisclYrEndMoNmb"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.PrtInactDt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtInactDt"
											value="#Variables.PrtInactDt#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtInactDt"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.PrtLglNm) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLglNm"
											value="#Variables.PrtLglNm#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLglNm"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.ValidPrtLglTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtLglTyp"
											value="#Variables.ValidPrtLglTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtLglTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.ValidPrtPrimCatTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtPrimCatTyp"
											value="#Variables.ValidPrtPrimCatTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtPrimCatTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtCurStatCd) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtCurStatCd"
											value="#Variables.PrtCurStatCd#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtCurStatCd"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.ValidPrtSubCatTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtSubCatTyp"
											value="#Variables.ValidPrtSubCatTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ValidPrtSubCatTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


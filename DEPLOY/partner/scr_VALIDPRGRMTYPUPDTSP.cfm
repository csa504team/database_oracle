<!--- Saved 11/16/2010 10:11:11. --->
PROCEDURE VALIDPRGRMTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrgrmTyp VARCHAR2 :=NULL,
 p_ValidPrgrmTypDispOrdNmb NUMBER:=0,
 p_ValidPrgrmTypDesc VARCHAR2 :=NULL,
 p_ValidPrgrmTypCreatUserId CHAR :=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 Begin
 SAVEPOINT ValidPrgrmTypUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 UPDATE ValidPrgrmTypTbl
 SET 
 ValidPrgrmTypDispOrdNmb = p_ValidPrgrmTypDispOrdNmb, 
 ValidPrgrmTypDesc = p_ValidPrgrmTypDesc, 
 ValidPrgrmTypCreatUserId = p_ValidPrgrmTypCreatUserId, 
 ValidPrgrmTypCreatDt = SYSDATE
 WHERE ValidPrgrmTyp = p_ValidPrgrmTyp;
 p_RetVal := SQL%ROWCOUNT;
 end;
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRGRMTYPUPD;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:53. --->
PROCEDURE VALIDPRGRMSUBCATINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_PrgrmId VARCHAR2 :=NULL,
 p_ValidPrtSubCatTyp VARCHAR2 :=NULL,
 p_ValidPrgrmSubCatDispOrdNmb NUMBER:=0,
 p_ValidPrgrmSubCatCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrgrmSubCatTbl 
 (
 PrgrmId, 
 ValidPrtSubCatTyp, 
 ValidPrgrmSubCatDispOrdNmb, 
 ValidPrgrmSubCatCreatUserId
 )
 VALUES (
 p_PrgrmId, 
 p_ValidPrtSubCatTyp, 
 p_ValidPrgrmSubCatDispOrdNmb, 
 p_ValidPrgrmSubCatCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN 
 ROLLBACK;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


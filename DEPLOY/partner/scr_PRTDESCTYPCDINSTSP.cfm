<!--- Saved 05/05/2015 13:17:34. --->
PROCEDURE PRTDESCTYPCDINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtDescTypCd NUMBER := 0,
 p_PrtDescTypTxt VARCHAR2 := NULL,
 p_PrtDescTypCdCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtDescTypCdIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtDescTypCdTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtDescTypCdTbl 
 (PrtDescTypCd, PrtDescTypTxt, PrtDescTypCdCreatUserId, PrtDescTypCdCreatDt)
 VALUES (p_PrtDescTypCd,p_PrtDescTypTxt,p_PrtDescTypCdCreatUserId,SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtDescTypCdIns;
 RAISE;
 END PRTDESCTYPCDINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


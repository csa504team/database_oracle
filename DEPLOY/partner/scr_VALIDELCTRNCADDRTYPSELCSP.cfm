<!--- Saved 05/05/2015 13:19:45. --->
PROCEDURE VALIDELCTRNCADDRTYPSELCSP
 (
 p_userName VARCHAR2:= NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER
 )
 AS
 
 BEGIN
 OPEN p_SelCur FOR
 
 SELECT 
 ValidElctrncAddrTyp, 
 ValidElctrncAddrTypDesc
 FROM ValidElctrncAddrTypTbl 
 WHERE ValidElctrncAddrTypDispOrdNmb != 0 
 ORDER BY ValidElctrncAddrTypDispOrdNmb ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDELCTRNCADDRTYPSELCSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


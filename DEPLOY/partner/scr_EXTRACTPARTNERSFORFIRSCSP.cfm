<!--- Saved 05/05/2015 13:16:13. --->
PROCEDURE ExtractPartnersForFIRSCSP AS
 /* __________________________________________________________________
 Object: Procedure
 Name: ExtractPartnersForFIRSCSP
 Description:
 This procedure creates a row in "PartnerMasterExtract" for
 every LocationId in the "LocationExtractStatus" table.
 The "LocationExtractStatus" table is populated with all Locations
 that have changed since the last extract.
 This procedure is copy of the procedure ExtractPartnersForFIRS,
 which extracts all locations from the Partner database with
 only the inital load table of "LocationCol".
 The two procedures should be merged in the near future.
 Date: 01/17/2000
 Author: Marcus Poulos, Aquas Inc.
 *************** REVISION HISTORY ************************************
 Revised By Date Description
 ---------- ----------- ----------------------------------------
 Ian Clark 04/22/2002 Modified to take into account the changes to the tables because of the
 database changes for contacts. The following tables were affected:
 PhyAddrTbl
 ElctrncAddrTbl
 PrtLocCntctTbl & PrtLocCntctPosTbl Replaced: PrtCntctTbl & PrtCntctLocTbl
 
 Ian Clark 11/07/2001 REMOVED ERROR Message for Locations with a status of "I" We will
 Still ignore these for processing. The main reason for these occuring is
 the treatment of closed/removed locations. We are working with pimsrep to resolve this.
 DSP Rajeswari 1.Joinied PrtCntctLocTbl and PrtCntctTbl
 to get the contact person details
 DSP Rajeswari 01/16/2001 1.Added code to skip record, if @FIRSRecordTyp is null.
 DSP Rajeswari 01/11/2001 1.Added code to insert country code in
 PrtMstrExtrctTbl
 2.Deleted the code which inserts "No_FIRS"
 in PrtMstrExtrctFIRSNmb column of PrtMstrExtrctTbl
 C.Woodard 08/24/2000 1.Added error #29917 when duplicate FIRS#
 is detected.
 C.Woodard 08/23/2000 1.Revised to skip processing of locations
 with FIRSNmb = "DELETED".
 DSP Rajeswari 08/17/2000 Corrected query to get HO Loan manager
 data for a BO.
 C.Woodard 08/08/2000 1.Revised to remove processed queue
 entries before extract process begins.
 Added error #29915 if failure.
 2.Added DELETE transaction to clear
 PrtMstrExtrctTbl before processing.
 Added error #29916 if DELETE fails.
 C.Woodard 07/18/2000 1.Revised to skip extract of locations in
 "I"(In doubt) status. A warning message
 #29904 will be issued.
 2.Revised to skip extract of locations with
 null FIRSNmb, instead of issuing error
 #29902.
 3.Removed error #29912, issued when a head
 office location had no parent partner,
 since that situation is normal.
 4.Added logic to set the parent FIRSNmb
 for holding companies and above.
 Added error #29914 if Parent FIRSNmb is
 NULL.
 5.Moved BEGIN TRAN statement to allow the
 procedure to execute in any transaction
 mode.
 6.Added zipcode and phone number processing
 for international addresses.
 C.Woodard 05/30/2000 Removed various unnecessary code (e.g.,
 Replicate and Substring functions).
 DSP Rajeswari 05/24/2000 Modified to get consistent data
 C.Woodard 04/24/2000 Modified to export "D" status of
 PartnerLocation as "C" for FIRS.
 Renamed procedure and restored table name.
 Marcus Poulos 02/10/2000 Uncommented code for CountyCd & CountyNm, that are
 now available. Modified code for First/Middle Names
 for CEO, Pres, & LoanMgr
 Marcus Poulos 02/08/2000 Added modifications from ExtractPartnersForFIRS &
 Code to update LocationExtractStatus.ExtractedInd
 **************** END OF COMMENTS ************************************
 */
 -- PartnerMasterExtract variables
 v_E_BankNm1 char(32); -- LocationNm
 v_E_BankNm2 char(32); -- LocationNm
 v_E_City char(30); -- Address
 v_E_Street char(33); -- Address
 v_E_CountyNm char(12); -- PhysicalAddress
 v_E_CountyCd char(5); -- PhysicalAddress
 v_E_StateCd char(2); -- Address 'Phys'
 v_E_POBox char(9); -- Mailing Address = "PO BOX"
 v_E_ZipCd char(11); -- Address 'Phys'
 v_E_MailStreet char(33); -- Address 'Mail'
 v_E_MailCity char(30); -- Address 'Mail'
 v_E_MailStateCd char(2); -- Address 'Mail'
 v_E_MailZipCd char(11); -- Address 'Mail'
 v_E_PresFirstNm char(29); -- Contact
 v_E_PresLastNm char(29); -- Contact
 v_E_CEOFirstNm char(29); -- Contact
 v_E_CEOLastNm char(29); -- Contact
 v_E_TotalAssets char(13); -- N/A
 v_E_Deposits char(13); -- N/A
 v_E_FDICNmb char(8); -- Alias
 v_E_FRBNmb char(10); -- Alias
 v_E_NCUANmb char(8); -- Alias
 v_E_PhoneNmb char(15); -- ElectronicAddress
 v_E_FaxNmb char(10); -- ElectronicAddress
 v_E_FaxNmbExt char(5); -- ElectronicAddress
 v_E_LoanMgrFirstNm char(33); -- Contact
 v_E_LoanMgrLastNm char(33); -- Contact
 v_E_InstitutionTyp char(4); -- institution_subcategory.subcategory_type
 v_E_OfficeTypeCd char(4); -- Location.LocationType
 v_E_CharterType char(8); -- Partner
 v_E_TIN char(9); -- Alias
 v_E_ClosedInd char(1); -- CurrentStatusCd
 v_E_ParentFIRSNmb char(7); -- Location.FIRSNmb of the parent of a "BO" or "HO"
 v_E_Prefix char(1); -- DataSourceId + FIRSRecordTyp
 v_E_CntCd char(2); -- Country
 v_E_CurrentAgreementInd char(1); -- N/A
 -- PartnerLocation Variables
 v_LocationId number(7);
 v_FIRSNmb char(7);
 v_FIRSRecordTyp char(2);
 v_PartnerId number(7);
 v_LocationNm varchar2(200);
 v_LocationType varchar2(5);
 v_LOC_ID number(8);
 v_INST_ID number(8);
 -- Partner Variables
 v_DataSourceId varchar2(20);
 -- institution Variables
 v_code number(5);
 -- Error Variables
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 -- Financial Variables *Not in effect yet*
 -- v_TotalAssets number(13);
 -- v_Deposits number(13);
 -- Address Variables
 v_IntlAddrInd CHAR(1); --indicates non-US address.
 v_Street varchar2(101);
 v_Street1 varchar2(50);
 v_Street2 varchar2(50);
 v_varCity varchar2(30);
 v_varStateCd varchar2(75);
 v_varPostalCd varchar2(20);
 v_ZipCd1 char(5);
 --r v_RZipCd2 varchar2(6);
 v_ZipCd2 varchar2(6);
 --r v_Zip2 char(6);
 -- Name Variables
 v_varFirstNm varchar2(27);
 v_varMI char(1);
 v_varFirstMI varchar2(29);
 -- Other Variables
 v_ExtractLocationId number(7);
 v_RemovedLocationQty number(7);
 v_BankNm2 varchar2(32);
 v_CharterType varchar2(15);
 v_Parent number(7);
 v_FDIC varchar2(8);
 v_NCUA varchar2(8);
 v_FRB varchar2(10);
 v_PhoneNmb varchar2(15);
 v_ElectronicAddressTxt varchar2(255);
 --r
 v_MaxPrtOwnrshpPct number(5,2);
 v_MaxPrtCntctNmb number(8);
 v_Cnt number(10);
 
 CURSOR LocCsr IS SELECT le.LocId FROM partner.LocExtrctStatTbl le
 Where le.LocExtrctStatExtrctInd = 'N';
 BEGIN
 BEGIN
 --Remove queue entries already processed. NOT USING partner.LocExtrctStatDel1CSP; v_Count = v_RemovedLocationQty OUTPUT
 DELETE FROM partner.LocExtrctStatTbl
 WHERE LocExtrctStatExtrctInd = 'Y';
 IF SQLCODE <> 0
 THEN
 BEGIN
 v_ErrorNmb := 29915;
 v_ReplicationErrorMessageTxt := 'Error Deleting queue entries already processed.';
 v_INST_ID := 0;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK;
 RETURN;
 END; --Error
 END IF;
 END; -- end delete LocExtractStatTbl
 
 BEGIN
 --Remove all existing PrtMstrExtrctTbl rows.
 DELETE partner.PrtMstrExtrctTbl;
 IF SQLCODE <> 0
 THEN
 BEGIN
 v_ErrorNmb := 29916;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Deleting PartnerMasterExtract.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK;
 RETURN;
 END; --Error.
 END IF;
 END;
 
 -- Set Constants
 v_E_TotalAssets := '0000000000000';
 v_E_Deposits := '0000000000000';
 v_E_CurrentAgreementInd := ' ';
 -- Error Variables
 v_ErrorNmb := 0;
 v_ReplicationErrorMessageTxt := Null;
 -- PartnerLocation Variables
 v_LocationId := 0;
 v_PartnerId := 0;
 v_LocationNm := Null;
 v_LocationType := Null;
 v_LOC_ID := 0;
 v_INST_ID := 0;
 -- Partner Variables
 v_CharterType := ' ';
 v_code := 0;
 
 OPEN LocCsr;
 LOOP
 FETCH LocCsr INTO v_ExtractLocationId;
 EXIT WHEN LocCsr%NOTFOUND;
 BEGIN
 -- _________________________________________________________
 -- Get Location data
 -- _________________________________________________________
 v_LocationId := NULL;
 v_FIRSNmb := NULL;
 v_FIRSRecordTyp := ' ';
 v_E_ClosedInd := ' ';
 v_DataSourceId := NULL;
 
 /*SELECT L.LocId, NVL(L.PrtLocFIRSNmb,' '), NVL(L.PrtLocFIRSRecrdTyp,' '), L.PrtId, L.PrtLocNm, L.PrtLocCurStatCd, 
 NVL(L.LOC_ID,0), NVL(L.INST_ID,0), NVL(P.PrtDataSourcId,' ')
 INTO v_LocationId, v_FIRSNmb, v_FIRSRecordTyp, v_PartnerId, v_LocationNm, v_E_ClosedInd, v_LOC_ID,
 v_INST_ID, v_DataSourceId
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;*/
 BEGIN
 SELECT L.LocId INTO v_LocationId 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_LocationId := NULL; END;
 BEGIN
 SELECT L.PrtLocFIRSNmb INTO v_FIRSNmb 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FIRSNmb := NULL; END;
 BEGIN
 SELECT L.PrtLocFIRSRecrdTyp INTO v_FIRSRecordTyp 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FIRSRecordTyp := NULL; END;
 BEGIN
 SELECT L.PrtId INTO v_PartnerId 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PartnerId := NULL; END;
 BEGIN
 SELECT L.PrtLocNm INTO v_LocationNm 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_LocationNm := NULL; END;
 BEGIN
 SELECT L.PrtLocCurStatCd INTO v_E_ClosedInd 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_ClosedInd := NULL; END;
 BEGIN
 SELECT L.LOC_ID INTO v_LOC_ID 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_LOC_ID := NULL; END;
 BEGIN
 SELECT L.INST_ID INTO v_INST_ID 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_INST_ID := NULL; END;
 BEGIN
 SELECT P.PrtDataSourcId INTO v_DataSourceId 
 FROM partner.PrtLocTbl L, partner.PrtTbl P
 WHERE L.LocId = v_ExtractLocationId
 AND L.PrtId = P.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_DataSourceId := NULL; END;
 
 IF SQLCODE <> 0
 THEN
 BEGIN -- Error on Location Lookup
 v_ErrorNmb := 29901;
 v_ReplicationErrorMessageTxt := 'Error Selecting PartnerLocation. PartnerMasterExtract row NOT inserted.' ||
 ' LocationId=' || to_char(v_LocationId) || ' LocationNm=' || v_LocationNm;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 GOTO NextOne;
 END; -- Error on Location Lookup
 END IF;
 
 IF v_FIRSNmb is Null OR v_FIRSNmb = 'DELETED' 
 THEN
 BEGIN
 GOTO NextOne;
 END; -- No FIRSNmb
 ELSIF v_FIRSRecordTyp is Null
 THEN
 BEGIN
 GOTO NextOne;
 END;
 END IF;
 
 SELECT count(*) into v_Cnt FROM partner.PrtMstrExtrctTbl
 WHERE PrtMstrExtrctFIRSNmb = v_FIRSNmb;
 IF v_Cnt > 0
 THEN
 BEGIN
 v_ErrorNmb := 29917;
 v_ReplicationErrorMessageTxt := 'Attempt to add duplicate FIRS# into' || ' Extract table. Location will not be added.' ||
 ' Duplicate info:' || ' FIRS#=' || v_FIRSNmb || ' LocationId=' || to_char(v_LocationId) ||
 ' LocationNm=' || v_LocationNm;
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 GOTO NextOne;
 END;
 END IF;
 
 IF v_E_ClosedInd = 'I'
 THEN
 BEGIN
 /*
 IAC 11/07/2001 - This is a meaningless error message. We are working to resolve these locations
 Status. Remove error message and skip these transactions:
 SELECT v_ErrorNmb = 29904
 SELECT v_ReplicationErrorMessageTxt =
 'Warning: PartnerLocation has status' +
 ' of 'I' (In doubt), and will not be' +
 ' extracted for FIRS.' +
 ' LocationId=' + Convert(varchar2(7), v_LocationId) +
 ' LocationNm=' + v_LocationNm
 EXEC partner.PrtReplicationErrInsCSP
 v_INST_ID = v_INST_ID,
 v_LOC_ID = v_LOC_ID,
 v_PrtReplicationErrProcNm =
 'ExtractPartnersForFIRSCSP',
 v_PrtReplicationErrNmb = v_ErrorNmb,
 v_PrtReplicationErrMsgTxt =
 v_ReplicationErrorMessageTxt
 END OF CODE REMOVAL IAC 11/07/2001 */
 GOTO NextOne;
 END; -- Skip In_doubt locations.
 END IF;
 
 IF v_LocationId is Null
 THEN
 BEGIN -- No FIRSNmb
 v_ErrorNmb := 29903;
 v_ReplicationErrorMessageTxt := 'PartnerLocation not found.' || ' PartnerMasterExtract row NOT inserted.' ||
 ' Queued LocationId=' || to_char(v_ExtractLocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 GOTO NextOne;
 END; -- No FIRSNmb
 END IF;
 
 -- _______________________________________________________
 IF v_E_ClosedInd = 'D'
 THEN
 v_E_ClosedInd := 'C';
 END IF;
 -- ________________________________________________________
 IF length(v_LocationNm) < 33
 THEN
 BEGIN
 v_E_BankNm1 := v_LocationNm;
 v_E_BankNm2 := ' ';
 END;
 ELSE
 BEGIN
 v_E_BankNm1 := substr(v_LocationNm, 1, 32);
 v_E_BankNm2 := substr(v_LocationNm, 33, 32);
 END;
 END IF;
 -- NO problem 
 -- _________________________________________________________
 -- Get PartnerAlias data
 -- _________________________________________________________
 v_FDIC := ' ';
 BEGIN
 SELECT max(A.PrtAliasNm) into v_FDIC FROM partner.PrtAliasTbl A
 WHERE A.PrtId = v_PartnerId and A.ValidPrtAliasTyp = 'FDIC';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FDIC := NULL; END;
 
 v_E_FDICNmb := substr(' '||trim(v_FDIC), -8);
 -- _________________________________________________________
 v_FRB := ' ';
 BEGIN
 SELECT max(A.PrtAliasNm) into v_FRB FROM partner.PrtAliasTbl A
 WHERE A.PrtId = v_PartnerId and A.ValidPrtAliasTyp = 'FRS';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_FRB := NULL; END;
 v_E_FRBNmb := substr(' ' || trim(v_FRB), -10);
 -- _________________________________________________________
 v_NCUA := ' ';
 BEGIN
 SELECT max(A.PrtAliasNm) into v_NCUA FROM partner.PrtAliasTbl A
 WHERE A.PrtId = v_PartnerId and A.ValidPrtAliasTyp = 'NCUA';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_NCUA := NULL; END;
 v_E_NCUANmb := substr(' ' || trim(v_NCUA), -8);
 -- ____________________________________________________________
 v_E_TIN := ' ';
 BEGIN
 --SELECT max(substr(trim(A.PrtAliasNm),1,9)) into v_E_TIN FROM partner.PrtAliasTbl A
 SELECT max(substr(trim(A.PrtAliasNm),-9)) into v_E_TIN FROM partner.PrtAliasTbl A
 WHERE A.PrtId = v_PartnerId and A.ValidPrtAliasTyp = 'TIN';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_TIN := NULL; END;
 -- ___________________________________________________________
 -- Get Institution Type data
 -- Retreived from the pimsrep.institution_subcategory
 -- __________________________________________________________
 v_E_InstitutionTyp := ' ';
 BEGIN
 SELECT max(S.code) into v_code FROM pimsrep.institution_subcategory S
 WHERE S.inst_id = v_INST_ID and S.rank = 1;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_code := NULL; END;
 v_E_InstitutionTyp := to_char(v_code);
 -- __________________________________________________________
 -- Get Charter Type
 -- Charter Type is passed directly from pimsrep
 -- Institution to Partner without any translation.
 -- The largest current value is 'National'
 -- 8 charecters (1/11/2000) which is the maximum FIRS
 -- can accept.
 -- ____________________________________________________________
 v_E_CharterType := ' ';
 BEGIN
 SELECT P.PrtChrtrTyp into v_CharterType FROM partner.PrtTbl P
 Where P.PrtId = v_PartnerId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_CharterType := NULL; END;
 v_E_CharterType := Substr(v_CharterType, 1, 8);
 -- ______________________________________________________
 -- Get Office Type pimsrep.location.location_type
 -- _______________________________________________________
 BEGIN
 SELECT max(L.type_code) into v_LocationType FROM pimsrep.location L
 WHERE L.loc_id = v_LOC_ID;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_LocationType := NULL; END;
 v_E_OfficeTypeCd := NVL(v_LocationType,' ');
 -- _____________________________________________________________
 -- Get Street Address data
 -- IAC 04/23/02: Modified both address WHERE statement to remove deleted column
 -- and check for PA.PrtCntctNmb is Null.
 -- Removed: and PA.ValidPrtRoleTyp
 -- Added: and PA.PrtCntctNmb is Null
 -- ______________________________________________________________
 v_Street1 := Null;
 v_Street2 := Null;
 v_varCity := Null;
 v_varStateCd := Null;
 v_varPostalCd := Null;
 v_E_CountyNm := ' ';
 v_E_CountyCd := ' ';
 v_IntlAddrInd := 'N';
 BEGIN
 SELECT PA.PhyAddrStr1Txt into v_Street1 FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Street1 := NULL; END;
 BEGIN
 SELECT PA.PhyAddrStr2Txt into v_Street2 FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Street2 := NULL; END;
 BEGIN
 SELECT trim(substr(PA.PhyAddrCtyNm,1,30)) into v_varCity FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varCity := NULL; END;
 BEGIN
 SELECT PA.PhyAddrFIPSCntyCd into v_E_CountyCd FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_CountyCd := NULL; END;
 BEGIN
 SELECT substr(PA.PhyAddrCntyNm,1,12) into v_E_CountyNm FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_CountyNm := NULL; END;
 BEGIN
 SELECT PA.PhyAddrStCd into v_varStateCd FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varStateCd := NULL; END;
 BEGIN
 SELECT PA.PhyAddrPostCd into v_varPostalCd FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varPostalCd := NULL; END;
 BEGIN
 SELECT trim(substr(PA.PhyAddrCntCd,1,2)) into v_E_CntCd FROM partner.PhyAddrTbl PA
 WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_CntCd := NULL; END;
 BEGIN
 SELECT CASE WHEN PA.PhyAddrCntCd = 'US' THEN 'N' ELSE 'Y' END into v_IntlAddrInd
 FROM partner.PhyAddrTbl PA WHERE PA.LocId = v_LocationId and PA.ValidAddrTyp = 'Phys' and PA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE PA.LocId = z.LocId and PA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_IntlAddrInd := 'Y'; END;
 
 IF v_Street2 IS NOT Null
 THEN
 v_E_Street := substr((regexp_replace(v_Street1, '[[:cntrl:]]', ' ') || ', ' || regexp_replace(v_Street2, '[[:cntrl:]]', ' ')),1,33);
 ELSE
 v_E_Street := substr(regexp_replace(v_Street1, '[[:cntrl:]]', ' '),1,33);
 END IF;
 
 v_E_City := NVL(substr(v_varCity,1,30), ' ');
 v_E_StateCd := NVL(substr(v_varStateCd, 1, 2), ' ');
 
 IF v_IntlAddrInd = 'N'
 THEN
 BEGIN
 v_ZipCd1 := NVL(substr(v_varPostalCd, 1, 5),'00000');
 v_ZipCd2 := NVL(substr(v_varPostalCd, 7, 6),'000000');
 v_E_ZipCd := substr(v_ZipCd1 || v_ZipCd2 || substr('000000', -(6-length(v_ZipCd2))),1,11);
 END;
 ELSE 
 v_E_ZipCd := substr(v_varPostalCd,1,11);
 END IF;
 -- _________________________________________________________________
 -- Get Mailing Address data
 -- _______________________________________________________________
 v_Street1 := Null;
 v_Street2 := Null;
 v_varCity := Null;
 v_varStateCd := Null;
 v_varPostalCd := Null;
 v_IntlAddrInd := 'N';
 
 BEGIN
 SELECT MA.PhyAddrStr1Txt into v_Street1 FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Street1 := NULL; END;
 BEGIN
 SELECT MA.PhyAddrStr2Txt into v_Street2 FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Street2 := NULL; END;
 BEGIN
 SELECT substr(MA.PhyAddrCtyNm,1,30) into v_varCity FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varCity := NULL; END;
 BEGIN
 SELECT MA.PhyAddrStCd into v_varStateCd FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varStateCd := NULL; END;
 BEGIN
 SELECT MA.PhyAddrPostCd into v_varPostalCd FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_varPostalCd := NULL; END;
 BEGIN
 SELECT CASE WHEN MA.PhyAddrCntCd = 'US' THEN 'N' ELSE 'Y' END into v_IntlAddrInd FROM partner.PhyAddrTbl MA
 WHERE MA.LocId = v_LocationId and MA.ValidAddrTyp = 'Mail' and MA.PrtCntctNmb is Null
 AND PhyAddrSeqNmb = (SELECT max(PhyAddrSeqNmb) from partner.PhyAddrTbl z
 WHERE MA.LocId = z.LocId and MA.ValidAddrTyp = z.ValidAddrTyp
 AND z.PrtCntctNmb IS NULL);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_IntlAddrInd := 'Y'; END;
 
 IF SQL%ROWCOUNT = 0
 THEN
 BEGIN
 v_E_MailStreet := v_E_Street;
 v_E_MailCity := v_E_City;
 v_E_MailStateCd := v_E_StateCd;
 v_E_MailZipCd := v_E_ZipCd;
 END;
 ELSE
 BEGIN
 IF v_Street2 IS NOT Null
 THEN
 v_E_MailStreet := substr((v_Street1 || ', ' || v_Street2),1,33);
 ELSE
 v_E_MailStreet := substr(v_Street1,1,33);
 END IF;
 v_E_MailCity := NVL(v_varCity, ' ');
 v_E_MailStateCd := NVL(substr(v_varStateCd, 1, 2), ' ');
 
 IF v_IntlAddrInd = 'N'
 THEN
 BEGIN
 v_ZipCd1 := NVL(substr(v_varPostalCd, 1, 5),'00000');
 v_ZipCd2 := NVL(substr(v_varPostalCd, 7, 6),'000000');
 v_E_MailZipCd := substr(v_ZipCd1 || v_ZipCd2 || substr('000000', -(6 - length(v_ZipCd2))),1,11);
 END;
 ELSE
 v_E_MailZipCd := substr(v_varPostalCd,1,11);
 END IF;
 END;
 END IF;
 --r
 -- ____________________________________________________________
 -- Fill POBox
 -- If the Mailing Address is a PO Box # then fill the POBox
 -- field with just the number.
 -- ____________________________________________________________
 v_E_POBox := ' ';
 IF substr(v_Street1, 1, 4) = 'Box '
 THEN
 v_E_POBox := substr(v_Street1, 5, 9);
 ELSIF substr(v_Street1, 1, 4) = 'POB '
 THEN
 v_E_POBox := substr(v_Street1, 5, 9);
 ELSIF substr(v_Street1, 1, 5) = 'POBox'
 THEN
 v_E_POBox := substr(v_Street1, 7, 9);
 ELSIF substr(v_Street1, 1, 6) = 'PO Box'
 THEN
 v_E_POBox := substr(v_Street1, 8, 9);
 ELSIF substr(v_Street1, 1, 6) = 'P O B '
 THEN
 v_E_POBox := substr(v_Street1, 7, 9);
 ELSIF substr(v_Street1, 1, 7) = 'P O Box'
 THEN
 v_E_POBox := substr(v_Street1, 9, 9);
 ELSIF substr(v_Street1, 1, 9) = 'PO Drawer'
 THEN
 v_E_POBox := LTrim(substr(v_Street1, 11, 9));
 END IF;
 -- ___________________________________________________________________
 -- Get Prefix & Parent FIRS #
 -- ________________________________________________________________
 v_Parent := NULL;
 v_E_ParentFIRSNmb := NULL;
 v_E_Prefix := ' ';
 IF v_FIRSRecordTyp = 'HC'
 THEN
 BEGIN
 IF v_DataSourceId = 'TFP-Replication'
 THEN
 v_E_Prefix := '1';
 ELSE
 v_E_Prefix := '4';
 END IF;
 
 -- Get Owning Partner if it Exists
 --r
 BEGIN
 Select Max(O.PrtOwnrshpPct) into v_MaxPrtOwnrshpPct From partner.PrtOwnrshpTbl O
 Where O.PrtId = v_PartnerId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtOwnrshpPct := NULL; END;
 
 BEGIN
 Select PO.OwningPrtId into v_Parent From partner.PrtOwnrshpTbl PO
 Where PO.PrtId = v_PartnerId and PO.PrtOwnrshpPct = v_MaxPrtOwnrshpPct;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Parent := NULL; END;
 
 IF v_Parent IS NOT NULL
 THEN
 BEGIN -- Get Holding Co. FIRSNmb
 BEGIN
 Select L.PrtLocFIRSNmb into v_E_ParentFIRSNmb From partner.PrtLocTbl L
 Where L.PrtId = v_Parent and L.ValidLocTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_ParentFIRSNmb := NULL; END;
 
 IF v_E_ParentFIRSNmb is Null
 THEN
 BEGIN -- Null HC FIRSNmb
 --SELECT v_E_ParentFIRSNmb = 'No_FIRS'
 v_E_ParentFIRSNmb := ' ';
 v_ErrorNmb := 29914;
 v_ReplicationErrorMessageTxt := 'Parent FIRSNmb is NULL. PrtId of parent=' || to_char(v_Parent) ||
 ' Queued child HC LocationId=' || to_char(v_ExtractLocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Null HC FIRSNmb
 END IF;
 END; -- Get Holding Co. FIRSNmb
 END IF;
 END; -- Prefix-1
 -- ----------------------------------------------------------
 ELSIF v_FIRSRecordTyp = 'HO'
 THEN
 BEGIN -- Prefix-2
 IF v_DataSourceId = 'TFP-Replication'
 THEN
 v_E_Prefix := '2';
 ELSE
 v_E_Prefix := '5';
 END IF;
 -- Get Owning Partner of Head Office if it Exists
 --r
 BEGIN
 Select Max(O.PrtOwnrshpPct) into v_MaxPrtOwnrshpPct From partner.PrtOwnrshpTbl O
 Where O.PrtId = v_PartnerId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtOwnrshpPct := NULL; END;
 BEGIN
 Select Max(PO.OwningPrtId) into v_Parent From partner.PrtOwnrshpTbl PO
 Where PO.PrtId = v_PartnerId and PO.PrtOwnrshpPct = v_MaxPrtOwnrshpPct;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_Parent := NULL; END;
 
 IF v_Parent IS NOT NULL
 THEN
 BEGIN -- Get Holding Co. FIRSNmb
 BEGIN
 Select L.PrtLocFIRSNmb into v_E_ParentFIRSNmb From partner.PrtLocTbl L
 Where L.PrtId = v_Parent and L.ValidLocTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_ParentFIRSNmb := NULL; END;
 
 IF v_E_ParentFIRSNmb is Null
 THEN
 BEGIN -- Null HC FIRSNmb
 --r SELECT v_E_ParentFIRSNmb = 'No_FIRS'
 v_E_ParentFIRSNmb := ' ';
 v_ErrorNmb := 29913;
 v_ReplicationErrorMessageTxt := 'Parent FIRSNmb is NULL. PrtId of parent=' || to_char(v_Parent) ||
 ' Queued Owned HO LocationId=' || to_char(v_ExtractLocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Null HC FIRSNmb
 END IF;
 END; -- Get Holding Co. FIRSNmb
 END IF;
 END; -- Prefix-2
 -- --------------------------------------------------------
 ELSIF v_FIRSRecordTyp = 'BO'
 THEN
 BEGIN -- Prefix-3
 IF v_DataSourceId = 'TFP-Replication'
 THEN
 v_E_Prefix := '3';
 ELSE
 v_E_Prefix := '6';
 END IF;
 
 BEGIN
 Select L.PrtLocFIRSNmb into v_E_ParentFIRSNmb From partner.PrtLocTbl L
 Where L.PrtId = v_PartnerId and L.PrtLocFIRSRecrdTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_ParentFIRSNmb := NULL; END;
 
 IF v_E_ParentFIRSNmb is Null
 THEN
 BEGIN -- No Parent FIRSNmb for Branch
 --r SELECT v_E_ParentFIRSNmb = 'HO_Null'
 v_E_ParentFIRSNmb := ' ';
 v_ErrorNmb := 29911;
 v_ReplicationErrorMessageTxt := 'FIRS Nmb of Head Office is NULL.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- No Parent FIRSNmb for Branch
 END IF;
 END; -- Prefix-3
 END IF;
 
 IF v_E_ParentFIRSNmb IS NULL
 THEN
 v_E_ParentFIRSNmb := ' ';
 END IF;
 -- ______________________________________________________________
 -- Get CEO Name
 -- ______________________________________________________________
 v_E_CEOFirstNm := ' ';
 v_E_CEOLastNm := ' ';
 BEGIN
 SELECT Max(PrtCntctNmb) into v_MaxPrtCntctNmb
 FROM PrtLocCntctPosTbl WHERE PrtCntctPosTypCd = 2 AND LocId =v_LocationId; -- 2 = CEO
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtCntctNmb := NULL; END;
 IF v_MaxPrtCntctNmb is not NULL -- CEO Contact exists
 THEN
 BEGIN
 BEGIN
 SELECT substr(PrtCntctFirstNm, 1, 27) || ' ' || substr(LTrim(PrtCntctInitialNm), 1, 1) into v_E_CEOFirstNm
 FROM PrtLocCntctTbl
 WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_CEOFirstNm := NULL; END;
 BEGIN
 SELECT substr(PrtCntctLastNm, 1, 29) into v_E_CEOLastNm
 FROM PrtLocCntctTbl
 WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_CEOLastNm := NULL; END;
 END;
 END IF;
 -- ________________________________________________________________
 -- Get President Name
 -- _______________________________________________________________
 v_E_PresFirstNm := ' ';
 v_E_PresLastNm := ' ';
 BEGIN
 SELECT Max(PrtCntctNmb) into v_MaxPrtCntctNmb
 FROM PrtLocCntctPosTbl WHERE PrtCntctPosTypCd = 1 AND LocId = v_LocationId; -- 1 = President
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtCntctNmb := NULL; END;
 
 IF v_MaxPrtCntctNmb is not NULL -- President Contact exists
 THEN
 BEGIN
 BEGIN
 SELECT substr(PrtCntctFirstNm, 1, 27) || ' ' || substr(LTrim(PrtCntctInitialNm), 1, 1) into v_E_PresFirstNm
 FROM PrtLocCntctTbl WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_PresFirstNm := NULL; END;
 BEGIN
 SELECT substr(PrtCntctLastNm, 1, 29) into v_E_PresLastNm
 FROM PrtLocCntctTbl WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_PresLastNm := NULL; END;
 END;
 END IF;
 -- ___________________________________________________________________
 -- Get Loan Manager Name
 -- __________________________________________________________________
 v_E_LoanMgrFirstNm := ' ';
 v_E_LoanMgrLastNm := ' ';
 BEGIN
 SELECT Max(PrtCntctNmb) into v_MaxPrtCntctNmb
 FROM PrtLocCntctPosTbl WHERE PrtCntctPosTypCd = 3 AND LocId = v_LocationId; -- 3 = Loan Officer
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtCntctNmb := NULL; END;
 IF v_MaxPrtCntctNmb is not NULL -- Loan Officer Contact exists
 THEN
 BEGIN
 BEGIN
 SELECT substr(PrtCntctFirstNm, 1, 27) || ' ' || substr(LTrim(PrtCntctInitialNm), 1, 1) into v_E_LoanMgrFirstNm
 FROM PrtLocCntctTbl WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_LoanMgrFirstNm := NULL; END;
 BEGIN
 SELECT substr(PrtCntctLastNm, 1, 29) into v_E_LoanMgrLastNm
 FROM PrtLocCntctTbl WHERE LocId = v_LocationId AND PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_LoanMgrLastNm := NULL; END;
 END;
 ELSE
 BEGIN -- If No Loan Manager exists for the location get the Head Office
 -- Loan Manager.
 IF v_FIRSRecordTyp = 'BO'
 THEN
 BEGIN
 BEGIN
 SELECT Max(PrtCntctNmb) into v_MaxPrtCntctNmb
 FROM PrtLocCntctPosTbl cp,PrtLocTbl l WHERE l.PrtId = v_PartnerId AND l.PrtLocFIRSRecrdTyp = 'HO'
 AND cp.LocId =l.LocId AND cp.PrtCntctPosTypCd = 3;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxPrtCntctNmb := NULL; END;
 IF v_MaxPrtCntctNmb is not NULL -- Loan Officer Contact exists
 THEN
 BEGIN
 BEGIN
 SELECT substr(ct.PrtCntctFirstNm, 1, 27) || ' ' || substr(LTrim(ct.PrtCntctInitialNm), 1, 1) into v_E_LoanMgrFirstNm
 FROM PrtLocCntctTbl ct,PrtLocTbl l WHERE l.PrtId = v_PartnerId AND l.PrtLocFIRSRecrdTyp = 'HO'
 AND ct.LocId =l.LocId AND ct.PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_LoanMgrFirstNm := NULL; END;
 BEGIN
 SELECT substr(PrtCntctLastNm, 1, 29) into v_E_LoanMgrLastNm
 FROM PrtLocCntctTbl ct,PrtLocTbl l WHERE l.PrtId = v_PartnerId AND l.PrtLocFIRSRecrdTyp = 'HO'
 AND ct.LocId =l.LocId AND ct.PrtCntctNmb = v_MaxPrtCntctNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_E_LoanMgrLastNm := NULL; END;
 END;
 END IF;
 END; -- get the Head Office Loan Manager.
 END IF;
 END; -- Loan Officer
 END IF;
 -- ______________________________________________________________
 -- Get Telephone data
 /*
 IAC 04/23/02 - Column ValidPrtRoleTyp does not exist any more
 Therefore we need to Remove:
 and EA.ValidPrtRoleTyp = 'Main'
 Replace it with:
 and EA.PrtCntctNmb is Null
 */
 -- ______________________________________________________________
 v_ElectronicAddressTxt := ' ';
 v_E_PhoneNmb := ' ';
 v_E_FaxNmb := ' ';
 v_E_FaxNmbExt := ' ';
 BEGIN
 SELECT EA.ElctrncAddrTxt into v_ElectronicAddressTxt
 FROM partner.ElctrncAddrTbl EA Where EA.LocId = v_LocationId and EA.ValidElctrncAddrTyp = 'Phone'
 and EA.PrtCntctNmb is Null and EA.ElctrncAddrNmb = (SELECT Max(EA.ElctrncAddrNmb) FROM partner.ElctrncAddrTbl EA
 Where EA.LocId = v_LocationId and EA.ValidElctrncAddrTyp = 'Phone'
 and EA.PrtCntctNmb is Null);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_ElectronicAddressTxt := NULL; END;
 IF (Trim(v_ElectronicAddressTxt) is Not Null)
 THEN
 BEGIN
 IF (v_IntlAddrInd = 'N')
 THEN
 v_E_PhoneNmb := substr(v_ElectronicAddressTxt, 1, 3) || substr(v_ElectronicAddressTxt, 5, 3) ||
 substr(v_ElectronicAddressTxt, 9, 4) || substr(v_ElectronicAddressTxt, 18, 5);
 ELSE
 v_E_PhoneNmb := substr(trim(v_ElectronicAddressTxt),1,15);
 END IF;
 END;
 END IF;
 -- _____________________________________________________________
 -- Get Fax data ###
 -- _____________________________________________________________
 v_ElectronicAddressTxt := ' ';
 BEGIN
 SELECT EA.ElctrncAddrTxt into v_ElectronicAddressTxt
 FROM partner.ElctrncAddrTbl EA Where EA.LocId = v_LocationId and EA.ValidElctrncAddrTyp = 'Fax'
 and EA.PrtCntctNmb is Null and EA.ElctrncAddrNmb = (SELECT Max(EA.ElctrncAddrNmb) FROM partner.ElctrncAddrTbl EA
 Where EA.LocId = v_LocationId and EA.ValidElctrncAddrTyp = 'Fax'
 and EA.PrtCntctNmb is Null);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_ElectronicAddressTxt := NULL; END;
 IF Trim(v_ElectronicAddressTxt) is Not Null
 THEN
 BEGIN
 IF (v_IntlAddrInd = 'N')
 THEN
 BEGIN
 v_E_FaxNmb := substr(v_ElectronicAddressTxt, 1, 3) || substr(v_ElectronicAddressTxt, 5, 3) ||
 substr(v_ElectronicAddressTxt, 9, 4);
 v_E_FaxNmbExt := NVL(substr(v_ElectronicAddressTxt, 18, 4), ' ');
 END;
 ELSE
 v_E_FaxNmb := substr(v_ElectronicAddressTxt,1,10);
 END IF;
 END;
 END IF;
 -- ____________________________________________________________
 -- v_TotalAssets - No Db field yet
 -- v_TotalDeposits - No Db field yet
 -- *************************************************************
 -- Insert row in MasterExtract
 -- ************************************************************
 INSERT INTO partner.PrtMstrExtrctTbl
 (PrtMstrExtrctFIRSNmb,
 PrtMstrExtrctBnkNm1,
 PrtMstrExtrctBnkNm2,
 PrtMstrExtrctPOBoxNmb,
 PrtMstrExtrctCtyNm,
 PrtMstrExtrctStrNmb,
 PrtMstrExtrctCntyNm,
 PrtMstrExtrctCntyCd,
 PrtMstrExtrctStCd,
 PrtMstrExtrctZipCd,
 PrtMstrExtrctMailStrNm,
 PrtMstrExtrctMailCtyNm,
 PrtMstrExtrctMailStCd,
 PrtMstrExtrctMailZipCd,
 PrtMstrExtrctPresFirstNm,
 PrtMstrExtrctPresLastNm,
 PrtMstrExtrctCEOFirstNm,
 PrtMstrExtrctCEOLastNm,
 PrtMstrExtrctTotAssets,
 PrtMstrExtrctDep,
 PrtMstrExtrctFDICNmb,
 PrtMstrExtrctFRBNmb,
 PrtMstrExtrctNCUANmb,
 PrtMstrExtrctPhnNmb,
 PrtMstrExtrctFaxNmb,
 PrtMstrExtrctFaxNmbExtn,
 PrtMstrExtrctLoanMgrFirstNm,
 PrtMstrExtrctLoanMgrLastNm,
 PrtMstrExtrctInstTyp,
 PrtMstrExtrctOfcTypCd,
 PrtMstrExtrctChrtrTyp,
 PrtMstrExtrctTIN,
 PrtMstrExtrctClsInd,
 PrtMstrExtrctParntFIRSNmb,
 PrtMstrExtrctPrefix,
 PrtMstrExtrctCurAgrmtInd,
 PrtMstrExtrctCntCd)
 VALUES
 (v_FIRSNmb,
 v_E_BankNm1,
 v_E_BankNm2,
 v_E_POBox,
 v_E_City,
 v_E_Street,
 v_E_CountyNm,
 v_E_CountyCd,
 v_E_StateCd,
 v_E_ZipCd,
 v_E_MailStreet,
 v_E_MailCity,
 v_E_MailStateCd,
 v_E_MailZipCd,
 v_E_PresFirstNm,
 v_E_PresLastNm,
 v_E_CEOFirstNm,
 v_E_CEOLastNm,
 v_E_TotalAssets,
 v_E_Deposits,
 v_E_FDICNmb,
 v_E_FRBNmb,
 v_E_NCUANmb,
 v_E_PhoneNmb,
 v_E_FaxNmb,
 v_E_FaxNmbExt,
 v_E_LoanMgrFirstNm,
 v_E_LoanMgrLastNm,
 v_E_InstitutionTyp,
 v_E_OfficeTypeCd,
 v_E_CharterType,
 v_E_TIN,
 v_E_ClosedInd,
 v_E_ParentFIRSNmb,
 v_E_Prefix,
 v_E_CurrentAgreementInd,
 v_E_CntCd);
 IF SQLCODE = 0 
 THEN
 BEGIN
 UPDATE partner.LocExtrctStatTbl
 SET LocExtrctStatExtrctInd = 'Y',
 LocExtrctStatCreatUserId = USER,
 LocExtrctStatCreatDt = SYSDATE
 WHERE LocId = v_LocationId;
 END;
 ELSE
 BEGIN
 v_ErrorNmb := 29900;
 v_ReplicationErrorMessageTxt := 'Error on Insert of PartnerMasterExtract. Row NOT inserted.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ExtractPartnersForFIRSCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error on Insert
 END IF;
 -- --------------------------------------------------------
 <<NextOne>>
 BEGIN
 -- Extract variables
 -- v_E_TotalAssets = '0000000000000',
 -- v_E_Deposits = '0000000000000',
 -- Error Variables
 v_ErrorNmb := 0;
 v_ReplicationErrorMessageTxt := Null;
 -- PartnerLocation Variables
 v_LocationId := 0;
 v_PartnerId := 0;
 v_LocationNm := Null;
 v_LocationType := Null;
 v_LOC_ID := 0;
 v_INST_ID := 0;
 -- Partner Variables
 v_CharterType := Null;
 -- Inst_Subcategory variable
 v_code := 0;
 --r
 v_MaxPrtOwnrshpPct := NULL;
 v_MaxPrtCntctNmb := NULL;
 END;
 END;
 END LOOP;
 /*EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 raise_application_error(-20001,v_ExtractLocationId);
 END;*/
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


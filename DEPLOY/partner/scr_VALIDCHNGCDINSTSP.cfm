<!--- Saved 05/05/2015 13:19:39. --->
PROCEDURE VALIDCHNGCDINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidChngCd VARCHAR2 :=NULL,
 p_ValidChngCdDesc VARCHAR2 :=NULL,
 p_ValidChngCdTyp CHAR :=NULL,
 p_ValidChngCdDispOrdNmb NUMBER:=0,
 p_ValidChngCdCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidChngCdIns;
 IF p_Identifier= 0 THEN
 /* Insert into Action Table */
 BEGIN
 INSERT INTO ValidChngCdTbl 
 (
 ValidChngCd, 
 ValidChngCdDesc, 
 ValidChngCdTyp, 
 ValidChngCdDispOrdNmb, 
 ValidChngCdCreatUserId
 )
 VALUES (
 p_ValidChngCd, 
 p_ValidChngCdDesc, 
 p_ValidChngCdTyp, 
 p_ValidChngCdDispOrdNmb, 
 p_ValidChngCdCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDCHNGCDINSTSP;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:14. --->
PROCEDURE PRTLOCDELCSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_LocId IN NUMBER DEFAULT NULL)
 AS
 OldDataRec PrtLocTbl%ROWTYPE;
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtLocTbl where LocId = p_LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 DELETE PrtLocTbl 
 WHERE LocId = p_LocId;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END PRTLOCDELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


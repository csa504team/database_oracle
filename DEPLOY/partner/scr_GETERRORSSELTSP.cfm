<!--- Saved 03/21/2016 14:40:40. --->
PROCEDURE GETERRORSSELTSP(
 p_Identifier number :=null,
 p_errorNmb number,
 p_SelCur OUT SYS_REFCURSOR,
 p_rownum number :=0,
 p_errorType CHAR := NULL )
 
 AS
 BEGIN
 
 IF p_Identifier = 0
 THEN
 BEGIN 
 OPEN p_SelCur FOR 
 SELECT re.PrtReplicationErrNmb, e.ErrMsgTxt, 
 count(distinct re.PrtReplicationErrId) as ErrCount
 FROM partner.PrtReplicationErrTbl re, partner.ErrTbl e 
 WHERE re.PrtReplicationErrNmb = e.ErrNmb
 and case when lower(p_errorType) = 'open' and re.PrtReplicationErrRslvDt IS NULL then 1
 when lower(p_errorType) <> 'open' and re.PrtReplicationErrRslvDt IS NOT NULL then 1
 else 0
 end = 1
 --AND re.PrtReplicationErrRslvDt IS NULL
 GROUP BY re.PrtReplicationErrNmb,e.ErrMsgTxt; 
 
 END;
 
 ELSIF p_Identifier = 2
 
 THEN 
 BEGIN
 
 
 OPEN p_SelCur FOR
 SELECT ROWNUM, 
 To_char(PrtReplicationErrTime,'MM/DD/YYYY') as ErrDate,
 PrtReplicationErrMsgTxt,INST_ID, 
 LOC_ID ,
 To_char(PrtReplicationErrRslvDt,'MM/DD/YYYY') as ResolvedDate, 
 PrtReplicationErrRemrksTxt 
 FROM partner.PrtReplicationErrTbl re
 WHERE PrtReplicationErrNmb = nvl(p_errorNmb,PrtReplicationErrNmb)
 AND ROWNUM <= p_rownum 
 and case when lower(p_errorType) = 'open' and re.PrtReplicationErrRslvDt IS NULL then 1
 when lower(p_errorType) <> 'open' and re.PrtReplicationErrRslvDt IS NOT NULL then 1
 else 0
 end = 1 
 ORDER BY PrtReplicationErrId;
 
 
 
 END;
 END IF;
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:06. --->
PROCEDURE VALIDPRTPRIMCATSELCSP
 (
 p_userName VARCHAR2:= NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidPrtPrimCatTyp, 
 ValidPrtPrimCatTypDesc
 FROM ValidPrtPrimCatTbl 
 WHERE ValidPrtPrimCatDispOrdNmb != 0 
 ORDER BY ValidPrtPrimCatDispOrdNmb ;
 p_RetVal := SQL%ROWCOUNT; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


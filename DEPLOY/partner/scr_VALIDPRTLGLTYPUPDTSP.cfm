<!--- Saved 05/05/2015 13:20:01. --->
PROCEDURE VALIDPRTLGLTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtLglTyp VARCHAR2:=NULL,
 p_ValidPrtLglTypDispOrdNmb NUMBER:=0,
 p_ValidPrtLglTypDesc VARCHAR2:=NULL,
 p_ValidPrtLglTypCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 UPDATE PARTNER.ValidPrtLglTypTbl
 SET 
 ValidPrtLglTypDispOrdNmb = p_ValidPrtLglTypDispOrdNmb, 
 ValidPrtLglTypDesc = p_ValidPrtLglTypDesc, 
 ValidPrtLglTypCreatUserId = p_ValidPrtLglTypCreatUserId, 
 ValidPrtLglTypCreatDt = SYSDATE
 WHERE ValidPrtLglTyp = p_ValidPrtLglTyp;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF;
 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 --ROLLBACK TO VALIDPRTLGLTYPUPDTSP;
 ROLLBACK;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


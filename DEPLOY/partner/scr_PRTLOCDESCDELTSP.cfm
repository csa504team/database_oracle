<!--- Saved 05/05/2015 13:18:16. --->
PROCEDURE PRTLOCDESCDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtLocDescSeqNmb NUMBER := 0)
 AS
 BEGIN
 SAVEPOINT PrtLocDescDel;
 IF p_Identifier = 0 THEN
 /* Delete PrtLocDescTbl Table based on Primary Key*/
 BEGIN
 DELETE PrtLocDescTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb 
 and PrtLocDescSeqNmb = p_PrtLocDescSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 /* Delete PrtLocDesc Table */
 BEGIN
 DELETE PrtLocDescTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescDel;
 RAISE;
 
 END PRTLOCDESCDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


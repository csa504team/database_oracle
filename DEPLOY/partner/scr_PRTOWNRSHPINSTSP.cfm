<!--- Saved 01/23/2018 15:32:02. --->
PROCEDURE PRTOWNRSHPINSTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtOwnrshpRank NUMBER := 0,
 p_OwningPrtId NUMBER := 0,
 p_ValidOwnrPrtTyp VARCHAR2 := NULL,
 p_PrtOwnrshpPct NUMBER := 0,
 p_ValidOwndPrtTyp VARCHAR2 := NULL,
 p_PrtOwnrshpCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 -- trigger variable
 NewDataRec PrtOwnrshpTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtOwnrshpIns;
 
 IF p_Identifier = 0
 THEN
 /* Insert into PrtOwnrshpTbl Table */
 BEGIN
 INSERT INTO PARTNER.PrtOwnrshpTbl (PrtId,
 PrtOwnrshpRank,
 OwningPrtId,
 ValidOwnrPrtTyp,
 PrtOwnrshpPct,
 ValidOwndPrtTyp,
 PrtOwnrshpCreatUserId,
 PrtOwnrshpCreatDt,
 lastupdtuserid,
 lastupdtdt)
 VALUES (p_PrtId,
 p_PrtOwnrshpRank,
 p_OwningPrtId,
 p_ValidOwnrPrtTyp,
 p_PrtOwnrshpPct,
 p_ValidOwndPrtTyp,
 p_PrtOwnrshpCreatUserId,
 SYSDATE,
 p_PrtOwnrshpCreatUserId,
 SYSDATE);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtOwnrshpTbl
 WHERE PrtId = p_PrtId AND PrtOwnrshpRank = p_PrtOwnrshpRank;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtOwnrshpInsUpdTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 BEGIN
 RAISE;
 ROLLBACK TO PrtOwnrshpIns;
 END;
 END PRTOWNRSHPINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


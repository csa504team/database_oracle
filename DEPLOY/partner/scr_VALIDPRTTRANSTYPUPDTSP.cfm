<!--- Saved 05/05/2015 13:20:14. --->
PROCEDURE VALIDPRTTRANSTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtTransTyp VARCHAR2 :=NULL,
 p_ValidPrtTransTypDispOrdNmb NUMBER:=0,
 p_ValidPrtTransTypDesc VARCHAR2 :=NULL,
 p_ValidPrtTransTypCreatUserId VARCHAR2 :=NULL
 
 )
 AS
 Begin
 SAVEPOINT ValidPrtTransTypUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE ValidPrtTransTypTbl
 SET 
 ValidPrtTransTypDispOrdNmb =p_ValidPrtTransTypDispOrdNmb, 
 ValidPrtTransTypDesc =p_ValidPrtTransTypDesc, 
 ValidPrtTransTypCreatUserId =p_ValidPrtTransTypCreatUserId, 
 ValidPrtTransTypCreatDt =SYSDATE
 WHERE ValidPrtTransTyp =p_ValidPrtTransTyp;
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTTRANSTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


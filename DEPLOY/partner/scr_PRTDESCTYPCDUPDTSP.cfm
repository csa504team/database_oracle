<!--- Saved 05/05/2015 13:17:36. --->
PROCEDURE PRTDESCTYPCDUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtDescTypCd NUMBER := 0,
 p_PrtDescTypTxt VARCHAR2 := NULL,
 p_PrtDescTypCdCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtDescTypCdUpd;
 IF p_Identifier = 0 THEN
 /* Update into PrtDescTypCd Table */
 BEGIN
 UPDATE PARTNER.PrtDescTypCdTbl
 SET PrtDescTypTxt = p_PrtDescTypTxt, 
 PrtDescTypCdCreatUserId = p_PrtDescTypCdCreatUserId,
 PrtDescTypCdCreatDT =SYSDATE
 WHERE PrtDescTypCd = p_PrtDescTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtDescTypCdUpd;
 RAISE;
 
 END PRTDESCTYPCDUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/03/2016 15:13:11. --->
procedure SrchNoteCSP
 (
 p_NoteSubjTxt varchar2 := Null,
 p_LocId number := Null,
 p_NoteEffDate date,
 p_StartDate date,
 p_EndDate date,
 p_PrtNoteEffDt date,
 p_NoteSubjTyp varchar2 := Null,
 p_AuthorFirstNm char := Null,
 p_AuthorLastNm char := Null,
 p_SbaOfcCd number := Null,
 P_NoteSubjSrchInd CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR
 
 )
 AS
 v_NoteSubjTxt char(200);
 v_AuthorFirstNm CHAR(200);
 v_AuthorLastNm CHAR(200);
 
 
 
 BEGIN
 
 if p_NoteSubjSrchInd='C' then
 
 v_NoteSubjTxt := '%'|| upper(p_NoteSubjTxt) ||'%';
 v_AuthorFirstNm := '%'|| upper(p_AuthorFirstNm) ||'%';
 v_AuthorLastNm := '%'|| upper(p_AuthorLastNm) ||'%';
 
 
 elsif p_NoteSubjSrchInd ='S' then
 
 v_NoteSubjTxt := upper(p_NoteSubjTxt) ||'%';
 v_AuthorFirstNm := upper(p_AuthorFirstNm) ||'%';
 v_AuthorLastNm := upper(v_AuthorLastNm) ||'%';
 
 elsif p_NoteSubjSrchInd ='E' then
 
 v_NoteSubjTxt := upper(p_NoteSubjTxt);
 v_AuthorFirstNm := upper(p_AuthorFirstNm);
 v_AuthorLastNm := upper(p_AuthorLastNm);
 else
 v_NoteSubjTxt := upper(p_NoteSubjTxt);
 v_AuthorFirstNm := upper(p_AuthorFirstNm);
 v_AuthorLastNm := upper(p_AuthorLastNm);
 end if;
 
 
 
 OPEN p_SelCur FOR 
 SELECT n.LocId,
 n.PrtNoteSubjTxt,
 n.PrtNoteSeqNmb,
 n.EmployeeId,
 n.ValidPrtNoteSubjTypCd,
 n.PrtNoteEffDt,
 v.ValidPrtNoteSubjTypDescTxt,
 l.PrtLocNm,
 l.PrtId ,
 e.IMUserFirstNm, 
 e.IMUserLastNm,
 e.SBAOfcCd, 
 o.SBAOfc1Nm
 FROM partner.PrtNoteTbl n, partner.PrtLocTbl l, security.IMUserTbl e,
 partner.ValidPrtNoteSubjTypTbl v , sbaref.SBAOfcTbl o
 WHERE l.LocId = n.LocId
 AND n.IMUserNm = e.IMUserNm
 AND n.ValidPrtNoteSubjTypCd = v.ValidPrtNoteSubjTypCd 
 AND e.SBAOfcCd = o.SBAOfcCd
 AND upper(n.PrtNoteSubjTxt) like trim(NVL(v_NoteSubjTxt,upper(n.PrtNoteSubjTxt)))
 AND n.locId = NVL(p_locId,n.locId)
 AND TRUNC(n.PrtNoteEffDt ) = TRUNC(NVL(p_PrtNoteEffDt,n.PrtNoteEffDt))
 AND TRUNC(n.PrtNoteEffDt) >= TRUNC(NVL(p_StartDate, n.PrtNoteEffDt ))
 AND TRUNC(n.PrtNoteEffDt) <= TRUNC(NVL(p_EndDate, n.PrtNoteEffDt ))
 AND v.ValidPrtNoteSubjTypCd = NVL(p_NoteSubjTyp,v.ValidPrtNoteSubjTypCd)
 AND upper(e.IMUserFirstNm) like trim(NVL(v_AuthorFirstNm,upper(e.IMUserFirstNm)))
 AND upper(e. IMUserLastNm) like trim(NVL(v_AuthorLastNm,upper(e. IMUserLastNm)))
 AND e.SBAOfcCd = NVL(P_SbaOfcCd,e.SBAOfcCd)
 ORDER BY n.PrtNoteEffDt DESC;
 
 
 
 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


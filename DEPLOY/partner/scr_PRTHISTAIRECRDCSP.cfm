<!--- Saved 05/05/2015 13:17:40. --->
PROCEDURE PRTHISTAIRECRDCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_PrtId NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtHistAIRecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR 
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR 
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR 
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt >= p_ReportBegDateTime 
 AND a.PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt <= p_ReportEndDateTime 
 AND a.PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 IF(p_PrtId is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(140)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' )
 AND a.PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSE
 BEGIN
 /*[SPCONV-ERR(155)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm AffectedTitle,
 i.PrtHistryLglNm InitiatorTitle,
 i.PrtHistryLglNm InitiatorCity,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.PrtHistryCreatDt DateAddedToPIMS
 FROM PrtHistryTbl a, PrtHistryTbl i
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.PrtHistryPartyTyp = 'A' 
 AND a.PrtId = i.PrtId 
 AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND a.PrtHistryCreatDt LIKE ( SUBSTR(SYSDATE,1, 11) || '%' ) 
 AND a.PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 END;
 END IF; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtHistAIRecrdP;
 RAISE;
 
 END PRTHISTAIRECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:35. --->
PROCEDURE VALIDACHACCTTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_ValidACHAcctTyp VARCHAR2 :=NULL,
 p_ValidACHAcctTypDesc VARCHAR2 :=NULL,
 p_ValidACHAcctTypDispOrdNmb NUMBER:=0,
 p_ValidACHAcctTypCreatUserId VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN
 SAVEPOINT ValidACHAcctTypUpd;
 IF p_Identifier= 0 THEN
 /* Update IMAdrTyp Table */
 BEGIN
 UPDATE ValidACHAcctTypTbl
 SET ValidACHAcctTypDesc =p_ValidACHAcctTypDesc, 
 ValidACHAcctTypDispOrdNmb =p_ValidACHAcctTypDispOrdNmb, 
 ValidACHAcctTypCreatUserId =p_ValidACHAcctTypCreatUserId,
 ValidACHAcctTypCreatDt =SYSDATE
 WHERE ValidACHAcctTyp =p_ValidACHAcctTyp ; 
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDACHACCTTYPUPDTSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


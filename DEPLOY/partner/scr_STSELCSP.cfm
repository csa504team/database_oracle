<!--- Saved 05/05/2015 13:19:30. --->
PROCEDURE STSELCSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT StCd,
 StNm
 FROM SBAREF.StTbl 
 WHERE (StEndDt > SYSDATE or StEndDt IS NULL) 
 ORDER BY StNm;
 END STSELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:29. --->
PROCEDURE LOCSRCHUPDPRTCSP(
 p_LocId number := NULL
 )
 AS
 --
 -- Refreshes the partner data in a specified row
 -- of the LocSrchTbl table.
 --
 -- Parameters:
 -- @LocId is the LocId of the location whose
 -- search data is being refreshed.
 --
 -- ---------------------------------------------------
 -- ---------------- Revision History -----------------
 -- ---------------------------------------------------
 -- 08/30/2000 C.Woodard Created.
 -- 04/05/2001 Rajeswari DSP Modified code to include RolePrivilege
 -- in search tables.
 -- ---------------------------------------------------
 -- ---------------------------------------------------
 v_PrtId number(7);
 v_SubcategoryCd varchar2(5);
 v_RolePrivilege varchar2(20);
 v_PrtCreatUserId varchar2(32);
 v_PrtCreatDt date;
 -- Error Handling variables
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 BEGIN
 v_INST_ID := 0;
 v_LOC_ID := 0;
 
 --Error checking
 IF p_LocId IS NULL
 THEN
 RETURN;
 END IF;
 
 SELECT p.PrtId, p.ValidPrtSubCatTyp, p.PrtCreatUserId, p.PrtCreatDt INTO v_PrtId, v_SubcategoryCd, v_PrtCreatUserId, v_PrtCreatDt
 FROM partner.PrtTbl p, partner.PrtLocTbl l
 WHERE l.LocId = p_LocId
 AND p.PrtId = l.PrtId;
 
 SELECT RolePrivilegePrgrmId into v_RolePrivilege FROM partner.ValidPrtSubCatTbl WHERE ValidPrtSubCatTyp = v_SubcategoryCd;
 
 UPDATE partner.LocSrchTbl
 SET PrtId = v_PrtId,
 ValidPrtSubCatTyp = v_SubcategoryCd,
 RolePrivilegePrgrmId = v_RolePrivilege, 
 LASTUPDTUSERID = v_PrtCreatUserId,
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; /* Use system message*/
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'NewLocSrchUpdPrtCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 51101;
 v_ReplicationErrorMessageTxt := 'Error updating LocSrchTbl. LocId=' || to_char(p_LocId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'NewLocSrchUpdPrtCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END;
 END IF;
 RETURN;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


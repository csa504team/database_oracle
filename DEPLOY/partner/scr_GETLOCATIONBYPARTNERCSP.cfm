<!--- Saved 03/31/2016 08:19:57. --->
PROCEDURE GETLOCATIONBYPARTNERCSP
 (
 p_locId number :=null ,
 p_prtId number := null,
 p_PhyAddrCtySrchNm varchar2 :=null,
 p_Identifier number :=null,
 p_STCD varchar2 := null,
 p_rowCount number := null,
 p_queryType varchar2 := null,
 p_dispOrdNmb number := null,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 v_sqltxt varchar2(2000);
 BEGIN
 v_sqltxt := 'SELECT LocId, ValidLocTyp, ValidLocTypDispOrdNmb,PhyAddrStr1Txt, 
 PhyAddrCtyNm, PhyAddrStCd,ValidPrtSubCatTyp, ValidLocTypDesc,PhyAddrCtySrchNm,
 PhyAddrPostCd, PrtLocNm , PrtLocCurStatCd, dispOrder
 FROM ( 
 SELECT a.LocId, a.ValidLocTyp, a.ValidLocTypDispOrdNmb,a.PhyAddrStr1Txt, a.PhyAddrCtyNm, a.PhyAddrStCd,
 a.ValidPrtSubCatTyp,a.PhyAddrCtySrchNm, a.PhyAddrPostCd,a. PrtLocNm ,a. PrtLocCurStatCd, b.ValidLocTypDesc,
 CASE WHEN a.ValidLocTyp = ''HO'' THEN 0 
 ELSE 1 END AS dispOrder
 FROM partner.LocSrchTbl a, partner.ValidLocTypTbl b 
 WHERE a.PrtId = '''||p_prtId||''' AND a.PrtLocCurStatCd in(''O'',''P'')
 AND a.ValidLocTyp = b.ValidLocTyp';
 
 If p_querytype = 'next' then 
 v_sqltxt := v_sqltxt || ' AND (
 (a.PhyAddrStCd > '''||p_STCD||''') OR 
 (a.PhyAddrStCd = '''||p_STCD||''' AND a.PhyAddrCtySrchNm > '''||p_PhyAddrCtySrchNm||''' ) OR 
 (a.PhyAddrStCd = '''||p_STCD||''' AND a.PhyAddrCtySrchNm = '''||p_PhyAddrCtySrchNm||''' AND 
 a.LocId > '''||p_LOCID||''' )
 )
 AND a.ValidLocTypDispOrdNmb >= '||p_dispOrdNmb;
 End If; 
 v_sqltxt := v_sqltxt || ') WHERE ROWNUM <= '||p_rowCount;
 
 
 open p_SelCur for v_sqltxt; 
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


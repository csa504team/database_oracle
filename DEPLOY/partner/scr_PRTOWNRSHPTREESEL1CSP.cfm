<!--- Saved 05/05/2015 13:19:02. --->
PROCEDURE PRTOWNRSHPTREESEL1CSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_PrtId IN NUMBER DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal out number)
 AS
 BEGIN
 SAVEPOINT PRTOWNRSHPTREESEL1;
 OPEN p_SelCur FOR
 SELECT DISTINCT r.PrtId PartnerId,
 CASE 
 WHEN r.LevelNmb=0 THEN RPAD( r.LevelNmb, 1, ' ') || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=1 THEN '|' || LPAD('-', 5,'-') || RPAD( r.LevelNmb,1, ' ') || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=2 THEN '|' || LPAD('-', 5,'-') || '|' || LPAD('-',5,'-') || RPAD( r.LevelNmb, 1, ' ')
 || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=3 THEN '|' || LPAD('-', 5,'-') || '|' || LPAD('-',5,'-') || '|' || LPAD('-', 5,'-')
 || RPAD( r.LevelNmb, 1, ' ') || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=4 THEN '|' || LPAD('-', 5,'-') || '|' || LPAD('-',5,'-') || '|' || LPAD('-', 5,'-')
 || '|' || LPAD('-', 5,'-') || RPAD( r.LevelNmb, 1, ' ') || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=5 THEN '|' || LPAD('-', 5,'-') || '|' || LPAD('-',5,'-') || '|' || LPAD('-', 5,'-')
 || '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || RPAD( r.LevelNmb, 1, 
 ' ') || ' ' || r.PrtLglNm 
 WHEN r.LevelNmb=6 THEN '|' || LPAD('-', 5,'-') || '|' || LPAD('-',5,'-') || '|' || LPAD('-', 5,'-')
 || '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-')
 || RPAD(r.LevelNmb, 1, ' ') || ' ' || r.PrtLglNm 
 ELSE '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || '|' ||
 LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || '|' || LPAD('-', 5,'-') || '|' 
 || LPAD('-',5,'-') || RPAD( r.LevelNmb, 1, ' ') || ' ' || r.PrtLglNm 
 END PartnerName,
 to_char(r.PrtOwnrshpPct) PrtOwnrshpPct,
 p.LocQty,
 p.ValidPrtPrimCatTyp,
 p.ValidPrtSubCatTyp,
 p.PrtLocFIRSNmb
 FROM PrtOwnrshpTreeTbl o, PrtOwnrshpTreeTbl r, PrtSrchTbl p 
 WHERE o.PrtId = p_PrtId 
 AND o.Root = r.Root 
 AND r.PrtId = p.PrtId 
 ORDER BY r.Root , r.NodeId ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PRTOWNRSHPTREESEL1;
 END;
 END PRTOWNRSHPTREESEL1CSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


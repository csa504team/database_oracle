<!--- Saved 05/05/2015 13:16:28. --->
PROCEDURE LOCSRCHUPDLOCCSP
 (
 p_LocId number := NULL
 )
 AS
 --
 -- Refreshes the location data in a specified row
 -- of the LocSrchTbl table.
 --
 -- Parameters:
 -- @LocId is the LocId of the location whose
 -- search data is being refreshed.
 --
 -- ---------------------------------------------------
 -- ---------------- Revision History -----------------
 -- ---------------------------------------------------
 -- 08/30/2000 C.Woodard Created.
 -- ---------------------------------------------------
 -- ---------------------------------------------------
 v_PrtLocNm varchar2(200);
 v_PrtLocFIRSNmb char(7);
 v_PrtLocFIRSRecrdTyp char(2);
 v_PrtLocOpndDt date;
 v_PrtLocInactDt date;
 v_PrtLocCurStatCd char(1);
 v_ValidLocTyp varchar2(5);
 v_ValidLocTypDispOrdNmb number(3,0);
 v_PrtLocSrchNm varchar2(200);
 v_PrtLocCreatUserId varchar2(32);
 v_PrtLocCreatDt date;
 -- Error Handling variables
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 CurrDataRec PrtLocTbl%ROWTYPE;
 BEGIN
 v_INST_ID := 0;
 v_LOC_ID := 0;
 
 --Error checking
 IF p_LocId IS NULL
 THEN
 RETURN;
 END IF;
 
 SELECT * INTO CurrDataRec FROM PrtLocTbl WHERE LocId = p_LocId;
 BEGIN
 SELECT v.ValidLocTypDispOrdNmb INTO v_ValidLocTypDispOrdNmb FROM PrtLocTbl l, partner.ValidLocTypTbl v
 WHERE l.LocId = p_LocId AND v.ValidLocTyp = l.ValidLocTyp;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_ValidLocTypDispOrdNmb := NULL; END;
 
 v_PrtLocSrchNm := UPPER(CurrDataRec.PrtLocNm);
 
 -- Add in code for "The"
 IF SubStr(CurrDataRec.PrtLocNm,1,4) = 'The '
 THEN
 v_PrtLocSrchNm := Ltrim(substr(v_PrtLocSrchNm,4,length(v_PrtLocSrchNm)-3)||' ,'||substr(v_PrtLocSrchNm,1,4));
 END IF;
 
 UPDATE partner.LocSrchTbl
 SET PrtLocNm = CurrDataRec.PrtLocNm ,
 PrtLocFIRSNmb = CurrDataRec.PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp = CurrDataRec.PrtLocFIRSRecrdTyp ,
 PrtLocOpndDt = CurrDataRec.PrtLocOpndDt,
 PrtLocInactDt = CurrDataRec.PrtLocInactDt,
 PrtLocCurStatCd = CurrDataRec.PrtLocCurStatCd,
 ValidLocTyp = CurrDataRec.ValidLocTyp,
 ValidLocTypDispOrdNmb = v_ValidLocTypDispOrdNmb,
 PrtLocSrchNm = v_PrtLocSrchNm,
 LastUpdtUserId = CurrDataRec.PrtLocCreatUserId,
 LastUpdtDt = CurrDataRec.PrtLocCreatDt
 WHERE LocId = p_LocId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL; /* Use system message*/
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdLocCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 51201;
 v_ReplicationErrorMessageTxt := 'Error updating LocSrchTbl. LocId=' || to_char(p_LocId);
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'LocSrchUpdLocCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


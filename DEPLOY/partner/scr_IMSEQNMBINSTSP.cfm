<!--- Saved 05/05/2015 13:16:16. --->
PROCEDURE IMSEQNMBINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_IMSeqId VARCHAR2 := NULL,
 p_IMSeqNmb NUMBER := 0,
 p_CreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT IMSeqNmbIns;
 IF p_Identifier = 0 THEN
 BEGIN
 INSERT INTO IMSeqNmbTbl (IMSeqId, IMSeqNmb, CreatUserId, CreatDt)
 VALUES (p_IMSeqId, p_IMSeqNmb, p_CreatUserId, SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 END IMSEQNMBINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


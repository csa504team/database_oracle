<!--- Saved 05/05/2015 13:17:57. --->
PROCEDURE PRTLOCCLSFSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocClsfSeqNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 error NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtLocClsfSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocClsfSeqNmb,
 ValidPrtLocClsfCd,
 PrtLocClsfCreatUserId,
 PrtLocClsfCreatDt,
 LASTUPDTUSERID,
 LASTUPDTDT
 FROM PrtLocClsfTbl 
 WHERE LocId = p_LocId 
 AND PrtLocClsfSeqNmb = p_PrtLocClsfSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocClsfTbl 
 WHERE LocId = p_LocId 
 AND PrtLocClsfSeqNmb = p_PrtLocClsfSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT lc.LocId,
 lc.PrtLocClsfSeqNmb,
 lc.ValidPrtLocClsfCd,
 vlc.ValidPrtLocClsfDescTxt
 FROM PrtLocClsfTbl lc, ValidPrtLocClsfCdTbl vlc 
 WHERE lc.LocId = p_LocId 
 AND lc.ValidPrtLocClsfCd = vlc.ValidPrtLocClsfCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocClsfSel;
 RAISE;
 
 END PRTLOCCLSFSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


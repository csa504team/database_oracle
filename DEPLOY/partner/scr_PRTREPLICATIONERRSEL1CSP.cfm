<!--- Saved 05/05/2015 13:19:07. --->
PROCEDURE PRTREPLICATIONERRSEL1CSP(
 p_ErrNmb IN NUMBER DEFAULT NULL,
 p_ReportBegDateTime IN DATE DEFAULT NULL,
 p_ReportEndDateTime IN DATE DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.PrtReplicationErrNmb,
 RPAD(TO_CHAR(e.PrtReplicationErrTime,'mm/dd/yyyy'), 10, ' '),
 e.INST_ID,
 e.LOC_ID,
 PrtReplicationErrProcNm,
 PrtReplicationErrMsgTxt
 FROM PrtReplicationErrTbl e 
 WHERE e.PrtReplicationErrNmb = p_ErrNmb 
 AND (PrtReplicationErrTime >= p_ReportBegDateTime 
 AND PrtReplicationErrTime <= p_ReportEndDateTime);
 END PRTREPLICATIONERRSEL1CSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


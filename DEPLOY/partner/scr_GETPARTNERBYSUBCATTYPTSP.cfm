<!--- Saved 04/25/2016 15:54:54. --->
PROCEDURE getPartnerBySubCatTypTSP (
 p_srchType CHAR := NULL,
 p_subCatTyp VARCHAR := null,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 IF p_srchType = 'Partner'
 THEN
 OPEN p_SelCur FOR
 SELECT DISTINCT
 ps.PrtId,
 ps.PrtLglNm,
 ps.PhyAddrStr1Txt,
 (SELECT PhyAddrStr2Txt
 FROM partner.PhyAddrTbl a
 WHERE a.LocId = ps.LocId
 AND PrtCntctNmb IS NULL
 AND ValidAddrTyp = 'Phys'
 AND PhyAddrSeqNmb =
 (SELECT MAX (PhyAddrSeqNmb)
 FROM partner.PhyAddrTbl z
 WHERE a.LocId = z.LocId
 AND z.PrtCntctNmb IS NULL
 AND z.ValidAddrTyp = a.ValidAddrTyp))
 AS PhyAddrStr2Txt,
 ps.PhyAddrCtyNm,
 ps.PhyAddrCtySrchNm,
 ps.PhyAddrStCd,
 ps.PhyAddrPostCd,
 s.StNm,
 ao.StCd
 FROM partner.PrtSrchTbl ps,
 partner.PrtAgrmtTbl pa,
 partner.PrtAreaOfOperTbl ao,
 sbaref.StTbl s
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND PrtCurStatCd = 'O'
 AND ps.PrtId = pa.PrtId
 AND pa.PrtAgrmtEffDt <= SYSDATE
 AND ( ( pa.PrtAgrmtExprDt >= SYSDATE
 OR pa.PrtAgrmtExprDt IS NULL)
 AND ( pa.PrtAgrmtTermDt > SYSDATE
 OR pa.PrtAgrmtTermDt IS NULL))
 AND pa.PrtId = ao.PrtId
 AND pa.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
 AND ao.StCd = s.StCd
 AND ao.PrtAreaOfOperCntyEndDt IS NULL
 ORDER BY s.StNm, ps.PrtLglNm;
 end if;
 
 IF p_srchType = 'contact'
 THEN
 OPEN p_SelCur FOR
 SELECT ps.PrtId,
 c.LocId,
 c.PrtCntctNmb,
 c.PrtCntctFirstNm,
 c.PrtCntctJobTitlNm,
 c.PrtCntctLastNm,
 c.PrtLocCntctMailStopNm,
 'Executive Director' AS ContactRole,
 0 AS Seq
 FROM partner.PrtSrchTbl ps,
 partner.PrtLocCntctTbl c,
 partner.PrtLocCntctPosTbl pc
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.LocId = c.LocId
 AND c.LocId = pc.LocId
 AND c.PrtCntctNmb = pc.PrtCntctNmb
 AND pc.PrtCntctPosTypCd = 4
 UNION
 SELECT ps.PrtId,
 c.LocId,
 c.PrtCntctNmb,
 c.PrtCntctFirstNm,
 c.PrtCntctJobTitlNm,
 c.PrtCntctLastNm,
 c.PrtLocCntctMailStopNm,
 'Microlending' AS ContactRole,
 1 AS Seq
 FROM partner.PrtSrchTbl ps,
 partner.PrtLocCntctTbl c,
 partner.PrtLocCntctSBARoleTbl cr
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.LocId = c.LocId
 AND c.LocId = cr.LocId
 AND c.PrtCntctNmb = cr.PrtCntctNmb
 AND cr.PrtCntctSBARoleTypCd = 1
 ORDER BY PrtId, LocId, Seq;
 END IF; 
 
 if p_srchType ='electronicAddress' then
 
 OPEN p_SelCur FOR
 SELECT ps.PrtId,e.LocId,e.ElctrncAddrNmb,
 e.ValidElctrncAddrTyp,e.PrtCntctNmb,e.ElctrncAddrTxt,
 ps.PhyAddrStCd
 FROM partner.ElctrncAddrTbl e, partner.PrtSrchTbl ps
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.LocId = e.LocId AND e.PrtCntctNmb IS NULL
 
 UNION
 
 SELECT ps.PrtId,e.LocId,
 e.ElctrncAddrNmb,
 e.ValidElctrncAddrTyp,
 e.PrtCntctNmb,
 e.ElctrncAddrTxt,
 ps.PhyAddrStCd
 FROM partner.ElctrncAddrTbl e,
 partner.PrtSrchTbl ps,partner.PrtLocCntctTbl c,partner.PrtLocCntctPosTbl pc
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.LocId = e.LocId
 AND e.LocId = c.LocId
 AND e.PrtCntctNmb = c.PrtCntctNmb
 AND c.LocId = pc.LocId
 AND c.PrtCntctNmb = pc.PrtCntctNmb
 AND pc.PrtCntctPosTypCd = 4
 
 
 UNION
 
 SELECT ps.PrtId, e.LocId, e.ElctrncAddrNmb,e.ValidElctrncAddrTyp,e.PrtCntctNmb, e.ElctrncAddrTxt,ps.PhyAddrStCd
 FROM partner.ElctrncAddrTbl e, partner.PrtSrchTbl ps,partner.PrtLocCntctTbl c,partner.PrtLocCntctSBARoleTbl cr
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.LocId = e.LocId
 AND e.LocId = c.LocId
 AND e.PrtCntctNmb = c.PrtCntctNmb
 AND c.LocId = cr.LocId
 AND c.PrtCntctNmb = cr.PrtCntctNmb
 AND cr.PrtCntctSBARoleTypCd = 1;
 
 
 end if;
 
 
 if p_srchType ='AgreementDesc' then
 OPEN p_SelCur FOR
 SELECT a.PrtId,ps.PhyAddrStCd,a.PrtAgrmtAreaOfOperTxt
 FROM partner.PrtSrchTbl ps, partner.PrtAgrmtTbl a
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.PrtId = a.PrtId
 AND a.PrtAgrmtEffDt <= sysdate
 AND (a.PrtAgrmtExprDt > sysdate or a.PrtAgrmtExprDt is null)
 AND (a.PrtAgrmtTermDt > sysdate or a.PrtAgrmtTermDt is null);
 end if;
 if p_srchType='AreaOfOperation' then
 OPEN p_SelCur FOR
 SELECT ao.PrtId, ao.PrtAreaOfOperSeqNmb,ao.StCd, ao.CntyCd,ps.PhyAddrStCd, PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt, c.CntyNm
 FROM partner.PrtSrchTbl ps, partner.PrtAgrmtTbl a, partner.PrtAreaOfOperTbl ao,
 sbaref.CntyTbl c
 WHERE ps.ValidPrtSubCatTyp = p_subCatTyp
 AND ps.PrtCurStatCd = 'O'
 AND ps.PrtId = a.PrtId AND a.PrtAgrmtEffDt <= sysdate
 AND (a.PrtAgrmtExprDt > sysdate or a.PrtAgrmtExprDt is null)
 AND (a.PrtAgrmtTermDt > sysdate or a.PrtAgrmtTermDt is null)
 AND a.PrtId = ao.PrtId
 AND a.PrtAgrmtSeqNmb = ao.PrtAgrmtSeqNmb
 AND ao.PrtAreaOfOperCntyEffDt <= sysdate
 AND (ao.PrtAreaOfOperCntyEndDt > sysdate OR ao.PrtAreaOfOperCntyEndDt is NULL)
 AND (ao.StCd = c.StCd AND ao.CntyCd = c.CntyCd)
 ORDER BY ao.PrtId, ao.StCd;
 end if;
 
 
 
 end; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


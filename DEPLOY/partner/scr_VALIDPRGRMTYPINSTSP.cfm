<!--- Saved 11/16/2010 10:11:08. --->
PROCEDURE VALIDPRGRMTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrgrmTyp VARCHAR2 :=NULL,
 p_ValidPrgrmTypDispOrdNmb NUMBER:=0,
 p_ValidPrgrmTypDesc VARCHAR2 :=NULL,
 p_ValidPrgrmTypCreatUserId CHAR :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrgrmTypIns;
 IF p_Identifier= 0 
 THEN
 BEGIN
 INSERT INTO ValidPrgrmTypTbl 
 (
 ValidPrgrmTyp, 
 ValidPrgrmTypDispOrdNmb, 
 ValidPrgrmTypDesc, 
 ValidPrgrmTypCreatUserId
 )
 VALUES (
 p_ValidPrgrmTyp, 
 p_ValidPrgrmTypDispOrdNmb, 
 p_ValidPrgrmTypDesc, 
 NVL(p_ValidPrgrmTypCreatUserId, user)
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDPRGRMTYPINS;
 RAISE;
 END ;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:25. --->
PROCEDURE PRTLOCHISTAIRRECRDCSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 TranFlag NUMBER(10,0);
 StoO_rowcnt NUMBER;
 BEGIN
 SAVEPOINT PrtLocHistAIRRecrd;
 IF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE 
 WHEN a.LocHistryCntCd!=i.LocHistryCntCd THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND i.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND i.LocId = r.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt > p_ReportBegDateTime 
 AND a.LocHistryCreatDt < p_ReportEndDateTime;
 END;
 ELSIF(p_ReportBegDateTime IS NOT NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd 
 THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND i.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND i.LocId = r.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt >= p_ReportBegDateTime;
 END;
 ELSIF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime IS NOT NULL) THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd 
 THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND i.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND i.LocId = r.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt <= p_ReportEndDateTime;
 END;
 END IF;
 IF(p_ReportBegDateTime is NULL AND p_ReportEndDateTime is NULL) THEN
 BEGIN
 /*[SPCONV-ERR(141)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT a.LocHistryAffctPrtId,
 a.LocId,
 a.LocHistryLocNm AffectedTitle,
 r.LocHistryLocNm ResultingTitle,
 a.LocHistryCtyNm AffectedCity,
 a.LocHistryStCd AffectedState,
 i.LocHistryLocNm InitiatorTitle,
 i.LocHistryCtyNm InitiatorCity,
 i.LocHistryStCd InitiatorState,
 CASE WHEN a.LocHistryCntCd!=i.LocHistryCntCd 
 THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry,
 RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 10, ' ') EffectiveDate,
 a.LocHistryCreatDt DateAddedToPIMS
 FROM LocHistryTbl a, LocHistryTbl i, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND i.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND i.LocId = r.LocId 
 AND (i.LocHistryPartyTyp = 'I' AND i.ValidChngCd = p_ValidChngCd) 
 AND (r.LocHistryPartyTyp = 'R' AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE, 1,11) || '%' );
 /*BEGIN
 SELECT 1 INTO StoO_rowcnt FROM DUAL
 WHERE EXISTS (
 SELECT a.LocHistryAffctPrtId, a.LocId, a.LocHistryLocNm AffectedTitle, 
 r.LocHistryLocNm ResultingTitle, a.LocHistryCtyNm AffectedCity, 
 a.LocHistryStCd AffectedState, i.LocHistryLocNm InitiatorTitle, 
 i.LocHistryCtyNm InitiatorCity, i.LocHistryStCd InitiatorState, 
 CASE 
 WHEN a.LocHistryCntCd!=i.LocHistryCntCd THEN i.LocHistryCntCd 
 ELSE NULL 
 END InitiatorCountry, RPAD(TO_CHAR(a.LocHistryEffDt,'mm/dd/yyyy'), 
 10, ' ') EffectiveDate, a.LocHistryCreatDt DateAddedToPIMS 
 FROM LocHistryTbl a, LocHistryTbl i, LocHistryTbl r 
 WHERE a.ValidChngCd = p_ValidChngCd 
 AND a.LocHistryPartyTyp = 'A' 
 AND a.LocHistryAffctPrtId = i.LocHistryAffctPrtId 
 AND i.LocHistryAffctPrtId = r.LocHistryAffctPrtId 
 AND a.LocId = i.LocId 
 AND i.LocId = r.LocId 
 AND 
 (i.LocHistryPartyTyp = 'I' 
 AND i.ValidChngCd = p_ValidChngCd) 
 AND 
 (r.LocHistryPartyTyp = 'R' 
 AND r.ValidChngCd = p_ValidChngCd) 
 AND a.LocHistryCreatDt LIKE ( SUBSTR(SYSDATE, 1, 
 11) || '%' ) );
 END;*/
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocHistAIRRecrd;
 RAISE;
 END PRTLOCHISTAIRRECRDCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:10. --->
PROCEDURE PRTLOCCNTCTSBAROLESELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtLocCntctSBARoleSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtCntctNmb,
 PrtCntctSBARoleTypCd
 FROM PrtLocCntctSBARoleTbl 
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocCntctSBARoleTbl 
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctSBARoleSel;
 RAISE;
 
 END PRTLOCCNTCTSBAROLESELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


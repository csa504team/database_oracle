<!--- Saved 05/05/2015 13:18:33. --->
PROCEDURE PRTLOCSBACNTCTDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocSBACntctSeqNmb NUMBER := 0)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctDel;
 IF p_Identifier = 0 THEN
 /* Delete PrtLocSBACntctTbl based on Primary Key*/
 BEGIN
 DELETE PrtLocSBACntctTbl 
 WHERE LocId = p_LocId 
 and PrtLocSBACntctSeqNmb = p_PrtLocSBACntctSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Delete all PrtLocSBACntctTbl for a given location */
 BEGIN
 DELETE PrtLocSBACntctTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctDel;
 RAISE;
 
 END PRTLOCSBACNTCTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


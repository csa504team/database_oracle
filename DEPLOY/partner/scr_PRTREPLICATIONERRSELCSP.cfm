<!--- Saved 05/05/2015 13:19:08. --->
PROCEDURE PRTREPLICATIONERRSELCSP(
 p_ReportBegDateTime IN DATE DEFAULT NULL,
 p_ReportEndDateTime IN DATE DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT e.PrtReplicationErrNmb,
 m.ErrMsgTxt,
 Count(*)
 FROM PrtReplicationErrTbl e, ErrTbl m 
 WHERE e.PrtReplicationErrNmb = m.ErrNmb 
 AND (PrtReplicationErrTime >= p_ReportBegDateTime 
 AND PrtReplicationErrTime <= p_ReportEndDateTime) 
 GROUP BY e.PrtReplicationErrNmb, m.ErrMsgTxt;
 END PRTREPLICATIONERRSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


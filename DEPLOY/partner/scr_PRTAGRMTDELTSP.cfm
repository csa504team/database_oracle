<!--- Saved 05/05/2015 13:16:53. --->
PROCEDURE PRTAGRMTDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0)
 AS
 OldDataRec PrtAgrmtTbl%ROWTYPE;
 BEGIN
 
 SAVEPOINT PrtAgrmtDel;
 
 IF p_Identifier = 0 THEN
 BEGIN
 --get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 DELETE PrtAgrmtTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAgrmtDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11 THEN
 BEGIN
 --get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtAgrmtTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb AND PrtAgrmtEffDt > SYSDATE;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 DELETE PrtAgrmtTbl 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAgrmtEffDt > SYSDATE;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAgrmtDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO SAVEPOINT PrtAgrmtDel;
 RAISE;
 
 
 END PRTAGRMTDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


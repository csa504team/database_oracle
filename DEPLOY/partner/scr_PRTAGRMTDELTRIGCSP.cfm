<!--- Saved 05/05/2015 13:16:52. --->
PROCEDURE PrtAgrmtDelTrigCSP (
 OldDataRec PrtAgrmtTbl%ROWTYPE
 )
 AS
 v_ErrorMessageTxt varchar2(255);
 v_ErrorNmb number(11);
 v_AgreementStatus char(1);
 BEGIN
 v_AgreementStatus := case when (OldDataRec.PrtAgrmtEffDt <= SYSDATE and (OldDataRec.PrtAgrmtExprDt >= SYSDATE or OldDataRec.PrtAgrmtExprDt IS NULL) 
 and (OldDataRec.PrtAgrmtTermDt > SYSDATE or OldDataRec.PrtAgrmtTermDt IS NULL))
 then 'A'
 when (OldDataRec.PrtAgrmtExprDt < SYSDATE and OldDataRec.PrtAgrmtTermDt IS NULL)
 then 'I'
 when (OldDataRec.PrtAgrmtTermDt is not NULL and OldDataRec.PrtAgrmtTermDt < SYSDATE)
 then 'T'
 when OldDataRec.PrtAgrmtEffDt > SYSDATE
 then 'P' end;
 IF (v_AgreementStatus != 'P')
 THEN
 BEGIN
 v_ErrorNmb := 31605;
 v_ErrorMessageTxt := 'Can delete only a pending agreement';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:31:56. --->
PROCEDURE ELCTRNCADDRINSTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ElctrncAddrNmb NUMBER := 0,
 p_ValidElctrncAddrTyp VARCHAR2 := NULL,
 p_PrtCntctNmb NUMBER := 0,
 p_ElctrncAddrTxt VARCHAR2 := NULL,
 p_LOC_ID NUMBER := NULL,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_PERSON_ID NUMBER := NULL,
 p_TYPE_CODE VARCHAR2 := NULL,
 p_RANK NUMBER := NULL,
 p_INST_ID NUMBER := NULL,
 p_ElctrncAddrCreatUserId VARCHAR2 := NULL)
 AS
 
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ElctrncAddrDataSourcId from ID 0,11
 */
 BEGIN
 --SAVEPOINT ElctrncAddrIns;
 IF p_Identifier = 0
 THEN
 BEGIN
 INSERT INTO PARTNER.ELCTRNCADDRTBL (LocId,
 ElctrncAddrNmb,
 ValidElctrncAddrTyp,
 PrtCntctNmb,
 ElctrncAddrTxt,
 LOC_ID,
 DEPARTMENT,
 PERSON_ID,
 TYPE_CODE,
 RANK,
 INST_ID,
 ElctrncAddrCreatUserId,
 lastupdtuserid,
 lastupdtdt)
 VALUES (p_LocId,
 p_ElctrncAddrNmb,
 p_ValidElctrncAddrTyp,
 p_PrtCntctNmb,
 p_ElctrncAddrTxt,
 p_LOC_ID,
 p_DEPARTMENT,
 p_PERSON_ID,
 p_TYPE_CODE,
 p_RANK,
 p_INST_ID,
 p_ElctrncAddrCreatUserId,
 p_ElctrncAddrCreatUserId,
 SYSDATE);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrInsUpdTrigCSP (p_LocId, p_ElctrncAddrCreatUserId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 11
 THEN
 BEGIN
 INSERT INTO PARTNER.ElctrncAddrTbl (LocId,
 ElctrncAddrNmb,
 ValidElctrncAddrTyp,
 PrtCntctNmb,
 ElctrncAddrTxt,
 LOC_ID,
 DEPARTMENT,
 PERSON_ID,
 TYPE_CODE,
 RANK,
 INST_ID,
 ElctrncAddrCreatUserId,
 lastupdtuserid,
 lastupdtdt)
 VALUES (p_LocId,
 NVL ( (SELECT MAX (ElctrncAddrNmb)
 FROM ElctrncAddrTbl
 WHERE LocId = p_LocId),
 0)
 + 1,
 p_ValidElctrncAddrTyp,
 p_PrtCntctNmb,
 p_ElctrncAddrTxt,
 p_LOC_ID,
 p_DEPARTMENT,
 p_PERSON_ID,
 p_TYPE_CODE,
 p_RANK,
 p_INST_ID,
 p_ElctrncAddrCreatUserId,
 p_ElctrncAddrCreatUserId,
 SYSDATE);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrInsUpdTrigCSP (p_LocId, p_ElctrncAddrCreatUserId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK;
 END ELCTRNCADDRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


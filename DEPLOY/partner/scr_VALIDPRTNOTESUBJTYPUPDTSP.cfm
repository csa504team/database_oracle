<!--- Saved 05/05/2015 13:20:05. --->
PROCEDURE VALIDPRTNOTESUBJTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtNoteSubjTypCd NUMBER:=0,
 p_ValidPrtNoteSubjTypDescTxt VARCHAR2:=NULL,
 p_ValidPrtNoteSubjCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 Begin
 SAVEPOINT ValidPrtNoteSubjTypUpd;
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE ValidPrtNoteSubjTypTbl
 SET 
 ValidPrtNoteSubjTypCd = p_ValidPrtNoteSubjTypCd, 
 ValidPrtNoteSubjTypDescTxt = p_ValidPrtNoteSubjTypDescTxt, 
 ValidPrtNoteSubjCreatUserId = p_ValidPrtNoteSubjCreatUserId, 
 ValidPrtNoteSubjTypCreatDt = SYSDATE
 WHERE ValidPrtNoteSubjTypCd=p_ValidPrtNoteSubjTypCd;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTNOTESUBJTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by sherryLiu. 
DATE:				01/23/2018.
DESCRIPTION:		Standardized call to PRTLOCINSTSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	01/23/2018, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 43)
	and	(Left(CGI.Script_Name,    43) is "/cfincludes/oracle/partner/spc_PRTLOCINSTSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"				default="">
<cfparam name="Variables.ErrMsg"					default="">
<cfparam name="Variables.SkippedSpcs"				default="">
<cfparam name="Variables.TxnErr"					default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs					= ListAppend(Variables.SkippedSpcs, "PRTLOCINSTSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"				default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"				default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"				default="call PRTLOCINSTSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName					= "PRTLOCINSTSP">
	<cfparam name="Variables.PrtLocCreatUserId"		default="">
	<cfparam name="Variables.PrtLocFIRSNmb"			default="">
	<cfparam name="Variables.PrtLocFIRSRecrdTyp"	default="">
	<cfparam name="Variables.PrtId"					default="">
	<cfparam name="Variables.PrtLocInactDt"			default="">
	<cfparam name="Variables.PrtLocNm"				default="">
	<cfparam name="Variables.PrtLocOpndDt"			default="">
	<cfparam name="Variables.PrtLocCurStatCd"		default="">
	<cfparam name="Variables.ValidLocTyp"			default="">
	<cfparam name="Variables.PrtLocOperHrsTxt"		default="">
	<cfparam name="Variables.assigned_id"			default="">
	<cfparam name="Variables.RetVal"				default="">
	<cftry>
		<cfstoredproc procedure="PARTNER.PRTLOCINSTSP" datasource="#Variables.db#"
											username="#Variables.username#" password="#Variables.password#">
		<cfif Len(Variables.PrtLocCreatUserId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocCreatUserId"
											value="#Variables.PrtLocCreatUserId#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocCreatUserId"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtLocFIRSNmb) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocFIRSNmb"
											value="#Variables.PrtLocFIRSNmb#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocFIRSNmb"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.PrtLocFIRSRecrdTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocFIRSRecrdTyp"
											value="#Variables.PrtLocFIRSRecrdTyp#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocFIRSRecrdTyp"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.PrtId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtId"
											value="#Variables.PrtId#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtId"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.PrtLocInactDt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocInactDt"
											value="#Variables.PrtLocInactDt#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocInactDt"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.PrtLocNm) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocNm"
											value="#Variables.PrtLocNm#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocNm"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtLocOpndDt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocOpndDt"
											value="#Variables.PrtLocOpndDt#"
											cfsqltype="CF_SQL_TIMESTAMP">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocOpndDt"
											null="Yes"
											cfsqltype="CF_SQL_TIMESTAMP">
		</cfif>
		<cfif Len(Variables.PrtLocCurStatCd) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocCurStatCd"
											value="#Variables.PrtLocCurStatCd#"
											cfsqltype="CF_SQL_CHAR">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocCurStatCd"
											null="Yes"
											cfsqltype="CF_SQL_CHAR">
		</cfif>
		<cfif Len(Variables.ValidLocTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ValidLocTyp"
											value="#Variables.ValidLocTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ValidLocTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtLocOperHrsTxt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocOperHrsTxt"
											value="#Variables.PrtLocOperHrsTxt#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtLocOperHrsTxt"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfprocparam		type="Out"		dbvarname=":p_assigned_id"
											variable="Variables.assigned_id"
											cfsqltype="CF_SQL_NUMBER">
		<cfprocparam		type="Out"		dbvarname=":p_RetVal"
											variable="Variables.RetVal"
											cfsqltype="CF_SQL_NUMBER">
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


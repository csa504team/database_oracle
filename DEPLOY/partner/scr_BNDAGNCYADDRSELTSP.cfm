<!--- Saved 11/24/2015 13:48:07. --->
PROCEDURE BNDAGNCYADDRSELTSP
 (
 p_Identifier IN NUMBER := 0 ,
 p_RetVal OUT NUMBER ,
 p_LocId IN NUMBER := NULL ,
 p_SelCursor1 OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 OPEN p_SelCursor1 FOR
 SELECT DISTINCT a.PrtLocNm,
 b.PhyAddrStr1Txt,
 b.PhyAddrStr2Txt,
 b.PhyAddrCtyNm,
 b.PhyAddrStCd,
 b.PhyAddrPostCd,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Phone'
 AND c.LocId = p_LocId ) PrtPhone,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Fax'
 AND c.LocId = p_LocId ) PrtFax,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Email'
 AND c.LocId = p_LocId ) PrtEmail,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Web'
 AND c.LocId = p_LocId ) PrtWeb
 FROM PrtLocTbl a INNER JOIN PhyAddrTbl b 
 ON(a.LocId = b.LocId)
 LEFT OUTER JOIN ElctrncAddrTbl c
 ON ( b.LocId = c.LocId)
 WHERE a.LocId = p_LocId;
 
 
 --and a.PrtLocCurStatCd = 'O'
 p_RetVal := SQL%ROWCOUNT ;
 END;
 END IF; 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


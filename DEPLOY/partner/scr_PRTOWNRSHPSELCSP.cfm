<!--- Saved 01/25/2018 10:38:00. --->
PROCEDURE PRTOWNRSHPSELCSP (
 p_userName IN VARCHAR2 := NULL,
 p_PrtId IN NUMBER := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd,PrtDataSourcId
 */
 BEGIN
 SAVEPOINT PRTOWNRSHPSEL;
 
 OPEN p_SelCur FOR
 SELECT po.OwningPrtId,
 po.ValidOwnrPrtTyp,
 po.PrtOwnrshpPct,
 po.ValidOwndPrtTyp,
 po.PrtOwnrshpRank,
 p.OfcCd,
 p.PrtChrtrTyp,
 p.PrtEstbDt,
 p.PrtFisclYrEndDayNmb,
 p.PrtFisclYrEndMoNmb,
 p.PrtInactDt,
 p.PrtLglNm,
 p.ValidPrtLglTyp,
 p.PrtCurStatCd,
 p.ValidPrtSubCatTyp
 FROM PrtTbl p, PrtOwnrshpTbl po
 WHERE p.PrtId = po.OwningPrtId AND po.PrtId = p_PrtId;
 
 p_RetVal := SQL%ROWCOUNT;
 EXCEPTION
 WHEN OTHERS
 THEN
 BEGIN
 RAISE;
 ROLLBACK TO PrtNoteUpd;
 END;
 END PRTOWNRSHPSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


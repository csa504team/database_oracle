<!--- Saved 05/05/2015 13:16:19. --->
PROCEDURE LocExtrctStatDel1CSP
 (
 p_Count OUT NUMBER , --# of Queue entries removed.
 p_Status OUT NUMBER
 )
 
 AS
 v_sys_error NUMBER := 0;
 -- Removes queue entries (locations) from the
 -- LocationExtractStatus table if the queue entry
 -- has already been extracted.
 v_ErrorNmb NUMBER(5);
 
 BEGIN
 
 v_ErrorNmb := 0 ;
 BEGIN
 DELETE LocExtrctStatTbl
 WHERE LocExtrctStatExtrctInd = 'Y';
 p_Count := SQL%ROWCOUNT ;
 p_Status :=0;
 EXCEPTION
 WHEN OTHERS THEN
 v_ErrorNmb := 99999 ;
 p_Status:= -1;
 END;
 
 End;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


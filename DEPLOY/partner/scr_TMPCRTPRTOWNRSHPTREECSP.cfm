<!--- Saved 05/05/2015 13:19:32. --->
PROCEDURE TMPCRTPRTOWNRSHPTREECSP
 (
 p_CreatUserId VARCHAR2
 )
 AS
 v_NodeId number(7);
 v_RootPrtId number(7);
 v_ErrorNmb number(11);
 v_cnt number(5);
 /*drop table parnter.PrtOwnrshpTreeTbl
 /
 CREATE TABLE parnter.PrtOwnrshpTreeTbl
 (
 NodeId number(7,0) IDENTITY,
 Root number(7,0) NULL,
 LevelNmb number(2,0) NOT NULL,
 PrtLglNm varchar(200) NOT NULL,
 PrtId number(7,0) NOT NULL,
 OwningPrtId number(7,0) NULL,
 PrtOwnrshpPct number(5,2) NULL,
 PrtOwnrshpTreeTblCreatUserId char(15) NOT NULL default USER,
 PrtOwnrshpTreeTblCreatDt date NOT NULL default SYSDATE
 )
 tablespace partnerdatatbs
 /
 GRANT SELECT ON partner.PrtOwnrshpTreeTbl TO PartnerReadAllRole
 /
 grant select on PrtOwnrshpTreeTbl to poolpimsupdate
 /
 ALTER TABLE PARTNER.PrtOwnrshpTreeTbl
 ADD CONSTRAINT XPKPrtOwnrshpTreeTbl
 PRIMARY KEY (NodeId)
 USING INDEX TABLESPACE PARTNERDATTBS
 /
 CREATE INDEX PARTNER.NodePartnerId
 ON PARTNER.PrtOwnrshpTreeTbl(PrtId)
 TABLESPACE PARTNERINDTBS
 /
 CREATE INDEX PARTNER.NodeOwningPartnerId
 ON PARTNER.PrtOwnrshpTreeTbl(OwningPrtId)
 TABLESPACE PARTNERINDTBS
 /
 */
 BEGIN
 execute immediate 'truncate table parnter.PrtOwnrshpTreeTbl';
 
 --select root partner from the ownership table
 INSERT INTO partner.PrtOwnrshpTreeTbl
 (
 NodeId,
 Root,
 LevelNmb,
 PrtLglNm,
 PrtId,
 OwningPrtId,
 PrtOwnrshpPct,
 PrtOwnrshpTreeTblCreatUserId,
 PrtOwnrshpTreeTblCreatDt)
 SELECT
 rownum,
 PrtId,
 0,
 PrtLglNm,
 PrtId,
 NULL,
 NULL,
 p_CreatUserId,
 PrtCreatDt
 FROM partner.PrtTbl p
 WHERE p.PrtId in (SELECT DISTINCT po.OwningPrtId FROM partner.PrtOwnrshpTbl po, partner.PrtTbl zp
 WHERE po.OwningPrtId = zp.PrtId AND po.OwningPrtId NOT IN (SELECT PrtId FROM partner.PrtOwnrshpTbl z
 where z.PrtId = po.OwningPrtId))
 order by p.PrtLglNm;
 v_NodeId := -1;
 
 select count(*) into v_cnt from partner.PrtOwnrshpTreeTbl where LevelNmb = 0 and NodeId > v_NodeId;
 while v_cnt > 0
 loop
 begin
 select min(NodeId) into v_NodeId from partner.PrtOwnrshpTreeTbl where LevelNmb = 0 and NodeId > v_NodeId;
 select PrtId into v_RootPrtId from partner.PrtOwnrshpTreeTbl where LevelNmb = 0 and NodeId = v_NodeId;
 
 partner.PutOwnedPartnersIntoTreeCSP (v_RootPrtId, v_RootPrtId, 0);
 
 IF SQLCODE <> 0
 THEN
 BEGIN
 v_ErrorNmb := 99999;
 partner.PrtReplicationErrInsCSP (0, 0, 'PutRootPartnerIntoTreeCSP', v_ErrorNmb, 'Error calling PutOwnedPartnersIntoTreeCSP.', NULL);
 END; --Error.
 END IF;
 v_cnt := v_cnt - 1;
 end;
 end loop;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


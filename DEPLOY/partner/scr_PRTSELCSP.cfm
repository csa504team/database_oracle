<!--- Saved 09/14/2017 10:54:01. --->
PROCEDURE PRTSELCSP
 (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtSelCSP;
 
 IF p_Identifier = 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 select a.PrtId,
 b.PrtAliasNm,
 a.PrtLglNm
 from PrtTbl a, PrtAliasTbl b, PrtLocTbl c
 where a.PrtId = b.PrtId
 and a.PrtId = c.PrtId
 and c.LocId = NVL(p_locid, c.LocId)
 and a.VALIDPRTSUBCATTYP = 'CDC'
 order by upper(a.PrtLglNm);
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO PrtSelCSP;
 RAISE;
 END ;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/25/2018 10:37:54. --->
PROCEDURE PRTALIASINSCSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_PrtAliasCreatUserId CHAR := NULL,
 p_ErrorMsg OUT VARCHAR2)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 v_temp NUMBER (5, 0) := 0;
 BEGIN
 SAVEPOINT PRTALIASINSCSP;
 
 IF p_Identifier = 0
 THEN
 BEGIN
 BEGIN
 SELECT COUNT (*)
 INTO v_temp
 FROM DUAL
 WHERE EXISTS
 (SELECT PrtId
 FROM PrtAliasTbl
 WHERE ValidPrtAliasTyp = p_ValidPrtAliasTyp
 AND TRIM (PrtAliasNm) = p_PrtAliasNm);
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 v_temp := 0;
 END;
 
 IF (v_temp = 0)
 THEN
 BEGIN
 INSERT INTO PrtAliasTbl (PrtId,
 ValidPrtAliasTyp,
 PrtAliasNm,
 PrtAliasCreatUserId,
 LASTUPDTUSERID,
 LASTUPDTDT)
 VALUES (p_PrtId,
 p_ValidPrtAliasTyp,
 p_PrtAliasNm,
 NVL (p_PrtAliasCreatUserId, USER),
 NVL (p_PrtAliasCreatUserId, USER),
 SYSDATE);
 
 --call trigger
 
 IF SQLCODE != 0
 THEN
 BEGIN
 PrtAliasInsUpdTrigCSP (p_PrtId, p_ValidPrtAliasTyp);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 p_ErrorMsg := NULL;
 END;
 ELSE
 BEGIN
 p_ErrorMsg :=
 'The TaxID provided as alias is already associated with another Partner.';
 --raiserror 99999 'Partner already exists with this TaxID and Alias Name'
 END;
 END IF;
 END;
 END IF;
 
 p_RetVal := NVL (p_RetVal, 0);
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PRTALIASINSCSP;
 RAISE;
 END PRTALIASINSCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


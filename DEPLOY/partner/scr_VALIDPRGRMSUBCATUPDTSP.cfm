<!--- Saved 05/05/2015 13:19:54. --->
PROCEDURE VALIDPRGRMSUBCATUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_PrgrmId VARCHAR2 :=NULL,
 p_ValidPrtSubCatTyp VARCHAR2 :=NULL,
 p_ValidPrgrmSubCatDispOrdNmb NUMBER:=0,
 p_ValidPrgrmSubCatCreatUserId VARCHAR2 :=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrgrmSubCatUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 UPDATE ValidPrgrmSubCatTbl
 SET 
 ValidPrgrmSubCatDispOrdNmb =p_ValidPrgrmSubCatDispOrdNmb, 
 ValidPrgrmSubCatCreatUserId = p_ValidPrgrmSubCatCreatUserId,
 ValidPrgrmSubCatCreatDt =SYSDATE
 where PrgrmId =p_PrgrmId 
 and ValidPrtSubCatTyp =p_ValidPrtSubCatTyp;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRGRMSUBCATUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


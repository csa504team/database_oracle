<!--- Saved 05/05/2015 13:18:39. --->
PROCEDURE PRTLOCSBACNTCTUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := NULL,
 p_PrtLocSBACntctSeqNmb NUMBER := NULL,
 p_EmployeeId CHAR := NULL,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_PrtLocSBACntctPrimScndryInd CHAR := NULL,
 p_PrtLocSBACntctCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctUpd;
 IF p_Identifier = 0 THEN
 /* Updert into PrtLocSBACntct Table */
 BEGIN
 UPDATE PARTNER.PrtLocSBACntctTbl
 SET EmployeeId = p_EmployeeId, 
 PrtLocSBACntctTypCd = p_PrtLocSBACntctTypCd, 
 PrtLocSBACntctPrimScndryInd = p_PrtLocSBACntctPrimScndryInd, 
 LASTUPDTUSERID = p_PrtLocSBACntctCreatUserId, 
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId 
 and PrtLocSBACntctSeqNmb = p_PrtLocSBACntctSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctUpd;
 RAISE;
 
 END PRTLOCSBACNTCTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


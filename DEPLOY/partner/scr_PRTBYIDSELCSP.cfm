<!--- Saved 01/25/2018 10:37:56. --->
PROCEDURE PRTBYIDSELCSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_userName VARCHAR2 := NULL,
 p_PrtId NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /* SB -- 09/11/2014 Added PARTNER.VALIDCURSTATCDTBL to grab the code description
 RY--01/24/2018--OPSMDEV-1335::Removed the references of PrtDataSourcId*/
 BEGIN
 SAVEPOINT PrtByIdSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT RPAD(p.PrtId, 16, ' ') PrtId,
 p.OfcCd,
 p.PrtChrtrTyp,
 p.PrtEstbDt,
 p.PrtFisclYrEndDayNmb,
 p.PrtFisclYrEndMoNmb,
 p.PrtInactDt,
 p.PrtLglNm,
 p.ValidPrtLglTyp,
 p.PrtCurStatCd,
 p.ValidPrtSubCatTyp,
 p.ValidPrtPrimCatTyp,
 ps.LocQty,
 ps.SubsidQty,
 ps.RolePrivilegePrgrmId,
 ps.PrtIsOwned,
 CASE WHEN ps.PrtIsOwned='Y' THEN 
 CASE WHEN (SELECT COUNT(*) FROM PrtOwnrshpTbl WHERE PrtId = p_PrtId )=1 
 THEN(SELECT MIN(RPAD(p1.PrtId,7,' '))||' '||MIN(p1.PrtLglNm)||' '||MIN(p1.PrtCurStatCd)
 FROM PrtTbl p1, PrtOwnrshpTbl po 
 WHERE po.PrtId = p_PrtId 
 AND po.OwningPrtId = p1.PrtId) 
 ELSE 'PARTNER OWNED BY MORE THAN ONE ENTITY' 
 END 
 ELSE ' ' 
 END OwnerData,
 ps.LocId,
 vp.ValidPrtPrimCatTypDesc,
 vl.ValidPrtLglTypDesc,
 vs.ValidPrtSubCatDesc,
 o.SBAOfc1Nm,
 o.StCd,
 vc.CURDESCTXT
 FROM PrtTbl p 
 left outer join ValidPrtLglTypTbl vl
 on p.ValidPrtLglTyp = vl.ValidPrtLglTyp
 left outer join sbaref.SBAOfcTbl o
 on p.OfcCd = o.SBAOfcCd,
 PrtSrchTbl ps, ValidPrtPrimCatTbl vp, ValidPrtSubCatTbl vs, VALIDCURSTATCDTBL vc 
 WHERE p.PrtId = p_PrtId 
 AND ps.PrtId = p.PrtId 
 AND upper(p.ValidPrtPrimCatTyp) = upper(vp.ValidPrtPrimCatTyp)
 AND p.ValidPrtSubCatTyp = vs.ValidPrtSubCatTyp
 AND p.PRTCURSTATCD = vc.CURSTATCD; 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtByIdSel;
 RAISE;
 END PRTBYIDSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


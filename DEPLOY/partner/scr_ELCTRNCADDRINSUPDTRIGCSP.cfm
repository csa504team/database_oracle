<!--- Saved 05/05/2015 13:16:07. --->
PROCEDURE ElctrncAddrInsUpdTrigCSP (
 p_LocId number := 0,
 p_CreatUserId varchar2 := NULL
 )
 --
 -- Adds the corresponding location to the extract queue.
 --
 -- Revision History -----------------------------------------
 -- 04/19/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 AS
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 BEGIN
 v_LocationId := p_LocId;
 
 -- Add location to extract queue.
 partner.LocExtrctStatInsCSP (v_LocationId, p_CreatUserId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 30102;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'ElctrncAddrInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


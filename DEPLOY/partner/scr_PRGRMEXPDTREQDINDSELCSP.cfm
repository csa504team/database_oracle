<!--- Saved 05/05/2015 13:16:42. --->
PROCEDURE PRGRMEXPDTREQDINDSELCSP(
 p_userName VARCHAR2 := NULL,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrgrmMonths OUT NUMBER,
 p_indOK OUT CHAR
 )
 AS
 StoO_selcnt NUMBER DEFAULT 0;
 BEGIN
 
 BEGIN
 StoO_selcnt := 0;
 SELECT 1 INTO StoO_selcnt
 FROM DUAL
 WHERE EXISTS ( SELECT * FROM PrgrmTbl 
 WHERE PrgrmId = p_PrgrmId 
 AND PrgrmMaxTrmMoQty IS NOT NULL );
 
 END;
 IF StoO_selcnt != 0 THEN
 BEGIN
 
 FOR rec IN ( SELECT PrgrmMaxTrmMoQty FROM PrgrmTbl WHERE PrgrmId = p_PrgrmId 
 AND PrgrmMaxTrmMoQty IS NOT NULL)
 LOOP
 p_PrgrmMonths := rec.PrgrmMaxTrmMoQty ; 
 
 END LOOP;
 
 p_indOK := 'Y';
 
 END;
 ELSE
 
 p_indOK := 'N';
 
 END IF;
 --END;
 END PRGRMEXPDTREQDINDSELCSP;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


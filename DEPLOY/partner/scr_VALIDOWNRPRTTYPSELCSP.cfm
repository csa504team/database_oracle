<!--- Saved 05/05/2015 13:19:51. --->
PROCEDURE VALIDOWNRPRTTYPSELCSP
 (
 p_userName VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidOwnrPrtTyp, 
 ValidOwnrPrtTypDesc
 FROM ValidOwnrPrtTypTbl 
 WHERE ValidOwnrPrtTypDispOrdNmb != 0 
 ORDER BY ValidOwnrPrtTyp ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 --ROLLBACK TO VALIDOWNRPRTTYPSELCSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


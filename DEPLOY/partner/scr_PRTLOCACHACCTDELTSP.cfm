<!--- Saved 05/05/2015 13:17:49. --->
procedure PRTLOCACHACCTDELTSP
 (p_Identifier number := 0, 
 p_RetVal out number,
 p_LocId number := 0,
 p_ValidACHAcctTyp varchar2 := NULL
 )
 as
 begin
 savepoint PrtLocACHAcctDel;
 if p_Identifier = 0 then
 begin
 DELETE PrtLocACHAcctTbl
 WHERE LocId = p_LocId
 AND ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := sql%rowcount;
 end;
 end if;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocACHAcctDel;
 RAISE;
 end PrtLocACHAcctDelTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


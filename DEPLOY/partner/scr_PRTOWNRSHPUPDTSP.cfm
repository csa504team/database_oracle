<!--- Saved 01/23/2018 15:32:03. --->
PROCEDURE PRTOWNRSHPUPDTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtOwnrshpRank NUMBER := 0,
 p_OwningPrtId NUMBER := 0,
 p_ValidOwnrPrtTyp VARCHAR2 := NULL,
 p_PrtOwnrshpPct NUMBER := 0,
 p_ValidOwndPrtTyp VARCHAR2 := NULL,
 p_PrtOwnrshpCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 -- trigger variable
 NewDataRec PrtOwnrshpTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PrtOwnrshpUpd;
 
 IF p_Identifier = 0
 THEN
 /* Updert into PrtOwnrshpTbl Table */
 BEGIN
 UPDATE PARTNER.PrtOwnrshpTbl
 SET PrtId = p_PrtId,
 PrtOwnrshpRank = p_PrtOwnrshpRank,
 OwningPrtId = p_OwningPrtId,
 ValidOwnrPrtTyp = p_ValidOwnrPrtTyp,
 PrtOwnrshpPct = p_PrtOwnrshpPct,
 ValidOwndPrtTyp = p_ValidOwndPrtTyp,
 lastupdtuserid = p_PrtOwnrshpCreatUserId,
 lastupdtdt = SYSDATE
 WHERE PrtId = p_PrtId AND PrtOwnrshpRank = p_PrtOwnrshpRank;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtOwnrshpTbl
 WHERE PrtId = p_PrtId AND PrtOwnrshpRank = p_PrtOwnrshpRank;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtOwnrshpInsUpdTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 BEGIN
 RAISE;
 ROLLBACK TO PrtOwnrshpUpd;
 END;
 END PRTOWNRSHPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


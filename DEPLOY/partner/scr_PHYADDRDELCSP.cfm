<!--- Saved 05/05/2015 13:16:34. --->
PROCEDURE PHYADDRDELCSP(
 p_userName IN VARCHAR2 := NULL,
 p_LocId IN NUMBER := NULL,
 p_PhyAddrSeqNmb IN NUMBER := NULL,
 p_ValidAddrTyp IN VARCHAR2 := NULL)
 AS
 OldDataRec PhyAddrTbl%ROWTYPE;
 BEGIN
 -- old snapshot
 SELECT * INTO OldDataRec FROM PhyAddrTbl WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb AND ValidAddrTyp = p_ValidAddrTyp;
 DELETE PhyAddrTbl 
 WHERE LocId = p_LocId 
 AND PhyAddrSeqNmb = p_PhyAddrSeqNmb 
 AND ValidAddrTyp = p_ValidAddrTyp;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END PHYADDRDELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


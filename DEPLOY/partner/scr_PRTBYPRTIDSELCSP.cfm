<!--- Saved 05/05/2015 13:17:22. --->
PROCEDURE PRTBYPRTIDSELCSP(
 p_pSwitch number := 0,
 p_PrtId NUMBER := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 IF NVL(p_pSwitch,0) = 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.PrtId ,p.PrtLglNm, p.PrtLglSrchNm, p.ValidPrtPrimCatTyp, l.PrtLocNm, l.ValidLocTyp,
 l.PhyAddrStr1Txt, l.PhyAddrCtyNm, l.PhyAddrStCd , l.PhyAddrPostCd ,l.LocId ,
 l.PrtLocFIRSNmb
 FROM LocSrchTbl l , PrtSrchTbl p
 WHERE l.PrtId = p_PrtId
 AND l.PrtId = p.PrtId
 AND p.PrtCurStatCd = 'O'
 ORDER BY l.LocId;
 END;
 ELSIF p_pSwitch = 1 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.PrtId ,p.PrtLglNm, p.PrtLglSrchNm, p.ValidPrtPrimCatTyp, l.PrtLocNm, l.ValidLocTyp,
 l.PhyAddrStr1Txt, l.PhyAddrCtyNm, l.PhyAddrStCd , l.PhyAddrPostCd ,l.LocId ,
 l.PrtLocFIRSNmb
 FROM LocSrchTbl l , PrtSrchTbl p
 WHERE l.PrtId = p_PrtId
 AND l.PrtId = p.PrtId
 AND (p.PrtCurStatCd = 'O' OR p.PrtCurStatCd = 'I' OR p.PrtCurStatCd = 'C')
 ORDER BY l.LocId;
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


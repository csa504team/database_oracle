<!--- Saved 05/05/2015 13:19:16. --->
PROCEDURE PRTSRCHUPDPRTCSP(
 p_PrtId IN NUMBER DEFAULT NULL)
 AS
 v_PrtLglNm VARCHAR2(200);
 v_PrtLglSrchNm VARCHAR2(200);
 v_PrimaryTyp VARCHAR2(5);
 v_SubcategoryTyp VARCHAR2(5);
 v_RolePrivilege VARCHAR2(20);
 v_PrtCurStatCd CHAR(1);
 v_PrtCreatUserId VARCHAR2(32);
 v_PrtCreatDt DATE;
 v_INST_ID NUMBER(8,0);
 v_ErrorNmb NUMBER(5,0);
 v_ReplicationErrorMessageTxt VARCHAR2(255);
 BEGIN
 
 v_INST_ID := 0;
 if p_PrtId is null then
 begin
 Return;
 end;
 end if;
 FOR rec IN ( SELECT p.PrtLglNm, p.ValidPrtPrimCatTyp, p.ValidPrtSubCatTyp, p.PrtCurStatCd,
 p.PrtCreatUserId, p.PrtCreatDt FROM PrtTbl p WHERE p.PrtId = p_PrtId)
 LOOP
 v_PrtLglNm := rec.PrtLglNm ; 
 v_PrimaryTyp := rec.ValidPrtPrimCatTyp ; 
 v_SubcategoryTyp := rec.ValidPrtSubCatTyp ; 
 v_PrtCurStatCd := rec.PrtCurStatCd ; 
 v_PrtCreatUserId := rec.PrtCreatUserId ; 
 v_PrtCreatDt := rec.PrtCreatDt ; 
 END LOOP;
 FOR rec IN ( SELECT RolePrivilegePrgrmId FROM ValidPrtSubCatTbl 
 WHERE ValidPrtSubCatTyp = v_SubcategoryTyp)
 LOOP
 v_RolePrivilege := rec.RolePrivilegePrgrmId ; 
 END LOOP;
 IF SUBSTR(v_PrtLglNm, 1, 4) = 'The ' THEN
 BEGIN
 v_PrtLglSrchNm := LTRIM(UPPER(SUBSTR(v_PrtLglNm, 4, LENGTH(v_PrtLglNm) 
 - 3) || ' ,' || SUBSTR(v_PrtLglNm, 1, 4)));
 END;
 ELSE
 BEGIN
 v_PrtLglSrchNm := LTRIM(UPPER(v_PrtLglNm));
 END;
 END IF;
 UPDATE PARTNER.PrtSrchTbl
 SET PrtLglNm = v_PrtLglNm, 
 PrtLglSrchNm = v_PrtLglSrchNm, 
 ValidPrtPrimCatTyp = v_PrimaryTyp, 
 ValidPrtSubCatTyp = v_SubcategoryTyp, 
 RolePrivilegePrgrmId = v_RolePrivilege, 
 PrtCurStatCd = v_PrtCurStatCd, 
 lastupdtuserid = v_PrtCreatUserId, 
 LastUpdtDt = v_PrtCreatDt
 WHERE PrtId = p_PrtId;
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,'PrtSrchUpdPrtCSP',v_ErrorNmb,v_ReplicationErrorMessageTxt
 ,null);
 v_ErrorNmb := 50101;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl.' 
 || ' PrtId=' || RPAD(p_PrtId, 7, ' ');
 PARTNER.PrtReplicationErrInsCSP(v_INST_ID,null,'PrtSrchUpdPrtCSP',v_ErrorNmb,v_ReplicationErrorMessageTxt
 ,null);
 
 END;
 END IF;
 
 END PRTSRCHUPDPRTCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


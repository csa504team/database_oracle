<!--- Saved 05/05/2015 13:16:51. --->
procedure PRTAGRMTDELCSP( 
 p_PrtId number := 0,
 p_PrgrmId varchar2 := null,
 p_RetVal out number
 )
 AS
 begin
 savepoint PrtAgrmtDelCSP;
 DELETE PrtAgrmtTbl
 WHERE PrtId = p_PrtId
 AND PrgrmId = p_PrgrmId;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtDelCSP;
 RAISE;
 END PrtAgrmtDelCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


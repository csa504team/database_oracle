<!--- Saved 12/23/2015 12:57:56. --->
PROCEDURE VALIDATEPRTPHYADDRCSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_locid NUMBER:= NULL,
 p_PhyAddrCtyNm VARCHAR2 := NULL,
 p_PhyAddrCntCd VARCHAR2 := NULL,
 p_PhyAddrPostCd VARCHAR2 := NULL,
 p_PhyAddrStCd VARCHAR2 := NULL,
 p_PhyAddrStr1Txt VARCHAR2 := NULL,
 p_PhyAddrStr2Txt VARCHAR2 := NULL,
 p_CREATUSERID VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 v_count number:=0;
 p_prtid number;
 BEGIN
 
 select prtid into p_prtid from partner.prtloctbl where locid=p_locid;
 
 DELETE PARTNER.PRTVALIDATIONERRTBL WHERE PRTID = p_PRTID;
 
 SELECT count(*) into v_count
 FROM PARTNER.PHYADDRTBL
 WHERE PhyAddrCtyNm = p_PhyAddrCtyNm
 and PhyAddrCntCd = p_PhyAddrCntCd
 and substr(PhyAddrPostCd,1,5) = substr(p_PhyAddrPostCd,1,5)
 and PhyAddrStCd = p_PhyAddrStCd
 and PhyAddrStr1Txt = p_PhyAddrStr1Txt
 and PhyAddrStr2Txt = p_PhyAddrStr2Txt;
 
 if v_count > 0 then
 PRTVALIDATIONERRINSCSP(p_prtid,114,p_CreatUserId);
 end if ; 
 END VALIDATEPRTPHYADDRCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


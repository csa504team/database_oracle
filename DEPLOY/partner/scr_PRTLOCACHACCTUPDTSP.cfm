<!--- Saved 05/05/2015 13:17:52. --->
procedure PRTLOCACHACCTUPDTSP
 (p_Identifier number := 0, 
 p_RetVal out number,
 p_LocId number := 0,
 p_ValidACHAcctTyp varchar2 := NULL,
 p_ACHRoutingNmb varchar2 := NULL,
 p_ACHAcctNmb varchar2 := NULL,
 p_ACHAcctDesc varchar := NULL,
 p_PrtLocACHAcctCreatUserId VARCHAR2 := NULL
 )
 as
 begin
 savepoint PrtLocACHAcctUpd;
 if p_Identifier = 0 then
 /* Update PrtLocACHAcctTbl Table */
 begin
 UPDATE PrtLocACHAcctTbl
 SET
 ACHRoutingNmb = p_ACHRoutingNmb,
 ACHAcctNmb = p_ACHAcctNmb,
 ACHAcctDesc = p_ACHAcctDesc,
 LASTUPDTUSERID = p_PrtLocACHAcctCreatUserId,
 LASTUPDTDT = sysdate
 WHERE LocId = p_LocId
 AND ValidACHAcctTyp = p_ValidACHAcctTyp;
 p_RetVal := sql%rowcount;
 end;
 end if; 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocACHAcctUpd;
 RAISE;
 end PrtLocACHAcctUpdTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


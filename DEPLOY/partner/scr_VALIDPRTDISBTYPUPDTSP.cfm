<!--- Saved 05/05/2015 13:19:58. --->
PROCEDURE VALIDPRTDISBTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtDisbTyp VARCHAR2:=NULL,
 p_ValidPrtDisbTypDesc VARCHAR2:=NULL,
 p_ValidPrtDisbTypDispOrdNmb NUMBER:=0,
 p_ValidPrtDisbTypCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtDisbTypUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 UPDATE ValidPrtDisbTypTbl
 SET 
 ValidPrtDisbTypDesc = p_ValidPrtDisbTypDesc, 
 ValidPrtDisbTypDispOrdNmb = p_ValidPrtDisbTypDispOrdNmb, 
 ValidPrtDisbTypCreatUserId = p_ValidPrtDisbTypCreatUserId, 
 ValidPrtDisbTypCreatDt = SYSDATE 
 WHERE ValidPrtDisbTyp = p_ValidPrtDisbTyp;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTDISBTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


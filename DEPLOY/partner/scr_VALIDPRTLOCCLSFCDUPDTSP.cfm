<!--- Saved 05/05/2015 13:20:03. --->
PROCEDURE VALIDPRTLOCCLSFCDUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtLocClsfCd NUMBER:=0,
 p_ValidPrtLocClsfDescTxt VARCHAR2:=NULL,
 p_ValidPrtLocClsfCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtLocClsfCdUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.ValidPrtLocClsfCdTbl
 SET 
 ValidPrtLocClsfCd = p_ValidPrtLocClsfCd, 
 ValidPrtLocClsfDescTxt = p_ValidPrtLocClsfDescTxt,
 ValidPrtLocClsfCreatUserId = p_ValidPrtLocClsfCreatUserId, 
 ValidPrtLocClsfCreatDt = SYSDATE
 WHERE ValidPrtLocClsfCd = p_ValidPrtLocClsfCd;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 end if ;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTLOCCLSFCDUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


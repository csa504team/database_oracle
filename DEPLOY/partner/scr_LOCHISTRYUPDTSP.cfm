<!--- Saved 01/23/2018 15:31:58. --->
PROCEDURE LOCHISTRYUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocHistryAffctPrtId NUMBER := 0,
 p_LocId NUMBER := 0,
 p_LocHistryEffDt DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_LocHistryPartyTyp CHAR := NULL,
 p_LocHistryInitPrtId NUMBER := 0,
 p_LocHistryResultingPrtId NUMBER := 0,
 p_LocHistryChngDesc VARCHAR2 := NULL,
 p_LocHistryLocNm VARCHAR2 := NULL,
 p_LocHistryLocTyp VARCHAR2 := NULL,
 p_LocHistryCtyNm VARCHAR2 := NULL,
 p_LocHistryCntCd VARCHAR2 := NULL,
 p_LocHistryFIPSCntyCd CHAR := NULL,
 p_LocHistryPostCd VARCHAR2 := NULL,
 p_LocHistryStCd VARCHAR2 := NULL,
 p_LocHistryStr1Txt VARCHAR2 := NULL,
 p_LocHistryStr2Txt VARCHAR2 := NULL,
 p_LocHistryUSDpndyInd CHAR := NULL,
 p_LocHistryTFPHistryDt DATE := NULL,
 p_AFFECTED_INST_ID NUMBER := 0,
 p_LOC_ID NUMBER := 0,
 p_EFFECTIVE_DATE DATE := NULL,
 p_PARTY_TYPE_INDICATOR CHAR := NULL,
 p_CHANGE_CODE VARCHAR2 := NULL,
 p_INITIATOR_INST_ID NUMBER := 0,
 p_STATUS CHAR := NULL,
 p_LocHistryCreatUserId VARCHAR2 := NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT LocHistryUpd;
 IF p_Identifier = 0 
 THEN
 BEGIN
 UPDATE PARTNER.LocHistryTbl
 SET LocHistryResultingPrtId =p_LocHistryResultingPrtId,
 LocHistryChngDesc =p_LocHistryChngDesc,
 LocHistryLocNm =p_LocHistryLocNm,
 LocHistryLocTyp =p_LocHistryLocTyp,
 LocHistryCtyNm =p_LocHistryCtyNm,
 LocHistryCntCd =p_LocHistryCntCd,
 LocHistryFIPSCntyCd =p_LocHistryFIPSCntyCd,
 LocHistryPostCd =p_LocHistryPostCd,
 LocHistryStCd =p_LocHistryStCd,
 LocHistryStr1Txt =p_LocHistryStr1Txt,
 LocHistryStr2Txt =p_LocHistryStr2Txt,
 LocHistryUSDpndyInd =p_LocHistryUSDpndyInd,
 LocHistryTFPHistryDt =p_LocHistryTFPHistryDt,
 AFFECTED_INST_ID =p_AFFECTED_INST_ID,
 LOC_ID =p_LOC_ID,
 EFFECTIVE_DATE =p_EFFECTIVE_DATE,
 PARTY_TYPE_INDICATOR =p_PARTY_TYPE_INDICATOR,
 CHANGE_CODE =p_CHANGE_CODE,
 INITIATOR_INST_ID =p_INITIATOR_INST_ID,
 STATUS =p_STATUS,
 lastupdtuserid =p_LocHistryCreatUserId,
 lastupdtdt =SYSDATE
 WHERE LocHistryAffctPrtId =p_LocHistryAffctPrtId
 AND LocId =p_LocId
 AND LocHistryEffDt =p_LocHistryEffDt
 AND ValidChngCd =p_ValidChngCd
 AND LocHistryPartyTyp =p_LocHistryPartyTyp
 AND LocHistryInitPrtId =p_LocHistryInitPrtId;
 p_RetVal :=SQL%ROWCOUNT;
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO LocHistryUpd;
 
 END;
 END IF;
 
 END LOCHISTRYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


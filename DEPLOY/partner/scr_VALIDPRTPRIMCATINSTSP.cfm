<!--- Saved 05/05/2015 13:20:05. --->
PROCEDURE VALIDPRTPRIMCATINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtPrimCatTyp VARCHAR2 :=NULL,
 p_ValidPrtPrimCatDispOrdNmb NUMBER:=0,
 p_ValidPrtPrimCatTypDesc VARCHAR2 :=NULL,
 p_ValidPrtPrimCatCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtPrimCatIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtPrimCatTbl 
 (
 ValidPrtPrimCatTyp,
 ValidPrtPrimCatDispOrdNmb, 
 ValidPrtPrimCatTypDesc, 
 ValidPrtPrimCatCreatUserId
 )
 VALUES (
 p_ValidPrtPrimCatTyp, 
 p_ValidPrtPrimCatDispOrdNmb, 
 p_ValidPrtPrimCatTypDesc, 
 p_ValidPrtPrimCatCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTPRIMCATINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


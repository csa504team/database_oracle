<!--- Saved 05/04/2016 15:54:08. --->
PROCEDURE GETPRTHISTRYCPCSP
 (
 p_PrtId Number := NULL,
 p_StrtDt Date := NULL,
 p_EndDt Date := NULL,
 p_ChngCd VARCHAR2 :=Null,
 p_SrchTyp VARCHAR2 :=Null,
 p_ChngOrdNmb Number :=Null,
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_ValidChngCd VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS 
 
 
 BEGIN
 
 IF lower(p_SrchTyp) = 'bydate' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT p.ValidChngCd,
 v.ValidChngCdDispOrdNmb, 
 v.ValidChngCdDesc,
 count( distinct p.PrtId) as PrtNum 
 FROM partner.PrtHistryTbl p , partner.ValidChngCdTbl v
 WHERE trunc(p.PrtHistryCreatDt) like trunc(sysdate) --p.PrtHistryCreatDt like substr(sysdate,1,11)||'%' 
 AND v.ValidChngCd = p.ValidChngCd
 GROUP BY p.ValidChngCd ,v.ValidChngCdDesc,v.ValidChngCdDispOrdNmb
 ORDER BY p.ValidChngCd;
 
 END; 
 END IF;
 IF lower(P_SrchTyp) = 'bychngcd' THEN
 IF p_chngOrdNmb != 0 THEN
 IF P_ChngOrdNmb in (1,2,3,4) THEN
 
 partner.PrtHistAIRRecrdCSP(p_ReportBegDateTime,p_ReportEndDateTime,p_ValidChngCd,p_PrtId,p_SelCur);
 
 END IF;
 END IF;
 IF p_ChngOrdNmb = 0 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtHistryLglNm as AffectedTitle, 
 p.PrtHistryEffDt as EffectiveDate,--To_char(p.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 p.PrtHistryCreatDt as DateAddedToPIMS ,
 p.ValidChngCd 
 FROM partner.PrtHistryTbl p 
 WHERE TRUNC(p.PrtHistryCreatDt) like TRUNC(sysdate);--p.PrtHistryCreatDt like (substr(sysdate,1,11)||'%')
 END;
 END IF;
 END IF;
 IF lower(p_srchTyp) ='byprtid' THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT DISTINCT P.ValidChngCd , 
 v.ValidChngCdDesc,
 v.ValidChngCdDesc,
 p.PrtHistryLglNm,
 p.PrtHistryLglNm as AffectedTitle,
 p.PrtHistryPartyTyp, 
 p.PrtHistryLglNm as ResultingTitle,
 p.PrtHistryEffDt as EffectiveDate,
 p.PrtHistryLglNm as InitiatorTitle,
 p.PrtHistryChrtrTyp as AffectedCharter,
 p.PrtHistryChrtrTyp as ResultingCharter,
 p.PrtHistryReglAgencyNm as AffectedRegulator ,
 p.PrtHistryReglAgencyNm as InitiatorRegulator,
 p.PrtHistryReglAgencyNm as ResultingRegulator,
 p.PrtHistryEffDt as EffectiveDate,--To_char(p.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 p.PrtId as AffectedPrtId,
 p.PrtHistryInitPrtId,
 p.PrtHistryResultingPrtId 
 FROM partner.PrtHistryTbl p , partner.ValidChngCdTbl v 
 WHERE p.PrtId = p_prtId 
 AND v.ValidChngCd = p.ValidChngCd
 AND p.PrtHistryCreatDt >= p_StrtDt 
 AND p.PrtHistryCreatDt <= p_EndDt 
 ORDER BY p.PrtHistryEffDt;
 
 
 END;
 END IF;
 
 IF lower(p_SrchTyp) = 'forpartnerhistory' THEN
 if p_ChngCd IS NOT NULL THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT i.PrtId as InitiatorPrtId,
 i.PrtHistryLglNm as InitiatorTitle,
 v.ValidChngCdDesc,
 a.PrtId as AffectedPrtId,
 a.PrtHistryLglNm as AffectedTitle,
 r.PrtId as ResultingPrtId,
 r.PrtHistryLglNm as ResultingTitle,
 a.PrtHistryEffDt as EffectiveDate,--To_char(a.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 a.ValidChngCd,
 v.ValidChngCdDispOrdNmb,
 a.PrtHistryCreatDt as DateAddedToPIMS, 
 a.PrtHistryChrtrTyp as AffectedCharter,
 r.PrtHistryChrtrTyp as ResultingCharter,
 a.PrtHistryReglAgencyNm as AffectedRegulator ,
 i.PrtHistryReglAgencyNm as InitiatorRegulator,
 r.PrtHistryReglAgencyNm as ResultingRegulator, 
 a.ValidDataSourcCd 
 FROM partner.PrtHistryTbl a 
 LEFT OUTER JOIN partner.PrtHistryTbl i ON a.PrtId = i.PrtId 
 AND a.PrtHistryEffDt=i.PrtHistryEffDt 
 AND i.PrtHistryPartyTyp = 'I' 
 AND a.ValidChngCd = i.ValidChngCd 
 LEFT OUTER JOIN partner.PrtHistryTbl r ON a.PrtId = r.PrtId 
 AND a.PrtHistryEffDt=r.PrtHistryEffDt 
 AND r.PrtHistryPartyTyp = 'R' 
 AND a.ValidChngCd = r.ValidChngCd
 and a.ValidChngCd= NVL(p_ChngCd,a.ValidChngCd) 
 LEFT OUTER JOIN partner.ValidChngCdTbl v ON a.ValidChngCd =v.ValidChngCd
 WHERE a.PrtHistryPartyTyp = 'A'
 and case when p_ChngOrdNmb = 0 
 AND TRUNC(a.PrtHistryCreatDt) like TRUNC(sysdate)
 THEN 1
 
 when p_StrtDt IS NOT NULL AND p_EndDt IS NOT NULL 
 AND TRUNC(a.PrtHistryEffDt ) >=TRUNC( NVL(p_StrtDt,a.PrtHistryEffDt))
 AND TRUNC(a.PrtHistryEffDt ) <=TRUNC(NVL(p_EndDt,a.PrtHistryEffDt))
 THEN 1
 
 when p_prtId >0 
 and a.prtId = p_prtId
 THEN 1
 else 0
 end = 1
 ORDER BY a.PrtHistryEffDt DESC;
 end;
 end if;
 
 IF p_StrtDt IS NOT NULL AND p_EndDt IS NOT NULL THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT i.PrtId as InitiatorPrtId,
 i.PrtHistryLglNm as InitiatorTitle,
 v.ValidChngCdDesc,
 a.PrtId as AffectedPrtId,
 a.PrtHistryLglNm as AffectedTitle,
 r.PrtId as ResultingPrtId,
 r.PrtHistryLglNm as ResultingTitle,
 a.PrtHistryEffDt as EffectiveDate,--To_char(a.PrtHistryEffDt,'MM/DD/YYYY') as EffectiveDate,
 a.ValidChngCd,
 v.ValidChngCdDispOrdNmb,
 a.PrtHistryCreatDt as DateAddedToPIMS, 
 a.PrtHistryChrtrTyp as AffectedCharter,
 r.PrtHistryChrtrTyp as ResultingCharter,
 a.PrtHistryReglAgencyNm as AffectedRegulator ,
 i.PrtHistryReglAgencyNm as InitiatorRegulator,
 r.PrtHistryReglAgencyNm as ResultingRegulator, 
 a.ValidDataSourcCd 
 FROM partner.PrtHistryTbl a 
 LEFT OUTER JOIN partner.PrtHistryTbl i ON a.PrtId = i.PrtId 
 AND a.PrtHistryEffDt=i.PrtHistryEffDt 
 AND i.PrtHistryPartyTyp = 'I' 
 AND a.ValidChngCd = i.ValidChngCd 
 LEFT OUTER JOIN partner.PrtHistryTbl r ON a.PrtId = r.PrtId 
 AND a.PrtHistryEffDt=r.PrtHistryEffDt 
 AND r.PrtHistryPartyTyp = 'R' 
 AND a.ValidChngCd = r.ValidChngCd
 and a.ValidChngCd= NVL(p_ChngCd,a.ValidChngCd) 
 LEFT OUTER JOIN partner.ValidChngCdTbl v ON a.ValidChngCd =v.ValidChngCd
 WHERE a.PrtHistryPartyTyp = 'A'
 AND TRUNC(a.PrtHistryEffDt ) >=TRUNC( NVL(p_StrtDt,a.PrtHistryEffDt))
 AND TRUNC(a.PrtHistryEffDt ) <=TRUNC(NVL(p_EndDt,a.PrtHistryEffDt))
 ORDER BY a.PrtHistryEffDt DESC;
 end;
 end if;
 
 
 END IF;
 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


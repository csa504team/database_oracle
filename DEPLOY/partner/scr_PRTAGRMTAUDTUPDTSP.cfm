<!--- Saved 05/05/2015 13:16:51. --->
PROCEDURE PRTAGRMTAUDTUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtAudtNmb NUMBER := 0,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtAudtDocLocOfcCd CHAR := NULL,
 p_PrtAgrmtAudtAreaOfOperTxt VARCHAR2 := NULL,
 p_PrtAgrmtAudtDocLocTxt VARCHAR2 := NULL,
 p_PrtAgrmtAudtEffDt DATE := NULL,
 p_PrtAgrmtAudtExprDt DATE := NULL,
 p_PrtAgrmtAudtTermDt DATE := NULL,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtAudtWvrInd CHAR := NULL,
 p_PrtAgrmtAudtCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 
 SAVEPOINT PrtAgrmtAudtUpd;
 
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.PrtAgrmtAudtTbl
 SET PrtAgrmtAudtNmb =p_PrtAgrmtAudtNmb, 
 PrtId =p_PrtId, 
 PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb, 
 PrgrmId =p_PrgrmId, 
 PrtAgrmtAudtDocLocOfcCd =p_PrtAgrmtAudtDocLocOfcCd, 
 PrtAgrmtAudtAreaOfOperTxt =p_PrtAgrmtAudtAreaOfOperTxt, 
 PrtAgrmtAudtDocLocTxt =p_PrtAgrmtAudtDocLocTxt, 
 PrtAgrmtAudtEffDt =p_PrtAgrmtAudtEffDt, 
 PrtAgrmtAudtExprDt =p_PrtAgrmtAudtExprDt, 
 PrtAgrmtAudtTermDt =p_PrtAgrmtAudtTermDt, 
 PrtAgrmtTermRsnCd =p_PrtAgrmtTermRsnCd, 
 PrtAgrmtAudtWvrInd =p_PrtAgrmtAudtWvrInd, 
 lastupdtuserid =p_PrtAgrmtAudtCreatUserId, 
 LASTUPDTDT = SYSDATE
 WHERE PrtAgrmtAudtNmb = p_PrtAgrmtAudtNmb;
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtAudtUpd;
 RAISE;
 
 END PRTAGRMTAUDTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


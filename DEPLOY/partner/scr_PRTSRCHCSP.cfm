<!--- Saved 05/05/2016 11:04:33. --->
procedure prtsrchcsp
 (
 p_lglNmSrchInd char := null,
 p_subcategoryTyp varchar2 := null, 
 p_cityNm varchar2 := null, 
 p_citySrchInd char := null,
 p_stateCd varchar2 := null, 
 p_zipCd varchar2 := null,
 p_zipSrchInd char := null, 
 p_curStatCd varchar2 := null,
 p_lglSrchNm varchar2 := null, 
 p_officeCd varchar2 := null, 
 p_regionCd varchar2 := null, 
 p_cntyCd varchar2 := null, 
 p_congDistCd varchar2 := null, 
 p_programId varchar2 := null, 
 p_prgrmStatCd varchar2 := null,
 p_queryTyp varchar2 := null,
 p_rowCount number := 50, 
 p_stateCity varchar2 := null,
 p_selcur out sys_refcursor,
 p_retval out number
 
 )
 AS
 v_identifier number(2);
 v_lglSrchNm varchar2(48);
 v_cityNm varchar2(80);
 v_zipCd varchar2(10);
 v_lglNmSrchInd char(1);
 BEGIN
 
 
 if p_programId is null then
 if lower(p_queryTyp) = 'next' then
 v_identifier := 12;
 else
 v_identifier := 0;
 end if;
 else 
 v_identifier := 11;
 end if;
 
 v_lglNmSrchInd := p_lglNmSrchInd;
 
 if upper(substr(p_lglSrchNm,1,3)) = 'THE' then
 v_lglSrchNm := upper(substr(p_lglSrchNm,1,4)); 
 v_lglNmSrchInd := 'C'; 
 else
 v_lglSrchNm := p_lglSrchNm; 
 end if; 
 
 if v_lglNmSrchInd = 'C' then
 v_lglSrchNm := '%'|| upper(v_lglSrchNm) ||'%';
 elsif v_lglNmSrchInd = 'S' then
 v_lglSrchNm := upper(v_lglSrchNm) ||'%';
 elsif v_lglNmSrchInd = 'E' then
 v_lglSrchNm := upper(v_lglSrchNm);
 else
 v_lglSrchNm := upper(v_lglSrchNm);
 end if; 
 
 v_cityNm := p_cityNm;
 
 if p_citySrchInd = 'C' then
 v_cityNm := '%'|| upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'S' then
 v_cityNm := upper(v_cityNm) ||'%';
 elsif p_citySrchInd = 'E' then
 v_cityNm := upper(v_cityNm);
 else
 v_cityNm := upper(v_cityNm);
 end if; 
 
 v_zipCd := p_zipCd;
 
 
 if p_zipSrchInd = 'C' then
 v_zipCd := '%'|| v_zipCd ||'%';
 elsif p_zipSrchInd = 'S' then
 v_zipCd := v_zipCd ||'%';
 elsif p_zipSrchInd = 'E' then
 v_zipCd := v_zipCd;
 else
 v_zipCd := v_zipCd;
 end if;
 
 
 
 if v_identifier = 0 then
 begin
 open p_selcur for
 SELECT rownum,
 p.PrtId, 
 p.PrtLglNm, 
 p.PrtCurStatCd,
 p.ValidPrtPrimCatTyp, 
 p.ValidPrtSubCatTyp, 
 p.PrtLocNm, 
 p.ValidLocTyp, 
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm, 
 p.PhyAddrStCd , 
 p.PhyAddrPostCd , 
 p.LocId,
 p.PrtLocFIRSNmb, 
 p.LocQty , p.PrtLglSrchNm ,p.PhyAddrCtySrchNm, 
 CASE WHEN p.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ' 
 ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId) 
 END AS StateCity
 FROM partner.PrtSrchTbl p
 WHERE p.PrtLglSrchNm like nvl(v_lglSrchNm,p.PrtLglSrchNm)
 AND p.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,p.ValidPrtSubCatTyp)
 AND p.PhyAddrCtySrchNm like nvl(v_cityNm,p.PhyAddrCtySrchNm)
 AND p.PhyAddrStCd IN nvl(p_stateCd,p.PhyAddrStCd)
 AND p.PhyAddrPostCd like nvl(v_zipCd,p.PhyAddrPostCd)
 AND p.OrignOfcCd IN nvl(p_officeCd,p.OrignOfcCd)
 AND p.OrignOfcCd like nvl(p_regionCd||'%',p.OrignOfcCd)
 AND CASE WHEN length(p_cntyCd) = 1 AND p_cntyCd in ('0','1','2','3','4','5','6','7','8','9') AND P.CNTYCD = '00'||p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) between 1 and 3 AND P.CNTYCD = p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) > 3 AND P.CNTYNM LIKE p_CntyCd||'%'
 THEN 1
 WHEN p_CntyCd is null and p.CntyCd = p.CntyCd
 THEN 1
 ELSE 0
 END = 1
 AND p.CongrsnlDistNmb = nvl(p_congDistCd,p.CongrsnlDistNmb)
 AND p.PrtCurStatCd = nvl(p_curStatCd,p.PrtCurStatCd)
 AND rownum <= p_rowCount
 ORDER BY p.PrtLglSrchNm,StateCity;
 end;
 elsif v_identifier = 11 then
 begin
 open p_selcur for
 SELECT rownum,
 p.PrtId, 
 p.PrtLglNm, 
 p.PrtCurStatCd,
 p.ValidPrtPrimCatTyp, 
 p.ValidPrtSubCatTyp, 
 p.PrtLocNm, 
 p.ValidLocTyp, 
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm, 
 p.PhyAddrStCd , 
 p.PhyAddrPostCd , 
 p.LocId,
 p.PrtLocFIRSNmb, 
 p.LocQty , p.PrtLglSrchNm ,p.PhyAddrCtySrchNm, 
 CASE WHEN p.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ' 
 ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId) 
 END AS StateCity
 FROM partner.PrtSrchTbl p, partner.PrtAgrmtTbl a
 WHERE p.PrtLglSrchNm like nvl(v_lglSrchNm,p.PrtLglSrchNm)
 AND p.ValidPrtSubCatTyp IN nvl(p_subcategoryTyp,p.ValidPrtSubCatTyp)
 AND p.PhyAddrCtySrchNm like nvl(v_cityNm,p.PhyAddrCtySrchNm)
 AND p.PhyAddrStCd IN nvl(p_stateCd,p.PhyAddrStCd)
 AND p.PhyAddrPostCd like nvl(v_zipCd,p.PhyAddrPostCd)
 AND p.OrignOfcCd IN nvl(p_officeCd,p.OrignOfcCd)
 AND p.OrignOfcCd like nvl(p_regionCd||'%',p.OrignOfcCd)
 AND CASE WHEN length(p_cntyCd) = 1 AND p_cntyCd in ('0','1','2','3','4','5','6','7','8','9') AND P.CNTYCD = '00'||p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) between 1 and 3 AND P.CNTYCD = p_cntyCd
 THEN 1
 WHEN length(p_cntyCd) > 3 AND P.CNTYNM LIKE p_CntyCd||'%'
 THEN 1
 WHEN p_CntyCd is null and p.CntyCd = p.CntyCd
 THEN 1
 ELSE 0
 END = 1
 AND p.CongrsnlDistNmb = nvl(p_congDistCd,p.CongrsnlDistNmb)
 AND p.PrtCurStatCd = nvl(p_curStatCd,p.PrtCurStatCd)
 AND p.PrtId = a.PrtId
 AND CASE WHEN p_prgrmStatCd = 'A' AND a.PrgrmId IN (p_programId)
 AND a.PrtAgrmtEffDt<=sysdate AND ((a.PrtAgrmtExprDt >= sysdate OR a.PrtAgrmtExprDt is NULL)
 AND (a.PrtAgrmtTermDt >sysdate OR a.PrtAgrmtTermDt is NULL)) 
 THEN 1
 WHEN p_prgrmStatCd = 'I' AND a.PrgrmId IN (p_programId)
 AND (a.PrtAgrmtExprDt < sysdate 
 AND a.PrtAgrmtTermDt is NULL 
 AND a.PrtAgrmtSeqNmb=(Select Max(b.PrtAgrmtSeqNmb) from partner.PrtAgrmtTbl b WHERE a.PrtId = b.PrtId AND b.PrgrmId IN (p_programId))
 )
 THEN 1
 WHEN p_prgrmStatCd = 'T' AND a.PrgrmId IN (p_programId) 
 AND a.PrtAgrmtTermDt <=sysdate
 THEN 1
 WHEN p_prgrmStatCd = 'P' AND a.PrgrmId IN (p_programId) 
 AND a.PrtAgrmtEffDt > sysdate
 THEN 1 
 WHEN (p_prgrmStatCd IS NULL OR p_prgrmStatCd NOT IN('A','I','T','P')) AND a.PrgrmId IN(p_programId) THEN 1
 ELSE 0
 END = 1
 AND rownum <= p_rowCount
 ORDER BY p.PrtLglSrchNm,StateCity;
 end;
 elsif v_identifier = 12 then
 begin
 open p_selcur for
 SELECT rownum,
 p.PrtId, 
 p.PrtLglNm, 
 p.PrtCurStatCd,
 p.ValidPrtPrimCatTyp, 
 p.ValidPrtSubCatTyp, 
 p.PrtLocNm, 
 p.ValidLocTyp, 
 p.PhyAddrStr1Txt,
 p.PhyAddrCtyNm, 
 p.PhyAddrStCd , 
 p.PhyAddrPostCd , 
 p.LocId,
 p.PrtLocFIRSNmb, 
 p.LocQty , p.PrtLglSrchNm ,p.PhyAddrCtySrchNm, 
 CASE WHEN p.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ' 
 ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId) 
 END AS StateCity
 FROM partner.PrtSrchTbl p
 WHERE p.PrtLglSrchNm >= upper(p_lglSrchNm)
 AND (CASE WHEN p.PhyAddrCtyNm is Null THEN 'ZZZZZZZZZZ' 
 ELSE p.PhyAddrStCd||p.PhyAddrCtySrchNm||Upper(p.PhyAddrStr1Txt)||to_char(p.PrtId)
 END ) >= upper(p_stateCity)
 ORDER BY p.PrtLglSrchNm,StateCity;
 end;
 end if;
 
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


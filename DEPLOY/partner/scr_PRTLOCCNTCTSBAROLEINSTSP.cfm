<!--- Saved 05/05/2015 13:18:08. --->
PROCEDURE PRTLOCCNTCTSBAROLEINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctSBARoleCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocCntctSBARoleIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtLocCntctSBARole Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocCntctSBARoleTbl(LocId,PrtCntctNmb,PrtCntctSBARoleTypCd,PrtCntctSBARoleCreatUserId,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_LocId,p_PrtCntctNmb,p_PrtCntctSBARoleTypCd,p_PrtCntctSBARoleCreatUserId,p_PrtCntctSBARoleCreatUserId,SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctSBARoleIns;
 RAISE; 
 
 END PRTLOCCNTCTSBAROLEINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


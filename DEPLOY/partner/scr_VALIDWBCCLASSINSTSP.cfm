<!--- Saved 05/05/2015 13:20:14. --->
PROCEDURE VALIDWBCCLASSINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidWBCClassCd NUMBER:=0,
 p_ValidWBCClassDescTxt VARCHAR2 :=NULL,
 p_ValidWBCClassCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidWBCClassIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidWBCClassTbl 
 (
 ValidWBCClassCd, 
 ValidWBCClassDescTxt, 
 ValidWBCClassCreatUserId
 )
 VALUES (
 p_ValidWBCClassCd, 
 p_ValidWBCClassDescTxt, 
 p_ValidWBCClassCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDWBCCLASSINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


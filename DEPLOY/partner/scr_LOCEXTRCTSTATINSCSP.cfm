<!--- Saved 05/05/2015 13:16:21. --->
PROCEDURE LOCEXTRCTSTATINSCSP (
 p_LocId number := NULL, --Location to be added.
 p_CreatUserID VARCHAR2 := null
 )
 AS
 -- Adds a queue entries (location) into the
 -- LocExtrctStatTbl table for the given LocId.
 --
 -- Microlenders will specifically be excluded from the
 -- queue by this procedure.
 -- Return status of 0 indicates successful completion.
 -- Return status of -100 indicates an error condition.
 -- Revision History -----------------------------------------
 -- 04/18/2001 Rajeswari DSP Primary Typ of Microlender has been
 -- changed from 'Lendr' to 'NBLDR'
 -- 04/18/2000 C.Woodard Revised to exclude Microlenders
 -- from queue.
 -- ----------------------------------------------------------
 v_CurrentTime date;
 v_ErrorNmb number(5);
 v_PrimaryTyp varchar2(5);
 v_SubcategoryTyp varchar2(5);
 v_PrtLocFIRSNmb CHAR(7);
 v_cnt number(11);
 BEGIN
 v_CurrentTime := SYSDATE;
 v_ErrorNmb := 0;
 
 -- Error Checking...
 IF p_LocId IS NULL
 THEN
 v_ErrorNmb := 99999;
 ELSE
 BEGIN
 SELECT count(*) INTO v_cnt FROM partner.PrtLocTbl l WHERE l.LocId = p_LocId;
 IF v_cnt = 0
 THEN
 v_ErrorNmb := 99999;
 END IF;
 END;
 END IF;
 
 --Determine location eligibility for FIRS processing.
 --Skip processing of Microlender locations.
 BEGIN
 SELECT p.ValidPrtPrimCatTyp INTO v_PrimaryTyp FROM partner.PrtTbl p, partner.PrtLocTbl pl
 WHERE p.PrtId = pl.PrtId AND pl.LocId = p_LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrimaryTyp := NULL; END;
 BEGIN
 SELECT p.ValidPrtSubCatTyp INTO v_SubcategoryTyp FROM partner.PrtTbl p, partner.PrtLocTbl pl
 WHERE p.PrtId = pl.PrtId AND pl.LocId = p_LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_SubcategoryTyp := NULL; END;
 BEGIN
 SELECT pl.PrtLocFIRSNmb INTO v_PrtLocFIRSNmb FROM partner.PrtTbl p, partner.PrtLocTbl pl
 WHERE p.PrtId = pl.PrtId AND pl.LocId = p_LocId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtLocFIRSNmb := NULL; END;
 
 select count(*) into v_cnt from ValidPrtSubCatTbl where ValidPrtSubCatFIRSInd = 'N' and ValidPrtSubCatTyp = v_SubcategoryTyp;
 IF v_PrtLocFIRSNmb IS NULL OR v_cnt > 0
 THEN
 RETURN;
 END IF;--Microlender exclusion.
 
 -- Update or insert queue entry.
 IF v_ErrorNmb = 0
 THEN
 BEGIN
 SELECT count(*) INTO v_cnt FROM partner.LocExtrctStatTbl s WHERE s.LocId = p_LocId;
 IF v_cnt > 0
 THEN
 BEGIN
 UPDATE partner.LocExtrctStatTbl
 SET LocExtrctStatLastQueueTime = v_CurrentTime,
 LocExtrctStatExtrctInd = 'N', --Reset status.
 LastUpdtUserId = p_CreatUserId,
 LastUpdtDt = SYSDATE
 WHERE LocId = p_LocId;
 
 v_ErrorNmb := SQLCODE;
 END;
 ELSE
 BEGIN
 INSERT INTO LocExtrctStatTbl
 (LocId,
 LocExtrctStatFirstQueueTime,
 LocExtrctStatLastQueueTime,
 LocExtrctStatCreatUserId,
 LocExtrctStatCreatDt,
 LastUpdtUserId,
 LastUpdtDt)
 VALUES
 (p_LocId,
 v_CurrentTime,
 v_CurrentTime,
 p_CreatUserId,
 SYSDATE,
 p_CreatUserId,
 SYSDATE); --ExtractedInd defaults to 'N'
 v_ErrorNmb := SQLCODE;
 END;
 END IF;
 END;
 END IF;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


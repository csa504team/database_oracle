<!--- Saved 05/05/2015 13:19:45. --->
PROCEDURE VALIDELCTRNCADDRTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidElctrncAddrTyp VARCHAR2 :=NULL,
 pValidElctrncAddrTypDispOrdNmb NUMBER:=0,
 p_ValidElctrncAddrTypDesc VARCHAR2 :=NULL,
 p_ValidElctrncCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidElctrncAddrTypIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidElctrncAddrTypTbl 
 (
 ValidElctrncAddrTyp, 
 ValidElctrncAddrTypDispOrdNmb, 
 ValidElctrncAddrTypDesc, 
 ValidElctrncAddrCreatUserId
 )
 VALUES (
 p_ValidElctrncAddrTyp, 
 pValidElctrncAddrTypDispOrdNmb, 
 p_ValidElctrncAddrTypDesc, 
 p_ValidElctrncCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDELCTRNCADDRTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


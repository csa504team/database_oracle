<!--- Saved 01/25/2018 10:37:58. --->
PROCEDURE PRTLOCSEL1CSP(
 p_userName IN VARCHAR2 DEFAULT NULL,
 p_PrtId IN NUMBER DEFAULT NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/24/2018--OPSMDEV-1335:: Removed the references of PhyAddrDataSourcId
 */
 BEGIN
 SAVEPOINT PRTLOCSEL1CSP;
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtId,
 PrtLocNm,
 ValidLocTyp,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp,
 PrtLocOpndDt,
 PrtLocInactDt,
 PrtLocCurStatCd,
 ValidPrtRoleTyp,
 ValidAddrTyp,
 PhyAddrStr1Txt,
 PhyAddrStr2Txt,
 PhyAddrCtyNm,
 PhyAddrStCd,
 PhyAddrPostCd,
 PhyAddrFIPSCntyCd,
 PhyAddrCntCd,
 PhyAddrUSDpndcyInd,
 ValidPrtSubCatTyp
 FROM LocSrchTbl 
 WHERE PrtId = p_PrtId;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCSEL1CSP;
 RAISE;
 
 END PRTLOCSEL1CSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:59. --->
PROCEDURE VALIDPRTLGLTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtLglTyp VARCHAR2 :=NULL,
 p_ValidPrtLglTypDispOrdNmb NUMBER:=0,
 p_ValidPrtLglTypDesc VARCHAR2 :=NULL,
 p_ValidPrtLglTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtLglTypIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtLglTypTbl 
 (
 ValidPrtLglTyp, 
 ValidPrtLglTypDispOrdNmb, 
 ValidPrtLglTypDesc, 
 ValidPrtLglTypCreatUserId
 )
 VALUES (
 p_ValidPrtLglTyp, 
 p_ValidPrtLglTypDispOrdNmb, 
 p_ValidPrtLglTypDesc, 
 p_ValidPrtLglTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 
 WHEN OTHERS THEN
 ROLLBACK TO VALIDPRTLGLTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


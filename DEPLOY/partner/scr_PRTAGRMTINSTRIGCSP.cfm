<!--- Saved 05/05/2015 13:16:54. --->
PROCEDURE PRTAGRMTINSTRIGCSP (
 NewDataRec PrtAgrmtTbl%ROWTYPE
 )
 AS
 v_error number(11);
 v_EffectiveDt DATE;
 v_ExpirationDt DATE;
 v_PrtAgrmtSeqNmb number(3);
 v_PrtAgrmtTermDt DATE;
 v_ErrorMessageTxt varchar2(255);
 v_ErrorNmb number(11);
 v_MaxTermMonthQty number(3);
 v_PartnerId number(7);
 v_ProgramId varchar2(20);
 v_UserId VARCHAR2(32);
 v_SubcategoryTyp varchar2(5);
 v_today DATE;
 v_PrtAgrmtLoanLimtInd char(1);
 v_PrtAgrmtMaxMoLoanQty number(11);
 v_PrtAgrmtMaxFyLoanQty number(11);
 v_PrgrmParntPrgrmId varchar2(20);
 v_PrgrmAreaOfOperReqdInd CHAR(1);
 v_LocId number(7,0);
 v_FIRSNmb Char(7);
 -- Temporary variables
 v_cnt number(11);
 -- declare cursor
 CURSOR FirCur IS SELECT LocId FROM PrtLocTbl WHERE PrtLocFIRSNmb IS NULL AND PrtId = v_PartnerId;
 BEGIN
 v_today := trunc(SYSDATE);
 v_PartnerId := NewDataRec.PrtId;
 v_PrtAgrmtSeqNmb := NewDataRec.PrtAgrmtSeqNmb;
 v_ProgramId := NewDataRec.PrgrmId;
 v_EffectiveDt := NewDataRec.PrtAgrmtEffDt;
 v_ExpirationDt := NewDataRec.PrtAgrmtExprDt;
 v_PrtAgrmtTermDt := NewDataRec.PrtAgrmtTermDt;
 v_UserId := NewDataRec.PrtAgrmtCreatUserId;
 BEGIN
 SELECT p.ValidPrtSubCatTyp INTO v_SubcategoryTyp FROM PrtTbl p WHERE p.PrtId = NewDataRec.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_SubcategoryTyp := NULL; END;
 BEGIN
 SELECT prog.PrgrmMaxTrmMoQty INTO v_MaxTermMonthQty FROM PrgrmTbl prog WHERE prog.PrgrmId = NewDataRec.PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxTermMonthQty := NULL; END;
 BEGIN
 SELECT prog.PrgrmParntPrgrmId INTO v_PrgrmParntPrgrmId FROM PrgrmTbl prog WHERE prog.PrgrmId = NewDataRec.PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrgrmParntPrgrmId := NULL; END;
 BEGIN
 SELECT prog.PrgrmAreaOfOperReqdInd INTO v_PrgrmAreaOfOperReqdInd FROM PrgrmTbl prog WHERE prog.PrgrmId = NewDataRec.PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrgrmAreaOfOperReqdInd := NULL; END;
 
 --Check compatibility of Subcat and Program.
 SELECT count(*) into v_cnt FROM ValidPrgrmSubCatTbl v
 WHERE v.PrgrmId = v_ProgramId
 AND v.ValidPrtSubCatTyp = v_SubcategoryTyp;
 IF v_cnt = 0
 THEN
 BEGIN
 v_ErrorNmb := -20605;
 v_ErrorMessageTxt := 'Partner Subcategory not compatible with Program.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END; -- Error.
 END IF;
 
 /* Check to see that the Expiration Date doesn't exceed the maximumm term limit */
 IF v_ExpirationDt IS NOT NULL
 THEN
 BEGIN
 --Validate dates.
 IF v_EffectiveDt >= v_ExpirationDt
 THEN
 BEGIN
 v_ErrorNmb := -20603;
 v_ErrorMessageTxt := 'Expiration date is earlier than Effective date.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END; /* End Validate dates */
 END IF;
 
 IF trunc(MONTHS_BETWEEN(v_ExpirationDt,v_EffectiveDt)) > v_MaxTermMonthQty
 THEN
 BEGIN
 v_ErrorNmb := -20604;
 v_ErrorMessageTxt := 'Agreement term exceeds program maximum.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF; /* End DATEDIFF(mm, v_EffectiveDt, v_ExpirationDt) > v_MaxTermMonthQty */
 END; /* End v_ExpirationDt IS NOT NULL */
 END IF;
 
 /* Check to see if this is Active agreement no other active agreement exists */
 IF (v_EffectiveDt <= SYSDATE and (v_ExpirationDt >= v_today or v_ExpirationDt IS NULL) and
 (v_PrtAgrmtTermDt > SYSDATE or v_PrtAgrmtTermDt IS NULL))
 THEN
 BEGIN
 select count(*) into v_cnt from PrtAgrmtTbl
 where PrtId = v_PartnerId
 and PrgrmId = v_ProgramId
 and (PrtAgrmtEffDt <= SYSDATE and (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt IS NULL) 
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL));
 IF v_cnt > 1
 THEN
 BEGIN
 v_ErrorNmb := -20602;
 v_ErrorMessageTxt := 'Active Agreement already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* End if active agreement */
 END IF;
 
 /* Check to see if this is Pending agreement no other pending agreement exists */
 IF (v_EffectiveDt > SYSDATE)
 THEN
 BEGIN
 select count(*) into v_cnt from PrtAgrmtTbl
 where PrtId = v_PartnerId
 and PrgrmId = v_ProgramId
 and PrtAgrmtEffDt > SYSDATE;
 
 IF v_cnt > 1
 THEN
 BEGIN
 v_ErrorNmb := -20602;
 v_ErrorMessageTxt := 'Pending Agreement already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 
 /* CMX Limits for Community Express */
 IF (v_ProgramId = 'Community Express')
 THEN
 BEGIN
 BEGIN
 select PrtAgrmtMaxMoLoanQty into v_PrtAgrmtMaxMoLoanQty from PrtAgrmtTbl a
 where PrtId = v_PartnerId and PrgrmId = v_ProgramId and PrtAgrmtLoanLimtInd='Y' 
 and PrtAgrmtSeqNmb <> v_PrtAgrmtSeqNmb
 and PrtAgrmtSeqNmb = (select max(PrtAgrmtSeqNmb) from PrtAgrmtTbl z where z.PrtId = a.PrtId
 and z.PrgrmId=a.PrgrmId and z.PrtAgrmtLoanLimtInd=z.PrtAgrmtLoanLimtInd
 and PrtAgrmtSeqNmb <> v_PrtAgrmtSeqNmb);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtMaxMoLoanQty := NULL; END;
 BEGIN
 select PrtAgrmtMaxFyLoanQty into v_PrtAgrmtMaxFyLoanQty from PrtAgrmtTbl a
 where PrtId = v_PartnerId and PrgrmId = v_ProgramId and PrtAgrmtLoanLimtInd='Y' 
 and PrtAgrmtSeqNmb <> v_PrtAgrmtSeqNmb
 and PrtAgrmtSeqNmb = (select max(PrtAgrmtSeqNmb) from PrtAgrmtTbl z where z.PrtId = a.PrtId
 and z.PrgrmId=a.PrgrmId and z.PrtAgrmtLoanLimtInd=z.PrtAgrmtLoanLimtInd
 and PrtAgrmtSeqNmb <> v_PrtAgrmtSeqNmb);
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtAgrmtMaxFyLoanQty := NULL; END;
 
 IF (v_PrtAgrmtMaxMoLoanQty is not null AND v_PrtAgrmtMaxFyLoanQty is not null)
 THEN
 BEGIN
 update PrtAgrmtTbl
 set PrtAgrmtMaxMoLoanQty = v_PrtAgrmtMaxMoLoanQty,
 PrtAgrmtMaxFyLoanQty = v_PrtAgrmtMaxFyLoanQty,
 PrtAgrmtLoanLimtInd = 'Y'
 where PrtId = v_PartnerId
 and PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb;
 
 IF SQLCODE != 0
 THEN
 rollback;
 END IF;
 END;
 END IF;
 END; -- End CMX
 END IF;
 
 /* Check to see this pending agreement period doesn't overlap with the active agreement */
 select count(*) into v_cnt from PrtAgrmtTbl where PrtId = v_PartnerId and PrgrmId = v_ProgramId
 and (PrtAgrmtExprDt >= v_EffectiveDt or PrtAgrmtExprDt IS NULL)
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL);
 
 IF v_cnt > 1
 THEN
 BEGIN
 v_ErrorNmb := -20602;
 v_ErrorMessageTxt := 'Active Agreement covering the period exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* End if pending agreement */
 END IF;
 
 IF v_PrgrmAreaOfOperReqdInd = 'P'
 THEN
 BEGIN
 INSERT INTO PrtAreaOfOperTbl
 (PrtId,
 PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 PrtAreaOfOperCntyEffDt,
 PrtAreaOfOperCntyEndDt,
 PrtAreaOfOperCreatUserId,
 PrtAreaOfOperCreatDt) 
 SELECT PrtId,
 v_PrtAgrmtSeqNmb,
 PrtAreaOfOperSeqNmb,
 StCd,
 CntyCd,
 v_EffectiveDt,
 Null,
 NVL(v_UserId,USER),
 SYSDATE 
 FROM PrtAreaOfOperTbl
 WHERE PrtId= v_PartnerId 
 AND PrtAgrmtSeqNmb = (SELECT MAX(PrtAgrmtSeqNmb) FROM PrtAgrmtTbl 
 WHERE PrtId = v_PartnerId AND PrgrmId = v_PrgrmParntPrgrmId )
 AND PrtAreaOfOperCntyEndDt IS NULL;
 END;
 END IF;
 
 IF (v_ProgramId = 'General 7(a)')
 THEN
 BEGIN
 v_FIRSNmb := NULL;
 open FirCur;
 fetch FirCur into v_LocId;
 LOOP
 EXIT WHEN FirCur%NOTFOUND;
 BEGIN
 v_FIRSNmb := null;
 partner.GenerateFIRSNmbCSP(v_FIRSNmb,v_UserId);
 
 update PrtLocTbl
 set PrtLocFIRSNmb = v_FIRSNmb
 where LocId = v_LocId;
 
 fetch FirCur into v_LocId;
 END; -- End of FirCur
 END LOOP;
 close FirCur;
 END; -- v_ProgramId = 'General 7(a)'
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


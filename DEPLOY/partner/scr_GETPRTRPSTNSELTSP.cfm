<!--- Saved 05/20/2016 14:04:10. --->
PROCEDURE GETPRTRPSTNSELTSP (
 p_locId NUMBER := 0,
 p_PrtCntctNmb NUMBER := NULL,
 p_queryType CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 IF p_queryType = 'all' THEN
 
 
 OPEN p_SelCur FOR
 SELECT cps.PrtCntctPosTypTxt, cps.PrtCntctPosTypCd
 FROM partner.PrtCntctPosTypTbl cps;
 
 ELSE IF p_queryType = 'selected' THEN
 
 OPEN p_SelCur FOR
 SELECT cps.PrtCntctPosTypTxt,
 CASE
 WHEN ps.PrtCntctPosTypCd IS NULL THEN 'N'
 ELSE 'Y'
 END
 PrtCntctPosTypTxt
 FROM partner.PrtLocCntctPosTbl ps,
 partner.PrtCntctPosTypTbl cps
 WHERE cps.PrtCntctPosTypCd = ps.PrtCntctPosTypCd(+)
 AND ps.LocId = p_locId
 AND ps.PrtCntctNmb = NVL (p_PrtCntctNmb, ps.PrtCntctNmb);
 
 ELSE
 
 OPEN p_SelCur FOR
 SELECT cps.PrtCntctPosTypTxt, cps.PrtCntctPosTypCd
 FROM partner.PrtLocCntctTbl c,
 partner.PrtLocCntctPosTbl ps,
 partner.PrtCntctPosTypTbl cps
 WHERE c.LocId = ps.LocId
 AND c.PrtCntctNmb = ps.PrtCntctNmb
 AND ps.PrtCntctPosTypCd = cps.PrtCntctPosTypCd(+)
 AND c.LocId = p_locId
 AND c.PrtCntctNmb = NVL (p_PrtCntctNmb, c.PrtCntctNmb);
 
 
 
 END if;
 
 end if;
 end; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


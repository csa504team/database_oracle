<!--- Saved 05/05/2015 13:19:02. --->
PROCEDURE PRTOWNRSHPTREESELCSP(
 p_PrtId IN NUMBER := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTOWNRSHPTREESEL;
 OPEN p_SelCur FOR
 SELECT DISTINCT 
 o.PrtId,
 r.Root,
 r.NodeId,
 r.LevelNmb,
 r.PrtLglNm,
 r.PrtId PartnerId,
 r.OwningPrtId,
 r.PrtOwnrshpPct,
 p.LocQty,
 p.ValidPrtPrimCatTyp,
 p.ValidPrtSubCatTyp,
 p.PrtLocFIRSNmb
 FROM PrtOwnrshpTreeTbl o, PrtOwnrshpTreeTbl r, PrtSrchTbl p 
 WHERE o.PrtId = p_PrtId 
 AND o.Root = r.Root 
 AND r.PrtId = p.PrtId 
 ORDER BY r.Root , r.NodeId ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteSel;
 RAISE;
 END PRTOWNRSHPTREESELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


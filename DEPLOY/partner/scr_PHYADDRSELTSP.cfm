<!--- Saved 01/23/2018 15:31:59. --->
PROCEDURE PHYADDRSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb number :=null,
 p_PhyAddrSeqNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 /* RY --- 03/15/2016 -- ADDED new identifier 11 to get physical address.
 RY--01/23/18--OPSMDEV1335:: Removed the references of PhyAddrDataSourcId from ID 0
 */
 BEGIN
 
 
 SAVEPOINT PhyAddrSel;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 
 OPEN p_SelCur FOR SELECT LocId
 , PhyAddrSeqNmb
 , ValidAddrTyp
 , PrtCntctNmb
 , PhyAddrCtyNm
 , PhyAddrCntCd
 , PhyAddrCntyNm
 , PhyAddrFIPSCntyCd
 , PhyAddrPostCd
 , PhyAddrStCd
 , PhyAddrStr1Txt
 , PhyAddrStr2Txt
 , PhyAddrUSDpndcyInd
 , LOC_ID
 , DEPARTMENT
 , ADDRESS_TYPE_CODE
 , INST_ID
 FROM PhyAddrTbl 
 WHERE LocId = p_LocId 
 and PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 END;
 ELSIF p_Identifier = 2 
 THEN
 BEGIN
 
 OPEN p_SelCur FOR 
 SELECT 1 FROM PhyAddrTbl 
 WHERE LocId = p_LocId and PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 
 
 
 ELSIF p_Identifier = 11 THEN
 
 BEGIN
 
 OPEN p_SelCur FOR 
 
 SELECT a.LocId,a.PhyAddrSeqNmb, a.ValidAddrTyp,a.PhyAddrStr1Txt,a.PhyAddrStr2Txt,a.PhyAddrCtyNm,
 a.PhyAddrStCd,a.PhyAddrCtyNm,a.PhyAddrPostCd, v.ValidAddrTypDesc
 FROM partner.PhyAddrTbl a, partner.ValidAddrTypTbl v
 WHERE a.LocId = p_LocId
 AND a.PrtCntctNmb = p_PrtCntctNmb
 AND a.ValidAddrTyp (+)= v.ValidAddrTyp; 
 END;
 
 END IF;
 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO SAVEPOINT PhyAddrSel;
 
 
 END PHYADDRSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


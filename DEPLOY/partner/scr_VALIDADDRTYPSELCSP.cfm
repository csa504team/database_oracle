<!--- Saved 05/05/2015 13:19:36. --->
PROCEDURE VALIDADDRTYPSELCSP
 (
 p_userName VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidAddrTyp, 
 ValidAddrTypDesc
 FROM ValidAddrTypTbl 
 WHERE ValidAddrTypDispOrdNmb != 0 
 ORDER BY ValidAddrTypDispOrdNmb ;
 p_RetVal := SQL%ROWCOUNT; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDADDRTYPSELCSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


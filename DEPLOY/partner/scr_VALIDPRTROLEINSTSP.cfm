<!--- Saved 05/05/2015 13:20:07. --->
PROCEDURE VALIDPRTROLEINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtRoleTyp VARCHAR2 :=NULL,
 p_ValidPrtRoleDispOrdNmb NUMBER:=0,
 p_ValidPrtRoleDesc VARCHAR2 :=NULL,
 p_ValidPrtRoleCreatUserId VARCHAR2 :=NULL
 
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtRoleIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtRoleTbl 
 (
 ValidPrtRoleTyp, 
 ValidPrtRoleDispOrdNmb, 
 ValidPrtRoleDesc, 
 ValidPrtRoleCreatUserId
 )
 VALUES 
 (
 p_ValidPrtRoleTyp, 
 p_ValidPrtRoleDispOrdNmb, 
 p_ValidPrtRoleDesc, 
 p_ValidPrtRoleCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTROLEINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


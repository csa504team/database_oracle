<!--- Saved 05/05/2015 13:19:14. --->
PROCEDURE PRTSRCHUPDADDRCSP(
 p_LocId NUMBER := 0
 )
 AS
 /*
 -- Object : Stored Procedure
 -- Date : August 29, 2000
 -- Programmer : Rajeswari DSP
 -- Company : AQUAS, Inc.
 --This procedure updates the PrtSrchTbl table.
 *************** REVISION HISTORY ************************************
 Revised By Date Description
 ---------- ----------- ----------------------------------------
 DSP Rajeswari 01/24/2001 1.Added code to update Cntycd,CntyNm,OrignOfcCd
 and CongrsnlDistNmb.
 **************** END OF COMMENTS ************************************
 */
 CurDataRec PhyAddrTbl%ROWTYPE;
 v_PrtId number(7);
 v_PhyAddrPostCd varchar2(20);
 v_PhyAddrStCd varchar2(75);
 v_CntyCd CHAR(3);
 v_INST_ID number(8);
 v_LOC_ID number (8);
 v_ErrorNmb number(5);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_LastUpdtUserId varchar2(32);
 BEGIN
 v_INST_ID := 0;
 IF p_LocId IS NULL
 THEN
 RETURN;
 END IF;
 
 BEGIN
 SELECT PrtId INTO v_PrtId from PrtLocTbl where LocId = p_LocId and ValidLocTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_PrtId := NULL; END;
 
 BEGIN
 SELECT LastUpdtUserId INTO v_LastUpdtUserId from PrtLocTbl where LocId = p_LocId and ValidLocTyp = 'HO';
 EXCEPTION WHEN NO_DATA_FOUND THEN v_LastUpdtUserId := NULL; END;
 
 BEGIN
 SELECT * INTO CurDataRec FROM PhyAddrTbl a WHERE LocId = p_LocId AND PrtCntctNmb is null AND ValidAddrTyp = 'Phys'
 AND PhyAddrSeqNmb = (SELECT MAX(PhyAddrSeqNmb) FROM PhyAddrTbl z WHERE a.LocId = z.LocId AND z.PrtCntctNmb is null
 AND a.ValidAddrTyp = z.ValidAddrTyp);
 EXCEPTION WHEN NO_DATA_FOUND THEN CurDataRec := NULL; END;
 
 IF CurDataRec.PhyAddrFIPSCntyCd IS NOT NULL
 THEN
 v_CntyCd := substr(CurDataRec.PhyAddrFIPSCntyCd,3,3);
 END IF;
 v_PhyAddrStCd := CurDataRec.PhyAddrStCd;
 v_PhyAddrPostCd := CurDataRec.PhyAddrPostCd;
 
 Update PrtSrchTbl
 SET PhyAddrCntCd = CurDataRec.PhyAddrCntCd ,
 PhyAddrStr1Txt = CurDataRec.PhyAddrStr1Txt,
 PhyAddrCtyNm = CurDataRec.PhyAddrCtyNm,
 PhyAddrCtySrchNm = Upper(CurDataRec.PhyAddrCtyNm),
 PhyAddrStCd = CurDataRec.PhyAddrStCd,
 PhyAddrPostCd = CurDataRec.PhyAddrPostCd,
 PhyAddrFIPSCntyCd = CurDataRec.PhyAddrFIPSCntyCd,
 CntyCd = v_CntyCd,
 LastUpdtUserId = v_LastUpdtUserId,
 LastUpdtDt = SYSDATE
 where PrtId = v_PrtId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 50301;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl. PrtId=' || to_char(v_PrtId);
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END;
 END IF;
 
 --update PrtSrchTbl with CntCd, CntyNm and OrignOfcCd for each partner
 IF v_CntyCd IS NOT NULL AND v_PhyAddrStCd IS NOT NULL
 THEN
 UPDATE PrtSrchTbl s
 SET (s.CntyNm, s.OrignOfcCd) = (select c.CntyNm, c.OrignOfcCd FROM sbaref.CntyTbl c
 where c.CntyCd = v_CntyCd and c.StCd = ltrim(v_PhyAddrStCd)
 and (c.CntyEndDt > SYSDATE or c.CntyEndDt is Null))
 WHERE ltrim(PhyAddrCntCd) = 'US'
 and LocId = p_LocId;
 END IF;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 50302;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl. PrtId=' || to_char(v_PrtId);
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; --Error.
 END IF;
 
 --update CongrsnlDistNmb for each partner
 IF v_PhyAddrPostCd IS NOT NULL
 THEN
 UPDATE PrtSrchTbl s
 SET s.CongrsnlDistNmb = (select c.IMCongrsnlDistCd from sbaref.Zip5CdTbl c where substr(v_PhyAddrPostCd,1,5) = c.Zip5Cd)
 where ltrim(PhyAddrCntCd) = 'US' and LocId = p_LocId;
 END IF;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 50303;
 v_ReplicationErrorMessageTxt := 'Error updating PrtSrchTbl. PrtId=' || to_char(v_PrtId);
 PrtReplicationErrInsCSP (v_INST_ID, p_LocId, 'PrtSrchUpdAddrCSP', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 RETURN;
 END; --Error.
 END IF;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


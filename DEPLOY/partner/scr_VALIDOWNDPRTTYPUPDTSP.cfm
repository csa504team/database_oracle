<!--- Saved 05/05/2015 13:19:50. --->
PROCEDURE VALIDOWNDPRTTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidOwndPrtTyp VARCHAR2 :=NULL,
 p_ValidOwndPrtTypDispOrdNmb NUMBER:=0,
 p_ValidOwndPrtTypDesc VARCHAR2 :=NULL,
 p_ValidOwndPrtTypCreatUserId VARCHAR2 :=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidOwndPrtTypUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 
 UPDATE ValidOwndPrtTypTbl
 SET 
 ValidOwndPrtTypDispOrdNmb = p_ValidOwndPrtTypDispOrdNmb, 
 ValidOwndPrtTypDesc = p_ValidOwndPrtTypDesc, 
 ValidOwndPrtTypCreatUserId = p_ValidOwndPrtTypCreatUserId, 
 ValidOwndPrtTypCreatDt = SYSDATE
 WHERE ValidOwndPrtTyp = p_ValidOwndPrtTyp;
 p_RetVal := SQL%ROWCOUNT;
 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDOWNDPRTTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


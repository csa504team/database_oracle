<!--- Saved 01/23/2018 15:32:00. --->
PROCEDURE PRTHISTRYUPDTSP (
 p_Identifier IN NUMBER DEFAULT 0,
 p_RetVal IN OUT NUMBER,
 p_PrtId IN NUMBER DEFAULT 0,
 p_PrtHistryEffDt IN DATE DEFAULT NULL,
 p_ValidChngCd IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryPartyTyp IN CHAR DEFAULT NULL,
 p_PrtHistryInitPrtId IN NUMBER DEFAULT 0,
 p_PrtHistryResultingPrtId IN NUMBER DEFAULT 0,
 p_PrtHistryChrtrTyp IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryEstbDt IN DATE DEFAULT NULL,
 p_PrtHistryLglNm IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryPrimTyp IN VARCHAR2 DEFAULT NULL,
 p_PrtHistrySubCatTyp IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryReglAgencyNm IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryTFPHistryDt IN DATE DEFAULT NULL,
 p_AFFECTED_INST_ID IN NUMBER DEFAULT 0,
 p_EFFECTIVE_DATE IN DATE DEFAULT NULL,
 p_PARTY_TYPE_INDICATOR IN CHAR DEFAULT NULL,
 p_CHANGE_CODE IN VARCHAR2 DEFAULT NULL,
 p_PrtHistryCreatUserId IN VARCHAR2 DEFAULT NULL)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PrtHistryUpd;
 
 IF p_Identifier = 0
 THEN
 /* Update PrtHistryTbl Table */
 BEGIN
 UPDATE PARTNER.PrtHistryTbl
 SET PrtHistryInitPrtId = p_PrtHistryInitPrtId,
 PrtHistryResultingPrtId = p_PrtHistryResultingPrtId,
 PrtHistryChrtrTyp = p_PrtHistryChrtrTyp,
 PrtHistryEstbDt = p_PrtHistryEstbDt,
 PrtHistryLglNm = p_PrtHistryLglNm,
 PrtHistryPrimTyp = p_PrtHistryPrimTyp,
 PrtHistrySubCatTyp = p_PrtHistrySubCatTyp,
 PrtHistryReglAgencyNm = p_PrtHistryReglAgencyNm,
 PrtHistryTFPHistryDt = p_PrtHistryTFPHistryDt,
 AFFECTED_INST_ID = p_AFFECTED_INST_ID,
 EFFECTIVE_DATE = p_EFFECTIVE_DATE,
 PARTY_TYPE_INDICATOR = p_PARTY_TYPE_INDICATOR,
 CHANGE_CODE = p_CHANGE_CODE,
 LASTUPDTUSERID = p_PrtHistryCreatUserId,
 LASTUPDTDT = SYSDATE
 WHERE PrtId = p_PrtId
 AND PrtHistryEffDt = p_PrtHistryEffDt
 AND ValidChngCd = p_ValidChngCd
 AND PrtHistryPartyTyp = p_PrtHistryPartyTyp;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtHistryUpd;
 RAISE;
 END PRTHISTRYUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:21. --->
PROCEDURE PRTLOCDESCTYPUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtDescTypCd NUMBER := 0,
 p_PrtLocDescTypCreatUserId VARCHAR2 := NULL)
 AS
 TranFlag NUMBER(10,0);
 error NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtLocDescTypUpd;
 IF p_Identifier = 0 THEN
 /* Update of PrtLocDescTyp Table */
 BEGIN
 UPDATE PARTNER.PrtLocDescTypTbl
 SET PrtDescTypCd = p_PrtDescTypCd, 
 lastupdtuserid = p_PrtLocDescTypCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescTypUpd;
 RAISE; 
 
 -- END;
 END PRTLOCDESCTYPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


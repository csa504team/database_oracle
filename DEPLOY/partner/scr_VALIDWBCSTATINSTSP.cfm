<!--- Saved 05/05/2015 13:20:16. --->
PROCEDURE VALIDWBCSTATINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidWBCStatCd NUMBER:=0,
 p_ValidWBCStatDescTxt VARCHAR2 :=NULL,
 p_ValidWBCStatCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 
 SAVEPOINT ValidWBCStatIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidWBCStatTbl 
 (
 ValidWBCStatCd, 
 ValidWBCStatDescTxt, 
 ValidWBCStatCreatUserId,
 ValidWBCStatCreatDt 
 )
 VALUES (
 p_ValidWBCStatCd, 
 p_ValidWBCStatDescTxt, 
 p_ValidWBCStatCreatUserId, 
 sysdate
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDWBCSTATINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


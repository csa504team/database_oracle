<!--- Saved 04/27/2016 21:35:08. --->
PROCEDURE GETNOTEBYPRTSTRTSP (
 p_Prtstring CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR )
 AS
 begin 
 
 
 if length(p_Prtstring)=1 then 
 begin
 OPEN p_SelCur FOR
 SELECT p.PrtId,p.PrtLglNm,l.LocId,l.PrtLocNm,l.PrtLocFIRSNmb,l.PrtLocFIRSRecrdTyp,l.PrtLocOpndDt,
 l.PrtLocInactDt,l.PrtLocCurStatCd,l.ValidLocTyp,n.PrtNoteEffDt,n.PrtNoteSubjTxt,n.PrtNoteSeqNmb 
 FROM partner.PrtSrchTbl p , partner.LocSrchTbl l , partner.PrtNoteTbl n
 WHERE p.PrtId = l.PrtId 
 AND l.LocId = n.LocId(+)
 AND P.PrtId = NVL(p_Prtstring,P.PrtId);
 
 
 END;
 
 
 elsif length(p_Prtstring)>1 then
 
 
 
 if SUBSTR(p_Prtstring,1,1)in (0,1,2,3,4,5,6,7,8,9)
 
 THEN 
 
 OPEN p_SelCur FOR
 SELECT p.PrtId,p.PrtLglNm,l.LocId,l.PrtLocNm,l.PrtLocFIRSNmb,l.PrtLocFIRSRecrdTyp,l.PrtLocOpndDt,
 l.PrtLocInactDt,l.PrtLocCurStatCd,l.ValidLocTyp,n.PrtNoteEffDt,n.PrtNoteSubjTxt,n.PrtNoteSeqNmb 
 FROM partner.PrtSrchTbl p , partner.LocSrchTbl l , partner.PrtNoteTbl n
 WHERE p.PrtId = l.PrtId 
 AND l.LocId = n.LocId(+)
 AND P.PrtId = NVL(p_Prtstring,P.PrtId);
 
 
 END IF;
 
 elsif p_prtstring is not null then 
 
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT p.PrtId,p.PrtLglNm,l.LocId,l.PrtLocNm,l.PrtLocFIRSNmb,l.PrtLocFIRSRecrdTyp,l.PrtLocOpndDt,
 l.PrtLocInactDt,l.PrtLocCurStatCd,l.ValidLocTyp,n.PrtNoteEffDt,n.PrtNoteSubjTxt,n.PrtNoteSeqNmb 
 FROM partner.PrtSrchTbl p , partner.LocSrchTbl l , partner.PrtNoteTbl n
 WHERE p.PrtId = l.PrtId 
 AND l.LocId = n.LocId(+)
 AND Upper(p.PrtLglSrchNm) like '%p_Prtstring%'
 ORDER BY p.PrtLglNm ;
 END;
 
 
 END IF;
 
 
 end; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


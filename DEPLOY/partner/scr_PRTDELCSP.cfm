<!--- Saved 05/05/2015 13:17:32. --->
PROCEDURE PRTDELCSP(
 user_ IN VARCHAR2 DEFAULT NULL,
 p_PrtId IN NUMBER DEFAULT NULL)
 AS
 OldDataRec PrtTbl%ROWTYPE;
 BEGIN
 -- get old snapshot
 begin
 SELECT * INTO OldDataRec FROM PrtTbl WHERE PrtId = p_PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN OldDataRec := NULL; END;
 DELETE PrtTbl 
 WHERE PrtId = p_PrtId;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtDelTrigCSP (OldDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END PRTDELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:43. --->
PROCEDURE PRGRMINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrgrmAreaOfOperReqdInd CHAR := NULL,
 p_PrgrmDispOrdNmb NUMBER := 0,
 p_PrgrmDocReqdInd CHAR := NULL,
 p_PrgrmEffDt DATE := NULL,
 p_PrgrmFormId VARCHAR2 := NULL,
 p_PrgrmMaxTrmMoQty NUMBER := 0,
 p_PrgrmDesc VARCHAR2 := NULL,
 p_ValidPrgrmTyp VARCHAR2 := NULL,
 p_PrgrmParntPrgrmId VARCHAR2 := NULL,
 p_PrgrmCreatUserId VARCHAR2 := NULL
 )
 AS
 BEGIN
 
 SAVEPOINT PrgrmIns;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 INSERT INTO PARTNER.PrgrmTbl (PrgrmId, PrgrmAreaOfOperReqdInd, PrgrmDispOrdNmb, PrgrmDocReqdInd,
 PrgrmEffDt, PrgrmFormId, PrgrmMaxTrmMoQty, PrgrmDesc, ValidPrgrmTyp, PrgrmParntPrgrmId, PrgrmCreatUserId,PrgrmCreatDt,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_PrgrmId, p_PrgrmAreaOfOperReqdInd, p_PrgrmDispOrdNmb, 
 p_PrgrmDocReqdInd , p_PrgrmEffDt, p_PrgrmFormId,p_PrgrmMaxTrmMoQty, p_PrgrmDesc, p_ValidPrgrmTyp, 
 p_PrgrmParntPrgrmId,p_PrgrmCreatUserId,SYSDATE,p_PrgrmCreatUserId,sysdate);
 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO PhyAddrIns;
 
 END PRGRMINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


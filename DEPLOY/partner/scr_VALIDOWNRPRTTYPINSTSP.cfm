<!--- Saved 05/05/2015 13:19:51. --->
PROCEDURE VALIDOWNRPRTTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidOwnrPrtTyp VARCHAR2 :=NULL,
 p_ValidOwnrPrtTypDispOrdNmb NUMBER:=0,
 p_ValidOwnrPrtTypDesc VARCHAR2 :=NULL,
 p_ValidOwnrPrtTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidOwnrPrtTypIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidOwnrPrtTypTbl 
 (
 ValidOwnrPrtTyp, 
 ValidOwnrPrtTypDispOrdNmb, 
 ValidOwnrPrtTypDesc, 
 ValidOwnrPrtTypCreatUserId
 )
 
 VALUES (
 p_ValidOwnrPrtTyp, 
 p_ValidOwnrPrtTypDispOrdNmb, 
 p_ValidOwnrPrtTypDesc, 
 p_ValidOwnrPrtTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDOWNRPRTTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


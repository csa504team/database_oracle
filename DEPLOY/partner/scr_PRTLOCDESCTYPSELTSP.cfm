<!--- Saved 05/05/2015 13:18:19. --->
PROCEDURE PRTLOCDESCTYPSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtLocDescTypSel;
 IF p_Identifier = 0 THEN
 /* Select from PrtLocDescTyp Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocDescTypSeqNmb,
 PrtDescTypCd
 FROM PrtLocDescTypTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocDescTypTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescTypSel;
 RAISE;
 
 END PRTLOCDESCTYPSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


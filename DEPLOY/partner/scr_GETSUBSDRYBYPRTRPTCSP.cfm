<!--- Saved 04/14/2016 15:46:08. --->
PROCEDURE GetSubsdryByPrtRptCSP
 (
 p_PrtId Number := NULL,
 p_PrgrmId Varchar2 := NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT Number
 )
 AS
 BEGIN
 
 Open P_SelCur For
 SELECT p.PrtId,o.NodeId,o.LevelNmb,p.PrtLglNm as Name,pa.PrgrmId 
 FROM partner.PrtOwnrshpTreeTbl o,partner.PrtTbl p,partner.PrtAgrmtTbl pa
 WHERE o.NodeId in (Select r.NodeId 
 FROM partner.PrtOwnrshpTreeTbl r 
 WHERE r.Root in (SELECT DISTINCT o1.Root FROM partner.PrtOwnrshpTreeTbl o1 WHERE o1.PrtId = p_PrtId )) 
 AND o.PrtId = p.PrtId 
 AND p.PrtId = pa.PrtId 
 AND p.PrtCurStatCd = 'O' 
 AND pa.PrtAgrmtEffDt <=sysdate 
 AND ((pa.PrtAgrmtExprDt >= sysdate OR pa.PrtAgrmtExprDt is NULL ) AND (pa.PrtAgrmtTermDt > sysdate OR pa.PrtAgrmtTermDt is NULL)) 
 AND lower(pa.PrgrmId) = NVL(lower(p_PrgrmId),pa.PrgrmId)
 ORDER BY o.NodeId;
 
 p_RetVal := SQL%ROWCOUNT;
 
 END GetSubsdryByPrtRptCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


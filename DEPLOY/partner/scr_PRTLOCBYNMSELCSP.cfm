<!--- Saved 05/05/2015 13:17:54. --->
PROCEDURE PRTLOCBYNMSELCSP(
 p_userName VARCHAR2 := NULL,
 p_PrtLocNm VARCHAR2 := NULL,
 p_ValidLocTyp VARCHAR2 := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCBYNMSELCSP;
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtId,
 ValidLocTyp,
 PrtLocNm,
 PrtLocCurStatCd,
 PrtLocOpndDt,
 PrtLocInactDt,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp
 FROM PrtLocTbl 
 WHERE RTRIM(PrtLocNm) LIKE RTRIM(p_PrtLocNm) 
 AND ValidLocTyp = p_ValidLocTyp;
 p_RetVal := SQL%ROWCOUNT;
 END;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCBYNMSELCSP;
 RAISE;
 END PRTLOCBYNMSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


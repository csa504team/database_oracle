<!--- Saved 05/05/2015 13:20:01. --->
PROCEDURE VALIDPRTLOCCLSFCDINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtLocClsfCd NUMBER:=0,
 p_ValidPrtLocClsfDescTxt VARCHAR2 :=NULL,
 p_ValidPrtLocClsfCreatUserId VARCHAR2:= NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtLocClsfCdIns;
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtLocClsfCdTbl (
 ValidPrtLocClsfCd, 
 ValidPrtLocClsfDescTxt, 
 ValidPrtLocClsfCreatUserId, 
 ValidPrtLocClsfCreatDt
 )
 VALUES (
 p_ValidPrtLocClsfCd, 
 p_ValidPrtLocClsfDescTxt, 
 p_ValidPrtLocClsfCreatUserId,
 SYSDATE
 );
 
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 
 WHEN OTHERS THEN
 ROLLBACK TO VALIDPRTLOCCLSFCDINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/25/2018 10:37:54. --->
PROCEDURE GETALIASPRTSELTSP (
 --p_Identifier number :=null ,
 p_PrtId NUMBER := 0,
 p_PrtLglSrchNm CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd 
 */
 BEGIN
 IF LENGTH (p_PrtId) = 1
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 a.ValidPrtAliasTyp,
 a.PrtAliasNm,
 va.ValidPrtAliasTypDesc
 FROM partner.PrtSrchTbl p,
 partner.PrtAliasTbl a
 LEFT OUTER JOIN partner.ValidPrtAliasTypTbl va
 ON a.ValidPrtAliasTyp = va.ValidPrtAliasTyp
 WHERE p.PrtId = a.PrtId;
 END;
 ELSIF LENGTH (p_PrtId) > 1
 THEN
 IF SUBSTR (p_PrtId, 2, 1) IN (0,
 1,
 2,
 3,
 4,
 5,
 6,
 7,
 8,
 9)
 THEN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 a.ValidPrtAliasTyp,
 a.PrtAliasNm,
 va.ValidPrtAliasTypDesc
 FROM partner.PrtSrchTbl p,
 partner.PrtAliasTbl a
 LEFT OUTER JOIN partner.ValidPrtAliasTypTbl va
 ON a.ValidPrtAliasTyp = va.ValidPrtAliasTyp
 WHERE p.PrtId = a.PrtId AND p.PrtId = p_PrtId;
 END IF;
 ELSIF p_PrtLglSrchNm IS NOT NULL
 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 p.PrtLglNm,
 a.ValidPrtAliasTyp,
 a.PrtAliasNm,
 va.ValidPrtAliasTypDesc
 FROM partner.PrtSrchTbl p,
 partner.PrtAliasTbl a
 LEFT OUTER JOIN partner.ValidPrtAliasTypTbl va
 ON a.ValidPrtAliasTyp = va.ValidPrtAliasTyp
 WHERE p.PrtId = a.PrtId
 AND LOWER (p.PrtLglSrchNm) LIKE '%p_PrtLglSrchNm%';
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


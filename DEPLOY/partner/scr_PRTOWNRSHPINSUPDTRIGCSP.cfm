<!--- Saved 05/05/2015 13:18:57. --->
PROCEDURE PRTOWNRSHPINSUPDTRIGCSP (
 NewDataRec PrtOwnrshpTbl%ROWTYPE
 )
 AS
 --
 -- Adds the head office location for the owned Partner to the
 -- extract queue.
 --
 -- Revision History -----------------------------------------
 -- 07/25/2000 C.Woodard Removed code setting lone ownership
 -- rows with null percentage to 100
 -- percent.
 -- 05/04/2000 C.Woodard Modified UPDATE statement.
 -- 04/20/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 v_ErrorNmb number(5);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_OwnershipPct number(5,2);
 v_OwningPartnerId number(7);
 v_PartnerId number(7);
 v_CntPrtId number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 v_CreatUserId varchar2(32);
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_PartnerId := NewDataRec.PrtId;
 v_OwnershipPct := NewDataRec.PrtOwnrshpPct;
 v_OwningPartnerId := NewDataRec.OwningPrtId;
 v_CreatUserId := NewDataRec.LastUpdtUserId;
 
 -- Add head office location to extract queue.
 PrtLocExtrctStatInsCSP (v_PartnerId, v_CreatUserId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31502;
 v_ReplicationErrorMessageTxt := 'Unable to add HO location to extract queue.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --SAVEPOINT PrtOwnrshpInsUpdTrig;
 --update PrtSrchTbl with no. of subsidiaries for each partner
 UPDATE partner.PrtSrchTbl s
 SET (SubsidQty,LastUpdtUserId,LastUpdtDt) = (SELECT COUNT(po.PrtId), v_CreatUserId, SYSDATE FROM PrtOwnrshpTbl po, LocSrchTbl l
 WHERE po.OwningPrtId = s.PrtId AND po.OwningPrtId = s.PrtId
 AND l.PrtId = po.PrtId AND l.ValidLocTyp = 'HO')
 where s.PrtId = v_OwningPartnerId
 and exists (SELECT 1 FROM PrtOwnrshpTbl po, LocSrchTbl l WHERE po.OwningPrtId = s.PrtId AND po.OwningPrtId = s.PrtId
 AND l.PrtId = po.PrtId AND l.ValidLocTyp = 'HO');
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30056;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Updating PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK; --TO PrtOwnrshpInsUpdTrig;
 RETURN;
 END; --Error.
 END IF;
 
 SELECT COUNT(PrtId) into v_CntPrtId FROM partner.PrtOwnrshpTbl WHERE PrtId = v_PartnerId;
 IF v_CntPrtId > 0
 THEN
 BEGIN
 UPDATE partner.PrtSrchTbl
 SET PrtIsOwned = 'Y',
 LastUpdtUserId = v_CreatUserId,
 LastUpdtDt = SYSDATE
 WHERE PrtId = v_PartnerId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30059;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Updating PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK TO PrtOwnrshpInsUpdTrig;
 RETURN;
 END; --Error.
 END IF;
 END;
 END IF;
 --COMMIT; -- UPTO PrtOwnrshpInsUpdTrig;
 RETURN;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


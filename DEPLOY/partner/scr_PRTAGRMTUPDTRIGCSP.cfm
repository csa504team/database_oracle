<!--- Saved 05/05/2015 13:17:03. --->
PROCEDURE PrtAgrmtUpdTrigCSP (
 OldDataRec PrtAgrmtTbl%ROWTYPE,
 NewDataRec PrtAgrmtTbl%ROWTYPE
 )
 AS
 v_error number(11);
 v_EffectiveDt DATE;
 v_OldEffectiveDt DATE;
 v_ExpirationDt DATE;
 v_PrtAgrmtSeqNmb number(3);
 v_PrtAgrmtTermDt DATE;
 v_ErrorMessageTxt varchar2(255);
 v_ErrorNmb number(11);
 v_MaxTermMonthQty number(3);
 v_PartnerId number(7);
 v_ProgramId varchar2(20);
 v_UserId VARCHAR2(32);
 v_SubcategoryTyp varchar2(5);
 v_AgreementStatus char(1);
 v_PrtAgrmtAudtNmb number(11);
 v_today DATE;
 -- Temporary variables
 v_cnt1 number(11);
 v_cnt2 number(11);
 v_cnt3 number(11);
 BEGIN
 v_today := trunc(SYSDATE);
 v_OldEffectiveDt := OldDataRec.PrtAgrmtEffDt;
 v_AgreementStatus := case when (OldDataRec.PrtAgrmtEffDt <= SYSDATE and (OldDataRec.PrtAgrmtExprDt >= v_today or OldDataRec.PrtAgrmtExprDt IS NULL)
 and (OldDataRec.PrtAgrmtTermDt > SYSDATE or OldDataRec.PrtAgrmtTermDt IS NULL))
 then 'A'
 when (OldDataRec.PrtAgrmtExprDt < v_today and OldDataRec.PrtAgrmtTermDt IS NULL)
 then 'I'
 when (OldDataRec.PrtAgrmtTermDt is not NULL and OldDataRec.PrtAgrmtTermDt < SYSDATE)
 then 'T'
 when OldDataRec.PrtAgrmtEffDt > SYSDATE
 then 'P' end;
 v_PartnerId := NewDataRec.PrtId;
 v_PrtAgrmtSeqNmb := NewDataRec.PrtAgrmtSeqNmb;
 v_EffectiveDt := NewDataRec.PrtAgrmtEffDt;
 v_ExpirationDt := NewDataRec.PrtAgrmtExprDt;
 v_PrtAgrmtTermDt := NewDataRec.PrtAgrmtTermDt;
 v_UserId := NewDataRec.PrtAgrmtCreatUserId;
 BEGIN
 SELECT prog.PrgrmMaxTrmMoQty into v_MaxTermMonthQty /* Maximum term for the program */
 FROM PrgrmTbl prog WHERE prog.PrgrmId = NewDataRec.PrgrmId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_MaxTermMonthQty := NULL; END;
 
 IF (v_AgreementStatus not in ('A','P'))
 THEN
 BEGIN
 v_ErrorNmb := 31605;
 v_ErrorMessageTxt := 'Cannot update an agreement which is not active or pending';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 
 IF (OldDataRec.PrtAgrmtEffDt <> NewDataRec.PrtAgrmtEffDt) or (OldDataRec.PrtAgrmtExprDt <> NewDataRec.PrtAgrmtExprDt)
 THEN
 BEGIN
 /* If Effective date or Expiration date are updated for A or P make sure that one and only one A, P exists */
 select count(*) into v_cnt1 from PrtAgrmtTbl
 where PrtId = v_PartnerId and PrgrmId = v_ProgramId
 and (PrtAgrmtEffDt <= SYSDATE and (PrtAgrmtExprDt >= v_today or PrtAgrmtExprDt IS NULL)
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL));
 
 select count(*) into v_cnt2 from PrtAgrmtTbl
 where PrtId = v_PartnerId and PrgrmId = v_ProgramId and PrtAgrmtEffDt > SYSDATE;
 
 select count(*) into v_cnt3 from PrtAgrmtTbl
 where PrtId = v_PartnerId and PrgrmId = v_ProgramId
 and (PrtAgrmtExprDt >= v_ExpirationDt or PrtAgrmtExprDt IS NULL)
 and (PrtAgrmtTermDt > SYSDATE or PrtAgrmtTermDt IS NULL);
 
 IF v_cnt1 > 1
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Active Agreement already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 ELSIF v_cnt2 > 1
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Pending Agreement already exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 ELSIF v_cnt3 > 1
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Active Agreement covering the period exists';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 
 /* Check to see that the Expiration Date doesn't exceed the maximumm term limit */
 IF v_ExpirationDt IS NOT NULL
 THEN
 BEGIN
 --Validate dates.
 IF v_EffectiveDt >= v_ExpirationDt
 THEN
 BEGIN
 v_ErrorNmb := 31603;
 v_ErrorMessageTxt := 'Expiration date is earlier than Effective date.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END; /* End Validate dates */
 END IF;
 
 IF trunc(MONTHS_BETWEEN(v_ExpirationDt,v_EffectiveDt)) > v_MaxTermMonthQty
 THEN
 BEGIN
 v_ErrorNmb := 31604;
 v_ErrorMessageTxt := 'Agreement term exceeds program maximum.';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END; /* End DATEDIFF(mm, v_EffectiveDt, v_ExpirationDt) > v_MaxTermMonthQty */
 END IF;
 END; /* End v_ExpirationDt IS NOT NULL */
 END IF;
 END; /* end if update (PrtAgrmtEffDt) or update(PrtAgrmtExprDt) */
 END IF;
 
 /* If Agreement Effective date is updated cascade the changes to area of operation effective date where
 the previous effective date is same */
 IF (OldDataRec.PrtAgrmtEffDt <> NewDataRec.PrtAgrmtEffDt)
 THEN
 BEGIN
 UPDATE PrtAreaOfOperTbl
 SET PrtAreaOfOperCntyEffDt = v_EffectiveDt,
 LastUpdtUserId = v_UserId,
 LastUpdtDt = SYSDATE
 WHERE PrtId = v_PartnerId
 AND PrtAgrmtSeqNmb = v_PrtAgrmtSeqNmb
 AND trunc(PrtAreaOfOperCntyEffDt) = trunc(v_OldEffectiveDt)
 AND (PrtAreaOfOperCntyEndDt > v_EffectiveDt or PrtAreaOfOperCntyEndDt IS NULL);
 
 IF SQLCODE != 0
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'update of Area Of Operation Failed';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* end if update (PrtAgrmtEffDt) */
 END IF;
 
 /* If anything is changed for agreement status 'A' insert to audit */
 IF v_AgreementStatus = 'A'
 THEN
 BEGIN
 IncrmntSeqNmbCSP ('AuditNmb', USER, v_PrtAgrmtAudtNmb);
 IF SQLCODE != 0
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Getting Audit Sequence number from IMSeqNmbTbl failed';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 
 Insert into PrtAgrmtAudtTbl
 (PrtAgrmtAudtNmb,
 PrtId,
 PrtAgrmtSeqNmb,
 PrgrmId,
 PrtAgrmtAudtDocLocOfcCd,
 PrtAgrmtAudtAreaOfOperTxt,
 PrtAgrmtAudtDocLocTxt,
 PrtAgrmtAudtEffDt,
 PrtAgrmtAudtExprDt,
 PrtAgrmtAudtTermDt,
 PrtAgrmtTermRsnCd,
 PrtAgrmtAudtWvrInd,
 PrtAgrmtAudtCreatUserId,
 PrtAgrmtAudtCreatDt)
 VALUES
 (v_PrtAgrmtAudtNmb,
 OldDataRec.PrtId,
 OldDataRec.PrtAgrmtSeqNmb,
 OldDataRec.PrgrmId,
 OldDataRec.PrtAgrmtDocLocOfcCd,
 OldDataRec.PrtAgrmtAreaOfOperTxt,
 OldDataRec.PrtAgrmtDocLocTxt,
 OldDataRec.PrtAgrmtEffDt,
 OldDataRec.PrtAgrmtExprDt,
 OldDataRec.PrtAgrmtTermDt,
 OldDataRec.PrtAgrmtTermRsnCd,
 OldDataRec.PrtAgrmtWvrInd,
 OldDataRec.PrtAgrmtCreatUserId,
 OldDataRec.PrtAgrmtCreatDt);
 IF SQLCODE != 0
 THEN
 BEGIN
 v_ErrorNmb := 31602;
 v_ErrorMessageTxt := 'Insert to Audit table failed';
 raise_application_error(v_ErrorNmb, v_ErrorMessageTxt);
 ROLLBACK;
 RETURN;
 END;
 END IF;
 END; /* If Agreement is 'A' */
 END IF;
 END;
 /* end if v_rowcount */ 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


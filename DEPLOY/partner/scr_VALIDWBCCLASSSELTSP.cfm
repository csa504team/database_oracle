<!--- Saved 05/05/2015 13:20:15. --->
PROCEDURE VALIDWBCCLASSSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidWBCClassCd NUMBER:=0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 
 BEGIN
 SAVEPOINT ValidWBCClassSel;
 
 IF p_Identifier = 0 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 
 ValidWBCClassCd, 
 ValidWBCClassDescTxt
 FROM ValidWBCClassTbl 
 WHERE ValidWBCClassCd = p_ValidWBCClassCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier = 2 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 1
 FROM ValidWBCClassTbl 
 WHERE ValidWBCClassCd = p_ValidWBCClassCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier = 4 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT ValidWBCClassCd, 
 ValidWBCClassDescTxt
 FROM ValidWBCClassTbl;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDWBCCLASSSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


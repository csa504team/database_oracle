<!--- Saved 05/05/2015 13:18:11. --->
PROCEDURE PRTLOCCNTCTSBAROLEUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctSBARoleTypCd NUMBER := 0,
 p_PrtCntctSBARoleCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocCntctSBARoleUpd;
 IF p_Identifier = 0 THEN
 /* Updert into PrtLocCntctSBARole Table */
 BEGIN
 UPDATE PARTNER.PrtLocCntctSBARoleTbl
 SET LASTUPDTUSERID = p_PrtCntctSBARoleCreatUserId, 
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctSBARoleTypCd = p_PrtCntctSBARoleTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctSBARoleUpd;
 RAISE;
 
 END PRTLOCCNTCTSBAROLEUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


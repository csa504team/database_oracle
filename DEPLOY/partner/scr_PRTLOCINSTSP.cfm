<!--- Saved 01/23/2018 15:32:02. --->
PROCEDURE PRTLOCINSTSP (
 p_PrtLocCreatUserId VARCHAR2 := NULL,
 p_PrtLocFIRSNmb CHAR := NULL,
 p_PrtLocFIRSRecrdTyp CHAR := NULL,
 p_PrtId NUMBER := NULL,
 p_PrtLocInactDt DATE := NULL,
 p_PrtLocNm VARCHAR2 := NULL,
 p_PrtLocOpndDt DATE := NULL,
 p_PrtLocCurStatCd CHAR := NULL,
 p_ValidLocTyp VARCHAR2 := NULL,
 p_PrtLocOperHrsTxt VARCHAR2 := NULL,
 p_assigned_id OUT NUMBER,
 p_RetVal OUT NUMBER)
 AS
 NewDataRec PrtLocTbl%ROWTYPE;
 v_LocId NUMBER (11);
 /*
 Created By:
 Purpose:
 Parameters:
 Modified By: APK -- 08/02/2010-- Modified to include proper exception handler, savepoint and remove the return resultset cursor which was not required.
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */
 BEGIN
 SAVEPOINT PRTLOCINSTSP;
 v_LocId := partner.PrtLocIdSeq.NEXTVAL;
 p_assigned_id := v_LocId;
 
 INSERT INTO PARTNER.PrtLocTbl (LocId,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp,
 PrtId,
 PrtLocInactDt,
 PrtLocNm,
 PrtLocOpndDt,
 PrtLocCurStatCd,
 ValidLocTyp,
 PrtLocOperHrsTxt,
 PrtLocCreatUserId,
 lastupdtuserid,
 lastupdtdt)
 VALUES (v_LocId,
 p_PrtLocFIRSNmb,
 p_PrtLocFIRSRecrdTyp,
 p_PrtId,
 p_PrtLocInactDt,
 p_PrtLocNm,
 p_PrtLocOpndDt,
 p_PrtLocCurStatCd,
 p_ValidLocTyp,
 p_PrtLocOperHrsTxt,
 RTRIM (p_PrtLocCreatUserId),
 RTRIM (p_PrtLocCreatUserId),
 SYSDATE);
 
 p_RetVal := SQL%ROWCOUNT;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 BEGIN
 SELECT *
 INTO NewDataRec
 FROM PrtLocTbl
 WHERE LocId = v_LocId;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 NewDataRec := NULL;
 END;
 
 PrtLocInsTrigCSP (NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PRTLOCINSTSP;
 RAISE;
 END PRTLOCINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


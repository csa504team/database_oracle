<!--- Saved 01/23/2018 15:31:57. --->
PROCEDURE LOCHISTRYINSTSP(
 p_Identifier NUMBER := 0, 
 p_RetVal OUT NUMBER, 
 p_LocHistryAffctPrtId NUMBER := 0, 
 p_LocId NUMBER := 0, 
 p_LocHistryEffDt DATE := NULL, 
 p_ValidChngCd VARCHAR2 := NULL, 
 p_LocHistryPartyTyp CHAR := NULL, 
 p_LocHistryInitPrtId NUMBER := 0, 
 p_LocHistryResultingPrtId NUMBER := 0, 
 p_LocHistryChngDesc VARCHAR2 := NULL, 
 p_LocHistryLocNm VARCHAR2 := NULL, 
 p_LocHistryLocTyp VARCHAR2 := NULL, 
 p_LocHistryCtyNm VARCHAR2 := NULL, 
 p_LocHistryCntCd VARCHAR2 := NULL, 
 p_LocHistryFIPSCntyCd CHAR := NULL, 
 p_LocHistryPostCd VARCHAR2 := NULL, 
 p_LocHistryStCd VARCHAR2 := NULL, 
 p_LocHistryStr1Txt VARCHAR2 := NULL, 
 p_LocHistryStr2Txt VARCHAR2 := NULL, 
 p_LocHistryUSDpndyInd CHAR := NULL, 
 p_LocHistryTFPHistryDt DATE := NULL, 
 p_AFFECTED_INST_ID NUMBER := 0, 
 p_LOC_ID NUMBER := 0, 
 p_EFFECTIVE_DATE DATE := NULL, 
 p_PARTY_TYPE_INDICATOR CHAR := NULL, 
 p_CHANGE_CODE VARCHAR2 := NULL, 
 p_INITIATOR_INST_ID NUMBER := 0, 
 p_STATUS CHAR := NULL, 
 p_LocHistryCreatUserId VARCHAR2 := NULL
 )
 AS
 BEGIN
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of ValidDataSourcCd
 */ 
 SAVEPOINT LocHistryIns;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 INSERT INTO PARTNER.LocHistryTbl (
 LocHistryAffctPrtId,
 LocId, 
 LocHistryEffDt, 
 ValidChngCd,
 LocHistryPartyTyp, 
 LocHistryInitPrtId,
 LocHistryResultingPrtId,
 LocHistryChngDesc,
 LocHistryLocNm,
 LocHistryLocTyp,
 LocHistryCtyNm,
 LocHistryCntCd,
 LocHistryFIPSCntyCd,
 LocHistryPostCd,
 LocHistryStCd,
 LocHistryStr1Txt,
 LocHistryStr2Txt,
 LocHistryUSDpndyInd ,
 LocHistryTFPHistryDt,
 AFFECTED_INST_ID,
 LOC_ID,
 EFFECTIVE_DATE,
 PARTY_TYPE_INDICATOR, 
 CHANGE_CODE,
 INITIATOR_INST_ID,
 STATUS,
 LocHistryCreatUserId,
 LocHistryCreatdT,
 LASTUPDTUSERID,
 LASTUPDTDT
 )
 VALUES (
 p_LocHistryAffctPrtId,
 p_LocId,
 p_LocHistryEffDt,
 p_ValidChngCd,
 p_LocHistryPartyTyp,
 p_LocHistryInitPrtId,
 p_LocHistryResultingPrtId,
 p_LocHistryChngDesc,
 p_LocHistryLocNm,
 p_LocHistryLocTyp,
 p_LocHistryCtyNm,
 p_LocHistryCntCd,
 p_LocHistryFIPSCntyCd,
 p_LocHistryPostCd,
 p_LocHistryStCd,
 p_LocHistryStr1Txt,
 p_LocHistryStr2Txt,
 p_LocHistryUSDpndyInd ,
 p_LocHistryTFPHistryDt,
 p_AFFECTED_INST_ID,
 p_LOC_ID,
 p_EFFECTIVE_DATE,
 p_PARTY_TYPE_INDICATOR,
 p_CHANGE_CODE,
 p_INITIATOR_INST_ID,
 p_STATUS,
 p_LocHistryCreatUserId,
 SYSDATE,
 p_LocHistryCreatUserId,
 SYSDATE
 );
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 RAISE;
 ROLLBACK TO LocHistryIns;
 
 
 END;
 
 
 END LOCHISTRYINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


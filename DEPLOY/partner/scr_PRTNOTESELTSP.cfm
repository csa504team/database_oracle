<!--- Saved 05/05/2015 13:18:51. --->
PROCEDURE PRTNOTESELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtNoteSeqNmb NUMBER := 0,
 p_PrtId NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtNoteSel;
 IF p_Identifier = 0 THEN
 /* Select from PrtNote Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtNoteSeqNmb,
 ValidPrtNoteSubjTypCd,
 PrtNoteSubjTxt,
 EmployeeId,
 PrtNoteEffDt
 FROM PrtNoteTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtNoteTbl 
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Select from PrtNote Table based on LocId */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtNoteSeqNmb,
 PrtNoteSubjTxt,
 EmployeeId
 FROM PrtNoteTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 12 THEN
 /* Select from PrtNote Table based on LocId */
 BEGIN
 OPEN p_SelCur FOR
 SELECT n.LocId,
 n.PrtNoteSubjTxt,
 n.PrtNoteSeqNmb,
 n.EmployeeId,
 n.ValidPrtNoteSubjTypCd,
 n.PrtNoteEffDt,
 v.ValidPrtNoteSubjTypDescTxt,
 l.PrtLocNm,
 e.IMUserFirstNm as FirstNm ,
 e.IMUserLastNm as LastNm,
 e.SBAOfcCd as OfcCd,
 o.SBAOfc1Nm
 FROM PrtNoteTbl n, PrtLocTbl l, security.IMUserTbl e ,ValidPrtNoteSubjTypTbl v, sbaref.SBAOfcTbl o
 WHERE l.PrtId = p_PrtId
 AND l.LocId = n.LocId
 AND n.IMUserNm = e.IMUserNm
 AND n.ValidPrtNoteSubjTypCd = v.ValidPrtNoteSubjTypCd
 AND e.SBAOfcCd = o.SBAOfcCd
 ORDER BY n.PrtNoteEffDt DESC;
 /* e.IMUSERFIRSTNM,
 e.IMUSERLASTNM,
 e.SBAOFCCD,
 o.SBAOfc1Nm
 FROM partner.PrtNoteTbl n left outer join SECURITY.IMUSERTBL e on n.EmployeeId = e.IMUSERNm
 left outer join SBAREF.SBAOfcTbl o on e.SBAOfcCd = o.SBAOfcCd
 left outer join partner.ValidPrtNoteSubjTypTbl v on n.ValidPrtNoteSubjTypCd = v.ValidPrtNoteSubjTypCd,
 PrtLocTbl l 
 WHERE l.PrtId = p_PrtId 
 AND l.LocId = n.LocId 
 ORDER BY n.PrtNoteEffDt DESC;*/
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteSel;
 RAISE;
 
 END PRTNOTESELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


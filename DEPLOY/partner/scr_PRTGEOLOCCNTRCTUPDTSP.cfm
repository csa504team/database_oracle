<!--- Saved 05/05/2015 13:17:40. --->
PROCEDURE PRTGEOLOCCNTRCTUPDTSP
 (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0 ,
 p_PRTCNTCTNMB NUMBER := 0,
 p_STCD CHAR := NULL,
 p_CNTYCD CHAR := NULL,
 p_CreatUserId VARCHAR2 := NULL)
 
 AS
 /* SB -- 10/28/2014 -- Created for LINC Initial Lender Email */
 BEGIN
 
 SAVEPOINT PRTGEOLOCCNTRCTUPDTSP;
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 UPDATE PARTNER.PRTGEOLOCCNTRCTTBL 
 SET PRTCNTCTNMB = p_PRTCNTCTNMB,
 STCD = p_STCD,
 CNTYCD = p_CNTYCD,
 CreatUserId = p_CreatUserId,
 CreatDt = SYSDATE
 WHERE LocId = p_Locid;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:17:43. --->
PROCEDURE PRTHISTMECSP(
 p_ReportBegDateTime DATE := NULL,
 p_ReportEndDateTime DATE := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 BEGIN
 OPEN p_SelCur FOR
 SELECT a.PrtId,
 a.PrtHistryLglNm,
 r.PrtHistryLglNm,
 i.PrtHistryLglNm,
 RPAD(TO_CHAR(a.PrtHistryEffDt,'mm/dd/yyyy'), 12, ' ') EffectiveDate
 FROM PrtHistryTbl a, PrtHistryTbl r, PrtHistryTbl i 
 WHERE a.ValidChngCd = 'ME' 
 AND a.PrtHistryEffDt >= p_ReportBegDateTime 
 AND a.PrtHistryEffDt <= p_ReportEndDateTime 
 AND a.PrtHistryPartyTyp = 'A' 
 AND (a.PrtId = r.PrtId AND (r.PrtHistryPartyTyp = 'R' AND r.ValidChngCd = 'ME')) 
 AND (a.PrtId = i.PrtId AND (i.PrtHistryPartyTyp = 'I' AND i.ValidChngCd = 'ME'));
 END;
 END PRTHISTMECSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


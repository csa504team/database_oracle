<!--- Saved 05/05/2015 13:19:35. --->
PROCEDURE VALIDADDRTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidAddrTyp VARCHAR2 :=NULL,
 p_ValidAddrTypDesc VARCHAR2 :=NULL,
 p_ValidAddrTypDispOrdNmb NUMBER:=0,
 p_ValidAddrTypCreatUserId VARCHAR2 :=NULL
 
 )
 AS
 BEGIN
 SAVEPOINT ValidAddrTypIns;
 IF p_Identifier= 0 THEN
 /* Insert into Action Table */
 BEGIN
 INSERT INTO ValidAddrTypTbl 
 (
 ValidAddrTyp, 
 ValidAddrTypDesc, 
 ValidAddrTypDispOrdNmb, 
 ValidAddrTypCreatUserId
 )
 VALUES (
 p_ValidAddrTyp, 
 p_ValidAddrTypDesc, 
 p_ValidAddrTypDispOrdNmb, 
 p_ValidAddrTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDADDRTYPINSTSP;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:18. --->
PROCEDURE VALIDWBCSTATUPDTSP
 (p_Identifier number := 0,
 p_RetVal OUT number,
 p_ValidWBCStatCd number :=0,
 p_ValidWBCStatDescTxt varchar2:=NULL,
 p_ValidWBCStatCreatUserId VARCHAR2 :=NULL )
 as
 begin
 if p_Identifier = 0
 THEN
 /* Update ValidWBCStat Table */
 begin
 Update ValidWBCStatTbl
 set ValidWBCStatCd = p_ValidWBCStatCd,
 ValidWBCStatDescTxt=p_ValidWBCStatDescTxt,
 ValidWBCStatCreatUserId= p_ValidWBCStatCreatUserId,
 ValidWBCStatCreatDt = sysdate
 Where ValidWBCStatCd= p_ValidWBCStatCd;
 
 p_RetVal := SQL%ROWCOUNT ;
 
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


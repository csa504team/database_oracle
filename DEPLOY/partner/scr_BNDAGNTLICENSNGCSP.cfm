<!--- Saved 05/05/2015 13:15:52. --->
PROCEDURE BNDAGNTLICENSNGCSP
 (
 p_RetVal OUT NUMBER ,
 p_IMUserNm IN VARCHAR2 := NULL ,
 p_SelCursor1 OUT SYS_REFCURSOR
 )
 AS
 p_SelCursor SYS_REFCURSOR;
 v_RetStat NUMBER;
 v_LocId NUMBER;
 v_StCd CHAR(2);
 v_temp NUMBER(1, 0) := 0;
 BEGIN
 
 SAVEPOINT BndAgntLicensng;
 /*if not exists (select IMAppRoleId from security..IMAppRoleTbl a
 where IMAppRoleId in (select IMAppRoleId from security..IMUserRoleTbl b
 where IMUserId in (select IMUserId from security..IMUserTbl where IMUserNm=@IMUserNm )) and a.IMAppRoleNm='SBGEAAgent')*/
 BEGIN
 
 SELECT COUNT(a.IMAppRoleId)INTO v_temp
 FROM security.IMAppRoleTbl a,
 security.IMUserRoleTbl b,
 security.IMUserTbl c
 WHERE a.IMAppRoleId = b.IMAppRoleId
 AND b.IMUserId = c.IMUserId
 AND c.IMUserNm = p_IMUserNm
 AND a.IMAppRoleNm = 'SBGEAAgent' ;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp = 1 
 THEN
 
 BEGIN
 SELECT LocId INTO v_LocId
 FROM security.IMUserTbl 
 WHERE IMUserNm = p_IMUserNm;
 
 OPEN p_SelCursor1 FOR
 SELECT DISTINCT a.PrtLocNm,
 b.PhyAddrStr1Txt,
 b.PhyAddrStr2Txt,
 b.PhyAddrCtyNm,
 b.PhyAddrStCd,
 b.PhyAddrPostCd,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Phone'
 AND LocId = v_LocId ) PrtPhone,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Fax'
 AND LocId = v_LocId ) PrtFax,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Email'
 AND LocId = v_LocId ) PrtEmail,
 ( SELECT MAX(c.ElctrncAddrTxt)
 FROM ElctrncAddrTbl c
 WHERE c.ValidElctrncAddrTyp = 'Web'
 AND LocId = v_LocId ) PrtWeb
 FROM PrtLocTbl a,
 PhyAddrTbl b,
 ElctrncAddrTbl c
 WHERE a.LocId = v_LocId--@LocId
 AND a.LocId = b.LocId
 AND b.LocId = c.LocId(+)
 --and b.LocId=c.LocId
 AND a.PrtLocCurStatCd = 'O';
 END;
 ELSE
 
 BEGIN
 DBMS_OUTPUT.PUT_LINE('Not a vaild Bonding Agent');
 END;
 END IF;
 BndAgntLicensngSelTSP(0,p_RetVal, p_IMUserNm, p_SelCursor);
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 07/22/2015 10:01:41. --->
PROCEDURE VALIDATELSPBNKAGRMTCSP
 (
 p_LSPPRTID NUMBER :=NULL,
 p_LSPBNKAGRMTSEQNMB NUMBER :=NULL,
 p_CreatUserId VARCHAR2:=NULL,
 p_ErrSeqNmb OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 v_count NUMBER(3);
 v_BNKHQLOCID NUMBER(7);
 /*
 Purpose: Validations for agremeents between LSP and banks
 */
 
 BEGIN
 savepoint VALIDATELSPBNKAGRMTCSP;
 
 p_ErrSeqNmb := 0; 
 
 BEGIN
 SELECT BNKHQLOCID into v_BNKHQLOCID
 FROM PARTNER.LSPBNKAGRMTTBL
 WHERE LSPPRTID = p_LSPPRTID
 AND LSPBNKAGRMTSEQNMB =p_LSPBNKAGRMTSEQNMB;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_BNKHQLOCID := NULL;
 END;
 
 
 SELECT COUNT(*)
 INTO v_count
 FROM partner.PrtLocTbl
 WHERE LocId = v_BNKHQLOCID
 AND PrtLocCurStatCd = 'O'
 and trim(VALIDLOCTYP) = 'HO';
 
 IF v_count = 0 THEN
 
 p_ErrSeqNmb := p_ErrSeqNmb+1;
 PRTVALIDATIONERRINSCSP(p_LSPPRTID,110,p_CreatUserId);
 
 END IF; 
 
 OPEN p_SelCur for select * from partner.PRTVALIDATIONERRTBL WHERE PrtId = p_LSPPRTID;
 
 Exception When Others Then
 Rollback to VALIDATELSPBNKAGRMTCSP;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


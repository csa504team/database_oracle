<!--- Saved 05/05/2015 13:18:53. --->
PROCEDURE PRTNOTEUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtNoteSeqNmb NUMBER := 0,
 p_ValidPrtNoteSubjTypCd NUMBER := 0,
 p_PrtNoteSubjTxt VARCHAR2 := NULL,
 p_IMUserNm CHAR := NULL,
 p_PrtNoteEffDt DATE := NULL,
 p_PrtNoteCreatUserId VARCHAR2 := NULL)
 AS
 /* SB -- Changed from IMUserNm to IMUSER as per Java Req */
 BEGIN
 SAVEPOINT PrtNoteUpd;
 IF p_Identifier = 0 THEN
 /* Update of PrtNote Table */
 BEGIN
 UPDATE PARTNER.PrtNoteTbl
 SET LocId = p_LocId, 
 PrtNoteSeqNmb = p_PrtNoteSeqNmb, 
 ValidPrtNoteSubjTypCd = p_ValidPrtNoteSubjTypCd, 
 PrtNoteSubjTxt = p_PrtNoteSubjTxt, 
 IMUserNm = p_IMUserNm, 
 PrtNoteEffDt = p_PrtNoteEffDt, 
 lastupdtuserid = p_PrtNoteCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN 
 RAISE;
 ROLLBACK TO PrtNoteUpd;
 END;
 
 END PRTNOTEUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


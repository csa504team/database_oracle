<!--- Saved 05/05/2015 13:16:01. --->
PROCEDURE DLYMSGSELCSP(
 userName IN VARCHAR2 DEFAULT NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 
 BEGIN
 OPEN p_SelCur FOR
 SELECT DlyMsgDt
 , DlyMsgTxt
 , DlyMsgId
 FROM DlyMsgTbl 
 ORDER BY DlyMsgDt DESC, DlyMsgId DESC;
 END DLYMSGSELCSP;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:34. --->
PROCEDURE PRTLOCSBACNTCTINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := NULL,
 p_PrtLocSBACntctSeqNmb NUMBER := NULL,
 p_EmployeeId CHAR := NULL,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_PrtLocSBACntctPrimScndryInd CHAR := NULL,
 p_PrtLocSBACntctCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctIns;
 IF p_Identifier = 0 THEN
 /* Insert into PrtLocSBACntct Table */
 BEGIN
 INSERT INTO PARTNER.PrtLocSBACntctTbl 
 (LocId, PrtLocSBACntctSeqNmb, EmployeeId, PrtLocSBACntctTypCd, PrtLocSBACntctPrimScndryInd,
 PrtLocSBACntctCreatUserId, PrtLocSBACntctCreatDt,lastupdtuserid,lastupdtdt)
 SELECT p_LocId, NVL(MAX(PrtLocSBACntctSeqNmb), 0) + 1, p_EmployeeId, p_PrtLocSBACntctTypCd,
 p_PrtLocSBACntctPrimScndryInd, p_PrtLocSBACntctCreatUserId, SYSDATE,
 p_PrtLocSBACntctCreatUserId, SYSDATE 
 FROM PrtLocSBACntctTbl 
 WHERE LocId = p_LocId;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctIns;
 RAISE;
 
 END PRTLOCSBACNTCTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


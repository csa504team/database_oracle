<!--- Saved 05/05/2015 13:18:55. --->
PROCEDURE PRTOWNRSHPDELTRIGCSP (
 OldDataRec PrtOwnrshpTbl%ROWTYPE
 )
 AS
 --
 -- Adds the head office location for the owned Partner to the
 -- extract queue.
 --
 -- Revision History -----------------------------------------
 -- 04/20/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 v_ErrorNmb number(5);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_OwningPartnerId number(7);
 v_PartnerId number(7);
 v_CntPrtId number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_PartnerId := OldDataRec.PrtId;
 v_OwningPartnerId := OldDataRec.OwningPrtId;
 
 -- Add head office location to extract queue.
 PrtLocExtrctStatInsCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31582;
 v_ReplicationErrorMessageTxt := 'Unable to add HO location to extract queue.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --SAVEPOINT PrtOwnrshpDelTrig;
 --update PrtSrchTbl with no. of subsidiaries for each partner
 UPDATE partner.PrtSrchTbl s
 SET (SubsidQty,LastUpdtUserId,LastUpdtDt) = (SELECT COUNT(po.PrtId), USER, SYSDATE FROM PrtOwnrshpTbl po, LocSrchTbl l
 WHERE po.OwningPrtId = s.PrtId AND po.OwningPrtId = s.PrtId
 AND l.PrtId = po.PrtId AND l.ValidLocTyp = 'HO')
 where s.PrtId = v_OwningPartnerId
 and exists (SELECT 1 FROM PrtOwnrshpTbl po, LocSrchTbl l WHERE po.OwningPrtId = s.PrtId AND po.OwningPrtId = s.PrtId
 AND l.PrtId = po.PrtId AND l.ValidLocTyp = 'HO');
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30057;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Updating PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK; --TO PrtOwnrshpDelTrig;
 RETURN;
 END; --Error.
 END IF;
 
 SELECT COUNT(PrtId) into v_CntPrtId FROM partner.PrtOwnrshpTbl WHERE PrtId = v_PartnerId;
 IF v_CntPrtId = 0
 THEN
 BEGIN
 UPDATE partner.PrtSrchTbl
 SET PrtIsOwned = 'N'
 WHERE PrtId = v_PartnerId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 30058;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Updating PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtOwnrshpDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 ROLLBACK TO PrtOwnrshpDelTrig;
 RETURN;
 END; --Error.
 END IF;
 END; --
 END IF;
 
 --COMMIT; --UPTO PrtOwnrshpDelTrig;
 RETURN;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


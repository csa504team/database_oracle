<!--- Saved 05/05/2015 13:19:24. --->
PROCEDURE SBALINCLENDRSELCSP
 (
 p_Identifier NUMBER := 1,
 p_SelCur out sys_refcursor
 )
 as
 begin
 IF p_IDENTIFIER = 1 THEN
 open p_SelCur for
 Select p.PrtId,l.LocId,p.PrtLglNm
 FROM PARTNER.PRTTBL p, partner.PrtLocTbl l
 Where l.PrtId = p.PrtId 
 AND EXISTS(SELECT 1 From partner.PrtLocCntctSBARoleTbl r,partner.PrtCntCTSBARoleTypTbl t 
 Where r.PrtCntctSBARoleTypCd=t.PrtCntctSBARoleTypCd AND t.PRTCNTCTSBAROLETYPTXT = 'SBA LINC Contact'
 and r.locid = l.locid);
 END IF; 
 end;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


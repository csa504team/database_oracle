<!--- Saved 05/05/2015 13:17:06. --->
PROCEDURE PRTALIASINSUPDTRIGCSP (
 p_PartnerId number,
 p_AliasTyp VARCHAR2
 )
 --
 -- If a PartnerAlias of type FDIC or FRS or NCUA or TIN is
 -- affected, then ALL the locations for the Partner are
 -- added to the extract queue.
 --
 -- Revision History -----------------------------------------
 -- 07/14/2000 C.Woodard Corrected detection of error 30802.
 -- 07/12/2000 C.Woodard Added more info to error 30802.
 -- 04/20/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 AS
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 BEGIN
 BEGIN
 SELECT p.INST_ID into v_INST_ID FROM partner.PrtTbl p
 WHERE p.PrtId = p_PartnerId;
 EXCEPTION WHEN NO_DATA_FOUND THEN v_INST_ID := 0; END;
 
 IF p_AliasTyp IN ('FDIC', 'FRS', 'NCUA', 'TIN')
 THEN
 BEGIN
 -- Add all locations for Partner to extract queue.
 v_ErrorNmb := 0;
 partner.LocExtrctStatUpdCSP (p_PartnerId);
 v_ErrorNmb := SQLCODE;
 
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 30802;
 v_ReplicationErrorMessageTxt := 'Unable to add all locations to extract queue.' || ' PrtId=' || to_char(p_PartnerId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtAliasInsUpdTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 END; --Processing for selected Alias Type values.
 END IF;
 RETURN;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


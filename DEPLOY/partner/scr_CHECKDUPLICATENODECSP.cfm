<!--- Saved 05/05/2015 13:15:56. --->
PROCEDURE CHECKDUPLICATENODECSP(
 p_CheckLevelNmb NUMBER := NULL,
 p_CandidatePrtId NUMBER := NULL,
 p_DuplicateInd OUT CHAR)
 AS
 --
 -- Detects whether the specified partner (@CandidatePrtId)
 -- already exists in the partner ownership tree,
 -- in the path of ownership from the partner's
 -- parent upward to the root.
 --
 -- Parameters:
 -- @CheckLevelNmb is the initial level in the tree
 -- to be checked against the candidate partner.
 -- This value is normally the level of the parent
 -- node of the candidate.
 -- @CandidatePrtId is the PrtId of the partner being
 -- checked for a previous occurrence in the tree.
 -- @DuplicateInd is an output parameter which
 -- indicates whether a duplicate has been found.
 --
 -- Note 1: The subtrees under other roots are not checked.
 -- If the candidate also appears in the tree with a
 -- different root, or in another subtree below the root
 -- under which the candidate is being tested, the problem
 -- of a cycle will not result.
 --
 -- Note 2: This procedure is designed to check for cycles
 -- as the tree is being built. It will not detect cycles
 -- in the tree after the tree is complete.
 --
 -- -------------------------------------------
 -- 10/19/2000 C.Woodard Removed debugging
 -- statements.
 -- -------------------------------------------
 v_ErrorNodeId NUMBER(7,0);
 v_MaxLevelNmb NUMBER(2,0);
 v_NextLevelNmb NUMBER(2,0);
 v_PrtId NUMBER(7,0);
 v_PrtOwnrshpPct NUMBER(5,2);
 v_PrtLglNm VARCHAR2(200);
 v_INST_ID NUMBER(8,0);
 v_LOC_ID NUMBER(8,0);
 v_ErrorNmb NUMBER(5,0);
 v_ReplicationErrorMessageTxt VARCHAR2(255);
 v_StoO_selcnt NUMBER;
 v_CheckLevelNmb NUMBER := NULL;
 v_Cnt NUMBER(10);
 BEGIN
 v_MaxLevelNmb := 13;
 v_INST_ID := 0;
 v_LOC_ID := 0;
 
 v_CheckLevelNmb := p_CheckLevelNmb;
 
 IF p_CandidatePrtId IS NULL OR p_CheckLevelNmb IS NULL OR v_CheckLevelNmb < 0 OR v_CheckLevelNmb > v_MaxLevelNmb
 THEN
 RETURN;
 END IF;
 
 p_DuplicateInd := 'N';
 
 WHILE (p_DuplicateInd = 'N' AND v_CheckLevelNmb >= 0)
 LOOP
 BEGIN
 SELECT count(*) into v_Cnt FROM partner.PrtOwnrshpTreeTbl t
 WHERE t.PrtId = p_CandidatePrtId
 AND t.NodeId = (SELECT MAX(NodeId) FROM partner.PrtOwnrshpTreeTbl WHERE LevelNmb = v_CheckLevelNmb);
 
 IF v_Cnt != 0
 THEN
 p_DuplicateInd := 'Y';
 ELSE
 v_CheckLevelNmb := v_CheckLevelNmb - 1;
 END IF;
 END;
 END LOOP;
 
 END CHECKDUPLICATENODECSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


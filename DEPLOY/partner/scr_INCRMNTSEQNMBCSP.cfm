<!--- Saved 05/05/2015 13:16:19. --->
PROCEDURE INCRMNTSEQNMBCSP
 (
 p_IMSeqId IN VARCHAR2 := NULL ,
 p_CreatUserId IN VARCHAR2 := NULL,
 p_IMSeqNmb OUT NUMBER
 )
 AS
 
 v_RetStat NUMBER(10,0);
 p_RetVal NUMBER(10,0);
 
 BEGIN
 
 -- SAVEPOINT IncrmntSeqNmb;
 IMSeqNmbUpdTSP(11,p_IMSeqId, p_IMSeqNmb,p_CreatUserId);
 SELECT IMSeqNmb INTO p_IMSeqNmb
 FROM IMSeqNmbTbl
 WHERE IMSeqId = p_IMSeqId;
 /* EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO SAVEPOINT IncrmntSeqNmb;
 raise_application_error( -20002, 'Error....' );
 END;*/
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


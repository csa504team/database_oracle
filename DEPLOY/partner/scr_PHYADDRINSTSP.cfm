<!--- Saved 01/23/2018 15:31:58. --->
PROCEDURE PHYADDRINSTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PhyAddrSeqNmb NUMBER := 0,
 p_ValidAddrTyp VARCHAR2 := NULL,
 p_PrtCntctNmb NUMBER := 0,
 p_PhyAddrCtyNm VARCHAR2 := NULL,
 p_PhyAddrCntCd VARCHAR2 := NULL,
 p_PhyAddrCntyNm VARCHAR2 := NULL,
 p_PhyAddrFIPSCntyCd CHAR := NULL,
 p_PhyAddrPostCd VARCHAR2 := NULL,
 p_PhyAddrStCd VARCHAR2 := NULL,
 p_PhyAddrStr1Txt VARCHAR2 := NULL,
 p_PhyAddrStr2Txt VARCHAR2 := NULL,
 p_PhyAddrUSDpndcyInd CHAR := NULL,
 p_LOC_ID NUMBER := NULL,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_ADDRESS_TYPE_CODE VARCHAR2 := NULL,
 p_INST_ID NUMBER := NULL,
 p_PhyAddrCreatUserId VARCHAR2 := NULL)
 AS
 
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PhyAddrDataSourcId from ID 0,11
 */
 -- Trigger variable
 OldDataRec PhyAddrTbl%ROWTYPE;
 NewDataRec PhyAddrTbl%ROWTYPE;
 BEGIN
 SAVEPOINT PhyAddrIns;
 
 IF p_Identifier = 0
 THEN
 BEGIN
 INSERT INTO PARTNER.PhyAddrTbl (LocId,
 PhyAddrSeqNmb,
 ValidAddrTyp,
 PrtCntctNmb,
 PhyAddrCtyNm,
 PhyAddrCntCd,
 PhyAddrCntyNm,
 PhyAddrFIPSCntyCd,
 PhyAddrPostCd,
 PhyAddrStCd,
 PhyAddrStr1Txt,
 PhyAddrStr2Txt,
 PhyAddrUSDpndcyInd,
 LOC_ID,
 DEPARTMENT,
 ADDRESS_TYPE_CODE,
 INST_ID,
 PhyAddrCreatUserId,
 PHYADDRCREATDT,
 lastupdtuserid,
 lastupdtdt)
 VALUES (p_LocId,
 p_PhyAddrSeqNmb,
 p_ValidAddrTyp,
 p_PrtCntctNmb,
 p_PhyAddrCtyNm,
 p_PhyAddrCntCd,
 p_PhyAddrCntyNm,
 p_PhyAddrFIPSCntyCd,
 p_PhyAddrPostCd,
 p_PhyAddrStCd,
 p_PhyAddrStr1Txt,
 p_PhyAddrStr2Txt,
 p_PhyAddrUSDpndcyInd,
 p_LOC_ID,
 p_DEPARTMENT,
 p_ADDRESS_TYPE_CODE,
 p_INST_ID,
 p_PhyAddrCreatUserId,
 SYSDATE,
 p_PhyAddrCreatUserId,
 SYSDATE);
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 SELECT *
 INTO NewDataRec
 FROM PhyAddrTbl
 WHERE LocId = p_LocId AND PhyAddrSeqNmb = p_PhyAddrSeqNmb;
 
 PhyAddrInsUpdTrigCSP (OldDataRec, NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END;
 ELSIF p_Identifier = 11
 THEN
 BEGIN
 INSERT INTO PARTNER.PhyAddrTbl (LocId,
 PhyAddrSeqNmb,
 ValidAddrTyp,
 PrtCntctNmb,
 PhyAddrCtyNm,
 PhyAddrCntCd,
 PhyAddrCntyNm,
 PhyAddrFIPSCntyCd,
 PhyAddrPostCd,
 PhyAddrStCd,
 PhyAddrStr1Txt,
 PhyAddrStr2Txt,
 PhyAddrUSDpndcyInd,
 LOC_ID,
 DEPARTMENT,
 ADDRESS_TYPE_CODE,
 INST_ID,
 PhyAddrCreatUserId,
 PHYADDRCREATDT,
 lastupdtuserid,
 lastupdtdt)
 SELECT p_LocId,
 NVL (MAX (PhyAddrSeqNmb), 0) + 1,
 p_ValidAddrTyp,
 p_PrtCntctNmb,
 p_PhyAddrCtyNm,
 p_PhyAddrCntCd,
 p_PhyAddrCntyNm,
 p_PhyAddrFIPSCntyCd,
 p_PhyAddrPostCd,
 p_PhyAddrStCd,
 p_PhyAddrStr1Txt,
 p_PhyAddrStr2Txt,
 p_PhyAddrUSDpndcyInd,
 p_LOC_ID,
 p_DEPARTMENT,
 p_ADDRESS_TYPE_CODE,
 p_INST_ID,
 NVL (p_PhyAddrCreatUserId, USER),
 SYSDATE,
 p_PhyAddrCreatUserId,
 SYSDATE
 FROM PhyAddrTbl
 WHERE LocId = p_LocId;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 -- get new snapshot
 SELECT *
 INTO NewDataRec
 FROM PhyAddrTbl a
 WHERE LocId = p_LocId
 AND PhyAddrSeqNmb = (SELECT MAX (PhyAddrSeqNmb)
 FROM PhyAddrTbl z
 WHERE a.LocId = z.LocId);
 
 PhyAddrInsUpdTrigCSP (OldDataRec, NewDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 RAISE;
 ROLLBACK TO PhyAddrIns;
 END PHYADDRINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


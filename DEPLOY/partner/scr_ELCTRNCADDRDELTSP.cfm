<!--- Saved 05/05/2015 13:16:05. --->
PROCEDURE ELCTRNCADDRDELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_ElctrncAddrNmb NUMBER := 0)
 AS
 BEGIN
 IF p_Identifier = 0 
 THEN
 BEGIN
 DELETE ElctrncAddrTbl
 WHERE LocId = p_LocId
 AND ElctrncAddrNmb = p_ElctrncAddrNmb;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrDelTrigCSP (p_LocId);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK;
 
 END ELCTRNCADDRDELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


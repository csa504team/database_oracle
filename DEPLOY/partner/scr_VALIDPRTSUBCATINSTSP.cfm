<!--- Saved 05/05/2015 13:20:09. --->
PROCEDURE VALIDPRTSUBCATINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtSubCatTyp VARCHAR2 :=NULL,
 p_ValidPrtSubCatDispOrdNmb NUMBER:=0,
 p_ValidPrtSubCatDesc VARCHAR2 :=NULL,
 p_ValidSBAPrtPrimCatTyp VARCHAR2 :=NULL,
 p_RolePrivilegePrgrmId VARCHAR2 :=NULL,
 p_ValidPrtSubCatFIRSInd CHAR :=NULL,
 p_ValidPrtSubCatCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtSubCatIns;
 if p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidPrtSubCatTbl 
 (
 ValidPrtSubCatTyp, 
 ValidPrtSubCatDispOrdNmb, 
 ValidPrtSubCatDesc, 
 ValidSBAPrtPrimCatTyp, 
 RolePrivilegePrgrmId, 
 ValidPrtSubCatFIRSInd, 
 ValidPrtSubCatCreatUserId
 )
 VALUES (
 p_ValidPrtSubCatTyp, 
 p_ValidPrtSubCatDispOrdNmb, 
 p_ValidPrtSubCatDesc, 
 p_ValidSBAPrtPrimCatTyp, 
 p_RolePrivilegePrgrmId, 
 p_ValidPrtSubCatFIRSInd, 
 p_ValidPrtSubCatCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTSUBCATINS;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


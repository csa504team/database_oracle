<!--- Saved 05/05/2015 13:16:14. --->
PROCEDURE FIXLOCHISTRYCSP
 AS
 v_id NUMBER(10);
 v_LocHistryAffctPrtId NUMBER(7,0);
 v_LocId NUMBER(7,0);
 v_LocHistryEffDt DATE;
 v_ValidChngCd VARCHAR2(5);
 v_LocHistryPartyTyp CHAR(1);
 v_LocHistryInitPrtId NUMBER(7,0);
 v_LocHistryResultingPrtId NUMBER(7,0);
 v_initiator_inst_id NUMBER(8,0);
 v_resulting_inst_id NUMBER(8,0);
 v_temp NUMBER(1, 0) := 0;
 BEGIN
 INSERT INTO tt_TmpLocHistryTbl
 ( LocHistryAffctPrtId, LocId, LocHistryEffDt, ValidChngCd, LocHistryPartyTyp, LocHistryInitPrtId, LocHistryResultingPrtId, 
 initiator_inst_id, resulting_inst_id )
 ( SELECT LocHistryAffctPrtId,
 LocId,
 LocHistryEffDt,
 ValidChngCd,
 LocHistryPartyTyp,
 LocHistryInitPrtId,
 LocHistryResultingPrtId,
 t.initiator_inst_id,
 resulting_inst_id
 FROM thomson.location_history t, LocHistryTbl p
 WHERE t.affected_inst_id = p.AFFECTED_INST_ID
 AND t.loc_id = p.LOC_ID
 AND t.effective_date = p.EFFECTIVE_DATE
 AND t.party_type_indicator = p.PARTY_TYPE_INDICATOR
 AND t.change_code = p.CHANGE_CODE
 AND t.initiator_inst_id = p.INITIATOR_INST_ID
 AND ( ( NVL(p.LocHistryInitPrtId, 0) = 0
 AND NVL(t.initiator_inst_id, 0) != 0 )
 OR ( NVL(p.LocHistryResultingPrtId, 0) = 0
 AND NVL(t.resulting_inst_id, 0) != 0 ) ) );
 
 SELECT MIN(id) - 1 INTO v_id
 FROM tt_TmpLocHistryTbl ;
 
 LOOP
 BEGIN
 SELECT 1 INTO v_temp
 FROM tt_TmpLocHistryTbl 
 WHERE id > v_id ;
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 
 IF v_temp != 1 
 THEN
 EXIT;
 END IF;
 
 
 BEGIN
 SELECT MIN(id) INTO v_id
 FROM tt_TmpLocHistryTbl 
 WHERE id > v_id;
 SELECT LocHistryAffctPrtId,
 LocId,
 LocHistryEffDt,
 ValidChngCd,
 LocHistryPartyTyp,
 LocHistryInitPrtId,
 LocHistryResultingPrtId,
 initiator_inst_id,
 resulting_inst_id
 INTO v_LocHistryAffctPrtId,
 v_LocId,
 v_LocHistryEffDt,
 v_ValidChngCd,
 v_LocHistryPartyTyp,
 v_LocHistryInitPrtId,
 v_LocHistryResultingPrtId,
 v_initiator_inst_id,
 v_resulting_inst_id
 FROM tt_TmpLocHistryTbl 
 WHERE id = v_id;
 
 IF NVL(v_initiator_inst_id, 0) != 0 AND NVL(v_LocHistryInitPrtId, 0) = 0 
 THEN
 UPDATE LocHistryTbl p
 SET LocHistryInitPrtId =(select p.PrtId
 from partner.PrtTbl p
 WHERE p.INST_ID = v_initiator_inst_id
 AND LocHistryAffctPrtId = v_LocHistryAffctPrtId
 AND LocId = v_LocId
 AND LocHistryEffDt = v_LocHistryEffDt
 AND ValidChngCd = v_ValidChngCd
 AND LocHistryPartyTyp = v_LocHistryPartyTyp
 AND LocHistryInitPrtId = v_LocHistryInitPrtId);
 END IF;
 
 IF NVL(v_resulting_inst_id, 0) != 0 AND NVL(v_LocHistryResultingPrtId, 0) = 0 
 THEN
 UPDATE LocHistryTbl p
 SET LocHistryResultingPrtId = (SELECT p.PrtId
 from partner.PrtTbl p
 WHERE p.INST_ID = v_resulting_inst_id
 AND LocHistryAffctPrtId = v_LocHistryAffctPrtId
 AND LocId = v_LocId
 AND LocHistryEffDt = v_LocHistryEffDt
 AND ValidChngCd = v_ValidChngCd
 AND LocHistryPartyTyp = v_LocHistryPartyTyp
 AND LocHistryInitPrtId = v_LocHistryInitPrtId);
 END IF;
 
 END;
 END LOOP;
 END FixLocHistryCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


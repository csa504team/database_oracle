<!--- Saved 05/05/2015 13:18:34. --->
PROCEDURE PRTLOCSBACNTCTSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := NULL,
 p_PrtLocSBACntctSeqNmb NUMBER := NULL,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_PrtLocSBACntctPrimScndryInd CHAR := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocSBACntctSeqNmb,
 EmployeeId,
 PrtLocSBACntctTypCd,
 PrtLocSBACntctPrimScndryInd
 FROM PrtLocSBACntctTbl 
 WHERE LocId = p_LocId 
 and PrtLocSBACntctSeqNmb = p_PrtLocSBACntctSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocSBACntctTbl 
 WHERE LocId = p_LocId 
 and PrtLocSBACntctSeqNmb = p_PrtLocSBACntctSeqNmb;
 p_RetVal := SQl%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 11 THEN
 /* Select all the contacts for a given location based on Contact Type
 If contact type is not supplied all the contacts need to be retrieved */
 BEGIN
 OPEN p_SelCur FOR
 SELECT l.LocId,
 l.PrtLocSBACntctSeqNmb,
 l.EmployeeId,
 l.PrtLocSBACntctTypCd,
 ct.PrtLocSBACntctDescTxt,
 l.PrtLocSBACntctPrimScndryInd,
 e.IMUSERFIRSTNM,
 e.IMUSERLASTNM,
 e.IMUSERMIDNM,
 e.SBAOFCCD
 FROM PrtLocSBACntctTbl l, PrtLocSBACntctTypTbl ct, SECURITY.IMUSERTBL e 
 WHERE l.LocId = p_LocId 
 and l.PrtLocSBACntctTypCd = NVL(p_PrtLocSBACntctTypCd,l.PrtLocSBACntctTypCd) 
 and l.PrtLocSBACntctTypCd = ct.PrtLocSBACntctTypCd 
 and l.EmployeeId = e.IMUSERID 
 ORDER BY l.PrtLocSBACntctPrimScndryInd , e.IMUSERLASTNM ,e.IMUSERFIRSTNM ;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctSel;
 RAISE;
 END PRTLOCSBACNTCTSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


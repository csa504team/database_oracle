<!--- Saved 05/05/2015 13:18:43. --->
PROCEDURE PRTLOCSRCHSELCSP(
 p_userName VARCHAR2 := NULL,
 p_StateCode VARCHAR2 := NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PRTLOCSRCHSELCSPl;
 OPEN p_SelCur FOR
 SELECT PrtId,
 PrtLocNm,
 ValidLocTyp,
 PhyAddrStr1Txt,
 PhyAddrCtyNm,
 PhyAddrStCd,
 PhyAddrPostCd,
 LocId,
 PrtLocFIRSNmb,
 PrtLocCurStatCd,
 PrtLocOpndDt,
 PrtLocInactDt,
 PrtLocFIRSRecrdTyp,
 PhyAddrStr2Txt
 FROM LocSrchTbl 
 WHERE INSTR(p_StateCode,PhyAddrStCd) > 0 
 ORDER BY PrtLocNm , LocId ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCSRCHSELCSPL;
 RAISE;
 END PRTLOCSRCHSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


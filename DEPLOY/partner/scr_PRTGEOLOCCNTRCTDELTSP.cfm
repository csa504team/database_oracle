<!--- Saved 05/05/2015 13:17:37. --->
PROCEDURE PRTGEOLOCCNTRCTDELTSP
 (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0 ,
 p_PrtCntctNmb NUMBER := 0
 )
 
 AS
 /* PSP -- 11/12/2014 -- To delete all the records from the PARTNER.PRTGEOLOCCNTRCTTBL table based on LocId and PrtCntctNmb */
 BEGIN
 
 SAVEPOINT PRTGEOLOCCNTRCTDELTSP;
 
 IF p_Identifier = 0 
 THEN
 
 BEGIN
 DELETE FROM PARTNER.PRTGEOLOCCNTRCTTBL 
 WHERE LocId = p_Locid
 AND PrtCntctNmb = p_PrtCntctNmb;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 
 END IF;
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTGEOLOCCNTRCTDELTSP;
 RAISE;
 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


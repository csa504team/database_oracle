<!--- Saved 05/05/2015 13:17:59. --->
PROCEDURE PRTLOCCLSFUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtLocClsfSeqNmb NUMBER := 0,
 p_ValidPrtLocClsfCd NUMBER := 0,
 p_PrtLocClsfCreatUserId VARCHAR2 := NULL)
 AS
 error NUMBER(10,0);
 BEGIN
 SAVEPOINT PrtLocClsfUpd;
 IF p_Identifier = 0 THEN
 /* Update into PrtLocClsfTbl Table */
 BEGIN
 UPDATE PARTNER.PrtLocClsfTbl
 SET ValidPrtLocClsfCd = p_ValidPrtLocClsfCd, 
 LASTUPDTUSERID = p_PrtLocClsfCreatUserId, 
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId 
 AND PrtLocClsfSeqNmb = p_PrtLocClsfSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocClsfUpd;
 RAISE;
 END PRTLOCCLSFUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


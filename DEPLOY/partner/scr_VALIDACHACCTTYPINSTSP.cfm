<!--- Saved 05/05/2015 13:19:33. --->
PROCEDURE VALIDACHACCTTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidACHAcctTyp VARCHAR2 :=NULL,
 p_ValidACHAcctTypDesc VARCHAR2 :=NULL,
 p_ValidACHAcctTypDispOrdNmb NUMBER:=0,
 p_ValidACHAcctTypCreatUserId VARCHAR2 :=NULL
 
 )
 AS
 BEGIN
 SAVEPOINT ValidACHAcctTypIns;
 IF p_Identifier= 0 THEN
 /* Insert into Action Table */
 BEGIN
 INSERT INTO ValidACHAcctTypTbl 
 (
 ValidACHAcctTyp, 
 ValidACHAcctTypDesc, 
 ValidACHAcctTypDispOrdNmb, 
 ValidACHAcctTypCreatUserId
 )
 VALUES (
 p_ValidACHAcctTyp, 
 p_ValidACHAcctTypDesc, 
 p_ValidACHAcctTypDispOrdNmb, 
 p_ValidACHAcctTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDACHACCTTYPINSTSP;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:49. --->
PROCEDURE VALIDOWNDPRTTYPSELCSP
 (
 p_userName VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN
 OPEN p_SelCur FOR
 SELECT 
 ValidOwndPrtTyp, 
 ValidOwndPrtTypDesc
 FROM ValidOwndPrtTypTbl 
 WHERE ValidOwndPrtTypDispOrdNmb != 0 
 ORDER BY ValidOwndPrtTyp ;
 p_RetVal := SQL%ROWCOUNT;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 --ROLLBACK TO VALIDOWNDPRTTYPSELCSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


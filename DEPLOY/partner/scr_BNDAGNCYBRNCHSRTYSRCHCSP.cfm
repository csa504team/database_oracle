<!--- Saved 05/05/2015 13:15:50. --->
PROCEDURE BNDAGNCYBRNCHSRTYSRCHCSP(
 p_Identifier IN NUMBER := 0 ,
 p_RetVal OUT NUMBER ,
 p_PrtLglNm IN VARCHAR2 := NULL ,
 p_PrtAliasNm IN VARCHAR2 := NULL ,
 p_LocId IN NUMBER := 0 ,
 p_PhyAddrStCd IN VARCHAR2 := NULL ,
 p_PrtCurStatCd IN CHAR := NULL ,
 p_PrtLglNmSrchTyp IN NUMBER := 0 ,
 p_ValidLocTyp IN VARCHAR2 := NULL ,
 p_ValidPrtSubCatTyp IN VARCHAR2 := NULL ,
 p_SelCursor1 OUT SYS_REFCURSOR
 )
 AS
 v_x CHAR;
 v_y CHAR;
 v_z CHAR;
 v_cnt number(10);
 v_LicensingStCd CHAR(2);
 v_LocId number(7);
 v_LocId1 number(7);
 v_LicensingStCd1 VARCHAR2(255);
 CURSOR procur IS SELECT DISTINCT LocId, LicensingStCd FROM partner.BndAgntLicensngTbl 
 WHERE LocId IN (SELECT DISTINCT LocId FROM tmp1) 
 ORDER BY LocId, LicensingStCd;
 BEGIN
 /* End of Variable Declaration */
 delete tmp1;
 delete tt_tmp2;
 v_LocId := p_LocId;
 INSERT 
 INTO tmp1 
 SELECT DISTINCT pa.PrtAliasNm,
 l.PrtId,
 l.PrtLocNm,
 p.PrtLglNm,
 l.LocId,
 p.ValidPrtSubCatTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd,
 p.ValidPrtLglTyp,
 a.PhyAddrCntyNm,
 p.PrtCurStatCd,
 l.ValidLocTyp
 FROM partner.PrtAliasTbl pa, partner.PrtTbl p, partner.PrtLocTbl
 l, partner.PhyAddrTbl a, partner.ElctrncAddrTbl c 
 WHERE p.PrtId = pa.PrtId AND l.PrtId = pa.PrtId AND a.LocId = l.LocId
 AND a.LocId = c.LocId 
 AND pa.ValidPrtAliasTyp = 'TIN'
 AND upper(p.ValidPrtSubCatTyp) = upper(p_ValidPrtSubCatTyp)
 AND l.ValidLocTyp = NVL(l.ValidLocTyp,p_ValidLocTyp)
 --AND p.PrtCurStatCd = 'O'
 AND UPPER(p.PrtLglNm) LIKE (CASE WHEN p_PrtLglNmSrchTyp = 1 THEN NVL(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm)) || '%'
 WHEN p_PrtLglNmSrchTyp = 2 THEN '%' || NVL(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm)) || '%'
 WHEN p_PrtLglNmSrchTyp = 3 THEN NVL(UPPER(p_PrtLglNm), UPPER(p.PrtLglNm))
 ELSE UPPER(p.PrtLglNm) END) 
 AND pa.PrtAliasNm LIKE NVL(p_PrtAliasNm, pa.PrtAliasNm) 
 AND l.LocId = NVL(v_LocId, l.LocId) 
 AND a.PhyAddrStCd = NVL(p_PhyAddrStCd, a.PhyAddrStCd)
 --AND l.PrtLocCurStatCd = 'O' 
 AND a.PrtCntctNmb IS NULL 
 AND p.PrtCurStatCd IN (CASE WHEN p_PrtCurStatCd = 'O' THEN 'O'
 WHEN p_PrtCurStatCd = 'C' THEN 'C'
 WHEN p_PrtCurStatCd = 'D' THEN 'D'
 WHEN p_PrtCurStatCd = 'A' THEN p.PrtCurStatCd ELSE p.PrtCurStatCd end);
 
 SELECT COUNT(DISTINCT to_char(LocId) || LicensingStCd) into v_cnt FROM partner.BndAgntLicensngTbl
 WHERE LocId IN (SELECT DISTINCT LocId FROM tmp1);
 
 OPEN procur;
 FETCH procur INTO v_LocId, v_LicensingStCd;
 v_LocId1 := v_LocId;
 v_LicensingStCd1 := v_LicensingStCd;
 LOOP
 FETCH procur INTO v_LocId, v_LicensingStCd;
 EXIT WHEN procur%NOTFOUND;
 IF v_LocId = v_LocId1
 THEN
 v_LicensingStCd1 := v_LicensingStCd1 || ',' || v_LicensingStCd;
 ELSE
 BEGIN
 INSERT INTO tt_tmp2 VALUES (v_LocId1, v_LicensingStCd1);
 v_LocId1 := v_LocId;
 v_LicensingStCd1 := v_LicensingStCd ;
 END;
 END IF;
 END LOOP;
 INSERT INTO tt_tmp2 VALUES (v_LocId1, v_LicensingStCd1);
 OPEN p_SelCursor1 FOR
 SELECT a.PrtAliasNm,
 a.PrtId,
 a.PrtLocNm,
 UPPER(a.PrtLglNm) PrtLglNm,
 a.LocId,
 a.ValidPrtSubCatTyp,
 a.PhyAddrStr1Txt,
 a.PhyAddrStr2Txt,
 a.PhyAddrCtyNm,
 a.PhyAddrStCd,
 a.PhyAddrPostCd,
 b.LicensingStCd,
 a.ValidPrtLglTyp,
 a.PhyAddrCntyNm,
 a.PrtCurStatCd
 FROM tmp1 a, tt_tmp2 b
 WHERE a.LocId = b.LocId(+)
 AND a.ValidLocTyp = p_ValidLocTyp
 ORDER BY UPPER(a.PrtLglNm) ASC;
 
 p_RetVal := SQL%ROWCOUNT; 
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


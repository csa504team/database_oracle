<!--- Saved 05/05/2015 13:17:02. --->
PROCEDURE PRTAGRMTTERMRSNCDUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtTermRsnCd NUMBER :=0,
 p_PrtAgrmtTermRsnDispOrdNmb NUMBER := 0,
 p_PrtAgrmtTermRsnDescTxt VARCHAR2 := NULL,
 p_PrtAgrmtTermRsnCatCd NUMBER := 0,
 p_PrtAgrmtTermRsnCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtAgrmtTermRsnCdUpd;
 
 IF p_Identifier = 0 THEN
 /* Updert into PrtAgrmtTermRsnCd Table */
 BEGIN
 UPDATE PARTNER.PrtAgrmtTermRsnCdTbl
 SET PrtAgrmtTermRsnCd = p_PrtAgrmtTermRsnCd, 
 PrtAgrmtTermRsnDispOrdNmb = p_PrtAgrmtTermRsnDispOrdNmb, 
 PrtAgrmtTermRsnDescTxt = p_PrtAgrmtTermRsnDescTxt, 
 PrtAgrmtTermRsnCreatUserId = p_PrtAgrmtTermRsnCreatUserId,
 PrtAgrmtTermRsnCreatDt = SYSDATE
 WHERE PrtAgrmtTermRsnCd = p_PrtAgrmtTermRsnCd; 
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtIns;
 RAISE; 
 END PRTAGRMTTERMRSNCDUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:18:38. --->
PROCEDURE PRTLOCSBACNTCTTYPUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtLocSBACntctTypCd NUMBER := NULL,
 p_PrtLocSBACntctDescTxt VARCHAR2 := NULL,
 p_PrtLocSBACntctTypCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocSBACntctTypUpd;
 IF p_Identifier = 0 THEN
 /* Update of PrtLocSBACntctTyp Table */
 BEGIN
 UPDATE PARTNER.PrtLocSBACntctTypTbl
 SET PrtLocSBACntctDescTxt = p_PrtLocSBACntctDescTxt, 
 lastupdtuserid = p_PrtLocSBACntctTypCreatUserId, 
 lastupdtdt = SYSDATE
 WHERE PrtLocSBACntctTypCd = p_PrtLocSBACntctTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocSBACntctTypUpd;
 RAISE;
 
 END PRTLOCSBACNTCTTYPUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


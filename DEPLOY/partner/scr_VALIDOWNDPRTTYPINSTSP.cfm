<!--- Saved 05/05/2015 13:19:49. --->
PROCEDURE VALIDOWNDPRTTYPINSTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidOwndPrtTyp VARCHAR2 :=NULL,
 p_ValidOwndPrtTypDispOrdNmb NUMBER:=0,
 p_ValidOwndPrtTypDesc VARCHAR2 :=NULL,
 p_ValidOwndPrtTypCreatUserId VARCHAR2 :=NULL
 )
 AS
 BEGIN
 SAVEPOINT ValidOwndPrtTypIns;
 
 IF p_Identifier= 0 THEN
 BEGIN
 INSERT INTO ValidOwndPrtTypTbl 
 (
 ValidOwndPrtTyp, 
 ValidOwndPrtTypDispOrdNmb, 
 ValidOwndPrtTypDesc, 
 ValidOwndPrtTypCreatUserId
 )
 VALUES (
 p_ValidOwndPrtTyp, 
 p_ValidOwndPrtTypDispOrdNmb, 
 p_ValidOwndPrtTypDesc, 
 p_ValidOwndPrtTypCreatUserId
 );
 p_RetVal := SQL%ROWCOUNT;
 commit;
 END; 
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO VALIDOWNDPRTTYPINS;
 RAISE;
 END ; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


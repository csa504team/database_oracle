<!--- Saved 05/05/2015 13:19:57. --->
PROCEDURE VALIDPRTDISBTYPSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtDisbTyp VARCHAR2 :=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 
 
 SAVEPOINT ValidPrtDisbTypSel;
 
 IF p_Identifier= 0 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 
 ValidPrtDisbTyp, 
 ValidPrtDisbTypDesc, 
 ValidPrtDisbTypDispOrdNmb
 FROM ValidPrtDisbTypTbl 
 WHERE ValidPrtDisbTyp = p_ValidPrtDisbTyp;
 p_RetVal := SQL%ROWCOUNT; 
 end;
 ELSIF p_Identifier= 2 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 1
 FROM ValidPrtDisbTypTbl 
 WHERE ValidPrtDisbTyp = p_ValidPrtDisbTyp;
 
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTDISBTYPSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


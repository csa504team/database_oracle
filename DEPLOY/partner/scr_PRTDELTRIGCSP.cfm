<!--- Saved 05/05/2015 13:17:33. --->
PROCEDURE PrtDelTrigCSP (
 OldDataRec PrtTbl%ROWTYPE
 )
 AS
 -- Object : Trigger
 -- Date : August 29, 2000
 -- Programmer : Rajeswari DSP
 -- Company : AQUAS, Inc.
 --This trigger deletes the corresponding record from PrtSrchTbl table.
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_PartnerId number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_INST_ID := 0;
 v_PartnerId := OldDataRec.PrtId;
 
 DELETE partner.PrtSrchTbl
 where PrtId = v_PartnerId;
 
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ReplicationErrorMessageTxt := NULL;
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 v_ErrorNmb := 30008;
 v_ReplicationErrorMessageTxt := 'Unable to delete partner from PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END;
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


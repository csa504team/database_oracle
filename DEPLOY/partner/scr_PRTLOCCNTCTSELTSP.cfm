<!--- Saved 01/23/2018 15:32:01. --->
PROCEDURE PRTLOCCNTCTSELTSP (
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 P_LocId NUMBER := 0,
 P_PrtCntctNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /*
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtCntctDataSourcId
 */
 BEGIN
 SAVEPOINT PrtLocCntctSel;
 
 IF p_Identifier = 0
 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtCntctNmb,
 PrtCntctFirstNm,
 PrtCntctJobTitlNm,
 PrtCntctLastNm,
 PrtCntctInitialNm,
 PrtCntctPrefixNm,
 PrtCntctSfxNm,
 PrtLocCntctMailStopNm,
 PERSON_ID,
 INST_ID,
 LOC_ID
 FROM PrtLocCntctTbl
 WHERE LocId = P_LocId AND PrtCntctNmb = P_PrtCntctNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 2
 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1
 FROM PrtLocCntctTbl
 WHERE LocId = P_LocId AND PrtCntctNmb = P_PrtCntctNmb;
 
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK TO PrtLocCntctSeL;
 RAISE;
 END PRTLOCCNTCTSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


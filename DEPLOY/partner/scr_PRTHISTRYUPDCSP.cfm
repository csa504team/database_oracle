<!--- Saved 05/05/2015 13:17:46. --->
PROCEDURE PRTHISTRYUPDCSP 
 AS
 begin
 SAVEPOINT PrtHistryUpd;
 UPDATE PARTNER.PrtHistryTbl 
 set PrtHistryInitPrtId = (select p1.PrtId FROM THOMSON.INSTITUTION_HISTORY t, PARTNER.PRTHISTRYTBL p,
 PARTNER.PRTTBL P1
 WHERE t.affected_inst_id = p.AFFECTED_INST_ID
 AND t.effective_date =p.EFFECTIVE_DATE
 AND t.party_type_indicator=p.PARTY_TYPE_INDICATOR
 AND t.change_code =p.CHANGE_CODE
 AND t.initiator_inst_id is NOT NULL
 and (p.PrtHistryInitPrtId is null or PrtHistryInitPrtId = 0)
 AND t.initiator_inst_id = p1.INST_ID);
 
 /* if @error != 0
 begin
 if @@trancount > 0 rollback tran CPrtHistryUpd
 Print "problem updating PrtHistryTbl.PrtHistryInitPrtId"
 return @error
 end */
 UPDATE PrtHistryTbl 
 set PrtHistryResultingPrtId = (select p1.PrtId FROM THOMSON.INSTITUTION_HISTORY t, PARTNER.PrtHistryTbl p,
 PARTNER.PrtTbl p1
 WHERE t.affected_inst_id = p.AFFECTED_INST_ID
 AND t.effective_date =p.EFFECTIVE_DATE
 AND t.party_type_indicator=p.PARTY_TYPE_INDICATOR
 AND t.change_code =p.CHANGE_CODE
 AND t.resulting_inst_id is NOT NULL
 and (p.PrtHistryResultingPrtId is null or PrtHistryResultingPrtId = 0)
 AND t.resulting_inst_id = p1.INST_ID);
 -- select @error = @@error
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtHistryUpd;
 RAISE;
 END PrtHistryUpdCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 01/23/2018 15:32:01. --->
PROCEDURE PRTLOCCNTCTDELCSP (
 p_Identifier NUMBER := NULL,
 p_LocId NUMBER := NULL,
 --p_RetVal OUT NUMBER,
 p_PrtCntctNmb NUMBER := NULL)
 AS
 /*PSP : 11/14/2014 : Modified for: If we delete a partner contact and if they had a LINC Contact role we need to call the stored procedure (PRTGEOLOCCNTRCTDELTSP) that deletes the areas of operation
 RY--01/23/18--OPSMDEV1335:: Removed the references of PrtCntctDataSourcId */
 error NUMBER (10, 0);
 v_cnt NUMBER (11);
 v_rowcnt EXCEPTION;
 PhyAddrOldDataRec PhyAddrTbl%ROWTYPE;
 PrtLocCntctPosOldDataRec PrtLocCntctPosTbl%ROWTYPE;
 v_PrtCntctPosTypCd NUMBER (3);
 v_LincCount NUMBER (10);
 v_RetVal NUMBER (5);
 
 CURSOR CntctPosCur
 IS
 SELECT PrtCntctPosTypCd
 FROM PrtLocCntctPosTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 BEGIN
 --BEGIN
 --SAVEPOINT PrtLocCntctDel;
 IF p_Identifier = 1
 THEN
 BEGIN
 -- get old snapshot
 BEGIN
 SELECT *
 INTO PhyAddrOldDataRec
 FROM PhyAddrTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PhyAddrOldDataRec := NULL;
 END;
 
 DELETE PhyAddrTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 
 --call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (PhyAddrOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 DELETE ElctrncAddrTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrDelTrigCSP (p_LocId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 -- Delete PrtGeoLocCntrctTbl
 SELECT COUNT (*)
 INTO v_LincCount
 FROM PrtLocCntctSBARoleTbl
 WHERE LocId = p_LocId
 AND PrtCntctNmb = p_PrtCntctNmb
 AND PrtCntctSBARoleTypCd = 17; --checks for LINC contact role
 
 IF v_LincCount > 0
 THEN -- Deletes LINC area of operations if there is a LINC contact role
 PARTNER.PRTGEOLOCCNTRCTDELTSP (p_Identifier => 0,
 p_RetVal => v_RetVal,
 p_LocId => p_LocId,
 p_PrtCntctNmb => p_PrtCntctNmb);
 END IF;
 
 
 DELETE PrtLocCntctSBARoleTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 
 OPEN CntctPosCur;
 
 LOOP
 FETCH CntctPosCur INTO v_PrtCntctPosTypCd;
 
 EXIT WHEN CntctPosCur%NOTFOUND;
 
 BEGIN
 -- get old snapshot
 BEGIN
 SELECT *
 INTO PrtLocCntctPosOldDataRec
 FROM PrtLocCntctPosTbl
 WHERE LocId = p_LocId
 AND PrtCntctNmb = p_PrtCntctNmb
 AND PrtCntctPosTypCd = v_PrtCntctPosTypCd;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocCntctPosOldDataRec := NULL;
 END;
 
 DELETE PrtLocCntctPosTbl
 WHERE LocId = p_LocId
 AND PrtCntctNmb = p_PrtCntctNmb
 AND PrtCntctPosTypCd = v_PrtCntctPosTypCd;
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctPosDelTrigCSP (PrtLocCntctPosOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 END;
 END LOOP;
 
 CLOSE CntctPosCur;
 
 DELETE PrtLocCntctTbl
 WHERE LocId = p_LocId AND PrtCntctNmb = p_PrtCntctNmb;
 END;
 ELSIF p_Identifier = 2
 THEN
 BEGIN
 -- delete PhyAddrTbl
 BEGIN
 SELECT a.*
 INTO PhyAddrOldDataRec
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.ROWID =
 (SELECT MIN (a.ROWID)
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PhyAddrOldDataRec := NULL;
 END;
 
 DELETE PARTNER.PhyAddrTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (PhyAddrOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 -- Delete ElctrncAddrTbl
 DELETE ElctrncAddrTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM ElctrncAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrDelTrigCSP (p_LocId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 -- Delete PrtGeoLocCntrctTbl
 SELECT COUNT (*)
 INTO v_LincCount
 FROM PrtLocCntctSBARoleTbl
 WHERE LocId = p_LocId
 AND PrtCntctNmb = p_PrtCntctNmb
 AND PrtCntctSBARoleTypCd = 17; --checks for LINC contact role
 
 IF v_LincCount > 0
 THEN -- Deletes LINC area of operations if there is a LINC contact role
 PARTNER.PRTGEOLOCCNTRCTDELTSP (p_Identifier => 0,
 p_RetVal => v_RetVal,
 p_LocId => p_LocId,
 p_PrtCntctNmb => p_PrtCntctNmb);
 END IF;
 
 -- Delete PrtLocCntctSBARoleTbl
 DELETE PrtLocCntctSBARoleTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PrtLocCntctSBARoleTbl a,
 PrtLocCntctTbl b,
 PARTNER.PrtLocCntctSBARoleTbl
 WHERE a.LocId = p_LocId
 AND b.LocId = p_LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- get old snapshot
 BEGIN
 SELECT a.*
 INTO PrtLocCntctPosOldDataRec
 FROM PrtLocCntctPosTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocCntctPosOldDataRec := NULL;
 END;
 
 DELETE PrtLocCntctPosTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PrtLocCntctPosTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctPosDelTrigCSP (PrtLocCntctPosOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 DELETE PrtLocCntctTbl
 WHERE LocId = p_LocId;
 END;
 ELSIF p_Identifier = 3
 THEN
 BEGIN
 -- delete PhyAddrTbl
 BEGIN
 SELECT a.*
 INTO PhyAddrOldDataRec
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.ROWID =
 (SELECT MIN (a.ROWID)
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PhyAddrOldDataRec := NULL;
 END;
 
 DELETE PhyAddrTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PhyAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PhyAddrDelTrigCSP (PhyAddrOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 -- delete ElctrncAddrTbl
 DELETE ElctrncAddrTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM ElctrncAddrTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.ElctrncAddrDataSourcId !=
 'TFP-Replication');
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 ElctrncAddrDelTrigCSP (p_LocId);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 -- Delete PrtGeoLocCntrctTbl
 SELECT COUNT (*)
 INTO v_LincCount
 FROM PrtLocCntctSBARoleTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PrtLocCntctSBARoleTbl a,
 PrtLocCntctTbl b,
 PARTNER.PrtLocCntctSBARoleTbl
 WHERE a.LocId = p_LocId
 AND b.LocId = p_LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.PrtCntctSBARoleTypCd = 17);
 
 --checks for LINC contact role
 
 IF v_LincCount > 0
 THEN -- Deletes LINC area of operations if there is a LINC contact role
 PARTNER.PRTGEOLOCCNTRCTDELTSP (p_Identifier => 0,
 p_RetVal => v_RetVal,
 p_LocId => p_LocId,
 p_PrtCntctNmb => p_PrtCntctNmb);
 END IF;
 
 DELETE PrtLocCntctSBARoleTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PrtLocCntctSBARoleTbl a,
 PrtLocCntctTbl b,
 PARTNER.PrtLocCntctSBARoleTbl
 WHERE a.LocId = p_LocId
 AND b.LocId = p_LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb);
 
 -- get old snapshot
 BEGIN
 SELECT a.*
 INTO PrtLocCntctPosOldDataRec
 FROM PrtLocCntctPosTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.DEPARTMENT IS NULL;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 PrtLocCntctPosOldDataRec := NULL;
 END;
 
 DELETE PrtLocCntctPosTbl
 WHERE ROWID IN (SELECT a.ROWID
 FROM PrtLocCntctPosTbl a, PrtLocCntctTbl b
 WHERE a.LocId = p_LocId
 AND a.LocId = b.LocId
 AND a.PrtCntctNmb = b.PrtCntctNmb
 AND a.DEPARTMENT IS NULL);
 
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtLocCntctPosDelTrigCSP (PrtLocCntctPosOldDataRec);
 
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 DELETE PrtLocCntctTbl
 WHERE LocId = p_LocId;
 -- p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 EXCEPTION
 --WHEN v_rowcnt THEN
 -- raise_application_error (-20001,'Multi-row action not allowed.');
 WHEN OTHERS
 THEN
 --ROLLBACK TO PRTLOCCNTCTDEL;
 RAISE;
 END PRTLOCCNTCTDELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


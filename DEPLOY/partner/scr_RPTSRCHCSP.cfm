<!--- Saved 01/25/2018 10:38:00. --->
PROCEDURE rptsrchcsp (
 p_subcategoryTyp VARCHAR2 := NULL,
 p_stateCd VARCHAR2 := NULL,
 p_officeCd VARCHAR2 := NULL,
 p_regionCd VARCHAR2 := NULL,
 p_congDistCd VARCHAR2 := NULL,
 p_programId VARCHAR2 := NULL,
 p_sbaCntctRole VARCHAR2 := NULL,
 p_prtPosition VARCHAR2 := NULL,
 p_categoryTyp VARCHAR2 := NULL,
 p_reportType VARCHAR2 := NULL,
 p_startDate DATE := NULL,
 p_EndDate DATE := NULL,
 p_cntyCd VARCHAR2 := NULL,
 p_selcur OUT SYS_REFCURSOR,
 p_retval OUT NUMBER)
 AS
 /*
 RY--01/24/2018--OPSMDEV-1335:: Removed the references of PrtCntctDataSourcId,PhyAddrDataSourcId
 */
 v_regionCd VARCHAR2 (10);
 BEGIN
 IF p_reportType = '1-Mailing'
 OR p_reportType = '1-Email'
 OR p_reportType = '3'
 THEN
 BEGIN
 IF p_regionCd IS NOT NULL
 THEN
 v_regionCd := p_regionCd || '%';
 ELSE
 v_regionCd := p_regionCd;
 END IF;
 
 OPEN p_selcur FOR
 SELECT DISTINCT p.PrtId,
 p.ValidPrtSubCatTyp,
 c.PrtCntctPrefixNm,
 c.PrtCntctFirstNm,
 c.PrtCntctInitialNm,
 c.PrtCntctLastNm,
 c.PrtCntctJobTitlNm,
 p.PrtLglNm,
 pa.PhyAddrStr1Txt AS pStreet,
 pa.PhyAddrStr2Txt AS pStreet1,
 pa.PhyAddrCtyNm AS pCity,
 pa.PhyAddrStCd AS pState,
 pa.PhyAddrPostCd AS pZip,
 ma.PhyAddrStr1Txt AS mStreet,
 ma.PhyAddrStr2Txt AS mStreet1,
 ma.PhyAddrCtyNm AS mCity,
 ma.PhyAddrStCd AS mState,
 ma.PhyAddrPostCd AS mZip,
 sa.PhyAddrStr1Txt AS sStreet,
 sa.PhyAddrStr2Txt AS sStreet1,
 sa.PhyAddrCtyNm AS sCity,
 sa.PhyAddrStCd AS sState,
 sa.PhyAddrPostCd AS sZip,
 c.LocId,
 c.PrtCntctNmb,
 ph.ElctrncAddrTxt AS Phone,
 fa.ElctrncAddrTxt AS Fax,
 em.ElctrncAddrTxt AS Email,
 vp.ValidPrtPrimCatTypDesc,
 s.PrtCntctSBARoleTypTxt,
 NULL AS PrtCntctPosTypTxt,
 county.OrignOfcCd,
 a.PrgrmId,
 a.PrtAgrmtEffDt,
 a.PrtAgrmtExprDt,
 l.PrtLocFIRSNmb
 FROM partner.PrtLocCntctSBARoleTbl r,
 partner.PrtLocTbl l,
 partner.PrtTbl p
 LEFT OUTER JOIN partner.PrtSrchTbl ps
 ON (p.PrtId = ps.PrtId)
 LEFT OUTER JOIN partner.PrtAgrmtTbl a
 ON (p.PrtId = a.PrtId),
 partner.PhyAddrTbl pa,
 partner.PrtLocCntctTbl c
 LEFT OUTER JOIN partner.PhyAddrTbl ma
 ON ( c.LocId = ma.LocId
 AND ma.ValidAddrTyp = 'Mail'
 AND ma.PrtCntctNmb IS NULL)
 LEFT OUTER JOIN
 (SELECT sa1.PhyAddrStr1Txt,
 sa1.PhyAddrStr2Txt,
 sa1.PhyAddrCtyNm,
 sa1.PhyAddrStCd,
 sa1.PhyAddrPostCd,
 sa1.LocId,
 sa1.PhyAddrDataSourcId,
 sa1.PrtCntctNmb
 FROM partner.PhyAddrTbl sa1
 WHERE sa1.PhyAddrSeqNmb =
 (SELECT MIN (a.PhyAddrSeqNmb)
 FROM partner.PhyAddrTbl a
 WHERE a.LocId = sa1.LocId
 AND a.PrtCntctNmb = sa1.PrtCntctNmb)) sa
 ON ( c.LocId = sa.LocId
 AND c.PrtCntctNmb = sa.PrtCntctNmb)
 LEFT OUTER JOIN partner.ElctrncAddrTbl ph
 ON ( c.LocId = ph.LocId
 AND c.PrtCntctNmb = ph.PrtCntctNmb
 AND ph.PrtCntctNmb IS NOT NULL
 AND ph.ValidElctrncAddrTyp = 'Phone')
 LEFT OUTER JOIN partner.ElctrncAddrTbl fa
 ON ( c.LocId = fa.LocId
 AND c.PrtCntctNmb = fa.PrtCntctNmb
 AND fa.PrtCntctNmb IS NOT NULL
 AND fa.ValidElctrncAddrTyp = 'Fax')
 LEFT OUTER JOIN partner.ElctrncAddrTbl em
 ON ( c.LocId = em.LocId
 AND c.PrtCntctNmb = em.PrtCntctNmb
 AND em.PrtCntctNmb IS NOT NULL
 AND em.ValidElctrncAddrTyp = 'Email'),
 partner.PrtCntctSBARoleTypTbl s,
 partner.ValidPrtPrimCatTbl vp,
 sbaref.CntyTbl county
 WHERE r.PrtCntctSBARoleTypCd IN (NVL (
 p_sbaCntctRole,
 r.PrtCntctSBARoleTypCd))
 AND r.LocId = c.LocId
 AND r.PrtCntctNmb = c.PrtCntctNmb
 AND r.LocId = l.LocId
 AND l.PrtId = p.PrtId
 AND p.PrtCurStatCd = 'O'
 AND l.ValidLocTyp = 'HO'
 AND a.PrgrmId IN (NVL (p_programId, a.PrgrmId))
 AND ( a.PrtAgrmtEffDt <= SYSDATE
 AND ( a.PrtAgrmtExprDt > SYSDATE
 OR a.PrtAgrmtExprDt IS NULL)
 AND ( a.PrtAgrmtTermDt > SYSDATE
 OR a.PrtAgrmtTermDt IS NULL))
 /* if (reportType.equals("3")) {
 // sqlParms3.append(" AND a.PrtAgrmtExprDt >= to_date('"+ startDate +"','MM/DD/YYYY') " +
 // "AND a.PrtAgrmtExprDt <= to_date('"+ endDate +"','MM/DD/YYYY') ");*/
 AND p.ValidPrtSubCatTyp IN (NVL (p_subcategoryTyp,
 p.ValidPrtSubCatTyp))
 AND p.ValidPrtPrimCatTyp IN (NVL (p_categoryTyp,
 p.ValidPrtPrimCatTyp))
 AND p.ValidPrtPrimCatTyp = vp.ValidPrtPrimCatTyp
 AND ( ( c.LocId = pa.LocId
 AND pa.ValidAddrTyp = 'Phys'
 AND pa.PrtCntctNmb IS NULL)
 AND CASE
 WHEN ( p_officeCd IS NOT NULL
 AND p_stateCd IS NOT NULL)
 AND pa.PhyAddrFIPSCntyCd IS NOT NULL
 AND pa.PhyAddrFIPSCntyCd IN (SELECT cnty.NumrcStCd
 || cnty.CntyCd
 FROM sbaref.CntyTbl cnty
 WHERE cnty.OrignOfcCd IN (p_officeCd)
 AND cnty.StCd IN (p_stateCd))
 THEN
 1
 WHEN ( p_officeCd IS NOT NULL
 AND p_stateCd IS NULL)
 AND pa.PhyAddrFIPSCntyCd IS NOT NULL
 AND pa.PhyAddrFIPSCntyCd IN (SELECT cnty.NumrcStCd
 || cnty.CntyCd
 FROM sbaref.CntyTbl cnty
 WHERE cnty.OrignOfcCd IN (p_officeCd))
 THEN
 1
 WHEN ( p_officeCd IS NULL
 AND p_stateCd IS NOT NULL)
 AND pa.PhyAddrFIPSCntyCd IS NOT NULL
 AND pa.PhyAddrFIPSCntyCd IN (SELECT cnty.NumrcStCd
 || cnty.CntyCd
 FROM sbaref.CntyTbl cnty
 WHERE cnty.StCd IN (p_stateCd))
 THEN
 1
 ELSE
 0
 END = 1)
 AND r.PrtCntctSBARoleTypCd = s.PrtCntctSBARoleTypCd
 AND pa.PhyAddrFIPSCntyCd =
 (county.NumrcStCd || county.CntyCd)
 AND county.OrignOfcCd LIKE
 (NVL (v_regionCd, county.OrignOfcCd))
 AND ps.CongrsnlDistNmb =
 NVL (p_congDistCd, ps.CongrsnlDistNmb);
 END;
 END IF;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


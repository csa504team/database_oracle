<!--- Saved 05/05/2015 13:19:52. --->
PROCEDURE VALIDOWNRPRTTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidOwnrPrtTyp VARCHAR2 :=NULL,
 p_ValidOwnrPrtTypDispOrdNmb NUMBER:=0,
 p_ValidOwnrPrtTypDesc VARCHAR2 :=NULL,
 p_ValidOwnrPrtTypCreatUserId VARCHAR2 :=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidOwnrPrtTypUpd;
 
 IF p_Identifier= 0 THEN
 BEGIN
 UPDATE ValidOwnrPrtTypTbl
 SET 
 ValidOwnrPrtTypDispOrdNmb =p_ValidOwnrPrtTypDispOrdNmb, 
 ValidOwnrPrtTypDesc = p_ValidOwnrPrtTypDesc, 
 ValidOwnrPrtTypCreatUserId = p_ValidOwnrPrtTypCreatUserId,
 ValidOwnrPrtTypCreatDt = SYSDATE
 where ValidOwnrPrtTyp = p_ValidOwnrPrtTyp;
 p_RetVal := SQL%ROWCOUNT;
 END; 
 end if;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDOWNRPRTTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


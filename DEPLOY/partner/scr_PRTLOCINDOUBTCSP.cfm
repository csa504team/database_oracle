<!--- Saved 05/05/2015 13:18:30. --->
PROCEDURE PRTLOCINDOUBTCSP (
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT PRTLOCINDOUBT; 
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocFIRSNmb,
 PrtLocFIRSRecrdTyp,
 PrtId,
 PrtLocNm,
 PrtLocCurStatCd,
 ValidLocTyp,
 PrtLocCreatUserId,
 PrtLocCreatDt
 FROM PrtLocTbl 
 WHERE PrtLocFIRSNmb = 'DELETED' 
 OR PrtLocCurStatCd = 'I'; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCINDOUBT;
 RAISE;
 
 END PRTLOCINDOUBTCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


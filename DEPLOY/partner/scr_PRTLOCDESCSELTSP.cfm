<!--- Saved 05/05/2015 13:18:17. --->
PROCEDURE PRTLOCDESCSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER DEFAULT 0,
 p_PrtLocDescTypSeqNmb NUMBER := 0,
 p_PrtLocDescSeqNmb NUMBER := 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtLocDescSel;
 IF p_Identifier = 0 THEN
 /* Select from PrtLocDesc Table */
 BEGIN
 OPEN p_SelCur FOR
 SELECT LocId,
 PrtLocDescTypSeqNmb,
 PrtLocDescSeqNmb,
 PrtLocDescTxt
 FROM PrtLocDescTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb 
 and PrtLocDescSeqNmb = p_PrtLocDescSeqNmb;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 FROM PrtLocDescTbl 
 WHERE LocId = p_LocId 
 and PrtLocDescTypSeqNmb = p_PrtLocDescTypSeqNmb 
 and PrtLocDescSeqNmb = p_PrtLocDescSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocDescSel;
 RAISE;
 
 END PRTLOCDESCSELTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:16:45. --->
PROCEDURE PRGRMSELCSP(
 userName IN VARCHAR2 DEFAULT NULL,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 
 OPEN p_SelCur FOR
 SELECT PrgrmId
 , PrgrmDesc
 , PrgrmDocReqdInd
 , PrgrmEffDt
 , PrgrmFormId
 , ValidPrgrmTyp
 , PrgrmAreaOfOperReqdInd
 FROM PrgrmTbl 
 WHERE PrgrmDispOrdNmb != 0 
 ORDER BY PrgrmDispOrdNmb ;
 
 p_RetVal := SQL%ROWCOUNT;
 
 
 
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 
 END PRGRMSELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


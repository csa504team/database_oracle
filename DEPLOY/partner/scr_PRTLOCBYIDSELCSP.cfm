<!--- Saved 05/05/2015 13:17:53. --->
PROCEDURE PRTLOCBYIDSELCSP(
 p_userName VARCHAR2 := NULL,
 p_LocId NUMBER :=0,
 p_RetVal OUT NUMBER,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 /* SB -- 09/15/2014 -- Changed the Select Logic as per Ian's email */
 BEGIN
 SAVEPOINT PRTLOCBYIDSEL; 
 OPEN p_SelCur FOR
 SELECT l.LocId,
 l.PrtId,
 l.ValidLocTyp,
 l.PrtLocNm,
 l.PrtLocCurStatCd,
 l.PrtLocOpndDt,
 l.PrtLocInactDt,
 l.PrtLocFIRSNmb,
 l.PrtLocFIRSRecrdTyp,
 l.LOC_ID,
 l.INST_ID,
 ls.RolePrivilegePrgrmId,
 l.PrtLocOperHrsTxt,
 vl.VALIDLOCTYPDESC,
 vs.CURDESCTXT
 FROM partner.PrtLocTbl l, partner.LocSrchTbl ls , PARTNER.VALIDCURSTATCDTBL vs,PARTNER.VALIDLOCTYPTBL vl
 WHERE l.LocId = ls.LocId 
 AND l.LocId =p_LocId
 AND l.PRTLOCCURSTATCD=vs.CURSTATCD
 AND l.VALIDLOCTYP=vl.VALIDLOCTYP;
 
 /* SELECT l.LocId,
 l.PrtId,
 l.ValidLocTyp,
 l.PrtLocNm,
 l.PrtLocCurStatCd,
 l.PrtLocOpndDt,
 l.PrtLocInactDt,
 l.PrtLocFIRSNmb,
 l.PrtLocFIRSRecrdTyp,
 l.LOC_ID,
 l.INST_ID,
 ls.RolePrivilegePrgrmId,
 l.PrtLocOperHrsTxt
 FROM PrtLocTbl l, LocSrchTbl ls 
 WHERE l.LocId = ls.LocId 
 AND l.LocId =p_LocId; */
 p_RetVal := SQL%ROWCOUNT; 
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCBYIDSEL;
 RAISE;
 END PRTLOCBYIDSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


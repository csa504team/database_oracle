<!--- Saved 05/05/2015 13:18:05. --->
PROCEDURE PRTLOCCNTCTPOSUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER := 0,
 p_PrtCntctNmb NUMBER := 0,
 p_PrtCntctPosTypCd NUMBER := 0,
 p_DEPARTMENT VARCHAR2 := NULL,
 p_PrtLocCntctPosCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocCntctPosUpd;
 IF p_Identifier = 0 THEN
 /* Updert into PrtLocCntctPos Table */
 BEGIN
 UPDATE PARTNER.PrtLocCntctPosTbl
 SET DEPARTMENT = p_DEPARTMENT, 
 LASTUPDTUSERID = p_PrtLocCntctPosCreatUserId, 
 LASTUPDTDT = SYSDATE
 WHERE LocId = p_LocId 
 and PrtCntctNmb = p_PrtCntctNmb 
 and PrtCntctPosTypCd = p_PrtCntctPosTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtLocCntctPosUpd;
 RAISE;
 
 END PRTLOCCNTCTPOSUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


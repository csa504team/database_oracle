<!--- Saved 05/05/2015 13:18:49. --->
PROCEDURE PRTNOTECNTNTUPDTSP(
 p_Identifier IN NUMBER := 0,
 p_RetVal IN OUT NUMBER,
 p_LocId IN NUMBER := 0,
 p_PrtNoteSeqNmb IN NUMBER := 0,
 p_PrtNoteCntntSeqNmb IN NUMBER := 0,
 p_PrtNoteCntntTxt IN VARCHAR2 := NULL,
 p_PrtNoteCntntCreatUserId IN VARCHAR2 := NULL)
 AS
 BEGIN
 SAVEPOINT PrtNoteCntntUpd;
 IF p_Identifier = 0 THEN
 /* Update of PrtNoteCntnt Table */
 BEGIN
 UPDATE PARTNER.PrtNoteCntntTbl
 SET LocId = p_LocId, 
 PrtNoteSeqNmb = p_PrtNoteSeqNmb, 
 PrtNoteCntntSeqNmb = p_PrtNoteCntntSeqNmb, 
 PrtNoteCntntTxt = p_PrtNoteCntntTxt, 
 lastupdtuserid = p_PrtNoteCntntCreatUserId,
 lastupdtdt = SYSDATE
 WHERE LocId = p_LocId 
 and PrtNoteSeqNmb = p_PrtNoteSeqNmb 
 and PrtNoteCntntSeqNmb = p_PrtNoteCntntSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtNoteCntntUpd;
 RAISE;
 
 END PRTNOTECNTNTUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


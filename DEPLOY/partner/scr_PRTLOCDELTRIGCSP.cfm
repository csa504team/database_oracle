<!--- Saved 05/05/2015 13:18:15. --->
PROCEDURE PrtLocDelTrigCSP (
 OldDataRec PrtLocTbl%ROWTYPE
 )
 AS
 -- Revision History -----------------------------------------
 -- 08/28/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 -- --------------------------------------------------------
 -- Deletes corresponding row in location search table and
 -- refreshes location data in partner search table.
 -- --------------------------------------------------------
 v_ErrorNmb number(5);
 v_FIRSNmb CHAR(7);
 v_FIRSRecordTyp CHAR(2);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_LocationTyp varchar2(5);
 v_PartnerId number(7);
 v_PrimaryTyp varchar2(5);
 v_ProcessingMode CHAR(1); -- 'N'-Normal or 'I'-Initial Load
 v_OutFIRSRecordTyp CHAR(2);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 v_SubcategoryTyp varchar2(5);
 
 -- Temporary variables
 v_cnt number(11);
 
 BEGIN
 v_Rowcount := SQL%ROWCOUNT;
 v_ErrorNmb := 0;
 v_INST_ID := 0;
 v_LocationId := OldDataRec.LocId;
 v_FIRSNmb := OldDataRec.PrtLocFIRSNmb;
 v_PartnerId := OldDataRec.PrtId;
 v_LocationTyp := OldDataRec.ValidLocTyp;
 v_LOC_ID := OldDataRec.LOC_ID;
 v_INST_ID := NVL(OldDataRec.INST_ID, 0);
 
 --Delete location data from location search table.
 DELETE LocSrchTbl WHERE LocId = v_LocationId;
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31285;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error Deleting LocSrchTbl row.' || ' LocId=' || to_char(v_LocationId);
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh location data in partner search table.
 PrtSrchUpdLocCSP (v_PartnerId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31286;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.';
 PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PrtLocDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


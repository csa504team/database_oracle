<!--- Saved 05/05/2015 13:18:03. --->
PROCEDURE PRTLOCCNTCTPOSINSTSP(
 p_Identifier NUMBER :=0,
 p_RetVal OUT NUMBER,
 p_LocId NUMBER :=0,
 p_PrtCntctNmb NUMBER :=0,
 p_PrtCntctPosTypCd NUMBER :=0,
 p_DEPARTMENT VARCHAR2 :=NULL,
 p_PrtLocCntctPosCreatUserId VARCHAR2 :=NULL)
 AS
 BEGIN
 SAVEPOINT PrtLocCntctPosIns;
 IF p_Identifier = 0 THEN
 BEGIN
 INSERT INTO PARTNER.PrtLocCntctPosTbl 
 (LocId, PrtCntctNmb, PrtCntctPosTypCd, DEPARTMENT,PrtLocCntctPosCreatUserId,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_LocId,p_PrtCntctNmb,p_PrtCntctPosTypCd,p_DEPARTMENT,p_PrtLocCntctPosCreatUserId,p_PrtLocCntctPosCreatUserId,SYSDATE);
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PRTLOCCNTCTPOSINSTSPP;
 RAISE;
 
 END PRTLOCCNTCTPOSINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


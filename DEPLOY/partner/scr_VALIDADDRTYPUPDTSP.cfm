<!--- Saved 05/05/2015 13:19:37. --->
PROCEDURE VALIDADDRTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_ValidAddrTyp VARCHAR2 :=NULL,
 p_ValidAddrTypDesc VARCHAR2 :=NULL,
 p_ValidAddrTypDispOrdNmb NUMBER:=0,
 p_ValidAddrTypCreatUserId VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN
 SAVEPOINT ValidAddrTypUpd;
 IF p_Identifier= 0 THEN
 /* Update IMAdrTyp Table */
 BEGIN
 UPDATE ValidAddrTypTbl
 SET ValidAddrTypDesc = p_ValidAddrTypDesc, 
 ValidAddrTypDispOrdNmb = p_ValidAddrTypDispOrdNmb, 
 ValidAddrTypCreatUserId = p_ValidAddrTypCreatUserId,
 ValidAddrTypCreatDt = SYSDATE
 WHERE ValidAddrTyp =p_ValidAddrTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 END IF ; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDADDRTYPUPDTSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


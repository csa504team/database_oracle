<!---
AUTHOR:				Generated by /cfincludes/dsp_gendbfiles.cfm, 
					which was written by Steve Seaquist, TMS, and 
					invoked (for this generation) by sherryLiu. 
DATE:				01/23/2018.
DESCRIPTION:		Standardized call to ELCTRNCADDRINSTSP.
NOTES:				Intended to be executed by CFINCLUDE.
INPUT:				Mandatory Variables: ErrMsg, TxnErr, Variables.db, Variables.dbtype, Variables.username, Variables.password.
					Optional  Variables: (defined in the CFPARAMs, below)
OUTPUT:				Variables: ErrMsg, TxnErr. Possible database changes.
REVISION HISTORY:	01/23/2018, SRS:	Original implementation. 
--->

<cfif	(Len (CGI.Script_Name) gt 48)
	and	(Left(CGI.Script_Name,    48) is "/cfincludes/oracle/partner/spc_ELCTRNCADDRINSTSP")>
	<!--- (Includes logged-in and public versions if SelCSP/SelTSP.) --->
	<cfset Variables.PageName  = ListLast(CGI.Script_Name,'/')>
	<cfset Variables.DirName   = Replace (CGI.Script_Name,'/'&Variables.PageName,'','One')>
	<cfdirectory directory="#ExpandPath(Variables.DirName)#"
	filter= "#Variables.PageName#" name= "getSelf"
	sort= "datelastmodified desc">
	<cfif NOT IsDefined("Request.SlafServerName")>
		<cfinclude template="/library/cfincludes/get_actual_server_name.cfm">
	</cfif>
	<cfoutput>
#Request.SlafServerName#:#CGI.Script_Name#<br/>generated #getSelf.datelastmodified#,<br/>#getSelf.size# bytes.
</cfoutput>
	<cfinclude template="/library/cfincludes/OnRequestEnd.cfm">
	<cfabort>
</cfif>

<cfparam name="Variables.DebugSpcs"					default="">
<cfparam name="Variables.ErrMsg"						default="">
<cfparam name="Variables.SkippedSpcs"					default="">
<cfparam name="Variables.TxnErr"						default="No">
<cfif Variables.TxnErr>
	<cfset Variables.SkippedSpcs						= ListAppend(Variables.SkippedSpcs, "ELCTRNCADDRINSTSP")>
<cfelse>
	<!--- Optional parameters to retrieve CFPROCRESULT result set: --->
	<cfparam name="Variables.cfprname"					default="Ignored"><!--- cfprocresult name      attribute --->
	<cfparam name="Variables.cfprset"					default="1">      <!--- cfprocresult resultset attribute --->
	<!--- If a 2-dimensional array 'cfpra' is defined, it overrides cfprname and cfprset. --->
	<!--- First column of cfpra is name, second column is resultset. Allows retrieving multiple result sets. --->
	<cfparam name="Variables.LogAct"					default="call ELCTRNCADDRINSTSP"><!--- "logical action" (of this call) --->
	<cfset Variables.SleEntityName						= "ELCTRNCADDRINSTSP">
	<cfparam name="Variables.Identifier"				default="0">
	<cfparam name="Variables.RetVal"					default="">
	<cfparam name="Variables.LocId"						default="">
	<cfparam name="Variables.ElctrncAddrNmb"			default="">
	<cfparam name="Variables.ValidElctrncAddrTyp"		default="">
	<cfparam name="Variables.PrtCntctNmb"				default="">
	<cfparam name="Variables.ElctrncAddrTxt"			default="">
	<cfparam name="Variables.LOC_ID"					default="">
	<cfparam name="Variables.DEPARTMENT"				default="">
	<cfparam name="Variables.PERSON_ID"					default="">
	<cfparam name="Variables.TYPE_CODE"					default="">
	<cfparam name="Variables.RANK"						default="">
	<cfparam name="Variables.INST_ID"					default="">
	<cfparam name="Variables.ElctrncAddrCreatUserId"	default="">
	<cftry>
		<cfstoredproc procedure="PARTNER.ELCTRNCADDRINSTSP" datasource="#Variables.db#"
											username="#Variables.username#" password="#Variables.password#">
		<cfif Len(Variables.Identifier) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_Identifier"
											value="#Variables.Identifier#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_Identifier"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfprocparam		type="Out"		dbvarname=":p_RetVal"
											variable="Variables.RetVal"
											cfsqltype="CF_SQL_NUMBER">
		<cfif Len(Variables.LocId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_LocId"
											value="#Variables.LocId#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_LocId"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.ElctrncAddrNmb) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrNmb"
											value="#Variables.ElctrncAddrNmb#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrNmb"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.ValidElctrncAddrTyp) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ValidElctrncAddrTyp"
											value="#Variables.ValidElctrncAddrTyp#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ValidElctrncAddrTyp"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PrtCntctNmb) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PrtCntctNmb"
											value="#Variables.PrtCntctNmb#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PrtCntctNmb"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.ElctrncAddrTxt) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrTxt"
											value="#Variables.ElctrncAddrTxt#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrTxt"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.LOC_ID) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_LOC_ID"
											value="#Variables.LOC_ID#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_LOC_ID"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.DEPARTMENT) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_DEPARTMENT"
											value="#Variables.DEPARTMENT#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_DEPARTMENT"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.PERSON_ID) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_PERSON_ID"
											value="#Variables.PERSON_ID#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_PERSON_ID"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.TYPE_CODE) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_TYPE_CODE"
											value="#Variables.TYPE_CODE#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_TYPE_CODE"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif Len(Variables.RANK) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_RANK"
											value="#Variables.RANK#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_RANK"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.INST_ID) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_INST_ID"
											value="#Variables.INST_ID#"
											cfsqltype="CF_SQL_NUMBER">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_INST_ID"
											null="Yes"
											cfsqltype="CF_SQL_NUMBER">
		</cfif>
		<cfif Len(Variables.ElctrncAddrCreatUserId) GT 0>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrCreatUserId"
											value="#Variables.ElctrncAddrCreatUserId#"
											cfsqltype="CF_SQL_VARCHAR2">
		<cfelse>
			<cfprocparam	type="In"		dbvarname=":p_ElctrncAddrCreatUserId"
											null="Yes"
											cfsqltype="CF_SQL_VARCHAR2">
		</cfif>
		<cfif IsDefined("Variables.cfpra") AND IsArray(Variables.cfpra)>
			<cfloop index="cfpridx" from="1" to="#ArrayLen(Variables.cfpra)#">
				<cfprocresult name="#Variables.cfpra[cfpridx][1]#" resultset="#Variables.cfpra[cfpridx][2]#">
			</cfloop>
		<cfelse>
			<cfprocresult name="#Variables.cfprname#" resultset="#Variables.cfprset#">
		</cfif>
		</cfstoredproc>
		<cfcatch type="Any">
			<cfset Variables.ErrMsg	= "#Variables.ErrMsg# <li>An error occurred while trying to #Variables.LogAct#. "
									& "The following information may help: #CFCatch.Message# #CFCatch.Detail#</li>">
			<cfset Variables.TxnErr	= "Yes">
			<cfinclude template="/library/cfincludes/log_SleSPCCatch.cfm">
		</cfcatch>
	</cftry>
	<cfif NOT Variables.TxnErr>
		<cfinclude template="/library/cfincludes/log_SPCSuccess.cfm">
	</cfif>
	<cfset Variables.SleEntityName	= "">
	<cfif Variables.TxnErr AND IsDefined("Request.SpcUsingCFError") AND Request.SpcUsingCFError>
		<cfthrow type="Application" message="#Variables.ErrMsg#">
	</cfif>
</cfif><!--- /TxnErr --->


<!--- Saved 05/05/2015 13:20:04. --->
PROCEDURE VALIDPRTNOTESUBJTYPSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtNoteSubjTypCd NUMBER:=0,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtNoteSubjTypSel;
 
 IF p_Identifier= 0 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 
 ValidPrtNoteSubjTypCd, 
 ValidPrtNoteSubjTypDescTxt
 FROM ValidPrtNoteSubjTypTbl 
 WHERE ValidPrtNoteSubjTypCd = p_ValidPrtNoteSubjTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 ELSIF p_Identifier= 2 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 1
 FROM ValidPrtNoteSubjTypTbl 
 WHERE ValidPrtNoteSubjTypCd = p_ValidPrtNoteSubjTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 
 ELSIF p_Identifier= 4 THEN
 BEGIN
 
 OPEN p_SelCur FOR
 
 SELECT 
 ValidPrtNoteSubjTypCd, 
 ValidPrtNoteSubjTypDescTxt
 FROM ValidPrtNoteSubjTypTbl;
 p_RetVal := SQL%ROWCOUNT; 
 end; 
 END if;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTNOTESUBJTYPSELTSP;
 RAISE;
 END;
 END;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


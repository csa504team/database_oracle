<!--- Saved 05/05/2015 13:16:35. --->
PROCEDURE PhyAddrDelTrigCSP (
 OldDataRec PhyAddrTbl%ROWTYPE
 )
 -- Revision History -----------------------------------------
 -- 08/29/2000 C.Woodard Added code to refresh location data
 -- in partner search table and
 -- location search table.
 -- 04/18/2000 C.Woodard Created.
 -- ----------------------------------------------------------
 /* Object: Trigger
 Name: Priya Varigala
 Date Written: May 24, 2000
 Company: AQUAS, Inc.
 Company Comment: This trigger has been written to comply with the SBA
 Sybase standards in force at the time the trigger was
 written.
 Description:
 */
 AS
 v_ErrorNmb number(5);
 v_INST_ID number(8);
 v_LOC_ID number(8);
 v_LocationId number(7);
 v_LocationTyp varchar2(5);
 v_PartnerId number(7);
 v_ReplicationErrorMessageTxt varchar2(255);
 v_Rowcount number(12);
 
 BEGIN
 v_INST_ID := 0;
 v_LocationId := OldDataRec.LocId;
 
 -- Add location to extract queue.
 partner.LocExtrctStatInsCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb != 0
 THEN
 BEGIN
 v_ErrorNmb := 31802;
 v_ReplicationErrorMessageTxt := 'Unable to add location to extract queue.';
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; -- Error - location not queued for extract.
 END IF;
 
 --Refresh address data in partner search table.
 partner.PrtSrchUpdAddrCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31887;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing PrtSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 
 --Refresh address data in location search table.
 partner.LocSrchUpdAddrCSP (v_LocationId);
 v_ErrorNmb := SQLCODE;
 IF v_ErrorNmb <> 0
 THEN
 BEGIN
 v_ErrorNmb := 31888;
 v_INST_ID := 0;
 v_ReplicationErrorMessageTxt := 'Error refreshing LocSrchTbl.' || ' LocId=' || to_char(v_LocationId);
 partner.PrtReplicationErrInsCSP (v_INST_ID, v_LOC_ID, 'PhyAddrDelTrig', v_ErrorNmb, v_ReplicationErrorMessageTxt, NULL);
 END; --Error.
 END IF;
 RETURN;
 END;
 
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


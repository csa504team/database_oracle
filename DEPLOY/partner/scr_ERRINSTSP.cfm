<!--- Saved 05/05/2015 13:16:10. --->
PROCEDURE ERRINSTSP(
 p_ErrNmb NUMBER := 0,
 p_ErrMsgTxt VARCHAR2:= NULL,
 p_ErrProcNm VARCHAR2:= NULL,
 p_RetVal OUT NUMBER)
 AS
 BEGIN
 SAVEPOINT ERRINSTSP;
 INSERT INTO PARTNER.ERRTBL (ERRNMB,ERRMSGTXT,ERRPROCNM)
 VALUES (p_ErrNmb,p_ErrMsgTxt,p_ErrProcNm);
 p_RetVal :=SQL%ROWCOUNT;
 
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO ERRINSTSP;
 
 END ERRINSTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


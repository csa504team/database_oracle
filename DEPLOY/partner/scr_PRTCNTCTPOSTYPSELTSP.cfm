<!--- Saved 05/05/2015 13:17:26. --->
PROCEDURE PRTCNTCTPOSTYPSELTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtCntctPosTypCd NUMBER DEFAULT 0,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 SAVEPOINT PrtCntctPosTypSel;
 IF p_Identifier = 0 THEN
 /* Select Row On the basis of Key*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtCntctPosTypCd,
 PrtCntctPosTypTxt 
 FROM PrtCntctPosTypTbl 
 WHERE PrtCntctPosTypCd = p_PrtCntctPosTypCd;
 p_RetVal := SQL%ROWCOUNT; 
 END;
 ELSIF p_Identifier = 2 THEN
 /*Check The Existance Of Row*/
 BEGIN
 OPEN p_SelCur FOR
 SELECT 1 
 FROM PrtCntctPosTypTbl 
 WHERE PrtCntctPosTypCd = PrtCntctPosTypCd;
 p_RetVal := SQL%ROWCOUNT;
 END;
 ELSIF p_Identifier = 4 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT PrtCntctPosTypCd,
 PrtCntctPosTypTxt
 FROM PrtCntctPosTypTbl;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtCntctPosTypSe;
 RAISE;
 
 END PRTCNTCTPOSTYPSELTSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:57. --->
PROCEDURE VALIDPRTALIASTYPUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidPrtAliasTyp VARCHAR2:=NULL,
 p_ValidPrtAliasTypDesc VARCHAR2:=NULL,
 p_ValidPrtAliasTypDispOrdNmb NUMBER:=0,
 p_ValidPrtAliasTypCreatUserId VARCHAR2:=NULL
 --,
 --p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 SAVEPOINT ValidPrtAliasTypUpd;
 IF p_Identifier = 0 THEN
 BEGIN
 UPDATE PARTNER.ValidPrtAliasTypTbl
 SET 
 ValidPrtAliasTypDesc =p_ValidPrtAliasTypDesc, 
 ValidPrtAliasTypDispOrdNmb =p_ValidPrtAliasTypDispOrdNmb, 
 ValidPrtAliasTypCreatUserId = p_ValidPrtAliasTypCreatUserId,
 ValidPrtAliasTypCreatDt =SYSDATE
 WHERE ValidPrtAliasTyp = p_ValidPrtAliasTyp;
 p_RetVal := SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDPRTALIASTYPUPD;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


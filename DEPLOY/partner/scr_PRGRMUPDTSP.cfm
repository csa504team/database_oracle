<!--- Saved 05/05/2015 13:16:48. --->
PROCEDURE PRGRMUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrgrmAreaOfOperReqdInd CHAR := NULL,
 p_PrgrmDispOrdNmb NUMBER := 0,
 p_PrgrmDocReqdInd CHAR := NULL,
 p_PrgrmEffDt DATE := NULL,
 p_PrgrmFormId VARCHAR2 := NULL,
 p_PrgrmMaxTrmMoQty NUMBER := 0,
 p_PrgrmDesc VARCHAR2 := NULL,
 p_ValidPrgrmTyp VARCHAR2 := NULL,
 p_PrgrmParntPrgrmId VARCHAR2 := NULL,
 p_PrgrmCreatUserId VARCHAR2 := NULL
 )
 AS
 BEGIN
 SAVEPOINT PrgrmUpd;
 
 IF p_Identifier = 0 
 THEN
 BEGIN
 UPDATE PrgrmTbl
 SET 
 PrgrmAreaOfOperReqdInd=p_PrgrmAreaOfOperReqdInd,
 PrgrmDispOrdNmb=p_PrgrmDispOrdNmb,
 PrgrmDocReqdInd=p_PrgrmDocReqdInd,
 PrgrmEffDt=p_PrgrmEffDt,
 PrgrmFormId=p_PrgrmFormId,
 PrgrmMaxTrmMoQty=p_PrgrmMaxTrmMoQty,
 PrgrmDesc=p_PrgrmDesc,
 ValidPrgrmTyp=p_ValidPrgrmTyp,
 PrgrmParntPrgrmId=p_PrgrmParntPrgrmId,
 LASTUPDTUSERID = p_PrgrmCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrgrmId=p_PrgrmId;
 p_RetVal := SQL%ROWCOUNT;
 
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK TO PrgrmUpd; 
 
 
 
 END PRGRMUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


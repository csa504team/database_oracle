<!--- Saved 05/05/2015 13:18:23. --->
PROCEDURE PRTLOCFIRSRECRDSELCSP(
 p_LocId IN NUMBER DEFAULT NULL,
 p_PrtLocFIRSRecrdTyp OUT CHAR)
 AS
 v_LocationTyp VARCHAR2(5);
 v_PrimaryTyp VARCHAR2(5);
 v_SubcategoryTyp VARCHAR2(5);
 BEGIN
 begin
 SELECT l.ValidLocTyp,p.ValidPrtPrimCatTyp,p.ValidPrtSubCatTyp
 into v_LocationTyp,v_PrimaryTyp,v_SubcategoryTyp
 FROM PrtLocTbl l, PrtTbl p 
 WHERE l.LocId = p_LocId 
 AND l.PrtId = p.PrtId;
 EXCEPTION WHEN NO_DATA_FOUND THEN 
 v_LocationTyp := NULL; 
 v_PrimaryTyp := NULL; 
 v_SubcategoryTyp := NULL;
 end;
 
 IF(v_PrimaryTyp = 'HC') AND (v_LocationTyp = 'HO') THEN
 p_PrtLocFIRSRecrdTyp := 'HC';
 ELSIF(v_PrimaryTyp != 'HC') AND (v_LocationTyp = 'HO') THEN
 p_PrtLocFIRSRecrdTyp := 'HO';
 ELSIF(v_PrimaryTyp != 'HC') AND (v_LocationTyp = 'BO') THEN
 p_PrtLocFIRSRecrdTyp := 'BO';
 ELSIF(v_PrimaryTyp = 'NBLDR') AND (v_SubcategoryTyp = 'Micro') THEN
 p_PrtLocFIRSRecrdTyp := NULL;
 ELSE
 p_PrtLocFIRSRecrdTyp := 'BO';
 END IF;
 
 RETURN /*0*/;
 END PRTLOCFIRSRECRDSELCSP;
 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:20:11. --->
PROCEDURE VALIDPRTSUBCATSELTSP
 (
 p_Identifier NUMBER:=0,
 p_RetVal OUT NUMBER,
 p_ValidSBAPrtPrimCatTyp VARCHAR2:=NULL,
 p_SelCur OUT SYS_REFCURSOR
 )
 AS
 BEGIN
 IF p_Identifier= 11 THEN
 BEGIN
 OPEN p_SelCur FOR
 SELECT ValidPrtSubCatTyp
 FROM ValidPrtSubCatTbl 
 WHERE ValidSBAPrtPrimCatTyp = p_ValidSBAPrtPrimCatTyp;
 p_RetVal := SQL%ROWCOUNT; 
 END; 
 end if ; 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 --ROLLBACK TO VALIDPRTSUBCATSELTSP;
 RAISE;
 END;
 END; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


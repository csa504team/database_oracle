<!--- Saved 05/05/2015 13:16:49. --->
PROCEDURE PRTAGRMTAUDTINSTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtAgrmtAudtNmb NUMBER := 0,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrgrmId VARCHAR2 := NULL,
 p_PrtAgrmtAudtDocLocOfcCd CHAR := NULL,
 p_PrtAgrmtAudtAreaOfOperTxt VARCHAR2 := NULL,
 p_PrtAgrmtAudtDocLocTxt VARCHAR2 := NULL,
 p_PrtAgrmtAudtEffDt DATE := NULL,
 p_PrtAgrmtAudtExprDt DATE := NULL,
 p_PrtAgrmtAudtTermDt DATE := NULL,
 p_PrtAgrmtTermRsnCd NUMBER := 0,
 p_PrtAgrmtAudtWvrInd CHAR := NULL,
 p_PrtAgrmtAudtCreatUserId VARCHAR2 := NULL)
 AS
 BEGIN
 
 SAVEPOINT PrtAgrmtAudtIns;
 
 IF p_Identifier = 0 THEN
 BEGIN
 INSERT INTO PARTNER.PrtAgrmtAudtTbl (PrtAgrmtAudtNmb, PrtId, PrtAgrmtSeqNmb, PrgrmId,
 PrtAgrmtAudtDocLocOfcCd, PrtAgrmtAudtAreaOfOperTxt,
 PrtAgrmtAudtDocLocTxt, PrtAgrmtAudtEffDt, PrtAgrmtAudtExprDt, PrtAgrmtAudtTermDt,
 PrtAgrmtTermRsnCd, PrtAgrmtAudtWvrInd, PrtAgrmtAudtCreatUserId, PrtAgrmtAudtCreatDt,LASTUPDTUSERID,LASTUPDTDT)
 VALUES (p_PrtAgrmtAudtNmb, p_PrtId, p_PrtAgrmtSeqNmb, p_PrgrmId,
 p_PrtAgrmtAudtDocLocOfcCd, p_PrtAgrmtAudtAreaOfOperTxt,
 p_PrtAgrmtAudtDocLocTxt, p_PrtAgrmtAudtEffDt, p_PrtAgrmtAudtExprDt, p_PrtAgrmtAudtTermDt,
 p_PrtAgrmtTermRsnCd, p_PrtAgrmtAudtWvrInd, p_PrtAgrmtAudtCreatUserId, SYSDATE,p_PrtAgrmtAudtCreatUserId, SYSDATE);
 p_RetVal :=SQL%ROWCOUNT;
 END;
 END IF;
 
 EXCEPTION
 WHEN OTHERS THEN
 ROLLBACK TO PrtAgrmtAudtIns;
 RAISE;
 
 END PRTAGRMTAUDTINSTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


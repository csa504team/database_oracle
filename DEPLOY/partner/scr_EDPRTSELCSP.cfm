<!--- Saved 05/05/2015 13:16:03. --->
PROCEDURE EDPRTSELCSP(
 p_SelCur OUT SYS_REFCURSOR)
 AS
 p_CurFy NUMBER(4);
 BEGIN
 --SAVEPOINT EDPrtSelCSP;
 p_CurFy := to_number(to_char(sysdate,'YYYY')) + case when to_char(sysdate,'MM') > 9 then 1 else 0 end; 
 OPEN p_SelCur FOR 
 SELECT a.PrtId,
 b.PrtLocNm,
 ValidPrtSubCatTyp,
 PrtLocCurStatCd,
 SUBSTR(d.OrignOfcCd, 1, 2) || '00' RegionOfcCd,
 d.OrignOfcCd DistictOfcCd,
 PhyAddrStCd 
 FROM Partner.PrtTbl a, Partner.PrtLocTbl b, Partner.PhyAddrTbl c, SBAREF.CntyTbl d, sbaref.Zip5CdTbl e 
 WHERE a.PrtId = b.PrtId 
 and ValidPrtSubCatTyp IN ('SCORE', 'SBDC', 'WBC', 'NAASP', 'VBOC') 
 and (b.PrtLocCurStatCd = 'O' or EXISTS (SELECT 1 FROM ED.CusImpctQtrSumryTbl z
 WHERE z.LocId = b.LocId
 and z.CusPrtLocLeadInd = 'Y'
 and z.CusFisclYrNmb BETWEEN p_CurFy - 1 AND p_CurFy))
 and ValidLocTyp = 'HO'
 and b.LocId = c.LocId
 and ValidAddrTyp = 'Phys' 
 and PrtCntctNmb IS NULL 
 and e.StCd = d.StCd
 and e.CntyCd = d.CntyCd
 --and PhyAddrStCd = d.StCd 
 and SUBSTR(PhyAddrPostCd,1,5) = e.Zip5Cd
 order by PrtLocNm;
 EXCEPTION
 WHEN OTHERS THEN
 RAISE;
 ROLLBACK;
 
 END EDPRTSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


<!--- Saved 05/05/2015 13:19:40. --->
PROCEDURE VALIDCHNGCDUPDTSP
 (
 p_Identifier NUMBER:=0,
 p_ValidChngCd VARCHAR2 :=NULL,
 p_ValidChngCdDesc VARCHAR2 :=NULL,
 p_ValidChngCdTyp CHAR :=NULL,
 p_ValidChngCdDispOrdNmb NUMBER:=0,
 p_ValidChngCdCreatUserId VARCHAR2 :=NULL,
 p_RetVal OUT NUMBER
 )
 AS
 BEGIN 
 SAVEPOINT ValidChngCdUpd;
 IF p_Identifier= 0 THEN
 /* Update IMAdrTyp Table */
 BEGIN
 UPDATE ValidChngCdTbl
 SET ValidChngCdDesc = p_ValidChngCdDesc, 
 ValidChngCdTyp = p_ValidChngCdTyp, 
 ValidChngCdDispOrdNmb = p_ValidChngCdDispOrdNmb, 
 ValidChngCdCreatUserId = p_ValidChngCdCreatUserId,
 ValidChngCdCreatDt = SYSDATE
 WHERE ValidChngCd = p_ValidChngCd;
 p_RetVal := SQL%ROWCOUNT; 
 end;
 END IF ; 
 
 EXCEPTION
 WHEN OTHERS THEN
 BEGIN
 ROLLBACK TO VALIDCHNGCDUPDTSP;
 RAISE;
 END;
 end; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


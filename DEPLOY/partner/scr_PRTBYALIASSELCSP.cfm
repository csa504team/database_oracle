<!--- Saved 01/25/2018 10:37:56. --->
PROCEDURE PRTBYALIASSELCSP(
 p_userName VARCHAR2 := NULL,
 p_ValidPrtAliasTyp VARCHAR2 := NULL,
 p_PrtAliasNm VARCHAR2 := NULL,
 p_SelCur OUT SYS_REFCURSOR)
 AS
 BEGIN
 IF p_ValidPrtAliasTyp = 'None' THEN
 BEGIN
 /*[SPCONV-ERR(21)]:(LIKE) if using '[' Manual conversion required
 RY--01/24/2018--OPSMDEV-1335:: Removed the references of PrtDataSourcId*/
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 pa.PrtAliasNm,
 p.OfcCd,
 p.PrtChrtrTyp,
 p.PrtEstbDt,
 p.PrtFisclYrEndDayNmb,
 p.PrtFisclYrEndMoNmb,
 p.PrtInactDt,
 p.PrtLglNm,
 p.ValidPrtLglTyp,
 p.PrtCurStatCd,
 p.ValidPrtSubCatTyp
 FROM PrtTbl p, PrtAliasTbl pa 
 WHERE p.PrtId = pa.PrtId 
 AND pa.PrtAliasNm LIKE p_PrtAliasNm;
 END;
 ELSE
 BEGIN
 /*[SPCONV-ERR(32)]:(LIKE) if using '[' Manual conversion required*/
 OPEN p_SelCur FOR
 SELECT p.PrtId,
 pa.PrtAliasNm,
 p.OfcCd,
 p.PrtChrtrTyp,
 p.PrtEstbDt,
 p.PrtFisclYrEndDayNmb,
 p.PrtFisclYrEndMoNmb,
 p.PrtInactDt,
 p.PrtLglNm,
 p.ValidPrtLglTyp,
 p.PrtCurStatCd,
 p.ValidPrtSubCatTyp
 FROM PrtTbl p, PrtAliasTbl pa 
 WHERE p.PrtId = pa.PrtId 
 AND pa.ValidPrtAliasTyp = p_ValidPrtAliasTyp 
 AND pa.PrtAliasNm LIKE p_PrtAliasNm;
 END;
 END IF;
 END PRTBYALIASSELCSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


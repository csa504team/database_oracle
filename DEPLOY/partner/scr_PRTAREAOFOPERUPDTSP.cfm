<!--- Saved 05/05/2015 13:17:14. --->
PROCEDURE PRTAREAOFOPERUPDTSP(
 p_Identifier NUMBER := 0,
 p_RetVal OUT NUMBER,
 p_PrtId NUMBER := 0,
 p_PrtAgrmtSeqNmb NUMBER := 0,
 p_PrtAreaOfOperSeqNmb NUMBER := 0,
 p_StCd CHAR := NULL,
 p_CntyCd CHAR := NULL,
 p_PrtAreaOfOperCntyEffDt DATE := NULL,
 p_PrtAreaOfOperCntyEndDt DATE := NULL,
 p_PrtAreaOfOperCreatUserId VARCHAR2 := NULL,
 p_OfcCd CHAR := NULL)
 AS
 -- Trigger variable
 NewDataRec PrtAreaOfOperTbl%ROWTYPE;
 /*TranFlag NUMBER(10,0);
 error NUMBER(10,0);
 UF1_rowid ROWID;
 UF1_oval1 PrtAreaOfOperTbl.PrtAreaOfOperCntyEndDt%TYPE;
 UF1_oval2 PrtAreaOfOperTbl.PrtAreaOfOperCreatUserId%TYPE;
 UF1_oval3 PrtAreaOfOperTbl.PrtAreaOfOperCreatDt%TYPE;
 CURSOR UF1_cursor IS 
 SELECT a.ROWID, PrtAreaOfOperCntyEndDt, PrtAreaOfOperCreatUserId, SYSDATE
 FROM PrtAreaOfOperTbl a, SBAREF.CntyTbl b 
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND a.StCd = p_StCd 
 AND b.StCd = p_StCd 
 AND a.CntyCd = b.CntyCd 
 AND OrignOfcCd = p_OfcCd 
 AND CntyStrtDt <= SYSDATE 
 AND 
 (CntyEndDt > SYSDATE 
 or CntyEndDt IS NULL) 
 AND PrtAreaOfOperCntyEndDt IS NULL
 FOR UPDATE OF a.PrtAreaOfOperCntyEndDt, a.PrtAreaOfOperCreatUserId, a.PrtAreaOfOperCreatDt;*/
 BEGIN
 SAVEPOINT PrtAreaOfOperUpd;
 
 IF p_Identifier = 0 THEN
 /* Updert into PrtAreaOfOper Table */
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl
 SET PrtId =p_PrtId, 
 PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb, 
 PrtAreaOfOperSeqNmb =p_PrtAreaOfOperSeqNmb, 
 StCd =p_StCd, 
 CntyCd =p_CntyCd, 
 PrtAreaOfOperCntyEffDt =p_PrtAreaOfOperCntyEffDt, 
 PrtAreaOfOperCntyEndDt =p_PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId =p_PrtId 
 AND PrtAgrmtSeqNmb =p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb =p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END;
 ELSIF p_Identifier = 11 THEN
 /* update End Date of Area of Operation for Active Agreement */
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl
 SET PrtAreaOfOperCntyEndDt = p_PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END;
 ELSIF p_Identifier = 12 THEN
 /* update Effective Date and End Date of Area of Operation for Pending Agreement */
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl
 SET PrtAreaOfOperCntyEffDt = p_PrtAreaOfOperCntyEffDt, 
 PrtAreaOfOperCntyEndDt = p_PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND PrtAreaOfOperSeqNmb = p_PrtAreaOfOperSeqNmb;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END;
 ELSIF p_Identifier = 13 THEN
 /* update End Date of Area of Operation for Active Agreement based on State*/
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl
 SET PrtAreaOfOperCntyEndDt = p_PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = P_PrtId 
 AND PrtAgrmtSeqNmb = P_PrtAgrmtSeqNmb 
 AND StCd = P_StCd 
 AND PrtAreaOfOperCntyEndDt IS NULL;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND StCd = p_StCd AND PrtAreaOfOperCntyEndDt IS NULL;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END;
 ELSIF p_Identifier = 14 THEN
 /* update End Date of Area of Operation for Active Agreement based on State and Office */
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl a
 SET PrtAreaOfOperCntyEndDt = p_PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 where exists(select * from SBAREF.CntyTbl b
 where b.StCd = p_StCd
 and a.CntyCd = b.CntyCd
 AND OrignOfcCd = p_OfcCd
 AND CntyStrtDt <= SYSDATE
 AND (CntyEndDt > SYSDATE or CntyEndDt is null)
 )
 and PrtId = p_PrtId
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND a.StCd = p_StCd 
 AND PrtAreaOfOperCntyEndDt is NULL;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl a WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND StCd = p_StCd AND PrtAreaOfOperCntyEndDt IS NULL
 and exists (select 1 from SBAREF.CntyTbl b where b.StCd = p_StCd and a.CntyCd = b.CntyCd
 AND OrignOfcCd = p_OfcCd AND CntyStrtDt <= SYSDATE AND (CntyEndDt > SYSDATE or CntyEndDt is null));
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END;
 ELSIF p_Identifier = 15 THEN
 /* update End Date of Area of Operation for Active Agreement based on County*/
 BEGIN
 UPDATE PARTNER.PrtAreaOfOperTbl
 SET PrtAreaOfOperCntyEndDt = PrtAreaOfOperCntyEndDt, 
 LASTUPDTUSERID = p_PrtAreaOfOperCreatUserId,
 LASTUPDTDT =SYSDATE
 WHERE PrtId = p_PrtId 
 AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb 
 AND StCd = p_StCd 
 AND CntyCd = p_CntyCd 
 AND PrtAreaOfOperCntyEndDt IS NULL;
 p_RetVal := SQL%ROWCOUNT;
 -- get new snapshot
 begin
 SELECT * INTO NewDataRec FROM PrtAreaOfOperTbl WHERE PrtId = p_PrtId AND PrtAgrmtSeqNmb = p_PrtAgrmtSeqNmb
 AND StCd = p_StCd AND CntyCd = p_CntyCd AND PrtAreaOfOperCntyEndDt IS NULL;
 EXCEPTION WHEN NO_DATA_FOUND THEN NewDataRec := NULL; END;
 END; 
 END IF;
 -- call trigger
 IF SQLCODE = 0
 THEN
 BEGIN
 PrtAreaOfOperInsUpdTrigCSP (NewDataRec);
 IF SQLCODE != 0
 THEN
 ROLLBACK;
 END IF;
 END;
 END IF; -- end trigger
 
 END PRTAREAOFOPERUPDTSP; 
<cfoutput>
Nothing to see here! Move along!
</cfoutput>


define deploy_name=FirstPass
define package_name=UAT_0617
define package_buildtime=20210617132802
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FirstPass_UAT_0617 created on Thu 06/17/2021 13:28:02.73 by Jasleen Gorowada
prompt deploy scripts for deploy FirstPass_UAT_0617 created on Thu 06/17/2021 13:28:02.73 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FirstPass_UAT_0617: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\FIRSTPASSTBL.sql 

-- D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Misc\insert_FIRSTPASSTBL.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Tables\FIRSTPASSTBL.sql"
CREATE TABLE CDCONLINE.FIRSTPASSTBL
(
  FIRSTPASSTBLID   NUMBER GENERATED ALWAYS AS IDENTITY ( START WITH 229 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCYCLE CACHE 20 NOORDER NOKEEP) NOT NULL,
  CDCREGNCD        CHAR(2 BYTE)                 NOT NULL,
  CDCNMB           CHAR(4 BYTE)                 NOT NULL,
  SIXMONTHRATE     NUMBER(5,2),
  TWELVEMONTHRATE  NUMBER(5,2),
  CREATUSERID      VARCHAR2(32 BYTE)            DEFAULT user                  NOT NULL,
  CREATDT          DATE                         DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID   VARCHAR2(32 BYTE)            DEFAULT user                  NOT NULL,
  LASTUPDTDT       DATE                         DEFAULT sysdate               NOT NULL
)
TABLESPACE CDCONLINEDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX CNTRJGOROWADA.FIRSTPASSTBL_PK ON CDCONLINE.FIRSTPASSTBL
(FIRSTPASSTBLID)
LOGGING
TABLESPACE CDCONLINEDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE CDCONLINE.FIRSTPASSTBL ADD (
  CONSTRAINT FIRSTPASSTBL_PK
  PRIMARY KEY
  (FIRSTPASSTBLID)
  USING INDEX CNTRJGOROWADA.FIRSTPASSTBL_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CDCONLINECORPGOVPRTUPDATE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CDCONLINEDEVROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CDCONLINEPRTREAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CDCONLINEREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CNTRFHINES;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CNTRILIBERMAN;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CNTRSMANIAM;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.FIRSTPASSTBL TO STGCSA;

-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\CDCONLINE\Misc\insert_FIRSTPASSTBL.sql"
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '284 ', '', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '203 ', '', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '652 ', '80', '54');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '465 ', '54', '38');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '072 ', '57', '45');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '626 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '687 ', '69', '61');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '707 ', '70', '56');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '696 ', '48', '42');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '706 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '365 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '478 ', '80', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '122 ', '33', '45');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '308 ', '18', '11');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '420 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '646 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '551 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '305 ', '68', '62');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '073 ', '75', '82');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '236 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '431 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '398 ', '76', '75');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '303 ', '0', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '053 ', '0', '17');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '655 ', '77', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '067 ', '67', '57');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '616 ', '63', '49');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '714 ', '36', '17');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '261 ', '71', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '246 ', '61', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '017 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '316 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '253 ', '47', '41');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '676 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '190 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '132 ', '79', '70');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '677 ', '40', '25');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '285 ', '0', '13');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '019 ', '67', '65');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '330 ', '67', '69');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '377 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '453 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '585 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '713 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '349 ', '50', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '704 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '717 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '031 ', '36', '32');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '712 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '438 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '171 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '524 ', '43', '41');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '577 ', '76', '66');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '272 ', '41', '31');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '312 ', '', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '683 ', '50', '43');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '663 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '360 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '689 ', '75', '80');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '572 ', '44', '48');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '541 ', '55', '46');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '422 ', '53', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '647 ', '70', '67');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '265 ', '38', '42');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '562 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '581 ', '57', '44');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '281 ', '55', '47');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '371 ', '57', '53');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '698 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '207 ', '33', '14');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '645 ', '67', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '128 ', '38', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '623 ', '43', '36');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '381 ', '57', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '263 ', '0', '9');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '499 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '637 ', '89', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '270 ', '81', '77');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '708 ', '56', '40');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '644 ', '71', '63');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '129 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '356 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '328 ', '67', '75');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '488 ', '14', '6');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '464 ', '67', '61');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '315 ', '92', '84');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '701 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '702 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '496 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '634 ', '67', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '422 ', '60', '55');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '235 ', '33', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '429 ', '89', '90');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '188 ', '33', '29');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '021 ', '68', '55');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '020 ', '25', '25');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '042 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '593 ', '60', '71');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '290 ', '55', '53');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '703 ', '80', '75');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '657 ', '67', '80');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '102 ', '17', '30');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '313 ', '25', '25');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '153 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '694 ', '52', '47');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '718 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '151 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '625 ', '78', '71');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '715 ', '60', '43');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '103 ', '56', '47');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '494 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '598 ', '20', '25');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '172 ', '75', '68');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '711 ', '43', '38');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '672 ', '50', '38');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '131 ', '80', '83');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('0 ', '0   ', '61', '61');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '390 ', '42', '39');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '507 ', '40', '44');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '054 ', '42', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '329 ', '18', '14');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '318 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '695 ', '63', '58');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '697 ', '44', '41');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '174 ', '71', '56');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '288 ', '50', '14');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '409 ', '40', '40');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '163 ', '50', '75');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '424 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '238 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '058 ', '75', '70');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '092 ', '84', '79');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '684 ', '33', '30');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '113 ', '67', '61');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '393 ', '25', '9');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '262 ', '50', '41');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '198 ', '60', '56');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '186 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '476 ', '39', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '013 ', '72', '61');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '302 ', '69', '59');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '628 ', '74', '70');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '286 ', '74', '65');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '658 ', '50', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '046 ', '33', '20');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '669 ', '', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '609 ', '81', '79');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '685 ', '73', '71');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '632 ', '22', '19');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '654 ', '61', '63');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '640 ', '65', '68');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '421 ', '82', '77');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '548 ', '81', '70');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '324 ', '25', '20');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '201 ', '30', '19');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '243 ', '25', '9');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '361 ', '67', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '468 ', '56', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '267 ', '100', '100');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '024 ', '76', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '529 ', '60', '52');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '436 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '627 ', '50', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '244 ', '77', '63');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '229 ', '0', '11');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '187 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '202 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '642 ', '25', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '688 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '511 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '662 ', '67', '54');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '050 ', '', '100');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '335 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '307 ', '100', '50');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '230 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '362 ', '67', '56');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '631 ', '75', '75');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '179 ', '58', '47');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '690 ', '50', '41');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '123 ', '80', '46');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '705 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '040 ', '100', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '086 ', '88', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '667 ', '75', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '555 ', '56', '48');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '200 ', '', '100');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '471 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '134 ', '67', '56');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '691 ', '43', '48');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '223 ', '59', '54');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '018 ', '100', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '716 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '367 ', '50', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '373 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '220 ', '53', '48');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '692 ', '0', '13');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '434 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '009 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '204 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '038 ', '54', '44');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('03', '699 ', '33', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '006 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '109 ', '72', '65');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '615 ', '60', '35');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '417 ', '100', '100');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '540 ', '75', '67');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '011 ', '76', '68');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '392 ', '79', '54');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '622 ', '75', '72');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '493 ', '73', '67');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '426 ', '67', '54');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '590 ', '60', '38');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '656 ', '80', '71');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '311 ', '78', '73');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '264 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '366 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('10', '276 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('04', '641 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '594 ', '74', '62');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('02', '150 ', '40', '33');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('06', '649 ', '75', '62');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '495 ', '', '');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '139 ', '41', '40');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('05', '413 ', '78', '79');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('07', '611 ', '62', '63');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('09', '118 ', '77', '63');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('08', '680 ', '0', '0');
Insert into CDCONLINE.FIRSTPASSTBL
   (CDCREGNCD, CDCNMB, SIXMONTHRATE, TWELVEMONTHRATE)
 Values
   ('01', '219 ', '50', '50');
COMMIT;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

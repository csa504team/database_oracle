define deploy_name=FMLP_Pool_Inserts
define package_name=UAT_0616
define package_buildtime=20210616125518
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FMLP_Pool_Inserts_UAT_0616 created on Wed 06/16/2021 12:55:19.42 by Jasleen Gorowada
prompt deploy scripts for deploy FMLP_Pool_Inserts_UAT_0616 created on Wed 06/16/2021 12:55:19.42 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FMLP_Pool_Inserts_UAT_0616: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_records_MONLOANFINANTBL_0616.sql 

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\add_records_MONLOANFINANTBL_0616.sql"
SET DEFINE OFF;
Insert into LOAN.MONLOANFINANTBL
   (LOANNMB, LOANFINANRPTDT, LOANOUTBALAMT, LOANLASTPYMNTDT, MONGROSSPRINPAIDAMT, 
    MONGROSSINTPAIDAMT, MONADVNSBEGPRINBALAMT, MONADVNSBEGBALINTAMT, MONADVNSPRINPAIDAMT, MONADVNSINTPAIDAMT, 
    MONADVNSPRINCOLLAMT, MONADVNSINTCOLLAMT, MONADVNSENDPRINBALAMT, MONADVNSENDBALINTAMT, LOANLENDRSERVFEEAMT, 
    LOANFINANPOSTDT, LOANFINANPRCSSTATCD, LOANFINANUPLDDT, LOANFINANTRANSTYP, MONADVNSOTHAMT, 
    MONADVNSOTHCOLLAMT, LOANPOOLLOANNMB, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT)
 Values
   ('2770946004', TO_DATE('6/2/2021', 'MM/DD/YYYY'), 619141.3, NULL, 2272.6, 
    4371.82, NULL, NULL, 1783.77, 0, 
    1783.77, 0, NULL, NULL, 71.21, 
    TO_DATE('6/14/2021', 'MM/DD/YYYY'), NULL, NULL, NULL, 0, 
    0, '4260905006', user, sysdate, user, 
    sysdate);
Insert into LOAN.MONLOANFINANTBL
   (LOANNMB, LOANFINANRPTDT, LOANOUTBALAMT, LOANLASTPYMNTDT, MONGROSSPRINPAIDAMT, 
    MONGROSSINTPAIDAMT, MONADVNSBEGPRINBALAMT, MONADVNSBEGBALINTAMT, MONADVNSPRINPAIDAMT, MONADVNSINTPAIDAMT, 
    MONADVNSPRINCOLLAMT, MONADVNSINTCOLLAMT, MONADVNSENDPRINBALAMT, MONADVNSENDBALINTAMT, LOANLENDRSERVFEEAMT, 
    LOANFINANPOSTDT, LOANFINANPRCSSTATCD, LOANFINANUPLDDT, LOANFINANTRANSTYP, MONADVNSOTHAMT, 
    MONADVNSOTHCOLLAMT, LOANPOOLLOANNMB, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT)
 Values
   ('3074936002', TO_DATE('6/2/2021', 'MM/DD/YYYY'), 1924567.67, NULL, 0, 
    12000, NULL, NULL, 5824.18, 6780.43, 
    4930.18, 3892.82, NULL, NULL, 194.54, 
    TO_DATE('6/14/2021', 'MM/DD/YYYY'), NULL, NULL, NULL, 0, 
    0, '4260905006',  user, sysdate, user, 
    sysdate);
Insert into LOAN.MONLOANFINANTBL
   (LOANNMB, LOANFINANRPTDT, LOANOUTBALAMT, LOANLASTPYMNTDT, MONGROSSPRINPAIDAMT, 
    MONGROSSINTPAIDAMT, MONADVNSBEGPRINBALAMT, MONADVNSBEGBALINTAMT, MONADVNSPRINPAIDAMT, MONADVNSINTPAIDAMT, 
    MONADVNSPRINCOLLAMT, MONADVNSINTCOLLAMT, MONADVNSENDPRINBALAMT, MONADVNSENDBALINTAMT, LOANLENDRSERVFEEAMT, 
    LOANFINANPOSTDT, LOANFINANPRCSSTATCD, LOANFINANUPLDDT, LOANFINANTRANSTYP, MONADVNSOTHAMT, 
    MONADVNSOTHCOLLAMT, LOANPOOLLOANNMB, CREATUSERID, CREATDT, LASTUPDTUSERID, 
    LASTUPDTDT)
 Values
   ('3368106006', TO_DATE('6/2/2021', 'MM/DD/YYYY'), 493038.5, NULL, 2085.16, 
    2511.24, NULL, NULL, 0, 0, 
    0, 0, NULL, NULL, 56.96, 
    TO_DATE('6/14/2021', 'MM/DD/YYYY'), NULL, NULL, NULL, 0, 
    0, '4260905006',  user, sysdate, user, 
    sysdate);
COMMIT;

set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

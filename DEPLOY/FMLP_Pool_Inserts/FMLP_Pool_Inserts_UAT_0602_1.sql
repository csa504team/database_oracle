define deploy_name=FMLP_Pool_Inserts
define package_name=UAT_0602_1
define package_buildtime=20210602163111
column tsp new_value tsp 
column gn new_value gn 
column usrnm new_value usrnm 
column datetime new_value datetime 
select case instr(global_name,'.') when 0 then global_name else substr(global_name,1,instr(global_name,'.')-1) end gn, user usrnm, 
to_char(sysdate,'yyyymmddhh24miss') tsp,to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') datetime from global_name; 
spool &&deploy_name._&&gn._&&package_name._&&tsp..log 
-- deploy scripts for deploy FMLP_Pool_Inserts_UAT_0602_1 created on Wed 06/02/2021 16:31:13.72 by Jasleen Gorowada
prompt deploy scripts for deploy FMLP_Pool_Inserts_UAT_0602_1 created on Wed 06/02/2021 16:31:13.72 by Jasleen Gorowada
set echo off
set verify off
set feedback off
set heading off
set termout off 
spool check4errors.bat
select 'echo off' from dual;
select 'echo Checking script output for errors (no news is good news):' from dual;
select 'find "ORA-" &&DEPLOY_NAME._&&gn._&&package_name._&&TSP..log' from dual;
spool off
spool &&deploy_name._&&gn._&&package_name._&&tsp..log append 
set termout on
select 'deployed on '||global_name||' '
  ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' by '||user
from global_name;
--select 'deploy script &&deploy_name._&&package_name built &&package_buildtime'
--from dual;
set feedback on
set heading on
prompt Instructions for deploy FMLP_Pool_Inserts_UAT_0602_1: 
set termout on 
prompt
prompt No specific deploy instructions for this deploy.
prompt No errors are expected. 
prompt If rerun then duplicate object messages may be ignored.
prompt
prompt If unexpected errors are reported, please execute a "ROLLBACK" 
prompt and notify clearly that the script was rolled back for that reason.  
prompt Otherwise (If script produces no unexpected results) please exit SQL*Plus to commit the updates.
prompt
prompt Please execute using DOS SQL*Plus from a writeable directory for proper logging and error checking.
prompt
prompt Hit enter to continue ( or Ctrl+C to abort)... 
set termout off 
accept x 
/* execute GIT Pull to ensure all files are current 
Already up to date.
  */ 
/* contents of deploy D:\dba_oracle_Bitbucket\database_oracle\DEPLOY_WORK\file_list.txt
-- D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\update_records_MONLOANFINANTBL_0602.sql 
Jasleen Gorowada committed 1447a73 on Wed Jun 2 16:11:42 2021 -0400

*/
--
--
--
-- Deploy files start here:
set echo on 
set termout on 
-- Deploy component file "D:\dba_oracle_Bitbucket\database_oracle\LOAN\Misc\update_records_MONLOANFINANTBL_0602.sql"
update LOAN.MONLOANFINANTBL set LOANLASTPYMNTDT=to_date('05/4/2021','mm/dd/yyyy'),MONADVNSBEGPRINBALAMT=9515.39, MONADVNSENDPRINBALAMT=9620.61,MONADVNSBEGBALINTAMT=6167.64 
where LOANNMB='3247026010' and LOANFINANPOSTDT=to_date('05/14/2021','mm/dd/yyyy');

update LOAN.MONLOANFINANTBL set LOANLASTPYMNTDT=to_date('05/4/2021','mm/dd/yyyy'), MONADVNSBEGPRINBALAMT=440.8, MONADVNSENDPRINBALAMT=2750.52,MONADVNSENDBALINTAMT=825.32
where LOANNMB='3247026010' and LOANFINANPOSTDT=to_date('05/14/2021','mm/dd/yyyy');

update LOAN.MONLOANFINANTBL set LOANFINANRPTDT=to_date('05/2/2021','mm/dd/yyyy'),LOANPOOLLOANNMB='4925265010'
 where LOANFINANRPTDT=to_date('05/4/2021','mm/dd/yyyy') and  LOANNMB='4417155005';

commit;
set echo off
select 'Deploy package &&deploy_name ended at '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||' on &&gn' from dual;
spool off
HOST check4errors
prompt Output for deploy spooled to &&deploy_name._&&gn._&&package_name._&&tsp..log
HOST check4errors.bat >>&&deploy_name._&&gn._&&package_name._&&tsp..log
HOST del check4errors.bat

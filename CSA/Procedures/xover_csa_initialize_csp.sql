CREATE OR REPLACE procedure     csa.xover_csa_initialize_csp as
  -- V1.5 11 Feb 2019 - JL - first tested version
  -- V1.6 14 Mar 2019 - JL - Add outer exception handler to record error in XOVER_STATUS
  prog varchar2(40):='XOVER_CSA_INITIALIZE_CSP';
  ver  varchar2(40):='V1.6 14 Mar 2019, JL';
  Reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  msgtxt varchar2(4000);
  statusrec csa.xover_status%rowtype;
begin
  select * into statusrec from csa.xover_status;
  -- decided for restartability this pgm will run OK even if LAST_EVENT not expected value
  /*
  If statusrec.xover_last_event<>'CM' then
    msgtxt:='Error - Crossover(return) is initiating on CSA, but '
      ||'XOVER_LAST_EVENT='||statusrec.xover_last_event||', not "CM" as expected.';
    update csa.xover_status
      set err_step='CI', err_dt=sysdate, err_msg=msgtxt;
    commit;
    raise_application_error(-20234,'csa '||msgtxt);
  end if;
  */
  update csa.xover_status set xover_last_event='CI',
    csa_init_dt=sysdate,err_step=null,err_dt=null,err_msg=null;
  commit;
exception when others then
  -- outer exception handler for uncaught errors
  msgtxt:=sqlerrm(sqlcode)||' caught in outer exception handler for '||prog;
  update csa.xover_status set 
    err_step='CI',err_dt=sysdate,
    err_msg=msgtxt;  
  commit;   
  raise; 
end;
/

CREATE OR REPLACE VIEW csa.XOVER_SOFVwirekeytottbl_R2V
BEQUEATH DEFINER
AS 
with merge_end as  (SELECT CSA_MERGE_END_DT FROM CSA.xover_status)
  select * FROM CSA.SOFVwirekeytottbl, merge_end
    WHERE lastupdtdt >= CSA_MERGE_END_DT;


GRANT SELECT ON csa.XOVER_SOFVwirekeytottbl_R2V TO CSAREADALLROLE;

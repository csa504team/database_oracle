ALTER TABLE CSA.SOFVGNTYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE CSA.SOFVGNTYTBL CASCADE CONSTRAINTS;

CREATE TABLE CSA.SOFVGNTYTBL
(
  LOANNMB           CHAR(10 BYTE)               NOT NULL,
  GNTYDT            DATE                        NOT NULL,
  GNTYTYP           CHAR(1 BYTE)                NOT NULL,
  GNTYGNTYAMT       NUMBER(15,2),
  GNTYGNTYORGLDT    DATE,
  GNTYSTATCD        CHAR(1 BYTE),
  GNTYCLSDT         DATE,
  GNTYCREATDT       DATE,
  GNTYLASTMAINTNDT  DATE,
  GNTYCMNT          VARCHAR2(80 BYTE),
  GNTYCMNT2         VARCHAR2(80 BYTE),
  CREATUSERID       VARCHAR2(32 BYTE),
  CREATDT           DATE                        DEFAULT sysdate,
  LASTUPDTUSERID    VARCHAR2(32 BYTE),
  LASTUPDTDT        DATE                        DEFAULT sysdate
)
TABLESPACE CSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX CSA.SOFVGNTYTBL_LASTUPDT_IDX ON CSA.SOFVGNTYTBL
(LASTUPDTDT)
LOGGING
TABLESPACE CSAINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX CSA.SOFVGNTYTBL_PK ON CSA.SOFVGNTYTBL
(LOANNMB, GNTYDT, GNTYTYP)
LOGGING
TABLESPACE CSADATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE CSA.SOFVGNTYTBL ADD (
  CHECK ("GNTYGNTYAMT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYGNTYORGLDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYSTATCD" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYCLSDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYCREATDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYLASTMAINTNDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYCMNT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("GNTYCMNT2" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("CREATUSERID" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("CREATDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("LASTUPDTUSERID" IS NOT NULL)
  ENABLE NOVALIDATE,
  CHECK ("LASTUPDTDT" IS NOT NULL)
  ENABLE NOVALIDATE,
  CONSTRAINT SOFVGNTYTBL_PK
  PRIMARY KEY
  (LOANNMB, GNTYDT, GNTYTYP)
  USING INDEX CSA.SOFVGNTYTBL_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVGNTYTBL TO CSADEVROLE;

GRANT SELECT ON CSA.SOFVGNTYTBL TO CSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVGNTYTBL TO CSAUPDTROLE;

CREATE TABLE CSA.PYMT110RPTARCHVTBL
(
  POSTDT          DATE                          NOT NULL, 
  PRININTTOTAMT   NUMBER(15,2),
  UNALLOCAMT      NUMBER(15,2),
  REPDAMT         NUMBER(15,2),
  FEEPDAMT        NUMBER(15,2),
  LATEFEEAMT      NUMBER(15,2),
  LENDRFEEAMT     NUMBER(15,2),
  PREPAYAMT       NUMBER(15,2),
  PRCSIND         CHAR(1)                       DEFAULT 'N',
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
) tablespace csadatatbs;

create unique index csa.pymtrpt_ux1 
  on CSA.PYMT110RPTARCHVTBL(postdt)
  tablespace csaindtbs;

ALTER TABLE CSA.Pymt110RptArchvTbl
    ADD CONSTRAINT Pymt110RptArchTbl_PK PRIMARY KEY
    (postDt) 
USING INDEX csa.pymtrpt_ux1;

-- grant select, insert, update, delete on csa.PYMT110RPTARCHVTBL to CSADEVROLE;
grant select, insert, update, delete on csa.PYMT110RPTARCHVTBL to CSAUPDTROLE;
grant select, insert, update, delete on csa.PYMT110RPTARCHVTBL to CSAREADALLROLE;

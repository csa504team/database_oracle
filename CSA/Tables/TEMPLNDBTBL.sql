ALTER TABLE CSA.TEMPLNDBTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE CSA.TEMPLNDBTBL CASCADE CONSTRAINTS;

CREATE TABLE CSA.TEMPLNDBTBL
(
  LOANNMB                       CHAR(10 BYTE)   NOT NULL,
  LOANDTLSBAOFC                 CHAR(5 BYTE)    NOT NULL,
  LOANDTLBORRNM                 VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLBORRMAILADRSTR1NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLBORRMAILADRSTR2NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLBORRMAILADRCTYNM       VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLBORRMAILADRSTCD        CHAR(2 BYTE)    NOT NULL,
  LOANDTLBORRMAILADRZIPCD       CHAR(5 BYTE)    NOT NULL,
  LOANDTLBORRMAILADRZIP4CD      CHAR(4 BYTE)    NOT NULL,
  LOANDTLSBCNM                  VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSBCMAILADRSTR1NM       VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSBCMAILADRSTR2NM       VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSBCMAILADRCTYNM        VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSBCMAILADRSTCD         CHAR(2 BYTE)    NOT NULL,
  LOANDTLSBCMAILADRZIPCD        CHAR(5 BYTE)    NOT NULL,
  LOANDTLSBCMAILADRZIP4CD       CHAR(4 BYTE)    NOT NULL,
  LOANDTLTAXID                  CHAR(10 BYTE)   NOT NULL,
  LOANDTLCDCREGNCD              CHAR(2 BYTE)    NOT NULL,
  LOANDTLCDCCERT                CHAR(4 BYTE)    NOT NULL,
  LOANDTLDEFPYMT                CHAR(1 BYTE)    NOT NULL,
  LOANDTLDBENTRPRINAMT          NUMBER(15,2)    NOT NULL,
  LOANDTLDBENTRCURBALAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLDBENTRINTRTPCT         NUMBER(7,5)     NOT NULL,
  LOANDTLISSDT                  DATE            NOT NULL,
  LOANDTLDEFPYMTDTX             CHAR(8 BYTE)    NOT NULL,
  LOANDTLDBENTRPYMTDTTRM        CHAR(15 BYTE)   NOT NULL,
  LOANDTLDBENTRPROCDSAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLDBENTRPROCDSESCROWAMT  NUMBER(15,2)    NOT NULL,
  LOANDTLSEMIANNPYMTAMT         NUMBER(15,2)    NOT NULL,
  LOANDTLNOTEPRINAMT            NUMBER(15,2)    NOT NULL,
  LOANDTLNOTECURBALAMT          NUMBER(15,2)    NOT NULL,
  LOANDTLNOTEINTRTPCT           NUMBER(7,5)     NOT NULL,
  LOANDTLNOTEDT                 DATE            NOT NULL,
  LOANDTLACCTCD                 NUMBER          NOT NULL,
  LOANDTLACCTCHNGCD             CHAR(3 BYTE)    NOT NULL,
  LOANDTLREAMIND                CHAR(1 BYTE)    NOT NULL,
  LOANDTLNOTEPYMTDTTRM          CHAR(15 BYTE)   NOT NULL,
  LOANDTLRESRVDEPAMT            NUMBER(15,2)    NOT NULL,
  LOANDTLUNDRWTRFEEPCT          NUMBER(7,5)     NOT NULL,
  LOANDTLUNDRWTRFEEMTDAMT       NUMBER(15,2)    NOT NULL,
  LOANDTLUNDRWTRFEEYTDAMT       NUMBER(15,2)    NOT NULL,
  LOANDTLUNDRWTRFEEPTDAMT       NUMBER(15,2)    NOT NULL,
  LOANDTLCDCFEEPCT              NUMBER(7,5)     NOT NULL,
  LOANDTLCDCFEEMTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCDCFEEYTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCDCFEEPTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCSAFEEPCT              NUMBER(7,5)     NOT NULL,
  LOANDTLCSAFEEMTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCSAFEEYTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCSAFEEPTDAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLATTORNEYFEEAMT         NUMBER(15,2)    NOT NULL,
  LOANDTLINITFEEPCT             NUMBER(7,5)     NOT NULL,
  LOANDTLINITFEEDOLLRAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLFUNDFEEPCT             NUMBER(7,5)     NOT NULL,
  LOANDTLFUNDFEEDOLLRAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLCBNKNM                 VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCBNKMAILADRSTR1NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCBNKMAILADRSTR2NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCBNKMAILADRCTYNM       VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCBNKMAILADRSTCD        CHAR(2 BYTE)    NOT NULL,
  LOANDTLCBNKMAILADRZIPCD       CHAR(5 BYTE)    NOT NULL,
  LOANDTLCBNKMAILADRZIP4CD      CHAR(4 BYTE)    NOT NULL,
  LOANDTLCACCTNM                VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCOLDACCT               NUMBER          NOT NULL,
  LOANDTLCROUTSYM               CHAR(15 BYTE)   NOT NULL,
  LOANDTLCTRANSCD               CHAR(15 BYTE)   NOT NULL,
  LOANDTLCATTN                  VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLRBNKNM                 VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLRBNKMAILADRSTR1NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLRBNKMAILADRSTR2NM      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLRBNKMAILADRCTYNM       VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLRBNKMAILADRSTCD        CHAR(2 BYTE)    NOT NULL,
  LOANDTLRBNKMAILADRZIPCD       CHAR(5 BYTE)    NOT NULL,
  LOANDTLRBNKMAILADRZIP4CD      CHAR(4 BYTE)    NOT NULL,
  LOANDTLRACCTNM                VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLROLDACCT               NUMBER          NOT NULL,
  LOANDTLRROUTSYM               CHAR(15 BYTE)   NOT NULL,
  LOANDTLTRANSCD                CHAR(15 BYTE)   NOT NULL,
  LOANDTLRATTN                  VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLPYMT1AMT               NUMBER(15,2)    NOT NULL,
  LOANDTLPYMTDT1X               CHAR(8 BYTE)    NOT NULL,
  LOANDTLPRIN1AMT               NUMBER(15,2)    NOT NULL,
  LOANDTLINT1AMT                NUMBER(15,2)    NOT NULL,
  LOANDTLCSA1PCT                NUMBER(7,5)     NOT NULL,
  LOANDTLCSADOLLR1AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCDC1PCT                NUMBER(7,5)     NOT NULL,
  LOANDTLCDCDOLLR1AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTNM                 VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCURCSAFEEAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCURCDCFEEAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLCURRESRVAMT            NUMBER(15,2)    NOT NULL,
  LOANDTLCURFEEBALAMT           NUMBER(15,2)    NOT NULL,
  LOANDTLAMPRINLEFTAMT          NUMBER(15,2)    NOT NULL,
  LOANDTLAMTHRUDTX              CHAR(8 BYTE)    NOT NULL,
  LOANDTLAPPIND                 CHAR(1 BYTE)    NOT NULL,
  LOANDTLSPPIND                 CHAR(1 BYTE)    NOT NULL,
  LOANDTLSPPLQDAMT              NUMBER(15,2)    NOT NULL,
  LOANDTLSPPAUTOPYMTAMT         NUMBER(15,2)    NOT NULL,
  LOANDTLSPPAUTOPCT             NUMBER(7,5)     NOT NULL,
  LOANDTLSTMTINT1AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT2AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT3AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT4AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT5AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT6AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT7AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT8AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT9AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT10AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT11AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT12AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTINT13AMT           NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTAVGMOBALAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLAMPRINAMT              NUMBER(15,2)    NOT NULL,
  LOANDTLAMINTAMT               NUMBER(15,2)    NOT NULL,
  LOANDTLREFIIND                CHAR(1 BYTE)    NOT NULL,
  LOANDTLSETUPIND               CHAR(1 BYTE)    NOT NULL,
  LOANDTLCREATDT                DATE            NOT NULL,
  LOANDTLLMAINTNDT              DATE            NOT NULL,
  LOANDTLCONVDT                 DATE            NOT NULL,
  LOANDTLPRGRM                  CHAR(3 BYTE)    NOT NULL,
  LOANDTLNOTEMOPYMTAMT          NUMBER(15,2)    NOT NULL,
  LOANDTLDBENTRMATDT            DATE            NOT NULL,
  LOANDTLNOTEMATDT              DATE            NOT NULL,
  LOANDTLRESRVDEPPCT            NUMBER(7,5)     NOT NULL,
  LOANDTLCDCPFEEPCT             NUMBER(7,5)     NOT NULL,
  LOANDTLCDCPFEEDOLLRAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLDUEBORRAMT             NUMBER(15,2)    NOT NULL,
  LOANDTLAPPVDT                 DATE            NOT NULL,
  LOANDTLLTFEEIND               CHAR(1 BYTE)    NOT NULL,
  LOANDTLACHPRENTIND            CHAR(1 BYTE)    NOT NULL,
  LOANDTLAMSCHDLTYP             CHAR(2 BYTE)    NOT NULL,
  LOANDTLACHBNKNM               VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHBNKMAILADRSTR1NM    VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHBNKMAILADRSTR2NM    VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHBNKMAILADRCTYNM     VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHBNKMAILADRSTCD      CHAR(2 BYTE)    NOT NULL,
  LOANDTLACHBNKMAILADRZIPCD     CHAR(5 BYTE)    NOT NULL,
  LOANDTLACHBNKMAILADRZIP4CD    CHAR(4 BYTE)    NOT NULL,
  LOANDTLACHBNKBR               CHAR(15 BYTE)   NOT NULL,
  LOANDTLACHBNKACTYP            CHAR(1 BYTE)    NOT NULL,
  LOANDTLACHBNKACCT             VARCHAR2(30 BYTE) NOT NULL,
  LOANDTLACHBNKROUTNMB          CHAR(15 BYTE)   NOT NULL,
  LOANDTLACHBNKTRANS            CHAR(15 BYTE)   NOT NULL,
  LOANDTLACHBNKIDNOORATTEN      VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHDEPNM               VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLACHSIGNDT              DATE            NOT NULL,
  LOANDTLBORRNM2                VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSBCNM2                 VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCACCTNMB               VARCHAR2(20 BYTE) NOT NULL,
  LOANDTLRACCTNMB               VARCHAR2(20 BYTE) NOT NULL,
  LOANDTLINTLENDRIND            CHAR(1 BYTE)    NOT NULL,
  LOANDTLINTRMLENDR             NUMBER          NOT NULL,
  LOANDTLPYMTRECV               NUMBER          NOT NULL,
  LOANDTLTRMYR                  NUMBER          NOT NULL,
  LOANDTLLOANTYP                CHAR(1 BYTE)    NOT NULL,
  LOANDTLMLACCTNMB              VARCHAR2(20 BYTE) NOT NULL,
  LOANDTLEXCESSDUEBORRAMT       NUMBER(15,2)    NOT NULL,
  LOANDTL503SUBTYP              CHAR(1 BYTE)    NOT NULL,
  LOANDTLLPYMTDT                DATE            NOT NULL,
  LOANDTLPOOLSERS               CHAR(7 BYTE)    NOT NULL,
  LOANDTLSBAFEEPCT              NUMBER(7,5)     NOT NULL,
  LOANDTLSBAPAIDTHRUDTX         CHAR(8 BYTE)    NOT NULL,
  LOANDTLSBAFEEAMT              NUMBER(15,2)    NOT NULL,
  LOANDTLPRMAMT                 NUMBER(15,2)    NOT NULL,
  LOANDTLLENDRSBAFEEAMT         NUMBER(15,2)    NOT NULL,
  LOANDTLWITHHELDIND            CHAR(1 BYTE)    NOT NULL,
  LOANDTLPRGRMTYP               CHAR(3 BYTE)    NOT NULL,
  LOANDTLLATEIND                CHAR(1 BYTE)    NOT NULL,
  LOANDTLCMNT1                  VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLCMNT2                  VARCHAR2(80 BYTE) NOT NULL,
  LOANDTLSOD                    CHAR(4 BYTE)    NOT NULL,
  LOANDTLTOBECURAMT             NUMBER(15,2)    NOT NULL,
  LOANDTLGNTYREPAYPTDAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLCDCPAIDTHRUDTX         CHAR(8 BYTE)    NOT NULL,
  LOANDTLCSAPAIDTHRUDTX         CHAR(8 BYTE)    NOT NULL,
  LOANDTLINTPAIDTHRUDTX         CHAR(8 BYTE)    NOT NULL,
  LOANDTLSTRTBAL1AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTRTBAL2AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTRTBAL3AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTRTBAL4AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSTRTBAL5AMT            NUMBER(15,2)    NOT NULL,
  LOANDTLSBCIDNMB               CHAR(5 BYTE)    NOT NULL,
  LOANDTLACHLASTCHNGDT          DATE            NOT NULL,
  LOANDTLPRENOTETESTDT          DATE            NOT NULL,
  LOANDTLPYMTNMBDUE             NUMBER          NOT NULL,
  LOANDTLRESRVAMT               NUMBER(15,2)    NOT NULL,
  LOANDTLFEEBASEAMT             NUMBER(15,2)    NOT NULL,
  LOANDTLSTATOFLOANCD           CHAR(2 BYTE)    NOT NULL,
  LOANDTLACTUALCSAINITFEEAMT    NUMBER(15,2)    NOT NULL,
  LOANDTLSTATDT                 DATE            NOT NULL,
  LOANDTLSTMTCLSBALAMT          NUMBER(15,2)    NOT NULL,
  LOANDTLSTMTCLSDT              DATE            NOT NULL,
  LOANDTLLASTDBPAYOUTAMT        NUMBER(15,2)    NOT NULL,
  LOANDTLCHEMICALBASISDAYS      NUMBER          NOT NULL,
  CREATUSERID                   VARCHAR2(32 BYTE) NOT NULL,
  CREATDT                       DATE            DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID                VARCHAR2(32 BYTE) NOT NULL,
  LASTUPDTDT                    DATE            DEFAULT sysdate               NOT NULL
)
TABLESPACE CSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX CSA.TEMPLNDBTBL_PK ON CSA.TEMPLNDBTBL
(LOANNMB)
LOGGING
TABLESPACE CSAINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE CSA.TEMPLNDBTBL ADD (
  CONSTRAINT TEMPLNDBTBL_PK
  PRIMARY KEY
  (LOANNMB)
  USING INDEX CSA.TEMPLNDBTBL_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.TEMPLNDBTBL TO CSADEVROLE;

GRANT SELECT ON CSA.TEMPLNDBTBL TO CSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.TEMPLNDBTBL TO CSAUPDTROLE;

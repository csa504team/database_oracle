DROP TABLE CSA.XOVER_SOFVFTPOTBL CASCADE CONSTRAINTS;

CREATE TABLE CSA.XOVER_SOFVFTPOTBL
(
  PREPOSTRVWRECRDTYPCD            CHAR(1 BYTE),
  PREPOSTRVWTRANSCD               CHAR(15 BYTE),
  PREPOSTRVWTRANSITROUTINGNMB     CHAR(15 BYTE),
  PREPOSTRVWTRANSITROUTINGCHKDGT  CHAR(1 BYTE),
  PREPOSTRVWBNKACCTNMB            VARCHAR2(20 BYTE),
  PREPOSTRVWPYMTAMT               NUMBER(15,2),
  LOANNMB                         CHAR(10 BYTE) NOT NULL,
  PREPOSTRVWINDVLNM               VARCHAR2(80 BYTE),
  PREPOSTRVWCOMPBNKDISC           CHAR(2 BYTE),
  PREPOSTRVWADDENDRECIND          NUMBER,
  PREPOSTRVWTRACENMB              NUMBER,
  PREPOSTRVWCMNT                  VARCHAR2(80 BYTE),
  CREATUSERID                     VARCHAR2(32 BYTE),
  CREATDT                         DATE,
  LASTUPDTUSERID                  VARCHAR2(32 BYTE),
  LASTUPDTDT                      DATE
)
TABLESPACE CSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON CSA.XOVER_SOFVFTPOTBL TO CSADEVROLE;

GRANT SELECT ON CSA.XOVER_SOFVFTPOTBL TO CSAREADALLROLE;

GRANT SELECT ON CSA.XOVER_SOFVFTPOTBL TO CSAUPDTROLE;

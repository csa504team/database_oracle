ALTER TABLE CSA.SOFVPREXTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE CSA.SOFVPREXTBL CASCADE CONSTRAINTS;

CREATE TABLE CSA.SOFVPREXTBL
(
  LOANNMB         CHAR(10 BYTE)                 NOT NULL,
  LOANREST        VARCHAR2(80 BYTE)             NOT NULL,
  CREATUSERID     VARCHAR2(32 BYTE)             NOT NULL,
  CREATDT         DATE                          DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             NOT NULL,
  LASTUPDTDT      DATE                          DEFAULT sysdate               NOT NULL
)
TABLESPACE CSADATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX CSA.SOFVPREXTBL_PK ON CSA.SOFVPREXTBL
(LOANNMB)
LOGGING
TABLESPACE CSAINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE CSA.SOFVPREXTBL ADD (
  CONSTRAINT SOFVPREXTBL_PK
  PRIMARY KEY
  (LOANNMB)
  USING INDEX CSA.SOFVPREXTBL_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVPREXTBL TO CSADEVROLE;

GRANT SELECT ON CSA.SOFVPREXTBL TO CSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CSA.SOFVPREXTBL TO CSAUPDTROLE;

-- KU$VAT tables are Oracle generated and used for exporting views as tables
--   if the view structure changes, using the old matching KU table(s) is bad
-- In this case, col STGCNT_SOFVLND1TBL is only part of XOVER_STATUS 
begin
  for drop_ku_table in
    (select distinct 'drop table '||owner||'.'||table_name drop_stmt
       from dba_tab_columns where column_name='STGCNT_SOFVLND1TBL'
         and table_name like 'KU$VAT%')
  loop
    execute immediate drop_ku_table.drop_stmt;
  end loop;
end;
/
             
CREATE OR REPLACE PROCEDURE LOAN.VALIDATESUBPROCDCSP (
   p_Identifier             NUMBER := 0,
   p_LoanAppNmb             NUMBER,
   p_TransInd               NUMBER := 0,
   p_LoanAppRecvDt          DATE := NULL,
   p_VALIDATIONTYP      IN  CHAR := NULL,
   p_RetVal             OUT NUMBER,
   p_ErrSeqNmb          OUT NUMBER,
   p_SelCur1            OUT SYS_REFCURSOR,
   p_SelCur2            OUT SYS_REFCURSOR)
AS


   /*CURSOR SubProcd_Cur1
   IS
      SELECT LOANSUBPROCDID,
             LoanProcdAmt,
             LoanProcdTypCd,
             ProcdTypCd,
             LoanProcdOthTypTxt
        FROM LoanGntySubProcdTbl
       WHERE LoanAppNmb = p_LoanAppNmb;*/
       
    cursor fetch_procd_rec is
    select LOANPROCDSEQNMB,LoanProcdAmt,ProcdTypcd,LoanProcdTypCd  FROM LOAN.LOANGNTYPROCDTBL WHERE LoanAppNmb = p_LoanAppNmb;
   sub_recs_sum number:=0;  
   v_ErrCd number:=0;
   v_FININSTRMNTTYPIND             CHAR (1);
 
BEGIN
   p_ErrSeqNmb := 0;
    for rec in fetch_procd_rec
        loop
            select nvl(sum(LOANPROCDAMT),0) into sub_recs_sum from LOAN.LOANGNTYSUBPROCDTBL where 
            LOANPROCDTYPCD=rec.LOANPROCDTYPCD and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= rec.PROCDTYPCD;         	
                 IF rec.PROCDTYPCD = 'E' AND sub_recs_sum != rec.LoanProcdAmt THEN
                  BEGIN
                     p_ErrSeqNmb := p_ErrSeqNmb + 1;
                     v_ErrCd := 4289;

                     LoanGntyValidErrInsCSP (p_LoanAppNmb,
                                             v_ErrCd,
                                             2,
                                             p_VALIDATIONTYP,
                                             TO_CHAR (rec.PROCDTYPCD)||TO_CHAR (rec.LOANPROCDTYPCD),
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             v_FININSTRMNTTYPIND);
                  END;
                END IF;
        end loop;

            
 
      IF (p_Identifier = 11) OR (p_Identifier = 111)
      THEN
         BEGIN
          
            OPEN p_SelCur1 FOR
                 SELECT LoanGntyValidErrSeqNmb ErrSeqNmb,
                        ErrTypCd,
                        ErrCd,
                        LoanGntyValidErrTxt ErrTxt
                   FROM LoanGntyValidErrTbl
                  WHERE LoanAppNmb = p_LoanAppNmb AND ERRTYPIND = 'F'
               ORDER BY LoanGntyValidErrSeqNmb;

            OPEN p_SelCur2 FOR
                 SELECT LoanGntyValidErrSeqNmb ErrSeqNmb,
                        ErrTypCd,
                        ErrCd,
                        LoanGntyValidErrTxt ErrTxt
                   FROM LoanGntyValidErrTbl
                  WHERE LoanAppNmb = p_LoanAppNmb AND ERRTYPIND = 'W'
               ORDER BY LoanGntyValidErrSeqNmb;
          END;
      END IF;

      p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
      p_RetVal := NVL (p_RetVal, 0);
END VALIDATESUBPROCDCSP;
/


GRANT EXECUTE ON LOAN.VALIDATESUBPROCDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.VALIDATESUBPROCDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATESUBPROCDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.VALIDATESUBPROCDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATESUBPROCDCSP TO UPDLOANROLE;
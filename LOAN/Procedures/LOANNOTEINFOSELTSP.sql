CREATE OR REPLACE PROCEDURE LOAN.LOANNOTEINFOSELTSP
(
  p_LOANNOTEID IN NUMBER,
  p_GPNMBR IN CHAR,
  p_RECEIVEDDTMIN IN DATE,
  p_RECEIVEDDTMAX IN DATE,
  p_TRUSTEENM IN VARCHAR2,
  p_RELEASEDDT IN DATE,
  p_AIRBILLNMBR IN VARCHAR2,
  p_MODDT IN DATE,
  p_BORROWER IN VARCHAR2,
  p_BNKNM IN VARCHAR2,
  p_AMOUNT IN VARCHAR2,
  p_NOTETYPE IN VARCHAR2,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		 SELECT 
		 LOANNOTEID,
        LOANNMB, RECEIVEDDT, TRUSTEENM, 
        RELEASEDDT, AIRBILLNMBR, MODDT, 
        CMNTS,BORROWER, 
        AMOUNT, BNKNM, NOTETYPE
        FROM LOAN.LOANNOTEINFOTBL
       WHERE   (LOANNOTEID = p_LOANNOTEID or p_LOANNOTEID is null) and
               (lower(LoanNmb) like '%'||lower(p_GPNMBR)||'%' or p_GPNMBR is null) and
               ((trunc(RECEIVEDDT) >= p_RECEIVEDDTMIN or p_RECEIVEDDTMIN is null) and (trunc(RECEIVEDDT) <= p_RECEIVEDDTMAX or p_RECEIVEDDTMAX is null)) and
               (lower(TRUSTEENM) like '%'||lower(p_TRUSTEENM)||'%' or p_TRUSTEENM is null) and
               (RELEASEDDT = p_RELEASEDDT or p_RELEASEDDT is null) and
               (lower(AIRBILLNMBR) like '%'||lower(p_AIRBILLNMBR)||'%' or p_AIRBILLNMBR is null) and
               (MODDT = p_MODDT or p_MODDT is null) and
               (lower(BORROWER) like '%'||lower(p_BORROWER)||'%' or p_BORROWER is null) and
               (AMOUNT like '%'||p_AMOUNT||'%' or p_AMOUNT is null) and
               (lower(BNKNM) like '%'||lower(p_BNKNM)||'%' or p_BNKNM is null) and
               (NOTETYPE = p_NOTETYPE or p_NOTETYPE is null);
               

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/


GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO UPDLOANROLE;

GRANT EXECUTE ON LOAN.LOANNOTEINFOSELTSP TO ONPREADROLE;


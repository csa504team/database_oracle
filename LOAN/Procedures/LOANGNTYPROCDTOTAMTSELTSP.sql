CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOAN.LOANGNTYPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOAN.LOANGNTYSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANGNTYPROCDTOTAMTSELTSP;

    /* Select from LOAN.LOANGNTYPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDTOTAMTSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDTOTAMTSELTSP TO UPDLOANROLE;
CREATE OR REPLACE PROCEDURE LOAN.LoanAgntINSTSP (p_identifier             IN     NUMBER := 0,
                                                 p_retval                    OUT NUMBER,
                                                 p_LOANAPPNMB                    NUMBER := NULL,
                                                 p_LOANAGNTSEQNMB            OUT NUMBER,
                                                 p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                 p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                 p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                 p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                 p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                 p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                 p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                 p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                 p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                 p_LOANAGENTTYPEOTHER             VARCHAR2 := NULL,
                                                 p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                 p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                 p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                 p_LOANAGNTID                    NUMBER := NULL,
                                                 p_CREATUSERID                   VARCHAR2 := NULL,
                                                 p_CREATDT                       DATE := NULL,
                                                 p_LASTUPDTUSERID                VARCHAR2 := NULL,
                                                 p_LASTUPDTDT                    DATE := NULL)
AS
/*SS--07/16/2018--OPSMDEV1862
--08/2/2018--OPSMDEV 1873 Added LoanAgntId
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAIN for PII columns
*/
BEGIN
    SAVEPOINT LoanAgntINS;

    IF p_Identifier = 0
    THEN
        BEGIN
            INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                          LoanAgntSeqNmb,
                                          LOANAGNTBUSPERIND,
                                          LOANAGNTNM,
                                          LOANAGNTCNTCTFIRSTNM,
                                          LOANAGNTCNTCTMIDNM,
                                          LOANAGNTCNTCTLASTNM,
                                          LOANAGNTCNTCTSFXNM,
                                          LOANAGNTADDRSTR1NM,
                                          LOANAGNTADDRSTR2NM,
                                          LOANAGNTADDRCTYNM,
                                          LOANAGNTADDRSTCD,
                                          LOANAGNTADDRSTNM,
                                          LOANAGNTADDRZIPCD,
                                          LOANAGNTADDRZIP4CD,
                                          LOANAGNTADDRPOSTCD,
                                          LOANAGNTADDRCNTCD,
                                          LOANAGNTTYPCD,
                                          LOANAGNTDOCUPLDIND,
                                          LOANCDCTPLFEEIND,
                                          LOANCDCTPLFEEAMT,
                                          LOANAGNTID,
                                          CreatUserId,
                                          CreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LOANAGNTOTHTYPTXT)
                 VALUES (p_LOANAPPNMB,
                         (SELECT NVL (MAX (LoanAgntSeqNmb), 0) + 1
                            FROM LOAN.LoanAgntTBL z
                           WHERE z.loanappnmb = p_loanappnmb),
                         p_LOANAGNTBUSPERIND,
                         p_LOANAGNTNM,
                         OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                         p_LOANAGNTCNTCTMIDNM,
                         OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                         p_LOANAGNTCNTCTSFXNM,
                         OcaDataIn (p_LOANAGNTADDRSTR1NM),
                         OcaDataIn (p_LOANAGNTADDRSTR2NM),
                         p_LOANAGNTADDRCTYNM,
                         p_LOANAGNTADDRSTCD,
                         p_LOANAGNTADDRSTNM,
                         p_LOANAGNTADDRZIPCD,
                         p_LOANAGNTADDRZIP4CD,
                         p_LOANAGNTADDRPOSTCD,
                         p_LOANAGNTADDRCNTCD,
                         p_LOANAGNTTYPCD,
                         p_LOANAGNTDOCUPLDIND,
                         p_LOANCDCTPLFEEIND,
                         p_LOANCDCTPLFEEAMT,
                         (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                         p_CreatUserId,
                         SYSDATE,
                         p_LastUpdtUserId,
                         SYSDATE,
                         p_LOANAGENTTYPEOTHER);

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            INSERT INTO LOAN.LoanAgntTBL (LOANAPPNMB,
                                          LoanAgntSeqNmb,
                                          LOANAGNTBUSPERIND,
                                          LOANAGNTNM,
                                          LOANAGNTCNTCTFIRSTNM,
                                          LOANAGNTCNTCTMIDNM,
                                          LOANAGNTCNTCTLASTNM,
                                          LOANAGNTCNTCTSFXNM,
                                          LOANAGNTADDRSTR1NM,
                                          LOANAGNTADDRSTR2NM,
                                          LOANAGNTADDRCTYNM,
                                          LOANAGNTADDRSTCD,
                                          LOANAGNTADDRSTNM,
                                          LOANAGNTADDRZIPCD,
                                          LOANAGNTADDRZIP4CD,
                                          LOANAGNTADDRPOSTCD,
                                          LOANAGNTADDRCNTCD,
                                          LOANAGNTTYPCD,
                                          LOANAGNTDOCUPLDIND,
                                          LOANCDCTPLFEEIND,
                                          LOANCDCTPLFEEAMT,
                                          LOANAGNTID,
                                          CreatUserId,
                                          CreatDt,
                                          LastUpdtUserId,
                                          LastUpdtDt,
                                          LOANAGNTOTHTYPTXT)
                SELECT p_LOANAPPNMB,
                       (  NVL ((SELECT MAX (LoanAgntSeqNmb)
                                  FROM loan.loanagnttbl z
                                 WHERE z.loanappnmb = p_loanappnmb),
                               0)
                        + 1)
                           AS loanagntseqnmb,
                       p_LOANAGNTBUSPERIND,
                       p_LOANAGNTNM,
                       OcaDataIn (p_LOANAGNTCNTCTFIRSTNM),
                       p_LOANAGNTCNTCTMIDNM,
                       OcaDataIn (p_LOANAGNTCNTCTLASTNM),
                       p_LOANAGNTCNTCTSFXNM,
                       OcaDataIn (p_LOANAGNTADDRSTR1NM),
                       OcaDataIn (p_LOANAGNTADDRSTR2NM),
                       p_LOANAGNTADDRCTYNM,
                       p_LOANAGNTADDRSTCD,
                       p_LOANAGNTADDRSTNM,
                       p_LOANAGNTADDRZIPCD,
                       p_LOANAGNTADDRZIP4CD,
                       p_LOANAGNTADDRPOSTCD,
                       p_LOANAGNTADDRCNTCD,
                       p_LOANAGNTTYPCD,
                       p_LOANAGNTDOCUPLDIND,
                       p_LOANCDCTPLFEEIND,
                       p_LOANCDCTPLFEEAMT,
                       (SELECT NVL (MAX (LoanAgntID), 0) + 1 FROM LOAN.LoanAgntInfoCdTBL),
                       p_CreatUserId,
                       SYSDATE,
                       p_LastUpdtUserId,
                       SYSDATE,
                       p_LOANAGENTTYPEOTHER
                  FROM DUAL;

            p_retval := SQL%ROWCOUNT;

            SELECT MAX (LoanAgntSeqNmb)
              INTO p_retval
              FROM LoanAgntTBL
             WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    p_retval := NVL (p_retval, 0);
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            RAISE;
            ROLLBACK TO LoanAgntINS;
        END;
END LoanAgntINSTSP;
/

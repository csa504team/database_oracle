CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSELTSP (p_Identifier   IN     NUMBER := 0,
                                                 p_LoanAppNmb   IN     NUMBER := 0,
                                                 p_LoanNmb      IN     CHAR := NULL,
                                                 p_LocId        IN     NUMBER := NULL,
                                                 p_PrtId        IN     NUMBER := NULL,
                                                 p_FromDate     IN     DATE := NULL,
                                                 p_ToDate       IN     DATE := NULL,
                                                 p_FisclYrNmb   IN     NUMBER := NULL,
                                                 p_PrcsMthdCd          CHAR := NULL,
                                                 p_RetVal          OUT NUMBER,
                                                 p_SelCur          OUT SYS_REFCURSOR) AS
    /*
      Created by:
      Created on :
      Purpose    :
      Parameters:
      Modified by: APK 09/22/2010- modified Identifier 18 to use proper left outer join as the query was not returning data when there is no data in LoanTransTbl.
     APK         03/18/2011      Modified to add LoanAppMatExtDt
     APK 05/06/2011 -- modified to remove column OrigntnLocId as it was removed from the table.
     APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes.
     APK -- 11/03/2011-- modified to add new column GtyFeeUnCollAmt as the fee was not correctly calculated from elips.
     THis column will now be synched with elips.
     APk -- 11/15/2011 -- ID = 22, modified to remove the condition ' LoanStatCd  NOT IN (4, 5)' so as to display irrespective of the status.
     APK -- 11/18/2011 -- modified ID = 22 to add additional field PrtId.
     APK -- 11/29/2011-- modifed ID = 25 to use proper schema for sbaref.loanstatcdtbl instead of Loan.
     SP-----1/5/2011-----Modified ID=0 to add new column LOANPYMTTRANSCD
     SP-----2/2/2012-----Modified ID=18  to add new column LOANPYMTTRANSCD
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     SP --- 03/05/2012 -- Modified ID=0,18 to add new column LOANCOFSEFFDT
     SP --- 04/18/2012 -- Modified ID=0,18 to add new column LoanAppNetEarnInd
     SP --- 05/01/2012 -- Modified ID=0,18 to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
    LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
     RGG --- 10/01/2012 -- Changes ID = 0,17,18 to add LoanAppActPymtAmt field.
    SP -- 04/04/2013 -- ADDED UNDERWRITINGBY FOR DISPLAY PURPOSE
    RSURAPA -- 05/16/2013 Added selection of notedt from the loangntytbl for p_Identifier 18
    RSURAPA -- 05/22/2013 The column name notedt was changed to loangntynotedt in the loangntytbl.
    SP --- 05/29/2013 -- removed Changes ID = 0,17,18 to add LoanAppActPymtAmt field.
    RSURAPA -- 05/17/2013 -- Added select of new column LOANGNTYMATSTIND from the loangntytbl for p_Identifier 18
    RGG -- 09/19/2013 -- aded identifier 26 to Return the PMT close date and close reason codes for a given loan number
    SP---10/07/2013---Removed street number and street suffix name as part of appending these to street name changes
    SP -- 10/29/2013 -- Modified to add new column LOANAPPINTDTLCD to id=0
    SP -- 10/30/2013 -- Modified to add LOANDISASTRAPPNMB to id =0 as these values are passed into LoanGntyPreInsTsp in the case of cancel and reinstate web screens.
    RGG -- 11/05/2013 -- Added Id 27 to return the current LOANAPPINTDTLCD and LOANINTDTLDESCTXT.
    RSURAPA -- 11/09/13 -- modified identifier 18 to return additional column LoanAppIntDtlCd
    sp -- 11/27/2013 --Removed refernces to  columns :IMRtTypCd(0,17,18,24),LoanAppFixVarInd(0,17,18,24),
    LoanAppBaseRt(0,17,18,24),LoanAppInitlIntPct(0,17,18,24),LoanAppInitlSprdOvrPrimePct(0,17,18,24),LoanAppAdjPrdCd(0,18,24)
    SP -- 02/05/2014 Modified id=18 as the field name LoanGntyFeeWavrInd  was changed to LOANOrigntnFeeInd
    SP -- 04/03/2014 Modified id =0,18 to add LOANGNTYPURISSIND,LOANGNTYPURISSDT to alert a Loan Officer performing a purchase review in GPTS of possible guaranty issues
    RGG -- 04/04/2014 -- Removed NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM
    SB -- 04/04/2014 removed Loanappfrnchsind
    PSP -- 04/17/2014 -- Added license type,draw type,SBIC pooling date to id = 0,18 and added id = 28 to get all draw downs for a commitment
    SB -- Added Identifier 38 for Extract XML
    PSP -- 04/11/2014 -- Added draw type description to id = 28
    RGG -- 01/26/2015 -- Added LOANAUTOAMSSTOPIND to Id 18.
    PSP -- 02/25/2015 -- Modified id = 18,39 to add loanpurofccd to capture purchasing office using office code
    of the recommending official so would not lose count of the purchase once the file is assigned to the appropriate servicing office
    SB -- 03/24/2015 -- Added extra column a.LoanAppDisasterCntrlNmb for idnetifier 38
    RGG -- 05/19/2015 -- In order to allow lender updates to disbursed loans if they were lender-underwritten,UndrwritngBy is added to ID 18.
    NK --03/21/2016 --TO_CHAR (ColumnName, '990.999')  to TO_CHAR (ColumnName, 'FM990.000')
    NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
    RGG -- 04/04/2016 -- Added LOANLNDRASSGNLTRDT wherever LOANCOFSEFFDT is present.
    MZ -- 09/22/2016 -- Added identifier 40 to return micro lender with 8 digit loan #
    RGG -- 12/20/2016 -- OPSMDEV 1275 Added Interest Rate, Status Description to ID 18
    JP -- 2/27/2017 -- OPSMDEV-1341 Added NAICSYrNmb, 3 LoanFinanclStmt columns, LoanGntyNoteDt, LoanTotUndisbAmt and LoanAppProjCntyCd to Identifier 38
    RY --03/02/2017 -- OPSMDEV 1400--Added new column LoanGntyCsaAmtRcvd to ID 0,18,28,38
    JP -- 3/06/2017 -- OPSMDEV-1341 Added LoanAppFundDt to Identifier 38
    RY -- 03/17/2017 -- OPSMDEV 1401 -- Changed the references for 'EconDevObjctCd' in ID 38.
    SS--03/27/2017 --OPSMDEV 1329 Added identifier 29
    MZ -- 03/30/2017, Identifier 38, added LoanOutBalAmt and LoanOutBalDt
    JP -- 04/06/2017 -- Added LoanAppFirstDisbDt and LoanDisbInd to identifier 38
    RY--04/10/2017 --CAFSOPER518--Added LoanTypInd  to ID 0
    BJR -- 04/25/2017 -- CAFSOPER719 -- Added p_Identifier = 3 to Return LoanGntyTypInd from Loan.LoanGntyTbl
    SS--05/19/2017 --OPSMDEV1436 Added CDCServFeePct to identifier 0 and created identifier 41
    RY--06/07/2017-- OPSMDEV 1440--Added LOANGNTY1201ELCTRNCSTMTIND to ID 0 and 38
    NK -- 07/19/2017--CAFSOPER 805 -- Added LoanAppBalToBorrAmt,LoanAppCDCGrossDbentrAmt in ID 18,38
    NK--07/19/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt in ID 0, 18
    NK --08/08/2017-- Added LOANGNTY1201ELCTRNCSTMTIND to ID 18
    NK-- 08/22/2017--OPSMDEV-1439--Added  LOANPURCHSDIND and LOANGNTY1201ELCTRNCSTMTIND to ID 41
    SS--11/16/2017--OPSMDEV- 1568--Added LOANBUSESTDT to identifier 38
    NK--12/01/2017--OPSMDEV-- 1598--Added LoanMailAddrsInvldInd to Id 0, 38
    RY--01/16/2018--OPSMDEV--1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty to Id 0,17,18,24,28 and 38
    NK--02/05/2018--OPSMDEV --1507 -- Added LoanGntyFeeBillAdjAmt to ID 0, 18
    SS--03/23/2018--OPSMDEV 1728 --Added LoanGntyDbentrIntRt to identifier 0 and 18
    JP -- 05/07/2018 -- OPSMDEV 1717 -  LoanIntBaseRt and LoanIntRt fields formatted to 5 digit decimal as 'FM990.00000'
   SS--0/13/2018 --OPSMDEV 1861-- Added LoanAgntInvlvdInd to ID 0,18,41 and 38
   NK--08/15/2018--CAFSOPER 2021--added new id 42
   SS--10/26/2018--OPSMDEV 1965 added LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  to identifier 0and 38
   RY--07/08/2019--opsmdev-2240:: Added LoanGntycorrespLangCd  to 0,18 and 38
   SS--04/03/2020 Added LoanAppMonthPayroll to id 0 and 38 CARESACT - 66
   SS--04/30/2020 Added Loan1502CertInd to identifier 0 and 18
   SS--05/06/2020 Added   lendrRebateFeeAmt,LendrRebatefeePaidDt  CARESACT 105
   JP -- 5/22/2020 Rewrote the query for id 20
   JP -- 06/02/2020 id 18 modified to get ACH info from PIMS for PrcsMthdCd PPP
   JP -- 07/23/2020 CAFSOPER-3775 added DspstnStatCd to ID 0
   SL -- 08/28/2020 CARESACT-625 added DspstnStatCd to ID 38
   JP -- 09/17/2020 CAFSOPER-3937 added DspstnStatDescTxt to id 18
   JP--1/9/2021  - SODSTORY 376 added column LoanAppUsrEsInd to id 0,18,38
   SL--02/01/2021 SODSTORY 446 added column LoanGntyPayRollChngInd
   SL --03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT
   JP -- 05/11/2021 SODSTORY-857 ID 20   -- Removed TRUNC from LoanAppFundDt and LOANSTATDT where clause
   JP -- 05/27/2021 CAFSOPER-4119 ID 21   -- Removed TRUNC from LoanAppFundDt and LOANSTATDT where clause
   */
    v_FromDate   DATE;
    v_ToDate     DATE;
    v_fy         NUMBER;
BEGIN
    /* Select Row On the basis of Key*/
    --IF p_Identifier = 0 THEN
    CASE p_Identifier
        WHEN 0 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)              LoanAppRqstAmt,
                                         LoanAppSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)            LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         LoanAppRqstMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)              LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)              LoanGntyFeeAmt,
                                         LocId,
                                         LoanAppLSPHQLocId,
                                         PrtId,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)               LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanTotUndisbAmt,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanDisbInd,
                                         LoanSoldScndMrktInd,
                                         ACHRTNGNMB,
                                         ACHACCTNMB,
                                         ACHACCTTYPCD,
                                         ACHTinNmb,
                                         LoanIntPaidAmt,
                                         LoanIntPaidDt,
                                         RecovInd,
                                         LoanIntPaidAmt,
                                         LoanIntPaidDt,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)           LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)      LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)        LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)        LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)        LoanAppCDCClsCostAmt,
                                         ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)        LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)     LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)           LoanAppContribAmt,
                                         LoanAppOutPrgrmAreaOfOperInd,
                                         LoanAppParntLoanNmb,
                                         LoanAppPrtTaxId,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         POOLGROSSINITLINTPCT,
                                         POOLLENDRGROSSINITLAMT,
                                         POOLLENDRPARTINITLAMT,
                                         POOLORIGPARTINITLAMT,
                                         POOLSBAGNTYBALINITLAMT,
                                         POOLLENDRPARTCURRAMT,
                                         POOLORIGPARTCURRAMT,
                                         POOLLENDRGROSSCURRAMT,
                                         LoanPurchsdInd,
                                         LoanGntyFeeRebateAmt,
                                         LoanAppMatExtDt,
                                         Loan1201NotcInd,
                                         LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         LOANPYMTTRANSCD,
                                         LOANCOFSEFFDT,
                                         SBICLICNSNMB,
                                         LOANMAILSTR1NM,
                                         LOANMAILSTR2NM,
                                         LOANMAILCTYNM,
                                         LOANMAILSTCD,
                                         LOANMAILZIPCD,
                                         LOANMAILZIP4CD,
                                         LOANMAILCNTRYCD,
                                         LOANMAILSTNM,
                                         LOANMAILPOSTCD,
                                         LOANAPPNETEARNIND,
                                         LOANPYMTINSTLMNTTYPCD,
                                         LOANPYMNINSTLMNTFREQCD,
                                         LOANTOTDEFNMB,
                                         LOANTOTDEFMONMB,
                                         UNDRWRITNGBY,
                                         LOANGNTYNOTEDT,
                                         LOANGNTYMATSTIND,
                                         LOANAPPINTDTLCD,
                                         LOANDISASTRAPPNMB,
                                         LOANPYMTREPAYINSTLTYPCD,
                                         LOANGNTYPURISSIND,
                                         LOANGNTYPURISSDT,
                                         LOANSBICLICNSTYP,
                                         loansbicdrawtyp,
                                         loansbicschdlpooldt,
                                         LOANLNDRASSGNLTRDT,
                                         LoanGntyCsaAmtRcvd,
                                         LoanTypInd,
                                         CDCServFeePct,
                                         LOANGNTY1201ELCTRNCSTMTIND,
                                         LoanGntyNoteIntRt,
                                         LoanGntyNoteMntlyPymtAmt,
                                         LoanMailAddrsInvldInd,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanGntyFeeBillAdjAmt,
                                         LoanGntyFeeBillAmt,
                                         LoanGntyDbentrIntRt,
                                         LoanAgntInvlvdInd,
                                         LoanAppExtraServFeeAMT,
                                         LoanAppExtraServFeeInd,
                                         LoanGntycorrespLangCd,
                                         LoanAppMonthPayroll,
                                         LoanGntyPayRollChngInd,
                                         LOANAPPSCHEDCIND,
                                         LOANAPPSCHEDCYR,
                                         LOANAPPGROSSINCOMEAMT,
                                         Loan1502CertInd,
                                         LendrRebateFeeAmt,
                                         LendrRebatefeePaidDt,
                                         DspstnStatCd,
                                         LoanAppUsrEsInd
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 2 THEN
            /*Check The Existance Of Row*/
            BEGIN
                OPEN p_SelCur FOR SELECT 1
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 3 THEN
            /*Return LoanGntyTypInd*/
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppSBAGntyPct, loantypind AS LoanGntyTypInd
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 11 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppEligEvalInd,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)     LoanAppRqstNum,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         LoanAppPrtTaxId
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            --p_RetVal := SQL%ROWCOUNT;
            END;
        WHEN 15 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNm
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 16 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT DISTINCT LoanNmb,
                                                  LoanAppNmb,
                                                  PrgmCd,
                                                  PrcsMthdCd,
                                                  PrtId,
                                                  LocId,
                                                  LoanAppPrcsOfcCd,
                                                  LoanStatCd
                                  FROM Loan.LoanGntyTbl
                                  WHERE     LoanNmb = p_LoanNmb
                                        AND LocId = NVL (p_LocId, LocId)
                                        AND PrtId = NVL (p_PrtId, PrtId);
            END;
        WHEN 17 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNm,
                                         a.PrtId,
                                         a.LocId,
                                         e.PrtLglNm,
                                         PhyAddrStr1Txt,
                                         PhyAddrStr2Txt                                       PhyAddrCtyNm,
                                         PhyAddrStCd,
                                         PhyAddrPostCd,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)               LoanCurrAppvAmt,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)              LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)                LoanCurrGntyFeeAmt,
                                         ROUND (TO_NUMBER (c.LoanTransChngGntyFeeAmt), 2)     LoanInitlGntyFeeAmt,
                                         LoanAppSBAGntyPct                                    LoanCurrSBAGntyPct,
                                         c.LoanTransSBAGntyPct                                LoanInitlSBAGntyPct,
                                         LoanAppRqstMatMoQty                                  LoanCurrRqstMatMoQty,
                                         c.LoanTransMatMoQty                                  LoanInitlRqstMatMoQty,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)                LoanAppPymtAmt,
                                         LoanAppMoIntQty,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppEPCInd,
                                         LoanCollatInd,
                                         LoanAppInjctnInd,
                                         LoanAppEntryDt,
                                         LoanAppRecvDt,
                                         LoanLastAppvDt,
                                         LoanInitlAppvDt,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)                 LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppPrtTaxId,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty
                                  FROM Loan.LoanGntyTbl a,
                                       partner.PhyAddrTbl b,
                                       Loan.LoanTransTbl c,
                                       sbaref.IMRtTypTbl d,
                                       partner.PrtTbl e
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.LoanAppNmb = c.LoanAppNmb
                                        AND c.LoanTransTypCd = 1
                                        AND a.LocId = b.LocId
                                        AND a.PrtId = e.PrtId
                                        AND PhyAddrSeqNmb = (SELECT MAX (PhyAddrSeqNmb)
                                                             FROM partner.PhyAddrTbl z
                                                             WHERE     z.LocId = a.LocId
                                                                   AND z.PrtCntctNmb IS NULL
                                                                   AND ValidAddrTyp = 'Phys');
            END;
        WHEN 18                        /* RSURAPA - 05/16/2013 added selection of loangntynotedt from the loangntytbl */
                THEN                /* RSURAPA -- 06/17/2013 added selection of LOANGNTYMATSTIND from the loangntytbl */
            BEGIN
                OPEN p_SelCur FOR SELECT a.LoanAppNmb,
                                         PrgmCd,
                                         CASE
                                             WHEN a.PrgmCd != 'A' THEN 'N'
                                             WHEN a.LoanAppFundDt < TO_DATE ('20131001', 'yyyymmdd') THEN 'Y'
                                             ELSE a.LoanOngngFeeCollInd
                                         END
                                             AS LoanOngngFeeCollInd,
                                         NVL (LOANOrigntnFeeInd, 'Y')
                                             AS LoanOrigntnFeeInd,
                                         a.PrcsMthdCd,
                                         b.PrcsMthdDesc,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                                             LoanAppRqstAmt,
                                         a.LoanAppSBAGntyPct
                                             LoanCurrSBAGntyPct,
                                         e.LoanTransSBAGntyPct
                                             LoanOrigSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)
                                             LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)
                                             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         a.LoanAppRqstMatMoQty
                                             LoanCurrMatMoQty,
                                         e.LoanTransMatMoQty
                                             LoanOrigMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                                             LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         a.LoanAppOrigntnOfcCd,
                                         c.SBAOfc1Nm
                                             LoanOrigntnOfcNm,
                                         a.LoanAppPrcsOfcCd,
                                         d.SBAOfc1Nm
                                             LoanPrcsOfcNm,
                                         a.LoanAppServOfcCd,
                                         g.SBAOfc1Nm
                                             LoanServOfcNm,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)
                                             LoanCurrGntyFeeAmt,
                                         ROUND (
                                               (SELECT SUM (LOANTRANSGNTYFEEBILLCHNGAMT)
                                                FROM loan.loantranstbl z
                                                WHERE z.loanappnmb = a.loanappnmb)
                                             + NVL (a.LoanGntyFeeBillAdjAmt, 0),
                                             2)
                                             LoanGntyFeeBilledAmt,
                                         --   ROUND((a.LoanGntyFeeBillAmt + a.LoanGntyFeeBillAdjAmt),2)LoanGntyFeeBilledAmt,
                                         a.LoanGntyFeeBillAdjAmt,
                                         ROUND (TO_NUMBER (e.LoanTransChngGntyFeeAmt), 2)
                                             LoanOrigGntyFeeAmt,
                                         a.LocId,
                                         a.LoanAppLSPHQLocId,
                                         a.PrtId,
                                         a.LoanSrvsLocId,
                                         p.PrtLglNm
                                             LoanAppPrtNm,
                                         p.PrtLocFIRSNmb
                                             LoanAppFIRSNmb,
                                         p.PhyAddrStr1Txt
                                             LoanAppPrtStr1,
                                         p.PhyAddrCtyNm
                                             LoanAppPrtCtyNm,
                                         p.PhyAddrStCd
                                             LoanAppPrtStCd,
                                         p.PhyAddrPostCd,
                                         l.PrtLocNm,
                                         a.LoanAppPrtAppNmb,
                                         a.LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)
                                             LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanSoldScndMrktInd,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanTotUndisbAmt,
                                         LoanDisbInd,
                                         RecovInd,
                                         /*,  (select LoanAppInitlIntPct from loanapp.LoanAppTbl where LoanAppNmb =:p_LoanAppNmb) LoanOrignIntPct,*/
                                         CASE
                                             WHEN (SELECT COUNT (*)
                                                   FROM LoanGntyCollatTbl
                                                   WHERE LoanAppNmb = p_LoanAppNmb) > 0 THEN
                                                 'Y'
                                             ELSE
                                                 'N'
                                         END
                                             CollatExistInd,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         a.LoanAppParntLoanNmb,
                                         a.LoanAppMicroLendrId,
                                         (SELECT MAX (ACHROUTINGNMB)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHRtngNmb,
                                         (SELECT MAX (ACHACCTNMB)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHAcctNmb,
                                         (SELECT MAX (
                                                     CASE VALIDACHACCTTYP
                                                         WHEN 'Ck' THEN 'C'
                                                         WHEN 'GL' THEN 'G'
                                                         WHEN 'Sv' THEN 'S'
                                                         ELSE VALIDACHACCTTYP
                                                     END)
                                                     VALIDACHACCTTYP
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHAcctTypCd,
                                         (SELECT MAX (TaxId)
                                          FROM partner.PRTLOCACHACCTTBL p
                                          WHERE     VALIDPRTDISBTYP = '1102'
                                                AND EXISTS
                                                        (SELECT 1
                                                         FROM loanapp.loanappprttbl k
                                                         WHERE     k.locid = p.locid
                                                               AND a.PrcsMthdCd IN ('PPP', 'PPS')
                                                               AND k.loanappnmb = a.loanappnmb))
                                             ACHTinNmb,
                                         a.LoanIntPaidAmt,
                                         a.LoanIntPaidDt,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)
                                             LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)
                                             LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)
                                             LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)
                                             LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)
                                             LoanAppCDCClsCostAmt,
                                             ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)
                                             LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)
                                             LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)
                                             LoanAppContribAmt,
                                         LOANSTATCMNTCD,
                                         a.LoanGntyFeePaidAmt,
                                         a.LOANGNTYDFRTODT,
                                         a.LOANGNTYDFRDMNTHSNMB,
                                         a.LOANGNTYDFRDGRSAMT,
                                         LOANDISASTRAPPFEECHARGED,
                                         LOANDISASTRAPPDCSN,
                                         LOANASSOCDISASTRAPPNMB,
                                         LOANASSOCDISASTRLOANNMB,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LOANPYMTTRANSCD,
                                         a.LOANDISASTRAPPNMB,
                                         a.loancofseffdt,
                                         a.SBICLICNSNMB,
                                         a.LOANMAILSTR1NM,
                                         a.LOANMAILSTR2NM,
                                         a.LOANMAILCTYNM,
                                         a.LOANMAILSTCD,
                                         a.LOANMAILZIPCD,
                                         a.LOANMAILZIP4CD,
                                         a.LOANMAILCNTRYCD,
                                         a.LOANMAILSTNM,
                                         a.LOANMAILPOSTCD,
                                         a.LOANAPPNETEARNIND,
                                         a.LOANPYMTINSTLMNTTYPCD,
                                         a.LOANPYMNINSTLMNTFREQCD,
                                         a.LOANTOTDEFNMB,
                                         a.LOANTOTDEFMONMB,
                                         a.LOANGNTYNOTEDT,
                                         a.LOANGNTYMATSTIND,
                                         a.LoanAppIntDtlCd,
                                         a.LOANPYMTREPAYINSTLTYPCD,
                                         a.LOANGNTYPURISSIND,
                                         a.LOANGNTYPURISSDT,
                                         a.LOANSBICLICNSTYP,
                                         a.loansbicdrawtyp,
                                         a.loansbicschdlpooldt,
                                         a.LoanGntyFeeDscntRt,
                                         ROUND (a.LoanGntyFeeAmt * (100 - NVL (a.LoanGntyFeeDscntRt, 0)) / 100, 2)
                                             AS LoanGntyDscntFeeAmt,
                                         a.LOANAUTOAMSSTOPIND,
                                         a.loanpurofccd,
                                         (SELECT SBAOfc1Nm
                                          FROM sbaref.SBAOfcTbl o
                                          WHERE o.SBAOfcCd = a.loanpurofccd)
                                             AS LoanPurOfcNm,
                                         a.UndrwritngBy,
                                         a.LOANLNDRASSGNLTRDT,
                                         LoanGntyCsaAmtRcvd,
                                         i.loanintrt,
                                         (SELECT LOANSTATDESCTXT
                                          FROM SBAREF.LOANSTATCDTBL z
                                          WHERE Z.LOANSTATCD = a.loanstatcd)
                                             AS status,
                                         a.LoanAppCDCGrossDbentrAmt,
                                         a.LoanAppBalToBorrAmt,
                                         a.LoanGntyNoteIntRt,
                                         a.LoanGntyNoteMntlyPymtAmt,
                                         a.LoanGnty1201ElctrncStmtInd,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         a.LoanGntyFeeBillAmt,
                                         a.LoanGntyDbentrIntRt,
                                         a.LoanAgntInvlvdInd,
                                         a.LoanGntycorrespLangCd,
                                         a.Loan1502CertInd,
                                         a.lendrRebateFeeAmt,
                                         a.LoanGntyPayRollChngInd,
                                         a.LOANAPPSCHEDCIND,
                                         a.LOANAPPSCHEDCYR,
                                         a.LOANAPPGROSSINCOMEAMT,
                                         a.LendrRebatefeePaidDt,
                                         (SELECT dspstn.DspstnStatDescTxt
                                          FROM SBAREF.DSPSTNSTATCDTBL dspstn
                                          WHERE dspstn.DspstnStatCd = a.DspstnStatCd)
                                             DspstnStatDescTxt,
                                         LoanAppUsrEsInd
                                  FROM sbaref.PrcsMthdTbl b,
                                       sbaref.SBAOfcTbl c,
                                       sbaref.SBAOfcTbl d,
                                       sbaref.SBAOfcTbl g,
                                       loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.LoanTransTbl e
                                       ON (    e.LoanAppNmb = a.LoanAppNmb
                                           AND e.LoanAppNmb = p_LoanAppNmb
                                           AND e.LoanTransTypCd = 1)
                                       LEFT OUTER JOIN partner.PrtSrchTbl p
                                       ON a.PrtId = p.PrtId
                                       LEFT OUTER JOIN partner.LocSrchTbl l
                                       ON a.LOANSRVSLOCID = l.LOCID
                                       LEFT OUTER JOIN LOAN.LOANINTDTLTBL i
                                       ON a.loanappnmb = i.loanappnmb
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.PrcsMthdCd = b.PrcsMthdCd
                                        AND a.LoanAppOrigntnOfcCd = c.SBAOfcCd
                                        AND a.LoanAppPrcsOfcCd = d.SBAOfcCd
                                        AND a.LoanAppServOfcCd = g.SBAOfcCd;
            END;
        WHEN 19 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT SBAOfc1Nm,
                                         SBAOfc2Nm,
                                         --                      DECODE (SBAOfcStrNmb, NULL, '', SBAOfcStrNmb || ' ')
                                         --                   || NVL (SBAOfcStrNm, '')
                                         SBAOfcStrNm,
                                         SBAOfcStr2Nm,
                                         SBAOfcCtyNm,
                                         SBAOfcTypCd,
                                         StCd,
                                         ZipCd4,
                                         ZipCd5
                                  FROM Loan.LoanGntyTbl a, sbaref.SBAOfcTbl b
                                  WHERE     a.LoanAppNmb = p_LoanAppNmb
                                        AND a.LoanAppServOfcCd = b.SBAOfcCd;
            END;
        WHEN 20 THEN
            /* This report gives the total number of loans funded and the amount funded for the current Fiscal year
                Current month and Current day for each cohort  -- JP 5/22/2020 rewrote the query*/
            BEGIN
                -- get current Fiscal Year
                SELECT   EXTRACT (YEAR FROM SYSDATE)
                       + (CASE
                              WHEN EXTRACT (MONTH FROM SYSDATE) > 9 THEN 1
                              ELSE 0
                          END)
                           fy
                INTO v_fy
                FROM DUAL;

                SELECT TO_DATE ('10/01/' || (v_fy - 1), 'MM/DD/YYYY')                     v_FromDate,
                       TO_DATE ('09/30/' || (v_fy) || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS')     v_ToDate
                INTO v_FromDate, v_ToDate
                FROM DUAL;

                OPEN p_SelCur FOR
                    SELECT CohortCd,
                           'Approved'
                               StatusOfLoan,
                           COUNT (*)
                               cnt_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN 1
                                   ELSE 0
                               END)
                               cnt_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN 1
                                   ELSE 0
                               END)
                               cnt_dd,
                           SUM (LoanCurrAppvAmt)
                               sum_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN
                                       LoanCurrAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LoanAppFundDt, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN
                                       LoanCurrAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_dd
                    FROM Loan.LoanGntyTbl
                    -- JP 5/11/2021 removed TRUNC(LoanAppFundDt)
                    WHERE     LoanAppFundDt BETWEEN v_FromDate AND v_ToDate
                          AND LoanStatCd <> 4
                    GROUP BY CohortCd
                    UNION ALL
                    SELECT CohortCd,
                           'Cancelled'
                               StatusOfLoan,
                           COUNT (*)
                               cnt_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN 1
                                   ELSE 0
                               END)
                               cnt_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN 1
                                   ELSE 0
                               END)
                               cnt_dd,
                           SUM (LoanInitlAppvAmt)
                               sum_yy,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymm') = TO_CHAR (SYSDATE, 'yyyymm') THEN
                                       LoanInitlAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_mm,
                           SUM (
                               CASE
                                   WHEN TO_CHAR (LOANSTATDT, 'yyyymmdd') = TO_CHAR (SYSDATE, 'yyyymmdd') THEN
                                       LoanInitlAppvAmt
                                   ELSE
                                       0
                               END)
                               sum_dd
                    FROM Loan.LoanGntyTbl
                    -- JP 5/11/2021 removed TRUNC(LOANSTATDT)
                    WHERE     LOANSTATDT BETWEEN v_FromDate AND v_ToDate
                          AND LoanStatCd = 4
                    GROUP BY CohortCd;
            /* Replaced LoanAppFundDt >= v_FromDate  AND LoanAppFundDt < v_ToDate*/
            /* Total number of Funded loans for the current Fiscal Year */
            /* Total number of Funded loans for the current month */
            /* Total number of Funded loans for today */
            /* Total Amount Funded for the current Fiscal Year */
            /* Total Amount Funded for the current Month */
            /* Total Amount Funded for today */

            END;
        WHEN 21 THEN
            BEGIN
                IF p_FisclYrNmb IS NOT NULL THEN
                    BEGIN
                        --  v_FromDate := TO_DATE ('10/01/' || RPAD (p_FisclYrNmb - 1, 4, ' '), 'MM/DD/YYYY');
                        --   v_ToDate := TO_DATE ('10/01/' || RPAD (p_FisclYrNmb, 4, ' '), 'MM/DD/YYYY');

                        SELECT TO_DATE ('10/01/' || (p_FisclYrNmb - 1), 'MM/DD/YYYY')                     v_FromDate,
                               TO_DATE ('09/30/' || (p_FisclYrNmb) || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS')     v_ToDate
                        INTO v_FromDate, v_ToDate
                        FROM DUAL;
                    END;
                END IF;

                OPEN p_SelCur FOR SELECT CohortCd,
                                         'Approved'                           StatusOfLoan,
                                         COUNT (*)                            cnt_yy,
                                         ROUND (SUM (LoanCurrAppvAmt), 2)     sum_yy
                                  FROM loan.LoanGntyTbl
                                  -- JP 5/27/2021 removed TRUNC(LoanAppFundDt)
                                  WHERE     LoanAppFundDt BETWEEN v_FromDate AND v_ToDate
                                        AND LoanStatCd <> 4
                                  GROUP BY CohortCd
                                  UNION ALL
                                  SELECT CohortCd,
                                         'Cancelled'                           StatusOfLoan,
                                         COUNT (*)                             cnt_yy,
                                         ROUND (SUM (LoanInitlAppvAmt), 2)     sum_yy
                                  FROM loan.LoanGntyTbl
                                  -- JP 5/27/2021 removed TRUNC(LOANSTATDT)
                                  WHERE     LOANSTATDT BETWEEN v_FromDate AND v_ToDate
                                        AND LoanStatCd = 4
                                  GROUP BY CohortCd;
            /* IAC 05/21/2020 Replaced LoanAppFundDt with LOANSTATDT */
            /* Total number of Funded loans*/
            /* Total Amount Funded*/
            /* Total number of Funded loans*/
            /* Total Amount Funded*/
            END;
        WHEN 22 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         LoanNmb,
                                         LoanAppNm,
                                         PrcsMthdCd,
                                         LoanStatCd,
                                         LocId,
                                         LoanSrvsLocId,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)     LoanCurrAppvAmt,
                                         PrtId
                                  FROM LoanGntyTbl
                                  WHERE (   LocId = p_LocId
                                         OR LoanSrvsLocId = p_LocId);
            --AND LocId IS NOT NULL
            /*do not include purchased loans...DJ07/20/2010*/
            /*Need to display all loans, undid previous changes.APK 07/21/2010*/
            --and LoanStatCd  NOT IN (4, 5);
            END;
        WHEN 23 THEN
            BEGIN
                OPEN p_SelCur FOR
                    SELECT SUM (
                               CASE
                                   WHEN     EXTRACT (YEAR FROM LoanAppFundDt) = EXTRACT (YEAR FROM v_FromDate)
                                        AND EXTRACT (MONTH FROM LoanAppFundDt) = EXTRACT (MONTH FROM v_FromDate) THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurMonNmb,
                           SUM (
                               CASE
                                   WHEN (  EXTRACT (YEAR FROM LoanAppFundDt)
                                         + (CASE
                                                WHEN EXTRACT (MONTH FROM LoanAppFundDt) > 9 THEN 1
                                                ELSE 0
                                            END)) = (  EXTRACT (YEAR FROM v_FromDate)
                                                     + (CASE
                                                            WHEN EXTRACT (MONTH FROM v_FromDate) > 9 THEN 1
                                                            ELSE 0
                                                        END)) THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurFyNmb,
                           SUM (
                               CASE
                                   WHEN     EXTRACT (YEAR FROM LoanAppFundDt) = EXTRACT (YEAR FROM v_FromDate)
                                        AND TO_CHAR (LoanAppFundDt, 'IW') = TO_CHAR (v_FromDate, 'IW') THEN
                                       1
                                   ELSE
                                       0
                               END)
                               AS TotalCurWeekNmb
                    FROM LoanGntyTbl
                    WHERE     PrtId = p_PrtId
                          AND PrcsMthdCd = p_PrcsMthdCd
                          AND LoanStatCd != 4;
            END;
        WHEN 24 THEN
            /*Retrun row based on LoanNmb*/
            BEGIN
                OPEN p_SelCur FOR SELECT LoanAppNmb,
                                         PrgmCd,
                                         PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)              LoanAppRqstAmt,
                                         LoanAppSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)            LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         LoanAppRqstMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)              LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         LoanStatCd,
                                         LoanStatDt,
                                         LoanAppOrigntnOfcCd,
                                         LoanAppPrcsOfcCd,
                                         LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)              LoanGntyFeeAmt,
                                         LocId,
                                         PrtId,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)               LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanTotUndisbAmt,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanDisbInd,
                                         LoanSoldScndMrktInd,
                                         ACHRtngNmb,
                                         ACHAcctNmb,
                                         ACHAcctTypCd,
                                         ACHTinNmb,
                                         LoanAppPrtTaxId,
                                         ROUND (TO_NUMBER (LoanAppCDCGntyAmt), 2)           LoanAppCDCGntyAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCNetDbentrAmt), 2)      LoanAppCDCNetDbentrAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCFundFeeAmt), 2)        LoanAppCDCFundFeeAmt,
                                         LoanAppCDCSeprtPrcsFeeInd,
                                         ROUND (TO_NUMBER (LoanAppCDCPrcsFeeAmt), 2)        LoanAppCDCPrcsFeeAmt,
                                         ROUND (TO_NUMBER (LoanAppCDCClsCostAmt), 2)        LoanAppCDCClsCostAmt,
                                         ROUND (TO_NUMBER (LOANAPPCDCOTHCLSCOSTAMT), 2)        LOANAPPCDCOTHCLSCOSTAMT,
                                         ROUND (TO_NUMBER (LoanAppCDCUndrwtrFeeAmt), 2)     LoanAppCDCUndrwtrFeeAmt,
                                         LoanAppContribPct,
                                         ROUND (TO_NUMBER (LoanAppContribAmt), 2)           LoanAppContribAmt,
                                         LOANSTATCMNTCD,
                                         LoanGntyFeePaidAmt,
                                         LoanIntPaidDt,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         POOLLENDRGROSSINITLAMT,
                                         POOLLENDRPARTINITLAMT,
                                         POOLORIGPARTINITLAMT,
                                         POOLSBAGNTYBALINITLAMT,
                                         POOLLENDRPARTCURRAMT,
                                         POOLORIGPARTCURRAMT,
                                         POOLLENDRGROSSCURRAMT,
                                         LoanAppMatExtDt,
                                         Loan1201NotcInd,
                                         LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl
                                  WHERE LoanNmb = p_LoanNmb;
            END;
        WHEN 25 THEN
            BEGIN
                OPEN p_SelCur FOR /* to classify according to collateral types for liquidation loans -date created:7/28/2011*/
                                  SELECT a.loanappnmb,
                                         c.loannmb,
                                         a.loancollattypcd,
                                         b.loancollattypdesctxt,
                                         a.loancollatseqnmb,
                                         a.loancollatdesc,
                                         a.loancollatmrktvalamt,
                                         d.lqdstatcd,
                                         e.lqdstatdesctxt,
                                         c.loanstatcd,
                                         f.loanstatdesctxt,
                                         'classification'     AS classification
                                  FROM loan.loangntycollattbl a,
                                       sbaref.LOANCOLLATTYPCDTBL b,
                                       loan.loangntytbl c,
                                       loan.lqdtbl d,
                                       sbaref.lqdstatcdtbl e,
                                       sbaref.loanstatcdtbl f
                                  WHERE     c.loannmb = p_LoanNmb
                                        AND a.loancollattypcd = b.loancollattypcd(+)
                                        AND a.loanappnmb = c.loanappnmb
                                        AND a.loanappnmb = d.loanappnmb
                                        AND d.lqdstatcd = e.lqdstatcd(+)
                                        AND c.loanstatcd = f.loanstatcd(+)
                                        AND d.lqdseqnmb = (SELECT MAX (g.lqdseqnmb)
                                                           FROM lqdtbl g
                                                           WHERE g.loanappnmb = c.loanappnmb)
                                  ORDER BY a.loancollatseqnmb;
            END;
        WHEN 26 THEN
            BEGIN
                OPEN p_SelCur FOR         /* Return the PMT close date and close reason codes for a given loan number:*/
                                  SELECT a.loanappnmb,
                                         a.loannmb,
                                         a.LOANSTATCMNTCD,
                                         a.CURREFCLOSDT,
                                         a.CURREFSTATCD
                                  FROM loan.loangntytbl a
                                  WHERE a.loannmb = p_LoanNmb;
            END;
        WHEN 27 THEN
            BEGIN
                OPEN p_SelCur FOR         /* Return the PMT close date and close reason codes for a given loan number:*/
                                  SELECT a.loanappnmb, a.LOANAPPINTDTLCD, b.LOANINTDTLDESCTXT
                                  FROM loan.loangntytbl a, sbaref.LOANINTDTLCDTBL b
                                  WHERE     a.loanappnmb = p_loanappnmb
                                        AND a.LOANAPPINTDTLCD = b.LOANINTDTLCD;
            END;
        WHEN 28 THEN
            BEGIN
                OPEN p_SelCur FOR                                     /* get all draw downs for commitment loan number*/
                                  SELECT a.LoanAppNmb,
                                         PrgmCd,
                                         CASE
                                             WHEN a.PrgmCd != 'A' THEN 'N'
                                             WHEN a.LoanAppFundDt < TO_DATE ('20131001', 'yyyymmdd') THEN 'Y'
                                             ELSE a.LoanOngngFeeCollInd
                                         END
                                             AS LoanOngngFeeCollInd,
                                         NVL (LOANOrigntnFeeInd, 'Y')
                                             AS LoanOrigntnFeeInd,
                                         a.PrcsMthdCd,
                                         CohortCd,
                                         LoanAppNm,
                                         LoanAppRecvDt,
                                         LoanAppEntryDt,
                                         ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                                             LoanAppRqstAmt,
                                         a.LoanAppSBAGntyPct
                                             LoanCurrSBAGntyPct,
                                         ROUND (TO_NUMBER (LoanInitlAppvAmt), 2)
                                             LoanInitlAppvAmt,
                                         ROUND (TO_NUMBER (LoanCurrAppvAmt), 2)
                                             LoanCurrAppvAmt,
                                         LoanInitlAppvDt,
                                         LoanLastAppvDt,
                                         a.LoanAppRqstMatMoQty
                                             LoanCurrMatMoQty,
                                         LoanAppEPCInd,
                                         ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                                             LoanAppPymtAmt,
                                         LoanAppFullAmortPymtInd,
                                         LoanAppMoIntQty,
                                         LoanAppLifInsurRqmtInd,
                                         LoanAppRcnsdrtnInd,
                                         LoanAppInjctnInd,
                                         LoanCollatInd,
                                         LoanAppEWCPSnglTransPostInd,
                                         LoanAppEWCPSnglTransInd,
                                         LoanAppEligEvalInd,
                                         LoanAppNewBusCd,
                                         LoanAppDisasterCntrlNmb,
                                         a.LoanStatCd,
                                         LoanStatDt,
                                         a.LoanAppOrigntnOfcCd,
                                         a.LoanAppPrcsOfcCd,
                                         a.LoanAppServOfcCd,
                                         LoanAppFundDt,
                                         LoanNmb,
                                         ROUND (TO_NUMBER (LoanGntyFeeAmt), 2)
                                             LoanCurrGntyFeeAmt,
                                         ROUND (
                                             (SELECT SUM (LOANTRANSGNTYFEEBILLCHNGAMT)
                                              FROM loan.loantranstbl z
                                              WHERE z.loanappnmb = a.loanappnmb),
                                             2)
                                             LoanGntyFeeBilledAmt,
                                         a.LocId,
                                         a.PrtId,
                                         a.LoanSrvsLocId,
                                         p.PrtLglNm
                                             LoanAppPrtNm,
                                         p.PrtLocFIRSNmb
                                             LoanAppFIRSNmb,
                                         p.PhyAddrStr1Txt
                                             LoanAppPrtStr1,
                                         p.PhyAddrCtyNm
                                             LoanAppPrtCtyNm,
                                         p.PhyAddrStCd
                                             LoanAppPrtStCd,
                                         p.PhyAddrPostCd,
                                         l.PrtLocNm,
                                         LoanAppPrtAppNmb,
                                         LoanAppPrtLoanNmb,
                                         ROUND (TO_NUMBER (LoanOutBalAmt), 2)
                                             LoanOutBalAmt,
                                         LoanOutBalDt,
                                         LoanSoldScndMrktInd,
                                         LoanAppMatDt,
                                         LoanAppFirstDisbDt,
                                         LoanNextInstlmntDueDt,
                                         LoanTotUndisbAmt,
                                         LoanDisbInd,
                                         RecovInd,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         a.LoanAppParntLoanNmb,
                                         a.LoanAppMicroLendrId,
                                         a.LoanIntPaidAmt,
                                         a.LoanIntPaidDt,
                                         LOANSTATCMNTCD,
                                         a.LoanGntyFeePaidAmt,
                                         a.LOANGNTYDFRTODT,
                                         a.LOANGNTYDFRDMNTHSNMB,
                                         a.LOANGNTYDFRDGRSAMT,
                                         LOANDISASTRAPPFEECHARGED,
                                         LOANDISASTRAPPDCSN,
                                         LOANASSOCDISASTRAPPNMB,
                                         LOANASSOCDISASTRLOANNMB,
                                         LoanPymntSchdlFreq,
                                         FININSTRMNTTYPIND,
                                         NOTEPARENTLOANNMB,
                                         NOTEGENNEDIND,
                                         LoanPurchsdInd,
                                         a.POOLLENDRGROSSINITLAMT,
                                         a.POOLLENDRPARTINITLAMT,
                                         a.POOLORIGPARTINITLAMT,
                                         a.POOLSBAGNTYBALINITLAMT,
                                         a.POOLLENDRPARTCURRAMT,
                                         a.POOLORIGPARTCURRAMT,
                                         a.POOLLENDRGROSSCURRAMT,
                                         a.LoanGntyFeeRebateAmt,
                                         a.LoanAppMatExtDt,
                                         a.Loan1201NotcInd,
                                         a.LOANSERVGRPCD,
                                         FREEZETYPCD,
                                         FREEZERSNCD,
                                         FORGVNESFREEZETYPCD,
                                         DISBDEADLNDT,
                                         GtyFeeUnCollAmt,
                                         a.LOANPYMTTRANSCD,
                                         a.LOANDISASTRAPPNMB,
                                         a.loancofseffdt,
                                         a.SBICLICNSNMB,
                                         a.LOANMAILSTR1NM,
                                         a.LOANMAILSTR2NM,
                                         a.LOANMAILCTYNM,
                                         a.LOANMAILSTCD,
                                         a.LOANMAILZIPCD,
                                         a.LOANMAILZIP4CD,
                                         a.LOANMAILCNTRYCD,
                                         a.LOANMAILSTNM,
                                         a.LOANMAILPOSTCD,
                                         a.LOANAPPNETEARNIND,
                                         a.LOANPYMTINSTLMNTTYPCD,
                                         a.LOANPYMNINSTLMNTFREQCD,
                                         a.LOANTOTDEFNMB,
                                         a.LOANTOTDEFMONMB,
                                         a.LOANGNTYNOTEDT,
                                         a.LOANGNTYMATSTIND,
                                         a.LoanAppIntDtlCd,
                                         a.LOANPYMTREPAYINSTLTYPCD,
                                         a.LOANGNTYPURISSIND,
                                         a.LOANGNTYPURISSDT,
                                         a.LOANSBICLICNSTYP,
                                         a.loansbicdrawtyp,
                                         a.loansbicschdlpooldt,
                                         i.loanintrt,
                                         LOANSTATDESCTXT
                                             AS status,
                                         LOANSBICDRAWTYPDESC,
                                         a.LOANLNDRASSGNLTRDT,
                                         a.LoanGntyCsaAmtRcvd,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.loanintdtltbl i
                                       ON (    a.loanappnmb = i.loanappnmb
                                           AND i.loanintdtlseqnmb = 1)
                                       LEFT OUTER JOIN sbaref.loanstatcdtbl s
                                       ON (a.loanstatcd = s.loanstatcd)
                                       LEFT OUTER JOIN SBAREF.LOANSBICDRAWTYPTBL d
                                       ON (a.LOANSBICDRAWTYP = d.LOANSBICDRAWTYP)
                                       LEFT OUTER JOIN partner.PrtSrchTbl p
                                       ON (a.PrtId = p.PrtId)
                                       LEFT OUTER JOIN partner.LocSrchTbl l
                                       ON (a.LOANSRVSLOCID = l.LOCID)
                                  WHERE     a.LOANAPPPARNTLOANNMB = p_LOANNMB
                                        AND FININSTRMNTTYPIND = 'D';
            END;
        WHEN 29 THEN
            BEGIN
                OPEN p_selcur FOR SELECT LoanAppNmb, Locid
                                  FROM LOAN.LOANGNTYTBL
                                  WHERE LoanNmb = p_Loannmb;
            END;
        WHEN 30 THEN
            BEGIN
                OPEN p_SelCur FOR
                    SELECT LoanGntyFeePct,
                           LoanEfctvGntyFeePct,
                           LoanGntyFeeAmt,
                           LoanGntyFeeDscntRt,
                           ROUND (LoanGntyFeeAmt * (100 - NVL (LoanGntyFeeDscntRt, 0)) / 100, 2)
                               AS LoanGntyFeeBillAmt
                    FROM Loan.LoanGntyCmpnLoanCalcTbl
                    WHERE     LoanMainAppNmb = p_LoanAppNmb
                          AND LoanAppNmb = p_LoanAppNmb
                          AND LOANCMPNAPPNMB = p_LoanAppNmb;
            END;
        /* 38 = Leet for Extract XML */
        WHEN 38 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT a.LoanAppNmb,
                                         a.LoanNmb,
                                         p.AddtnlLocAcqLmtInd,
                                         TO_CHAR (r.ARMMarCeilRt, 'FM990.000')
                                             ARMMarCeilRt,
                                         TO_CHAR (r.ARMMarFloorRt, 'FM990.000')
                                             ARMMarFloorRt,
                                         r.ARMMarTypInd,
                                         p.BulkSaleLawComplyInd,
                                         TO_CHAR (p.CompsLmtAmt, 'FM999999999999990.00')
                                             CompsLmtAmt,
                                         p.CompsLmtInd,
                                         (SELECT COUNT (*)
                                          FROM loan.LoanGntyPreTbl
                                          WHERE LoanAppNmb = a.LoanAppNmb)
                                             AS CountPreTbl,
                                         e.LoanEconDevObjctCd
                                             AS EconDevObjctCd,
                                         r.EscrowAcctRqrdInd,
                                         a.FININSTRMNTTYPIND,
                                         TO_CHAR (p.FixAssetAcqLmtAmt, 'FM999999999999990.00')
                                             FixAssetAcqLmtAmt,
                                         p.FixAssetAcqLmtInd,
                                         p.FrnchsFeeDefInd,
                                         p.FrnchsFeeDefMonNmb,
                                         p.FrnchsLendrSameOppInd,
                                         p.FrnchsRecrdAccsInd,
                                         p.FrnchsTermNotcInd,
                                         i.IMRtTypCd,
                                         r.LateChgAftDayNmb,
                                         r.LateChgInd,
                                         TO_CHAR (r.LateChgFeePct, 'FM990.000')
                                             LateChgFeePct,
                                         i.LoanIntAdjPrdCd
                                             LoanAppAdjPrdCd,
                                         TO_CHAR (i.LoanIntBaseRt, 'FM990.00000')
                                             LoanAppBaseRt,
                                         TO_CHAR (a.LoanAppCDCClsCostAmt, 'FM999999999999990.00')
                                             LoanAppCDCClsCostAmt,
                                         TO_CHAR (a.LOANAPPCDCOTHCLSCOSTAMT, 'FM999999999999990.00')
                                             LOANAPPCDCOTHCLSCOSTAMT,
                                         p.LoanAppCDCJobRat,
                                         TO_CHAR (a.LoanAppCDCNetDbentrAmt, 'FM999999999999990.00')
                                             LoanAppCDCNetDbentrAmt,
                                         TO_CHAR (a.LoanAppCDCPrcsFeeAmt, 'FM999999999999990.00')
                                             LoanAppCDCPrcsFeeAmt,
                                         a.LoanAppCDCSeprtPrcsFeeInd,
                                         TO_CHAR (a.LoanAppContribAmt, 'FM999999999999990.00')
                                             LoanAppContribAmt,
                                         TO_CHAR (a.LoanAppContribPct, 'FM990.00')
                                             LoanAppContribPct,
                                         p.LoanAppCurrEmpQty,
                                         a.LoanAppDisasterCntrlNmb,
                                         TO_CHAR (a.LoanAppFirstDisbDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppFirstDisbDt,
                                         i.LoanIntFixVarInd
                                             LoanAppFixVarInd,
                                         p.LoanAppFrnchsCd,
                                         p.LoanAppFrnchsInd,
                                         p.LoanAppFrnchsNm,
                                         TO_CHAR (a.LoanAppFundDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppFundDt,
                                         TO_CHAR (i.LoanIntRt, 'FM990.00000')
                                             LoanAppInitlIntPct,
                                         TO_CHAR (i.LoanIntSprdOvrPct, 'FM990.000')
                                             LoanAppInitlSprdOvrPrimePct,
                                         a.LoanAppInjctnInd,
                                         a.LoanAppIntDtlCd,
                                         p.LoanAppJobCreatQty,
                                         p.LoanAppJobRtnd,
                                         p.LoanAppJobRqmtMetInd,
                                         TO_CHAR (a.LoanAppMatDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppMatDt,
                                         TO_CHAR (a.LoanAppMatExtDt, 'Mon dd yyyy hh:miAM')
                                             LoanAppMatExtDt,
                                         a.LoanAppMoIntQty,
                                         a.LoanAppNetEarnInd,
                                         TO_CHAR (p.LoanAppNetExprtAmt, 'FM999999999999990.00')
                                             LoanAppNetExprtAmt,
                                         a.LoanAppNewBusCd,
                                         a.LoanAppNm,
                                         a.LoanAppOutPrgrmAreaOfOperInd,
                                         p.LoanAppProjCntyCd,
                                         p.LoanAppProjCtyNm,
                                         p.LoanAppProjStCd,
                                         p.LoanAppProjStr1Nm,
                                         p.LoanAppProjStr2Nm,
                                         p.LoanAppProjZipCd,
                                         p.LoanAppProjZip4Cd,
                                         a.LoanAppPrtLoanNmb,
                                         TO_CHAR (a.LoanAppPymtAmt, 'FM999999999999990.00')
                                             LoanAppPymtAmt,
                                         a.LoanAppRecvDt,
                                         a.LoanAppRqstMatMoQty,
                                         p.LoanAppRuralUrbanInd,
                                         a.LoanAppSBAGntyPct,
                                         a.LoanAppServOfcCd,
                                         a.LoanCollatInd,
                                         TO_CHAR (a.LoanCurrAppvAmt, 'FM999999999999990.00')
                                             LoanCurrAppvAmt,
                                         a.LoanDisbInd,
                                         p.LoanFinanclStmtDueDt,
                                         p.LoanFinanclStmtFreqCd,
                                         p.LoanFinanclStmtFreqDtlDesc,
                                         a.LoanGntyMatStInd,
                                         a.LoanGntyNoteDt,
                                         a.LoanMailCtyNm,
                                         a.LoanMailCntryCd,
                                         a.LoanMailPostCd,
                                         a.LoanMailStCd,
                                         a.LoanMailStNm,
                                         a.LoanMailStr1Nm,
                                         a.LoanMailStr2Nm,
                                         a.LoanMailZipCd,
                                         a.LoanMailZip4Cd,
                                         a.LoanNextInstlmntDueDt,
                                         a.LoanOutBalAmt,
                                         a.LoanOutBalDt,
                                         a.LoanPymnInstlmntFreqCd,
                                         a.LoanPymtRepayInstlTypCd,
                                         a.LoanPymntSchdlFreq,
                                         a.LoanPurchsdInd,
                                         a.LoanServGrpCd,
                                         a.LoanStatCd,
                                         a.LoanTotUndisbAmt,
                                         a.LocId,
                                         a.LoanGntyCsaAmtRcvd,
                                         p.NAICSCd,
                                         p.NAICSYrNmb,
                                         r.NetEarnClauseInd,
                                         TO_CHAR (r.NetEarnPymntOverAmt, 'FM999999999999990.00')
                                             NetEarnPymntOverAmt,
                                         TO_CHAR (r.NetEarnPymntPct, 'FM990.000')
                                             NetEarnPymntPct,
                                         a.PrcsMthdCd,
                                         a.PrgmCd,
                                         a.LOANGNTY1201ELCTRNCSTMTIND,
                                         r.PymntBegnMoNmb,
                                         r.PymntDayNmb,
                                         r.PymntIntOnlyBegnMoNmb,
                                         r.PymntIntOnlyDayNmb,
                                         r.PymntIntOnlyFreqCd,
                                         r.StIntRtReductnInd,
                                         r.StIntRtReductnPrgmNm,
                                         p.LoanBusEstDt,
                                         TO_CHAR (a.LoanAppCDCGrossDbentrAmt, 'FM999999999999990.00')
                                             LoanAppCDCGrossDbentrAmt,
                                         TO_CHAR (a.LoanAppBalToBorrAmt, 'FM999999999999990.00')
                                             LoanAppBalToBorrAmt,
                                         a.LoanMailAddrsInvldInd,
                                         a.LoanAppRevlMoIntQty,
                                         a.LoanAppAmortMoIntQty,
                                         a.LoanAgntInvlvdInd,
                                         a.LoanAppExtraServFeeAMT,
                                         a.LoanAppExtraServFeeInd,
                                         a.Undrwritngby,
                                         a.LoanGntycorrespLangCd,
                                         a.LoanAppMonthPayroll,
                                         a.LoanGntyPayRollChngInd,
                                         a.LOANAPPSCHEDCIND,
                                         a.LOANAPPSCHEDCYR,
                                         a.LOANAPPGROSSINCOMEAMT,
                                         a.DspstnStatCd,
                                         LoanAppUsrEsInd
                                  FROM loan.LoanGntyTbl a
                                       LEFT OUTER JOIN loan.LoanIntDtlTbl i                            -- i for interest
                                       ON (    i.LoanAppNmb = a.LoanAppNmb
                                           AND i.LoanIntDtlSeqNmb = 1)
                                       LEFT OUTER JOIN loan.LoanGntyProjTbl p                              -- p for proj
                                       ON (p.LoanAppNmb = a.LoanAppNmb)
                                       LEFT OUTER JOIN loan.LoanGntyPymntTbl r                        -- r for repayment
                                       ON (r.LoanAppNmb = a.LoanAppNmb)
                                       LEFT OUTER JOIN loan.LoanGntyEconDevObjctChldTbl e        -- e for econdevobjctcd
                                       ON (    e.LoanAppNmb = a.LoanAppNmb
                                           AND e.LoanEconDevObjctSeqNmb = 1)
                                  WHERE a.LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 39 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loanappnmb, loanpurofccd, SBAOfc1Nm AS LoanPurOfcNm
                                  FROM loan.loangntytbl g
                                       LEFT OUTER JOIN sbaref.SBAOfcTbl o
                                       ON (g.loanpurofccd = o.SBAOfcCd)
                                  WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 40 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT PRGMCD, LOANAPPMICROLENDRID
                                  FROM Loan.LoanGntyTbl
                                  WHERE LoanNmb LIKE (RTRIM (p_LoanNmb) || '%');
            END;
        WHEN 41 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT CDCServFeePct,
                                         LOANPURCHSDIND,
                                         LOANGNTY1201ELCTRNCSTMTIND,
                                         LoanAgntInvlvdInd,
                                         LoanNmb,
                                         loantypind                                                -- NK-- CAFSOPER 2021
                                  FROM Loan.LoanGntyTbl
                                  WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 42 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loanappnmb,
                                         loanstatcd,
                                         prgmcd,
                                         loantypind
                                  FROM loan.loangntytbl
                                  WHERE LoanNmb = p_LoanNmb;
            END;
        WHEN 43 THEN
            BEGIN
                OPEN p_SelCur FOR SELECT loannmb
                                  FROM loan.loangntytbl
                                  WHERE loannmb = p_loannmb;
            END;
    END CASE;

    p_RetVal := SQL%ROWCOUNT;
    p_RetVal := NVL (p_RetVal, 0);
END LOANGNTYSELTSP;
/
CREATE OR REPLACE PROCEDURE LOAN.LOANSRVSFINUPDCSP (
   p_LoanAppNmb                     NUMBER,
   p_LoanStatCd                     NUMBER,
   p_LoanCurrAppvAmt                NUMBER := NULL,
   p_LoanAppSBAGntyPct              NUMBER := NULL,
   p_LoanAppRqstMatMoQty            NUMBER := NULL,
   p_LoanAppAdjPrdCd                CHAR := NULL,
   p_LoanAppPymtAmt                 NUMBER := NULL,
   p_LoanAppInjctnInd               CHAR := NULL,
   p_LoanCollatInd                  CHAR := NULL,
   p_LoanAppDisasterCntrlNmb        VARCHAR2 := NULL,
   p_LoanAppNetExprtAmt             NUMBER := NULL,
   p_LoanAppCDCGntyAmt              NUMBER := NULL,
   p_LoanAppCDCNetDbentrAmt         NUMBER := NULL,
   p_LoanAppCDCFundFeeAmt           NUMBER := NULL,
   p_LoanAppCDCSeprtPrcsFeeInd      CHAR := NULL,
   p_LoanAppCDCPrcsFeeAmt           NUMBER := NULL,
   p_LoanAppCDCClsCostAmt           NUMBER := NULL,
   p_LoanAppCDCOthClsCostAmt        NUMBER := NULL,
   p_LoanAppCDCUndrwtrFeeAmt        NUMBER := NULL,
   p_LoanAppContribPct              NUMBER := NULL,
   p_LoanAppContribAmt              NUMBER := NULL,
   p_LoanAppMoIntQty                NUMBER := NULL,
   p_LoanAppMatDt                   DATE := NULL,
   p_LoanAppCreatUserId             CHAR := NULL,
   p_LoanAppOutPrgrmAreaOfOperInd   CHAR := NULL,
   p_LoanAppParntLoanNmb            CHAR := NULL,
   p_ACHRtngNmb                     VARCHAR2 := NULL,
   p_ACHAcctNmb                     VARCHAR2 := NULL,
   p_ACHAcctTypCd                   CHAR := NULL,
   p_ACHTinNmb                      CHAR := NULL,
   p_LoanAppFirstDisbDt             DATE := NULL,
   p_LoanTotUndisbAmt               NUMBER := NULL,
   p_LOANSTATCMNTCD                 CHAR := NULL,
   p_LOANGNTYDFRTODT                DATE := NULL,
   p_LOANGNTYDFRDMNTHSNMB           NUMBER := NULL,
   p_LOANGNTYDFRDGRSAMT             NUMBER := NULL,
   p_LOANDISASTRAPPFEECHARGED       NUMBER := NULL,
   p_LOANDISASTRAPPDCSN             CHAR := NULL,
   p_LOANASSOCDISASTRAPPNMB         CHAR := NULL,
   p_LOANASSOCDISASTRLOANNMB        CHAR := NULL,
   p_LoanNextInstlmntDueDt          DATE := NULL,
   p_LoanPymntSchdlFreq             VARCHAR2 := NULL,
   p_LoanAppMatExtDt                DATE := NULL,
   p_Loan1201NotcInd                NUMBER := NULL,
   p_LOANSERVGRPCD                  VARCHAR2 := NULL,
   p_FREEZETYPCD                    CHAR := NULL,
   p_FREEZERSNCD                    NUMBER := NULL,
   p_FORGVNESFREEZETYPCD            NUMBER := NULL,
   p_DisbDeadLnDt                   DATE := NULL,
   p_LOANPYMTTRANSCD                NUMBER := NULL,
   p_LOANDISASTRAPPNMB              CHAR := NULL,
   p_LOANCOFSEFFDT                  DATE := NULL,
   p_SBICLICNSNMB                   CHAR := NULL,
   p_LOANMAILSTR1NM                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILSTR2NM                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILCTYNM                  VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILSTCD                   CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILZIPCD                  CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILZIP4CD                 CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILCNTRYCD                CHAR := NULL,                 --RGG 03/21/2014 Deleted references
   p_LOANMAILSTNM                   VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANMAILPOSTCD                 VARCHAR2 := NULL,             --RGG 03/21/2014 Deleted references
   p_LOANAPPNETEARNIND              CHAR := NULL,
   p_LOANPYMTINSTLMNTTYPCD          CHAR := NULL,
   p_LOANPYMNINSTLMNTFREQCD         CHAR := NULL,
   p_LOANTOTDEFNMB                  NUMBER := NULL,
   p_LOANTOTDEFMONMB                NUMBER := NULL,
   p_LoanAppNm                      VARCHAR2 := NULL,
   p_LOANOUTBALAMT                  NUMBER := NULL,
   p_LOANOUTBALDT                   DATE := NULL,
   p_LOANGNTYNOTEDT                 DATE := NULL,
   p_LOANGNTYMATSTIND               VARCHAR2 := NULL,
   p_LOANPYMTREPAYINSTLTYPCD        CHAR := NULL,
   p_LOANAPPINTDTLCD                VARCHAR2 := NULL,
   p_LOANLNDRASSGNLTRDT             DATE := NULL,
   p_LoanGntyNoteIntRt              NUMBER := NULL,
   p_LoanGntyNoteMntlyPymtAmt       NUMBER := NULL,
   p_LoanAppRevlMoIntQty            NUMBER := NULL,
   p_LoanAppAmortMoIntQty           NUMBER := NULL,
   p_LoanAppMonthPayroll            NUMBER := NULL,
   p_LoanGntyPayRollChngInd         CHAR := NULL)
AS
   /*
   Date     :
   Programmer   :
   Remarks      :
   ***********************************************************************************
   Revised By      Date        Comments
   ----------     --------    ----------------------------------------------------------
   APK         05/04/2010   added new input variable v_LOANSTATCMNTCD
   APK         03/21/2011   Modified to add new column LoanAppMatExtDt..
   APK         03/22/2011  Removed the LOANGNTYFEEREBATEAMT   changes that were added as
                             they there not needed.
   APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. also added Loan1201NotcInd and LOANSERVGRPCD previously added to the Gnty table. added to the call to LoanGntyTbl update statement.
   APK -- 10/19/2011 -- Modified to change Loan1201NotcInd data type from Char  to Number.
   APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
   SP  -- 1/9/2012   --  MODIFIED TO ADD NEW COLUMN LOANPYMTTRANSCD
   APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
   SP --- 3/5/2012   -- Modified to add new column loancofseffdt
   SP --- 4/18/2012  -- Modified to add new column LoanAppNetEarnInd
   SP --- 05/01/2012 -- Modified to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
   LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
   RGG -- 06/11/2012 -- Modified to add p_LoanAppNm and update LoanGntyTbl. It is passed from LoanSrvsPrcsPndLoanCSP
   SP  -- 06/12/2012 -- Modified  to accept p_LoanOutBalAmt and p_LoanOutBalDt from the parameter list and to propagate those values to LoanGntyTbl.
   RSURAPA -- 05/16/2013  --  Added a new parameter p_nodedt and added nodedt = p_notedt to the update statement of the loangntytbl table.
   RSURAPA -- 05/22/2013  -- The notedt column was changed to loangntynote date in the loangntytbl.
   RSURAPA -- 06/17/2013 -- Added new column LOANGNTYMATSTIND in the loangntytbl, modified procedure to include the column as parameter and in the update statement.
   RGG -- 03/21/2014 -- Deleted references to LOANMAILSTR1NM,LOANMAILSTR2NM,LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,
                         LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD
   SP  -- 04/10/2014 -- Added REPAYINSTLTYPCD
   --RGG - 04/04/2016 Added LOANLNDRASSGNLTRDT
   NK--07/20/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt
   SS--12/04/2017 --OPSMDEV 1589-- Added code for voluntary termination and reinstatement of disbursed current loans
   RY--01/24/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
   SL--02/01/2021 SODSTORY 447/448 added column LoanGntyPayRollChngInd
********************************************************************************
   */
   v_TotDisbAmt               NUMBER;
   v_LoanStatDt               DATE;
   v_LoanLastAppvDt           DATE;
   v_OldFixVarInd             CHAR (1);
   v_LoanStatCd               NUMBER;
   v_OldStatCd                NUMBER;
   v_OldLoanAmt               NUMBER;
   v_OldInitlPct              NUMBER;
   v_OldExprtAmt              NUMBER;
   v_OldMatDt                 DATE;
   v_OldFirstDisbDt           DATE;
   v_LoanTypInd               CHAR (1);
   v_OldDisbInd               CHAR (1);
   v_Loan1502Ind              CHAR (1);
   v_DefaultDt                DATE;
   v_OldRtTypCd               CHAR (3);
   v_LoanIntPctChngInd        CHAR (1);
   v_RetVal                   NUMBER;
   v_RetTxt                   VARCHAR2 (255);
   v_LoanAppNm                VARCHAR2 (80);
   v_LoanTotUndisbAmt         NUMBER (15, 2);
   v_LOANAPPCDCGROSSDBENTRAMT NUMBER;
   v_LOANAPPBALTOBORRAMT      NUMBER;
   v_LoanOutBalAmt            NUMBER;
   v_LoanNmb                  CHAR (10);
BEGIN
   v_DefaultDt         := '01-JAN-1800';
   v_LoanStatCd        := p_LoanStatCd;
   v_LoanTotUndisbAmt  := p_LoanTotUndisbAmt;

   BEGIN
      SELECT LOANAPPCDCGROSSDBENTRAMT, LOANAPPBALTOBORRAMT
      INTO   v_LOANAPPCDCGROSSDBENTRAMT, v_LOANAPPBALTOBORRAMT
      FROM   LoanGntyPreTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN OTHERS THEN
         SELECT LOANAPPCDCGROSSDBENTRAMT, LOANAPPBALTOBORRAMT
         INTO   v_LOANAPPCDCGROSSDBENTRAMT, v_LOANAPPBALTOBORRAMT
         FROM   LoanGntyTbl
         WHERE  LoanAppNmb = p_LoanAppNmb;
   END;

   --v_DefaultDt := '01/01/1800';
   IF p_LoanAppNm IS NULL THEN
      SELECT LoanAppNm
      INTO   v_LoanAppNm
      FROM   loangntytbl a
      WHERE  a.LoanAppNmb = p_LoanAppNmb;
   ELSE
      v_LoanAppNm  := p_LoanAppNm;
   END IF;

   BEGIN
      SELECT LoanStatCd,
             LoanStatDt,
             LoanLastAppvDt,
             LoanTypInd,
             LoanDisbInd,
             LoanCurrAppvAmt,
             LoanAppMatDt,
             LoanNmb
      INTO   v_OldStatCd,
             v_LoanStatDt,
             v_LoanLastAppvDt,
             v_LoanTypInd,
             v_OldDisbInd,
             v_OldLoanAmt,
             v_OldMatDt,
             v_LoanNmb
      FROM   LOAN.LoanGntyTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   END;

   BEGIN
      SELECT NVL (LoanAppNetExprtAmt, 0)
      INTO   v_OldExprtAmt
      FROM   LoanGntyProjTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   END;

   v_LoanOutBalAmt     := p_LoanOutBalAmt;

   IF     p_LoanStatCd = 10
      AND v_OldStatCd != 10 THEN
      INSERT INTO gntyloanrpt.loantbl@cael1 (LoanNmb,
                                             LoadDt,
                                             DataSourcId,
                                             PrcsDt,
                                             LoanOutBalAmt,
                                             ETRANEXTDT,
                                             ErrTxt,
                                             LastUpdtUserId,
                                             LastUpdtDt)
      VALUES      (v_LoanNmb,
                   SYSDATE,
                   'Colson',
                   TO_CHAR (SYSDATE, 'yyyymmdd'),
                   p_LoanOutBalAmt,
                   SYSDATE,
                   '<errors><validationerrors><error>1004</error></validationerrors></errors>',
                   p_LoanAppCreatUserId,
                   SYSDATE);

      --Voluntary Termination
      v_LoanOutBalAmt  := 0;
   END IF;

   IF     v_OldStatCd = 10
      AND p_LoanStatCd != 10 THEN
      --Reinstatement
      BEGIN
         SELECT LOANOUTBALAMT
         INTO   v_LoanOutBalAmt
         FROM   GNTYLOANRPT.LoanTbl@CAEL1
         WHERE      LoanNmb = v_LoanNmb
                AND ROWNUM = 1
                AND LOANOUTBALAMT > 0
                AND DATASOURCID = 'Colson'
                AND LoadDt = (SELECT MAX (LoadDt)
                              FROM   GNTYLOANRPT.LoanTbl@CAEL1 y
                              WHERE      y.LoanNmb = v_LoanNmb
                                     AND LOANOUTBALAMT > 0
                                     AND DATASOURCID = 'Colson');
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
   END IF;



   IF     p_LoanCurrAppvAmt = 0
      AND v_LoanStatCd = 4 THEN                                                         --Cancelation
      BEGIN
         v_LoanStatCd  := 4;
   		 v_LoanOutBalAmt := 0;
   		 
   		 IF v_OldLoanAmt > 0 THEN
   		 	v_LoanTotUndisbAmt := v_OldLoanAmt;
		 END IF;
      END;
   ELSIF     p_LoanCurrAppvAmt > 0
         AND v_LoanStatCd = 4 THEN                                                    --Reinstatement
      BEGIN
         v_LoanStatCd        := 1;
         v_LoanTotUndisbAmt  := p_LoanCurrAppvAmt;
      END;
   /*ELSIF p_LoanCurrAppvAmt > 0 and v_LoanStatCd = 2
     THEN                                                     --Voluntarily termination
        BEGIN
           v_LoanStatCd := 10;
           v_LOANOUTBALAMT :=0;
        END;

    ELSIF p_LoanCurrAppvAmt > 0 AND v_LoanStatCd = 10
     THEN                                                     --Reinstatement for Voluntarily termination
        BEGIN
           v_LoanStatCd := 2;
           v_LoanTotUndisbAmt := p_LoanCurrAppvAmt;
        END;*/


   ELSIF NVL (v_LoanTypInd, 'D') != 'G' THEN
      BEGIN
         --Retrieve total disbursement amount
         BEGIN
            SELECT NVL (SUM (LoanDisbAmt), 0)
            INTO   v_TotDisbAmt
            FROM   LoanDisbTbl
            WHERE  LoanAppNmb = p_LoanAppNmb;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;

         /* Check if loan status got changed based on disbursement amount and loan amount*/
         loan.LoanDisbStatCSP (v_TotDisbAmt, p_LoanCurrAppvAmt, v_LoanStatCd);
      /*if @error != 0
      begin
          IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
          RETURN @error
      end*/
      END;
   END IF;

   -- END IF;

   IF v_OldStatCd <> v_LoanStatCd THEN
      BEGIN
         v_LoanStatDt  := SYSDATE;
      END;
   END IF;


   /* v_LoanLastAppvDt : for increase/decrease, use current date */
   IF v_OldLoanAmt <> p_LoanCurrAppvAmt THEN
      BEGIN
         v_LoanLastAppvDt  := SYSDATE;
      END;
   END IF;

   loan.UpdtLoanFinCSP (
                        p_LoanAppNmb,
                        p_LoanAppSBAGntyPct,
                        p_LoanCurrAppvAmt,
                        v_LoanLastAppvDt,
                        p_LoanAppRqstMatMoQty,
                        NULL,
                        v_LoanStatCd,
                        v_LoanStatDt,
                        p_LoanAppCreatUserId,
                        v_RetVal,
                        v_RetTxt,
                        v_LoanAppNm
                       );

   /*IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/
   UPDATE LoanGntyTbl
   SET                                                         --LoanAppAdjPrdCd = p_LoanAppAdjPrdCd,
      LoanAppPymtAmt                    = p_LoanAppPymtAmt,
          LoanAppInjctnInd              = p_LoanAppInjctnInd,
          LoanCollatInd                 = p_LoanCollatInd,
          LoanAppDisasterCntrlNmb       = p_LoanAppDisasterCntrlNmb,
          LoanAppMoIntQty               = p_LoanAppMoIntQty,
          LoanAppMatDt                  = p_LoanAppMatDt,
          LastUpdtUserId                = p_LoanAppCreatUserId,
          LastUpdtDt                    = SYSDATE,
          LoanAppOutPrgrmAreaOfOperInd  = p_LoanAppOutPrgrmAreaOfOperInd,
          LoanAppParntLoanNmb           = p_LoanAppParntLoanNmb,
          ACHRtngNmb                    = p_ACHRtngNmb,
          ACHAcctNmb                    = p_ACHAcctNmb,
          ACHAcctTypCd                  = p_ACHAcctTypCd,
          ACHTinNmb                     = p_ACHTinNmb,
          LoanAppFirstDisbDt            = p_LoanAppFirstDisbDt,
          LoanTotUndisbAmt              = v_LoanTotUndisbAmt,
          LoanAppCDCGntyAmt             = p_LoanAppCDCGntyAmt,
          LoanAppCDCNetDbentrAmt        = p_LoanAppCDCNetDbentrAmt,
          LoanAppCDCFundFeeAmt          = p_LoanAppCDCFundFeeAmt,
          LoanAppCDCSeprtPrcsFeeInd     = p_LoanAppCDCSeprtPrcsFeeInd,
          LoanAppCDCPrcsFeeAmt          = p_LoanAppCDCPrcsFeeAmt,
          LoanAppCDCClsCostAmt          = p_LoanAppCDCClsCostAmt,
          LoanAppCDCOthClsCostAmt       = p_LoanAppCDCOthClsCostAmt,
          LoanAppCDCUndrwtrFeeAmt       = p_LoanAppCDCUndrwtrFeeAmt,
          LoanAppContribPct             = p_LoanAppContribPct,
          LoanAppContribAmt             = p_LoanAppContribAmt,
          LOANSTATCMNTCD                = p_LOANSTATCMNTCD,
          LOANGNTYDFRTODT               = p_LOANGNTYDFRTODT,
          LOANGNTYDFRDMNTHSNMB          = p_LOANGNTYDFRDMNTHSNMB,
          LOANGNTYDFRDGRSAMT            = p_LOANGNTYDFRDGRSAMT,
          LOANDISASTRAPPFEECHARGED      = p_LOANDISASTRAPPFEECHARGED,
          LOANDISASTRAPPDCSN            = p_LOANDISASTRAPPDCSN,
          LOANASSOCDISASTRAPPNMB        = p_LOANASSOCDISASTRAPPNMB,
          LOANASSOCDISASTRLOANNMB       = p_LOANASSOCDISASTRLOANNMB,
          LoanNextInstlmntDueDt         = p_LoanNextInstlmntDueDt,
          LoanPymntSchdlFreq            = p_LoanPymntSchdlFreq,
          LoanAppMatExtDt               = p_LoanAppMatExtDt,
          Loan1201NotcInd               = p_Loan1201NotcInd,
          LOANSERVGRPCD                 = p_LOANSERVGRPCD,
          FREEZETYPCD                   = p_FREEZETYPCD,
          FREEZERSNCD                   = p_FREEZERSNCD,
          FORGVNESFREEZETYPCD           = p_FORGVNESFREEZETYPCD,
          DisbDeadLnDt                  = p_DisbDeadLnDt,
          LOANPYMTTRANSCD               = p_LOANPYMTTRANSCD,
          LOANDISASTRAPPNMB             = p_LOANDISASTRAPPNMB,
          LOANCOFSEFFDT                 = p_LOANCOFSEFFDT,
          SBICLICNSNMB                  = p_SBICLICNSNMB,
          LOANAPPNETEARNIND             = p_LOANAPPNETEARNIND,
          LOANPYMTINSTLMNTTYPCD         = p_LOANPYMTINSTLMNTTYPCD,
          LOANPYMNINSTLMNTFREQCD        = p_LOANPYMNINSTLMNTFREQCD,
          LOANTOTDEFNMB                 = p_LOANTOTDEFNMB,
          LOANTOTDEFMONMB               = p_LOANTOTDEFMONMB,
          LOANSTATCD                    = v_LOANSTATCD,
          LOANSTATDT                    = v_LOANSTATDT,
          LOANAPPNM                     = v_LoanAppNm,                               --RGG 06-11-2012
          LOANOUTBALAMT                 = v_LOANOUTBALAMT,
          LOANOUTBALDT                  =
             CASE
                WHEN NVL (LOANOUTBALAMT, 0) = NVL (v_LOANOUTBALAMT, 0) THEN LOANOUTBALDT
                ELSE SYSDATE
             END,
          LOANGNTYNOTEDT                = p_LOANGNTYNOTEDT,
          LOANGNTYMATSTIND              = p_LOANGNTYMATSTIND,
          LOANPYMTREPAYINSTLTYPCD       = p_LOANPYMTREPAYINSTLTYPCD,
          LOANAPPINTDTLCD               = p_LOANAPPINTDTLCD,
          LOANLNDRASSGNLTRDT            = p_LOANLNDRASSGNLTRDT,
          LoanGntyNoteIntRt             = p_LoanGntyNoteIntRt,
          LoanGntyNoteMntlyPymtAmt      = p_LoanGntyNoteMntlyPymtAmt,
          LOANAPPCDCGROSSDBENTRAMT      = v_LOANAPPCDCGROSSDBENTRAMT,
          LOANAPPBALTOBORRAMT           = v_LOANAPPBALTOBORRAMT,
          LoanAppRevlMoIntQty           = p_LoanAppRevlMoIntQty,
          LoanAppAmortMoIntQty          = p_LoanAppAmortMoIntQty,
          LoanAppMonthPayroll           = p_LoanAppMonthPayroll,
          LoanGntyPayRollChngInd        = p_LoanGntyPayRollChngInd
   WHERE  LoanAppNmb = p_LoanAppNmb;

   /*SELECT @error = @@error
   IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/

   UPDATE LoanGntyProjTbl
   SET    LoanAppNetExprtAmt = p_LoanAppNetExprtAmt, LastUpdtUserId = p_LoanAppCreatUserId, LastUpdtDt = SYSDATE
   WHERE  LoanAppNmb = p_LoanAppNmb;

   /*SELECT @error = @@error
   IF @error != 0
   BEGIN
       IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
       RETURN @error
   END*/

   loan.LoanChngLogInsCSP (
                           p_LoanAppNmb,
                           'S',
                           p_LoanAppCreatUserId,
                           v_LoanIntPctChngInd
                          );

   IF    (    v_OldStatCd != 10
          AND p_LoanStatCd = 10)
      OR (    v_OldStatCd = 10
          AND p_LoanStatCd != 10) THEN
      --voluntary termination
      Loan.LoanChngLogInsCSP (
                              p_LoanAppNmb,
                              'R',
                              p_LoanAppCreatUserId,
                              NULL,
                              NULL
                             );
   END IF;
/*if @error != 0
begin
    IF @@trancount > 0 ROLLBACK TRANSACTION LoanSrvsFinUpdCSP
    RETURN @error
end*/
END;
/
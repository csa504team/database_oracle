CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPROCDSUBTYPSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANPROCDSUBTYPSELTSP;

    /* Select from LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANGNTYPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANGNTYPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                AND LOANPROCDTYPCD=p_LOANPROCDTYPCD 
                order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        TO_CHAR (LOANPROCDAMT, 'FM999999999999990.00') LOANPROCDAMT,
                        LoanProcdAmt ,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        Lender,
                        ProcdSubTyLkupId 
                FROM Loan.LOANGNTYPROCDSUBTYPTBL
                WHERE LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;        
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYPROCDSUBTYPSELTSP;
            RAISE;
        END;
END LOANGNTYPROCDSUBTYPSELTSP;
/

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYPROCDSUBTYPSELTSP TO UPDLOANROLE;

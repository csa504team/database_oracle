CREATE OR REPLACE PROCEDURE LOAN.LoanAGNTSELTSP (
   p_Identifier       IN     NUMBER := 0,
   p_LoanAppNmb       IN     NUMBER := 0,
   p_LoanAgntSeqNmb   IN     NUMBER := 0,
   p_SelCur              OUT SYS_REFCURSOR,
   p_RetVal              OUT NUMBER)
AS
--SS--OPSMDEV1862--07/16/2018--Collecting Agent data
--RY- 11/15/2018--OPSMDEV-2044:: Added OCADATAOUT for PII columns
BEGIN
   IF p_Identifier = 0
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE LOANAPPNMB = p_LOANAPPNMB;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 2
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT 1
              FROM LOAN.LoanAgntTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 11
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE     LOANAPPNMB = p_LOANAPPNMB
                   AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 12
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT,
                   LOANAGNTID,
                   CreatUserId,
                   CreatDt,
                   LastUpdtUserId,
                   LastUpdtDt
              FROM LOAN.LoanAgntTbl
             WHERE     LOANAPPNMB = p_LOANAPPNMB
                   AND LoanAgntSeqNmb > NVL (p_LoanAgntSeqNmb, 0);

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_identifier = 13
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT a.LOANAPPNMB,
                   a.LOANAGNTTYPCD,
                   a.LOANAGNTBUSPERIND,
                   a.LOANAGNTNM,
                      OcaDataOut (a.LOANAGNTCNTCTFIRSTNM)
                   || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                   || ' '
                   || OcaDataOut (a.LOANAGNTCNTCTLastNm)
                   || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                      AS LoanContactName,
                      OcaDataOut (a.LOANAGNTADDRSTR1NM)
                   || RTRIM (' ' || OcaDataOut (a.LOANAGNTADDRSTR2NM))
                   || ' '
                   || a.LOANAGNTADDRCTYNM
                   || ' '
                   || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                   || ' '
                   || a.LOANAGNTADDRZIPCD
                      AS LoanContactaddress,
                   a.LOANCDCTPLFEEIND,
                   a.LOANCDCTPLFEEAMT,
                   b.LOANAGNTSERVTYPCD,
                   b.LOANAGNTSERVOTHTYPTXT,
                   b.LOANAGNTAPPCNTPAIDAMT,
                   b.LOANAGNTSBALENDRPAIDAMT,
                   c.Prgmcd,
                   c.LoanAppNm,
                   c.LoanNmb,
                   d.PrtlglNm,
                   e.loanpartlendrtypcd,
                   e.loanpartlendrnm,
                      e.loanpartlendrstr1nm
                   || RTRIM (' ' || e.loanpartlendrstr2nm)
                   || ' '
                   || e.loanpartlendrctynm
                   || ' '
                   || RTRIM (' ' || e.loanpartlendrstcd)
                   || ' '
                   || e.loanpartlendrcntrycd
                   || ' '
                   || RTRIM (' ' || e.loanpartlendrzip5cd)
                      AS LendrAddress,
                   e.LOCID
              FROM loanagnttbl a,
                   loanagntfeedtltbl b,
                   loangntytbl c,
                   Partner.PrtTbl d,
                   LoanGntyPartLendrtbl e
             WHERE     a.loanappnmb = b.loanappnmb
                   AND b.loanappnmb = c.loanappnmb
                   AND a.LOANAPPNMB = p_loanappnmb
                   AND c.PrtId = d.PrtId
                   AND e.loanappnmb(+) = a.loanappnmb
                   AND a.LoanAgntSeqNmb = p_LoanAgntSeqNmb
                   AND a.LoanAgntSeqNmb = b.LoanAgntSeqNmb;
      END;
   ELSIF p_Identifier = 38
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LOANAPPNMB,
                   LoanAgntSeqNmb,
                   LOANAGNTBUSPERIND,
                   LOANAGNTNM,
                   OcaDataOut (LOANAGNTCNTCTFIRSTNM) AS LOANAGNTCNTCTFIRSTNM,
                   LOANAGNTCNTCTMIDNM,
                   OcaDataOut (LOANAGNTCNTCTLASTNM) AS LOANAGNTCNTCTLASTNM,
                   LOANAGNTCNTCTSFXNM,
                   OcaDataOut (LOANAGNTADDRSTR1NM) AS LOANAGNTADDRSTR1NM,
                   OcaDataOut (LOANAGNTADDRSTR2NM) AS LOANAGNTADDRSTR2NM,
                   LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND,
                   LOANAGNTID,
                   TO_CHAR (LOANCDCTPLFEEAMT, 'FM999999999999990.00')
                      LOANCDCTPLFEEAMT
              FROM LOAN.LoanAgntTbl
             WHERE LOANAPPNMB = p_LOANAPPNMB;


         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;
END;
/

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELTSP TO UPDLOANROLE;

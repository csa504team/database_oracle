CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYPRINTSELCSP (
    p_LoanAppNmb   IN     NUMBER := NULL,
    p_LoanNmb      IN     CHAR := NULL,
    p_SelCur1         OUT SYS_REFCURSOR,
    p_SelCur2         OUT SYS_REFCURSOR,
    p_SelCur3         OUT SYS_REFCURSOR,
    p_SelCur4         OUT SYS_REFCURSOR,
    p_SelCur5         OUT SYS_REFCURSOR,
    p_SelCur6         OUT SYS_REFCURSOR,
    p_SelCur7         OUT SYS_REFCURSOR,
    p_SelCur8         OUT SYS_REFCURSOR,
    p_SelCur9         OUT SYS_REFCURSOR,
    p_SelCur10        OUT SYS_REFCURSOR,                                    --
    p_SelCur11        OUT SYS_REFCURSOR,                                    --
    p_SelCur12        OUT SYS_REFCURSOR,                                    --
    p_SelCur13        OUT SYS_REFCURSOR,
    p_SelCur14        OUT SYS_REFCURSOR,                                    --
    p_SelCur15        OUT SYS_REFCURSOR,
    p_SelCur16        OUT SYS_REFCURSOR,
    p_SelCur17        OUT SYS_REFCURSOR,
    p_SelCur18        OUT SYS_REFCURSOR,
    p_selCur19        OUT SYS_REFCURSOR,
    p_SelCur20        OUT SYS_REFCURSOR,
    p_Selcur21        OUT SYS_REFCURSOR,
    p_Selcur22        OUT SYS_REFCURSOR,
    p_Selcur23        OUT SYS_REFCURSOR,
    p_Selcur24        OUT SYS_REFCURSOR)
AS
    /*
     Created by:
     Created on:
     Purpose:
     Parameters:
     Modified by: APK - 10/08/2010-- Removed column LOANLENDRSERVFEEPCT from the LoanGntyPartLendrTbl and LoanPartLendrHistryTbl table;
     Modified by: APK - 11/10/2010-- Modifed the query for cursor result set 2 as it was returing incorrect result set.
     APK         03/18/2011      Modified to add LoanAppMatExtDt
     APK          12/07/2011      Modified to comment the field "CAST(LoanCollatLqdValAmt AS NUMBER(15, 2)) LoanCollatLqdValNum" as LoanCollatLqdValAmt has been deleted from the underlying table.
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     SP :9/13/2012: Modified as LOANPRTCMNTSEQNMB is removed
     SP -- 11/26/2013 -- Removed references to columns :IMRtTypCd,LoanAppFixVarInd,
                         LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd frpm p_selcur2 and added it in p_selcur18
     RG -- 02/04/2014 -- Added new fields to result sets to address an issue with printing.
     RG -- 02/18/2014 -- Added p_SelCur19 to return the Associate Information.
     SB  --04/04/2014  -- Removed NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM columns
     SB  --04/08/2014  -- Added NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND columns
     NK--08/08/2016--OPSMDEV-1030-- Replaced LOANAFFILIND with LoanCntlIntInd and LoanCntlIntTyp
     RGG -- 09/12/2016 -- OPSMDEV 1103 Added new columns for making Use of Proceeds support Loan Auth LOANPROCDREFDESCTXT, LOANPROCDPURAGRMTDT,
                          LOANPROCDPURINTANGASSETAMT, LOANPROCDPURINTANGASSETDESC, LOANPROCDPURSTKHLDRNM
     NK--09/28/2016-- OPSMDEV-1181 added ,  LoanDisblInsurRqrdInd   ,  LoanInsurDisblDescTxt  ,  LoanLifeInsurRqrdInd    ,  LOANPRININSURAMT   ,  LoanPrinNoNCAEvdncInd   ,LoanComptitrNm
                            In Result set 7
     NK-- 10/03/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in   Result set 6 and 7.
     BR-- 10/24/2016 --OPSMDEV-1147 added GntySubCondTypCd to p_selcur11,  GntySubCondTypCd and LACGNTYCONDTYPCD to p_selcur12
     NK--10/27/2016--OPSMDEV 1171-- added NCAIncldInd,StkPurCorpText in result set 3.2
     NK-- 11/02/2016-- OPSMDEV 1180-- added ,a.LiabInsurRqrdInd,a.ProdLiabInsurRqrdInd,a.LiqLiabInsurRqrdInd,a.MalPrctsInsurRqrdInd ,a.OthInsurRqrdind ,a.OthInsurDescTxt,a.PymtLessCrdtInd ,a.InsurDisblDescTxt
                                             ,a.RecrdStmtTypCd ,a.RecrdStmtYrEndDayNmb in result set 5.1.
     NK--09/28/2016--OPSMDEV 1179-- added LoanInjctnFundTermYrNmb
     NK--11/10/2016--OPSMDEV 1229-- Added p_SelCur20 for Stand By Agreement.
     SS--11/17/16--OPSMDEV 1239 Added new columns for additional conditions collateral to cursor 5
     SS--11/21/2016--OPSMDEV     Added cursor 21 for repayment
     NK--12/09/2016-- OPSMDEV 1253 -- added LoanPrinInsurNm in result set 7
     SS--12/29/2016 Added Cohortcd,Undrwritngby,RecovInd,SBAOFC1NM to cursor 2
     BR--01/11/2017-- OPSMDEV-1286 -- added VetCertInd as selection from bus/per tbls
     RY--03/06/2017--OPSMDEV-1400-- Added LOANGNTYCSAAMTRCVD to p_SelCur2
     RY--03/06/2017--OPSMDEV-1400-- Added LoanPartLendrAcctNm,LoanPartLendrAcctNmb,LoanPartLendrTranCd,LoanPartLendrAttn,LndrLoanNmb to p_SelCur14
     BR--03/13/2017--OPSMDEV-1395-- Added  AdverChngInd  to p_SelCur10
     RY-- 03/28/2017-OPSMDEV1401--Removed EconDevObjctCd and changed the reference for EconDevObjctCd in  p_SelCur8
     RY -- 05/16/17 -- OPSMDEV-1433:: Added LOANINTADJPRDEFFDT in p_SelCur18
     SS--06/06/2017--OPSMDEV--1359 --Put outer join between LoanGntyProjTbl and loan.LoanGntyEconDevObjctChldTbl in p_selcur8
     SS--06/08/2017 OPSMDEV 1408 --Added ,LOANPARTLENDRPHYADDRCNTRYCD ,LOANPARTLENDRPHYADDRPOSTCD ,LOANPARTLENDRPHYADDRSTNM  to p_selcur 14
     BR -- 06/14/2017 -- OPSMDEV - 1359 -- Added p_SelCur23
     NK -- 07/19/2017--CAFSOPER 805 -- Added LoanAppBalToBorrAmt,LoanAppCDCGrossDbentrAmt in p_SelCur2
     NK--07/19/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt,LoanGntyNoteDt in p_SelCur2
     BR --11/14/2017--OPSMDEV 1577 -- Added LoanIntDtlGuarInd from Loan.loanintdtltbl p_SelCur18
     NK--11/14/2017--OPSMDEV 1558 Added BusAltPhnNmb, BusPrimEmailAdr,BusAltEmailAdr to borrower section
    NK-- 11/14/2017--OPSMDEV 1551 Added perAltPhnNmb ,perPrimEmailAdr,perAltEmailAdr,BusAltPhnNmb, BusPrimEmailAdr,BusAltEmailAdr and PerCitznShpCntNm p_selcur 12
    SS--11/15/2017--OPSMDEV 1567 Added LOANBUSESTDT to p_selcur 8 and BusPrimCntctNm,BusPrimCntctEmail to p_selcur 10;
    NK--12/13/2017--OPSMDEV 1605 Added LOANMAILSTR1NM, LOANMAILSTR2NM, LOANMAILCTYNM,LOANMAILSTCD,
    LOANMAILZIPCD, LOANMAILZIP4CD, LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD, LoanMailAddrsInvldInd to p_SelCur2
    RY--01/16/2018--OPSMDEV--1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty to p_SelCur2
    SS--03/23/2018 --OPSMDEV--1728--Added LoanGntyDbentrIntRt to p_SelCur2
    RY--04/20/2018--OPSMDEV-1786--Added WorkrsCompInsRqrdInd to p_SelCur10
    NK--07/31/2018-- CAFSOPER 1971 -- Added condition loanpartlendrtypcd = 'P'(participating lender) in getting sum ThirdPartyLoanAmt
    SS--08/06/2018--OPSMDEV 1882--Added p_SelCur24
    SS--10/26/2018--OPSMDEV 1966 Added LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd to p_selcur 2
     --RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns in LOANAGNTTBL
      RY--11/19/2018-OPSMDEV-2044:: Added OCADATAOUT to PII columns in LOANGNTYPARTLENDRTBL
       -- RY-11/27/2018--OPSMDEV-2044:: Added OCADATAOUT for PII columns in PERTBL
       RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt
       BR -- 02/01/2019--CAFSOPER2459-- Added  OcaDataOut() to missed Fields in p_SelCur11
 --RY--07/09/2019--OPSMDEV--2240--Added LoanGntycorrespLangCd to p_selcur 2
 SS--04/03/2020 Added LoanAppMonthPayroll to p_selcur 2 CARESACT - 66
 SS--04/26/2020 Added ACHAcctNmb,ACHRtngNmb,ACHAcctTypCd,ACHTinNmb to p_selcur 2
 SS--4/30/2020 Added Loan1502CertInd to p_selcur 2
 SS--5/7/2020 Added  lendrRebateFeeAmt,  LendrRebatefeePaidDt   to p_selcur 2 CARESACT 205
 SL--06/24/2020 CARESACT-595 Updated to use LoanGntyBusPrinTbl where it was BusPrinTbl
 SL--07/31/2020 CARESACT-595 Modifed to use LoanGntyBusTbl data instead of BusTbl when get loan level bus data
SS--08/03/2020 CARESACT 595 Modified P_selcur 10,11,12,19 to add loangntypertbl
JP -- 10/19/2020 - CAGSOPER 4001-- Added  and  a.LoanAppNmb = f.LoanAppNmb in Qry 11
SL--02/01/2021 SODSTORY 446 added column LoanGntyPayRollChngInd
JS--02/15/2021 Loan Authorization Modernization. Added LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to Guarantor - p_SelCur11
SL --03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 
*/
    v_LoanAppNmb   NUMBER := p_LoanAppNmb;
    p_sys_error    NUMBER := 0;
    p_error        NUMBER;
    p_ProcdTypCd   CHAR (1);
    p_SelCur       SYS_REFCURSOR;
BEGIN
    IF p_LoanAppNmb IS NULL
    THEN
        BEGIN
            SELECT LoanAppNmb
              INTO v_LoanAppNmb
              FROM LoanGntyTbl
             WHERE LoanNmb = p_LoanNmb;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                raise_application_error (-20002,
                                         '99999:Loan Number doesnt exist');
        END;
    END IF;

    BEGIN
        SELECT ProcdTypCd
          INTO p_ProcdTypCd
          FROM LoanGntyTbl a, sbaref.PrgrmValidTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.PrgmCd = b.PrgrmCd
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            p_ProcdTypCd := NULL;
    END;

    OPEN p_SelCur1 FOR                                        -- result set 1:
        /* Special purpose */
        SELECT b.SpcPurpsLoanCd, b.SpcPurpsLoanDesc
          FROM LoanGntySpcPurpsTbl a, sbaref.SpcPurpsLoanTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.SpcPurpsLoanCd = b.SpcPurpsLoanCd;

    OPEN p_SelCur2 FOR                                        -- result set 3:
        /* B. Loan Summary */
        /*APK -- 11/10/2010 modified below query*/
        SELECT LoanAppNm,
               a.PrgmCd,
               c.PrgrmDesc,
               a.PrcsMthdCd,
               b.PrcsMthdDesc,
               CAST (LoanInitlAppvAmt AS NUMBER (15, 2))
                   LoanInitlAppvAmt,
               CAST (LoanCurrAppvAmt AS NUMBER (15, 2))
                   LoanCurrAppvAmt,
               CAST (LoanAppRqstAmt AS NUMBER (15, 2))
                   LoanAppRqstNum,
               h.LoanTransMatMoQty
                   AS LoanInitMatMoQty,
               CAST (h.LoanTransChngGntyFeeAmt AS NUMBER (15, 2))
                   LoanInitGntyFeeAmt,
               h.LoanTransSBAGntyPct
                   AS LoanInitSBAGntyPct,
               LoanAppSBAGntyPct,
               LoanAppRqstMatMoQty,
               LoanAppEPCInd,
               CAST (LoanAppPymtAmt AS NUMBER (15, 2))
                   AS LoanAppPymtNum,
               LoanAppFullAmortPymtInd,
               LoanAppMoIntQty,
               LoanAppLifInsurRqmtInd,
               LoanAppRcnsdrtnInd,
               LoanAppInjctnInd,
               LoanAppEligEvalInd,
               LoanAppNewBusCd,
               BusAgeDesc
                   AS LoanAppNewBusTxt,
               LoanAppDisasterCntrlNmb,
               LOANDISASTRAPPFEECHARGED,
               LOANDISASTRAPPDCSN,
               LOANASSOCDISASTRAPPNMB,
               LOANASSOCDISASTRLOANNMB,
               LOANPYMNTSCHDLFREQ,
               NVL (
                   (SELECT CAST (SUM (LoanPartLendrAmt) AS NUMBER (15, 2))
                      FROM loan.LoanGntyPartLendrTbl
                     WHERE     LoanAppNmb = p_LoanAppNmb
                           AND LOANPARTLENDRTYPCD = 'P'),
                   0)
                   AS ThirdPartyLoanAmt,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanAppRecvDt,
               LoanAppEntryDt,
               CAST (LoanInitlAppvAmt AS NUMBER (15, 2))
                   AS LoanAppSBAAppvNum,
               LoanAppDisasterCntrlNmb,
               CAST (LoanOutBalAmt AS NUMBER (15, 2))
                   AS LoanOutBalNum,
               LoanOutBalDt,
               LoanInitlAppvDt,
               LoanLastAppvDt,
               g.LoanStatCd,
               g.LoanStatDescTxt,
               LoanStatDt,
               LoanAppOrigntnOfcCd,
               LoanAppPrcsOfcCd,
               LoanAppServOfcCd,
               LoanAppFundDt,
               LoanNmb,
               LoanGntyFeeAmt,
               a.PrtId,
               a.LocId,
               LoanAppPrtAppNmb,
               LoanAppPrtLoanNmb,
               LoanCollatInd,
               LoanAppFirstDisbDt,
               LoanAppMatDt,
               LoanSrvsLocId,
               PrtLocNm,
               LoanTotUndisbAmt,
               LoanNextInstlmntDueDt,
               LoanDisbInd,
               LoanOutBalAmt,
               LoanSoldScndMrktInd,
               CAST (LoanAppCDCGntyAmt AS NUMBER (15, 2))
                   AS LoanAppCDCGntyAmt,
               CAST (LoanAppCDCNetDbentrAmt AS NUMBER (15, 2))
                   LoanAppCDCNetDbentrAmt,
               CAST (LoanAppCDCFundFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCFundFeeAmt,
               LoanAppCDCSeprtPrcsFeeInd,
               CAST (LoanAppCDCPrcsFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCPrcsFeeAmt,
               CAST (LoanAppCDCClsCostAmt AS NUMBER (15, 2))
                   LoanAppCDCClsCostAmt,
               CAST (LoanAppCDCOthClsCostAmt AS NUMBER (15, 2))
                   LoanAppCDCOthClsCostAmt,
               CAST (LoanAppCDCUndrwtrFeeAmt AS NUMBER (15, 2))
                   LoanAppCDCUndrwtrFeeAmt,
               LoanAppContribPct,
               CAST (LoanAppContribAmt AS NUMBER (15, 2))
                   LoanAppContribAmt,
               a.FININSTRMNTTYPIND,
               a.NOTEPARENTLOANNMB,
               a.NOTEGENNEDIND,
               a.LOANPURCHSDIND,
               a.LoanAppMatExtDt,
               a.LOANDISASTRAPPNMB,
               a.LoanAppLSPHQLocId,
               a.LOANGNTYMATSTIND,
               a.Cohortcd,
               a.Undrwritngby,
               a.RecovInd,
               (SELECT INITCAP (d.Sbaofc1Nm)
                  FROM SBAREF.SBAOFCTBL d
                 WHERE a.LoanAppServOfcCd = d.SBAOFCCD)
                   AS SBAOFC1NM,
               (SELECT e.LOAN_GNTY_MAT_ST_DESC_TXT
                  FROM SBAREF.LOANMATSTINDCDTBL e
                 WHERE a.LOANGNTYMATSTIND = e.LOAN_GNTY_MAT_ST_IND)
                   AS LOANGNTYMATSTDESCTXT,
               (SELECT P.PRTLOCNM
                  FROM PARTNER.PRTLOCTBL p
                 WHERE P.LOCID = a.LoanAppLSPHQLocId)
                   LSPLocNm,
               LOANGNTYCSAAMTRCVD,
               a.LoanAppBalToBorrAmt,
               a.LoanAppCDCGrossDbentrAmt,
               a.LoanGntyNoteDt,
               a.LoanGntyNoteIntRt,
               a.LoanGntyNoteMntlyPymtAmt,
               a.LOANMAILSTR1NM,
               a.LOANMAILSTR2NM,
               a.LOANMAILCTYNM,
               a.LOANMAILSTCD,
               a.LOANMAILZIPCD,
               a.LOANMAILZIP4CD,
               a.LOANMAILCNTRYCD,
               a.LOANMAILSTNM,
               a.LOANMAILPOSTCD,
               a.LoanMailAddrsInvldInd,
               a.LoanAppRevlMoIntQty,
               a.LoanAppAmortMoIntQty,
               a.LoanGntyDbentrIntRt,
               a.LoanAgntInvlvdInd,
               a.LoanAppExtraServFeeAMT,
               a.LoanAppExtraServFeeInd,
               a.LoanGntycorrespLangCd,
               a.LoanAppMonthPayroll,
               a.LoanGntyPayRollChngInd,
               a.ACHAcctNmb,
               a.ACHRtngNmb,
               a.ACHAcctTypCd,
               a.ACHTinNmb,
               a.Loan1502CertInd,
               a.lendrRebateFeeAmt,
               a.LendrRebatefeePaidDt,
               a.LOANAPPSCHEDCYR, 
               a.LOANAPPGROSSINCOMEAMT
          FROM loan.LoanGntyTbl  a
               LEFT OUTER JOIN sbaref.PrcsMthdTbl b
                   ON (a.PrcsMthdCd = b.PrcsMthdCd)
               LEFT OUTER JOIN sbaref.PrgrmTbl c ON (a.PrgmCd = c.PrgrmCd)
               LEFT OUTER JOIN sbaref.BusAgeCdTbl f
                   ON (a.LoanAppNewBusCd = f.BusAgeCd)
               LEFT OUTER JOIN sbaref.LoanStatCdTbl g
                   ON (a.LoanStatCd = g.LoanStatCd)
               LEFT OUTER JOIN loan.LoanTransTbl h
                   ON (a.LoanAppNmb = h.LoanAppNmb AND h.LoanTransTypCd = 1)
               LEFT OUTER JOIN partner.LocSrchTbl l
                   ON (a.LoanSrvsLocId = l.LocId)
         WHERE a.LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur3 FOR                                      -- result set 3.1:
                                                               /* Injection */
        SELECT D.InjctnTypCd,
               r.InjctnTypTxt,
               CASE WHEN D.InjctnTypCd = 'O' THEN '~' ELSE r.InjctnTypCd END
                   DisplayOrder,
               CAST (D.LoanInjctnAmt AS NUMBER (12, 2))
                   LoanInjctnNum,
               D.LoanInjctnOthDescTxt,
               D.LoanInjctnFundTermYrNmb
          FROM LoanGntyInjctnTbl D, sbaref.InjctnTypCdTbl r
         WHERE D.LoanAppNmb = p_LoanAppNmb AND D.InjctnTypCd = r.InjctnTypCd
        UNION ALL
        SELECT a.InjctnTypCd,
               a.InjctnTypTxt,
               CASE WHEN a.InjctnTypCd = 'O' THEN '~' ELSE a.InjctnTypCd END
                   DisplayOrder,
               NULL,
               NULL,
               NULL
          FROM sbaref.InjctnTypCdTbl a
         WHERE     a.InjctnTypStrtDt <= SYSDATE
               AND (a.InjctnTypEndDt IS NULL OR a.InjctnTypEndDt >= SYSDATE)
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanGntyInjctnTbl z
                         WHERE     z.LoanAppNmb = p_LoanAppNmb
                               AND z.InjctnTypCd = a.InjctnTypCd)
        ORDER BY DisplayOrder;

    OPEN p_SelCur4 FOR                                      -- result set 3.2:
        /* C. Use of Proceeds */
        SELECT a.ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               CAST (a.LoanProcdAmt AS NUMBER (12, 2))     LoanProcdNum,
               a.LoanProcdOthTypTxt,
               a.LOANPROCDREFDESCTXT,
               a.LOANPROCDPURAGRMTDT,
               a.LOANPROCDPURINTANGASSETAMT,
               a.LOANPROCDPURINTANGASSETDESC,
               a.LOANPROCDPURSTKHLDRNM,
               a.NCAIncldInd,
               a.StkPurCorpText
          FROM Loan.LoanGntyProcdTbl a, sbaref.LoanProcdTypTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ProcdTypCd = b.ProcdTypCd
               AND a.LoanProcdTypCd = b.LoanProcdTypCd
        UNION ALL
        SELECT p_ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL
          FROM sbaref.LoanProcdTypTbl b
         WHERE     ProcdTypCd = p_ProcdTypCd
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanGntyProcdTbl a
                         WHERE     a.LoanAppNmb = p_LoanAppNmb
                               AND a.ProcdTypCd = b.ProcdTypCd
                               AND a.LoanProcdTypCd = b.LoanProcdTypCd)
               AND LoanProcdTypCdStrtDt <= SYSDATE
               AND (   LoanProcdTypCdEndDt IS NULL
                    OR LoanProcdTypCdEndDt + 1 > SYSDATE)
        ORDER BY LoanProcdTypCd;

    OPEN p_SelCur5 FOR                                        -- result set 4:
          /* D. Collateral */
          SELECT LoanCollatSeqNmb,
                 a.LoanCollatTypCd,
                 b.LoanCollatTypDescTxt,
                 a.LoanCollatDesc,
                 CAST (LoanCollatMrktValAmt AS NUMBER (15, 2))
                     LoanCollatMrktValNum,
                 LoanCollatOwnrRecrd,
                 LoanCollatSBALienPos,
                 a.LoanCollatValSourcCd,
                 c.LoanCollatValSourcDescTxt,
                 LoanCollatValDt,
                 LOANCOLLATSUBTYPCD,
                 SecurShrPariPassuInd                                     --SS
                                     ,
                 SecurShrPariPassuNonSBAInd,
                 SecurPariPassuLendrNm,
                 SecurPariPassuAmt,
                 SecurLienLimAmt,
                 SecurInstrmntTypCd,
                 SecurWaterRightInd,
                 SecurRentAsgnInd,
                 SecurSellerNm,
                 SecurPurNm,
                 SecurOwedToSellerAmt,
                 SecurCDCDeedInEscrowInd,
                 SecurSellerIntDtlInd,
                 SecurSellerIntDtlTxt,
                 SecurTitlSubjPriorLienInd,
                 SecurPriorLienTxt,
                 SecurPriorLienLimAmt,
                 SecurSubjPriorAsgnInd,
                 SecurPriorAsgnTxt,
                 SecurPriorAsgnLimAmt,
                 SecurALTATltlInsurInd,
                 SecurLeaseTermOverLoanYrNmb,
                 LandlordIntProtWaiverInd,
                 SecurDt,
                 SecurLessorTermNotcDaysNmb,
                 SecurDescTxt,
                 SecurPropAcqWthLoanInd,
                 SecurPropTypTxt,
                 SecurOthPropTxt,
                 SecurLienOnLqorLicnsInd,
                 SecurMadeYrNmb,
                 SecurLocTxt,
                 SecurOwnerNm,
                 SecurMakeNm,
                 SecurAmt,
                 SecurNoteSecurInd,
                 SecurStkShrNmb,
                 SecurLienHldrVrfyInd,
                 SecurTitlVrfyTypCd,
                 SecurTitlVrfyOthTxt,
                 FloodInsurRqrdInd,
                 REHazardInsurRqrdInd,
                 PerHazardInsurRqrdInd,
                 FullMarInsurRqrdInd,
                 EnvInvstgtSBAAppInd,
                 LeasePrmTypCd,
                 ApprTypCd,
                 CollatAppOrdDt,
                 (SELECT d.LAC_COND_TYP_DESC_TXT
                    FROM LOAN.LAC_COND_TYPCD_TBL d
                   WHERE a.LoanCollatSubTypCd = d.LAC_COND_TYP_CD)
                     AS LAC_COND_TYP_DESC_TXT,
                 (SELECT e.LAC_ALTA_PLCY_DESC_TXT
                    FROM LOAN.LAC_ALTA_PLCY_CD_TBL e
                   WHERE a.SecurTitlVrfyTypCd = e.LAC_ALTA_PLCY_CD)
                     AS LAC_ALTA_PLCY_DESC_TXT,
                 (SELECT z.leastypdesctxt
                    FROM sbaref.leastypcdtbl z
                   WHERE a.leaseprmtypcd = z.leastypcd)
                     AS LEASDESCTXT,
                 (SELECT g.Propapprsltypdesctxt
                    FROM sbaref.Propapprsltypcdtbl g
                   WHERE a.ApprTypcd = g.Propapprsltypcd)
                     AS APPRSLDESCTXT,
                 (SELECT f.collatstatdesctxt
                    FROM sbaref.collatstatcdtbl f
                   WHERE a.collatstatcd = f.collatstatcd)
                     AS COLLATSTATDESCTXT
            FROM LoanGntyCollatTbl           a,
                 sbaref.LoanCollatTypCdTbl   b,
                 sbaref.LoanCollatValSourcTbl c
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCollatTypCd = b.LoanCollatTypCd
                 AND a.LoanCollatValSourcCd = c.LoanCollatValSourcCd(+)
        ORDER BY LoanCollatSeqNmb;

    OPEN p_SelCur6 FOR                                      -- result set 4.1:
        /* Collateral Lien*/
        SELECT LoanCollatSeqNmb,
               LoanCollatLienSeqNmb,
               LoanCollatLienHldrNm,
               LoanCollatLienPos,
               CAST (LoanCollatLienBalAmt AS NUMBER (15, 2))
                   LoanCollatLienBalNum
          FROM LoanGntyCollatLienTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur7 FOR                                      -- result set 4.2:
                       /* E. Lender Comments */
                       SELECT LoanPrtCmntTxt
                         FROM LoanGntyPrtCmntTbl
                        WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur8 FOR                                        -- result set 5:
        /* G. Project Information*/
        SELECT a.LoanAppNmb,
               LoanAppProjStr1Nm,
               LoanAppProjStr2Nm,
               LoanAppProjCtyNm,
               LoanAppProjCntyCd,
               LoanAppProjStCd,
               LoanAppProjZipCd,
               LoanAppProjZip4Cd,
               LoanAppRuralUrbanInd,
               CAST (LoanAppNetExprtAmt AS NUMBER (15, 2))
                   LoanAppNetExprtAmt,
               LoanAppCurrEmpQty,
               LoanAppJobRtnd,
               LoanAppJobRqmtMetInd,
               LoanAppCDCJobRat,
               a.NAICSYRNMB,
               a.NAICSCD,
               a.LOANAPPFRNCHSCD,
               a.LOANAPPFRNCHSNM,
               a.LOANAPPFRNCHSIND,
               b.EconDevObjctTxt,
               ADDTNLLOCACQLMTIND                                         --SS
                                 ,
               FIXASSETACQLMTIND,
               FIXASSETACQLMTAMT,
               COMPSLMTIND,
               COMPSLMTAMT,
               BULKSALELAWCOMPLYIND,
               FRNCHSRECRDACCSIND,
               FRNCHSFEEDEFMONNMB,
               FRNCHSFEEDEFIND,
               FRNCHSTERMNOTCIND,
               FRNCHSLENDRSAMEOPPIND,
               LoanAppJobCreatQty,
               LOANBUSESTDT,
               (SELECT z.LqdCrTotScr
                  FROM loanapp.LqdCrWSCTbl z
                 WHERE     z.LoanAppNmb = p_LoanAppNmb
                       AND z.TaxId = o.TaxId
                       AND z.LqdCrSeqNmb =
                           (SELECT MAX (LqdCrSeqNmb)
                              FROM loanapp.LqdCrWSCTbl y
                             WHERE     y.LoanAppNmb = p_LoanAppNmb
                                   AND y.TaxId = o.TaxId))
                   AS LqdCrTotScr,
               (SELECT g.DESCTXT
                  FROM Sbaref.NAICSTBL g
                 WHERE a.NAICSCD = g.NAICSCD AND a.NAICSYRNMB = g.NAICSYRNMB)
                   AS NAICSDESCTXT
          FROM LoanGntyProjTbl                   a,
               LoanGntyBorrTbl                   o,
               sbaref.EconDevObjctCdTbl          b,
               loan.LoanGntyEconDevObjctChldTbl  c
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND o.LoanAppNmb = p_LoanAppNmb
               AND o.LOANBUSPRIMBORRIND = 'Y'
               AND o.LOANBORRACTVINACTIND = 'A'
               AND a.LoanAppNmb = c.LoanAppNmb(+)
               AND c.LoanEconDevObjctCd = b.EconDevObjctCd(+);

    OPEN p_SelCur9 FOR                                        -- result set 6:
        /* H. Financial Statements*/
        SELECT LoanBSDt,
               CAST (LoanBSCashEqvlntAmt AS NUMBER (15, 2))
                   LoanBSCashEqvlntAmt,
               CAST (LoanBSNetTrdRecvAmt AS NUMBER (15, 2))
                   LoanBSNetTrdRecvAmt,
               CAST (LoanBSTotInvtryAmt AS NUMBER (15, 2))
                   LoanBSTotInvtryAmt,
               CAST (LoanBSOthCurAssetAmt AS NUMBER (15, 2))
                   LoanBSOthCurAssetAmt,
               CAST (LoanBSTotCurAssetAmt AS NUMBER (15, 2))
                   LoanBSTotCurAssetAmt,
               CAST (LoanBSTotFixAssetAmt AS NUMBER (15, 2))
                   LoanBSTotFixAssetAmt,
               CAST (LoanBSTotOthAssetAmt AS NUMBER (15, 2))
                   LoanBSTotOthAssetAmt,
               CAST (LoanBSTotAssetAmt AS NUMBER (15, 2))
                   LoanBSTotAssetAmt,
               CAST (LoanBSAcctsPayblAmt AS NUMBER (15, 2))
                   LoanBSAcctsPayblAmt,
               CAST (LoanBSCurLTDAmt AS NUMBER (15, 2))
                   LoanBSCurLTDAmt,
               CAST (LoanBSOthCurLiabAmt AS NUMBER (15, 2))
                   LoanBSOthCurLiabAmt,
               CAST (LoanBSTotCurLiabAmt AS NUMBER (15, 2))
                   LoanBSTotCurLiabAmt,
               CAST (LoanBSLTDAmt AS NUMBER (15, 2))
                   LoanBSLTDAmt,
               CAST (LoanBSOthLTLiabAmt AS NUMBER (15, 2))
                   LoanBSOthLTLiabAmt,
               CAST (LoanBSStbyDbt AS NUMBER (15, 2))
                   LoanBSStbyDbt,
               CAST (LoanBSTotLiab AS NUMBER (15, 2))
                   LoanBSTotLiab,
               CAST (LoanBSBusNetWrth AS NUMBER (15, 2))
                   LoanBSBusNetWrth,
               CAST (LoanBSTngblNetWrth AS NUMBER (15, 2))
                   LoanBSTngblNetWrth,
               LoanBSActlPrfrmaInd,
               a.LoanFinanclStmtSourcCd,
               b.LoanFinanclStmtSourcDescTxt
          FROM LoanGntyBSTbl a, sbaref.LoanFinanclStmtSourcTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd;


    OPEN p_SelCur10 FOR                                     -- result set 5.1:
                                                              /* I.Borrower */
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm                            -- Pranesh 06/05/06
                               ,
               f.BusPhyAddrCntCd                               -- Pks 05/11/06
                                ,
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd                          -- Pranesh 06/05/06
                                 ,
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm                           -- Pranesh 06/05/06
                                ,
               f.BusMailAddrCntCd                              -- Pks 05/11/06
                                 ,
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd                         -- Pranesh 06/05/06
                                  ,
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               f.BusDUNSNmb,
               a.LoanCntlIntInd                              -- RGG 02/03/2014
                               ,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                           --NK 08/30/2016
                                    ,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb,
               a.WorkrsCompInsRqrdInd,
               b.VetCertInd,
               a.AdverChngInd,
               f.BusAltPhnNmb,
               f.BusPrimEmailAdr,
               f.BusAltEmailAdr                        --   , b.BusPrimCntctNm
                               ,
               f.BusPrimPhnNmb,
               NULL
                   PerOthrCmntTxt
          FROM BusTbl                     b,
               Loan.LoanGntyBusTbl        f,
               LoanGntyBorrTbl            a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D,
               sbaref.IMEPCOperCdTbl      e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND f.LoanAppNmb = a.LoanAppNmb
               AND a.TaxId = b.TaxId
               AND BorrBusPerInd = 'B'
               AND LoanBorrActvInactInd = 'A'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        UNION ALL
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               f.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               f.PerAlienRgstrtnNmb,
               f.PerAffilEmpFedInd,
               f.PerIOBInd,
               f.PerIndctPrleProbatnInd,
               f.PerCrmnlOffnsInd,
               f.PerCnvctInd,
               NULL
                   BusExprtInd,
               f.PerBnkrptcyInd,
               f.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (f.PerPrimPhnNmb)
                   AS PerPrimPhnNmb                            -- pks 05/03/06
                                   ,
               OcaDataOut (f.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcaDataOut (f.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               f.PerPhyAddrCtyNm,
               f.PerPhyAddrStCd,
               f.PerPhyAddrStNm                            -- Pranesh 06/05/06
                               ,
               f.PerPhyAddrCntCd,
               f.PerPhyAddrZipCd,
               f.PerPhyAddrZip4Cd,
               f.PerPhyAddrPostCd                          -- Pranesh 06/05/06
                                 ,
               OcaDataOut (f.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcaDataOut (f.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               f.PerMailAddrCtyNm,
               f.PerMailAddrStCd,
               f.PerMailAddrStNm                           -- Pranesh 06/05/06
                                ,
               f.PerMailAddrCntCd,
               f.PerMailAddrZipCd,
               f.PerMailAddrZip4Cd,
               f.PerMailAddrPostCd                         -- Pranesh 06/05/06
                                  ,
               f.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.PerExtrnlCrdtScorNmb,
               f.PerExtrnlCrdtScorDt,
               NULL
                   AS BusDUNSNmb,
               a.LoanCntlIntInd                              -- RGG 02/03/2014
                               ,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                           --NK 08/30/2016
                                    ,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb,
               a.WorkrsCompInsRqrdInd,
               b.VetCertInd,
               a.AdverChngInd,
               NULL
                   BusAltPhnNmb,
               NULL
                   BusPrimEmailAdr,
               NULL
                   BusAltEmailAdr                    --  , NULL BusPrimCntctNm
                                 ,
               NULL
                   BusPrimPhnNmb,
               B.PerOthrCmntTxt
          FROM PerTbl                     b,
               Loangntypertbl             f,
               LoanGntyBorrTbl            a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D,
               sbaref.IMEPCOperCdTbl      e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND f.loanappnmb = a.loanappnmb
               AND BorrBusPerInd = 'P'
               AND LoanBorrActvInactInd = 'A'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        ORDER BY BorrSeqNmb;

    OPEN p_SelCur11 FOR                                       -- result set 6:
        /* J. Guarantor */
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,                                -- pks 05/03/06
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm,                           -- Pranesh 06/05/06
               f.BusPhyAddrCntCd,                          -- Pranesh 05/03/06
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd,                         -- Pranesh 06/05/06
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm,                          -- Pranesh 06/05/06
               f.BusMailAddrCntCd,                             -- pks 05/03/06
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd,                        -- Pranesh 06/05/06
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/03/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.GntySubCondTypCd,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt,
               b.VetCertInd,                                   --BR 10/24/2016
               NULL
                   PerOthrCmntTxt
          FROM BusTbl                     b,
               Loan.LoanGntyBusTbl        f,
               LoanGntyGuarTbl            a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND a.loanappnmb = f.loanappnmb          -- JP added 10/19/2020
               AND GuarBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LoanGuarActvInactInd = 'A'
        UNION ALL
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               e.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               e.PerAlienRgstrtnNmb,
               e.PerAffilEmpFedInd,
               e.PerIOBInd,
               e.PerIndctPrleProbatnInd,
               e.PerCrmnlOffnsInd,
               e.PerCnvctInd,
               NULL
                   BusExprtInd,
               e.PerBnkrptcyInd,
               e.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (e.PerPrimPhnNmb)
                   AS PerPrimPhnNmb,                           -- pks 05/03/06
               OcadataOut (e.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcadataOut (e.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               e.PerPhyAddrCtyNm,
               e.PerPhyAddrStCd,
               e.PerPhyAddrStNm,                           -- Pranesh 06/05/06
               e.PerPhyAddrCntCd,                              -- pks 05/03/06
               e.PerPhyAddrZipCd,
               e.PerPhyAddrZip4Cd,
               e.PerPhyAddrPostCd,                         -- Pranesh 06/05/06
               OcadataOut (e.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcadataOut (e.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               e.PerMailAddrCtyNm,
               e.PerMailAddrStCd,
               e.PerMailAddrStNm,                          -- Pranesh 06/05/06
               e.PerMailAddrCntCd,                             -- pks 05/03/06
               e.PerMailAddrZipCd,
               e.PerMailAddrZip4Cd,
               e.PerMailAddrPostCd,                        -- Pranesh 06/05/06
               e.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               e.PerExtrnlCrdtScorNmb,
               e.PerExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                            --RGG 02/04/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.GntySubCondTypCd,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt,
               b.VetCertInd,
               B.PerOthrCmntTxt
          FROM PerTbl                     b,
               Loangntypertbl             e,
               LoanGntyGuarTbl            a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND e.loanappnmb = a.loanappnmb
               AND GuarBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LoanGuarActvInactInd = 'A'
        ORDER BY GuarSeqNmb;


    OPEN p_SelCur12 FOR                                       -- result set 7:
        /* K. Principal */
        SELECT a.BorrSeqNmb,
               a.TaxId
                   BorrTaxId,
               i.PrinTaxId,
               i.PrinBusPerInd,
               b.VetCd,
               e.VetTxt,
               b.GndrCd,
               f.GndrDesc,
               b.EthnicCd,
               g.EthnicDesc,
               h.LoanPrinGntyInd,
               NVL (i.BusPrinOwnrshpPct, 0)
                   LoanBusPrinPctOwnrshpBus,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               j.BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               j.BusExprtInd,
               j.BusBnkrptInd,
               j.BusLwsuitInd,
               j.BusPriorSBALoanInd,
               j.BusPrimPhnNmb,                                -- pks 05/03/06
               j.BusPhyAddrStr1Nm,
               j.BusPhyAddrStr2Nm,
               j.BusPhyAddrCtyNm,
               j.BusPhyAddrStCd,
               j.BusPhyAddrStNm,                           -- Pranesh 06/05/06
               j.BusPhyAddrCntCd,                              -- pks 05/03/06
               j.BusPhyAddrZipCd,
               j.BusPhyAddrZip4Cd,
               j.BusPhyAddrPostCd,                         -- Pranesh 06/05/06
               j.BusMailAddrStr1Nm,
               j.BusMailAddrStr2Nm,
               j.BusMailAddrCtyNm,
               j.BusMailAddrStCd,
               j.BusMailAddrStNm,                          -- Pranesh 06/05/06
               j.BusMailAddrCntCd,                             -- pks 05/03/06
               j.BusMailAddrZipCd,
               j.BusMailAddrZip4Cd,
               j.BusMailAddrPostCd,                        -- Pranesh 06/05/06
               j.BusExtrnlCrdtScorInd,
               j.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               j.BusExtrnlCrdtScorNmb,
               j.BusExtrnlCrdtScorDt,
               j.BusPrimBusExprnceYrNmb
                   AS LoanPrinPrimBusExprnceYrNmb,
               h.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE h.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp,                          --NK 08/15/2016
               h.LoanDisblInsurRqrdInd,
               h.LoanInsurDisblDescTxt,
               h.LoanLifeInsurRqrdInd,
               h.LOANPRININSURAMT,
               h.LoanPrinNoNCAEvdncInd,
               h.LoanComptitrNm,
               h.LoanPrinInsurNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = h.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = h.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               h.GntyLmtAmt,
               h.GntyLmtPct,
               h.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               h.GntyLmtYrNmb,
               GntySubCondTypCd,
               LACGNTYCONDTYPCD,
               b.VetCertInd,
               NULL
                   PerCitznShpCntNm,
               j.BusAltPhnNmb,
               j.BusPrimEmailAdr,
               j.BusAltEmailAdr
          FROM loan.LoanGntyBorrTbl  a
               LEFT OUTER JOIN loan.LoanGntyPrinTbl h
                   ON (a.BorrSeqNmb = h.BorrSeqNmb ---(--h.LoanAppNmb = 7630--a.LoanAppNmb= h.LoanAppNmb--and )
                                                   AND h.PrinBusPerInd = 'B')
               LEFT OUTER JOIN loan.LoanGntyBusPrinTbl i
                   ON (    a.TaxId = i.BusTaxId
                       AND h.PrinTaxId = i.PrinTaxId
                       AND i.LoanAppNmb = p_LoanAppNmb
                       AND i.PrinBusPerInd = 'B'
                       AND i.BusPrinActvInactInd = 'A')
               LEFT OUTER JOIN loan.BusTbl b ON (i.PrinTaxId = b.TaxId)
               LEFT OUTER JOIN Loan.LoanGntyBusTbl j
                   ON b.taxid = j.taxid AND i.LOANAPPNMB = j.LoanAppNmb
               LEFT OUTER JOIN sbaref.BusTypTbl c
                   ON (b.BusTypCd = c.BusTypCd)
               LEFT OUTER JOIN sbaref.IMCrdtScorSourcTbl d
                   ON (b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd)
               LEFT OUTER JOIN sbaref.VetTbl e ON (b.VetCd = e.VetCd)
               LEFT OUTER JOIN sbaref.GndrCdTbl f ON (b.GndrCd = f.GndrCd)
               LEFT OUTER JOIN sbaref.EthnicCdTbl g
                   ON (b.EthnicCd = g.EthnicCd)
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND h.LoanAppNmb = p_LoanAppNmb
               AND a.LoanBorrActvInactInd = 'A'
        /*FROM BusTbl b,
                  LoanGntyBorrTbl a,
                  sbaref.BusTypTbl c,
                  sbaref.IMCrdtScorSourcTbl D,
                  sbaref.VetTbl e,
                  sbaref.GndrCdTbl f,
                  sbaref.EthnicCdTbl g,
                  LoanGntyPrinTbl h,
                  BusPrinTbl i
              WHERE a.LoanAppNmb = p_LoanAppNmb
                      AND h.LoanAppNmb = p_LoanAppNmb
                      AND a.BorrSeqNmb =  h.BorrSeqNmb(+)
                      AND a.TaxId = i.BusTaxId
                      AND i.PrinTaxId =h.PrinTaxId(+)
                      AND i.PrinBusPerInd = 'B'
                      AND i.PrinTaxId = b.TaxId
                      AND h.PrinBusPerInd = 'B'
                      AND b.BusTypCd =c.BusTypCd(+)
                      AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
                      AND b.VetCd =e.VetCd(+)
                      AND b.GndrCd =f.GndrCd(+)
                      AND b.EthnicCd =g.EthnicCd(+)
                      AND i.BusPrinActvInactInd = 'A'
                      AND a.LoanBorrActvInactInd = 'A'*/
        UNION ALL
        SELECT a.BorrSeqNmb,
               a.TaxId
                   BorrTaxId,
               i.PrinTaxId,
               i.PrinBusPerInd,
               b.VetCd,
               e.VetTxt,
               b.GndrCd,
               f.GndrDesc,
               b.EthnicCd,
               g.EthnicDesc,
               h.LoanPrinGntyInd,
               NVL (BusPrinOwnrshpPct, 0)
                   LoanBusPrinPctOwnrshpBus,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               OcaDataOut (b.PerFirstNm)
                   AS PerFirstNm,
               OcaDataOut (b.PerLastNm)
                   AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               s.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               s.PerUSCitznInd,
               c.CitznshipTxt,
               s.PerAlienRgstrtnNmb,
               s.PerAffilEmpFedInd,
               s.PerIOBInd,
               s.PerIndctPrleProbatnInd,
               s.PerCrmnlOffnsInd,
               s.PerCnvctInd,
               NULL
                   BusExprtInd,
               s.PerBnkrptcyInd,
               s.PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               OcaDataOut (s.PerPrimPhnNmb)
                   AS PerPrimPhnNmb,                       -- Pranesh 05/03/06
               OcaDataOut (s.PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               OcaDataOut (s.PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               s.PerPhyAddrCtyNm,
               s.PerPhyAddrStCd,
               s.PerPhyAddrStNm,                           -- Pranesh 06/05/06
               s.PerPhyAddrCntCd,
               s.PerPhyAddrZipCd,
               s.PerPhyAddrZip4Cd,
               s.PerPhyAddrPostCd,                         -- Pranesh 06/05/06
               OcaDataOut (s.PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               OcaDataOut (s.PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               s.PerMailAddrCtyNm,
               s.PerMailAddrStCd,
               s.PerMailAddrStNm,                          -- Pranesh 06/05/06
               s.PerMailAddrCntCd,
               s.PerMailAddrZipCd,
               s.PerMailAddrZip4Cd,
               s.PerMailAddrPostCd,                        -- Pranesh 06/05/06
               s.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               s.PerExtrnlCrdtScorNmb,
               s.PerExtrnlCrdtScorDt,
               s.PerPrimBusExprnceYrNmb
                   LoanPrinPrimBusExprnceYrNmb,
               h.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE h.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp,                         --NK--08/15/2016
               h.LoanDisblInsurRqrdInd,
               h.LoanInsurDisblDescTxt,
               h.LoanLifeInsurRqrdInd,
               h.LOANPRININSURAMT,
               h.LoanPrinNoNCAEvdncInd,
               h.LoanComptitrNm,
               h.LoanPrinInsurNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = h.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = h.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               h.GntyLmtAmt,
               h.GntyLmtPct,
               h.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = h.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = h.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               h.GntyLmtYrNmb,
               GntySubCondTypCd,                               --BR 10/24/2016
               LACGNTYCONDTYPCD,                              --BR 10/24/2016,
               b.VetCertInd,
               (SELECT z.IMCNTRYNM
                  FROM SBAREF.IMCNTRYCDTBL z
                 WHERE z.IMCNTRYCD = s.PerCitznShpCntNm)
                   AS PerCitznShpCntNm,
               OcaDataOut (s.PerAltPhnNmb)
                   AS PerAltPhnNmb,
               OcaDataOut (s.PerPrimEmailAdr)
                   AS PerPrimEmailAdr,
               OcaDataOut (s.PerAltEmailAdr)
                   AS PerAltEmailAdr
          FROM loan.LoanGntyBorrTbl  a
               LEFT OUTER JOIN loan.LoanGntyPrinTbl h
                   ON (a.BorrSeqNmb = h.BorrSeqNmb AND h.PrinBusPerInd = 'P')
               ---h.LoanAppNmb = p_LoanAppNmb--a.LoanAppNmb = h.LoanAppNmb--and
               LEFT OUTER JOIN loan.LoanGntyBusPrinTbl i
                   ON (    a.TaxId = i.BusTaxId
                       AND i.loanAppNmb = p_LoanAppNmb
                       AND h.PrinTaxId = i.PrinTaxId
                       AND i.PrinBusPerInd = 'P'
                       AND i.BusPrinActvInactInd = 'A')
               LEFT OUTER JOIN loan.PerTbl b ON (i.PrinTaxId = b.TaxId)
               LEFT OUTER JOIN loan.Loangntypertbl s
                   ON (s.loanappnmb = a.loanappnmb)
               LEFT OUTER JOIN sbaref.CitznshipCdTbl c
                   ON (b.PerUSCitznInd = c.CitznshipCd)
               LEFT OUTER JOIN sbaref.IMCrdtScorSourcTbl d
                   ON (b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd)
               LEFT OUTER JOIN sbaref.VetTbl e ON (b.VetCd = e.VetCd)
               LEFT OUTER JOIN sbaref.GndrCdTbl f ON (b.GndrCd = f.GndrCd)
               LEFT OUTER JOIN sbaref.EthnicCdTbl g
                   ON (b.EthnicCd = g.EthnicCd)
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND h.LoanAppNmb = p_LoanAppNmb
               AND a.LoanBorrActvInactInd = 'A'
        ORDER BY BorrSeqNmb, PrinTaxId;

    /* FROM PerTbl b,
              LoanGntyBorrTbl a,
              LoanGntyPrinTbl h,
              sbaref.CitznshipCdTbl c,
              sbaref.IMCrdtScorSourcTbl D,
              sbaref.VetTbl e,
              sbaref.GndrCdTbl f,
              sbaref.EthnicCdTbl g,
              BusPrinTbl i
          WHERE a.LoanAppNmb = p_LoanAppNmb
                  AND h.LoanAppNmb = p_LoanAppNmb
                  AND a.BorrSeqNmb =h.BorrSeqNmb(+)
                  AND a.TaxId = i.BusTaxId
                  AND i.PrinTaxId =h.PrinTaxId(+)
                  AND i.PrinBusPerInd = 'P'
                  AND i.PrinTaxId = b.TaxId
                  AND h.PrinBusPerInd = 'P'
                  AND b.PerUSCitznInd =c.CitznshipCd(+)
                  AND b.IMCrdtScorSourcCd =D.IMCrdtScorSourcCd(+)
                  AND b.VetCd =               e.VetCd(+)
                  AND b.GndrCd =               f.GndrCd(+)
                  AND b.EthnicCd =               g.EthnicCd(+)
                  AND i.BusPrinActvInactInd = 'A'
                  AND a.LoanBorrActvInactInd = 'A'
                  ORDER BY BorrSeqNmb, PrinTaxId;*/
    OPEN p_SelCur13 FOR                                       -- result set 8:
        /* Principal Race */
        SELECT BorrSeqNmb,
               PrinTaxId,
               c.RaceCd,
               b.RaceTxt,
               a.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                          -- NK 08/15/2016
          FROM LoanGntyPrinTbl a, BusRaceTbl c, sbaref.RaceCdTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrinBusPerInd = 'B'
               AND a.PrinTaxId = c.TaxId
               AND c.RaceCd = b.RaceCd
        UNION ALL
        SELECT BorrSeqNmb,
               PrinTaxId,
               c.RaceCd,
               b.RaceTxt,
               a.LoanCntlIntInd,                             -- RGG 02/03/2014
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS LoanCntlIntTyp                          -- NK 08/15/2016
          FROM LoanGntyPrinTbl a, PerRaceTbl c, sbaref.RaceCdTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrinBusPerInd = 'P'
               AND a.PrinTaxId = c.TaxId
               AND c.RaceCd = b.RaceCd
        ORDER BY BorrSeqNmb, PrinTaxId;

    OPEN p_SelCur14 FOR                   --Resultset 11, participating lender
        SELECT D.LoanPartLendrNm,
               D.LocId,
               D.LoanPartLendrStr1Nm,
               D.LoanPartLendrStr2Nm,
               D.LoanPartLendrCtyNm,
               D.LoanPartLendrStCd,
               D.LoanPartLendrZip5Cd,
               D.LoanPartLendrZip4Cd,
               D.LoanLienPosCd,
               CAST (D.LoanPartLendrAmt AS NUMBER (12, 2))
                   LoanPartLendrNum,
               OcaDataOut (D.LoanPartLendrOfcrLastNm)
                   AS LoanPartLendrOfcrLastNm,
               OcaDataOut (D.LoanPartLendrOfcrFirstNm)
                   AS LoanPartLendrOfcrFirstNm,
               D.LoanPartLendrOfcrInitialNm,
               D.LoanPartLendrOfcrSfxNm,
               D.LoanPartLendrOfcrTitlTxt,
               OcaDataOut (D.LoanPartLendrOfcrPhnNmb)
                   AS LoanPartLendrOfcrPhnNmb,
               D.LOANPARTLENDRCNTRYCD,
               D.LOANPARTLENDRPOSTCD,
               D.LOANPARTLENDRSTNM,
               r.LoanPartLendrTypDescTxt,
               LOANLENDRTAXID,
               LOANLENDRGROSSINTPCT,
               LOANLENDRPARTPCT,
               LOANORIGPARTPCT,
               LOANSBAGNTYBALPCT,
               LOANLENDRPARTAMT,
               LOANORIGPARTAMT,
               LOANLENDRGROSSAMT,
               LOANSBAGNTYBALAMT,
               LOAN503POOLAPPNMB,
               LoanPartLendrAcctNm,
               LoanPartLendrAcctNmb,
               LoanPartLendrTranCd,
               LoanPartLendrAttn,
               LndrLoanNmb
          FROM LoanGntyPartLendrTbl        D,
               sbaref.LoanPartLendrTypTbl  r,
               Loan.LoanGntyTbl            G
         WHERE     D.LoanAppNmb = p_LoanAppNmb
               AND D.LoanAppNmb = G.LOANAPPNMB
               AND D.LoanPartLendrTypCd = r.LoanPartLendrTypCd;

    OPEN p_SelCur15 FOR                      --Resultset 12, loan disbursement
        SELECT LoanAppNmb,
               LoanDisbSeqNmb,
               LoanDisbDt,
               CAST (LoanDisbAmt AS NUMBER (15, 2))     LoanDisbAmt
          FROM LoanDisbTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    -- result set 12 : Companion list
    LoanTransCmpnSelCSP (p_LoanAppNmb, 'Y', p_SelCur16);

    -- result set 13:
    /* ARC Reason Text */
    /*06/17/2009 -APK- Added new result set 13*/
    OPEN p_SelCur17 FOR
        SELECT b.ARCLoanRsnCd, b.ARCLoanRsnDescTxt
          FROM loanapp.LoanAppARCRsnTbl a, sbaref.ARCLoanRsnTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ARCLoanRsnCd = b.ARCLoanRsnCd;

    -- get values from loanintdtltbl
    OPEN p_SelCur18 FOR
          SELECT a.LOANAPPNMB,
                 a.LOANINTDTLSEQNMB,
                 a.LOANINTLOANPARTPCT,
                 a.LOANINTLOANPARTMONMB,
                 a.LOANINTFIXVARIND,
                 a.LOANINTRT,
                 a.IMRTTYPCD,
                 D.IMRtTypDescTxt,
                 a.LOANINTBASERT,
                 a.LOANINTSPRDOVRPCT,
                 CASE
                     WHEN a.LOANINTADJPRDCD IS NULL
                     THEN
                         ' '
                     ELSE
                         (SELECT CALNDRPRDDESC
                            FROM sbaref.CalndrPrdTbl e
                           WHERE e.CALNDRPRDCD = a.LOANINTADJPRDCD)
                 END
                     CALNDRPRDDESC,
                 (SELECT b.Loanintdtldesctxt
                    FROM SBAREF.LOANINTDTLCDTBL b
                   WHERE g.Loanappintdtlcd = b.Loanintdtlcd)
                     AS Loanintdtldesctxt,
                 a.LOANINTADJPRDMONMB,
                 a.LOANINTADJPRDEFFDT,
                 a.LoanIntDtlGuarInd
            FROM LOAN.LOANINTDTLTBL a, LOAN.LOANGNTYTBL g, sbaref.IMRtTypTbl D
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.loanappnmb = g.loanappnmb
                 AND a.IMRtTypCd = D.IMRtTypCd
                 AND g.PrcsMthdCd = D.PrcsMthdCd
        ORDER BY a.LOANINTDTLSEQNMB;

    --Associate Information
    OPEN p_SelCur19 FOR
        SELECT a.LOANASSOCSEQNMB,
               a.TAXID,
               a.LOANASSOCTYPCD,
               e.LOANASSOCDESCTXT,
               a.LOANASSOCBUSPERIND,
               a.LOANASSOCCMNTTXT,
               c.BusTypCd,
               c.BusTypTxt,
               b.BusNm,
               f.BusTrdNm,
               NULL     PerFirstNm,
               NULL     PerLastNm,
               NULL     PerInitialNm,
               NULL     PerSfxNm,
               NULL     PerTitlTxt,
               NULL     PerBrthDt,
               NULL     PerBrthCtyNm,
               NULL     PerBrthStCd,
               NULL     PerBrthCntCd,
               NULL     PerBrthCntNm,
               NULL     PerUSCitznInd,
               NULL     CitznshipTxt,
               NULL     PerAlienRgstrtnNmb,
               NULL     PerAffilEmpFedInd,
               NULL     PerIOBInd,
               NULL     PerIndctPrleProbatnInd,
               NULL     PerCrmnlOffnsInd,
               NULL     PerCnvctInd,
               f.BusExprtInd,
               f.BusBnkrptInd,
               f.BusLwsuitInd,
               f.BusPriorSBALoanInd,
               f.BusPrimPhnNmb,
               f.BusPhyAddrStr1Nm,
               f.BusPhyAddrStr2Nm,
               f.BusPhyAddrCtyNm,
               f.BusPhyAddrStCd,
               f.BusPhyAddrStNm,
               f.BusPhyAddrCntCd,
               f.BusPhyAddrZipCd,
               f.BusPhyAddrZip4Cd,
               f.BusPhyAddrPostCd,
               f.BusMailAddrStr1Nm,
               f.BusMailAddrStr2Nm,
               f.BusMailAddrCtyNm,
               f.BusMailAddrStCd,
               f.BusMailAddrStNm,
               f.BusMailAddrCntCd,
               f.BusMailAddrZipCd,
               f.BusMailAddrZip4Cd,
               f.BusMailAddrPostCd,
               f.BusExtrnlCrdtScorInd,
               f.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.BusExtrnlCrdtScorNmb,
               f.BusExtrnlCrdtScorDt,
               f.BusDUNSNmb,
               b.VetCertInd,
               NULL     PerOthrCmntTxt
          FROM LOAN.BusTbl                b,
               Loan.LoanGntyBusTbl        f,
               LOAN.LOANASSOCTBL          a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  D,
               LOAN.LOANASSOCTYPTBL       e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = f.LoanAppNmb
               AND a.TaxId = b.TaxId
               AND a.LOANASSOCBUSPERIND = 'B'
               AND a.LOANASSOCACTVINACTIND = 'A'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LOANASSOCTYPCD = e.LOANASSOCTYPCD(+)
        UNION ALL
        SELECT a.LOANASSOCSEQNMB,
               a.TAXID,
               a.LOANASSOCTYPCD,
               e.LOANASSOCDESCTXT,
               a.LOANASSOCBUSPERIND,
               a.LOANASSOCCMNTTXT,
               NULL                                 BusTypCd,
               NULL                                 BusTypTxt,
               NULL                                 BusNm,
               NULL                                 BusTrdNm,
               OcaDataOut (b.PerFirstNm)            AS PerFirstNm,
               OcaDataOut (b.PerLastNm)             AS PerLastNm,
               b.PerInitialNm,
               b.PerSfxNm,
               f.PerTitlTxt,
               b.PerBrthDt,
               b.PerBrthCtyNm,
               b.PerBrthStCd,
               b.PerBrthCntCd,
               b.PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               f.PerAlienRgstrtnNmb,
               f.PerAffilEmpFedInd,
               f.PerIOBInd,
               f.PerIndctPrleProbatnInd,
               f.PerCrmnlOffnsInd,
               f.PerCnvctInd,
               NULL                                 BusExprtInd,
               f.PerBnkrptcyInd,
               f.PerPndngLwsuitInd,
               NULL                                 BusPriorSBALoanInd,
               OcadataOut (f.PerPrimPhnNmb)         AS PerPrimPhnNmb,
               OcaDataOut (f.PerPhyAddrStr1Nm)      AS PerPhyAddrStr1Nm,
               OcaDataOut (f.PerPhyAddrStr2Nm)      AS PerPhyAddrStr2Nm,
               f.PerPhyAddrCtyNm,
               f.PerPhyAddrStCd,
               f.PerPhyAddrStNm,
               f.PerPhyAddrCntCd,
               f.PerPhyAddrZipCd,
               f.PerPhyAddrZip4Cd,
               f.PerPhyAddrPostCd,
               OcaDataOut (f.PerMailAddrStr1Nm)     AS PerMailAddrStr1Nm,
               OcaDataOut (f.PerMailAddrStr2Nm)     AS PerMailAddrStr2Nm,
               f.PerMailAddrCtyNm,
               f.PerMailAddrStCd,
               f.PerMailAddrStNm,
               f.PerMailAddrCntCd,
               f.PerMailAddrZipCd,
               f.PerMailAddrZip4Cd,
               f.PerMailAddrPostCd,
               f.PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               D.IMCrdtScorSourcDescTxt,
               f.PerExtrnlCrdtScorNmb,
               f.PerExtrnlCrdtScorDt,
               NULL                                 AS BusDUNSNmb,
               b.VetCertInd,
               b.PerOthrCmntTxt
          FROM LOAN.PerTbl                b,
               loan.loangntypertbl        f,
               LOAN.LOANASSOCTBL          a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  D,
               LOAN.LOANASSOCTYPTBL       e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = b.TaxId
               AND f.loanappnmb = a.loanappnmb
               AND a.LOANASSOCBUSPERIND = 'P'
               AND a.LOANASSOCACTVINACTIND = 'A'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = D.IMCrdtScorSourcCd(+)
               AND a.LOANASSOCTYPCD = e.LOANASSOCTYPCD(+)
        ORDER BY LOANASSOCSEQNMB;

    --Stand By Agreement---
    OPEN p_SelCur20 FOR
        SELECT a.LOANSTBYCRDTRNM,
               a.LOANSTBYAMT,
               (SELECT s.STBYPYMTTYPDESCTXT
                  FROM SBAREF.STBYPYMTTYPCDTBL s
                 WHERE a.LOANSTBYREPAYTYPCD = S.STBYPYMTTYPCD)
                   AS LOANSTBYPYMTTYPDESCTXT,
               a.LOANSTBYPYMTINTRT,
               a.LOANSTBYPYMTOTHDESCTXT,
               a.LOANSTBYPYMTAMT,
               a.LOANSTBYREPAYTYPCD,
               a.LOANSTBYPYMTSTRTDT
          FROM LOAN.LOANGNTYStbyAgrmtDtlTBL a
         WHERE a.LOANAPPNMB = p_LOANAPPNMB;


    OPEN p_SelCur21 FOR
        SELECT LOANAPPPYMTAMT,
               CASE
                   WHEN LOANPYMNINSTLMNTFREQCD = 'M' THEN 'Monthly'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'Quarterly'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'S' THEN 'Semi Annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'A' THEN 'Annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'O' THEN 'Other'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTFREQTXT,
               PYMNTDAYNMB,
               PYMNTBEGNMONMB,
               ESCROWACCTRQRDIND,
               NETEARNCLAUSEIND,
               ARMMARTYPIND,
               CASE
                   WHEN ARMMARTYPIND = 'N' THEN 'None'
                   WHEN ARMMARTYPIND = 'F' THEN 'Ceiling and Floor Fluctuate'
                   WHEN ARMMARTYPIND = 'X' THEN 'Ceiling and Floor are Fixed'
                   ELSE NULL
               END
                   AS ARMMARTYPTXT,
               ARMMARCEILRT,
               ARMMARFLOORRT,
               STINTRTREDUCTNIND,
               LATECHGIND,
               LATECHGAFTDAYNMB,
               LATECHGFEEPCT,
               PYMNTINTONLYBEGNMONMB,
               PYMNTINTONLYFREQCD,
               CASE
                   WHEN PYMNTINTONLYFREQCD = 'M' THEN 'Monthly'
                   WHEN PYMNTINTONLYFREQCD = 'Q' THEN 'Quarterly'
                   WHEN PYMNTINTONLYFREQCD = 'S' THEN 'Semi Annual'
                   WHEN PYMNTINTONLYFREQCD = 'A' THEN 'Annual'
                   WHEN PYMNTINTONLYFREQCD = 'O' THEN 'Other'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTINTONLYFREQTXT,
               PYMNTINTONLYDAYNMB,
               NetEarnPymntPct,
               NetEarnPymntOverAmt,
               StIntRtReductnPrgmNm,
               a.LOANAPPMOINTQTY,
               a.LoanPymtRepayInstlTypCd,
               a.LOANPYMNINSTLMNTFREQCD,
               RTRIM (
                      SUBSTR (LoanPymntSchdlFreq, 1, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 3, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 5, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 7, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 9, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 11, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 13, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 15, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 17, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 19, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 21, 2)
                   || ', '
                   || SUBSTR (LoanPymntSchdlFreq, 23, 2),
                   ', ')
                   AS freq,
               a.LOANAPPINTDTLCD
          FROM LOAN.LOANGNTYPYMNTTBL p, LOAN.LoanGNTYTbl a
         WHERE a.LoanAppNmb = p_LoanAppNmb AND a.LoanAppNmb = p.LoanAppNmb(+);

    OPEN p_SelCur22 FOR
        SELECT LoanCollatSeqNmb,
               TaxId,
               BUSPERIND,
               CASE
                   WHEN BUSPERIND = 'B'
                   THEN
                       (SELECT BusNm
                          FROM loan.bustbl z
                         WHERE z.taxid = c.taxid AND ROWNUM <= 1)
                   ELSE
                       (SELECT    OcaDataOut (PerFirstNm)
                               || LTRIM (
                                         ' '
                                      || CASE
                                             WHEN PerInitialNm IS NOT NULL
                                             THEN
                                                 PerInitialNm || '.'
                                             ELSE
                                                 NULL
                                         END)
                               || LTRIM (' ' || OcaDataOut (PerLastNm))
                               || LTRIM (' ' || PerSfxNm)
                          FROM loan.pertbl z
                         WHERE z.taxid = c.taxid)
               END
                   AS BUSPERNm
          FROM loan.LoanLmtGntyCollatTbl c
         WHERE LoanAppNmb = p_LoanAppNmb;

    --Export Country Codes---
    OPEN p_SelCur23 FOR
          SELECT a.LoanAppNmb,
                 a.LoanExprtCntryCd,
                 b.imcntrynm,
                 a.LoanExprtCntrySeqNmb
            FROM LOAN.LOANEXPRTCNTRYTBL a, SBAREF.IMCNTRYCDTBL b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.LoanExprtCntryCd = b.ImCntryCd
        ORDER BY LOANEXPRTCNTRYSEQNMB ASC;

    OPEN p_SelCur24 FOR
          SELECT a.LOANAGNTSEQNMB,
                 a.LOANAGNTID,
                 a.LOANAGNTBUSPERIND,
                 a.LOANAGNTNM,
                    OcaDataOut (a.LOANAGNTCNTCTFIRSTNM)
                 || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                 || ' '
                 || OcaDataOut (a.LOANAGNTCNTCTLastNm)
                 || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                     AS LoanContactName,
                    OcaDataOut (a.LOANAGNTADDRSTR1NM)
                 || RTRIM (' ' || OcaDataOut (a.LOANAGNTADDRSTR2NM))
                 || ' '
                 || a.LOANAGNTADDRCTYNM
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTNM)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIPCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIP4CD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRPOSTCD)
                 || ' '
                 || a.LOANAGNTADDRCNTCD
                     AS LoanContactaddress,
                 a.LOANAGNTTYPCD,
                 a.LOANAGNTDOCUPLDIND,
                 a.LOANCDCTPLFEEIND,
                 a.LOANCDCTPLFEEAMT,
                 b.LOANAGNTSERVTYPCD,
                 b.LOANAGNTSERVOTHTYPTXT,
                 b.LOANAGNTAPPCNTPAIDAMT,
                 b.LOANAGNTSBALENDRPAIDAMT
            FROM LoanAgntTbl a, loanagntfeedtltbl b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.loanappnmb = b.loanappnmb
                 AND a.loanagntseqnmb = b.loanagntseqnmb
        ORDER BY loanagntseqnmb ASC;
END LOANGNTYPRINTSELCSP;
/

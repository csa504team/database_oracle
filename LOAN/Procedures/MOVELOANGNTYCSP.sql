CREATE OR REPLACE PROCEDURE LOAN.MOVELOANGNTYCSP (p_Identifier            IN     NUMBER := 0,
                                                  p_LoanAppNmb            IN     NUMBER := 0,
                                                  p_LoanNmb               IN     CHAR := NULL,
                                                  p_LoanGntyFeeAmt        IN     NUMBER := NULL,
                                                  p_LoanAppServOfcCd      IN     CHAR := NULL,
                                                  p_CohortCd              IN     CHAR := NULL,
                                                  p_RetVal                   OUT NUMBER,
                                                  p_RetStat                  OUT NUMBER,
                                                  p_RetTxt                   OUT VARCHAR2,
                                                  p_LoanOrigntnFeeInd            CHAR := NULL,
                                                  p_LOANONGNGFEECOLLIND          CHAR := NULL) AS
    /*
        Parameters   :  No.   Name        Type     Description
                        1     p_Identifier  int     Identifies which batch
                                                   to execute.
                        2     p_LoanAppNmb  int     LoanApplication Number to be moved
                        3     p_CreatUserId char(15)User Id
                        4     p_RetVal      int     No. of rows returned/
                                                   affected
                        5     p_RetStat     int     The return value for the update
                        6     p_RetTxt      varchar return value text
    *********************************************************************************
    MODIFIED BY: APK - 11/02/2010 - Modified for 503 changes
                Need to modify the LOAN.MOVELOANGNTYCSP to add the following additional
                fields for process method SMP. For non-SMP set to null.All the �v_� come
                from LOANAPP.POOLAPPPRTPNTINFOCSP for given 503 LoanAppNmb
                PoolGrossInitlIntPct = g.LoanAppInitlIntPct, -- Gross Interest at the time
                of funding,PoolLendrGrossInitlAmt, = v_PoolLendrGrossInitlAmt, --Balance at
                the time of funding,PoolLendrPartInitlAmt = v_PoolLendrPartInitlAmt, --
                lender�s participation amount at time of  funding,PoolOrigPartInitlAmt =
                v_PoolOrigPartInitlAmt, -- originator�s participation
                amount at time of  funding,PoolSBAGntyBalInitlAmt =
                v_PoolSBAGntyBalInitlAmt, -- SBA�s participation amount at time of  funding.
    APK - 11/16/2010 - Modified to add the Financial instrument indicator.
    APK - 05/10/2011 - Modified to add the purchase indicator.
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP  -- 04/18/2012 -- Modified to add new column LoanAppNetEarnInd
    SP  -- 04/04/2013 -- modified to add UNDRWRITNGBY
    RGG -- 06/23/2013 -- Added the new column LOANGNTYMATSTIND to decide whether LoanGntyNoteDt or First disturbment date should be used.
    SP  -- 07/29/2013 -- Modified to remove check for prgrmcd = X for setting LOANGNTYMATSTIND
    RGG -- 07/29/2013 -- Added the new column LOANGNTYLCKOUTUPDIND.
    RGG -- 09/05/2013 -- Removed references for Street Number and Street Suffix Columns
    SB -- 04/04/2014 -- Removed references for NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND
    PSP -- 08/04/2014 -- Modified to update fininstrmnttypind to E where prgmcd =  E
    SB -- 09/03/2014 --  Modified to replace logic to set loan.LoanGntyTbl. FinInstrmntTypInd logic with loan.LoanGntyTbl. FinInstrmntTypInd = loanapp.LoanAppTbl. FinInstrmntTypInd
    RY-- 12/20/16 OPSMDEV 1293,1294 -- Commented out the code for LOANGNTYMATSTIND as added a column in loanapptbl.
    RY --12/27/16 OPSMDEV 1293,1294-- Added the  column'LOANGNTYNOTEDT' to movefrom origination to servicing.
    RY--12/28/16 OPSMDEV1314--Added NoteDt
    SS--06/05/2017 OPSMDEV 1456--Added exception to two begin statements
    SS--10/25/2017 OPSMDEV 1541--Added LOANPYMNINSTLMNTFREQCD,LoanPymntSchdlFreq
    RY--01/17/18 OPSMDEV-1644 --Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
    RY--04/23/2018--OPSMDEV-1783--Added LoanPymtRepayInstlTypCd in loangntytbl call
    SS--10/26/2018--OPSMDEV 1965 Added LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd
    RY--11/27/2018--OPSMDEV-2044--Added OCADATAOut for PII columns in PERtbl
    JP--11/14/2019 -- CAFSOPER 3213 -- added LoanAgntInvlvdInd in the insert to LoanGntyTbl table
    SS--04/03/2020 Added LoanAppMonthPayroll to loangntytbl insert CARESACT - 66
    JP -- 06/12/2020 added LOAN1502CERTIND
    JP--1/9/2021  - SODSTORY 376 added column LoanAppUsrEsInd
    SL--03/03/2021 SODSTORY-573 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 
*/
    v_POOLLOANSBAGNTYBALAMT      NUMBER (19, 4);
    v_POOLLOANLENDRGROSSAMT      NUMBER (19, 4);
    v_POOLLOANORIGPARTAMT        NUMBER (19, 4);
    v_POOLLOANLENDRPARTAMT       NUMBER (19, 4);
    v_PrcsMthdCd                 CHAR (4);
    v_PoolSBAGntyFeeInitlPct     NUMBER (6, 3);
    v_LOANLENDRSERVFEEPCT        NUMBER (6, 3);
    v_POOLLOANLENDRGROSSINTPCT   NUMBER (6, 3);
    v_LOANLENDRNETINTPCT         NUMBER (6, 3);
    v_PoolSBAGntyFeePct          NUMBER (6, 3);
    v_LOANLENDRPARTPCT           NUMBER (6, 3);
    v_LOANORIGPARTPCT            NUMBER (6, 3);
    v_LOANSBAGNTYBALPCT          NUMBER (6, 3);
    --v_FININSTRMNTTYPIND         CHAR(1);
    v_LoanMailStr1Nm             VARCHAR2 (80);
    v_LoanMailStr2Nm             VARCHAR2 (80);
    v_LoanMailCtyNm              VARCHAR2 (40);
    v_LoanMailStCd               CHAR (2);
    v_LoanMailZipCd              CHAR (5);
    v_LoanMailZip4Cd             CHAR (4);
    v_LoanMailCntryCd            CHAR (2);
    v_LoanMailStNm               VARCHAR2 (60);
    v_LoanMailPostCd             VARCHAR2 (20);
    v_PrgmCd                     CHAR (1);
    v_TaxId                      CHAR (10);
    v_BorrBusPerInd              CHAR (1);
    v_LOANGNTYEIDLAMT            NUMBER (15, 2);
    v_LOANGNTYPHYAMT             NUMBER (15, 2);
    v_LOANGNTYMATSTIND           CHAR (1);
    v_POOLGROSSINITLINTPCT       NUMBER (6, 3);
    v_LoanGntyFeeDscntRt         NUMBER;
    v_LoanGntyFeeBillAmt         NUMBER;
BEGIN
    /* Insert into LoanGntyTbl Table */
    BEGIN
        SELECT PrgmCd, PrcsMthdCd
        INTO v_PrgmCd, v_PrcsMthdCd
        FROM LOANAPP.LOANAPPTBL
        WHERE LoanAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    BEGIN
        SELECT LOANINTLOANPARTPCT
        INTO v_POOLGROSSINITLINTPCT
        FROM LOANAPP.LOANINTDTLTBL
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND LOANINTDTLSEQNMB = 1;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    BEGIN
        SELECT LoanGntyFeeDscntRt,
               ROUND (LoanGntyFeeAmt * (100 - NVL (LoanGntyFeeDscntRt, 0)) / 100, 2)     AS LoanGntyFeeBillAmt
        INTO v_LoanGntyFeeDscntRt, v_LoanGntyFeeBillAmt
        FROM loan.LoanGntyCmpnLoanCalcTbl
        WHERE     LoanMainAppNmb = p_LoanAppNmb
              AND LoanAppNmb = p_LoanAppNmb
              AND LoanCmpnAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END;

    /* IF v_PrgmCd = 'E'
     THEN
      v_FININSTRMNTTYPIND := 'E';
     ELSE
      v_FININSTRMNTTYPIND := 'L';
     END IF;  */

    IF p_Identifier = 0 THEN
        IF v_PrcsMthdCd = 'SMP' THEN
            LOANAPP.POOLAPPPRTPNTINFOCSP (
                p_LoanAppNmb,
                v_POOLLOANSBAGNTYBALAMT,
                v_POOLLOANLENDRGROSSAMT,
                v_POOLLOANORIGPARTAMT,
                v_POOLLOANLENDRPARTAMT,
                v_PoolSBAGntyFeeInitlPct,
                v_LOANLENDRSERVFEEPCT,
                v_POOLLOANLENDRGROSSINTPCT,
                v_LOANLENDRNETINTPCT,
                v_PoolSBAGntyFeePct,
                v_LOANLENDRPARTPCT,
                v_LOANORIGPARTPCT,
                v_LOANSBAGNTYBALPCT);
        END IF;

        IF v_PrgmCd = 'H' THEN
            SELECT TaxId, BORRBUSPERIND
            INTO v_TaxId, v_BORRBUSPERIND
            FROM loanapp.LoanBorrTbl
            WHERE     loanappnmb = p_LoanAppNmb
                  AND LOANBUSPRIMBORRIND = 'Y';

            IF v_BorrBusPerInd = 'B' THEN
                SELECT BUSPHYADDRSTR1NM,
                       BUSPHYADDRSTR2NM,
                       BUSPHYADDRCTYNM,
                       BUSPHYADDRSTCD,
                       BUSPHYADDRZIPCD,
                       BUSPHYADDRZIP4CD,
                       BUSPHYADDRCNTCD,
                       BUSPHYADDRSTNM,
                       BUSPHYADDRPOSTCD
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd,
                     v_LoanMailStNm,
                     v_LoanMailPostCd
                FROM loanapp.LoanBusTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND BUSMAILADDRSTR1NM IS NULL
                UNION ALL
                SELECT BUSMAILADDRSTR1NM,
                       BUSMAILADDRSTR2NM,
                       BUSMAILADDRCTYNM,
                       BUSMAILADDRSTCD,
                       BUSMAILADDRZIPCD,
                       BUSMAILADDRZIP4CD,
                       BUSMAILADDRCNTCD,
                       BUSMAILADDRSTNM,
                       BUSMAILADDRPOSTCD
                FROM loanapp.LoanBusTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND BUSMAILADDRSTR1NM IS NOT NULL;
            ELSE
                SELECT OcaDataOut (PERPHYADDRSTR1NM),
                       OcaDataOut (PERPHYADDRSTR2NM),
                       PERPHYADDRCTYNM,
                       PERPHYADDRSTCD,
                       PERPHYADDRZIPCD,
                       PERPHYADDRZIP4CD,
                       PERPHYADDRCNTCD,
                       PERPHYADDRSTNM,
                       PERPHYADDRPOSTCD
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd,
                     v_LoanMailStNm,
                     v_LoanMailPostCd
                FROM loanapp.LoanPERTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND PERMAILADDRSTR1NM IS NULL
                UNION ALL
                SELECT OcaDataOut (PERMAILADDRSTR1NM)     AS PERMAILADDRSTR1NM,
                       OcaDataOut (PERMAILADDRSTR2NM)     AS PERMAILADDRSTR2NM,
                       PERMAILADDRCTYNM,
                       PERMAILADDRSTCD,
                       PERMAILADDRZIPCD,
                       PERMAILADDRZIP4CD,
                       PERMAILADDRCNTCD,
                       PERMAILADDRSTNM,
                       PERMAILADDRPOSTCD
                FROM loanapp.LoanPERTbl
                WHERE     LoanAppNmb = p_LoanAppNmb
                      AND TaxId = v_TaxId
                      AND PERMAILADDRSTR1NM IS NOT NULL;
            END IF;
        ELSE
            BEGIN
                SELECT LoanAppProjStr1Nm,
                       LOANAPPPROJSTR2NM,
                       LOANAPPPROJCTYNM,
                       LOANAPPPROJSTCD,
                       LOANAPPPROJZIPCD,
                       LOANAPPPROJZIP4CD,
                       'US'
                INTO v_LoanMailStr1Nm,
                     v_LoanMailStr2Nm,
                     v_LoanMailCtyNm,
                     v_LoanMailStCd,
                     v_LoanMailZipCd,
                     v_LoanMailZip4Cd,
                     v_LoanMailCntryCd
                FROM loanapp.LoanAppProjTbl
                WHERE LoanAppNmb = p_LoanAppNmb;
            EXCEPTION
                WHEN OTHERS THEN
                    NULL;
            END;
        END IF;

        IF v_PrgmCd = 'H' THEN
            SELECT SUM (CASE WHEN LOANPROCDTYPCD = '04' THEN LOANPROCDAMT ELSE 0 END),
                   SUM (CASE WHEN LOANPROCDTYPCD != '04' THEN LOANPROCDAMT ELSE 0 END)
            INTO v_LOANGNTYEIDLAMT, v_LOANGNTYPHYAMT
            FROM loanapp.LoanProcdTbl
            WHERE loanappnmb = p_Loanappnmb;
        END IF;

        /* --RGG 06/23/2013 Loan maturity start date indicator changes
         IF --(v_PrcsMthdCd = 'ILP' OR v_PrcsMthdCd = 'MLD') THEN
             v_PrgmCd <> 'C' THEN
             v_LOANGNTYMATSTIND := 'N';
         ELSE
             v_LOANGNTYMATSTIND := 'F';
         END IF;
         --RGG 06/23/2013 Loan maturity start date indicator changes*/

        INSERT INTO LoanGntyTbl (LoanAppNmb,
                                 PrgmCd,
                                 PrcsMthdCd,
                                 CohortCd,
                                 LoanAppNm,
                                 LoanAppRecvDt,
                                 LoanAppEntryDt,
                                 LoanAppRqstAmt,
                                 LoanAppSBAGntyPct,
                                 LoanInitlAppvAmt,
                                 LoanCurrAppvAmt,
                                 LoanInitlAppvDt,
                                 LoanLastAppvDt,
                                 LoanAppRqstMatMoQty,
                                 LoanAppEPCInd,
                                 LoanAppPymtAmt,
                                 LoanAppFullAmortPymtInd,
                                 LoanAppMoIntQty,
                                 LoanAppLifInsurRqmtInd,
                                 LoanAppRcnsdrtnInd,
                                 LoanAppInjctnInd,
                                 LoanCollatInd,
                                 LoanAppEWCPSnglTransPostInd,
                                 LoanAppEWCPSnglTransInd,
                                 LoanAppEligEvalInd,
                                 LoanAppNewBusCd,
                                 LoanAppDisasterCntrlNmb,
                                 LoanStatCd,
                                 LoanStatDt,
                                 LoanAppOrigntnOfcCd,
                                 LoanAppPrcsOfcCd,
                                 LoanAppServOfcCd,
                                 LoanAppFundDt,
                                 LoanAppMatDt,
                                 LoanNmb,
                                 LoanGntyFeeAmt,
                                 LoanGntyFeeBillAmt,
                                 LocId,
                                 LoanAppLSPHQLocId,
                                 PrtId,
                                 LoanSrvsLocId,
                                 LoanAppPrtAppNmb,
                                 LoanAppPrtLoanNmb,
                                 LoanOutBalAmt,
                                 LoanOutBalDt,
                                 LoanTypInd,
                                 RecovInd,
                                 LoanAppOutPrgrmAreaOfOperInd,
                                 LoanAppParntLoanNmb,
                                 LoanAppMicroLendrId,
                                 LoanTotUndisbAmt,
                                 ACHRtngNmb,
                                 ACHAcctNmb,
                                 ACHAcctTypCd,
                                 ACHTinNmb,
                                 LoanAppCDCGntyAmt,
                                 LoanAppCDCNetDbentrAmt,
                                 LoanAppCDCFundFeeAmt,
                                 LoanAppCDCSeprtPrcsFeeInd,
                                 LoanAppCDCPrcsFeeAmt,
                                 LoanAppCDCClsCostAmt,
                                 LoanAppCDCOthClsCostAmt,
                                 LoanAppCDCUndrwtrFeeAmt,
                                 LoanAppContribPct,
                                 LoanAppContribAmt,
                                 LoanAppPrtTaxId,
                                 LOANDISASTRAPPFEECHARGED,
                                 LOANDISASTRAPPDCSN,
                                 LOANASSOCDISASTRAPPNMB,
                                 LOANASSOCDISASTRLOANNMB,
                                 LoanAppCreatUserId,
                                 LoanAppCreatDt,
                                 POOLGROSSINITLINTPCT,
                                 POOLSBAGNTYBALINITLAMT,
                                 POOLLENDRGROSSINITLAMT,
                                 POOLORIGPARTINITLAMT,
                                 POOLLENDRPARTINITLAMT,
                                 FININSTRMNTTYPIND,
                                 LOANPURCHSDIND,
                                 LOANGNTYDFRDMNTHSNMB,
                                 LOANGNTYDFRTODT,
                                 LOANDISASTRAPPNMB,
                                 LoanMailStr1Nm,
                                 LoanMailStr2Nm,
                                 LoanMailCtyNm,
                                 LoanMailStCd,
                                 LoanMailZipCd,
                                 LoanMailZip4Cd,
                                 LoanMailCntryCd,
                                 LoanMailStNm,
                                 LoanMailPostCd,
                                 LoanAppNetEarnInd,
                                 LOANGNTYEIDLAMT,
                                 LOANGNTYEIDLCNCLAMT,
                                 LOANGNTYPHYAMT,
                                 LOANGNTYPHYCNCLAMT,
                                 UNDRWRITNGBY,
                                 LOANGNTYMATSTIND,
                                 LOANGNTYLCKOUTUPDIND,
                                 LOANAPPINTDTLCD,
                                 LoanOrigntnFeeInd,
                                 LOANONGNGFEECOLLIND,
                                 LoanGntyFeeDscntRt,
                                 loangntynotedt,
                                 LOANAPPCDCGROSSDBENTRAMT,
                                 LOANAPPBALTOBORRAMT,
                                 LOANPYMNINSTLMNTFREQCD,
                                 LoanPymntSchdlFreq,
                                 LoanAppRevlMoIntQty,
                                 LoanAppAmortMoIntQty,
                                 LoanPymtRepayInstlTypCd,
                                 LoanAppExtraServFeeAMT,
                                 LoanAppExtraServFeeInd,
                                 LoanAgntInvlvdInd,
                                 LoanAppMonthPayroll,
                                 LOAN1502CERTIND,
                                 LoanAppUsrEsInd,
                                 LOANAPPSCHEDCIND, 
                                 LOANAPPSCHEDCYR, 
                                 LOANAPPGROSSINCOMEAMT )
            SELECT p_LoanAppNmb,
                   PrgmCd,
                   PrcsMthdCd,
                   p_CohortCd,
                   LoanAppNm,
                   LoanAppRecvDt,
                   LoanAppEntryDt,
                   LoanAppRqstAmt,
                   LoanAppSBAGntyPct,
                   LoanAppSBARcmndAmt,                                                     /* Initial Approval Amount */
                   LoanAppSBARcmndAmt,                                                     /* Current Approval Amount */
                   LoanAppAppvDt,                                                            /* Initial Approval Date */
                   LoanAppAppvDt,                                                            /* Current Approval Date */
                   LoanAppRqstMatMoQty,
                   LoanAppEPCInd,
                   LoanAppPymtAmt,
                   LoanAppFullAmortPymtInd,
                   LoanAppMoIntQty,
                   LoanAppLifInsurRqmtInd,
                   LoanAppRcnsdrtnInd,
                   LoanAppInjctnInd,
                   LoanCollatInd,
                   LoanAppEWCPSnglTransPostInd,
                   LoanAppEWCPSnglTransInd,
                   'Y',
                   LoanAppNewBusCd,
                   LoanAppDisasterCntrlNmb,
                   1
                       AS LoanStatCd,
                   SYSDATE
                       AS LoanStatDt,
                   LoanAppOrigntnOfcCd,
                   LoanAppPrcsOfcCd,
                   p_LoanAppServOfcCd,                                                            /* Servicing Office */
                   SYSDATE
                       AS LoanFundDt,
                   ADD_MONTHS (SYSDATE, TRUNC (LoanAppRqstMatMoQty))
                       AS LoanAppMatDt,
                   p_LoanNmb,
                   p_LoanGntyFeeAmt,
                   v_LoanGntyFeeBillAmt,
                   LocId,
                   LoanAppLSPHQLocId,
                   PrtId,
                   LocId,
                   LoanAppPrtAppNmb,
                   LoanAppPrtLoanNmb,
                   0,
                   NULL,
                   (SELECT LoanTypInd
                    FROM sbaref.PrcsMthdTbl z
                    WHERE z.PrcsMthdCd = a.PrcsMthdCd),
                   RecovInd,
                   NVL (LoanAppOutPrgrmAreaOfOperInd, 'N'),
                   LoanAppParntLoanNmb,
                   LoanAppMicroLendrId,
                   LoanAppSBARcmndAmt,
                   ACHRtngNmb,
                   ACHAcctNmb,
                   ACHAcctTypCd,
                   ACHTinNmb,
                   LoanAppCDCGntyAmt,
                   LoanAppCDCNetDbentrAmt,
                   LoanAppCDCFundFeeAmt,
                   NVL (LoanAppCDCSeprtPrcsFeeInd, 'Y'),
                   LoanAppCDCPrcsFeeAmt,
                   LoanAppCDCClsCostAmt,
                   LoanAppCDCOthClsCostAmt,
                   LoanAppCDCUndrwtrFeeAmt,
                   LoanAppContribPct,
                   LoanAppContribAmt,
                   b.LoanAppPrtTaxId,
                   LOANDISASTRAPPFEECHARGED,
                   LOANDISASTRAPPDCSN,
                   LOANASSOCDISASTRAPPNMB,
                   LOANASSOCDISASTRLOANNMB,
                   LoanAppCreatUserId,
                   LoanAppCreatDt,
                   CASE WHEN v_PrcsMthdCd = 'SMP' THEN v_POOLGROSSINITLINTPCT ELSE NULL END,
                   v_POOLLOANSBAGNTYBALAMT,
                   v_POOLLOANLENDRGROSSAMT,
                   v_POOLLOANORIGPARTAMT,
                   v_POOLLOANLENDRPARTAMT,
                   FININSTRMNTTYPIND,
                   (CASE
                        WHEN (SELECT LoanTypInd
                              FROM sbaref.PrcsMthdTbl z
                              WHERE z.PrcsMthdCd = a.PrcsMthdCd) != 'D' THEN
                            'LL'
                        ELSE
                            'SS'
                    END),
                   LOANAPPPYMTDEFMONMB,
                   ADD_MONTHS (SYSDATE, LOANAPPPYMTDEFMONMB),
                   LOANDISASTRAPPNMB,
                   v_LoanMailStr1Nm,
                   v_LoanMailStr2Nm,
                   v_LoanMailCtyNm,
                   v_LoanMailStCd,
                   v_LoanMailZipCd,
                   v_LoanMailZip4Cd,
                   v_LoanMailCntryCd,
                   v_LoanMailStNm,
                   v_LoanMailPostCd,
                   LoanAppNetEarnInd,
                   NVL (v_LOANGNTYEIDLAMT, 0),
                   0,
                   NVL (v_LOANGNTYPHYAMT, 0),
                   0,
                   CASE WHEN PrcsMthdCd IN ('PPP', 'PPS') THEN 'LNDR' ELSE UNDRWRITNGBY END
                       UNDRWRITNGBY,
                   LOANGNTYMATSTIND,
                   'M',
                   LOANAPPINTDTLCD,
                   p_LoanOrigntnFeeInd,
                   p_LOANONGNGFEECOLLIND,
                   v_LoanGntyFeeDscntRt,
                   loangntynotedt,
                   LOANAPPCDCGROSSDBENTRAMT,
                   LOANAPPBALTOBORRAMT,
                   LOANPYMNINSTLMNTFREQCD,
                   LoanPymntSchdlFreq,
                   LoanAppRevlMoIntQty,
                   LoanAppAmortMoIntQty,
                   LoanPymtRepayInstlTypCd,
                   LoanAppExtraServFeeAMT,
                   LoanAppExtraServFeeInd,
                   LoanAgntInvlvdInd,
                   LoanAppMonthPayroll,
                   -- JP 06/12/2020 added LOAN1502CERTIND
                   (SELECT 'Y'     LOAN1502CERTIND
                    FROM DUAL
                    WHERE EXISTS
                              (SELECT 1
                               FROM PARTNER.PRTLOCACHACCTTBL p
                               WHERE     b.locid = p.locid
                                     AND p.VALIDPRTDISBTYP = '1102'
                                     AND p.LNDRCERTIND = 'Y'))
                       AS LOAN1502CERTIND,
                   LoanAppUsrEsInd,
                   LOANAPPSCHEDCIND, 
                   LOANAPPSCHEDCYR, 
                   LOANAPPGROSSINCOMEAMT
            FROM loanapp.LoanAppTbl a, loanapp.LoanAppPrtTbl b
            WHERE     a.LoanAppNmb = p_LoanAppNmb
                  AND a.LoanAppNmb = b.LoanAppNmb(+);

        p_RetStat := SQLCODE;
        p_RetVal := SQL%ROWCOUNT;

        IF v_PrcsMthdCd = 'SMP' THEN
            LOAN.LOANCHNGLOGINSCSP (p_LoanAppNmb, 'P', USER, NULL, NULL);
        END IF;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
    p_RetStat := NVL (p_RetStat, 0);
END MOVELOANGNTYCSP;
/
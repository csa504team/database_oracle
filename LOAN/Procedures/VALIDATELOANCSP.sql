CREATE OR REPLACE PROCEDURE LOAN.VALIDATELOANCSP (p_Identifier         IN     NUMBER := 0,
                                                  p_LoanAppNmb         IN     NUMBER := NULL,
                                                  p_TransInd           IN     NUMBER := NULL,
                                                  p_VALIDATIONTYP      IN     CHAR := NULL,
                                                  p_LOANCOLLATSEQNMB   IN     NUMBER := NULL,
                                                  p_RetVal                OUT NUMBER,
                                                  p_SelCur1               OUT SYS_REFCURSOR,
                                                  p_SelCur2               OUT SYS_REFCURSOR) AS
    /*
     created by:
     purpose:
     parameters:
     Revision:
    APK -- 11/17/2011 -- modified to add new parameter VALIDATIONTYP and to modify ID = 0 to inlude the
    new mapping table loanapp.LoanValidationErrMapTbl m to be joined, as part of servicing, post servicing validataion seperation.
    Also,modified calls to the validation procedure for above mentioned additional parameter
    APK -- 12/01/2011-- modified the ID = 10 inorder to get result based on 'S', 'P' or 'B'.
    APK -- 12/08/2011-- modified to comment out procedure calls for collaterals, as not sure if these should be called from here, as per Sheri.
    APK --12/12/2011-- Modified to add new parameter p_LOANCOLLATSEQNMB, also to collat and collat lien validations calls with additional p_SelCur parameter.
    APK -- 12/15/2011-- modified id = 11 inorder to use "in ('P','B')"
    SP  -- 6/7/2012  -- modified id=0 as P,B,S are not used
    SP  -- 11/18/2013 -- Modified to remove references to ValidateBusPrinCSP
    RY -- 08/11/2017-- Modified Validateprincsp call to include  NULL for P_currborrseqnmb.
    RY--09/06/2018-- Modified validateborrcsp call to include NULL for p_AchChngInd.
    JP -- 2/16/2021  SODSTORY-503 modified call to ValidatePrinCSP
    */
    v_sys_error       NUMBER := 0;
    v_ErrSeqNmb       NUMBER (10, 0);
    v_LoanAppRecvDt   DATE;
--  v_tranflag      NUMBER (10, 0);
BEGIN
    SAVEPOINT ValidateLoanCSP;

    OPEN p_SelCur1 FOR SELECT *
                       FROM DUAL
                       WHERE 1 = 2;

    BEGIN
        SELECT LoanAppRecvDt
        INTO v_LoanAppRecvDt
        FROM LoanGntyTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_LoanAppRecvDt := NULL;
    END;

    v_ErrSeqNmb := 0;

    BEGIN
        DELETE LoanGntyValidErrTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
    EXCEPTION
        WHEN OTHERS THEN
            v_sys_error := SQLCODE;
    END;


    ValidatePrinRaceCSP (0, p_LoanAppNmb, p_TransInd, p_VALIDATIONTYP, v_LoanAppRecvDt, p_RetVal, v_ErrSeqNmb);

    ValidatePrtCmntCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        p_VALIDATIONTYP,
        v_LoanAppRecvDt,
        v_ErrSeqNmb,
        p_RetVal,
        p_SelCur1,
        p_SelCur2);

    ValidateBusCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        NULL,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    --ValidateBusPrinCSP(0,p_LoanAppNmb,p_TransInd,v_LoanAppRecvDt,p_VALIDATIONTYP,p_RetVal,v_ErrSeqNmb,p_SelCur1,p_SelCur2);

    ValidateCollatCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_LOANCOLLATSEQNMB,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateCollatLienCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_VALIDATIONTYP,
        p_LOANCOLLATSEQNMB,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateBorrCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        NULL,
        NULL,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateGuarCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        NULL,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateInjctnCSP (0, p_LoanAppNmb, p_TransInd, v_LoanAppRecvDt, p_RetVal, v_ErrSeqNmb, p_SelCur1, p_SelCur2);

    ValidatePerCSP (
        p_Identifier => 0,
        p_LoanAppNmb => p_LoanAppNmb,
        p_TransInd => p_TransInd,
        p_LoanAppRecvDt => v_LoanAppRecvDt,
        p_PerTaxId => NULL,
        p_VALIDATIONTYP => p_VALIDATIONTYP,
        p_RetVal => p_RetVal,
        p_ErrSeqNmb => v_ErrSeqNmb,
        p_SelCur1 => p_SelCur1,
        p_SelCur2 => p_SelCur2);

    /*ValidatePrinCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        NULL,
        v_LoanAppRecvDt,
        NULL,
        p_VALIDATIONTYP,
        NULL, -- added JP 2/16/2021
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);*/
        
    -- JP rewrote the above 2/16/2021
    ValidatePrinCSP (
        p_Identifier => 0,
        p_LoanAppNmb => p_LoanAppNmb,
        p_TransInd => p_TransInd,
        p_LoanAppRecvDt => v_LoanAppRecvDt,
        p_VALIDATIONTYP => p_VALIDATIONTYP,
        -- p_PrinBusPerInd    IN     CHAR := NULL,
        p_RetVal => p_RetVal,
        p_ErrSeqNmb => v_ErrSeqNmb,
        p_SelCur1 => p_SelCur1,
        p_SelCur2 => p_SelCur2);

    ValidateProcdCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);
        
        ValidateSubProcdCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidatePartLendrCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateGntyFinCSP (0, p_LoanAppNmb, p_TransInd, p_VALIDATIONTYP, NULL, v_ErrSeqNmb, p_SelCur1, p_SelCur2);

    ValidateGntyNonFinCSP (0, p_LoanAppNmb, p_TransInd, p_VALIDATIONTYP, v_ErrSeqNmb, p_SelCur1, p_SelCur2);

    ValidateGntyNonFinOthCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        p_VALIDATIONTYP,
        p_Retval,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    ValidateLoanGntyPymntCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        p_VALIDATIONTYP,
        p_Retval,
        v_Errseqnmb,
        p_SelCur1,
        p_SelCur2);

    ValidateScurePartyCSP (0, p_LoanAppNmb, p_TransInd, p_VALIDATIONTYP, p_Retval, v_Errseqnmb, p_SelCur1, p_SelCur2);

    ValidateLOANEXPRTCNTRYCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        p_VALIDATIONTYP,
        p_Retval,
        v_Errseqnmb,
        p_SelCur1,
        p_SelCur2);

    VALIDATESBICDRAWDWNCSP (0, p_LoanAppNmb, p_TransInd, p_VALIDATIONTYP, p_Retval, v_Errseqnmb, p_SelCur1, p_SelCur2);


    VALIDATEBORRGUARCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        NULL,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);
    ValidateAgntCsp (
        0,
        p_RetVal,
        p_LoanAppNmb,
        v_errseqnmb,
        p_TransInd,
        NULL,
        p_VALIDATIONTYP,
        p_SelCur1,
        p_SelCur2);

    ValidateSpcPurpsCSP (
        0,
        p_LoanAppNmb,
        p_TransInd,
        v_LoanAppRecvDt,
        p_VALIDATIONTYP,
        p_RetVal,
        v_ErrSeqNmb,
        p_SelCur1,
        p_SelCur2);

    IF p_Identifier = 0 THEN
        BEGIN
            OPEN p_SelCur1 FOR SELECT LoanGntyValidErrSeqNmb     ErrSeqNmb,
                                      ErrTypCd,
                                      ErrCd,
                                      LoanGntyValidErrTxt        ErrTxt
                               FROM LoanGntyValidErrTbl
                               WHERE LoanAppNmb = p_LoanAppNmb
                               ORDER BY LoanGntyValidErrSeqNmb;
        END;
    END IF;
END ValidateLoanCSP;
/

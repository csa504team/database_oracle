CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARINSCSP (
   p_LoanAppNmb                      NUMBER := 0,
   p_TaxId                           CHAR := NULL,
   p_GuarBusPerInd                   CHAR := NULL,
   p_LoanGuarOperCoInd               VARCHAR2 := NULL,
   p_LoanGuarLastUpdtUserId          CHAR := NULL,
   p_OUTLAWCD                        CHAR := NULL,
   p_OUTLAWOTHRDSCTXT                VARCHAR2 := NULL,
   p_TaxIdChngInd             IN     CHAR := NULL,
   p_NewTaxId                 IN     CHAR := NULL,
   p_LACGNTYCONDTYPCD                NUMBER := NULL,
   p_GuarSeqNmb                  OUT NUMBER,
   p_GntySubCondTypCd                NUMBER := NULL,
   p_GntyLmtAmt                      NUMBER := NULL,
   p_GntyLmtPct                      NUMBER := NULL,
   p_LoanCollatSeqNmb                NUMBER := NULL,
   p_GntyLmtYrNmb                    NUMBER := NULL,
   p_LIABINSURRQRDIND               CHAR:=NULL,
   p_PRODLIABINSURRQRDIND           CHAR:=NULL,
   p_LIQLIABINSURRQRDIND            CHAR:=NULL,
   p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
   p_OTHINSURRQRDIND                CHAR:=NULL,
   p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
   p_OTHINSURDESCTXT                VARCHAR2:=NULL)
AS
   /*SP:06/05/2013: Modified to insert/update LoanAffilInd in LoanGntyGuarTbl for caluculating gnty fee
     SP:11/12/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
     NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd
    NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .

    */
   v_count1   NUMBER;
BEGIN
   SAVEPOINT LoanGntyGuarInsCSP;

   BEGIN
      SELECT COUNT (*)
        INTO v_count1
        FROM LoanGntyGuarTbl
       WHERE     LoanAppNmb = p_LoanAppNmb
             AND TaxId = p_TaxId
             AND GuarBusPerInd = p_GuarBusPerInd;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF (V_COUNT1 = 0)
   THEN
      BEGIN
         INSERT INTO LoanGntyGuarTbl (LoanAppNmb,
                                      GuarSeqNmb,
                                      TaxId,
                                      GuarBusPerInd,
                                      LoanGuarOperCoInd,
                                      LoanGuarActvInactInd,
                                      LoanGuarCreatDt,
                                      LoanGuarLastUpdtUserId,
                                      LoanGuarLastUpdtDt,
                                      OUTLAWCD,
                                      OUTLAWOTHRDSCTXT,
                                      LACGNTYCONDTYPCD,
                                      GntySubCondTypCd,
                                      GntyLmtAmt,
                                      GntyLmtPct,
                                      LoanCollatSeqNmb,
                                      GntyLmtYrNmb,
                                      LIABINSURRQRDIND,
                                      PRODLIABINSURRQRDIND,
                                      LIQLIABINSURRQRDIND,
                                      MALPRCTSINSURRQRDIND,
                                      OTHINSURRQRDIND,
                                      WORKRSCOMPINSRQRDIND,
                                      OTHINSURDESCTXT)
            SELECT p_LoanAppNmb,
                   (  NVL ( (SELECT MAX (z.GuarSeqNmb)
                               FROM LoanGntyGuarTbl z
                              WHERE z.LoanAppNmb = p_LoanAppNmb),
                           0)
                    + 1)
                      AS GuarSeqNmb,
                   p_TaxId,
                   p_GuarBusPerInd,
                   p_LoanGuarOperCoInd,
                   'A',
                   SYSDATE,
                   p_LoanGuarLastUpdtUserId,
                   SYSDATE,
                   p_OUTLAWCD,
                   p_OUTLAWOTHRDSCTXT,
                   p_LACGNTYCONDTYPCD,
                   p_GntySubCondTypCd,
                   p_GntyLmtAmt,
                   p_GntyLmtPct,
                   p_LoanCollatSeqNmb,
                   p_GntyLmtYrNmb,
                   p_LIABINSURRQRDIND,
                   p_PRODLIABINSURRQRDIND,
                   p_LIQLIABINSURRQRDIND,
                   p_MALPRCTSINSURRQRDIND,
                   p_OTHINSURRQRDIND,
                   p_WORKRSCOMPINSRQRDIND,
                   p_OTHINSURDESCTXT
              FROM DUAL;

         --LoanGntyGuarTbl
         --where LoanAppNmb = p_LoanAppNmb;


         BEGIN
            SELECT MAX (GuarSeqNmb)
              INTO p_GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;
      END;
   ELSE
      BEGIN
         BEGIN
            SELECT GuarSeqNmb
              INTO p_GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND TaxId = p_TaxId
                   AND GuarBusPerInd = p_GuarBusPerInd;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         UPDATE LoanGntyGuarTbl
            SET TaxId =
                   (CASE
                       WHEN p_TaxIdChngInd = 'Y' THEN p_NewTaxId
                       ELSE p_TaxId
                    END),
                LoanGuarActvInactInd = 'A',
                LoanGuarLastUpdtUserId = p_LoanGuarLastUpdtUserId,
                LoanGuarLastUpdtDt = SYSDATE,
                OUTLAWCD = p_OUTLAWCD,
                OUTLAWOTHRDSCTXT = p_OUTLAWOTHRDSCTXT,
                LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                GntySubCondTypCd = p_GntySubCondTypCd,
                GntyLmtAmt = p_GntyLmtAmt,
                GntyLmtPct = p_GntyLmtPct,
                LoanCollatSeqNmb = p_LoanCollatSeqNmb,
                GntyLmtYrNmb = p_GntyLmtYrNmb,
                LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                OTHINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                OTHINSURDESCTXT = p_OTHINSURDESCTXT
          WHERE LoanAppNmb = p_LoanAppNmb AND GuarSeqNmb = p_GuarSeqNmb;
      END;
   END IF;

 

   p_GuarSeqNmb := NVL (p_GuarSeqNmb, 0);
END LOANGNTYGUARINSCSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARINSCSP TO UPDLOANROLE;

CREATE OR REPLACE procedure LOAN.LOANAGNTDOCSDELTSP
(
	p_IDENTIFIER	NUMBER :=0,
	p_RETVAL OUT NUMBER,
	p_LOANAPPNMB	NUMBER := 0,
	p_LOANAGNTSEQNMB	NUMBER := 0,
	p_LOANAGNTID	NUMBER :=0
		
)
AS
BEGIN
	SAVEPOINT LOANAGNTDOCSDELTSP;
	if p_IDENTIFIER = 0
	then
		begin
			DELETE FROM  LOAN.LOANAGNTDOCTBL
			WHERE LOANAPPNMB = p_LOANAPPNMB AND
			LOANAGNTSEQNMB = p_LOANAGNTSEQNMB AND
			LOANAGNTID = p_LOANAGNTID;
			p_RETVAL := SQL%ROWCOUNT;
		end;
	end if;
EXCEPTION
	WHEN OTHERS
	THEN
		BEGIN
			RAISE;
			ROLLBACK TO LOANAGNTDOCSDELTSP;
		END;
END LOANAGNTDOCSDELTSP;
/



GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTDOCSDELTSP TO UPDLOANROLE;

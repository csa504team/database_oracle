CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARSELTSP (p_Identifier          IN     NUMBER := 0,
                                                     p_LoanAppNmb          IN     NUMBER := 0,
                                                     p_GuarSeqNmb          IN     NUMBER := 0,
                                                     p_TaxId               IN     CHAR := NULL,
                                                     p_GuarBusPerInd       IN     CHAR := NULL,
                                                     p_RetVal                 OUT NUMBER,
                                                     p_SelCur                 OUT SYS_REFCURSOR)
AS
/*SP:9/26/2012 : Modified as column LoanAffilInd is added to the table for determining companion relationship
 *SP:11/12/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
  RSURAPS : 11/21/2013 : add LACGNTYCONDTYPCD to identifier 14 to support LADS screens
  SB -- Added Identifier 38 for Extract XML
  NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd  in ID 0 and 38
  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in ID 0 and 38.
  RY--11/27/2018--OPSMDEV-2044:: Added Ocadataout for PII columns of PERTBL
  SL--07/31/2020--CARESACT-595: Modifed to use LoanGntyBusTbl data instead of BusTbl to get loan level bus data
  --SS--07/31/2020-- CARESACT 595 Replaced pertbl with loan level loangntypertbl
*/
BEGIN
   /* Select Row On the basis of Key*/
   IF p_Identifier = 0
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LoanGuarCreatDt,
                   OutLawCd,
                   OutLawOthrDscTxt,
                   LACGNTYCONDTYPCD,
                   GntySubCondTypCd,
                   GntyLmtAmt,
                   GntyLmtPct,
                   LoanCollatSeqNmb,
                   GntyLmtYrNmb,
                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 2
   THEN
      /*Check The Existance Of Row*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT 1
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 11
   THEN
      /* return all guarantors of the loan*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LoanGuarCreatDt
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND LoanGuarActvInactInd = 'A';

         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 12
   THEN
      /*Check The Existance Of Row based on LoanAppNmb and TaxId*/
      BEGIN
         OPEN p_SelCur FOR
            SELECT GuarSeqNmb
              FROM LoanGntyGuarTbl
             WHERE     LoanAppNmb = p_LoanAppNmb
                   AND TaxId = p_TaxId
                   AND GuarBusPerInd = p_GuarBusPerInd
                   AND LoanGuarActvInactInd = 'A';


         p_RetVal := SQL%ROWCOUNT;
      END;
   ELSIF p_Identifier = 14
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT GuarSeqNmb,
                   GuarBusPerInd,
                   a.TaxId,
                   BusNm,
                   NULL PerFirstNm,
                   NULL PerLastNm,
                   NULL PerInitialNm,
                   NULL PerSfxNm,
                   LACGNTYCONDTYPCD
              FROM Loan.BusTbl b, LoanGntyGuarTbl a
             WHERE     a.LoanAppNmb = p_LoanAppNmb
                   AND a.TaxId = b.TaxId
                   AND GuarBusPerInd = 'B'
                   AND LoanGuarActvInactInd = 'A'
            UNION
            SELECT GuarSeqNmb,
                   GuarBusPerInd,
                   a.TaxId,
                   NULL BusNm,
                   OcaDataOut (PerFirstNm),
                   OcaDataOut (PerLastNm),
                   PerInitialNm,
                   PerSfxNm,
                   LACGNTYCONDTYPCD
              FROM PerTbl b, LoanGntyGuarTbl a
             WHERE     a.LoanAppNmb = p_LoanAppNmb
                   AND a.TaxId = b.TaxId
                   AND GuarBusPerInd = 'P'
                   AND LoanGuarActvInactInd = 'A'
            ORDER BY GuarSeqNmb;


         p_RetVal := SQL%ROWCOUNT;
      END;
   /* 38 = Leet for Extract XML, initially indented for union */
   ELSIF p_Identifier = 38
   THEN
      BEGIN
         OPEN p_SelCur FOR
            SELECT m.TaxId,
                   m.GuarBusPerInd,
                   m.GuarSeqNmb,                                                                       --NK-- 01/12/2017
                   m.LoanGuarOperCoInd,
                   m.LACGntyCondTypCd,
                   NULL LoanAffilInd,                                                     --NK--10/12/2016--OPSMDEV 1018
                   m.OutlawCd,
                   m.OutlawOthrDscTxt,
                   m.GntySubCondTypCd,
                   --m.GntyLmtAmt ,
                   TO_CHAR (m.GntyLmtAmt, 'FM999999999999990.00') GntyLmtAmt,
                   --m.GntyLmtPct,
                   TO_CHAR (m.GntyLmtPct, 'FM990.000') GntyLmtPct,
                   m.LoanCollatSeqNmb,
                   m.GntyLmtYrNmb,
                   -- Both Bus and Per
                   bnk.ACHAcctNmb,
                   bnk.ACHAcctTypCd,
                   bnk.ACHRoutingNmb,
                   c.BusAltEmailAdr AltEmailAdr,
                   c.BusAltPhnNmb AltPhnNmb,
                   m.GuarBusPerInd BusPerInd,
                   b.EthnicCd,
                   TO_CHAR (c.BusExtrnlCrdtScorDt, 'Mon dd yyyy hh:miAM') ExtrnlCrdtScorDt,
                   c.BusExtrnlCrdtScorInd ExtrnlCrdtScorInd,
                   c.BusExtrnlCrdtScorNmb ExtrnlCrdtScorNmb,
                   b.GndrCd,
                   c.IMCrdtScorSourcCd,
                   c.BusMailAddrCntCd MailAddrCntCd,
                   c.BusMailAddrCtyNm MailAddrCtyNm,
                   c.BusMailAddrPostCd MailAddrPostCd,
                   c.BusMailAddrStCd MailAddrStCd,
                   c.BusMailAddrStNm MailAddrStNm,
                   c.BusMailAddrStr1Nm MailAddrStr1Nm,
                   c.BusMailAddrStr2Nm MailAddrStr2Nm,
                   c.BusMailAddrZip4Cd MailAddrZip4Cd,
                   c.BusMailAddrZipCd MailAddrZipCd,
                   c.BusPhyAddrCntCd PhyAddrCntCd,
                   c.BusPhyAddrCtyNm PhyAddrCtyNm,
                   c.BusPhyAddrPostCd PhyAddrPostCd,
                   c.BusPhyAddrStCd PhyAddrStCd,
                   c.BusPhyAddrStNm PhyAddrStNm,
                   c.BusPhyAddrStr1Nm PhyAddrStr1Nm,
                   c.BusPhyAddrStr2Nm PhyAddrStr2Nm,
                   c.BusPhyAddrZip4Cd PhyAddrZip4Cd,
                   c.BusPhyAddrZipCd PhyAddrZipCd,
                   c.BusPrimEmailAdr PrimEmailAdr,
                   c.BusPrimPhnNmb PrimPhnNmb,
                   b.VetCd,
                   -- Unique to Bus
                   c.BusDUNSNmb,
                   b.BusEINCertInd,
                   b.BusNm,
                   c.BusPriorSBALoanInd,
                   c.BusTrdNm,
                   b.BusTypCd,
                   -- Unique to Per
                   NULL PerFirstNm,
                   NULL PerInitialNm,
                   NULL PerLastNm,
                   NULL PerSfxNm,
                   NULL PerTitlTxt,
                   NULL PerUSCitznInd,
                   null LIABINSURRQRDIND,
                   null PRODLIABINSURRQRDIND,
                   null LIQLIABINSURRQRDIND,
                   null MALPRCTSINSURRQRDIND,
                   null OTHINSURRQRDIND,
                   null WORKRSCOMPINSRQRDIND 
              FROM loan.LoanGntyGuarTbl m                                  -- main, keeps code similar to borr/guar/prin
                   INNER JOIN loan.BusTbl b ON (m.TaxId = b.TaxId)
                   INNER JOIN Loan.LoanGntyBusTbl c ON m.Taxid = c.Taxid AND m.LoanAppNmb = c.LoanAppNmb
                   LEFT JOIN loan.BnkTbl bnk
                      ON (    (bnk.TaxId = b.TaxId)
                          AND (bnk.BusPerInd = 'B'))
             WHERE     m.LoanAppNmb = p_LoanAppNmb
                   AND m.LoanGuarActvInactInd = 'A'
                   AND m.GuarBusPerInd = 'B'
            UNION
            SELECT m.TaxId,
                   m.GuarBusPerInd,
                   m.GuarSeqNmb,
                   m.LoanGuarOperCoInd,
                   m.LACGntyCondTypCd,
                   NULL LoanAffilInd,
                   m.OutlawCd,
                   m.OutlawOthrDscTxt,
                   m.GntySubCondTypCd,
                   --m.GntyLmtAmt ,
                   TO_CHAR (m.GntyLmtAmt, 'FM999999999999990.00') GntyLmtAmt,
                   --m.GntyLmtPct,
                   TO_CHAR (m.GntyLmtPct, 'FM990.000') GntyLmtPct,
                   m.LoanCollatSeqNmb,
                   m.GntyLmtYrNmb,
                   -- Both Bus and Per
                   bnk.ACHAcctNmb,
                   bnk.ACHAcctTypCd,
                   bnk.ACHRoutingNmb,
                   OcaDataOut (p.PerAltEmailAdr) AS AltEmailAdr,
                   OcaDataOut (p.PerAltPhnNmb) AS AltPhnNmb,
                   m.GuarBusPerInd BusPerInd,
                   pt.EthnicCd,
                   TO_CHAR (p.PerExtrnlCrdtScorDt, 'Mon dd yyyy hh:miAM') ExtrnlCrdtScorDt,
                   p.PerExtrnlCrdtScorInd ExtrnlCrdtScorInd,
                   p.PerExtrnlCrdtScorNmb ExtrnlCrdtScorNmb,
                   pt.GndrCd,
                   p.IMCrdtScorSourcCd,
                   p.PerMailAddrCntCd MailAddrCntCd,
                   p.PerMailAddrCtyNm MailAddrCtyNm,
                   p.PerMailAddrPostCd MailAddrPostCd,
                   p.PerMailAddrStCd MailAddrStCd,
                   p.PerMailAddrStNm MailAddrStNm,
                   OcaDataOut (p.PerMailAddrStr1Nm) AS MailAddrStr1Nm,
                   OcaDataOut (p.PerMailAddrStr2Nm) AS MailAddrStr2Nm,
                   p.PerMailAddrZip4Cd MailAddrZip4Cd,
                   p.PerMailAddrZipCd MailAddrZipCd,
                   p.PerPhyAddrCntCd PhyAddrCntCd,
                   p.PerPhyAddrCtyNm PhyAddrCtyNm,
                   p.PerPhyAddrPostCd PhyAddrPostCd,
                   p.PerPhyAddrStCd PhyAddrStCd,
                   p.PerPhyAddrStNm PhyAddrStNm,
                   OcaDataOut (p.PerPhyAddrStr1Nm) AS PhyAddrStr1Nm,
                   OcaDataOut (p.PerPhyAddrStr2Nm) AS PhyAddrStr2Nm,
                   p.PerPhyAddrZip4Cd PhyAddrZip4Cd,
                   p.PerPhyAddrZipCd PhyAddrZipCd,
                   OcaDataOut (p.PerPrimEmailAdr) AS PrimEmailAdr,
                   OcaDataOut (p.PerPrimPhnNmb) AS PrimPhnNmb,
                   pt.VetCd,
                   -- Unique to Bus
                   NULL BusDUNSNmb,
                   NULL BusEINCertInd,
                   NULL BusNm,
                   NULL BusPriorSBALoanInd,
                   NULL BusTrdNm,
                   NULL BusTypCd,
                   -- Unique to Per
                   OcaDataOut (pt.PerFirstNm) AS PerFirstNm,
                   pt.PerInitialNm,
                   OcaDataOut (pt.PerLastNm) AS PerLastNm,
                   pt.PerSfxNm,
                   p.PerTitlTxt,
                   p.PerUSCitznInd,
                   null LIABINSURRQRDIND,
                   null PRODLIABINSURRQRDIND,
                   null LIQLIABINSURRQRDIND,
                   null MALPRCTSINSURRQRDIND,
                   null OTHINSURRQRDIND,
                   null WORKRSCOMPINSRQRDIND 
              FROM loan.LoanGntyGuarTbl m                                  -- main, keeps code similar to borr/guar/prin
                   INNER JOIN loan.PerTbl pt ON (m.TaxId = pt.TaxId)
                   INNER JOIN loan.loangntyPerTbl p ON (m.TaxId = p.TaxId)
                   LEFT JOIN loan.BnkTbl bnk
                      ON (    (bnk.TaxId = p.TaxId)
                          AND (bnk.BusPerInd = 'P'))
             WHERE     m.LoanAppNmb = p_LoanAppNmb
                   AND m.LoanGuarActvInactInd = 'A'
                   AND m.GuarBusPerInd = 'P'
                   
            ORDER BY 1, 2;                                                                           -- TaxId, BusPerInd


         p_RetVal := SQL%ROWCOUNT;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
END LOANGNTYGUARSELTSP;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLANAREADROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANLOOKUPUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO READLOANROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARSELTSP TO UPDLOANROLE;

CREATE OR REPLACE PROCEDURE LOAN.EXTRACTSELCSP (p_LoanAppNmb   IN     NUMBER := NULL,
                                                p_LoanNmb      IN     VARCHAR2 := NULL,
                                                p_RetVal          OUT NUMBER,
                                                p_SelCur1         OUT SYS_REFCURSOR,
                                                p_SelCur2         OUT SYS_REFCURSOR,
                                                p_SelCur3         OUT SYS_REFCURSOR,
                                                p_SelCur4         OUT SYS_REFCURSOR,
                                                p_SelCur5         OUT SYS_REFCURSOR,
                                                p_SelCur6         OUT SYS_REFCURSOR,
                                                p_SelCur7         OUT SYS_REFCURSOR,
                                                p_SelCur8         OUT SYS_REFCURSOR,
                                                p_SelCur9         OUT SYS_REFCURSOR,
                                                p_SelCur10        OUT SYS_REFCURSOR,
                                                p_SelCur11        OUT SYS_REFCURSOR,
                                                p_SelCur12        OUT SYS_REFCURSOR,
                                                p_SelCur13        OUT SYS_REFCURSOR,
                                                p_SelCur14        OUT SYS_REFCURSOR,
                                                p_SelCur15        OUT SYS_REFCURSOR,
                                                p_SelCur16        OUT SYS_REFCURSOR,
                                                p_SelCur17        OUT SYS_REFCURSOR,
                                                p_SelCur18        OUT SYS_REFCURSOR,
                                                p_SelCur19        OUT SYS_REFCURSOR,
                                                p_SelCur20        OUT SYS_REFCURSOR,
                                                p_SelCur21        OUT SYS_REFCURSOR,
                                                p_SelCur22        OUT SYS_REFCURSOR,
                                                p_SelCur23        OUT SYS_REFCURSOR,
                                                p_SelCur24        OUT SYS_REFCURSOR,
                                                p_SelCur25        OUT SYS_REFCURSOR,
                                                p_SelCur26        OUT SYS_REFCURSOR,
                                                p_SelCur27        OUT SYS_REFCURSOR,
                                                p_SelCur28        OUT SYS_REFCURSOR) AS
    /*SB -- This procedure created for Extract XML */
    /*SB -- Added p_selcur18 on 02/25/2015 */
    /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met, Replaced the initialization block to check for correct loannmb and loanappnmb*/
    /*NK -- 01/12/2017 Added  p_SELCUR20 and p_SELCUR21*/
    /*JP -- 01/30/2017 CAFSOPER-1341 Changed p_SelCur19�s procedure from LOANINFOEXTRACTCSP to LOANINFOEXTRACTSELCSP. (Added "SEL".)
    RY -- 03/17/2017 --OPSMDEV --1401-- Added P_SELCUR22
    BR -- 05/26/2017 --OPSMDEV-1434 -- Added P_SelCur23 Loan.LoanExprtCntrySelTSP
    SS--07/19/2018--OPSMDEV--1861-- Added  P_SELCUR24 and  P_SELCUR25 for Agents
    JP - CAfsoper-2988 08/08/2019 - Modified call to LoanGntyGuarSelTSP
    RY:: 12/16/2019- OPSMDEV-2347 :: Added LOAN.DEFRMNTHISTRYSELTSP to the list
    -- JP 8/21/2020 CARESACT 621 added call to LOAN.LoanHldStatSelTSP
    */
    --
    v_LoanNmb      CHAR (10);
    v_LoanAppNmb   NUMBER;
    v_RetVal       NUMBER;
BEGIN
    -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
    BEGIN
        IF (   p_LoanAppNmb IS NOT NULL
            OR p_LoanNmb IS NOT NULL) THEN
            SELECT LoanAppNmb, LoanNmb
            INTO v_LoanAppNmb, v_LoanNmb
            FROM loan.LoanGntyTbl
            WHERE     LoanAppNmb = NVL (p_LoanAppNmb, LoanAppNmb)
                  AND LoanNmb = NVL (p_LoanNmb, LoanNmb);
        ELSE
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            v_LoanAppNmb := NULL;
            v_LoanNmb := NULL;
    END;



    IF v_LoanAppNmb IS NULL THEN
        BEGIN
            p_RetVal := 0;

            OPEN p_SelCur1 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur2 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur3 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur4 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur5 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur6 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur7 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur8 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur9 FOR SELECT v_RetVal
                               FROM DUAL
                               WHERE 1 = 0;

            OPEN p_SelCur10 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur11 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur12 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur13 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur14 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur15 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur16 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur17 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur18 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur19 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur20 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur21 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur22 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur23 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur24 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;

            OPEN p_SelCur25 FOR SELECT v_RetVal
                                FROM DUAL
                                WHERE 1 = 0;
            
            OPEN p_SelCur28 FOR
                SELECT v_RetVal
                  FROM DUAL
                 WHERE 1 = 0;
        END;
    ELSE
        BEGIN
            LoanGntySelTSP (
                38,
                v_LoanAppNmb,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                P_RetVal,
                p_SelCur1);
            LoanAssocSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur2);
            LoanGntyBorrSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur3);
            LoanGntyBSSelTSP (38, v_RetVal, v_LoanAppNmb, NULL, p_SelCur4);
            LoanGntyCollatSelTSP (38, v_LoanAppNmb, NULL, p_SelCur5);
            LoanGntyCollatLienSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur6);
            LoanDisbSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur7);
            LoanDisbPymtSelTSP (38, v_LoanAppNmb, NULL, NULL, p_SelCur8);
            /*LoanGntyGuarSelTSP (38,
                                v_LoanAppNmb,
                                NULL,
                                NULL,
                                v_RetVal,
                                p_SelCur9);*/
            -- JP modified for CAFSOPER - 2988
            LoanGntyGuarSelTSP (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SelCur9);

            LoanGntyInjctnSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur10);
            LoanIntDtlSelTSP (38, v_LoanAppNmb, p_SelCur11);
            LoanGntyPartLendrSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur12);

            LoanGntyPrinSelTSP (38, v_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur13);
            LoanGntyRaceSelTSP (38, v_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
            LoanGntyProcdSelTSP (38, NULL, v_LoanAppNmb, NULL, v_RetVal, p_SelCur15);
            LoanGntySubProcdSelTSP (38, p_LoanAppNmb, NULL,NULL, p_SelCur28, v_RetVal);
            LoanGntyPrtCmntSelTSP (38, v_LoanAppNmb, v_RetVal, p_SelCur16);
            LoanGntySpcPurpsSelTSP (38, v_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur17);
            LoanGntyStbyAgrmtDtlSelTSP (38, v_LoanAppNmb, NULL, p_SELCUR20, v_RETVAL);
            LoanLmtGntyCollatSelTSP (38, v_LOANAPPNMB, NULL, NULL, NULL, NULL, P_SELCUR21, v_RETVAL);

            LoanGntyEconDevObjctChldSelTsp (38, v_LOANAPPNMB, NULL, P_SELCUR22, v_RETVAL);
            LoanExprtCntrySelTsp (
                p_IDENTIFIER => 38,
                p_LOANAPPNMB => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SELCUR => P_SELCUR23);
            --(38, v_LOANAPPNMB, P_SELCUR23, v_RETVAL);
            LoanAgntSelTsp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR24);
            --(38, v_LoanAppNmb, NULL, p_SELCUR24, v_RetVal);


            LoanAgntfeeDtlSelTSp (
                p_Identifier => 38,
                p_LoanAppNmb => v_LoanAppNmb,
                p_RetVal => v_RetVal,
                p_SelCur => p_SELCUR25);
            --(38, v_LoanAppNmb, NULL, p_SELCUR25, v_RetVal);

            LOAN.DEFRMNTHISTRYSELTSP (38, v_loanappnmb, NULL, v_RetVal, p_SELCUR26);
            -- JP 8/21/2020 CARESACT 621
            LOAN.LoanHldStatSelTSP (p_Identifier => 38, p_LoanAppNmb => v_LoanAppNmb, p_SelCur => p_SELCUR27);

            OPEN p_SELCUR18 FOR SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.BusRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'B'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                UNION
                                SELECT m.TaxId AS BorrTaxId, m.BorrBusPerInd, r.RaceCd
                                FROM LoanGntyBorrTbl m
                                     INNER JOIN loan.PerRaceTbl r
                                     ON (    (m.TaxId = r.TaxId)
                                         AND (m.BorrBusPerInd = 'P'))
                                WHERE m.LoanAppNmb = v_LoanAppNmb
                                ORDER BY 1, 2;                                             /*BorrTaxId, BorrBusPerInd */

            LOAN.LOANINFOEXTRACTSELCSP (v_LoanAppNmb, v_LoanNmb, p_SelCur19);
        END;
    END IF;
END ExtractSelCSP;
/



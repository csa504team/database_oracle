CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYSUBPROCDINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LoanAppNmb                NUMBER := 0,
	p_PROCDTYPCD 				CHAR,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_LOANSUBPROCDTYPCD		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCDTXTBLCKTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
    cursor desc_cur is select SUBPROCDTYPDESCTXT from SBAREF.LOANSUBPROCDTYPTBL where LOANSUBPROCDTYPCD = p_LOANSUBPROCDTYPCD; 
	cursor procdcdtbl_cur is select nvl(LOANPROCDSEQNMB,0) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb and LOANPROCDTYPCD=p_LoanProcdTypcd and PROCDTYPCD=p_PROCDTYPCD;
	cursor procdtbl_max is select max(nvl(LOANPROCDSEQNMB,0)) from LOAN.LOANGNTYPROCDTBL where LOANAPPNMB=p_LoanAppNmb;
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_SUBPROCDTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
	v_LOANPROCDSEQNMB number := 0;
	p_LOANSUBPROCDTYPID number :=0;
	
	v_Identifier 	        NUMBER:=0;
    v_RetVal                 NUMBER:=0;
    v_LoanAppNmb 	        NUMBER:=0;
    v1_LoanProcdSeqNmb 	     NUMBER:=0;
    v1_LoanProcdTypCd 	    CHAR:=NULL;
    v_ProcdTypCd 	        CHAR:=NULL;
    v_LoanProcdOthTypTxt 	VARCHAR2(150):=NULL;
    v_LoanProcdAmt 	        NUMBER:=NULL;
    v_LoanProcdCreatUserId 	CHAR:=NULL;
    v_LoanProcdRefDescTxt       VARCHAR2(50):=NULL;
    v_LoanProcdPurAgrmtDt       DATE;
    v_LoanProcdPurIntangAssetAmt NUMBER:=NULL;
    v_LoanProcdPurIntangAssetDesc VARCHAR2(50):=NULL;
    v_LoanProcdPurStkHldrNm          VARCHAR2(50):=NULL;
    v_NCAIncldInd    char:=NULL;
    v_StkPurCorpText    char:=NULL;
    v_LASTUPDTUSERID    varchar2(50):=NULL;
    max_LOANPROCDSEQNMB number :=0;
    
BEGIN
    SAVEPOINT LOANGNTYSUBPROCDINSTSP;

    /* Insert into LOANSUBPROCDTBL Table */
    IF p_Identifier = 0 --for XML
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_SUBPROCDTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> '04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_SUBPROCDTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc1]',  SUBSTR(v_SUBPROCDTYPDESCTXT, 0, INSTR(v_SUBPROCDTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc2]',SUBSTR(v_SUBPROCDTYPDESCTXT,INSTR(v_SUBPROCDTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
		
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if v_LOANPROCDSEQNMB=0  then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
        
    ELSIF p_Identifier = 1 --for CF code 
    THEN
		open procdcdtbl_cur;
		fetch procdcdtbl_cur into v_LOANPROCDSEQNMB;
		open procdtbl_max;
		fetch procdtbl_max into max_LOANPROCDSEQNMB;
		if  v_LOANPROCDSEQNMB=0 then 
            LOAN.LOANGNTYPROCDINSTSP(0,p_LoanAppNmb,max_LOANPROCDSEQNMB+1,p_PROCDTYPCD,p_LoanProcdTypCd,v_LoanProcdOthTypTxt,p_LOANPROCDAMT,user,user,
            v_LoanProcdRefDescTxt,v_LoanProcdPurAgrmtDt,v_LoanProcdPurIntangAssetAmt,v_LoanProcdPurIntangAssetDesc,v_LoanProcdPurStkHldrNm,v_NCAIncldInd,v_StkPurCorpText,v_RetVal);
            --v_LOANPROCDSEQNMB := v1_LoanProcdSeqNmb;
        end if;
            
        BEGIN
            select LOAN.LOANGNTYSUBPROCDIDSEQ.nextval into p_LOANSUBPROCDTYPID from dual;
            INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
                SELECT 	p_LOANSUBPROCDTYPID,
                       	p_LoanAppNmb,
						--v_LOANPROCDSEQNMB,
                       	p_LoanProcdTypcd,
						p_PROCDTYPCD,
                       	p_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			p_LOANSUBPROCDTYPCD,
			p_LoanProcdDesc,
			p_Lender
                  FROM DUAL;
            LOANGNTYSUBPROCDSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd,p_PROCDTYPCD);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANGNTYSUBPROCDINSTSP;
            RAISE;
        END;
END LOANGNTYSUBPROCDINSTSP;
/
GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYSUBPROCDINSTSP TO UPDLOANROLE;

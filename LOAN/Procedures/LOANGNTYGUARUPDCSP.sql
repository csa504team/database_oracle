CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARUPDCSP
(
    p_LoanAppNmb            number := 0 ,
    p_CreatUserId char := NULL
)as
/*
 Revision:
 APK -- 12/2/2011-- modified to add last update userid and last update date during insert into LoanHistryInfoTbl as this was missing. 
 APK -- 12/2/2011-- modified to add LastUpdtUserId,LastUpdtDt additionally to the insert call to LoanSpcPurpsHistryTbl as it was taking default USER from DB side.
 SP:9/26/2012: Modified as column LoanAffilInd is added to the table for determining companion relationship
 SP:11/15/2013: Modified to add LACGNTYCONDTYPCD to support LADS screens
NK--08/08/2016--OPSMDEV-1030-- Removed LoanCntlIntInd
  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .
 */
v_LoanHistrySeqNmb number;
v_temp number;
v_temp1 number;
begin  
	--Get the history number based on the loan number and create user id
	loan.LoanHistrySelCSP(p_LoanAppNmb,p_CreatUserId,v_LoanHistrySeqNmb,'LoanGuarHistryTbl');
	
    --select count(*) into v_temp from LoanGuarHistryTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb;
    --if v_temp = 0 then
    --begin
        --Insert into history if history not exists
         INSERT INTO LoanGuarHistryTbl
           ( LoanHistrySeqNmb, GuarSeqNmb, TaxId, GuarBusPerInd, LoanGuarOperCoInd, LoanGuarHistryCreatUserId, LoanGuarHistryCreatDt,OUTLAWCD, OUTLAWOTHRDSCTXT,LastUpdtUserId,
			    LastUpdtDt,LACGNTYCONDTYPCD, GntySubCondTypCd  , 
               GntyLmtAmt , 
               GntyLmtPct, 
               LoanCollatSeqNmb , 
               GntyLmtYrNmb ,                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT   )
           SELECT v_LoanHistrySeqNmb,
                    GuarSeqNmb,
                    TaxId,
                    GuarBusPerInd,
                    LoanGuarOperCoInd,
                    LoanGuarLastUpdtUserId,
                    LoanGuarLastUpdtDt,
                    OUTLAWCD, 
                    OUTLAWOTHRDSCTXT,
                    LoanGuarLastUpdtUserId,
                    LoanGuarLastUpdtDt,
                    LACGNTYCONDTYPCD,
                     GntySubCondTypCd  , 
                     GntyLmtAmt , 
                     GntyLmtPct, 
                     LoanCollatSeqNmb , 
                     GntyLmtYrNmb  ,
                    LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT 
                    
             FROM LoanGntyGuarTbl a
                WHERE a.LoanAppNmb = p_LoanAppNmb;
				--AND a.LoanGuarActvInactInd = 'A' );
	--END;
    --end if;
	
	/*select count(*) into v_temp from LoanGuarHistryTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb;
    select count(*) into v_temp1 from LoanHistryInfoTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb and LoanHistryTblNm = 'LoanGuarHistryTbl';
	
    if v_temp = 0 AND v_temp1 = 0 then
	begin
		--Insert into history if history not exists
		insert into LoanHistryInfoTbl
		(
			LoanHistrySeqNmb,
			LoanHistryTblNm,
            LastUpdtUserId,
			    LastUpdtDt
		)
		values
		(
			v_LoanHistrySeqNmb,
			'LoanGuarHistryTbl',
            p_CreatUserId,
			SYSDATE
		);		
	end;
    end if;*/
		
	--loan.LoanChngLogInsCSP(p_LoanAppNmb,'S',p_CreatUserId,null,null);	
end;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDCSP TO UPDLOANROLE;

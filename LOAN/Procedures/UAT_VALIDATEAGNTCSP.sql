CREATE OR REPLACE PROCEDURE LOAN.VALIDATEAGNTCSP ( p_Identifier     IN     NUMBER := 0,
                                                    p_RetVal         OUT NUMBER,
                                                     p_LoanAppNmb  IN     NUMBER DEFAULT NULL,
                                                     p_ErrSeqNmb   IN OUT NUMBER,
                                                     p_TransInd    IN     NUMBER DEFAULT NULL,
                                                     p_LOANAGNTSeqNmb IN NUMBER DEFAULT NULL,
                                                     p_VALIDATIONTYP  IN     CHAR := NULL,
                                                     p_SelCur1           OUT SYS_REFCURSOR,
                                                     p_SelCur2           OUT SYS_REFCURSOR)

AS
--SS--07/27/2018 --OPSMDEV1861
--BR--10/26/2018--CAFSOPER2199- removed NOT from NULL analysis for both LOANAGNTAPPCNTPAIDAMT and LOANAGNTSBALENDRPAIDAMT because error should only fires if BOTH are null;
--RY- 11/15/2018--OPSMDEV-1913:: Added OCADATAOUT for PII columns
--BR--12/12/2018--CAFSOPER2345--Added  'B' to if then else list for v_LOANAGNTBUSPERIND for error code 4173, 4174

   v_LoanAgntSeqNmb       NUMBER (10);
   v_LOANAGNTBUSPERIND    CHAR (1);
   v_LOANAGNTNM           VARCHAR2 (200);
   v_LOANAGNTCNTCTFIRSTNM VARCHAR2 (40);
   v_LOANAGNTCNTCTMIDNM   CHAR (1);
   v_LOANAGNTCNTCTLASTNM  VARCHAR2 (40);
   v_LOANAGNTCNTCTSFXNM   VARCHAR2 (4);
   v_LOANAGNTADDRSTR1NM   VARCHAR2 (80);
   v_LOANAGNTADDRSTR2NM   VARCHAR2 (80);
   v_LOANAGNTADDRCTYNM    VARCHAR2 (40);
   v_LOANAGNTADDRSTCD     CHAR (2);
   v_LOANAGNTADDRSTNM     VARCHAR2 (60);
   v_LOANAGNTADDRZIPCD    CHAR (5);
   v_LOANAGNTADDRZIP4CD   CHAR (4);
   v_LOANAGNTADDRPOSTCD   VARCHAR2 (20);
   v_LOANAGNTADDRCNTCD    CHAR (2);
   v_LOANAGNTTYPCD        NUMBER (3);
   v_LOANAGNTDOCUPLDIND   CHAR (1);
   v_LOANCDCTPLFEEIND     CHAR (1);
   v_LOANCDCTPLFEEAMT     NUMBER (15, 2);
   v_PRGMCD               CHAR (2);
   v_LOANAGNTSERVTYPCD   Number(3);
   v_ChkVal      Number;
   v_LOANAGNTAPPCNTPAIDAMT   Number(15,2);
   v_LOANAGNTSBALENDRPAIDAMT  Number(15,2);
   v_chkval1   Number;
   v_ErrCd                 NUMBER (10, 2);
   v_feedtlcnt NUMBER;

   CURSOR Agnt_Cur
   IS
      SELECT LoanAgntSeqNmb,
             LOANAGNTBUSPERIND,
             LOANAGNTNM,
             OcaDataOut(LOANAGNTCNTCTFIRSTNM),
             LOANAGNTCNTCTMIDNM,
             OcaDataOut(LOANAGNTCNTCTLASTNM),
             LOANAGNTCNTCTSFXNM,
             OcaDataOut(LOANAGNTADDRSTR1NM),
             OcaDataOut(LOANAGNTADDRSTR2NM),
             LOANAGNTADDRCTYNM,
             LOANAGNTADDRSTCD,
             LOANAGNTADDRSTNM,
             LOANAGNTADDRZIPCD,
             LOANAGNTADDRZIP4CD,
             LOANAGNTADDRPOSTCD,
             LOANAGNTADDRCNTCD,
             LOANAGNTTYPCD,
             LOANAGNTDOCUPLDIND,
             LOANCDCTPLFEEIND,
             LOANCDCTPLFEEAMT
      FROM   loanagnttbl
      WHERE  loanappnmb = p_loanappnmb
      and   LOANAGNTSeqNmb=  p_LOANAGNTSeqNmb;
        BEGIN
   p_ErrSeqNmb := 0;

   IF p_Identifier < 100 Then
      DELETE LoanGntyValidErrTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   END IF;    
--Begin

  BEGIN
      SELECT PRGMCD
      INTO   v_PRGMCD
      FROM   LoanGntytbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PRGMCD := NULL;
   END;
        

 
   OPEN Agnt_Cur;
   FETCH Agnt_Cur
      INTO v_LoanAgntSeqNmb,
           v_LOANAGNTBUSPERIND,
           v_LOANAGNTNM,
           v_LOANAGNTCNTCTFIRSTNM,
           v_LOANAGNTCNTCTMIDNM,
           v_LOANAGNTCNTCTLASTNM,
           v_LOANAGNTCNTCTSFXNM,
           v_LOANAGNTADDRSTR1NM,
           v_LOANAGNTADDRSTR2NM,
           v_LOANAGNTADDRCTYNM,
           v_LOANAGNTADDRSTCD,
           v_LOANAGNTADDRSTNM,
           v_LOANAGNTADDRZIPCD,
           v_LOANAGNTADDRZIP4CD,
           v_LOANAGNTADDRPOSTCD,
           v_LOANAGNTADDRCNTCD,
           v_LOANAGNTTYPCD,
           v_LOANAGNTDOCUPLDIND,
           v_LOANCDCTPLFEEIND,
           v_LOANCDCTPLFEEAMT;
          
   
     BEGIN
      SELECT COUNT (loanappnmb)
      INTO   v_chkval1
      FROM   loan.loangntytbl a
      WHERE      a.loanappnmb = p_loanappnmb
             AND a.LOANAGNTINVLVDIND = 'Y'
             
             AND (   v_PRGMCD = 'A'
                  OR v_PRGMCD = 'E')
             AND a.loanappnmb NOT IN (SELECT Loanappnmb FROM LOAN.LOANAGNTTBL);

      IF v_chkval1 > 0
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4182;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;
   END;
   
                  
   
   v_feedtlcnt := 0;
   
   FOR c2 IN (SELECT loanagntservtypcd, LOANAGNTSERVOTHTYPTXT,LOANAGNTAPPCNTPAIDAMT,LOANAGNTSBALENDRPAIDAMT
              FROM   LoanAgntFeeDtlTbl
              WHERE  Loanappnmb = p_Loanappnmb
               and   LOANAGNTSeqNmb = p_LOANAGNTSeqNmb 
               and (LOANAGNTAPPCNTPAIDAMT IS NOT NULL OR LOANAGNTSBALENDRPAIDAMT IS NOT NULL))
   LOOP
         v_feedtlcnt := v_feedtlcnt + 1;
         
         IF c2.LOANAGNTAPPCNTPAIDAMT IS NULL AND c2.LOANAGNTSBALENDRPAIDAMT IS NULL THEN
             NULL;
              BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4172;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL

                                         );
               END;--add error for both amount is not null
         END IF;
         
      IF     c2.loanagntservtypcd = 5 and c2.LOANAGNTSERVOTHTYPTXT IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4181;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
   END LOOP;

 
   WHILE (Agnt_Cur%FOUND)
   LOOP
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND = 'B'
         AND v_LOANAGNTNM IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4170;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND  v_LOANAGNTTYPCD IS NULL
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4171;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
     
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTFIRSTNM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4173;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF;  
      
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND  v_LOANAGNTCNTCTLASTNM   is null
      THEN
      BEgin
       
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4174;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                          To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      
     IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTR1NM   is null
      THEN
          BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4175;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRCTYNM   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4176;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRSTCD   is null
      THEN
         BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4177;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                          
                                         );
               END;
      END IF; 
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIPCD   is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4178;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                        
                                         );
               END;
      END IF;       
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANAGNTADDRZIP4CD   is null
      THEN
        BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4179;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;     
      IF     v_PRGMCD IN  ('E')
         AND v_LOANAGNTBUSPERIND in('B','P')
         AND v_LOANCDCTPLFEEIND='Y' Then
            if  v_LOANCDCTPLFEEAMT is null
      THEN
       BEGIN
                  p_ErrSeqNmb := p_ErrSeqNmb + 1;
                  v_ErrCd := 4180;

                  LoanGntyValidErrInsCSP (
                                          p_LoanAppNmb,
                                          v_ErrCd,
                                          p_TransInd,
                                          p_VALIDATIONTYP,
                                           To_char(v_Loanagntseqnmb),
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL
                                         
                                         );
               END;
      END IF;   
      END IF;  
    
     
      
     FETCH Agnt_cur
         INTO v_LoanAgntSeqNmb,
              v_LOANAGNTBUSPERIND,
              v_LOANAGNTNM,
              v_LOANAGNTCNTCTFIRSTNM,
              v_LOANAGNTCNTCTMIDNM,
              v_LOANAGNTCNTCTLASTNM,
              v_LOANAGNTCNTCTSFXNM,
              v_LOANAGNTADDRSTR1NM,
              v_LOANAGNTADDRSTR2NM,
              v_LOANAGNTADDRCTYNM,
              v_LOANAGNTADDRSTCD,
              v_LOANAGNTADDRSTNM,
              v_LOANAGNTADDRZIPCD,
              v_LOANAGNTADDRZIP4CD,
              v_LOANAGNTADDRPOSTCD,
              v_LOANAGNTADDRCNTCD,
              v_LOANAGNTTYPCD,
              v_LOANAGNTDOCUPLDIND,
              v_LOANCDCTPLFEEIND,
              v_LOANCDCTPLFEEAMT;
   END LOOP;

   CLOSE Agnt_Cur;
 
    IF    (p_Identifier = 11)
      OR (p_Identifier = 111)
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'F'
            ORDER BY LoanGntyValidErrSeqNmb;

         OPEN p_SelCur2 FOR
            SELECT   LoanGntyValidErrSeqNmb ErrSeqNmb,
                     ErrTypCd,
                     ErrCd,
                     LoanGntyValidErrTxt ErrTxt
            FROM     LoanGntyValidErrTbl
            WHERE        LoanAppNmb = p_LoanAppNmb
                     AND ErrTypInd = 'W'
            ORDER BY LoanGntyValidErrSeqNmb;
      END;
   END IF;

   p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
   END;
/

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LMSDEV;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.VALIDATEAGNTCSP TO UPDLOANROLE;

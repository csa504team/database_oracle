CREATE OR REPLACE PROCEDURE LOAN.LOANGNTYGUARUPDTSP (p_IDENTIFIER                      NUMBER := 0,
                                                     p_RETVAL                      OUT NUMBER,
                                                     p_LOANAPPNMB                      NUMBER := 0,
                                                     p_GUARSEQNMB                      NUMBER := NULL,
                                                     p_TAXID                           VARCHAR2 := NULL,
                                                     p_GUARBUSPERIND                   VARCHAR2 := NULL,
                                                     p_LOANGUAROPERCOIND               CHAR := NULL,
                                                     p_LOANGUARACTVINACTIND            VARCHAR2 := NULL,
                                                     p_LOANGUARLASTUPDTUSERID          VARCHAR2 := NULL,
                                                     p_OUTLAWCD                        CHAR := NULL,
                                                     p_OUTLAWOTHRDSCTXT                VARCHAR2 := NULL,
                                                     p_TAXIDCHNGIND             IN     CHAR := NULL,
                                                     p_NEWTAXID                 IN     CHAR := NULL,
                                                     p_LACGNTYCONDTYPCD                NUMBER := NULL,
                                                     p_GntySubCondTypCd                NUMBER := NULL,
                                                     p_GntyLmtAmt                      NUMBER := NULL,
                                                     p_GntyLmtPct                      NUMBER := NULL,
                                                     p_LoanCollatSeqNmb                NUMBER := NULL,
                                                     p_GntyLmtYrNmb                    NUMBER := NULL,
                                                     p_LIABINSURRQRDIND               CHAR:=NULL,
                                                     p_PRODLIABINSURRQRDIND           CHAR:=NULL,
                                                     p_LIQLIABINSURRQRDIND            CHAR:=NULL,
                                                     p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
                                                     p_OTHINSURRQRDIND                CHAR:=NULL,
                                                     p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
                                                     p_OTHINSURDESCTXT                VARCHAR2:=NULL) AS
/*
  SP:11/12/2013:Created to support LADS screens
 *NK--08/08/2016--OPSMDEV-1030--  Removed LoanCntlIntInd  */
--  NK-- 10/07/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb .
--   NK -- 02/14/2018 --OPSMDEV 1672-- Added ID 14

BEGIN
    SAVEPOINT LOANGNTYGUARUPD;

    IF p_IDENTIFIER = 0 THEN
        BEGIN
            UPDATE LOAN.LOANGNTYGUARTBL
            SET TAXID = p_TAXID,
                GUARBUSPERIND = p_GUARBUSPERIND,
                LOANGUAROPERCOIND = p_LOANGUAROPERCOIND,
                LOANGUARACTVINACTIND = p_LOANGUARACTVINACTIND,
                OUTLAWCD = p_OUTLAWCD,
                OUTLAWOTHRDSCTXT = p_OUTLAWOTHRDSCTXT,
                LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
                LOANGUARLASTUPDTDT = SYSDATE,
                GntySubCondTypCd = p_GntySubCondTypCd,
                GntyLmtAmt = p_GntyLmtAmt,
                GntyLmtPct = p_GntyLmtPct,
                LoanCollatSeqNmb = p_LoanCollatSeqNmb,
                GntyLmtYrNmb = p_GntyLmtYrNmb,
                LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                OTHINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                OTHINSURDESCTXT = p_OTHINSURDESCTXT
            WHERE     LOANAPPNMB = p_LOANAPPNMB
                  AND GUARSEQNMB = p_GUARSEQNMB;

            p_RETVAL := SQL%ROWCOUNT;
        END;
    ELSIF p_IDENTIFIER = 11 THEN
        -- Update LACGNTYCONDTYPCD for LADS screens
        BEGIN
            UPDATE LOAN.LOANGNTYGUARTBL
            SET LACGNTYCONDTYPCD = p_LACGNTYCONDTYPCD,
                LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
                LOANGUARLASTUPDTDT = SYSDATE
            WHERE     LOANAPPNMB = p_LOANAPPNMB
                  AND GUARSEQNMB = p_GUARSEQNMB;

            p_RETVAL := SQL%ROWCOUNT;
        END;
    END IF;

    IF p_Identifier = 14 THEN
        UPDATE Loan.LoanGntyGuarTbl
        SET LoanGuarOperCoInd = p_LoanGuarOperCoInd
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND TaxId = p_TaxId;
    END IF;

    IF p_Identifier = 15 THEN
        UPDATE Loan.LoanGntyGuarTbl
        SET LOANGUARACTVINACTIND = p_LOANGUARACTVINACTIND,
            LOANGUARLASTUPDTUSERID = p_LOANGUARLASTUPDTUSERID,
            LOANGUARLASTUPDTDT = SYSDATE
        WHERE     LoanAppNmb = p_LoanAppNmb
              AND TaxId = p_TaxId;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            ROLLBACK TO LOANGNTYGUARUPD;
            RAISE;
        END;
END;
/


GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LCMSMANAGER;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LMSDEV;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANGNTYGUARUPDTSP TO UPDLOANROLE;

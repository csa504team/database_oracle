CREATE OR REPLACE PROCEDURE LOAN.MOVELOANGNTYPROCDCSP(
p_Identifier 	IN NUMBER  := 0,
p_LoanAppNmb 	IN NUMBER  := 0,
p_RetVal  	OUT NUMBER,
p_RetStat 	OUT NUMBER,
p_RetTxt 	OUT VARCHAR2)
AS
/*     Parameters   :  No.   Name        Type     Description
                    1     p_Identifier  int     Identifies which batch
                                               to execute.
                    2     p_LoanAppNmb  int     LoanApplication Number to be moved
                    3     p_CreatUserId char(15)User Id
                    4     p_RetVal      int     No. of rows returned/
                                               affected
                    5     p_RetStat     int     The return value for the update
                    6     p_RetTxt      varchar return value text
*******************
*/
--NK  -- 08/29/2016--OPMSDEV 1099-- added LoanProcdRefDescTxt,LoanProcdPurAgrmtDt,LoanProcdPurIntangAssetAmt,LoanProcdPurIntangAssetDesc,LoanProcdPurStkHldrNm
--RY-- 10/03/2016 ---OPSMDEV 1177---Added NCAIncldInd,StkPurCorpText
BEGIN
--	savepoint moveloangntyprocdcsp;
/* Insert into LoanGntyProcd Table */
		IF  p_Identifier = 0 
		THEN
		
			BEGIN
				
				INSERT INTO LoanGntyProcdTbl (LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText    
                )
				SELECT  LoanAppNmb, LoanProcdSeqNmb, LoanProcdTypCd, ProcdTypCd,
				LoanProcdOthTypTxt, LoanProcdAmt, LoanProcdCreatUserId, LoanProcdCreatDt,LASTUPDTUSERID,LASTUPDTDT,
                LoanProcdRefDescTxt,
              LoanProcdPurAgrmtDt,
              LoanProcdPurIntangAssetAmt,
              LoanProcdPurIntangAssetDesc,
              LoanProcdPurStkHldrNm,
              NCAIncldInd    ,
                StkPurCorpText     

				FROM loanapp.LoanProcdTbl
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				
	/* Insert into LOANGNTYPROCDSUBTYPTBL Table */			
		INSERT INTO LOAN.LOANGNTYSUBPROCDTBL (
			LOANSUBPROCDID, LOANAPPNMB, /*LOANPROCDSEQNMB, */
			LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, 
			LOANPROCDAMT, LOANPROCDADDR, LOANSUBPROCDTYPCD, 
			LOANPROCDDESC, LENDER) 
				SELECT  LOANSUBPROCDID, LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD, LOANPROCDTXTBLCK, LOANPROCDAMT, LoanProcdAddr,LOANSUBPROCDTYPCD, LoanProcdDesc, Lender     

				FROM loanapp.LOANSUBPROCDTBL
				WHERE LoanAppNmb = p_LoanAppNmb;
				
				p_RetStat := SQLCODE;
				p_RetVal :=  SQL%ROWCOUNT;
	
				IF  p_RetStat <> 0 
				THEN
					BEGIN
		
						p_RetTxt :=  'LoanGntyProcdTbl not copied';
		
					END;
				END IF;
		        END;
		END IF;
p_RetVal := nvl(p_RetVal,0);
        p_RetStat := nvl(p_RetStat,0);		
/*exception when others then
begin
    raise;
    rollback to moveloangntyprocdcsp;
end;	*/
END MOVELOANGNTYPROCDCSP;
/


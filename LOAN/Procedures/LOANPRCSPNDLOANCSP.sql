CREATE OR REPLACE PROCEDURE LOAN.LOANPRCSPNDLOANCSP (
   p_LoanAppNmb    NUMBER := NULL,
   p_LoanAppNm     VARCHAR2 := NULL)
AS
   /*
   Date     :
   Programmer   :
   Remarks      :
   ***********************************************************************************
   Revised By      Date        Comments
   ----------     --------    ----------------------------------------------------------
   APK -- 01/14/2011 -- Modified the procedure to make the called procedure Loan.LoanSrvsFinUpdCSP, so that the parameters being passed and the actual parameters that the procedure Loan.LoanSrvsFinUpdCSP ecpects matches. The parameters were coded incorrectly.
   APK -- 03/21/2011 -- Modified to add new column LoanAppMatExtDt..
   APK     03/22/2011  Removed the LOANGNTYFEEREBATEAMT   changes that were added as
                             they there not needed.
   APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. also added Loan1201NotcInd , LOANSERVGRPCD previously added to the Gnty table. added to the call to LoanGntyTbl update statement.
   APK -- 10/19/2011 -- Modified to change Loan1201NotcInd data type from Char  to Number.
   APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
   SP  -- 1/9/2012   --  MODIFIED TO ADD NEW COLUMN LOANPYMTTRANSCD
   APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
   SP  -- 3/5/2012   -- Modified to add new column loancofseffdt
   SP  -- 4/18/2012  -- Modified to add new column LoanAppNetEarnInd
   SP  -- 04/26/2012 -- Modified as ACTIONCD was renamed to ACTNCD
   SP --- 05/01/2012 -- Modified  to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
   LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
   APK -- 05/16/2012 -- Modified as per the below requirement, to add 'RETURN' statement to abort when there is no record in the LoanGntyPreTbl:
   'In the case of disaster disburements, servicing web service is not writing to the PreTables, as there is not a rule of two. Therefore, we need to have loan.LOANPRCSPNDLOANCSP modified to check to see if there exists a record in the LoanGntyPreTbl prior to processing. If there is no data in the LoanGntyPreTbl, the procedure should exit.'
   RGG -- 06/11/2012 -- Modified to add p_LoanAppNm and update it based on logic "if input parameter loannm passed and not null, then update based on loanappnmb, otherwise get
                           from  loan.loangntytbl"
   SP -- 06/12/2012  -- Modified  to retrieve LoanOutBalAmt and LoanOutBalDt from LoanGntyPreTbl and pass them on the parameter list in the call to LoanSrvsFinUpdCSP.
   RSURAPA -- 05/16/2013 -- Added a variable v_NOTEDT, retrived the NOTEDT value from the LoanGntyPreTbl and added the variable as a parameter for the call to the LoanSrvsFinUpdCSP procedure.
   RSURAPA -- 05/22/2013 -- The notedt column was changed to loangntynotedt in the loangntytbl, changed all the reference of notedt in the procedure to refelect the change.
   RGG -- 03/21/2014 -- Deleted references to LOANMAILSTR1NM,LOANMAILSTR2NM,LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,
                         LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD
   SP  -- 04/10/2014 -- Added REPAYINSTLTYPCD
   RGG -- 04/05/2016 -- Added LOANLNDRASSGNLTRDT
   NK  -- 09/07/2016--OPMSDEV 1099-- added LoanProcdRefDescTxt,LoanProcdPurAgrmtDt,LoanProcdPurIntangAssetAmt,LoanProcdPurIntangAssetDesc,LoanProcdPurStkHldrNm
   NK-- 10/25/2016 --OPMSDEV 1216---Added NCAIncldInd,StkPurCorpText
   NK--07/20/2017--CAFSOPER 1007-- Added LoanGntyNoteIntRt and LoanGntyNoteMntlyPymtAmt
   BR--12/05/2018--CAFSOPER 2235-- corrected LastUpdtUserId update for LoanGntyTbl and LoanAudtLogTbl (set by trigger)
   SL--02/01/2021 SODSTORY 447/448 added column LoanGntyPayRollChngInd

   ********************************************************************************
   */
   v_CohortCd                       CHAR (20);
   v_RetVal                         NUMBER (10, 0);
   v_RetTxt                         VARCHAR2 (255);
   v_LoanAppSBAGntyPct              NUMBER (6, 3);
   v_LoanCurrAppvAmt                NUMBER (19, 4);
   v_LoanCurrAppvAmt_old            NUMBER (19, 4);
   v_LoanLastAppvDt                 DATE;
   v_LoanAppRqstMatMoQty            NUMBER (3, 0);
   v_LoanAppRecvDt                  DATE;
   v_LoanAppFundDt                  DATE;
   v_LoanStatCd                     NUMBER (3, 1);
   v_LoanStatCd_old                 NUMBER (3, 1);
   v_LoanStatDt                     DATE;
   v_FyLoanTransChngLoanAmt         NUMBER (19, 4);
   v_LoanTransFisclYrNmb            NUMBER (4, 0);
   v_LastUpdtUserId                 CHAR (15);
   v_LoanAppAdjPrdCd                CHAR (1);
   v_LoanAppPymtAmt                 NUMBER (19, 4);
   v_LoanAppInjctnInd               CHAR (1);
   v_LoanCollatInd                  CHAR (1);
   v_LoanAppDisasterCntrlNmb        VARCHAR2 (20);
   v_LoanAppNetExprtAmt             NUMBER (19, 4);
   v_LoanAppCDCGntyAmt              NUMBER (19, 4);
   v_LoanAppCDCNetDbentrAmt         NUMBER (19, 4);
   v_LoanAppCDCFundFeeAmt           NUMBER (19, 4);
   v_LoanAppCDCSeprtPrcsFeeInd      CHAR (1);
   v_LoanAppCDCPrcsFeeAmt           NUMBER (19, 4);
   v_LoanAppCDCClsCostAmt           NUMBER (19, 4);
   v_LoanAppCDCOthClsCostAmt           NUMBER (19, 4);
   v_LoanAppCDCUndrwtrFeeAmt        NUMBER (19, 4);
   v_LoanAppContribPct              NUMBER (6, 3);              --number(5,2);
   v_LoanAppContribAmt              NUMBER (19, 4);
   v_LoanAppMoIntQty                NUMBER (10, 0);
   v_LoanAppMatDt                   DATE;
   v_LoanAppOutPrgrmAreaOfOperInd   CHAR (1);
   v_LoanAppParntLoanNmb            CHAR (10);
   v_ACHRtngNmb                     VARCHAR2 (25);
   v_ACHAcctNmb                     VARCHAR2 (25);
   v_ACHAcctTypCd                   CHAR (1);
   v_ACHTinNmb                      CHAR (10);
   v_LoanAppFirstDisbDt             DATE;
   v_LoanTotUndisbAmt               NUMBER (19, 4);
   v_LoanProcdSeqNmb                NUMBER (3, 0);
   v_ProcdTypCd                     CHAR (1);
   v_LoanProcdTypCd                 CHAR (2);
   v_LoanProcdOthTypTxt             VARCHAR2 (80);
   v_LoanProcdAmt                   NUMBER (19, 4);
   v_LoanProcdCreatUserId           CHAR (15);
   v_LoanProcdCreatDt               DATE;
   v_ProcdSeqNmb                    NUMBER (3, 0);
   v_LoanHistrySeqNmb               NUMBER (10, 0);
   v_LoanTransSeqNmb                NUMBER;
   v_LoanCancelFyNmb                NUMBER;
   v_RetStat                        NUMBER;
   v_temp                           NUMBER;
   v_temp1                          NUMBER;
   v_temp2                          NUMBER;
   v_LOANSTATCMNTCD                 CHAR (2);
   v_LOANGNTYDFRTODT                DATE;
   v_LOANGNTYDFRDMNTHSNMB           NUMBER (5);
   v_LOANGNTYDFRDGRSAMT             NUMBER (19, 4);
   v_LoanNextInstlmntDueDt          DATE;
   v_LoanPymntSchdlFreq             VARCHAR2 (24);
   v_LOANDISASTRAPPFEECHARGED       NUMBER (19, 4);
   v_LOANDISASTRAPPDCSN             CHAR (15);
   v_LOANASSOCDISASTRAPPNMB         CHAR (30);
   v_LOANASSOCDISASTRLOANNMB        CHAR (10);
   v_LoanAppMatExtDt                DATE;
   v_loanhighappvamt                NUMBER (15, 2);
   v_reinstateind                   CHAR (1);
   v_increaseind                    CHAR (1);
   v_Loan1201NotcInd                NUMBER (3, 0);
   v_LOANSERVGRPCD                  CHAR (1);
   v_FREEZETYPCD                    CHAR (1);
   v_FREEZERSNCD                    NUMBER (3, 0);
   v_FORGVNESFREEZETYPCD            NUMBER (3, 0);
   v_DisbDeadLnDt                   DATE;
   v_LOANPYMTTRANSCD                NUMBER (3);
   v_LOANDISASTRAPPNMB              CHAR (30);
   v_LOANCOFSEFFDT                  DATE;
   v_SBICLICNSNMB                   CHAR (8);
   v_LOANAPPNETEARNIND              CHAR (1);
   v_LOANPYMTINSTLMNTTYPCD          CHAR (1);
   v_LOANPYMNINSTLMNTFREQCD         CHAR (1);
   v_LOANTOTDEFNMB                  NUMBER (3, 0);
   v_LOANTOTDEFMONMB                NUMBER (3, 0);
   v_LoanAppNm                      VARCHAR2 (80);
   v_LOANOUTBALAMT                  NUMBER (19, 4);
   v_LOANOUTBALDT                   DATE;
   v_EIDLAmt                        NUMBER (15, 2);
   v_PHYAmt                         NUMBER (15, 2);
   v_PrgmCd                         CHAR (1);
   v_LOANGNTYNOTEDT                 DATE;
   v_LOANGNTYMATSTIND               VARCHAR2 (1);
   v_LOANPYMTREPAYINSTLTYPCD        CHAR (1);
   v_LOANAPPINTDTLCD                VARCHAR2 (2);
   v_LOANLNDRASSGNLTRDT             DATE;
   v_LoanGntyNoteIntRt              NUMBER (6, 3);
   v_LoanGntyNoteMntlyPymtAmt       NUMBER (19, 4);
   v_LoanAppRevlMoIntQty            NUMBER;
   v_LoanAppAmortMoIntQty           NUMBER;
   v_LoanAppMonthPayroll            NUMBER;
   v_LoanGntyPayRollChngInd         CHAR;
/*
---05/04/2010 APK added new variable v_LOANSTATCMNTCD, and this is retrieved and passed to the procedure LOANSRVSFINUPDCSP.
RY--01/24/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty,LoanAppAmortMoIntQty
*/
BEGIN
   IF p_LoanAppNm IS NULL
   THEN
      SELECT LoanAppNm
        INTO v_LoanAppNm
        FROM loangntytbl a
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   ELSE
      v_LoanAppNm := p_LoanAppNm;
   END IF;

   BEGIN
      SELECT LoanStatCd, LoanCurrAppvAmt, PrgmCd
        INTO v_LoanStatCd_old, v_LoanCurrAppvAmt_old, v_PrgmCd
        FROM LoanGntyTbl a
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   BEGIN
      SELECT LoanAppSBAGntyPct,
             LoanCurrAppvAmt,
             LoanLastAppvDt,
             LoanAppRqstMatMoQty,
             LoanAppFundDt,
             LoanAppFundDt,
             LoanStatCd,
             LoanStatDt,
             a.LastUpdtUserId,                      --last update to pre table
             CohortCd,
             LoanAppCDCGntyAmt,
             LoanAppCDCNetDbentrAmt,
             LoanAppCDCFundFeeAmt,
             LoanAppCDCSeprtPrcsFeeInd,
             LoanAppCDCPrcsFeeAmt,
             LoanAppCDCClsCostAmt,
             LoanAppCDCOthClsCostAmt,
             LoanAppCDCUndrwtrFeeAmt,
             LoanAppContribPct,
             LoanAppContribAmt,
             LoanAppMatDt,
             LoanCurrAppvAmt - v_LoanCurrAppvAmt_old,
               EXTRACT (YEAR FROM LoanLastAppvDt)
             + CASE
                  WHEN EXTRACT (MONTH FROM LoanLastAppvDt) > 9 THEN 1
                  ELSE 0
               END,
             LoanTotUndisbAmt,
             LoanAppPymtAmt,
             LoanAppInjctnInd,
             LoanCollatInd,
             LoanAppDisasterCntrlNmb,
             LoanAppNetExprtAmt,
             LoanAppMoIntQty,
             LoanAppOutPrgrmAreaOfOperInd,
             LoanAppParntLoanNmb,
             ACHRtngNmb,
             ACHAcctNmb,
             ACHAcctTypCd,
             ACHTinNmb,
             LoanAppFirstDisbDt,
             a.LOANSTATCMNTCD,
             a.LOANGNTYDFRTODT,
             a.LOANGNTYDFRDMNTHSNMB,
             a.LOANGNTYDFRDGRSAMT,
             LoanNextInstlmntDueDt,
             LoanPymntSchdlFreq,
             LOANDISASTRAPPFEECHARGED,
             LOANDISASTRAPPDCSN,
             LOANASSOCDISASTRAPPNMB,
             LOANASSOCDISASTRLOANNMB,
             LoanAppMatExtDt,
             Loan1201NotcInd,
             LOANSERVGRPCD,
             FREEZETYPCD,
             FREEZERSNCD,
             FORGVNESFREEZETYPCD,
             DisbDeadLnDt,
             LOANPYMTTRANSCD,
             LOANDISASTRAPPNMB,
             LOANCOFSEFFDT,
             SBICLICNSNMB,
             LOANAPPNETEARNIND,
             LOANPYMTINSTLMNTTYPCD,
             LOANPYMNINSTLMNTFREQCD,
             LOANTOTDEFNMB,
             LOANTOTDEFMONMB,
             LOANOUTBALAMT,
             LOANOUTBALDT,
             LOANGNTYNOTEDT,
             LOANGNTYMATSTIND,
             LOANPYMTREPAYINSTLTYPCD,
             LOANAPPINTDTLCD,
             LOANLNDRASSGNLTRDT,
             LoanGntyNoteIntRt,
             LoanGntyNoteMntlyPymtAmt,
             LoanAppRevlMoIntQty,
             LoanAppAmortMoIntQty,
             LoanAppMonthPayroll,
             LoanGntyPayRollChngInd
        INTO v_LoanAppSBAGntyPct,
             v_LoanCurrAppvAmt,
             v_LoanLastAppvDt,
             v_LoanAppRqstMatMoQty,
             v_LoanAppRecvDt,
             v_LoanAppFundDt,
             v_LoanStatCd,
             v_LoanStatDt,
             v_LastUpdtUserId,
             v_CohortCd,
             v_LoanAppCDCGntyAmt,
             v_LoanAppCDCNetDbentrAmt,
             v_LoanAppCDCFundFeeAmt,
             v_LoanAppCDCSeprtPrcsFeeInd,
             v_LoanAppCDCPrcsFeeAmt,
             v_LoanAppCDCClsCostAmt,
             v_LoanAppCDCOthClsCostAmt,
             v_LoanAppCDCUndrwtrFeeAmt,
             v_LoanAppContribPct,
             v_LoanAppContribAmt,
             v_LoanAppMatDt,
             v_FyLoanTransChngLoanAmt,
             v_LoanTransFisclYrNmb,
             v_LoanTotUndisbAmt,
             v_LoanAppPymtAmt,
             v_LoanAppInjctnInd,
             v_LoanCollatInd,
             v_LoanAppDisasterCntrlNmb,
             v_LoanAppNetExprtAmt,
             v_LoanAppMoIntQty,
             v_LoanAppOutPrgrmAreaOfOperInd,
             v_LoanAppParntLoanNmb,
             v_ACHRtngNmb,
             v_ACHAcctNmb,
             v_ACHAcctTypCd,
             v_ACHTinNmb,
             v_LoanAppFirstDisbDt,
             v_LOANSTATCMNTCD,
             v_LOANGNTYDFRTODT,
             v_LOANGNTYDFRDMNTHSNMB,
             v_LOANGNTYDFRDGRSAMT,
             v_LoanNextInstlmntDueDt,
             v_LoanPymntSchdlFreq,
             v_LOANDISASTRAPPFEECHARGED,
             v_LOANDISASTRAPPDCSN,
             v_LOANASSOCDISASTRAPPNMB,
             v_LOANASSOCDISASTRLOANNMB,
             v_LoanAppMatExtDt,
             v_Loan1201NotcInd,
             v_LOANSERVGRPCD,
             v_FREEZETYPCD,
             v_FREEZERSNCD,
             v_FORGVNESFREEZETYPCD,
             v_DisbDeadLnDt,
             v_LOANPYMTTRANSCD,
             v_LOANDISASTRAPPNMB,
             v_LOANCOFSEFFDT,
             v_SBICLICNSNMB,
             v_LOANAPPNETEARNIND,
             v_LOANPYMTINSTLMNTTYPCD,
             v_LOANPYMNINSTLMNTFREQCD,
             v_LOANTOTDEFNMB,
             v_LOANTOTDEFMONMB,
             v_LOANOUTBALAMT,
             v_LOANOUTBALDT,
             v_LOANGNTYNOTEDT,
             v_LOANGNTYMATSTIND,
             v_LOANPYMTREPAYINSTLTYPCD,
             v_LOANAPPINTDTLCD,
             v_LOANLNDRASSGNLTRDT,
             v_LoanGntyNoteIntRt,
             v_LoanGntyNoteMntlyPymtAmt,
             v_LoanAppRevlMoIntQty,
             v_LoanAppAmortMoIntQty,
             v_LoanAppMonthPayroll,
             v_LoanGntyPayRollChngInd
        --from LoanGntyPreTbl where LoanAppNmb = v_LoanAppNmb
        FROM LoanGntyPreTbl a
             LEFT OUTER JOIN LoanGntyProjTbl b
                ON (a.LoanAppNmb = b.LoanAppNmb)
       WHERE a.LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN;
   END;

   Loan.LoanSrvsFinUpdCSP (p_LoanAppNmb,
                           v_LoanStatCd,
                           v_LoanCurrAppvAmt,
                           v_LoanAppSBAGntyPct,
                           v_LoanAppRqstMatMoQty,
                           v_LoanAppAdjPrdCd,
                           v_LoanAppPymtAmt,
                           v_LoanAppInjctnInd,
                           v_LoanCollatInd,
                           v_LoanAppDisasterCntrlNmb,
                           v_LoanAppNetExprtAmt,
                           v_LoanAppCDCGntyAmt,
                           v_LoanAppCDCNetDbentrAmt,
                           v_LoanAppCDCFundFeeAmt,
                           v_LoanAppCDCSeprtPrcsFeeInd,
                           v_LoanAppCDCPrcsFeeAmt,
                           v_LoanAppCDCClsCostAmt,
                           v_LoanAppCDCOthClsCostAmt,
                           v_LoanAppCDCUndrwtrFeeAmt,
                           v_LoanAppContribPct,
                           v_LoanAppContribAmt,
                           v_LoanAppMoIntQty,
                           v_LoanAppMatDt,
                           v_LastUpdtUserId,
                           v_LoanAppOutPrgrmAreaOfOperInd,
                           v_LoanAppParntLoanNmb,
                           v_ACHRtngNmb,
                           v_ACHAcctNmb,
                           v_ACHAcctTypCd,
                           v_ACHTinNmb,
                           v_LoanAppFirstDisbDt,
                           v_LoanTotUndisbAmt,
                           v_LOANSTATCMNTCD,
                           v_LOANGNTYDFRTODT,
                           v_LOANGNTYDFRDMNTHSNMB,
                           v_LOANGNTYDFRDGRSAMT,
                           v_LOANDISASTRAPPFEECHARGED,
                           v_LOANDISASTRAPPDCSN,
                           v_LOANASSOCDISASTRAPPNMB,
                           v_LOANASSOCDISASTRLOANNMB,
                           v_LoanNextInstlmntDueDt,
                           v_LoanPymntSchdlFreq,
                           v_LoanAppMatExtDt,
                           v_Loan1201NotcInd,
                           v_LOANSERVGRPCD,
                           v_FREEZETYPCD,
                           v_FREEZERSNCD,
                           v_FORGVNESFREEZETYPCD,
                           v_DisbDeadLnDt,
                           v_LOANPYMTTRANSCD,
                           v_LOANDISASTRAPPNMB,
                           v_LOANCOFSEFFDT,
                           v_SBICLICNSNMB,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           v_LOANAPPNETEARNIND,
                           v_LOANPYMTINSTLMNTTYPCD,
                           v_LOANPYMNINSTLMNTFREQCD,
                           v_LOANTOTDEFNMB,
                           v_LOANTOTDEFMONMB,
                           v_LoanAppNm,
                           v_LOANOUTBALAMT,
                           v_LOANOUTBALDT,
                           v_LOANGNTYNOTEDT,
                           v_LOANGNTYMATSTIND,
                           v_LOANPYMTREPAYINSTLTYPCD,
                           v_LOANAPPINTDTLCD,
                           v_LOANLNDRASSGNLTRDT,
                           v_LoanGntyNoteIntRt,
                           v_LoanGntyNoteMntlyPymtAmt,
                           v_LoanAppRevlMoIntQty,
                           v_LoanAppAmortMoIntQty,
                           v_LoanAppMonthPayroll,
                           v_LoanGntyPayRollChngInd);


   SELECT COUNT (*)
     INTO v_temp1
     FROM LoanGntyPreProcdTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF v_temp1 > 0
   THEN                                        --or v_LoanCurrAppvAmt = 0 then
      BEGIN
         --create history for proceed update
         Loan.LoanGntyProcdUpdCSP (p_LoanAppNmb, v_LastUpdtUserId);

         DELETE LoanGntyProcdTbl
          WHERE LoanAppNmb = p_LoanAppNmb;

         INSERT INTO LoanGntyProcdTbl (LoanProcdSeqNmb,
                                       LoanAppNmb,
                                       ProcdTypCd,
                                       LoanProcdTypCd,
                                       LoanProcdOthTypTxt,
                                       LoanProcdAmt,
                                       LoanProcdCreatUserId,
                                       LoanProcdCreatDt,
                                       LoanProcdRefDescTxt,
                                       LoanProcdPurAgrmtDt,
                                       LoanProcdPurIntangAssetAmt,
                                       LoanProcdPurIntangAssetDesc,
                                       LoanProcdPurStkHldrNm,
                                       NCAIncldInd,
                                       StkPurCorpText)
            SELECT LoanProcdSeqNmb,
                   LoanAppNmb,
                   ProcdTypCd,
                   LoanProcdTypCd,
                   LoanProcdOthTypTxt,
                   LoanProcdAmt,
                   LoanProcdCreatUserId,
                   LoanProcdCreatDt,
                   LoanProcdRefDescTxt,
                   LoanProcdPurAgrmtDt,
                   LoanProcdPurIntangAssetAmt,
                   LoanProcdPurIntangAssetDesc,
                   LoanProcdPurStkHldrNm,
                   NCAIncldInd,
                   StkPurCorpText
              FROM LoanGntyPreProcdTbl
             WHERE LoanAppNmb = p_LoanAppNmb;

         DELETE LoanGntyPreProcdTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      END;
   END IF;

   DELETE LoanGntyPreTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   SELECT PrgmCd, LoanStatCd
     INTO v_PrgmCd, v_LoanStatCd
     FROM loan.loangntytbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF v_PrgmCd = 'H'
   THEN
      SELECT SUM (
                CASE WHEN LOANPROCDTYPCD != '04' THEN LOANPROCDAMT ELSE 0 END),
             SUM (
                CASE WHEN LOANPROCDTYPCD = '04' THEN LOANPROCDAMT ELSE 0 END)
        INTO v_PhyAmt, v_EIDLAmt
        FROM loan.loangntyprocdtbl
       WHERE LoanAppNmb = p_LoanAppNmb;

      UPDATE loan.loangntytbl
         SET LoanGntyEIDLAmt =
                CASE
                   WHEN LoanGntyEIDLAmt > v_EIDLAmt THEN LoanGntyEIDLAmt
                   ELSE v_EIDLAmt
                END,
             LoanGntyEIDLCnclAmt =
                  CASE
                     WHEN LoanGntyEIDLAmt > v_EIDLAmt THEN LoanGntyEIDLAmt
                     ELSE v_EIDLAmt
                  END
                - CASE WHEN v_LoanStatCd = 4 THEN 0 ELSE v_EIDLAmt END,
             LoanGntyPhyAmt =
                CASE
                   WHEN LoanGntyPhyAmt > v_PhyAmt THEN LoanGntyPhyAmt
                   ELSE v_PhyAmt
                END,
             LoanGntyPhyCnclAmt =
                  CASE
                     WHEN LoanGntyPhyAmt > v_PhyAmt THEN LoanGntyPhyAmt
                     ELSE v_PhyAmt
                  END
                - CASE WHEN v_LoanStatCd = 4 THEN 0 ELSE v_PhyAmt END,
             LastUpdtUserId = v_LastUpdtUserId,
             LastUpdtDt = SYSDATE
       WHERE LoanAppNmb = p_LoanAppNmb;
   END IF;

   LoanChngLogInsCSP (p_LoanAppNmb,
                      'S',
                      v_LastUpdtUserId,
                      NULL,
                      NULL);
END;
/
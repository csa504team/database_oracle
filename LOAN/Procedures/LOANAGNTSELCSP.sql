CREATE OR REPLACE procedure LOAN.LOANAGNTSELCSP
(p_Identifier 	IN NUMBER  := 0,
p_LocId in NUMBER :=0,
p_StartDt	IN DATE := null,
p_EndDt	IN DATE := null,
p_LoanNmb IN CHAR := null,
p_AgntNm IN VARCHAR2 := null,
p_RetVal  	OUT NUMBER,
p_SelCur 	OUT SYS_REFCURSOR)
as
begin
  if(p_Identifier =0) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM)) as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission


       FROM
       LOAN.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOAN.LOANGNTYBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOAN.BUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B'
       left join LOAN.PERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P'
       left join LOAN.LOANGNTYTBL l on l.LOANAPPNMB = a.LOANAPPNMB
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       (l.LOCID =  p_LocId or p_LocId is null) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null) and
        ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null))
       and rownum<=10001
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    elsif (p_Identifier=1) then
      open p_SelCur for
        SELECT
            l.LOANNMB as SBA_Loan_No
           ,lp.LOANAPPFIRSNMB as SBA_Lender_FIRS
           ,lp.LOCID
           ,sa.LOANAGNTTYPDESCTXT as Agent_Type_Desc
           ,case when b.BORRBUSPERIND = 'B' then
            bb.BUSNM
            when b.BORRBUSPERIND = 'P' then
            UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(bp.PERLASTNM))
            end as Applicant
           ,(select sum(nvl(f.LOANAGNTAPPCNTPAIDAMT,0) + nvl(f.LOANAGNTSBALENDRPAIDAMT,0))
            from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Total_compensation
            , UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTFIRSTNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTMIDNM))||' '||UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTCNTCTLASTNM ))as AGNT_CONTACT_PERSON
            ,a.LOANAGNTNM as AGNTNM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR1NM)) as LOANAGNTADDRSTR1NM
            ,UTL_RAW.CAST_TO_VARCHAR2(rawtohex(a.LOANAGNTADDRSTR2NM)) as LOANAGNTADDRSTR2NM
            ,a.LOANAGNTADDRCTYNM as LOANAGNTADDRCTYNM
            ,a.LOANAGNTADDRSTCD as LOANAGNTADDRSTCD
            ,a.LOANAGNTADDRZIPCD as LOANAGNTADDRZIPCD
            ,lp.LOANAPPPRTNM as LNDRNM
           ,(select sum(f.LOANAGNTAPPCNTPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Applicant_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0) as Applicant_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTAPPCNTPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Applicant_Other_type_Service_description
           ,(select sum(f.LOANAGNTSBALENDRPAIDAMT)  from LOAN.LOANAGNTFEEDTLTBL f where a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB) as Lender_total_compensation
           ,(select listagg(LOANAGNTSERVTYPDESCTXT,', ') within group(order by LOANAGNTSERVTYPDESCTXT)  from
            LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s  where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0) as Lender_Services
           ,(select listagg(nvl2(f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||f.LOANAGNTSERVOTHTYPTXT,decode(rownum,1,' ',' ,')||'No description for Other type of Service'))
             within group(order by rownum)
             from
             LOAN.LOANAGNTFEEDTLTBL f, SBAREF.LOANAGNTSERVTYPCDTBL s where f.LOANAGNTSERVTYPCD=s.LOANAGNTSERVTYPCD and a.LOANAPPNMB = f.LOANAPPNMB and a.LOANAGNTSEQNMB=f.LOANAGNTSEQNMB
            and f.LOANAGNTSBALENDRPAIDAMT>0 and s.LOANAGNTSERVTYPCD =5 ) as Lender_Other_type_Service_description
           ,a.LASTUPDTDT as Date_of_Submission

       FROM
       LOAN.LOANAGNTTBL a
       left join SBAREF.LOANAGNTTYPCDTBL sa on a.LOANAGNTTYPCD = sa.LOANAGNTTYPCD
       left join LOAN.LOANGNTYBORRTBL b on a.LOANAPPNMB = b.LOANAPPNMB and b.LOANBUSPRIMBORRIND='Y'
       left join LOAN.BUSTBL bb on b.TAXID = bb.TAXID  AND  b.BORRBUSPERIND = 'B'
       left join LOAN.PERTBL bp on b.TAXID = bp.TAXID  AND  b.BORRBUSPERIND = 'P'
       left join LOAN.LOANGNTYTBL  l on l.LOANAPPNMB = a.LOANAPPNMB
       left join LOANAPP.LOANAPPPRTTBL lp on lp.LOANAPPNMB = a.LOANAPPNMB
       where
       l.LOCID=p_LocId and
       ((trunc(a.LASTUPDTDT) >=  p_StartDt or p_StartDt is null) and (trunc(a.LASTUPDTDT) <= p_EndDt or p_EndDt is null)) and
       (l.LOANNMB= p_LoanNmb or p_LoanNmb is null) and
       (a.LOANAGNTNM = p_AgntNm or p_AgntNm is null)
       and rownum<=10001
       order by locid,loannmb;
       p_RetVal := SQL%ROWCOUNT;
    end if;
end;
/





GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVCSAUPDATE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVPRTRECV;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANSERVSBAAGENT;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOAN.LOANAGNTSELCSP TO UPDLOANROLE;

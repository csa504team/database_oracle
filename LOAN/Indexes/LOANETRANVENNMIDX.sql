DROP INDEX LOAN.LOANETRANVENNMIDX;

CREATE INDEX LOAN.LOANETRANVENNMIDX ON LOAN.LOANETRANTBL
(LOANAPPETRANVENNM)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

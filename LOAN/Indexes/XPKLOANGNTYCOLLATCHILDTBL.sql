DROP INDEX LOAN.XPKLOANGNTYCOLLATCHILDTBL;

CREATE UNIQUE INDEX LOAN.XPKLOANGNTYCOLLATCHILDTBL ON LOAN.LOANGNTYCOLLATCHILDTBL
(LOANCOLLATCHILDSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

DROP INDEX LOAN.XPKLAAC1_PRIN_NC_EVD_SEQNMB;

CREATE UNIQUE INDEX LOAN.XPKLAAC1_PRIN_NC_EVD_SEQNMB ON LOAN.LAAC1_PRIN_NONCOMP_EVD_TBL
(LAAC1_OCD_ID, LAAC1_PRIN_NC_EVD_SEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

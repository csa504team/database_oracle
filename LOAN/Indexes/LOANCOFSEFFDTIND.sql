DROP INDEX LOAN.LOANCOFSEFFDTIND;

CREATE INDEX LOAN.LOANCOFSEFFDTIND ON LOAN.LOANGNTYTBL
(LOANCOFSEFFDT)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

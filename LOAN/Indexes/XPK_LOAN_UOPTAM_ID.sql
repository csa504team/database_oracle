DROP INDEX LOAN.XPK_LOAN_UOPTAM_ID;

CREATE UNIQUE INDEX LOAN.XPK_LOAN_UOPTAM_ID ON LOAN.LOAN_UOPTAM_TBL
(LOAN_UOPTAM_ID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

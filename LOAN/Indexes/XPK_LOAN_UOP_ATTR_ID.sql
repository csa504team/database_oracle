DROP INDEX LOAN.XPK_LOAN_UOP_ATTR_ID;

CREATE UNIQUE INDEX LOAN.XPK_LOAN_UOP_ATTR_ID ON LOAN.LOAN_UOP_ATTR_TBL
(LOAN_UOP_ATTR_ID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

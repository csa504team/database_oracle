DROP INDEX LOAN.XPKLAAC_DTL_ID;

CREATE UNIQUE INDEX LOAN.XPKLAAC_DTL_ID ON LOAN.LAAC_DTL_TBL
(LAAC_DTL_ID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

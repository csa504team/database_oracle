DROP INDEX LOAN.XPKLOANTRANSMFMAPTBL;

CREATE UNIQUE INDEX LOAN.XPKLOANTRANSMFMAPTBL ON LOAN.LOANTRANSMFMAPTBL
(LOANTRANSTYPCD, LOANMFUPLOADFORMATSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

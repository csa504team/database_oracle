DROP INDEX LOAN.UPDTLOGLOANAPPNMBIDX;

CREATE INDEX LOAN.UPDTLOGLOANAPPNMBIDX ON LOAN.LOANUPDTLOGTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

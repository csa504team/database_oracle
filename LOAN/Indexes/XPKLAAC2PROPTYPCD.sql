DROP INDEX LOAN.XPKLAAC2PROPTYPCD;

CREATE UNIQUE INDEX LOAN.XPKLAAC2PROPTYPCD ON LOAN.LAAC2_PROPTYP_CDTBL
(LAAC2_PROPTYP_CD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

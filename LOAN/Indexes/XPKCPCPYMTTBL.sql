DROP INDEX LOAN.XPKCPCPYMTTBL;

CREATE UNIQUE INDEX LOAN.XPKCPCPYMTTBL ON LOAN.CPCPYMTTBL
(CPCPYMTID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

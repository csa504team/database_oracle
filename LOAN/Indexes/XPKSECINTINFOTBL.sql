DROP INDEX LOAN.XPKSECINTINFOTBL;

CREATE UNIQUE INDEX LOAN.XPKSECINTINFOTBL ON LOAN.LAC_SEC_INT_INFO_TBL
(LOAN_AUTH_ID, LOAN_COLLAT_SEQ_NMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

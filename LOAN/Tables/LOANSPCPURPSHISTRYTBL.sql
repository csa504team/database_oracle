ALTER TABLE LOAN.LOANSPCPURPSHISTRYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANSPCPURPSHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANSPCPURPSHISTRYTBL
(
  LOANHISTRYSEQNMB               NUMBER(10)     NOT NULL,
  LOANSPCPURPSSEQNMB             NUMBER(3)      NOT NULL,
  SPCPURPSLOANCD                 CHAR(4 BYTE)   NOT NULL,
  LOANSPCPURPSHISTRYCREATUSERID  VARCHAR2(32 BYTE) DEFAULT USER NOT NULL,
  LOANSPCPURPSHISTRYCREATDT      DATE           DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID                 VARCHAR2(32 BYTE) DEFAULT USER NOT NULL,
  LASTUPDTDT                     DATE           DEFAULT SYSDATE               NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANSPCPURPSHISTRYTBL ON LOAN.LOANSPCPURPSHISTRYTBL
(LOANHISTRYSEQNMB, LOANSPCPURPSSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANSPCPURPSHISTRYTBL ADD (
  CONSTRAINT XPKLOANSPCPURPSHISTRYTBL
  PRIMARY KEY
  (LOANHISTRYSEQNMB, LOANSPCPURPSSEQNMB)
  USING INDEX LOAN.XPKLOANSPCPURPSHISTRYTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANSPCPURPSHISTRYTBL ADD (
  CONSTRAINT LOANHISTRYHASSPCPURPSIFK 
  FOREIGN KEY (LOANHISTRYSEQNMB) 
  REFERENCES LOAN.LOANHISTRYTBL (LOANHISTRYSEQNMB)
  DISABLE NOVALIDATE);

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANSPCPURPSHISTRYTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANSPCPURPSHISTRYTBL TO UPDLOANROLE;

ALTER TABLE LOAN.LQDTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LQDTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LQDTBL
(
  LOANAPPNMB             NUMBER(10)             NOT NULL,
  LQDSEQNMB              NUMBER(10)             NOT NULL,
  LQDNM                  VARCHAR2(80 BYTE),
  BOROWRNM               VARCHAR2(42 BYTE),
  LQDINDT                DATE,
  LQDOUTDT               DATE,
  LITINDT                DATE,
  LITOUTDT               DATE,
  LQDSTATCD              CHAR(2 BYTE),
  LQDJUSTFCTNCD          CHAR(2 BYTE),
  LQDJUSTFCTNOTHDESCTXT  VARCHAR2(255 BYTE),
  REFTO                  VARCHAR2(80 BYTE),
  PURDT                  DATE,
  LOANSTATCMNT           CHAR(2 BYTE),
  ORIGESTNETREC          NUMBER(19,4),
  PARTCPNTBNKNM          VARCHAR2(43 BYTE),
  AMTCLCTD               NUMBER(19,4),
  LOANAMT                NUMBER(19,4),
  ESTPRINBAL             NUMBER(19,4),
  BOROWRLOANAPPNMB       NUMBER(3),
  INTINTOLQD             NUMBER(19,4),
  PRININTOLQD            NUMBER(19,4),
  LITOFCL                VARCHAR2(80 BYTE),
  PROTCTVBID             NUMBER(19,4),
  LQDCNTCTFIRSTNM        RAW(128),
  LQDCNTCTLASTNM         RAW(128),
  LQDCNTCTMIDNM          VARCHAR2(128 BYTE),
  LQDPHNAREACD           VARCHAR2(5 BYTE),
  LQDPHNLCLNMB           RAW(128),
  LQDSTR1ATXT            VARCHAR2(80 BYTE),
  LQDSTR2TXT             VARCHAR2(80 BYTE),
  LQDCTYNM               VARCHAR2(60 BYTE),
  STCD                   CHAR(2 BYTE),
  ZIP5CD                 CHAR(5 BYTE),
  LQDUPDTID              VARCHAR2(32 BYTE),
  ASSETSALESTAT          CHAR(15 BYTE),
  GTYRPRRSN              VARCHAR2(2 BYTE),
  CREATUSERID            VARCHAR2(32 BYTE)      NOT NULL,
  CREATDT                DATE                   NOT NULL,
  LASTUPDTUSERID         VARCHAR2(32 BYTE)      NOT NULL,
  LASTUPDTDT             DATE                   NOT NULL,
  LOANTASKSEQNMB         NUMBER(10),
  MFSEQNMB               NUMBER(2),
  LOANCOFSEFFDT          DATE,
  LOANLNDRASSGNLTRDT     DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLQDTBL ON LOAN.LQDTBL
(LOANAPPNMB, LQDSEQNMB)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.LQDUPDINSTRIG
AFTER UPDATE OR INSERT
ON LOAN.LQDTBL
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
BEGIN
    IF :NEW.LqdStatCd = 12 AND NVL(:OLD.LqdStatCd,0) != 12 THEN
        UPDATE loan.loangntytbl set LoanpymtTransCd = 385, LastUpdtUserId = :NEW.LastUpdtUserId, LastUpdtDt = SYSDATE
        WHERE loanappnmb = :NEW.loanappnmb;
        
        loan.loanchngloginscsp(:NEW.LoanAppNmb,'S',:NEW.LastUpdtUserId,null,null);
    END IF;
END;
/


CREATE OR REPLACE TRIGGER LOAN.LQD_HIST_TRIG
    AFTER UPDATE
    ON LOAN.lqdTbl
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_header    VARCHAR2 (2000);
    v_log_txt   CLOB;
    v_loannmb   VARCHAR2 (200);
/***********************************************************
 NAME:       lqd_HIST_TRIG
 PURPOSE: To generate history for LOAN.lqdTbl
 1.0        Aug 2018      JPaul
 --RY--11/16/2018--OPSMDEV-2044--Added OcaDataOut for PII columns.
 JP - 04/10/2019 -- CAFSOPER-2586 OCADATAOUT used for :New and :Old values of raw columns
                    History of RAW columns will be stored as string 
******************************************************************************/

BEGIN
    FOR rec IN (SELECT MAX (g.LOANNMB)     loannmb
                  FROM LOAN.LOANGNTYTBL g
                 WHERE g.LOANAPPNMB = :OLD.LOANAPPNMB)
    LOOP
        v_loannmb   := rec.loannmb;
    END LOOP;

    v_header    := '<History><LoanNmb>' || v_loannmb || '</LoanNmb>';
    v_log_txt   := v_header;

    BEGIN
        IF NVL (TO_CHAR (:OLD.lqdnm), 'x') != NVL (TO_CHAR (:NEW.lqdnm), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdnm', :OLD.lqdnm, :NEW.lqdnm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.borowrnm), 'x') != NVL (TO_CHAR (:NEW.borowrnm), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('Borrower Name', :OLD.borowrnm, :NEW.borowrnm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdindt), 'x') != NVL (TO_CHAR (:NEW.lqdindt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdindt', :OLD.lqdindt, :NEW.lqdindt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdoutdt), 'x') != NVL (TO_CHAR (:NEW.lqdoutdt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdoutdt', :OLD.lqdoutdt, :NEW.lqdoutdt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.litindt), 'x') != NVL (TO_CHAR (:NEW.litindt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('litindt', :OLD.litindt, :NEW.litindt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.litoutdt), 'x') != NVL (TO_CHAR (:NEW.litoutdt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('litoutdt', :OLD.litoutdt, :NEW.litoutdt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdstatcd), 'x') != NVL (TO_CHAR (:NEW.lqdstatcd), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdstatcd', :OLD.lqdstatcd, :NEW.lqdstatcd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdjustfctncd), 'x') != NVL (TO_CHAR (:NEW.lqdjustfctncd), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdjustfctncd', :OLD.lqdjustfctncd, :NEW.lqdjustfctncd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdjustfctnothdesctxt), 'x') != NVL (TO_CHAR (:NEW.lqdjustfctnothdesctxt), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdjustfctnothdesctxt',
                                      :OLD.lqdjustfctnothdesctxt,
                                      :NEW.lqdjustfctnothdesctxt,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.refto), 'x') != NVL (TO_CHAR (:NEW.refto), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('refto', :OLD.refto, :NEW.refto, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.purdt), 'x') != NVL (TO_CHAR (:NEW.purdt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('purdt', :OLD.purdt, :NEW.purdt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.loanstatcmnt), 'x') != NVL (TO_CHAR (:NEW.loanstatcmnt), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('loanstatcmnt', :OLD.loanstatcmnt, :NEW.loanstatcmnt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.origestnetrec), 'x') != NVL (TO_CHAR (:NEW.origestnetrec), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('origestnetrec', :OLD.origestnetrec, :NEW.origestnetrec, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.partcpntbnknm), 'x') != NVL (TO_CHAR (:NEW.partcpntbnknm), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('partcpntbnknm', :OLD.partcpntbnknm, :NEW.partcpntbnknm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.amtclctd), 'x') != NVL (TO_CHAR (:NEW.amtclctd), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('amtclctd', :OLD.amtclctd, :NEW.amtclctd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.loanamt), 'x') != NVL (TO_CHAR (:NEW.loanamt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('loanamt', :OLD.loanamt, :NEW.loanamt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.estprinbal), 'x') != NVL (TO_CHAR (:NEW.estprinbal), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('estprinbal', :OLD.estprinbal, :NEW.estprinbal, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.borowrloanappnmb), 'x') != NVL (TO_CHAR (:NEW.borowrloanappnmb), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('borowrloanappnmb',
                                      :OLD.borowrloanappnmb,
                                      :NEW.borowrloanappnmb,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.intintolqd), 'x') != NVL (TO_CHAR (:NEW.intintolqd), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('intintolqd', :OLD.intintolqd, :NEW.intintolqd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.prinintolqd), 'x') != NVL (TO_CHAR (:NEW.prinintolqd), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('prinintolqd', :OLD.prinintolqd, :NEW.prinintolqd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.litofcl), 'x') != NVL (TO_CHAR (:NEW.litofcl), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('litofcl', :OLD.litofcl, :NEW.litofcl, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.protctvbid), 'x') != NVL (TO_CHAR (:NEW.protctvbid), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('protctvbid', :OLD.protctvbid, :NEW.protctvbid, v_log_txt);
        END IF;

        IF NVL (OcaDataOut (:OLD.lqdcntctfirstnm), 'x') != NVL (OcaDataOut (:NEW.lqdcntctfirstnm), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdcntctfirstnm',
                                      OcaDataOut (:OLD.lqdcntctfirstnm),
                                      OcaDataOut (:NEW.lqdcntctfirstnm),
                                      v_log_txt);
        END IF;

        IF NVL (OcaDataOut (:OLD.lqdcntctlastnm), 'x') != NVL (OcaDataOut (:NEW.lqdcntctlastnm), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdcntctlastnm',
                                      OcaDataOut (:OLD.lqdcntctlastnm),
                                      OcaDataOut (:NEW.lqdcntctlastnm),
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdcntctmidnm), 'x') != NVL (TO_CHAR (:NEW.lqdcntctmidnm), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdcntctmidnm', :OLD.lqdcntctmidnm, :NEW.lqdcntctmidnm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdphnareacd), 'x') != NVL (TO_CHAR (:NEW.lqdphnareacd), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdphnareacd', :OLD.lqdphnareacd, :NEW.lqdphnareacd, v_log_txt);
        END IF;

        IF NVL (OcaDataOut (:OLD.lqdphnlclnmb), 'x') != NVL (OcaDataOut (:NEW.lqdphnlclnmb), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdphnlclnmb',
                                      OcaDataOut (:OLD.lqdphnlclnmb),
                                      OcaDataOut (:NEW.lqdphnlclnmb),
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdstr1atxt), 'x') != NVL (TO_CHAR (:NEW.lqdstr1atxt), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('lqdstr1atxt', :OLD.lqdstr1atxt, :NEW.lqdstr1atxt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdstr2txt), 'x') != NVL (TO_CHAR (:NEW.lqdstr2txt), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdstr2txt', :OLD.lqdstr2txt, :NEW.lqdstr2txt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdctynm), 'x') != NVL (TO_CHAR (:NEW.lqdctynm), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdctynm', :OLD.lqdctynm, :NEW.lqdctynm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.stcd), 'x') != NVL (TO_CHAR (:NEW.stcd), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('stcd', :OLD.stcd, :NEW.stcd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.zip5cd), 'x') != NVL (TO_CHAR (:NEW.zip5cd), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('zip5cd', :OLD.zip5cd, :NEW.zip5cd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.lqdupdtid), 'x') != NVL (TO_CHAR (:NEW.lqdupdtid), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('lqdupdtid', :OLD.lqdupdtid, :NEW.lqdupdtid, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.assetsalestat), 'x') != NVL (TO_CHAR (:NEW.assetsalestat), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('assetsalestat', :OLD.assetsalestat, :NEW.assetsalestat, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.gtyrprrsn), 'x') != NVL (TO_CHAR (:NEW.gtyrprrsn), 'x') THEN
            v_log_txt   := LOAN.CONCAT_HIST_TXT ('gtyrprrsn', :OLD.gtyrprrsn, :NEW.gtyrprrsn, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.loantaskseqnmb), 'x') != NVL (TO_CHAR (:NEW.loantaskseqnmb), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('loantaskseqnmb', :OLD.loantaskseqnmb, :NEW.loantaskseqnmb, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANCOFSEFFDT), 'x') != NVL (TO_CHAR (:NEW.LOANCOFSEFFDT), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('LOANCOFSEFFDT', :OLD.LOANCOFSEFFDT, :NEW.LOANCOFSEFFDT, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLNDRASSGNLTRDT), 'x') != NVL (TO_CHAR (:NEW.LOANLNDRASSGNLTRDT), 'x') THEN
            v_log_txt   :=
                LOAN.CONCAT_HIST_TXT ('LOANLNDRASSGNLTRDT',
                                      :OLD.LOANLNDRASSGNLTRDT,
                                      :NEW.LOANLNDRASSGNLTRDT,
                                      v_log_txt);
        END IF;
    END;

    -- insert only if there is history
    IF v_log_txt != v_header THEN
        v_log_txt   := v_log_txt || '</History>';

        INSERT INTO LOAN.LoanAudtLogTbl (LOANAUDTLOGSEQNMB,
                                         LOANAPPNMB,
                                         LOANAUDTLOGTXT,
                                         LOANUPDTUSERID,
                                         LOANUPDTDT)
             VALUES ((SELECT MAX (LoanAudtLogSeqNmb) + 1 FROM loan.LoanAudtLogTbl),
                     :OLD.LOANAPPNMB,
                     v_log_txt,
                     NVL (:NEW.LASTUPDTUSERID, USER),
                     SYSDATE);
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Error code ' || SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 64));
        RAISE;
END lqd_HIST_TRIG;
/


ALTER TABLE LOAN.LQDTBL ADD (
  CONSTRAINT XPKLQDTBL
  PRIMARY KEY
  (LOANAPPNMB, LQDSEQNMB)
  USING INDEX LOAN.XPKLQDTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.LQDTBL ADD (
  CONSTRAINT LOANTASKSEQNMBFORLQD 
  FOREIGN KEY (LOANTASKSEQNMB) 
  REFERENCES LOAN.LOANTASKTBL (LOANTASKSEQNMB)
  ENABLE VALIDATE,
  CONSTRAINT LQDHASLOANAPPNMB 
  FOREIGN KEY (LOANAPPNMB) 
  REFERENCES LOAN.LOANGNTYTBL (LOANAPPNMB)
  ENABLE VALIDATE,
  CONSTRAINT LQDTBLHASLQDJUSTFCTNCDNFK 
  FOREIGN KEY (LQDJUSTFCTNCD) 
  REFERENCES SBAREF.LQDJUSTFCTNCDTBL (LQDJUSTFCTNCD)
  ENABLE VALIDATE,
  CONSTRAINT LQDTBLHASLQDSTATCDNFL 
  FOREIGN KEY (LQDSTATCD) 
  REFERENCES SBAREF.LQDSTATCDTBL (LQDSTATCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LQDTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LQDTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LQDTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LQDTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANACCT;

GRANT SELECT ON LOAN.LQDTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANAPP;

GRANT SELECT ON LOAN.LQDTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANCMNTREADALLROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANCMNTWRITEROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANLOOKUPUPDROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LQDTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LQDTBL TO LOANUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LQDTBL TO POOLSECADMIN;

GRANT SELECT ON LOAN.LQDTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LQDTBL TO READLOANROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LQDTBL TO STAGE;

GRANT SELECT ON LOAN.LQDTBL TO UPDLOANROLE;

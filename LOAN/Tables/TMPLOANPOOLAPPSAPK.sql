DROP TABLE LOAN.TMPLOANPOOLAPPSAPK CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPLOANPOOLAPPSAPK
(
  "Seq"                      VARCHAR2(10 BYTE),
  "ColsonCd"                 VARCHAR2(255 BYTE),
  "OriginalRowID"            VARCHAR2(255 BYTE),
  "ValidLoanInd"             VARCHAR2(320 BYTE),
  "EffectiveDt"              DATE,
  "Amount"                   VARCHAR2(320 BYTE),
  "LoanNmb"                  VARCHAR2(320 BYTE),
  "BorrowerNm"               VARCHAR2(255 BYTE),
  "Principal503Amount"       VARCHAR2(320 BYTE),
  "Interest503Amount"        VARCHAR2(320 BYTE),
  "Premium503Amount"         VARCHAR2(320 BYTE),
  "RemainingGuarantyAmount"  VARCHAR2(320 BYTE),
  "DebentureAmount"          VARCHAR2(320 BYTE),
  "GuarantyAmount"           VARCHAR2(320 BYTE),
  "MiscAmount"               VARCHAR2(320 BYTE),
  "ColsonCd2"                VARCHAR2(255 BYTE),
  "Amount3"                  VARCHAR2(320 BYTE),
  "PAmount"                  VARCHAR2(320 BYTE),
  "JVAmount"                 VARCHAR2(320 BYTE),
  "PCount"                   VARCHAR2(320 BYTE),
  "JVCount"                  VARCHAR2(320 BYTE),
  "ExternalDepositTicket"    VARCHAR2(320 BYTE),
  "InternalDepositTicket"    VARCHAR2(320 BYTE),
  "ExternalDepositTicketDt"  DATE,
  "TransactionCd"            VARCHAR2(320 BYTE),
  "BlockNmb"                 VARCHAR2(255 BYTE),
  "BlockDt"                  VARCHAR2(255 BYTE),
  "BlockType"                VARCHAR2(255 BYTE),
  "MatchDt"                  DATE,
  "PostDt"                   DATE,
  "MatchWireAudit"           VARCHAR2(320 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPLOANPOOLAPPSAPK TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPLOANPOOLAPPSAPK TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPLOANPOOLAPPSAPK TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPLOANPOOLAPPSAPK TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPLOANPOOLAPPSAPK TO LOANSERVLOANPRTUPLOAD;

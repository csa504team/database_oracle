ALTER TABLE LOAN.LOANAGNTTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANAGNTTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANAGNTTBL
(
  LOANAPPNMB            NUMBER(10),
  LOANAGNTSEQNMB        NUMBER(10),
  LOANAGNTBUSPERIND     CHAR(1 BYTE),
  LOANAGNTNM            VARCHAR2(200 BYTE),
  LOANAGNTCNTCTFIRSTNM  RAW(40),
  LOANAGNTCNTCTMIDNM    CHAR(1 BYTE),
  LOANAGNTCNTCTLASTNM   RAW(40),
  LOANAGNTCNTCTSFXNM    VARCHAR2(4 BYTE),
  LOANAGNTADDRSTR1NM    RAW(80),
  LOANAGNTADDRSTR2NM    RAW(80),
  LOANAGNTADDRCTYNM     VARCHAR2(40 BYTE),
  LOANAGNTADDRSTCD      CHAR(2 BYTE),
  LOANAGNTADDRSTNM      VARCHAR2(60 BYTE),
  LOANAGNTADDRZIPCD     CHAR(5 BYTE),
  LOANAGNTADDRZIP4CD    CHAR(4 BYTE),
  LOANAGNTADDRPOSTCD    VARCHAR2(20 BYTE),
  LOANAGNTADDRCNTCD     CHAR(2 BYTE),
  LOANAGNTTYPCD         NUMBER(3),
  LOANAGNTDOCUPLDIND    CHAR(1 BYTE),
  LOANCDCTPLFEEIND      CHAR(1 BYTE),
  LOANCDCTPLFEEAMT      NUMBER(15,2),
  CREATUSERID           VARCHAR2(32 BYTE),
  CREATDT               DATE,
  LASTUPDTUSERID        VARCHAR2(32 BYTE),
  LASTUPDTDT            DATE,
  LOANAGNTID            NUMBER(10)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANAGNTTBL ON LOAN.LOANAGNTTBL
(LOANAPPNMB, LOANAGNTSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANAGNTTBL ADD (
  CONSTRAINT XPKLOANAGNTTBL
  PRIMARY KEY
  (LOANAPPNMB, LOANAGNTSEQNMB)
  USING INDEX LOAN.XPKLOANAGNTTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANAGNTTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANAGNTTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANORIGHQOVERRIDE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANSERVSBICGOV;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANAGNTTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANAGNTTBL TO POOLSECADMINROLE;

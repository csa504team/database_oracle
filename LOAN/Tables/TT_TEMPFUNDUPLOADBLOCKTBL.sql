DROP TABLE LOAN.TT_TEMPFUNDUPLOADBLOCKTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TT_TEMPFUNDUPLOADBLOCKTBL
(
  LOANUPLOADBLOCK  CHAR(189 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO GPTS;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO LOANACCT;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TT_TEMPFUNDUPLOADBLOCKTBL TO LOANSERVLOANPRTUPLOAD;

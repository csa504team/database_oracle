ALTER TABLE LOAN.BUSTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.BUSTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.BUSTBL
(
  TAXID                   CHAR(10 BYTE)         NOT NULL,
  BUSEINCERTIND           CHAR(1 BYTE),
  BUSTYPCD                NUMBER(3),
  BUSNM                   VARCHAR2(80 CHAR)     NOT NULL,
  BUSSRCHNM               VARCHAR2(80 CHAR)     NOT NULL,
  BUSTRDNM                VARCHAR2(80 BYTE),
  BUSEXPRTIND             CHAR(1 BYTE),
  BUSBNKRPTIND            CHAR(1 BYTE),
  BUSLWSUITIND            CHAR(1 BYTE),
  BUSPRIORSBALOANIND      CHAR(1 BYTE),
  VETCD                   NUMBER(3),
  GNDRCD                  CHAR(1 BYTE),
  ETHNICCD                CHAR(2 BYTE),
  BUSPRIMPHNNMB           CHAR(10 BYTE),
  BUSALTPHNNMB            CHAR(10 BYTE),
  BUSPRIMEMAILADR         VARCHAR2(80 BYTE),
  BUSALTEMAILADR          VARCHAR2(80 BYTE),
  BUSPHYADDRSTR1NM        VARCHAR2(80 BYTE),
  BUSPHYADDRSTR2NM        VARCHAR2(80 BYTE),
  BUSPHYADDRCTYNM         VARCHAR2(40 BYTE),
  BUSPHYADDRSTCD          CHAR(2 BYTE),
  BUSPHYADDRSTNM          VARCHAR2(60 BYTE),
  BUSPHYADDRCNTCD         CHAR(2 BYTE),
  BUSPHYADDRZIPCD         CHAR(5 BYTE),
  BUSPHYADDRZIP4CD        CHAR(4 BYTE),
  BUSPHYADDRPOSTCD        VARCHAR2(20 BYTE),
  BUSMAILADDRSTR1NM       VARCHAR2(80 BYTE),
  BUSMAILADDRSTR2NM       VARCHAR2(80 BYTE),
  BUSMAILADDRCTYNM        VARCHAR2(40 BYTE),
  BUSMAILADDRSTCD         CHAR(2 BYTE),
  BUSMAILADDRSTNM         VARCHAR2(60 BYTE),
  BUSMAILADDRCNTCD        CHAR(2 BYTE),
  BUSMAILADDRZIPCD        CHAR(5 BYTE),
  BUSMAILADDRZIP4CD       CHAR(4 BYTE),
  BUSMAILADDRPOSTCD       VARCHAR2(20 BYTE),
  BUSEXTRNLCRDTSCORIND    CHAR(1 BYTE),
  IMCRDTSCORSOURCCD       NUMBER(3),
  BUSEXTRNLCRDTSCORNMB    NUMBER(10),
  BUSEXTRNLCRDTSCORDT     DATE,
  BUSPRIMBUSEXPRNCEYRNMB  NUMBER(3),
  BUSDUNSNMB              CHAR(9 BYTE),
  BUSCREATUSERID          VARCHAR2(32 BYTE)     DEFAULT USER                  NOT NULL,
  BUSCREATDT              DATE                  DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID          VARCHAR2(32 BYTE)     DEFAULT USER,
  LASTUPDTDT              DATE                  DEFAULT SYSDATE,
  LAST4TAXID              VARCHAR2(4 BYTE),
  VETCERTIND              CHAR(1 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING
ENABLE ROW MOVEMENT;

COMMENT ON COLUMN LOAN.BUSTBL.LAST4TAXID IS 'Last 4 digits of the taxid for use in search screen';


CREATE INDEX LOAN.BUSLAST4TAXIDIDX ON LOAN.BUSTBL
(LAST4TAXID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.BUSPHYADDZPCDIND ON LOAN.BUSTBL
(BUSPHYADDRZIPCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.BUSSRCHNMIDX ON LOAN.BUSTBL
(BUSSRCHNM, BUSPHYADDRSTR1NM)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.BUSSRCHNMIND ON LOAN.BUSTBL
(BUSSRCHNM)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.XPKBUSCURRTBL ON LOAN.BUSTBL
(TAXID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.BUSUPDTRIG
    AFTER UPDATE
    ON LOAN.BUSTBL
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_Hist_CreatUserId   VARCHAR2 (32);
    --v_BusHistrySeqNmb    NUMBER;
    v_TaxId              CHAR (10);
BEGIN
    /*
     IF business name is changed, update the loanappnm
           ****************************************************************************
           SP: 10/07/2013: Removed street number and street suffix name as part of appending these to street name changes
           JP - 03/27/2019 CAFSOPER-2614 - changed history created user to record updated user instead of record created user
           JP - 04/10/2019 -- CAFSOPER-2586- History for LOAN.BUSTBL being created by BUS_HIST_TRIG trigger
        ****************************************************************************
    */

    IF :old.BusNm != :new.BusNm THEN
        FOR REC IN (SELECT loanappnmb
                      FROM loan.loangntyborrtbl
                     WHERE     LOANBUSPRIMBORRIND = 'Y'
                           AND LOANBORRACTVINACTIND = 'A'
                           AND BORRBUSPERIND = 'B'
                           AND TAXID = :new.TAXID)
        LOOP
            UPDATE loan.loangntytbl
               SET loanappnm = :new.BusNm, lastupdtuserid = :new.lastupdtuserid, lastupdtdt = SYSDATE
             WHERE loanappnmb = REC.loanappnmb;
        END LOOP;
    END IF;

    v_TaxId := :New.TaxId;
    v_Hist_CreatUserId := NVL (:new.lastupdtuserid, USER);                              --:NEW.BusCreatUserId;

    --Get the history number based on the loan number and create user id
    --IncrmntSeqNmbCSP ('BusHistry', v_Hist_CreatUserId, v_BusHistrySeqNmb);

    --Insert into business history -- history now geenrated through BUS_HIST_TRIG trigger
    /* INSERT INTO BusHistryTbl (BusHistrySeqNmb,
                               TaxId,
                               BusEINCertInd,
                               BusTypCd,
                               BusNm,
                               BusTrdNm,
                               BusExprtInd,
                               BusBnkrptInd,
                               BusLwsuitInd,
                               BusPriorSBALoanInd,
                               VetCd,
                               GndrCd,
                               EthnicCd,
                               BusPrimPhnNmb,
                               BusAltPhnNmb,
                               BusPrimEmailAdr,
                               BusAltEmailAdr,
                               BusPhyAddrStr1Nm,
                               BusPhyAddrStr2Nm,
                               BusPhyAddrCtyNm,
                               BusPhyAddrStCd,
                               BusPhyAddrStNm,
                               BusPhyAddrCntCd,
                               BusPhyAddrZipCd,
                               BusPhyAddrZip4Cd,
                               BusPhyAddrPostCd,
                               BusMailAddrStr1Nm,
                               BusMailAddrStr2Nm,
                               BusMailAddrCtyNm,
                               BusMailAddrStCd,
                               BusMailAddrStNm,
                               BusMailAddrCntCd,
                               BusMailAddrZipCd,
                               BusMailAddrZip4Cd,
                               BusMailAddrPostCd,
                               BusExtrnlCrdtScorInd,
                               IMCrdtScorSourcCd,
                               BusExtrnlCrdtScorNmb,
                               BusExtrnlCrdtScorDt,
                               BusPrimBusExprnceYrNmb,
                               --OUTLAWCD,
                               --OUTLAWOTHRDSCTXT,
                               BusCreatUserId,
                               BusCreatDt,
                               BusHistryCreatUserId,
                               BusHistryCreatDt,
                               LAST4TAXID)
          VALUES (v_BusHistrySeqNmb,
                  :OLD.TaxId,
                  :OLD.BusEINCertInd,
                  :OLD.BusTypCd,
                  :OLD.BusNm,
                  :OLD.BusTrdNm,
                  :OLD.BusExprtInd,
                  :OLD.BusBnkrptInd,
                  :OLD.BusLwsuitInd,
                  :OLD.BusPriorSBALoanInd,
                  :OLD.VetCd,
                  :OLD.GndrCd,
                  :OLD.EthnicCd,
                  :OLD.BusPrimPhnNmb,
                  :OLD.BusAltPhnNmb,
                  :OLD.BusPrimEmailAdr,
                  :OLD.BusAltEmailAdr,
                  :OLD.BusPhyAddrStr1Nm,
                  :OLD.BusPhyAddrStr2Nm,
                  :OLD.BusPhyAddrCtyNm,
                  :OLD.BusPhyAddrStCd,
                  :OLD.BusPhyAddrStNm,
                  :OLD.BusPhyAddrCntCd,
                  :OLD.BusPhyAddrZipCd,
                  :OLD.BusPhyAddrZip4Cd,
                  :OLD.BusPhyAddrPostCd,
                  :OLD.BusMailAddrStr1Nm,
                  :OLD.BusMailAddrStr2Nm,
                  :OLD.BusMailAddrCtyNm,
                  :OLD.BusMailAddrStCd,
                  :OLD.BusMailAddrStNm,
                  :OLD.BusMailAddrCntCd,
                  :OLD.BusMailAddrZipCd,
                  :OLD.BusMailAddrZip4Cd,
                  :OLD.BusMailAddrPostCd,
                  :OLD.BusExtrnlCrdtScorInd,
                  :OLD.IMCrdtScorSourcCd,
                  :OLD.BusExtrnlCrdtScorNmb,
                  :OLD.BusExtrnlCrdtScorDt,
                  :OLD.BusPrimBusExprnceYrNmb,
                  --:OLD.OUTLAWCD,
                  --:OLD.OUTLAWOTHRDSCTXT,
                  :OLD.BusCreatUserId,
                  :OLD.BusCreatDt,
                  v_Hist_CreatUserId,
                  SYSDATE,
                  :OLD.LAST4TAXID);

    INSERT INTO BusPrinHistryTbl (BusHistrySeqNmb,
                                  BusTaxId,
                                  PrinTaxId,
                                  PrinBusPerInd,
                                  BusPrinOwnrshpPct,
                                  BusPrinHistryCreatUserId,
                                  BusPrinHistryCreatDt)
        SELECT v_BusHistrySeqNmb,
               BusTaxId,
               PrinTaxId,
               PrinBusPerInd,
               BusPrinOwnrshpPct,
               BusPrinCreatUserId,
               BusPrinCreatDt
          FROM BusPrinTbl
         WHERE     BusTaxId = v_TaxId
               AND BusPrinActvInactInd = 'A';

    INSERT INTO BusRaceHistryTbl (BusHistrySeqNmb,
                                  TaxId,
                                  RaceCd,
                                  BusRaceHistryCreatUserId,
                                  BusRaceHistryCreatDt)
        SELECT v_BusHistrySeqNmb,
               TaxId,
               RaceCd,
               BusRaceCreatUserId,
               BusRaceCreatDt
          FROM BusRaceTbl
         WHERE TaxId = v_TaxId;*/

    IF    (    RTRIM (:NEW.BusPrimPhnNmb) IS NOT NULL
           AND NVL (:OLD.BusPrimPhnNmb, ' ') != :NEW.BusPrimPhnNmb)
       OR (    RTRIM (:NEW.BusAltPhnNmb) IS NOT NULL
           AND NVL (:OLD.BusAltPhnNmb, ' ') != :NEW.BusAltPhnNmb)
       OR (    RTRIM (:NEW.BusPrimEmailAdr) IS NOT NULL
           AND NVL (:OLD.BusPrimEmailAdr, ' ') != :NEW.BusPrimEmailAdr)
       OR (    RTRIM (:NEW.BusAltEmailAdr) IS NOT NULL
           AND NVL (:OLD.BusAltEmailAdr, ' ') != :NEW.BusAltEmailAdr) THEN
        FOR c1 IN (SELECT a.loanappnmb
                     FROM loan.loangntyborrtbl a, loan.dlcstbl b
                    WHERE     a.loanappnmb = b.loanappnmb
                          AND a.LOANBUSPRIMBORRIND = 'Y'
                          AND a.BORRBUSPERIND = 'B'
                          AND a.LOANBORRACTVINACTIND = 'A'
                          AND a.taxid = :NEW.taxid)
        LOOP
            UPDATE loan.dlcstbl
               SET DLCSWORKPHNNMB = NVL (RTRIM (:NEW.BusPrimPhnNmb), DLCSWORKPHNNMB),
                   DLCSCELLPHNNMB = NVL (RTRIM (:NEW.BusAltPhnNmb), DLCSCELLPHNNMB),
                   DLCSPRIMEMAILADR = NVL (RTRIM (:NEW.BusPrimEmailAdr), DLCSPRIMEMAILADR),
                   DLCSALTEMAILADR = NVL (RTRIM (:NEW.BusAltEmailAdr), DLCSALTEMAILADR),
                   LASTUPDTUSERID = :NEW.BusCreatUserId,
                   LASTUPDTDT = SYSDATE
             WHERE loanappnmb = c1.loanappnmb;
        END LOOP;
    END IF;
END;
/


CREATE OR REPLACE TRIGGER LOAN.BUS_HIST_TRIG
    AFTER UPDATE
    ON LOAN.BUSTBL
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_header    VARCHAR2 (2000);
    v_log_txt   CLOB;
/***********************************************************
 NAME:       BUS_HIST_TRIG
 PURPOSE: To generate history for LOAN.BUSTBL
 1.0        8/8/2018      JPaul
******************************************************************************/
BEGIN
    v_header := '<History><TaxId>' || TRIM (:OLD.TAXID) || '</TaxId>';

    v_log_txt := v_header;

    IF NVL (TO_CHAR (:OLD.BusEINCertInd), 'x') != NVL (TO_CHAR (:NEW.BusEINCertInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('EINCertInd', :OLD.BusEINCertInd, :NEW.BusEINCertInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusTypCd), 'x') != NVL (TO_CHAR (:NEW.BusTypCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LegalOrgnztnCd', :OLD.BusTypCd, :NEW.BusTypCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusNm), 'x') != NVL (TO_CHAR (:NEW.BusNm), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('BusinessName', :OLD.BusNm, :NEW.BusNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusTrdNm), 'x') != NVL (TO_CHAR (:NEW.BusTrdNm), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('TradeName', :OLD.BusTrdNm, :NEW.BusTrdNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusExprtInd), 'x') != NVL (TO_CHAR (:NEW.BusExprtInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ExporterInd', :OLD.BusExprtInd, :NEW.BusExprtInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusBnkrptInd), 'x') != NVL (TO_CHAR (:NEW.BusBnkrptInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('BnkrptcyInd', :OLD.BusBnkrptInd, :NEW.BusBnkrptInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusLwsuitInd), 'x') != NVL (TO_CHAR (:NEW.BusLwsuitInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LawsuitInd', :OLD.BusLwsuitInd, :NEW.BusLwsuitInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPriorSBALoanInd), 'x') != NVL (TO_CHAR (:NEW.BusPriorSBALoanInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PriorSBALoanInd',
                                  :OLD.BusPriorSBALoanInd,
                                  :NEW.BusPriorSBALoanInd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.VetCd), 'x') != NVL (TO_CHAR (:NEW.VetCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('VetCd', :OLD.VetCd, :NEW.VetCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.GndrCd), 'x') != NVL (TO_CHAR (:NEW.GndrCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('GNDRCD', :OLD.GndrCd, :NEW.GndrCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.EthnicCd), 'x') != NVL (TO_CHAR (:NEW.EthnicCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ETHNICCD', :OLD.EthnicCd, :NEW.EthnicCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPrimPhnNmb), 'x') != NVL (TO_CHAR (:NEW.BusPrimPhnNmb), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PrimPhnNmb', :OLD.BusPrimPhnNmb, :NEW.BusPrimPhnNmb, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusAltPhnNmb), 'x') != NVL (TO_CHAR (:NEW.BusAltPhnNmb), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('AlternatePhone', :OLD.BusAltPhnNmb, :NEW.BusAltPhnNmb, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPrimEmailAdr), 'x') != NVL (TO_CHAR (:NEW.BusPrimEmailAdr), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PrimaryEmail', :OLD.BusPrimEmailAdr, :NEW.BusPrimEmailAdr, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusAltEmailAdr), 'x') != NVL (TO_CHAR (:NEW.BusAltEmailAdr), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('AlternateEmail', :OLD.BusAltEmailAdr, :NEW.BusAltEmailAdr, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrStr1Nm), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrStr1Nm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysStrtName1', :OLD.BusPhyAddrStr1Nm, :NEW.BusPhyAddrStr1Nm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrStr2Nm), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrStr2Nm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysStrtName2', :OLD.BusPhyAddrStr2Nm, :NEW.BusPhyAddrStr2Nm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrCtyNm), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrCtyNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysCityName', :OLD.BusPhyAddrCtyNm, :NEW.BusPhyAddrCtyNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrStCd), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrStCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PhysStCd', :OLD.BusPhyAddrStCd, :NEW.BusPhyAddrStCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrStNm), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrStNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BusPhyAddrStNm', :OLD.BusPhyAddrStNm, :NEW.BusPhyAddrStNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrCntCd), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrCntCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysCntyCd', :OLD.BusPhyAddrCntCd, :NEW.BusPhyAddrCntCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrZipCd), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrZipCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysZipCd', :OLD.BusPhyAddrZipCd, :NEW.BusPhyAddrZipCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrZip4Cd), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrZip4Cd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysZip4Cd', :OLD.BusPhyAddrZip4Cd, :NEW.BusPhyAddrZip4Cd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPhyAddrPostCd), 'x') != NVL (TO_CHAR (:NEW.BusPhyAddrPostCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysPostalCd', :OLD.BusPhyAddrPostCd, :NEW.BusPhyAddrPostCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrStr1Nm), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrStr1Nm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailStrtName1', :OLD.BusMailAddrStr1Nm, :NEW.BusMailAddrStr1Nm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrStr2Nm), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrStr2Nm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailStrtName2', :OLD.BusMailAddrStr2Nm, :NEW.BusMailAddrStr2Nm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrCtyNm), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrCtyNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailCityName', :OLD.BusMailAddrCtyNm, :NEW.BusMailAddrCtyNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrStCd), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrStCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('MailStCd', :OLD.BusMailAddrStCd, :NEW.BusMailAddrStCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrStNm), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrStNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailAddrStNm ', :OLD.BusMailAddrStNm, :NEW.BusMailAddrStNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrCntCd), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrCntCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailCountyCd', :OLD.BusMailAddrCntCd, :NEW.BusMailAddrCntCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrZipCd), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrZipCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailZipCd', :OLD.BusMailAddrZipCd, :NEW.BusMailAddrZipCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrZip4Cd), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrZip4Cd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailZip4Cd', :OLD.BusMailAddrZip4Cd, :NEW.BusMailAddrZip4Cd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusMailAddrPostCd), 'x') != NVL (TO_CHAR (:NEW.BusMailAddrPostCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailPostalCd', :OLD.BusMailAddrPostCd, :NEW.BusMailAddrPostCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusExtrnlCrdtScorInd), 'x') != NVL (TO_CHAR (:NEW.BusExtrnlCrdtScorInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorInd',
                                  :OLD.BusExtrnlCrdtScorInd,
                                  :NEW.BusExtrnlCrdtScorInd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.IMCrdtScorSourcCd), 'x') != NVL (TO_CHAR (:NEW.IMCrdtScorSourcCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('CreditScorSourcCd',
                                  :OLD.IMCrdtScorSourcCd,
                                  :NEW.IMCrdtScorSourcCd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusExtrnlCrdtScorNmb), 'x') != NVL (TO_CHAR (:NEW.BusExtrnlCrdtScorNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorNmb',
                                  :OLD.BusExtrnlCrdtScorNmb,
                                  :NEW.BusExtrnlCrdtScorNmb,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusExtrnlCrdtScorDt), 'x') != NVL (TO_CHAR (:NEW.BusExtrnlCrdtScorDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorDt',
                                  :OLD.BusExtrnlCrdtScorDt,
                                  :NEW.BusExtrnlCrdtScorDt,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.BusPrimBusExprnceYrNmb), 'x') != NVL (TO_CHAR (:NEW.BusPrimBusExprnceYrNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PrimBusExprnceYrNmb',
                                  :OLD.BusPrimBusExprnceYrNmb,
                                  :NEW.BusPrimBusExprnceYrNmb,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LAST4TAXID), 'x') != NVL (TO_CHAR (:NEW.LAST4TAXID), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LAST4TAXID', :OLD.LAST4TAXID, :NEW.LAST4TAXID, v_log_txt);
    END IF;

    -- insert only if there is history
    IF v_log_txt != v_header THEN
        v_log_txt := v_log_txt || '</History>';

        INSERT INTO LOAN.LoanAudtLogTbl (LOANAUDTLOGSEQNMB,
                                         TAXID,
                                         BUSPERIND,
                                         LOANAUDTLOGTXT,
                                         LOANUPDTUSERID,
                                         LOANUPDTDT)
             VALUES ((SELECT MAX (LoanAudtLogSeqNmb) + 1 FROM loan.LoanAudtLogTbl),
                     :OLD.TAXID,
                     'B',                                                                    --:OLD.BUSPERIND,
                     v_log_txt,
                     NVL (:NEW.LASTUPDTUSERID, USER),
                     SYSDATE);
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Error code ' || SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 64));
        RAISE;
END BUS_HIST_TRIG;
/


ALTER TABLE LOAN.BUSTBL ADD (
  CONSTRAINT XPKBUSCURRTBL
  PRIMARY KEY
  (TAXID)
  USING INDEX LOAN.XPKBUSCURRTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.BUSTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.BUSTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.BUSTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.BUSTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.BUSTBL TO GPTS;

GRANT SELECT ON LOAN.BUSTBL TO LANAREAD;

GRANT SELECT ON LOAN.BUSTBL TO LANAUPDATE;

GRANT REFERENCES, SELECT ON LOAN.BUSTBL TO LOANACCT;

GRANT SELECT ON LOAN.BUSTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANAPP;

GRANT SELECT ON LOAN.BUSTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANLANAUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.BUSTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANREADROLE;

GRANT SELECT ON LOAN.BUSTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.BUSTBL TO LOANUPDROLE;

GRANT UPDATE ON LOAN.BUSTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.BUSTBL TO READLOANROLE;

GRANT INSERT, SELECT ON LOAN.BUSTBL TO STAGE;

GRANT SELECT ON LOAN.BUSTBL TO UPDLOANROLE;

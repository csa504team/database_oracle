DROP TABLE LOAN.RECORDSINASSOCFORPERTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.RECORDSINASSOCFORPERTBL
(
  LOANAPPNMB               NUMBER(10),
  LOANASSOCSEQNMB          NUMBER(3),
  TAXID                    CHAR(10 BYTE),
  LOANASSOCTYPCD           NUMBER(3),
  LOANASSOCBUSPERIND       CHAR(1 BYTE),
  LOANASSOCFIRSTNM         VARCHAR2(40 BYTE),
  LOANASSOCLASTNM          VARCHAR2(160 BYTE),
  LOANASSOCINITIALNM       CHAR(1 BYTE),
  LOANASSOCBUSNM           VARCHAR2(80 BYTE),
  PHYADDRSTR1NM            VARCHAR2(80 BYTE),
  PHYADDRSTR2NM            VARCHAR2(80 BYTE),
  PHYADDRCTYNM             VARCHAR2(40 BYTE),
  PHYADDRSTCD              CHAR(2 BYTE),
  PHYADDRSTNM              VARCHAR2(60 BYTE),
  PHYADDRCNTCD             CHAR(2 BYTE),
  PHYADDRZIPCD             CHAR(5 BYTE),
  PHYADDRZIP4CD            CHAR(4 BYTE),
  MAILADDRSTR1NM           VARCHAR2(80 BYTE),
  MAILADDRSTR2NM           VARCHAR2(80 BYTE),
  MAILADDRCTYNM            VARCHAR2(40 BYTE),
  MAILADDRSTCD             CHAR(2 BYTE),
  MAILADDRSTNM             VARCHAR2(60 BYTE),
  MAILADDRCNTCD            CHAR(2 BYTE),
  MAILADDRZIPCD            CHAR(5 BYTE),
  MAILADDRZIP4CD           CHAR(4 BYTE),
  LOANASSOCPHNNMB          VARCHAR2(20 BYTE),
  LOANASSOCEMAILADR        VARCHAR2(80 BYTE),
  LOANASSOCCMNTTXT         VARCHAR2(255 BYTE),
  LOANASSOCACTVINACTIND    CHAR(1 BYTE),
  LOANASSOCCREATUSERID     VARCHAR2(32 BYTE),
  LOANASSOCCREATDT         DATE,
  LOANASSOCLASTUPDTUSERID  VARCHAR2(32 BYTE),
  LOANASSOCLASTUPDTDT      DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.RECORDSINASSOCFORPERTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.RECORDSINASSOCFORPERTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.RECORDSINASSOCFORPERTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.RECORDSINASSOCFORPERTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.RECORDSINASSOCFORPERTBL TO LOANSERVLOANPRTUPLOAD;

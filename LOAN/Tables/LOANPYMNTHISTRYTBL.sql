DROP TABLE LOAN.LOANPYMNTHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANPYMNTHISTRYTBL
(
  LOANHISTRYSEQNMB            NUMBER(10),
  PYMNTDAYNMB                 NUMBER(3),
  PYMNTBEGNMONMB              NUMBER(3),
  ESCROWACCTRQRDIND           CHAR(1 BYTE),
  NETEARNCLAUSEIND            CHAR(1 BYTE),
  ARMMARTYPIND                CHAR(1 BYTE),
  ARMMARCEILRT                NUMBER(6,3),
  ARMMARFLOORRT               NUMBER(6,3),
  STINTRTREDUCTNIND           CHAR(1 BYTE),
  LATECHGIND                  CHAR(1 BYTE),
  LATECHGAFTDAYNMB            NUMBER(3),
  LATECHGFEEPCT               NUMBER(6,3),
  PYMNTINTONLYBEGNMONMB       NUMBER(3),
  PYMNTINTONLYFREQCD          CHAR(1 BYTE),
  PYMNTINTONLYDAYNMB          NUMBER(3),
  LASTUPDTUSERID              VARCHAR2(32 BYTE),
  LASTUPDTDT                  DATE,
  NETEARNPYMNTPCT             NUMBER(6,3),
  NETEARNPYMNTOVERAMT         NUMBER(15,2),
  STINTRTREDUCTNPRGMNM        VARCHAR2(255 BYTE),
  LOANPYMNTHISTRYCREATDT      DATE,
  LOANPYMNTHISTRYCREATUSERID  VARCHAR2(32 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT INSERT, SELECT, UPDATE ON LOAN.LOANPYMNTHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANPYMNTHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANPYMNTHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LOANPYMNTHISTRYTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANPYMNTHISTRYTBL TO UPDLOANROLE;

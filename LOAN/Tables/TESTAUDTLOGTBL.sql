DROP TABLE LOAN.TESTAUDTLOGTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TESTAUDTLOGTBL
(
  LOANAUDTLOGSEQNMB  NUMBER(10)                 NOT NULL,
  LOANAPPNMB         NUMBER(10),
  TAXID              CHAR(10 BYTE),
  BUSPERIND          CHAR(1 BYTE),
  LOANAUDTLOGTXT     CLOB                       NOT NULL,
  LOANUPDTUSERID     VARCHAR2(32 BYTE)          NOT NULL,
  LOANUPDTDT         DATE                       NOT NULL,
  ENCRYPTLOGTXT      BLOB
)
LOB (LOANAUDTLOGTXT) STORE AS SECUREFILE (
  TABLESPACE  LOANDATATBS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          104K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                 ))
LOB (ENCRYPTLOGTXT) STORE AS SECUREFILE (
  TABLESPACE  LOANDATATBS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          104K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                 ))
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TESTAUDTLOGTBL TO LOANSERVLOANPRTUPLOAD;

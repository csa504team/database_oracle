DROP TABLE LOAN.TT_TMPLOANBORRTBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE LOAN.TT_TMPLOANBORRTBL
(
  TAXID               CHAR(10 BYTE),
  BORRSEQNMB          NUMBER(10),
  BORRBUSPERIND       CHAR(1 BYTE),
  LOANBUSPRIMBORRIND  CHAR(1 BYTE),
  BUSNM               VARCHAR2(80 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO GPTS;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO LOANACCT;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TT_TMPLOANBORRTBL TO LOANSERVLOANPRTUPLOAD;

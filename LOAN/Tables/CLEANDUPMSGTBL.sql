DROP TABLE LOAN.CLEANDUPMSGTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.CLEANDUPMSGTBL
(
  LOANAPPNMB  NUMBER(10),
  LOANNMB     CHAR(10 BYTE),
  ELIPSFEE    NUMBER(15,2),
  ELENDFEE    NUMBER(15,2),
  CLEANIND    CHAR(1 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.CLEANDUPMSGTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.CLEANDUPMSGTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.CLEANDUPMSGTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.CLEANDUPMSGTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.CLEANDUPMSGTBL TO LOANSERVLOANPRTUPLOAD;

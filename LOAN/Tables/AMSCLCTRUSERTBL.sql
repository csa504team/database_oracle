ALTER TABLE LOAN.AMSCLCTRUSERTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.AMSCLCTRUSERTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.AMSCLCTRUSERTBL
(
  SBAOFCCD            CHAR(4 BYTE)              NOT NULL,
  SERVGRPSEQNMB       NUMBER(10)                NOT NULL,
  SERVGRPCD           CHAR(1 BYTE),
  AMSCLCTRUSERID      VARCHAR2(32 BYTE)         NOT NULL,
  AMSCLCTRPHNNMB      VARCHAR2(10 BYTE),
  AMSCLCTRPHNEXTNNMB  VARCHAR2(4 BYTE),
  CREATUSERID         VARCHAR2(32 BYTE)         NOT NULL,
  CREATDT             DATE                      NOT NULL,
  LASTUPDTUSERID      VARCHAR2(32 BYTE)         NOT NULL,
  LASTUPDTDT          DATE                      NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKAMSCLCTRUSERTBL ON LOAN.AMSCLCTRUSERTBL
(SBAOFCCD, SERVGRPSEQNMB)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.AMSCLCTRUSERTBL ADD (
  CONSTRAINT XPKAMSCLCTRUSERTBL
  PRIMARY KEY
  (SBAOFCCD, SERVGRPSEQNMB)
  USING INDEX LOAN.XPKAMSCLCTRUSERTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANACCT;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANAPP;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANCMNTREADALLROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANCMNTWRITEROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANLOOKUPUPDROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO READLOANROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.AMSCLCTRUSERTBL TO STAGE;

GRANT SELECT ON LOAN.AMSCLCTRUSERTBL TO UPDLOANROLE;

ALTER TABLE LOAN.LOANGNTYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANGNTYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANGNTYTBL
(
  LOANAPPNMB                    NUMBER(10)      NOT NULL,
  PRGMCD                        CHAR(1 BYTE)    NOT NULL,
  PRCSMTHDCD                    CHAR(3 BYTE)    NOT NULL,
  COHORTCD                      CHAR(20 BYTE)   NOT NULL,
  LOANAPPNM                     VARCHAR2(80 BYTE) NOT NULL,
  LOANAPPRECVDT                 DATE            NOT NULL,
  LOANAPPENTRYDT                DATE            NOT NULL,
  LOANAPPRQSTAMT                NUMBER(19,4)    NOT NULL,
  LOANAPPSBAGNTYPCT             NUMBER(6,3)     NOT NULL,
  LOANINITLAPPVAMT              NUMBER(19,4)    NOT NULL,
  LOANCURRAPPVAMT               NUMBER(19,4)    NOT NULL,
  LOANINITLAPPVDT               DATE            NOT NULL,
  LOANLASTAPPVDT                DATE            NOT NULL,
  LOANAPPRQSTMATMOQTY           NUMBER(3)       NOT NULL,
  LOANAPPOTHADJPRDTXT           VARCHAR2(80 BYTE),
  LOANAPPEPCIND                 CHAR(1 BYTE),
  LOANAPPPYMTAMT                NUMBER(19,4),
  LOANAPPFULLAMORTPYMTIND       CHAR(1 BYTE),
  LOANAPPMOINTQTY               NUMBER(3),
  LOANAPPLIFINSURRQMTIND        CHAR(1 BYTE),
  LOANAPPRCNSDRTNIND            CHAR(1 BYTE),
  LOANAPPINJCTNIND              CHAR(1 BYTE),
  LOANCOLLATIND                 CHAR(1 BYTE),
  LOANAPPEWCPSNGLTRANSPOSTIND   CHAR(1 BYTE),
  LOANAPPEWCPSNGLTRANSIND       CHAR(1 BYTE),
  LOANAPPELIGEVALIND            CHAR(1 BYTE)    NOT NULL,
  LOANAPPNEWBUSCD               CHAR(1 BYTE),
  NAICSYRNMB                    NUMBER(4),
  NAICSCD                       NUMBER(10),
  LOANAPPFRNCHSIND              CHAR(1 BYTE),
  LOANAPPFRNCHSCD               CHAR(5 BYTE),
  LOANAPPFRNCHSNM               VARCHAR2(80 BYTE),
  LOANAPPDISASTERCNTRLNMB       VARCHAR2(20 BYTE),
  LOANSTATCD                    NUMBER(3,1)     NOT NULL,
  LOANSTATDT                    DATE            NOT NULL,
  LOANAPPORIGNTNOFCCD           CHAR(4 BYTE)    NOT NULL,
  LOANAPPPRCSOFCCD              CHAR(4 BYTE)    NOT NULL,
  LOANAPPSERVOFCCD              CHAR(4 BYTE),
  LOANAPPFUNDDT                 DATE            NOT NULL,
  LOANNMB                       CHAR(10 BYTE)   NOT NULL,
  LOANGNTYFEEAMT                NUMBER(19,4),
  LOCID                         NUMBER(7),
  PRTID                         NUMBER(7),
  LOANAPPPRTAPPNMB              VARCHAR2(20 BYTE),
  LOANAPPPRTLOANNMB             VARCHAR2(20 BYTE),
  LOANOUTBALAMT                 NUMBER(19,4),
  LOANOUTBALDT                  DATE,
  LOANSOLDSCNDMRKTIND           CHAR(1 BYTE),
  LOANAPPCREATUSERID            VARCHAR2(32 BYTE) NOT NULL,
  LOANAPPCREATDT                DATE            NOT NULL,
  LOANTYPIND                    CHAR(1 BYTE),
  LOANSRVSLOCID                 NUMBER(7),
  LOANAPPMATDT                  DATE,
  LOANAPPFIRSTDISBDT            DATE,
  LOANNEXTINSTLMNTDUEDT         DATE,
  LOANTOTUNDISBAMT              NUMBER(19,4),
  LOANDISBIND                   CHAR(1 BYTE),
  RECOVIND                      CHAR(1 BYTE),
  LOANAPPOUTPRGRMAREAOFOPERIND  CHAR(1 BYTE),
  LOANAPPPARNTLOANNMB           CHAR(10 BYTE),
  LOANAPPMICROLENDRID           CHAR(10 BYTE),
  ACHRTNGNMB                    VARCHAR2(25 BYTE),
  ACHACCTNMB                    VARCHAR2(25 BYTE),
  ACHACCTTYPCD                  CHAR(1 BYTE),
  LOANINTPAIDAMT                NUMBER(19,4),
  LOANINTPAIDDT                 DATE,
  ACHTINNMB                     CHAR(10 BYTE),
  LOANAPPCDCGNTYAMT             NUMBER(19,4),
  LOANAPPCDCNETDBENTRAMT        NUMBER(19,4),
  LOANAPPCDCFUNDFEEAMT          NUMBER(19,4),
  LOANAPPCDCSEPRTPRCSFEEIND     CHAR(1 BYTE),
  LOANAPPCDCPRCSFEEAMT          NUMBER(19,4),
  LOANAPPCDCCLSCOSTAMT          NUMBER(19,4),
  LOANAPPCDCUNDRWTRFEEAMT       NUMBER(19,4),
  LOANAPPCONTRIBPCT             NUMBER(5,2),
  LOANAPPCONTRIBAMT             NUMBER(19,4),
  LOANAPPPRTTAXID               CHAR(10 BYTE),
  LOANSTATCMNTCD                CHAR(2 BYTE),
  LOANGNTYFEEPAIDAMT            NUMBER(19,4),
  LOANGNTYDFRTODT               DATE,
  LOANGNTYDFRDMNTHSNMB          NUMBER(10),
  LOANGNTYDFRDGRSAMT            NUMBER(19,4),
  LOANDISASTRAPPNMB             CHAR(30 BYTE),
  LOANDISASTRAPPFEECHARGED      NUMBER(19,4),
  LOANDISASTRAPPDCSN            CHAR(15 BYTE),
  LOANASSOCDISASTRAPPNMB        CHAR(30 BYTE),
  LOANASSOCDISASTRLOANNMB       CHAR(10 BYTE),
  LOANPYMNTSCHDLFREQ            VARCHAR2(24 BYTE),
  FININSTRMNTTYPIND             CHAR(1 BYTE)    NOT NULL,
  NOTEPARENTLOANNMB             CHAR(10 BYTE),
  NOTEGENNEDIND                 CHAR(1 BYTE),
  LOANPURCHSDIND                CHAR(2 BYTE),
  LASTUPDTUSERID                VARCHAR2(32 BYTE) DEFAULT USER,
  LASTUPDTDT                    DATE            DEFAULT SYSDATE,
  POOLORIGPARTINITLAMT          NUMBER(19,4),
  POOLSBAGNTYBALINITLAMT        NUMBER(19,4),
  POOLLENDRPARTCURRAMT          NUMBER(19,4),
  POOLORIGPARTCURRAMT           NUMBER(19,4),
  POOLLENDRPARTINITLAMT         NUMBER(19,4),
  POOLLENDRGROSSINITLAMT        NUMBER(19,4),
  POOLGROSSINITLINTPCT          NUMBER(6,3),
  POOLLENDRGROSSCURRAMT         NUMBER(19,4),
  LOANGNTYFEEREBATEAMT          NUMBER(19,4),
  LOANAPPMATEXTDT               DATE,
  LOAN1201NOTCIND               NUMBER(3),
  LOANSERVGRPCD                 CHAR(1 BYTE),
  FREEZETYPCD                   CHAR(1 BYTE),
  FREEZERSNCD                   NUMBER(3),
  FORGVNESFREEZETYPCD           NUMBER(3),
  DISBDEADLNDT                  DATE,
  GTYFEEUNCOLLAMT               NUMBER(19,4),
  LOANPYMTTRANSCD               NUMBER(3),
  LOANCOFSEFFDT                 DATE,
  SBICLICNSNMB                  CHAR(8 BYTE),
  LOANMAILSTR1NM                VARCHAR2(80 BYTE) NOT NULL,
  LOANMAILSTR2NM                VARCHAR2(80 BYTE),
  LOANMAILCTYNM                 VARCHAR2(40 BYTE) NOT NULL,
  LOANMAILSTCD                  CHAR(2 BYTE),
  LOANMAILZIPCD                 CHAR(5 BYTE),
  LOANMAILZIP4CD                CHAR(4 BYTE),
  LOANMAILCNTRYCD               CHAR(2 BYTE),
  LOANMAILSTNM                  VARCHAR2(60 BYTE),
  LOANMAILPOSTCD                VARCHAR2(20 BYTE),
  LOANAPPNETEARNIND             CHAR(1 BYTE),
  LOANPYMTINSTLMNTTYPCD         CHAR(1 BYTE),
  LOANPYMNINSTLMNTFREQCD        CHAR(1 BYTE),
  LOANTOTDEFNMB                 NUMBER(3),
  LOANTOTDEFMONMB               NUMBER(3),
  LOANGNTYFEEBILLAMT            NUMBER(15,2),
  LOANGNTYEIDLAMT               NUMBER(15,2),
  LOANGNTYEIDLCNCLAMT           NUMBER(15,2),
  LOANGNTYPHYAMT                NUMBER(15,2),
  LOANGNTYPHYCNCLAMT            NUMBER(15,2),
  UNDRWRITNGBY                  VARCHAR2(4 BYTE),
  LOANGNTYNOTEDT                DATE,
  LOANGNTYMATSTIND              VARCHAR2(1 BYTE),
  LOANGNTYLCKOUTUPDIND          CHAR(1 BYTE),
  CURREFCLOSDT                  DATE,
  CURREFSTATCD                  CHAR(2 BYTE),
  LOANONGNGFEECOLLIND           CHAR(1 BYTE),
  LOANAPPINTDTLCD               VARCHAR2(2 BYTE),
  LOANORIGNTNFEEIND             CHAR(1 BYTE),
  LOANPYMTREPAYINSTLTYPCD       CHAR(1 BYTE),
  LOANGNTYPURISSIND             VARCHAR2(1 BYTE),
  LOANGNTYPURISSDT              DATE,
  LOANSBICLICNSTYP              VARCHAR2(1 BYTE),
  LOANSBICDRAWTYP               VARCHAR2(1 BYTE),
  LOANSBICSCHDLPOOLDT           DATE,
  LOANGNTYFEEDSCNTRT            NUMBER(5,2),
  LOANVETOWNIND                 CHAR(1 BYTE),
  LOANAUTOAMSSTOPIND            VARCHAR2(1 BYTE),
  LOANPUROFCCD                  VARCHAR2(4 BYTE),
  LOANAPPLSPHQLOCID             NUMBER(7),
  LOANLNDRASSGNLTRDT            DATE,
  LOANGNTYCSAAMTRCVD            NUMBER(13),
  CDCSERVFEEPCT                 NUMBER(6,3),
  LOANGNTY1201ELCTRNCSTMTIND    CHAR(1 BYTE)    DEFAULT 'N'                   NOT NULL,
  LOANGNTYNOTEINTRT             NUMBER(6,3),
  LOANGNTYNOTEMNTLYPYMTAMT      NUMBER(19,4),
  LOANAPPCDCGROSSDBENTRAMT      NUMBER(15,2),
  LOANAPPBALTOBORRAMT           NUMBER(15,2),
  LOANMAILADDRSINVLDIND         CHAR(1 BYTE),
  LOANAPPREVLMOINTQTY           NUMBER(3),
  LOANAPPAMORTMOINTQTY          NUMBER(3),
  LOANGNTYFEEBILLADJAMT         NUMBER(19,4),
  LOANGNTYDBENTRINTRT           NUMBER(8,5),
  LOANAGNTINVLVDIND             VARCHAR2(1 BYTE),
  LOANAPPEXTRASERVFEEAMT        NUMBER(19,4),
  LOANAPPEXTRASERVFEEIND        CHAR(1 BYTE),
  LOANGNTYCORRESPLANGCD         CHAR(3 BYTE),
  LOANAPPMONTHPAYROLL           NUMBER(15,2),
  LENDRREBATEFEEAMT             NUMBER(19,4),
  LENDRREBATEFEEPAIDDT          DATE,
  LOAN1502CERTIND               CHAR(1 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.LOANAPPFUNDDTNNIND ON LOAN.LOANGNTYTBL
(LOANAPPFUNDDT)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANAPPLOCIDNNIND ON LOAN.LOANGNTYTBL
(LOCID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANAPPPRTIDNNIND ON LOAN.LOANGNTYTBL
(PRTID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANCOFSEFFDTIND ON LOAN.LOANGNTYTBL
(LOANCOFSEFFDT)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYAPPSERVOFCCDIND ON LOAN.LOANGNTYTBL
(LOANAPPSERVOFCCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX CNTRMZ01.LOANGNTYLSPHQLOCIDIND ON LOAN.LOANGNTYTBL
(LOANAPPLSPHQLOCID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYMICRLENDRINDIND ON LOAN.LOANGNTYTBL
(LOANAPPMICROLENDRID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX CNTRMZ01.LOANGNTYPARNTLOANNMBIND ON LOAN.LOANGNTYTBL
(LOANAPPPARNTLOANNMB)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYPRCSMTHDNNIND ON LOAN.LOANGNTYTBL
(PRCSMTHDCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYPRGNCDIND ON LOAN.LOANGNTYTBL
(PRGMCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYSBICLICNSNMBIND ON LOAN.LOANGNTYTBL
(SBICLICNSNMB)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANGNTYSRVSLOCIDIND ON LOAN.LOANGNTYTBL
(LOANSRVSLOCID)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LOANSTATCDIND ON LOAN.LOANGNTYTBL
(LOANSTATCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.PURCHSDIND ON LOAN.LOANGNTYTBL
(LOANPURCHSDIND)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.UKLOANNMB ON LOAN.LOANGNTYTBL
(LOANNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.XPKLOANGNTYTBL ON LOAN.LOANGNTYTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.LOANGNTYUPDTRIG
   AFTER UPDATE
   ON LOAN.LOANGNTYTBL
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
DECLARE
   /*
   Date         :
   Programmer   :
   Remarks      :
   ***********************************************************************************
   Revised By      Date        Comments
   ----------     --------    ----------------------------------------------------------
   APK         03/18/2011      Modified to add LoanAppMatExtDt
   APK -- 10/13/2011 -- modified to add three new columns FREEZETYPCD,FREEZERSNCD,FORGVNESFREEZETYPCD as part of TO3 - LAU changes. also added Loan1201NotcInd  previously added to the Gnty table.
   APK -- 10/19/2011 -- added new field DisbDeadLnDt as it has been added to the table.
   APK -- 11/03/2011-- modified to add new column GtyFeeUnCollAmt as the fee was not correctly calculated from elips.
    THis column will now be synched with elips.
   SP  -- 1/9/2012  -- Modified to add new column LOANPYMTTRANSCD
   SP  -- 3/5/2012  -- Modified to add new column LOANCOFSEFFDT
   KL  -- 3/22/2012 add -- LOANGNTYUPDTRIG
   SP  -- 4/18/2012  -- Modified to add new column LoanAppNetEarnInd
   SP --- 05/01/2012 -- Modified  to add new columns SBICLICNSNMB,LOANMAILSTR1NM,LOANMAILSTR2NM,
   LOANMAILCTYNM,LOANMAILSTCD,LOANMAILZIPCD,LOANMAILZIP4CD,LOANMAILCNTRYCD,LOANMAILSTNM,LOANMAILPOSTCD,LOANPYMTINSTLMNTTYPCD,LOANPYMNINSTLMNTFREQCD,LOANTOTDEFNMB,LOANTOTDEFMONMB
   APK -- 08/09/2012 -- modified to add LoanAssocDisastrLoanNmb  to insert into the histry as it was missing.
   SP -- 05/29/2013 -- Modified to add LoanGntyNoteDt
   RSURAPA -- 06/13/2013 -- Modified the trigger to add LOANGNTYMATSTIND(new Column Maturity statrt indicator).
   RGG     -- 12/23/2013 -- Added LoanAppIntDtlCd to the trigger
   SP      -- 04/03/2014 --  Added LOANGNTYPURISSIND,LOANGNTYPURISSDT to alert a Loan Officer performing a purchase review in GPTS of possible guaranty issues
   SB -- 04/04/2014 -- Commented out NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LOANAPPFRNCHSIND columns
   PSP -- 04/23/2014 -- Added SBICcolumns:LOANSBICLICNSTYP,LOANSBICDRAWTYP,LOANSBICSCHDLPOOLDT
   PSP -- 02/25/2015 -- Added loanpurofccd to capture purchasing office using office code of the recommending official so would not lose count of the purchase once the file is assigned to the appropriate servicing office
   RGG -- 11/09/2015 -- Added SMP Pool Loan Amount Fields
   ********************************************************************************
   */
   v_LoanAppNmb       NUMBER(10, 0);
   v_CreatUserId      VARCHAR2(32);
   v_LoanHistrySeqNmb NUMBER(10, 0);
   v_temp             NUMBER(10);
BEGIN
   --get the loan application number
   v_LoanAppNmb   := :NEW.LoanAppNmb;
   v_CreatUserId  := :NEW.LastUpdtUserId;

   --Exit here for loan origination
   SELECT COUNT(*)
   INTO   v_temp
   FROM   loan.LoanTransTbl
   WHERE  LoanAppNmb = v_LoanAppNmb;

   IF     v_temp = 0
      AND :OLD.PrgmCd != 'C' THEN --not exists(select 1 from loan..LoanTransTbl where LoanAppNmb = @LoanAppNmb)
      RETURN;
   END IF;

   --write to history first
   --Get the history number based on the loan number and create user id
   --loan.LoanHistrySelCSP(v_LoanAppNmb, v_CreatUserId, v_LoanHistrySeqNmb, 'LoanGntyHistryTbl');

   /* -- history table population commneted out JP 08/24/2018
      SELECT COUNT(*)
      INTO   v_temp
      FROM   loan.LoanGntyHistryTbl
      WHERE  LoanHistrySeqNmb = v_LoanHistrySeqNmb;

      IF v_temp = 0 THEN --not exists(select 1 from LoanGntyHistryTbl where LoanHistrySeqNmb = @LoanHistrySeqNmb)
         BEGIN
            --Insert into history if history not exists
            INSERT INTO LoanGntyHistryTbl(LoanHistrySeqNmb,
                                          PrcsMthdCd,
                                          LoanAppSBAGntyPct,
                                          LoanCurrAppvAmt,
                                          LoanLastAppvDt,
                                          LoanAppRqstMatMoQty,
                                          LoanAppPymtAmt,
                                          LoanAppInjctnInd,
                                          LoanCollatInd,
                                          LoanAppDisasterCntrlNmb,
                                          LoanStatCd,
                                          LoanStatDt,
                                          LoanAppServOfcCd,
                                          LoanGntyFeeAmt,
                                          LocId,
                                          LoanSrvsLocId,
                                          PrtId,
                                          LoanAppPrtLoanNmb,
                                          LoanOutBalAmt,
                                          LoanOutBalDt,
                                          LoanAppMoIntQty,
                                          LoanAppNewBusCd,
                                          LoanAppMatDt,
                                          LoanAppFirstDisbDt,
                                          LoanNextInstlmntDueDt,
                                          LoanTotUndisbAmt,
                                          LoanDisbInd,
                                          LoanAppCDCGntyAmt,
                                          LoanAppCDCNetDbentrAmt,
                                          LoanAppCDCFundFeeAmt,
                                          LoanAppCDCSeprtPrcsFeeInd,
                                          LoanAppCDCPrcsFeeAmt,
                                          LoanAppCDCClsCostAmt,
                                          LoanAppCDCUndrwtrFeeAmt,
                                          LoanAppContribPct,
                                          LoanAppContribAmt,
                                          LOANAPPNM,
                                          ACHRTNGNMB,
                                          ACHACCTNMB,
                                          ACHACCTTYPCD,
                                          ACHTINNMB,
                                          LOANSTATCMNTCD,
                                          LOANGNTYFEEPAIDAMT,
                                          LOANGNTYDFRTODT,
                                          LOANGNTYDFRDGRSAMT,
                                          LOANGNTYDFRDMNTHSNMB,
                                          LoanPymntSchdlFreq,
                                          LoanGntyHistryCreatUserId,
                                          LoanGntyHistryCreatDt,
                                          LoanAppMatExtDt,
                                          Loan1201NotcInd,
                                          LOANSERVGRPCD,
                                          FREEZETYPCD,
                                          FREEZERSNCD,
                                          FORGVNESFREEZETYPCD,
                                          DISBDEADLNDT,
                                          GtyFeeUnCollAmt,
                                          LOANPYMTTRANSCD,
                                          LOANCOFSEFFDT,
                                          NoteGennedInd,
                                          SBICLICNSNMB,
                                          LOANMAILSTR1NM,
                                          LOANMAILSTR2NM,
                                          LOANMAILCTYNM,
                                          LOANMAILSTCD,
                                          LOANMAILZIPCD,
                                          LOANMAILZIP4CD,
                                          LOANMAILCNTRYCD,
                                          LOANMAILSTNM,
                                          LOANMAILPOSTCD,
                                          LOANAPPNETEARNIND,
                                          LOANPYMTINSTLMNTTYPCD,
                                          LOANPYMNINSTLMNTFREQCD,
                                          LOANTOTDEFNMB,
                                          LOANTOTDEFMONMB,
                                          LOANAPPOUTPRGRMAREAOFOPERIND,
                                          LOANGNTYEIDLAMT,
                                          LOANGNTYEIDLCNCLAMT,
                                          LOANGNTYPHYAMT,
                                          LOANGNTYPHYCNCLAMT,
                                          LoanAssocDisastrLoanNmb,
                                          LoanAssocDisastrAppNmb,
                                          LoanGntyNoteDt,
                                          LOANGNTYMATSTIND,
                                          LoanOngngFeeCollInd,
                                          LoanOrigntnFeeInd,
                                          LoanAppIntDtlCd,
                                          LOANPYMTREPAYINSTLTYPCD,
                                          LOANGNTYPURISSIND,
                                          LOANGNTYPURISSDT,
                                          LOANPURCHSDIND,
                                          LOANSBICLICNSTYP,
                                          LOANSBICDRAWTYP,
                                          LOANSBICSCHDLPOOLDT,
                                          LOANAUTOAMSSTOPIND,
                                          LOANPUROFCCD,
                                          POOLORIGPARTINITLAMT,
                                          POOLSBAGNTYBALINITLAMT,
                                          POOLLENDRPARTCURRAMT,
                                          POOLORIGPARTCURRAMT,
                                          POOLLENDRPARTINITLAMT,
                                          POOLLENDRGROSSINITLAMT,
                                          POOLGROSSINITLINTPCT,
                                          POOLLENDRGROSSCURRAMT,
                                          LOANSOLDSCNDMRKTIND)
            VALUES      (v_LoanHistrySeqNmb,
                         :OLD.PrcsMthdCd,
                         :OLD.LoanAppSBAGntyPct,
                         :OLD.LoanCurrAppvAmt,
                         :OLD.LoanLastAppvDt,
                         :OLD.LoanAppRqstMatMoQty,
                         :OLD.LoanAppPymtAmt,
                         :OLD.LoanAppInjctnInd,
                         :OLD.LoanCollatInd,
                         :OLD.LoanAppDisasterCntrlNmb,
                         :OLD.LoanStatCd,
                         :OLD.LoanStatDt,
                         :OLD.LoanAppServOfcCd,
                         :OLD.LoanGntyFeeAmt,
                         :OLD.LocId,
                         :OLD.LoanSrvsLocId,
                         :OLD.PrtId,
                         :OLD.LoanAppPrtLoanNmb,
                         :OLD.LoanOutBalAmt,
                         :OLD.LoanOutBalDt,
                         :OLD.LoanAppMoIntQty,
                         :OLD.LoanAppNewBusCd,
                         :OLD.LoanAppMatDt,
                         :OLD.LoanAppFirstDisbDt,
                         :OLD.LoanNextInstlmntDueDt,
                         :OLD.LoanTotUndisbAmt,
                         :OLD.LoanDisbInd,
                         :OLD.LoanAppCDCGntyAmt,
                         :OLD.LoanAppCDCNetDbentrAmt,
                         :OLD.LoanAppCDCFundFeeAmt,
                         :OLD.LoanAppCDCSeprtPrcsFeeInd,
                         :OLD.LoanAppCDCPrcsFeeAmt,
                         :OLD.LoanAppCDCClsCostAmt,
                         :OLD.LoanAppCDCUndrwtrFeeAmt,
                         :OLD.LoanAppContribPct,
                         :OLD.LoanAppContribAmt,
                         :OLD.LOANAPPNM,
                         :OLD.ACHRTNGNMB,
                         :OLD.ACHACCTNMB,
                         :OLD.ACHACCTTYPCD,
                         :OLD.ACHTINNMB,
                         :OLD.LOANSTATCMNTCD,
                         :OLD.LOANGNTYFEEPAIDAMT,
                         :OLD.LOANGNTYDFRTODT,
                         :OLD.LOANGNTYDFRDGRSAMT,
                         :OLD.LOANGNTYDFRDMNTHSNMB,
                         :OLD.LoanPymntSchdlFreq,
                         NVL(:OLD.LastUpdtUserId, USER),
                         NVL(:OLD.LastUpdtDt, SYSDATE),
                         :OLD.LoanAppMatExtDt,
                         :OLD.Loan1201NotcInd,
                         :OLD.LOANSERVGRPCD,
                         :OLD.FREEZETYPCD,
                         :OLD.FREEZERSNCD,
                         :OLD.FORGVNESFREEZETYPCD,
                         :OLD.DISBDEADLNDT,
                         :OLD.GtyFeeUnCollAmt,
                         :OLD.LOANPYMTTRANSCD,
                         :OLD.LOANCOFSEFFDT,
                         :OLD.NoteGennedInd,
                         :OLD.SBICLICNSNMB,
                         :OLD.LOANMAILSTR1NM,
                         :OLD.LOANMAILSTR2NM,
                         :OLD.LOANMAILCTYNM,
                         :OLD.LOANMAILSTCD,
                         :OLD.LOANMAILZIPCD,
                         :OLD.LOANMAILZIP4CD,
                         :OLD.LOANMAILCNTRYCD,
                         :OLD.LOANMAILSTNM,
                         :OLD.LOANMAILPOSTCD,
                         :OLD.LOANAPPNETEARNIND,
                         :OLD.LOANPYMTINSTLMNTTYPCD,
                         :OLD.LOANPYMNINSTLMNTFREQCD,
                         :OLD.LOANTOTDEFNMB,
                         :OLD.LOANTOTDEFMONMB,
                         :OLD.LOANAPPOUTPRGRMAREAOFOPERIND,
                         :OLD.LOANGNTYEIDLAMT,
                         :OLD.LOANGNTYEIDLCNCLAMT,
                         :OLD.LOANGNTYPHYAMT,
                         :OLD.LOANGNTYPHYCNCLAMT,
                         :OLD.LoanAssocDisastrLoanNmb,
                         :OLD.LoanAssocDisastrAppNmb,
                         :OLD.LoanGntyNoteDt,
                         :OLD.LOANGNTYMATSTIND,
                         :OLD.LoanOngngFeeCollInd,
                         :OLD.LoanOrigntnFeeInd,
                         :OLD.LoanAppIntDtlCd,
                         :OLD.LOANPYMTREPAYINSTLTYPCD,
                         :OLD.LOANGNTYPURISSIND,
                         :OLD.LOANGNTYPURISSDT,
                         :OLD.LOANPURCHSDIND,
                         :OLD.LOANSBICLICNSTYP,
                         :OLD.LOANSBICDRAWTYP,
                         :OLD.LOANSBICSCHDLPOOLDT,
                         :OLD.LOANAUTOAMSSTOPIND,
                         :OLD.LOANPUROFCCD,
                         :OLD.POOLORIGPARTINITLAMT,
                         :OLD.POOLSBAGNTYBALINITLAMT,
                         :OLD.POOLLENDRPARTCURRAMT,
                         :OLD.POOLORIGPARTCURRAMT,
                         :OLD.POOLLENDRPARTINITLAMT,
                         :OLD.POOLLENDRGROSSINITLAMT,
                         :OLD.POOLGROSSINITLINTPCT,
                         :OLD.POOLLENDRGROSSCURRAMT,
                         :OLD.LOANSOLDSCNDMRKTIND);
         END;
      END IF; */

   IF     RTRIM(:NEW.LastUpdtUserId) != 'elipsupdate'
      AND SUBSTR(:NEW.LOANPURCHSDIND, 2, 1) = 'S'
      AND    :OLD.LOANPYMNTSCHDLFREQ
          || TO_CHAR(NVL(ROUND(:OLD.LOANAPPPYMTAMT * 100, 0), 0), 'FM00000000')
          || TO_CHAR(:OLD.LOANNEXTINSTLMNTDUEDT, 'MMDDYY') !=
                :NEW.LOANPYMNTSCHDLFREQ
             || TO_CHAR(NVL(ROUND(:NEW.LOANAPPPYMTAMT * 100, 0), 0), 'FM00000000')
             || TO_CHAR(:NEW.LOANNEXTINSTLMNTDUEDT, 'MMDDYY') THEN
      Loan.LoanUpdtLogInsCSP(
         :NEW.LoanAppNmb,
         'B2G2',
            :OLD.LOANPYMNTSCHDLFREQ
         || TO_CHAR(NVL(ROUND(:OLD.LOANAPPPYMTAMT * 100, 0), 0), 'FM00000000')
         || TO_CHAR(:OLD.LOANNEXTINSTLMNTDUEDT, 'MMDDYY'),
            :NEW.LOANPYMNTSCHDLFREQ
         || TO_CHAR(NVL(ROUND(:NEW.LOANAPPPYMTAMT * 100, 0), 0), 'FM00000000')
         || TO_CHAR(:NEW.LOANNEXTINSTLMNTDUEDT, 'MMDDYY'),
         RTRIM(:NEW.LastUpdtUserId));
   END IF;
/* -- the following moved to compound trigger to avoid mutating trigger
  IF     :OLD.LoanStatCd = 3
     AND :NEW.LoanStatCd != 3 THEN
      UPDATE lqdtbl
     SET    LQDOUTDT = SYSDATE, LASTUPDTUSERID = :NEW.LastUpdtUserId, LASTUPDTDT = SYSDATE
     WHERE      LoanAppNmb = :NEW.LoanAppNmb
            AND LQDOUTDT IS NULL;
    END IF;*/
END;
/


CREATE OR REPLACE TRIGGER LOAN.LOANGNTY_COMP_TRIG
   FOR UPDATE
   ON LOAN.LOANGNTYTBL
COMPOUND TRIGGER
   TYPE lqdtbl_upd_rec IS RECORD
   (
      UserId     VARCHAR2(40),
      LoanAppNmb NUMBER(10)
   );

   TYPE row_level_info_t IS TABLE OF lqdtbl_upd_rec
      INDEX BY PLS_INTEGER;

   upd_data_t row_level_info_t;

 /***********************************************************
  NAME:       LOANGNTY_COMP_TRIG
  PURPOSE: To avoid mutating condition when updating loan.lqdtbl
  1.0        1/9/2019      JPaul
 ******************************************************************************/

   AFTER EACH ROW
   IS
   BEGIN
      IF     :OLD.LoanStatCd = 3
         AND :NEW.LoanStatCd != 3 THEN
         upd_data_t(upd_data_t.COUNT + 1).UserId      := :new.LastUpdtUserId;
         upd_data_t(upd_data_t.COUNT + 1).LoanAppNmb  := :new.LoanAppNmb;
      END IF;
   END AFTER EACH ROW;

   AFTER STATEMENT
   IS
   BEGIN
      FORALL indx IN 1 .. upd_data_t.COUNT
         UPDATE loan.lqdtbl
         SET    LQDOUTDT = TRUNC(SYSDATE),
                LASTUPDTUSERID = NVL(upd_data_t(indx).UserId, USER),
                LASTUPDTDT = SYSDATE
         WHERE      LoanAppNmb = upd_data_t(indx).LoanAppNmb
                AND LQDOUTDT IS NULL;
   END AFTER STATEMENT;
END LOANGNTY_COMP_TRIG;
/


CREATE OR REPLACE TRIGGER LOAN.LOANGNTY_HIST_TRIG
    AFTER UPDATE
    ON LOAN.LOANGNTYTBL
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_header    VARCHAR2 (2000);
    v_log_txt   CLOB;
 /***********************************************************
 NAME:       LOANGNTY_HIST_TRIG
 PURPOSE: To generate history for LOAN.LOANGNTYTBL
 1.0        8/1/2018      JPaul
  SS--5/09/2020Added LoanAppMonthPayroll,Loan1502CertInd,LendrRebateFeeAmt,LendrRebatefeePaidDt CARESACt 105 and 205
******************************************************************************/
BEGIN
    v_header := '<History><LoanNmb>' || :OLD.LOANNMB || '</LoanNmb>';
    v_log_txt := v_header;

    IF NVL (TO_CHAR (:OLD.PrcsMthdCd), 'x') != NVL (TO_CHAR (:NEW.PrcsMthdCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ProcessingMethodCd', :OLD.PrcsMthdCd, :NEW.PrcsMthdCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppSBAGntyPct), 'x') != NVL (TO_CHAR (:NEW.LoanAppSBAGntyPct), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('SBAGntyPct', :OLD.LoanAppSBAGntyPct, :NEW.LoanAppSBAGntyPct, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanCurrAppvAmt), 'x') != NVL (TO_CHAR (:NEW.LoanCurrAppvAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('RequestedAmt', :OLD.LoanCurrAppvAmt, :NEW.LoanCurrAppvAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanLastAppvDt), 'x') != NVL (TO_CHAR (:NEW.LoanLastAppvDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LastApprovedDt', :OLD.LoanLastAppvDt, :NEW.LoanLastAppvDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppRqstMatMoQty), 'x') != NVL (TO_CHAR (:NEW.LoanAppRqstMatMoQty), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanTermMnths',
                :OLD.LoanAppRqstMatMoQty,
                :NEW.LoanAppRqstMatMoQty,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppPymtAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppPymtAmt), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PymtAmt', :OLD.LoanAppPymtAmt, :NEW.LoanAppPymtAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppInjctnInd), 'x') != NVL (TO_CHAR (:NEW.LoanAppInjctnInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('InjectionInd', :OLD.LoanAppInjctnInd, :NEW.LoanAppInjctnInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanCollatInd), 'x') != NVL (TO_CHAR (:NEW.LoanCollatInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('CollateralInd', :OLD.LoanCollatInd, :NEW.LoanCollatInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppDisasterCntrlNmb), 'x') != NVL (TO_CHAR (:NEW.LoanAppDisasterCntrlNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'DisasterCntrlNmb',
                :OLD.LoanAppDisasterCntrlNmb,
                :NEW.LoanAppDisasterCntrlNmb,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanStatCd), 'x') != NVL (TO_CHAR (:NEW.LoanStatCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('StatusCd', :OLD.LoanStatCd, :NEW.LoanStatCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanStatDt), 'x') != NVL (TO_CHAR (:NEW.LoanStatDt), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('StatusDt', :OLD.LoanStatDt, :NEW.LoanStatDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppServOfcCd), 'x') != NVL (TO_CHAR (:NEW.LoanAppServOfcCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'SBAServicingOffice',
                :OLD.LoanAppServOfcCd,
                :NEW.LoanAppServOfcCd,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanGntyFeeAmt), 'x') != NVL (TO_CHAR (:NEW.LoanGntyFeeAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('GuarantyFeeAmt', :OLD.LoanGntyFeeAmt, :NEW.LoanGntyFeeAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LocId), 'x') != NVL (TO_CHAR (:NEW.LocId), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LendrLocId', :OLD.LocId, :NEW.LocId, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanSrvsLocId), 'x') != NVL (TO_CHAR (:NEW.LoanSrvsLocId), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LoanSrvsLocId', :OLD.LoanSrvsLocId, :NEW.LoanSrvsLocId, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PrtId), 'x') != NVL (TO_CHAR (:NEW.PrtId), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PrtId', :OLD.PrtId, :NEW.PrtId, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppPrtLoanNmb), 'x') != NVL (TO_CHAR (:NEW.LoanAppPrtLoanNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LenderLoanNmb', :OLD.LoanAppPrtLoanNmb, :NEW.LoanAppPrtLoanNmb, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanOutBalAmt), 'x') != NVL (TO_CHAR (:NEW.LoanOutBalAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('OutstandingBalance', :OLD.LoanOutBalAmt, :NEW.LoanOutBalAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanOutBalDt), 'x') != NVL (TO_CHAR (:NEW.LoanOutBalDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('OutstandingBalanceDt', :OLD.LoanOutBalDt, :NEW.LoanOutBalDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppMoIntQty), 'x') != NVL (TO_CHAR (:NEW.LoanAppMoIntQty), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LoanAppMoIntQty', :OLD.LoanAppMoIntQty, :NEW.LoanAppMoIntQty, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppNewBusCd), 'x') != NVL (TO_CHAR (:NEW.LoanAppNewBusCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LoanAppNewBusCd', :OLD.LoanAppNewBusCd, :NEW.LoanAppNewBusCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppMatDt), 'x') != NVL (TO_CHAR (:NEW.LoanAppMatDt), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('MaturityDt', :OLD.LoanAppMatDt, :NEW.LoanAppMatDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppFirstDisbDt), 'x') != NVL (TO_CHAR (:NEW.LoanAppFirstDisbDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'FirstDisbursementDt',
                :OLD.LoanAppFirstDisbDt,
                :NEW.LoanAppFirstDisbDt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanNextInstlmntDueDt), 'x') != NVL (TO_CHAR (:NEW.LoanNextInstlmntDueDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'NextInstlDt',
                :OLD.LoanNextInstlmntDueDt,
                :NEW.LoanNextInstlmntDueDt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanTotUndisbAmt), 'x') != NVL (TO_CHAR (:NEW.LoanTotUndisbAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'TotUndisbursedAmt',
                :OLD.LoanTotUndisbAmt,
                :NEW.LoanTotUndisbAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanDisbInd), 'x') != NVL (TO_CHAR (:NEW.LoanDisbInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('DisbursedInd', :OLD.LoanDisbInd, :NEW.LoanDisbInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCGntyAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCGntyAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('GntyFeeAmt', :OLD.LoanAppCDCGntyAmt, :NEW.LoanAppCDCGntyAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCNetDbentrAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCNetDbentrAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'NetDebentrAmt',
                :OLD.LoanAppCDCNetDbentrAmt,
                :NEW.LoanAppCDCNetDbentrAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCFundFeeAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCFundFeeAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppCDCFundFeeAmt',
                :OLD.LoanAppCDCFundFeeAmt,
                :NEW.LoanAppCDCFundFeeAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCSeprtPrcsFeeInd), 'x') !=
       NVL (TO_CHAR (:NEW.LoanAppCDCSeprtPrcsFeeInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'SeprateProcessFeeInd',
                :OLD.LoanAppCDCSeprtPrcsFeeInd,
                :NEW.LoanAppCDCSeprtPrcsFeeInd,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCPrcsFeeAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCPrcsFeeAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppCDCPrcsFeeAmt',
                :OLD.LoanAppCDCPrcsFeeAmt,
                :NEW.LoanAppCDCPrcsFeeAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCClsCostAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCClsCostAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'ClosingCostAmt',
                :OLD.LoanAppCDCClsCostAmt,
                :NEW.LoanAppCDCClsCostAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppCDCUndrwtrFeeAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppCDCUndrwtrFeeAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppCDCUndrwtrFeeAmt',
                :OLD.LoanAppCDCUndrwtrFeeAmt,
                :NEW.LoanAppCDCUndrwtrFeeAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppContribPct), 'x') != NVL (TO_CHAR (:NEW.LoanAppContribPct), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppContribPct',
                :OLD.LoanAppContribPct,
                :NEW.LoanAppContribPct,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppContribAmt), 'x') != NVL (TO_CHAR (:NEW.LoanAppContribAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppContribAmt',
                :OLD.LoanAppContribAmt,
                :NEW.LoanAppContribAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPNM), 'x') != NVL (TO_CHAR (:NEW.LOANAPPNM), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LOANAPPNM', :OLD.LOANAPPNM, :NEW.LOANAPPNM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.ACHRTNGNMB), 'x') != NVL (TO_CHAR (:NEW.ACHRTNGNMB), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ACHRTNGNMB', :OLD.ACHRTNGNMB, :NEW.ACHRTNGNMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.ACHACCTNMB), 'x') != NVL (TO_CHAR (:NEW.ACHACCTNMB), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ACHAccountNmb', :OLD.ACHACCTNMB, :NEW.ACHACCTNMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.ACHACCTTYPCD), 'x') != NVL (TO_CHAR (:NEW.ACHACCTTYPCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ACHAccountType', :OLD.ACHACCTTYPCD, :NEW.ACHACCTTYPCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.ACHTINNMB), 'x') != NVL (TO_CHAR (:NEW.ACHTINNMB), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ACHTINNMB', :OLD.ACHTINNMB, :NEW.ACHTINNMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSTATCMNTCD), 'x') != NVL (TO_CHAR (:NEW.LOANSTATCMNTCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('STATCMNTCD', :OLD.LOANSTATCMNTCD, :NEW.LOANSTATCMNTCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYFEEPAIDAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYFEEPAIDAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('FEEPAIDAMT', :OLD.LOANGNTYFEEPAIDAMT, :NEW.LOANGNTYFEEPAIDAMT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYDFRTODT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYDFRTODT), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('DFRTODT', :OLD.LOANGNTYDFRTODT, :NEW.LOANGNTYDFRTODT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYDFRDGRSAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYDFRDGRSAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('DFRDGRSAMT', :OLD.LOANGNTYDFRDGRSAMT, :NEW.LOANGNTYDFRDGRSAMT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYDFRDMNTHSNMB), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYDFRDMNTHSNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'DFRDMNTHSNMB',
                :OLD.LOANGNTYDFRDMNTHSNMB,
                :NEW.LOANGNTYDFRDMNTHSNMB,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanPymntSchdlFreq), 'x') != NVL (TO_CHAR (:NEW.LoanPymntSchdlFreq), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'PymtSchedule',
                :OLD.LoanPymntSchdlFreq,
                :NEW.LoanPymntSchdlFreq,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppMatExtDt), 'x') != NVL (TO_CHAR (:NEW.LoanAppMatExtDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MaturityExtDt', :OLD.LoanAppMatExtDt, :NEW.LoanAppMatExtDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.Loan1201NotcInd), 'x') != NVL (TO_CHAR (:NEW.Loan1201NotcInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('Loan1201NotcInd', :OLD.Loan1201NotcInd, :NEW.Loan1201NotcInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSERVGRPCD), 'x') != NVL (TO_CHAR (:NEW.LOANSERVGRPCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANSERVGRPCD', :OLD.LOANSERVGRPCD, :NEW.LOANSERVGRPCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.FREEZETYPCD), 'x') != NVL (TO_CHAR (:NEW.FREEZETYPCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('FREEZETYPCD', :OLD.FREEZETYPCD, :NEW.FREEZETYPCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.FREEZERSNCD), 'x') != NVL (TO_CHAR (:NEW.FREEZERSNCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('FREEZERSNCD', :OLD.FREEZERSNCD, :NEW.FREEZERSNCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.FORGVNESFREEZETYPCD), 'x') != NVL (TO_CHAR (:NEW.FORGVNESFREEZETYPCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'FORGVNESFREEZETYPCD',
                :OLD.FORGVNESFREEZETYPCD,
                :NEW.FORGVNESFREEZETYPCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.DISBDEADLNDT), 'x') != NVL (TO_CHAR (:NEW.DISBDEADLNDT), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('DISBDEADLNDT', :OLD.DISBDEADLNDT, :NEW.DISBDEADLNDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.GtyFeeUnCollAmt), 'x') != NVL (TO_CHAR (:NEW.GtyFeeUnCollAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('GtyFeeUnCollAmt', :OLD.GtyFeeUnCollAmt, :NEW.GtyFeeUnCollAmt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPYMTTRANSCD), 'x') != NVL (TO_CHAR (:NEW.LOANPYMTTRANSCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANPYMTTRANSCD', :OLD.LOANPYMTTRANSCD, :NEW.LOANPYMTTRANSCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANCOFSEFFDT), 'x') != NVL (TO_CHAR (:NEW.LOANCOFSEFFDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANCOFSEFFDT', :OLD.LOANCOFSEFFDT, :NEW.LOANCOFSEFFDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.NoteGennedInd), 'x') != NVL (TO_CHAR (:NEW.NoteGennedInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('NoteGennedInd', :OLD.NoteGennedInd, :NEW.NoteGennedInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.SBICLICNSNMB), 'x') != NVL (TO_CHAR (:NEW.SBICLICNSNMB), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('SBICLICNSNMB', :OLD.SBICLICNSNMB, :NEW.SBICLICNSNMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILSTR1NM), 'x') != NVL (TO_CHAR (:NEW.LOANMAILSTR1NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingStrtName1', :OLD.LOANMAILSTR1NM, :NEW.LOANMAILSTR1NM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILSTR2NM), 'x') != NVL (TO_CHAR (:NEW.LOANMAILSTR2NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingStrtName2', :OLD.LOANMAILSTR2NM, :NEW.LOANMAILSTR2NM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILCTYNM), 'x') != NVL (TO_CHAR (:NEW.LOANMAILCTYNM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingCityName', :OLD.LOANMAILCTYNM, :NEW.LOANMAILCTYNM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILSTCD), 'x') != NVL (TO_CHAR (:NEW.LOANMAILSTCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('BillingStCd', :OLD.LOANMAILSTCD, :NEW.LOANMAILSTCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILZIPCD), 'x') != NVL (TO_CHAR (:NEW.LOANMAILZIPCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('BillingZipCd', :OLD.LOANMAILZIPCD, :NEW.LOANMAILZIPCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILZIP4CD), 'x') != NVL (TO_CHAR (:NEW.LOANMAILZIP4CD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingZip4Cd', :OLD.LOANMAILZIP4CD, :NEW.LOANMAILZIP4CD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILCNTRYCD), 'x') != NVL (TO_CHAR (:NEW.LOANMAILCNTRYCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingCountryCd', :OLD.LOANMAILCNTRYCD, :NEW.LOANMAILCNTRYCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILSTNM), 'x') != NVL (TO_CHAR (:NEW.LOANMAILSTNM), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('BillingStNm', :OLD.LOANMAILSTNM, :NEW.LOANMAILSTNM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANMAILPOSTCD), 'x') != NVL (TO_CHAR (:NEW.LOANMAILPOSTCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BillingPostalCd', :OLD.LOANMAILPOSTCD, :NEW.LOANMAILPOSTCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPNETEARNIND), 'x') != NVL (TO_CHAR (:NEW.LOANAPPNETEARNIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'NetEarningsClause',
                :OLD.LOANAPPNETEARNIND,
                :NEW.LOANAPPNETEARNIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPYMTINSTLMNTTYPCD), 'x') != NVL (TO_CHAR (:NEW.LOANPYMTINSTLMNTTYPCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANPYMTINSTLMNTTYPCD',
                :OLD.LOANPYMTINSTLMNTTYPCD,
                :NEW.LOANPYMTINSTLMNTTYPCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPYMNINSTLMNTFREQCD), 'x') != NVL (TO_CHAR (:NEW.LOANPYMNINSTLMNTFREQCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'PymtFrequency',
                :OLD.LOANPYMNINSTLMNTFREQCD,
                :NEW.LOANPYMNINSTLMNTFREQCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANTOTDEFNMB), 'x') != NVL (TO_CHAR (:NEW.LOANTOTDEFNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANTOTDEFNMB', :OLD.LOANTOTDEFNMB, :NEW.LOANTOTDEFNMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANTOTDEFMONMB), 'x') != NVL (TO_CHAR (:NEW.LOANTOTDEFMONMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANTOTDEFMONMB', :OLD.LOANTOTDEFMONMB, :NEW.LOANTOTDEFMONMB, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPOUTPRGRMAREAOFOPERIND), 'x') !=
       NVL (TO_CHAR (:NEW.LOANAPPOUTPRGRMAREAOFOPERIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'OutPrgrmAreaOfOperInd',
                :OLD.LOANAPPOUTPRGRMAREAOFOPERIND,
                :NEW.LOANAPPOUTPRGRMAREAOFOPERIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYEIDLAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYEIDLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANGNTYEIDLAMT', :OLD.LOANGNTYEIDLAMT, :NEW.LOANGNTYEIDLAMT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYEIDLCNCLAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYEIDLCNCLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANGNTYEIDLCNCLAMT',
                :OLD.LOANGNTYEIDLCNCLAMT,
                :NEW.LOANGNTYEIDLCNCLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYPHYAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYPHYAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANGNTYPHYAMT', :OLD.LOANGNTYPHYAMT, :NEW.LOANGNTYPHYAMT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYPHYCNCLAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYPHYCNCLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANGNTYPHYCNCLAMT',
                :OLD.LOANGNTYPHYCNCLAMT,
                :NEW.LOANGNTYPHYCNCLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAssocDisastrLoanNmb), 'x') != NVL (TO_CHAR (:NEW.LoanAssocDisastrLoanNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'DisasterLoanNmb',
                :OLD.LoanAssocDisastrLoanNmb,
                :NEW.LoanAssocDisastrLoanNmb,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAssocDisastrAppNmb), 'x') != NVL (TO_CHAR (:NEW.LoanAssocDisastrAppNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'ASSOCDISASTRAPPNMB',
                :OLD.LoanAssocDisastrAppNmb,
                :NEW.LoanAssocDisastrAppNmb,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanGntyNoteDt), 'x') != NVL (TO_CHAR (:NEW.LoanGntyNoteDt), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('NoteDt', :OLD.LoanGntyNoteDt, :NEW.LoanGntyNoteDt, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYMATSTIND), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYMATSTIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanTermStartTypInd',
                :OLD.LOANGNTYMATSTIND,
                :NEW.LOANGNTYMATSTIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanOngngFeeCollInd), 'x') != NVL (TO_CHAR (:NEW.LoanOngngFeeCollInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'OngoingFeeInd',
                :OLD.LoanOngngFeeCollInd,
                :NEW.LoanOngngFeeCollInd,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanOrigntnFeeInd), 'x') != NVL (TO_CHAR (:NEW.LoanOrigntnFeeInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('OrigntnFeeInd', :OLD.LoanOrigntnFeeInd, :NEW.LoanOrigntnFeeInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanAppIntDtlCd), 'x') != NVL (TO_CHAR (:NEW.LoanAppIntDtlCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'InterestStructureCd',
                :OLD.LoanAppIntDtlCd,
                :NEW.LoanAppIntDtlCd,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPYMTREPAYINSTLTYPCD), 'x') != NVL (TO_CHAR (:NEW.LOANPYMTREPAYINSTLTYPCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'PymtTypeCode',
                :OLD.LOANPYMTREPAYINSTLTYPCD,
                :NEW.LOANPYMTREPAYINSTLTYPCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYPURISSIND), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYPURISSIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANGNTYPURISSIND',
                :OLD.LOANGNTYPURISSIND,
                :NEW.LOANGNTYPURISSIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYPURISSDT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYPURISSDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANGNTYPURISSDT',
                :OLD.LOANGNTYPURISSDT,
                :NEW.LOANGNTYPURISSDT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPURCHSDIND), 'x') != NVL (TO_CHAR (:NEW.LOANPURCHSDIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANPURCHSDIND', :OLD.LOANPURCHSDIND, :NEW.LOANPURCHSDIND, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSBICLICNSTYP), 'x') != NVL (TO_CHAR (:NEW.LOANSBICLICNSTYP), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANSBICLICNSTYP',
                :OLD.LOANSBICLICNSTYP,
                :NEW.LOANSBICLICNSTYP,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSBICDRAWTYP), 'x') != NVL (TO_CHAR (:NEW.LOANSBICDRAWTYP), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANSBICDRAWTYP', :OLD.LOANSBICDRAWTYP, :NEW.LOANSBICDRAWTYP, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSBICSCHDLPOOLDT), 'x') != NVL (TO_CHAR (:NEW.LOANSBICSCHDLPOOLDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANSBICSCHDLPOOLDT',
                :OLD.LOANSBICSCHDLPOOLDT,
                :NEW.LOANSBICSCHDLPOOLDT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAUTOAMSSTOPIND), 'x') != NVL (TO_CHAR (:NEW.LOANAUTOAMSSTOPIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANAUTOAMSSTOPIND',
                :OLD.LOANAUTOAMSSTOPIND,
                :NEW.LOANAUTOAMSSTOPIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANPUROFCCD), 'x') != NVL (TO_CHAR (:NEW.LOANPUROFCCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LOANPUROFCCD', :OLD.LOANPUROFCCD, :NEW.LOANPUROFCCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLORIGPARTINITLAMT), 'x') != NVL (TO_CHAR (:NEW.POOLORIGPARTINITLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLORIGPARTINITLAMT',
                :OLD.POOLORIGPARTINITLAMT,
                :NEW.POOLORIGPARTINITLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLSBAGNTYBALINITLAMT), 'x') != NVL (TO_CHAR (:NEW.POOLSBAGNTYBALINITLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLSBAGNTYBALINITLAMT',
                :OLD.POOLSBAGNTYBALINITLAMT,
                :NEW.POOLSBAGNTYBALINITLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLLENDRPARTCURRAMT), 'x') != NVL (TO_CHAR (:NEW.POOLLENDRPARTCURRAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLLENDRPARTCURRAMT',
                :OLD.POOLLENDRPARTCURRAMT,
                :NEW.POOLLENDRPARTCURRAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLORIGPARTCURRAMT), 'x') != NVL (TO_CHAR (:NEW.POOLORIGPARTCURRAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLORIGPARTCURRAMT',
                :OLD.POOLORIGPARTCURRAMT,
                :NEW.POOLORIGPARTCURRAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLLENDRPARTINITLAMT), 'x') != NVL (TO_CHAR (:NEW.POOLLENDRPARTINITLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLLENDRPARTINITLAMT',
                :OLD.POOLLENDRPARTINITLAMT,
                :NEW.POOLLENDRPARTINITLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLLENDRGROSSINITLAMT), 'x') != NVL (TO_CHAR (:NEW.POOLLENDRGROSSINITLAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLLENDRGROSSINITLAMT',
                :OLD.POOLLENDRGROSSINITLAMT,
                :NEW.POOLLENDRGROSSINITLAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLGROSSINITLINTPCT), 'x') != NVL (TO_CHAR (:NEW.POOLGROSSINITLINTPCT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLGROSSINITLINTPCT',
                :OLD.POOLGROSSINITLINTPCT,
                :NEW.POOLGROSSINITLINTPCT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.POOLLENDRGROSSCURRAMT), 'x') != NVL (TO_CHAR (:NEW.POOLLENDRGROSSCURRAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'POOLLENDRGROSSCURRAMT',
                :OLD.POOLLENDRGROSSCURRAMT,
                :NEW.POOLLENDRGROSSCURRAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANSOLDSCNDMRKTIND), 'x') != NVL (TO_CHAR (:NEW.LOANSOLDSCNDMRKTIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANSOLDSCNDMRKTIND',
                :OLD.LOANSOLDSCNDMRKTIND,
                :NEW.LOANSOLDSCNDMRKTIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPMONTHPAYROLL), 'x') != NVL (TO_CHAR (:NEW.LOANAPPMONTHPAYROLL), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'MonthPayRoll',
                :OLD.LOANAPPMONTHPAYROLL,
                :NEW.LOANAPPMONTHPAYROLL,
                v_log_txt);
    END IF;

    --- newly added beyond this JP 4/17/2010
    IF NVL (TO_CHAR (:OLD.PRGMCD), 'x') != NVL (TO_CHAR (:NEW.PRGMCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PRGMCD', :OLD.PRGMCD, :NEW.PRGMCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPPARNTLOANNMB), 'x') != NVL (TO_CHAR (:NEW.LOANAPPPARNTLOANNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANAPPPARNTLOANNMB',
                :OLD.LOANAPPPARNTLOANNMB,
                :NEW.LOANAPPPARNTLOANNMB,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.RECOVIND), 'x') != NVL (TO_CHAR (:NEW.RECOVIND), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('RECOVIND', :OLD.RECOVIND, :NEW.RECOVIND, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.NOTEPARENTLOANNMB), 'x') != NVL (TO_CHAR (:NEW.NOTEPARENTLOANNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'NOTEPARENTLOANNMB',
                :OLD.NOTEPARENTLOANNMB,
                :NEW.NOTEPARENTLOANNMB,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.FININSTRMNTTYPIND), 'x') != NVL (TO_CHAR (:NEW.FININSTRMNTTYPIND), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'FININSTRMNTTYPIND',
                :OLD.FININSTRMNTTYPIND,
                :NEW.FININSTRMNTTYPIND,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPENTRYDT), 'x') != NVL (TO_CHAR (:NEW.LOANAPPENTRYDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANAPPENTRYDT', :OLD.LOANAPPENTRYDT, :NEW.LOANAPPENTRYDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPFUNDDT), 'x') != NVL (TO_CHAR (:NEW.LOANAPPFUNDDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANAPPFUNDDT', :OLD.LOANAPPFUNDDT, :NEW.LOANAPPFUNDDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPMICROLENDRID), 'x') != NVL (TO_CHAR (:NEW.LOANAPPMICROLENDRID), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANAPPMICROLENDRID',
                :OLD.LOANAPPMICROLENDRID,
                :NEW.LOANAPPMICROLENDRID,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPORIGNTNOFCCD), 'x') != NVL (TO_CHAR (:NEW.LOANAPPORIGNTNOFCCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANAPPORIGNTNOFCCD',
                :OLD.LOANAPPORIGNTNOFCCD,
                :NEW.LOANAPPORIGNTNOFCCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LoanGntyNoteMntlyPymtAmt), 'x') !=
       NVL (TO_CHAR (:NEW.LoanGntyNoteMntlyPymtAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanGntyNoteMntlyPymtAmt',
                :OLD.LoanGntyNoteMntlyPymtAmt,
                :NEW.LoanGntyNoteMntlyPymtAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPPRCSOFCCD), 'x') != NVL (TO_CHAR (:NEW.LOANAPPPRCSOFCCD), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANAPPPRCSOFCCD',
                :OLD.LOANAPPPRCSOFCCD,
                :NEW.LOANAPPPRCSOFCCD,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPPRTTAXID), 'x') != NVL (TO_CHAR (:NEW.LOANAPPPRTTAXID), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANAPPPRTTAXID', :OLD.LOANAPPPRTTAXID, :NEW.LOANAPPPRTTAXID, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANAPPRECVDT), 'x') != NVL (TO_CHAR (:NEW.LOANAPPRECVDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANAPPRECVDT', :OLD.LOANAPPRECVDT, :NEW.LOANAPPRECVDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.COHORTCD), 'x') != NVL (TO_CHAR (:NEW.COHORTCD), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('COHORTCD', :OLD.COHORTCD, :NEW.COHORTCD, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANGNTYFEEREBATEAMT), 'x') != NVL (TO_CHAR (:NEW.LOANGNTYFEEREBATEAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANGNTYFEEREBATEAMT',
                :OLD.LOANGNTYFEEREBATEAMT,
                :NEW.LOANGNTYFEEREBATEAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANINITLAPPVAMT), 'x') != NVL (TO_CHAR (:NEW.LOANINITLAPPVAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LOANINITLAPPVAMT',
                :OLD.LOANINITLAPPVAMT,
                :NEW.LOANINITLAPPVAMT,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANINITLAPPVDT), 'x') != NVL (TO_CHAR (:NEW.LOANINITLAPPVDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANINITLAPPVDT', :OLD.LOANINITLAPPVDT, :NEW.LOANINITLAPPVDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANINTPAIDAMT), 'x') != NVL (TO_CHAR (:NEW.LOANINTPAIDAMT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANINTPAIDAMT', :OLD.LOANINTPAIDAMT, :NEW.LOANINTPAIDAMT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANINTPAIDDT), 'x') != NVL (TO_CHAR (:NEW.LOANINTPAIDDT), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LOANINTPAIDDT', :OLD.LOANINTPAIDDT, :NEW.LOANINTPAIDDT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LOANTYPIND), 'x') != NVL (TO_CHAR (:NEW.LOANTYPIND), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LOANTYPIND', :OLD.LOANTYPIND, :NEW.LOANTYPIND, v_log_txt);
    END IF;

   /* IF NVL (TO_CHAR (:OLD.LoanAppMonthPayroll), 'x') != NVL (TO_CHAR (:NEW.LoanAppMonthPayroll), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LoanAppMonthPayroll',
                :OLD.LoanAppMonthPayroll,
                :NEW.LoanAppMonthPayroll,
                v_log_txt);
    END IF;*/

    IF NVL (TO_CHAR (:OLD.Loan1502CertInd), 'x') != NVL (TO_CHAR (:NEW.Loan1502CertInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('Loan1502CertInd', :OLD.Loan1502CertInd, :NEW.Loan1502CertInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LendrRebateFeeAmt), 'x') != NVL (TO_CHAR (:NEW.LendrRebateFeeAmt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LendrRebateFeeAmt',
                :OLD.LendrRebateFeeAmt,
                :NEW.LendrRebateFeeAmt,
                v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LendrRebatefeePaidDt), 'x') != NVL (TO_CHAR (:NEW.LendrRebatefeePaidDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT (
                'LendrRebatefeePaidDt',
                :OLD.LendrRebatefeePaidDt,
                :NEW.LendrRebatefeePaidDt,
                v_log_txt);
    END IF;

    -- insert only if there is history
    IF v_log_txt != v_header THEN
        v_log_txt := v_log_txt || '</History>';

        INSERT INTO LOAN.LoanAudtLogTbl (LOANAUDTLOGSEQNMB,
                                         LOANAPPNMB,
                                         LOANAUDTLOGTXT,
                                         LOANUPDTUSERID,
                                         LOANUPDTDT)
        VALUES ((SELECT MAX (LoanAudtLogSeqNmb) + 1 FROM loan.LoanAudtLogTbl),
                :OLD.LOANAPPNMB,
                v_log_txt,
                NVL (:NEW.LASTUPDTUSERID, USER),
                SYSDATE);
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Error code ' || SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 64));
        RAISE;
END LOANGNTY_HIST_TRIG;
/


ALTER TABLE LOAN.LOANGNTYTBL ADD (
  CONSTRAINT XPKLOANGNTYTBL
  PRIMARY KEY
  (LOANAPPNMB)
  USING INDEX LOAN.XPKLOANGNTYTBL
  ENABLE VALIDATE,
  CONSTRAINT UKLOANNMB
  UNIQUE (LOANNMB)
  USING INDEX LOAN.UKLOANNMB
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANGNTYTBL ADD (
  CONSTRAINT BUSAGECDFORLOANGNTY 
  FOREIGN KEY (LOANAPPNEWBUSCD) 
  REFERENCES SBAREF.BUSAGECDTBL (BUSAGECD)
  ENABLE VALIDATE,
  CONSTRAINT FORGVNESFREEZETYPCDFORLOANGNTY 
  FOREIGN KEY (FORGVNESFREEZETYPCD) 
  REFERENCES SBAREF.FORGVNESFREEZETYPCDTBL (FORGVNESFREEZETYPCD)
  ENABLE VALIDATE,
  CONSTRAINT FREEZERSNCDFORLOANGNTY 
  FOREIGN KEY (FREEZERSNCD) 
  REFERENCES SBAREF.FREEZERSNCDTBL (FREEZERSNCD)
  ENABLE VALIDATE,
  CONSTRAINT FREEZETYPCDFORLOANGNTY 
  FOREIGN KEY (FREEZETYPCD) 
  REFERENCES SBAREF.FREEZETYPCDTBL (FREEZETYPCD)
  ENABLE VALIDATE,
  CONSTRAINT GNTYHASLOANPYMTTRANSCD 
  FOREIGN KEY (LOANPYMTTRANSCD) 
  REFERENCES SBAREF.LOANPYMTTRANSCDTBL (LOANPYMTTRANSCD)
  ENABLE VALIDATE,
  CONSTRAINT LOANGNTYHASSTATCD 
  FOREIGN KEY (LOANSTATCD) 
  REFERENCES SBAREF.LOANSTATCDTBL (LOANSTATCD)
  ENABLE VALIDATE,
  CONSTRAINT PRCSMTHDFORLOANGNTY 
  FOREIGN KEY (PRCSMTHDCD) 
  REFERENCES SBAREF.PRCSMTHDTBL (PRCSMTHDCD)
  ENABLE VALIDATE,
  CONSTRAINT PRGRMFORLOANGNTY 
  FOREIGN KEY (PRGMCD) 
  REFERENCES SBAREF.PRGRMTBL (PRGRMCD)
  ENABLE VALIDATE,
  CONSTRAINT SBAOFCISORIGTNOFCLOANGNTY 
  FOREIGN KEY (LOANAPPORIGNTNOFCCD) 
  REFERENCES SBAREF.SBAOFCTBL (SBAOFCCD)
  ENABLE VALIDATE,
  CONSTRAINT SBAOFCISPRCSOFCLOANGNTY 
  FOREIGN KEY (LOANAPPPRCSOFCCD) 
  REFERENCES SBAREF.SBAOFCTBL (SBAOFCCD)
  ENABLE VALIDATE,
  CONSTRAINT SBAOFCISSERVOFCLOANGNTY 
  FOREIGN KEY (LOANAPPSERVOFCCD) 
  REFERENCES SBAREF.SBAOFCTBL (SBAOFCCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRACHANNURI WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRARICHARDS WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRDFREDERICKS WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRNSALATOVA WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRRDOYLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRSSEAQUIST;

GRANT SELECT ON LOAN.LOANGNTYTBL TO CNTRVRAJAN;

GRANT SELECT ON LOAN.LOANGNTYTBL TO ELIPSREAD;

GRANT SELECT ON LOAN.LOANGNTYTBL TO FDCADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTS WITH GRANT OPTION;
GRANT DELETE, INSERT, REFERENCES, UPDATE ON LOAN.LOANGNTYTBL TO GPTS;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSADMIN WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSDISTOFCROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSHQADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSMGMTREPORTS WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSOFCADMIN WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSOFCADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSOFCGRPROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSOFO WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSOFOROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSREAD WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSREADROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSSERVCNTRROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO GPTSUPDT WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO ILPERS;

GRANT SELECT ON LOAN.LOANGNTYTBL TO ILPERSADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO ILPERSLENDERROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LANAREAD;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LANAUPDATE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LLTSREADALLROLE;

GRANT REFERENCES, SELECT, UPDATE ON LOAN.LOANGNTYTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANACCTUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANAPPLOOKUPUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANAPPUPDAPPROLE;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYTBL TO LOANCMNT;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANCMNTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANCMNTWRITEROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANDOCS;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANELIGIBILITYREADGRP;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANELIGIBILITYREADROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANELIGIBILITYUPDATEGRP;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANELIGIBILITYUPDATEROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANLOOKUPUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANORIGHQOVERRIDE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANREADROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANSERVPRTRECV;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANSERVSBICGOV;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANGNTYTBL TO MICROLENDERROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO MICROLOAN WITH GRANT OPTION;

GRANT SELECT ON LOAN.LOANGNTYTBL TO MICROLOANADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO MPERSASSIGNROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO PARTNER;

GRANT SELECT ON LOAN.LOANGNTYTBL TO POOLSECADMIN;

GRANT SELECT ON LOAN.LOANGNTYTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO READLOANROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYTBL TO SBAREF;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYTBL TO STAGE;

GRANT SELECT ON LOAN.LOANGNTYTBL TO UPDLOANROLE;

DROP TABLE LOAN.TMPGBLOCK CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPGBLOCK
(
  BLOCK_TEXT  VARCHAR2(4000 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPGBLOCK TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPGBLOCK TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPGBLOCK TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPGBLOCK TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPGBLOCK TO LOANSERVLOANPRTUPLOAD;

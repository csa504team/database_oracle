ALTER TABLE LOAN.COLLATPROPTYPCDTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.COLLATPROPTYPCDTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.COLLATPROPTYPCDTBL
(
  COLLATPROPTYPCD           CHAR(1 BYTE)        NOT NULL,
  COLLATPROPTYPDESCTXT      VARCHAR2(25 BYTE)   NOT NULL,
  COLLATPROPTYPSTRTDT       DATE                NOT NULL,
  COLLATPROPTYPENDDT        DATE,
  COLLATPROPTYPCREATUSERID  VARCHAR2(32 BYTE)   DEFAULT user                  NOT NULL,
  COLLATPROPTYPCREATDT      DATE                DEFAULT sysdate               NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKCOLLATPROPTYPCDTBL ON LOAN.COLLATPROPTYPCDTBL
(COLLATPROPTYPCD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.COLLATPROPTYPCDTBL ADD (
  CONSTRAINT XPKCOLLATPROPTYPCDTBL
  PRIMARY KEY
  (COLLATPROPTYPCD)
  USING INDEX LOAN.XPKCOLLATPROPTYPCDTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.COLLATPROPTYPCDTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.COLLATPROPTYPCDTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.COLLATPROPTYPCDTBL TO CNTRNSALATOVA;

GRANT INSERT, SELECT, UPDATE ON LOAN.COLLATPROPTYPCDTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.COLLATPROPTYPCDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.COLLATPROPTYPCDTBL TO LOANSERVLOANPRTUPLOAD;

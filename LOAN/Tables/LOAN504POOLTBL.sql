ALTER TABLE LOAN.LOAN504POOLTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOAN504POOLTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOAN504POOLTBL
(
  LOANAPPNMB                NUMBER(10),
  LOANPOOLAPPNMB            NUMBER(10),
  GROSSPOOLAMT              NUMBER(19,4),
  GROSSMONTHENDPOOLBAL      NUMBER(19,4),
  POOLORIGMONTHENDBAL       NUMBER(19,4),
  PARTLENDSHAREMONTHENDBAL  NUMBER(19,4),
  GROSSMONTHLYPRINPAIDAMT   NUMBER(19,4),
  GROSSMONTHLYINTPAIDAMT    NUMBER(19,4),
  INITLADVTOTLOANBAL        NUMBER(19,4),
  ADVNCEPAIDDURNGMONTH      NUMBER(19,4),
  ADVNCECOLLDURNGMONTH      NUMBER(19,4),
  MONTHENDADVNCEBAL         NUMBER(19,4),
  LOANAPPCREATUSERID        VARCHAR2(32 BYTE),
  LOANAPPCREATDT            DATE,
  LASTUPDTUSERID            VARCHAR2(32 BYTE)   DEFAULT USER,
  LASTUPDTDT                DATE                DEFAULT SYSDATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOAN504POOLTBL ON LOAN.LOAN504POOLTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOAN504POOLTBL ADD (
  CHECK ('LOANAPPCREATDT' IS NOT NULL)
  ENABLE VALIDATE,
  CHECK ('LOANAPPCREATUSERID' IS NOT NULL)
  ENABLE VALIDATE,
  CHECK ('LOANPOOLAPPNMB' IS NOT NULL)
  ENABLE VALIDATE,
  CHECK ('LOANAPPNMB' IS NOT NULL)
  ENABLE VALIDATE,
  CONSTRAINT XPKLOAN504POOLTBL
  PRIMARY KEY
  (LOANAPPNMB)
  USING INDEX LOAN.XPKLOAN504POOLTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOAN504POOLTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO CNTRNSALATOVA;

GRANT SELECT, UPDATE ON LOAN.LOAN504POOLTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOAN504POOLTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOAN504POOLTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOAN504POOLTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOAN504POOLTBL TO LOANUPDROLE;

DROP TABLE LOAN.TMPDUETBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPDUETBL
(
  LOANNMB                CHAR(10 BYTE)          NOT NULL,
  LOANNEXTINSTLMNTDUEDT  DATE,
  DUEDT                  DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPDUETBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPDUETBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPDUETBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPDUETBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPDUETBL TO LOANSERVLOANPRTUPLOAD;

DROP TABLE LOAN.TMPINTRANSTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPINTRANSTBL
(
  LOANAPPNMB  NUMBER(10)                        NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPINTRANSTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPINTRANSTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPINTRANSTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPINTRANSTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPINTRANSTBL TO LOANSERVLOANPRTUPLOAD;

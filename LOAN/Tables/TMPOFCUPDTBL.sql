DROP TABLE LOAN.TMPOFCUPDTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPOFCUPDTBL
(
  LOANOFC  CHAR(14 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPOFCUPDTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPOFCUPDTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPOFCUPDTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPOFCUPDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPOFCUPDTBL TO LOANSERVLOANPRTUPLOAD;

ALTER TABLE LOAN.LARD_CLNDRDYS_CD_TBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LARD_CLNDRDYS_CD_TBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LARD_CLNDRDYS_CD_TBL
(
  LARD_CLNDRDYS_CD          NUMBER(2)           NOT NULL,
  LARD_CLNDRDYS_DESC_TXT    VARCHAR2(40 BYTE)   NOT NULL,
  LARD_CLNDRDYS_STRTDT      DATE                NOT NULL,
  LARD_CLNDRDYS_ENDDT       DATE,
  LARD_DTL_CREATDT          DATE                NOT NULL,
  LARD_DTL_CREATUSERID      VARCHAR2(32 BYTE)   NOT NULL,
  LARD_DTL_LAST_UPDTDT      DATE                NOT NULL,
  LARD_DTL_LAST_UPDTUSERID  VARCHAR2(32 BYTE)   NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLARDCLNDRDYSCD ON LOAN.LARD_CLNDRDYS_CD_TBL
(LARD_CLNDRDYS_CD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LARD_CLNDRDYS_CD_TBL ADD (
  CONSTRAINT XPKLARDCLNDRDYSCD
  PRIMARY KEY
  (LARD_CLNDRDYS_CD)
  USING INDEX LOAN.XPKLARDCLNDRDYSCD
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO LOANAPP;

GRANT INSERT, SELECT, UPDATE ON LOAN.LARD_CLNDRDYS_CD_TBL TO LOANREAD;

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LARD_CLNDRDYS_CD_TBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LARD_CLNDRDYS_CD_TBL TO SBACDTBLUPDT;

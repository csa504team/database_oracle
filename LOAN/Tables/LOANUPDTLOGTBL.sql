ALTER TABLE LOAN.LOANUPDTLOGTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANUPDTLOGTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANUPDTLOGTBL
(
  LOANUPDTLOGSEQNMB     NUMBER(15),
  LOANAPPNMB            NUMBER(10),
  LOANUPDTFLDNMB        VARCHAR2(10 BYTE),
  LOANUPDTFLDOLDVALTXT  VARCHAR2(255 BYTE),
  LOANUPDTFLDNEWVALTXT  VARCHAR2(255 BYTE),
  CREATUSERID           VARCHAR2(32 BYTE),
  CREATDT               DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.UPDTLOGLOANAPPNMBIDX ON LOAN.LOANUPDTLOGTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.XPKLOANUPDTLOGTBL ON LOAN.LOANUPDTLOGTBL
(LOANUPDTLOGSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANUPDTLOGTBL ADD (
  CONSTRAINT XPKLOANUPDTLOGTBL
  PRIMARY KEY
  (LOANUPDTLOGSEQNMB)
  USING INDEX LOAN.XPKLOANUPDTLOGTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANUPDTLOGTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANUPDTLOGTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANUPDTLOGTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANUPDTLOGTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANUPDTLOGTBL TO LOANSERVLOANPRTUPLOAD;

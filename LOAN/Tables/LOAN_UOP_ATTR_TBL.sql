ALTER TABLE LOAN.LOAN_UOP_ATTR_TBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOAN_UOP_ATTR_TBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOAN_UOP_ATTR_TBL
(
  LOAN_UOP_ATTR_LOANAPPNMB      NUMBER,
  LOAN_UOP_ATTR_LOANPROCDSEQNM  NUMBER(3)       NOT NULL,
  LOAN_UOP_ATTR_ID              VARCHAR2(32 BYTE) NOT NULL,
  LOAN_UOP_ATTR_LOAN_UOPTAM_ID  VARCHAR2(32 BYTE),
  LOAN_UOP_ATTR_VAL             VARCHAR2(80 BYTE) NOT NULL,
  LOAN_UOP_ATTR_CMNT            VARCHAR2(200 BYTE),
  LOAN_UOP_ATTR_CREAT_DT        DATE            NOT NULL,
  LOAN_UOP_ATTR_CREAT_USER_ID   VARCHAR2(32 BYTE) NOT NULL,
  LOAN_UOP_ATTR_UPDT_DT         DATE            NOT NULL,
  LOAN_UOP_ATTR_UPDT_USER_ID    VARCHAR2(32 BYTE) NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPK_LOAN_UOP_ATTR_ID ON LOAN.LOAN_UOP_ATTR_TBL
(LOAN_UOP_ATTR_ID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOAN_UOP_ATTR_TBL ADD (
  CONSTRAINT XPK_LOAN_UOP_ATTR_ID
  PRIMARY KEY
  (LOAN_UOP_ATTR_ID)
  USING INDEX LOAN.XPK_LOAN_UOP_ATTR_ID
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOAN_UOP_ATTR_TBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOAN_UOP_ATTR_TBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOAN_UOP_ATTR_TBL TO CNTRNSALATOVA;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOAN_UOP_ATTR_TBL TO LOANREAD;

GRANT SELECT ON LOAN.LOAN_UOP_ATTR_TBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOAN_UOP_ATTR_TBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOAN_UOP_ATTR_TBL TO SBACDTBLUPDT;

ALTER TABLE LOAN.LOANPRTCMNTHISTRYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANPRTCMNTHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANPRTCMNTHISTRYTBL
(
  LOANHISTRYSEQNMB           NUMBER(10)         NOT NULL,
  LOANPRTCMNTTXT             CLOB               NOT NULL,
  LOANCMNTHISTRYCREATUSERID  VARCHAR2(32 BYTE)  DEFAULT USER                  NOT NULL,
  LOANCMNTHISTRYCREATDT      DATE               DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID             VARCHAR2(32 BYTE)  DEFAULT USER,
  LASTUPDTDT                 DATE               DEFAULT SYSDATE
)
LOB (LOANPRTCMNTTXT) STORE AS BASICFILE (
  TABLESPACE  LOANDATATBS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  PCTVERSION  10
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                 ))
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANPRTCMNTHISTRYTBL ON LOAN.LOANPRTCMNTHISTRYTBL
(LOANHISTRYSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANPRTCMNTHISTRYTBL ADD (
  CONSTRAINT XPKLOANPRTCMNTHISTRYTBL
  PRIMARY KEY
  (LOANHISTRYSEQNMB)
  USING INDEX LOAN.XPKLOANPRTCMNTHISTRYTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANPRTCMNTHISTRYTBL ADD (
  CONSTRAINT LOANHISTRYHASPRTCMNTIFK 
  FOREIGN KEY (LOANHISTRYSEQNMB) 
  REFERENCES LOAN.LOANHISTRYTBL (LOANHISTRYSEQNMB)
  DISABLE NOVALIDATE);

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANPRTCMNTHISTRYTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO STAGE;

GRANT SELECT ON LOAN.LOANPRTCMNTHISTRYTBL TO UPDLOANROLE;

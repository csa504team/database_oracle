DROP TABLE LOAN.TMPPROJTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPPROJTBL
(
  LOANNMB  CHAR(10 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.XPKTMPROJTBL ON LOAN.TMPPROJTBL
(LOANNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

GRANT SELECT ON LOAN.TMPPROJTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPPROJTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPPROJTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPPROJTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPPROJTBL TO LOANSERVLOANPRTUPLOAD;

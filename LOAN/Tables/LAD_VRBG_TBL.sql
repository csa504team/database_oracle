ALTER TABLE LOAN.LAD_VRBG_TBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LAD_VRBG_TBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LAD_VRBG_TBL
(
  LAD_VRBG_ID             VARCHAR2(32 BYTE)     NOT NULL,
  LAD_VRBG_LAD_ID         VARCHAR2(32 BYTE),
  LAD_VRBG_LADVV_ID       NUMBER(10),
  LAD_VRBG_LADV_SCTN_ID   VARCHAR2(32 BYTE)     NOT NULL,
  LAD_VRBG_TXT            VARCHAR2(18 BYTE),
  LAD_VRBG_CMNT           VARCHAR2(200 BYTE),
  LAD_VRBG_CREAT_DT       DATE                  NOT NULL,
  LAD_VRBG_CREAT_USER_ID  VARCHAR2(32 BYTE)     NOT NULL,
  LAD_VRBG_UPDT_DT        DATE                  NOT NULL,
  LAD_VRBG_UPDT_USER_ID   VARCHAR2(32 BYTE)     NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLAD_VRBG_ID ON LOAN.LAD_VRBG_TBL
(LAD_VRBG_ID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LAD_VRBG_TBL ADD (
  CONSTRAINT XPKLAD_VRBG_ID
  PRIMARY KEY
  (LAD_VRBG_ID)
  USING INDEX LOAN.XPKLAD_VRBG_ID
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LAD_VRBG_TBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LAD_VRBG_TBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LAD_VRBG_TBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LAD_VRBG_TBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LAD_VRBG_TBL TO LOANSERVLOANPRTUPLOAD;

DROP TABLE LOAN.TT_LOANDLCSCMNTTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TT_LOANDLCSCMNTTBL
(
  LOANAPPNMB        NUMBER(10),
  LOANCHRONCMNTTXT  CHAR(180 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.XPKTT_LOANDLCSCMNTTBL ON LOAN.TT_LOANDLCSCMNTTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

GRANT SELECT ON LOAN.TT_LOANDLCSCMNTTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TT_LOANDLCSCMNTTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TT_LOANDLCSCMNTTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TT_LOANDLCSCMNTTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TT_LOANDLCSCMNTTBL TO LOANSERVLOANPRTUPLOAD;

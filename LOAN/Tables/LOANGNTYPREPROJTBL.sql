ALTER TABLE LOAN.LOANGNTYPREPROJTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANGNTYPREPROJTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANGNTYPREPROJTBL
(
  LOANAPPNMB                  NUMBER(10)        NOT NULL,
  LOANAPPPROJSTR1NM           VARCHAR2(80 BYTE) NOT NULL,
  LOANAPPPROJSTR2NM           VARCHAR2(80 BYTE),
  LOANAPPPROJCTYNM            VARCHAR2(40 BYTE) NOT NULL,
  LOANAPPPROJCNTYCD           CHAR(3 BYTE)      NOT NULL,
  LOANAPPPROJSTCD             CHAR(2 BYTE)      NOT NULL,
  LOANAPPPROJZIPCD            CHAR(5 BYTE)      NOT NULL,
  LOANAPPPROJZIP4CD           CHAR(4 BYTE),
  LOANAPPRURALURBANIND        CHAR(1 BYTE),
  LOANAPPNETEXPRTAMT          NUMBER(19,4),
  LOANAPPCURREMPQTY           NUMBER(10),
  LOANAPPJOBCREATQTY          NUMBER(10),
  LOANAPPJOBRTND              NUMBER(10),
  LOANAPPJOBRQMTMETIND        CHAR(1 BYTE),
  LOANAPPCDCJOBRAT            NUMBER(6,3),
  LOANAPPPROJCREATUSERID      CHAR(15 BYTE)     NOT NULL,
  LOANAPPPROJCREATDT          DATE              NOT NULL,
  LOANAPPSBARMBRSFEEAMT       NUMBER(19,4),
  LASTUPDTUSERID              CHAR(15 BYTE)     DEFAULT USER,
  LASTUPDTDT                  DATE              DEFAULT SYSDATE,
  LOANFINANCLSTMTDUEDT        DATE,
  LOANFINANCLSTMTFREQCD       CHAR(1 BYTE),
  LOANFINANCLSTMTFREQDTLDESC  VARCHAR2(255 BYTE),
  UCCLIENRFILESTDT            DATE,
  UCCLIENRFILECNTYDT          DATE,
  YRBTWNSTRFILE               NUMBER(10),
  YRBTWNCNTYRFILE             NUMBER(10),
  UCCLIENRFILELOCALDT         DATE,
  YRBTWNLOCALRFILE            NUMBER(10),
  LOANAPPRURALINITIATVIND     CHAR(1 BYTE),
  LOANAPPPROJGEOCD            CHAR(11 BYTE),
  LOANAPPHUBZONEIND           CHAR(1 BYTE),
  LOANAPPPROJLAT              NUMBER,
  LOANAPPPROJLONG             NUMBER
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANGNTYPREPROJTBL ON LOAN.LOANGNTYPREPROJTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANGNTYPREPROJTBL ADD (
  CONSTRAINT XPKLOANGNTYPREPROJTBL
  PRIMARY KEY
  (LOANAPPNMB)
  USING INDEX LOAN.XPKLOANGNTYPREPROJTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANLANAUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPREPROJTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPREPROJTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYPREPROJTBL TO UPDLOANROLE;

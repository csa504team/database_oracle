ALTER TABLE LOAN.LOANEMAILHISTRYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANEMAILHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANEMAILHISTRYTBL
(
  EMAILHISTRYSEQNMB  NUMBER(10),
  LOANAPPNMB         NUMBER(10),
  EMAILTYPCD         NUMBER(10),
  EMAILBODYTXT       CLOB,
  EMAILSUBJTXT       VARCHAR2(255 BYTE),
  EMAILRCPNTTXT      VARCHAR2(255 BYTE),
  CREATUSERID        VARCHAR2(32 BYTE),
  CREATDT            DATE
)
LOB (EMAILBODYTXT) STORE AS SECUREFILE (
  TABLESPACE  LOANDATATBS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  NOCACHE
  LOGGING)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANEMAILHISTRYTBL ON LOAN.LOANEMAILHISTRYTBL
(EMAILHISTRYSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANEMAILHISTRYTBL ADD (
  CONSTRAINT XPKLOANEMAILHISTRYTBL
  PRIMARY KEY
  (EMAILHISTRYSEQNMB)
  USING INDEX LOAN.XPKLOANEMAILHISTRYTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANLANAUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANEMAILHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANEMAILHISTRYTBL TO LOANUPDROLE;

GRANT SELECT, UPDATE ON LOAN.LOANEMAILHISTRYTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO READLOANROLE;

GRANT INSERT, SELECT ON LOAN.LOANEMAILHISTRYTBL TO STAGE;

GRANT SELECT ON LOAN.LOANEMAILHISTRYTBL TO UPDLOANROLE;

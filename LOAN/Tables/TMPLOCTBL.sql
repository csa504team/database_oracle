DROP TABLE LOAN.TMPLOCTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPLOCTBL
(
  LOANNMB  CHAR(10 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPLOCTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPLOCTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPLOCTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPLOCTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPLOCTBL TO LOANSERVLOANPRTUPLOAD;

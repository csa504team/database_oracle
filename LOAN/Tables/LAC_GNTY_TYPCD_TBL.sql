ALTER TABLE LOAN.LAC_GNTY_TYPCD_TBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LAC_GNTY_TYPCD_TBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LAC_GNTY_TYPCD_TBL
(
  LAC_GNTY_TYP_CD          CHAR(2 BYTE)         NOT NULL,
  LAC_GNTY_TYP_DESC_TXT    VARCHAR2(255 BYTE)   NOT NULL,
  LAC_GNTY_TYP_STRTDT      DATE                 NOT NULL,
  LAC_GNTY_TYP_ENDDT       DATE,
  LAC_DTL_CREATDT          DATE                 NOT NULL,
  LAC_DTL_CREATUSERID      VARCHAR2(32 BYTE)    NOT NULL,
  LAC_DTL_LAST_UPDTDT      DATE                 NOT NULL,
  LAC_DTL_LAST_UPDTUSERID  VARCHAR2(32 BYTE)    NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLAC_GNTY_TYPCD ON LOAN.LAC_GNTY_TYPCD_TBL
(LAC_GNTY_TYP_CD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LAC_GNTY_TYPCD_TBL ADD (
  CONSTRAINT XPKLAC_GNTY_TYPCD
  PRIMARY KEY
  (LAC_GNTY_TYP_CD)
  USING INDEX LOAN.XPKLAC_GNTY_TYPCD
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LAC_GNTY_TYPCD_TBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LAC_GNTY_TYPCD_TBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LAC_GNTY_TYPCD_TBL TO CNTRNSALATOVA;

GRANT INSERT, SELECT, UPDATE ON LOAN.LAC_GNTY_TYPCD_TBL TO LOANREAD;

GRANT SELECT ON LOAN.LAC_GNTY_TYPCD_TBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LAC_GNTY_TYPCD_TBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LAC_GNTY_TYPCD_TBL TO SBACDTBLUPDT;

ALTER TABLE LOAN.PERTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.PERTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.PERTBL
(
  TAXID                   CHAR(10 BYTE)         NOT NULL,
  PERFIRSTNM              RAW(50),
  PERFIRSTSRCHNM          VARCHAR2(40 BYTE),
  PERLASTNM               RAW(100),
  PERLASTSRCHNM           VARCHAR2(40 BYTE)     NOT NULL,
  PERINITIALNM            CHAR(1 BYTE),
  PERSFXNM                CHAR(4 BYTE),
  PERTITLTXT              VARCHAR2(40 BYTE),
  PERBRTHDT               DATE,
  PERBRTHCTYNM            VARCHAR2(40 BYTE),
  PERBRTHSTCD             CHAR(2 BYTE),
  PERBRTHCNTCD            CHAR(2 BYTE),
  PERBRTHCNTNM            VARCHAR2(40 BYTE),
  PERUSCITZNIND           CHAR(2 BYTE),
  PERALIENRGSTRTNNMB      VARCHAR2(12 BYTE),
  PERPNDNGLWSUITIND       CHAR(1 BYTE),
  PERAFFILEMPFEDIND       CHAR(1 BYTE),
  PERIOBIND               CHAR(1 BYTE),
  PERINDCTPRLEPROBATNIND  CHAR(1 BYTE),
  PERCRMNLOFFNSIND        CHAR(1 BYTE),
  PERCNVCTIND             CHAR(1 BYTE),
  PERBNKRPTCYIND          CHAR(1 BYTE),
  VETCD                   NUMBER(3),
  GNDRCD                  CHAR(1 BYTE),
  ETHNICCD                CHAR(2 BYTE),
  PERPRIMPHNNMB           RAW(10),
  PERALTPHNNMB            RAW(10),
  PERPRIMEMAILADR         RAW(80),
  PERALTEMAILADR          RAW(80),
  PERPHYADDRSTR1NM        RAW(100),
  PERPHYADDRSTR2NM        RAW(100),
  PERPHYADDRSTRSFXNM      VARCHAR2(5 BYTE),
  PERPHYADDRCTYNM         VARCHAR2(40 BYTE),
  PERPHYADDRSTCD          CHAR(2 BYTE),
  PERPHYADDRSTNM          VARCHAR2(60 BYTE),
  PERPHYADDRCNTCD         CHAR(2 BYTE),
  PERPHYADDRZIPCD         CHAR(5 BYTE),
  PERPHYADDRZIP4CD        CHAR(4 BYTE),
  PERPHYADDRPOSTCD        VARCHAR2(20 BYTE),
  PERMAILADDRSTR1NM       RAW(80),
  PERMAILADDRSTR2NM       RAW(80),
  PERMAILADDRSTRSFXNM     VARCHAR2(5 BYTE),
  PERMAILADDRCTYNM        VARCHAR2(40 BYTE),
  PERMAILADDRSTCD         CHAR(2 BYTE),
  PERMAILADDRSTNM         VARCHAR2(60 BYTE),
  PERMAILADDRCNTCD        CHAR(2 BYTE),
  PERMAILADDRZIPCD        CHAR(5 BYTE),
  PERMAILADDRZIP4CD       CHAR(4 BYTE),
  PERMAILADDRPOSTCD       VARCHAR2(20 BYTE),
  PEREXTRNLCRDTSCORIND    CHAR(1 BYTE),
  IMCRDTSCORSOURCCD       NUMBER(3),
  PEREXTRNLCRDTSCORNMB    NUMBER(10),
  PEREXTRNLCRDTSCORDT     DATE,
  PERPRIMBUSEXPRNCEYRNMB  NUMBER(3),
  PERCREATUSERID          VARCHAR2(32 BYTE)     DEFAULT USER                  NOT NULL,
  PERCREATDT              DATE                  DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID          VARCHAR2(32 BYTE)     DEFAULT USER,
  LASTUPDTDT              DATE                  DEFAULT SYSDATE,
  LAST4TAXID              VARCHAR2(4 BYTE),
  VETCERTIND              CHAR(1 BYTE),
  PERCITZNSHPCNTNM        CHAR(2 BYTE),
  PERFIRSTNM1             RAW(50),
  PERLASTNM1              RAW(50),
  PERPHYADDRSTR2NM1       RAW(100),
  PERLASTNM2              RAW(100),
  PERPHYADDRSTR1NM1       RAW(100),
  PERPRIMEMAILADR1        RAW(80),
  PERPRIMPHNNMB1          RAW(10),
  PERALTEMAILADR1         RAW(80),
  PEROTHRCMNTTXT          VARCHAR2(255 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING
ENABLE ROW MOVEMENT;

COMMENT ON COLUMN LOAN.PERTBL.LAST4TAXID IS 'Last 4 digits of the taxid for use in search screen';


CREATE INDEX LOAN.PERFIRSTSRCHNMNNIND ON LOAN.PERTBL
(PERFIRSTSRCHNM)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.PERLAST4TAXIDIDX ON LOAN.PERTBL
(LAST4TAXID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.PERLASTSRCHNMNNIND ON LOAN.PERTBL
(PERLASTSRCHNM)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.PERPHYADDRZPCDIND ON LOAN.PERTBL
(PERPHYADDRZIPCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.PERSRCHNMIDX ON LOAN.PERTBL
(PERFIRSTSRCHNM, PERLASTSRCHNM, PERPHYADDRSTR1NM)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.XPKLOANPERTBL ON LOAN.PERTBL
(TAXID)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.PERUPDTRIG
    AFTER UPDATE
    ON LOAN.PERTBL
    REFERENCING NEW AS NEW OLD AS OLD
    FOR EACH ROW
DECLARE
    v_Hist_CreatUserId   VARCHAR2 (32);
    --v_PerHistrySeqNmb    NUMBER (12);
    v_TaxId              CHAR (10);
--v_temp               NUMBER (10);
BEGIN
    /*
     Removed Outlawcod and OUTLAWOTHRDSCTXT as per business requirements 07/06/2010 APK
      SP: 10/07/2013: Removed street number and street suffix name as part of appending these to street name changes
      JP - 03/27/2019 CAFSOPER-2614 - changed history created user to record updated user instead of record created user
      JP - 04/10/2019 -- CAFSOPER-2586- History for LOAN.BUSTBL being created by BUS_HIST_TRIG trigger
      */
    v_TaxId := :NEW.TaxId;
    v_Hist_CreatUserId := NVL (:new.lastupdtuserid, USER);                          --    :new.lastupdtuserid;

    --Get the history number based on the loan number and create user id
    --IncrmntSeqNmbCSP ('PerHistry', v_Hist_CreatUserId, v_PerHistrySeqNmb);

    --Insert into person history -- JP commented out 04/10/2019 Historycreated by PER_HIST_TRIG trigger
    /* INSERT INTO PerHistryTbl (PerHistrySeqNmb,
                               TaxId,
                               PerFirstNm,
                               PerLastNm,
                               PerInitialNm,
                               PerSfxNm,
                               PerTitlTxt,
                               PerBrthDt,
                               PerBrthCtyNm,
                               PerBrthStCd,
                               PerBrthCntCd,
                               PerBrthCntNm,
                               PerUSCitznInd,
                               PerAlienRgstrtnNmb,
                               PerPndngLwsuitInd,
                               PerAffilEmpFedInd,
                               PerIOBInd,
                               PerIndctPrleProbatnInd,
                               PerCrmnlOffnsInd,
                               PerCnvctInd,
                               PerBnkrptcyInd,
                               VetCd,
                               GndrCd,
                               EthnicCd,
                               PerPrimPhnNmb,
                               PerAltPhnNmb,
                               PerPrimEmailAdr,
                               PerAltEmailAdr,
                               PerPhyAddrStr1Nm,
                               PerPhyAddrStr2Nm,
                               PerPhyAddrCtyNm,
                               PerPhyAddrStCd,
                               PerPhyAddrStNm                                               -- Added by Pranesh
                                             ,
                               PerPhyAddrCntCd,
                               PerPhyAddrZipCd,
                               PerPhyAddrZip4Cd,
                               PerPhyAddrPostCd                                             -- Added by Pranesh
                                               ,
                               PerMailAddrStr1Nm,
                               PerMailAddrStr2Nm,
                               PerMailAddrCtyNm,
                               PerMailAddrStCd,
                               PerMailAddrStNm                                              -- Added by Pranesh
                                              ,
                               PerMailAddrCntCd,
                               PerMailAddrZipCd,
                               PerMailAddrZip4Cd,
                               PerMailAddrPostCd,
                               PerExtrnlCrdtScorInd,
                               IMCrdtScorSourcCd,
                               PerExtrnlCrdtScorNmb,
                               PerExtrnlCrdtScorDt,
                               PerPrimBusExprnceYrNmb,
                               PerCreatUserId,
                               PerCreatDt,
                               PerHistryCreatUserId,
                               PerHistryCreatDt,
                               LAST4TAXID)                                                  -- Added by Pranesh
          VALUES (v_PerHistrySeqNmb,
                  :OLD.TaxId,
                  :OLD.PerFirstNm,
                  :OLD.PerLastNm,
                  :OLD.PerInitialNm,
                  :OLD.PerSfxNm,
                  :OLD.PerTitlTxt,
                  :OLD.PerBrthDt,
                  :OLD.PerBrthCtyNm,
                  :OLD.PerBrthStCd,
                  :OLD.PerBrthCntCd,
                  :OLD.PerBrthCntNm,
                  :OLD.PerUSCitznInd,
                  :OLD.PerAlienRgstrtnNmb,
                  :OLD.PerPndngLwsuitInd,
                  :OLD.PerAffilEmpFedInd,
                  :OLD.PerIOBInd,
                  :OLD.PerIndctPrleProbatnInd,
                  :OLD.PerCrmnlOffnsInd,
                  :OLD.PerCnvctInd,
                  :OLD.PerBnkrptcyInd,
                  :OLD.VetCd,
                  :OLD.GndrCd,
                  :OLD.EthnicCd,
                  :OLD.PerPrimPhnNmb,
                  :OLD.PerAltPhnNmb,
                  :OLD.PerPrimEmailAdr,
                  :OLD.PerAltEmailAdr,
                  :OLD.PerPhyAddrStr1Nm,
                  :OLD.PerPhyAddrStr2Nm,
                  :OLD.PerPhyAddrCtyNm,
                  :OLD.PerPhyAddrStCd,
                  :OLD.PerPhyAddrStNm                                                       -- Added by Pranesh
                                     ,
                  :OLD.PerPhyAddrCntCd,
                  :OLD.PerPhyAddrZipCd,
                  :OLD.PerPhyAddrZip4Cd,
                  :OLD.PerPhyAddrPostCd                                                     -- Added by Pranesh
                                       ,
                  :OLD.PerMailAddrStr1Nm,
                  :OLD.PerMailAddrStr2Nm,
                  :OLD.PerMailAddrCtyNm,
                  :OLD.PerMailAddrStCd,
                  :OLD.PerMailAddrStNm                                                      -- Added by Pranesh
                                      ,
                  :OLD.PerMailAddrCntCd,
                  :OLD.PerMailAddrZipCd,
                  :OLD.PerMailAddrZip4Cd,
                  :OLD.PerMailAddrPostCd,
                  :OLD.PerExtrnlCrdtScorInd,
                  :OLD.IMCrdtScorSourcCd,
                  :OLD.PerExtrnlCrdtScorNmb,
                  :OLD.PerExtrnlCrdtScorDt,
                  :OLD.PerPrimBusExprnceYrNmb,
                  :OLD.PerCreatUserId,
                  :OLD.PerCreatDt,
                  v_Hist_CreatUserId,
                  SYSDATE,
                  :OLD.LAST4TAXID);
    -- Added by Pranesh

    INSERT INTO PerRaceHistryTbl (PerHistrySeqNmb,
                                  TaxId,
                                  RaceCd,
                                  PerRaceHistryCreatUserId,
                                  PerRaceHistryCreatDt)
        (SELECT v_PerHistrySeqNmb,
                TaxId,
                RaceCd,
                PerRaceCreatUserId,
                PerRaceCreatDt
           FROM PerRaceTbl
          WHERE TaxId = v_TaxId);*/

    IF    (    RTRIM (:NEW.PerPrimPhnNmb) IS NOT NULL
           AND NVL (:OLD.PerPrimPhnNmb, ' ') != :NEW.PerPrimPhnNmb)
       OR (    RTRIM (:NEW.PerAltPhnNmb) IS NOT NULL
           AND NVL (:OLD.PerAltPhnNmb, ' ') != :NEW.PerAltPhnNmb)
       OR (    RTRIM (:NEW.PerPrimEmailAdr) IS NOT NULL
           AND NVL (:OLD.PerPrimEmailAdr, ' ') != :NEW.PerPrimEmailAdr)
       OR (    RTRIM (:NEW.PerAltEmailAdr) IS NOT NULL
           AND NVL (:OLD.PerAltEmailAdr, ' ') != :NEW.PerAltEmailAdr) THEN
        FOR c1 IN (SELECT a.loanappnmb
                     FROM loan.loangntyborrtbl a, loan.dlcstbl b
                    WHERE     a.loanappnmb = b.loanappnmb
                          AND a.LOANBUSPRIMBORRIND = 'Y'
                          AND a.BORRBUSPERIND = 'P'
                          AND a.LOANBORRACTVINACTIND = 'A'
                          AND a.taxid = :NEW.taxid)
        LOOP
            UPDATE loan.dlcstbl
               SET DLCSHOMEPHNNMB = NVL (RTRIM (OCADATAOUT (:NEW.PerPrimPhnNmb)), DLCSHOMEPHNNMB),
                   DLCSCELLPHNNMB = NVL (RTRIM (OCADATAOUT (:NEW.PerAltPhnNmb)), DLCSCELLPHNNMB),
                   DLCSPRIMEMAILADR = NVL (RTRIM (OCADATAOUT (:NEW.PerPrimEmailAdr)), DLCSPRIMEMAILADR),
                   DLCSALTEMAILADR = NVL (RTRIM (OCADATAOUT (:NEW.PerAltEmailAdr)), DLCSALTEMAILADR),
                   LASTUPDTUSERID = :NEW.PerCreatUserId,
                   LASTUPDTDT = SYSDATE
             WHERE loanappnmb = c1.loanappnmb;
        END LOOP;
    END IF;
END;
/


CREATE OR REPLACE TRIGGER LOAN.PER_HIST_TRIG
    AFTER UPDATE
    ON LOAN.PERTBL
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_header    VARCHAR2 (2000);
    v_log_txt   CLOB;
/***********************************************************
 NAME:       PER_HIST_TRIG
 PURPOSE: To generate history for LOAN.PERTBL
 1.0        4/11/2018      JPaul
 -- History for RAW columns will be maintained as string values (CAFSOPER-2586)
******************************************************************************/
BEGIN
    v_header := '<History><TaxId>' || TRIM (:OLD.TAXID) || '</TaxId>';

    v_log_txt := v_header;

    IF NVL (OcaDataOut (:OLD.PERFIRSTNM), 'x') != NVL (OcaDataOut (:NEW.PERFIRSTNM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('FirstName',
                                  OcaDataOut (:OLD.PERFIRSTNM),
                                  OcaDataOut (:NEW.PERFIRSTNM),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERLASTNM), 'x') != NVL (OcaDataOut (:NEW.PERLASTNM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LastName',
                                  OcaDataOut (:OLD.PERLASTNM),
                                  OcaDataOut (:NEW.PERLASTNM),
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PERINITIALNM), 'x') != NVL (TO_CHAR (:NEW.PERINITIALNM), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('MiddleInitial', :OLD.PERINITIALNM, :NEW.PERINITIALNM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PERSFXNM), 'x') != NVL (TO_CHAR (:NEW.PERSFXNM), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('NameSuffix', :OLD.PERSFXNM, :NEW.PERSFXNM, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PERTITLTXT), 'x') != NVL (TO_CHAR (:NEW.PERTITLTXT), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('Title', :OLD.PERTITLTXT, :NEW.PERTITLTXT, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerUSCitznInd), 'x') != NVL (TO_CHAR (:NEW.PerUSCitznInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('USCitznInd', :OLD.PerUSCitznInd, :NEW.PerUSCitznInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerAlienRgstrtnNmb), 'x') != NVL (TO_CHAR (:NEW.PerAlienRgstrtnNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('AlienRgstrtnNmb',
                                  :OLD.PerAlienRgstrtnNmb,
                                  :NEW.PerAlienRgstrtnNmb,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPndngLwsuitInd), 'x') != NVL (TO_CHAR (:NEW.PerPndngLwsuitInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('LawsuitInd', :OLD.PerPndngLwsuitInd, :NEW.PerPndngLwsuitInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerAffilEmpFedInd), 'x') != NVL (TO_CHAR (:NEW.PerAffilEmpFedInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('FedEmplyAffiltInd',
                                  :OLD.PerAffilEmpFedInd,
                                  :NEW.PerAffilEmpFedInd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerIOBInd), 'x') != NVL (TO_CHAR (:NEW.PerIOBInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('IntrstOthBusinessInd', :OLD.PerIOBInd, :NEW.PerIOBInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerIndctPrleProbatnInd), 'x') != NVL (TO_CHAR (:NEW.PerIndctPrleProbatnInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('IndctPrleProbatnInd',
                                  :OLD.PerIndctPrleProbatnInd,
                                  :NEW.PerIndctPrleProbatnInd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerCrmnlOffnsInd), 'x') != NVL (TO_CHAR (:NEW.PerCrmnlOffnsInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('CrmnlOffnsInd', :OLD.PerCrmnlOffnsInd, :NEW.PerCrmnlOffnsInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerCnvctInd), 'x') != NVL (TO_CHAR (:NEW.PerCnvctInd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('ConvictdInd', :OLD.PerCnvctInd, :NEW.PerCnvctInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerBnkrptcyInd), 'x') != NVL (TO_CHAR (:NEW.PerBnkrptcyInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('BnkrptcyInd', :OLD.PerBnkrptcyInd, :NEW.PerBnkrptcyInd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.VetCd), 'x') != NVL (TO_CHAR (:NEW.VetCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('VetCd', :OLD.VetCd, :NEW.VetCd, v_log_txt);
    END IF;

    /* -- following unlikely to change, hence, commented out

        IF NVL (TO_CHAR (:OLD.GndrCd), 'x') != NVL (TO_CHAR (:NEW.GndrCd), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('GndrCd', :OLD.GndrCd, :NEW.GndrCd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.EthnicCd), 'x') != NVL (TO_CHAR (:NEW.EthnicCd), 'x') THEN
                v_log_txt := LOAN.CONCAT_HIST_TXT ('EthnicCd', :OLD.EthnicCd, :NEW.EthnicCd, v_log_txt);
        END IF;
    */

    IF NVL (OcaDataOut (:OLD.PERPRIMPHNNMB), 'x') != NVL (OcaDataOut (:NEW.PERPRIMPHNNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PrimaryPhone',
                                  OcaDataOut (:OLD.PERPRIMPHNNMB),
                                  OcaDataOut (:NEW.PERPRIMPHNNMB),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERALTPHNNMB), 'x') != NVL (OcaDataOut (:NEW.PERALTPHNNMB), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('AlternatePhone',
                                  OcaDataOut (:OLD.PERALTPHNNMB),
                                  OcaDataOut (:NEW.PERALTPHNNMB),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERPRIMEMAILADR), 'x') != NVL (OcaDataOut (:NEW.PERPRIMEMAILADR), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PrimaryEmail',
                                  OcaDataOut (:OLD.PERPRIMEMAILADR),
                                  OcaDataOut (:NEW.PERPRIMEMAILADR),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERALTEMAILADR), 'x') != NVL (OcaDataOut (:NEW.PERALTEMAILADR), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('AlternateEmail',
                                  OcaDataOut (:OLD.PERALTEMAILADR),
                                  OcaDataOut (:NEW.PERALTEMAILADR),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERPHYADDRSTR1NM), 'x') != NVL (OcaDataOut (:NEW.PERPHYADDRSTR1NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysStrtName1',
                                  OcaDataOut (:OLD.PERPHYADDRSTR1NM),
                                  OcaDataOut (:NEW.PERPHYADDRSTR1NM),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERPHYADDRSTR2NM), 'x') != NVL (OcaDataOut (:NEW.PERPHYADDRSTR2NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysStrtName2',
                                  OcaDataOut (:OLD.PERPHYADDRSTR2NM),
                                  OcaDataOut (:NEW.PERPHYADDRSTR2NM),
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrCtyNm), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrCtyNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysCityName', :OLD.PerPhyAddrCtyNm, :NEW.PerPhyAddrCtyNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrStCd), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrStCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PhysStCd', :OLD.PerPhyAddrStCd, :NEW.PerPhyAddrStCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrStNm), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrStNm), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('PhysStNm', :OLD.PerPhyAddrStNm, :NEW.PerPhyAddrStNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrCntCd), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrCntCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysCountryCd', :OLD.PerPhyAddrCntCd, :NEW.PerPhyAddrCntCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrZipCd), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrZipCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysZipCd', :OLD.PerPhyAddrZipCd, :NEW.PerPhyAddrZipCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrZip4Cd), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrZip4Cd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysZip4Cd', :OLD.PerPhyAddrZip4Cd, :NEW.PerPhyAddrZip4Cd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPhyAddrPostCd), 'x') != NVL (TO_CHAR (:NEW.PerPhyAddrPostCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PhysPostalCd', :OLD.PerPhyAddrPostCd, :NEW.PerPhyAddrPostCd, v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERMAILADDRSTR1NM), 'x') != NVL (OcaDataOut (:NEW.PERMAILADDRSTR1NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailStrtName1',
                                  OcaDataOut (:OLD.PERMAILADDRSTR1NM),
                                  OcaDataOut (:NEW.PERMAILADDRSTR1NM),
                                  v_log_txt);
    END IF;

    IF NVL (OcaDataOut (:OLD.PERMAILADDRSTR2NM), 'x') != NVL (OcaDataOut (:NEW.PERMAILADDRSTR2NM), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailStrtName2',
                                  OcaDataOut (:OLD.PERMAILADDRSTR2NM),
                                  OcaDataOut (:NEW.PERMAILADDRSTR2NM),
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrCtyNm), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrCtyNm), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailCityName', :OLD.PerMailAddrCtyNm, :NEW.PerMailAddrCtyNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrStCd), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrStCd), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('MailStCd', :OLD.PerMailAddrStCd, :NEW.PerMailAddrStCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrStNm), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrStNm), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('MailStNm', :OLD.PerMailAddrStNm, :NEW.PerMailAddrStNm, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrCntCd), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrCntCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailCountryCd', :OLD.PerMailAddrCntCd, :NEW.PerMailAddrCntCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrZipCd), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrZipCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailZipCd', :OLD.PerMailAddrZipCd, :NEW.PerMailAddrZipCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrZip4Cd), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrZip4Cd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailZip4Cd', :OLD.PerMailAddrZip4Cd, :NEW.PerMailAddrZip4Cd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerMailAddrPostCd), 'x') != NVL (TO_CHAR (:NEW.PerMailAddrPostCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('MailPostalCd', :OLD.PerMailAddrPostCd, :NEW.PerMailAddrPostCd, v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerExtrnlCrdtScorInd), 'x') != NVL (TO_CHAR (:NEW.PerExtrnlCrdtScorInd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorInd',
                                  :OLD.PerExtrnlCrdtScorInd,
                                  :NEW.PerExtrnlCrdtScorInd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.IMCrdtScorSourcCd), 'x') != NVL (TO_CHAR (:NEW.IMCrdtScorSourcCd), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('CreditScorSourcCd',
                                  :OLD.IMCrdtScorSourcCd,
                                  :NEW.IMCrdtScorSourcCd,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerExtrnlCrdtScorNmb), 'x') != NVL (TO_CHAR (:NEW.PerExtrnlCrdtScorNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorNmb',
                                  :OLD.PerExtrnlCrdtScorNmb,
                                  :NEW.PerExtrnlCrdtScorNmb,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerExtrnlCrdtScorDt), 'x') != NVL (TO_CHAR (:NEW.PerExtrnlCrdtScorDt), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('ExtrnlCreditScorDt',
                                  :OLD.PerExtrnlCrdtScorDt,
                                  :NEW.PerExtrnlCrdtScorDt,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.PerPrimBusExprnceYrNmb), 'x') != NVL (TO_CHAR (:NEW.PerPrimBusExprnceYrNmb), 'x') THEN
        v_log_txt :=
            LOAN.CONCAT_HIST_TXT ('PrimBusExprnceYrNmb',
                                  :OLD.PerPrimBusExprnceYrNmb,
                                  :NEW.PerPrimBusExprnceYrNmb,
                                  v_log_txt);
    END IF;

    IF NVL (TO_CHAR (:OLD.LAST4TAXID), 'x') != NVL (TO_CHAR (:NEW.LAST4TAXID), 'x') THEN
        v_log_txt := LOAN.CONCAT_HIST_TXT ('LAST4TAXID', :OLD.LAST4TAXID, :NEW.LAST4TAXID, v_log_txt);
    END IF;

    /*
        -- The following never going to change, hence commented out
        IF NVL (TO_CHAR (:OLD.PERBRTHDT), 'x') != NVL (TO_CHAR (:NEW.PERBRTHDT), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('BirthDt', :OLD.PERBRTHDT, :NEW.PERBRTHDT, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.PERBRTHCTYNM), 'x') != NVL (TO_CHAR (:NEW.PERBRTHCTYNM), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('BirthCityName', :OLD.PERBRTHCTYNM, :NEW.PERBRTHCTYNM, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.PERBRTHSTCD), 'x') != NVL (TO_CHAR (:NEW.PERBRTHSTCD), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('BirthStCd', :OLD.PERBRTHSTCD, :NEW.PERBRTHSTCD, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.PERBRTHCNTCD), 'x') != NVL (TO_CHAR (:NEW.PERBRTHCNTCD), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('BirthCountryCd', :OLD.PERBRTHCNTCD, :NEW.PERBRTHCNTCD, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.PerBrthCntNm), 'x') != NVL (TO_CHAR (:NEW.PerBrthCntNm), 'x') THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('BirthCntryName', :OLD.PerBrthCntNm, :NEW.PerBrthCntNm, v_log_txt);
        END IF;
      */

    -- insert only if there is history
    IF v_log_txt != v_header THEN
        v_log_txt := v_log_txt || '</History>';

        INSERT INTO LOAN.LoanAudtLogTbl (LOANAUDTLOGSEQNMB,
                                         TAXID,
                                         BUSPERIND,
                                         LOANAUDTLOGTXT,
                                         LOANUPDTUSERID,
                                         LOANUPDTDT)
             VALUES ((SELECT MAX (LoanAudtLogSeqNmb) + 1 FROM loan.LoanAudtLogTbl),
                     :OLD.TAXID,
                     'P',                                                                    --:OLD.BUSPERIND,
                     v_log_txt,
                     NVL (:NEW.LASTUPDTUSERID, USER),
                     SYSDATE);
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Error code ' || SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 64));
        RAISE;
END PER_HIST_TRIG;
/


ALTER TABLE LOAN.PERTBL ADD (
  CONSTRAINT XPKLOANPERTBL
  PRIMARY KEY
  (TAXID)
  USING INDEX LOAN.XPKLOANPERTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.PERTBL ADD (
  CONSTRAINT CITZNSHPCDFORPERCURR 
  FOREIGN KEY (PERUSCITZNIND) 
  REFERENCES SBAREF.CITZNSHIPCDTBL (CITZNSHIPCD)
  ENABLE VALIDATE,
  CONSTRAINT CRDTSCORSOURCFORPERNFK 
  FOREIGN KEY (IMCRDTSCORSOURCCD) 
  REFERENCES SBAREF.IMCRDTSCORSOURCTBL (IMCRDTSCORSOURCCD)
  ENABLE VALIDATE,
  CONSTRAINT ETHNICCDFORPERNFK 
  FOREIGN KEY (ETHNICCD) 
  REFERENCES SBAREF.ETHNICCDTBL (ETHNICCD)
  ENABLE VALIDATE,
  CONSTRAINT GNDRCDFORPERNFK 
  FOREIGN KEY (GNDRCD) 
  REFERENCES SBAREF.GNDRCDTBL (GNDRCD)
  ENABLE VALIDATE,
  CONSTRAINT PERTBLFK 
  FOREIGN KEY (PERCITZNSHPCNTNM) 
  REFERENCES SBAREF.IMCNTRYCDTBL (IMCNTRYCD)
  ENABLE VALIDATE,
  CONSTRAINT VETCDFORPERNFK 
  FOREIGN KEY (VETCD) 
  REFERENCES SBAREF.VETTBL (VETCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.PERTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.PERTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.PERTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.PERTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.PERTBL TO CNTRRDOYLE;

GRANT SELECT ON LOAN.PERTBL TO CNTRVRAJAN;

GRANT SELECT ON LOAN.PERTBL TO GPTS;

GRANT SELECT ON LOAN.PERTBL TO LANAREAD;

GRANT SELECT ON LOAN.PERTBL TO LANAUPDATE;

GRANT REFERENCES, SELECT ON LOAN.PERTBL TO LOANACCT;

GRANT SELECT ON LOAN.PERTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANAPP;

GRANT SELECT ON LOAN.PERTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANPOSTSERVSU;

GRANT INSERT, SELECT, UPDATE ON LOAN.PERTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.PERTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.PERTBL TO LOANREAD;

GRANT SELECT ON LOAN.PERTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANREADROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.PERTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.PERTBL TO LOANUPDT;

GRANT UPDATE ON LOAN.PERTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.PERTBL TO READLOANROLE;

GRANT INSERT, SELECT ON LOAN.PERTBL TO STAGE;

GRANT SELECT ON LOAN.PERTBL TO UPDLOANROLE;

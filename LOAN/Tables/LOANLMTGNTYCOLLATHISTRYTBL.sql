DROP TABLE LOAN.LOANLMTGNTYCOLLATHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANLMTGNTYCOLLATHISTRYTBL
(
  LOANHISTRYSEQNMB  NUMBER(10),
  TAXID             CHAR(10 BYTE),
  BUSPERIND         CHAR(1 BYTE),
  LOANCOLLATSEQNMB  NUMBER(10),
  CREATUSERID       VARCHAR2(32 BYTE),
  CREATDT           DATE,
  LASTUPDTUSERID    VARCHAR2(32 BYTE),
  LASTUPDTDT        DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANLMTGNTYCOLLATHISTRYTBL TO UPDLOANROLE;

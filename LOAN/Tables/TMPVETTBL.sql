DROP TABLE LOAN.TMPVETTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPVETTBL
(
  LOANAPPNMB  NUMBER(10)                        NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TMPVETTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPVETTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPVETTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPVETTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPVETTBL TO LOANSERVLOANPRTUPLOAD;

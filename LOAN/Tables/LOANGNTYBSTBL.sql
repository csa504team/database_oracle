ALTER TABLE LOAN.LOANGNTYBSTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANGNTYBSTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANGNTYBSTBL
(
  LOANAPPNMB              NUMBER(10)            NOT NULL,
  LOANBSFY                CHAR(4 BYTE)          NOT NULL,
  LOANBSDT                DATE,
  LOANBSCASHEQVLNTAMT     NUMBER(19,4),
  LOANBSNETTRDRECVAMT     NUMBER(19,4),
  LOANBSTOTINVTRYAMT      NUMBER(19,4),
  LOANBSOTHCURASSETAMT    NUMBER(19,4),
  LOANBSTOTFIXASSETAMT    NUMBER(19,4),
  LOANBSTOTOTHASSETAMT    NUMBER(19,4),
  LOANBSTOTCURASSETAMT    NUMBER(19,4),
  LOANBSACCTSPAYBLAMT     NUMBER(19,4),
  LOANBSTOTASSETAMT       NUMBER(19,4)          NOT NULL,
  LOANBSCURLTDAMT         NUMBER(19,4),
  LOANBSOTHCURLIABAMT     NUMBER(19,4),
  LOANBSTOTCURLIABAMT     NUMBER(19,4),
  LOANBSLTDAMT            NUMBER(19,4),
  LOANBSOTHLTLIABAMT      NUMBER(19,4),
  LOANBSSTBYDBT           NUMBER(19,4),
  LOANBSTOTLIAB           NUMBER(19,4),
  LOANBSBUSNETWRTH        NUMBER(19,4),
  LOANBSTNGBLNETWRTH      NUMBER(19,4),
  LOANBSACTLPRFRMAIND     CHAR(1 BYTE),
  LOANFINANCLSTMTSOURCCD  NUMBER(3),
  LOANBSCREATUSERID       VARCHAR2(32 BYTE)     NOT NULL,
  LOANBSCREATDT           DATE                  NOT NULL,
  LASTUPDTUSERID          VARCHAR2(32 BYTE)     DEFAULT USER,
  LASTUPDTDT              DATE                  DEFAULT SYSDATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.LOANAPPNMBUCIND ON LOAN.LOANGNTYBSTBL
(LOANAPPNMB, LOANBSFY)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LOANGNTYBSTBL ADD (
  CONSTRAINT LOANAPPNMBUCIND
  PRIMARY KEY
  (LOANAPPNMB, LOANBSFY)
  USING INDEX LOAN.LOANAPPNMBUCIND
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANGNTYBSTBL ADD (
  CONSTRAINT GNTYBSHASFINANCLSTMTSOURC 
  FOREIGN KEY (LOANFINANCLSTMTSOURCCD) 
  REFERENCES SBAREF.LOANFINANCLSTMTSOURCTBL (LOANFINANCLSTMTSOURCCD)
  ENABLE VALIDATE,
  CONSTRAINT LOANGNTYHASLOANGNTYBS 
  FOREIGN KEY (LOANAPPNMB) 
  REFERENCES LOAN.LOANGNTYTBL (LOANAPPNMB)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYBSTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANLANAUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYBSTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYBSTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYBSTBL TO UPDLOANROLE;

ALTER TABLE LOAN.COLLATPURSALETYPCDTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.COLLATPURSALETYPCDTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.COLLATPURSALETYPCDTBL
(
  COLLATPURSALETYPCD       CHAR(1 BYTE)         NOT NULL,
  COLLATPURSALETYPDESCTXT  VARCHAR2(50 BYTE)    NOT NULL,
  COLLATPURSALETYPSTARTDT  DATE                 NOT NULL,
  COLLATPURSALETYPENDDT    DATE,
  CREATUSERID              VARCHAR2(32 BYTE)    DEFAULT user                  NOT NULL,
  CREATDT                  DATE                 DEFAULT sysdate               NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKCOLLATPURSALETYPCDTBL ON LOAN.COLLATPURSALETYPCDTBL
(COLLATPURSALETYPCD)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.COLLATPURSALETYPCDTBL ADD (
  CONSTRAINT XPKCOLLATPURSALETYPCDTBL
  PRIMARY KEY
  (COLLATPURSALETYPCD)
  USING INDEX LOAN.XPKCOLLATPURSALETYPCDTBL
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.COLLATPURSALETYPCDTBL TO LOANSERVLOANPRTUPLOAD;

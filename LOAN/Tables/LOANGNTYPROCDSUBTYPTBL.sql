CREATE TABLE LOAN.LOANGNTYPROCDSUBTYPTBL
(
  LOANAPPNMB             NUMBER(10)             NOT NULL,
  LOANPROCDTYPCD         CHAR(3 BYTE)           NOT NULL,
  LOANGNTYPROCDSUBTYPID  NUMBER                 NOT NULL,
  LOANPROCDTEXTBLOCK     VARCHAR2(500 BYTE),
  LOANPROCDAMT           NUMBER(19,4),
  LOANPROCDADDR          VARCHAR2(500 BYTE),
  PROCDSUBTYLKUPID       NUMBER(10),
  LOANPROCDDESC          VARCHAR2(200 BYTE),
  CREATUSERID            CHAR(15 BYTE)          DEFAULT user,
  CREATDT                DATE                   DEFAULT sysdate,
  LASTUPDTUSERID         CHAR(15 BYTE)          DEFAULT user,
  LASTUPDTDT             DATE                   DEFAULT sysdate,
  LENDER                 VARCHAR2(80 BYTE)      DEFAULT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


--  There is no statement for index LOAN.SYS_C00215261.
--  The object is created when the parent object is created.

ALTER TABLE LOAN.LOANGNTYPROCDSUBTYPTBL ADD (
  PRIMARY KEY
  (LOANGNTYPROCDSUBTYPID)
  USING INDEX
    TABLESPACE LOANDATATBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);


GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANORIGHQOVERRIDE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANSERVSBICGOV;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYPROCDSUBTYPTBL TO UPDLOANROLE;

CREATE TABLE LOAN.LOANAGNTDOCTBL
(
  LOANAPPNMB      NUMBER(10)                    NOT NULL,
  LOANAGNTSEQNMB  NUMBER(10)                    NOT NULL,
  LOANAGNTID      NUMBER(10)                    NOT NULL,
  DOCID           NUMBER(10)                    NOT NULL,
  LOANAGNTDOCID   NUMBER(3)                     NOT NULL,
  CREATUSERID     VARCHAR2(32 BYTE)             DEFAULT user,
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             DEFAULT user,
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;



GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO CDCONLINEREADALLROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTDOCTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANORIGHQOVERRIDE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTDOCTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANSERVSBICGOV;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANAGNTDOCTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO LOANUPDT;

GRANT SELECT ON LOAN.LOANAGNTDOCTBL TO POOLSECADMINROLE;

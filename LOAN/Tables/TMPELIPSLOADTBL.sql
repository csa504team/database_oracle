DROP TABLE LOAN.TMPELIPSLOADTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TMPELIPSLOADTBL
(
  LOANNMB  CHAR(10 BYTE),
  ERRMSG   VARCHAR2(255 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.XPKTMPELIPSLOADTBL ON LOAN.TMPELIPSLOADTBL
(LOANNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

GRANT SELECT ON LOAN.TMPELIPSLOADTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TMPELIPSLOADTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TMPELIPSLOADTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TMPELIPSLOADTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TMPELIPSLOADTBL TO LOANSERVLOANPRTUPLOAD;

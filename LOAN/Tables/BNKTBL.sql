ALTER TABLE LOAN.BNKTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.BNKTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.BNKTBL
(
  TAXID            CHAR(10 BYTE)                NOT NULL,
  BUSPERIND        CHAR(1 BYTE)                 NOT NULL,
  ACHROUTINGNMB    VARCHAR2(9 BYTE),
  ACHACCTNMB       VARCHAR2(20 BYTE),
  ACHACCTTYPCD     CHAR(1 BYTE),
  CREATUSERID      VARCHAR2(32 BYTE)            NOT NULL,
  CREATDT          DATE                         NOT NULL,
  LASTUPDTUSERID   VARCHAR2(32 BYTE)            NOT NULL,
  LASTUPDTDT       DATE                         NOT NULL,
  ACHPYMNTDISBIND  CHAR(1 BYTE)                 NOT NULL,
  ACHBNKNM         VARCHAR2(255 BYTE),
  ACHDEPOSITORNM   VARCHAR2(255 BYTE),
  LOANAPPNMB       NUMBER(10)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKBNKTBL ON LOAN.BNKTBL
(LOANAPPNMB, TAXID, BUSPERIND, ACHPYMNTDISBIND)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.BNKUPDTRIG
   AFTER UPDATE
   ON LOAN.BnkTbl
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
DECLARE
   --BJR 08/27/2017 -- OPSMDEV1501 -- Added Bank Name and Depositor Name to update
   v_CreatUserId     CHAR (15);
   v_BnkHistrySeqNmb NUMBER;
   v_TaxId           CHAR (10);
   v_temp            NUMBER;
BEGIN
   v_TaxId        := :New.TaxId;
   v_CreatUserId  := :NEW.CreatUserId;

   SELECT COUNT (*)
   INTO   v_temp
   FROM   BnkHistryTbl
   WHERE      TaxId = v_TaxId
          AND CreatUserId = v_CreatUserId
          AND TO_CHAR (CreatDt, 'mmddyyyy') = TO_CHAR (SYSDATE, 'mmddyyyy');

   IF v_temp > 0 THEN
      --History already exists
      RETURN;
   END IF;

   --Get the history number based on the loan number and create user id
   IncrmntSeqNmbCSP ('BnkHistry', v_CreatUserId, v_BnkHistrySeqNmb);

   --Insert into business history
   INSERT INTO BnkHistryTbl (BnkHistrySeqNmb,
                             LOANAPPNMB,
                             TAXID,
                             BUSPERIND,
                             ACHROUTINGNMB,
                             ACHACCTNMB,
                             ACHACCTTYPCD,
                             CREATUSERID,
                             CREATDT,
                             LASTUPDTUSERID,
                             LASTUPDTDT,
                             ACHPYMNTDISBIND,
                             ACHBnkNm,
                             ACHDepositorNm)
   VALUES      (v_BnkHistrySeqNmb,
                :OLD.LoanAppNmb,
                :OLD.TAXID,
                :OLD.BUSPERIND,
                :OLD.ACHROUTINGNMB,
                :OLD.ACHACCTNMB,
                :OLD.ACHACCTTYPCD,
                :OLD.CREATUSERID,
                :OLD.CREATDT,
                :OLD.LASTUPDTUSERID,
                :OLD.LASTUPDTDT,
                :OLD.ACHPYMNTDISBIND,
                :OLD.ACHBnkNm,
                :OLD.ACHDepositorNm);
END;
/


ALTER TABLE LOAN.BNKTBL ADD (
  CONSTRAINT XPKBNKTBL
  PRIMARY KEY
  (LOANAPPNMB, TAXID, BUSPERIND, ACHPYMNTDISBIND)
  USING INDEX LOAN.XPKBNKTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.BNKTBL ADD (
  CONSTRAINT BNKHASACHACCTTYPCD 
  FOREIGN KEY (ACHACCTTYPCD) 
  REFERENCES SBAREF.ACHACCTTYPTBL (ACHACCTTYPCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.BNKTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.BNKTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.BNKTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.BNKTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANACCT;

GRANT SELECT ON LOAN.BNKTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANAPP;

GRANT SELECT ON LOAN.BNKTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANCMNTREADALLROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANCMNTWRITEROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANLOOKUPUPDROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.BNKTBL TO LOANREADALLROLE;

GRANT DELETE, SELECT, UPDATE ON LOAN.BNKTBL TO LOANSERVCSAUPDATE;

GRANT SELECT ON LOAN.BNKTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, SELECT, UPDATE ON LOAN.BNKTBL TO LOANSERVPRTRECV;

GRANT DELETE, SELECT, UPDATE ON LOAN.BNKTBL TO LOANSERVSBAAGENT;

GRANT SELECT ON LOAN.BNKTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.BNKTBL TO POOLSECADMINROLE;

GRANT SELECT ON LOAN.BNKTBL TO READLOANROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.BNKTBL TO STAGE;

GRANT SELECT ON LOAN.BNKTBL TO UPDLOANROLE;

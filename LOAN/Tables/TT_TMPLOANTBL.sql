DROP TABLE LOAN.TT_TMPLOANTBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE LOAN.TT_TMPLOANTBL
(
  LOANAPPNMB  NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON LOAN.TT_TMPLOANTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO GPTS;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO LOANACCT;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TT_TMPLOANTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT ON LOAN.TT_TMPLOANTBL TO STAGE;

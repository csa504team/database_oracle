DROP TABLE LOAN.LLTSUSERMAPTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LLTSUSERMAPTBL
(
  IMUSERLASTNM        VARCHAR2(80 BYTE),
  IMUSERFIRSTNM       VARCHAR2(80 BYTE),
  IMUSERMIDNM         VARCHAR2(40 BYTE),
  SBAOFCCD            CHAR(4 BYTE),
  IMUSERACTINACTVIND  CHAR(1 BYTE),
  IMUSERCREATDT       DATE,
  IMUSERNM            VARCHAR2(32 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.LLTSUSERMAPIDIND ON LOAN.LLTSUSERMAPTBL
(IMUSERNM)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX LOAN.LLTSUSERMAPNMIND ON LOAN.LLTSUSERMAPTBL
(IMUSERFIRSTNM, IMUSERLASTNM, SBAOFCCD)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO LLTSREADALLROLE;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LLTSUSERMAPTBL TO STAGE;

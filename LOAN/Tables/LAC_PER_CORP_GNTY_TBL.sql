ALTER TABLE LOAN.LAC_PER_CORP_GNTY_TBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LAC_PER_CORP_GNTY_TBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LAC_PER_CORP_GNTY_TBL
(
  LOAN_AUTH_ID             VARCHAR2(32 BYTE)    NOT NULL,
  LOAN_COLLAT_SEQ_NMB      NUMBER(3),
  LAC_PER_CORP_SEQ_NMB     NUMBER(3)            NOT NULL,
  LAC_COND_TYP_CD          NUMBER(3)            NOT NULL,
  LAC_GNTR_NM              VARCHAR2(255 BYTE),
  LAC_GNTR_STCD            CHAR(2 BYTE),
  LAC_GNTR_STCNTRY_TXT     VARCHAR2(80 BYTE),
  LAC_ADDTNL_SEC_IND       CHAR(1 BYTE),
  LAC_GNTY_TYP_CD          CHAR(2 BYTE),
  LAC_GNTY_LMT_TXT         VARCHAR2(80 BYTE),
  LAC_DTL_CREATDT          DATE                 NOT NULL,
  LAC_DTL_CREATUSERID      VARCHAR2(32 BYTE)    NOT NULL,
  LAC_DTL_LAST_UPDTDT      DATE                 NOT NULL,
  LAC_DTL_LAST_UPDTUSERID  VARCHAR2(32 BYTE)    NOT NULL,
  BAL_RDCTN_LMT_AMT        NUMBER(19,2),
  PRIN_RDCTN_LMT_AMT       NUMBER(19,2),
  MAX_LIAB_LMT_AMT         NUMBER(19,2),
  MAX_LMT_PCT              NUMBER(6,3),
  YEAR_LMT_NMB             NUMBER(3)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLACPERCORPGNTY ON LOAN.LAC_PER_CORP_GNTY_TBL
(LOAN_AUTH_ID, LAC_PER_CORP_SEQ_NMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.LAC_PER_CORP_GNTY_TBL ADD (
  CONSTRAINT XPKLACPERCORPGNTY
  PRIMARY KEY
  (LOAN_AUTH_ID, LAC_PER_CORP_SEQ_NMB)
  USING INDEX LOAN.XPKLACPERCORPGNTY
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO CNTRNSALATOVA;

GRANT INSERT, SELECT, UPDATE ON LOAN.LAC_PER_CORP_GNTY_TBL TO LOANREAD;

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOAN.LAC_PER_CORP_GNTY_TBL TO LOANUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LAC_PER_CORP_GNTY_TBL TO SBACDTBLUPDT;

DROP TABLE LOAN.LOANPREASSOCTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANPREASSOCTBL
(
  LOANAPPNMB               NUMBER(10)           NOT NULL,
  LOANASSOCSEQNMB          NUMBER(3)            NOT NULL,
  TAXID                    CHAR(10 BYTE),
  LOANASSOCTYPCD           NUMBER(3),
  LOANASSOCBUSPERIND       CHAR(1 BYTE),
  LOANASSOCCMNTTXT         VARCHAR2(255 BYTE),
  LOANASSOCACTVINACTIND    CHAR(1 BYTE),
  LOANASSOCCREATUSERID     VARCHAR2(32 BYTE),
  LOANASSOCCREATDT         DATE,
  LOANASSOCLASTUPDTUSERID  VARCHAR2(32 BYTE),
  LOANASSOCLASTUPDTDT      DATE
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.LOANPREASSOCTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANPREASSOCTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANPREASSOCTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANPREASSOCTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANPREASSOCTBL TO LOANSERVLOANPRTUPLOAD;

ALTER TABLE LOAN.LOANGNTYPARTLENDRTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANGNTYPARTLENDRTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANGNTYPARTLENDRTBL
(
  LOANAPPNMB                    NUMBER(10)      NOT NULL,
  LOANPARTLENDRSEQNMB           NUMBER(10)      NOT NULL,
  LOANPARTLENDRTYPCD            CHAR(1 BYTE),
  LOANPARTLENDRAMT              NUMBER(19,4),
  LOCID                         NUMBER(7),
  LOANPARTLENDRNM               VARCHAR2(200 BYTE) NOT NULL,
  LOANPARTLENDRSTR1NM           VARCHAR2(80 BYTE),
  LOANPARTLENDRSTR2NM           VARCHAR2(80 BYTE),
  LOANPARTLENDRCTYNM            VARCHAR2(40 BYTE),
  LOANPARTLENDRSTCD             CHAR(2 BYTE),
  LOANPARTLENDRZIP5CD           CHAR(5 BYTE),
  LOANPARTLENDRZIP4CD           CHAR(4 BYTE),
  LOANPARTLENDROFCRLASTNM       RAW(40),
  LOANPARTLENDROFCRFIRSTNM      RAW(40),
  LOANPARTLENDROFCRINITIALNM    CHAR(1 BYTE),
  LOANPARTLENDROFCRSFXNM        CHAR(4 BYTE),
  LOANPARTLENDROFCRTITLTXT      VARCHAR2(40 BYTE),
  LOANPARTLENDROFCRPHNNMB       RAW(20),
  LOANLIENPOSCD                 NUMBER(3),
  LOANPARTLENDRSRLIENPOSFEEAMT  NUMBER(19,4),
  LOANLENDRTAXID                CHAR(10 BYTE),
  LOANLENDRGROSSINTPCT          NUMBER(6,3),
  LOANLENDRSERVFEEPCT           NUMBER(6,3),
  LOANLENDRPARTPCT              NUMBER(8,5),
  LOANORIGPARTPCT               NUMBER(8,5),
  LOANSBAGNTYBALPCT             NUMBER(8,5),
  LOANLENDRPARTAMT              NUMBER(19,4),
  LOANORIGPARTAMT               NUMBER(19,4),
  LOANLENDRGROSSAMT             NUMBER(19,4),
  LOANSBAGNTYBALAMT             NUMBER(19,4),
  LOAN503POOLAPPNMB             NUMBER(10),
  LOANPARTLENDRCREATUSERID      VARCHAR2(32 BYTE) DEFAULT USER,
  LOANPARTLENDRCREATDT          DATE            DEFAULT sysdate,
  LASTUPDTUSERID                VARCHAR2(32 BYTE) DEFAULT USER,
  LASTUPDTDT                    DATE            DEFAULT SYSDATE,
  LOANLENDRGROSSINITLINTPCT     NUMBER(6,3),
  LOANLENDRGROSSINITLAMT        NUMBER(19,4),
  LOANLENDRPARTINITLAMT         NUMBER(19,4),
  LOANORIGPARTINITLAMT          NUMBER(19,4),
  LOANSBAGNTYBALINITLAMT        NUMBER(19,4),
  LOANNOTENMB                   CHAR(10 BYTE),
  LOANADVNSBALAMT               NUMBER(15,2),
  LOANINTPCT                    NUMBER(6,3),
  LOANINTPAIDAMT                NUMBER(15,2),
  LOANINSTLAMT                  NUMBER(15,2),
  LOANPARTLENDRACCTNM           VARCHAR2(80 BYTE),
  LOANPARTLENDRACCTNMB          VARCHAR2(20 BYTE),
  LOANPARTLENDRTRANCD           VARCHAR2(32 BYTE),
  LOANPARTLENDRATTN             VARCHAR2(255 BYTE),
  LNDRLOANNMB                   VARCHAR2(10 BYTE),
  LOANPARTLENDRSTNM             VARCHAR2(60 BYTE),
  LOANPARTLENDRPOSTCD           VARCHAR2(20 BYTE),
  LOANPARTLENDRCNTRYCD          VARCHAR2(2 BYTE),
  LOANPARTLENDRACCTTYPCD        CHAR(1 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE INDEX LOAN.LOANGNTYPARTLENDRPOOLAPPNMBIND ON LOAN.LOANGNTYPARTLENDRTBL
(LOAN503POOLAPPNMB)
LOGGING
TABLESPACE LOANINDTBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE UNIQUE INDEX LOAN.XPKLOANPRTTBL ON LOAN.LOANGNTYPARTLENDRTBL
(LOANAPPNMB, LOANPARTLENDRSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.LOANGNTYPARTLENDRUPDINSTRIG
   BEFORE INSERT OR
          UPDATE OF LOANPARTLENDRTYPCD,
                    LOANPARTLENDRACCTNM,
                    LOANPARTLENDRACCTNMB,
                    LOANPARTLENDRTRANCD,
                    LOANPARTLENDRATTN,
                    LOANPARTLENDRACCTTYPCD
   ON LOAN.LOANGNTYPARTLENDRTBL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
/******************************************************************************
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/23/2018      JPaul       1. Created this trigger.

   NOTES:
   populate following columns to null when LOANPARTLENDRTYPCD value is other than 'I':
   LOANPARTLENDRACCTNM
   LOANPARTLENDRACCTNMB
   LOANPARTLENDRTRANCD
   LOANPARTLENDRATTN
   LOANPARTLENDRACCTTYPCD
******************************************************************************/
BEGIN
   CASE
      WHEN :new.LOANPARTLENDRTYPCD = 'I' THEN
         -- no action
         NULL;
      WHEN NVL (:new.LOANPARTLENDRTYPCD, 'x') != 'I' THEN
         -- populate columns to null
         :new.LOANPARTLENDRACCTNM     := NULL;
         :new.LOANPARTLENDRACCTNMB    := NULL;
         :new.LOANPARTLENDRTRANCD     := NULL;
         :new.LOANPARTLENDRATTN       := NULL;
         :new.LOANPARTLENDRACCTTYPCD  := NULL;
      WHEN     (    UPDATING
                AND :old.LOANPARTLENDRTYPCD != 'I')
           AND (   :new.LOANPARTLENDRACCTNM IS NOT NULL
                OR :new.LOANPARTLENDRACCTNMB IS NOT NULL
                OR :new.LOANPARTLENDRTRANCD IS NOT NULL
                OR :new.LOANPARTLENDRATTN IS NOT NULL
                OR :new.LOANPARTLENDRACCTTYPCD IS NOT NULL) THEN
         -- populate columns to null
         :new.LOANPARTLENDRACCTNM     := NULL;
         :new.LOANPARTLENDRACCTNMB    := NULL;
         :new.LOANPARTLENDRTRANCD     := NULL;
         :new.LOANPARTLENDRATTN       := NULL;
         :new.LOANPARTLENDRACCTTYPCD  := NULL;
   END CASE;
EXCEPTION
   WHEN OTHERS THEN
      RAISE;
END LoanGntyPartLendrTbl_BIU;
/


CREATE OR REPLACE TRIGGER LOAN.LOANGNTYPARTLENDR_HIST_TRIG
    AFTER UPDATE
    ON LOAN.LoanGntyPartLendrTbl
    REFERENCING OLD AS OLD NEW AS NEW
    FOR EACH ROW
DECLARE
    v_header    VARCHAR2 (2000);
    v_log_txt   CLOB;
    v_loannmb   VARCHAR2 (200);
/***********************************************************
 NAME:       LoanGntyPartLendr_HIST_TRIG
 PURPOSE: To generate history for LOAN.LoanGntyPartLendrTbl
 1.0        Aug 2018      JPaul


 RY--11/19/2018-OPSMDEV-2044:: Added OCADATAOUT to PII columns
 JP - 04/10/2019 -- CAFSOPER-2586 OCADATAOUT used for :New and :Old values of raw columns
                    History of RAW columns will be stored as string 
******************************************************************************/
BEGIN
    FOR rec IN (SELECT MAX (g.LOANNMB)     loannmb
                  FROM LOAN.LOANGNTYTBL g
                 WHERE g.LOANAPPNMB = :OLD.LOANAPPNMB)
    LOOP
        v_loannmb := rec.loannmb;
    END LOOP;

    v_header := '<History><LoanNmb>' || v_loannmb || '</LoanNmb>';
    v_log_txt := v_header;

    BEGIN
        IF NVL (TO_CHAR (:OLD.LoanPartLendrTypCd), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrTypCd), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrTypCd',
                                      :OLD.LoanPartLendrTypCd,
                                      :NEW.LoanPartLendrTypCd,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrAmt), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrAmt), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrAmt', :OLD.LoanPartLendrAmt, :NEW.LoanPartLendrAmt, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LocId), 'x') != NVL (TO_CHAR (:NEW.LocId), 'x')
        THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('LendrLocId', :OLD.LocId, :NEW.LocId, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrNm), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrNm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrName', :OLD.LoanPartLendrNm, :NEW.LoanPartLendrNm, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrStr1Nm), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrStr1Nm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrStr1Nm',
                                      :OLD.LoanPartLendrStr1Nm,
                                      :NEW.LoanPartLendrStr1Nm,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrStr2Nm), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrStr2Nm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrStr2Nm',
                                      :OLD.LoanPartLendrStr2Nm,
                                      :NEW.LoanPartLendrStr2Nm,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrCtyNm), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrCtyNm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrCtyNm',
                                      :OLD.LoanPartLendrCtyNm,
                                      :NEW.LoanPartLendrCtyNm,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrStCd), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrStCd), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrStCd',
                                      :OLD.LoanPartLendrStCd,
                                      :NEW.LoanPartLendrStCd,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrZip5Cd), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrZip5Cd), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrZip5Cd',
                                      :OLD.LoanPartLendrZip5Cd,
                                      :NEW.LoanPartLendrZip5Cd,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrZip4Cd), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrZip4Cd), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrZip4Cd',
                                      :OLD.LoanPartLendrZip4Cd,
                                      :NEW.LoanPartLendrZip4Cd,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (OcaDataOut (:OLD.LoanPartLendrOfcrLastNm)), 'x') !=
           NVL (TO_CHAR (OcaDataOut (:NEW.LoanPartLendrOfcrLastNm)), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LastName',
                                      OcaDataOut (:OLD.LoanPartLendrOfcrLastNm),
                                      OcaDataOut (:NEW.LoanPartLendrOfcrLastNm),
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (OcaDataOut (:OLD.LoanPartLendrOfcrFirstNm)), 'x') !=
           NVL (TO_CHAR (OcaDataOut (:NEW.LoanPartLendrOfcrFirstNm)), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('FirstName',
                                      OcaDataOut (:OLD.LoanPartLendrOfcrFirstNm),
                                      OcaDataOut (:NEW.LoanPartLendrOfcrFirstNm),
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrOfcrInitialNm), 'x') !=
           NVL (TO_CHAR (:NEW.LoanPartLendrOfcrInitialNm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('MiddleInitial',
                                      :OLD.LoanPartLendrOfcrInitialNm,
                                      :NEW.LoanPartLendrOfcrInitialNm,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrOfcrSfxNm), 'x') !=
           NVL (TO_CHAR (:NEW.LoanPartLendrOfcrSfxNm), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('NameSuffix',
                                      :OLD.LoanPartLendrOfcrSfxNm,
                                      :NEW.LoanPartLendrOfcrSfxNm,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrOfcrTitlTxt), 'x') !=
           NVL (TO_CHAR (:NEW.LoanPartLendrOfcrTitlTxt), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('Title',
                                      :OLD.LoanPartLendrOfcrTitlTxt,
                                      :NEW.LoanPartLendrOfcrTitlTxt,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (OcaDataOut (:OLD.LoanPartLendrOfcrPhnNmb)), 'x') !=
           NVL (TO_CHAR (OcaDataOut (:NEW.LoanPartLendrOfcrPhnNmb)), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrPhnNmb',
                                      OcaDataOut (:OLD.LoanPartLendrOfcrPhnNmb),
                                      OcaDataOut (:NEW.LoanPartLendrOfcrPhnNmb),
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanLienPosCd), 'x') != NVL (TO_CHAR (:NEW.LoanLienPosCd), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LienPosition', :OLD.LoanLienPosCd, :NEW.LoanLienPosCd, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrSrLienPosFeeAmt), 'x') !=
           NVL (TO_CHAR (:NEW.LoanPartLendrSrLienPosFeeAmt), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LoanPartLendrSrLienPosFeeAmt',
                                      :OLD.LoanPartLendrSrLienPosFeeAmt,
                                      :NEW.LoanPartLendrSrLienPosFeeAmt,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.Loan503PoolAppNmb), 'x') != NVL (TO_CHAR (:NEW.Loan503PoolAppNmb), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('POOLAPPNMB',
                                      :OLD.Loan503PoolAppNmb,
                                      :NEW.Loan503PoolAppNmb,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRTAXID), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRTAXID), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LOANLENDRTAXID', :OLD.LOANLENDRTAXID, :NEW.LOANLENDRTAXID, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRGROSSINTPCT), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRGROSSINTPCT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('GrossInterestPct',
                                      :OLD.LOANLENDRGROSSINTPCT,
                                      :NEW.LOANLENDRGROSSINTPCT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRSERVFEEPCT), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRSERVFEEPCT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrServFeePct',
                                      :OLD.LOANLENDRSERVFEEPCT,
                                      :NEW.LOANLENDRSERVFEEPCT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRPARTPCT), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRPARTPCT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LOANLENDRPARTPCT',
                                      :OLD.LOANLENDRPARTPCT,
                                      :NEW.LOANLENDRPARTPCT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANORIGPARTPCT), 'x') != NVL (TO_CHAR (:NEW.LOANORIGPARTPCT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ORIGPARTPCT', :OLD.LOANORIGPARTPCT, :NEW.LOANORIGPARTPCT, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanSBAGntyBalPct), 'x') != NVL (TO_CHAR (:NEW.LoanSBAGntyBalPct), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('SBAGNTYBALPCT',
                                      :OLD.LoanSBAGntyBalPct,
                                      :NEW.LoanSBAGntyBalPct,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRPARTAMT), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRPARTAMT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LOANLENDRPARTAMT',
                                      :OLD.LOANLENDRPARTAMT,
                                      :NEW.LOANLENDRPARTAMT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANORIGPARTAMT), 'x') != NVL (TO_CHAR (:NEW.LOANORIGPARTAMT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ORIGPARTAMT', :OLD.LOANORIGPARTAMT, :NEW.LOANORIGPARTAMT, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANLENDRGROSSAMT), 'x') != NVL (TO_CHAR (:NEW.LOANLENDRGROSSAMT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('GrossBalanceAmt',
                                      :OLD.LOANLENDRGROSSAMT,
                                      :NEW.LOANLENDRGROSSAMT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANSBAGNTYBALAMT), 'x') != NVL (TO_CHAR (:NEW.LOANSBAGNTYBALAMT), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LOANSBAGNTYBALAMT',
                                      :OLD.LOANSBAGNTYBALAMT,
                                      :NEW.LOANSBAGNTYBALAMT,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrACCTNM), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrACCTNM), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ACHAccountName',
                                      :OLD.LoanPartLendrACCTNM,
                                      :NEW.LoanPartLendrACCTNM,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrACCTNMB), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrACCTNMB), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ACHAccountNmb',
                                      :OLD.LoanPartLendrACCTNMB,
                                      :NEW.LoanPartLendrACCTNMB,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrTRANCD), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrTRANCD), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ACHRoutingNmb',
                                      :OLD.LoanPartLendrTRANCD,
                                      :NEW.LoanPartLendrTRANCD,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LoanPartLendrATTN), 'x') != NVL (TO_CHAR (:NEW.LoanPartLendrATTN), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ACHAttention',
                                      :OLD.LoanPartLendrATTN,
                                      :NEW.LoanPartLendrATTN,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LNDRLOANNMB), 'x') != NVL (TO_CHAR (:NEW.LNDRLOANNMB), 'x')
        THEN
            v_log_txt := LOAN.CONCAT_HIST_TXT ('LNDRLOANNMB', :OLD.LNDRLOANNMB, :NEW.LNDRLOANNMB, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANPARTLENDRCNTRYCD), 'x') != NVL (TO_CHAR (:NEW.LOANPARTLENDRCNTRYCD), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrCountryCd',
                                      :OLD.LOANPARTLENDRCNTRYCD,
                                      :NEW.LOANPARTLENDRCNTRYCD,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANPARTLENDRPOSTCD), 'x') != NVL (TO_CHAR (:NEW.LOANPARTLENDRPOSTCD), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrPostalCd',
                                      :OLD.LOANPARTLENDRPOSTCD,
                                      :NEW.LOANPARTLENDRPOSTCD,
                                      v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANPARTLENDRSTNM), 'x') != NVL (TO_CHAR (:NEW.LOANPARTLENDRSTNM), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('LendrStNm', :OLD.LOANPARTLENDRSTNM, :NEW.LOANPARTLENDRSTNM, v_log_txt);
        END IF;

        IF NVL (TO_CHAR (:OLD.LOANPARTLENDRACCTTYPCD), 'x') !=
           NVL (TO_CHAR (:NEW.LOANPARTLENDRACCTTYPCD), 'x')
        THEN
            v_log_txt :=
                LOAN.CONCAT_HIST_TXT ('ACHAccountType',
                                      :OLD.LOANPARTLENDRACCTTYPCD,
                                      :NEW.LOANPARTLENDRACCTTYPCD,
                                      v_log_txt);
        END IF;
    END;

    -- insert only if there is history
    IF v_log_txt != v_header
    THEN
        v_log_txt := v_log_txt || '</History>';

        INSERT INTO LOAN.LoanAudtLogTbl (LOANAUDTLOGSEQNMB,
                                         LOANAPPNMB,
                                         LOANAUDTLOGTXT,
                                         LOANUPDTUSERID,
                                         LOANUPDTDT)
             VALUES ((SELECT MAX (LoanAudtLogSeqNmb) + 1 FROM loan.LoanAudtLogTbl),
                     :OLD.LOANAPPNMB,
                     v_log_txt,
                     NVL (:NEW.LASTUPDTUSERID, USER),
                     SYSDATE);
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        DBMS_OUTPUT.PUT_LINE ('Error code ' || SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 64));
        RAISE;
END LoanGntyPartLendr_HIST_TRIG;
/


ALTER TABLE LOAN.LOANGNTYPARTLENDRTBL ADD (
  CONSTRAINT XPKLOANPRTTBL
  PRIMARY KEY
  (LOANAPPNMB, LOANPARTLENDRSEQNMB)
  USING INDEX LOAN.XPKLOANPRTTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANGNTYPARTLENDRTBL ADD (
  CONSTRAINT LIENPOSFORGNTYPARTLENDRNFK 
  FOREIGN KEY (LOANLIENPOSCD) 
  REFERENCES LOANAPP.LOANLIENPOSCDTBL (LOANLIENPOSCD)
  ENABLE VALIDATE,
  CONSTRAINT LOANGNTYHASPARTLENDR 
  FOREIGN KEY (LOANAPPNMB) 
  REFERENCES LOAN.LOANGNTYTBL (LOANAPPNMB)
  ENABLE VALIDATE,
  CONSTRAINT PARTLENDRTYPFORGNTYPARTLENDR 
  FOREIGN KEY (LOANPARTLENDRTYPCD) 
  REFERENCES SBAREF.LOANPARTLENDRTYPTBL (LOANPARTLENDRTYPCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO CNTRNSALATOVA;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPARTLENDRTBL TO CNTRXLIU;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO GPTS;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPARTLENDRTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPARTLENDRTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANGNTYPARTLENDRTBL TO UPDLOANROLE;

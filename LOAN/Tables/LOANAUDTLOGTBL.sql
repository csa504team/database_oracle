DROP TABLE LOAN.LOANAUDTLOGTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANAUDTLOGTBL
(
  LOANAUDTLOGSEQNMB  NUMBER(10)                 NOT NULL,
  LOANAPPNMB         NUMBER(10),
  TAXID              CHAR(10 BYTE),
  BUSPERIND          CHAR(1 BYTE),
  LOANAUDTLOGTXT     CLOB                       NOT NULL,
  LOANUPDTUSERID     VARCHAR2(32 BYTE)          NOT NULL,
  LOANUPDTDT         DATE                       NOT NULL
)
LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
  TABLESPACE  LOANDATA02TBS
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  PCTVERSION  10
  NOCACHE
  LOGGING)
NOCOMPRESS 
TABLESPACE LOANDATA02TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
           )
LOGGING
PARTITION BY RANGE (LOANUPDTDT)
(  
  PARTITION AUDTRANGE02 VALUES LESS THAN (TO_DATE(' 2008-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA02TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03 VALUES LESS THAN (TO_DATE(' 2009-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA03TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04 VALUES LESS THAN (TO_DATE(' 2010-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA04TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05 VALUES LESS THAN (TO_DATE(' 2011-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA05TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06 VALUES LESS THAN (TO_DATE(' 2012-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA06TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07 VALUES LESS THAN (TO_DATE(' 2013-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA07TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08 VALUES LESS THAN (TO_DATE(' 2014-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA08TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          64K
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09 VALUES LESS THAN (TO_DATE(' 2015-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA09TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10 VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    LOB (LOANAUDTLOGTXT) STORE AS BASICFILE (
      TABLESPACE  LOANDATA10TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11 VALUES LESS THAN (TO_DATE(' 2018-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    LOB (LOANAUDTLOGTXT) STORE AS SECUREFILE (
      TABLESPACE  LOANDATA11TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12 VALUES LESS THAN (TO_DATE(' 2019-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    LOB (LOANAUDTLOGTXT) STORE AS SECUREFILE (
      TABLESPACE  LOANDATA12TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    LOB (LOANAUDTLOGTXT) STORE AS SECUREFILE (
      TABLESPACE  LOANDATA10TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14 VALUES LESS THAN (TO_DATE(' 2026-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    LOB (LOANAUDTLOGTXT) STORE AS SECUREFILE (
      TABLESPACE  LOANDATA13TBS
      ENABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING
          STORAGE    (
                      INITIAL          8M
                      NEXT             1M
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOCACHE
MONITORING;


CREATE INDEX LOAN.AUDTLOGBUSPERLOCALIND ON LOAN.LOANAUDTLOGTBL
(BUSPERIND)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
             )
LOCAL (  
  PARTITION AUDTRANGE02
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

CREATE INDEX LOAN.AUDTLOGLOANAPPNMBLOCALIND ON LOAN.LOANAUDTLOGTBL
(LOANAPPNMB)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
             )
LOCAL (  
  PARTITION AUDTRANGE02
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

CREATE INDEX LOAN.AUDTLOGSEQNMBLOCALIND ON LOAN.LOANAUDTLOGTBL
(LOANAUDTLOGSEQNMB)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
             )
LOCAL (  
  PARTITION AUDTRANGE02
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

CREATE INDEX LOAN.AUDTLOGTAXIDLOCALIND ON LOAN.LOANAUDTLOGTBL
(TAXID)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
             )
LOCAL (  
  PARTITION AUDTRANGE02
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

CREATE INDEX LOAN.AUDTLOGUPDTDTLOCALIND ON LOAN.LOANAUDTLOGTBL
(LOANUPDTDT)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
             )
LOCAL (  
  PARTITION AUDTRANGE02
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA02TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE03
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA03TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE04
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA04TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE05
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA05TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE06
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA06TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE07
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA07TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE08
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA08TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE09
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA09TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE10
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE11
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA11TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE12
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA12TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE13
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA10TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUDTRANGE14
    LOGGING
    NOCOMPRESS 
    TABLESPACE LOANDATA13TBS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

ALTER TABLE LOAN.LOANAUDTLOGTBL ADD (
  CONSTRAINT LOANAPPNMBFORAUDTLOGNFK 
  FOREIGN KEY (LOANAPPNMB) 
  REFERENCES LOAN.LOANGNTYTBL (LOANAPPNMB)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.LOANAUDTLOGTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANAUDTLOGTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANAUDTLOGTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO READLOANROLE;

GRANT SELECT ON LOAN.LOANAUDTLOGTBL TO STAGE;

ALTER TABLE LOAN.PERHISTRYTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.PERHISTRYTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.PERHISTRYTBL
(
  PERHISTRYSEQNMB         NUMBER(10)            NOT NULL,
  TAXID                   CHAR(10 BYTE)         NOT NULL,
  PERFIRSTNM              RAW(50),
  PERLASTNM               RAW(50)               NOT NULL,
  PERINITIALNM            CHAR(1 BYTE),
  PERSFXNM                CHAR(4 BYTE),
  PERTITLTXT              VARCHAR2(40 BYTE),
  PERBRTHDT               DATE,
  PERBRTHCTYNM            VARCHAR2(40 BYTE),
  PERBRTHSTCD             CHAR(2 BYTE),
  PERBRTHCNTCD            CHAR(2 BYTE),
  PERBRTHCNTNM            VARCHAR2(40 BYTE),
  PERUSCITZNIND           CHAR(2 BYTE),
  PERALIENRGSTRTNNMB      VARCHAR2(12 BYTE),
  PERPNDNGLWSUITIND       CHAR(1 BYTE),
  PERAFFILEMPFEDIND       CHAR(1 BYTE),
  PERIOBIND               CHAR(1 BYTE),
  PERINDCTPRLEPROBATNIND  CHAR(1 BYTE),
  PERCRMNLOFFNSIND        CHAR(1 BYTE),
  PERCNVCTIND             CHAR(1 BYTE),
  PERBNKRPTCYIND          CHAR(1 BYTE),
  VETCD                   NUMBER(3),
  GNDRCD                  CHAR(1 BYTE),
  ETHNICCD                CHAR(2 BYTE),
  PERPRIMPHNNMB           RAW(10),
  PERALTPHNNMB            RAW(10),
  PERPRIMEMAILADR         RAW(80),
  PERALTEMAILADR          RAW(80),
  PERPHYADDRSTR1NM        RAW(80),
  PERPHYADDRSTR2NM        RAW(80),
  PERPHYADDRCTYNM         VARCHAR2(40 BYTE),
  PERPHYADDRSTCD          CHAR(2 BYTE),
  PERPHYADDRSTNM          VARCHAR2(60 BYTE),
  PERPHYADDRCNTCD         CHAR(2 BYTE),
  PERPHYADDRZIPCD         CHAR(5 BYTE),
  PERPHYADDRZIP4CD        CHAR(4 BYTE),
  PERPHYADDRPOSTCD        VARCHAR2(20 BYTE),
  PERMAILADDRSTR1NM       RAW(80),
  PERMAILADDRSTR2NM       RAW(80),
  PERMAILADDRCTYNM        VARCHAR2(40 BYTE),
  PERMAILADDRSTCD         CHAR(2 BYTE),
  PERMAILADDRSTNM         VARCHAR2(60 BYTE),
  PERMAILADDRCNTCD        CHAR(2 BYTE),
  PERMAILADDRZIPCD        CHAR(5 BYTE),
  PERMAILADDRZIP4CD       CHAR(4 BYTE),
  PERMAILADDRPOSTCD       VARCHAR2(20 BYTE),
  PEREXTRNLCRDTSCORIND    CHAR(1 BYTE),
  IMCRDTSCORSOURCCD       NUMBER(3),
  PEREXTRNLCRDTSCORNMB    NUMBER(10),
  PEREXTRNLCRDTSCORDT     DATE,
  PERPRIMBUSEXPRNCEYRNMB  NUMBER(3),
  PERCREATUSERID          VARCHAR2(32 BYTE)     NOT NULL,
  PERCREATDT              DATE                  NOT NULL,
  PERHISTRYCREATUSERID    VARCHAR2(32 BYTE)     DEFAULT USER                  NOT NULL,
  PERHISTRYCREATDT        DATE                  DEFAULT sysdate               NOT NULL,
  LASTUPDTUSERID          VARCHAR2(32 BYTE)     DEFAULT USER,
  LASTUPDTDT              DATE                  DEFAULT SYSDATE,
  LAST4TAXID              VARCHAR2(4 BYTE),
  VETCERTIND              CHAR(1 BYTE),
  PERCITZNSHPCNTNM        CHAR(2 BYTE),
  PEROTHRCMNTTXT          VARCHAR2(255 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.LOANAPPPRINSEQNMBUCIND ON LOAN.PERHISTRYTBL
(PERHISTRYSEQNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE LOAN.PERHISTRYTBL ADD (
  CONSTRAINT LOANAPPPRINSEQNMBUCIND
  PRIMARY KEY
  (PERHISTRYSEQNMB)
  USING INDEX LOAN.LOANAPPPRINSEQNMBUCIND
  ENABLE VALIDATE);

ALTER TABLE LOAN.PERHISTRYTBL ADD (
  CONSTRAINT PERHISTRYTBLFK 
  FOREIGN KEY (PERCITZNSHPCNTNM) 
  REFERENCES SBAREF.IMCNTRYCDTBL (IMCNTRYCD)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.PERHISTRYTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.PERHISTRYTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.PERHISTRYTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.PERHISTRYTBL TO GPTS;

GRANT REFERENCES, SELECT ON LOAN.PERHISTRYTBL TO LOANACCT;

GRANT SELECT ON LOAN.PERHISTRYTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO LOANAPPUPDAPPROLE;

GRANT INSERT, SELECT, UPDATE ON LOAN.PERHISTRYTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.PERHISTRYTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO READLOANROLE;

GRANT SELECT ON LOAN.PERHISTRYTBL TO UPDLOANROLE;

DROP TABLE LOAN.TT_TMPPIMSDATATBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.TT_TMPPIMSDATATBL
(
  L1FIRS     CHAR(7 BYTE)                       NOT NULL,
  L1PIMS     CHAR(7 BYTE)                       NOT NULL,
  L1NAME     CHAR(30 BYTE)                      NOT NULL,
  L1CITY     CHAR(30 BYTE)                      NOT NULL,
  L1STATE    CHAR(2 BYTE)                       NOT NULL,
  L2FIRS     CHAR(7 BYTE)                       NOT NULL,
  L2PIMS     CHAR(7 BYTE)                       NOT NULL,
  L2NAME     CHAR(30 BYTE)                      NOT NULL,
  L2STREET   CHAR(30 BYTE)                      NOT NULL,
  L2CITY     CHAR(30 BYTE)                      NOT NULL,
  L2STATE    CHAR(2 BYTE)                       NOT NULL,
  L2ZIP      CHAR(5 BYTE)                       NOT NULL,
  L2COUNTY   CHAR(3 BYTE)                       NOT NULL,
  L2DISTOFF  CHAR(4 BYTE)                       NOT NULL,
  L2FDIC     CHAR(20 BYTE)                      NOT NULL,
  L2NCUAN    CHAR(20 BYTE)                      NOT NULL,
  L2TOMP     CHAR(16 BYTE)                      NOT NULL,
  L2CAT      CHAR(5 BYTE)                       NOT NULL,
  L2SCAT     CHAR(5 BYTE)                       NOT NULL,
  L2CPST     CHAR(1 BYTE)                       NOT NULL,
  L2PLP      CHAR(1 BYTE)                       NOT NULL,
  L2SBX      CHAR(1 BYTE)                       NOT NULL,
  L2CLP      CHAR(1 BYTE)                       NOT NULL,
  L2LDP      CHAR(1 BYTE)                       NOT NULL,
  L27AS      CHAR(1 BYTE)                       NOT NULL,
  L27AG      CHAR(1 BYTE)                       NOT NULL,
  L2PLW      CHAR(1 BYTE)                       NOT NULL,
  L2CMX      CHAR(1 BYTE)                       NOT NULL,
  L2PCP      CHAR(1 BYTE)                       NOT NULL,
  L2ALP      CHAR(1 BYTE)                       NOT NULL,
  L2CDC      CHAR(1 BYTE)                       NOT NULL,
  L3FIRS     CHAR(7 BYTE)                       NOT NULL,
  L3PIMS     CHAR(7 BYTE)                       NOT NULL,
  L3NAME     CHAR(30 BYTE)                      NOT NULL,
  L3CITY     CHAR(30 BYTE)                      NOT NULL,
  L3STATE    CHAR(2 BYTE)                       NOT NULL,
  L4FIRS     CHAR(7 BYTE)                       NOT NULL,
  L4PIMS     CHAR(7 BYTE)                       NOT NULL,
  L4NAME     CHAR(30 BYTE)                      NOT NULL,
  L4CITY     CHAR(30 BYTE)                      NOT NULL,
  L4STATE    CHAR(2 BYTE)                       NOT NULL,
  L5FIRS     CHAR(7 BYTE)                       NOT NULL,
  L5PIMS     CHAR(7 BYTE)                       NOT NULL,
  L5NAME     CHAR(30 BYTE)                      NOT NULL,
  L5CITY     CHAR(30 BYTE)                      NOT NULL,
  L5STATE    CHAR(2 BYTE)                       NOT NULL,
  L5FRNMB    CHAR(10 BYTE)                      NOT NULL,
  L2PLPEXP   CHAR(8 BYTE)                       NOT NULL,
  L2SBXEXP   CHAR(8 BYTE)                       NOT NULL,
  L2PLWEXP   CHAR(8 BYTE)                       NOT NULL,
  L2CMXEXP   CHAR(8 BYTE)                       NOT NULL,
  L2PCPEXP   CHAR(8 BYTE)                       NOT NULL
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


GRANT SELECT ON LOAN.TT_TMPPIMSDATATBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.TT_TMPPIMSDATATBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.TT_TMPPIMSDATATBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.TT_TMPPIMSDATATBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.TT_TMPPIMSDATATBL TO LOANSERVLOANPRTUPLOAD;

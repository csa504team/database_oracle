ALTER TABLE LOAN.LOANGNTYPROJTBL
 DROP PRIMARY KEY CASCADE;

DROP TABLE LOAN.LOANGNTYPROJTBL CASCADE CONSTRAINTS;

CREATE TABLE LOAN.LOANGNTYPROJTBL
(
  LOANAPPNMB                  NUMBER(10)        NOT NULL,
  LOANAPPPROJSTR1NM           VARCHAR2(80 BYTE) NOT NULL,
  LOANAPPPROJSTR2NM           VARCHAR2(80 BYTE),
  LOANAPPPROJCTYNM            VARCHAR2(40 BYTE) NOT NULL,
  LOANAPPPROJCNTYCD           CHAR(3 BYTE),
  LOANAPPPROJSTCD             CHAR(2 BYTE)      NOT NULL,
  LOANAPPPROJZIPCD            CHAR(5 BYTE)      NOT NULL,
  LOANAPPPROJZIP4CD           CHAR(4 BYTE),
  LOANAPPRURALURBANIND        CHAR(1 BYTE)      NOT NULL,
  LOANAPPNETEXPRTAMT          NUMBER(19,4),
  LOANAPPCURREMPQTY           NUMBER(10),
  LOANAPPJOBCREATQTY          NUMBER(10),
  LOANAPPJOBRTND              NUMBER(10),
  LOANAPPJOBRQMTMETIND        CHAR(1 BYTE),
  LOANAPPCDCJOBRAT            NUMBER(6,3),
  LOANAPPPROJCREATUSERID      VARCHAR2(32 BYTE) NOT NULL,
  LOANAPPPROJCREATDT          DATE              NOT NULL,
  LOANAPPSBARMBRSFEEAMT       NUMBER(19,4),
  LASTUPDTUSERID              VARCHAR2(32 BYTE) DEFAULT USER,
  LASTUPDTDT                  DATE              DEFAULT SYSDATE,
  LOANFINANCLSTMTDUEDT        DATE,
  LOANFINANCLSTMTFREQCD       CHAR(1 BYTE),
  LOANFINANCLSTMTFREQDTLDESC  VARCHAR2(255 BYTE),
  UCCLIENRFILESTDT            DATE,
  UCCLIENRFILECNTYDT          DATE,
  YRBTWNSTRFILE               NUMBER(10),
  YRBTWNCNTYRFILE             NUMBER(10),
  UCCLIENRFILELOCALDT         DATE,
  YRBTWNLOCALRFILE            NUMBER(10),
  NAICSYRNMB                  NUMBER(4),
  NAICSCD                     NUMBER(10),
  LOANAPPFRNCHSCD             CHAR(5 BYTE),
  LOANAPPFRNCHSNM             VARCHAR2(255 BYTE),
  LOANAPPFRNCHSIND            CHAR(1 BYTE),
  ADDTNLLOCACQLMTIND          CHAR(1 BYTE),
  FIXASSETACQLMTIND           CHAR(1 BYTE),
  FIXASSETACQLMTAMT           NUMBER(15,2),
  COMPSLMTIND                 CHAR(1 BYTE),
  COMPSLMTAMT                 NUMBER(15,2),
  BULKSALELAWCOMPLYIND        CHAR(1 BYTE),
  FRNCHSRECRDACCSIND          CHAR(1 BYTE),
  FRNCHSFEEDEFMONNMB          NUMBER(3),
  FRNCHSFEEDEFIND             CHAR(1 BYTE),
  FRNCHSTERMNOTCIND           CHAR(1 BYTE),
  FRNCHSLENDRSAMEOPPIND       CHAR(1 BYTE),
  LOANBUSESTDT                DATE,
  LOANAPPRURALINITIATVIND     CHAR(1 BYTE),
  LOANAPPPROJGEOCD            CHAR(11 BYTE),
  LOANAPPHUBZONEIND           CHAR(1 BYTE),
  LOANAPPPROJLAT              NUMBER,
  LOANAPPPROJLONG             NUMBER,
  LOANAPPPROJGEOCDFLRCT       NUMBER(2),
  LOANAPPCBRSIND              CHAR(1 BYTE),
  LOANAPPOPPZONEIND           CHAR(1 BYTE)
)
TABLESPACE LOANDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


CREATE UNIQUE INDEX LOAN.XPKLOANGNTYPROJTBL ON LOAN.LOANGNTYPROJTBL
(LOANAPPNMB)
LOGGING
TABLESPACE LOANDATATBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE TRIGGER LOAN.LOANGNTYPROJUPDTRIG
   AFTER UPDATE
   ON LOAN.LOANGNTYPROJTBL
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
DISABLE
DECLARE
   v_LoanAppNmb         NUMBER (10, 0);
   v_CreatUserId        CHAR (15);
   v_LoanHistrySeqNmb   NUMBER (10, 0);
  -- v_temp               NUMBER (10);
/*
Date         :
Programmer   :
Remarks      :
***********************************************************************************
Revised By      Date        Comments
----------     --------    ----------------------------------------------------------
APK -- 10/13/2011 -- modified to add new columns LOANFINANCLSTMTDUEDT,LOANFINANCLSTMTFREQCD,LOANFINANCLSTMTFREQDTLDESC ,UCCLIENRFILESTDT ,UCCLIENRFILECNTYDT,YRBTWNSTRFILE  ,
YRBTWNCNTYRFILE as part of TO3 - LAU changes
APK -- 11/16/2011 -- modified to add lastupdtuserid and lastupdtdt fields which were missing to be inserted into the histry table.
SP --   2/9/2012 --Modified to add new columns UCCLIENRFILELOCALDT,YRBTWNLOCALRFILE
SP --   10/07/2013 -- Removed street number and street suffix name as part of appending these to street name changes
SS--12/6/2016-- Modified to add new columnsOPSMDEV1268
RY-- 03/28/2017-OPSMDEV1401--Removed EconDevObjctCd from the list
********************************************************************************
*/
BEGIN
   v_LoanAppNmb := :NEW.LoanAppNmb;
   v_CreatUserId := :NEW.LastUpdtUserId;

   --Get the history number based on the loan number and create user id
   Loan.LoanHistrySelCSP (v_LoanAppNmb,
                          v_CreatUserId,
                          v_LoanHistrySeqNmb,
                          'LoanGntyProjHistryTbl');

   --select count(*) into v_temp from Loan.LoanGntyProjHistryTbl where LoanHistrySeqNmb = v_LoanHistrySeqNmb;

   --if v_temp = 0 then --not exists(select 1 from LoanGntyProjHistryTbl where LoanHistrySeqNmb = @LoanHistrySeqNmb)
   --begin
   --Insert into history if history not exists
   INSERT INTO LOAN.LoanGntyProjHistryTbl (LoanHistrySeqNmb,
                                           LoanAppProjStr1Nm,
                                           LoanAppProjStr2Nm,
                                           LoanAppProjCtyNm,
                                           LoanAppProjCntyCd,
                                           LoanAppProjStCd,
                                           LoanAppProjZipCd,
                                           LoanAppProjZip4Cd,
                                           LoanAppRuralUrbanInd,
                                           LoanAppNetExprtAmt,
                                           LoanAppCurrEmpQty,
                                           LoanAppJobCreatQty,
                                           LoanAppJobRtnd,
                                           LoanAppJobRqmtMetInd,
                                           LoanAppCDCJobRat,
                                           --EconDevObjctCd,
                                           LoanProjHistryCreatUserId,
                                           LoanProjHistryCreatDt,
                                           LOANFINANCLSTMTDUEDT,
                                           LOANFINANCLSTMTFREQCD,
                                           LOANFINANCLSTMTFREQDTLDESC,
                                           UCCLIENRFILESTDT,
                                           UCCLIENRFILECNTYDT,
                                           YRBTWNSTRFILE,
                                           YRBTWNCNTYRFILE,
                                           UCCLIENRFILELOCALDT,
                                           YRBTWNLOCALRFILE,
                                           NAICSYRNMB,
                                           NAICSCD,
                                           LOANAPPFRNCHSCD,
                                           LOANAPPFRNCHSNM,
                                           LOANAPPFRNCHSIND,
                                           AddtnlLocAcqLmtInd,
                                           FixAssetAcqLmtInd,
                                           FixAssetAcqLmtAmt,
                                           CompsLmtInd,
                                           CompsLmtAmt,
                                           BulkSaleLawComplyInd,
                                           FrnchsRecrdAccsInd,
                                           FrnchsFeeDefMonNmb,
                                           FrnchsFeeDefInd,
                                           FrnchsTermNotcInd,
                                           FrnchsLendrSameOppInd,
                                           LastUpdtUserId,
                                           LastUpdtDt)
   VALUES (v_LoanHistrySeqNmb,
           :OLD.LoanAppProjStr1Nm,
           :OLD.LoanAppProjStr2Nm,
           :OLD.LoanAppProjCtyNm,
           :OLD.LoanAppProjCntyCd,
           :OLD.LoanAppProjStCd,
           :OLD.LoanAppProjZipCd,
           :OLD.LoanAppProjZip4Cd,
           :OLD.LoanAppRuralUrbanInd,
           :OLD.LoanAppNetExprtAmt,
           :OLD.LoanAppCurrEmpQty,
           :OLD.LoanAppJobCreatQty,
           :OLD.LoanAppJobRtnd,
           :OLD.LoanAppJobRqmtMetInd,
           :OLD.LoanAppCDCJobRat,
           --:OLD.EconDevObjctCd,
           NVL (:OLD.LastUpdtUserId, :OLD.LoanAppProjCreatUserId),
           NVL (:OLD.LastUpdtDt, :OLD.LoanAppProjCreatDt),
           :OLD.LOANFINANCLSTMTDUEDT,
           :OLD.LOANFINANCLSTMTFREQCD,
           :OLD.LOANFINANCLSTMTFREQDTLDESC,
           :OLD.UCCLIENRFILESTDT,
           :OLD.UCCLIENRFILECNTYDT,
           :OLD.YRBTWNSTRFILE,
           :OLD.YRBTWNCNTYRFILE,
           :OLD.UCCLIENRFILELOCALDT,
           :OLD.YRBTWNLOCALRFILE,
           :OLD.NAICSYRNMB,
           :OLD.NAICSCD,
           :OLD.LOANAPPFRNCHSCD,
           :OLD.LOANAPPFRNCHSNM,
           :OLD.LOANAPPFRNCHSIND,
           :OLD.AddtnlLocAcqLmtInd,
           :OLD.FixAssetAcqLmtInd,
           :OLD.FixAssetAcqLmtAmt,
           :OLD.CompsLmtInd,
           :OLD.CompsLmtAmt,
           :OLD.BulkSaleLawComplyInd,
           :OLD.FrnchsRecrdAccsInd,
           :OLD.FrnchsFeeDefMonNmb,
           :OLD.FrnchsFeeDefInd,
           :OLD.FrnchsTermNotcInd,
           :OLD.FrnchsLendrSameOppInd,
           NVL (:OLD.LastUpdtUserId, :OLD.LoanAppProjCreatUserId),
           NVL (:OLD.LastUpdtDt, SYSDATE));
--end;
--end if;
END;
/


CREATE OR REPLACE TRIGGER LOAN.LOANGNTYPROJ_HIST_TRIG
   AFTER UPDATE
   ON LOAN.LOANGNTYPROJTBL
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
DECLARE
   v_header  VARCHAR2(2000);
   v_log_txt CLOB;
   v_loannmb VARCHAR2(200);
/***********************************************************
 NAME:       LOANGNTYPROJ_HIST_TRIG
 PURPOSE: To generate history for LOAN.LOANGNTYPROJTBL
 1.0        8/1/2018      JPaul
******************************************************************************/
BEGIN
   FOR rec IN (SELECT MAX(g.LOANNMB) loannmb
               FROM   LOAN.LOANGNTYTBL g
               WHERE  g.LOANAPPNMB = :OLD.LOANAPPNMB)
   LOOP
      v_loannmb  := rec.loannmb;
   END LOOP;

   v_header   := '<History><LoanNmb>' || v_loannmb || '</LoanNmb>';
   v_log_txt  := v_header;

   IF NVL(TO_CHAR(:OLD.LoanAppProjStr1Nm), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjStr1Nm), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectStrtName1', :OLD.LoanAppProjStr1Nm, :NEW.LoanAppProjStr1Nm, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjStr2Nm), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjStr2Nm), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectStrtName2', :OLD.LoanAppProjStr2Nm, :NEW.LoanAppProjStr2Nm, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjCtyNm), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjCtyNm), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectCityName', :OLD.LoanAppProjCtyNm, :NEW.LoanAppProjCtyNm, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjCntyCd), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjCntyCd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectCountyCode', :OLD.LoanAppProjCntyCd, :NEW.LoanAppProjCntyCd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjStCd), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjStCd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectStCd', :OLD.LoanAppProjStCd, :NEW.LoanAppProjStCd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjZipCd), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjZipCd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectZipCd', :OLD.LoanAppProjZipCd, :NEW.LoanAppProjZipCd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppProjZip4Cd), 'x') != NVL(TO_CHAR(:NEW.LoanAppProjZip4Cd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('ProjectZip4Cd', :OLD.LoanAppProjZip4Cd, :NEW.LoanAppProjZip4Cd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppRuralUrbanInd), 'x') != NVL(TO_CHAR(:NEW.LoanAppRuralUrbanInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('RuralUrbanInd', :OLD.LoanAppRuralUrbanInd, :NEW.LoanAppRuralUrbanInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppNetExprtAmt), 'x') != NVL(TO_CHAR(:NEW.LoanAppNetExprtAmt), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('NetExprtAmt', :OLD.LoanAppNetExprtAmt, :NEW.LoanAppNetExprtAmt, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppCurrEmpQty), 'x') != NVL(TO_CHAR(:NEW.LoanAppCurrEmpQty), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('CurrEmpQty', :OLD.LoanAppCurrEmpQty, :NEW.LoanAppCurrEmpQty, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppJobCreatQty), 'x') != NVL(TO_CHAR(:NEW.LoanAppJobCreatQty), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('JobsCreatdQty', :OLD.LoanAppJobCreatQty, :NEW.LoanAppJobCreatQty, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppJobRtnd), 'x') != NVL(TO_CHAR(:NEW.LoanAppJobRtnd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('JobsRetaindQty', :OLD.LoanAppJobRtnd, :NEW.LoanAppJobRtnd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppJobRqmtMetInd), 'x') != NVL(TO_CHAR(:NEW.LoanAppJobRqmtMetInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('JobRqmtMetInd', :OLD.LoanAppJobRqmtMetInd, :NEW.LoanAppJobRqmtMetInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LoanAppCDCJobRat), 'x') != NVL(TO_CHAR(:NEW.LoanAppCDCJobRat), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('OverallPortfolioJobRatio', :OLD.LoanAppCDCJobRat, :NEW.LoanAppCDCJobRat, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANFINANCLSTMTDUEDT), 'x') != NVL(TO_CHAR(:NEW.LOANFINANCLSTMTDUEDT), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LOANFINANCLSTMTDUEDT', :OLD.LOANFINANCLSTMTDUEDT, :NEW.LOANFINANCLSTMTDUEDT, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANFINANCLSTMTFREQCD), 'x') != NVL(TO_CHAR(:NEW.LOANFINANCLSTMTFREQCD), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LOANFINANCLSTMTFREQCD', :OLD.LOANFINANCLSTMTFREQCD, :NEW.LOANFINANCLSTMTFREQCD, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANFINANCLSTMTFREQDTLDESC), 'x') != NVL(TO_CHAR(:NEW.LOANFINANCLSTMTFREQDTLDESC), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LOANFINANCLSTMTFREQDTLDESC', :OLD.LOANFINANCLSTMTFREQDTLDESC, :NEW.LOANFINANCLSTMTFREQDTLDESC, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.UCCLIENRFILESTDT), 'x') != NVL(TO_CHAR(:NEW.UCCLIENRFILESTDT), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('UCCLIENRFILESTDT', :OLD.UCCLIENRFILESTDT, :NEW.UCCLIENRFILESTDT, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.UCCLIENRFILECNTYDT), 'x') != NVL(TO_CHAR(:NEW.UCCLIENRFILECNTYDT), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('UCCLIENRFILECNTYDT', :OLD.UCCLIENRFILECNTYDT, :NEW.UCCLIENRFILECNTYDT, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.YRBTWNSTRFILE), 'x') != NVL(TO_CHAR(:NEW.YRBTWNSTRFILE), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('YRBTWNSTRFILE', :OLD.YRBTWNSTRFILE, :NEW.YRBTWNSTRFILE, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.YRBTWNCNTYRFILE), 'x') != NVL(TO_CHAR(:NEW.YRBTWNCNTYRFILE), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('YRBTWNCNTYRFILE', :OLD.YRBTWNCNTYRFILE, :NEW.YRBTWNCNTYRFILE, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.UCCLIENRFILELOCALDT), 'x') != NVL(TO_CHAR(:NEW.UCCLIENRFILELOCALDT), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('UCCLIENRFILELOCALDT', :OLD.UCCLIENRFILELOCALDT, :NEW.UCCLIENRFILELOCALDT, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.YRBTWNLOCALRFILE), 'x') != NVL(TO_CHAR(:NEW.YRBTWNLOCALRFILE), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('YRBTWNLOCALRFILE', :OLD.YRBTWNLOCALRFILE, :NEW.YRBTWNLOCALRFILE, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.NAICSYRNMB), 'x') != NVL(TO_CHAR(:NEW.NAICSYRNMB), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('NAICSYRNMB', :OLD.NAICSYRNMB, :NEW.NAICSYRNMB, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.NAICSCD), 'x') != NVL(TO_CHAR(:NEW.NAICSCD), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('NAICSCD', :OLD.NAICSCD, :NEW.NAICSCD, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANAPPFRNCHSCD), 'x') != NVL(TO_CHAR(:NEW.LOANAPPFRNCHSCD), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LOANAPPFRNCHSCD', :OLD.LOANAPPFRNCHSCD, :NEW.LOANAPPFRNCHSCD, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANAPPFRNCHSNM), 'x') != NVL(TO_CHAR(:NEW.LOANAPPFRNCHSNM), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseName', :OLD.LOANAPPFRNCHSNM, :NEW.LOANAPPFRNCHSNM, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.LOANAPPFRNCHSIND), 'x') != NVL(TO_CHAR(:NEW.LOANAPPFRNCHSIND), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseInd', :OLD.LOANAPPFRNCHSIND, :NEW.LOANAPPFRNCHSIND, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.AddtnlLocAcqLmtInd), 'x') != NVL(TO_CHAR(:NEW.AddtnlLocAcqLmtInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LimitLocation', :OLD.AddtnlLocAcqLmtInd, :NEW.AddtnlLocAcqLmtInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FixAssetAcqLmtInd), 'x') != NVL(TO_CHAR(:NEW.FixAssetAcqLmtInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LimitFixedAssetInd', :OLD.FixAssetAcqLmtInd, :NEW.FixAssetAcqLmtInd, v_log_txt);
   END IF;

   IF NVL((:OLD.FixAssetAcqLmtAmt), -1) != NVL((:NEW.FixAssetAcqLmtAmt), -1) THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LimitFixedAssetAmt', :OLD.FixAssetAcqLmtAmt, :NEW.FixAssetAcqLmtAmt, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.CompsLmtInd), 'x') != NVL(TO_CHAR(:NEW.CompsLmtInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LimitCompensationInd', :OLD.CompsLmtInd, :NEW.CompsLmtInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.CompsLmtAmt), 'x') != NVL(TO_CHAR(:NEW.CompsLmtAmt), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('LimitCompensationAmt', :OLD.CompsLmtAmt, :NEW.CompsLmtAmt, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.BulkSaleLawComplyInd), 'x') != NVL(TO_CHAR(:NEW.BulkSaleLawComplyInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('StateLawComplianceForBulkInd', :OLD.BulkSaleLawComplyInd, :NEW.BulkSaleLawComplyInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FrnchsRecrdAccsInd), 'x') != NVL(TO_CHAR(:NEW.FrnchsRecrdAccsInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiserBooksInd', :OLD.FrnchsRecrdAccsInd, :NEW.FrnchsRecrdAccsInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FrnchsFeeDefMonNmb), 'x') != NVL(TO_CHAR(:NEW.FrnchsFeeDefMonNmb), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseDeferPymtMonths', :OLD.FrnchsFeeDefMonNmb, :NEW.FrnchsFeeDefMonNmb, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FrnchsFeeDefInd), 'x') != NVL(TO_CHAR(:NEW.FrnchsFeeDefInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseDeferPymtInd', :OLD.FrnchsFeeDefInd, :NEW.FrnchsFeeDefInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FrnchsTermNotcInd), 'x') != NVL(TO_CHAR(:NEW.FrnchsTermNotcInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseWarnTermInd', :OLD.FrnchsTermNotcInd, :NEW.FrnchsTermNotcInd, v_log_txt);
   END IF;

   IF NVL(TO_CHAR(:OLD.FrnchsLendrSameOppInd), 'x') != NVL(TO_CHAR(:NEW.FrnchsLendrSameOppInd), 'x') THEN
      v_log_txt  := LOAN.CONCAT_HIST_TXT('FrnchiseOpporCureInd', :OLD.FrnchsLendrSameOppInd, :NEW.FrnchsLendrSameOppInd, v_log_txt);
   END IF;

   -- insert only if there is history
   IF v_log_txt != v_header THEN
      v_log_txt  := v_log_txt || '</History>';

      INSERT INTO LOAN.LoanAudtLogTbl(LOANAUDTLOGSEQNMB,
                                      LOANAPPNMB,
                                      LOANAUDTLOGTXT,
                                      LOANUPDTUSERID,
                                      LOANUPDTDT)
      VALUES      ((SELECT MAX(LoanAudtLogSeqNmb) + 1
                    FROM   loan.LoanAudtLogTbl),
                   :OLD.LOANAPPNMB,
                   v_log_txt,
                   NVL(:NEW.LASTUPDTUSERID, USER),
                   SYSDATE);
   END IF;
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error code ' || SQLCODE || ': ' || SUBSTR(SQLERRM, 1, 64));
      RAISE;
END LOANGNTYPROJ_HIST_TRIG;
/


ALTER TABLE LOAN.LOANGNTYPROJTBL ADD (
  CONSTRAINT XPKLOANGNTYPROJTBL
  PRIMARY KEY
  (LOANAPPNMB)
  USING INDEX LOAN.XPKLOANGNTYPROJTBL
  ENABLE VALIDATE);

ALTER TABLE LOAN.LOANGNTYPROJTBL ADD (
  CONSTRAINT FINANCLSTMTFREQCDFORGNTYPROJ 
  FOREIGN KEY (LOANFINANCLSTMTFREQCD) 
  REFERENCES SBAREF.LOANFINANCLSTMTFREQTBL (LOANFINANCLSTMTFREQCD)
  ENABLE VALIDATE,
  CONSTRAINT GNTYHASGNTYPROJ 
  FOREIGN KEY (LOANAPPNMB) 
  REFERENCES LOAN.LOANGNTYTBL (LOANAPPNMB)
  ENABLE VALIDATE);

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO CNTRACHANNURI;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO FDCADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROJTBL TO GPTS;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSDISTOFCROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSHQADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSOFCADMINROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSOFCGRPROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSOFOROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO GPTSSERVCNTRROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LANAREAD;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LANAUPDATE;

GRANT REFERENCES, SELECT ON LOAN.LOANGNTYPROJTBL TO LOANACCT;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANACCTUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANAPP;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANAPPLOOKUPUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANAPPUPDAPPROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANCMNTREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANCMNTWRITEROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANELIGIBILITYREADGRP;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANELIGIBILITYREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANELIGIBILITYUPDATEGRP;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANELIGIBILITYUPDATEROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANLANAREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANLANAUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANLOOKUPUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANPOSTSERVSU;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROJTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANPRTREAD;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANPRTUPDT;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANREAD;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANREADALLROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANREADROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANSERVLOANPRTUPLOAD;

GRANT INSERT, SELECT, UPDATE ON LOAN.LOANGNTYPROJTBL TO LOANUPDROLE;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO LOANUPDT;

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, READ, DEBUG, FLASHBACK ON LOAN.LOANGNTYPROJTBL TO PUBLIC;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO READLOANROLE;

GRANT UPDATE ON LOAN.LOANGNTYPROJTBL TO SBAREF;

GRANT SELECT ON LOAN.LOANGNTYPROJTBL TO UPDLOANROLE;

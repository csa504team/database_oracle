-- Add new deliverable "ALP/PCLP Renewal" - DONE on Dev db
INSERT INTO CDCDOCS.DLVRTBL 
(
	DLVRID, DLVRNM, DLVRSTARTDT, 
	DLVRENDDT, ACTVIND, CREATUSERID, 
	CREATDT, LASTUPDUSERID, LASTUPDDT
) 
VALUES 
( 
	(select max(DLVRID)+1 from CDCDOCS.DLVRTBL where DLVRNM='ALP/PCLP Renewal'),
	 'Annual Report',
	to_date('01-01-21','DD-MM-YY'),
	null,
	'Y',
	user,
	sysdate,
	user,
	sysdate 
);
 commit;

--================ Add New Column 'EmailSent' in CDCDOCS.CDCDlvrTbl =================================================================================

ALTER TABLE CDCDOCS.CDCDLVRTBL ADD EmailSent CHAR(1) DEFAULT 'N';
COMMIT;

--================ Set EmailSent = 'N' for all record in CDCDOCS.CDCDlvrTbl =================================================================================

UPDATE CDCDOCS.CDCDLVRTBL SET EmailSent = 'N';
COMMIT;

--=================================================================================================
--ALP/PCLP Renewal
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'ALP/PCLP Renewal Narrative', SYSDATE, NULL, USER	, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Internal Control Policy & Board Approval
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Internal Control Policy ' || chr(38) || ' Board Approval', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Policies: Credit Approval & Loan Servicing
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Policies: Credit Approval ' || chr(38) || ' Loan Servicing', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Professional Services Contracts
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Professional Services Contracts', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Quarterly Delinquency Reports
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Quarterly Delinquency Reports', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Designated Attorney List
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Designated Attorney List', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Independent Auditor Engagement Letter
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Independent Auditor Engagement Letter', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------
--Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)', SYSDATE, NULL, USER, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;
----------------------------------------------------------------------------------------------

--===========Insert new doctypes into CdcDocs.DLVRReqDocTypTbl for new deliverable "ALP/PCLP Renewal" =======================================

--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "ALP/PCLP Renewal"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%ALP/PCLP Renewal%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Bylaws and Amendments"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Bylaws and Amendments%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Internal Control Policy & Board Approval"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Internal Control Policy '||CHR(38)||' Board Approval%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Policies: Credit Approval & Loan Servicing"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Policies: Credit Approval '||CHR(38)||' Loan Servicing%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Policies: Credit Approval & Loan Servicing"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Policies: Credit Approval '||CHR(38)||' Loan Servicing%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Organizational Chart"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Organizational Chart%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Board/Committee Members & Staff List"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Board/Committee Members '||CHR(38)||' Staff List%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Professional Services Contracts"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Professional Services Contracts%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Quarterly Delinquency Reports"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Quarterly Delinquency Reports%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "E&O Insurance Binder"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%E'||CHR(38)||'O Insurance Binder%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "D&O Insurance Binder"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%D'||CHR(38)||'O Insurance Binder%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Designated Attorney List"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Designated Attorney List%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Board Meeting Details & Minutes"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Board Meeting Details '||CHR(38)||' Minutes%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Independent Auditor Engagement Letter"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Independent Auditor Engagement Letter%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Economic Development Report"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Economic Development Report%';
COMMIT;
----------------------------------------------------------------------------------------------
--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for "Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)"
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'ALP/PCLP Renewal'),
	D.DocTypCd, user, sysdate, user, sysdate
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Evidence - Meets Loan Loss Reserve Fund Requirements (PCLP only)%';
COMMIT;
----------------------------------------------------------------------------------------------
--==================== Set EmailSent = 'Y' Using CDCDOCS.SetDlvrCDCEMailSent stored proc=========================================================================

create or replace PROCEDURE         CDCDOCS.SetDlvrCDCEMailSent( 
p_identifier    NUMBER := NULL,
p_cdcdlvrid	NUMBER := NULL,
p_dlvrid	NUMBER := NULL,
p_cdcregncd     VARCHAR2,
p_cdcnmb        VARCHAR2,
p_creatuserid    VARCHAR2,	
	
p_SelCur         OUT SYS_REFCURSOR)
AS
BEGIN
    
    -- Retrieve all active deliverable names and their associated years
    IF p_identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            UPDATE
                CDCDOCS.CDCDLVRTBL
	    SET
		EmailSent = 'Y'	
            WHERE
                dlvrid = p_dlvrid
	    AND cdcdlvrid = p_cdcdlvrid
	    AND TRIM (cdcnmb) = TRIM (p_cdcnmb)
            AND TRIM (cdcregncd) = TRIM (p_cdcregncd)
            AND TRIM (creatuserid) = TRIM (p_creatuserid);
        END;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    BEGIN
        RAISE;
    END;
END;


--==================== Check for EmailSent CDCDOCS.CheckDlvrCDCEMailSent stored proc=========================================================================

create or replace PROCEDURE         CDCDOCS.CheckDlvrCDCEMailSent( 
p_identifier    NUMBER := NULL,
p_cdcdlvrid	NUMBER := NULL,
p_dlvrid	NUMBER := NULL,
p_cdcregncd     VARCHAR2,
p_cdcnmb        VARCHAR2,
p_creatuserid    VARCHAR2,	
	
p_SelCur         OUT SYS_REFCURSOR)
AS
BEGIN
    
    -- Retrieve all active deliverable names and their associated years
    IF p_identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            SELECT
                *
            FROM
                CDCDOCS.CDCDLVRTBL
            WHERE
                dlvrid = p_dlvrid
	    AND cdcdlvrid = p_cdcdlvrid
	    AND TRIM (cdcnmb) = TRIM (p_cdcnmb)
            AND TRIM (cdcregncd) = TRIM (p_cdcregncd)
            AND TRIM (creatuserid) = TRIM (p_creatuserid);
        END;
    END IF;
EXCEPTION
WHEN OTHERS THEN
    BEGIN
        RAISE;
    END;
END;


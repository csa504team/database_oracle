--Ownership and Control - For 'Annual Report' 2020
INSERT INTO sbaref.doctyptbl(doctypcd,doctypdesctxt,doctypstrtdt,doctypenddt,creatuserid,creatdt)
VALUES((SELECT MAX(doctypcd)+1 FROM sbaref.doctyptbl), 'Ownership and Control', SYSDATE, NULL, USER	, SYSDATE);
COMMIT;

INSERT INTO sbaref.doctypbusprcsmthdtbl(doctypcd,busprcstypcd,prcsmthdcd,doctypbusprcsmthdstrtdt,doctypbusprcsmthdenddt,creatuserid,creatdt,doctypvalidtypcd)
VALUES(	(SELECT MAX(doctypcd) FROM sbaref.doctyptbl), 
	(SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
	, '504', SYSDATE, NULL, USER, SYSDATE, 'O');
COMMIT;

--Adding DoctypCd into CdcDocs.DLVRReqDocTypTbl for 'Ownership and Control' - under 'Annual Report' 2020
INSERT INTO CdcDocs.DLVRReqDocTypTbl
	select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
	2,
	D.DocTypCd, user, sysdate, user, sysdate,
        'Y'
FROM 	SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     	p.BusPrcsTypCd = 12
AND     	(p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     	D.DocTypDescTxt Like '%Ownership and Control%';
COMMIT;
UPDATE CDCDOCS.DLVRTBL
SET    
       DLVRENDDT     = to_date('31-12-19','DD-MM-YY'),
       LASTUPDUSERID = user,
       LASTUPDDT     = sysdate
WHERE  DLVRENDDT        IS NOT NULL;
commit;


INSERT INTO CDCDOCS.DLVRTBL (
   DLVRID, DLVRNM, DLVRSTARTDT, 
   DLVRENDDT, ACTVIND, CREATUSERID, 
   CREATDT, LASTUPDUSERID, LASTUPDDT) 
VALUES ( (select max(DLVRID)+1 from CDCDOCS.DLVRTBL where DLVRNM='Annual Report'),
 'Annual Report',
 to_date('01-01-20','DD-MM-YY'),
 null,
 'Y',
 user,
 sysdate,
 user,
 sysdate );
 commit;
 
 
INSERT INTO CdcDocs.DLVRReqDocTypTbl 
select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and DLVRSTARTDT=to_date('01-01-20','DD-MM-YY')),
DocTypCd,user,sysdate,user,sysdate FROM CDCDOCS.DLVRReqDocTypTbl where 
DLVRID in (select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and trunc(DLVRSTARTDT)=to_date('01-01-19','DD-MM-YY'));
commit;


insert into  CdcDocs.DLVRReqDocTypTbl
select CDCDOCS.DLVRREQDOCTYPIDSEQ.nextval,
(select DLVRID from CDCDOCS.DLVRTBL where  DLVRNm = 'Annual Report' and DLVRSTARTDT=to_date('01-01-20','DD-MM-YY')),
D.DocTypCd,
user,sysdate,user,sysdate
FROM SBAREF.DocTypTbl D, SBAREF.DOCTYPBUSPRCSMTHDTBL P
WHERE   D.DocTypCd = P.DOCTYPCD
AND     p.BusPrcsTypCd = (SELECT BUSPRCSTYPCD FROM SBAREF.BusPrcsTypTbl WHERE BusPrcsTypDescTxt = 'Corporate Governance Module')
AND     (p.PrcsMthdCd IS NULL OR p.PrcsMthdCd = 504)
AND     D.DocTypCd=1203;
commit;
CREATE TABLE LOANAPPJOBCREATQTYTBL_11282012
(
  JOBCREATQTYSEQNMB       NUMBER(5),
  PRCSMTHDCD              CHAR(3 BYTE),
  LOANAMTMIN              NUMBER(19,4),
  LOANAMTMAX              NUMBER(19,4),
  JOBQTYMIN               NUMBER,
  JOBQTYMAX               NUMBER,
  JOBCREATQTY             NUMBER,
  JOBCREATQTYSTRTDT       DATE,
  JOBCREATQTYENDDT        DATE,
  JOBCREATQTYCREATUSERID  VARCHAR2(32 BYTE),
  JOBCREATQTYCREATDT      DATE,
  LASTUPDTUSERID          VARCHAR2(32 BYTE),
  LASTUPDTDT              DATE,
  STRTDT                  DATE,
  ENDDT                   DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRACHANNURI;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRDFREDERICKS;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRNKUMARAS;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRNSALATOVA;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRRYALAVAR;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO CNTRSMSHONTH;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANAPPJOBCREATQTYTBL_11282012 TO LOANSERVLOANPRTUPLOAD;

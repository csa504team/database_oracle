CREATE TABLE IMSEQNMBTBL
(
  IMSEQID              VARCHAR2(20 BYTE),
  IMSEQNMB             NUMBER(10),
  IMSEQNMBCREATUSERID  VARCHAR2(32 BYTE),
  IMSEQNMBCREATDT      DATE,
  LASTUPDTUSERID       VARCHAR2(32 BYTE)        DEFAULT USER,
  LASTUPDTDT           DATE                     DEFAULT SYSDATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON IMSEQNMBTBL TO CNTRACHANNURI;

GRANT SELECT ON IMSEQNMBTBL TO CNTRDFREDERICKS;

GRANT SELECT ON IMSEQNMBTBL TO CNTRNKUMARAS;

GRANT SELECT ON IMSEQNMBTBL TO CNTRNNAMILAE;

GRANT SELECT ON IMSEQNMBTBL TO CNTRNSALATOVA;

GRANT SELECT ON IMSEQNMBTBL TO CNTRRYALAVAR;

GRANT SELECT ON IMSEQNMBTBL TO CNTRSMSHONTH;

GRANT SELECT ON IMSEQNMBTBL TO LOAN;

GRANT SELECT ON IMSEQNMBTBL TO LOANACCT;

GRANT SELECT ON IMSEQNMBTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON IMSEQNMBTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON IMSEQNMBTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON IMSEQNMBTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON IMSEQNMBTBL TO LOANREADALLROLE;

GRANT SELECT ON IMSEQNMBTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON IMSEQNMBTBL TO LOANUPDROLE;

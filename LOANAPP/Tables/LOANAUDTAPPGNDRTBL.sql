CREATE TABLE LOANAUDTAPPGNDRTBL
(
  LOANAPPNMB             NUMBER(10),
  LOANAUDTSEQNMB         NUMBER(10),
  GNDRCD                 CHAR(1 BYTE),
  LOANGNDRSUMPCTOWNRSHP  NUMBER(7,3),
  CREATEDT               DATE,
  CREATEUSERID           VARCHAR2(32 BYTE),
  LASTUPDTUSERID         VARCHAR2(32 BYTE)      DEFAULT USER,
  LASTUPDTDT             DATE                   DEFAULT SYSDATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTAPPGNDRTBL TO LOANSERVLOANPRTUPLOAD;

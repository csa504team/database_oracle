CREATE TABLE LOANAUDTAPPCNSELNGTBL
(
  LOANAPPNMB      NUMBER(10),
  LOANAUDTSEQNMB  NUMBER(10),
  IMCNSELNGTYPCD  NUMBER(3),
  IMCNSELNGHRCD   NUMBER(3),
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTAPPCNSELNGTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANAUDTAPPCNSELNGTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANAUDTAPPCNSELNGTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANAUDTAPPCNSELNGTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANAUDTAPPCNSELNGTBL TO LOANSERVLOANPRTUPLOAD;

CREATE GLOBAL TEMPORARY TABLE TT_LOANTBL_1
(
  LOANAPPNMB      INTEGER,
  MAXAUDTSEQNMB   INTEGER,
  LASTSTATDT      DATE,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             DEFAULT USER,
  LASTUPDTDT      DATE                          DEFAULT SYSDATE
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TT_LOANTBL_1 TO CNTRACHANNURI;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRDFREDERICKS;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRNKUMARAS;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRNNAMILAE;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRNSALATOVA;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRRYALAVAR;

GRANT SELECT ON TT_LOANTBL_1 TO CNTRSMSHONTH;

GRANT SELECT ON TT_LOANTBL_1 TO LOANAPPREADALLROLE;

GRANT SELECT ON TT_LOANTBL_1 TO LOANSERVLOANPRTUPLOAD;

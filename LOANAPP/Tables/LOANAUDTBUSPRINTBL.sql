CREATE TABLE LOANAUDTBUSPRINTBL
(
  LOANAPPNMB                NUMBER(10),
  LOANAUDTSEQNMB            NUMBER(10),
  BORRSEQNMB                NUMBER(10),
  PRINSEQNMB                NUMBER(10),
  LOANPRINGNTYIND           CHAR(1 BYTE),
  LOANBUSPRINCREATUSERID    VARCHAR2(32 BYTE),
  LOANBUSPRINCREATDT        DATE,
  LASTUPDTUSERID            VARCHAR2(32 BYTE)   DEFAULT USER,
  LASTUPDTDT                DATE                DEFAULT SYSDATE,
  LOANCNTLINTIND            CHAR(1 BYTE),
  LOANCNTLINTTYP            NUMBER,
  LOANBUSPRINPCTOWNRSHPBUS  NUMBER(10)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOAN;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOANACCT;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBUSPRINTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBUSPRINTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANAUDTBUSPRINTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBUSPRINTBL TO LOANUPDROLE;

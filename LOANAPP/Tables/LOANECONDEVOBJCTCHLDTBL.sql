CREATE TABLE LOANECONDEVOBJCTCHLDTBL
(
  LOANAPPNMB              NUMBER(10),
  LOANECONDEVOBJCTCD      CHAR(3 BYTE),
  LOANECONDEVOBJCTSEQNMB  NUMBER(3),
  CREATUSERID             VARCHAR2(32 BYTE),
  CREATDT                 DATE,
  LASTUPDTUSERID          VARCHAR2(32 BYTE),
  LASTUPDTDT              DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT INSERT, SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOAN;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOANACCT;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANECONDEVOBJCTCHLDTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANECONDEVOBJCTCHLDTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANECONDEVOBJCTCHLDTBL TO LOANUPDROLE;

GRANT SELECT ON LOANECONDEVOBJCTCHLDTBL TO POOLSECADMINROLE;

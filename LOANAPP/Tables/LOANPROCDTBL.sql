CREATE TABLE LOANPROCDTBL
(
  LOANAPPNMB                   NUMBER(10),
  LOANPROCDSEQNMB              NUMBER(5),
  LOANPROCDTYPCD               CHAR(2 BYTE),
  PROCDTYPCD                   CHAR(1 BYTE),
  LOANPROCDOTHTYPTXT           VARCHAR2(80 BYTE),
  LOANPROCDAMT                 NUMBER(19,4),
  LOANPROCDCREATUSERID         VARCHAR2(32 BYTE),
  LOANPROCDCREATDT             DATE,
  LASTUPDTUSERID               VARCHAR2(32 BYTE) DEFAULT USER,
  LASTUPDTDT                   DATE             DEFAULT SYSDATE,
  LOANPROCDREFDESCTXT          VARCHAR2(255 BYTE),
  LOANPROCDPURAGRMTDT          DATE,
  LOANPROCDPURINTANGASSETAMT   NUMBER(15,2),
  LOANPROCDPURINTANGASSETDESC  VARCHAR2(255 BYTE),
  LOANPROCDPURSTKHLDRNM        VARCHAR2(255 BYTE),
  NCAINCLDIND                  CHAR(1 BYTE),
  STKPURCORPTEXT               CHAR(1 BYTE)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANPROCDTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANPROCDTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANPROCDTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANPROCDTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANPROCDTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANPROCDTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANPROCDTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANPROCDTBL TO LOAN;

GRANT SELECT ON LOANPROCDTBL TO LOANACCT;

GRANT SELECT ON LOANPROCDTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANPROCDTBL TO LOANAPP;

GRANT SELECT ON LOANPROCDTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANPROCDTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANPROCDTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANPROCDTBL TO LOANREAD;

GRANT SELECT ON LOANPROCDTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANPROCDTBL TO LOANSERVLOANPRTUPLOAD;

GRANT SELECT ON LOANPROCDTBL TO LOANSERVSBICGOV;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANPROCDTBL TO LOANUPDROLE;

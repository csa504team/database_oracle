CREATE TABLE LQDCRROOTTBL
(
  LOANAPPNMB            NUMBER(10),
  LQDCRSEQNMB           NUMBER(10),
  LQDCRUSERID           CHAR(48 BYTE),
  LQDCROFFRNGID         CHAR(1 BYTE),
  LQDCRTRANSID          CHAR(36 BYTE),
  LQDCRSUBMTID          CHAR(36 BYTE),
  LQDCRSUBMTCROSSREFID  CHAR(255 BYTE),
  LQDCRCUSTID           CHAR(20 BYTE),
  LQDCRPRODCATID        CHAR(4 BYTE),
  LQDCRTIMESTAMP        DATE,
  LQDCRFORMATID         CHAR(20 BYTE),
  LQDCRLOANAMT          NUMBER(7),
  LQDCRLOANTERM         NUMBER(3),
  LQDCRLOANPYMTAMT      NUMBER(7),
  LQDCRLOANDOWNPYMTAMT  NUMBER(7),
  LQDCRLOANPURPS        CHAR(40 BYTE),
  LQDCRSTATCD           NUMBER(5),
  CREATUSERID           VARCHAR2(32 BYTE),
  CREATDT               DATE,
  LASTUPDTUSERID        VARCHAR2(32 BYTE)       DEFAULT USER,
  LASTUPDTDT            DATE                    DEFAULT SYSDATE,
  LQDCRTXNSTATCD        CHAR(3 BYTE),
  LQDCRTXNSTATTXT       VARCHAR2(255 BYTE)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LQDCRROOTTBL TO CNTRACHANNURI;

GRANT SELECT ON LQDCRROOTTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LQDCRROOTTBL TO CNTRNKUMARAS;

GRANT SELECT ON LQDCRROOTTBL TO CNTRNNAMILAE;

GRANT SELECT ON LQDCRROOTTBL TO CNTRNSALATOVA;

GRANT SELECT ON LQDCRROOTTBL TO CNTRRYALAVAR;

GRANT SELECT ON LQDCRROOTTBL TO CNTRSMSHONTH;

GRANT SELECT ON LQDCRROOTTBL TO LOAN;

GRANT SELECT ON LQDCRROOTTBL TO LOANACCT;

GRANT SELECT ON LQDCRROOTTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LQDCRROOTTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LQDCRROOTTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LQDCRROOTTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LQDCRROOTTBL TO LOANREADALLROLE;

GRANT SELECT ON LQDCRROOTTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LQDCRROOTTBL TO LOANUPDROLE;

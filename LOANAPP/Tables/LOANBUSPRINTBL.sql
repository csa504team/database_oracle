CREATE TABLE LOANBUSPRINTBL
(
  LOANAPPNMB                NUMBER(10),
  BORRSEQNMB                NUMBER(10),
  PRINSEQNMB                NUMBER(10),
  LOANPRINGNTYIND           CHAR(1 BYTE),
  LOANBUSPRINCREATUSERID    VARCHAR2(32 BYTE),
  LOANBUSPRINCREATDT        DATE,
  LASTUPDTUSERID            VARCHAR2(32 BYTE)   DEFAULT USER,
  LASTUPDTDT                DATE                DEFAULT SYSDATE,
  LOANCNTLINTIND            CHAR(1 BYTE),
  LOANCNTLINTTYP            NUMBER,
  LOANBUSPRINPCTOWNRSHPBUS  NUMBER(13,2)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANBUSPRINTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANBUSPRINTBL TO CNTRSMSHONTH;

GRANT SELECT, UPDATE ON LOANBUSPRINTBL TO LOAN;

GRANT SELECT ON LOANBUSPRINTBL TO LOANACCT;

GRANT SELECT ON LOANBUSPRINTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANBUSPRINTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANBUSPRINTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANBUSPRINTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANBUSPRINTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANBUSPRINTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANBUSPRINTBL TO LOANUPDROLE;

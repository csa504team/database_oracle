CREATE TABLE TEMPAPK
(
  LOANAPPETRANFILE  VARCHAR2(4000 BYTE)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON TEMPAPK TO CNTRACHANNURI;

GRANT SELECT ON TEMPAPK TO CNTRDFREDERICKS;

GRANT SELECT ON TEMPAPK TO CNTRNKUMARAS;

GRANT SELECT ON TEMPAPK TO CNTRNSALATOVA;

GRANT SELECT ON TEMPAPK TO CNTRRYALAVAR;

GRANT SELECT ON TEMPAPK TO CNTRSMSHONTH;

GRANT SELECT ON TEMPAPK TO LOANAPPREADALLROLE;

GRANT SELECT ON TEMPAPK TO LOANSERVLOANPRTUPLOAD;

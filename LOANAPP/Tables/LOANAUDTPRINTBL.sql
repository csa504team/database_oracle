CREATE TABLE LOANAUDTPRINTBL
(
  LOANAPPNMB                   NUMBER(10),
  LOANAUDTSEQNMB               NUMBER(10),
  PRINSEQNMB                   NUMBER(10),
  TAXID                        CHAR(10 BYTE),
  PRINBUSPERIND                CHAR(1 BYTE),
  VETCD                        NUMBER(5),
  GNDRCD                       CHAR(1 BYTE),
  ETHNICCD                     CHAR(2 BYTE),
  LOANPRINPRIMBUSEXPRNCEYRNMB  NUMBER(5),
  LOANPRININSURNM              VARCHAR2(80 BYTE),
  LOANPRININSURAMT             NUMBER(19,4),
  PRINCREATUSERID              VARCHAR2(32 BYTE),
  PRINCREATDT                  DATE,
  LASTUPDTUSERID               VARCHAR2(32 BYTE) DEFAULT USER,
  LASTUPDTDT                   DATE             DEFAULT SYSDATE,
  LACGNTYCONDTYPCD             NUMBER(3),
  GNTYSUBCONDTYPCD             NUMBER(3),
  GNTYLMTAMT                   NUMBER(15,2),
  GNTYLMTPCT                   NUMBER(6,3),
  LOANCOLLATSEQNMB             NUMBER(10),
  GNTYLMTYRNMB                 NUMBER(4),
  LOANCOMPTITRNM               VARCHAR2(255 BYTE),
  LOANPRINNONCAEVDNCIND        CHAR(1 BYTE),
  LOANLIFEINSURRQRDIND         CHAR(1 BYTE),
  LOANINSURDISBLDESCTXT        VARCHAR2(255 BYTE),
  LOANDISBLINSURRQRDIND        CHAR(1 BYTE)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTPRINTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANAUDTPRINTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANAUDTPRINTBL TO LOAN;

GRANT SELECT ON LOANAUDTPRINTBL TO LOANACCT;

GRANT SELECT ON LOANAUDTPRINTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAUDTPRINTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRINTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRINTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAUDTPRINTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANAUDTPRINTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRINTBL TO LOANUPDROLE;

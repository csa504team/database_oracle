CREATE TABLE LOANVALIDATIONERRTBL_050316
(
  ERRCD                    NUMBER(10),
  ERRTYP                   NUMBER(5),
  ERRTXT                   VARCHAR2(255 BYTE),
  LOANVALIDERRCREATUSERID  VARCHAR2(32 BYTE),
  LOANVALIDERRCREATDT      DATE,
  LASTUPDTUSERID           VARCHAR2(32 BYTE),
  LASTUPDTDT               DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANVALIDATIONERRTBL_050316 TO CNTRNKUMARAS;

GRANT SELECT ON LOANVALIDATIONERRTBL_050316 TO CNTRRYALAVAR;

GRANT SELECT ON LOANVALIDATIONERRTBL_050316 TO CNTRSMSHONTH;

GRANT SELECT ON LOANVALIDATIONERRTBL_050316 TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANVALIDATIONERRTBL_050316 TO LOANSERVLOANPRTUPLOAD;

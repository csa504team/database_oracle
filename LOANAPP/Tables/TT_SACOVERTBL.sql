CREATE GLOBAL TEMPORARY TABLE TT_SACOVERTBL
(
  LOANAPPNMB      INTEGER,
  LOANAPPNM       VARCHAR2(80 BYTE),
  RVWRNM          VARCHAR2(40 BYTE),
  HRSOVRDUE       INTEGER,
  LASTUPDTUSERID  VARCHAR2(32 BYTE)             DEFAULT USER,
  LASTUPDTDT      DATE                          DEFAULT SYSDATE
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TT_SACOVERTBL TO CNTRACHANNURI;

GRANT SELECT ON TT_SACOVERTBL TO CNTRDFREDERICKS;

GRANT SELECT ON TT_SACOVERTBL TO CNTRNKUMARAS;

GRANT SELECT ON TT_SACOVERTBL TO CNTRNNAMILAE;

GRANT SELECT ON TT_SACOVERTBL TO CNTRNSALATOVA;

GRANT SELECT ON TT_SACOVERTBL TO CNTRRYALAVAR;

GRANT SELECT ON TT_SACOVERTBL TO CNTRSMSHONTH;

GRANT SELECT ON TT_SACOVERTBL TO LOANAPPREADALLROLE;

GRANT SELECT ON TT_SACOVERTBL TO LOANSERVLOANPRTUPLOAD;

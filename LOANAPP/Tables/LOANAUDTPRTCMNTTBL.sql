CREATE TABLE LOANAUDTPRTCMNTTBL
(
  LOANAPPNMB              NUMBER(10),
  LOANAUDTSEQNMB          NUMBER(10),
  LOANPRTCMNTTXT          CLOB,
  LOANPRTCMNTCREATUSERID  VARCHAR2(32 BYTE),
  LOANPRTCMNTCREATDT      DATE,
  LASTUPDTUSERID          VARCHAR2(32 BYTE)     DEFAULT USER,
  LASTUPDTDT              DATE                  DEFAULT SYSDATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOAN;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOANACCT;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRTCMNTTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRTCMNTTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANAUDTPRTCMNTTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTPRTCMNTTBL TO LOANUPDROLE;

GRANT INSERT, SELECT, UPDATE ON LOANAUDTPRTCMNTTBL TO STAGE;

CREATE TABLE LOANPRTCMNTTBL_11252012
(
  LOANAPPNMB              NUMBER(10),
  LOANPRTCMNTSEQNMB       NUMBER(5),
  LOANPRTCMNTTXT          VARCHAR2(255 CHAR),
  LOANPRTCMNTCREATUSERID  VARCHAR2(32 BYTE),
  LOANPRTCMNTCREATDT      DATE,
  LASTUPDTUSERID          VARCHAR2(32 BYTE),
  LASTUPDTDT              DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRACHANNURI;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRDFREDERICKS;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRNKUMARAS;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRNSALATOVA;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRRYALAVAR;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO CNTRSMSHONTH;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANPRTCMNTTBL_11252012 TO LOANSERVLOANPRTUPLOAD;

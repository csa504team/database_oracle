CREATE TABLE LOANAUDTBORRTBL
(
  LOANAPPNMB            NUMBER(10),
  LOANAUDTSEQNMB        NUMBER(10),
  BORRSEQNMB            NUMBER(10),
  TAXID                 CHAR(10 BYTE),
  LOANBUSPRIMBORRIND    CHAR(1 BYTE),
  BORRBUSPERIND         CHAR(1 BYTE),
  IMEPCOPERCD           NUMBER(5),
  BORRCREATUSERID       VARCHAR2(32 BYTE),
  BORRCREATDT           DATE,
  LASTUPDTUSERID        VARCHAR2(32 BYTE)       DEFAULT USER,
  LASTUPDTDT            DATE                    DEFAULT SYSDATE,
  LOANCNTLINTIND        CHAR(1 BYTE),
  LOANCNTLINTTYP        NUMBER,
  LIABINSURRQRDIND      CHAR(1 BYTE),
  PRODLIABINSURRQRDIND  CHAR(1 BYTE),
  LIQLIABINSURRQRDIND   CHAR(1 BYTE),
  MALPRCTSINSURRQRDIND  CHAR(1 BYTE),
  OTHINSURRQRDIND       CHAR(1 BYTE),
  OTHINSURDESCTXT       VARCHAR2(255 BYTE),
  PYMTLESSCRDTIND       CHAR(1 BYTE),
  ETHNICCD              CHAR(1 BYTE),
  GNDRCD                CHAR(1 BYTE),
  RECRDSTMTTYPCD        NUMBER(3),
  RECRDSTMTYRENDDAYNMB  NUMBER(3),
  WORKRSCOMPINSRQRDIND  CHAR(1 BYTE)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANAUDTBORRTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANAUDTBORRTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANAUDTBORRTBL TO LOAN;

GRANT SELECT ON LOANAUDTBORRTBL TO LOANACCT;

GRANT SELECT ON LOANAUDTBORRTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANAUDTBORRTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBORRTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBORRTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANAUDTBORRTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANAUDTBORRTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANAUDTBORRTBL TO LOANUPDROLE;

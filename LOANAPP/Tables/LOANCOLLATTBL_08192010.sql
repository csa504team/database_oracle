CREATE TABLE LOANCOLLATTBL_08192010
(
  LOANAPPNMB             NUMBER(10),
  LOANCOLLATSEQNMB       NUMBER(5),
  LOANCOLLATTYPCD        NUMBER(5),
  LOANCOLLATDESC         VARCHAR2(255 BYTE),
  LOANCOLLATMRKTVALAMT   NUMBER(19,4),
  LOANCOLLATOWNRRECRD    VARCHAR2(80 BYTE),
  LOANCOLLATLQDVALAMT    NUMBER(19,4),
  LOANCOLLATSBALIENPOS   NUMBER(5),
  LOANCOLLATVALSOURCCD   NUMBER(5),
  LOANCOLLATVALDT        DATE,
  LOANCOLLATCREATUSERID  CHAR(15 BYTE),
  LOANCOLLATCREATDT      DATE,
  LASTUPDTUSERID         CHAR(15 BYTE),
  LASTUPDTDT             DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRACHANNURI;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRDFREDERICKS;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRNKUMARAS;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRNSALATOVA;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRRYALAVAR;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO CNTRSMSHONTH;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANCOLLATTBL_08192010 TO LOANSERVLOANPRTUPLOAD;

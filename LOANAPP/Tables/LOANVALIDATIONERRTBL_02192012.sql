CREATE TABLE LOANVALIDATIONERRTBL_02192012
(
  ERRCD                    NUMBER(10),
  ERRTYP                   NUMBER(5),
  ERRTXT                   VARCHAR2(255 BYTE),
  LOANVALIDERRCREATUSERID  CHAR(15 BYTE),
  LOANVALIDERRCREATDT      DATE,
  LASTUPDTUSERID           CHAR(15 BYTE),
  LASTUPDTDT               DATE
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRACHANNURI;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRDFREDERICKS;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRNKUMARAS;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRNSALATOVA;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRRYALAVAR;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO CNTRSMSHONTH;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO LOANAPPREADALLROLE;

GRANT SELECT ON LOANVALIDATIONERRTBL_02192012 TO LOANSERVLOANPRTUPLOAD;

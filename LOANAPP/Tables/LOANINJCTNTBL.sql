CREATE TABLE LOANINJCTNTBL
(
  LOANAPPNMB               NUMBER(10),
  LOANINJCTNSEQNMB         NUMBER(10),
  INJCTNTYPCD              CHAR(1 BYTE),
  LOANINJCTNOTHDESCTXT     VARCHAR2(255 BYTE),
  LOANINJCTNAMT            NUMBER(19,4),
  LOANINJCTNCREATUSERID    VARCHAR2(32 BYTE),
  LOANINJCTNCREATDT        DATE,
  LASTUPDTUSERID           VARCHAR2(32 BYTE)    DEFAULT USER,
  LASTUPDTDT               DATE                 DEFAULT SYSDATE,
  LOANINJCTNFUNDTERMYRNMB  NUMBER(3)
)
LOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL;


GRANT SELECT ON LOANINJCTNTBL TO CNTRACHANNURI;

GRANT SELECT ON LOANINJCTNTBL TO CNTRDFREDERICKS;

GRANT SELECT ON LOANINJCTNTBL TO CNTRNKUMARAS;

GRANT SELECT ON LOANINJCTNTBL TO CNTRNNAMILAE;

GRANT SELECT ON LOANINJCTNTBL TO CNTRNSALATOVA;

GRANT SELECT ON LOANINJCTNTBL TO CNTRRYALAVAR;

GRANT SELECT ON LOANINJCTNTBL TO CNTRSMSHONTH;

GRANT SELECT ON LOANINJCTNTBL TO LOAN;

GRANT SELECT ON LOANINJCTNTBL TO LOANACCT;

GRANT SELECT ON LOANINJCTNTBL TO LOANACCTREADALLROLE;

GRANT SELECT ON LOANINJCTNTBL TO LOANAPPREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANINJCTNTBL TO LOANAPPUPDAPPROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANINJCTNTBL TO LOANPOSTSERVSUROLE;

GRANT SELECT ON LOANINJCTNTBL TO LOANREADALLROLE;

GRANT SELECT ON LOANINJCTNTBL TO LOANSERVLOANPRTUPLOAD;

GRANT DELETE, INSERT, SELECT, UPDATE ON LOANINJCTNTBL TO LOANUPDROLE;

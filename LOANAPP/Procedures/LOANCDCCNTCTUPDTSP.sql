CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTUPDTSP (p_Identifier           IN     NUMBER DEFAULT 0,
                                                        P_Loanappnmb           IN     NUMBER DEFAULT NULL,
                                                        P_CDCCntctFullNm       IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctphone        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctemailadr     IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctfax          IN     VARCHAR2 DEFAULT NULL,
                                                        P_CLSAttyFullNm        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyphone         IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyemailadr      IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyfax           IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattysbadesigind   IN     VARCHAR2 DEFAULT NULL,
                                                        P_Debauthdt            IN     DATE DEFAULT   SYSDATE
                                                                                                   - 1000,
                                                        P_Fileorderdt          IN     DATE DEFAULT   SYSDATE
                                                                                                   - 1000,
                                                        P_Lastupdtuserid       IN     VARCHAR2 DEFAULT USER,
                                                        P_Lastupdtdt           IN     DATE DEFAULT SYSDATE,
                                                        p_RetVal                  OUT NUMBER)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    SAVEPOINT LOANCDCCNTCTUPDTSP;

    /* To update LOANCDCCNTCTTBL */
    IF p_Identifier = 0 THEN
        UPDATE LOANAPP.LOANCDCCNTCTTBL
           SET CDCCntctFullNm = NVL (P_CDCCntctFullNm, CDCCntctFullNm),
               CdccntctPhone = NVL (P_Cdccntctphone, Cdccntctphone),
               CdccntctEmailadr = NVL (P_Cdccntctemailadr, Cdccntctemailadr),
               CdccntctFax = NVL (P_Cdccntctfax, Cdccntctfax),
               CLSAttyFullNm = NVL (P_CLSAttyFullNm, CLSAttyFullNm),
               ClsattyPhone = NVL (P_Clsattyphone, Clsattyphone),
               ClsattyEmailadr = NVL (P_Clsattyemailadr, Clsattyemailadr),
               ClsattyFax = NVL (P_Clsattyfax, Clsattyfax),
               ClsattySBAdesigind = NVL (P_Clsattysbadesigind, Clsattysbadesigind),
               DebAuthdt = NVL (P_Debauthdt, DebAuthdt),
               FileOrderdt = NVL (P_Fileorderdt, P_Fileorderdt),
               Lastupdtuserid = P_Lastupdtuserid,
               Lastupdtdt = P_Lastupdtdt
         WHERE LoanAppNmb = P_LoanAppNmb;
    ELSIF p_Identifier = 1 THEN
        UPDATE LOANAPP.LOANCDCCNTCTTBL
           SET CDCCntctFullNm = P_CDCCntctFullNm,
               CdccntctPhone = P_Cdccntctphone,
               CdccntctEmailadr = P_Cdccntctemailadr,
               CdccntctFax = P_Cdccntctfax,
               CLSAttyFullNm = P_CLSAttyFullNm,
               ClsattyPhone = P_Clsattyphone,
               ClsattyEmailadr = P_Clsattyemailadr,
               ClsattyFax = P_Clsattyfax,
               ClsattySBAdesigind = P_Clsattysbadesigind,
               DebAuthdt = P_Debauthdt,
               FileOrderdt = P_Fileorderdt,
               Lastupdtuserid = P_Lastupdtuserid,
               Lastupdtdt = P_Lastupdtdt
         WHERE LoanAppNmb = P_LoanAppNmb;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
        ROLLBACK TO LOANCDCCNTCTUPDTSP;
END LOANCDCCNTCTUPDTSP;
/
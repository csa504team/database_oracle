CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATESUBPROCDCSP(
   p_RetVal             OUT NUMBER,
   p_LoanAppNmb      IN     NUMBER DEFAULT NULL,
   p_ErrSeqNmb       IN OUT NUMBER,
   p_TransInd        IN     NUMBER DEFAULT NULL,
   p_LoanAppRecvDt   IN     DATE DEFAULT NULL)
AS
cursor fetch_procd_rec is
select LOANPROCDSEQNMB,LoanProcdAmt,ProcdTypcd,LoanProcdTypCd  FROM LOANAPP.LOANPROCDTBL WHERE LoanAppNmb = p_LoanAppNmb;

sub_recs_sum number:=0;          

     	
BEGIN
    SAVEPOINT VALIDATESUBPROCDCSP;
    
    for rec in fetch_procd_rec
    loop
        select nvl(sum(LOANPROCDAMT),0) into sub_recs_sum from LOANAPP.LOANSUBPROCDTBL where 
        LOANPROCDTYPCD=rec.LOANPROCDTYPCD and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= rec.PROCDTYPCD;         	
             IF rec.PROCDTYPCD = 'E' AND sub_recs_sum != rec.LoanProcdAmt and sub_recs_sum !=0 THEN
                p_ErrSeqNmb := ADD_ERROR (p_LoanAppNmb,
                                          p_ErrSeqNmb,
                                          4289,
                                          p_TransInd,
                                          TO_CHAR (rec.PROCDTYPCD)||TO_CHAR (rec.LOANPROCDTYPCD),
                                          TO_CHAR (rec.LOANPROCDSEQNMB),
                                          rec.LoanProcdAmt,
                                          null,
                                          NULL,
                                          null);
             END IF;
    end loop;
            p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
END;
/

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANLANAUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATESUBPROCDCSP TO UPDLOANROLE;

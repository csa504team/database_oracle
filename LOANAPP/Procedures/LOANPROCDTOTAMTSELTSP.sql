CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDTOTAMTSELTSP (
    p_Identifier       NUMBER := 0,
    p_RetVal       OUT NUMBER,
    p_LoanAppNmb       NUMBER := 0,
    p_LoanProcdTypcd   CHAR:=NULL,
    p_ProcdTypcd       CHAR:=NULL,
    check_rec_val       OUT number)
AS
cursor check_rec
    is
        SELECT LoanProcdAmt FROM LOANAPP.LOANPROCDTBL
                        	WHERE LoanAppNmb = p_LoanAppNmb AND PROCDTYPCD = p_ProcdTypcd and LOANPROCDTYPCD=p_LoanProcdTypcd ;        
                        	
cursor check_rec_1
    is
        SELECT nvl(sum(LOANPROCDAMT),0) As TotalAmount from LOANAPP.LOANSUBPROCDTBL where LOANPROCDTYPCD=p_LoanProcdTypcd and LOANAPPNMB=p_LoanAppNmb and PROCDTYPCD= p_ProcdTypcd;         	
                        	
BEGIN
    SAVEPOINT LOANPROCDTOTAMTSELTSP;

    /* Select from LOANAPP.LOANPROCDTBL Table */
    IF p_Identifier = 0
    THEN
        check_rec_val:=0;
    	open check_rec;
        fetch check_rec
        into  check_rec_val;
        IF nvl(check_rec_val,0)=0 then
				BEGIN
                    open check_rec_1;
                    fetch check_rec_1
                    into  check_rec_val;		
				END;

		END IF;
			
        p_RetVal := SQL%ROWCOUNT;
    END IF;
    p_RetVal := NVL (p_RetVal, 0);

EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDTOTAMTSELTSP;
            RAISE;
        END;
END LOANPROCDTOTAMTSELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDTOTAMTSELTSP TO POOLSECADMINROLE;

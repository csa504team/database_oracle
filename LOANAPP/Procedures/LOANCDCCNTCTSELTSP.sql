CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTSELTSP (p_Identifier   IN     NUMBER DEFAULT 0,
                                                        p_LoanAppNmb   IN     NUMBER DEFAULT NULL,
                                                        p_RetVal          OUT NUMBER,
                                                        p_SelCur          OUT SYS_REFCURSOR)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    IF p_Identifier = 0 THEN
        /* Select Row On the basis of Key*/
        OPEN p_SelCur FOR SELECT LoanAppNmb,
                                 CDCCntctFullNm,
                                 CdccntctPhone,
                                 CdccntctEmailadr,
                                 CdccntctFax,
                                 CLSAttyFullNm,
                                 ClsattyPhone,
                                 ClsattyEmailadr,
                                 ClsattyFax,
                                 ClsattySBAdesigind,
                                 DebAuthdt,
                                 FileOrderdt,
                                 CreatUserid,
                                 CreatDt                                                                   --,
                            -- Lastupdtuserid,
                            --  Lastupdtdt
                            FROM LOANAPP.LOANCDCCNTCTTBL
                           WHERE LoanAppNmb = p_LoanAppNmb;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
END LOANCDCCNTCTSELTSP;
/